# General installation notes

The installation scripts assume that there is a connection to the internet and that commands such
as `wget`, `git` and `pip` will be able to access public internet servers. If you are behind a
proxy then see below.

## Sample proxy settings

If you are behind a proxy then you are likely to need to include the following in your settings
for the duration of the installation process.

*If you make the modifications below please keep in mind file permissions (e.g use 
`chmod go-rwx <file>` !) to avoid inadvertent disclosure of your password!*

`~/.wgetrc`

```
http_proxy=http://<user>:<password>@<proxy>:<proxy port>
https_proxy=https://<user>:<password>@<proxy>:<proxy port>
use_proxy=on
```

`~/.gitconfig`

```
[https]
    proxy = https://<user>:<password>@<proxy>:<proxy port>
```

Python

You'll need to add the following to the `1chaste_libs\buildscript\python.sh` install script.

```
export http_proxy=http://<user>:<password>@<proxy>:<proxy port>
export https_proxy=https://<user>:<password>@<proxy>:<proxy port>
```

## Presence of python-rtslib and "ValueError: invalid version number: 'GIT_VERSION'" *hack*

It seems that /usr/lib/python2.6/site-packages/rtslib-GIT_VERSION-py2.6.egg-info can cause some
bother for the 4Suite-XML-1.0.2 installation. If this library exists (it doesn't in the minimum 
install CentOS) then please use the files
[here](https://bitbucket.org/gef_work/ap_predict_online/src/master/tools/ApPredict/install/x86_64/CentOS/bonus/python/1/ "Special Python install") during the installation of chaste-libs.