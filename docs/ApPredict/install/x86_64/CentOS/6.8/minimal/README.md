# Example installation of `ApPredict` (including [Chaste](https://chaste.cs.ox.ac.uk/ "Chaste home") and its dependent libraries) on a minimal 64-bit [CentOS](https://www.centos.org/ "CentOS home").

The instructions for [CentOS 6.5](https://bitbucket.org/gef_work/ap_predict_online/src/master/docs/ApPredict/install/x86_64/CentOS/6.5/minimal/)
are also applicable to version 6.8.