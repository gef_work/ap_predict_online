# Example installation of `ApPredict` (including [Chaste](https://chaste.cs.ox.ac.uk/ "Chaste home") and its dependent libraries) on a minimal 64-bit [CentOS](https://www.centos.org/ "CentOS home").

(For information on `AP-Portal` (i.e. the web serving part of the application) installation see 
[here](https://bitbucket.org/gef_work/ap_predict_online/src/master/docs/AP-Portal/install/).

The following is a sequence for installing `ApPredict` on the 7.3 CentOS version.

`ApPredict` is explained [here](https://chaste.cs.ox.ac.uk/trac/wiki/ApPredict "cs.ox.ac.uk - ApPredict project")
as being a 'bolt-on' project of the Chaste software package. `ApPredict` and Chaste are primarily
written in C++ but Chaste does use languages such as Python. An `ApPredict` binary is 
compiled using the technique below (as an example) and generally takes IC50/pIC50/Hill Coefficient
values as simulation input to determine expected Action Potentials at certain compound concentrations.  

The scripts used to build Chaste and it's dependent libraries (generally referred to as
'chaste-libs') are derived from the documentation at 
[InstallGuide](https://chaste.cs.ox.ac.uk/trac/wiki/InstallGuides/InstallGuide "cs.ox.ac.uk - Chaste InstallGuide"),
[DependencyVersions](https://chaste.cs.ox.ac.uk/trac/wiki/InstallGuides/DependencyVersions "cs.ox.ac.uk - Chaste DependencyVersions"), and
[AccessCodeRepository](https://chaste.cs.ox.ac.uk/trac/wiki/ChasteGuides/AccessCodeRepository "cs.ox.ac.uk - Chaste AccessCodeRepository").
One difference between the technique in the above resources compared to the technique below is that
below, all the dependent libraries (and binaries, includes, shares, etc), when built, will be
colocated in subdirectories of a single 'chaste-libs' directory.

As the `AP-Portal` does not make use of Chaste's visualisation facilities, e.g. VTK, these will
not be built.

## General installation notes.

Please quickly look at the 
[general installation notes](https://bitbucket.org/gef_work/ap_predict_online/src/master/docs/ApPredict/install/ "General installation notes").

## Minimum hardware requirements for build/deploy/run.

Note: These are the requirements for `ApPredict` when used by the `AP-Portal` as the portal may generate up to 
15 concurrent `ApPredict` invocations for a single compound (for example for three pacing frequencies when the 
compound had (p)IC50 data for five assays). Normally a single `ApPredict` invocation will consume a processor/core
for approx 4 to 10 minutes if using a processor made around 2015 - the actual time taken depends a lot on the
nature of the computation, e.g. the cell model, the time to reach 'steady state'.

 1. Internet access (or some way of copying downloaded files to the deployment machine).
 1. Processor/Core(s) : In theory just 1! In reality at least 4, and preferably 8-16.
 1. Hard disk space : 16 Gb.
    1. Chaste dependency libs/bins/includes/etc. : Approx 400Mb (up to 800Mb during build).
    1. Chaste + ApPredict source code : Total Approx 2Gb.
 1. RAM : 4 Gb : Particularly if using multiple processors/cores during Chaste build.

## Install CentOS.

In my case I've been testing this installation procedure out using `VirtualBox`, simply pointing to
the downloaded `.iso` when first booting a new VM.

 * Visit (for example) [CentOS ISOs mirror](http://mirror.ox.ac.uk/sites/mirror.centos.org/7/isos/x86_64/ "CentOS ISO repository").
 * Select `CentOS-7-x86_64-Minimal-1611.iso` to download.
 * Do checksums.
 * Install the downloaded '.iso'.

## Once-only admin-level package installation and tasks.

Chaste needs a few minimal things to be installed beforehand.

 * Optional temporary networking setup : [Example instructions](https://lintut.com/how-to-setup-network-after-rhelcentos-7-minimal-installation/)
 * `yum install wget gcc-c++ gcc-gfortran cmake patch python-devel python-setuptools python-dateutil git libxslt-devel bzip2`.  
   (`python-dateutil` added Apr '19 as otherwise Amara install fails due  "... your setuptools is
   too old" may appear!)

## Build Chaste's library dependencies 'chaste-libs' beneath <chaste-libs-base>.

**Estimated time to build** : 30 minutes.  
**Desired structure** : Simular to the following :
```
<chaste-libs-base>
       |
       +----------/chaste-libs
                        |
                        +-----/170717
                        |         +--/bin
                        |         |    +-/mpicc
                        |         +--/etc
                        |         +--/include
                        |         |      +---/HYPRE.h
                        |         +--/lib
                        |         |    +-/libboost_filesystem.so
                        |         |    +-/libhdf5.so
                        |         |    +-/python2.7/site-packages
                        |         |                        +-----/4Suite_XML-1.0.2-py2.7-linux-x86_64.egg
                        |         +--/lib64
                        |         |     +--/python2.7/site-packages
                        |         |                          +-----/lxml-3.4.4-py2.7-linux-x86_64.egg
                        |         +--/share
                        +-----/180730
                                  +--
```

 1. `cd <any empty temporary directory>`
 1. Download [chaste-lib build scripts](https://bitbucket.org/gef_work/ap_predict_online/src/master/tools/ApPredict/install/x86_64/CentOS/7.3/minimal/1chaste_libs/), retaining directory structure.  
    (Explanation [here](http://stackoverflow.com/questions/7106012/download-a-single-folder-or-directory-from-a-github-repo) for why it's unfortunately not a one-line option!).
 1. `cd 1chaste_libs`
 1. `./1install.sh <chaste-libs-base> <processor count>`, e.g. `./1install.sh ${HOME} 8`  
   NOTE: The python install may fail with a "TypeError: 'NoneType' object is not callable" message.
         If it does, re-run the command.
 1. `cd ..`
 1. `rm -rf 1chaste_libs`

All the Chaste dependencies should now be installed in a subdirectory of <chaste-libs-base>.

## Build `ApPredict` binary executable.

**Estimated time to build** : 90 minutes (when built using 8 processors, i.e. `-j8` below).

 1. `cd <git>`, e.g. `cd ${HOME}/git`
 1. Download [Chaste build scripts](https://bitbucket.org/gef_work/ap_predict_online/src/master/tools/ApPredict/install/x86_64/CentOS/7.3/minimal/2chaste/) content.
 1. Run `PATH=<chaste-libs>/bin:${PATH} ./1build.sh <chaste-libs> <processor count>`  
    e.g. `PATH=${HOME}/chaste-libs/170717/bin:${PATH} ./1build.sh ${HOME}/chaste-libs/170717/ 8`  
    (Note: As of 25th Oct. 2019 this will checkout Chaste 
    [fa6617c](https://github.com/Chaste/Chaste/commit/fa6617cd23373091eb9d215714fe6ec411cdbaab)
    and the most recent revision of `ApPredict` -- adjust
    [1build.sh](https://bitbucket.org/gef_work/ap_predict_online/src/master/tools/ApPredict/install/x86_64/CentOS/7.3/minimal/2chaste/1build.sh)
    to checkout alternative revisions. This is due to a switch to 
    [c++14 compiler flags](https://github.com/Chaste/Chaste/commit/9778de6c95c354810d4b3a0cbb17709879eda7ae)
    not readily available in CentOS 7.x.)

## Test `ApPredict` binary executable.

 1. `cd <git>`, e.g. `cd ${HOME}/git`
 1. Run `./2runtest.sh <chaste-libs>/lib/`, e.g. `./2runtest.sh ${HOME}/chaste-libs/170717/lib/`

## Install `ApPredict` binary executable.

**Estimated time to install** : A few minutes.  
**Desired structure** :
```
<app-base>
     |
     +----/appredict
               |----/shannon_wang_puglisi_weber_bers_2004_model_updated_4d_hERG_IKs_INa_ICaL_0.5Hz_generator.arch
               |----/shannon_wang_puglisi_weber_bers_2004_model_updated_4d_hERG_IKs_INa_ICaL_1Hz_generator.arch
               +----/170717
               |        +--/ApPredict
               |        +--/ApPredict.sh
               |        +--/libsg
               |              +-/libApPredict.so
               |              +-/libcontinuum_mechanics.so
               |              +-/libglobal.so
               |              +-/libheart.so
               |              +-/libio.so
               |              +-/liblinalg.so
               |              +-/libmesh.so
               |              +-/libode.so
               |              +-/libpde.so
               +----/180730
                        +--/ApPredict
                        +..
```

 1. `cd <git>`, e.g. `cd ${HOME}/git`
 1. Run `./3install.sh <app-base> <chaste-libs>/lib/`,  
    e.g. `./3install.sh ${HOME}/apps ${HOME}/chaste-libs/170717/lib/` would create a date-specific
    subdirectory, e.g. `${HOME}/apps/appredict/170717`.
 1. `rm 1build.sh 2runtest.sh 3install.sh ApPredict.sh default.py.patch* README.md SCons*`
 1. `3install.sh` only downloads two of the available emulators - to retrieve them all and generate
    binaries, use
    [these scripts](https://bitbucket.org/gef_work/ap_predict_online/src/master/tools/ApPredict/emulators/).