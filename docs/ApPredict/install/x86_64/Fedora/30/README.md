# Fedora 30

## HDF5 install issue.

The Fedora 20 build scripts had used hdf5-1.8.10-patch1. Instead
[hdf5-1.10.6](https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.10/hdf5-1.10.6/src/)
should now be used (although verify the md5 checksum!).

## Python install issue

On Fedora 26 it was possible to package-install `python-4Suite-XML-1.0.2-24.fc26.x86_64` and 
`python-amara-1.2.0.2-18.fc26.noarch` but in Fedora 30 I *would* have tried :

`yum install python2-amara python2-4suite-xml`

However a [quick check](https://lists.fedoraproject.org/archives/list/devel@lists.fedoraproject.org/thread/2O2PTTYRY73QCG452HC5CO4CPUTQX3FA/)
indicates that since 2018 (so approx F30/"Rawhide") `python2-4suite-xml` may have been "retired".
Instead you could install from source, although I ran into a syntax problem and ended up
using the `StreamWriter.c.diff` patch in this directory based on
[this commit](https://src.fedoraproject.org/rpms/python-4Suite-XML/c/752e8d220c39c95d7a3b20404e7c09c010bc028b?branch=master).

Thereafter I also ended up doing a `yum install pyparsing` which installed
`python2-pyparsing-2.4.0-1.fc30.noarch.rpm`. 