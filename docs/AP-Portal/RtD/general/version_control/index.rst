.. include:: ../../global.rst

.. _version_control_conflicts:

Conflicts due to :term:`local` file changes
-------------------------------------------

If version-controlled files or directory structures are changed locally, e.g.
file content changed or new files/directories added, then this potentially
causes conflicts if |AP-Portal| is updated to the latest release of the
software.