.. include:: ../global.rst

.. _tools_propertyfileencrypt:

Property file {de,en}cryption
==============================

 #. Prior to early 2019 the |client| and |client-direct| components were able to
    use `Jasypt` encryption but since that time some dependency library updating
    for security purposes has meant that with |Spring| v.5 it was no longer
    possible (or rather, with there being no `Jasypt` activity since 2014 it was
    decided not to continue using it).

 #. Download and unpack http://www.jasypt.org/download.html (current version is
    1.9.2) and run the relevant ``bin`` directory ``.bat`` or ``.sh`` scripts.
    (|AP-Portal| uses the ``org.jasypt.encryption.pbe.StandardPBEStringEncryptor``
    class, as do the scripts).

    ::

      encrypt.sh input=<property file value> password=<jasypt encrypting pwd>

    e.g. ``./encrypt.sh input=fish password=chips``.

    Note: You may prefer the above command containing your password not to appear in your shell
    history, in which case various techniques are available, e.g. check 
    `stackoverflow <http://stackoverflow.com/questions/640403/can-you-prevent-a-command-from-going-into-the-bash-shell-command-history>`_.

 #. The output generated then needs to be placed into the relevant ``.properties``
    file, e.g.

    Sample ``encrypt.sh`` output... ::

      ----ARGUMENTS-------------------

      input: fish
      password: chips

      ----OUTPUT----------------------

      MHWlsEFq4rI/Rx7s1H27pg==

    ... is placed into a ``spring.properties``::

      securement.business.password=ENC(MHWlsEFq4rI/Rx7s1H27pg==)

 #. ``<jasypt encrypting pwd>`` needs to be assigned to the environment variable
    ``JASYPT_PWD`` prior to the :doc:`component </installation/components/index>`
    being started, e.g. a component's start file may look something like::

      export JASYPT_PWD=chips
      ./bin/catalina.sh start