.. include:: ../global.rst

.. _tools_tomcat:

.. note:: The instructions below are for a version of Tomcat which by the time
   you read this may have security issues!

Tomcat
======

Tomcat's website is `here <http://tomcat.apache.org/>`_

Example install
---------------

.. note:: Other servlet containers can be used - there's nothing Tomcat-specific
   in the codebase, but alternatives haven't been tested.

#. Estimated time to build : 2 minutes.
#. Desired structure : Similar (eventually) to the following, based on the 
   "Advanced Configuration - Multiple Tomcat Instances" section of
   `this document <https://tomcat.apache.org/tomcat-7.0-doc/RUNNING.txt>`_.

::

 <app-base>
     |
     +----/tomcat
              +---/CATALINA_HOME
              |          +------/app-manager -> ../vhosts/app-manager
              |          +------/app-manager.env
              |          +------/bin/catalina.sh
              |          |        +-/startup.sh
              |          +------/client -> ../vhosts/client
              |          +------/client.env
              |          +------/client-direct -> ../vhosts/client-direct
              |          +------/client-direct.env
              |          +------/host.pkcs12
              |          +------/lib/catalina.jar
              |          |        +-/servlet-api.jar
              |          |        +-/tomcat7-websocket.jar
              |          +------/local_runner.sh
              |          +------/logs/
              |          +------/prepare.sh
              |          +------/procdir/
              |          +------/README.openssl
              |          +------/reset_all.sh
              |          +------/reset.sh
              |          +------/secure.env
              |          +------/start_all.sh
              |          +------/trustedkeystore.jks
              |
              +---/vhosts
                      +--/app-manager
                      |         +----/bin/tomcat-juli.jar
                      |         +----/conf
                      |         |       +-/Catalina/
                      |         |       +-/server.xml
                      |         +----/host.pkcs12 -> ../../CATALINA_HOME/host.pkcs12
                      |         +----/lib/
                      |         +----/trustedkeystore.jks -> ../../CATALINA_HOME/trustedkeystore.jks
                      |         +----/webapps/
                      |
                      +--/client
                      |      +--/bin/tomcat-juli.jar


#. ``cd <any empty temporary directory>``
#. Download `Tomcat install scripts <https://bitbucket.org/gef_work/ap_predict_online/src/master/tools/AP-Portal/install/Tomcat/>`_,
   retaining directory structure.  
#. Run ``1install.sh <tomcat source code> <tomcat destination> <tomcat version>`` |br|
   e.g. ``1install.sh /apps/src/apache-tomcat-7.0.56 /apps/tomcat 7.0.56``
#. ``cd <tomcat destination>/CATALINA_HOME`` and follow instructions in :file:`README.openssl`