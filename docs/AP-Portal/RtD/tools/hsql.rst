.. include:: ../global.rst

.. _tools_hsql:

HSQL
====

HSQL's website is `here <http://hsqldb.org/>`_

HyperSQL is a 100% Java database which, in this application, is generally used
as an embedded (i.e. you don't need to set up database usernames
and passwords, the db is only in existence for as long as the application is
running) database either just during build processes, or for some components
(:ref:`heads-up <installation_components_general_database_choice>`!), in a
deployment environment.