.. include:: ../global.rst

.. _tools_mysql:

.. note:: The instructions below are for a version of MySQL which by the time
   you read this may have security issues!

MySQL
=====

MySQL's website is `here <https://www.mysql.com/>`_

Example install
---------------

.. note:: Other databases can be used as |AP-Portal| components use
  `Hibernate <http://hibernate.org/>`_, but alternatives haven't been tested.


#. Estimated time to build : 2 minutes.
#. Desired structure : Similar (eventually) to the following :

::

 <data_dir>
     |
     +------/app_manager
     +------/business_manager
     +------/client_direct
     +------/etc/init.d/mysql_apportal
     +------/innodb
     +------/my.cnf
     +------/mysql
     +------/var
              +-/lib/mysql/mysql.sock (when running)
              +-/log/mysql/mysql-bin.
              |        +--/mysqld-log
              |        +--/mysqld-log.err
              +-/run/mysqld/mysqld.pid (when running)

#. ``cd <any empty temporary directory>``
#. Download `MySQL install scripts <https://bitbucket.org/gef_work/ap_predict_online/src/master/tools/AP-Portal/install/MySQL/>`_,
   retaining directory structure.
#. Run ``1install.sh <mysql binary location> <database dir> <system_user> <system_group>`` |br|
   e.g. ``1install.sh /apps/mysql/5.6.27 /data/apportal user_me group_me``