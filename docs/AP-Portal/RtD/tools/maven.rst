.. include:: ../global.rst

.. _tools_maven:

Maven
=====

Maven's website is `here <https://maven.apache.org/>`_, it's Java-based
and is used to build projects from source.