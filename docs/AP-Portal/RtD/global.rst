.. |AMQ| replace:: :abbr:`AMQ (ActiveMQ)`
.. |API| replace:: :abbr:`API (Application Programming Interface)`
.. |CLI| replace:: :abbr:`CLI (Command-line Interface)`
.. |CSRF| replace:: :abbr:`CSRF (Cross-Site Request Forgery)`
.. |CSS| replace:: :abbr:`CSS (Cascading Style Sheet)`
.. |FAQ| replace:: :abbr:`FAQ (Frequently Asked Question)`
.. |GUI| replace:: :abbr:`GUI (Graphical User Interface)`
.. |HTS| replace:: :abbr:`HTS` (High Throughput Screening)`
.. |HTTP| replace:: :abbr:`HTTP (Hypertext Transfer Protocol)`
.. |HTTPS| replace:: :abbr:`HTTPS (Hypertext Transfer Protocol (Secure))`
.. |i18n| replace:: :abbr:`i18n (internationalisation)`
.. |IC20| replace:: :abbr:`IC20 (20% Inhibitory Concentration)`
.. |IC50| replace:: :abbr:`IC50 (50% Inhibitory Concentration)`
.. |IE| replace:: :abbr:`IE (Internet Explorer)`
.. |IP| replace:: :abbr:`IP (Internet Protocol)`
.. |IT| replace:: :abbr:`IT (Information Technology)`
.. |IDE| replace:: :abbr:`IDE (Integrated Development Environment)`
.. |JDBC| replace:: :abbr:`JDBC (Java DataBase Connectivity)`
.. |JDK| replace:: :abbr:`JDK (Java Development Kit)`
.. |JNI| replace:: :abbr:`JNI (Java Native Interface)`
.. |NTLM| replace:: :abbr:`NTLM (NT LAN Manager)`
.. |PII| replace:: :abbr:`PII (Personally Identifiable Information)`
.. |PK| replace:: :abbr:`PK (Pharmacokinetic)`
.. |QSAR| replace:: :abbr:`QSAR (Quantitative structure–activity relationship)`
.. |RAM| replace:: :abbr:`RAM (Random Access Memory)`
.. |RTFM| replace:: :abbr:`RTFM (Read The F!!!ing Manual)`
.. |SPA| replace:: :abbr:`SPA (Single-Page Application)`
.. |SMTP| replace:: :abbr:`SMTP (Simple Mail Transfer Protocol)`
.. |SQL| replace:: :abbr:`SQL (Structured Query Language)`
.. |TLS| replace:: :abbr:`TLS (Transport Level Security)`
.. |UI| replace:: :abbr:`UI (User Interface)`
.. |URI| replace:: :abbr:`URI (Uniform Resource Identifier)`
.. |URL| replace:: :abbr:`URL (Uniform Resource Locator)`
.. |VCS| replace:: :abbr:`VCS (Version Control System)`
.. |VM| replace:: :abbr:`VM (Virtual Machine)`
.. |WS| replace:: :abbr:`WS (Web Service)`
.. |WSDL| replace:: :abbr:`WSDL (Web Service Definition Language)`
.. |WSS| replace:: :abbr:`WSS (Web Services Security)`
.. |XML| replace:: :abbr:`XML (eXtensible Markup Language)`
.. |XSD| replace:: :abbr:`XSD (XML Schema Definition)`

.. |S| replace:: :abbr:`[ S ] (Scientific content)`
.. |T| replace:: :abbr:`[ T ] (Technical content)`
.. |V| replace:: :abbr:`[ V ] (Visualisation content)`

.. |_A| replace:: :abbr:`A (Added)`
.. |_M| replace:: :abbr:`M (Modified)`
.. |_D| replace:: :abbr:`D (Deleted)`

.. |AP-Nimbus| replace:: ``AP-Nimbus``
.. |AP-Portal| replace:: ``AP-Portal``
.. |ApPredict| replace:: ``ApPredict``
.. |CHASTE| replace:: ``CHASTE``

.. |HSQL| replace:: :ref:`HSQL <tools_hsql>`
.. |Maven| replace:: :ref:`Maven <tools_maven>`
.. |MySQL| replace:: :ref:`MySQL <tools_mysql>`
.. |Oracle| replace:: :ref:`Oracle <tools_oracle>`
.. |Spring| replace:: :ref:`Spring <tools_spring>`
.. |Tomcat| replace:: :ref:`Tomcat <tools_tomcat>`

.. |app-manager| replace:: ``app-manager``
.. |business-manager| replace:: ``business-manager``
.. |business-manager-api| replace:: ``business-manager-api``
.. |client| replace:: ``client``
.. |client-direct| replace:: ``client-direct``
.. |client-parent| replace:: ``client-parent``
.. |client-shared| replace:: ``client-shared``
.. |dose-response-jni| replace:: ``dose-response-jni``
.. |dose-response-manager| replace:: ``dose-response-manager``
.. |site-business| replace:: ``site-business``

.. |clients-bespoke| replace:: "*bespoke user authentication*"
.. |clients-prepopulated| replace:: "*prepopulated user authentication*"
.. |role| replace:: "*Role*"

.. This role directive permits the use of :underline:`some text` ..
.. role:: underline
    :class: underline
.. role:: strikethrough
    :class: strikethrough

.. |br| raw:: html

   <br />