.. include:: ../../global.rst

.. _maintenance_requests_user:

Example User Requests
=====================

.. seealso:: An :ref:`example updating strategy <maintenance_update_multi_env>`.

.. toctree::

   client

If you've arrived at this page directly it may be worth briefly taking in some
of the general :ref:`configuration <configuration>` notes.

Similarly, if you're receiving requests which are likely to require updating
|AP-Portal| to the latest public version additional, general notes are 
available in the :ref:`remote_updates` section of this documentation.

Please change the values of ion channel :term:`variability`.
------------------------------------------------------------

 * |client| |br|
   The :term:`variability` values are defined in the |site-business|
   :file:`appCtx.config.assays.site.xml` file as per the 
   :ref:`extensibility instructions <installation_extensibility_site_business_ctxs>`.

 * |client-direct| |br|
   The visible default values of spread :term:`variability` are defined in the
   |client-direct| :file:`appCtx.config.site.xml` file as per the
   :ref:`installation instructions <installation_components_client_direct_config_site>`.

.. note:: See [#f3]_ regarding the change deployment technique, and [#f4]_
          for additional information.

Please change the default CellML model.
---------------------------------------

This is set by having only one of the available models assigned a
``defaultModel`` value of ``true`` in the following :

 * |client| |br|
   The |site-business| :file:`appCtx.config.cellModels.site.xml` file as per the
   :ref:`extensibility instructions <installation_extensibility_site_business_ctxs>`.

 * |client-direct| |br|
   The |client-direct| :file:`appCtx.config.cellModels.site.xml` file as per the
   :ref:`installation instructions <installation_components_client_direct_config_cellmodels>`.

.. note:: See [#f3]_ regarding the change deployment technique.

Please change the values of credible interval percentiles.
----------------------------------------------------------

The :term:`variability` percentile values are defined in the following :

 * |client| |br|
   The |site-business| :file:`appCtx.config.actionPotential.site.xml` file as
   per the
   :ref:`extensibility instructions <installation_extensibility_site_business_ctxs>`.

 * |client-direct| |br|
   The |client-direct| :file:`appCtx.config.site.xml` file as per the
   :ref:`installation instructions <installation_components_client_direct_config_site>`.

.. note:: See [#f3]_ regarding the change deployment technique, and [#f4]_
          for additional information.

Please modify information displayed by the information icons.
-------------------------------------------------------------

.. |info| image:: ../../_static/images/info.png
   :alt: Information icon.
   :class: info

**Currently the information displayed by** |info| **is not configurable.** [#f2]_

Information icons appear in various places throughout the |client| and
|client-direct| |UI|\ s. Modification of the information they display is
generally not possible as they are designed to enable the provision of an
:term:`internationalised` |AP-Portal|. It is of course possible to directly
change the |AP-Portal| code base, rebuild and redeploy, but if tempted to do so
please be careful regarding
:ref:`version control conflicts <version_control_conflicts>`). |br|
Having said that, parts of the values they display may be flexible, e.g. the
sample below is taken from a :file:`middle.jsp` file in |client-direct|.

.. code-block:: HTML
    :linenos:

    <td colspan="4" class="input_division cs_rounded_5">
      <spring:message code="general.notes" />
      <img src="${info_location}" class="info"
           title="<spring:message code="input.notes_max_chars"
                                  arguments="${notes_length}" />" />
    </td>

In the above we can see the use of the ``<spring:message code="general.notes" />``
which, when the |UI| is to display the corresponding web page, will read in
and display the ``general.notes`` property value derived from
the :file:`src/main/resources/bundle/general[<_locale>].properties` files [#f1]_. |br|
What is also visible is the use of the ``<spring:message code=".." arguments="<args>" />``
option which allows dynamic replacement of values which may be derived from, 
for example, values defined in configuration settings and so could be a technique
for future (non-|i18n|!) information display. 

Please add the new CellML models the |ApPredict| developer has released.
------------------------------------------------------------------------

Please see :ref:`maintenance_remote_appredict`.

Please modify the warning/error messages.
-----------------------------------------

As is the case with `Please modify information displayed by the information icons.`_
the warning messages are general not modifiable except by request to the
|AP-Portal| developers. Whilst some error messages are :term:`internationalised`
some are also derived from hard-coded messages in :term:`back-end components`.

Please update |AP-Portal| to incorporate the new development in the public version.
-----------------------------------------------------------------------------------

There are unfortunately no *one-size-fits-all* step-by-step guides to doing this
unless it's known what the modifications to the |AP-Portal| have been between the
currently installed version and the latest available public version.

Some :ref:`general updating notes <remote_updates>` are available which may
provide guidance.

.. rubric:: Footnotes

.. [#f1] The "locale" is considered to be the browser's current preferred
   display language.

.. [#f2] Any suggestions for change or queries would be welcomed by the portal
   developers!

.. [#f3] Modification of the configuration files should ideally be done prior
   to rebuilding and redeploying the relevant component (as opposed to directly
   modifying the content of an expanded ``.war`` file in the servlet
   container and restarting the component).

.. [#f4] See also :ref:`Variability Configuration <configuration_variability>`.

