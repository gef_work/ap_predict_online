.. include:: ../../global.rst

.. _maintenance_requests_it:

Example |IT| Dept. Requests
===========================

.. seealso:: An :ref:`example updating strategy <maintenance_update_multi_env>`.

If you've arrived at this page directly it may be worth briefly taking in some
of the general :ref:`configuration <configuration>` notes.

We need to restart |AP-Portal|\ 's own databases, do we need to shut down the components?
-----------------------------------------------------------------------------------------

The short answer is 'yes'.

The reason being that of the :term:`generic portal components`, |app-manager| and
|business-manager| frequently poll the database to find newly persisted data
(e.g. a new simulation request). In doing so a momentarily disappeared database
may cause some issues (albeit probably not fatal ones) if the portal is running
, even if there are no users using the portal at the time!