.. include:: ../../global.rst

.. _maintenance_requests_user_client:

Example |client|\ -only Portal User Requests
============================================

Please include a new assay in the results.
------------------------------------------

If a new assay is to be introduced then the scientists will need to determine
a number of aspects prior to making any change in the portal configuration or
code, e.g. :

 #. What is the definitive name of the assay?
 #. Where in the :term:`assay level` hierarchy of assays is this new assay to
    appear?
 #. Which :term:`assay group` is the assay to belong to?
 #. How will the assay data be retrieved?
 #. Will the data associated with this assay be the same as other, existing 
    assays, or will it differ in, for example, units, representations, 
    processing requirements, :term:`individual data` status, etc.? |br|
    If the assay will be generating results in an identical manner to other
    assays then there won't be a need for much additional code to be written.
 
Essential
^^^^^^^^^

 #. :file:`appCtx.config.assays.site.xml`. |br|
    See the :ref:`extensibility instructions <installation_extensibility_site_business_ctxs>`
    related to assay details.

 #. Create additional code reflecting the technique, e.g. |SQL|, |WS|, for
    reading in the new assay data and, if necessary, additional processing
    before passing such data over to the |business-manager| (via
    |business-manager-api| structures). 

Optional
^^^^^^^^

 #. Create new
    `datarecord <https://bitbucket.org/gef_work/ap_predict_online/src/master/business-manager-api/src/main/java/uk/ac/ox/cs/nc3rs/business_manager/api/value/object/datarecord/>`_
    implementations if necessary. |br|
    See the :ref:`extensibility instructions <installation_extensibility_site_business_java>`
    related to defining :term:`site-specific` data record structures derived
    from |business-manager-api|.

 #. If there is a significant change of processing required for this new assay
    then processing steps such as those demonstrated in the generic
    `site-business <https://bitbucket.org/gef_work/ap_predict_online/src/master/site-business/src/main/resources/META-INF/spring/ctx/integration/simulation/processing/>`_
    may be required, e.g. as in :ref:`installation_extensibility_site_business_processing`.

.. warning:: If a new assay is to be included then the simulation database
   needs to be purged of all existing simulation results because previously
   used assay identifiers which are persisted in the simulation results
   will (more than likely) no longer be valid.

.. _maintenance_requests_user_client_sim_fail:

The simulation's failed to run - why?
-------------------------------------

.. |flag| image:: ../../_static/images/flag.png
   :alt: Simulation job progress flag
   :class: flag

There is a hidden option to `right-click` whilst over the job flag, i.e.
|flag|\ , **after** clicking (or re-clicking) the `submit` button. This will
expand the diagnostics details at the base of the page, which can be removed
either by repeating the `right-click` or by choosing a different simulation. 