.. include:: ../../../global.rst

|site-business|
===============

Changing database connectivity
------------------------------

There are two ways to change the database connectivity, both of which can
make use of the :doc:`property file encryption </tools/propertyfileencrypt>` if the
data is sensitive :

#. Temporary change, i.e. the change will be overwritten on a subsequent
   redeployment unless the permanent change is undertaken.

   If |site-business| has been deployed in the servlet container (e.g. the
   :file:`site_business-{<version>}.war` file has been expanded in the *webapps* 
   directory), then modify the content of the file :file:`site_business.properties`, e.g. 
   :file:`{CATALINA_HOME}/site-business/webapps/site_business-{<version>}/WEB-INF/classes/META-INF/properties/site_business.properties`
   and :ref:`restart <maintenance_general_restart>` |site-business|, upon
   which |site-business| will attempt to connect to the new database.

#. Permanent change.

   For a change to become effective when |site-business| is 
   :ref:`redeployed <site_business_deploy>`, the |site-business| source code
   file :file:`{<ap_predict_online>}/site-business/src/properties/database/site/site.database.properties`
   needs to be assigned the desired values, then the source code needs to be
   :ref:`rebuilt <site_business_build>` and :ref:`redeployed <site_business_deploy>`.
