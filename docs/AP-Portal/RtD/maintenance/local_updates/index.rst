Updating -- :term:`Local` changes
=================================

These changes generally reflect changes in user preferences or changes to
:term:`site-specific` configurations, e.g. new database connection settings.

.. seealso:: An :ref:`example updating strategy <maintenance_update_multi_env>`.

.. toctree::

   site-business/index