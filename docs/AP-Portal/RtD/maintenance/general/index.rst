.. include:: ../../global.rst

General
=======

.. _maintenance_general_start:

Component Start
---------------

Example start script

::

  #!/bin/bash -e

  if [ $# -ne 1 ]; then
    echo ""
    echo "  Usage: start.sh <component>"
    echo "    e.g. start.sh app-manager"
    echo ""

    exit 1
  fi

  component=$1

  if [ -d ${component} ] ; then
    rm -rf ${component}/{logs,temp,work}/*
    rm -f logs/${component}.log*
  fi

  . `pwd`/${component}.env
  `pwd`/bin/catalina.sh start

.. seealso:: :ref:`environment_setting` below (for adjusting, for example, ``CATALINA_OPTS``).

.. _maintenance_general_stop:

Component Stop
--------------

Example stop script

::

  #!/bin/bash -e

  if [ $# -ne 1 ]; then
    echo ""
    echo "  Usage: stop.sh <component>"
    echo "    e.g. stop.sh app-manager"
    echo ""

    exit 1
  fi

  component=$1

  . `pwd`/${component}.env
  `pwd`/bin/shutdown.sh

.. _maintenance_general_restart:

Component Restart
-----------------

See :ref:`maintenance_general_start` and :ref:`maintenance_general_stop`!

.. _environment_setting:

Environment setting
-------------------

Default values for Java memory allocations may be available using, for example,
``java -XX:+PrintFlagsFinal -version | grep 'PermSize'``.

In the example scripts above reference is made to a :term:`local`
environment-setting file. An example of such file, e.g.
:file:`app-manager.env`, for |Tomcat| [#f1]_ use follows :

::

   JAVA_HOME=/usr/java/current
   CATALINA_HOME=/home/me/apps/tomcat/CATALINA_HOME
   CATALINA_BASE=/home/me/apps/tomcat/vhosts/app-manager

   # Xmx/Xms = JVM heap (max/initial). Object instance storage. Equal values == "fully committed" - avoids GC as initial expands to max.
   # Xss     = Thread stack size. If you get a StackOverFlow exception, increase this value!
   # -XX:PermSize -XX:MaxPermSize = Class files storage.
   #CATALINA_OPTS="-Xms256m -Xmx256m -Xss256k -XX:PermSize=64m -XX:MaxPermSize=64m"

   # Set in a 32-bit Win environment. Auto-enabled on 64-bit OSs
   # JAVA_OPTS="-server"
   JAVA_OPTS="-Dspring.profiles.active=app_manager_mysql"

   JASYPT_PWD=<password>

   export JAVA_HOME JAVA_OPTS CATALINA_BASE CATALINA_HOME JASYPT_PWD


.. seealso:: :ref:`Java troubleshooting <troubleshooting_java>`.

.. rubric:: Footnotes

.. [#f1] In earlier versions of |AP-Portal| the ``CATALINA_OPTS`` option was
         trialled to test the mimimum memory required but since the introduction
         of PK processing (around late 2017) the memory requirements of most
         components increased significantly and so memory requirements may need
         to be specified towards the higher ranges for some components (
         |client| for example may require ``-XX:MaxPermSize:512m``).
