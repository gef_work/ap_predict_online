.. include:: ../../global.rst

.. _maintenance_update_multi_env:

Example strategy for updating many hosted environments
======================================================

Preamble
--------

The policy adopted in this example strategy is that there are two mechanisms for
managing :term:`site-specific` data, these are :

 #. Environment-agnostic code, configurations and settings which 
    :underline:`do not` contain sensitive data (e.g. passwords) are placed in a
    :ref:`subversion <tools_vcs_subversion>` repository accessible to all
    servers. |br|
    The assumption here being that subversion repository is a "public" facility
    which is :underline:`not` sufficiently secured to guarantee that portal
    code will be accessible only to |AP-Portal| developers/administrators.
 #. Environment-specific code, configurations and settings, and objects
    containing sensitive data, are placed in the component sub-directories
    of (the :ref:`git <tools_vcs_git>`-managed) ap_portal_online.

When building for a particular environment components which are ...

 #. :term:`generic portal components`: That component's subversion-resident
    objects are overlayed over its corresponding
    :ref:`git <tools_vcs_git>`-managed ap_portal_online sub-directory code just
    prior to invoking the build instruction in the cloned git directory.
 #. :term:`site-specific`: That component's building takes place in the
    subversion repository code working directory.

Hardware
^^^^^^^^

The example hardware configuration is as follows :

 +---------+-------------+
 | Server  | Environment |
 +=========+=============+
 | server1 | development |
 +---------+-------------+
 | server1 | test        |
 +---------+-------------+
 | server2 | production  |
 +---------+-------------+

Version control system
^^^^^^^^^^^^^^^^^^^^^^

A :ref:`subversion <tools_vcs_subversion>` repository, accessible to all servers
, hosts the :term:`site-specific` code, configurations and settings which 
**don't** contain sensitive data and are environment-agnostic, i.e. objects
created in the *development* environment which can be deployed without
modification during updates to *test* and *production* environments.

For example, the structure of the subversion repository would be similar to the
following, whereby the *<git hash>* would be the ap_predict_online commit short
hash used at the time of building the update :

::

 svnrepo/client/branches
    |       +--/tags/development                    --+
    |       |    |    +-/160912_<git hash 1>          |
    |       |    |    +-/160930_<git hash 2>          |
    |       |    |    +-/161015_<git hash 3>          |
    |       |    |    +-/161016_<git hash 4>          |
    |       |    |    +-/161030_<git hash 5>          |
    |       |    +--/production                       |-- Records of historical tagged updates
    |       |    |    +-/160925_<git hash 1>          |
    |       |    |    +-/161022_<git hash 4>          |
    |       |    +--/test                             |
    |       |         +-/160920_<git hash 1>          |
    |       |         +-/161002_<git hash 2>          |
    |       |         +-/161020_<git hash 4>        --+
    |       |
    |       +--/trunk/pom.xml                       --+
    |             +--/src/main/java                   |
    |                  |    |    +-/com               |
    |                  |    +-/resources              +-- Latest development code
    |                  |    +-/webapp                 |
    |                  +-/test/java                   |
    |                            +-/com             --+
    |
    +---/site-business/branches                     --+
                +-----/tags                           +-- Bespoke site-business component
                +-----/trunk                        --+

Update cycle
^^^^^^^^^^^^
.. figure:: ../../_static/images/update_dev.png
    :align: center
    :alt: Sample dev -> test update considerations
    :figclass: align-center

    Sample dev -> test update considerations.

.. figure:: ../../_static/images/update_cycle.png
    :align: center
    :alt: Sample update cycle
    :figclass: align-center

    Sample update cycle.