.. include:: ../../../global.rst

.. _remote_updates_general:

General updating concepts
=========================

The updating process will involve some or all of the steps below and will *at
least* involve the portal's technical administrator and potentially a scientist
responsible for the portal's scientific settings (e.g. someone who would know
information such as the assays and ion channels being processed) :

The steps are :

 #. Reflect any new files in the :term:`portal repository` source code onto the
    :term:`local` system, i.e., create files according to any modified
    installation instruction documentation file(s).
 #. Reflect any modifications to the :term:`portal repository`
    :file:`sample.{<file_name>}` files onto the :term:`local` configuration
    files (which were originally created during
    :ref:`installation <installation>`). |br|
    Details of files which may have been modified may be found in the
    :ref:`remote_updates_changelog`.
 #. Reflect any modifications to the :term:`portal repository` files onto
    :term:`site-specific` (including bespoke) code and configurations.

    This may be necessary if, for example, the |business-manager-api| or
    |business-manager| code changes, and there is bespoke, :term:`site-specific` code
    which references any of the changed objects or settings.
 #. If you have more than one installation environment to maintain, e.g.
    *development*, *test* and *production*, then for each update in the feeder
    environment please record the modifications made so that accumulated
    changes can be applied to the consumer environment.

    In theory, a scientist would only need to present for the update of a 
    *development* environment as changes to the *test* and/or *production*
    environment could be derived from recorded decisions made during the
    update of the *development* environment.

    See :ref:`here <maintenance_update_multi_env>` as an example.

Retrieving and analysing changes in portal source code
------------------------------------------------------

In order to update the :term:`generic portal components` the standard procedure
is to execute the following ``git`` commands in sequence at the base of the
cloned |AP-Portal| directory. A collection of the latest command examples is
stored in the :ref:`tools section <tools_vcs_git>` :

 #. ``git fetch``

    (Outcome: Does not update the :term:`local` source code files or directories
    , only updates git's internal version control system, but allows us to
    anticipate what changes will be required locally.)

 #. ``git diff --name-status HEAD...<git hash>``, or |br|
    ``git diff --name-status HEAD...origin``

    (Outcome: Lists the source code file changes and the nature of change)

    Generally we focus on the (**M**)\ odified, (**D**)\ eleted or (**A**)\ dded 
    of the following file types, but also check the :ref:`remote_updates_changelog`:

    1. :file:`sample.{<file_name>}`
    2. :file:`.gitignore`
    3. :file:`{<file_name>}.xsd`
    4. :file:`installation/components/<component>/index.rst`

    A sample command would be
    ``git diff --name-status HEAD...origin | grep -P '(/sample\.|\.gitignore|\.xsd|installation/components/.*?/index\.rst$|/appCtx\.config\.|/pom\.xml|\.sql)'``

    If any changes have been made to the source code of such files please
    record their names for closer examination in the next step.

 #. ``git diff HEAD...<git hash> <file_name>``, or |br|
    ``git diff HEAD...origin <file_name>``

    (Outcome: Prints a ``diff`` output of the changes of a specified file if it
    has been (**M**)\ odified since the last ``git merge origin``.)

    Determine what, if any, :term:`local` changes will be required **after** an
    instruction to "merge" the update (see next step) is executed. See
    :ref:`Updating configuration and settings files <remote_updates_configuration_and_settings>`
    and :ref:`Updating web services <remote_updates_web_services>`.

 #. ``git merge <git hash>``, or |br|
    ``git merge origin``

    (Outcome: Updates the source code files and directories.)

.. _remote_updates_configuration_and_settings:

Updating configuration and settings files
-----------------------------------------

During the original installation of each of the components it is very likely
that :term:`site-specific` configurations were derived from generic
configuration or settings files, e.g. files such as
:file:`src/properties/filter.properties` derived from their corresponding
"sample" file :file:`src/properties/sample.filter.properties`, or in the case of
the |client| portal, :ref:`installation_extensibility_site_business`.
Invariably these instructions were defined in the component's installation
instruction file.

If the content of the "sample" files has been changed by the update request
(e.g. perhaps a new configuration option has been added to a component) then
any such changes will need to be implemented in the derived :term:`site-specific`
implementations of those files.

At this stage the best option is to view the output of the ``git diff
--name-status HEAD...origin`` command to check which of the files listed in a
component's installation instructions documentation file have been modified and
manually apply changes to the :term:`local` version.

.. _remote_updates_web_services:

Updating web services
---------------------

If any of the components' |WS| |XSD| files have been modified (usually located
in the component's :file:`src/main/resources/META-INF/schema/` directory) then,
if the change results in a change of contract (as opposed to a change of
documentation) of the component's |WS| |API|, any components with which it
communicates *may* also need to be updated.

Restarting components
---------------------

Once any changes have been made to configuration and settings files the
components need to be rebuilt (see Build instructions in 
:ref:`Installation <installation>`), redeployed and :ref:`restarted <maintenance_general_start>`.