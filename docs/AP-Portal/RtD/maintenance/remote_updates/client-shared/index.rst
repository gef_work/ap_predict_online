.. include:: ../../../global.rst

|client-shared| updating
========================

First take a look at the :ref:`general update instructions <remote_updates_general>`.

Based on what has appeared previously in |client-shared|'s 
`installation instructions <http://apportal.readthedocs.io/en/latest/installation/components/client-shared/index.html>`_,
:term:`local` versions of the following files are likely to require updating if
the corresponding :file:`sample.{<file_name>}` files have changed in the
:term:`portal repository`.

 #. :file:`src/main/resources/META-INF/data/spring-security/local/users.sql` 
 #. :file:`src/main/resources/META-INF/resources/resources/css/site/client-shared-site.css`
 #. :file:`src/main/resources/META-INF/resources/WEB-INF/spring/ctx/config/appCtx.features.xml`
 #. :file:`src/main/resources/META-INF/resources/WEB-INF/tiles/layout/common/logo.jsp`
 #. :file:`src/main/resources/META-INF/resources/WEB-INF/tiles/layout/common/logout.jsp`
 #. :file:`src/main/resources/META-INF/resources/WEB-INF/tiles/layout/contact/contact.jsp`
 #. :file:`src/main/resources/WEB-INF/spring/authn/appCtx.bespoke.xml`
 #. :file:`src/main/resources/WEB-INF/spring/authn/appCtx.sitePreauthFilter.xml`
 #. :file:`src/main/resources/WEB-INF/spring/root-context.site.xml`
 #. :file:`src/properties/cs-filter.properties`
 #. :file:`pom.xml`