.. include:: ../../global.rst

.. _remote_updates:

Updating -- Remote |AP-Portal|/|ApPredict| changes
==================================================

These changes reflect changes in the source code of the generic functionality.

.. seealso:: An :ref:`example updating strategy <maintenance_update_multi_env>`.

.. toctree::

   general/index
   app-manager/index
   business-manager/index
   business-manager-api/index
   client/index
   client-direct/index
   client-parent/index
   client-shared/index
   dose-response-jni/index
   dose-response-manager/index
   site-business/index
   ApPredict/index
   ChangeLog