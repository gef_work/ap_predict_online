.. include:: ../../../global.rst

|client-parent| updating
========================

First take a look at the :ref:`general update instructions <remote_updates_general>`.

Generally there shouldn't need to be any manual changes to this component as
part of an update.