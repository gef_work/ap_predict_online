.. include:: ../../../global.rst

.. _maintenance_remote_app_manager:

|app-manager| updating
======================

First take a look at the :ref:`general update instructions <remote_updates_general>`.

Based on what has appeared previously in |app-manager|'s
`installation instructions <http://apportal.readthedocs.io/en/latest/installation/components/app-manager/index.html>`_,
:term:`local` versions of the following files are likely to require updating if
the corresponding :file:`sample.{<file_name>}` files have changed in the 
:term:`portal repository`.

 #. :file:`src/main/resources/META-INF/spring/ctx/ws/appCtx.ws.security-incoming.xml`
 #. :file:`src/main/resources/META-INF/spring/ctx/ws/appCtx.ws.security-outgoing.xml`
 #. :file:`src/properties/database/database.filter.properties`
 #. Files derived from :file:`src/properties/database/sample.database.spring.properties`
 #. :file:`src/properties/filter.properties`
 #. :file:`src/properties/spring.properties`
 #. :file:`pom.xml`

.. warning:: Both |client-direct| and/or |business-manager| communicate with the
   |app-manager| |WS| |API|. |br|
   If |app-manager|'s |WS| |XSD| file
   (`app_manager.xsd <https://bitbucket.org/gef_work/ap_predict_online/src/master/app-manager/src/main/resources/META-INF/schema/app_manager.xsd>`_)
   has changed, and the change results in a new |WSDL| contract, then it will
   require |client-direct| and/or |business-manager| (and hence |site-business|)
   to be rebuilt.

.. seealso:: If you are updating |app-manager| then it is possible that you may
   also need to :ref:`update ApPredict <maintenance_remote_appredict>`