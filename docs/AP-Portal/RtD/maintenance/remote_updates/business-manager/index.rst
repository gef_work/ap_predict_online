.. include:: ../../../global.rst

|business-manager| updating
===========================

First take a look at the :ref:`general update instructions <remote_updates_general>`.

Based on what has appeared previously in |business-manager|'s 
`installation instructions <http://apportal.readthedocs.io/en/latest/installation/components/business-manager/index.html>`_,
:term:`local` versions of the following files are likely to require updating if
the corresponding :file:`sample.{<file_name>}` files have changed in the
:term:`portal repository`.

 #. :file:`src/main/resources/META-INF/spring/ctx/ws/appCtx.ws.security-incoming.xml`
 #. :file:`src/main/resources/META-INF/spring/ctx/ws/appCtx.ws.security-outgoing.xml`
 #. :file:`src/properties/database/database.filter.properties`
 #. Files derived from :file:`src/properties/database/sample.database.spring.properties`
 #. :file:`src/properties/filter.properties`
 #. :file:`src/properties/spring.properties`
 #. :file:`pom.xml`

.. warning:: Both |client| and/or |app-manager| communicate with the
  |business-manager| |WS| |API|. |br|
  If |business-manager|'s |WS| |XSD| file
  (`business_manager.xsd <https://bitbucket.org/gef_work/ap_predict_online/src/master/business-manager/src/main/resources/META-INF/schema/business_manager.xsd>`_)
  has changed, and the change results in a new |WSDL| contract, then it will
  require |client| and/or |app-manager| to be rebuilt. 