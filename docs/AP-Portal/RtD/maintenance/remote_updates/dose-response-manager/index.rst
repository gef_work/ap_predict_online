.. include:: ../../../global.rst

|dose-response-manager| updating
================================

First take a look at the :ref:`general update instructions <remote_updates_general>`.

Based on what has appeared previously in |dose-response-manager|'s 
`installation instructions <http://apportal.readthedocs.io/en/latest/installation/components/dose-response-manager/index.html>`_,
:term:`local` versions of the following files are likely to require updating if
the corresponding :file:`sample.{<file_name>}` files have changed in the
:term:`portal repository`.

 #. :file:`env.properties`

.. warning:: |business-manager| communicates with the |dose-response-manager|
  |WS| |API|. |br|
  If |dose-response-manager|'s |WS| |XSD| file
  (`fdr_manager.xsd <https://bitbucket.org/gef_work/ap_predict_online/src/master/dose-response-manager/src/main/resources/schema/fdr_manager.xsd>`_)
  has changed, and the change results in a new |WSDL| contract, then it will require
  |business-manager| (and hence |site-business|) to be rebuilt.