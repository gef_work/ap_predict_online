.. include:: ../../../global.rst

.. _maintenance_remote_appredict:

|ApPredict| updating
====================

If there's a new version of |ApPredict| then there are a number of issues to
consider which may have a greater impact, such as :

 * If we were to update to the latest version of |ApPredict| to allow us to use
   the new CellML models, has the |ApPredict| developer also made other,
   potentially significant, changes which will require a new version of 
   |AP-Portal| to be installed?

   #. If |ApPredict| has changed considerably there's most likely a new version
      of |AP-Portal| (or at least |app-manager| to install - see
      :ref:`update app-manager <maintenance_remote_app_manager>`).

   #. If the |ApPredict| invocation mechanism has changed then perhaps it will
      be necessary to update the 
      :ref:`app-manager tools <installation_components_app_manager_tools>`
      e.g. :file:`{CATALINA_HOME}/ApPredict.sh` 

 * If |ApPredict| has added a new CellML model to the available collection (i.e.
   has |ApPredict|\ 's ``--model`` range expanded), are there also new 
   :term:`variability lookup tables` available for download?

 * Does the new CellML model behave differently to existing CellML models? |br|
   If it does (i.e. perhaps it is adversely affects the performance of 
   |AP-Portal|), then please let the portal developers know [#f1]_!

|ApPredict| has a new CellML model available ...
------------------------------------------------

In the scenario that the only change to |ApPredict| is a new CellML model
(or models) which behave much like existing models, and new
:term:`variability lookup tables` are available, please complete the following :

 #. Install the new |ApPredict| (making sure not to provisionally avoid
    overwriting/removing the operational version) - see 
    :ref:`ApPredict installation <installation_minreqs_software_appredict>`.

 #. Download any new :term:`variability lookup tables` and deploy them as per
    previous lookup table installation - see 
    :ref:`Variability Configuration <configuration_variability>`.

 #. Whichever portal (|client| and/or |client-direct|) you are using, CellML
    model data is contained in the
    :file:`src/main/resources/META-INF/spring/ctx/config/appCtx.config.cellModels.site.xml`
    file :

    a. |client| portal |br|
       Adjust the |site-business| file as per the 
       :ref:`extensibility instructions <installation_extensibility_site_business_ctxs>`
       to include the new CellML model details.

    b. |client-direct| portal |br|
       Adjust the |client-direct| file as per the 
       :ref:`installation instructions <installation_components_client_direct_config_cellmodels>`
       to include the new CellML model details.

.. rubric:: Footnotes

.. [#f1] Any suggestions for change or queries would be welcomed by the portal
   developers!