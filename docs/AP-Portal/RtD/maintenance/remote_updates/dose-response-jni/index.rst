.. include:: ../../../global.rst

|dose-response-jni| updating
============================

First take a look at the :ref:`general update instructions <remote_updates_general>`.

Based on what has appeared previously in |dose-response-jni|'s 
`installation instructions <http://apportal.readthedocs.io/en/latest/installation/components/dose-response-jni/index.html>`_,
:term:`local` versions of the following files are likely to require updating if
the corresponding :file:`sample.{<file_name>}` files have changed in the
:term:`portal repository`.

 #. :file:`makefile`
 
If there has been a change then downstream components (e.g. |dose-response-manager|) will also
need updating.