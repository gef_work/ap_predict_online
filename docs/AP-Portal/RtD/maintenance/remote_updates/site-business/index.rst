.. include:: ../../../global.rst

|site-business| updating
========================

.. note:: These instructions refer to the |site-business| of the 
          :term:`generic portal components`, and are unlikely to require
          updating unless you're demo'ing the fictional company compounds. |br|
          If you're looking for instructions for a :term:`site-specific`
          |site-business| update then please visit the installation information
          contained in :ref:`installation_extensibility`.

First take a look at the :ref:`general update instructions <remote_updates_general>`.

Based on what has appeared previously in |site-business|'s 
`installation instructions <http://apportal.readthedocs.io/en/latest/installation/components/site-business/index.html>`_,
:term:`local` versions of the following files are likely to require updating if
the corresponding :file:`sample.{<file_name>}` files have changed in the
:term:`portal repository`.

 #. :file:`env.properties`
 #. :file:`src/properties/database/site/site.database.properties`