.. include:: ../../../global.rst

|client-direct| updating
========================

First take a look at the :ref:`general update instructions <remote_updates_general>`.

Based on what has appeared previously in |client-direct|'s 
`installation instructions <http://apportal.readthedocs.io/en/latest/installation/components/client-direct/index.html>`_,
:term:`local` versions of the following files are likely to require updating if
the corresponding :file:`sample.{<file_name>}` files have changed in the
:term:`portal repository`.

 #. :file:`src/main/resources/META-INF/spring/ctx/config/appCtx.config.cellModels.site.xml`
 #. :file:`src/main/resources/META-INF/spring/ctx/config/appCtx.config.site.xml`
 #. :file:`src/main/resources/META-INF/spring/ctx/ws/appCtx.ws.security-outgoing.xml`
 #. :file:`src/main/webapp/resources/js/site/site.js`
 #. :file:`src/properties/database/database.filter.properties`
 #. Files derived from :file:`src/properties/database/sample.database.spring.properties`
 #. :file:`src/properties/filter.properties`
 #. :file:`src/properties/spring.properties`
 #. :file:`pom.xml`

If you have a |client-direct|\-specific |clients-prepopulated|
:ref:`user authentication mechanism <installation_extensibility_authnauthz>`
database then also check the following :

 #. :file:`src/main/resources/META-INF/data/spring-security/local/users.sql` 
