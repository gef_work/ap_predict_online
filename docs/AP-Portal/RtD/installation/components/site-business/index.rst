.. include:: ../../../global.rst

|site-business|
===============

|site-business| is the final phase of building the business logic after building
|business-manager-api| and then |business-manager|. |br|
The |site-business| component in the ap_predict_online repository represents a 
compound database and associated site data processing **of a fictional company**. |br|
If you wish to build your own |site-business| see
:ref:`here <installation_extensibility_site_business>`.

Dependencies
------------

 * |Maven| (build)
 * Java 7 or higher (build, deploy)
 * Java Servlet Container, e.g. Apache |Tomcat| (deploy)
 * Database (build, deploy)
 * |business-manager-api| and |business-manager|

Database choice
---------------

.. seealso:: :ref:`Database choice <installation_components_general_database_choice>`

Initial installation
--------------------

Download the project source and go to this component's root directory (i.e.
`here <https://bitbucket.org/gef_work/ap_predict_online/src/master/site-business/>`_)
and follow the steps below.

  #. |T| Copy `src/properties/sample.env.properties <https://bitbucket.org/gef_work/ap_predict_online/src/master/site-business/src/properties/sample.env.properties>`_
     to :file:`src/properties/env.properties`
  #. |T| Copy `src/properties/database/site/sample.site.database.properties <https://bitbucket.org/gef_work/ap_predict_online/src/master/site-business/src/properties/database/site/sample.site.database.properties>`_
     to :file:`src/properties/database/site/site.database.properties`

Configuration
-------------

:file:`src/properties/env.properties`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   log.file.site_business=

Component log file location on disk, e.g. 'logs/site-business.log'.

::

   log.level.site_business=

Component log level, e.g. ``(trace|debug|info|warn|error|fatal)``.

::

   log.level.general=

Log level of code from other libraries, e.g. ``(trace|debug|info|warn|error|fatal)``.

:file:`src/properties/database/site/site.database.properties`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you wish to use the embedded |HSQL| database then this file can have all
content removed, otherwise you will need to assign the appropriate JDBC driver
and user details.

.. _site_business_build:

Build
-----

``-Dspring.profiles.active=``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Options are currently ``(sitedata_embedded|sitedata_mysql)``.

.. warning:: At build time it is **important** to use the ``-Dspring.profiles.active=sitedata_embedded``
   arg because during integration testing rubbish may be written to the database
   so it is important to use the embedded database during this process.

Example build instructions :

::

   cd <ap_predict_online>/site-business
   mvn clean verify -Dspring.profiles.active=sitedata_embedded
   
.. _site_business_deploy:

Deploy
------

If the build command has completed successfully the webapp ``.war`` file should be
in the ``target`` directory. Copy this to the servlet container's relevant file, e.g
for |Tomcat|, the ``webapps`` directory.

Run
---

See :ref:`start <maintenance_general_start>`.

Start-up problems
-----------------

TODO