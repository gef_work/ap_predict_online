.. include:: ../../../global.rst

.. _installation_components_dose_response_jni:

|dose-response-jni|
===================

|dose-response-jni| provides the :file:`libfittingdoseresponse.so` and
:file:`dose-response-fitter.jar` files which are used by
|dose-response-manager|. |br|
The original :term:`dose-response data` fitting C++ source code written by
Kylie Beattie can be found
`in Kylie's repository <https://trix.cs.ox.ac.uk/trac/browser/kylieb>`_.

.. note:: TODO: Test and explain that if there is no :term:`dose-response data`
   then it is not necessary to build/deploy |dose-response-jni|,
   |dose-response-manager|!

Dependencies
------------

 * Java 7 or higher (build)
 * ``boost-devel`` (or a non-package-installed :term:`local` equivalent) (build)
 * ``g++`` (build)
 * ``make`` (build)
 * `libjson <http://sourceforge.net/projects/libjson/>`_ (build)

Initial installation
--------------------

Download the project source and go to this component's root directory (i.e.
`here <https://bitbucket.org/gef_work/ap_predict_online/src/master/dose-response-jni/>`_)
and follow the steps below.

  #. |T| Copy `sample.makefile <https://bitbucket.org/gef_work/ap_predict_online/src/master/dose-response-jni/sample.makefile>`_
     to :file:`makefile`

Configuration
-------------

:file:`makefile`
^^^^^^^^^^^^^^^^

This is the only file which should need to change.

::

   JAVA_HOME=

Adjust this according to wherever your Java ``jar``, ``java``, ``javac`` and
``javah`` binaries are located (try ``which javah``), e.g. ``/usr``. |br|
NOTE: The binaries may not all be located in the same directory, in which case
manually adjust each ``$(JAVA_HOME)`` occurrence.

::

   BOOST_HOME=

Specify a value if your Boost ``include`` directory is not in a default path for
searching, e.g. ``/home/me/myincludes/boost``. |br|
NOTE: If the ``include`` is in a default path for searching then you can remove
the ``-I$(BOOST_HOME)`` from the ``CPPFLAGS`` line.

::

   JSON_HOME=

Specify a value if your libjson ``include`` and ``lib`` directories are not in
default paths for searching, e.g. ``/home/me/mylibjson``. |br|
NOTE: If the libjson ``include`` and ``lib`` directories are in default paths
for searching then you can remove the references to ``$(JSON_HOME)`` from the
file.

Build
-----

Example build instructions :

::

   cd <ap_predict_online>/dose-response-jni
   make

Deploy
------

If the ``make`` command has completed successfully the files
:file:`dose-response-fitter.jar` and :file:`libfittingdoseresponse.so` should
be in the ``target`` directory. Copy these two files to the ``lib`` directory
of |dose-response-manager|.

Done that! What's next?
-----------------------

Conventionally it will be :ref:`installation_components_dose_response_manager`.