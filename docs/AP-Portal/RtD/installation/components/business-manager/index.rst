.. include:: ../../../global.rst

|business-manager|
==================

|business-manager| contains generic business processing code.

Dependencies
------------

 * |Maven| (build)
 * Java 7 or higher (build, deploy)
 * Java Servlet Container, e.g. Apache |Tomcat| (deploy)
 * Database (build, deploy)
 * A |Maven|-``install``\ ed |business-manager-api|. See 
   :ref:`business-manager-api <installation_components_business_manager_api_install>`

Initial installation
--------------------

Download the project source and go to this component's root directory (i.e.
`here <https://bitbucket.org/gef_work/ap_predict_online/src/master/business-manager/>`_)
and follow the steps below.

 * Either ...

  #. Run `tools/one_time_copying.sh <https://bitbucket.org/gef_work/ap_predict_online/src/master/business-manager/tools/one_time_copying.sh>`_

 * ... or ...

  #. |T| Copy `sample.pom.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/business-manager/sample.pom.xml>`_ to :file:`pom.xml`
  #. |T| Copy `src/main/resources/META-INF/spring/ctx/ws/sample.appCtx.ws.security-incoming.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/business-manager/src/main/resources/META-INF/spring/ctx/ws/sample.appCtx.ws.security-incoming.xml>`_
     to :file:`src/main/resources/META-INF/spring/ctx/ws/appCtx.ws.security-incoming.xml`
  #. |T| Copy `src/main/resources/META-INF/spring/ctx/ws/sample.appCtx.ws.security-outgoing.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/business-manager/src/main/resources/META-INF/spring/ctx/ws/sample.appCtx.ws.security-outgoing.xml>`_
     to :file:`src/main/resources/META-INF/spring/ctx/ws/appCtx.ws.security-outgoing.xml`
  #. |T| Copy `src/properties/database/sample.database.filter.properties <https://bitbucket.org/gef_work/ap_predict_online/src/master/business-manager/src/properties/database/sample.database.filter.properties>`_
     to :file:`src/properties/database/database.filter.properties`
  #. |T| Copy `src/properties/sample.filter.properties <https://bitbucket.org/gef_work/ap_predict_online/src/master/business-manager/src/properties/sample.filter.properties>`_
     to :file:`src/properties/filter.properties`
  #. |T| Copy `src/properties/sample.spring.properties <https://bitbucket.org/gef_work/ap_predict_online/src/master/business-manager/src/properties/sample.spring.properties>`_
     to :file:`src/properties/spring.properties`

 * ... then ...

  #. Follow :ref:`database choice <installation_components_general_database_choice>` instructions.

 * ... finally

  #. Edit each of the copied files according to your desired `configuration`_.

Configuration
-------------

The illustration below depicts generic |business-manager| workflow :

.. figure:: ../../../_static/images/overall-business-logic-generic.png
   :align: center
   :alt: Overall generic business logic
   :width: 700px

:file:`pom.xml`
^^^^^^^^^^^^^^^

It is likely that there is a :term:`site-specific` database driver (such as a
|MySQL| driver) which needs adding to the :file:`pom.xml` file.

:file:`src/properties/filter.properties`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   app_manager.soap.location=

|WS| |URL| of |app-manager| component, e.g. 'http://localhost:18380/app_manager-0.0.4/aws/'

::

   business_manager.input_data_gathering.timeout=

Timeout (in milliseconds) whilst waiting for :term:`input data gathering`
requests to return. Depending on how responsive the site components are (e.g.
web services), this may need to be a corresponding number of seconds.

.. warning:: If no response is received within the specified timeout the system
   is likely to consider it as an indication that no data is available, rather 
   than issue a warning. (TODO: Verify this!)

::

   business_manager.amq_transport_connector.host=

|AMQ| Transport connector |URI| host (see appCtx.jms.ActiveMQ.xml), e.g. '127.0.0.1'.

::

   business_manager.amq_transport_connector.port=

|AMQ| Transport connector |URI| port (see appCtx.jms.ActiveMQ.xml), e.g. '61616'.

::

  business_manager.request_processing.polling.filter=1000

Default simulation request processing polling period (in ms).

When incoming simulation requests are received a few first-phase "request processing" tests will
take place immediately to determine if a simulation request should proceed to the second "simulation
processing" phase. If it is determined that the simulation should proceed then it is persisted in
the database and periodically (as determined by the value of this property) the database will be
polled for simulations awaiting second-phase processing.

::

   log.file.business_manager=

Component log file location on disk, e.g. 'logs/business-manager.log'

::

   log.level.business_manager=

Component log level, e.g. ``(trace|debug|info|warn|error|fatal)``.

::

   log.level.general=

Log level of code from other libraries, e.g. ``(trace|debug|info|warn|error|fatal)``.

:file:`src/properties/spring.properties`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   fdr.soap.location=

|WS| |URL| of |dose-response-manager|, e.g. 'http://127.0.0.1:18180/fdr_manager-0.0.1-SNAPSHOT/fws/'

::

   app_manager.capacity_indicator=

|app-manager| operating at capacity indicator, e.g. 'appmanager.at_capacity' |br|
The value used corresponds to the |client| |i18n| bundle identifier.

::
 
   app_manager.progress_pfx=
   
|app-manager| default progress prefix, e.g. 'PROG: ' (@see |app-manager|'s ProgressMonitor#PROGRESS_PREFIX?)


:: 

   app_manager.status_date_format=

|app-manager| status date format, e.g. 'yyyy:MM:dd HH:mm:ss'

::

   app_manager.default_saturation_level=

|app-manager| default saturation level (%) value, e.g. '0'

::

  business_manager.request_processing.polling=@business_manager.request_processing.polling.filter@

Generally no change necessary - value to use is derived from :file:`filter.properties`.

::

   fdr.default_coefficient=

|dose-response-manager| default Hill coefficient, e.g. '1'

::

   securement.app.username=

|app-manager| |WSS| username.

::
 
   securement.app.password=

|app-manager| |WSS| password.

::

   securement.business.username=

Component |WSS| username.

::

   securement.business.password=

Component |WSS| password.

:file:`src/main/resources/META-INF/spring/ctx/ws/appCtx.ws.security-incoming.xml`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Generally no change necessary.


:file:`src/main/resources/META-INF/spring/ctx/ws/appCtx.ws.security-outgoing.xml`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Generally no change necessary.

:file:`src/properties/database/database.filter.properties`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   business_manager.database.queryTimeout=

Hibernate (javax.persistence.query.timeout) query timeout (in milliseconds), e.g. '1'.

::
 
   business_manager.database.hbm2ddl=

Hibernate hbm2ddl schema DDL options e.g. ``(validate|update|create|create-drop)``.

:file:`src/properties/database/dev.database.<deploy.db_vendor>.properties`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. seealso:: :ref:`Database choice <installation_components_general_database_choice>`

Build
-----

.. seealso:: :ref:`Database choice <installation_components_general_database_choice>`
   and :ref:`Spring profiles <tools_spring_profiles>` for additional information.

``-Dspring.profiles.active=``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Options are currently ``business_manager__(embedded|mysql|oracle10g)``.

.. warning:: At build time it is **important** to use the 
   ``-Dspring.profiles.active=business_manager_embedded`` arg because during
   integration testing rubbish may be written to the database so it is important
   to use the embedded database during this process.

``-Ddeploy.db_vendor=``
^^^^^^^^^^^^^^^^^^^^^^^

The name of the database vendor, e.g. ``mysql``, ``postgres``, used in the 
intended deployment (**not build**) environment. The actual name used
corresponds exactly to the `<deploy.db_vendor>` element of the file derived from
:file:`sample.database.spring.properties` during the installation process.

:underline:`Example build instructions` :
 
::

   cd <ap_predict_online>/business-manager
   #export MAVEN_OPTS="-Xms512m -Xmx2g -XX:PermSize=2048m -XX:MaxPermSize=2048m"
   mvn clean install -Dspring.profiles.active=business_manager_embedded -Ddeploy.db_vendor=mysql

.. note:: Unlike in other components the ``-Ddeploy.env`` is hardcoded as ``dev``.

Deploy
------

This component when built should not be deployed into a servlet container, 
instead it is |Maven|-``install``\ ed in the :term:`local` |Maven| repository
and then a subsequent build of |site-business| retrieves the built ``.war`` file
and uses it in the |site-business| build process.

Run
---

See `Deploy`_

Start-up problems
-----------------

See `Deploy`_

Done that! What's next?
-----------------------

Conventionally the next step, having built and |Maven|-``install``\ ed 
|business-manager|, would be to build a |site-business|. |br|
This is because a change to |business-manager| operations and/or
configurations will only be visible once a |site-business| is subsequently
built and deployed (as the build of a |site-business| would use the latest 
``install``\ ed |business-manager|).
