.. include:: ../../../global.rst

.. _installation_components_dose_response_manager:

|dose-response-manager|
=======================

|dose-response-manager| provides the |WS| front-end to the dose-response
fitting functionality.

.. note:: TODO: Test and explain that if there is no :term:`dose-response data`
   then it is not necessary to build/deploy |dose-response-jni|,
   |dose-response-manager|!

Dependencies
------------

 * |Maven| (build)
 * Java 7 or higher (build, deploy)
 * Java Servlet Container, e.g. Apache |Tomcat| (deploy)
 * |dose-response-jni|

Initial installation
--------------------

Download the project source and go to this component's root directory (i.e.
`here <https://bitbucket.org/gef_work/ap_predict_online/src/master/dose-response-manager/>`_)
and follow the steps below.

  #. |T| Copy |dose-response-jni| :file:`target/dose-response-fitter.jar`
     to :file:`lib/dose-response-fitter.jar`
  #. |T| Copy |dose-response-jni| :file:`target/libfittingdoseresponse.so`
     to :file:`lib/libfittingdoseresponse.so`
  #. |T| Copy `src/properties/sample.env.properties <https://bitbucket.org/gef_work/ap_predict_online/src/master/dose-response-manager/src/properties/sample.env.properties>`_
     to :file:`src/properties/env.properties`

Configuration
-------------

:file:`src/properties/env.properties`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   soap.location=

|WS| |URL| of this component, e.g. 'http://localhost:18180/fdr_manager-0.0.1-SNAPSHOT/fws/'

::

   log.file=

Component log file location on disk, e.g. 'logs/dose-response-manager.log'.

::

   log.level=

Component log level, e.g. ``(trace|debug|info|warn|error|fatal)``.

Build
-----

``-P it``
^^^^^^^^^

The |Maven| `profile` to use. At the moment the :file:`pom.xml` defines only
the use of the ``it`` profile for integration testing.

Example build instructions :

::

   cd <ap_predict_online>/dose-response-manager
   mvn -P it clean verify

.. note:: Building with the integration testing activated will require that
   the ".so" files of :file:`libfittingdoseresponse.so` and libjson can be
   found in the system library path. |br|
   e.g ``export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:<ap_predict_online>/dose-response-manager/lib:/home/me/mylibjson/lib``

Deploy
------

If the build command has completed successfully the webapp ``.war`` file should be
in the ``target`` directory. Copy this to the servlet container's relevant file, e.g
for |Tomcat|, the ``webapps`` directory.

.. note:: As in the `Build`_ situation, the ".so" files of :file:`libfittingdoseresponse.so`
   and libjson will need to be found by the deployed ``.war`` file. |br|
   For |Tomcat| this would mean something like the following before start-up.
   #. Copy :file:`libfittingdoseresponse.so` to a "lib" directory
   #. ``export JAVA_OPTS="-Djava.library.path=lib"`` (i.e. the directory in the previous step).
   #. ``export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/me/mylibjson/lib``

Run
---

See :ref:`start <maintenance_general_start>`.

Start-up problems
-----------------

``java.lang.UnsatisfiedLinkError: no fittingdoseresponse in java.library.path``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Indicates that :file:`libfittingdoseresponse.so` can't be found in the 
``java.library.path`` (which is generally used by Java to locate native 
libraries). Ensure that the appropriate ``JAVA_OPTS="-Djava.library.path=<directory of libfittingdoseresponse.so>"``
value is set before starting.