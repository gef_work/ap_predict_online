.. include:: ../../global.rst

.. _installation_components_general_database_choice:

Database choice
----------------

The |app-manager|, |business-manager|, |client|, |client-direct| and
|site-business| components require a :term:`local` database to be configured.

Currently only |MySQL|, |Oracle| (10g) and |HSQL| database use is available
pre-configured.

.. warning:: If an unavailable db vendor is used then another |Spring| 
   :ref:`profile <tools_spring_profiles>` in the 
   :file:`src/main/resources/META-INF/spring/ctx/data/appCtx.database.xml` file
   is required (e.g. see |app-manager|\ 's
   `appCtx.database.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/app-manager/src/main/resources/META-INF/spring/ctx/data/appCtx.database.xml>`_)
   and the :file:`pom.xml` (derived from :file:`sample.pom.xml`) to include the
   vendor-specific JDBC driver. |br| 
   A corresponding build arg ``-Dspring.profiles.active=`` specific to the new
   vendor will be needed at build time and run time. |br|
   The ability to modify :file:`appCtx.database.xml` locally to accommodate
   alternative database vendors is not yet implemented as the file is under
   version control. Until such time any :term:`local` modifications to this file
   may in the future result in version control conflicts.

For each potential database system you intend to use in a deployment
environment, substitute the following where applicable :

`<deploy.env>` = ``dev`` (see [#f1]_) |br|
`<deploy.db_vendor>` = ``(embedded|mysql|oracle10g|<your db vendor, e.g. postgres>)``

:file:`pom.xml`
^^^^^^^^^^^^^^^

Whereas the |HSQL| driver is built into :file:`sample.pom.xml` files out of 
necessity, if you're using |MySQL|, |Oracle|, or your preferred db, then
:file:`pom.xml` files must contain references to your database driver, e.g.

::

    <dependency>
      <groupId>com.oracle</groupId>
      <artifactId>ojdbc6</artifactId>
      <version>11.2.0.2.0</version>
    </dependency>

    <dependency>
      <groupId>mysql</groupId>
      <artifactId>mysql-connector-java</artifactId>
      <version>5.1.15</version>
    </dependency>

:file:`src/properties/database/dev.database.<deploy.db_vendor>.properties`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

(Using |client-direct| as an example ...) |br|
|T| Copy `src/properties/database/sample.database.spring.properties <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-direct/src/properties/database/sample.database.spring.properties>`_ 
to :file:`src/properties/database/dev.database.<deploy.db_vendor>.properties` e.g. :file:`dev.database.mysql.properties`.
 
Adjust your JDBC connectivity settings to your :term:`local` environment.

:underline:`Example settings` (for |MySQL|) :

For |app-manager|, |business-manager|, |site-business| components.

:: 

   app_manager.database.driverClassName=com.mysql.jdbc.Driver
   app_manager.database.url=jdbc:mysql://localhost:4003/app_manager
   business_manager.database.url=jdbc:mysql://127.0.0.1:4003/business_manager

.. note:: Historically ``?autoDeserialize=true`` was appended for |MySQL| but
   this has been removed (since mid-2018) as data is manually (de)serialised.

For |client| and |client-direct| components.

::

   client_direct.database.url=jdbc:mysql://localhost:4003/client_direct?useUnicode=true&characterEncoding=UTF-8&characterSetResults=utf8&connectionCollation=utf8_general_ci   

.. note:: In the case of an ``embedded`` `<deploy.db_vendor>` being active in a
   deployed environment a corresponding :file:`dev.database.embedded.properties`
   file does not need to be created as all database connectivity and activity
   is managed internally. |br|
   Note also that all content written to an ``embedded`` database is destroyed
   when the application is stopped.

.. _installation_components_general_i18n:

Internationalisation
--------------------

The |client| and |client-direct| web interfaces are for the most part
internationalised but only using online translations of English to Spanish and
Chinese (Traditional) rather than accurate translation services. Some
untranslated messages originating from the business logic, particularly error
messages, will appear in the web interface.

Language bundles are in :file:`src/main/resources/bundle/`.

Bundles are referenced in the 
:file:`src/main/resources/META-INF/spring/ctx/appCtx.view.xml` ``messageSource``
bean definition.

FAQ

 #. How do I modify :term:`site-specific` messages? |br|
    At the moment only |client| has this feature, whereby any entries in 
    :file:`src/main/resources/bundle/site{*}.properties` can be changed to
    anything :term:`site-specific`.

 #. How do I add a new language bundle, e.g. Italian? |br|
    The appropriate new language bundle files will need to be copied from existing examples,
    e.g. create :file:`index_it.properties` based on :file:`index.properties`, and 
    :file:`appCtx.view.xml` updated to include the new bundle reference.  
    (Note however the potential for :ref:`version control conflicts <version_control_conflicts>`)

.. _installation_components_general_jasypt:

JASYPT
------

The ``JASYPT_PWD`` environment variable needs to be assigned in situations where;

 #. integration testing involves property file use, and,
 #. application runtime.

 For example:

 * Bash environment : ``export JASYPT_PWD=<encrypting password>``
 * Eclipse : ``Run -> Run Configurations -> Environment tab``.

This is whether the properties file value-encoding facilities of Jasypt are used or not - in the
latter case set '<encrypting password>' to be an empty string.

.. note:: Keep in mind that it is better to restrict access rights on any files
   which may contain the ``JASYPT`` password.

.. rubric:: Footnotes

.. [#f1] This was originally intended to be one of ``(dev|test|live)``,
         depending on the particular deployment environment, however only
         ``dev`` has ever been utilised (and may itself be removed at some
         point). |br|
         In |business-manager| the value is hardcoded. In |app-manager|,
         |client|, and |client-direct| it needs to be specified.