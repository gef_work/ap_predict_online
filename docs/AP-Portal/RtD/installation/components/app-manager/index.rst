.. include:: ../../../global.rst

|app-manager|
=============

|app-manager| contains code to invoke and monitor |ApPredict|.

|app-manager| was designed to enable the isolation of |ApPredict| invocation so
that any client (e.g. a |WS|-invoking Perl script similar to
`query_business_manager_WSS.pl <https://bitbucket.org/gef_work/ap_predict_online/src/master/business-manager/tools/query_business_manager_WSS.pl>`_
- not necessarily either of the |AP-Portal| |site-business| and/or |client-direct|
components) could call it to run |ApPredict|. |br|
It was also isolated so that it could reside on hardware which was able to run
many concurrent invocations, e.g. on a workstation with a lot of |RAM| and
processors, without degrading the client's response times.

Below is a diagrammatic representation |app-manager| |ApPredict| invocation
operations (see `tools/prepare_<appredict_invocation_mechanism>.sh`_,
`tools/local_runner.sh`_ and `tools/<appredict_invocation_mechanism>.sh`_
for more info).

.. figure:: ../../../_static/images/app-manager_overview.png
   :width: 1200px
   :figclass: align-center
   :alt: app-manager overview


Dependencies
------------

 * Maven (build)
 * Java 7 or higher (build, deploy)
 * Java Servlet Container, e.g. Apache |Tomcat| (deploy)
 * Database (build, deploy)

.. _installation_components_app_manager_initial:

Initial installation
--------------------

Download the project source and go to this component's root directory (i.e.
`here <https://bitbucket.org/gef_work/ap_predict_online/src/master/app-manager/>`_)
and follow the steps below.

 * Either ...

  #. Run `tools/one_time_copying.sh <https://bitbucket.org/gef_work/ap_predict_online/src/master/app-manager/tools/one_time_copying.sh>`_

 * ... or ...

  #. |T| Copy `sample.pom.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/app-manager/sample.pom.xml>`_ to :file:`pom.xml`
  #. |T| Copy `src/main/resources/META-INF/spring/ctx/ws/sample.appCtx.ws.security-incoming.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/app-manager/src/main/resources/META-INF/spring/ctx/ws/sample.appCtx.ws.security-incoming.xml>`_
     to :file:`src/main/resources/META-INF/spring/ctx/ws/appCtx.ws.security-incoming.xml`
  #. |T| Copy `src/main/resources/META-INF/spring/ctx/ws/sample.appCtx.ws.security-outgoing.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/app-manager/src/main/resources/META-INF/spring/ctx/ws/sample.appCtx.ws.security-outgoing.xml>`_
     to :file:`src/main/resources/META-INF/spring/ctx/ws/appCtx.ws.security-outgoing.xml`
  #. |T| Copy `src/properties/database/sample.database.filter.properties <https://bitbucket.org/gef_work/ap_predict_online/src/master/app-manager/src/properties/database/sample.database.filter.properties>`_
     to :file:`src/properties/database/database.filter.properties`
  #. |T| Copy `src/properties/sample.filter.properties <https://bitbucket.org/gef_work/ap_predict_online/src/master/app-manager/src/properties/sample.filter.properties>`_
     to :file:`src/properties/filter.properties`
  #. |T| Copy `src/properties/sample.spring.properties <https://bitbucket.org/gef_work/ap_predict_online/src/master/app-manager/src/properties/sample.spring.properties>`_
     to :file:`src/properties/spring.properties`

 * ... then ...

  #. Follow :ref:`database choice <installation_components_general_database_choice>` instructions.

  #. |T| Copy the ``bash`` shell scripts and RelaxNG schema files, e.g. ...

     #. :file:`*.sh`
     #. :file:`mathml2.rng`

     ... from `/app-manager/tools/ <https://bitbucket.org/gef_work/ap_predict_online/src/master/app-manager/tools>`_
     to your equivalent of |Tomcat|'s `CATALINA_HOME`. |br|
     Continue to read `tools/prepare_<appredict_invocation_mechanism>.sh`_,
     `tools/local_runner.sh`_  and `tools/<appredict_invocation_mechanism>.sh`_
     for further configuration instructions.
  #. |T| Create the directory ``procdir`` in your equivalent of |Tomcat|'s
     `CATALINA_HOME`

 * ... finally

  #. Edit each of the copied files according to your desired `configuration`_.

Configuration
-------------

:file:`pom.xml`
^^^^^^^^^^^^^^^

It is likely that there is a :term:`site-specific` database driver (such as a
|MySQL| driver) which needs adding to the :file:`pom.xml` file.

:file:`src/properties/filter.properties`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   app.soap.location=

|WS| |URL| of this component, e.g. 'http://localhost:18380/app_manager-0.0.4/aws/'

::

   app.soap.wsdl=

This component's WSDL name, e.g. 'app_services.wsdl' |br|
The combination of ``app.soap.location`` and ``app.soap.wsdl``, e.g. 
'http://localhost:18380/app_manager-0.0.4/aws/app_services.wsdl'
should reveal the SOAP web service when the component is running.

::

   business.soap.location=

|WS| |URL| of |site-business| component, e.g. 'http://localhost:18080/site_business-0.0.2/bws/'

::

   log.file=

Component log file location on disk, e.g. 'logs/app-manager.log'

::

   log.level.app_manager=

Log level of this component, e.g. ``(trace|debug|info|warn|error|fatal)``.

::

   log.level.non_app_manager

Log level of code from other libraries, e.g. ``(trace|debug|info|warn|error|fatal)``.

:file:`src/properties/spring.properties`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   base.dir=

Directory used as abase for |ApPredict| invocations, e.g. '/home/user/app_run/'

.. warning:: Assign cautiously! |ApPredict| is invoked in subdirectories of this
   directory and these subdirectories are created and destroyed
   (by `PostSimulationRunTidyUp.java <https://bitbucket.org/gef_work/ap_predict_online/src/master/app-manager/src/main/java/uk/ac/ox/cs/nc3rs/app_manager/srvact/postrun/PostSimulationRunTidyUp.java>`_)
   as appropriate.

::

   monitor.frequency=

Simulation process monitoring frequency (in seconds), e.g. '15'.

::

   invocation.limit=

Maximum number of concurrent |ApPredict| invocations, e.g. '8'.

::

   securement.app.username=

Component |WSS| username.

::

   securement.app.password=

Component |WSS| password.

::

   securement.business.username=

|site-business| |WSS| username.

::

   securement.business.password=

|site-business| |WSS| password.

:file:`src/main/resources/META-INF/spring/ctx/ws/appCtx.ws.security-incoming.xml`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Generally no change necessary.


:file:`src/main/resources/META-INF/spring/ctx/ws/appCtx.ws.security-outgoing.xml`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Generally no change necessary.


:file:`src/properties/database/database.filter.properties`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Generally no change necessary.

:file:`src/properties/database/dev.database.<deploy.db_vendor>.properties`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. seealso:: :ref:`Database choice <installation_components_general_database_choice>`

.. _installation_components_app_manager_tools:

:file:`tools/prepare_<appredict_invocation_mechanism>.sh`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

One of the following files will be your simulation preparation script of choice
according to your local |ApPredict| installation.

 #. :file:`prepare_docker.sh`
 #. :file:`prepare_localfs.sh`
 #. :file:`prepare_singularity.sh`

Its role generally is to copy across (or symlink) the |ApPredict| invoker of
choice file (see `tools/<appredict_invocation_mechanism>.sh`_) into the
directory in which the simulation will run (i.e. a subdirectory of `base.dir`
(as defined in `src/properties/spring.properties`_' `base.dir`)), and to swap a
couple of placeholder values therein.

The name `prepare.sh` is hardcoded into
`ApPredictRunPreProcessor.java <https://bitbucket.org/gef_work/ap_predict_online/src/master/app-manager/src/main/java/uk/ac/ox/cs/nc3rs/app_manager/srvact/prerun/ApPredictRunPreProcessor.java>`_,
and therefore depending on how you intend to run |ApPredict| you will need to
symlink one of above files to :file:`prepare.sh`, as below ...

.. figure:: ../../../_static/images/CATALINA_HOME-ls-l.png
   :figclass: align-center
   :alt: CATALINA_HOME directory listing

:file:`tools/local_runner.sh`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You shouldn't need to make any changes to this file.

The name `local_runner.sh` is hardcoded in
`ApPredictInvoker.java <https://bitbucket.org/gef_work/ap_predict_online/src/master/app-manager/src/main/java/uk/ac/ox/cs/nc3rs/app_manager/srvact/prerun/ApPredictInvoker.java>`_.

`<https://bitbucket.org/gef_work/ap_predict_online/src/master/app-manager/tools/local_runner.sh>`_
is the script that will be called by :file:`ApPredictInvoker.java` and it will ... 

 #. ``cd`` to the :term:`local` filesystem dir (as determined by the `base.dir`
    property and job identifier), then
 #. run whichever :file:`ApPredict.sh` wrapper script (as derived from
    :file:`ApPredict.sh`, :file:`Singularity.sh` or :file:`Docker.sh`)
    which will in turn invoke the `ApPredict` binary.
 #. It will also generate the `VRE_INFO` and `VRE_OUTPUT` files, the former
    contains general environment info, the latter contains the `stdout` and
    `stderr` from the invocation, both of which get persisted if the 
    simulation runs successfully.
 
:file:`tools/<appredict_invocation_mechanism>.sh`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

One of the following files will be your |ApPredict| invoker of choice for your
installation. The content of which you may need to modify according to your
local |ApPredict| installation.

 #. :file:`ApPredict.sh`
 #. :file:`Singularity.sh`
 #. :file:`Docker.sh`

The name `ApPredict.sh` is hardcoded in
`ApPredictInvoker.java <https://bitbucket.org/gef_work/ap_predict_online/src/master/app-manager/src/main/java/uk/ac/ox/cs/nc3rs/app_manager/srvact/prerun/ApPredictInvoker.java>`_,
and therefore you need to have symlinked your relevant :file:`prepare_<appredict_invocation_mechanism>.sh`
to :file:`prepare.sh` (as explained in
`tools/prepare_<appredict_invocation_mechanism>.sh`_), such that the
|ApPredict| invoker of choice wrapper script will be used (via an invocation
of :file:`local_runner.sh`).

``procdir``
^^^^^^^^^^^

``procdir`` is used to deposit system process data into files which can be read
by the application for the purpose of monitoring the system process state, i.e.
to determine if |ApPredict| is still running. |br|
It is hardcoded in two places :

 #. File `appCtx.int.processMonitoring.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/app-manager/src/main/resources/META-INF/spring/ctx/integration/appCtx.int.processMonitoring.xml>`_
    has hardcoded (in ``int-file:inbound-channel-adapter``) the directory 
    ``procdir`` where the application expects system process data files to be
    deposited by the system at runtime upon |ApPredict| invocation.
 #. :file:`local_runner.sh` also has the same hardcoding.

Build
-----

.. seealso:: :ref:`Database choice <installation_components_general_database_choice>`
   and :ref:`Spring profiles <tools_spring_profiles>` for additional information.

``-Dspring.profiles.active=``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Options are currently ``app_manager_(embedded|mysql|oracle10g)``.

.. warning:: At build time it is **important** to use the 
   ``-Dspring.profiles.active=app_manager_embedded`` arg because during
   integration testing rubbish may be written to the database so it is important
   to use the embedded database during this process.

``-Ddeploy.db_vendor=``
^^^^^^^^^^^^^^^^^^^^^^^

The name of the database vendor, e.g. ``mysql``, ``postgres``, used in the 
intended deployment (**not build**) environment. The actual name used
corresponds exactly to the `<deploy.db_vendor>` element of the file derived from
:file:`sample.database.spring.properties` during the installation process.

``-Ddeploy.env=``
^^^^^^^^^^^^^^^^^

The environment name, i.e. ``dev``.

:underline:`Example build instructions` (illustrating use of `embedded` during
the build process, and `mysql` in the deploy environment):

::

   cd <ap_predict_online>/app-manager
   mvn clean verify -Dspring.profiles.active=app_manager_embedded -Ddeploy.db_vendor=mysql -Ddeploy.env=dev

Deploy
------

If the build command has completed successfully the webapp ``.war`` file should be
in the ``target`` directory. Copy this to the servlet container's relevant file, e.g
for |Tomcat|, the ``webapps`` directory.

Run
---

See :ref:`start <maintenance_general_start>`.

Start-up problems
-----------------

``Property 'driverClassName' threw exception; nested exception is java.lang.IllegalStateException: Could not load JDBC driver class [appCtx.database.xml_unassigned]``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Probably means that the ``mvn`` build command had the wrong combination of 
`-Ddeploy.db_vendor=`_ and/or `-Ddeploy.env=`_ values assigned. |br|
What generally happens is that the :file:`pom.xml` ``generate-resources`` phase
will concatenate two properties files into :file:`src/main/resources/META-INF/properties/app_manager.properties`
but if the wrong (or no) database properties file is referenced then the
database properties will be missing.

``No bean named 'appDataSource' is defined``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

It could be that wrong ``JAVA_OPTS`` value for ``spring.profiles.active`` is 
being set prior to the component being started,
e.g. ``JAVA_OPTS="-Dspring.profiles.active=app_manager_myyysql"``.

Various notes
-------------

See :ref:`JASYPT <installation_components_general_jasypt>` use.
