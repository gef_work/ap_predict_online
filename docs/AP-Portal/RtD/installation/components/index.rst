.. include:: ../../global.rst

.. _installation_components:

Generic |AP-Portal| Components
==============================

The following are :term:`generic portal components`

 * |app-manager| : |ApPredict|-invoking |WS|.
 * |business-manager-api| : |site-business|-developer |API| to |business-manager|
   operations.
 * |business-manager| : Generic 'business' processing |WS|.
 * |client| : A web interface which talks to the |business-manager|.
 * |client-direct| : A web interface which talks directly to the |app-manager|.
 * |dose-response-jni| : Dose-Response fitting |JNI|.
 * |dose-response-manager| : Dose-Response fitting |WS|.
   (which uses |dose-response-jni|).

The following are modularised components used when building some aspects of the
portal.

 * |client-parent| : |Maven| project build feature used by client components.
 * |client-shared| : Shared functionality across the |client| and
   |client-direct| components.

The following is an example :term:`site-specific` component containing fictional
compound data and processing which interacts with the generic components for
demonstration purposes.

 * |site-business| : Example demonstrator of :term:`site-specific` processing.

Component Installation
----------------------

.. toctree::
   :maxdepth: 2

   app-manager/index
   business-manager/index
   business-manager-api/index
   client/index
   client-direct/index
   client-parent/index
   client-shared/index
   dose-response-jni/index
   dose-response-manager/index
   site-business/index