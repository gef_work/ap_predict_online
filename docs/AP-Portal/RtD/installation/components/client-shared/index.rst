.. include:: ../../../global.rst

.. _installation_components_client_shared:

|client-shared|
===============

|client-shared| is some of the shared operations and configurations between
the |client| and |client-direct| components.

See options for customisable
:ref:`User Authentication and Authorisation <installation_extensibility_authnauthz>`.

See options for customisable
:ref:`user registration <installation_extensibility_registration>`.

Dependencies
------------

 * |Maven| (build)
 * Java :strikethrough:`7` **8** [#f1]_  or higher (build, deploy)

Initial installation
--------------------

Download the project source and go to this component's root directory (i.e.
`client-shared <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/>`_)
and follow the steps below.

 * Either ...

  #. Run `tools/one_time_copying.sh <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/tools/one_time_copying.sh>`_

 * ... or ...

  #. |T| Copy `sample.pom.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/sample.pom.xml>`_
     to :file:`pom.xml`
  #. |T| Copy `src/main/resources/META-INF/data/spring-security/local/sample.users.sql <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/src/main/resources/META-INF/data/spring-security/local/sample.users.sql>`_
     to :file:`src/main/resources/META-INF/data/spring-security/local/users.sql`
  #. |T| Copy `src/main/resources/META-INF/resources/resources/css/site/sample.client-shared-site.css <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/src/main/resources/META-INF/resources/resources/css/site/sample.client-shared-site.css>`_
     to :file:`src/main/resources/META-INF/resources/resources/css/site/client-shared-site.css`
  #. |T| Copy `src/main/resources/META-INF/resources/WEB-INF/spring/ctx/config/sample.appCtx.features.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/src/main/resources/META-INF/resources/WEB-INF/spring/ctx/config/sample.appCtx.features.xml>`_
     to :file:`src/main/resources/META-INF/resources/WEB-INF/spring/ctx/config/appCtx.features.xml`
  #. |T| Copy `src/main/resources/META-INF/resources/WEB-INF/tiles/layout/common/sample.logo.jsp <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/src/main/resources/META-INF/resources/WEB-INF/tiles/layout/common/sample.logo.jsp>`_
     to :file:`src/main/resources/META-INF/resources/WEB-INF/tiles/layout/common/logo.jsp`
  #. |T| Copy `src/main/resources/META-INF/resources/WEB-INF/tiles/layout/common/sample.logout.jsp <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/src/main/resources/META-INF/resources/WEB-INF/tiles/layout/common/sample.logout.jsp>`_
     to :file:`src/main/resources/META-INF/resources/WEB-INF/tiles/layout/common/logout.jsp`
  #. |T| Copy `src/main/resources/META-INF/resources/WEB-INF/tiles/layout/contact/sample.contact.jsp <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/src/main/resources/META-INF/resources/WEB-INF/tiles/layout/contact/sample.contact.jsp>`_
     to :file:`src/main/resources/META-INF/resources/WEB-INF/tiles/layout/contact/contact.jsp`
  #. |S| Copy `src/main/resources/WEB-INF/spring/authn/sample.appCtx.bespoke.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/src/main/resources/WEB-INF/spring/authn/sample.appCtx.bespoke.xml>`_
     to :file:`src/main/resources/WEB-INF/spring/authn/appCtx.bespoke.xml`
  #. |S| Copy `src/main/resources/WEB-INF/spring/authn/sample.appCtx.sitePreauthFilter.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/src/main/resources/WEB-INF/spring/authn/sample.appCtx.sitePreauthFilter.xml>`_
     to :file:`src/main/resources/WEB-INF/spring/authn/appCtx.sitePreauthFilter.xml`
  #. |S| Copy `src/main/resources/WEB-INF/spring/sample.root-context.site.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/src/main/resources/WEB-INF/spring/sample.root-context.site.xml>`_
     to :file:`src/main/resources/WEB-INF/spring/root-context.site.xml`
  #. |T| Copy `src/properties/sample.cs-filter.properties <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/src/properties/sample.cs-filter.properties>`_
     to :file:`src/properties/cs-filter.properties`

 * ... finally

  #. Edit each of the copied files according to your desired `configuration`_.

Configuration
-------------

:file:`pom.xml`
^^^^^^^^^^^^^^^

It may necessary in some circumstances, e.g. if there is :term:`site-specific` access
control code being overlayed into the |client| ``src/main/java``
directory, or if using a |MySQL| driver, to adapt the :file:`pom.xml` file for
|Maven|-building the component.

:file:`src/main/resources/META-INF/data/spring-security/local/users.sql`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Adjust this according to your expected usage requirements :

  * If you are using a :term:`local` |client|\- and/or |client-direct|\-specific
    |clients-prepopulated| mechanism for authentication and authorisation, this
    file is required. |br|
    Please also check the following **Note**.
  * If you are using a |clients-bespoke| mechanism then this file is not
    required.

.. note:: If there is a new registration request the requesting user's details
   are not automatically written to the user database. |br|
   New users must be manually added to this :file:`users.sql` file and the
   system restarted -- for this reason it is better to create a number of
   unissued usernames and passwords (commensurate with the total anticipated
   usage) and then distribute them to new registrations as they arrive.

:file:`src/main/resources/META-INF/resources/resources/css/site/client-shared-site.css`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Generally no change necessary.

.. _installation_components_client_shared_configfeatures:

:file:`src/main/resources/META-INF/resources/WEB-INF/spring/ctx/config/appCtx.features.xml`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Adjust the visibility of certain portal features (e.g. entering ion channel spread values) to 
certain groups of users as determined by their |role|.

.. seealso:: :ref:`Authentication Mechanisms <installation_extensibility_authnauthz_mechanisms>`

:file:`src/main/resources/META-INF/resources/WEB-INF/spring/ctx/appCtx.multipart.xml`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Generally no change necessary.

  * ``filterMultipartResolver`` controls the maximum file size which can be uploaded.
  * ``fileStorageCellML`` and ``fileStoragePK`` control the enforcement of ``UTF-8`` encoding
    file content.

.. seealso:: :ref:`src/properties/cs-filter.properties <installation_components_client_shared_properties>`
             for assignment of ``multipart.fileupload.maxinmemorysize`` and
             ``multipart.fileupload.maxuploadsize``.

.. seealso:: `MySQL's max_allowed_packet <https://dev.mysql.com/doc/refman/5.7/en/server-system-variables.html#sysvar_max_allowed_packet>`_,
             which may need to be set to perhaps 40Mb or more, depending on PK file data content
             size, and, |br| 
             `uk.ac.ox.cs.compbio.client_shared.entity.PortalFile#MAX_SIZE <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/src/main/java/uk/ac/ox/cs/compbio/client_shared/entity/PortalFile.java>`_ 

:file:`src/main/resources/META-INF/resources/WEB-INF/tiles/layout/common/logo.jsp`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Adjust the HTML according to the desired appearance of the top-left of the page
which is usually where the logo is found.

The example :file:`logo.jsp` content below places the fictional company logo in
the top left of the display by referencing the corresponding logo image placed
in the ``src/main/resources/META-INF/resources/resources/img/site/`` directory
prior to build.

::

   <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
   <img src="<c:url value="/resources/img/site/company_logo.png" />" alt="Company logo" />

:file:`src/main/resources/META-INF/resources/WEB-INF/tiles/layout/common/logout.jsp`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Adjust according to your logging out mechanism.

:file:`src/main/resources/META-INF/resources/WEB-INF/tiles/layout/contact/contact.jsp`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Adjust the HTML to provide contact details of the relevant people.

An example :file:`contact.jsp` content may be as follows.

::

   Click the following links to email support :

   <p>
     Scientific support : <a href="mailto:ap-portal-scientific@company.com">click here</a>
   </p>
   <p>
     Technical support : <a href="mailto:ap-portal-technical@company.com">click here</a>
   </p>

:file:`src/main/resources/WEB-INF/spring/authn/appCtx.bespoke.xml`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Generally no change necessary.

:file:`src/main/resources/WEB-INF/spring/authn/appCtx.sitePreauthFilter.xml`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Needs to be adjusted if adopting the |clients-bespoke| mechanism.

:file:`src/main/resources/WEB-INF/spring/root-context.site.xml`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Generally no change necessary.

.. _installation_components_client_shared_properties:

:file:`src/properties/cs-filter.properties`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. seealso:: For the following email-related properties take a look at the
             `JavaMail API documentation <https://javamail.java.net/nonav/docs/api/com/sun/mail/smtp/package-summary.html>`_

::

   mail.smtp.host=

New registration email mail server |SMTP| host, e.g. 'smtp.company.com'.

::

   mail.smtp.port=

New registration email mail server port, e.g. '25' or '587'. 

.. warning:: **Always** assign an integer value here, even if no |SMTP| service
   is available.

::

   mail.smtp.username=

New registration email mail server user name.

::

   mail.smtp.password=

New registration email mail server user's password.

::

   mail.regn.from=

New registration email "From" email address, e.g. 'noreply-portal-registrations@company.com'.

::

   mail.regn.to=

New registration email "To" email address, e.g. 'portal-registrations@company.com'.

::

   mail.regn.subject=

New regisration email subject title, e.g. 'New registration'.

::

   mail.transport.protocol=

New registration email mail transport protocol (see :file:`appCtx.mail.xml`), e.g. 'smtp'.

::

   mail.smtp.auth=

New registration email |SMTP| authentication required (see :file:`appCtx.mail.xml`), e.g.
``(true|false)``.

::

   mail.smtp.connectiontimeout=

New registration email |SMTP| connection timeout (i.e. time to respond once connected) in
milliseconds (see :file:`appCtx.mail.xml`), e.g. '2000'.

::

   mail.smtp.timeout=

New registration email |SMTP| timeout (to establish a connection) in milliseconds (see
:file:`appCtx.mail.xml`), e.g. '2000'.

::

   mail.smtp.starttls.enable=

New registration email |SMTP| connection instruction to start |TLS| (see :file:`appCtx.mail.xml`),
e.g. ``(true|false)``.

::

   mail.smtp.ssl.enable=

New registration email |SMTP| connection instruction to start |TLS| (see :file:`appCtx.mail.xml`),
e.g. ``(true|false)``.

::

   mail.debug=

New registration email debugging switch (see :file:`appCtx.mail.xml`), e.g. ``(true|false)``.

::

   multipart.fileupload.maxinmemorysize=

From the `Spring docs <https://docs.spring.io/autorepo/docs/spring/3.2.3.RELEASE/javadoc-api/org/springframework/web/multipart/commons/CommonsFileUploadSupport.html>`_ 
: 

   *Set the maximum allowed size (in bytes) before uploads are written to disk. Uploaded files will
   still be received past this amount, but they will not be stored in memory. Default is 10240,
   according to Commons FileUpload.*

::

   multipart.fileupload.maxuploadsize=

From the docs `here <https://docs.spring.io/autorepo/docs/spring/3.2.3.RELEASE/javadoc-api/org/springframework/web/multipart/commons/CommonsFileUploadSupport.html>`_ 
:

   *Set the maximum allowed size (in bytes) before uploads are refused. -1 indicates no limit
   (the default)*.

::

   log.file.client_shared=

Component log file location on disk, e.g. 'logs/client-shared.log'.

::

   log.level.client_shared=

Component log level, e.g. ``(trace|debug|info|warn|error|fatal)``.

::

   log.level.general=

Log level of code from other libraries, e.g. ``(trace|debug|info|warn|error|fatal)``.

Build
-----

Example build instructions :

::

   cd <ap_predict_online>/client-shared
   mvn clean install

Deploy
------

This component when built should not be deployed into a servlet container,
instead it is |Maven|-``install``\ ed in the :term:`local` |Maven| repository
and then a subsequent build of |client| and/or |client-direct| retrieves the
built files and uses them.

Run
---

See `Deploy`_

Start-up problems
-----------------

See `Deploy`_

Done that! What's next?
-----------------------

Conventionally the next step, having built and |Maven|-``install``\ ed
|client-shared|, would be to build |client| and/or |client-direct|. |br|
This is because both of |client| and |client-direct| are dependent on
|client-shared| (and |client-parent|) and need to be kept in-sync.

.. rubric:: Footnotes

.. [#f1] Since early 2019.