.. include:: ../../../global.rst

.. _installation_components_business_manager_api:

|business-manager-api|
======================

|business-manager-api| contains the |API| to |business-manager| operations, i.e.
it allows a/your |site-business| to pass data to the generic |business-manager|
operations.

Dependencies
------------

 * |Maven| (build)
 * Java 7 or higher (build, deploy)

Initial installation
--------------------

Download the project source and go to this component's root directory (i.e.
`here <https://bitbucket.org/gef_work/ap_predict_online/src/master/business-manager-api/>`_)
and follow the steps below.

Configuration
-------------

No configuration necessary.

Build
-----

.. _installation_components_business_manager_api_install:

Example build instructions :

::

   cd <ap_predict_online>/business-manager-api
   mvn clean install

Deploy
------

This component when built should not be deployed into a servlet container,
instead it is |Maven|-``install``\ ed in the :term:`local` |Maven| repository
and then a subsequent build of |business-manager| or |site-business| retrieves
the built ``.war`` file and uses it.

Run
---

See `Deploy`_

Start-up problems
-----------------

See `Deploy`_

Done that! What's next?
-----------------------

Conventionally the next step, having built and |Maven|-``install``\ ed
|business-manager-api|, would be to build |business-manager| and then a
|site-business|. |br|
This is because both |business-manager| and |site-business| are dependent on
|business-manager-api| and need to be kept in-sync.

