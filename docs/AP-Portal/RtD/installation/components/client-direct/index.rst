.. include:: ../../../global.rst

.. _installation_components_client_direct:

|client-direct|
===============

.. figure:: ../../../_static/images/client-direct.png
   :align: left
   :alt: client-direct appearance
   :width: 300px

|client-direct| provides the web interface to the |app-manager| component and
uses a number of different web pages, i.e. it's not an |SPA|.

The |client-direct| interface is intended for running simulations using manually
input values, e.g. pIC50, Hill Coefficients.

Dependencies
------------

 * |Maven| (build)
 * Java :strikethrough:`7` **8** [#f1]_  or higher (build, deploy)
 * Java Servlet Container, e.g. Apache |Tomcat| (deploy)
 * Database (build, deploy)
 * A |Maven|-``install``\ ed |client-parent| and |client-shared|. See 
   :ref:`client-parent <installation_components_client_parent>` and
   :ref:`client-shared <installation_components_client_shared>`

General topics
--------------

 * :ref:`Database <installation_components_general_database_choice>`
 * :ref:`Internationalisation <installation_components_general_i18n>`
 * :ref:`User Authentication and Authorisation <installation_extensibility_authnauthz>`.
 * :ref:`User Registration <installation_extensibility_registration>`.

Initial installation
--------------------

Download the project source and go to this component's root directory (i.e.
`here <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-direct/>`_)
and follow the steps below.

 * Either ...

  #. Run `tools/one_time_copying.sh <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-direct/tools/one_time_copying.sh>`_

 * ... or ...

  #. |T| Copy `sample.pom.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-direct/sample.pom.xml>`_
     to :file:`pom.xml`
  #. |S| Copy `src/main/resources/META-INF/spring/ctx/config/sample.appCtx.config.cellModels.site.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-direct/src/main/resources/META-INF/spring/ctx/config/sample.appCtx.config.cellModels.site.xml>`_
     to :file:`src/main/resources/META-INF/spring/ctx/config/appCtx.config.cellModels.site.xml`
  #. |S| Copy `src/main/resources/META-INF/spring/ctx/config/sample.appCtx.config.site.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-direct/src/main/resources/META-INF/spring/ctx/config/sample.appCtx.config.site.xml>`_
     to :file:`src/main/resources/META-INF/spring/ctx/config/appCtx.config.site.xml`
  #. |T| Copy `src/main/resources/META-INF/spring/ctx/ws/sample.appCtx.ws.security-outgoing.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-direct/src/main/resources/META-INF/spring/ctx/ws/sample.appCtx.ws.security-outgoing.xml>`_
     to :file:`src/main/resources/META-INF/spring/ctx/ws/appCtx.ws.security-outgoing.xml`
  #. |V| Copy `src/main/webapp/resources/js/site/sample.site.js <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-direct/src/main/webapp/resources/js/site/sample.site.js>`_
     to :file:`src/main/webapp/resources/css/site/site.js`
  #. |T| Copy `src/properties/database/sample.database.filter.properties <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-direct/src/properties/database/sample.database.filter.properties>`_
     to :file:`src/properties/database/database.filter.properties`
  #. |T| Copy `src/properties/database/sample.database.spring.properties <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-direct/src/properties/database/sample.database.spring.properties>`_
     to :file:`src/properties/database/dev.database.embedded.properties`
  #. See :ref:`Database choice <installation_components_general_database_choice>`
  #. |T| Copy `src/properties/sample.filter.properties <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-direct/src/properties/sample.filter.properties>`_
     to :file:`src/properties/filter.properties`
  #. |T| Copy `src/properties/sample.spring.properties <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-direct/src/properties/sample.spring.properties>`_
     to :file:`src/properties/spring.properties`

If you intend to have a |client-direct|\-specific |clients-prepopulated|
:ref:`user authentication mechanism <installation_extensibility_authnauthz>` database then also do
the following :

  #. |T| Copy `src/main/resources/META-INF/data/spring-security/local/sample.users.sql <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-direct/src/main/resources/META-INF/data/spring-security/local/sample.users.sql>`_
     to :file:`src/main/resources/META-INF/data/spring-security/local/users.sql`

 * ... finally

  #. Edit each of the copied files according to your desired `configuration`_.

Configuration
-------------

:file:`pom.xml`
^^^^^^^^^^^^^^^

It may necessary in some circumstances, e.g. if there is :term:`site-specific`
access control code being overlayed into the |client-direct| :file:`src/main/java`
directory, or if using a |MySQL| driver, to adapt the :file:`pom.xml` file for
|Maven|-building the component.

.. _installation_components_client_direct_users:

:file:`src/main/resources/META-INF/data/spring-security/local/users.sql`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Adjust this according to your expected usage requirements.

  * If you are using a :term:`local` |client-direct|\-specific
    |clients-prepopulated| mechanism for authentication and authorisation, this
    file is required. |br|
    Please also check the following **Note**.
  * If you are using a |clients-bespoke| mechanism, or if |clients-prepopulated| users
    are shared between |client| and |client-direct| installations (and hence defined
    in |client-shared|), then this file is not required.

.. note:: If there is a new registration request the requesting user's details
   are not automatically written to the user database. |br|
   New users must be manually added to this :file:`users.sql` file and the
   system restarted -- for this reason it is better to create a number of
   unissued usernames and passwords (commensurate with the total anticipated
   usage) and then distribute them to new registrations as they arrive.

.. _installation_components_client_direct_config_cellmodels:

:file:`src/main/resources/META-INF/spring/ctx/config/appCtx.config.cellModels.site.xml`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Collection of ion channel models which the user can choose from.

Probably the only modifications likely are :

 #. To comment out any ion channel model(s) which is/are not relevant to the
    portal users, and/or,
 #. To assign a default ion channel model to use by way of assigning a
    `single` ion channel model with a ``defaultModel`` arg with an
    attribute ``value`` of ``true``.

.. warning:: These ion channel models are built into |ApPredict| and the detail
  **MUST** be identical to the values which |ApPredict| expects, particularly the
  ``identifier`` and ``name`` values.

.. _installation_components_client_direct_config_site:

:file:`src/main/resources/META-INF/spring/ctx/config/appCtx.config.site.xml`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   c50Units

Preferred units for displaying inhibitory concentration, e.g. IC50 [nM], IC50 [µM], pIC50. |br|
Cut-and-paste your preferred unit to be the top/first ``<entry />`` value.

::

   configuration -> recommendedPlasmaConcMax

Default `recommended` value for maximum plasma concentration (µM) value. |br|
Users can enter values higher than the recommended maximum if they wish, i.e. this
is not an enforced limit.

::

   configuration -> plasmaConcMin

Minimum plasma concentration (µM). |br|
Users cannot enter values lower than this, i.e. it is an enforced limit.

::

   configuration -> spreads

Define the default variability values for specified ion channels.

.. seealso:: :ref:`Variability Configuration <configuration_variability>`

:file:`src/main/resources/META-INF/spring/ctx/ws/appCtx.ws.security-outgoing.xml`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Generally no change necessary.

:file:`src/main/webapp/resources/css/site/site.js`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Generally no change necessary.

:file:`src/properties/database/database.filter.properties`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   client.database.queryTimeout=

Data query timeout (in milliseconds). Shouldn't need to be higher than 200.

:file:`src/properties/database/dev.database.<deploy.db_vendor>.properties`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. seealso:: :ref:`Database choice <installation_components_general_database_choice>`

:file:`src/properties/filter.properties`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   app_manager.soap.location=

|WS| |URL| of |app-manager| component, e.g. 'http://localhost:18380/app_manager-0.0.4/aws/'

.. seealso:: For the following email-related properties take a look at the
             `JavaMail API documentation <https://javamail.java.net/nonav/docs/api/com/sun/mail/smtp/package-summary.html>`_

::

   log.file.client_direct=

Component log file location on disk, e.g. 'logs/client-direct.log'.

::

   log.level.client_direct=

Component log level, e.g. ``(trace|debug|info|warn|error|fatal)``.

::

   log.level.general=

Log level of code from other libraries, e.g. ``(trace|debug|info|warn|error|fatal)``.

:file:`src/properties/spring.properties`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   recaptcha.private.key=

reCAPTCHA private key.

::

   recaptcha.public.key=

reCAPTCHA public key. If this field is left empty it will be assumed that no
reCAPTCHA is available!

::

   securement.app.username=

|app-manager| |WSS| username.

::
 
   securement.app.password=

|app-manager| |WSS| password.

Build
-----

.. seealso:: :ref:`Database choice <installation_components_general_database_choice>`
   and :ref:`Spring profiles <tools_spring_profiles>` for additional information.

``-Dspring.profiles.active=``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Options are currently ``client-shared_(embedded|mysql|oracle10g),client-shared_(bespoke|prepopulated)``.

.. warning:: At build time it is **important** to include the 
   ``client-shared_embedded`` spring profile during building because during
   integration testing rubbish may be written to the database so it is important
   to use the embedded database during the build process.

``-Ddeploy.db_vendor=``
^^^^^^^^^^^^^^^^^^^^^^^

The name of the database vendor, e.g. ``mysql``, ``postgres``, used in the 
intended deployment (**not build**) environment. The actual name used
corresponds exactly to the `<deploy.db_vendor>` element of the file derived from
:file:`sample.database.spring.properties` during the installation process.

``-Ddeploy.env=``
^^^^^^^^^^^^^^^^^

The environment name, i.e. ``dev``.

:underline:`Example build instructions` (illustrating use of `embedded` during
the build process, and `mysql` in the deploy environment):

::

   cd <ap_predict_online>/client-direct
   mvn clean verify -Dspring.profiles.active=client-shared_embedded,client-shared_prepopulated -Ddeploy.db_vendor=mysql -Ddeploy.env=dev

Deploy
------

If the `Build`_ command has completed successfully the webapp ``.war`` file should be
in the ``target`` directory. Copy this to the servlet container's relevant file, e.g.
for |Tomcat|, the ``webapps`` directory.

Run
---

See :ref:`start <maintenance_general_start>`.

Start-up problems
-----------------

.. seealso:: :ref:`Log file <troubleshooting_logfiles>` troubleshooting.

.. rubric:: Footnotes

.. [#f1] Since early 2019.
