.. include:: ../../../global.rst

.. _installation_components_client_parent:

|client-parent|
===============

|client-parent| contains the |Maven| `parent pom` for determining the shared
build configurations of the client components.

Dependencies
------------

 * |Maven| (build)
 * Java 7 or higher (build, deploy)

Initial installation
--------------------

Download the project source and go to this component's root directory (i.e.
`here <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-parent/>`_)
and follow the steps below.

Configuration
-------------

No configuration necessary.

Build
-----

Example build instructions :

::

   cd <ap_predict_online>/client-parent
   mvn --non-recursive clean install

The optional ``--non-recursive`` instruction prevents the sub-projects from
automatically being built.

Deploy
------

This component when built should not be deployed into a servlet container,
instead it is |Maven|-``install``\ ed in the :term:`local` |Maven| repository
and then a subsequent build of |client-shared| retrieves the built ``.pom``
files and uses it.

Run
---

See `Deploy`_

Start-up problems
-----------------

See `Deploy`_

Done that! What's next?
-----------------------

Conventionally the next step, having built and |Maven|-``install``\ ed
|client-parent|, would be to build |client-shared| and thereafter |client|
and/or |client-direct|. |br|
This is because all of |client-shared|, |client| and |client-direct| are
dependent on |client-parent| and need to be kept in-sync.