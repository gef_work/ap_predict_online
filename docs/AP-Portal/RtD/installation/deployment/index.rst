.. include:: ../../global.rst

Deployment options
==================

Decision chart
--------------

.. note:: In all cases a 'Code User' or 'Code Developer' version" of |ApPredict|
   also needs to be installed (see `official ApPredict build instructions <https://chaste.cs.ox.ac.uk/trac/wiki/ApPredict>`_
   and `sample ApPredict install scripts <https://bitbucket.org/gef_work/ap_predict_online/src/master/tools/ApPredict>`_)!

.. note:: The names |app-manager|, |business-manager|, |business-manager-api|,
  |client|, |client-direct|, |dose-response-jni|, |dose-response-manager| and
  |site-business| are all references to the :term:`generic portal components`
  of |AP-Portal| (|client| and |client-direct| both require |client-parent| and
  |client-shared| to be installed).

::

                          +--------------------+
                          | Do you intend to   |
                          |  manually enter    |
                          | IC50/pIC50 values? |
                          +--------------------+
                           |                  |
                          Yes                No
                           |                  |
            +------------------------+  +------------------+
            | Install app-manager    |  | Do you intend to |
            | and client-direct only |  | install the demo |
            +------------------------+  | system only?     |
                                        +------------------+
                                         |                |
                                        Yes              No
                                         |                |
               +---------------------------+   +--------------------------------+
               | Install app-manager,      |   | Do you have dose-response data |
               | business-manager,         |   | available to use?              |
               | business-manager-api,     |   +--------------------------------+
               | client, dose-response-jni |    |                        |
               | dose-response-manager     |    |                       No
               | and site-business         |   Yes                       |
               +---------------------------+    |       +-------------------------------+
                                                |       | Install app-manager,          |
                     +-------------------------------+  | business-manager,             |
                     | Install app-manager,          |  | business-manager-api and      |
                     | business-manager,             |  | client.                       |
                     | business-manager-api, client, |  | Create your own site-business |
                     | dose-response-jni and         |  +-------------------------------+
                     | dose-response-manager.        |
                     | Create your own site-business |
                     +-------------------------------+

Options diagram
---------------

The illustration below depicts typical installation options (`Option A`_ and
`Option B`_) of all the generic |AP-Portal| components in two scenarios :

.. figure:: ../../_static/images/deployment_diagram.png
   :align: center
   :alt: Deployment diagram
   :width: 700px

Option A
--------

The simplest installation consisting solely of the |client-direct| and
|app-manager| components.

This configuration expects users to manually enter compound IC50, Hill and
saturation values in the portal interface. Simulation results are stored
in |client-direct|.

.. figure:: ../../_static/images/Option_A.png
   :align: center
   :alt: Option A
   :width: 700px

Option B
--------

The more intricate installation involves the interaction of |client|,
**a** |site-business|, |dose-response-manager| (optional) and |app-manager|
components all working together to query :term:`local` compound databases to
retrieve compound IC50, Hill and other values across a range of assays.

.. warning:: A |site-business| must be installed. If you wish to see
   your own compound data then you will need to
   :ref:`create a bespoke <installation_extensibility_site_business>`
   |site-business|.

.. figure:: ../../_static/images/Option_B.png
   :align: center
   :alt: Option B
   :width: 700px