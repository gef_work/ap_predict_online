.. include:: ../../global.rst

.. _installation_minreqs_software:

Software
========

Linux (or similar)
------------------

The majority of |AP-Portal| components, being Java-based, **could** (i.e. it
hasn't been tested) run on Windows, but not the |app-manager| component or
|ApPredict|.

Java 7+ |JDK|
-------------

For example, `Oracle JDK <http://www.oracle.com/technetwork/java/javase/overview/index.html>`_,
OpenJDK or similar.

.. warning:: Since early 2019 |client| and |client-direct| require Java 8
   to run.

.. warning:: Java PermGen errors sometimes occur during |Maven| building
   when using Java 7 - so whilst not essential, perhaps it would be better to
   use Java 8. 

Database
--------

.. warning:: AP-Portal has been running on |MySQL|, |Oracle| (10g) and |HSQL|
   databases. Other databases would be possible but their configuration would
   require the modification of files which are currently under version control,
   i.e. they have not yet been made locally modifiable.

Used to persist data, e.g. |MySQL| or similar.

Apache |Maven|
--------------

The |AP-Portal| component build mechanism.

Servlet container
-----------------

Used to host the |AP-Portal| components, e.g. |Tomcat| or similar.

`Git <https://git-scm.com/>`_
-----------------------------

Version control system used by |AP-Portal| and |ApPredict|.

.. _installation_minreqs_software_appredict:

`ApPredict <https://chaste.cs.ox.ac.uk/trac/wiki/ApPredict>`_
------------------------------------------------------------- 

Sample build operations, which allow for versioned building of |ApPredict| (and
Chaste), are available for
`CentOS 6.5 <https://bitbucket.org/gef_work/ap_predict_online/src/master/docs/ApPredict/install/x86_64/CentOS/6.5/minimal/>`_ 
and for `Fedora 20 <https://bitbucket.org/gef_work/ap_predict_online/src/master/docs/ApPredict/install/x86_64/Fedora/20/MATE_Live/>`_.

.. warning:: Building |ApPredict| (or more specifically the collection of 
   dependencies - invariably referred to as "*chaste-libs*") is potentially a
   complex and time-consuming task. Once the dependencies have been built it
   can be relatively straightforward to (re)build |ApPredict|.