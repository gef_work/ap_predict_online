.. include:: ../../global.rst

.. _installation_minreqs_hardware:

Hardware
========

Servers
-------

.. note:: **Minimum 1** (!?)

Although 2 would be useful to isolate the operations of the |app-manager|\ 's
compute-intensive |ApPredict| simulation invocations if many concurrent 
simulations are anticipated.

All inter-component communication (e.g. |client-direct| ``<-->`` |app-manager|,
or |site-business| ``<-->`` |app-manager|) is via |WS| communication so 
components do not need to reside on the same hardware.

Internet access
---------------

Without direct internet access there will be a significant amount of copying of
files from one machine to another during |AP-Portal| and |ApPredict|
installation.

Direct internet access preferred for :

 #. Downloading |AP-Portal| source code from `bitbucket <https://bitbucket.org/gef_work/ap_predict_online>`_.
 #. Downloading |ApPredict| source code from `github <https://github.com/Chaste/ApPredict>`_.
 #. Downloading library dependencies for |AP-Portal| and |ApPredict|. |br|
    For example, during Maven building of |AP-Portal|, and for Python package
    and |CHASTE| `requirements <https://chaste.cs.ox.ac.uk/trac/wiki/InstallGuides/InstallGuide>`_
    for building of |ApPredict|.

Once built and installed there is no further need for direct internet access
except by |ApPredict| for downloading :term:`variability lookup tables` (see
also :ref:`Variability Configuration <configuration_variability>`).

Processing cores
----------------

.. note:: **Minimum 4, preferably 8-16**.

In theory just 1 but that would be impractical as it would take considerable
time to install the software and operationally the application, once in use,
would become frequently unresponsive.

A single |ApPredict| simulation can take usually between 3 to 7+ minutes, 
depending on aspects such as :

 #. The ion channel model (i.e. some run quicker than others),
 #. The IC50 values and how quickly a "steady state" is achieved.

If |client-direct| is being used then there is only a single invocation of 
|ApPredict| for each simulation run. If however |client| is being used then
there will be an invocation of |ApPredict| for each combination of :

 #. Frequency, e.g. 0.5Hz, 1Hz
 #. Assay (or Assay Group), e.g. Barracuda, PatchXpress/Qpatch.

Potentially therefore |app-manager| could be invoking 10+ concurrent |ApPredict|
simulations if there are more than one frequency and multiple assays -- each
simulation potentially consuming 100% of a core.

The actual number of maximum concurrent simulations is configurable in
|app-manager| such that some cores can remain free to service undemanding
|client| or |client-direct| requests if hosting everything on one machine.

Hard disk space
---------------

.. note:: **Minimum 50 Gb, preferably 500Gb+** [#f1]_.

+----------------------------------------------------------------+---------------------------+
| Item(s)                                                        | Approx. min. size         |
+================================================================+===========================+
||CHASTE| dependency ``lib``\ s, ``bin``\ s, ``include``\ s, etc.| 800Mb                     |
+----------------------------------------------------------------+---------------------------+
||CHASTE| + |ApPredict| source code                              | 3Gb                       |
+----------------------------------------------------------------+---------------------------+
||ApPredict| :term:`variability lookup tables`                   | 5Gb [#f2]_                |
+----------------------------------------------------------------+---------------------------+
||AP-Portal| |PK| processing capability                          | Up to 100Mb per |PK| sim! |
+----------------------------------------------------------------+---------------------------+


Non-|PK| and non-:term:`variability` simulations generally consume very little
hard disk space or |RAM|, however since late 2017 the ability to run both |PK|
and :term:`variability` simulations in the |client-direct| component has
significantly increased the operational memory requirements of |AP-Portal|
operations. There is also now available a much-expanded range of
:term:`variability lookup tables` (see 
:ref:`Variability Configuration <configuration_variability>`).

|RAM|
-----

.. note:: **Minimum 8Gb, preferably 12-16Gb+**.

A large amount of |RAM| is required during the build operations as part of the
installation of |AP-Portal| and |ApPredict|, particularly if building using
multiple cores.

During runtime |RAM| can also be consumed during simulation invocation if
:term:`variability` is being calculated, as each simulation may attempt to load
a 700Mb+ file into |RAM|. [#f3]_

.. rubric:: Footnotes

.. [#f1] With the arrival of |PK| processing (early 2018) there has been a
   commensurate increase in the hard disk space required as there is now the 
   ability to upload |PK| data files in the region of 40Mb - these files, and
   the large results datasets they generate, are persisted in the database and
   so over time the amount of memory consumed may rise considerably if such
   |PK| simulations are not manually deleted after the results have been
   downloaded.

.. [#f2] Originally there were only a couple of :term:`variability lookup tables`
   (which can be 700Mb+ in size) available for |ApPredict| - however since
   late 2017 there are a number of these files available which should ideally
   be available on the local filesystem (see also 
   :ref:`Variability Configuration <configuration_variability>`).

.. [#f3] Originally :term:`variability` processing had been available only in
   the |client| component but since late 2017 it has been possible to also do
   it in the |client-direct| component.