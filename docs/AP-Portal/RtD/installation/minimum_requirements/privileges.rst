.. include:: ../../global.rst

System user privilege level
===========================

No component of |AP-Portal| requires elevated (e.g. root) privileges for either
installation or operation, although this does restrict the port numbers which
|client| and |client-direct| components can listen on some systems and may make
for unsightly |URL|\'s.

Similarly, if the application is installed as a non-privileged user then on
some systems it may not be straightforward to have the components and 
dependent applications automatically started if the native system is rebooted.
For example, ``cron``\'s ``@reboot`` may not work on some systems. In such
cases it may require the assistance of someone with elevated privileges to
create, for example, a ``SysV init`` script.