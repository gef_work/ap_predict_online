.. include:: ../../global.rst

.. _installation_minreqs_checklist:

Guidance Checklist
==================

'R' - Required; 'D' - Desired; 'O' - Optional

Base checklist
--------------

+---+--------------------------------------------------------------------------------------+----+
|   | Requirement                                                                          |    |
+===+======================================================================================+====+
| R | :ref:`Hardware <installation_minreqs_hardware>`                                      |    |
+---+--------------------------------------------------------------------------------------+----+
| R | :ref:`Software <installation_minreqs_software>`                                      |    |
+---+--------------------------------------------------------------------------------------+----+
| R | :ref:`Skills <installation_minreqs_skills>`                                          |    |
+---+--------------------------------------------------------------------------------------+----+
| R | User account [#f1]_ on host server                                                   |    |
+---+--------------------------------------------------------------------------------------+----+
| O | Component restart on reboot, e.g. ``cron``\'s ``@reboot``, ``SysV``                  |    |
+---+--------------------------------------------------------------------------------------+----+
|   |                                                                                      |    |
+---+--------------------------------------------------------------------------------------+----+

For a bespoke |site-business| installation
------------------------------------------

.. seealso:: :ref:`bespoke site-business installation <installation_extensibility_site_business>`

+---+------------------------------------------------------------------------------+----+
|   | Requirement                                                                  |    |
+===+==============================================================================+====+
| R | **Read-only** site assay database account for |AP-Portal|                    |    |
+---+------------------------------------------------------------------------------+----+
| R | Determine assays (e.g PatchXpress, |QSAR|), assay grouping                   |    |
+---+------------------------------------------------------------------------------+----+
| R | Determine if :term:`dose-response data` fitting required                     |    |
+---+------------------------------------------------------------------------------+----+
| R | Determine ion channels, e.g. CaV1.2, hERG                                    |    |
+---+------------------------------------------------------------------------------+----+
| R | Determine CellML models (and the default) to use, e.g. Shannon, ten Tusscher |    |
+---+------------------------------------------------------------------------------+----+
| R | Determine default units, e.g. pIC50, IC50                                    |    |
+---+------------------------------------------------------------------------------+----+
| R | Determine strategies for selecting pIC50 [#f2]_ to use                       |    |
+---+------------------------------------------------------------------------------+----+
| R | |SQL| [#f3]_ to retrieve assay, ion channel, etc., data                      |    |
+---+------------------------------------------------------------------------------+----+
| R | Details of services, e.g. |WS|\s, which supply additional data               |    |
+---+------------------------------------------------------------------------------+----+
| R | |IT| personnel to implement Java/|Spring| code                               |    |
+---+------------------------------------------------------------------------------+----+
|   |                                                                              |    |
+---+------------------------------------------------------------------------------+----+

For a bespoke |client-direct| installation
------------------------------------------

.. seealso:: :ref:`bespoke client authentication <installation_extensibility_authnauthz>`

+---+--------------------------------------------------------------------------+----+
|   | Requirement                                                              |    |
+===+==========================================================================+====+
| D | Company logo image [#f7]_                                                |    |
+---+--------------------------------------------------------------------------+----+
| D | Company login and logout mechanism [#f7]_                                |    |
+---+--------------------------------------------------------------------------+----+
| D | Company "contact" information (e.g. :term:`local` contact emails) [#f7]_ |    |
+---+--------------------------------------------------------------------------+----+
| D | X.509 certificates (optional self-signed [#f4]_) for |HTTPS|             |    |
+---+--------------------------------------------------------------------------+----+
| O | Generate and populate usernames and passwords |SQL| file [#f5]_ [#f7]_   |    |
+---+--------------------------------------------------------------------------+----+
|   |                                                                          |    |
+---+--------------------------------------------------------------------------+----+

For a bespoke |client| installation
-----------------------------------

.. seealso:: :ref:`bespoke client authentication <installation_extensibility_authnauthz>`

+---+--------------------------------------------------------------------------+----+
|   | Requirement                                                              |    |
+===+==========================================================================+====+
| D | Company logo image [#f7]_                                                |    |
+---+--------------------------------------------------------------------------+----+
| D | Company login and logout mechanism [#f7]_                                |    |
+---+--------------------------------------------------------------------------+----+
| D | Company "contact" information (e.g. :term:`local` contact emails) [#f7]_ |    |
+---+--------------------------------------------------------------------------+----+
| D | X.509 certificates (optional self-signed [#f4]_) for |HTTPS|             |    |
+---+--------------------------------------------------------------------------+----+
| O | |IT| personnel to implement Java/|Spring| code [#f6]_                    |    |
+---+--------------------------------------------------------------------------+----+
| O | Generate and populate usernames and passwords |SQL| file [#f5]_ [#f7]_   |    |
+---+--------------------------------------------------------------------------+----+
|   |                                                                          |    |
+---+--------------------------------------------------------------------------+----+

.. rubric:: Footnotes

.. [#f1] No need for elevated privileges user account on the native filesystem
         (although need to consider auto-restarting component services or use
         of TCP ports 80 and 443!).
.. [#f2] Different sites have data of varying characteristics requiring specific actions.
.. [#f3] Assuming that the data is held in an |SQL| database!
.. [#f4] Use of self-signed certificates generate threatening browser warnings!
.. [#f5] Not "Required" if you're just trying things out!
.. [#f6] If you wish to restrict access |AP-Portal|
.. [#f7] These operations should take place in |client-shared|.