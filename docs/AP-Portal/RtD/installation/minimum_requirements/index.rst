.. include:: ../../global.rst

Minimum requirements
====================

In order to build and host the |AP-Portal| **AND** the required |ApPredict|
application the following are the general minimum requirements.

.. toctree::
   :maxdepth: 2

   hardware
   software
   skills
   privileges

.. toctree::
   :maxdepth: 1

   checklist