.. include:: ../../global.rst

.. _installation_minreqs_skills:

Skills
======

Linux command line
------------------

Some aspects of |AP-Portal| building and installation can be assisted by using
a tool such as as the Eclipse |IDE|, but some aspects of |app-manager|
installation (which handles |ApPredict| invocation) in particular do require a
good knowledge of command line operations.

Building |ApPredict| requires a good understanding of command line operations.

Java and |Spring|
-----------------

|AP-Portal| is a Java-based application which makes extensive use of the 
`Spring Framework <https://spring.io/>`_ and its various
`projects <https://spring.io/projects>`_.

If you wish to create your own |site-business| then it is inevitable that you
will need to be proficient in Java and potentially |Spring|.

|XML|
-----

Some of the |AP-Portal| configuration files are |XML|\ -based and some
knowledge of |XML| document structure and complimentary technologies would
probably be appropriate.

If you wish to create your own |site-business| then it is inevitable that you
will need to be comfortable with |XML|.

Databases and |SQL|
-------------------

Whilst various components require interactions with databases -- requiring an
understanding of their installation and operations -- an in-depth knowledge
of |SQL| is not necessary. Generally the more complex |SQL| used by |AP-Portal|
is derived from using `Hibernate <http://hibernate.org/>`_, although on
occasions it may be appropriate to understand some of the database 
configuration options.

If you wish to create your own |site-business| then it is very likely that if
they are |SQL|-based then a corresponding level of |SQL| understanding would
be required for bespoke database interaction to your compound databases.

Servlet Containers
------------------

|AP-Portal| is dependent on servlet container software, e.g. |Tomcat|, and
therefore familiarity with the configuration and operation of these is required.