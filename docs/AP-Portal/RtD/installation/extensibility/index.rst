.. include:: ../../global.rst

.. _installation_extensibility:

|AP-Portal| extensibility
=========================

Before considering installation options for the :term:`generic portal components`
there are a some things which may require consideration if you intend to have a
:term:`site-specific` installation of the portal. This ambition will require
additional on-site work which is likely to require a lot of organisation,
development and support time to create a bespoke implementation.

Below are the main areas of :term:`site-specific` preparation:

.. toctree::
   :maxdepth: 1

   authn_authz
   registration
   site_business