.. include:: ../../global.rst

.. _installation_extensibility_site_business:

Bespoke |site-business| functionality
=====================================

Brief outline
-------------

If you intend to use the |client| portal to directly access your site's compound
data then all the code to do so will need to be written.

To assist in this process there is the generic |site-business| component
available in the :term:`portal repository` which represents a fictional site
compound database - this component mirrors the configuration required for your
site's data to be passed to the generic |business-manager| processing via the
|business-manager-api|.

Task responsibility
-------------------

The primary objective of this activity involves a good deal of cooperation
between the scientists and the relevant |IT| personnel in the organisation to
determine how to extract the requisite assay data from the site databases.

The |IT| people involved will need to have some experience of Java software
development (including |Spring|), as well as |XML| formats.

Technical Process
-----------------

 #. For the of the installation of the :term:`generic portal components` each
    component's configuration requires the creation and assignment of
    :term:`site-specific` files and values. Much of this process is based on
    deriving actual files from generic :file:`sample.{X}` template files and
    then modifying their content to a site's requirements by following each
    component's specific installation instructions (e.g. as in the
    :ref:`installation <installation_components>` instructions).
 #. For a bespoke |site-business| component there will be additional steps
    required (illustrated below) which will ultimately make use of a |Maven| 
    `WarPath <http://static.appfuse.org/maven-warpath-plugin/>`_ plugin to
    overlay :term:`site-specific` functionality over compiled and |Maven|\ -installed
    |business-manager| code.

The structure below and explanations thereafter (taken from the sample
|site-business| component used for demonstration purposes), can be used to
explain specifications and operations of generally required bespoke
|site-business| functionality.

::

 site-business/src/main/java/uk/ac/ox/cs/epsrc/site_business/business/selection/maxresponse/CSDemoMaxRespStrategy.java                 --+
                |    |                                |          |                    +----/CSMaxRespDataSelector.java                   |
                |    |                                |          +---/SiteDataLoader.java                                                |
                |    |                                |-----/dao/SiteDAOImpl.java                                                        |
                |    |                                |-----/request/validator/SiteSimulationsRequestValidator.java                      |
                |    |                                |-----/service/ExperimentalDataServiceImpl.java                                    |
                |    |                                |-----/SiteIdenifiers.java                                                         |-- 1
                |    |                                +-----/value/object/datarecord/doseresponse/CSDoseResponseDataRecordVO.java        |
                |    |                                                         |----/individual/CSIndividualDataRecordVO.java            |
                |    |                                                         |          |----/CSManualPatchCaV12DataRecordVO.java      |
                |    |                                                         |          |----/CSManualPatchHERGDataRecordVO.java       |
                |    |                                                         |          +----/CSManualPatchNaV15DataRecordVO.java      |
                |    |                                                         +----/summary/CSSummaryDataRecordVO.java                --+
                |    |-/resources/log4j.xml
                |    |      +----/META-INF/properties/.gitignore
                |    |                |         +----/README
                |    |                |---/spring/ctx/appCtx.business.site.xml
                |    |                |            |-/config/appCtx.config.actionPotential.site.xml                                    --+
                |    |                |            |    |---/appCtx.config.assays.site.xml                                               |
                |    |                |            |    |---/appCtx.config.cellModel.site.xml                                            |
                |    |                |            |    |---/appCtx.config.changeCheckers.site.xml                                       |-- 2
                |    |                |            |    |---/appCtx.config.doseResponseFitting.site.xml                                  |
                |    |                |            |    |---/appCtx.config.ionChannels.site.xml                                          |
                |    |                |            |    |---/appCtx.config.pc50Evaluators.site.xml                                       |
                |    |                |            |    +---/appCtx.config.site.xml                                                    --+
                |    |                |            |-/data/site/appCtx.database.site.xml                                               ----- 3
                |    |                |            +-/integration/simulation/processing/experimental/appCtx.proc.site.experimental.xml --+
                |    |                |                                           |----/qsar/appCtx.proc.site.qsar.xml                   |-- 4
                |    |                |                                           +----/screening/appCtx.proc.site.screening.xml       --+
                |    |                +---/sql/site/site.embedded.sql                                                                  --+__ 5
                |    |                           +-/site.mysql.sql                                                                     --+
                |    +-/webapp/WEB-INF/spring/spring-site-config.xml
                |-/properties/database/site/.gitignore                                                                                 --+
                |       |                |-/README                                                                                       |-- 6
                |       |                +-/sample.site.database.properties                                                            --+
                |       |----/.gitignore                                                                                               --+__ 7
                |       +----/sample.env.properties                                                                                    --+
                +-/test/ ....                                                                                                          ----- 8

.. _installation_extensibility_site_business_java:

1 - Writing the Java classes
----------------------------

Java classes used to :

 #. Define :term:`site-specific` data record structures derived from
    |business-manager-api|  (e.g. extending 
    `AbstractIndividualDataRecordVO <https://bitbucket.org/gef_work/ap_predict_online/src/master/business-manager-api/src/main/java/uk/ac/ox/cs/nc3rs/business_manager/api/value/object/datarecord/individual/AbstractIndividualDataRecordVO.java>`_
    as in the case of 
    `CSManualPatchHERGDataRecordVO <https://bitbucket.org/gef_work/ap_predict_online/src/master/site-business/src/main/java/uk/ac/ox/cs/epsrc/site_business/value/object/datarecord/individual/CSManualPatchHERGDataRecordVO.java>`_).
 #. Query the site database to load site data into those data records. |br|
    For this the relevant |SQL| will most likely need defining to extract all
    the relevant information from the site databases.
 #. Return the site data to the generic |business-manager| processing.
 #. :term:`pIC50 evaluation`.

.. _installation_extensibility_site_business_ctxs:

2 - Define CellMLs, Assays, Ion Channels, etc.
----------------------------------------------

.. note:: A site's configuration files are ultimately derived from the |XML|
   files in the |business-manager|
   `sample configs <https://bitbucket.org/gef_work/ap_predict_online/src/master/business-manager/src/test/resources/META-INF/spring/ctx/config/>`_
   directory. Examples of how these templates are copied and modified to fit to
   a site's unique specifications is demonstrated in the |site-business|
   component `configs <https://bitbucket.org/gef_work/ap_predict_online/src/master/site-business/src/main/resources/META-INF/spring/ctx/config/>`_.

|XML| configuration files to define a site's unique setup.

 #. :file:`appCtx.config.actionPotential.site.xml` : |ApPredict| invocation settings.

    a. Default frequencies and compound concentrations in situations where there
       is no :term:`experimental data` available (|ApPredict|\ 's ``--pacing-freq``
       and ``--plasma-concs`` arg. values).
    b. Default credible interval percentiles (|ApPredict|\ 's
       ``--credible-intervals`` arg. value). [#f2]_ |br|
       The extension of functionality of this argument appeared around end-May
       2018 to improve on the previous use of an unmodifiable singular default
       credible interval percentile of 95%. |br|
    c. Default maximum pacing time (|ApPredict|\ 's ``--pacing-max-time`` arg.
       value).
    d. Default plasma concentration count (|ApPredict|\ 's ``--plasma-conc-count``
       arg. value).
    e. Default plasma concentration log scale (|ApPredict|\ 's
       ``--plasma-conc-logscale`` arg. value).

 #. :file:`appCtx.config.assays.site.xml` : Assay details.

    a. Assay names.
    b. :term:`Assay level`\ s. [#f1]_ |br|
       Assays in |AP-Portal| are considered to be hierarchical and a level
       value is assigned to each assay. Historically the hierarchy has
       reflected assay accuract, with the least accurate assay assigned a level
       value  of ``0`` (zero), and increasingly more accurate assays assigned
       incremental level values.
    c. :term:`Assay group`\ s. [#f1]_ |br|
       When different assays have similar characteristics they can be grouped,
       and as with **assay levels**, the grouping is hierarchical. Historically
       the assay groups levels assigned have mirrored the assay levels assigned,
       with the least accurate groups assigned a group level of ``0`` (zero)
       and increasingly more accurate assay groups assigned incremental group
       level values. 
    d. Assay :term:`variability`. [#f2]_

 #. :file:`appCtx.config.cellModel.site.xml` : CellML models. |br|

    The collection of CellML models contained in this configuration file
    **must** correspond to the CellML models which |ApPredict| is expecting. |br|
    If you are uncertain which CellML models |ApPredict| has been built to
    work with, then invoke |ApPredict| at the command line with no arguments
    supplied, which should generate output containing something like the
    following :

    :: 

       ***********************************************************************************************
       * ApPredict::Please provide some of these inputs:
       *
       * EITHER --model
       *   options: 1 = Shannon, 2 = TenTusscher (06), 3 = Mahajan,
       *            4 = Hund-Rudy, 5 = Grandi, 6 = O'Hara-Rudy 2011 (endo),
       *            7 = Paci (ventricular), 8 = O'Hara-Rudy CiPA v1 2017 (endo)
       *            9 = Faber-Rudy.
       * OR --cellml <file>

    It's unlikely that the CellML models data would need to change except in the
    specification of the ``defaultModel`` - i.e. the model which should appear
    selected by default in the |UI|.

 #. :file:`appCtx.config.changeCheckers.site.xml` - Change checkers.

    :term:`Change checker`\ s should generally not require modification.

 #. :file:`appCtx.config.doseResponseFitting.site.xml` - :term:`Dose-Response data` fitting
    preferences.

    Only relevant if :term:`dose-response data` fitting.

 #. :file:`appCtx.config.ionChannels.site.xml` - Active ion channels.

    Collection of the actively tested ion channels.

 #. :file:`appCtx.config.pc50Evaluators.site.xml` - pIC50 evaluators.

    :term:`pIC50 evaluation` involves activating/deactivating and arranging
    evaluators according to site requirements. |br|
    It is likely that the scientists will need to be consulted on which
    evaluators are to be used and their hierarchical order (``0`` (zero) being
    the first evaluator to seek a pIC50 value from). From experience it is
    likely that additional :term:`site-specific` evaluators which also need to
    be implemented.

 #. :file:`appCtx.config.site.xml` - Various |client| options settings.

    a. ``defaultForceReRun`` - **use with care!**
    b. :term:`Assay value inheritance` defaults.
    c. Information display levels.
    d. ``defaultQuietPeriod`` - Time (in minutes) following a simulation's run
       or re-run check in which another re-run check will not be undertaken. |br|
       This is to avoid multiple concurrent requests to view a compound
       resulting in simulation re-runs.

3 - Connecting to the site's database
-------------------------------------

Connectivity to the site's database.

.. _installation_extensibility_site_business_processing:

4 - Define Screening, |QSAR|, Experimental processing
-----------------------------------------------------

Different sources of data to process, e.g. screening, |QSAR|, experimental data.

5, 6 - Ignore
-------------

(Sample site database structure)

7 - Define programming environment properties
---------------------------------------------

Sample environment properties.

8 - Writing the Java unit testing classes.
------------------------------------------

Java test classes.

.. rubric:: Footnotes

.. [#f1] :term:`assay level`\ s and :term:`assay group` levels are used in
         :term:`assay value inheritance`.

.. [#f2] See also :ref:`Variability Configuration <configuration_variability>`.
