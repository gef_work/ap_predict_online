.. include:: ../../global.rst

.. _installation_extensibility_registration:

User registration
=================

Brief outline
-------------

If you are keen to allow users to register themselves to use |AP-Portal| in
either of the |client| or |client-direct| interfaces then there is the option to
interact with :term:`local` |SMTP| facilities and use Google's `reCAPTCHA
<https://www.google.com/recaptcha/intro/>`_ facilities to improve the likelihood
that the registrations are from genuine potential users.

.. note:: The option to have a registration mechanism only applies when the
   |clients-prepopulated| :ref:`user authentication mechanism
   <installation_extensibility_authnauthz>` is active. If a |clients-bespoke|
   mechanism is used then there is no option to display a registration form in
   either of the |client| or |client-direct| interfaces.

.. note:: "Registration" in |AP-Portal| represents a portal administrator
   being notified, via email, of a user's desire to use the system. It does not 
   provide features such as dynamic user database population, user password
   selection or "remind me" features common on most web sites.

Task responsibility
-------------------

The primary objective of this activity most likely involves the |IT| personnel
determining how to configure interaction with the :term:`local` |SMTP| server
and, if using `reCAPTCHA <https://www.google.com/recaptcha/intro/>`_, how to 
request the necessary keys.

Additional detail
-----------------

Scenarios related to registration :

 * :term:`Local` |SMTP| service configured.

   Visitors to the |client| and/or |client-direct| interfaces who wish to
   register can complete a registration form (which prompts for a contact
   email address) which when submitted uses the configured |SMTP| service to
   send an email notification to the portal administrator who can then send the
   registering person one of the predefined usernames and passwords. |br| 
   If the predefined list becomes exhausted then more usernames and passwords
   must be manually created and added to :file:`users.sql` and the component
   restarted.

   The registration form submission may be controlled by the use of the
   `reCAPTCHA <https://www.google.com/recaptcha/intro/>`_ facility.

 * :term:`Local` |SMTP| service not configured.

   Visitors to the |client| and/or |client-direct| interface who wish to
   register are directed to the "contact" page where it is expected there will
   be the relevant contact information.

 * `reCAPTCHA <https://www.google.com/recaptcha/intro/>`_
 
   A public and private key needs to be obtained from Google.

   To use this feature there needs to be internet access as you need to call a 
   Google internet service. (I'm not sure how flexible it is with intranet web
   addresses!).