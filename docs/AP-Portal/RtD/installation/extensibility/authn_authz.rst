.. include:: ../../global.rst

.. _installation_extensibility_authnauthz:

User authentication and authorisation
=====================================

Brief outline
-------------

How users are authenticated by the |client| and |client-direct| components
takes one of two techniques provided by their parent |client-shared| component.
At runtime the technique used is determined by the use of
:ref:`Spring profiles <tools_spring_profiles>`.

 #. |clients-prepopulated|

    The `off-the-shelf` portal authentication mechanism which makes user of
    manually populated user databases.

    This mechanism is relatively straightforward, in that the :file:`users.sql`
    files (see :ref:`client <installation_components_client_users>` and
    :ref:`client-direct <installation_components_client_direct_users>`)
    containing usernames, passwords and `role`\ s, are created and populated
    during portal installation and manually updated thereafter.

    Only when using this option do potential portal users have the ability to
    :ref:`register <installation_extensibility_registration>` to use the portal
    using the portal interface.

 #. |clients-bespoke|
 
    The provision of a bespoke authentication mechanism (e.g. |NTLM|, `Active 
    Directory`) by the site itself.

    Whilst the authentication mechanism may already be available on-site, there
    will need to be a bespoke `filter` (see `Request Filtering`_)
    created which interfaces with this mechanism.
 
Task responsibility (for |clients-bespoke|)
-------------------------------------------

The primary objective of this activity most likely involves the |IT| personnel
determining how to control access to the |client| and |client-direct| user 
interfaces by intercepting requests (see `Request Filtering`_) to determine user
identity and return a browser redirect to site authentication mechanisms
before the user is granted access.

The |IT| people involved will need to have some experience of Java software
development (including |Spring|), as well as |XML| formats.

Additional detail
-----------------

Request Filtering
^^^^^^^^^^^^^^^^^

The |client| and |client-direct| components both make use of |Spring|
Security's ``springSecurityFilterChain``
`FilterChainProxy <http://docs.spring.io/autorepo/docs/spring-security/3.2.5.RELEASE/apidocs/org/springframework/security/web/FilterChainProxy.html>`_
by virtue of the :file:`src/main/webapp/WEB-INF/web.xml` filter naming.
This mechanism intercepts all incoming requests and passes control to a variety
of filters which can take whatever action is appropriate for the circumstances,
e.g. accept (and pass the request to the next filter in the chain), or reject
(and perhaps show a default page or redirect to an authentication mechanism).

Amongst these filters there are some standard checks such as for security (e.g.
|CSRF|) and valid sessions, but there can also be :term:`site-specific` checks which
can, for example, check for the presence of site cookies or query :term:`local`
authentication/authorisation mechanisms, e.g. |NTLM|. Equally, some portal pages
could be deemed to be available for perusal by any visitor, e.g. such as in
`appCtx.noSecurity.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/src/main/resources/WEB-INF/spring/authn/appCtx.noSecurity.xml>`_

If a request arrives at a portal from an unauthenticated user hoping to visit
restricted access areas then a :term:`site-specific` filter (e.g.
`sample.appCtx.sitePreauthFilter.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/src/main/resources/WEB-INF/spring/authn/sample.appCtx.sitePreauthFilter.xml>`_)
injected into the ``springSecurityFilterChain`` (e.g. ``custom-filter`` in 
`sample.appCtx.bespoke.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/src/main/resources/WEB-INF/spring/authn/sample.appCtx.bespoke.xml>`_)
will reject the request and most likely forward the user to the site's
authentication mechanism.

.. _installation_extensibility_authnauthz_mechanisms:

Authorisation mechanisms
^^^^^^^^^^^^^^^^^^^^^^^^

Access to certain areas of the portal are controlled by the |role|\ s which were
assigned to the user at authentication time.

As a general rule all users accessing the |client| or |client-direct| portals must, unless they are
accessing unrestricted areas, be in the ``ROLE_USER`` group (e.g. as determined by 
`sample.appCtx.bespoke.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/src/main/resources/WEB-INF/spring/authn/sample.appCtx.bespoke.xml>`_ 
or `appCtx.prepopulated.xml <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/src/main/resources/WEB-INF/spring/authn/appCtx.prepopulated.xml>`_).

Currently the only supported roles are defined in 
`Role.java <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-shared/src/main/java/uk/ac/ox/cs/compbio/client_shared/value/security/Role.java>`_

  * ``ROLE_USER`` |br| 
    Generally every portal visitor must be authenticated and assigned this |role|.
  * ``ROLE_POWER_USER`` |br| 
    Generally assigned to users with access to certain "elevated privileges" functionality, such
    as entering ion channel spread values, or uploading CellML files.
  * ``ROLE_ADMIN`` |br| 
    Assigned to the person(s) responsible for administrative portal duties. 

.. seealso:: :ref:`Portal feature access <installation_components_client_shared_configfeatures>`