.. include:: ../../global.rst

.. _security_communication:

Communication security
======================

.. note:: Communication security is only really essential for the communication
   to |site-business| as that component should only process requests containing
   compound identifiers arriving from the |client| component (or any other
   suitably secured "client", e.g. a Perl script) which is *expected* to have
   an appropriate authentication mechanism configured. See 
   :ref:`here <installation_extensibility_authnauthz>` for further
   information on generic client authentication options. |br|
   For |app-manager| and |dose-response-manager| component |WS|'s, these could
   be kept as "open" resources for any potential "client" application, e.g. a
   command-line script, as the |WS| message content will never contain any data
   which identifies a compound.

|HTTPS|
-------

All inter-component |WS| communication uses `WSS`_ and so all such communication
should be encrypted |HTTP|, e.g. using X.509 certificates (self-signed or 
otherwise).

An example configuration of such a setup is available for |Tomcat|.

|WSS|
-----

All inter-component |WS| communication uses |WSS| (`description here <https://en.wikipedia.org/wiki/WS-Security>`_)
of the ``User ID/Password`` variety. For this to be effective the passwords
used should be :

 #. :ref:`Stored in an encrypted format <tools_propertyfileencrypt>`
 #. Communicated in an encrypted format, i.e. `HTTPS`_.
 #. :ref:`In access-controlled files <security_filesystem>`

|WS| |IP| access control
------------------------

Access to any component's |WS|, e.g. |app-manager|, |site-business| and
|dose-response-manager|, can be restricted to specified |IP|
origin addresses by, for example, the inclusion of a `Remote Address Filter <https://tomcat.apache.org/tomcat-7.0-doc/config/filter.html#Remote_Address_Filter>`_
in |Tomcat|'s :file:`web.xml` configuration file.

WWW/Internet communication
--------------------------

Once installed, |AP-Portal|\ 's :term:`generic portal components` have no need
to access the internet -- the portal could operate unhindered within
corporate firewalls. Having said that, |ApPredict| on the other hand will
attempt to retrieve :term:`variability lookup tables` if they cannot be found
locally (see :ref:`variability configuration <configuration_variability>`).

Having said that, there would be nothing preventing :term:`site-specific`
component implementations or extensions, e.g. for |client| or |site-business|,
from communicating through corporate firewalls if requirements necessitate
doing so.