.. include:: ../../global.rst

.. _security_gui:

|GUI| security
==============

The |client| and |client-direct| component both undertake user input and output
validation and "sanitisation".