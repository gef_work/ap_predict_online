.. include:: ../../global.rst

.. _security_filesystem:

Filesystem security
===================

Filesystem access control
-------------------------

Security is improved by ensuring that files which contain sensitive information
or data are appropriately access-controlled at a filesystem level, for example,
use of the ``chmod 600 <file_name>`` command.

.. warning:: It would be easier to create a specific account for |AP-Portal|
   and restrict access to that account's directory solely to that account
   (rather than, for example, also allowing group or unrestricted access).

``JASYPT`` file content encryption
----------------------------------

See :ref:`Property file {de,en}cryption <tools_propertyfileencrypt>`.

This risk is avoided if there is suitable `Filesystem access control`_.

Log files
---------

With log levels of ``debug`` or ``trace`` the log files may contain a large
amount of data, some of which may be sensitive so it is advised that under 
normal operation a log level of at least ``info`` is used.

This risk is avoided if there is suitable `Filesystem access control`_.