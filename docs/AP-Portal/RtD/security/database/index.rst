.. include:: ../../global.rst

.. _security_database:

Database security
=================

Access to whichever database which |AP-Portal| is configured to use should be
appropriately controlled to ensure that the content can not be accessed. This
is particularly the case for the |site-business| component which stores compound
identifiers alongside simulation input values and results, but equally for
|client-direct| which retains the portal usernames and passwords.

This risk is avoided if there is suitable :ref:`Filesystem access control <security_filesystem>`.