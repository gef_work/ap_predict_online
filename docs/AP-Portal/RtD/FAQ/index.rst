.. include:: ../global.rst

|FAQ|\ s
========

How do I control user access to the portal?
-------------------------------------------

For the |client| and |client-direct| components access is by default
restricted to most areas, only information such as contact details and "about"
information are unrestricted.

Both components share the ability to use a pre-populated user database (by means
of an |SQL| script, e.g.
`sample.users.sql <https://bitbucket.org/gef_work/ap_predict_online/src/master/client-direct/src/main/resources/META-INF/data/spring-security/local/sample.users.sql>`_
as a source of authentication (|clients-prepopulated|), however if you wish to
use a :term:`site-specific` authentication mechanism (|clients-bespoke|) then a
corresponding :ref:`bespoke client <installation_extensibility_authnauthz>`
extension needs to be developed to provide or interact with the desired
authentication mechanisms.

For other components, e.g. |app-manager|, |dose-response-manager| and
|site-business|, access control is determined by a number of factors, such as
those listed in :ref:`communication security <security_communication>`.

How do I adjust the .....?
--------------------------

.. seealso:: The various :ref:`maintenance` sections for common requests for
   altering configurable portal aspects.