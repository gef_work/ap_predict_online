.. include:: global.rst

.. figure:: _static/images/banner.png
   :align: left
   :alt: Action Potential Prediction

Welcome to AP-Portal's Technical documentation
==============================================

:Project Home: https://bitbucket.org/gef_work/ap_predict_online
:Documentation: http://apportal.readthedocs.io/
:Created: |today|
:Version: |release|

IMPORTANT
---------

As of early 2019 this application, originally created at the `University of Oxford <https://www.ox.ac.uk/>`_\ 's
`Department of Computer Science <https://www.cs.ox.ac.uk/>`_, is no longer being actively developed.

Subsequent activity has been transferred to the development of a newer version, |AP-Nimbus|
at https://github.com/CardiacModelling/ap-nimbus by the `University 
of Nottingham <https://www.nottingham.ac.uk/>`_\ 's `School of Mathematical Sciences 
<https://www.nottingham.ac.uk/mathematics/research/>`_ .

Preamble
--------

 * User/Scientific documentation is accessible via https://chaste.cs.ox.ac.uk/ActionPotential/about.
 * In many places the documentation may appear generalised as some aspects of
   the |AP-Portal|\ 's operational dependencies, e.g. databases, Java servlet
   containers, etc., are *predominantly* unconstrained. |br|
   Anyone intending to use the portal should be familiar with installing and
   operating the dependencies independently.
 * There is no documentation herein on how to install |ApPredict|, which is the
   portal's underlying cardiac simulator. For such instructions please visit
   https://chaste.cs.ox.ac.uk/trac/wiki/ApPredict. |br|
   Alternatively, there are some sample installations beneath the |AP-Portal|
   repository `install <https://bitbucket.org/gef_work/ap_predict_online/src/master/docs/ApPredict/install/>`_
   directory.
 * Full source code, Javadocs, and limited additional documentation is
   available at https://bitbucket.org/gef_work/ap_predict_online. |br|
   It is anticipated that this documentation will be the principle source of 
   technical information, which can be accessed at http://apportal.readthedocs.io/
   and via that resource, exported to a number of formats.

Installation
------------

.. toctree::
   :maxdepth: 2

   installation/index

Security
--------

.. toctree::
   :maxdepth: 2

   security/index

Personal Data
-------------

.. toctree::
   :maxdepth: 2

   personal_data/index

Configuration
-------------

.. toctree::
   :maxdepth: 2

   configuration/index

Maintenance
-----------

.. toctree::
   :maxdepth: 2

   maintenance/index

General
-------

.. toctree::
   :maxdepth: 2

   general/index

Tools
-----

.. toctree::
   :maxdepth: 2

   tools/index

|FAQ|
-----

.. toctree::
   :maxdepth: 2

   FAQ/index

TroubleShooting
---------------

.. toctree::
   :maxdepth: 2

   trouble_shooting/index

Glossary
--------

.. toctree::
   :maxdepth: 2

   glossary/index
