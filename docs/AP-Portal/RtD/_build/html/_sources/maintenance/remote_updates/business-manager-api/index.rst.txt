.. include:: ../../../global.rst

|business-manager-api| updating
===============================

First take a look at the :ref:`general update instructions <remote_updates_general>`.

Generally any modifications to files in this component will result in possible changes necessary 
only in dependent components, i.e. |business-manager| or |site-business|.

If there has been a change in |business-manager-api| then downstream components (e.g. 
|business-manager| and/or |site-business|) will also need updating.