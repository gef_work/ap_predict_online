.. include:: ../global.rst

.. _tools_oracle:

Oracle
======

Oracle's website is `here <https://www.oracle.com/>`_

To emulate Oracle use it is possible to register for and download Oracle XE
(`Express Edition`) which can be package-installed, for example, into a |VM|
and networked to appear as a local database. [#f1]_

If you're downloading Oracle stuff, you'll probably also need to download the
appropriate |JDBC| driver and more than likely the `SQL Developer`
application to view table structure and content.

.. rubric:: Footnotes

.. [#f1] I used `Oracle Database Express Edition 11g Release 2` installed in a
         `VirtualBox` Fedora 20 for testing purposes - although I did need to
         `increase swap space <https://ask.fedoraproject.org/en/question/33450/increase-swap-space-in-fedora-19/>`_
         and tried ``alter system set processes=200 scope=spfile`` when in the
         ``sqlplus system`` |CLI|.