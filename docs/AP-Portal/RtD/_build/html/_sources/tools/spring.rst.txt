.. include:: ../global.rst

.. _tools_spring:

Spring
======

.. _tools_spring_profiles:

Spring profiles
---------------

`Spring profiles <https://docs.spring.io/spring/docs/current/spring-framework-reference/html/beans.html#beans-definition-profiles-java>`_
are used as an easy way to have different application configuration options
active in different environments without a need to modify the codebase or
recompile, instead the configuration to be used is determined by the passing of
the ``spring.profiles.active`` Java system property to the application (e.g.
by use of the ``-Dspring.profiles.active=<profile1>,<profile2>,<etc>``
arg when starting the portal from the command-line). |br|
Whilst this means that all anticipated configurations are packaged it does 
permit a degree of flexibility so that, for example, a single package can be
used in both the build and deployment environment.