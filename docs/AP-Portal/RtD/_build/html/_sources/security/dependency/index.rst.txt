.. include:: ../../global.rst

.. _security_dependency:

|AP-Portal| dependency security
===============================

The portal, like any software, does have a number of library dependencies which
may contain security vulnerabilities which are presently unknown or may be
disclosed in the future.

The portal developers will attempt to keep up-to-date with emerging potential
security vulnerabilities, e.g. via https://nvd.nist.gov/download/nvd-rss-analyzed.xml,
but even if a vulnerability appears it may not be possible to update the portal
software in a reasonable time frame.

Dependencies are listed in the various component Maven ``site`` build reports no
longer available except my building the components.

Useful tools
------------

If you wish to determine the most up-to-date security statuses for any of the
dependency libraries of components then there is the following which may be
useful :

``mvn org.sonatype.ossindex.maven:ossindex-maven-plugin:audit -f pom.xml``