.. include:: ../global.rst

Personal Data
=============

No aspect of |AP-Portal| operation involves the storage or use of |PII| nor
any form of personal data.