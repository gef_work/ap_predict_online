.. include:: ../global.rst

Glossary
========

.. glossary::

   assay group
     Refers to the grouping of different assays (of differing :term:`assay level`\ s)
     which demonstrate similar characteristics (historically based on their
     accuracy) into a hierarchically organised representative group [#f1]_.

   assay level
     Refers to the hierarchical organisation of individual assays (historically
     based on their accuracy) and the assignment of a corresponding assay level
     value [#f1]_.

   assay value inheritance
     Refers to |AP-Portal|\ 's mechanism for enabling |IC50| values recorded by
     one assay to be inherited by another assay (based on their hierarchical
     ordering) as a substitute for missing measurements. For example, an
     IonWorks Barracuda® |IC50| value for NaV1.5 being substituted into
     PatchXpress® assay data if no PatchXpress® NaV1.5 measurement existed [#f1]_.

   back-end components
     Refers generally to the non- public-interfacing components (for example
     |business-manager|, |app-manager|, |dose-response-manager|, etc).

   change checker
     Device which detects whether an existing simulation for a compound
     needs to be re-run for whatever reason, for example, if it detects
     different input |IC50| values between the previous run request and the
     current run request (due to new assay data availability) [#f1]_.

   dose-response data
     A generic term indicating the dose-response data (e.g. the individual
     dose-response points) associated with a :term:`individual data` record.

   emulator
     See :term:`variability lookup tables`.

   experimental data
     Refers to data derived from animal studies such as ventricular wedge
     assays.

   front-end components
     Refers generally to the public-interfacing components, i.e. |client| and
     |client-direct|.

   generic portal components
     Refers to the open source, publicly available portal components, e.g. 
     |client|, |business-manager-api|, etc, which have generic (i.e. non-
     :term:`site-specific`) processing.

   individual data
     A generic term indicating the result of an assay experiment. There may be
     a number of :term:`individual data` records for a compound, and for some
     systems these :term:`individual data` records are aggregated into a 
     :term:`summary data` record. Alternative names which may be encountered
     are "full curve" or "percent inhibition" data. |br|
     An :term:`individual data` record may also link to the detailed
     :term:`dose-response data` from which it is derived.

   input data gathering
     Refers to the process of directly calling |site-business| via a |WS|
     invocation in order to determine the pIC50 input data for a compound, i.e.
     it won't run a simulation [#f1]_.

   internationalised
     |AP-Portal|\ 's |i18n| capabilities.

   local
     Very generally refers to objects on the portal's host filesystem, the
     portal itself, or perhaps the wider `on-site` area.

   pIC50 evaluation 
    pIC50 evaluation is the technique by which |AP-Portal| determines
    the pIC50 value (or values) to use (**or reject, perhaps due to quality
    control**) in a simulation for each available ion channel and assay
    combination. |br|
    There will be a number of pIC50 evaluators available, each representing a
    method of obtaining a pIC50 from the available data (e.g. perhaps both
    |IC50| and |IC20| values are available), organised hierarchically, with
    the expectation that the pIC50 value to use (or reject) will be derived
    from the first evaluator to provide (or reject) a pIC50. [#f1]_.

   portal repository
     Refers to the portal's generic, version-controlled source code repository
     at https://bitbucket.org/gef_work/ap_predict_online.

   screening data
     Refers to data derived from ion channel screening device assays such as
     |HTS| machines, e.g. PatchXpress®.

   site-specific
     Refers to bespoke configurations, settings or source code which is specific
     to the site which is hosting the portal, e.g. a site's ion channel data
     processing code, a site's user authentication code, or a site's ion
     channels and assay configurations.

   summary data
     A generic term indicating that :term:`individual data` has been aggregated
     into a summary record somehow.

   variability
     "*variability*" may be used interchangeably with "*spread*" and 
     "*uncertainty*"! |br|
     This term refers to ion channel variability, as explained in this
     `research article <https://doi.org/10.1016/j.vascn.2013.04.007>`_.

   variability lookup tables
     In order to present :term:`variability` results |ApPredict| requires the
     availability of large *lookup table* files. |br|
     Since early 2018 these tables have been also been referred to as 
     "Emulators".

.. rubric:: Footnotes

.. [#f1] |client| system only feature.