.. include:: ../global.rst

.. _configuration_variability:

:term:`Variability` Configuration
=================================

.. seealso:: Related research paper:
             `Journal of Pharmacological and Toxicological Methods <https://doi.org/10.1016/j.vascn.2013.04.007>`_

In order for variability data to appear in simulation results in the |UI|\ s of
both the |client| and |client-direct| components, |ApPredict| needs to have made
available to it :term:`variability lookup tables` - a manifest of which is
available from `Gary Mirams' repository 
<http://www.cs.ox.ac.uk/people/gary.mirams/files/appredict_lookup_table_manifest.txt>`_.

Currently (as of Jan. 2018), whenever |ApPredict| is run with the
``--credible-intervals`` (which itself had its functionality extended May 2018
to allow the specification of credible interval percentile values) and 
``--model <model id>`` args supplied, it will look in |ApPredict|\ 's current
working directory [#f1]_ for an unpacked version [#f2]_ of the relevant lookup
table. If it is not found |ApPredict| will attempt to download the packed file
from the aforementioned repository and if such a file exists it will be
downloaded, unpacked locally, and loaded into memory [#f3]_.

.. rubric:: Footnotes

.. [#f1] This has been achieved by modifying |app-manager|\ 's
   :file:`prepare.sh` file to symlink to manually downloaded unpacked
   :term:`variability lookup tables` in |ApPredict|\ 's current working
   directory. |br|
   Ideally it is the ``*_BINARY.arch`` files that should be made available
   (as these are created dynamically from the non-``BINARY`` ``.arch`` files
   when |ApPredict| loads them), although there may be compatibility issues
   using legacy versions derived from historical compilations.

.. [#f2] Conventionally the :term:`variability lookup tables` are released in
   ``.tgz`` format due to their large (e.g. 700Mb+) size.

.. [#f3] This has the potential to drain the hardware's available |RAM| which
   may impact server performance.