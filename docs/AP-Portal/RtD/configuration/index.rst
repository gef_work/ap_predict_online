.. include:: ../global.rst

.. _configuration:

Configuration
=============

.. note:: If you're reading this because a part of the portal is not working
   then please also consider visiting the :ref:`troubleshooting <troubleshooting>`
   pages.

.. note:: Generally changes to |ApPredict| are reflected in |AP-Portal|, which
   is good news for anyone trying to keep up-to-date with the latest
   developments, however there is currently no mechanism of version-checking
   between, for example, the |app-manager| component and the version of
   |ApPredict| which it invokes. It is possible that the |ApPredict| could 
   undergo many changes without the need to update |AP-Portal|, and vice-versa. |br|
   Similarly, there are no checks in place prior to deployment which verify
   that communication between the |AP-Portal|\ 's :term:`generic portal components`
   is compatible (although generally, any incompatibilities that existed would
   soon appear at runtime as system errors!).

Initial component configuration is handled in the component's
:ref:`installation <installation_components>` section [#f1]_.

Specific Functionality
----------------------

.. toctree::
   :maxdepth: 2

   variability

.. rubric:: Footnotes

.. [#f1] |AP-Portal| development up to now has predominantly focused on the
         ability to quickly adapt to frequently changing requirements to
         develop this application, with developers being actively involved in
         all aspects of |AP-Portal| installation, support and maintenance. |br|
         Whilst this approach has been effective in terms of being able to
         quickly deliver and advance a proof-of-concept application, many
         aspects of configuration take place at a systems level at install or
         upgrade time, e.g. |XML| file content is modified before components
         are (re)built and (re)deployed. |br|
         Once configured the |AP-Portal| provides the users with an environment
         which meets their requirements (although there is a growing list of
         feature requests), however a number of what would be considered
         infrequent "business" configuration changes, e.g. a different
         CellML model is to be assigned as the default, or a different units
         assigned as the default, would require the modification at a filesystem
         level and a component restart. |br|
         In due course the majority of anticipatable configuration changes will
         be available via the |UI|.
