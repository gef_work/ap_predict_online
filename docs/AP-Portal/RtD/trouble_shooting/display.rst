.. include:: ../global.rst

Display problems
================

|client| display failing
------------------------

If the display is failing following an update to |AP-Portal| then it could be
symptomatic of legacy files being found in the browser's cache, thus preventing
the latest operations being executed properly. See the following section on
`Clearing a browser cache`_

Clearing a browser cache
------------------------

If the portal has been updated but there appears to be no change to what the
user is seeing in their browser then it's possible that they are seeing the
content from the browser's cache. In this case, then below is some general
advice for forcing content to be removed from the browser cache.

|IE| (based on ver. 11)

``Tools`` -> ``Internet Options`` -> ``Browsing History`` -> 
``Delete...`` -> make sure `Temporary Internet files and website files`
and `Cookies and website data` are checked -> ``Delete``.

**Chrome** (based on ver. 56.0.2924.87)

``Ctrl+Shift+Del`` (or click on the Chrome button
which looks like |ccgc| then ``More tools`` -> ``Clear browsing data...``) ->
make sure `Cached images and files` is checked -> ``Clear browsing data``.

**Firefox** (based on ver. 38.0.5)

``Edit`` -> ``Preferences`` -> ``Advanced`` -> For
`Cached Web Content` click the ``Clear Now`` button.

.. |ccgc| image:: ../_static/images/ccgc.png
           :alt: Customize and control Google Chrome
