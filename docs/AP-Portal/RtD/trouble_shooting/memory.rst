.. include:: ../global.rst

.. _troubleshooting_memory:

Server memory problems
======================

Low `free` |RAM|
----------------

If you have a device which is low on |RAM| or doesn't manage buffering/caching
well you may periodically need to flush it, e.g. ``sync; echo 1 > /proc/sys/vm/drop_caches``
(although please |RTFM| as to do so may cause issues in certain circumstances). 
