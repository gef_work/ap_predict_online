.. include:: ../global.rst

.. _troubleshooting_logfiles:

Log file analysis problems
==========================

These are a great source of information if the log level allows plenty of 
debugging information to appear. Generally the log level and location of the
portal components is determined by the properties files (e.g. 
:file:`src/properties/env.properties`, :file:`src/properties/filter.properties`)
when building components, or in the :file:`<component>/webapps/<component_name>/WEB-INF/classes/log4j.xml`
files when starting/running the component.

Apart from the component logging there is also the servlet container logging,
e.g. |Tomcat|'s ``catalina.out``, which may assist in problem solving.

``NullPointerException``\s in |site-business| logs
--------------------------------------------------

If :file:`AppManagerServiceImpl.java`\'s ``persistResults()`` is generating
``NullPointerException``\s, this can happen because of 
`issue #23 <https://bitbucket.org/gef_work/ap_predict_online/issues/23/app-manager-inappropriate>`_.
