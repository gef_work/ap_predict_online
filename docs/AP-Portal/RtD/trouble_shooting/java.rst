.. include:: ../global.rst

.. _troubleshooting_java:

Java problems
=============

This is probably most relevant with regard to |Tomcat| configuration.

Java has a number of performance and configuration options available which can
be determined using sources such as the following 

 * For command-line `Java 7 <http://docs.oracle.com/javase/7/docs/technotes/tools/windows/java.html>`_ args.
 * Determining the defaults : ``java -XX:+PrintFlagsFinal -version``.
 * For `Java 7 Garbage Collection <http://docs.oracle.com/javase/7/docs/technotes/guides/vm/gc-ergonomics.html>`_.
 * 3rd party `Heap Space explanation <https://plumbr.eu/outofmemoryerror/java-heap-space>`_.
 * `Stack Overflow application memory usage <http://stackoverflow.com/questions/131303/how-to-measure-actual-memory-usage-of-an-application-or-process>`_.

.. seealso:: :ref:`Java environment setting <environment_setting>`.

Java memory use / ``OutOfMemoryError``
--------------------------------------

If this error appears in the process of :ref:`log file analysis <troubleshooting_logfiles>`
then it may be necessary to increase the amount of memory available to Java,
perhaps using the ``JAVA_OPTS`` environment var (or ``CATALINA_OPTS`` if
specifically for |Tomcat|), e.g.
``CATALINA_OPTS="-Xms1g -Xmx1g -Xss1g -XX:PermSize=256m -XX:MaxPermSize=256m"``. |br|
Having said that, it has been noted by some commentators that increasing the
available memory may simply obscure temporarily a deeper problem such as memory
leaking.

Java memory use / ``PermGen``
-----------------------------

It looks like |Maven| ``site`` building may fail on Java 7 due to PermGen space
issues in some circumstances. Java 8 runs fine.