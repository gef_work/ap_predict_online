# ArgoUML (class diagrams)

 1. Download `ArgoUML-0.34.tar.gz` from [ArgoUML](http://argouml.tigris.org/ "ArgoUML website").
 1. `tar -zxvf ArgoUML-0.34.tar.gz`
 1. `cd argouml-0.34`
 1. Start the application : `java -jar argouml.jar`
 1. Read in the `.uml` file.

# sdedit (sequence diagrams)

 1. `git clone https://github.com/sdedit/sdedit.git`
 1. Follow instructions at `sdedit/QSD/README`
 1. Read in the `.sd` file.