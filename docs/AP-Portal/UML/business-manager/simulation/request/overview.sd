#!>>
#! Simplification of business-manager simulation request handling operations
#!
#! The Spring Integration Workflow uses a payload-type-router (integration/appCtx.int.xml) to pipeline the incoming 
#! ProcessSimulationsRequest to the SimulationsRequestProcessor (integration/simulation/appCtx.sim.processRequest.xml)
#!
#! This stage overview captures what happens when there's an inbound WS request to run a simulation -- what happens is 
#! that a Simulation object may (or may not) be persisted in the database, and there's an .
#!
#!<<

client:Actor
simulationsRequestProcessor:SimulationsRequestProcessor[a]
simulationsRequestValidatorChain:SimulationsRequestValidatorChain[a]
simulation:Simulation[a]
simulationService:SimulationService[a]
processedSimulationRequestVO:ProcessedSimulationRequestVO[a]
configuration:Configuration[a]
configurationActionPotential:ConfigurationActionPotential[a]
configurationDoseResponseFitting:ConfigurationDoseResponseFitting[a]
simulationManager:SimulationManager[a]
simulationDAO:SimulationDAO[a]
simulationProcessingGateway:SimulationProcessingGateway[a]

client:simulationsRequestProcessor.WS ProcessSimulationsRequest
simulationsRequestProcessor:simulationsRequestValidatorChain.simulationsRequestValidation(ProcessSimulationsRequest)

[c:loop SimulationDetail]
  simulationsRequestProcessor:simulationRequestVO=simulationService.userInputValidation(SimulationDetail properties)
  simulationService:simulationRequestVO=simulationManager.userInputValidation(SimulationDetail properties)
  simulationManager:properties=configuration.getConfiguration properties
  simulationManager:properties=configurationDoseResponseFitting.getConfigurationDoseResponseFitting properties
  simulationManager:properties=configurationActionPotential.getConfigurationActionPotential properties
  simulationManager:\\n/* Populate SimulationRequestVO */
[/c]
simulationsRequestProcessor:List<ProcessedSimulationRequestVO>=simulationService.processSimulationsRequest(List<SimulationRequestVO>)
[c:loop SimulationRequestVO]
  [c:alt verifiedSimulationRequest.isInputDataOnly() == true]
    simulationService:ProcessedSimulationRequestVO=simulationManager.requestProcessingInputDataOnly(verifiedSimulationRequest)
    simulationManager:createNewSimulation(SimulationRequestVO properties)
    simulationManager:ProcessedDataVO=simulationProcessingGateway.inputDataGathering(Simulation)
  --[else]
    simulationService:simulation=simulationManager.find(verifiedSimulationRequest)
    [c:alt simulation exists]
      simulationService:ProcessedSimulationRequestVO=simulationManager.requestProcessingExistingSimulation(verifiedSimulationRequest)
      [c:alt simulationRequest.isForceReRun() || simulationRequest.isReset()]
        [c:alt simulationRequest.isReset() && !simulation.isBusy()]
          simulationManager:\\n/* Ignoring request to reset a non-busy (free) simulation */
        --[else if simulation.assignProcessTask(forceReRun, reset)]
          simulationManager:makeAvailableForProcessingSelection(simulation)
        --[else]
          simulationManager:\\n/* Ignoring request to re-run or reset simulation as no change imposed */   
        [/c]
      --[else]
        simulationManager:\\n/* Not forcing a re-run or resetting */
        [c:alt simulation.isBusy() || !simulation.isRequestProcessed()]
          simulationManager:\\n/* Ignoring request as simulation busy, or not busy and awaiting processing */
        --[else]
          simulationManager:makeAvailableForProcessingSelection(simulation)
        [/c]
      [/c]
      [c:makeAvailableForProcessingSelection]
        simulationManager:simulation.makeAvailableForProcessing()
        simulationManager:save(simulation)
      [/c]
    --[else]
      simulationService:\\n/* New Simulation */
      simulationService:ProcessedSimulationRequestVO=simulationManager.requestProcessingNewSimulation(verifiedSimulationRequest)
      simulationManager:createNewSimulation(SimulationRequestVO properties)
      simulationManager:save(simulation)
      [c:save]
        simulationManager:simulation=simulationDAO.store(simulation)
      [/c]
    [/c]
  [/c]
[/c]
[c:loop ProcessSimulationRequestVO]
  simulationsRequestProcessor:String=processedSimulationRequestVO.getCompoundIdentifier()
  simulationsRequestProcessor:long=processedSimulationRequestVO.getSimulationId()
*1 simulationsRequestProcessor
Simulation id use by client
to query for progress
*1
(1)simulationsRequestProcessor
  simulationsRequestProcessor:processedDataVO=processedSimulationRequestVO.getInputData()
[/c]
simulationsRequestProcessor:client.WS ProcessSimulationsResponse