# Action Potential predict -- Dose-Response Manager

This is the component which handles requests to determine IC50 values from dose-response data
provided by `site-business`.

## Installation

Please see either of the following :

 1. http://apportal.readthedocs.io/en/latest/installation/components/dose-response-manager/index.html
 1. ap_predict_online/docs/AP-Portal/RtD/_build/html/installation/components/dose-response-manager/index.html