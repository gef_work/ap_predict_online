/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.fdr_manager.srvact;

import java.util.List;
import java.util.ArrayList;

import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.fdr_manager.ws.schema.jaxb.FDRRunRequest;
import uk.ac.ox.cs.epsrc.fdr_manager.ws.schema.jaxb.FDRRunResponse;
import uk.ac.ox.cs.epsrc.fdr_manager.ws.schema.jaxb.ObjectFactory;
import uk.ac.ox.cs.epsrc.jni.DoseResponseFitter;

/**
 * 
 *
 * @author Geoff Williams
 */
@Component
public class FdrRequestProcessor {

  private static final String RESPONSE_IC50 = "IC50";
  private static final String RESPONSE_HILLCOEFFICIENT = "HillCoefficient";

  private static final DoseResponseFitter doseResponseFitter = new DoseResponseFitter();

  private static final ObjectFactory objectFactory = new ObjectFactory();

  private static final Log log = LogFactory.getLog(FdrRequestProcessor.class);

  /**
   * Invoke the dose-response fitting code.
   * 
   * @param fdrRunRequest Incoming SOAP request.
   * @return Outgoing SOAP response.
   */
  @ServiceActivator
  public FDRRunResponse processFDRRequest(final FDRRunRequest fdrRunRequest) {
    log.trace("~int.processFDRRequest() : Thread '" + Thread.currentThread().getName() + "'.");

    final FDRRunResponse fdrRunResponse = objectFactory.createFDRRunResponse();

    final String concentrationsCSV = fdrRunRequest.getConcentrations();
    final String inhibitionsCSV = fdrRunRequest.getInhibitions();

    if (concentrationsCSV == null || concentrationsCSV.trim().length() == 0 ||
        inhibitionsCSV == null || inhibitionsCSV.trim().length() == 0) {
      final String errorMessage = "No values... at a minimum provide both concentrations and inhibitions.";
      log.debug("~int.processFDRRequest() : " + errorMessage);
      fdrRunResponse.setError(errorMessage);
      return fdrRunResponse;
    }
       
    final String[] concentrations = concentrationsCSV.split(",");
    final String[] inhibitions = inhibitionsCSV.split(",");

    if (concentrations.length != inhibitions.length) {
      final String errorMessage = "Unequal lengths... concentrations vs. inhibitions : " + concentrations.length + "-" + inhibitions.length + ".";
      log.debug("~int.processFDRRequest() : " + errorMessage);
      fdrRunResponse.setError(errorMessage);
      return fdrRunResponse;
    }

    final String parameterFitting = fdrRunRequest.getParameterfitting();
    final Boolean rounded = fdrRunRequest.getRounded();
    final Float hillMin = fdrRunRequest.getHillmin();
    final Float hillMax = fdrRunRequest.getHillmax();

    final List<String> componentList = new ArrayList<String>();
    componentList.add("\"concentrations\" : [" + StringUtils.join(concentrations, ",") + "]");
    componentList.add("\"inhibitions\" : [" + StringUtils.join(inhibitions, ",") + "]");
    if (parameterFitting != null && parameterFitting.trim().length() > 0)
      componentList.add("\"parameterfitting\" : \"" + parameterFitting.trim() + "\"");
    if (rounded != null) componentList.add("\"rounded\" : " + rounded);
    if (hillMin != null) componentList.add("\"hillmin\" : " + hillMin);
    if (hillMax != null) componentList.add("\"hillmax\" : " + hillMax);

    final String fdrInputs = "{".concat(StringUtils.join(componentList, ",")).concat("}");
    log.debug("~int.processFDRRequest() : FDR request '" + fdrInputs + "'.");

    final JSONObject fdrJSONResponse = JSONObject.fromObject(doseResponseFitter.fitDoseResponse(fdrInputs));

    log.debug("~int.processFDRRequest() : FDR response '" + fdrJSONResponse.toString() + "'.");
    fdrRunResponse.setIC50(fdrJSONResponse.getString(RESPONSE_IC50));
    fdrRunResponse.setHillCoefficient(fdrJSONResponse.getString(RESPONSE_HILLCOEFFICIENT));

    return fdrRunResponse;
  }

  // crude way to effectively serialize incoming web service requests
  private synchronized String fitIt(final String fdrInputs) {
    final DoseResponseFitter doseResponseFitter = new DoseResponseFitter();
    return doseResponseFitter.fitDoseResponse(fdrInputs);
  }
}
