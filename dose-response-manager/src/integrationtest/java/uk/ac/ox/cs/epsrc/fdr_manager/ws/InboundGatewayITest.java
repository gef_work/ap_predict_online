/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.fdr_manager.ws;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.ws.MarshallingWebServiceInboundGateway;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ws.context.DefaultMessageContext;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.pox.dom.DomPoxMessage;
import org.springframework.ws.pox.dom.DomPoxMessageFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * Testing the web service inbound gateway FDR run request handling.
 *
 * @author Geoff Williams
 */
@ContextConfiguration(locations = { "classpath:META-INF/spring/ctx/appCtx.business.xml",
                                    "classpath:META-INF/spring/ctx/appCtx.int.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class InboundGatewayITest {

  private static final String namespace = "http://cs.ox.ac.uk/epsrc/fdr_manager/ws/1";

  private static final String KEY_IC50 = "IC50";
  private static final String KEY_HILL_COEFFICIENT = "HillCoefficient";

  @Autowired(required=true)
  private MarshallingWebServiceInboundGateway gateway;

  private DocumentBuilder documentBuilder;
  private DomPoxMessageFactory messageFactory;
  private Transformer transformer;

  private static final Log log = LogFactory.getLog(InboundGatewayITest.class);

  @Before
  public void setUp() throws ParserConfigurationException, TransformerConfigurationException,
                             TransformerFactoryConfigurationError {
    final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    documentBuilderFactory.setNamespaceAware(true);
    documentBuilder = documentBuilderFactory.newDocumentBuilder();

    messageFactory = new DomPoxMessageFactory();

    transformer = TransformerFactory.newInstance().newTransformer();
  }

  @After
  public void shutDown() {
    documentBuilder = null;
    messageFactory = null;
    transformer = null;
  }

  @SuppressWarnings("deprecation")
  private Map<String, String> invokeGateway(final String xml) throws Exception {
    final Document document = documentBuilder.parse(new InputSource(new StringReader(xml)));
    final DomPoxMessage request = new DomPoxMessage(document, transformer, "text/xml");
    final MessageContext messageContext = new DefaultMessageContext(request, messageFactory);
    gateway.invoke(messageContext);
    final Object reply = messageContext.getResponse().getPayloadSource();
    assertThat(reply, is(DOMSource.class));

    final DOMSource replySource = (DOMSource) reply;
    final Element element = (Element) replySource.getNode().getFirstChild();
    assertThat(element.getTagName(), equalTo("FDRRunResponse"));

    final NodeList nodeList = element.getChildNodes();
    final Map<String, String> results = new HashMap<String, String>();
    for (int nodeIdx = 0 ; nodeIdx < nodeList.getLength(); nodeIdx++) {
      final Node node = nodeList.item(nodeIdx);
      final String nodeName = node.getNodeName();
      if (KEY_IC50.equals(nodeName)) {
        results.put(KEY_IC50, node.getFirstChild().getTextContent());
      } else if (KEY_HILL_COEFFICIENT.equals(nodeName)) {
        results.put(KEY_HILL_COEFFICIENT, node.getFirstChild().getTextContent());
      }
    }

    return results;
  }

  @Test
  public void test1_CaV12() throws Exception {
    log.debug("test1_CaV12() : Invoked.");
    final String xml = "<ws:FDRRunRequest xmlns:ws=\"" + namespace + "\">" +
                       "  <ws:concentrations>15.00000000000000000000,0.00001430511474609380,3.75000000000000000000,0.00005722045898437500,0.23437500000000000000,0.93750000000000000000,0.00366210937500000000,0.01464843750000000000,0.00091552734375000000,0.05859375000000000000,0.00022888183593750000,0.00091552734375000000,15.00000000000000000000,0.00366210937500000000,0.93750000000000000000,3.75000000000000000000,0.23437500000000000000,0.00001430511474609380,0.01464843750000000000,0.00005722045898437500,0.05859375000000000000,0.00022888183593750000,0.23437500000000000000,0.05859375000000000000,0.00005722045898437500,3.75000000000000000000,0.93750000000000000000,0.00022888183593750000,0.00001430511474609380,0.00366210937500000000,0.01464843750000000000,15.00000000000000000000,0.00091552734375000000,0.00366210937500000000,0.00001430511474609380,0.00022888183593750000,0.00091552734375000000,0.93750000000000000000,0.05859375000000000000,0.01464843750000000000,0.00005722045898437500,3.75000000000000000000,15.00000000000000000000,0.23437500000000000000,0.05859375000000000000,0.00022888183593750000,15.00000000000000000000,0.23437500000000000000,0.00366210937500000000,3.75000000000000000000,0.93750000000000000000,0.00005722045898437500,0.01464843750000000000,0.00091552734375000000,0.00001430511474609380,0.93750000000000000000,0.23437500000000000000,0.01464843750000000000,0.00091552734375000000,3.75000000000000000000,15.00000000000000000000,0.00005722045898437500,0.00022888183593750000,0.00001430511474609380,0.05859375000000000000,0.00366210937500000000</ws:concentrations>" +
                       "  <ws:inhibitions>50,28,24,21,19,15,15,14,13,13,4,28,22,22,20,20,19,19,16,10,4,2,16,14,13,10,5,3,1,-9,-12,-15,-20,34,23,21,21,20,19,17,16,13,12,9,19,18,16,14,13,8,8,4,4,-3,-8,26,23,21,19,17,15,-1,-3,-5,-10,-16</ws:inhibitions>" +
                       "</ws:FDRRunRequest>";
    invokeGateway(xml);
    final Map<String, String> results = invokeGateway(xml);
    assertEquals("1000000", results.get(KEY_IC50));
    assertEquals("1", results.get(KEY_HILL_COEFFICIENT));
  }

  @Test
  public void test1_KCNQ1() throws Exception {
    log.debug("test1_KCNQ1() : Invoked.");
    final String xml = "<ws:FDRRunRequest xmlns:ws=\"" + namespace + "\">" +
                       "  <ws:concentrations>0.01143118427069040000,0.00127013158563227000,0.30864197530864200000,0.10288065843621400000,0.00042337719521075700,0.03429355281207130000,0.00381039475689681000,0.92592592592592600000,8.33333333333333000000,2.77777777777778000000,25.00000000000000000000,0.03429355281207130000,0.00381039475689681000,2.77777777777778000000,0.00127013158563227000,0.01143118427069040000,0.30864197530864200000,0.92592592592592600000,0.00042337719521075700,8.33333333333333000000,25.00000000000000000000,0.10288065843621400000</ws:concentrations>" +
                       "  <ws:inhibitions>15,6,3,-1,-5,-8,-10,-17,-31,-32,-36,11,10,8,3,2,-5,-6,-6,-8,-22,-44</ws:inhibitions>" +
                       "</ws:FDRRunRequest>";
    invokeGateway(xml);
    final Map<String, String> results = invokeGateway(xml);
    assertEquals("1000000", results.get(KEY_IC50));
    assertEquals("1", results.get(KEY_HILL_COEFFICIENT));
  }

  @Test
  public void test1_NaV15() throws Exception {
    log.debug("test1_NaV15() : Invoked.");
    final String xml = "<ws:FDRRunRequest xmlns:ws=\"" + namespace + "\">" +
                       "  <ws:concentrations>11.11111111111110000000,0.13717421124828500000,0.04572473708276180000,0.00508052634252909000,0.00169350878084303000,3.70370370370370000000,1.23456790123457000000,0.41152263374485600000,33.33333333333330000000,0.01524157902758730000,100.00000000000000000000,33.33333333333330000000,1.23456790123457000000,0.04572473708276180000,11.11111111111110000000,100.00000000000000000000,0.13717421124828500000,0.41152263374485600000,3.70370370370370000000,0.00508052634252909000,0.01524157902758730000,0.00169350878084303000</ws:concentrations>" +
                       "  <ws:inhibitions>24,22,18,14,14,11,11,10,10,9,3,22,16,14,13,13,12,10,7,7,4,3</ws:inhibitions>" +
                       "</ws:FDRRunRequest>";
    invokeGateway(xml);
    final Map<String, String> results = invokeGateway(xml);
    assertEquals("1000000", results.get(KEY_IC50));
    assertEquals("1", results.get(KEY_HILL_COEFFICIENT));
  }

  @Test
  public void testFDRRunRequest1() throws Exception {
    log.debug("testFDRRunRequest1() : Invoked.");
    final String xml = "<ws:FDRRunRequest xmlns:ws=\"" + namespace + "\">" +
                       "  <ws:concentrations>0.0000143051147460938000000,0.0002288818359375000000,0.000057220458984375000000,0.00091552734375000000,0.003662109375000000,0.0146484375000000,0.9375000000,0.234375000000,3.75000000,0.05859375000000,15.000000,0.000057220458984375000000,0.0000143051147460938000000,0.00091552734375000000,0.0146484375000000,0.0002288818359375000000,15.000000,0.003662109375000000,0.234375000000,0.9375000000,0.05859375000000,3.75000000,15.000000,3.75000000,0.234375000000,0.9375000000,0.0002288818359375000000,0.00091552734375000000,0.000057220458984375000000,0.0000143051147460938000000,0.003662109375000000,0.05859375000000,0.0146484375000000,0.00091552734375000000,15.000000,0.234375000000,0.003662109375000000,0.0000143051147460938000000,0.05859375000000,0.000057220458984375000000,0.9375000000,3.75000000,0.0002288818359375000000,0.0146484375000000,15.000000,0.00091552734375000000,0.05859375000000,3.75000000,0.003662109375000000,0.234375000000,0.0146484375000000,0.000057220458984375000000,0.0000143051147460938000000,0.9375000000,0.0002288818359375000000,3.75000000,15.000000,0.234375000000,0.9375000000,0.00091552734375000000,0.0002288818359375000000,0.000057220458984375000000,0.05859375000000,0.0146484375000000,0.0000143051147460938000000,0.003662109375000000</ws:concentrations>" +
                       "  <ws:inhibitions>21,21,20,19,5,5,5,4,-1,-3,-16,15,13,12,8,7,6,3,1,0,-1,-8,25,15,12,11,10,10,9,2,1,1,-3,-24,-24,-27,-29,-29,-31,-31,-35,-37,-42,-52,-2,-6,-7,-8,-12,-14,-19,-19,-19,-20,-29,4,3,0,-2,-3,-7,-7,-8,-8,-8,-11</ws:inhibitions>" +
                       "</ws:FDRRunRequest>";

    invokeGateway(xml);
    final Map<String, String> results = invokeGateway(xml);
    assertEquals("3435973843", results.get(KEY_IC50));
    assertEquals("1.08125", results.get(KEY_HILL_COEFFICIENT));
  }

  @Test
  public void testFDRRunRequest2() throws Exception {
    log.debug("testFDRRunRequest2() : Invoked.");
    final String xml = "<ws:FDRRunRequest xmlns:ws=\"" + namespace + "\">" +
                       "  <ws:concentrations>0.37,1.11,3.33,10</ws:concentrations>" +
                       "  <ws:inhibitions>27.49,51.45,74.8,88.49</ws:inhibitions>" +
                       "</ws:FDRRunRequest>";
    final Map<String, String> results = invokeGateway(xml);
    assertEquals("1.046176", results.get(KEY_IC50));
    assertEquals("0.924986", results.get(KEY_HILL_COEFFICIENT));
  }


  @Test
  public void testFDRRunRequest3() throws Exception {
    log.debug("testFDRRunRequest3() : Invoked.");
    final String xml = "<ws:FDRRunRequest xmlns:ws=\"" + namespace + "\">" +
                       "  <ws:concentrations>25.000000,8.33333333333333000000,0.925925925925926000000,2.77777777777778000000,0.0114311842706904000000,0.308641975308642000000,0.102880658436214000000,0.000423377195210757000000,0.0342935528120713000000,0.00127013158563227000000,0.00381039475689681000000,25.000000,0.0342935528120713000000,2.77777777777778000000,0.925925925925926000000,0.00381039475689681000000,8.33333333333333000000,0.308641975308642000000,0.00127013158563227000000,0.0114311842706904000000,0.000423377195210757000000,8.33333333333333000000,25.000000,0.308641975308642000000,0.925925925925926000000,2.77777777777778000000,0.0114311842706904000000,0.000423377195210757000000,0.00127013158563227000000,0.102880658436214000000,0.0342935528120713000000,0.00381039475689681000000,25.000000,8.33333333333333000000,0.000423377195210757000000,2.77777777777778000000,0.308641975308642000000,0.0342935528120713000000,0.00381039475689681000000,0.0114311842706904000000,0.102880658436214000000,0.925925925925926000000,0.00127013158563227000000,0.00381039475689681000000,0.0114311842706904000000,0.000423377195210757000000,25.000000,0.0342935528120713000000,0.102880658436214000000,0.925925925925926000000,2.77777777777778000000,8.33333333333333000000,0.308641975308642000000,0.000423377195210757000000,0.0114311842706904000000,0.00127013158563227000000,0.00381039475689681000000,25.000000,8.33333333333333000000,0.102880658436214000000,0.0342935528120713000000,0.925925925925926000000,2.77777777777778000000,0.308641975308642000000</ws:concentrations>" +
                       "  <ws:inhibitions>29,14,6,5,3,3,2,2,-1,-2,-3,48,2,-2,-3,-5,-6,-7,-8,-10,-17,14,-3,-4,-7,-9,-13,-15,-15,-18,-18,-20,23,6,3,-1,-2,-2,-2,-4,-6,-14,-25,60,55,52,41,29,28,23,-4,-5,-12,112,84,78,69,45,34,29,29,22,17,-5</ws:inhibitions>" +
                       "</ws:FDRRunRequest>";

    invokeGateway(xml);
    final Map<String, String> results = invokeGateway(xml);
    assertEquals("46.91319", results.get(KEY_IC50));
    assertEquals("1.310617", results.get(KEY_HILL_COEFFICIENT));
  }
}
