/*

  Copyright (c) 2005-2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual;

import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.MarkedDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;

/**
 * Strategy to indicate if data in an individual record represents invalid data.
 * <p>
 * Whereas in some systems the data is marked invalid at the point of data entry (in which case
 * either the data can be discarded when being read in by the portal, or it will be removed by
 * the business manager <tt>QCOfficer#removeQCFailedData</tt> in combination with 
 * {@link MarkedDataHolder#isMarkedInvalid} processing), sometimes invalid values are encountered
 * in the screening results data and these need to be flagged to the user (and potentially
 * removed from workflow processing).
 * 
 * @author geoff
 */
public interface ModelAsInvalidStrategy extends PC50EvaluationStrategy {

  /**
   * Indicate if the individual data record is to be modeled as being invalid.
   * 
   * @param assayVO Assay value object.
   * @param individualData Individual data record.
   * @param individualC50Data ICData for this individual record.
   * @param identifyingPrefix Logging prefix.
   * @return Reason if to model as inactive, otherwise <tt>null</tt>.
   * @throws InvalidValueException If an invalid value was encountered.
   */
  // Being a bit optimistic here that these parameters are appropriate for an API!
  String modelAsInvalid(AssayVO assayVO, IndividualDataRecord individualData,
                        ICData individualC50Data, String identifyingPrefix)
                        throws InvalidValueException;

}