/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.helper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.util.ConverterUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICRawDataSource;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICType;

/**
 * Helper for inhibitory concentration data.
 *
 * @author geoff
 */
public class ICDataHelper {

  private static final Log log = LogFactory.getLog(ICDataHelper.class);

  // Prevent instantiation.
  private ICDataHelper() {}

  /**
   * Create a new inhibitory concentration data object from raw data based on retrieval of data 
   * from the specified column names.
   * 
   * @param dataRecord Raw data.
   * @param icType IC type.
   * @param valueColumnName Raw data column name of the IC value.
   * @param unitColumnName Raw data column name of the IC units, or {@code null} to apply default
   *                       for IC type.
   * @param modifierColumnName Raw data column name of the IC modifier.
   * @param recordCountColumnName Raw data column name of the IC record count column name, or
   *                                 {@code null} if not available.
   * @param allRecordCountColumnName Raw data column name of the all record count column name, or
   *                                 {@code null} if not available.
   * @param stdErrColumnName Raw data column name of the standard error value, or {@code null}
   *                            if not available.
   * @return Created IC data or {@code null} if invalid value encountered during creation of IC
   *         object, e.g. a null IC50 value was in the raw data record.
   * @throws IllegalArgumentException If {@code null} passed for any of the required arguments, or
   *                                  invalid column name used for {@code dataRecord} value
   *                                  extraction (already logged).
   * 
   * @see ICDataVO
   */
  public static ICData createICData(final DataRecord dataRecord, final ICType icType,
                                    final String valueColumnName,
                                    final String unitColumnName,
                                    final String modifierColumnName,
                                    final String recordCountColumnName,
                                    final String allRecordCountColumnName,
                                    final String stdErrColumnName)
                                    throws IllegalArgumentException {
    log.debug("~createICData() : Invoked.");
    if (dataRecord == null || icType == null || valueColumnName == null) {
      log.error("~createICData() : Invalid null in dataRecord '" + dataRecord + "', icType '" + 
                icType + "', valueColumnName '" + valueColumnName + "'.");
      throw new IllegalArgumentException("ICData creation requires : 1. DataRecord, 2. ICType, 3. Value column name.");
    }

    final Object value = dataRecord.getRawDataValueByColumnName(valueColumnName);

    Unit icUnit = icType.getDefaultUnit();
    if (unitColumnName != null) {
      final String rawDataUnit = (String) dataRecord.getRawDataValueByColumnName(unitColumnName);
      if (rawDataUnit != null) {
        icUnit = Unit.valueOf(rawDataUnit);
      }
    }

    String modifier = null;
    if (modifierColumnName != null) {
      modifier = (String) dataRecord.getRawDataValueByColumnName(modifierColumnName);
    }
    final Integer recordCount = intValue(dataRecord, recordCountColumnName);
    final Integer allRecordCount = intValue(dataRecord, allRecordCountColumnName);
    Object stdErr = null;
    if (stdErrColumnName != null) {
      stdErr = dataRecord.getRawDataValueByColumnName(stdErrColumnName);
    }
    ICData icData = null;
    BigDecimal valueBD = null;
    BigDecimal stdErrBD = null;
    try {
      valueBD = ConverterUtil.toBigDecimal(value, valueColumnName);
      stdErrBD = ConverterUtil.toBigDecimal(stdErr, stdErrColumnName);
      icData = new ICDataVO(valueBD, valueColumnName, icUnit, modifier, modifierColumnName,
                            recordCount, recordCountColumnName, allRecordCount,
                            allRecordCountColumnName, stdErrBD, stdErrColumnName, icType);
    } catch (IllegalArgumentException e) {
      // Could be as a consequence of insufficient data in a record, e.g. no IC value in record.
      final StringBuffer info = new StringBuffer();
      info.append("Exception creating ICDataVO based on ");
      info.append("'" + valueColumnName + "' = '" + valueBD + "' (based on '" + value + "'), ");
      info.append("icUnit = '" + icUnit + "', ");
      info.append("'" + modifierColumnName + "' = '" + modifier + "', ");
      info.append("'" + recordCountColumnName + "' = '" + recordCount + "', ");
      info.append("'" + allRecordCountColumnName + "' = '" + allRecordCount + "', ");
      info.append("'" + stdErrColumnName + "' = '" + stdErrBD + "' (based on '" + stdErr + "'), ");
      info.append("icType = '" + icType + "'.");
      log.info("~createICData() : ".concat(info.toString()));
    } catch (InvalidValueException e) {
      // Could be as a consequence of dodgy data, e.g. a -ve IC value, or dodgy std err value.
      log.info("~createICData() : Invalid value encountered. '" + e.getMessage() + "'.");
    }

    return icData;
  }

  /**
   * Retrieve eligible inhibitory concentration data from the raw data record.
   * <p>
   * Collections of IC data are returned in the order in which they are specified in the raw data
   * sources.
   * 
   * @param icRawDataSources Potential sources of IC data in the raw data record.
   * @param dataRecord The raw data record (required).
   * @return Collection (potentially empty) of IC data.
   * @throws IllegalArgumentException If {@code null} passed for {@code dataRecord}.
   */
  public static List<ICData> eligibleICData(final List<ICRawDataSource> icRawDataSources,
                                            final DataRecord dataRecord) 
                                            throws IllegalArgumentException {
    if (dataRecord == null) {
      log.warn("~eligibleICData() : dataRecord '" + dataRecord + "'.");
      throw new IllegalArgumentException("Eligible ICData creation requires : 1. DataRecord.");
    }

    final List<ICData> eligibleICData = new ArrayList<ICData>();

    for (final ICRawDataSource icRawDataSource : icRawDataSources) {
      log.debug("~eligibleICData() : ICRawDataSource '" + icRawDataSource.toString() + "'.");

      final String valueColumnName = icRawDataSource.getValueColumnName();
      final String unitColumnName = icRawDataSource.getUnitColumnName();
      final String modifierColumnName = icRawDataSource.getModifierColumnName();
      final String recordCountColumnName = icRawDataSource.getRecordCountColumnName();
      final String allRecordCountColumnName = icRawDataSource.getAllRecordCountColumnName();
      final String stdErrColumnName = icRawDataSource.getStdErrColumnName();
      final ICType icType = icRawDataSource.getICType();
      ICData icData = null;

      try {
        icData = createICData(dataRecord, icType, valueColumnName, unitColumnName,
                              modifierColumnName, recordCountColumnName, allRecordCountColumnName,
                              stdErrColumnName);
        if (icData != null) {
          eligibleICData.add(icData);
        }
      } catch (IllegalArgumentException e) {
        // Already logged occurrences of null args passed, or invalid column names passed.
      }
    }

    return eligibleICData;
  }

  /**
   * Grab the integer value from the data record with specified field name.
   * 
   * @param dataRecord Raw data record (required).
   * @param columnName Column name to extract value from (required).
   * @return Integer value of content, or  {@code null} if not a valid number or {@code null}
   *         parameters passed.
   * @throws IllegalArgumentException {@link DataRecord#getRawDataValueByColumnName(String)}.
   */
  private static Integer intValue(final DataRecord dataRecord, final String columnName) 
                                  throws IllegalArgumentException {
    Integer count = null;

    if (dataRecord != null && columnName != null) {
      final Object countObject = dataRecord.getRawDataValueByColumnName(columnName);
      if (countObject != null) {
        try {
          final BigDecimal bdObject = new BigDecimal(countObject.toString());
          count = bdObject.intValue();
        } catch (NumberFormatException e) {
          log.warn("~intValue() : Error converting '" + columnName + "' count value '" + countObject.toString() + "' to integer from '" + dataRecord.toString() + "'!");
        }
      } else {
        log.info("~intValue() : Null object returned as count from '" + dataRecord.toString() + "'.");
      }
    }

    return count;
  }
}