/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.transfer.screening.GenericSiteData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 * Generic site data value object.
 *
 * @author geoff
 */
public abstract class AbstractGenericSiteDataVO implements GenericSiteData {

  private final AssayVO assay;
  private final Map<IndividualDataRecord, List<DoseResponseDataRecord>> nonSummaryDataRecords =
                new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();
  private final IonChannel ionChannel;
  private final SummaryDataRecord summaryDataRecord;

  private static final transient Log log = LogFactory.getLog(AbstractGenericSiteDataVO.class);

  /**
   * Initialising constructor.
   * 
   * @param assay Assay.
   * @param ionChannel Ion Channel.
   * @param summaryDataRecord Summary data record (Optionally null if only individual data available).
   * @param nonSummaryDataRecords Non-summary (i.e. individual and (if appropriate) dose-response) data records.
   * @param isIrregular Indicate if creating an irregular site data object.
   * @throws IllegalArgumentException On various conditions.
   */
  public AbstractGenericSiteDataVO(final AssayVO assay, final IonChannel ionChannel,
                                   final SummaryDataRecord summaryDataRecord,
                                   final Map<IndividualDataRecord, List<DoseResponseDataRecord>> nonSummaryDataRecords,
                                   final boolean isIrregular)
                                   throws IllegalArgumentException {
    if (!isIrregular && assay == null) {
      final String errorMessage = "Non-irregular site data must have an assay value";
      log.warn("~AbstractGenericSiteDataVO() : " + errorMessage);
      throw new IllegalArgumentException(errorMessage);
    }
    if (!isIrregular && ionChannel == null) {
      final String errorMessage = "Non-irregular site data must have an ion channel value";
      log.warn("~AbstractGenericSiteDataVO() : " + errorMessage);
      throw new IllegalArgumentException(errorMessage);
    }
    this.assay = assay;
    this.ionChannel = ionChannel;

    final int individualDataRecordCount = nonSummaryDataRecords == null ? 0 : nonSummaryDataRecords.size();
    if (summaryDataRecord == null && individualDataRecordCount == 0) {
      final String errorMessage = "There must be either a summary record or individual records (or both) assigned";
      log.warn("~AbstractGenericSiteDataVO() : " + errorMessage);
      throw new IllegalArgumentException(errorMessage);
    }
    this.summaryDataRecord = summaryDataRecord;

    if (nonSummaryDataRecords != null) {
      this.nonSummaryDataRecords.putAll(nonSummaryDataRecords);
    }
  }

  /**
   * Retrieve the individual records data source name. Use with {@link #hasIndividualData()} if 
   * null is returned, but a source name should always have been assigned to a data record!
   * 
   * @return Individual data records data source name (or null if no individual records).
   */
  public String getIndividualDataSourceName() {
    return hasIndividualData() ? getNonSummaryDataRecords().entrySet().iterator().next().getKey().getSourceName() : null;
  }

  /**
   * Retrieve the summary data source name. Use with {@link #hasSummaryData()} if null is returned,
   * but a source name should always have been assigned to a data record.
   * 
   * @return Summary data record source name (or null if no summary record).
   */
  public String getSummaryDataSourceName() {
    return hasSummaryData() ? getSummaryDataRecord(false).getSourceName() : null;
  }

  /**
   * Indicator of whether the site data has individual data records.
   * 
   * @return True if individual data records present.
   */
  public boolean hasIndividualData() {
    return (!getNonSummaryDataRecords().isEmpty());
  }

  /**
   * Indicator of whether the site data has summary data.
   * 
   * @return True if summary data present, otherwise false.
   */
  public boolean hasSummaryData() {
    return (getSummaryDataRecord(false) != null);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "AbstractGenericSiteDataVO [assay=" + assay + ", nonSummaryDataRecords="
           + nonSummaryDataRecords + ", ionChannel=" + ionChannel + ", summaryDataRecord="
           + summaryDataRecord + "]";
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.site_data.GenericSiteData#getAssay()
   */
  public AssayVO getAssay() {
    return assay;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.site_data.GenericSiteData#getNonSummaryDataRecords()
   */
  public Map<IndividualDataRecord, List<DoseResponseDataRecord>> getNonSummaryDataRecords() {
    return Collections.unmodifiableMap(nonSummaryDataRecords);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.transfer.screening.GenericSiteData#getSummaryDataRecord(boolean)
   */
  @Override
  public SummaryDataRecord getSummaryDataRecord(boolean whenSummaryDataOnly) {
    SummaryDataRecord returnRecord = null;
    if (whenSummaryDataOnly) {
      if (!hasIndividualData()) {
        returnRecord = summaryDataRecord;
      }
    } else {
      returnRecord = summaryDataRecord;
    }

    return returnRecord;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.site_data.GenericSiteData#getIonChannel()
   */
  public IonChannel getIonChannel() {
    return ionChannel;
  }
}