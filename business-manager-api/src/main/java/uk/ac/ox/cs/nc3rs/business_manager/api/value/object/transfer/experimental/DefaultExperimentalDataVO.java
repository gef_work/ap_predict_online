/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.experimental;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ExperimentalDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.transfer.experimental.ExperimentalResult;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.AbstractProblemTransferVO;

/**
 * Default experimental data object.
 * <p>
 * This class is for experimental data results which generate QT pct change values at various
 * concentrations and various pacing frequencies. There is not expected to be any replication of
 * pacing frequencies or concentrations for a specific pacing frequency.
 * 
 * @author geoff
 */
public class DefaultExperimentalDataVO extends AbstractProblemTransferVO 
                                       implements ExperimentalDataHolder {

  private Set<ExperimentalResult> experimentalResults = new HashSet<ExperimentalResult>();
  private Map<BigDecimal, Set<BigDecimal>> perFreqConcentrations = new HashMap<BigDecimal, Set<BigDecimal>>();

  private static final Log log = LogFactory.getLog(DefaultExperimentalDataVO.class);

  /**
   * Initialising constructor.
   * <p>
   * Both arguments may be null in the case of there being no experimental data for a compound and
   * no problems encountered.
   * 
   * @param experimentalResults Experimental results.
   * @param problems Problems generating data.
   */
  public DefaultExperimentalDataVO(final Set<ExperimentalResult> experimentalResults,
                                   final List<String> problems)
                                   throws IllegalArgumentException {
    super(problems);

    if (experimentalResults != null) {
      this.experimentalResults.addAll(experimentalResults);

      for (final ExperimentalResult experimentalResult : getExperimentalResults()) {
        final BigDecimal pacingFrequency = experimentalResult.getPacingFrequency();

        final Set<BigDecimal> compoundConcentrations = new TreeSet<BigDecimal>();
        for (final ConcQTPctChangeValuesVO concQTPctChangeValues : experimentalResult.getConcentrationQTPctChangeValues()) {
          final BigDecimal compoundConcentration = concQTPctChangeValues.getCompoundConcentration();
          if (!compoundConcentrations.contains(compoundConcentration)) {
            compoundConcentrations.add(compoundConcentration);
          } else {
            final String warnMessage = "Detected the presence of duplicated '" + compoundConcentration + 
                                       "' concentration for at frequency '" + pacingFrequency +
                                       "' in experimental data. Ignoring duplicated value.";
            log.warn("~retrievePerFreqCompoundConcs() : " + warnMessage);
          }
        }

        if (perFreqConcentrations.containsKey(pacingFrequency)) {
          final String warnMessage = "Detected the presence of duplicated '" + pacingFrequency + 
                                     "' pacing frequency in experimental data. Ignoring duplicated value.";
          log.warn("~retrievePerFreqCompoundConcs() : " + warnMessage);
        } else {
          perFreqConcentrations.put(pacingFrequency, compoundConcentrations);
        }
      }
    }
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "DefaultExperimentalDataVO [experimentalResults="
        + experimentalResults + ", perFreqConcentrations="
        + perFreqConcentrations + ", problems=" + problems + "]";
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ExperimentalDataHolder#containsData()
   */
  @Override
  public boolean containsData() {
    return (!getExperimentalResults().isEmpty());
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ExperimentalDataHolder#retrievePerFreqCompoundConcs()
   */
  @Override
  public Map<BigDecimal, Set<BigDecimal>> retrievePerFreqCompoundConcs() {
    return Collections.unmodifiableMap(perFreqConcentrations);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.transfer.experimental.ExperimentalDataHolder#getExperimentalResults()
   */
  @Override
  public Set<ExperimentalResult> getExperimentalResults() {
    return Collections.unmodifiableSet(experimentalResults);
  }
}