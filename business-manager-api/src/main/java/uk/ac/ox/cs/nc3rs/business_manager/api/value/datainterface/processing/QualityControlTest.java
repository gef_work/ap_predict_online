/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.QualityControlFailureVO;

/**
 * Interface inherited by all objects which contain tests of data quality.
 * <p>
 * If the test is invoked the calling code is expected to take appropriate action depending on the 
 * outcome, e.g. return a {@link QualityControlFailureVO} if the test detected invalid data.
 *
 * @author geoff
 */
public interface QualityControlTest {

  /**
   * Indicator of whether the test was passed or failed.
   * <p>
   * <b>Note</b> that a test passing does not necessarily represent a positive overall outcome, i.e.
   * if the test is for the presence of invalid data and it is found this method will return true!
   * 
   * @param dataRecord Data record to test.
   * @return True if test passed, false if failed.
   * @throws InvalidValueException If invalid value encountered.
   */
  boolean passed(final DataRecord dataRecord) throws InvalidValueException;

  /**
   * Retrieve the nature of the test.
   * 
   * @return Nature of test being undertaken.
   */
  String nature();
}