/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.IrregularSiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.AbstractProblemTransferVO;

/**
 * Value object containing the raw compound data in structures matching summary records to 
 * individual records for an ion channel and assay. 
 * <br>
 * Object also represents a means of passing problems encountered from the retrieval of site data
 * to the generic business manager processing, via the {@link #AggregatedSiteDataVO(List)} 
 * assignment.
 *
 * @author Geoff Williams
 */
public class AggregatedSiteDataVO extends AbstractProblemTransferVO {

  // collection of site data items.
  private final List<SiteDataHolder> siteDataHolders = new ArrayList<SiteDataHolder>();

  private final List<IrregularSiteDataHolder> irregularSiteDataHolders = new ArrayList<IrregularSiteDataHolder>();

  private static final transient Log log = LogFactory.getLog(AggregatedSiteDataVO.class);

  /**
   * Initialising constructor used in case of problems being encountered.
   * <br>
   * Represents a means of returning site-internal errors/problems to the business processing.
   * 
   * @param problems Site-internal errors/problems, optionally {@code null}.
   */
  public AggregatedSiteDataVO(final List<String> problems) {
    super(problems);
  }

  /**
   * Initialising constructor.
   * 
   * @param siteDataHolders Collection of data for the assays and ion channels.
   * @param irregularSiteDataHolders Collection of irregular data for the assays and ion channels
   *                                 (optionally {@code null}).
   * @param problems Problems generating data, optionally {@code null}.
   * @throws IllegalArgumentException If siteDataHolder is {@code null}.
   */
  public AggregatedSiteDataVO(final List<SiteDataHolder> siteDataHolders,
                              final List<IrregularSiteDataHolder> irregularSiteDataHolders,
                              final List<String> problems) {
    this(problems);
    if (siteDataHolders == null) {
      final String errorMessage = "Site data must be provided for aggregation.";
      log.warn("~AggregatedSiteDataVO() : " + errorMessage);
      throw new IllegalArgumentException(errorMessage);
    }

    this.siteDataHolders.addAll(siteDataHolders);
    if (irregularSiteDataHolders != null) {
      this.irregularSiteDataHolders.addAll(irregularSiteDataHolders);
    }
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "AggregatedSiteDataVO [siteDataHolders=" + siteDataHolders + ", irregularSiteDataHolders="
        + irregularSiteDataHolders + ", problems=" + getProblems() + "]";
  }

  /**
   * Indicator of whether the value object holds irregular site data.
   * 
   * @return {@code true} if contains irregular site data, otherwise {@code false}.
   */
  public boolean hasIrregularSiteData() {
    return !getIrregularSiteDataHolders().isEmpty();
  }

  /**
   * Indicator of whether the value object holds problems associated with data retrieval.
   * 
   * @return {@code true} if problems encountered during data retrieval, otherwise {@code false}.
   */
  public boolean hasProblems() {
    return !getProblems().isEmpty();
  }

  /**
   * Indicator of whether the value object holds site data.
   * <p>
   * This does not take irregular site data into consideration.
   * 
   * @return {@code true} if contains site data, otherwise {@code false}.
   */
  public boolean hasSiteData() {
    return !getSiteDataHolders().isEmpty();
  }

  /**
   * Retrieve the ion channel data collection.
   * 
   * @return Unmodifiable ion channel data. Empty collection if none available.
   */
  public List<SiteDataHolder> getSiteDataHolders() {
    return Collections.unmodifiableList(siteDataHolders);
  }

  /**
   * Retrieve the irregular ion channel data collection.
   * 
   * @return Unmodifiable irregular ion channel data. Empty collection if none available.
   */
  public List<IrregularSiteDataHolder> getIrregularSiteDataHolders() {
    return Collections.unmodifiableList(irregularSiteDataHolders);
  }

  /**
   * Retrieve any problems encountered.
   * 
   * @return Problems. Empty collection if none available.
   */
  public List<String> getProblems() {
    return Collections.unmodifiableList(problems);
  }
}