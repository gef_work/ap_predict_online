/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
/**
 * Many of the interfaces in this package have methods which throw the 
 * {@link uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException}
 * as the implementing classes are generally responsible for extracting the relevant
 * values from a single raw table data collection at instantiation time or during workflow
 * processing, e.g. a single table record of individual data would be expected to have all the 
 * data for population of the various holders which the individual record inherits. See 
 * {@link uk.ac.ox.cs.nc3rs.business_manager.api.value.object.datarecord.individual.AbstractIndividualDataRecordVO}.
 * <p>
 * The exceptions being interfaces such as {@link uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ExperimentalDataHolder},
 * which have implementing classes which are unlikely to be populated from a single database record,
 * but rather a number of records in a table, and as such the processing to populare the holder is
 * undertaken not in the holder's implementation itself, but elsewhere such as the
 * {@link uk.ac.ox.cs.nc3rs.business_manager.api.dao.SiteDAO} implementation, and it is in such
 * interfaces where the <code>InvalidValueException</code> will be generated.
 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder;