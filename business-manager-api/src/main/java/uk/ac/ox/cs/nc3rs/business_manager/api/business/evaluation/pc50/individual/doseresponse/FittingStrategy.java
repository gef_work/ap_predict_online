/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.doseresponse;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingParams;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingResult;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.IndividualProcessing;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;

/**
 * Strategy for fitting dose-response data from {@link ICData} objects.
 * <p>
 * Examples of implementing classes are :
 * <ul>
 *   <li>business_manager.business.evaluation.pc50.individual.doseresponse.AllDoseResponses</li>
 *   <li>business_manager.business.evaluation.pc50.individual.doseresponse.NearestSub50Response</li>
 * </ul>
 *
 * @author geoff
 */
public interface FittingStrategy {

  /**
   * Evaluate the PC50 based on fitting of dose-response data.
   * 
   * @param simulationId Simulation identifier.
   * @param inputDataGathering True if performing input data gathering, otherwise false.
   * @param individualProcessing Individual processing object.
   * @param individualICData Individual inhibitory concentration data object.
   * @param recordIdentifier (Optional) Identifier of the record being processed.
   * @param doseResponseFittingParams Dose-Response fitting parameters.
   * @param pctIC The inhibitory concentration (e.g. 20%, 50%) to compare the fitting value against 
   * @return Dose-response fitting result, or {@code null} if no PC50 determinable.
   * @throws IllegalArgumentException If a invalid argument value was encountered.
   * @throws InvalidValueException If an invalid value was encountered.
   */
  DoseResponseFittingResult fitDoseResponseData(long simulationId,
                                                boolean inputDataGathering,
                                                IndividualProcessing individualProcessing,
                                                ICData individualICData,
                                                String recordIdentifier,
                                                DoseResponseFittingParams doseResponseFittingParams,
                                                ICPercent pctIC)
                                                throws IllegalArgumentException,
                                                       InvalidValueException;
}