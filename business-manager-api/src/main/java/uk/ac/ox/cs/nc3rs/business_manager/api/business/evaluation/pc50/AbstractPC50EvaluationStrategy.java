/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50;

/**
 * Abstract class which all PC50 evaluation strategies must extend.
 * 
 * @author geoff
 */
// See appCtx.config.pc50Evaluators.site.xml
public abstract class AbstractPC50EvaluationStrategy implements PC50EvaluationStrategy {

  private static final long serialVersionUID = 6172043259130370998L;

  private final boolean defaultActive;
  private final int defaultInvocationOrder;
  private final String description;

  /**
   * Initialising constructor.
   * 
   * @param description Description of the PC50 evaluation strategy, e.g.
   *                    "pIC50 from individual. Nearest integer (respecting modifier)."
   * @param defaultActive Indicator of whether the strategy should be active by default.
   * @param defaultInvocationOrder Default strategy invocation order.
   * @throws IllegalArgumentException If <code>description</code> is not provided.
   */
  protected AbstractPC50EvaluationStrategy(final String description, final boolean defaultActive,
                                           final int defaultInvocationOrder)
                                           throws IllegalArgumentException {
    if (description == null) {
      final String errorMessage = "Illegal attempt to create an abstract PC50 evaluation strategy with a null description.";
      throw new IllegalArgumentException(errorMessage);
    }

    this.defaultActive = defaultActive;
    this.description = description;
    this.defaultInvocationOrder = defaultInvocationOrder;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy#getDefaultActive()
   */
  @Override
  public boolean getDefaultActive() {
    return defaultActive;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy#getDescription()
   */
  @Override
  public String getDescription() {
    return description;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy#getOrder()
   */
  @Override
  public int getOrder() {
    return defaultInvocationOrder;
  }
}