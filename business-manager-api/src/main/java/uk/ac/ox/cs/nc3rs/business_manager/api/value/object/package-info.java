/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
/**
 * Default implementation of objects usually derived from interfaces in the 
 * {@link uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface} package.
 * <p>
 * These objects are generally used for transferring data from site-specific processing to generic
 * business manager processing.
 * </p>
 * <hr>
 * <p>
 *   <b style="font-size: 150%">Loading site-specific data</b>
 * </p>
 * The process which generally occurs is that the site-specific DAO will take the raw 
 * <tt>Map&lt;String, Object&gt;</tt> results of a database query and load it into a site's individual or
 * summary {@link uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DataRecord} implementation
 * (e.g. classes extended from 
 * {@link uk.ac.ox.cs.nc3rs.business_manager.api.value.object.datarecord.individual.AbstractIndividualDataRecordVO} 
 * such as the sample site class <tt>CSIndividualDataRecordVO</tt>). At this point no data is 
 * actually extracted from the raw data record.<br>
 * These <tt>DataRecord</tt>s are then loaded up into the 
 * {@link uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder} 
 * implementations and placed in the 
 * {@link uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening.AggregatedSiteDataVO AggregatedSiteDataVO}
 * which is returned from the site's code to the <tt>business-manager</tt> code. It's not until 
 * the <tt>business-manager</tt>'s <tt>WorkflowProcessor</tt> starts processing the workflow that
 * the application tries various techniques to extract the most suitable C20/C50 data to use, using
 * various classes such as <tt>C50DataSelector</tt> in combination with
 * {@link uk.ac.ox.cs.nc3rs.business_manager.api.business.selection.ic.ICDataSelectionStrategy} 
 * implementations ... as follows :<br >
 * By the time it arrives at the workflow processing a {@link uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DataRecord}
 * implementation is merely holding the raw data record - but it does also have defined the 
 * technique for extracting potentially useable C20 and/or C50 objects from the raw data, in the
 * form of {@link uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICRawDataSource}s which 
 * define columns in the raw data to extract values from. This collection of <tt>ICRawDataSource</tt>
 * eligible C20 and/or C50 objects are then loaded up by the
 * {@link uk.ac.ox.cs.nc3rs.business_manager.api.value.helper.ICDataHelper} and then the 
 * <tt>WorkflowProcessor</tt> calls the <tt>C50DataSelector</tt> which potentially has configured a
 * number of {@link uk.ac.ox.cs.nc3rs.business_manager.api.business.selection.ic.ICDataSelectionStrategy}
 * objects which ultimately decide which data should be used as the definitive C50 and C20 values
 * for the original <tt>DataRecord</tt>. This definitive record is then used by the workflow
 * processing.
 * <hr>
 *  
 * @author geoff
 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object;