/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.datarecord;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DataRecord;

/**
 * Abstract database record.
 * 
 * @author Geoff Williams
 */
public abstract class AbstractGenericDataRecordVO {

  private final String id;
  private final Map<String, Object> rawData = new HashMap<String, Object>();
  private final String sourceName;

  private static final Log log = LogFactory.getLog(AbstractGenericDataRecordVO.class);

  /**
   * Initialising constructor.
   * 
   * @param siteId Unique site-specific identifier. If none is available pass a <code>null</code> 
   *               and a random {@link UUID} is created.
   * @param sourceName Name of the database (if data from multiple tables) or table of data source (required).
   * @param rawData Raw data record (required).
   * @throws IllegalArgumentException If required arguments are not supplied.
   */
  protected AbstractGenericDataRecordVO(final String siteId, final String sourceName,
                                        final Map<String, Object> rawData)
                                        throws IllegalArgumentException {
    if (sourceName == null || rawData == null) {
      final String errorMessage = "Invalid null in sourceName '" + sourceName + "', rawData '" +
                                  rawData + "'";
      log.warn("~AbstractGenericDataRecordVO() : " + errorMessage);
      throw new IllegalArgumentException(errorMessage);
    }
    if (siteId == null) {
      this.id = UUID.randomUUID().toString();
    } else {
      this.id = siteId;
    }
    log.debug("~AbstractGenericDataRecordVO() : Assigning an id of '" + this.id + "'.");
    this.sourceName = sourceName;
    //if (rawData != null) { 
      this.rawData.putAll(rawData);
    //}
  }

  /**
   * This will also write an error to the logs if the column name does not exist.
   * 
   * @param columnName Name of the column in the raw data record (required).
   * @return Object found in the specified column (potentially a {@code null} value).
   * @throws IllegalArgumentException If {@code columnName} is {@code null} or raw data doesn't
   *                                  contain the specified {@code columnName}.
   * @see DataRecord#getRawDataValueByColumnName(String)
   */
  public Object getRawDataValueByColumnName(final String columnName) throws IllegalArgumentException {
    if (columnName == null) {
      throw new IllegalArgumentException("Invalid attempt to retrieve a raw data value using a null column name.");
    }
    if (!getRawData().containsKey(columnName)) {
      final String errorMessage = "No column named '" + columnName + "' in available names '" + getRawData().keySet() + "'";
      log.error("~getRawDataValueByColumnName() : " + errorMessage + "'.");
      throw new IllegalArgumentException(errorMessage);
    }

    return getRawData().get(columnName);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "AbstractGenericDataRecordVO [id=" + id + ", rawData=" + rawData
        + ", sourceName=" + sourceName + "]";
  }

  /**
   * Retrieve the record identifier.
   * <p>
   * This will be a value as designated in the raw data record, or a system-assigned <code>UUID</code>
   * value.
   * 
   * @return Record-designated or <code>UUID</code>-assignedAssigned
   */
  public String getId() {
    return id;
  }

  /**
   * Retrieve the raw data record.
   * 
   * @return Raw data record.
   */
  public Map<String, Object> getRawData() {
    return Collections.unmodifiableMap(rawData);
  }

  /**
   * Retrieve the raw data source name, e.g a database table or database.
   * 
   * @return Raw data source name.
   */
  public String getSourceName() {
    return sourceName;
  }
}