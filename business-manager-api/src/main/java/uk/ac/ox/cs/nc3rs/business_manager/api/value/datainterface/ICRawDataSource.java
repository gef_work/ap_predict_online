/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICType;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;

/**
 * Interface to objects identifying the nature and source of inhibitory concentration data in raw
 * data records.
 * 
 * @author geoff
 */
public interface ICRawDataSource {

  /**
   * Retrieve the column name of the field representing the count of all available records.
   * 
   * @return Column name of field holding the count of all available records (or <tt>null</tt> if
   *         not specified).
   */
  String getAllRecordCountColumnName();

  /**
   * Retrieve the type of inhibitory concentration data specified.
   * 
   * @return The type of inhibitory concentration data being referenced.
   */
  ICType getICType();

  /**
   * Retrieve the name of the column which contains the modifier value.
   * 
   * @return Name of the column which contains the modifier value (or <tt>null</tt> if not
   *         specified).
   */
  String getModifierColumnName();

  /**
   * Retrieve the column name of the field representing the count of records used in calculation.
   * 
   * @return Column name of the field holding the count of the records used in the calculation
   *         (or <tt>null</tt> if not specified).
   */
  String getRecordCountColumnName();

  /**
   * Retrieve the name of the column which contains the standard error data.
   * 
   * @return Name of the column which contains the standard error data (or <tt>null</tt> if not
   *         specified).
   */
  String getStdErrColumnName();

  /**
   * Retrieve the name of the column which contains the IC unit data.
   * <p>
   * <b>Important 1!</b><br>
   * If <tt>null</tt> is entered then the default {@linkplain Unit} for the specified
   * {@linkplain ICType} will be used.
   * </p>
   * <p>
   * <b>Important 2!</b><br>
   * If a <tt>null</tt> or empty value is returned from the raw data in the specified column then
   * the default {@linkplain Unit} will be used. If a value is retrieved but it is not recognized
   * the program will fail.
   * </p>
   * 
   * @return Name of the column which contains the IC unit data, or <tt>null</tt> to use default
   *         type.
   */
  String getUnitColumnName();

  /**
   * Retrieve the name of the column which contains the inhibitory concentration value.
   * 
   * @return Name of the column which contains the inhibitory concentration value.
   */
  String getValueColumnName();
}