/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.business;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ExperimentalDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.QSARDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening.AggregatedSiteDataVO;

/**
 * Temporary class used in the generation of statistical results for Gary! 
 *
 * @author geoff
 */
public class StaticDataHolder {

  public static final Map<String, QSARDataHolder> QSAR_DATA = new HashMap<String, QSARDataHolder>();
  public static final Map<String, AggregatedSiteDataVO> SCREENING_DATA = new HashMap<String, AggregatedSiteDataVO>();
  public static final Map<String, ExperimentalDataHolder> EXPERIMENTAL_DATA = new HashMap<String, ExperimentalDataHolder>();

  public enum DATA_TYPE {
    EXPERIMENTAL,
    QSAR,
    SCREENING
  }

  private static final Log log = LogFactory.getLog(StaticDataHolder.class);

  public static void load(final String compoundIdentifier, final DATA_TYPE dataType,
                          final Object object) {
    log.info("~load() : '" + compoundIdentifier + "' '" + dataType + "'.");
    switch (dataType) {
      case EXPERIMENTAL :
        EXPERIMENTAL_DATA.put(compoundIdentifier, (ExperimentalDataHolder) object);
        break;
      case QSAR :
        QSAR_DATA.put(compoundIdentifier, (QSARDataHolder) object);
        break;
      case SCREENING :
        SCREENING_DATA.put(compoundIdentifier, (AggregatedSiteDataVO) object);
        break;
      default :
        log.error("~load() : Unrecognized DATA_TYPE of '" + dataType + "'.");
        break;
    }
  }

  public static void remove(final String compoundIdentifier) {
    log.info("~remove() : Removing '" + compoundIdentifier + "' from loaded static data.");
    if (SCREENING_DATA.containsKey(compoundIdentifier)) {
      SCREENING_DATA.remove(compoundIdentifier);
    }
    if (QSAR_DATA.containsKey(compoundIdentifier)) {
      QSAR_DATA.remove(compoundIdentifier);
    }
    if (EXPERIMENTAL_DATA.containsKey(compoundIdentifier)) {
      EXPERIMENTAL_DATA.remove(compoundIdentifier);
    }
  }

  public static boolean contains(final String compoundIdentifier, final DATA_TYPE dataType) {
    boolean contains = false;
    switch (dataType) {
      case EXPERIMENTAL :
        contains = EXPERIMENTAL_DATA.containsKey(compoundIdentifier);
        break;
      case QSAR :
        contains = QSAR_DATA.containsKey(compoundIdentifier);
        break;
      case SCREENING :
        contains = SCREENING_DATA.containsKey(compoundIdentifier);
        break;
      default :
        log.error("~contails() : Unrecognized DATA_TYPE of '" + dataType + "'.");
        break;
    }

    return contains;
  }

  public static Object retrieve(final String compoundIdentifier, final DATA_TYPE dataType) {
    Object object = null;
    switch (dataType) {
      case EXPERIMENTAL :
        object = EXPERIMENTAL_DATA.get(compoundIdentifier);
        break;
      case QSAR :
        object = QSAR_DATA.get(compoundIdentifier);
        break;
      case SCREENING :
        object = SCREENING_DATA.get(compoundIdentifier);
        break;
      default :
        log.error("~retrieve() : Unrecognized DATA_TYPE of '" + dataType + "'.");
        break;
    }

    log.info("~retrieve() : Retrieving '" + compoundIdentifier + "', '" + dataType + "'.");

    return object;
  }
}
