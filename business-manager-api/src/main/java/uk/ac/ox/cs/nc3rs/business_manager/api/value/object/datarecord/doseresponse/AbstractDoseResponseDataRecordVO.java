/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.datarecord.doseresponse;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.datarecord.AbstractGenericDataRecordVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.doseresponse.DoseResponsePairVO;

/**
 * Abstract value object representing a Dose-Response record.
 * 
 * @author Geoff Williams
 */
public abstract class AbstractDoseResponseDataRecordVO extends AbstractGenericDataRecordVO
                                                       implements DoseResponseDataRecord {

  private static final Log log = LogFactory.getLog(AbstractDoseResponseDataRecordVO.class);

  /**
   * Initialising constructor.
   * 
   * @param siteId Site-specific identifier (or null if none available).
   * @param sourceName Name of the data source (required).
   * @param rawData Raw data record (required).
   * @throws IllegalArgumentException If required arguments are not supplied or if the argument
   *                                  validation fails.
   * @throws InvalidValueException If an invalid value is encountered.
   */
  public AbstractDoseResponseDataRecordVO(final String siteId, final String sourceName,
                                          final Map<String, Object> rawData)
                                          throws IllegalArgumentException, InvalidValueException {
    super(siteId, sourceName, rawData);
    postConstructionValidation();
  }

  // check the validity of data which has been assigned.
  protected void postConstructionValidation() throws InvalidValueException {
    if (getRawData() == null) {
      // This will never be hit if the superclass constructor insists on raw data being assigned.
      log.debug("Null raw dose-response data : " + getSourceName() + " - Assuming assignment as irregular data!");
    } else if (getRawData().isEmpty()) {
      // An empty raw data collection suggests a programming error.
      throw new IllegalArgumentException("Illegal empty raw dose-response data record assigned to constructor");
    } else {
      final DoseResponsePairVO doseResponsePoint = eligibleDoseResponseData();
      if (doseResponsePoint == null) {
        log.info("~postConstructionValidation() : No eligible dose-response point in '" + getRawData() + "'.");
      }
    }
  }
}