/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.type;

/**
 * Problems which are encountered during processing. The problem keys are references to bundle 
 * message keys in the client interface but this is not a prerequisite at problems may be internal
 * and not necessarily broadcast via the client interface.
 * and need not be broadcast!
 *
 * @author geoff
 */
public enum ProblemType {
  /* api_b? === Business,
     api_s === Site Business,
     api_ws? === Web Service */
  DATABASE_INTERACTION_SITE("progress.api_b1", "Problems interacting with the site database."),
  DUPLICATED_DOSE_IN_DOSE_RESPONSES("progress.api_b2", "Processing of dose-responses generated duplicated doses."),
  INDETERMINABLE_ASSAY("progress.api_b3", "Not possible to determine the assay type."),
  INDETERMINABLE_ION_CHANNEL("progress.api_b4", "Not possible to determine the ion channel type."),
  INDIVIDUAL_BUT_NO_SUMMARY_RAW_DATA("progress.api_b5", "Individual raw data records yet no parent summary raw data record."),
  INVALID_VALUE("progress.api_b6", "Invalid value encountered."),
  UNATTRIBUTABLE_DOSE_RESPONSES("progress.api_b7", "Not possible to attribute dose response records to an individual record."),
  NO_DOSE_RESPONSES_MATCHED("progress.api_b8", "Dose-response records available, but none matched for an individual record."),
  NO_DOSE_RESPONSES("progress.api_b9", "No dose-response records found."),
  INDETERMINABLE_50_PER_CENT_INHIB_CONC("progress.api_b10", "Indeterminable 50% inhibitory concentration."),
  CONFLICTING_REQUEST("progress.api_b11", "The received request contained conflicting entries."),
  INVALID_COMPOUND_NAME("progress.api_ws1", "Invalid compound name(s) encountered."),
  INVALID_SIMULATION_ID("progress.api_ws2", "Invalid simulation id(s) encountered."),
  SITE_SPECIFIC_PROBLEM("progress.api_s", "Site problem");

  private final String messageKey;
  private final String description;

  /**
   * Initialising constructor.
   * 
   * @param messageKey Corresponds to a message key in client interface bundle.
   * @param description Description.
   */
  ProblemType(final String messageKey, final String description) {
    this.messageKey = messageKey;
    this.description = description;
  }

  /**
   * Retrieve the client interface bundle message key.
   * 
   * @return Client interface bundle message key.
   */
  public String getMessageKey() {
    return messageKey;
  }

  /**
   * Retrieve informative description of the problem (for use outside of client interface).
   * 
   * @return Problem description.
   */
  public String getDescription() {
    return description;
  }
}