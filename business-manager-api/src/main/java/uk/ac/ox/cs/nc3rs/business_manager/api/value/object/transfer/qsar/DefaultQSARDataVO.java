/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.qsar;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.QSARDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.AbstractProblemTransferVO;

/**
 * Default QSAR data holder.
 *
 * @author geoff
 */
public class DefaultQSARDataVO extends AbstractProblemTransferVO implements QSARDataHolder {

  private final AssayVO assay;
  private final Set<DataPacket> dataPackets = new HashSet<DataPacket>();

  private static final transient Log log = LogFactory.getLog(DefaultQSARDataVO.class);

  /**
   * Initialising constructor.
   * 
   * @param assay Assay (required).
   * @param dataPackets Per-channel pIC50 data (or null/empty collection if no data available).
   * @param problems Any problems encountered while acquiring data.
   * @throws IllegalArgumentException If required arguments not assigned.
   */
  public DefaultQSARDataVO(final AssayVO assay, final Set<DataPacket> dataPackets,
                           final List<String> problems) throws IllegalArgumentException {
    super(problems);
    if (assay == null) {
      final String errorMessage = "Invalid null in assay argument";
      log.warn("~DefaultQSARDataVO() : " + errorMessage);
      throw new IllegalArgumentException(errorMessage);
    }
    this.assay = assay;
    if (dataPackets != null) {
      this.dataPackets.addAll(dataPackets);
    }
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "DefaultQSARDataVO [assay=" + assay + ", dataPackets=" + dataPackets
        + ", problems=" + problems + "]";
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.transfer.qsar.QSARDataHolder#containsData()
   */
  public boolean containsData() {
    return (!dataPackets.isEmpty());
  }

  /**
   * @return the assay
   */
  public AssayVO getAssay() {
    return assay;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.transfer.qsar.QSARDataHolder#getDataPackets()
   */
  public Set<DataPacket> getDataPackets() {
    return Collections.unmodifiableSet(dataPackets);
  }
}