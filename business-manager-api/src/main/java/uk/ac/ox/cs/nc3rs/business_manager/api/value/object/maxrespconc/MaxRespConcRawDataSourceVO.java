/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.maxrespconc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;

/**
 * Value object for holding details of the raw data columns of max response and concentration data
 * and the units of concentration.
 *
 * @author geoff
 */
public class MaxRespConcRawDataSourceVO {

  private final String maxRespColumnName;
  private final String maxRespConcColumnName;
  private final Unit maxRespConcUnit;

  private static final Log log = LogFactory.getLog(MaxRespConcRawDataSourceVO.class);

  /**
   * Initialising constructor.
   * 
   * @param maxRespColumnName Column name of raw data's max response data (required).
   * @param maxRespConcColumnName Column name of raw data's max response concentration data (required).
   * @param maxRespConcUnit Unit of max response concentration, e.g. M, uM (required).
   * @throws IllegalArgumentException If any required argument is a <code>null</code> value.
   */
  public MaxRespConcRawDataSourceVO(final String maxRespColumnName, final String maxRespConcColumnName,
                                    final Unit maxRespConcUnit) {
    if (maxRespColumnName == null || maxRespConcColumnName == null || maxRespConcUnit == null) {
      final String errorMessage = "Illegal null value in maxRespColumnName '" + maxRespColumnName + 
                                  "', maxRespConcColumnName '" + maxRespConcColumnName + 
                                  "', maxRespConcUnit '" + maxRespConcUnit + "'";
      log.error("~MaxRespConcRawDataSourceVO() : " + errorMessage);
      throw new IllegalArgumentException(errorMessage);
    }

    this.maxRespColumnName = maxRespColumnName;
    this.maxRespConcColumnName = maxRespConcColumnName;
    this.maxRespConcUnit = maxRespConcUnit;
  }

  /**
   * @return the maxRespColumnName
   */
  public String getMaxRespColumnName() {
    return maxRespColumnName;
  }

  /**
   * @return the maxRespConcColumnName
   */
  public String getMaxRespConcColumnName() {
    return maxRespConcColumnName;
  }

  /**
   * @return the maxRespConcUnit
   */
  public Unit getMaxRespConcUnit() {
    return maxRespConcUnit;
  }
}