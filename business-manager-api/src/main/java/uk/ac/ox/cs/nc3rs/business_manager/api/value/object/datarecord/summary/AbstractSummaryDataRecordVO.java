/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.datarecord.summary;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.datarecord.AbstractGenericDataRecordVO;

/**
 * Abstract value object representing a summary data record.
 * 
 * @author Geoff Williams
 */
public abstract class AbstractSummaryDataRecordVO extends AbstractGenericDataRecordVO
                                                  implements SummaryDataRecord {

  private static final Log log = LogFactory.getLog(AbstractSummaryDataRecordVO.class);

  /**
   * Initialising constructor.
   * <p>
   * The {@code siteId} arg <b>MUST</b> be a genuinely unique record in the system, i.e. no more
   * than one summary data record is allowed to have this identifier.
   * 
   * @param siteId <b>Unique</b> site-specific identifier. If none is available pass a {@code null} 
   *               (and a random {@link java.util.UUID} will be used internally).
   * @param sourceName Name of the source of the data.
   * @param rawData Raw data record .
   * @throws IllegalArgumentException If required arguments are not supplied or if the argument
   *                                  validation fails.
   * @throws InvalidValueException If an invalid value is encountered.
   */
  public AbstractSummaryDataRecordVO(final String siteId, final String sourceName,
                                     final Map<String, Object> rawData)
                                     throws IllegalArgumentException, InvalidValueException {
    super(siteId, sourceName, rawData);
    postConstructionValidation();
  }

  // check the validity of data which has been assigned.
  protected void postConstructionValidation() throws InvalidValueException {
    if (getRawData() == null || getRawData().isEmpty()) {
      // TODO : Not aware of a valid reason for this happening.
      // An empty raw data collection suggests a programming error.
      final String message = "Invalid use of null or empty raw summary data for '" + getSourceName() + "'.";
      System.out.println(message);
      log.error("~AbstractSummaryDataRecordVO() : " + message);
      throw new IllegalArgumentException(message);
    } else {
      if (eligibleICData().isEmpty()) {
        // Note : This is a valid outcome as some tables don't hold sufficient data.
        log.info("~postConstructionValidation() : No eligible IC data in '" + getRawData() + "'.");
      }
    }
  }
}