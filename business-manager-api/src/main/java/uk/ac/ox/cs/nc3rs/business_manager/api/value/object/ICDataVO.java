/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.NoSuchDataException;
import uk.ac.ox.cs.nc3rs.business_manager.api.util.ConverterUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Modifier;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICType;

/**
 * Value object holding the IC20 / pIC20 and/or IC50 / pIC50 / pXC50 values
 * <p>
 * These objects are used to hold the IC20 / pIC20 and/or IC50 / pIC50 / pXC50 data found in
 * individual and summary records.<br>
 * There may be more than one of these possible for a particular data record as there may
 * be both a pIC50 value (inc. modifier) and an IC50 value (inc. modifier) recorded - however only
 * one can be used for workflow processing and so it must be selected in a site-specific manner
 * when the data is read.
 *
 * @author geoff
 */
public class ICDataVO implements ICData, Serializable {

  private static Map<ICType, List<Unit>> unitCheck = new HashMap<ICType, List<Unit>>();

  private static final long serialVersionUID = 8746361445057883543L;

  static {
    for (final ICType icType : ICType.values()) {
      unitCheck.put(icType, Arrays.asList(icType.getOptionalUnits()));
    }
  }

  private interface ExposedICData {
    BigDecimal originalICValue();
    Unit originalUnit();
    String originalModifier();
    Integer originalRecordCount();
    Integer originalAllRecordCount();
    BigDecimal originalStdErr();
    ICType originalType();

    // This is the modifier direction when considering the ICType
    boolean effectivePCGreaterModifier() throws IllegalStateException;
    Modifier systemModifier();
    Modifier effectivePCModifier();
    /**
     * Is the data originally from an IC value rather than a pIC value?
     * 
     * @return True if IC20/50, otherwise false.
     */
    boolean isICSource();
    boolean isModifierEquals();
    boolean isPotentialDose(BigDecimal dose);

    String originalInfo();

    BigDecimal equivalentPC50Value();
    BigDecimal equivalentIC50uMValue();
  }

  private abstract class AbstractICData implements ExposedICData {
    private final BigDecimal icValue;
    private final String icValueColumnName;
    private final Unit unit;
    private final String modifier;
    private final String modifierColumnName;
    private final BigDecimal stdErr;
    private final String stdErrColumnName;
    private final Integer recordCount;
    private final String recordCountColumnName;
    private final Integer allRecordCount;
    private final String allRecordCountColumnName;
    private final ICType icType;

    /**
     * Initialising constructor.
     * <p>
     * Note: Log information not written here - it should be handled in the calling code.
     * 
     * @param icValue Inhibitory concentration value.
     * @param icValueColumnName Inhibitory concentration column name.
     * @param unit Inhibitory concentration unit.
     * @param modifier Modifier.
     * @param modifierColumnName Modifier column name.
     * @param stdErr Standard error.
     * @param stdErrColumnName Standard error column name.
     * @param recordCount Record count.
     * @param recordCountColumnName Record count column name.
     * @param allRecordCount Count of all records.
     * @param allRecordCountColumnName All record count column name.
     * @param icType Inhibitory concentration type.
     * @throws IllegalArgumentException If {@linkplain #icValue}, {@linkplain #icValueColumnName},
     *                                  {@linkplain #unit} is {@code null}, or a value is not-{@code null}
     *                                  where a corresponding column name is {@code null}.
     * @throws InvalidValueException If {@linkplain #icValue} is negative.
     */
    protected AbstractICData(final BigDecimal icValue, final String icValueColumnName,
                             final Unit unit,
                             final String modifier, final String modifierColumnName,
                             final BigDecimal stdErr, final String stdErrColumnName,
                             final Integer recordCount, final String recordCountColumnName,
                             final Integer allRecordCount, final String allRecordCountColumnName,
                             final ICType icType) throws IllegalArgumentException,
                                                         InvalidValueException {
      // Type is usually the first item tested for as otherwise it's unknown which subclass to instantiate!
      assert (icType != null) : "Invalid null ICType encountered during ICData construction!";

      if (icValue == null) {
        throw new IllegalArgumentException("An inhibitory concentration value must be provided as constructor arg!");
      }
      if (icValueColumnName == null) {
        throw new IllegalArgumentException("Invalid null value in valueColumnName!");
      }
      if (unit == null) {
        throw new IllegalArgumentException("The unit of IC must be specified!");
      }
      if ((recordCount != null && recordCountColumnName == null) ||
          (allRecordCount != null && allRecordCountColumnName == null) ||
          (stdErr != null && stdErrColumnName == null)) {
        throw new IllegalArgumentException("Values are being read in when no corresponding column names defined");
      }

      if (icType.compareTo(ICType.IC20) == 0 || icType.compareTo(ICType.IC50) == 0) {
        final boolean hasNegativeValue = icValue.compareTo(BigDecimal.ZERO) == -1;
        if (hasNegativeValue) {
          throw new InvalidValueException(new Object[] { "Cannot accept a negative value of '" + icValue.toPlainString() + "' for IC type '" + icType + "'." });
        }
      }

      this.icValue = icValue;
      this.icValueColumnName = icValueColumnName;
      this.unit = unit;
      this.modifier = modifier == null ? null : modifier.trim();
      this.modifierColumnName = modifierColumnName;
      this.stdErr = stdErr;
      this.stdErrColumnName = stdErrColumnName;
      this.recordCount = recordCount;
      this.recordCountColumnName = recordCountColumnName;
      this.allRecordCount = allRecordCount;
      this.allRecordCountColumnName = allRecordCountColumnName;
      this.icType = icType;

      // Do some validation.
      if (!unitCheck.get(icType).contains(unit)) {
        throw new IllegalArgumentException("IC type '" + icType + "' can not be assigned a unit of '" + unit + "'!");
      }
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#originalAllRecordCount()
     */
    @Override
    public Integer originalAllRecordCount() {
      return allRecordCount;
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#effectiveGreaterModifier()
     */
    @Override
    public boolean effectivePCGreaterModifier() throws IllegalStateException {
      if (isModifierEquals()) {
        throw new IllegalStateException("Method call not allowed when equality modifier assigned");
      }
      return ((isICSource() && Modifier.LESS.isIdentifier(modifier)) ||
              (!isICSource() && Modifier.GREATER.isIdentifier(modifier)));
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#systemModifier()
     */
    @Override
    public Modifier systemModifier() {
      Modifier effectiveModifier = null;
      if (isModifierEquals()) {
        effectiveModifier = Modifier.EQUALS;
      } else if (Modifier.GREATER.isIdentifier(modifier)) {
        effectiveModifier = Modifier.GREATER;
      } else if (Modifier.LESS.isIdentifier(modifier)) {
        effectiveModifier = Modifier.LESS;
      } else {
        final String errorMessage = "Could not determine modifier based on value '" + modifier + "'.";
        log.error(errorMessage);
        throw new IllegalStateException(errorMessage);
      }
      return effectiveModifier;
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#effectivePCModifier()
     */
    @Override
    public Modifier effectivePCModifier() {
      Modifier effectivePCModifier = null;
      if (isModifierEquals()) {
        effectivePCModifier = Modifier.EQUALS;
      } else if (effectivePCGreaterModifier()) {
        effectivePCModifier = Modifier.GREATER;
      } else {
        effectivePCModifier = Modifier.LESS;
      }
      return effectivePCModifier;
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#originalICValue()
     */
    @Override
    public BigDecimal originalICValue() {
      return icValue;
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#originalUnit()
     */
    @Override
    public Unit originalUnit() {
      return unit;
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#originalModifier()
     */
    @Override
    public String originalModifier() {
      return modifier;
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#originalRecordCount()
     */
    @Override
    public Integer originalRecordCount() {
      return recordCount;
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#originalStdErr()
     */
    @Override
    public BigDecimal originalStdErr() {
      return stdErr;
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#originalType()
     */
    @Override
    public ICType originalType() {
      return icType;
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#isModifierEquals()
     */
    @Override
    public boolean isModifierEquals() {
      return (modifier == null || Modifier.EQUALS.isIdentifier(modifier));
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#originalInfo()
     */
    @Override
    public String originalInfo() {
      final List<String> originalInfo = new ArrayList<String>();
      if (icValueColumnName != null) {
        originalInfo.add(icValueColumnName + "='" + icValue + "'");
      }
      if (modifierColumnName != null) {
        originalInfo.add(modifierColumnName + "='" + modifier + "'");
      }
      if (stdErrColumnName != null) {
        originalInfo.add(stdErrColumnName + "='" + stdErr + "'");
      }
      if (allRecordCountColumnName != null) {
        originalInfo.add(allRecordCountColumnName + "='" + allRecordCount + "'");
      }
      if (recordCountColumnName != null) {
        originalInfo.add(recordCountColumnName + "='" + recordCount + "'");
      }

      return StringUtils.join(originalInfo, ", ");
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
      return "AbstractICData [icValue=" + icValue + ", icValueColumnName="
          + icValueColumnName + ", unit=" + unit + ", modifier=" + modifier
          + ", modifierColumnName=" + modifierColumnName + ", stdErr=" + stdErr
          + ", stdErrColumnName=" + stdErrColumnName + ", recordCount="
          + recordCount + ", recordCountColumnName=" + recordCountColumnName
          + ", allRecordCount=" + allRecordCount
          + ", allRecordCountColumnName=" + allRecordCountColumnName
          + ", icType=" + icType + "]";
    }

    /**
     * @return the modifier
     */
    protected String getModifier() {
      return modifier;
    }
  }

  // The class used to hold IC20 or pIC20.
  private class Pct20Data extends AbstractICData {
    /**
     * @see AbstractICData#AbstractICData(BigDecimal, String, Unit, String, String, BigDecimal, String, Integer, String, Integer, String, ICType)
     */
    protected Pct20Data(final BigDecimal value, final String valueColumnName,
                        final Unit unit,
                        final String modifier, final String modifierColumnName,
                        final BigDecimal stdErr, final String stdErrColumnName,
                        final Integer recordCount, final String recordCountColumnName,
                        final Integer allRecordCount, final String allRecordCountColumnName,
                        final ICType icType) throws IllegalArgumentException,
                                                    InvalidValueException {
      super(value, valueColumnName, unit, modifier, modifierColumnName, stdErr, stdErrColumnName,
            recordCount, recordCountColumnName, allRecordCount, allRecordCountColumnName, icType);
      assert(is20Pct(icType)) : "Cannot assign a non-20% inhibitory concentration type to a Pct20Data object!";
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#isICSource()
     */
    @Override
    public boolean isICSource() {
      return originalType().compareTo(ICType.IC20) == 0;
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#isPotentialDose2(java.math.BigDecimal)
     */
    @Override
    public boolean isPotentialDose(final BigDecimal dose) {
      throw new UnsupportedOperationException("Operation not yet implemented");
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#equivalentPC50Value()
     */
    @Override
    public BigDecimal equivalentPC50Value() {
      // TODO Auto-generated method stub
      throw new UnsupportedOperationException("Operation not yet implemented");
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#equivalentIC50uMValue()
     */
    @Override
    public BigDecimal equivalentIC50uMValue() {
      // TODO Auto-generated method stub
      throw new UnsupportedOperationException("Operation not yet implemented");
    }
  }

  // The class used to hold IC50 or pIC50.
  private class Pct50Data extends AbstractICData {
    /**
     * @see AbstractICData#AbstractICData(BigDecimal, String, Unit, String, String, BigDecimal, String, Integer, String, Integer, String, ICType)
     */
    protected Pct50Data(final BigDecimal value, final String valueColumnName,
                        final Unit unit,
                        final String modifier, final String modifierColumnName,
                        final BigDecimal stdErr, final String stdErrColumnName,
                        final Integer recordCount, final String recordCountColumnName,
                        final Integer allRecordCount, final String allRecordCountColumnName,
                        final ICType icType) throws IllegalArgumentException,
                                                    InvalidValueException {
      super(value, valueColumnName, unit, modifier, modifierColumnName, stdErr, stdErrColumnName,
            recordCount, recordCountColumnName, allRecordCount, allRecordCountColumnName, icType);
      assert(!is20Pct(icType)) : "Cannot assign 20% inhibitory concentration data to Pct50Data!";
    }

    // Return if it was an IC50 (as opposed to a pIC50, IC20, etc., value).
    private boolean isIC50Source() {
      return (originalType().compareTo(ICType.IC50) == 0);
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#isICSource()
     */
    @Override
    public boolean isICSource() {
      return (originalType().compareTo(ICType.IC50) == 0);
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#isPotentialDose2(java.math.BigDecimal)
     */
    @Override
    public boolean isPotentialDose(final BigDecimal dose) {
      final boolean doseGreater = dose.compareTo(equivalentIC50uMValue()) > 0;
      final boolean modifierGreater = Modifier.GREATER.isIdentifier(getModifier());
      if ((isICSource() && modifierGreater) || (!isICSource() && !modifierGreater)) {
        // if (IC50&&> || pIC50&&<) && (dose > IC50 equiv) return true!
        return doseGreater ? true : false;
      } else {
        // if (IC50&&< || pIC50&&>) && (dose > IC50 equiv) return false!
        return doseGreater ? false : true;
      }
    }


    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#equivalentPC50Value()
     */
    @Override
    public BigDecimal equivalentPC50Value() {
      BigDecimal equivalentPC50Value = null;
      try {
        equivalentPC50Value = ConverterUtil.convert(originalType(), originalUnit(), ICType.PIC50,
                                                    Unit.minusLogM, originalICValue());
      } catch (InvalidValueException e) {
        log.error("~equivalentPC50Value() + '" + e.getMessage() + "'.");
      }

      return equivalentPC50Value;
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO.ExposedICData#equivalentIC50uMValue()
     */
    @Override
    public BigDecimal equivalentIC50uMValue() {
      BigDecimal equivalentIC50uMValue = null;
      try {
        equivalentIC50uMValue = ConverterUtil.convert(originalType(), originalUnit(), ICType.IC50, Unit.uM,
                                                      originalICValue());
      } catch (InvalidValueException e) {
        log.error("~equivalentIC50Value() + '" + e.getMessage() + "'.");
      }

      return equivalentIC50uMValue;
    }
  }

  private final Pct20Data pct20Data;
  private final Pct50Data pct50Data;

  private static final transient Log log = LogFactory.getLog(ICDataVO.class);

  /**
   * Create a dual-valued ICData object from a 50 and a 20 percent inhibition object.
   * 
   * @param ic50Data 50% inhibitory concentration object.
   * @param ic20Data 20% inhibitory concentration object.
   */
  public ICDataVO(final ICData ic50Data, final ICData ic20Data) {
    if (ic50Data == null || ic20Data == null) {
      throw new IllegalArgumentException("A 50 and/or a 20 percent inhibitory concentration object are required (1)!");
    }

    // Need to verify that the ICData objects do have the expected content!
    final Pct50Data pct50Data = ((ICDataVO) ic50Data).getPct50Data();
    final Pct20Data pct20Data = ((ICDataVO) ic20Data).getPct20Data();

    if (pct50Data == null || pct20Data == null) {
      throw new IllegalArgumentException("A 50 and/or a 20 percent inhibitory concentration object are required (2)!");
    }

    this.pct50Data = pct50Data;
    this.pct20Data = pct20Data;
  }

  /**
   * Minimum assignment initialising constructor.
   * 
   * @param value Table record value for IC20 / IC50 / pIC50 / pXC50.
   * @param valueColumnName Table column name from which value derived.
   * @param unit Unit of inhibitory concentration value, e.g. uM, -log(M).
   * @param modifier Table record value for corresponding IC20 / IC50 / etc., modifier. (Optional).
   * @param modifierColumnName Table column name from which modifier derived. (Optional).
   * @param icType Inhibitory concentration type, i.e. IC50, pXC50.
   * @throws IllegalArgumentException If any required value has a {@code null} value.
   * @throws InvalidValueException If a negative IC50 value. 
   */
  public ICDataVO(final BigDecimal value, final String valueColumnName,
                  final Unit unit,
                  final String modifier, final String modifierColumnName,
                  final ICType icType) throws IllegalArgumentException, InvalidValueException {
    this(value, valueColumnName, unit, modifier, modifierColumnName, null, null, null, null, null,
         null, icType);
  }

  /**
   * Construction of an ICDataVO holding only one of the possible two inhibitory concentration
   * data values.
   * 
   * @param value Table record value for IC50 / pIC50 / pXC50.
   * @param valueColumnName Table column name from which value derived.
   * @param unit Unit of inhibitory concentration value, e.g. uM, -log(M).
   * @param modifier Table record value for modifier, or {@code null} if not available.
   * @param modifierColumnName Table column name from which modifier derived, or {@code null} if not
   *                           available.
   * @param recordCount Count of the number of records from which the c50Value was derived, or
   *                    {@code null} if not available.
   * @param recordCountColumnName Table column name of the c50RecordCount value, or {@code null} if
   *                              not available.
   * @param allRecordCount Count of the number of records available for c50Value calculation, or
   *                      {@code null} if not available.
   * @param allRecordCountColumnName Table column name of the allRecordCount value, or{@code null}
   *                                 if not available.
   * @param stdErr Table record value for standard error, or {@code null} if not available.
   * @param stdErrColumnName Table column name of the standard error value, or {@code null} if not
   *                         available.
   * @param icType Inhibitory concentration type, i.e. IC50, pXC50.
   * @throws IllegalArgumentException If any required value has a {@code null} value.
   * @throws InvalidValueException If a negative IC20 or IC50 value.
   * 
   * @see Pct20Data#Pct20Data(BigDecimal, String, Unit, String, String, BigDecimal, String, Integer, String, Integer, String, ICType)
   * @see Pct50Data#Pct50Data(BigDecimal, String, Unit, String, String, BigDecimal, String, Integer, String, Integer, String, ICType)
   */
  public ICDataVO(final BigDecimal value, final String valueColumnName,
                  final Unit unit,
                  final String modifier, final String modifierColumnName,
                  final Integer recordCount, final String recordCountColumnName,
                  final Integer allRecordCount, final String allRecordCountColumnName,
                  final BigDecimal stdErr, final String stdErrColumnName,
                  final ICType icType) throws IllegalArgumentException, InvalidValueException {

    if (icType == null) {
      final String errorMessage = "Invalid null in ICType";
      log.error("~ICDataVO() : " + errorMessage);
      throw new IllegalArgumentException(errorMessage);
    }

    switch (icType) {
      case IC20 :
      case PIC20 :
        this.pct50Data = null;
        this.pct20Data = new Pct20Data(value, valueColumnName, unit, modifier, modifierColumnName,
                                       stdErr, stdErrColumnName, recordCount, recordCountColumnName,
                                       allRecordCount, allRecordCountColumnName, icType);
        break;
      case IC50 :
      case PIC50 :
      case PXC50 :
        this.pct20Data = null;
        this.pct50Data = new Pct50Data(value, valueColumnName, unit, modifier, modifierColumnName,
                                       stdErr, stdErrColumnName, recordCount, recordCountColumnName,
                                       allRecordCount, allRecordCountColumnName, icType);
        break;
      default : 
        throw new IllegalArgumentException("Unknown inhibitory concentration type of '" + icType + "' encountered!");
    }
  }

  /**
   * Initialising constructor which allows two different (they must differ in their inhibitory
   * concentration percentage) inhibitory concentration data to be assigned. 
   * <p>
   * Note :
   * <ul>
   *   <li>
   *     A {@code null} {@code aValue} or {@code bValue} may be derived from the original data but
   *     it will be rejected as it would render {@linkplain ICDataVO} creation futile.
   *   </li>
   *   <li>
   *     A {@code null} {@code aModifier} or {@code bModifier} will be interpreted to represent an
   *     equality modifier.
   *   </li>
   *   <li>
   *     A {@code null} column name field or {@code aICType} or {@code bICType} is symptomatic of a
   *     programming error.
   *   </li>
   * </ul>
   * 
   * @param aValue Table record value for IC20 / IC50 / pIC50 / pXC50.
   * @param aValueColumnName Table column name from which {@code aValue} derived.
   * @param aUnit Inhibitory concentration unit, e.g. uM, M.
   * @param aModifier Table record value for modifier, or {@code null} if not available.
   * @param aModifierColumnName Table column name from which {@code aModifier} derived, or 
   *                            {@code null} if not available.
   * @param aRecordCount Count of the number of records from which the {@code aValue} was derived,
   *                     or {@code null} if not available.
   * @param aRecordCountColumnName Table column name of the {@code aRecordCount} value, or
   *                               {@code null} if not available.
   * @param aAllRecordCount Count of the number of records available for {@code aValue} calculation,
   *                        or {@code null} if not available.
   * @param aAllRecordCountColumnName Table column name of the {@code aAllRecordCount} value, or
   *                                  {@code null} if not available.
   * @param aStdErr Table record value for standard error, or {@code null} if not available.
   * @param aStdErrColumnName Table column name of the {@code aStdErr} value, or {@code null} if
   *                          not available.
   * @param aICType Inhibitory concentration type, i.e. IC50, pXC50.
   * @param bValue Table record value for IC20 / IC50 / pIC50 / pXC50.
   * @param bValueColumnName Table column name from which {@code bValue} derived.
   * @param bUnit Inhibitory concentration unit, e.g. uM, M.
   * @param bModifier Table record value for modifier, or {@code null} if not available.
   * @param bModifierColumnName Table column name from which {@code bModifier} derived, or {@code null}
   *                            if not available.
   * @param bRecordCount Count of the number of records from which the {@code bValue} was derived,
   *                     or {@code null} if not available.
   * @param bRecordCountColumnName Table column name of the {@code bRecordCount} value, or
   *                               {@code null} if not available.
   * @param bAllRecordCount Count of the number of records available for {@code bValue} calculation,
   *                        or {@code null} if not available.
   * @param bAllRecordCountColumnName Table column name of the {@code bAllRecordCount} value, or
   *                                  {@code null} if not available.
   * @param bStdErr Table record value for standard error, or {@code null} if not available.
   * @param bStdErrColumnName Table column name of the {@code bStdErr} value, or {@code null} if
   *                          not available.
   * @param bICType Inhibitory concentration type, i.e. IC50, pXC50.
   * @throws IllegalArgumentException If any required value has a {@code null} value.
   * @throws InvalidValueException If a negative IC20 or IC50 value.
   */
  public ICDataVO(final BigDecimal aValue, final String aValueColumnName,
                  final Unit aUnit,
                  final String aModifier, final String aModifierColumnName,
                  final Integer aRecordCount, final String aRecordCountColumnName,
                  final Integer aAllRecordCount, final String aAllRecordCountColumnName,
                  final BigDecimal aStdErr, final String aStdErrColumnName,
                  final ICType aICType,
                  final BigDecimal bValue, final String bValueColumnName,
                  final Unit bUnit,
                  final String bModifier, final String bModifierColumnName,
                  final Integer bRecordCount, final String bRecordCountColumnName,
                  final Integer bAllRecordCount, final String bAllRecordCountColumnName,
                  final BigDecimal bStdErr, final String bStdErrColumnName,
                  final ICType bICType) throws IllegalArgumentException, InvalidValueException {
    final boolean aDeclaration = (aICType != null);
    final boolean bDeclaration = (bICType != null);

    if (!aDeclaration && !bDeclaration) {
      final String errorMessage = "No valid inhibitory concentration value(s) provided!";
      log.error("~ICDataVO() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    if (aDeclaration && bDeclaration) {
      final boolean aIsPct20 = is20Pct(aICType);
      final boolean bIsPct20 = is20Pct(bICType);

      if ((aIsPct20 && bIsPct20) || (!aIsPct20 && !bIsPct20)) {
        final String errorMessage = "Cannot define two inhibitory concentration values for the same percentage (i.e. 2 x IC20)!";
        log.error("~ICDataVO() : ".concat(errorMessage));
        throw new IllegalArgumentException(errorMessage);
      }
    }

    Pct20Data pct20Data = null;
    if (aDeclaration && is20Pct(aICType)) {
      pct20Data = new Pct20Data(aValue, aValueColumnName, aUnit, aModifier, aModifierColumnName,
                                aStdErr, aStdErrColumnName, aRecordCount,
                                aRecordCountColumnName, aAllRecordCount,
                                aAllRecordCountColumnName, aICType);
    }
    if (bDeclaration && is20Pct(bICType)) {
      pct20Data = new Pct20Data(bValue, bValueColumnName, bUnit, bModifier, bModifierColumnName,
                                bStdErr, bStdErrColumnName, bRecordCount,
                                bRecordCountColumnName, bAllRecordCount,
                                bAllRecordCountColumnName, bICType);
    }
    this.pct20Data = pct20Data;

    Pct50Data pct50Data = null;
    if (aDeclaration && !is20Pct(aICType)) {
      pct50Data = new Pct50Data(aValue, aValueColumnName, aUnit, aModifier, aModifierColumnName,
                                aStdErr, aStdErrColumnName, aRecordCount,
                                aRecordCountColumnName, aAllRecordCount,
                                aAllRecordCountColumnName, aICType);
    }
    if (bDeclaration && !is20Pct(bICType)) {
      pct50Data = new Pct50Data(bValue, bValueColumnName, bUnit, bModifier, bModifierColumnName,
                                bStdErr, bStdErrColumnName, bRecordCount,
                                bRecordCountColumnName, bAllRecordCount,
                                bAllRecordCountColumnName, bICType);
    }
    this.pct50Data = pct50Data;
  }

  private boolean is20Pct(final ICType icType) {
    return (icType.compareTo(ICType.IC20) == 0 || icType.compareTo(ICType.PIC20) == 0);
  }

  private Pct20Data getPct20Data() {
    return pct20Data;
  }

  private Pct50Data getPct50Data() {
    return pct50Data;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData#hasICPctData(uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent)
   */
  @Override
  public boolean hasICPctData(final ICPercent pctIC) {
    boolean hasData = false;
    try {
      hasData = (retrieveICData(pctIC) != null);
    } catch (NoSuchDataException e) {}
    return hasData;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData#hasIC50Source()
   */
  @Override
  public boolean hasIC50Source() {
    return (pct50Data != null && pct50Data.isIC50Source());
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData#hasPC50Source()
   */
  @Override
  public boolean hasPC50Source() {
    return (pct50Data != null && (pct50Data.originalType().compareTo(ICType.PIC50) == 0 ||
                                  pct50Data.originalType().compareTo(ICType.PXC50) == 0));
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData#isEquivalentPC50Negative(uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData.ICPercent)
   */
  @Override
  public boolean isEquivalentPC50Negative(ICPercent pctIC) throws IllegalArgumentException,
                                                                  NoSuchDataException {
    return (retrieveEquivalentPC50Value(pctIC).compareTo(BigDecimal.ZERO) == -1);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData#isModifierEquals(uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData.ICPercent)
   */
  @Override
  public boolean isModifierEquals(final ICPercent pctIC) throws NoSuchDataException {
    return retrieveICData(pctIC).isModifierEquals();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData#isPotentialDose(uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData.ICPercent, java.math.BigDecimal)
   */
  @Override
  public boolean isPotentialDose(ICPercent pctIC, BigDecimal dose) throws NoSuchDataException {
    return (retrieveICData(pctIC).isPotentialDose(dose));
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData#retrieveCountOfAvailableRecords(uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData.ICPercent)
   */
  @Override
  public Integer retrieveCountOfAvailableRecords(final ICPercent pctIC) throws NoSuchDataException {
    return retrieveICData(pctIC).originalAllRecordCount();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData#retrieveCountOfRecordsUsedToDetermineICValue(uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData.ICPercent)
   */
  @Override
  public Integer retrieveCountOfRecordsUsedToDetermineICValue(final ICPercent pctIC) throws NoSuchDataException {
    return retrieveICData(pctIC).originalRecordCount();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData#retrieveOriginalICValue(uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData.ICPercent)
   */
  @Override
  public BigDecimal retrieveOriginalICValue(final ICPercent pctIC) throws NoSuchDataException {
    return retrieveICData(pctIC).originalICValue();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData#retrieveOriginalInfo(uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData.ICPercent)
   */
  @Override
  public String retrieveOriginalInfo(final ICPercent pctIC) throws NoSuchDataException {
    return retrieveICData(pctIC).originalInfo();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData#retrieveOriginalStdErr(uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData.ICPercent)
   */
  @Override
  public BigDecimal retrieveOriginalStdErr(final ICPercent pctIC) throws NoSuchDataException {
    return retrieveICData(pctIC).originalStdErr();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData#isPCComparisonEffectiveGreaterModifier(uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData.ICPercent)
   */
  @Override
  public boolean isPCComparisonEffectiveGreaterModifier(final ICPercent pctIC)
                                                        throws IllegalStateException,
                                                               NoSuchDataException {
    return retrieveICData(pctIC).effectivePCGreaterModifier();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData#retrieveEffectivePCModifier(uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData.ICPercent)
   */
  @Override
  public Modifier retrieveEffectivePCModifier(final ICPercent pctIC) throws NoSuchDataException {
    return retrieveICData(pctIC).effectivePCModifier();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData#retrieveEquivalentIC50Value(uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData.ICPercent)
   */
  @Override
  public BigDecimal retrieveEquivalentIC50Value(final ICPercent pctIC) throws NoSuchDataException {
    return retrieveICData(pctIC).equivalentIC50uMValue();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData#retrieveEquivalentPC50Value(uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData.ICPercent)
   */
  @Override
  public BigDecimal retrieveEquivalentPC50Value(final ICPercent pctIC) throws IllegalArgumentException,
                                                                              NoSuchDataException {
    return retrieveICData(pctIC).equivalentPC50Value();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData#retrievePC50BasedOnIC20(java.math.BigDecimal)
   */
  @Override
  public BigDecimal retrievePC50BasedOnIC20(final BigDecimal hillCoefficient) {
    BigDecimal pct50pIC = null;

    if (pct20Data == null) {
      return pct50pIC;
    }
    assert (pct20Data.originalICValue() != null) : "Invalid 20% inhibition data object found with no inhibitory concentration value!";

    try {
      BigDecimal pct20IC = pct20Data.originalICValue();
      final Unit pct20Unit = pct20Data.originalUnit();
      if (Unit.uM.compareTo(pct20Unit) != 0) {
        // Convert from non-uM to uM 20% inh. conc.
        pct20IC = ConverterUtil.convert(pct20Unit, Unit.uM, pct20IC);
      }

      pct50pIC = ConverterUtil.convertIC20ToPC50(pct20IC, hillCoefficient);
    } catch (IllegalArgumentException e) {
    } catch (InvalidValueException e) {
    }

    return pct50pIC;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData#retrieveTranslatedModifier(uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent)
   */
  @Override
  public Modifier retrieveTranslatedModifier(final ICPercent pctIC) throws NoSuchDataException {
    return retrieveICData(pctIC).systemModifier();
  }

  protected ExposedICData retrieveICData(final ICPercent pctIC) throws NoSuchDataException {
    AbstractICData icData = null;
    switch (pctIC) {
      case PCT20 :
        if (pct20Data == null) {
          throw new NoSuchDataException(new Object[] { "pct20" });
        }
        icData = pct20Data;
        break;
      case PCT50 :
        if (pct50Data == null) {
          throw new NoSuchDataException(new Object[] { "pct50" });
        }
        icData = pct50Data;
        break;
      default :
        throw new UnsupportedOperationException("Unrecognized inhibitory concentration type of '" + pctIC + "' encountered!");
    }
    return icData;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "ICDataVO [pct20Data=" + pct20Data + ", pct50Data=" + pct50Data + "]";
  }
}