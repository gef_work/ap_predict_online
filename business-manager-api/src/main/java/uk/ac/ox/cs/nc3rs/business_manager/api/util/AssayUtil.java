/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.util;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;

/**
 * Utility class for Assay processing.
 * 
 * @author geoff
 */
public class AssayUtil {

  /** Consistent name for the Ionworks Barracuda assay */
  public static final String ASSAY_NAME_BARRACUDA = "Barracuda";
  /** Consistent name for the Manual Patch */
  public static final String ASSAY_NAME_MANUAL_PATCH = "Manual Patch";
  /** Consistent name for the PatchXpress assay */
  public static final String ASSAY_NAME_PATCHXPRESS = "PatchXpress";
  /** Consistent name for the Qpatch assay */
  public static final String ASSAY_NAME_QPATCH = "Qpatch";
  /** Consistent name for the Quantitative Structure-Activity Relationship model assay */
  public static final String ASSAY_NAME_QSAR = "QSAR";
  /** Consistent name for the Ionworks Quattro / FLIPR assay */
  public static final String ASSAY_NAME_QUATTRO_FLIPR = "Quattro_FLIPR";

  // This class may be extended by a site-specific assay utility class.

  /**
   * Retrieve indicator of assay being Ionworks Barracuda.
   * 
   * @param assay Assay being queried.
   * @return True if Ionworks Barracuda assay.
   */
  public static boolean isAssayBarracuda(final AssayVO assay) {
    return (ASSAY_NAME_BARRACUDA.equalsIgnoreCase(assay.getName()));
  }

  /**
   * Retrieve indicator of assay being Manual Patch.
   * 
   * @param assay Assay being queried.
   * @return True if PatchXpress assay.
   */
  public static boolean isAssayManualPatch(final AssayVO assay) {
    return (ASSAY_NAME_MANUAL_PATCH.equalsIgnoreCase(assay.getName()));
  }

  /**
   * Retrieve indicator of assay being PatchXpress.
   * 
   * @param assay Assay being queried.
   * @return True if PatchXpress assay.
   */
  public static boolean isAssayPatchXpress(final AssayVO assay) {
    return (ASSAY_NAME_PATCHXPRESS.equalsIgnoreCase(assay.getName()));
  }

  /**
   * Retrieve indicator of assay being Qpatch.
   * 
   * @param assay Assay being queried.
   * @return True if Qpatch assay.
   */
  public static boolean isAssayQpatch(final AssayVO assay) {
    return (ASSAY_NAME_QPATCH.equalsIgnoreCase(assay.getName()));
  }

  /**
   * Retrieve indicator of assay being QSAR.
   * 
   * @param assay Assay being queried.
   * @return True if QSAR assay.
   */
  public static boolean isAssayQSAR(final AssayVO assay) {
    return (ASSAY_NAME_QSAR.equalsIgnoreCase(assay.getName()));
  }

  /**
   * Retrieve indicator of assay being Ionworks Quattro / FLIPR.
   * 
   * @param assay Assay being queried.
   * @return True if Ionworks Quattro / FLIPR assay.
   */
  public static boolean isAssayQuattroFLIPR(final AssayVO assay) {
    return (ASSAY_NAME_QUATTRO_FLIPR.equalsIgnoreCase(assay.getName()));
  }
}