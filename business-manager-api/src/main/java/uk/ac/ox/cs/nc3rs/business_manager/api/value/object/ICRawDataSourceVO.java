/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICRawDataSource;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICType;

/**
 * Value object for holding details of the raw data columns and type of inhibitory concentration
 * data.
 *
 * @author geoff
 */
public class ICRawDataSourceVO implements ICRawDataSource {
  private final String valueColumnName;
  private final String unitColumnName;
  private final String modifierColumnName;
  private final String recordCountColumnName;
  private final String allRecordCountColumnName;
  private final String stdErrColumnName;
  private final ICType icType;

  /**
   * Initialising constructor.
   * 
   * @param valueColumnName Raw data column name of the inhibitory concentration value.
   * @param unitColumnName Raw data column name of the inhibitory concentration unit, or
   *                       <tt>null</tt> if using default unit for IC type
   *                       ({@link ICType#getDefaultUnit()}).
   * @param modifierColumnName Raw data column name of the modifier.
   * @param recordCountColumnName Raw data column name of the record count used in inhibitory 
   *                              concentration value calculation, or <tt>null</tt> if not available.
   * @param allRecordCountColumnName Raw data column name of the all available record count, or 
   *                                 <tt>null</tt> if not available.
   * @param stdErrColumnName Raw data column name of the std error value, or <tt>null</tt> if not
   *                         available.
   * @param icType Inhibitory concentration type.
   * @throws IllegalArgumentException If the value column name is <tt>null</tt>.
   */
  public ICRawDataSourceVO(final String valueColumnName, final String unitColumnName,
                           final String modifierColumnName, final String recordCountColumnName,
                           final String allRecordCountColumnName, final String stdErrColumnName,
                           final ICType icType) throws IllegalArgumentException {
    if (valueColumnName == null) {
      throw new IllegalArgumentException("The raw data column name of the inhibitory concentration data must be specified.");
    }
    this.valueColumnName = valueColumnName;
    this.unitColumnName = unitColumnName;
    this.modifierColumnName = modifierColumnName;
    this.recordCountColumnName = recordCountColumnName;
    this.allRecordCountColumnName = allRecordCountColumnName;
    this.stdErrColumnName = stdErrColumnName;
    this.icType = icType;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "ICRawDataSourceVO [valueColumnName=" + valueColumnName
        + ", unitColumnName=" + unitColumnName + ", modifierColumnName="
        + modifierColumnName + ", recordCountColumnName="
        + recordCountColumnName + ", allRecordCountColumnName="
        + allRecordCountColumnName + ", stdErrColumnName=" + stdErrColumnName
        + ", icType=" + icType + "]";
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICRawDataSource#getAllRecordCountColumnName()
   */
  @Override
  public String getAllRecordCountColumnName() {
    return allRecordCountColumnName;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICRawDataSource#getICType()
   */
  @Override
  public ICType getICType() {
    return icType;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICRawDataSource#getModifierColumnName()
   */
  @Override
  public String getModifierColumnName() {
    return modifierColumnName;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICRawDataSource#getRecordCountColumnName()
   */
  @Override
  public String getRecordCountColumnName() {
    return recordCountColumnName;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICRawDataSource#getStdErrColumnName()
   */
  @Override
  public String getStdErrColumnName() {
    return stdErrColumnName;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICRawDataSource#getUnitColumnName()
   */
  @Override
  public String getUnitColumnName() {
    return unitColumnName;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICRawDataSource#getValueColumnName()
   */
  @Override
  public String getValueColumnName() {
    return valueColumnName;
  }
}