/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.summary;

import java.math.BigDecimal;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;

/**
 * Interface for all strategy objects which retrieve the PC50 for a compound/assay/ion channel
 * entity based on the summary data when there is no non-summary data.
 * <p>
 * In this situation it's anticipated that the summary record would hold a mean or median value of 
 * the PC50 (and Hill Coefficient) derived from an indicated quantity of individual records. What
 * the processing is likely to do is to create the corresponding number of individual PC50/Hill
 * values to pass to ApPredict.
 *
 * @author geoff
 */
public interface SummaryOnlyC50DataStrategy {

  /**
   * Evaluate the PC50 derived from site data where there is <b>only</b> summary data available,
   * i.e. no non-summary data.  <b>If the site data has non-summary data then the PC50 evaluation
   * MUST return a <code>null</code> value</b>.
   * 
   * @param siteDataHolder Site data.
   * @param recordIdentifier System record identifier from site data.
   * @return PC50, or <code>null</code> if indeterminable, e.g. if non-summary data is present.
   * @throws IllegalArgumentException If a <code>null</code> arg is passed.
   * @throws InvalidValueException If an invalid value is encountered.
   */
  BigDecimal evaluatePC50(SiteDataHolder siteDataHolder, String recordIdentifier)
                          throws IllegalArgumentException, InvalidValueException;

}