/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Interface implemented by objects representing the result of the Dose-Response fitting processing.
 * 
 * @author geoff
 */
public interface DoseResponseFittingResult extends Serializable {

  /**
   * @return the error
   */
  String getError();

  /**
   * @return the hillCoefficient
   */
  BigDecimal getHillCoefficient();

  /**
   * Retrieve the IC50 value.
   * 
   * @return IC50 value (in uM).
   */
  BigDecimal getIC50();

  /**
   * Get the pIC50 / pXC50 equivalent of the IC50 uM dose-response fitting.
   * 
   * @return pIC50 / pXC50 equivalent of the IC50 uM dose-response fitting (or null if the IC50
   *         value was 0).
   * @see #getPC50ValueShortened()
   */
  BigDecimal getPC50Value();

  /**
   * Get a shortened (3 decimal places and rounded floor) pIC50 / pXC50 equivalent of the IC50 uM
   * dose-response fitting.
   * 
   * @return Shortened value of pIC50 / pXC50.
   * @see #getPC50Value()
   */
  String getPC50ValueShortened();
}