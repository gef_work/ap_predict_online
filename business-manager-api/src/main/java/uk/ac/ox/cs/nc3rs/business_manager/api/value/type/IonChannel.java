/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.type;

/**
 * Eligible ion channel values.
 * 
 * @author Geoff Williams
 */
public enum IonChannel {
  // DisplayOrder numbers are 0-based.
  CaV1_2("Calcium 1.2", (short) 1),
  NaV1_5("Sodium 1.5", (short) 2),
  KCNQ1("KCNQ1", (short) 3),
  hERG("hERG", (short) 0);

  // Textual description
  private final String description;
  private final Short displayOrder;

  // Initialising constructor
  private IonChannel(final String description, final Short displayOrder) {
    this.description = description;
    this.displayOrder = displayOrder;
  }

  /**
   * Retrieve the description of this enum value.
   * 
   * @return Description of enum value.
   */
  public String getDescription() {
    return this.description;
  }

  /**
   * Retrieve the default display order of the ion channel.<br>
   * 0-based, whereby 0 represents the first column.
   * 
   * @return Default display order.
   */
  public short getDisplayOrder() {
    return displayOrder;
  }
}