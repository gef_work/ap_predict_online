/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder;

import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.ModelAsInactiveStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.QualityControlFailureVO;

/**
 * Extended by interfaces, e.g. {@link IndividualDataRecord}, which potentially contain data 
 * recorded in the database as marked as either invalid or inactive following internal, 
 * pre-persistence processing and/or quality controls.
 * <p>
 *
 * @author geoff
 */
public interface MarkedDataHolder {

  /**
   * Indicate if the data has been marked as invalid by a site's internal quality controls.
   * <p>
   * Records marked as invalid are likely to be removed from processing by 
   * <tt>QCOfficer#removeQCFailedData</tt> (and the user notified). Records with data not specifically
   * marked as invalid yet which contain invalid data, e.g. recorded erroneously, need to be handled
   * by site-specific {@linkplain PC50EvaluationStrategy} implementations.
   * <p>
   * Please note that this check could equally be applied when site data is being read in in site-
   * specific processing and not subsequently returned to the business manager for processing!
   * 
   * @return Object if the data labeled invalid, otherwise null if valid.
   * @throws InvalidValueException If invalid data is encountered.
   */
  QualityControlFailureVO isMarkedInvalid() throws InvalidValueException;

  /**
   * Indicate if the compound has been marked inactive by a site's internal quality controls.
   * <p>
   * If data is to be deemed inactive as a result of some user-defined portal processing then that
   * should probably be defined by a {@link ModelAsInactiveStrategy} other than, e.g.
   * <tt>DataMarkedAsInactive</tt>.
   * 
   * @return Object if the compound labeled inactive, otherwise null.
   * @throws InvalidValueException If invalid data is encountered.
   */
  QualityControlFailureVO isMarkedInactive() throws InvalidValueException;

}