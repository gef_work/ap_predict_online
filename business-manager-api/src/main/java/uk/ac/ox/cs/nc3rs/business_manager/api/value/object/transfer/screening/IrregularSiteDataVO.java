/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.IrregularSiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ProblemVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 * Irregular site data value object.
 * 
 * @author Geoff Williams
 */
public class IrregularSiteDataVO extends AbstractGenericSiteDataVO
                                 implements IrregularSiteDataHolder {

  private final List<ProblemVO> problems = new ArrayList<ProblemVO>();

  /**
   * Initialising constructor.
   * <p>
   * There must be either summary or individual data or both.
   * 
   * @param assay Assay (optionally null if cannot be determined).
   * @param ionChannel Ion Channel (optionally null if cannot be determined).
   * @param summaryDataRecord Summary data record (Optionally null if only individual data available).
   * @param nonSummaryDataRecords Non-summary (i.e. individual and (if appropriate) dose-response) data records.
   * @param problems Collection of problems.
   * @throws IllegalArgumentException See {@link AbstractGenericSiteDataVO#AbstractGenericSiteDataVO(AssayVO, IonChannel, SummaryDataRecord, Map, boolean)}
   */
  public IrregularSiteDataVO(final AssayVO assay, final IonChannel ionChannel,
                             final SummaryDataRecord summaryDataRecord,
                             final Map<IndividualDataRecord, List<DoseResponseDataRecord>> nonSummaryDataRecords,
                             final List<ProblemVO> problems)
                             throws IllegalArgumentException {
    super(assay, ionChannel, summaryDataRecord, nonSummaryDataRecords, true);
    if (problems != null)
      this.problems.addAll(problems);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "IrregularSiteDataVO [problems=" + problems + "]";
  }


  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.site_data.IrregularSiteData#getProblems()
   */
  public List<ProblemVO> getProblems() {
    return Collections.unmodifiableList(problems);
  }
}