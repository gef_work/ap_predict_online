/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder;

import java.util.List;

import uk.ac.ox.cs.nc3rs.business_manager.api.business.selection.ic.ICDataSelectionStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;

/**
 * Interface which all records holding percent inhibitory concentration data (e.g. IC20, IC50,
 * pIC50, pXC50) implement.
 *
 * @author geoff
 */
public interface ICDataHolder {

  /**
   * Return the eligible IC20 / IC50 / pIC50 / pXC50 values available for use. For example, a
   * summary data record may have more than one field which represent the compound's block, so a
   * collection of {@link ICData} objects can be extracted and selected from.
   * <p>
   * It is expected but not mandated that the eligible collection is ordered by priority, such that
   * the first element, e.g. pIC50 data, is the preferred source of inhibitory concentration data.
   * 
   * @return Eligible field values, or empty collection if none available.
   * @throws InvalidValueException If an invalid value is encountered.
   * @see ICDataSelectionStrategy (and C50DataSelector in business manager)
   */
  List<ICData> eligibleICData() throws InvalidValueException;
}