/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.experimental;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.transfer.experimental.ExperimentalResult;

/**
 * Experimental results for specified pacing frequency.
 *
 * @author geoff
 */
public class DefaultExperimentalResultVO implements ExperimentalResult {

  private final BigDecimal pacingFrequency;
  private final List<ConcQTPctChangeValuesVO> concQTPctChangeValues = new ArrayList<ConcQTPctChangeValuesVO>();

  private static final transient Log log = LogFactory.getLog(DefaultExperimentalResultVO.class);

  /**
   * Initialising constructor.
   * 
   * @param pacingFrequency Experimental pacing frequency (in Hz) (required).
   * @param concQTPctChangeValues Experimental results - QT pct changes values per concentration.
   * @throws IllegalArgumentException If required argument(s) are not provided.
   */
  public DefaultExperimentalResultVO(final BigDecimal pacingFrequency,
                                     final List<ConcQTPctChangeValuesVO> concQTPctChangeValues)
                                     throws IllegalArgumentException {
    if (pacingFrequency == null) {
      final String errorMessage = "Invalid null in pacingFrequency argument";
      log.warn("~DefaultExperimentalResultVO() : " + errorMessage);
      throw new IllegalArgumentException(errorMessage);
    }
    this.pacingFrequency = pacingFrequency;
    if (concQTPctChangeValues != null) {
      this.concQTPctChangeValues.addAll(concQTPctChangeValues);
    }
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "DefaultExperimentalResultVO [pacingFrequency=" + pacingFrequency
        + ", concQTPctChangeValues=" + concQTPctChangeValues + "]";
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.transfer.experimental.ExperimentalResult#getPacingFrequency()
   */
  @Override
  public BigDecimal getPacingFrequency() {
    return pacingFrequency;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.transfer.experimental.ExperimentalResult#getConcentrationQTPctChangeValues()
   */
  @Override
  public List<ConcQTPctChangeValuesVO> getConcentrationQTPctChangeValues() {
    return Collections.unmodifiableList(concQTPctChangeValues);
  }
}