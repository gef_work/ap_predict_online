/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICType;

/**
 * General utilities for converting things.
 *
 * @author Geoff Williams
 */
public class ConverterUtil {

  public static final BigDecimal QUARTER = new BigDecimal("0.25");
  public static final BigDecimal FOUR = new BigDecimal("4");
  /** One billion */
  public static final BigDecimal BILLION = new BigDecimal("1000000000");
  /** One million */
  public static final BigDecimal MILLION = new BigDecimal("1000000");
  public static final BigDecimal MINUS1 = new BigDecimal("-1");
  /** One thousand */
  public static final BigDecimal THOUSAND = new BigDecimal("1000");

  private static final Log log = LogFactory.getLog(ConverterUtil.class);

  // Prevent instantiation.
  private ConverterUtil() {}

  /**
   * Retrieve a shortened, textual representation of a {@link BigDecimal} value using the
   * default decimal place length (of 3).
   * 
   * @param value BigDecimal value.
   * @return Shortened textual representation, or {@code null} if {@code null} provided.
   */
  public static String bdShortened(final BigDecimal value) {
    return bdShortened(value, 3);
  }

  /**
   * Retrieve a shortened, textual representation of a {@link BigDecimal} value using the
   * default decimal place length.
   * 
   * @param value BigDecimal value.
   * @param decimalPlaces Number of decimal places to show.
   * @return Shortened textual representation, or {@code null} if {@code null} provided.
   */
  public static String bdShortened(final BigDecimal value, final int decimalPlaces) {
    String shortened = null;
    if (value != null) {
      shortened = value.setScale(decimalPlaces, RoundingMode.FLOOR).toPlainString();
    }
    return shortened;
  }

  /**
   * Convert an IC20 uM inhibitory concentration to an IC50 uM inhibitory concentration.
   * 
   * @param ic20 IC20 inhibitory concentration <b>in µM</b>.
   * @param hill Hill Coefficient, or {@code null} if defaulting to 1.
   * @return Corresponding pIC50 value.
   * @throws IllegalArgumentException If a {@code null} ic20 value, or 0 (zero) Hill is passed
   * @throws InvalidValueException {@link #convert(ICType, Unit, ICType, Unit, BigDecimal)}
   */
  public static BigDecimal convertIC20ToPC50(final BigDecimal ic20, final BigDecimal hill)
                                             throws IllegalArgumentException, InvalidValueException {
    log.debug("~convertIC20ToPC50() : Invoked for '" + ic20 + "', '" + hill + "'.");
    if (ic20 == null) {
      throw new IllegalArgumentException("Invalid null value received for IC20 uM value!");
    }

    BigDecimal ic50 = null;
    if (hill == null || hill.compareTo(BigDecimal.ONE) == 0) {
      ic50 =  ic20.multiply(FOUR);
    } else {
      if (hill.compareTo(BigDecimal.ZERO) == 0) {
        throw new IllegalArgumentException("Inappropriate 0 (zero) value received for Hill Coefficient!");
      }
      final int bigEnoughScale = 20;                              // TODO: Is this scale big enough?
      final double power = (BigDecimal.ONE.divide(hill, bigEnoughScale,
                                                  RoundingMode.HALF_UP)).doubleValue();
      final BigDecimal divisor = new BigDecimal(Math.pow(QUARTER.doubleValue(), power));
      ic50 = ic20.divide(divisor, bigEnoughScale, RoundingMode.HALF_UP);
    }

    final BigDecimal pIC50 = convert(ICType.IC50, Unit.uM, ICType.PIC50, Unit.minusLogM, ic50);

    return pIC50;
  }

  /**
   * Convert value from a particular percentage inhibition type, e.g. {@link ICType#IC50} (and
   * optional unit, e.g. {@link Unit#uM}), to a different {@linkplain ICType#PIC50} (and
   * optional unit).
   * <p>
   * <b>Important!</b>
   * <ol>
   *   <li>
   *     The conversion may involve {@link java.lang.Math} double-precision calculations and
   *     this is may introduce a loss of accuracy.
   *   </li>
   *   <li>
   *     Conversion from IC20 to something else assumes that Hill Coefficient is 1 (one).
   *   </li>
   * </ol>
   * 
   * @param fromICType From inhibitory concentration type, e.g. IC20.
   * @param fromUnit From value unit, e.g. uM.
   * @param toICType To inhibitory concentration type, e.g. pIC50.
   * @param toUnit To value unit.
   * @param fromValue Value to convert.
   * @return Converted value.
   * @throws InvalidValueException Negative IC50 or if number format exception generated somehow.
   * @throws IllegalArgumentException If a {@code null} argument encountered, or if converting an
   *                                  IC50 value of 0 (zero) to something else.
   */
  public static BigDecimal convert(final ICType fromICType, final Unit fromUnit,
                                   final ICType toICType, final Unit toUnit,
                                   final BigDecimal fromValue) throws InvalidValueException,
                                                                      IllegalArgumentException {
    log.debug("~convert() : Invoked.");

    if (fromValue == null) {
      return null;
    }

    if (fromICType == null || fromUnit == null || toICType == null || toUnit == null) {
      final String errorMessage = "Invalid null value in " +
                                  "fromICType '" + fromICType + "', fromUnit '" + fromUnit + "', " +
                                  "toICType '" + toICType + "', toUnit '" + toUnit + "' of value '" +
                                  fromValue + "'";
      log.error("~convert() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    // Check for no conversion necessary.
    if (fromICType.compareTo(toICType) == 0 && fromUnit.compareTo(toUnit) == 0) {
      return fromValue;
    }

    BigDecimal toValue = null;

    final String text = fromICType + " (" + fromUnit + ") to " + toICType + " (" + toUnit + ") conversion not implemented!";

    switch (fromICType) {
      case IC20 :
        switch (toICType) {
          case PIC50 :
            // These assume that the Hill Coefficient is 1!
            switch (fromUnit) {
              case nM :
              case M :
                final BigDecimal ic20uM = convert(fromUnit, Unit.uM, fromValue);
                toValue = convertIC20ToPC50(ic20uM, null);
                break;
              case uM :
                toValue = convertIC20ToPC50(fromValue, null);
                break;
              default :
                throw new UnsupportedOperationException(text);
            }
            break;
          default :
            throw new UnsupportedOperationException(text);
        }
        break;
      case IC50 :
        assert(fromUnit.compareTo(Unit.minusLogM) != 0) : "IC50 don't have a -log(M) unit!";
        switch (toICType) {
          case PIC50 :
          case PXC50 :
            switch (fromUnit) {
              case M :
                // IC50 (M) -> p{I,X}C50 (-log(M))
                throw new UnsupportedOperationException(text);
              case uM :
                // IC50 (µM) -> p{I,X}C50 (-log(M))
                if (BigDecimal.ZERO.compareTo(fromValue) == 0) {
                  // It is valid to have an IC50 of 0 if responses all above 50%!
                  final String infoMessage = "Cannot convert an IC50 of 0 (zero) to a pIC50 value";
                  log.warn("~convert() : " + infoMessage);
                  throw new IllegalArgumentException(infoMessage);
                } else if (BigDecimal.ZERO.compareTo(fromValue) > 0) {
                  // From value is below zero...
                  final Object[] nature = new Object[] { "Invalid negative IC50 value of '" + fromValue + "' encountered!" };
                  throw new InvalidValueException(nature);
                } else {
                  final Double multiple = Math.log10((convert(Unit.uM, Unit.M, fromValue)).doubleValue());
                  try {
                    toValue = MINUS1.multiply(new BigDecimal(multiple));
                  } catch (NumberFormatException e) {
                    final String errorMessage = "Illegal multiple value of '" + multiple + "' generated from IC50 '" + fromValue.toPlainString() + "'!";
                    log.error("~convert() : " + errorMessage);
                    final Object[] nature = new Object[] { errorMessage };
                    throw new InvalidValueException(nature);
                  }
                }
                log.debug("~convert() : IC50 (uM) '" + fromValue + "' converted to pI/XC50 '" + toValue + "'.");
                
                break;
              default:
                throw new UnsupportedOperationException(text);
            }
            break;
          default :
            throw new UnsupportedOperationException(text);
        }
        break;
      case PIC50 :
      case PXC50 :
        assert(fromUnit.compareTo(Unit.minusLogM) == 0) : "p{I,X}C50 expected units is -log(M)!";
        switch (toICType) {
          case IC50 :
            switch (toUnit) {
              case uM :
                // pIC50 (-log(M)) -> IC50 (µM)
                log.debug("~convert() : Invoked with pIC50 value '" + fromValue + "'");
                final Double multiple = Math.pow(10.0, (MINUS1.multiply(fromValue)).doubleValue());
                toValue = convert(Unit.M, Unit.uM, new BigDecimal(multiple));
                log.debug("~convert() : pIC50 '" + fromValue + "' converted to IC50 (uM) '" + toValue + "'.");
                break;
              default :
                throw new UnsupportedOperationException(text);
            }
            break;
          default :
            throw new UnsupportedOperationException(text);
        }
        break;
      default:
        throw new UnsupportedOperationException(text);
    }

    return toValue;
  }

  /**
   * Convert a value between two molarity units.
   * <p>
   * Note that a received null value will be generate a null return value.
   * 
   * @param fromUnit Unit to convert from.
   * @param toUnit Unit to convert to.
   * @param fromValue Value to convert.
   * @return Converted value.
   */
  public static BigDecimal convert(final Unit fromUnit, final Unit toUnit,
                                   final BigDecimal fromValue) {
    if (fromValue == null) return null;
    if (fromUnit.compareTo(toUnit) == 0) {
      return fromValue;
    }

    final String unimplementedMessage = "Conversion from '" + fromUnit + "' to '" + toUnit + "' is not currently implemented.";

    BigDecimal toValue = fromValue;
    switch (fromUnit) {
      case M :
        switch (toUnit) {
          case nM :
            toValue = BILLION.multiply(fromValue);
            break;
          case uM :
            toValue = MILLION.multiply(fromValue);
            break;
          default :
            throw new UnsupportedOperationException(unimplementedMessage);
        }
        break;
      case uM :
        switch (toUnit) {
          case M :
            toValue = fromValue.divide(MILLION);
            break;
          case nM :
            toValue = THOUSAND.multiply(fromValue);
            break;
          default :
            throw new UnsupportedOperationException(unimplementedMessage);
        }
        break;
      case nM :
        switch (toUnit) {
          case M :
            toValue = fromValue.divide(BILLION);
            break;
          case uM :
            toValue = fromValue.divide(THOUSAND);
            break;
          default :
            throw new UnsupportedOperationException(unimplementedMessage);
        }
        break;
      default :
        throw new UnsupportedOperationException(unimplementedMessage);
    }

    return toValue;
  }

  /**
   * Convert an object representing a number to a {@link BigDecimal} value.
   * 
   * @param object Object to convert.
   * @param loggingIdentifier Logging identifier (or null if not relevant).
   * @return BigDecimal value of object if possible, or null if null input.
   * @throws InvalidValueException If can't convert the object to a numeric value.
   */
  public static BigDecimal toBigDecimal(final Object object, final String loggingIdentifier)
                                        throws InvalidValueException {
    BigDecimal converted = null;
    if (object != null) {
      try {
        converted = new BigDecimal(object.toString());
      } catch (NumberFormatException e) {
        final String message = "Exception converting '" + loggingIdentifier + "' column value of '" + object.toString() + "' to a BigDecimal object.";
        log.warn("~toBigDecimal() : " + message);
        throw new InvalidValueException(new Object[] { message });
      }
    }
    return converted;
  }
}
