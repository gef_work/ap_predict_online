/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.AssaySpread;

/**
 * Value object representing an assay.
 * 
 * @author Geoff Williams
 */
public class AssayVO implements Serializable {

  private static final long serialVersionUID = -2107476747241337556L;

  private final String name;
  private final AssayGroupVO group;
  private final short level;
  private final Set<AssaySpread> spreads = new HashSet<AssaySpread>(); 

  private static final transient Log log = LogFactory.getLog(AssayVO.class);

  /**
   * Minimal initialising constructor.
   * 
   * @param name Assay name (required).
   * @param group Assay group, for grouping of individual assays (required).
   * @param level Assay level, for determining the assay hierarchy (required).
   * @throws IllegalArgumentException If any required argument is a <code>null</code> value.
   */
  public AssayVO(final String name, final AssayGroupVO group, final short level)
                 throws IllegalArgumentException {
    if (name == null || group == null) {
      final String errorMessage = "Invalid null in name '" + name + "', group '" + group + "'";
      log.warn("~AssayVO() : " + errorMessage);
      throw new IllegalArgumentException(errorMessage);
    }
    this.name = name;
    this.group = group;
    this.level = level;
  }

  /**
   * Initialising constructor with spread / confidence interval assignment.
   * 
   * @param name Assay name (required).
   * @param group Assay group, for grouping of individual assays (required).
   * @param level Assay level, for determining the assay hierarchy (required).
   * @param spreads Assay spreads / confidence intervals, or null if none available.
   * @throws IllegalArgumentException If any required argument is a <code>null</code> value.
   */
  public AssayVO(final String name, final AssayGroupVO group, final short level, 
                 final Set<AssaySpread> spreads) throws IllegalArgumentException {
    this(name, group, level);
    if (spreads != null) {
      this.spreads.addAll(spreads);
    }
  }

  /**
   * Indicator of whether the assay has been assigned confidence interval spread values.
   * 
   * @return True if confidence interval spread values assigned, otherwise false.
   */
  public boolean hasConfidenceIntervalSpreads() {
    return !spreads.isEmpty();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "AssayVO [name=" + name + ", group=" + group + ", level=" + level
        + ", spreads=" + spreads + "]";
  }

  /**
   * Retrieve the assay name.
   * 
   * @return Assay name.
   */
  public String getName() {
    return name;
  }

  /**
   * Retrieve the group number this assay is a member of.
   * 
   * @return Group number.
   */
  public AssayGroupVO getGroup() {
    return group;
  }

  /**
   * Retrieve the assay level this assay has in the assay hierarchy.
   * 
   * @return Assay level.
   */
  public short getLevel() {
    return level;
  }

  /**
   * Retrieve the assay variability spreads.
   * 
   * @return Assay variability spreads (or empty collection if none specified).
   */
  public Set<AssaySpread> getAssaySpreads() {
    return Collections.unmodifiableSet(spreads);
  }
}