/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.doseresponse;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.util.ConverterUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;

/**
 * Individual dose-response raw data pairing.
 * 
 * @author geoff
 */
public class DoseResponsePairVO implements Serializable {

  private static final long serialVersionUID = -4978224421869581852L;

  private final BigDecimal dose;
  private final BigDecimal response;

  private static transient final Log log = LogFactory.getLog(DoseResponsePairVO.class);

  /**
   * Initialising constructor.
   * 
   * @param dose Dose in uM (required).
   * @param response Response (%) (required).
   * @throws IllegalArgumentException If required values not provided, or dose is a negative value. 
   */
  public DoseResponsePairVO(final BigDecimal dose, final BigDecimal response)
                            throws IllegalArgumentException {
    log.debug("~DoseResponsePairVO() : Invoked with dose '" + dose + "', response '" + response + "'.");
    if (dose == null || response == null) {
      final String errorMessage = "Invalid null in dose '" + dose + "', response '" + response + "'";
      log.warn("~DoseResponsePairVO() : " + errorMessage);
      throw new IllegalArgumentException(errorMessage);
    }
    if (dose.compareTo(BigDecimal.ZERO) < 0) {
      final String errorMessage = "Invalid negative dose value of '" + dose + "'";
      log.warn("~DoseResponsePairVO() : " + errorMessage);
      throw new IllegalArgumentException(errorMessage);
    }
    if (response.compareTo(BigDecimal.ZERO) < 0) {
      // happens quite a lot
      final String debug = "Dose-response pairing had a -ve response of '" + response + "'.";
      log.debug("~DoseResponsePairVO() : " + debug);
    }
    this.dose = dose;
    this.response = response;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "DoseResponsePairVO [dose=" + dose + ", response=" + response
        + "]";
  }

  /**
   * Retrieve the dose <b>in the default units of uM</b>.
   * 
   * @return Dose in uM.
   */
  public BigDecimal getDose() {
    return dose;
  }

  /**
   * Retrieve the dose in the specified units.
   * 
   * @param unit Units for dose.
   * @return The dose.
   */
  public BigDecimal getDose(final Unit unit) {
    BigDecimal returnDose = null;
    switch (unit) {
      case M :
        returnDose = ConverterUtil.convert(Unit.uM, Unit.M, dose);
        break;
      case uM :
        returnDose = dose;
        break;
      default :
        throw new IllegalArgumentException("Unrecognised unit '" + unit + "' specified for conversion of dose data.");
    }
    return returnDose;
  }

  /**
   * @return the response
   */
  public BigDecimal getResponse() {
    return response;
  }
}