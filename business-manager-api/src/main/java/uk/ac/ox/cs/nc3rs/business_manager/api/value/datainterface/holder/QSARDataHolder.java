/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder;

import java.math.BigDecimal;
import java.util.Set;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 * Interface implemented by all object which transfer QSAR data from site-specific workflows to
 * the Business Manager workflow.
 *
 * @author geoff
 * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.qsar.DefaultQSARDataVO
 */
public interface QSARDataHolder {

  /**
   * Per-channel computed pIC50 data.
   *
   * @author geoff
   */
  public class DataPacket {
    private final IonChannel ionChannel;
    private final BigDecimal pIC50;

    /**
     * Initialising constructor.
     * 
     * @param ionChannel Non-null ion channel.
     * @param pIC50 Non-null pIC50.
     * @throws IllegalArgumentException If required values are assigned <code>null</code> values.
     */
    public DataPacket(final IonChannel ionChannel, final BigDecimal pIC50)
                      throws IllegalArgumentException {
      if (ionChannel == null || pIC50 == null) {
        throw new IllegalArgumentException("DataPacket constructor requires an ion channel and pIC50 value to be specified.");
      }
      this.ionChannel = ionChannel;
      this.pIC50 = pIC50;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
      return "DataPacket [ionChannel=" + ionChannel + ", pIC50=" + pIC50 + "]";
    }

    /** 
     * Retrieve the ion channel value.
     * 
     * @return Non-null ion channel value.
     */
    public IonChannel getIonChannel() {
      return ionChannel;
    }

    /** Retrieve the pIC50 value.
     * 
     * @return Non-null pIC50 value.
     */
    public BigDecimal getpIC50() {
      return pIC50;
    }
  }

  /**
   * Indicate whether the object contains QSAR data.
   * 
   * @return True if has QSAR data, otherwise false.
   */
  boolean containsData();

  /**
   * Assay type.
   * 
   * @return Assay type.
   */
  /*
   * Note: The nature of the QSAR assay is site-specific, as such this value is set in 
   *       site-specific code, not generic business manager code!
   */
  AssayVO getAssay();

  /**
   * Retrieve the channel pIC50s.
   * 
   * @return Channel pIC50s, or empty collection if none available.
   */
  Set<DataPacket> getDataPackets();
}