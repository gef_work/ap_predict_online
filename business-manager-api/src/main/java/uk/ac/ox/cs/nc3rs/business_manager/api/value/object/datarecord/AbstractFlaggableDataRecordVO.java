/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.datarecord;

import java.util.Map;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.FlaggableDataRecord;

/**
 * Data record for processing use to indicate if the raw data record is to flagged as "used" or not.
 *
 * @author geoff
 */
public abstract class AbstractFlaggableDataRecordVO extends AbstractGenericDataRecordVO
                                                    implements FlaggableDataRecord {

  private transient boolean used = false;

  /**
   * Initialising constructor. 
   * 
   * @param siteId Site-specific identifier (or null if none available).
   * @param sourceName Name of the table (or tables) of the data source.
   * @param rawData Raw data record.
   */
  public AbstractFlaggableDataRecordVO(final String siteId, final String sourceName,
                                       final Map<String, Object> rawData) {
    super(siteId, sourceName, rawData);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.FlaggableDataRecord#isUsed()
   */
  public boolean isUsed() {
    return (used);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.FlaggableDataRecord#resetUsedFlag()
   */
  public void resetUsedFlag() {
    used = false;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.FlaggableDataRecord#setAsUsed()
   */
  public void setAsUsed() {
    used = true;
  }
}