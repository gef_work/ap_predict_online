/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface;

import java.math.BigDecimal;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.NoSuchDataException;
import uk.ac.ox.cs.nc3rs.business_manager.api.util.ConverterUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Modifier;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICType;

/**
 * Interface to an inhibitory concentration data containing value object.
 * <p>
 * The implemented objects are expected to be derived from either summary or individual data.
 *
 * @see ICDataVO
 * @see ICRawDataSource
 * @author geoff
 */
public interface ICData {

  /**
   * Test to see if the data has inhibitory concentration data at the specified percentage, e.g
   * 20, 50.
   * 
   * @param pctIC Inhibitory concentration percentage.
   * @return True if data available for specified percentage, otherwise false.
   */
  boolean hasICPctData(ICPercent pctIC);

  /**
   * Does the data contain a 50% inhibitory concentration value derived from an
   * {@link ICType#IC50} value?
   * 
   * @return True if containing IC50 data, otherwise false;
   */
  boolean hasIC50Source();

  /**
   * Does the data contain a 50% inhibitory concentration value derived from an
   * {@link ICType#PIC50} or {@link ICType#PXC50} value?
   * 
   * @return True if containing pIC50 or pXC50 data, otherwise false;
   */
  boolean hasPC50Source();

  /**
   * Indicate if the original data has/implies a -ve pIC50 value.
   *
   * @param pctIC Inhibitory concentration percentage.
   * @return {@code true} if -ve pIC50, else {@code false}.
   * @throws IllegalArgumentException See {@link ConverterUtil#convert(ICType, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit, ICType, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit, BigDecimal)}
   * @throws NoSuchDataException If nonexistent requested percentage inhibitory concentration data.
   */
  boolean isEquivalentPC50Negative(ICPercent pctIC) throws IllegalArgumentException,
                                                           NoSuchDataException;

  /**
   * Does the original modifier equate to an {@link Modifier#EQUALS}.
   * 
   * @param pctIC Inhibitory concentration percentage.
   * @return Indicator of modifier being equivalent to {@link Modifier#EQUALS}.
   * @throws NoSuchDataException If nonexistent requested percentage inhibitory concentration data.
   */
  boolean isModifierEquals(ICPercent pctIC) throws NoSuchDataException;

  /**
   * Is the specified dose in agreement with the modifier and the original IC50 / pIC50 / pXC50
   * value, i.e. a ICData with an IC50 of 10uM (10^-5M) and a "&lt;" modifier would consider a dose of
   * 9uM ok, whereas a ICData with a pIC50 of 5 (=== 10uM) and a "&lt;" would consider a dose of 9uM
   * invalid. 
   * 
   * @param pctIC Inhibitory concentration percentage.
   * @param dose (expressed as IC50-equivalent uM value).
   * @return Indicator of whether inhibition can be considered.
   * @throws NoSuchDataException If nonexistent requested percentage inhibitory concentration data.
   */
  boolean isPotentialDose(ICPercent pctIC, BigDecimal dose) throws NoSuchDataException;

  /**
   * Retrieve the count of the total number of records available to potentially be used in the 
   * determination of the inhibitory concentration data value.<br>
   * This should generally be greater than or equal to the value returned by 
   * {@link #retrieveCountOfRecordsUsedToDetermineICValue(ICPercent)}.
   * 
   * @param pctIC Inhibitory concentration percentage.
   * @return Count of the number of records (or null if value not provided in raw data).
   * @throws NoSuchDataException If nonexistent requested percentage inhibitory concentration data.
   */
  Integer retrieveCountOfAvailableRecords(ICPercent pctIC) throws NoSuchDataException;

  /**
   * Retrieve the count of the number of records from which the inhibitory concentration data was
   * derived.<br>
   * See {@link #retrieveCountOfAvailableRecords(ICPercent)} for the count of all available
   * records.
   * 
   * @param pctIC Inhibitory concentration percentage.
   * @return Count of the number of records (or null if value not provided in raw data).
   * @throws NoSuchDataException If nonexistent requested percentage inhibitory concentration data.
   */
  Integer retrieveCountOfRecordsUsedToDetermineICValue(ICPercent pctIC) throws NoSuchDataException;

  /**
   * Does the modifier represent a {@linkplain Modifier#GREATER} value for pIC / pXC comparison?
   * <p>
   * If the raw data is from a pIC / pXC value and the modifier is <tt>GREATER</tt> 
   * then this is the same as using a raw data IC source value with a modifier of
   * {@linkplain Modifier#LESS}.
   * <p>
   * Should not be used if the {@link #isModifierEquals(ICPercent)} returns true. Furthermore,
   * only tests against the <tt>GREATER</tt> and <tt>LESS</tt> values.
   * 
   * @param pctIC Inhibitory concentration percentage.
   * @return True if modifier being equivalent to {@linkplain Modifier#GREATER}.
   * @throws IllegalStateException If an equality modifier has been assigned.
   * @throws NoSuchDataException If nonexistent requested percentage inhibitory concentration data.
   */
  boolean isPCComparisonEffectiveGreaterModifier(ICPercent pctIC)
                                                 throws IllegalStateException,
                                                        NoSuchDataException;

  /**
   * Retrieve the modifier for the inhibitory concentration percentage in a consistent (i.e.
   * application-defined) form.
   * 
   * @param pctIC Inhibitory concentration percentage. 
   * @return <b>Non-<tt>null</tt></b> application-defined form for the original modifier.
   * @throws NoSuchDataException If nonexistent requested percentage inhibitory concentration data.
   */
  Modifier retrieveTranslatedModifier(ICPercent pctIC) throws NoSuchDataException;

  /**
   * Retrieve the modifier from a pIC/pXC perspective, i.e. perhaps translated from IC input data.
   * 
   * @param pctIC Inhibitory concentration percentage.
   * @return The effective PC modifier.
   * @throws NoSuchDataException If nonexistent requested percentage inhibitory concentration data.
   */
  Modifier retrieveEffectivePCModifier(ICPercent pctIC) throws NoSuchDataException;

  /**
   * Retrieve the IC50 (uM) value. If the original value was pIC50 / pXC50 then it'll be the IC50
   * equivalent of that value.
   * 
   * @param pctIC Inhibitory concentration percentage.
   * @return IC50 value.
   * @throws NoSuchDataException If nonexistent requested percentage inhibitory concentration data.
   */
  BigDecimal retrieveEquivalentIC50Value(ICPercent pctIC) throws NoSuchDataException;

  /**
   * Retrieve the equivalent pIC50 value. If the original value was IC20 or IC50 then this will be
   * the pIC50 representation of that original value.
   * 
   * @param pctIC Inhibitory concentration percentage.
   * @return pIC50 representation of original value.
   * @throws IllegalArgumentException See {@link ConverterUtil#convert(ICType, Unit, ICType, Unit, BigDecimal)}
   * @throws NoSuchDataException If nonexistent requested percentage inhibitory concentration data.
   */
  BigDecimal retrieveEquivalentPC50Value(ICPercent pctIC) throws IllegalArgumentException,
                                                                 NoSuchDataException;

  /**
   * Retrieve the original inhibitory concentration value as read in from raw data.
   * 
   * @param pctIC Inhibitory concentration percentage.
   * @return Non-null, original inhibitory value.
   * @throws NoSuchDataException If nonexistent requested percentage inhibitory concentration data.
   */
  BigDecimal retrieveOriginalICValue(ICPercent pctIC) throws NoSuchDataException;

  /**
   * Retrieve the originally entered info for the inhibitory concentration percentage..
   * 
   * @param pctIC Inhibitory concentration percentage.
   * @return Originally supplied information used in constructor.
   * @throws NoSuchDataException If nonexistent requested percentage inhibitory concentration data.
   */
  String retrieveOriginalInfo(ICPercent pctIC) throws NoSuchDataException;

  /**
   * Retrieve the original Std Err value as read in from the raw data.
   * 
   * @param pctIC Inhibitory concentration percentage.
   * @return Original Std Err value, or null if not specified.
   * @throws NoSuchDataException If nonexistent requested percentage inhibitory concentration data.
   */
  BigDecimal retrieveOriginalStdErr(ICPercent pctIC) throws NoSuchDataException;

  /**
   * Retrieve a pIC50 value based on a recorded IC20.
   * <ul>
   *   <li>
   *     This function <b>ONLY</b> considers IC20 and Hill Coefficient. It does not concern
   *     itself with any existing 50% inh. conc. data, nor modifiers.</li>
   *   <li>If a Hill Coefficient is not provided it is assumed to have a value of 1.</li>
   * </ul>
   * 
   * @param hillCoefficient Hill Coefficient of IC20.
   * @return pIC50 if possible, otherwise {@code null}.
   */
  BigDecimal retrievePC50BasedOnIC20(BigDecimal hillCoefficient);
}