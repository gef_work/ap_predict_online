/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object;

import java.util.Arrays;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProblemType;

/**
 * Value object for transferring problem information.
 * <p>
 * Generally the problems are discovered in site-specific processing, e.g. when trying unsuccessfully
 * to match individual records to dose-response records, and they're likely to be reported once
 * the site data has been returned to the business manager, e.g. in 
 * <code>SiteDataRecorderServiceActivator</code>! 
 *
 * @author geoff
 */
public class ProblemVO {

  private final ProblemType problem;
  private final Object[] params;

  /**
   * Initialising constructor.
   * 
   * @param problem The {@link ProblemType} being encountered.
   * @param params Message parameters.
   */
  public ProblemVO(final ProblemType problem, final Object[] params) {
    this.problem = problem;
    this.params = params;
  }

  /**
   * Retrieve the problem parameters as a <code>String</code> array.
   * 
   * @return String versions of problem parameters.
   */
  public String[] retrieveParamsAsStringArray() {
    if (params == null) {
      return null;
    }

    final String[] stringParams = new String[params.length];
    int idx = 0;
    for (final Object object : params) {
      stringParams[idx++] = object.toString();
    }
    return stringParams;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    final String paramStr = params == null ? "null" : Arrays.asList(params).toString();
    return "ProblemVO [problem=" + problem + ", params=" + paramStr + "]";
  }

  /**
   * Retrieve the problem type.
   * 
   * @return Problem type, or null if not assigned.
   */
  public ProblemType getProblem() {
    return problem;
  }

  /**
   * Retrieve the problem params.
   * 
   * @return Problem params, or null if not assigned.
   */
  public Object[] getParams() {
    return params;
  }
}