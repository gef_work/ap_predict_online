/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ICDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.selection.ic.ICDataSelectionStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.selection.maxrespconc.MaxRespDataSelectionStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.HillCoefficientDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.MaxRespDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.MarkedDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.datarecord.individual.AbstractIndividualDataRecordVO;

/**
 * Interface which all site-specific individual data records must implement.
 * <p>
 * Implementations derived from this interface must be defined in site-specific code because they
 * must define which elements of the site's screening data are to be considered when determining one
 * or all of the following :
 * <ul>
 *   <li>Eligible Hill Coefficient values</li>
 *   <li>Eligible IC (e.g. IC50, IC20) values</li>
 *   <li>Eligible Max response and concentration values</li>
 * </ul>
 * Please note the term 'eligible', as some databases may have a number of ways of expressing a
 * valid value, e.g. separate IC50 and pIC50 data entries for a screening result, and therefore the
 * must be able to read all the data in. How it subsequently chooses which of the eligible values
 * is to be considered <i>the</i> inhibitory concentration (or whatever) is documented in classes
 * derived from, e.g. {@link ICDataSelectionStrategy}, or {@link MaxRespDataSelectionStrategy}. 
 * 
 * @see AbstractIndividualDataRecordVO
 * @author geoff
 */
public interface IndividualDataRecord extends ICDataHolder, DataRecord, 
                                              HillCoefficientDataHolder, MaxRespDataHolder,
                                              MarkedDataHolder {
}