/*

  Original work: Copyright (c) 2015, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham. 
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.dao;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.SiteDatabaseInteractionException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ExperimentalDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.QSARDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening.AggregatedSiteDataVO;

/**
 * Data access interface for the site's experimental/screening/QSAR data.
 * <p>
 * The acronym DAO in this instance does not imply that the data must exist in
 * the database, it is merely the logical interface to whichever means the site
 * provides to retrieve the site's data for processing, i.e. some data may be
 * derived from web services or means other than a database.
 *
 * @author Geoff Williams
 */
public interface SiteDAO {

  /**
   * Retrieve the site's raw data in a format which the Business Manager
   * workflow will understand.
   * 
   * @param compoundIdentifier Identifier of compound to retrieve data for.
   * @param userId (Optional) User identifier.
   * @return Aggregated site raw compound and experimental data.
   * @throws InvalidValueException If an invalid value is encountered.
   * @throws SiteDatabaseInteractionException Problem communicating with the
   *                                          site database.
   */
  AggregatedSiteDataVO retrieveAggregatedSiteData(String compoundIdentifier,
                                                  String userId)
                                                  throws InvalidValueException,
                                                         SiteDatabaseInteractionException;

  /**
   * Retrieve the site's experimental data in an object which the Business
   * Manager workflow will understand.<br>
   * There'll may be experimental results for different pacing frequencies.
   * 
   * @param compoundIdentifier Identifier of compound to retrieve data for.
   * @param userId (Optional) User identifier.
   * @return An experimental data holder object (perhaps empty, or only holding
   *         problems), but not {@code null}!
   * @throws InvalidValueException If an invalid value is encountered.
   * @throws SiteDatabaseInteractionException Problem communicating with the
   *                                          site database.
   */
  ExperimentalDataHolder retrieveExperimentalData(String compoundIdentifier,
                                                  String userId)
                                                  throws InvalidValueException,
                                                         SiteDatabaseInteractionException;

  /**
   * Retrieve the site's QSAR data in an object which the Business Manager
   * workflow will understand
   * 
   * @param compoundIdentifier Identifier of compound to retrieve data for.
   * @param userId (Optional) User identifier.
   * @return Site's QSAR data.
   * @throws InvalidValueException If an invalid value is encountered.
   * @throws SiteDatabaseInteractionException Problem communicating with the
   *                                          site database.
   */
  QSARDataHolder retrieveQSARData(String compoundIdentifier,
                                  String userId)
                                  throws InvalidValueException,
                                         SiteDatabaseInteractionException;
}