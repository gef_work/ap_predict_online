/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual;

import java.math.BigDecimal;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;

/**
 * Strategy for deriving individual record PC50 data from {@link ICData} objects.
 * <p>
 * Examples of implementing classes are :
 * <ul>
 *   <li>business_manager.business.evaluation.pc50.individual.C50EqualityModifier</li>
 *   <li>business_manager.business.evaluation.pc50.individual.DataMarkedAsInactive</li>
 * </ul>
 *
 * @author geoff
 */
public interface IndividualC50DataStrategy {

  /**
   * Evaluate the PC50 value based on the content of the record's C50 data.
   *  
   * @param icData Individual record's ICData.
   * @param recordIdentifier (Optional) Logging identifier.
   * @return PC50 value according to strategy (or null if cannot be evaluated).
   * @throws IllegalArgumentException If <code>c50Data</code> value is null.
   */
  BigDecimal evaluatePC50(ICData icData, String recordIdentifier) throws IllegalArgumentException;

}