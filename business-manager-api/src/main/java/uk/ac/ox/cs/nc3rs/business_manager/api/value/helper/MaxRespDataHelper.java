/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.helper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.util.ConverterUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.individual.MaxRespConcData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.maxrespconc.MaxRespConcDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.maxrespconc.MaxRespConcRawDataSourceVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;

/**
 * Helper for max response and max response concentration data.
 *
 * @author geoff
 */
public class MaxRespDataHelper {

  private static final Log log = LogFactory.getLog(MaxRespDataHelper.class);

  // Method called internally so required parameter input is assured.
  private static MaxRespConcData createMaxRespData(final DataRecord dataRecord,
                                                   final String maxRespColumnName,
                                                   final String maxRespConcColumnName,
                                                   final Unit maxRespConcUnit)
                                                   throws IllegalArgumentException,
                                                          InvalidValueException {
    log.debug("~createMaxRespData() : Invoked.");
    final Object response = dataRecord.getRawDataValueByColumnName(maxRespColumnName);
    final Object dose = dataRecord.getRawDataValueByColumnName(maxRespConcColumnName);

    MaxRespConcData maxRespConcData = null;
    if (response != null && dose != null) {
      final BigDecimal bdDose = ConverterUtil.toBigDecimal(dose, maxRespConcColumnName);
      BigDecimal maxRespConcM = null;
      switch (maxRespConcUnit) {
        case uM :
          maxRespConcM = ConverterUtil.convert(Unit.uM, Unit.M, bdDose);
          break;
        case M : 
          maxRespConcM = bdDose;
          break;
        default :
          // Programming error?
          final String error = "TODO : Currently impossible to convert from unit '" + maxRespConcUnit.toString() + "' to M (Molar).";
          log.error("~createMaxRespData() : " + error);
          throw new UnsupportedOperationException(error);
      }

      maxRespConcData = new MaxRespConcDataVO(ConverterUtil.toBigDecimal(response, maxRespColumnName),
                                              maxRespColumnName, maxRespConcM, maxRespConcColumnName);
    }

    return maxRespConcData;
  }

  /**
   * Retrieve eligible max response and max response concentration data from the raw data record.
   * 
   * @param rawDataSources Potential sources of max response and corresponding conc data in the raw
   *                       data record (required).
   * @param dataRecord The raw data record (required).
   * @return Collection of max response data (or empty collection, e.g. if nulls encountered).
   * @throws IllegalArgumentException If null passed for any of the required arguments or column
   *                                  name(s) provided are not found in the data record.
   * @throws InvalidValueException Problems interpreting raw data values.
   */
  public static List<MaxRespConcData> eligibleMaxRespData(final List<MaxRespConcRawDataSourceVO> rawDataSources,
                                                          final DataRecord dataRecord)
                                                          throws IllegalArgumentException,
                                                                 InvalidValueException {
    log.debug("~eligibleMaxRespData() : Invoked.");
    if (dataRecord == null || rawDataSources == null || rawDataSources.isEmpty()) {
      final String errorMessage = "Invalid null/empty in dataRecord '" + dataRecord + 
                                  "', rawDataSources '" + rawDataSources + "'";
      log.warn("~eligibleMaxRespData() : " + errorMessage);
      throw new IllegalArgumentException(errorMessage);
    }
    final List<MaxRespConcData> eligibleMaxRespData = new ArrayList<MaxRespConcData>();
    for (final MaxRespConcRawDataSourceVO maxRespDataSource : rawDataSources) {
      final MaxRespConcData maxRespConcData = createMaxRespData(dataRecord,
                                                                maxRespDataSource.getMaxRespColumnName(),
                                                                maxRespDataSource.getMaxRespConcColumnName(), 
                                                                maxRespDataSource.getMaxRespConcUnit());
      if (maxRespConcData != null) {
        eligibleMaxRespData.add(maxRespConcData);
      }
    }

    return eligibleMaxRespData;
  }
}