/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.transfer.screening;

import java.util.List;
import java.util.Map;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 * Generic site data methods.
 *
 * @author geoff
 */
public interface GenericSiteData {

  /**
   * Retrieve the Non-summary (i.e. individual and (if appropriate) dose-response) data records.
   * 
   * @return Non-summary (i.e. individual and (if appropriate) dose-response) data records (or
   *         empty collection if none assigned).
   */
  Map<IndividualDataRecord, List<DoseResponseDataRecord>> getNonSummaryDataRecords();

  /**
   * Retrieve the summary data record.
   * 
   * @param whenSummaryDataOnly If {@code true}, then if there is summary data and no individual data
   *                            present return the summary data, otherwise return {@code null}
   *                            (indicating either no summary data assigned, or both summary and 
   *                            individual data assigned).
   *                            <p>
   *                            If {@code false}, return whatever is assigned.
   * @return Summary data record (or {@code null} depending on circumstances).
   */
  SummaryDataRecord getSummaryDataRecord(boolean whenSummaryDataOnly);

  /**
   * Retrieve the assay the data pertains to.
   * 
   * @return Data assay.
   */
  public AssayVO getAssay();

  /**
   * Retrieve the ion channel the data pertains to.
   * 
   * @return Data ion channel.
   */
  public IonChannel getIonChannel();
}