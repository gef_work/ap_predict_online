/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50;

import java.io.Serializable;

/**
 * Strategies for evaluation of a pIC50/pXC50 value.
 * <p>
 * This interface defines methods which are used during strategy configuration to convey purpose,
 * and also to enable ordering of the strategies during processing.
 *
 * @author geoff
 */
public interface PC50EvaluationStrategy extends Serializable {

  /**
   * Retrieve an indicator of whether the strategy should be used as part of the default strategies
   * collection.
   * 
   * @return Active by default indicator.
   */
  boolean getDefaultActive();

  /**
   * Retrieve the description of the strategy.
   * 
   * @return Description of the strategy.
   */
  String getDescription();

  /**
   * Retrieve the order number this strategy appears in the invocation list.
   * 
   * @return Strategy invocation order value.
   */
  /* This function is used by the Spring Ordered interface to determine the position the strategy
     has in the ordered workflow invocation sequence. */
  int getOrder();

  /**
   * Does the strategy derive the PC50 value from observing an equality modifier?
   * <p>
   * Can be used to ascertain whether the evaluated PC50 is as per the raw data.
   * 
   * @return True if the retrieved PC50 value is derived from an equality modifier, otherwise false.
   */
  boolean usesEqualityModifier();
}