/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening;

import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.nc3rs.business_manager.api.TestUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 * Unit test the abstract generic site data value object.
 *
 * @author geoff
 */
public class AbstractGenericSiteDataVOTest {

  private static final AssayVO dummyValidAssayVO = TestUtil.retrieveValidAssayVO();
  private static final IonChannel dummyValidIonChannel = IonChannel.CaV1_2;
  private IMocksControl mocksControl;
  private SummaryDataRecord mockSummaryDataRecord;
  private IndividualDataRecord mockIndividualDataRecord;
  private DoseResponseDataRecord mockDoseResponseDataRecord1;
  private DoseResponseDataRecord mockDoseResponseDataRecord2;
  private Map<IndividualDataRecord, List<DoseResponseDataRecord>> dummyNonSummaryDataRecords;
  private boolean dummyIsIrregular;

  private class TestAbstractGenericSiteDataVO extends AbstractGenericSiteDataVO {
    public TestAbstractGenericSiteDataVO(final AssayVO dummyAssay,
                                         final IonChannel dummyIonChannel,
                                         final SummaryDataRecord dummySummaryDataRecord,
                                         final Map<IndividualDataRecord, List<DoseResponseDataRecord>> dummyNonSummaryDataRecords,
                                         final boolean dummyIsIrregular)
                                         throws IllegalArgumentException {
      super(dummyAssay, dummyIonChannel, dummySummaryDataRecord, dummyNonSummaryDataRecords,
            dummyIsIrregular);
    }
  }

  @Before
  public void setUp() {
    mocksControl = createControl();

    mockSummaryDataRecord = mocksControl.createMock(SummaryDataRecord.class); 
    mockIndividualDataRecord = mocksControl.createMock(IndividualDataRecord.class);
    mockDoseResponseDataRecord1 = mocksControl.createMock(DoseResponseDataRecord.class);
    mockDoseResponseDataRecord2 = mocksControl.createMock(DoseResponseDataRecord.class);
    final List<DoseResponseDataRecord> dummyDoseResponseRecords = new ArrayList<DoseResponseDataRecord> ();
    dummyDoseResponseRecords.add(mockDoseResponseDataRecord1);
    dummyDoseResponseRecords.add(mockDoseResponseDataRecord2);
    dummyNonSummaryDataRecords = new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();
    dummyNonSummaryDataRecords.put(mockIndividualDataRecord, dummyDoseResponseRecords);

    dummyIsIrregular = false;
  }

  @Test
  public void testConstructorExceptionsThrown() {
    try {
      new TestAbstractGenericSiteDataVO(null, dummyValidIonChannel,
                                        mockSummaryDataRecord,
                                        dummyNonSummaryDataRecords,
                                        dummyIsIrregular);
      fail("Should not permit the non-assignment of assay if not irregular data");
    } catch (IllegalArgumentException e) {}

    try {
      new TestAbstractGenericSiteDataVO(dummyValidAssayVO, null,
                                        mockSummaryDataRecord,
                                        dummyNonSummaryDataRecords,
                                        dummyIsIrregular);
      fail("Should not permit the non-assignment of ion channel if not irregular data");
    } catch (IllegalArgumentException e) {}

    try {
      new TestAbstractGenericSiteDataVO(dummyValidAssayVO, dummyValidIonChannel,
                                        null,
                                        new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>(),
                                        dummyIsIrregular);
      fail("Should not permit the non-existence of summary and non-summary data records");
    } catch (IllegalArgumentException e) {}
  }

  @Test
  public void testIrregularData() {
    // Irregular data with only a summary data record
    final String dummySummaryDataSourceName = "fakeSummaryDataSourceName";
    dummyIsIrregular = true;

    expect(mockSummaryDataRecord.getSourceName()).andReturn(dummySummaryDataSourceName);

    mocksControl.replay();

    TestAbstractGenericSiteDataVO testVO = new TestAbstractGenericSiteDataVO(null, null, 
                                                                             mockSummaryDataRecord,
                                                                             null, dummyIsIrregular);

    assertNull(testVO.getAssay());
    assertNull(testVO.getIonChannel());
    assertNotNull(testVO.toString());

    assertNull(testVO.getIndividualDataSourceName());
    assertTrue(testVO.getNonSummaryDataRecords().isEmpty());
    assertNotNull(testVO.getSummaryDataRecord(false));
    assertEquals(dummySummaryDataSourceName, testVO.getSummaryDataSourceName());
    assertFalse(testVO.hasIndividualData());
    assertTrue(testVO.hasSummaryData());

    mocksControl.verify();

    mocksControl.reset();

    // Irregular data with only an individual data record.
    final String dummyIndividualDataSourceName = "fakeIndividualDataSourceName";
    expect(mockIndividualDataRecord.getSourceName())
          .andReturn(dummyIndividualDataSourceName);

    mocksControl.replay();

    testVO = new TestAbstractGenericSiteDataVO(null, null, null,
                                               dummyNonSummaryDataRecords,
                                               dummyIsIrregular);

    assertNull(testVO.getAssay());
    assertNull(testVO.getIonChannel());
    assertNotNull(testVO.toString());

    assertEquals(dummyIndividualDataSourceName, testVO.getIndividualDataSourceName());
    assertFalse(testVO.getNonSummaryDataRecords().isEmpty());
    assertNull(testVO.getSummaryDataRecord(false));
    assertNull(testVO.getSummaryDataSourceName());
    assertTrue(testVO.hasIndividualData());
    assertFalse(testVO.hasSummaryData());

    mocksControl.verify();
  }

  @Test
  public void testRegularData() {
    final String dummySummaryDataSourceName = "fakeSummaryDataSourceName";
    final String dummyIndividualDataSourceName = "fakeIndividualDataSourceName";

    expect(mockSummaryDataRecord.getSourceName()).andReturn(dummySummaryDataSourceName);
    expect(mockIndividualDataRecord.getSourceName()).andReturn(dummyIndividualDataSourceName);

    TestAbstractGenericSiteDataVO testVO = new TestAbstractGenericSiteDataVO(dummyValidAssayVO,
                                                                             dummyValidIonChannel,
                                                                             mockSummaryDataRecord,
                                                                             dummyNonSummaryDataRecords,
                                                                             dummyIsIrregular);

    mocksControl.replay();

    assertNotNull(testVO.getAssay());
    assertNotNull(testVO.getIonChannel());
    assertNotNull(testVO.toString());
    assertEquals(dummySummaryDataSourceName, testVO.getSummaryDataSourceName());
    assertEquals(dummyIndividualDataSourceName, testVO.getIndividualDataSourceName());
    assertFalse(testVO.getNonSummaryDataRecords().isEmpty());
    assertNotNull(testVO.getSummaryDataRecord(false));
    assertTrue(testVO.hasIndividualData());
    assertTrue(testVO.hasSummaryData());

    mocksControl.verify();
  }

  @Test(expected=UnsupportedOperationException.class)
  public void testRegularDataModificationExceptionThrown() {
    TestAbstractGenericSiteDataVO testVO = new TestAbstractGenericSiteDataVO(dummyValidAssayVO,
                                                                             dummyValidIonChannel,
                                                                             mockSummaryDataRecord,
                                                                             dummyNonSummaryDataRecords,
                                                                             dummyIsIrregular);
    testVO.getNonSummaryDataRecords().clear();
  }
}