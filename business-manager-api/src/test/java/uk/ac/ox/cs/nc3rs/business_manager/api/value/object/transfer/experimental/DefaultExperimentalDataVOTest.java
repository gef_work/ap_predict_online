/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.experimental;

import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.transfer.experimental.ExperimentalResult;

/**
 * Unit test the default experimental data value object.
 *
 * @author geoff
 */
public class DefaultExperimentalDataVOTest {

  private ExperimentalResult mockExperimentalResults1;
  private ExperimentalResult mockExperimentalResults2;
  private IMocksControl mocksControl;
  private Set<ExperimentalResult> dummyValidExperimentalResults = new HashSet<ExperimentalResult>();
  private List<String> dummyValidProblems = new ArrayList<String>();

  @Before
  public void setUp() {
    mocksControl = createControl();
    mockExperimentalResults1 = mocksControl.createMock(ExperimentalResult.class);
    mockExperimentalResults2 = mocksControl.createMock(ExperimentalResult.class);
  }

  @Test
  public void testConstructorSuccessEvenWithANull() {
    DefaultExperimentalDataVO testVO = null;
    // Emulate an empty object.
    testVO = new DefaultExperimentalDataVO(null, null);
    assertNotNull(testVO);
    assertFalse(testVO.containsData());
    assertTrue(testVO.getExperimentalResults().isEmpty());
    assertTrue(testVO.retrievePerFreqCompoundConcs().isEmpty());

    testVO = new DefaultExperimentalDataVO(null, dummyValidProblems);

    assertNotNull(testVO);
    assertFalse(testVO.containsData());
    assertTrue(testVO.getExperimentalResults().isEmpty());
    assertTrue(testVO.retrievePerFreqCompoundConcs().isEmpty());

    testVO = new DefaultExperimentalDataVO(dummyValidExperimentalResults, null);
    assertNotNull(testVO);
    assertFalse(testVO.containsData());
    assertTrue(testVO.getExperimentalResults().isEmpty());
    assertTrue(testVO.retrievePerFreqCompoundConcs().isEmpty());
  }

  @Test
  public void testConstructorHandlingDuplication() {
    // Test a duplicated pacing frequency
    final BigDecimal dummyValidPacingFrequency1 = BigDecimal.ONE; 
    final BigDecimal dummyValidPacingFrequency2 = BigDecimal.TEN;

    dummyValidExperimentalResults.add(mockExperimentalResults1);
    dummyValidExperimentalResults.add(mockExperimentalResults2);

    expect(mockExperimentalResults1.getPacingFrequency())
          .andReturn(dummyValidPacingFrequency1);
    expect(mockExperimentalResults1.getConcentrationQTPctChangeValues())
          .andReturn(new ArrayList<ConcQTPctChangeValuesVO>());
    expect(mockExperimentalResults2.getPacingFrequency())
          .andReturn(dummyValidPacingFrequency1);
    expect(mockExperimentalResults2.getConcentrationQTPctChangeValues())
          .andReturn(new ArrayList<ConcQTPctChangeValuesVO>());

    mocksControl.replay();

    DefaultExperimentalDataVO testVO = null;
    testVO = new DefaultExperimentalDataVO(dummyValidExperimentalResults, null);

    mocksControl.verify();

    assertNotNull(testVO);
    assertTrue(testVO.containsData());

    Map<BigDecimal, Set<BigDecimal>> perFreqCompoundConcs = testVO.retrievePerFreqCompoundConcs();

    assertTrue(1 == perFreqCompoundConcs.size());

    mocksControl.reset();

    // Test a duplicated concentration within a pacing frequency
    final List<ConcQTPctChangeValuesVO> dummyDuplicatedConcs = new ArrayList<ConcQTPctChangeValuesVO>();
    dummyDuplicatedConcs.add(new ConcQTPctChangeValuesVO(BigDecimal.ZERO,
                                                         BigDecimal.ONE,
                                                         BigDecimal.TEN));
    dummyDuplicatedConcs.add(new ConcQTPctChangeValuesVO(BigDecimal.ZERO,
                                                         BigDecimal.ONE,
                                                         BigDecimal.TEN));
    dummyDuplicatedConcs.add(new ConcQTPctChangeValuesVO(BigDecimal.ZERO,
                                                         BigDecimal.ONE,
                                                         BigDecimal.TEN));
    expect(mockExperimentalResults1.getPacingFrequency())
          .andReturn(dummyValidPacingFrequency1);
    expect(mockExperimentalResults1.getConcentrationQTPctChangeValues())
          .andReturn(dummyDuplicatedConcs);
    expect(mockExperimentalResults2.getPacingFrequency())
          .andReturn(dummyValidPacingFrequency2);
    expect(mockExperimentalResults2.getConcentrationQTPctChangeValues())
          .andReturn(new ArrayList<ConcQTPctChangeValuesVO>());

    mocksControl.replay();

    testVO = new DefaultExperimentalDataVO(dummyValidExperimentalResults, null);

    mocksControl.verify();

    assertNotNull(testVO);
    assertTrue(testVO.containsData());

    perFreqCompoundConcs = testVO.retrievePerFreqCompoundConcs();

    assertTrue(2 == perFreqCompoundConcs.size());
    assertTrue(1 == perFreqCompoundConcs.get(dummyValidPacingFrequency1).size());
    assertTrue(0 == perFreqCompoundConcs.get(dummyValidPacingFrequency2).size());
  }

  @Test
  public void testRegularDataSuccess() {
    final BigDecimal dummyValidPacingFrequency1 = BigDecimal.ONE; 
    final BigDecimal dummyValidPacingFrequency2 = BigDecimal.TEN;

    final List<ConcQTPctChangeValuesVO> dummyDuplicatedConcs1 = new ArrayList<ConcQTPctChangeValuesVO>();
    dummyDuplicatedConcs1.add(new ConcQTPctChangeValuesVO(BigDecimal.ZERO,
                                                          BigDecimal.ONE,
                                                          BigDecimal.TEN));
    dummyDuplicatedConcs1.add(new ConcQTPctChangeValuesVO(BigDecimal.TEN,
                                                          BigDecimal.ONE,
                                                          BigDecimal.TEN));
    dummyDuplicatedConcs1.add(new ConcQTPctChangeValuesVO(BigDecimal.ONE,
                                                          BigDecimal.ONE,
                                                          BigDecimal.TEN));
    final List<ConcQTPctChangeValuesVO> dummyDuplicatedConcs2 = new ArrayList<ConcQTPctChangeValuesVO>();
    dummyDuplicatedConcs2.add(new ConcQTPctChangeValuesVO(BigDecimal.TEN,
                                                          BigDecimal.ONE,
                                                          BigDecimal.TEN));
    dummyDuplicatedConcs2.add(new ConcQTPctChangeValuesVO(BigDecimal.ONE,
                                                          BigDecimal.ONE,
                                                          BigDecimal.TEN));

    expect(mockExperimentalResults1.getPacingFrequency())
          .andReturn(dummyValidPacingFrequency1);
    expect(mockExperimentalResults1.getConcentrationQTPctChangeValues())
          .andReturn(dummyDuplicatedConcs1);
    expect(mockExperimentalResults2.getPacingFrequency())
          .andReturn(dummyValidPacingFrequency2);
    expect(mockExperimentalResults2.getConcentrationQTPctChangeValues())
          .andReturn(dummyDuplicatedConcs2);

    mocksControl.replay();

    dummyValidExperimentalResults.add(mockExperimentalResults1);
    dummyValidExperimentalResults.add(mockExperimentalResults2);

    DefaultExperimentalDataVO testVO = new DefaultExperimentalDataVO(dummyValidExperimentalResults,
                                                                     null);
    mocksControl.verify();

    assertNotNull(testVO);
    assertTrue(testVO.containsData());

    Map<BigDecimal, Set<BigDecimal>> perFreqCompoundConcs = testVO.retrievePerFreqCompoundConcs();

    assertTrue(2 == perFreqCompoundConcs.size());
    assertTrue(3 == perFreqCompoundConcs.get(dummyValidPacingFrequency1).size());
    // Test the use of TreeSet has sorted the compound concentrations.
    assertTrue(BigDecimal.TEN.compareTo((BigDecimal) perFreqCompoundConcs.get(dummyValidPacingFrequency1).toArray()[2]) == 0);
    assertTrue(BigDecimal.ONE.compareTo((BigDecimal) perFreqCompoundConcs.get(dummyValidPacingFrequency2).toArray()[0]) == 0);
    assertTrue(2 == perFreqCompoundConcs.get(dummyValidPacingFrequency2).size());

    assertNotNull(testVO.toString());
  }

  @Test
  public void testRegularDataModificationExceptionThrown() {
    final BigDecimal dummyValidPacingFrequency1 = BigDecimal.ONE; 

    final List<ConcQTPctChangeValuesVO> dummyDuplicatedConcs1 = new ArrayList<ConcQTPctChangeValuesVO>();
    dummyDuplicatedConcs1.add(new ConcQTPctChangeValuesVO(BigDecimal.ZERO,
                                                          BigDecimal.ONE,
                                                          BigDecimal.TEN));
    dummyDuplicatedConcs1.add(new ConcQTPctChangeValuesVO(BigDecimal.TEN,
                                                          BigDecimal.ONE,
                                                          BigDecimal.TEN));
    dummyDuplicatedConcs1.add(new ConcQTPctChangeValuesVO(BigDecimal.ONE,
                                                          BigDecimal.ONE,
                                                          BigDecimal.TEN));
    expect(mockExperimentalResults1.getPacingFrequency())
          .andReturn(dummyValidPacingFrequency1);
    expect(mockExperimentalResults1.getConcentrationQTPctChangeValues())
          .andReturn(dummyDuplicatedConcs1);

    mocksControl.replay();

    dummyValidExperimentalResults.add(mockExperimentalResults1);

    DefaultExperimentalDataVO testVO = new DefaultExperimentalDataVO(dummyValidExperimentalResults,
                                                                     null);

    mocksControl.verify();

    assertNotNull(testVO);

    try {
      final Set<ExperimentalResult> experimentalResults = testVO.getExperimentalResults();
      experimentalResults.remove(0);
      fail("Should not permit the modification of returned experimental results");
    } catch (UnsupportedOperationException e) {}

    try {
      final Map<BigDecimal, Set<BigDecimal>> perFreqConcs = testVO.retrievePerFreqCompoundConcs();
      perFreqConcs.clear();
      fail("Should not permit the modification of returned per-frequency concentrations");
    } catch (UnsupportedOperationException e) {}
  }
}