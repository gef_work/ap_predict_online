/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import org.junit.Test;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.NoSuchDataException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Modifier;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICType;

/**
 * Unit test the ICData value object.
 *
 * @author geoff
 */
public class ICDataVOC50Test {

  private BigDecimal dummyValidC50Value = BigDecimal.ONE;
  private String dummyValidC50ValueColumnName = "fakeC50ValueColumnName";
  private Unit dummyValidC50Unit = Unit.uM;
  private String dummyValidC50Modifier = ">";
  private String dummyValidC50ModifierColumnName = "fakeC50ModifierColumnName";
  private Integer dummyValidC50RecordCount = 3;
  private String dummyValidC50RecordCountColumnName = "fakeC50RecordCountColumnName";
  private Integer dummyValidAllRecordCount = 4;
  private String dummyValidAllRecordCountColumnName = "fakeAllRecordCountColumnName";
  private BigDecimal dummyValidC50StdErr = BigDecimal.TEN;
  private String dummyValidC50StdErrColumnName = "fakeC50StdErrColumnName";
  private ICType dummyValidC50Type = ICType.IC50;

  private ICData icData;

  @Test
  public void testMinimumAssignmentConstructor() throws IllegalArgumentException,
                                                        InvalidValueException, NoSuchDataException {
    ICDataVO testVO = new ICDataVO(dummyValidC50Value, dummyValidC50ValueColumnName,
                                   dummyValidC50Unit,
                                   dummyValidC50Modifier, dummyValidC50ModifierColumnName,
                                   dummyValidC50Type);
    assertNull(testVO.retrieveCountOfAvailableRecords(ICPercent.PCT50));
    assertNull(testVO.retrieveCountOfRecordsUsedToDetermineICValue(ICPercent.PCT50));
    assertEquals(dummyValidC50Value, testVO.retrieveOriginalICValue(ICPercent.PCT50));
    assertNotNull(testVO.retrieveOriginalInfo(ICPercent.PCT50));
    assertTrue(Modifier.GREATER.compareTo(testVO.retrieveTranslatedModifier(ICPercent.PCT50)) == 0);
    assertNull(testVO.retrieveOriginalStdErr(ICPercent.PCT50));
    assertFalse(testVO.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50));
    assertTrue(Modifier.LESS.compareTo(testVO.retrieveEffectivePCModifier(ICPercent.PCT50)) == 0);
    assertTrue(new BigDecimal("6").compareTo(testVO.retrieveEquivalentPC50Value(ICPercent.PCT50)) == 0);
    assertTrue(dummyValidC50Value.compareTo(testVO.retrieveEquivalentIC50Value(ICPercent.PCT50)) == 0);
    assertTrue(testVO.hasIC50Source());
    assertFalse(testVO.isModifierEquals(ICPercent.PCT50));
    assertFalse(testVO.isEquivalentPC50Negative(ICPercent.PCT50));
    assertTrue(testVO.isPotentialDose(ICPercent.PCT50, BigDecimal.TEN));
  }

  @Test(expected=InvalidValueException.class)
  public void testInvalidValueExceptionGeneration() throws InvalidValueException {
    new ICDataVO(BigDecimal.ZERO.subtract(BigDecimal.ONE), dummyValidC50ValueColumnName,
                 dummyValidC50Unit, null, dummyValidC50ModifierColumnName, null, null, null, null,
                 null, null, dummyValidC50Type);
  }

  @Test
  public void testRegularDataNoEqualityModifier() throws IllegalArgumentException,
                                                         InvalidValueException, NoSuchDataException {
    final Integer dummyC50Count = 3;
    Integer dummyAllC50Count = 4;
    BigDecimal dummyC50StdErr = null;

    BigDecimal dummyC50Value = new BigDecimal("100.0");
    String dummyC50Modifier = "<";
    icData = new ICDataVO(dummyC50Value, dummyValidC50ValueColumnName, dummyValidC50Unit,
                          dummyC50Modifier, dummyValidC50ModifierColumnName, dummyC50Count,
                          dummyValidC50RecordCountColumnName, dummyAllC50Count,
                          dummyValidAllRecordCountColumnName, dummyC50StdErr,
                          dummyValidC50StdErrColumnName, ICType.IC50);

    assertTrue(icData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50));

    assertFalse(icData.isPotentialDose(ICPercent.PCT50, new BigDecimal("101.0")));
    assertTrue(icData.isPotentialDose(ICPercent.PCT50, new BigDecimal("99.0")));

    dummyC50Modifier = ">";
    icData = new ICDataVO(dummyC50Value, dummyValidC50ValueColumnName, dummyValidC50Unit,
                          dummyC50Modifier, dummyValidC50ModifierColumnName, dummyC50Count,
                          dummyValidC50RecordCountColumnName, dummyAllC50Count,
                          dummyValidAllRecordCountColumnName, dummyC50StdErr,
                          dummyValidC50StdErrColumnName, ICType.IC50);

    assertFalse(icData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50));

    assertTrue(icData.isPotentialDose(ICPercent.PCT50, new BigDecimal("101.0")));
    assertFalse(icData.isPotentialDose(ICPercent.PCT50, new BigDecimal("99.0")));

    dummyC50Value = new BigDecimal("4.0"); // equiv to 100uM
    icData = new ICDataVO(dummyC50Value, dummyValidC50ValueColumnName, Unit.minusLogM,
                          dummyC50Modifier, dummyValidC50ModifierColumnName, dummyC50Count,
                          dummyValidC50RecordCountColumnName, dummyAllC50Count,
                          dummyValidAllRecordCountColumnName, dummyC50StdErr,
                          dummyValidC50StdErrColumnName, ICType.PIC50);

    assertTrue(icData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50));

    assertFalse(icData.isPotentialDose(ICPercent.PCT50, new BigDecimal("1000.0"))); // equiv to pIC50 of 3
    assertTrue(icData.isPotentialDose(ICPercent.PCT50, new BigDecimal("10.0")));    // equiv to pIC50 of 5

    dummyC50Modifier = "<";
    icData = new ICDataVO(dummyC50Value, dummyValidC50ValueColumnName, Unit.minusLogM,
                          dummyC50Modifier, dummyValidC50ModifierColumnName, dummyC50Count,
                          dummyValidC50RecordCountColumnName, dummyAllC50Count,
                          dummyValidAllRecordCountColumnName, dummyC50StdErr,
                          dummyValidC50StdErrColumnName, ICType.PIC50);

    assertFalse(icData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50));

    assertTrue(icData.isPotentialDose(ICPercent.PCT50, new BigDecimal("1000.0"))); // equiv to pIC50 of 3
    assertFalse(icData.isPotentialDose(ICPercent.PCT50, new BigDecimal("10.0")));  // equiv to pIC50 of 5

    dummyC50Modifier = "=";
    icData = new ICDataVO(dummyC50Value, dummyValidC50ValueColumnName, Unit.minusLogM,
                          dummyC50Modifier, dummyValidC50ModifierColumnName, dummyC50Count,
                          dummyValidC50RecordCountColumnName, dummyAllC50Count,
                          dummyValidAllRecordCountColumnName, dummyC50StdErr,
                          dummyValidC50StdErrColumnName, ICType.PIC50);
  }

  @Test
  public void testRegularDataEqualityModifierTest() throws IllegalArgumentException,
                                                       InvalidValueException, NoSuchDataException {
    final Integer dummyC50Count = 3;
    Integer dummyAllC50Count = 4;
    BigDecimal dummyC50StdErr = null;

    BigDecimal dummyC50Value = new BigDecimal("100.0");
    String dummyC50Modifier = "=";
    icData = new ICDataVO(dummyC50Value, dummyValidC50ValueColumnName, dummyValidC50Unit,
                          dummyC50Modifier, dummyValidC50ModifierColumnName, dummyC50Count,
                          dummyValidC50RecordCountColumnName, dummyAllC50Count,
                          dummyValidAllRecordCountColumnName, dummyC50StdErr,
                          dummyValidC50StdErrColumnName, ICType.IC50);
    assertNotNull(icData.retrieveOriginalInfo(ICPercent.PCT50));
    assertTrue(Modifier.EQUALS.compareTo(icData.retrieveEffectivePCModifier(ICPercent.PCT50)) == 0);
    assertTrue(icData.isModifierEquals(ICPercent.PCT50));

    dummyC50Modifier = null;
    icData = new ICDataVO(dummyC50Value, dummyValidC50ValueColumnName, dummyValidC50Unit,
                          dummyC50Modifier, dummyValidC50ModifierColumnName, dummyC50Count,
                          dummyValidC50RecordCountColumnName, dummyAllC50Count,
                          dummyValidAllRecordCountColumnName, dummyC50StdErr,
                          dummyValidC50StdErrColumnName, ICType.IC50);
    assertNotNull(icData.retrieveOriginalInfo(ICPercent.PCT50));
    assertTrue(Modifier.EQUALS.compareTo(icData.retrieveEffectivePCModifier(ICPercent.PCT50)) == 0);
    assertTrue(icData.isModifierEquals(ICPercent.PCT50));

    dummyC50Modifier = "";
    icData = new ICDataVO(dummyC50Value, dummyValidC50ValueColumnName, dummyValidC50Unit,
                          dummyC50Modifier, dummyValidC50ModifierColumnName, dummyC50Count,
                          dummyValidC50RecordCountColumnName, dummyAllC50Count,
                          dummyValidAllRecordCountColumnName, dummyC50StdErr,
                          dummyValidC50StdErrColumnName, ICType.IC50);
    assertNotNull(icData.retrieveOriginalInfo(ICPercent.PCT50));
    assertTrue(Modifier.EQUALS.compareTo(icData.retrieveEffectivePCModifier(ICPercent.PCT50)) == 0);
    assertTrue(icData.isModifierEquals(ICPercent.PCT50));

    try {
      icData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50);
      fail("Should not allow comparison when equality modifier assigned");
    } catch (IllegalStateException e) {}
  }

  @Test
  public void testRegularDataNegativePC50Value() throws IllegalArgumentException,
                                                        InvalidValueException, NoSuchDataException {
    final Integer dummyC50Count = 3;
    Integer dummyAllC50Count = 4;
    BigDecimal dummyC50StdErr = null;

    BigDecimal dummyC50Value = BigDecimal.ZERO.subtract(BigDecimal.ONE);
    String dummyC50Modifier = "=";
    icData = new ICDataVO(dummyC50Value, dummyValidC50ValueColumnName, Unit.minusLogM,
                          dummyC50Modifier, dummyValidC50ModifierColumnName, dummyC50Count,
                          dummyValidC50RecordCountColumnName, dummyAllC50Count,
                          dummyValidAllRecordCountColumnName, dummyC50StdErr,
                          dummyValidC50StdErrColumnName, ICType.PIC50);
    assertTrue(icData.isEquivalentPC50Negative(ICPercent.PCT50));
  }

  @Test
  public void testRegularDataConversionOfZeroIC50ToPIC50Exception() throws IllegalArgumentException,
                                                                           InvalidValueException,
                                                                           NoSuchDataException {
    final Integer dummyC50Count = 3;
    Integer dummyAllC50Count = 4;
    BigDecimal dummyC50StdErr = null;

    BigDecimal dummyC50Value = BigDecimal.ZERO;
    String dummyC50Modifier = "=";
    icData = new ICDataVO(dummyC50Value, dummyValidC50ValueColumnName, dummyValidC50Unit,
                          dummyC50Modifier, dummyValidC50ModifierColumnName, dummyC50Count,
                          dummyValidC50RecordCountColumnName, dummyAllC50Count,
                          dummyValidAllRecordCountColumnName, dummyC50StdErr,
                          dummyValidC50StdErrColumnName, ICType.IC50);
    try {
      icData.retrieveEquivalentPC50Value(ICPercent.PCT50);
      fail("Should not permit conversion of zero-valued IC50 to pIC50 1");
    } catch (IllegalArgumentException e) {}

    try {
      icData.isEquivalentPC50Negative(ICPercent.PCT50);
      fail("Should not permit conversion of zero-valued IC50 to pIC50 2");
    } catch (IllegalArgumentException e) {}
  }

  @Test
  public void testRetrievePC50BasedOnIC20InsufficientData() throws IllegalArgumentException,
                                                                   InvalidValueException {
    // Test that no 20% data generates a null.
    ICDataVO testVO = new ICDataVO(dummyValidC50Value, dummyValidC50ValueColumnName,
                                   dummyValidC50Unit, dummyValidC50Modifier,
                                   dummyValidC50ModifierColumnName,
                                   dummyValidC50RecordCount, dummyValidC50RecordCountColumnName,
                                   dummyValidAllRecordCount, dummyValidAllRecordCountColumnName,
                                   dummyValidC50StdErr, dummyValidC50StdErrColumnName,
                                   dummyValidC50Type);
    assertNull(testVO.retrievePC50BasedOnIC20(null));
  }
}