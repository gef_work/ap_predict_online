/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.qsar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.nc3rs.business_manager.api.TestUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.QSARDataHolder.DataPacket;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 * Unit test the default QSAR data value object.
 *
 * @author geoff
 */
public class DefaultQSARDataVOTest {

  private final IonChannel dummyValidIonChannel = IonChannel.CaV1_2;
  private final BigDecimal dummyValidPIC50 = BigDecimal.ONE;
  private DataPacket mockDataPacket;
  private Set<DataPacket> dummyValidDataPackets = new HashSet<DataPacket>();
  
  private static final AssayVO dummyValidAssayVO = TestUtil.retrieveValidAssayVO();

  @Before
  public void setUp() {
    dummyValidDataPackets.add(mockDataPacket);
  }

  @Test(expected=IllegalArgumentException.class)
  public void testConstructorExceptionsThrown() {
    new DefaultQSARDataVO(null, null, null);
    fail("Should not permit the assignment of a null assay");
  }

  @Test
  public void testRegularDataSuccess() {
    DefaultQSARDataVO testVO = new DefaultQSARDataVO(dummyValidAssayVO, null, null);
    assertNotNull(testVO);
    assertEquals(TestUtil.dummyValidAssayName, testVO.getAssay().getName());
    assertFalse(testVO.containsData());
    assertTrue(testVO.getDataPackets().isEmpty());

    testVO = new DefaultQSARDataVO(dummyValidAssayVO, dummyValidDataPackets, null);
    assertFalse(testVO.getDataPackets().isEmpty());
    assertTrue(testVO.containsData());
    assertTrue(1 == testVO.getDataPackets().size());
    assertNotNull(testVO.toString());
  }

  @Test(expected=UnsupportedOperationException.class)
  public void testRegularDataModificationExceptionThrown() {
    DefaultQSARDataVO testVO = new DefaultQSARDataVO(dummyValidAssayVO, dummyValidDataPackets, null);
    testVO.getDataPackets().clear();
  }

  @Test
  public void testDataPacketConstructorExceptionsThrown() {
    try {
      new DataPacket(null, null);
      fail("Should not permit assignments of null arguments");
    } catch (IllegalArgumentException e) {}

    try {
      new DataPacket(null, dummyValidPIC50);
      fail("Should not permit assignments of null ion channel");
    } catch (IllegalArgumentException e) {}

    try {
      new DataPacket(dummyValidIonChannel, null);
      fail("Should not permit assignments of null pIC50");
    } catch (IllegalArgumentException e) {}    
  }

  @Test
  public void testDataPacketRegularDataSuccess() {
    final DataPacket dataPacket = new DataPacket(dummyValidIonChannel, dummyValidPIC50);
    assertEquals(dummyValidIonChannel, dataPacket.getIonChannel());
    assertEquals(dummyValidPIC50, dataPacket.getpIC50());
    assertNotNull(dataPacket.toString());
  }
}