/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.junit.Test;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICType;

/**
 * Unit test 
 *
 * @author geoff
 */
public class ICRawDataSourceVOTest {

  private String dummyValidValueColumnName = "dummyValueColumnName";
  private String dummyValidUnitColumnName = "dummyUnitColumnName";
  private String dummyValidModifierColumnName = "dummyModifierColumnName";
  private String dummyValidRecordCountColumnName = "dummyRecordCountColumnName";
  private String dummyValidAllRecordCountColumnName = "dummyAllRecordCountColumnName";
  private String dummyValidStdErrColumnName = "dummyStdErrColumnName";
  private ICType dummyValidType = ICType.IC50;

  @Test
  public void testConstructorExceptionsThrown() {
    try {
      new ICRawDataSourceVO(null, dummyValidModifierColumnName, dummyValidUnitColumnName, null,
                           null, null, null);
      fail("Should not permit assignment of null IC value column name");
    } catch (IllegalArgumentException e) {}

    ICRawDataSourceVO testVO = new ICRawDataSourceVO(dummyValidValueColumnName,
                                                     dummyValidUnitColumnName,
                                                     dummyValidModifierColumnName, null, null, null,
                                                     null);
    assertNull(testVO.getAllRecordCountColumnName());
    assertEquals(dummyValidValueColumnName, testVO.getValueColumnName());
    assertNull(testVO.getRecordCountColumnName());
    assertNull(testVO.getStdErrColumnName());
    assertNull(testVO.getICType());
    assertEquals(dummyValidModifierColumnName, testVO.getModifierColumnName());
  }

  @Test
  public void testRegularData() {
    ICRawDataSourceVO testVO = new ICRawDataSourceVO(dummyValidValueColumnName,
                                                     dummyValidUnitColumnName,
                                                     dummyValidModifierColumnName,
                                                     dummyValidRecordCountColumnName,
                                                     dummyValidAllRecordCountColumnName,
                                                     dummyValidStdErrColumnName,
                                                     dummyValidType);
    assertEquals(dummyValidAllRecordCountColumnName, testVO.getAllRecordCountColumnName());
    assertEquals(dummyValidValueColumnName, testVO.getValueColumnName());
    assertEquals(dummyValidRecordCountColumnName, testVO.getRecordCountColumnName());
    assertEquals(dummyValidStdErrColumnName, testVO.getStdErrColumnName());
    assertEquals(dummyValidType, testVO.getICType());
    assertEquals(dummyValidModifierColumnName, testVO.getModifierColumnName());
  }
}