/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.helper;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICRawDataSource;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Modifier;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICType;

/**
 * Unit test the ICDataHelper.
 * 
 * @author geoff
 */
public class ICDataHelperTest {

  private static final ICType dummyValidICType = ICType.IC50;
  private static final String dummyValidValue = "100";
  private static final String dummyValidUnitValue = Unit.uM.toString();
  private static final String dummyInvalidICValue = "-";
  private static final String dummyValidModifier = "<";
  private static final String dummyValidRecordCountValue = "5";
  private static final String dummyValidAllRecordCountValue = "6";
  private static final String dummyInvalidICRecordCountValue = "_";
  private static final String dummyValidStdErrValue = "0.43";
  private DataRecord mockDataRecord;
  private IMocksControl mocksControl;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockDataRecord = mocksControl.createMock(DataRecord.class);
  }

  @Test
  public void testICDataCreationFailOnNullRequiredParameters() {
    mockDataRecord = null;
    ICType dummyType = null;
    String dummyValueColumnName = null;
    String dummyUnitColumnName = null;
    String dummyModifierColumnName = null;
    String dummyRecordCountColumnName = null;
    String dummyAllRecordCountColumnName = null;
    String dummyStdErrColumnName = null;

    try {
      ICDataHelper.createICData(mockDataRecord, dummyType, dummyValueColumnName,
                                dummyUnitColumnName, dummyModifierColumnName,
                                dummyRecordCountColumnName,
                                dummyAllRecordCountColumnName,
                                dummyStdErrColumnName);
      fail("Should not permit null values for required parameters.");
    } catch (IllegalArgumentException e) {}

    mockDataRecord = mocksControl.createMock(DataRecord.class);
    try {
      ICDataHelper.createICData(mockDataRecord, dummyType, dummyValueColumnName,
                                dummyUnitColumnName, dummyModifierColumnName,
                                dummyRecordCountColumnName,
                                dummyAllRecordCountColumnName,
                                dummyStdErrColumnName);
      fail("Should not permit null values for required parameters.");
    } catch (IllegalArgumentException e) {}

    dummyType = dummyValidICType;
    try {
      ICDataHelper.createICData(mockDataRecord, dummyType, dummyValueColumnName,
                                dummyUnitColumnName, dummyModifierColumnName,
                                dummyRecordCountColumnName,
                                dummyAllRecordCountColumnName,
                                dummyStdErrColumnName);
      fail("Should not permit null values for required parameters.");
    } catch (IllegalArgumentException e) {}

    dummyValueColumnName = "dummyICValueColumnName";
    ICData createdICData = null;
    expect(mockDataRecord.getRawDataValueByColumnName(dummyValueColumnName))
          .andReturn(dummyValidValue);

    mocksControl.replay();

    try {
      createdICData = ICDataHelper.createICData(mockDataRecord, dummyType,
                                                dummyValueColumnName,
                                                dummyUnitColumnName,
                                                dummyModifierColumnName,
                                                dummyRecordCountColumnName,
                                                dummyAllRecordCountColumnName,
                                                dummyStdErrColumnName);
    } catch (IllegalArgumentException e) {
      fail("Shouldn't throw an exception.");
    }

    mocksControl.verify();

    assertNotNull(createdICData);
  }

  @Test
  public void testICDataCreationProblems() {
    ICType dummyType = dummyValidICType;
    String dummyValueColumnName = "dummyICValueColumnName";
    String dummyUnitColumnName = "dummyICUnitColumnName";
    String dummyModifierColumnName = "dummyModifierColumnName";
    String dummyRecordCountColumnName = null;
    String dummyAllRecordCountColumnName = null;
    String dummyStdErrColumnName = null;

    ICData createdICData = null;

    /*
     * Test 1. Test reaction to specification of an invalid column name.
     */
    expect(mockDataRecord.getRawDataValueByColumnName(dummyValueColumnName))
          .andThrow(new IllegalArgumentException());

    mocksControl.replay();

    try {
      ICDataHelper.createICData(mockDataRecord, dummyType,
                                dummyValueColumnName, dummyUnitColumnName, 
                                dummyModifierColumnName,
                                dummyRecordCountColumnName,
                                dummyAllRecordCountColumnName,
                                dummyStdErrColumnName);
      fail("Helper should throw the exception generated by the getRawDataValueByColumnName method");
    } catch (IllegalArgumentException e) {}

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Test 2. A null value is potentially a valid value to return from raw data even though it's
     *         not one we're interested in using.
     */
    String dummyValue = null;

    expect(mockDataRecord.getRawDataValueByColumnName(dummyValueColumnName))
          .andReturn(dummyValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyUnitColumnName))
          .andReturn(dummyValidUnitValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyModifierColumnName))
          .andReturn(dummyValidModifier);

    mocksControl.replay();

    try {
      createdICData = ICDataHelper.createICData(mockDataRecord, dummyType,
                                                dummyValueColumnName,
                                                dummyUnitColumnName,
                                                dummyModifierColumnName,
                                                dummyRecordCountColumnName,
                                                dummyAllRecordCountColumnName,
                                                dummyStdErrColumnName);
    } catch (Exception e) {
      fail("Should not throw an exception '" + e.getMessage() + "'.");
    }

    mocksControl.verify();

    assertNull(createdICData);

    mocksControl.reset();

    /*
     * Test 3a. Test the private intValue routine (null value extracted from raw data)
     */
    dummyRecordCountColumnName = "dummyRecordCountColumnName";

    expect(mockDataRecord.getRawDataValueByColumnName(dummyValueColumnName))
          .andReturn(dummyValidValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyUnitColumnName))
          .andReturn(dummyValidUnitValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyModifierColumnName))
          .andReturn(dummyValidModifier);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyRecordCountColumnName))
          .andReturn(null);

    mocksControl.replay();

    try {
      createdICData = ICDataHelper.createICData(mockDataRecord, dummyType,
                                                dummyValueColumnName,
                                                dummyUnitColumnName,
                                                dummyModifierColumnName,
                                                dummyRecordCountColumnName,
                                                dummyAllRecordCountColumnName,
                                                dummyStdErrColumnName);
    } catch (Exception e) {
      fail("Should not throw an exception '" + e.getMessage() + "'.");
    }

    mocksControl.verify();

    assertNotNull(createdICData);

    mocksControl.reset();

    /*
     * Test 3b. Test the private intValue routine (numeric value extracted from raw data)
     */
    expect(mockDataRecord.getRawDataValueByColumnName(dummyValueColumnName))
          .andReturn(dummyValidValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyUnitColumnName))
          .andReturn(dummyValidUnitValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyModifierColumnName))
          .andReturn(dummyValidModifier);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyRecordCountColumnName))
          .andReturn(dummyValidRecordCountValue);

    mocksControl.replay();

    try {
      createdICData = ICDataHelper.createICData(mockDataRecord, dummyType,
                                                dummyValueColumnName,
                                                dummyUnitColumnName,
                                                dummyModifierColumnName,
                                                dummyRecordCountColumnName,
                                                dummyAllRecordCountColumnName,
                                                dummyStdErrColumnName);
    } catch (Exception e) {
      fail("Should not throw an exception '" + e.getMessage() + "'.");
    }

    mocksControl.verify();

    assertNotNull(createdICData);

    mocksControl.reset();

    /*
     * Test 3c. Test the private intValue routine (non-numeric value extracted from raw data)
     */
    expect(mockDataRecord.getRawDataValueByColumnName(dummyValueColumnName))
          .andReturn(dummyValidValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyUnitColumnName))
          .andReturn(dummyValidUnitValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyModifierColumnName))
          .andReturn(dummyValidModifier);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyRecordCountColumnName))
          .andReturn(dummyInvalidICRecordCountValue);

    mocksControl.replay();

    try {
      createdICData = ICDataHelper.createICData(mockDataRecord, dummyType,
                                                dummyValueColumnName,
                                                dummyUnitColumnName,
                                                dummyModifierColumnName,
                                                dummyRecordCountColumnName,
                                                dummyAllRecordCountColumnName,
                                                dummyStdErrColumnName);
    } catch (Exception e) {
      fail("Should not throw an exception '" + e.getMessage() + "'.");
    }

    mocksControl.verify();

    assertNotNull(createdICData);

    mocksControl.reset();

    /*
     * Test 4. Test the presence of the std err column name.
     */
    dummyStdErrColumnName = "dummyStdErrColumnName";

    expect(mockDataRecord.getRawDataValueByColumnName(dummyValueColumnName))
          .andReturn(dummyValidValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyUnitColumnName))
          .andReturn(dummyValidUnitValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyModifierColumnName))
          .andReturn(dummyValidModifier);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyRecordCountColumnName))
          .andReturn(dummyValidRecordCountValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyStdErrColumnName))
          .andReturn(dummyValidStdErrValue);

    mocksControl.replay();

    try {
      createdICData = ICDataHelper.createICData(mockDataRecord, dummyType,
                                                dummyValueColumnName,
                                                dummyUnitColumnName,
                                                dummyModifierColumnName,
                                                dummyRecordCountColumnName,
                                                dummyAllRecordCountColumnName,
                                                dummyStdErrColumnName);
    } catch (Exception e) {
      fail("Should not throw an exception '" + e.getMessage() + "'.");
    }

    mocksControl.verify();

    assertNotNull(createdICData);

    mocksControl.reset();

    /*
     * Test 5. Test the throwing of an exception on ICDataVO construction.
     */
    dummyStdErrColumnName = "dummyStdErrColumnName";

    expect(mockDataRecord.getRawDataValueByColumnName(dummyValueColumnName))
          .andReturn(dummyInvalidICValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyUnitColumnName))
          .andReturn(dummyValidUnitValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyModifierColumnName))
          .andReturn(dummyValidModifier);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyRecordCountColumnName))
          .andReturn(dummyValidRecordCountValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyStdErrColumnName))
          .andReturn(dummyValidStdErrValue);

    mocksControl.replay();

    try {
      createdICData = ICDataHelper.createICData(mockDataRecord, dummyType,
                                                dummyValueColumnName,
                                                dummyUnitColumnName,
                                                dummyModifierColumnName,
                                                dummyRecordCountColumnName,
                                                dummyAllRecordCountColumnName,
                                                dummyStdErrColumnName);
    } catch (Exception e) {
      fail("Should not throw an exception '" + e.getMessage() + "'.");
    }

    mocksControl.verify();

    assertNull(createdICData);
  }

  @Test
  public void testICDataCreationSuccess() {
    ICType dummyType = dummyValidICType;
    String dummyValueColumnName = "dummyICValueColumnName";
    String dummyUnitColumnName = "dummyICUnitColumnName";
    String dummyModifierColumnName = "dummyModifierColumnName";
    String dummyRecordCountColumnName = "dummyRecordCountColumnName";
    String dummyAllRecordCountColumnName = "dummyAllRecordCountColumnName";
    String dummyStdErrColumnName = "dummyStdErrColumnName";

    ICData createdICData = null;

    expect(mockDataRecord.getRawDataValueByColumnName(dummyValueColumnName))
          .andReturn(dummyValidValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyUnitColumnName))
          .andReturn(dummyValidUnitValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyModifierColumnName))
          .andReturn(dummyValidModifier);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyRecordCountColumnName))
          .andReturn(dummyValidRecordCountValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyAllRecordCountColumnName))
          .andReturn(dummyValidAllRecordCountValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyStdErrColumnName))
          .andReturn(dummyValidStdErrValue);

    mocksControl.replay();

    try {
      createdICData = ICDataHelper.createICData(mockDataRecord, dummyType,
                                                dummyValueColumnName,
                                                dummyUnitColumnName,
                                                dummyModifierColumnName,
                                                dummyRecordCountColumnName,
                                                dummyAllRecordCountColumnName,
                                                dummyStdErrColumnName);
    } catch (Exception e) {
      fail("Should not throw an exception '" + e.getMessage() + "'.");
    }

    mocksControl.verify();

    assertNotNull(createdICData);
    try {
      assertEquals(Integer.valueOf(dummyValidAllRecordCountValue),
                   createdICData.retrieveCountOfAvailableRecords(ICPercent.PCT50));
      assertEquals(Integer.valueOf(dummyValidRecordCountValue),
                   createdICData.retrieveCountOfRecordsUsedToDetermineICValue(ICPercent.PCT50));
      assertEquals(new BigDecimal(dummyValidValue), createdICData.retrieveOriginalICValue(ICPercent.PCT50));
      assertTrue(createdICData.retrieveOriginalInfo(ICPercent.PCT50).contains(dummyValidValue));
      assertEquals(new BigDecimal(dummyValidStdErrValue), createdICData.retrieveOriginalStdErr(ICPercent.PCT50));
      assertTrue(createdICData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50));
      assertEquals(Modifier.GREATER, createdICData.retrieveEffectivePCModifier(ICPercent.PCT50));
      assertEquals(new BigDecimal("4"), createdICData.retrieveEquivalentPC50Value(ICPercent.PCT50));
      assertEquals(new BigDecimal(dummyValidValue),
                   createdICData.retrieveEquivalentIC50Value(ICPercent.PCT50));
      assertTrue(createdICData.hasIC50Source());
      assertFalse(createdICData.isModifierEquals(ICPercent.PCT50));
      assertFalse(createdICData.isEquivalentPC50Negative(ICPercent.PCT50));
      assertTrue(createdICData.isPotentialDose(ICPercent.PCT50,
                                               new BigDecimal(dummyValidValue).subtract(BigDecimal.TEN)));
    } catch (Exception e) {
      fail("Should not throw an exception '" + e.getMessage() + "'.");
    }
  }

  @Test
  public void testICDataEligibility() {
    final ICRawDataSource mockICRawDataSource = mocksControl.createMock(ICRawDataSource.class);

    final String dummyValueColumnName = "dummyColumnName";
    final String dummyUnitColumnName = "dummyUnitColumnName";
    final String dummyModifierColumnName = "dummyModifierColumnName";
    final String dummyRecordCountColumnName = "dummyRecordCountColumnName";
    final String dummyAllRecordCountColumnName = "dummyAllRecordCountColumnName";
    final String dummyStdErrColumnName = "dummyStdErrColumnName";
    final ICType dummyType = ICType.PIC50;

    final List<ICRawDataSource> dummyRawDataSources = new ArrayList<ICRawDataSource>();
    dummyRawDataSources.add(mockICRawDataSource);

    expect(mockICRawDataSource.getValueColumnName())
          .andReturn(dummyValueColumnName);
    expect(mockICRawDataSource.getUnitColumnName())
          .andReturn(dummyUnitColumnName);
    expect(mockICRawDataSource.getModifierColumnName())
          .andReturn(dummyModifierColumnName);
    expect(mockICRawDataSource.getRecordCountColumnName())
          .andReturn(dummyRecordCountColumnName);
    expect(mockICRawDataSource.getAllRecordCountColumnName())
          .andReturn(dummyAllRecordCountColumnName);
    expect(mockICRawDataSource.getStdErrColumnName())
          .andReturn(dummyStdErrColumnName);
    expect(mockICRawDataSource.getICType()).andReturn(dummyType);

    expect(mockDataRecord.getRawDataValueByColumnName(dummyValueColumnName))
          .andReturn(dummyValidValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyUnitColumnName))
          .andReturn(dummyValidUnitValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyModifierColumnName))
          .andReturn(dummyValidModifier);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyRecordCountColumnName))
          .andReturn(dummyValidRecordCountValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyAllRecordCountColumnName))
          .andReturn(dummyValidAllRecordCountValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyStdErrColumnName))
          .andReturn(dummyValidStdErrValue);

    mocksControl.replay();

    List<ICData> icData = ICDataHelper.eligibleICData(dummyRawDataSources, mockDataRecord);

    mocksControl.verify();

    // It's empty because it's a pIC50 ICType yet the dummy valid unit value is uM
    assertTrue(icData.isEmpty());

    mocksControl.reset();

    expect(mockICRawDataSource.getValueColumnName())
          .andReturn(dummyValueColumnName);
    expect(mockICRawDataSource.getUnitColumnName())
          .andReturn(dummyUnitColumnName);
    expect(mockICRawDataSource.getModifierColumnName())
          .andReturn(dummyModifierColumnName);
    expect(mockICRawDataSource.getRecordCountColumnName())
          .andReturn(dummyRecordCountColumnName);
    expect(mockICRawDataSource.getAllRecordCountColumnName())
          .andReturn(dummyAllRecordCountColumnName);
    expect(mockICRawDataSource.getStdErrColumnName())
          .andReturn(dummyStdErrColumnName);
    expect(mockICRawDataSource.getICType()).andReturn(dummyType);

    expect(mockDataRecord.getRawDataValueByColumnName(dummyValueColumnName))
          .andReturn(dummyValidValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyUnitColumnName))
          .andReturn(Unit.minusLogM.toString());
    expect(mockDataRecord.getRawDataValueByColumnName(dummyModifierColumnName))
          .andReturn(dummyValidModifier);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyRecordCountColumnName))
          .andReturn(dummyValidRecordCountValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyAllRecordCountColumnName))
          .andReturn(dummyValidAllRecordCountValue);
    expect(mockDataRecord.getRawDataValueByColumnName(dummyStdErrColumnName))
          .andReturn(dummyValidStdErrValue);

    mocksControl.replay();

    icData = ICDataHelper.eligibleICData(dummyRawDataSources, mockDataRecord);

    mocksControl.verify();

    // It's populated because it's a pIC50 ICType and the dummy valid unit value is -log(M)
    assertFalse(icData.isEmpty());
  }

  @Test
  public void testICDataEligibilityExceptionCatch() {
    final ICRawDataSource mockICRawDataSource = mocksControl.createMock(ICRawDataSource.class);

    final String dummyValueColumnName = "dummyValueColumnName";
    final String dummyUnitColumnName = "dummyUnitColumnName";
    final String dummyModifierColumnName = "dummyModifierColumnName";
    final String dummyRecordCountColumnName = "dummyRecordCountColumnName";
    final String dummyAllRecordCountColumnName = "dummyAllRecordCountColumnName";
    final String dummyStdErrColumnName = "dummyStdErrColumnName";
    final ICType dummyType = ICType.PIC50;

    final List<ICRawDataSource> dummyRawDataSources = new ArrayList<ICRawDataSource>();
    dummyRawDataSources.add(mockICRawDataSource);

    expect(mockICRawDataSource.getValueColumnName()).andReturn(dummyValueColumnName);
    expect(mockICRawDataSource.getUnitColumnName()).andReturn(dummyUnitColumnName);
    expect(mockICRawDataSource.getModifierColumnName()).andReturn(dummyModifierColumnName);
    expect(mockICRawDataSource.getRecordCountColumnName()).andReturn(dummyRecordCountColumnName);
    expect(mockICRawDataSource.getAllRecordCountColumnName()).andReturn(dummyAllRecordCountColumnName);
    expect(mockICRawDataSource.getStdErrColumnName()).andReturn(dummyStdErrColumnName);
    expect(mockICRawDataSource.getICType()).andReturn(dummyType);

    // Emulate column name not existing
    expect(mockDataRecord.getRawDataValueByColumnName(dummyValueColumnName))
          .andThrow(new IllegalArgumentException("Fish"));

    mocksControl.replay();
    List<ICData> eligibleICData = ICDataHelper.eligibleICData(dummyRawDataSources,
                                                              mockDataRecord);
    mocksControl.verify();

    assertEquals(0, eligibleICData.size());

    mocksControl.reset();

    expect(mockICRawDataSource.getValueColumnName()).andReturn(dummyValueColumnName);
    expect(mockICRawDataSource.getUnitColumnName()).andReturn(dummyUnitColumnName);
    expect(mockICRawDataSource.getModifierColumnName()).andReturn(dummyModifierColumnName);
    expect(mockICRawDataSource.getRecordCountColumnName()).andReturn(dummyRecordCountColumnName);
    expect(mockICRawDataSource.getAllRecordCountColumnName()).andReturn(dummyAllRecordCountColumnName);
    expect(mockICRawDataSource.getStdErrColumnName()).andReturn(dummyStdErrColumnName);
    expect(mockICRawDataSource.getICType()).andReturn(dummyType);

    // Emulate column name having a null value
    expect(mockDataRecord.getRawDataValueByColumnName(dummyValueColumnName))
          .andThrow(new IllegalArgumentException("Chips"));

    mocksControl.replay();
    eligibleICData = ICDataHelper.eligibleICData(dummyRawDataSources, mockDataRecord);
    mocksControl.verify();

    assertEquals(0, eligibleICData.size());
  }

  @Test(expected=IllegalArgumentException.class)
  public void testICEligibilityExceptionThrown1() throws IllegalArgumentException,
                                                         InvalidValueException {
    ICDataHelper.eligibleICData(null, null);
  }
}