/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.datarecord;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;

/**
 * Unit test the abstract generic data record value object.
 *
 * @author geoff
 */
public class AbstractGenericDataRecordVOTest {

  private final String dummySiteId = "fakeSiteId";
  private final String dummySourceName = "fakeSourceName";
  private Map<String, Object> dummyRawData;

  private class TestAbstractGenericDataRecordVO extends AbstractGenericDataRecordVO {
    protected TestAbstractGenericDataRecordVO(final String dummySiteId, final String dummySourceName,
                                              final Map<String, Object> dummyRawData)
                                              throws IllegalArgumentException {
      super(dummySiteId, dummySourceName, dummyRawData);
    }
  }

  @Before
  public void setUp() {
    dummyRawData = new HashMap<String, Object>();
  }

  @Test
  public void testConstructorIllegalArguments() {
    try {
      new TestAbstractGenericDataRecordVO(null, null, null);
      fail("Should not allow null sourceName or rawData argument");
    } catch (IllegalArgumentException e) {}

    try {
      new TestAbstractGenericDataRecordVO(null, dummySourceName, null);
      fail("Should not allow null rawData argument");
    } catch (IllegalArgumentException e) {}

    try {
      new TestAbstractGenericDataRecordVO(null, null, dummyRawData);
      fail("Should not allow null rawData argument");
    } catch (IllegalArgumentException e) {}
  }

  @Test
  public void testConstructorValidArgumentAssignment() {
    TestAbstractGenericDataRecordVO testVO = new TestAbstractGenericDataRecordVO(null,
                                                                                 dummySourceName,
                                                                                 dummyRawData);
    assertNotNull(testVO.getId());
    assertEquals(dummySourceName, testVO.getSourceName());
    assertEquals(dummyRawData, testVO.getRawData());

    testVO = new TestAbstractGenericDataRecordVO(dummySiteId, dummySourceName, dummyRawData);
    assertEquals(dummySiteId, testVO.getId());
    assertEquals(dummySourceName, testVO.getSourceName());
    assertEquals(dummyRawData, testVO.getRawData());
  }

  @Test
  public void testRawDataRetrievalExceptionCatch() throws InvalidValueException {
    TestAbstractGenericDataRecordVO testVO = new TestAbstractGenericDataRecordVO(null,
                                                                                 dummySourceName,
                                                                                 dummyRawData);
    try {
      testVO.getRawDataValueByColumnName(null);
      fail("Should not allow retrieval of raw data value using a null argument");
    } catch (IllegalArgumentException e) {}

    try {
      testVO.getRawDataValueByColumnName("fishcakes");
      fail("Should not allow retrieval of raw data value using a non-existent column name");
    } catch (IllegalArgumentException e) {}
  }

  @Test
  public void testRawDataRetrieval() throws InvalidValueException {
    final String dummyValidRawDataColumnName = "fakeRawDataColumnName";
    final Object dummyValidRawDataObject = BigDecimal.TEN;
    dummyRawData.put(dummyValidRawDataColumnName, dummyValidRawDataObject);
    TestAbstractGenericDataRecordVO testVO = new TestAbstractGenericDataRecordVO(null,
                                                                                 dummySourceName,
                                                                                 dummyRawData);
    final Object retrieved = testVO.getRawDataValueByColumnName(dummyValidRawDataColumnName);
    assertSame(dummyValidRawDataObject, retrieved);
    assertNotNull(testVO.toString());
  }

  @Test(expected=UnsupportedOperationException.class)
  public void testRawDataModification() {
    final String dummyValidRawDataColumnName = "fakeRawDataColumnName";
    final Object dummyValidRawDataObject = BigDecimal.TEN;
    dummyRawData.put(dummyValidRawDataColumnName, dummyValidRawDataObject);
    TestAbstractGenericDataRecordVO testVO = new TestAbstractGenericDataRecordVO(null,
                                                                                 dummySourceName,
                                                                                 dummyRawData);
    final Map<String, Object> retrieved = testVO.getRawData();
    assertNotNull(retrieved);
    retrieved.put("key", new Object());
  }
}