/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayGroupVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.maxrespconc.MaxRespConcRawDataSourceVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;

/**
 * Utility class for testing purposes.
 *
 * @author geoff
 */
public class TestUtil {

  public static final String dummyValidMaxRespColumnName = "fakeMaxRespColumnName";
  public static final String dummyValidMaxRespConcColumnName = "fakeMaxRespConcColumnName";
  public static final Unit dummyValidMaxRespConcUMUnit = Unit.uM;

  /**
   * Retrieve a valid {@link MaxRespConcRawDataSourceVO} object (uM units).
   * 
   * @return Valid {@link MaxRespConcRawDataSourceVO} object.
   */
  public static MaxRespConcRawDataSourceVO retrieveValidMaxRespConcUMRawDataSourceVO() {
    return new MaxRespConcRawDataSourceVO(dummyValidMaxRespColumnName,
                                          dummyValidMaxRespConcColumnName,
                                          dummyValidMaxRespConcUMUnit);
  }

  public static final Unit dummyValidMaxRespConcMUnit = Unit.M;

  /**
   * Retrieve a valid {@link MaxRespConcRawDataSourceVO} object (uM units).
   * 
   * @return Valid {@link MaxRespConcRawDataSourceVO} object.
   */
  public static MaxRespConcRawDataSourceVO retrieveValidMaxRespConcMRawDataSourceVO() {
    return new MaxRespConcRawDataSourceVO(dummyValidMaxRespColumnName,
                                          dummyValidMaxRespConcColumnName,
                                          dummyValidMaxRespConcMUnit);
  }

  public static final String dummyValidAssayGroupName = "fakeAssayGroupName";
  public static final short dummyValidAssayGroupLevel = 1;

  /**
   * Retrieve a valid {@link AssayGroupVO} object.
   * 
   * @return Valid {@link AssayGroupVO} object.
   */
  public static AssayGroupVO createValidAssayGroupVO() {
    return new AssayGroupVO(dummyValidAssayGroupName, dummyValidAssayGroupLevel);
  }

  public static final String dummyValidAssayName = "fakeAssayName";
  public static final short dummyValidAssayLevel = 2;

  /**
   * Retrieve a valid {@link AssayVO} object.
   * 
   * @return Valid {@link AssayVO} object.
   */
  public static AssayVO retrieveValidAssayVO() {
    return new AssayVO(dummyValidAssayName, createValidAssayGroupVO(), dummyValidAssayLevel);
  }
}