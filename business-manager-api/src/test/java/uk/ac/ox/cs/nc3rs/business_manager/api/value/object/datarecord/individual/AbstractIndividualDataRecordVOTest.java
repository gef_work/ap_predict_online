/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.datarecord.individual;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.individual.MaxRespConcData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.QualityControlFailureVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.datarecord.individual.AbstractIndividualDataRecordVO;

/**
 * Unit test the abstract individual data record value object.
 *
 * @author geoff
 */
public class AbstractIndividualDataRecordVOTest {

  private List<ICData> dummyICDataCollection = new ArrayList<ICData>();
  private final String dummySiteId = "fakeSiteId";
  private final String dummySourceName = "fakeSourceName";
  private Map<String, Object> dummyRawData;

  private class TestAbstractIndividualDataRecordVOValued extends AbstractIndividualDataRecordVO {
    public TestAbstractIndividualDataRecordVOValued(final String dummySiteId, final String dummySourceName,
                                                    final Map<String, Object> dummyRawData)
                                                    throws IllegalArgumentException,
                                                           InvalidValueException {
      super(dummySiteId, dummySourceName, dummyRawData);
    }
    public List<ICData> eligibleICData() {
      return dummyICDataCollection;
    }
    public BigDecimal eligibleHillCoefficient() {
      throw new UnsupportedOperationException("unwritten section");
    }
    public List<MaxRespConcData> eligibleMaxRespData() {
      throw new UnsupportedOperationException("unwritten section");
    }
    public QualityControlFailureVO isMarkedInvalid() {
      throw new UnsupportedOperationException("unwritten section");
    }
    public QualityControlFailureVO isMarkedInactive() {
      throw new UnsupportedOperationException("unwritten section");
    }
  }

  private class TestAbstractIndividualDataRecordVOEmpty extends AbstractIndividualDataRecordVO {
    public TestAbstractIndividualDataRecordVOEmpty(final String dummySiteId, final String dummySourceName,
                                                   final Map<String, Object> dummyRawData)
                                                   throws IllegalArgumentException,
                                                          InvalidValueException {
      super(dummySiteId, dummySourceName, dummyRawData);
    }
    public List<ICData> eligibleICData() {
      return new ArrayList<ICData>();
    }
    public BigDecimal eligibleHillCoefficient() {
      throw new UnsupportedOperationException("unwritten section");
    }
    public List<MaxRespConcData> eligibleMaxRespData() {
      throw new UnsupportedOperationException("unwritten section");
    }
    public QualityControlFailureVO isMarkedInvalid() {
      throw new UnsupportedOperationException("unwritten section");
    }
    public QualityControlFailureVO isMarkedInactive() {
      throw new UnsupportedOperationException("unwritten section");
    }
  }

  @Before
  public void setUp() {
    dummyRawData = new HashMap<String, Object>();
    dummyICDataCollection.add(EasyMock.createMock(ICData.class));
  }

  @Test(expected=IllegalArgumentException.class)
  public void testPostConstructionValidationFailsOnEmptyRawData() throws IllegalArgumentException,
                                                                         InvalidValueException {
    new TestAbstractIndividualDataRecordVOValued(dummySiteId, dummySourceName, dummyRawData);
  }

  @Test
  public void testPostConstructionValidationSuccess() throws IllegalArgumentException,
                                                             InvalidValueException {
    // Put at least one value into the raw data.
    final String dummyValidRawDataColumnName = "fakeRawDataColumnName";
    final Object dummyValidRawDataObject = BigDecimal.TEN;
    dummyRawData.put(dummyValidRawDataColumnName, dummyValidRawDataObject);
    TestAbstractIndividualDataRecordVOEmpty testVO1 = new TestAbstractIndividualDataRecordVOEmpty(null,
                                                                                                  dummySourceName,
                                                                                                  dummyRawData);
    assertTrue(testVO1.eligibleICData().isEmpty());

    TestAbstractIndividualDataRecordVOValued testVO2 = new TestAbstractIndividualDataRecordVOValued(null,
                                                                                                    dummySourceName,
                                                                                                    dummyRawData);
    assertFalse(testVO2.eligibleICData().isEmpty());
  }
}