/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening;

import static org.easymock.EasyMock.createControl;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.IrregularSiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;

/**
 * Unit test the aggregated site data value object.
 *
 * @author geoff
 */
public class AggregatedSiteDataVOTest {

  private IMocksControl mocksControl;
  private List<SiteDataHolder> dummySiteDataHolders;
  private List<IrregularSiteDataHolder> dummyIrregularSiteDataHolders;

  @Before
  public void setUp() {
    mocksControl = createControl();
    dummySiteDataHolders = new ArrayList<SiteDataHolder>();
    dummySiteDataHolders.add(mocksControl.createMock(SiteDataHolder.class));
    dummySiteDataHolders.add(mocksControl.createMock(SiteDataHolder.class));

    dummyIrregularSiteDataHolders = new ArrayList<IrregularSiteDataHolder>();
    dummyIrregularSiteDataHolders.add(mocksControl.createMock(IrregularSiteDataHolder.class));
    dummyIrregularSiteDataHolders.add(mocksControl.createMock(IrregularSiteDataHolder.class));
  }

  @Test
  public void testMinimalConstructor() {
    AggregatedSiteDataVO testVO = new AggregatedSiteDataVO(null);
    assertNotNull(testVO);

    assertTrue(testVO.getIrregularSiteDataHolders().isEmpty());
    assertFalse(testVO.hasIrregularSiteData());
    assertTrue(testVO.getProblems().isEmpty());
    assertFalse(testVO.hasProblems());
    assertTrue(testVO.getSiteDataHolders().isEmpty());
    assertFalse(testVO.hasSiteData());
    assertNotNull(testVO.toString());

    testVO = new AggregatedSiteDataVO(Arrays.asList(new String[] { "fakeProblem1",
                                                                   "fakeProblem2" }));
    assertNotNull(testVO);

    assertTrue(testVO.getIrregularSiteDataHolders().isEmpty());
    assertNotNull(testVO.getProblems());
    assertTrue(testVO.hasProblems());
    assertSame(2, testVO.getProblems().size());
    assertTrue(testVO.getSiteDataHolders().isEmpty());
    assertFalse(testVO.hasIrregularSiteData());
    assertFalse(testVO.hasSiteData());
    assertNotNull(testVO.toString());
  }

  @Test
  public void testConstructorExceptionThrown() {
    try {
      new AggregatedSiteDataVO(null, null, null);
      fail("Should not allow null value for siteDataHolders");
    } catch (IllegalArgumentException e) {}
  }

  @Test
  public void testConstructorRegularData() {
    AggregatedSiteDataVO testVO = new AggregatedSiteDataVO(new ArrayList<SiteDataHolder>(),
                                                           new ArrayList<IrregularSiteDataHolder>(),
                                                           null);
    assertSame(0, testVO.getSiteDataHolders().size());
    assertSame(0, testVO.getIrregularSiteDataHolders().size());

    mocksControl.replay();

    testVO = new AggregatedSiteDataVO(dummySiteDataHolders, null, null);

    mocksControl.verify();

    assertSame(2, testVO.getSiteDataHolders().size());
    assertSame(0, testVO.getIrregularSiteDataHolders().size());

    mocksControl.reset();

    mocksControl.replay();

    testVO = new AggregatedSiteDataVO(dummySiteDataHolders,
                                      dummyIrregularSiteDataHolders, null);

    mocksControl.verify();

    assertSame(2, testVO.getSiteDataHolders().size());
    assertSame(2, testVO.getIrregularSiteDataHolders().size());
  }

  @Test
  public void testRegularDataModificationExceptionThrown() {
    mocksControl.replay();

    AggregatedSiteDataVO testVO = new AggregatedSiteDataVO(dummySiteDataHolders,
                                                           dummyIrregularSiteDataHolders,
                                                           null);

    mocksControl.verify();

    assertTrue(testVO.hasIrregularSiteData());
    assertTrue(testVO.hasSiteData());
    assertFalse(testVO.hasProblems());

    try {
      testVO.getSiteDataHolders().clear();
      fail("Should not allow modification of the site data");
    } catch (UnsupportedOperationException e) {};
    try {
      testVO.getIrregularSiteDataHolders().clear();
      fail("Should not allow modification of the irregular site data");
    } catch (UnsupportedOperationException e) {};
  }
}