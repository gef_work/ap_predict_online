/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 * 
 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import org.junit.Test;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.NoSuchDataException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICType;

/**
 * 
 *
 * @author 
 */
public class ICDataVOTest {

  private BigDecimal dummyValidValue = BigDecimal.ONE;
  private String dummyValidValueColumnName = "fakeValueColumnName";
  private Unit dummyValidUnit = Unit.uM;
  private String dummyValidModifier = ">";
  private String dummyValidModifierColumnName = "fakeModifierColumnName";
  private Integer dummyValidRecordCount = 3;
  private String dummyValidRecordCountColumnName = "fakeRecordCountColumnName";
  private Integer dummyValidAllRecordCount = 4;
  private String dummyValidAllRecordCountColumnName = "fakeAllRecordCountColumnName";
  private BigDecimal dummyValidStdErr = BigDecimal.TEN;
  private String dummyValidStdErrColumnName = "fakeStdErrColumnName";
  private ICType dummyValidType = ICType.IC50;

  @Test
  public void testSingleConstructorUnconditionalNullArgFail() throws InvalidValueException {
    try {
      new ICDataVO(null, null, null, null, null, null, null, null, null, null, null, null);
      fail("Should not accept null ICType in constructor.");
    } catch (IllegalArgumentException e) {}

    try {
      new ICDataVO(null, null, null, null, null, null, null, null, null, null, null,
                   dummyValidType);
      fail("Should not accept null ICValue in constructor.");
    } catch (IllegalArgumentException e) {}

    try {
      new ICDataVO(dummyValidValue, null, null, null, null, null, null, null, null, null, null,
                   dummyValidType);
      fail("Should not accept null value column name in constructor.");
    } catch (IllegalArgumentException e) {}

    try {
      new ICDataVO(dummyValidValue, dummyValidValueColumnName, null, null, null, null, null,
                    null, null, null, null, dummyValidType);
      fail("Should not accept null unit in constructor.");
    } catch (IllegalArgumentException e) {}
  }

  @Test
  public void testSingleConstructorConditionalNullArgFail() throws InvalidValueException {
    try {
      new ICDataVO(dummyValidValue, dummyValidValueColumnName, dummyValidUnit, null,
          dummyValidModifierColumnName, dummyValidRecordCount, null, null, null, null, null,
          dummyValidType);
      fail("Should not accept conditional null RecordCount column name value");
    } catch (IllegalArgumentException e) {}

    try {
      new ICDataVO(dummyValidValue, dummyValidValueColumnName, dummyValidUnit, null,
          dummyValidModifierColumnName, null, null, dummyValidAllRecordCount, null, null, null,
          dummyValidType);
      fail("Should not accept conditional null AllRecordCount column name value");
    } catch (IllegalArgumentException e) {}

    try {
      new ICDataVO(dummyValidValue, dummyValidValueColumnName, dummyValidUnit, null,
          dummyValidModifierColumnName, null, null, null, null, dummyValidStdErr, null,
          dummyValidType);
      fail("Should not accept conditional null StdErr column name value");
    } catch (IllegalArgumentException e) {}
  }

  @Test
  public void testDoubleConstructorFailure() throws IllegalArgumentException, InvalidValueException,
                                                    NoSuchDataException {
    try {
      new ICDataVO(null, dummyValidValueColumnName, dummyValidUnit, dummyValidModifier,
                   dummyValidModifierColumnName, dummyValidRecordCount,
                   dummyValidRecordCountColumnName, dummyValidAllRecordCount,
                   dummyValidAllRecordCountColumnName, dummyValidStdErr, dummyValidStdErrColumnName,
                   dummyValidType,
                   null, dummyValidValueColumnName, dummyValidUnit, dummyValidModifier,
                   dummyValidModifierColumnName, dummyValidRecordCount,
                   dummyValidRecordCountColumnName, dummyValidAllRecordCount,
                   dummyValidAllRecordCountColumnName, dummyValidStdErr, dummyValidStdErrColumnName,
                   dummyValidType);
      fail("Should not permit two null inhibitory concentration values!");
    } catch (IllegalArgumentException e) {}

    try {
      new ICDataVO(dummyValidValue, dummyValidValueColumnName, dummyValidUnit, dummyValidModifier,
                   dummyValidModifierColumnName, dummyValidRecordCount,
                   dummyValidRecordCountColumnName, dummyValidAllRecordCount,
                   dummyValidAllRecordCountColumnName, dummyValidStdErr, dummyValidStdErrColumnName,
                   null,
                   dummyValidValue, dummyValidValueColumnName, dummyValidUnit, dummyValidModifier,
                   dummyValidModifierColumnName, dummyValidRecordCount,
                   dummyValidRecordCountColumnName, dummyValidAllRecordCount,
                   dummyValidAllRecordCountColumnName, dummyValidStdErr, dummyValidStdErrColumnName,
                   null);
      fail("Should not permit two null inhibitory concentration types!");
    } catch (IllegalArgumentException e) {}

    try {
      new ICDataVO(null, dummyValidValueColumnName, dummyValidUnit, dummyValidModifier,
                   dummyValidModifierColumnName, dummyValidRecordCount,
                   dummyValidRecordCountColumnName, dummyValidAllRecordCount,
                   dummyValidAllRecordCountColumnName, dummyValidStdErr, dummyValidStdErrColumnName,
                   dummyValidType,
                   dummyValidValue, dummyValidValueColumnName, dummyValidUnit, dummyValidModifier,
                   dummyValidModifierColumnName, dummyValidRecordCount,
                   dummyValidRecordCountColumnName, dummyValidAllRecordCount,
                   dummyValidAllRecordCountColumnName, dummyValidStdErr, dummyValidStdErrColumnName,
                   null);
      fail("Should not permit incomplete data 1!");
    } catch (IllegalArgumentException e) {}

    try {
      new ICDataVO(dummyValidValue, dummyValidValueColumnName, dummyValidUnit, dummyValidModifier,
                   dummyValidModifierColumnName, dummyValidRecordCount,
                   dummyValidRecordCountColumnName, dummyValidAllRecordCount,
                   dummyValidAllRecordCountColumnName, dummyValidStdErr, dummyValidStdErrColumnName,
                   null,
                   null, dummyValidValueColumnName, dummyValidUnit, dummyValidModifier,
                   dummyValidModifierColumnName, dummyValidRecordCount,
                   dummyValidRecordCountColumnName, dummyValidAllRecordCount,
                   dummyValidAllRecordCountColumnName, dummyValidStdErr, dummyValidStdErrColumnName,
                   dummyValidType);
      fail("Should not permit incomplete data 2!");
    } catch (IllegalArgumentException e) {}

    try {
      new ICDataVO(dummyValidValue, dummyValidValueColumnName, dummyValidUnit, dummyValidModifier,
                   dummyValidModifierColumnName, dummyValidRecordCount,
                   dummyValidRecordCountColumnName, dummyValidAllRecordCount,
                   dummyValidAllRecordCountColumnName, dummyValidStdErr, dummyValidStdErrColumnName,
                   dummyValidType,
                   dummyValidValue, dummyValidValueColumnName, dummyValidUnit, dummyValidModifier,
                   dummyValidModifierColumnName, dummyValidRecordCount,
                   dummyValidRecordCountColumnName, dummyValidAllRecordCount,
                   dummyValidAllRecordCountColumnName, dummyValidStdErr, dummyValidStdErrColumnName,
                   dummyValidType);
      fail("Should not permit identical ICType values!");
    } catch (IllegalArgumentException e) {}
  }

  @Test
  public void testDoubleConstructor() throws IllegalArgumentException, InvalidValueException {
    new ICDataVO(dummyValidValue, dummyValidValueColumnName, dummyValidUnit, dummyValidModifier,
                  dummyValidModifierColumnName, dummyValidRecordCount,
                  dummyValidRecordCountColumnName, dummyValidAllRecordCount,
                  dummyValidAllRecordCountColumnName, dummyValidStdErr, dummyValidStdErrColumnName,
                  dummyValidType,
                  dummyValidValue, dummyValidValueColumnName, dummyValidUnit, dummyValidModifier,
                  dummyValidModifierColumnName, dummyValidRecordCount,
                  dummyValidRecordCountColumnName, dummyValidAllRecordCount,
                  dummyValidAllRecordCountColumnName, dummyValidStdErr, dummyValidStdErrColumnName,
                  ICType.IC20);
  }

  @Test
  public void testDoubleICDataConstructorFailures() throws InvalidValueException {
    final ICDataVO testVO20 = new ICDataVO(dummyValidValue, dummyValidValueColumnName, dummyValidUnit,
                                           dummyValidModifier, dummyValidModifierColumnName,
                                           dummyValidRecordCount, dummyValidRecordCountColumnName,
                                           dummyValidAllRecordCount, dummyValidAllRecordCountColumnName,
                                           dummyValidStdErr, dummyValidStdErrColumnName, ICType.IC20);

    try {
      new ICDataVO(null, testVO20);
      fail("Should not permit a null 50% inhibition data!");
    } catch (IllegalArgumentException e) {}

    final ICDataVO testVO50_1 = new ICDataVO(dummyValidValue, dummyValidValueColumnName, dummyValidUnit,
                                             dummyValidModifier, dummyValidModifierColumnName,
                                             dummyValidRecordCount, dummyValidRecordCountColumnName,
                                             dummyValidAllRecordCount, dummyValidAllRecordCountColumnName,
                                             dummyValidStdErr, dummyValidStdErrColumnName, dummyValidType);

    try {
      new ICDataVO(testVO50_1, null);
      fail("Should not permit a null 20% inhibition data!");
    } catch (IllegalArgumentException e) {}

    final ICDataVO testVO50_2 = new ICDataVO(dummyValidValue, dummyValidValueColumnName, dummyValidUnit,
                                             dummyValidModifier, dummyValidModifierColumnName,
                                             dummyValidRecordCount, dummyValidRecordCountColumnName,
                                             dummyValidAllRecordCount, dummyValidAllRecordCountColumnName,
                                             dummyValidStdErr, dummyValidStdErrColumnName, dummyValidType);

    try {
      new ICDataVO(testVO50_1, testVO50_2);
      fail("Should not permit the second argument to be a 50% value!");
    } catch (IllegalArgumentException e) {}

    try {
      new ICDataVO(testVO20, testVO50_1);
      fail("Should not permit the first argument to be a 20% value!");
    } catch (IllegalArgumentException e) {}
  }

  @Test
  public void testDoubleICDataConstructorSuccess() throws InvalidValueException, NoSuchDataException {
    final BigDecimal pct20Value = BigDecimal.ONE;
    final BigDecimal pct50Value = BigDecimal.TEN;

    ICDataVO testVO50 = new ICDataVO(pct50Value, dummyValidValueColumnName, dummyValidUnit,
                                     dummyValidModifier, dummyValidModifierColumnName,
                                     dummyValidRecordCount, dummyValidRecordCountColumnName,
                                     dummyValidAllRecordCount, dummyValidAllRecordCountColumnName,
                                     dummyValidStdErr, dummyValidStdErrColumnName, dummyValidType);
    ICDataVO testVO20 = new ICDataVO(pct20Value, dummyValidValueColumnName, dummyValidUnit,
                                     dummyValidModifier, dummyValidModifierColumnName,
                                     dummyValidRecordCount, dummyValidRecordCountColumnName,
                                     dummyValidAllRecordCount, dummyValidAllRecordCountColumnName,
                                     dummyValidStdErr, dummyValidStdErrColumnName, ICType.IC20);

    final ICDataVO testVO = new ICDataVO(testVO50, testVO20);

    assertTrue(pct20Value.compareTo(testVO.retrieveOriginalICValue(ICPercent.PCT20)) == 0);
    assertTrue(pct50Value.compareTo(testVO.retrieveOriginalICValue(ICPercent.PCT50)) == 0);
  }
}