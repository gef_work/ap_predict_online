/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.Test;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICType;

/**
 * Unit test the converter utility.
 * 
 * @author geoff
 */
public class ConverterUtilTest {

  /**
   * Test shortening of BigDecimal to the default length.
   */
  @Test
  public void testBDShortened() {
    BigDecimal toShorten = BigDecimal.ONE;
    String shortened = ConverterUtil.bdShortened(toShorten);
    assertSame(3, shortened.split("\\.")[1].length());

    toShorten = new BigDecimal("1.0349880");
    shortened = ConverterUtil.bdShortened(toShorten);
    assertSame(3, shortened.split("\\.")[1].length());

    shortened = ConverterUtil.bdShortened(null, 3);
    assertNull(shortened);

    toShorten = BigDecimal.ONE;
    int maxLen = 5;
    shortened = ConverterUtil.bdShortened(toShorten, maxLen);
    assertSame(maxLen, shortened.split("\\.")[1].length());

    toShorten = new BigDecimal("1.0349880");
    shortened = ConverterUtil.bdShortened(toShorten, maxLen);
    assertSame(maxLen, shortened.split("\\.")[1].length());

    toShorten = new BigDecimal("-1.0349880");
    shortened = ConverterUtil.bdShortened(toShorten, maxLen);
    assertSame(maxLen, shortened.split("\\.")[1].length());
  }

  @Test
  public void testConvertNullValues() throws InvalidValueException {
    ICType dummyFromICType = null;
    ICType dummyToICType = ICType.IC20;
    Unit dummyFromUnit = Unit.M;
    Unit dummyToUnit = Unit.M;
    BigDecimal dummyFromValue = BigDecimal.ONE;

    try {
      ConverterUtil.convert(dummyFromICType, dummyFromUnit, dummyToICType, dummyToUnit,
                            dummyFromValue);
      fail("Should not permit a null fromICType arg");
    } catch (IllegalArgumentException e) {}

    dummyFromICType = ICType.IC20;
    dummyFromUnit = null;
    try {
      ConverterUtil.convert(dummyFromICType, dummyFromUnit, dummyToICType, dummyToUnit,
                            dummyFromValue);
      fail("Should not permit a null fromUnit arg");
    } catch (IllegalArgumentException e) {}

    dummyFromUnit = Unit.M;
    dummyToICType = null;
    try {
      ConverterUtil.convert(dummyFromICType, dummyFromUnit, dummyToICType, dummyToUnit,
                            dummyFromValue);
      fail("Should not permit a null toICType arg");
    } catch (IllegalArgumentException e) {}

    dummyToICType = ICType.IC20;
    dummyToUnit = null;
    try {
      ConverterUtil.convert(dummyFromICType, dummyFromUnit, dummyToICType, dummyToUnit,
                            dummyFromValue);
      fail("Should not permit a null toUnit arg");
    } catch (IllegalArgumentException e) {}

    dummyToUnit = Unit.M;
    dummyFromValue = null;

    // null converts to null!
    BigDecimal converted = ConverterUtil.convert(dummyFromICType, dummyFromUnit, dummyToICType,
                                                 dummyToUnit, dummyFromValue);
    assertNull(converted);
  }

  @Test
  public void testConvertC20A() throws InvalidValueException {
    BigDecimal dummyFromValue = ConverterUtil.QUARTER;
    BigDecimal converted = null;

    converted = ConverterUtil.convert(ICType.IC20, Unit.uM, ICType.PIC50, Unit.minusLogM,
                                      dummyFromValue);

    assertTrue(converted.compareTo(new BigDecimal("6")) == 0);

    dummyFromValue = new BigDecimal("0.00000025");
    converted = ConverterUtil.convert(ICType.IC20, Unit.M, ICType.PIC50, Unit.minusLogM,
                                      dummyFromValue);

    assertTrue(converted.compareTo(new BigDecimal("6")) == 0);
  }

  @Test
  public void testConvertC20B() throws InvalidValueException {
    BigDecimal dummyIC20 = null;
    BigDecimal dummyHill = null;

    BigDecimal converted = null;
    try {
      converted = ConverterUtil.convertIC20ToPC50(dummyIC20, dummyHill);
      fail("Should not attempt to convert a null IC20!");
    } catch (IllegalArgumentException e) {}

    dummyIC20 = ConverterUtil.QUARTER;
    converted = ConverterUtil.convertIC20ToPC50(dummyIC20, dummyHill);
    assertTrue(converted.compareTo(new BigDecimal("6")) == 0);

    dummyHill = BigDecimal.ONE;
    converted = ConverterUtil.convertIC20ToPC50(dummyIC20, dummyHill);
    assertTrue(converted.compareTo(new BigDecimal("6")) == 0);

    dummyHill = BigDecimal.ZERO;
    try {
      converted = ConverterUtil.convertIC20ToPC50(dummyIC20, dummyHill);
      fail("Should not permit the Hill to be zero if supplied!");
    } catch (IllegalArgumentException e) {}

    // This tests the first divide operation doesn't on ArithmeticException: Non-terminating decimal expansion; no exact representable decimal result
    dummyHill = new BigDecimal("0.9999999999999999999");
    converted = ConverterUtil.convertIC20ToPC50(dummyIC20, dummyHill);
    assertTrue(converted.compareTo(new BigDecimal("6")) == 0);

    dummyIC20 = ConverterUtil.THOUSAND;
    dummyHill = new BigDecimal("0.5");
    converted = ConverterUtil.convertIC20ToPC50(dummyIC20, dummyHill);
    assertTrue(converted.setScale(5, RoundingMode.DOWN).compareTo(new BigDecimal("1.79588")) == 0);

    // This tests the second divide operation doesn't on ArithmeticException: Non-terminating decimal expansion; no exact representable decimal result
    dummyIC20 = new BigDecimal("12.000");
    dummyHill = new BigDecimal("0.940");
    converted = ConverterUtil.convertIC20ToPC50(dummyIC20, dummyHill);
    System.out.println(converted);
  }

  @Test
  public void testConvertC50() throws InvalidValueException {
    /* Non-conversion!
    BigDecimal fromValue = BigDecimal.TEN; 
    BigDecimal converted = ConverterUtil.convert(IC_Unit.uM_IC50, IC_Unit.uM_IC50, fromValue);
    assertSame(fromValue, converted);
    converted = ConverterUtil.convert(IC_Unit.pIC50, IC_Unit.pIC50, fromValue);
    assertSame(fromValue, converted);
    converted = ConverterUtil.convert(IC_Unit.pXC50, IC_Unit.pXC50, fromValue);
    assertSame(fromValue, converted);
    converted = ConverterUtil.convert(IC_Unit.pIC50, IC_Unit.pXC50, fromValue);
    assertSame(fromValue, converted);
    converted = ConverterUtil.convert(IC_Unit.pXC50, IC_Unit.pIC50, fromValue);
    assertSame(fromValue, converted);
    */

    /*
     * IC50 to pIC50 conversion.
     */
    BigDecimal dummyFromValue = ConverterUtil.MINUS1;
    BigDecimal converted = null;

    try {
      ConverterUtil.convert(ICType.IC50, Unit.uM, ICType.PIC50, Unit.minusLogM, dummyFromValue);
      fail("Should not accept a negative IC50 value");
    } catch (InvalidValueException e) {}

    dummyFromValue = BigDecimal.ZERO;
    try {
      converted = ConverterUtil.convert(ICType.IC50, Unit.uM, ICType.PIC50, Unit.minusLogM,
                                        dummyFromValue);
      fail("Should not convert a 0 (zero) IC50 to a pIC50");
    } catch (IllegalArgumentException e) {}

    dummyFromValue = ConverterUtil.MILLION;
    converted = ConverterUtil.convert(ICType.IC50, Unit.uM, ICType.PIC50, Unit.minusLogM,
                                      dummyFromValue);
    assertEquals(BigDecimal.ZERO, converted);

    dummyFromValue = BigDecimal.TEN;
    converted = ConverterUtil.convert(ICType.IC50, Unit.uM, ICType.PIC50, Unit.minusLogM,
                                      dummyFromValue);
    assertEquals(new BigDecimal("5"), converted);

    dummyFromValue = ConverterUtil.MILLION.multiply(ConverterUtil.MILLION);
    converted = ConverterUtil.convert(ICType.IC50, Unit.uM, ICType.PIC50, Unit.minusLogM,
                                      dummyFromValue);
    assertEquals(new BigDecimal("-6"), converted);

    /*
     * pIC50 to IC50 conversion.
     */
    dummyFromValue = null;
    converted = ConverterUtil.convert(ICType.PIC50, Unit.minusLogM, ICType.IC50, Unit.uM,
                                      dummyFromValue);
    assertNull(converted);

    dummyFromValue = ConverterUtil.MINUS1;
    converted = ConverterUtil.convert(ICType.PIC50, Unit.minusLogM, ICType.IC50, Unit.uM,
                                      dummyFromValue);
    assertEquals(new BigDecimal("10000000"), converted);

    dummyFromValue = BigDecimal.ZERO;
    converted = ConverterUtil.convert(ICType.PIC50, Unit.minusLogM, ICType.IC50, Unit.uM,
                                      dummyFromValue);
    assertEquals(ConverterUtil.MILLION, converted);

    dummyFromValue = BigDecimal.ONE;
    converted = ConverterUtil.convert(ICType.PIC50, Unit.minusLogM, ICType.IC50, Unit.uM,
                                      dummyFromValue);
    assertEquals(new BigDecimal("100000"), converted.setScale(0, RoundingMode.HALF_UP));

    dummyFromValue = new BigDecimal("5");
    converted = ConverterUtil.convert(ICType.PIC50, Unit.minusLogM, ICType.IC50, Unit.uM,
                                      dummyFromValue);
    assertEquals(BigDecimal.TEN, converted.setScale(0, RoundingMode.HALF_UP));

    dummyFromValue = ConverterUtil.MILLION;
    converted = ConverterUtil.convert(ICType.PIC50, Unit.minusLogM, ICType.IC50, Unit.uM,
                                      dummyFromValue);
    assertEquals(BigDecimal.ZERO, converted);
  }

  @Test
  public void testConvertMolar() {
    BigDecimal fromValue = null;
    BigDecimal converted = ConverterUtil.convert(Unit.M, Unit.uM, fromValue);
    assertNull(converted);

    fromValue = BigDecimal.ZERO;
    converted = ConverterUtil.convert(Unit.M, Unit.uM, fromValue);
    assertEquals(BigDecimal.ZERO, converted);

    fromValue = BigDecimal.ONE;
    converted = ConverterUtil.convert(Unit.M, Unit.uM, fromValue);
    assertEquals(ConverterUtil.MILLION, converted);

    fromValue = null;
    converted = ConverterUtil.convert(Unit.uM, Unit.M, fromValue);
    assertNull(converted);

    fromValue = BigDecimal.ZERO;
    converted = ConverterUtil.convert(Unit.uM, Unit.M, fromValue);
    assertEquals(BigDecimal.ZERO, converted);

    fromValue = BigDecimal.ONE;
    converted = ConverterUtil.convert(Unit.uM, Unit.M, fromValue);
    assertTrue(new BigDecimal("0.000001").compareTo(converted) == 0);

    // No conversion?
    converted = ConverterUtil.convert(Unit.uM, Unit.uM, fromValue);
    assertTrue(fromValue.compareTo(converted) == 0);

    // No conversion?
    converted = ConverterUtil.convert(Unit.M, Unit.M, fromValue);
    assertTrue(fromValue.compareTo(converted) == 0);

    converted = ConverterUtil.convert(Unit.M, Unit.nM, fromValue);
    assertTrue(ConverterUtil.BILLION.compareTo(converted) == 0);

    converted = ConverterUtil.convert(Unit.uM, Unit.nM, fromValue);
    assertTrue(ConverterUtil.THOUSAND.compareTo(converted) == 0);

    converted = ConverterUtil.convert(Unit.nM, Unit.uM, fromValue);
    assertTrue(new BigDecimal("0.001").compareTo(converted) == 0);

    converted = ConverterUtil.convert(Unit.nM, Unit.M, fromValue);
    assertTrue(new BigDecimal("0.000000001").compareTo(converted) == 0);
  }

  @Test
  public void testToBigDecimal() throws InvalidValueException {
    String dummyLoggingIdentifier = null;
    Object dummyObject = null;
    BigDecimal converted = ConverterUtil.toBigDecimal(dummyObject, dummyLoggingIdentifier);
    assertNull(converted);

    dummyObject = "10";
    converted = ConverterUtil.toBigDecimal(dummyObject, dummyLoggingIdentifier);
    assertEquals(BigDecimal.TEN, converted);

    dummyObject = "croak";
    try {
      converted = ConverterUtil.toBigDecimal(dummyObject, dummyLoggingIdentifier);
      fail("Should not permit the conversion of '" + dummyObject + "' to a BigDecimal!");
    } catch (InvalidValueException e) {}
  }
}