/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.helper;

import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.QualityControlTest;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.QualityControlFailureVO;

/**
 * Unit test the quality control data helper.
 *
 * @author geoff
 */
public class QualityControlDataHelperTest {

  private DataRecord mockDataRecord;
  private IMocksControl mocksControl;
  private List<QualityControlTest> qualityControlTests = new ArrayList<QualityControlTest>();
  private QualityControlTest mockTest1;
  private QualityControlTest mockTest2;

  @Before
  public void setUp() {
    mocksControl = createControl();
    mockDataRecord = mocksControl.createMock(DataRecord.class);
    mockTest1 = mocksControl.createMock(QualityControlTest.class);
    mockTest2 = mocksControl.createMock(QualityControlTest.class);

    qualityControlTests.add(mockTest1);
    qualityControlTests.add(mockTest2);
  }

  @Test
  public void testCompoundInactivityInvalidArgumentInvocation() throws InvalidValueException {
    try {
      QualityControlDataHelper.qcCheck(null, mockDataRecord);
      fail("Should not accept null value for compound inactivity quality control test collection");
    } catch (IllegalArgumentException e) {}
    try {
      QualityControlDataHelper.qcCheck(qualityControlTests, null);
      fail("Should not accept null value for compound inactivity data record");
    } catch (IllegalArgumentException e) {}
  }

  @Test
  public void testCompoundInactivity() throws InvalidValueException {
    final String dummyTest1Nature = "fakeTest1Nature";
    final String dummyTest2Nature = "fakeTest2Nature";

    expect(mockTest1.passed(mockDataRecord)).andReturn(false);
    expect(mockTest1.nature()).andReturn(dummyTest1Nature);

    mocksControl.replay();

    QualityControlFailureVO qualityControlFailure = QualityControlDataHelper.qcCheck(qualityControlTests,
                                                                                     mockDataRecord);

    mocksControl.verify();

    assertNotNull(qualityControlFailure);
    assertEquals(dummyTest1Nature, qualityControlFailure.getReason());

    mocksControl.reset();

    expect(mockTest1.passed(mockDataRecord)).andReturn(true);
    expect(mockTest2.passed(mockDataRecord)).andReturn(false);
    expect(mockTest2.nature()).andReturn(dummyTest2Nature);

    mocksControl.replay();

    qualityControlFailure = QualityControlDataHelper.qcCheck(qualityControlTests,
                                                             mockDataRecord);

    mocksControl.verify();

    assertNotNull(qualityControlFailure);
    assertEquals(dummyTest2Nature, qualityControlFailure.getReason());

    mocksControl.reset();

    expect(mockTest1.passed(mockDataRecord)).andReturn(true);
    expect(mockTest2.passed(mockDataRecord)).andReturn(true);

    mocksControl.replay();

    qualityControlFailure = QualityControlDataHelper.qcCheck(qualityControlTests,
                                                             mockDataRecord);

    mocksControl.verify();

    assertNull(qualityControlFailure);
  }
  
  @Test
  public void testDataInvalidityInvalidArgumentInvocation() throws InvalidValueException {
    try {
      QualityControlDataHelper.qcCheck(null, mockDataRecord);
      fail("Should not accept null value for invalid data quality control test collection");
    } catch (IllegalArgumentException e) {}
    try {
      QualityControlDataHelper.qcCheck(qualityControlTests, null);
      fail("Should not accept null value for invalid data data record");
    } catch (IllegalArgumentException e) {}
  }

  @Test
  public void testDataInvalidity() throws InvalidValueException {
    final String dummyTest1Nature = "fakeTest1Nature";
    final String dummyTest2Nature = "fakeTest2Nature";

    expect(mockTest1.passed(mockDataRecord)).andReturn(false);
    expect(mockTest1.nature()).andReturn(dummyTest1Nature);

    mocksControl.replay();

    QualityControlFailureVO qualityControlFailure = QualityControlDataHelper.qcCheck(qualityControlTests,
                                                                                     mockDataRecord);

    mocksControl.verify();

    assertNotNull(qualityControlFailure);
    assertEquals(dummyTest1Nature, qualityControlFailure.getReason());

    mocksControl.reset();

    expect(mockTest1.passed(mockDataRecord)).andReturn(true);
    expect(mockTest2.passed(mockDataRecord)).andReturn(false);
    expect(mockTest2.nature()).andReturn(dummyTest2Nature);

    mocksControl.replay();

    qualityControlFailure = QualityControlDataHelper.qcCheck(qualityControlTests,
                                                             mockDataRecord);

    mocksControl.verify();

    assertNotNull(qualityControlFailure);
    assertEquals(dummyTest2Nature, qualityControlFailure.getReason());

    mocksControl.reset();

    expect(mockTest1.passed(mockDataRecord)).andReturn(true);
    expect(mockTest2.passed(mockDataRecord)).andReturn(true);

    mocksControl.replay();

    qualityControlFailure = QualityControlDataHelper.qcCheck(qualityControlTests,
                                                             mockDataRecord);

    mocksControl.verify();

    assertNull(qualityControlFailure);
  }
}