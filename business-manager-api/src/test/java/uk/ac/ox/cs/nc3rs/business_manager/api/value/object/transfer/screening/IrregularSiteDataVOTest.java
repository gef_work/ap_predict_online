/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening;

import static org.easymock.EasyMock.createControl;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ProblemVO;

/**
 * Unit test the irregular site data value object.
 *
 * @author geoff
 */
public class IrregularSiteDataVOTest {

  private IMocksControl mocksControl;
  private List<ProblemVO> dummyProblems;
  private SummaryDataRecord mockSummaryDataRecord;

  @Before
  public void setUp() {
    mocksControl = createControl();
    mockSummaryDataRecord = mocksControl.createMock(SummaryDataRecord.class);
    dummyProblems = new ArrayList<ProblemVO>();
    dummyProblems.add(mocksControl.createMock(ProblemVO.class));
    dummyProblems.add(mocksControl.createMock(ProblemVO.class));
  }

  @Test
  public void testProblemHandling() {
    mocksControl.replay();

    // See AbstractGenericSiteDataVO constructor (need at least a summary data record).
    IrregularSiteDataVO testVO = new IrregularSiteDataVO(null, null,
                                                         mockSummaryDataRecord,
                                                         null, null);

    mocksControl.verify();

    assertNotNull(testVO);
    assertNotNull(testVO.toString());
    assertTrue(testVO.getProblems().isEmpty());

    mocksControl.reset();

    mocksControl.replay();

    testVO = new IrregularSiteDataVO(null, null, mockSummaryDataRecord, null,
                                     dummyProblems);

    mocksControl.verify();

    assertNotNull(testVO);
    assertNotNull(testVO.toString());
    assertTrue(2 == testVO.getProblems().size());
  }

  @Test
  public void testRegularDataModificationExceptionThrown() {
    mocksControl.replay();

    final IrregularSiteDataVO testVO = new IrregularSiteDataVO(null, null,
                                                               mockSummaryDataRecord,
                                                               null, dummyProblems);

    mocksControl.verify();

    try {
      testVO.getProblems().clear();
      fail("Should not permit the modification of the assigned problems");
    } catch (UnsupportedOperationException e) {}
  }
}