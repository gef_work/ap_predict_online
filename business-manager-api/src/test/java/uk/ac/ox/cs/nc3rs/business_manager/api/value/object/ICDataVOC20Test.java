/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object;

import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.NoSuchDataException;
import uk.ac.ox.cs.nc3rs.business_manager.api.util.ConverterUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Modifier;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICType;

/**
 * Unit test the ICData value object.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(ConverterUtil.class)
public class ICDataVOC20Test {

  private BigDecimal dummyValidC20Value = BigDecimal.ONE;
  private String dummyValidC20ValueColumnName = "fakeC20ValueColumnName";
  private Unit dummyValidC20Unit = Unit.uM;
  private String dummyValidC20Modifier = ">";
  private String dummyValidC20ModifierColumnName = "fakeC20ModifierColumnName";
  private Integer dummyValidC20RecordCount = 3;
  private String dummyValidC20RecordCountColumnName = "fakeC20RecordCountColumnName";
  private Integer dummyValidAllRecordCount = 4;
  private String dummyValidAllRecordCountColumnName = "fakeAllRecordCountColumnName";
  private BigDecimal dummyValidC20StdErr = BigDecimal.TEN;
  private String dummyValidC20StdErrColumnName = "fakeC20StdErrColumnName";
  private ICType dummyValidC20Type = ICType.IC20;

  @Test
  public void testMinimumAssignmentConstructor() throws IllegalArgumentException,
                                                        InvalidValueException, NoSuchDataException {
    ICDataVO testVO = new ICDataVO(dummyValidC20Value, dummyValidC20ValueColumnName,
                                   dummyValidC20Unit,
                                   dummyValidC20Modifier, dummyValidC20ModifierColumnName,
                                   dummyValidC20Type);
    assertFalse(testVO.hasIC50Source());
    assertTrue(testVO.hasICPctData(ICPercent.PCT20));
    assertFalse(testVO.hasPC50Source());

    try {
      testVO.isEquivalentPC50Negative(ICPercent.PCT20);
      fail("Not yet implemented!");
    } catch (UnsupportedOperationException e) {}
    assertFalse(testVO.isModifierEquals(ICPercent.PCT20));
    assertFalse(testVO.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT20));
    //testVO.isPotentialDose(ICPercent.PCT20, dose);
    assertNull(testVO.retrieveCountOfAvailableRecords(ICPercent.PCT20));
    assertNull(testVO.retrieveCountOfRecordsUsedToDetermineICValue(ICPercent.PCT20));
    assertTrue(Modifier.LESS.compareTo(testVO.retrieveEffectivePCModifier(ICPercent.PCT20)) == 0);
    try {
      testVO.retrieveEquivalentIC50Value(ICPercent.PCT20);
      fail("Not yet implemented!");
    } catch (UnsupportedOperationException e) {}
    try {
      testVO.retrieveEquivalentPC50Value(ICPercent.PCT20);
      fail("Not yet implemented!");
    } catch (UnsupportedOperationException e) {}
    assertNotNull(testVO.retrieveICData(ICPercent.PCT20));
    assertSame(dummyValidC20Value, testVO.retrieveOriginalICValue(ICPercent.PCT20));
    assertNotNull(testVO.retrieveOriginalInfo(ICPercent.PCT20));
    assertTrue(Modifier.GREATER.compareTo(testVO.retrieveTranslatedModifier(ICPercent.PCT20)) == 0);
    assertNull(testVO.retrieveOriginalStdErr(ICPercent.PCT20));

    assertFalse(testVO.hasICPctData(ICPercent.PCT50));
    try {
      testVO.isEquivalentPC50Negative(ICPercent.PCT50);
      fail("No such data!");
    } catch (NoSuchDataException e) {}
    try {
      testVO.isModifierEquals(ICPercent.PCT50);
      fail("No such data!");
    } catch (NoSuchDataException e) {}
    try {
      testVO.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50);
      fail("No such data!");
    } catch (NoSuchDataException e) {}
    //testVO.isPotentialDose(ICPercent.PCT50, dose);
    try {
      testVO.retrieveCountOfAvailableRecords(ICPercent.PCT50);
      fail("No such data!");
    } catch (NoSuchDataException e) {}
    try {
      testVO.retrieveCountOfRecordsUsedToDetermineICValue(ICPercent.PCT50);
      fail("No such data!");
    } catch (NoSuchDataException e) {}
    try {
      testVO.retrieveEffectivePCModifier(ICPercent.PCT50);
      fail("No such data!");
    } catch (NoSuchDataException e) {}
    try {
      testVO.retrieveEquivalentIC50Value(ICPercent.PCT50);
      fail("No such data!");
    } catch (NoSuchDataException e) {}
    try {
      testVO.retrieveEquivalentPC50Value(ICPercent.PCT50);
      fail("No such data!");
    } catch (NoSuchDataException e) {}
    try {
      testVO.retrieveICData(ICPercent.PCT50);
      fail("No such data!");
    } catch (NoSuchDataException e) {}
    try {
      testVO.retrieveOriginalICValue(ICPercent.PCT50);
      fail("No such data!");
    } catch (NoSuchDataException e) {}
    try {
      testVO.retrieveOriginalInfo(ICPercent.PCT50);
      fail("No such data!");
    } catch (NoSuchDataException e) {}
    try {
      testVO.retrieveOriginalStdErr(ICPercent.PCT50);
      fail("No such data!");
    } catch (NoSuchDataException e) {}

    assertNotNull(testVO.toString());
  }

  @Test
  public void testSingleConstructorFail() {
    final BigDecimal dummyNegativeC20Value = BigDecimal.ZERO.subtract(BigDecimal.ONE);
    try {
      new ICDataVO(dummyNegativeC20Value, dummyValidC20ValueColumnName, dummyValidC20Unit,
                   dummyValidC20Modifier, dummyValidC20ModifierColumnName, dummyValidC20RecordCount,
                   dummyValidC20RecordCountColumnName, dummyValidAllRecordCount,
                   dummyValidAllRecordCountColumnName, dummyValidC20StdErr,
                   dummyValidC20StdErrColumnName, dummyValidC20Type);
      fail("Should not permit the assignment of a negative IC20 value!");
    } catch (InvalidValueException e) {}
  }

  @Test
  public void testSingleConstructor() throws IllegalArgumentException, InvalidValueException,
                                             NoSuchDataException {
    ICDataVO testVO = new ICDataVO(dummyValidC20Value, dummyValidC20ValueColumnName,
                                   dummyValidC20Unit, dummyValidC20Modifier,
                                   dummyValidC20ModifierColumnName,
                                   dummyValidC20RecordCount, dummyValidC20RecordCountColumnName,
                                   dummyValidAllRecordCount, dummyValidAllRecordCountColumnName,
                                   dummyValidC20StdErr, dummyValidC20StdErrColumnName,
                                   dummyValidC20Type);
    assertFalse(testVO.hasIC50Source());
    assertTrue(testVO.hasICPctData(ICPercent.PCT20));
    assertFalse(testVO.hasPC50Source());

    try {
      testVO.isEquivalentPC50Negative(ICPercent.PCT20);
      fail("Not yet implemented!");
    } catch (UnsupportedOperationException e) {}
    assertFalse(testVO.isModifierEquals(ICPercent.PCT20));
    assertFalse(testVO.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT20));
    //testVO.isPotentialDose(ICPercent.PCT20, dose);
    assertSame(dummyValidAllRecordCount, testVO.retrieveCountOfAvailableRecords(ICPercent.PCT20));
    assertSame(dummyValidC20RecordCount, testVO.retrieveCountOfRecordsUsedToDetermineICValue(ICPercent.PCT20));
    assertTrue(Modifier.LESS.compareTo(testVO.retrieveEffectivePCModifier(ICPercent.PCT20)) == 0);
    try {
      testVO.retrieveEquivalentIC50Value(ICPercent.PCT20);
      fail("Not yet implemented!");
    } catch (UnsupportedOperationException e) {}
    try {
      testVO.retrieveEquivalentPC50Value(ICPercent.PCT20);
      fail("Not yet implemented!");
    } catch (UnsupportedOperationException e) {}
    assertNotNull(testVO.retrieveICData(ICPercent.PCT20));
    assertSame(dummyValidC20Value, testVO.retrieveOriginalICValue(ICPercent.PCT20));
    assertNotNull(testVO.retrieveOriginalInfo(ICPercent.PCT20));
    assertSame(dummyValidC20StdErr, testVO.retrieveOriginalStdErr(ICPercent.PCT20));

    assertNotNull(testVO.toString());
  }

  // Note : Also a test in ICDataVOC50Test
  @Test
  public void testRetrievePC50BasedOnIC20() throws IllegalArgumentException, InvalidValueException {
    // 1. uM IC20 data, no Hill value, conversion throws IllegalArgumentException.
    ICDataVO testVO = new ICDataVO(dummyValidC20Value, dummyValidC20ValueColumnName,
                                   dummyValidC20Unit, dummyValidC20Modifier,
                                   dummyValidC20ModifierColumnName,
                                   dummyValidC20RecordCount, dummyValidC20RecordCountColumnName,
                                   dummyValidAllRecordCount, dummyValidAllRecordCountColumnName,
                                   dummyValidC20StdErr, dummyValidC20StdErrColumnName,
                                   dummyValidC20Type);

    mockStatic(ConverterUtil.class);

    final BigDecimal dummyHillCoefficient = null;
    expect(ConverterUtil.convertIC20ToPC50(dummyValidC20Value, dummyHillCoefficient)).
           andThrow(new IllegalArgumentException ());

    replayAll();

    BigDecimal pct50pIC = testVO.retrievePC50BasedOnIC20(dummyHillCoefficient);

    verifyAll();

    assertNull(pct50pIC);

    resetAll();

    // 2. uM IC20 data, no Hill value, conversion throws InvalidValueException.
    expect(ConverterUtil.convertIC20ToPC50(dummyValidC20Value, dummyHillCoefficient)).
           andThrow(new InvalidValueException(null));

    replayAll();

    pct50pIC = testVO.retrievePC50BasedOnIC20(dummyHillCoefficient);

    verifyAll();

    assertNull(pct50pIC);

    resetAll();

    // 3. non-uM IC20 data, no Hill value.
    final Unit alternativeC20Unit = Unit.M;
    testVO = new ICDataVO(dummyValidC20Value, dummyValidC20ValueColumnName, alternativeC20Unit,
                          dummyValidC20Modifier, dummyValidC20ModifierColumnName,
                          dummyValidC20RecordCount, dummyValidC20RecordCountColumnName,
                          dummyValidAllRecordCount, dummyValidAllRecordCountColumnName,
                          dummyValidC20StdErr, dummyValidC20StdErrColumnName, dummyValidC20Type);

    final BigDecimal dummyConvertedC20Value = BigDecimal.TEN;
    expect(ConverterUtil.convert(alternativeC20Unit, Unit.uM, dummyValidC20Value))
           .andReturn(dummyConvertedC20Value);
    final BigDecimal dummyPct50pIC = new BigDecimal("4");
    expect(ConverterUtil.convertIC20ToPC50(dummyConvertedC20Value, dummyHillCoefficient)).
           andReturn(dummyPct50pIC);

    replayAll();

    pct50pIC = testVO.retrievePC50BasedOnIC20(dummyHillCoefficient);

    verifyAll();

    assertTrue(dummyPct50pIC.compareTo(pct50pIC) == 0);
  }
}