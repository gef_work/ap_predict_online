/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.helper;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.nc3rs.business_manager.api.TestUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.individual.MaxRespConcData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.maxrespconc.MaxRespConcRawDataSourceVO;

/**
 * Unit test the max. response and concentration data helper.
 *
 * @author geoff
 */
public class MaxRespDataHelperTest {

  private static final String dummyValidMaxRespValue = "34.3";
  private static final String dummyValidMaxRespConcValue = "0.00001";

  private DataRecord mockDataRecord;
  private IMocksControl mocksControl;
  private List<MaxRespConcRawDataSourceVO> dummyRawDataSources;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();

    mockDataRecord = mocksControl.createMock(DataRecord.class);

    final MaxRespConcRawDataSourceVO validMaxRespConcRawDataSourceVO = TestUtil.retrieveValidMaxRespConcUMRawDataSourceVO();
    dummyRawDataSources = new ArrayList<MaxRespConcRawDataSourceVO>();
    dummyRawDataSources.add(validMaxRespConcRawDataSourceVO);
  }

  @Test
  public void testMaxRespDataEligibilityFailureOnNullArgument() throws IllegalArgumentException,
                                                                       InvalidValueException {
    try {
      MaxRespDataHelper.eligibleMaxRespData(dummyRawDataSources, null);
      fail("Should not accept null dataRecord parameter");
    } catch (IllegalArgumentException e) {}

    try {
      MaxRespDataHelper.eligibleMaxRespData(null, mockDataRecord);
      fail("Should not accept null rawDataSources parameters");
    } catch (IllegalArgumentException e) {}

    try {
      MaxRespDataHelper.eligibleMaxRespData(new ArrayList<MaxRespConcRawDataSourceVO>(),
                                            mockDataRecord);
      fail("Should not accept empty rawDataSources parameter");
    } catch (IllegalArgumentException e) {}

  }

  @Test(expected=IllegalArgumentException.class)
  public void testMaxRespDataEligibilityFailsOnInvalidColumnName() throws IllegalArgumentException,
                                                                          InvalidValueException {
    expect(mockDataRecord.getRawDataValueByColumnName(TestUtil.dummyValidMaxRespColumnName))
          .andThrow(new IllegalArgumentException());

    mocksControl.replay();

    MaxRespDataHelper.eligibleMaxRespData(dummyRawDataSources, mockDataRecord);

    mocksControl.verify();
  }

  @Test(expected=InvalidValueException.class)
  public void testMaxRespDataEligibilityFailsOnInvalidColumnValue() throws IllegalArgumentException,
                                                                           InvalidValueException {
    expect(mockDataRecord.getRawDataValueByColumnName(TestUtil.dummyValidMaxRespColumnName))
          .andReturn("fish");
    expect(mockDataRecord.getRawDataValueByColumnName(TestUtil.dummyValidMaxRespConcColumnName))
          .andReturn(dummyValidMaxRespConcValue);

    mocksControl.replay();

    MaxRespDataHelper.eligibleMaxRespData(dummyRawDataSources, mockDataRecord);

    mocksControl.verify();
  }

  @Test
  public void testMaxRespDataEligibilitySuccessOnNullRawDataValues() throws IllegalArgumentException,
                                                                            InvalidValueException {
    expect(mockDataRecord.getRawDataValueByColumnName(TestUtil.dummyValidMaxRespColumnName))
          .andReturn(null);
    expect(mockDataRecord.getRawDataValueByColumnName(TestUtil.dummyValidMaxRespConcColumnName))
          .andReturn(dummyValidMaxRespConcValue);

    mocksControl.replay();

    List<MaxRespConcData> eligibleMaxRespData = MaxRespDataHelper.eligibleMaxRespData(dummyRawDataSources,
                                                                                      mockDataRecord);

    mocksControl.verify();

    assertTrue(eligibleMaxRespData.isEmpty());

    mocksControl.reset();

    expect(mockDataRecord.getRawDataValueByColumnName(TestUtil.dummyValidMaxRespColumnName))
          .andReturn(dummyValidMaxRespValue);
    expect(mockDataRecord.getRawDataValueByColumnName(TestUtil.dummyValidMaxRespConcColumnName))
          .andReturn(null);

    mocksControl.replay();

    eligibleMaxRespData = MaxRespDataHelper.eligibleMaxRespData(dummyRawDataSources, mockDataRecord);

    mocksControl.verify();

    assertTrue(eligibleMaxRespData.isEmpty());
  }

  @Test
  public void testMaxRespDataEligibility() throws IllegalArgumentException, InvalidValueException {
    // Default uM units.
    expect(mockDataRecord.getRawDataValueByColumnName(TestUtil.dummyValidMaxRespColumnName))
          .andReturn(dummyValidMaxRespValue);
    // The max resp conc value will be converted from uM to M for the VO
    expect(mockDataRecord.getRawDataValueByColumnName(TestUtil.dummyValidMaxRespConcColumnName))
          .andReturn(dummyValidMaxRespConcValue);

    mocksControl.replay();

    List<MaxRespConcData> eligibleMaxRespData = MaxRespDataHelper.eligibleMaxRespData(dummyRawDataSources,
                                                                                      mockDataRecord);

    mocksControl.verify();

    assertTrue(1 == eligibleMaxRespData.size());
    MaxRespConcData thisEligibleMaxRespData = eligibleMaxRespData.get(0);
    assertTrue(new BigDecimal(dummyValidMaxRespValue).compareTo(thisEligibleMaxRespData.getMaxResp()) == 0);
    assertEquals(TestUtil.dummyValidMaxRespColumnName, thisEligibleMaxRespData.getMaxRespColumnName());
    assertTrue(new BigDecimal(dummyValidMaxRespConcValue).compareTo(thisEligibleMaxRespData.getMaxRespConc(TestUtil.dummyValidMaxRespConcUMUnit)) == 0);
    assertEquals(TestUtil.dummyValidMaxRespConcColumnName, thisEligibleMaxRespData.getMaxRespConcColumnName());

    mocksControl.reset();

    // Try with M units.
    dummyRawDataSources.clear();
    dummyRawDataSources.add(TestUtil.retrieveValidMaxRespConcMRawDataSourceVO());

    expect(mockDataRecord.getRawDataValueByColumnName(TestUtil.dummyValidMaxRespColumnName))
         .andReturn(dummyValidMaxRespValue);
    // No conversion of max resp conc value for storage in VO
    expect(mockDataRecord.getRawDataValueByColumnName(TestUtil.dummyValidMaxRespConcColumnName))
         .andReturn(dummyValidMaxRespConcValue);

    mocksControl.replay();

    eligibleMaxRespData = MaxRespDataHelper.eligibleMaxRespData(dummyRawDataSources,
                                                                mockDataRecord);

    mocksControl.verify();

    assertTrue(1 == eligibleMaxRespData.size());
    thisEligibleMaxRespData = eligibleMaxRespData.get(0);
    assertTrue(new BigDecimal(dummyValidMaxRespValue).compareTo(thisEligibleMaxRespData.getMaxResp()) == 0);
    assertEquals(TestUtil.dummyValidMaxRespColumnName, thisEligibleMaxRespData.getMaxRespColumnName());
    assertTrue(new BigDecimal(dummyValidMaxRespConcValue).compareTo(thisEligibleMaxRespData.getMaxRespConc(TestUtil.dummyValidMaxRespConcMUnit)) == 0);
    assertEquals(TestUtil.dummyValidMaxRespConcColumnName, thisEligibleMaxRespData.getMaxRespConcColumnName());
  }
}