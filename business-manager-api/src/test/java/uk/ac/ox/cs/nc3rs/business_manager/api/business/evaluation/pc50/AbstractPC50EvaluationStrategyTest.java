/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * Test the abstracted functionality involved in PC50 evaluation strategies.
 *
 * @author geoff
 */
public class AbstractPC50EvaluationStrategyTest {

  private class TestPC50EvaluationStrategy extends AbstractPC50EvaluationStrategy {

    private static final long serialVersionUID = 1L;

    public static final boolean defaultUsesEqualityModifier = true;

    protected TestPC50EvaluationStrategy(final String description, final boolean defaultActive,
                                         final int defaultInvocationOrder) {
      super(description, defaultActive, defaultInvocationOrder);
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy#usesEqualityModifier()
     */
    @Override
    public boolean usesEqualityModifier() {
      return defaultUsesEqualityModifier;
    }
  }

  @Test
  public void testConstructor() {
    String dummyDescription = null;
    boolean dummyDefaultActive = true;
    int dummyDefaultInvocationOrder = 3;

    PC50EvaluationStrategy pc50EvaluationStrategy = null;
    try {
      new TestPC50EvaluationStrategy(dummyDescription, dummyDefaultActive,
                                     dummyDefaultInvocationOrder);
      fail("Should not permit the assignment of a null description in constructor.");
    } catch (IllegalArgumentException e) {}

    dummyDescription = "fakeDescription";
    pc50EvaluationStrategy = new TestPC50EvaluationStrategy(dummyDescription, dummyDefaultActive,
                                                            dummyDefaultInvocationOrder);

    assertTrue(pc50EvaluationStrategy instanceof PC50EvaluationStrategy);
    assertTrue(dummyDescription.equalsIgnoreCase(pc50EvaluationStrategy.getDescription()));
    assertTrue(pc50EvaluationStrategy.getDefaultActive());
    assertSame(dummyDefaultInvocationOrder, pc50EvaluationStrategy.getOrder());
    assertSame(TestPC50EvaluationStrategy.defaultUsesEqualityModifier,
               pc50EvaluationStrategy.usesEqualityModifier());
  }
}