/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import org.junit.Test;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 * Unit test the assay spread value object.
 *
 * @author geoff
 */
public class AssaySpreadVOTest {

  @Test
  public void testVO() {
    try {
      new AssaySpreadVO(null, null, null, null);
      fail("Should not permit the assignment of a null ion channel value");
    } catch (IllegalArgumentException e) {}

    final IonChannel dummyValidIonChannel = IonChannel.CaV1_2;
    AssaySpreadVO testVO = new AssaySpreadVO(dummyValidIonChannel, null, null, null);
    assertNotNull(testVO);
    assertNotNull(testVO.toString());
    assertSame(dummyValidIonChannel, testVO.getIonChannel());
    assertNull(testVO.getSpreadHill());
    assertNull(testVO.getSpreadIC50());
    assertNull(testVO.getSpreadPIC50());

    final BigDecimal dummySpreadIC50 = BigDecimal.ZERO;
    final BigDecimal dummySpreadPIC50 = BigDecimal.ONE;
    final BigDecimal dummySpreadHill = BigDecimal.TEN;
    testVO = new AssaySpreadVO(dummyValidIonChannel, dummySpreadIC50, dummySpreadPIC50,
                               dummySpreadHill);
    assertTrue(dummySpreadHill.compareTo(testVO.getSpreadHill()) == 0);
    assertTrue(dummySpreadIC50.compareTo(testVO.getSpreadIC50()) == 0);
    assertTrue(dummySpreadPIC50.compareTo(testVO.getSpreadPIC50()) == 0);
  }
}