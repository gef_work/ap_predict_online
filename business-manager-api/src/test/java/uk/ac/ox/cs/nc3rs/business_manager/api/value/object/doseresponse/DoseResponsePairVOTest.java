/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.business_manager.api.value.object.doseresponse;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import org.junit.Test;

import uk.ac.ox.cs.nc3rs.business_manager.api.util.ConverterUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;

/**
 * Unit test the dose-response pair value object.
 *
 * @author geoff
 */
public class DoseResponsePairVOTest {

  private final BigDecimal dummyValidDose = BigDecimal.ONE;
  private final BigDecimal dummyValidResponse = BigDecimal.TEN;

  @Test
  public void testConstructorExceptionsThrown() {
    try {
      new DoseResponsePairVO(null, BigDecimal.ONE);
      fail("Constructor should not permit assignment of null dose");
    } catch (IllegalArgumentException e) {}

    try {
      new DoseResponsePairVO(BigDecimal.ONE, null);
      fail("Constructor should not permit assignment of null response");
    } catch (IllegalArgumentException e) {}

    try {
      new DoseResponsePairVO(BigDecimal.ZERO.subtract(BigDecimal.ONE), BigDecimal.ONE);
      fail("Constructor should not permit assignment of negative dose");
    } catch (IllegalArgumentException e) {}
  }

  @Test
  public void testConstructorSuccess() {
    // Try with a negative response
    final BigDecimal dummyValidNegativeResponse = BigDecimal.ZERO.subtract(BigDecimal.ONE);
    DoseResponsePairVO doseResponsePair = new DoseResponsePairVO(dummyValidDose,
                                                                 dummyValidNegativeResponse);
    assertTrue(dummyValidDose.compareTo(doseResponsePair.getDose()) == 0);
    assertTrue(dummyValidNegativeResponse.compareTo(doseResponsePair.getResponse()) == 0);

    doseResponsePair = new DoseResponsePairVO(dummyValidDose, dummyValidResponse);
    assertTrue(dummyValidDose.compareTo(doseResponsePair.getDose()) == 0);
    assertTrue(dummyValidResponse.compareTo(doseResponsePair.getResponse()) == 0);

    assertNotNull(doseResponsePair.toString());
  }

  @Test
  public void testDoseRetrievalByUnit() {
    final DoseResponsePairVO doseResponsePair = new DoseResponsePairVO(dummyValidDose,
                                                                       dummyValidResponse);
    assertTrue(dummyValidDose.compareTo(doseResponsePair.getDose(Unit.uM)) == 0);
    assertTrue(dummyValidDose.divide(ConverterUtil.MILLION).compareTo(doseResponsePair.getDose(Unit.M)) == 0);
  }
}