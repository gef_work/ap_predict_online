# Action Potential prediction -- Business Manager API

Defines objects which `business_manager` and `site business` share.

## Installation

Please see either of the following :

  1. http://apportal.readthedocs.io/en/latest/installation/components/business-manager-api/index.html
  1. ap_predict_online/docs/AP-Portal/RtD/_build/html/installation/components/business-manager-api/index.html

## Developer information

 1. Implement your own [SiteDAOImpl](https://bitbucket.org/gef_work/ap_predict_online/src/master/site-business/src/main/java/uk/ac/ox/cs/epsrc/site_business/dao/SiteDAOImpl.java "Example SiteDAOImpl.java") which extends [SiteDAO](https://bitbucket.org/gef_work/ap_predict_online/src/master/business-manager-api/src/main/java/uk/ac/ox/cs/nc3rs/business_manager/api/business/dao/SiteDAO.java "SiteDAO.java"), using raw data holders such as [CSIndividualDataRecordVO](https://bitbucket.org/gef_work/ap_predict_online/src/master/site-business/src/main/java/uk/ac/ox/cs/epsrc/site_business/value/object/datarecord/individual/CSIndividualDataRecordVO.java "Example IndividualDataRecord implementation"). The assumed structure
of a site's different compound data is of the form; "Summary" [1]<->[\*] "Individual" [1]<->[\*] 
"Dose/Response" (although the presence of summary data is not enforced, it is optional).
 1. UML diagrams are available in the [UML directory](https://bitbucket.org/gef_work/ap_predict_online/src/master/docs/AP-Portal/UML "UML directory").