/*
 * HillFunction.hpp
 *
 *  Created on: 9 Jan 2012
 *      Author: kybea
 */

#ifndef HILLFUNCTION_HPP_
#define HILLFUNCTION_HPP_

#include <vector>

/**
 * A class that will evaluate an error metric for the match between a Hill function with the specified parameters,
 * and an experimental dose-response curve.
 */
class HillFunction
{
public:
    /**
     * HillFunction constructor
     *
     * @param low  The lowest possible Hill coefficient to assign in the fitting process.
     * @param high  The highest possible Hill coefficient to assign in the fitting process.
     */
	HillFunction(double low, double high);

	/**
	 * To set penalty by which negative and too high hill coefficients will be penalised
	 * @param penalty_value
	 */
	void SetPenalty(double penalty_value);

	/**
	 * Method taking in vector of current parameter values to evaluate error associated with this parameter combination
	 * passed from Nelder-Mead Simplex algorithm after fitting to appropriate hill function (fitting for 1 or 2 parameters,
	 * just IC50 or IC50 and hill coefficient) depending on number of data points input.
	 *
	 * @param rParameters the parameters with which to perform the error function evaluation.
	 * @return Error metric
	 */
	double Evaluate(const std::vector<double>& rParameters);

	/**
	 * Methods to set concentrations as observed in experimental data.
	 * Entries of the vector should have a 1:1 correspondence with inhibitions.
	 *
	 * @param rConcentrations  the doses or concentrations at which responses/inhibitions were recorded.
	 * @param rInhibitions  the % inhibitions that were recorded in the experiment.
	 */
	void SetConcentrationsAndInhibitions(std::vector<double>& rConcentrations, std::vector<double>& rInhibitions);

private:
	// Member variables used in the class

	/** Amount by which a Hill coefficient parameter going out of bounds is penalised - can be set by user.*/
	double mPenalty;
	/** A vector of concentrations at which experimental data points were taken*/
	std::vector<double> mConcentrations;
	/** A vector of inhibitions (expressed as %ages of control current) recorded experimentally.*/
	std::vector<double> mInhibitions;

	/** The maximum value a Hill coefficient should take in the fitting process (defaults to 5 in constructor). */
	double mMinHill;
	/** The minimum value a Hill coefficient should take in the fitting process (defaults to 0 in constructor). */
	double mMaxHill;

};

#endif /* HILLFUNCTION_HPP_ */
