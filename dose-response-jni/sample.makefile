JAVA_HOME = /usr/java/current
BOOST_HOME = /home/chaste/build1/chaste_libs/include
JSON_HOME = /home/user
IDE_PATH  = .
SRCDIR    = $(IDE_PATH)/src
TARGETDIR = $(IDE_PATH)/target
TESTDIR   = $(IDE_PATH)/test

CPPFLAGS  = -I$(JAVA_HOME)/include -I$(JAVA_HOME)/include/linux -I$(JSON_HOME)/include -I$(BOOST_HOME) -I$(TARGETDIR) -L$(JSON_HOME)/lib -ljson

all: clean copy libfittingdoseresponse.so test jar

jar: test
	$(JAVA_HOME)/bin/jar cvf $(TARGETDIR)/dose-response-fitter.jar -C $(TARGETDIR) uk/ac/ox/cs/epsrc/jni/DoseResponseFitter.class

test: libfittingdoseresponse.so
	$(JAVA_HOME)/bin/javac -cp $(TARGETDIR) $(TESTDIR)/JNITests.java
	LD_LIBRARY_PATH=$(TARGETDIR):$(JSON_HOME)/lib $(JAVA_HOME)/bin/java -cp $(TESTDIR):$(TARGETDIR) JNITests

libfittingdoseresponse.so: DoseResponseFitter.h WSDoseResponseFitter.cpp
	g++ -shared -Wall -fPIC -DNDEBUG $(IDE_PATH)/WSDoseResponseFitter.cpp $(IDE_PATH)/RunHillFunctionMinimization.cpp $(IDE_PATH)/HillFunction.cpp $(IDE_PATH)/NelderMeadMinimizer.cpp -o $(TARGETDIR)/libfittingdoseresponse.so $(CPPFLAGS)

# For javah the -jni flag is set by default
DoseResponseFitter.h: DoseResponseFitter.class
	$(JAVA_HOME)/bin/javah -d $(TARGETDIR) -classpath $(TARGETDIR) uk.ac.ox.cs.epsrc.jni.DoseResponseFitter

DoseResponseFitter.class:
	$(JAVA_HOME)/bin/javac -cp $(TARGETDIR) $(TARGETDIR)/uk/ac/ox/cs/epsrc/jni/DoseResponseFitter.java

copy:
	cp *.cpp $(TARGETDIR)/
	cp *.hpp $(TARGETDIR)/
	cp -r $(SRCDIR)/* $(TARGETDIR)/

clean:
	mkdir -p $(TARGETDIR)
	rm -rf $(TARGETDIR)/*
	rm -f $(TESTDIR)/*.class
