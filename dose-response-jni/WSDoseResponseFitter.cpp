#include "RunHillFunctionMinimization.hpp"
#include <iostream>
#include <string>
#include <cassert>
#include <vector>
#include <cmath>
#include <boost/assign/list_of.hpp>

#include "libjson/libjson.h"

//*****************************************************************
//This is the bit that permits the JNI invocation.

#include <syslog.h>
#include <jni.h>
#include "uk_ac_ox_cs_epsrc_jni_DoseResponseFitter.h"

// Debugging printout
void ShowVectorValues(const std::string name, std::vector<double> & values);
// Takes the JSON node and pushes its values into the vector
void LoadInputValues(const JSONNode & n, std::vector<double> & values);
// Receives the parsed incoming JSON and populates the other vars passed in with it.
void ParseJSON(const JSONNode & parsedjsoninput,
               std::vector<double> & concentrations,
               std::vector<double> & inhibitions,
               unsigned & parameterfitting,
               bool & rounded,
               float & hillmin,
               float & hillmax);

//*****************************************************************

const std::string IC50_ONLY_NAME = "IC50_ONLY";
const std::string IC50_AND_HILL_NAME = "IC50_AND_HILL";

const unsigned IC50_ONLY = 1u;
const unsigned IC50_AND_HILL = 2u;

const bool ROUND_VALUES = true;
const bool DONT_ROUND_VALUES = false;

const unsigned DEFAULT_PARAMETER_FITTING = IC50_ONLY;
const bool DEFAULT_VALUE_ROUNDING = DONT_ROUND_VALUES;
const float DEFAULT_HILL_MIN = 0.0;
const float DEFAULT_HILL_MAX = 0.0;

jstring Java_uk_ac_ox_cs_epsrc_jni_DoseResponseFitter_fitDoseResponse(JNIEnv *env, jobject obj, jstring jsonvalues) {
  const char *pjsoninput = env->GetStringUTFChars(jsonvalues, 0);

  //printf("Incoming arg is '%s'\n", pjsoninput);

  JSONNode parsedjsoninput = libjson::parse(pjsoninput);

  std::vector<double> concentrations;
  std::vector<double> inhibitions;
  unsigned parameterfitting = DEFAULT_PARAMETER_FITTING;
  bool valuerounding = DEFAULT_VALUE_ROUNDING;
  float hillmin = DEFAULT_HILL_MIN;
  float hillmax = DEFAULT_HILL_MAX;

  //std::cout << "Default parameter fitting " << parameterfitting << '\n';
  //std::cout << "Default value rounding    " << valuerounding << '\n';
  //std::cout << "Default hill min          " << hillmin << '\n';
  //std::cout << "Default hill max          " << hillmax << '\n';

  ParseJSON(parsedjsoninput, concentrations, inhibitions, parameterfitting, valuerounding, hillmin, hillmax);

  //ShowVectorValues("concentrations", concentrations);
  //ShowVectorValues("inhibitions", inhibitions);

  //std::cout << "Parsed parameter fitting " << parameterfitting << '\n';
  //std::cout << "Parsed value rounding    " << valuerounding << '\n';
  //std::cout << "Parsed hill min          " << hillmin << '\n';
  //std::cout << "Parsed hill max          " << hillmax << '\n';

  env->ReleaseStringUTFChars(jsonvalues, pjsoninput);

  RunHillFunctionMinimization compoundValues (concentrations, inhibitions, parameterfitting, valuerounding);

  if ((DEFAULT_HILL_MIN != hillmin) || (DEFAULT_HILL_MAX != hillmax)) {
    //std::cout << "Setting hill limits" << '\n';
    compoundValues.SetHillLimits(hillmin, hillmax);
  }

  std::vector<double> results = compoundValues.Run();

  if (parameterfitting == IC50_ONLY) {
    // fill in a Hill Coefficient of 1
    results.push_back(1.0);
  }

  JSONNode jsonNode(JSON_NODE);
  jsonNode.push_back(JSONNode("IC50", results[0]));
  jsonNode.push_back(JSONNode("HillCoefficient", results[1]));
  std::string stdresponse = jsonNode.write_formatted();
  const char* response = stdresponse.c_str();

  //printf("Outgoing response is '%s'\n", response);

  return env->NewStringUTF(response);
}

//*****************************************************************

void ShowVectorValues(const std::string name, std::vector<double> & values) {
  std::vector <double>::iterator itr;
  std::cout << name << ':' << '\t';
  for (itr = values.begin(); itr != values.end(); ++itr)
    std::cout << *itr << '\t';
  std::cout << std::endl;
}

void LoadInputValues(const JSONNode & jsonNode, std::vector<double> & values) {
  JSONNode::const_iterator arriterator = jsonNode.begin();
  while (arriterator != jsonNode.end()) {
    //printf("Nameless '%s' as string\n", arriterator -> as_string().c_str());
    values.push_back(arriterator -> as_float());
    arriterator++;
  }
}

void ParseJSON(const JSONNode & parsedjsoninput,
               std::vector<double> & concentrations,
               std::vector<double> & inhibitions,
               unsigned & parameterfitting,
               bool & valuerounding,
               float & hillmin,
               float & hillmax){

  JSONNode::const_iterator conciterator = parsedjsoninput.find("concentrations");
  while (conciterator != parsedjsoninput.end()) {
    std::string node_name = conciterator -> name();
    if (node_name == "concentrations")
      LoadInputValues(*conciterator, concentrations);
    conciterator++;
  }

  JSONNode::const_iterator inhibiterator = parsedjsoninput.find("inhibitions");
  while (inhibiterator != parsedjsoninput.end()) {
    std::string node_name = inhibiterator -> name();
    if (node_name == "inhibitions")
      LoadInputValues(*inhibiterator, inhibitions);
    inhibiterator++;
  }

  JSONNode::const_iterator parameterfittingiterator = parsedjsoninput.find("parameterfitting");
  while (parameterfittingiterator != parsedjsoninput.end()) {
    std::string node_name = parameterfittingiterator -> name();
    if (node_name == "parameterfitting") {
      //std::cout << "parameterfitting found" << '\n';  

      std::string parameterfittingvalue = parameterfittingiterator -> as_string();
      if (IC50_ONLY_NAME == parameterfittingvalue) {
        parameterfitting = IC50_ONLY;
      } else if (IC50_AND_HILL_NAME == parameterfittingvalue) {
        parameterfitting = IC50_AND_HILL;
      } else {
        std::cout << "Error : Unrecognized parameterfitting value of '" << parameterfittingvalue << "'" << '\n'; 
      }
    }
    parameterfittingiterator++;
  }

  JSONNode::const_iterator roundediterator = parsedjsoninput.find("rounded");
  while (roundediterator != parsedjsoninput.end()) {
    std::string node_name = roundediterator -> name();
    if (node_name == "rounded") {
      //std::cout << "rounded found" << '\n';

      std::string roundedvalue = roundediterator -> as_string();
      if ((roundedvalue == "true") || (roundedvalue == "1")) {
        valuerounding = ROUND_VALUES;
      } else if ((roundedvalue == "false") || (roundedvalue == "0")) {
        valuerounding = DONT_ROUND_VALUES;
      } else {
        std::cout << "Error : Unrecognized rounded value of '" << roundedvalue << "'" << '\n';
      }
    }
    roundediterator++;
  }

  JSONNode::const_iterator hillminiterator = parsedjsoninput.find("hillmin");
  while (hillminiterator != parsedjsoninput.end()) {
    std::string node_name = hillminiterator -> name();
    if (node_name == "hillmin") {
      //std::cout << "hill min found" << '\n';

      hillmin = hillminiterator -> as_float();
    }
    hillminiterator++;
  }

  JSONNode::const_iterator hillmaxiterator = parsedjsoninput.find("hillmax");
  while (hillmaxiterator != parsedjsoninput.end()) {
    std::string node_name = hillmaxiterator -> name();
    if (node_name == "hillmax") {
      //std::cout << "hill max found" << '\n';

      hillmax = hillmaxiterator -> as_float();
    }
    hillmaxiterator++;
  }
}
