# Action Potential predict -- Dose-Response JNI

This is the step which converts C++ code into Java objects so that `dose-response-manager` can
call it to process dose-response data.

## Installation

Please see either of the following :

 1. README.build
 1. http://apportal.readthedocs.io/en/latest/installation/components/dose-response-jni/index.html
 1. ap_predict_online/docs/AP-Portal/RtD/_build/html/installation/components/dose-response-jni/index.html