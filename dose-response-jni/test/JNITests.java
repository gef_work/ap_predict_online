import uk.ac.ox.cs.epsrc.jni.DoseResponseFitter; 

class JNITests {

  private static final String IC50_ONLY = "IC50_ONLY";
  private static final String IC50_AND_HILL = "IC50_AND_HILL";

  static {
    System.loadLibrary("fittingdoseresponse");
  }

  public static void main(final String[] args) {
    System.out.println("\n vvv Test output vvv\n");
    final DoseResponseFitter doseResponseFitter = new DoseResponseFitter();

    final String concentrations = "0.1,0.3";
    final String inhibitions = "4.4,5.5"; 
    final String compute = IC50_ONLY;
    final String rounded = "true"; 
    final String hillMin = "0.5";
    final String hillMax = "2.0";

    final String fdrInputs = "{ \"concentrations\" : [" + concentrations + "]," +
                               "\"inhibitions\" : [" + inhibitions + "]," +
                               "\"compute\" : \"" + compute + "\"," +
                               "\"rounded\" : " + rounded + "," +
                               "\"hillmin\" : " + hillMin + "," +
                               "\"hillmax\" : " + hillMax + "}";

    doseResponseFitter.fitDoseResponse(fdrInputs);    
    System.out.println("\n ^^^ Finished ^^^\n");
  }
}
