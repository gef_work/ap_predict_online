/*
 * NelderMeadMinimizer.hpp
 *
 *  Created on: 9 Jan 2012
 *      Author: kybea
 */

#ifndef NELDERMEADMINIMIZER_HPP_
#define NELDERMEADMINIMIZER_HPP_
#include "HillFunction.hpp"
#include <vector>

class NelderMeadMinimizer
{
public:

	/**
	 * Constructor which accepts input parameter value vector
	 * passed by reference and pointer to appropriate hill function to be used
	 */
	NelderMeadMinimizer(std::vector<double>& rParameters, HillFunction* p_hill_function);

	/**
	 * To set maximum number of iterations Nelder-Mead algorithm can take
	 */
	void SetMaxNumIterations(unsigned numIterations)
	{
		mMaxNumIterations = numIterations;
	}

	/**
	 * @return the number of function evaluations that were performed.
	 */
	unsigned GetNumEvaluations(void)
	{
	    return mNumFunctionEvaluations;
	}

	/**
	 * To set tolerance specified for acceptance of parameters yielding minimum error
	 *
	 * @param tolerance  the tolerance of ?? at which to stop iterating.
	 */
	void SetTolerance(double tolerance)
	{
		mTolerance = tolerance;
	}

	/**
	 * To set reflection coefficient used in Nelder-Mead algorithm
	 *
	 * @param reflectionCoefficient  The reflection coefficient to use.
	 */
	void SetReflectionCoeff(double reflectionCoefficient)
	{
		mReflectionCoefficient = reflectionCoefficient;
	}

	/**
	 * To set expansion coefficient used in Nelder-Mead algorithm
	 *
	 * @param expansionCoefficient  The expansion coefficient to use.
	 */
	void SetExpansionCoeff(double expansionCoefficient)
	{
		mExpansionCoefficient = expansionCoefficient;
	}

	/**
	 * To set contraction coefficient used in Nelder-Mead algorithm
	 *
	 * @param contrationCoefficient  The contraction coefficient to use.
	 */
	void SetContractionCoeff(double contrationCoefficient)
	{
		mContractionCoefficient = contrationCoefficient;
	}

	/**
	 * To set shrink coefficient used in Nelder-Mead algorithm
	 *
	 * @param shrinkCoefficient  The shrink coefficient to use.
	 */
	void SetShrinkCoeff(double shrinkCoefficient)
	{
		mShrinkCoefficient = shrinkCoefficient;
	}

	/**
	 * Allows information on iterations and convergence to be output to screen
	 *
	 * @param display Whether to print information on the iterations to std::cout
	 */
	void SetDisplayIterations(bool display=true)
	{
	    mDisplayIterations = display;
	}

	/**
	 * Method in which minimization algorithm using the Nelder-Mead simplex method is implemented
	 * takes in parameters, calculates new parameters based on rules described in algorithm and
	 * and then passes this to hill function to calculate error associated with fitting with this
	 * parameters. This is repeated until the returned error for parameter combination converges.
	 */
	void Minimize();

private:
	//Reference to vector of parameter values to be passed around
	std::vector<double>& mrParameters;

	//Pointer to appropriate hill function (fitting for one or 2 parameters depending on number of input data points)
	HillFunction* mpFunctionToMinimise;

	//member variables used in the methods in class
	unsigned mNumParameters;

	//member variables which can be set by the user. If not set values set as default in constructor are used
	unsigned mMaxNumIterations;
	double mReflectionCoefficient;
	double mTolerance;
	double mContractionCoefficient;
	double mExpansionCoefficient;
	double mShrinkCoefficient;

	bool mDisplayIterations;

	/** Keep track of the number of function evaluations we perform */
	unsigned mNumFunctionEvaluations;

};

#endif /* NELDERMEADMINIMIZER_HPP_ */
