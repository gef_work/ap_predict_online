/*
 * HillFunction.cpp
 *
 *  Created on: 9 Jan 2012
 *      Author: kybea
 */

#include <cmath>
#include <cassert>
#include <vector>
#include "HillFunction.hpp"

//HillFunction constructor with default variables for initialisation
HillFunction::HillFunction(double low, double high)
    : mPenalty(1e10),
      mMinHill(low),
      mMaxHill(high)
{
}

//To set penalty by which negative and too high hill coefficients will be penalised
void HillFunction::SetPenalty(double penalty_value)
{
    mPenalty = penalty_value;
}

//Methods to set concentrations and inhibitions from initial input data
void HillFunction::SetConcentrationsAndInhibitions(std::vector<double>& rConcentrations, std::vector<double>& rInhibitions)
{
    assert(rConcentrations.size()==rInhibitions.size());
    mConcentrations = rConcentrations;
    mInhibitions = rInhibitions;
}

// Evaluates error after fitting to appropriate hill function with parameters passed from
// Nelder-Mead algorithm used

double HillFunction::Evaluate(const std::vector<double>& rParameters)
{
	//Assigning hill coefficient appropriately depending on whether we are fitting for 1 or 2 parameters

	double hill_coefficient = 1.0;
	if (rParameters.size() > 1)
	{
		hill_coefficient = rParameters[1];
	}

	double cumerror = 0; // Cumulative error

	for (unsigned i=0; i<mConcentrations.size(); i++)
	{
		double expected_inhibition = (100.0/(1.0+(pow((rParameters[0]/mConcentrations[i]),hill_coefficient))));

		double error = expected_inhibition - mInhibitions[i];

		cumerror += error*error;
	}

    // Penalising error for negative ic50 according to specified penalty
    if (rParameters[0] < 0)
    {
        cumerror -= mPenalty*rParameters[0];
    }

    if (rParameters.size() > 1) // If we are fitting Hill coefficient
    {
        // Penalising error for too small a hill coefficient according to specified penalty
        if (hill_coefficient < mMinHill)
        {
            cumerror += mPenalty*(mMinHill - hill_coefficient);
        }

        //Penalising for too high a hill coefficient according to specified penalty
        if(hill_coefficient > mMaxHill)
        {
            cumerror += mPenalty*(hill_coefficient - mMaxHill);
        }
    }

	return (cumerror);
}
