/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.jni;

/**
 * The Java Native Interface class which in this case allows Java classes to
 * call the C++ dose-response fitting code compiled as a shared object library.
 * <p />
 * In brief: This class is translated into a <code>.h</code> file, e.g.
 * <code>uk_ac_ox_cs_epsrc_jni_DoseResponseFitter.h</code, which is used by
 * <code>WSDoseResponseFitter.cpp</code> which is the C++ code which invokes
 * the C++ dose-response fitting code compiled as an <code>.so</code> file.
 *
 * @author Geoff Williams
 */
public class DoseResponseFitter {

  /**
   * The method which the Java system uses to call C++ code.
   *
   * @param jsonValues The JSON string which contains the values to fit.
   * @return The results of the fitting, e.g. IC50, Hill Coefficient.
   */
  public native String fitDoseResponse(String jsonValues);

  /**
   * This is the C++ compiled .so (shared object) library name which will be
   * called and which will need to be found by this compiled Java class.
   * The actual .so file name sought, by convention, will be called
   * <code>libfittingdoseresponse.so</code>.
   * <p />
   * (I think on windows systems the equivalent would be 
   * <code>fittingdoseresponse.dll</code>!)
   * <p />
   * Generally the finding of the .so / .dll file can be achieved by setting 
   * the <code>java.library.path</code>, e.g. for <code>bash</code> shell :
   * <code>export JAVA_OPTS="-Djava.library.path=shared/lib"</code> or
   * similar.
   */
  static {
    System.loadLibrary("fittingdoseresponse");
  }
}
