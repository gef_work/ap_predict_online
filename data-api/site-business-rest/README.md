# Action Potential prediction -- Data API-calling Site Business

Calls a "Data API", e.g. `rest-data-api` and provides a bit of site business logic,
placing compound data into `business-manager-api` defined objects which
`business-manager-skeleton` can subsequently process.