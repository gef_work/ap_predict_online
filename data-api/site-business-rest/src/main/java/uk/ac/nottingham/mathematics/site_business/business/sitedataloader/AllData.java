/*

  Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Nottingham nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.nottingham.mathematics.site_business.business.sitedataloader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.annotation.Header;
import org.springframework.web.client.RestTemplate;

import uk.ac.nottingham.mathematics.site_business.SiteIdentifiers;
import uk.ac.nottingham.mathematics.site_business.manager.DataProcessor;
import uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata.ProcessedSiteDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.APIIdentifiers;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;

/**
 * Component which loads the site's data into a generic format for onward
 * processing.
 * 
 * @author geoff
 */
public class AllData {

  @Autowired @Qualifier(SiteIdentifiers.COMPONENT_DATA_PROCESSOR)
  private DataProcessor dataProcessor;

  @Autowired @Qualifier(APIIdentifiers.COMPONENT_ASSAY_QSAR)
  private AssayVO qsarAssay;

  private final RestTemplate dataAPIRestTemplate;
  private final String dataAPIURL;

  private static final Log log = LogFactory.getLog(AllData.class);

  /**
   * Initialising constructor.
   * 
   * @param dataAPIRestTemplate Data API REST template.
   * @param dataAPIURL Data API URL
   */
  public AllData(final RestTemplate dataAPIRestTemplate,
                 final String dataAPIURL) {
    this.dataAPIRestTemplate = dataAPIRestTemplate;
    this.dataAPIURL = dataAPIURL;
  }

  /**
   * Read in the site's data for onward processing.
   * 
   * @param compoundIdentifier Compound name.
   * @return Aggregated site data for the compound.
   */
  // method name specified in appCtx.proc.site.dataapi.xml
  public ProcessedSiteDataVO loadAllData(final @Header(required=true,
                                                        value=APIIdentifiers.SI_HDR_COMPOUND_IDENTIFIER)
                                                String compoundIdentifier) {
    final String identifyingPrefix = "~loadAllData() : [" + compoundIdentifier + "] : ";
    log.debug(identifyingPrefix.concat("Invoked."));

    final Map<String, String> urlVariables = new HashMap<String, String>();
    urlVariables.put(SiteIdentifiers.REST_API_URL_VAR_COMPOUND_ID,
                     compoundIdentifier);

    ProcessedSiteDataVO processedSiteData = null;

    String result = null;
    try {
      result = dataAPIRestTemplate.getForObject(dataAPIURL, String.class,
                                                urlVariables);
    } catch (Exception e) {
      final List<String> problems = new ArrayList<String>();
      problems.add(e.getMessage());

      log.error(identifyingPrefix.concat("REST API threw exception '" + e.getMessage() + "'"));
    }

    if (result == null) {
      log.error(identifyingPrefix.concat("Retrieved null object from data REST API"));
    } else {
      log.debug(identifyingPrefix.concat("Response : '" + result + "'"));
      processedSiteData = dataProcessor.process(compoundIdentifier, result);
      if (processedSiteData != null) {
        log.debug(identifyingPrefix.concat("Processed site data : '" + processedSiteData.toString() + "'"));
      }
    }

    return processedSiteData;
  }
}