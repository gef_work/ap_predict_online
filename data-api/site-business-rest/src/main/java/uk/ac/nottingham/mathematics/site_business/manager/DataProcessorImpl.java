/*

  Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Nottingham nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.nottingham.mathematics.site_business.manager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.MapIterator;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.collections.map.MultiKeyMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import uk.ac.nottingham.mathematics.site_business.SiteIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.InputValueSource;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.QSARIVS;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.RESTAPIIVS;
import uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata.ProcessedSiteDataVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.workflow.OutcomeVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.APIIdentifiers;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.IndeterminableAssayException;
import uk.ac.ox.cs.nc3rs.business_manager.api.util.AssayUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ExperimentalDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.transfer.experimental.ExperimentalResult;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.experimental.ConcQTPctChangeValuesVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.experimental.DefaultExperimentalDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.experimental.DefaultExperimentalResultVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 * Site data processing implementation.
 *
 * @author geoff
 */
@Component(SiteIdentifiers.COMPONENT_DATA_PROCESSOR)
public class DataProcessorImpl implements DataProcessor {

  @Autowired @Qualifier(APIIdentifiers.COMPONENT_ASSAY_BARRACUDA)
  private AssayVO barracudaAssay;
  @Autowired @Qualifier(APIIdentifiers.COMPONENT_ASSAY_MANUAL_PATCH)
  private AssayVO manualPatchAssay;
  @Autowired @Qualifier(APIIdentifiers.COMPONENT_ASSAY_PATCHXPRESS)
  private AssayVO patchXpressAssay;
  @Autowired @Qualifier(APIIdentifiers.COMPONENT_ASSAY_QPATCH)
  private AssayVO qpatchAssay;
  @Autowired @Qualifier(APIIdentifiers.COMPONENT_ASSAY_QSAR)
  private AssayVO qsarAssay;
  @Autowired @Qualifier(APIIdentifiers.COMPONENT_ASSAY_QUATTRO_FLIPR)
  private AssayVO quattroFLIPRAssay;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_CONFIGURATION)
  private Configuration configuration;

  @Autowired @Qualifier(SiteIdentifiers.COMPONENT_EXPERIMENTAL_DATA_MANAGER)
  private ExperimentalDataManager experimentalDataManager;

  //private static final String assayGroupLevelKey = "assayGroupLevel";
  private static final String assayGroupNameKey = "assayGroupName";
  private static final String hillKey = "hill";
  private static final String errorKey = "error";
  private static final String ionChannelNameKey = "ionChannelName";
  private static final String ionChannelValuesKey = "ionChannelValues";
  //private static final String originalKey = "original";
  private static final String perFrequencyConcentrationsKey = "perFrequencyConcentrations";
  private static final String pIC50DataKey = "pIC50Data";
  private static final String processedDataKey = "processedData";
  //private static final String simulationIdKey = "simulationId";
  private static final String sourceAssayNameKey = "sourceAssayName";
  //private static final String strategyOrderKey = "strategyOrder";
  private static final String successKey = "success";
  private static final String valueKey = "value";

  private static final Log log = LogFactory.getLog(DataProcessorImpl.class);

  // determine the assay value object using the assay name.
  private AssayVO determineAssay(final String assayName)
                                 throws IndeterminableAssayException {

    AssayVO assay = null;
    if (AssayUtil.ASSAY_NAME_BARRACUDA.equalsIgnoreCase(assayName)) {
      assay = barracudaAssay;
    } else if (AssayUtil.ASSAY_NAME_MANUAL_PATCH.equalsIgnoreCase(assayName)) {
      assay = manualPatchAssay;
    } else if (AssayUtil.ASSAY_NAME_PATCHXPRESS.equalsIgnoreCase(assayName)) {
      assay = patchXpressAssay;
    } else if (AssayUtil.ASSAY_NAME_QPATCH.equalsIgnoreCase(assayName)) {
      assay = qpatchAssay;
    } else if (AssayUtil.ASSAY_NAME_QSAR.equalsIgnoreCase(assayName)) {
      assay = qsarAssay;
    } else if (AssayUtil.ASSAY_NAME_QUATTRO_FLIPR.equalsIgnoreCase(assayName)) {
      assay = quattroFLIPRAssay;
    } else {
      throw new IndeterminableAssayException(assayName);
    }
    return assay;
  }

  /* (non-Javadoc)
   * @see uk.ac.nottingham.mathematics.site_business.manager.DataProcessor#process(java.lang.String, java.lang.String)
   */
  @Override
  public ProcessedSiteDataVO process(final String compoundIdentifier,
                                     final String restAPIResponse) {
    log.debug("~process() : Invoked.");

    ProcessedSiteDataVO processedSiteData = null;

    JSONObject successJSON = null;
    try {
      successJSON = new JSONObject(restAPIResponse.trim()).getJSONObject(successKey);
    } catch (JSONException e1) {
      try {
        final String errorMessage = new JSONObject(restAPIResponse.trim()).getString(errorKey);
        log.warn("~process() : Error from REST API of '" + errorMessage + "'.");
        return new ProcessedSiteDataVO(null, null, null);
      } catch (JSONException e2) {
        // Neither a "success" nor an "error"!?!
        final String errorMessage = e2.getMessage();
        log.error("~process() : Error parsing REST API response JSON '" + errorMessage + "'.");
      }
    }

    if (successJSON != null) {
      try {
        processedSiteData = new ProcessedSiteDataVO(extractExperimental(compoundIdentifier,
                                                                        successJSON),
                                                    extractQSAR(compoundIdentifier,
                                                                successJSON),
                                                    extractScreening(compoundIdentifier,
                                                                     successJSON));
      } catch (JSONException e) {
        
      }
    }

    return processedSiteData;
  }

  /*
   * It's important to note that input data gathering only returns frequencies
   * and concentrations for experimental data (or the default values if no
   * experimental data was found). Therefore there will not be any actual
   * experimental data being assigned via input data gathering.
   * 
   * @param compoundIdentifier Compound identifier.
   * @param successJSON "success" part of REST API response.
   * @return Experimental data holder containing concentrations only.
   */
  protected ExperimentalDataHolder extractExperimental(final String compoundIdentifier,
                                                       final JSONObject successJSON)
                                                       throws JSONException {
    final String identifyingLogPrefix = "~extractExperimental() : '" + compoundIdentifier + " : ";
    log.debug(identifyingLogPrefix.concat("Invoked."));

    final Set<ExperimentalResult> experimentalResults = new HashSet<ExperimentalResult>();

    final JSONObject perFreqConcs = successJSON.getJSONObject(perFrequencyConcentrationsKey);
    if (perFreqConcs != null) {
      Iterator<?> frequencyKeys = perFreqConcs.keys();
      final Set<String> frequencies = new HashSet<String>();
      while (frequencyKeys.hasNext()) {
        frequencies.add((String) frequencyKeys.next());
      }

      if (!frequencies.isEmpty()) {
        for (final String frequency : frequencies) {
          final JSONArray concentrations = perFreqConcs.getJSONArray(frequency);
          final List<ConcQTPctChangeValuesVO> concQTPctChangeValues = new ArrayList<ConcQTPctChangeValuesVO>();
          for (int idx = 0; idx < concentrations.length(); idx++) {
            /*

            This is how data should be being assigned....!

            concQTPctChangeValues.add(new ConcQTPctChangeValuesVO(new BigDecimal("0.300"),
                                                                  new BigDecimal("2.000"),
                                                                  new BigDecimal("1.400")));
            */
            concQTPctChangeValues.add(new ConcQTPctChangeValuesVO(new BigDecimal(concentrations.getDouble(idx)),
                                                                  null, null));
          }
          experimentalResults.add(new DefaultExperimentalResultVO(new BigDecimal(frequency),
                                                                  concQTPctChangeValues));
        }
        log.debug(identifyingLogPrefix.concat("Generated : '" + experimentalResults.toString() + "'"));
      }
    }

    return experimentalDataManager.store(compoundIdentifier, 
                                         new DefaultExperimentalDataVO(experimentalResults,
                                                                       new ArrayList<String>()));
  }

  /*
   * Extract QSAR data.
   * 
   * @param compoundIdentifier Compound identifier
   * @param successJSON "success" part of REST API response.
   * @return QSAR data.
   */
  protected List<OutcomeVO> extractQSAR(final String compoundIdentifier,
                                        final JSONObject successJSON)
                                        throws JSONException {
    final String identifyingLogPrefix = "~extractQSAR() : '" + compoundIdentifier + " : ";
    log.debug(identifyingLogPrefix.concat("Invoked."));

    final List<OutcomeVO> qsar = new ArrayList<OutcomeVO>();

    final JSONArray processedData = successJSON.getJSONArray(processedDataKey);
    if (processedData.length() > 0) {
      for (int processedDataIdx = 0; processedDataIdx < processedData.length(); processedDataIdx++) { 
        final JSONObject groupedData = processedData.getJSONObject(processedDataIdx);

        //final Double simulationId = groupedData.getDouble(simulationIdKey);
        final JSONArray ionChannelValuesArr = groupedData.getJSONArray(ionChannelValuesKey);
        //final Double assayGroupLevel = groupedData.getDouble(assayGroupLevelKey);
        final String assayGroupName = groupedData.getString(assayGroupNameKey);

        if (qsarAssay.getName().equals(assayGroupName)) {
          final List<InputValueSource> inputValueSourcesCaV = new ArrayList<InputValueSource>(1);
          final List<InputValueSource> inputValueSourcesNaV = new ArrayList<InputValueSource>(1);
          final List<InputValueSource> inputValueSourcesHERG = new ArrayList<InputValueSource>(1);

          for (int ionChannelValuesIdx = 0; ionChannelValuesIdx < ionChannelValuesArr.length();
               ionChannelValuesIdx++) {
            final JSONObject ionChannelValuesObj = ionChannelValuesArr.getJSONObject(ionChannelValuesIdx);
            final String ionChannelName = ionChannelValuesObj.getString(ionChannelNameKey);
            final IonChannel ionChannel = IonChannel.valueOf(ionChannelName);

            final JSONArray pIC50Data = ionChannelValuesObj.getJSONArray(pIC50DataKey);
            final BigDecimal pIC50 = new BigDecimal(((JSONObject) pIC50Data.get(0)).getDouble(valueKey));

            switch (ionChannel) {
              case CaV1_2 :
                inputValueSourcesCaV.add(new QSARIVS(pIC50));
                break;
              case hERG :
                inputValueSourcesHERG.add(new QSARIVS(pIC50));
                break;
              case KCNQ1 :
                break;
              case NaV1_5 :
                inputValueSourcesNaV.add(new QSARIVS(pIC50));
                break;
              default :
                final String errorMessage = "Unrecognised ion channel of '" + ionChannel + "' encountered!";
                log.error(identifyingLogPrefix.concat(errorMessage));
                throw new UnsupportedOperationException(errorMessage);
            }
          }

          qsar.add(new OutcomeVO(qsarAssay, IonChannel.hERG, inputValueSourcesHERG));
          qsar.add(new OutcomeVO(qsarAssay, IonChannel.CaV1_2, inputValueSourcesCaV));
          qsar.add(new OutcomeVO(qsarAssay, IonChannel.NaV1_5, inputValueSourcesNaV));

          break;
        }
      }
    }

    return qsar;
  }

  /**
   * 
   * 
   * @param compoundIdentifier Compound identifier
   * @param successJSON "success" part of REST API response.
   * @return Screening data.
   */
  protected List<OutcomeVO> extractScreening(final String compoundIdentifier,
                                             final JSONObject successJSON)
                                             throws JSONException {
    final String identifyingLogPrefix = "~extractScreening() : '" + compoundIdentifier + " : ";
    log.debug(identifyingLogPrefix.concat("Invoked."));

    final JSONArray processedData = successJSON.getJSONArray(processedDataKey);
    final MultiKeyMap ionChannelData = new MultiKeyMap();

    if (processedData.length() > 0) {
      for (int processedDataIdx = 0; processedDataIdx < processedData.length(); processedDataIdx++) { 
        final JSONObject groupedData = processedData.getJSONObject(processedDataIdx);

        //final Double simulationId = groupedData.getDouble(simulationIdKey);
        final JSONArray ionChannelValuesArr = groupedData.getJSONArray(ionChannelValuesKey);
        //final Double assayGroupLevel = groupedData.getDouble(assayGroupLevelKey);
        final String assayGroupName = groupedData.getString(assayGroupNameKey);

        if (!qsarAssay.getName().equals(assayGroupName)) {
          for (int ionChannelValuesIdx = 0; ionChannelValuesIdx < ionChannelValuesArr.length();
               ionChannelValuesIdx++) {
            final JSONObject ionChannelValuesObj = ionChannelValuesArr.getJSONObject(ionChannelValuesIdx);

            final String ionChannelName = ionChannelValuesObj.getString(ionChannelNameKey);
            final String sourceAssayName = ionChannelValuesObj.getString(sourceAssayNameKey);
            final JSONArray pIC50DataArr = ionChannelValuesObj.getJSONArray(pIC50DataKey);

            if (ionChannelData.containsKey(sourceAssayName, ionChannelName)) {
              log.debug(identifyingLogPrefix.concat("Exists " + ionChannelName + "+" + sourceAssayName + "'"));
            } else {
              AssayVO assay = null;
              try {
                assay = determineAssay(sourceAssayName);
              } catch (IndeterminableAssayException e) {
                final String errorMessage = e.getMessage();
                log.error(identifyingLogPrefix.concat(errorMessage));
                throw new UnsupportedOperationException(errorMessage);
              }

              if (assay != null) {
                final List<InputValueSource> inputValueSources = new ArrayList<InputValueSource>();
                for (int pIC50DataIdx = 0; pIC50DataIdx < pIC50DataArr.length();
                     pIC50DataIdx++) {
                  final JSONObject pIC50DataObj = pIC50DataArr.getJSONObject(pIC50DataIdx);

                  final Double value = pIC50DataObj.getDouble(valueKey);
                  final Double hill = pIC50DataObj.getDouble(hillKey);

                  inputValueSources.add(new RESTAPIIVS(new BigDecimal(value),
                                                       new BigDecimal(hill)));
                }

                final OutcomeVO outcome = new OutcomeVO(assay,
                                                        IonChannel.valueOf(ionChannelName),
                                                        inputValueSources);
                log.debug(identifyingLogPrefix.concat("Adding " + outcome.toString()));
                ionChannelData.put(sourceAssayName, ionChannelName, outcome);
              }
            }
          }
        }
      }
    }

    final List<OutcomeVO> outcomes = new ArrayList<OutcomeVO>();
    final MapIterator iterator = ionChannelData.mapIterator();
    while (iterator.hasNext()) {
      iterator.next();
      final MultiKey key = (MultiKey) iterator.getKey();

      final String sourceAssayName = (String) key.getKey(0);
      final String ionChannelName = (String) key.getKey(1);

      final OutcomeVO outcome = (OutcomeVO) ionChannelData.get(sourceAssayName,
                                                               ionChannelName);

      outcomes.add(outcome);
    }

    log.debug(identifyingLogPrefix.concat("Returning '" + outcomes + "'."));

    return outcomes;
  }

}