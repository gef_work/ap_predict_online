/*

  Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Nottingham nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.nottingham.mathematics.site_business.manager;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import uk.ac.nottingham.mathematics.site_business.SiteIdentifiers;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ExperimentalDataHolder;

/**
 *
 *
 * @author geoff
 */
@Component(SiteIdentifiers.COMPONENT_EXPERIMENTAL_DATA_MANAGER)
public class ExperimentalDataManagerImpl implements ExperimentalDataManager {

  private static final Map<String, ExperimentalDataHolder> store = new HashMap<String, ExperimentalDataHolder>();

  private static final Log log = LogFactory.getLog(ExperimentalDataManagerImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.nottingham.mathematics.site_business.manager.ExperimentalDataManager#store(java.lang.String, uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ExperimentalDataHolder)
   */
  @Override
  public ExperimentalDataHolder store(final String compoundIdentifier,
                                      final ExperimentalDataHolder experimentalDataHolder) {
    log.debug("~store() : " + compoundIdentifier + " : Invoked.");

    store.put(compoundIdentifier, experimentalDataHolder);

    return experimentalDataHolder;
  }

  /* (non-Javadoc)
   * @see uk.ac.nottingham.mathematics.site_business.manager.ExperimentalDataManager#retrieve(java.lang.String)
   */
  @Override
  public ExperimentalDataHolder retrieve(final String compoundIdentifier) {
    log.debug("~retrieve() : " + compoundIdentifier + " : Invoked.");

    return store.get(compoundIdentifier);
  }

}