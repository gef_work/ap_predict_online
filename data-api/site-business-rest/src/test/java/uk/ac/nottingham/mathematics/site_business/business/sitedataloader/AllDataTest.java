/*

  Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Nottingham nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.nottingham.mathematics.site_business.business.sitedataloader;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import uk.ac.nottingham.mathematics.site_business.SiteIdentifiers;
import uk.ac.nottingham.mathematics.site_business.business.sitedataloader.AllData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening.AggregatedSiteDataVO;

/**
 * Unit test site data loading.
 *
 * @author geoff
 */
public class AllDataTest {

  private IMocksControl mocksControl;
  private RestTemplate mockDataAPIRESTTemplate;
  private AllData siteDataLoader;
  private String dummyCompoundIdentifier;
  private String fakeDataAPIURL;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockDataAPIRESTTemplate = mocksControl.createMock(RestTemplate.class);

    siteDataLoader = new AllData(mockDataAPIRESTTemplate,
                                        fakeDataAPIURL);

    dummyCompoundIdentifier = null;
  }

  //@Test
  public void testLoadScreeningData() {

    Capture<String> captureFakeDataAPIURL = newCapture();
    Capture<Map<String, String>> captureURLVars = newCapture();
    String gotten = null;
    expect(mockDataAPIRESTTemplate.getForObject(capture(captureFakeDataAPIURL),
                                                eq(String.class),
                                                capture(captureURLVars)))
          .andReturn(gotten);

    mocksControl.replay();

    siteDataLoader.loadAllData(dummyCompoundIdentifier);

    mocksControl.verify();

    //assertNull(aggregatedSiteDataVO);
    //String capturedFakeDataAPIURL = captureFakeDataAPIURL.getValue();
    //assertSame(fakeDataAPIURL, capturedFakeDataAPIURL);
    //Map<String, String> capturedURLVars = captureURLVars.getValue();
    //assertSame(1, capturedURLVars.size());
    //assertTrue(capturedURLVars.containsKey(SiteIdentifiers.REST_API_URL_VAR_COMPOUND_ID));
    //assertEquals(dummyCompoundIdentifier,
    //             capturedURLVars.get(SiteIdentifiers.REST_API_URL_VAR_COMPOUND_ID));
  }
}