# `data-api` Non-integral Data API work.

Not an integral component of ap-predict-online -- this activity is for emulating a Data API as part
of an effort to encourage sites towards developing their own APIs for ion channel data retrieval.

## `business-manager-skeleton` Skeleton Business Manager

Reduced functionality `business-manager`. The idea being that rather than have
a full-functionality `business-manager` doing all things like dose-response-fitting,
pIC50 evaluation strategies, experimental data retrieval, etc, instead a site
provides a "data API" (a bit like `rest-data-api`) which does all that work.

The overlaid `site-business-rest` will actually do the call out to the "data API".

## `rest-data-api` API to `business-manager` Input Data Gathering

A [Sprint Boot](https://spring.io/projects/spring-boot) API to invoke the input data gathering
process of `business-manager`.

Basically it calls the `business-manager` web service with an "input data gathering" request.

## `site-business-rest` Data API-calling Site Business

Calls a "Data API", e.g. `rest-data-api` and provides a bit of site business logic,
placing compound data into `business-manager-api` defined objects which
`business-manager-skeleton` can subsequently process.