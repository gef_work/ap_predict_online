/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb;

import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ObjectFactory;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProcessSimulationsRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.SimulationDetail;

/**
 * JAXB object builder.
 *
 * @author geoff
 */
public class SchemaObjectBuilder {

  public static final String COMPOUND_IDENTIFIER_INVALID_1 = "Compound 1Compound 1Compound 1Compound 1Compound 1Compound 1Compound 1Compound 1Compound 1Compound 1Compound 1";
  public static final String COMPOUND_IDENTIFIER_INVALID_2 = "Compound 2Compound 2Compound 2Compound 2Compound 2Compound 2Compound 2Compound 2Compound 2Compound 2Compound 2";
  public static final String COMPOUND_IDENTIFIER_VALID_1 = "Compound 1";
  public static final String COMPOUND_IDENTIFIER_VALID_2 = "Compound 2";

  public static final String SIMULATION_ID_INVALID_1 = "Simulation 1Simulation 1Simulation 1Simulation 1Simulation 1Simulation 1Simulation 1Simulation 1Simulation 1";
  public static final String SIMULATION_ID_INVALID_2 = "-100";
  public static final String SIMULATION_ID_VALID_1 = "100";
  public static final String SIMULATION_ID_VALID_2 = "200";

  private static final ObjectFactory objectFactory = new ObjectFactory();

  /**
   * Create and return an invalid {@link ProcessSimulationsRequest}.
   */
  public static ProcessSimulationsRequest createInvalidProcessSimulationsRequest() {
    final ProcessSimulationsRequest processSimulationsRequest = objectFactory.createProcessSimulationsRequest();
    final SimulationDetail simulationDetail1 = objectFactory.createSimulationDetail();
    simulationDetail1.setCompoundIdentifier(COMPOUND_IDENTIFIER_INVALID_1);
    processSimulationsRequest.getSimulationDetail().add(simulationDetail1);
    final SimulationDetail simulationDetail2 = objectFactory.createSimulationDetail();
    simulationDetail2.setCompoundIdentifier(COMPOUND_IDENTIFIER_INVALID_2);
    processSimulationsRequest.getSimulationDetail().add(simulationDetail2);
    return processSimulationsRequest;
  }

  /**
   * Create a {@link SimulationDetail} object.
   * 
   * @param compoundIdentifier Compound name.
   * @param forceReRun Force re-run flag.
   * @param reset Reset flag.
   * @return SimulationDetail object.
   */
  public static SimulationDetail createSimulationDetail(final String compoundIdentifier,
                                                        final boolean forceReRun,
                                                        final boolean reset) {
    final SimulationDetail simulationDetail = objectFactory.createSimulationDetail();
    simulationDetail.setCompoundIdentifier(compoundIdentifier);
    simulationDetail.setForceReRun(forceReRun);
    simulationDetail.setReset(reset);

    return simulationDetail;
  }

  /**
   * Create and return a valid {@link ProcessSimulationsRequest}.
   */
  public static ProcessSimulationsRequest createValidProcessSimulationsRequest() {
    final ProcessSimulationsRequest processSimulationsRequest = objectFactory.createProcessSimulationsRequest();
    final SimulationDetail simulationDetail1 = objectFactory.createSimulationDetail();
    simulationDetail1.setCompoundIdentifier(COMPOUND_IDENTIFIER_VALID_1);
    processSimulationsRequest.getSimulationDetail().add(simulationDetail1);
    final SimulationDetail simulationDetail2 = objectFactory.createSimulationDetail();
    simulationDetail2.setCompoundIdentifier(COMPOUND_IDENTIFIER_VALID_2);
    processSimulationsRequest.getSimulationDetail().add(simulationDetail2);
    return processSimulationsRequest;
  }
}