/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.service;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.entity.Job;
import uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager;
import uk.ac.ox.cs.epsrc.business_manager.manager.JobManager;
import uk.ac.ox.cs.epsrc.business_manager.value.object.AppManagerJobProgressVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.InformationVO;
import uk.ac.ox.cs.epsrc.business_manager.ws.app_manager.manager.AppManagerManager;

/**
 * Unit test the information service implementation
 *
 * @author geoff
 */
public class InformationServiceImplTest {

  private AppManagerManager mockAppManagerManager;
  private IMocksControl mocksControl;
  private InformationManager mockInformationManager;
  private InformationService informationService;
  private JobManager mockJobManager;

  @Before
  public void setUp() {
    informationService = new InformationServiceImpl();
    mocksControl = createStrictControl();
    mockAppManagerManager = mocksControl.createMock(AppManagerManager.class);
    mockInformationManager = mocksControl.createMock(InformationManager.class);
    mockJobManager = mocksControl.createMock(JobManager.class);

    ReflectionTestUtils.setField(informationService, BusinessIdentifiers.COMPONENT_APP_MANAGER_MANAGER,
                                 mockAppManagerManager);
    ReflectionTestUtils.setField(informationService, BusinessIdentifiers.COMPONENT_INFORMATION_MANAGER,
                                 mockInformationManager);
    ReflectionTestUtils.setField(informationService, BusinessIdentifiers.COMPONENT_JOB_MANAGER,
                                 mockJobManager);
  }

  @Test
  public void testRetrieveProgress() {
    final long dummySimulationId = 1l;

    // 1. Emulate no jobs being found
    final InformationVO mockInformationVO = mocksControl.createMock(InformationVO.class);
    expect(mockInformationManager.retrieveProgress(dummySimulationId)).andReturn(mockInformationVO);
    final List<Job> dummyJobs = new ArrayList<Job>();
    expect(mockJobManager.findJobsBySimulationId(dummySimulationId)).andReturn(dummyJobs);

    mocksControl.replay();

    InformationVO informationVO = informationService.retrieveProgress(dummySimulationId);

    mocksControl.verify();
    assertSame(informationVO, mockInformationVO);

    mocksControl.reset();

    // 2. Emulate jobs found, but no app manager id for job
    expect(mockInformationManager.retrieveProgress(dummySimulationId)).andReturn(mockInformationVO);
    final Job mockJob = mocksControl.createMock(Job.class);
    dummyJobs.add(mockJob);
    expect(mockJobManager.findJobsBySimulationId(dummySimulationId)).andReturn(dummyJobs);
    String dummyAppManagerId = null;
    expect(mockJob.getAppManagerId()).andReturn(dummyAppManagerId);
    final Long dummyJobId = 2L;
    expect(mockJob.getId()).andReturn(dummyJobId);

    mocksControl.replay();

    informationVO = informationService.retrieveProgress(dummySimulationId);

    mocksControl.verify();

    mocksControl.reset();
    
    // 3. Emulate jobs found and app manager id for job.
    expect(mockInformationManager.retrieveProgress(dummySimulationId)).andReturn(mockInformationVO);
    expect(mockJobManager.findJobsBySimulationId(dummySimulationId)).andReturn(dummyJobs);
    dummyAppManagerId = "4";
    expect(mockJob.getAppManagerId()).andReturn(dummyAppManagerId);
    expect(mockJob.getId()).andReturn(dummyJobId);
    final Capture<Long> captureAppManagerId = newCapture();
    mockAppManagerManager.retrieveJobProgress(isA(AppManagerJobProgressVO.class),
                                              capture(captureAppManagerId));
    final Capture<String> captureJobProgressJSON = newCapture();
    mockInformationVO.assignAppManagerJobProgress(capture(captureJobProgressJSON));

    mocksControl.replay();

    informationVO = informationService.retrieveProgress(dummySimulationId);

    mocksControl.verify();
    final Long assignedAppManagerId = (Long) captureAppManagerId.getValue();
    assertEquals(Long.valueOf(dummyAppManagerId), assignedAppManagerId);
    final String assignedJobProgressJSON = (String) captureJobProgressJSON.getValue();
    assertTrue(assignedJobProgressJSON.contains("progress"));
    assertTrue(assignedJobProgressJSON.contains("jobId"));
  }
}