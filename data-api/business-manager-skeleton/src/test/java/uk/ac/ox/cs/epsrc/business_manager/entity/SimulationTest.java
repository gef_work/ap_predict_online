/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;

/**
 * Unit test the Simulation entity.
 *
 * @author geoff
 */
public class SimulationTest {

  private Simulation simulation;

  @Test
  public void testInitialisingConstructor() {
    String dummyCompoundIdentifier = null;
    boolean dummyAssayGrouping = false;
    boolean dummyValueInheriting = false;
    boolean dummyBetweenGroups = false;
    boolean dummyWithinGroups = false;
    short dummyCellModelIdentifier = 0;
    BigDecimal dummyPacingMaxTime = null;
    try {
      simulation = new Simulation(dummyCompoundIdentifier, dummyAssayGrouping,
                                  dummyValueInheriting, dummyBetweenGroups,
                                  dummyWithinGroups, dummyCellModelIdentifier,
                                  dummyPacingMaxTime);
      fail("Should not permit a non-valued compound identifier!");
    } catch (IllegalArgumentException e) {}
    dummyCompoundIdentifier = "dummyCompoundIdentifier";
    simulation = new Simulation(dummyCompoundIdentifier, dummyAssayGrouping,
                                dummyValueInheriting, dummyBetweenGroups,
                                dummyWithinGroups, dummyCellModelIdentifier,
                                dummyPacingMaxTime);
    assertNotNull(simulation.getCompoundIdentifier());

    dummyAssayGrouping = true;
    dummyValueInheriting = true;
    dummyBetweenGroups = true;
    dummyWithinGroups = true;
    dummyPacingMaxTime = BigDecimal.ONE;

    simulation = new Simulation(dummyCompoundIdentifier, dummyAssayGrouping,
                                dummyValueInheriting, dummyBetweenGroups,
                                dummyWithinGroups, dummyCellModelIdentifier,
                                dummyPacingMaxTime);

    assertNull(simulation.getCompleted());
    assertNull(simulation.getId());
    assertNull(simulation.getLatestCheck());
    assertFalse(simulation.hasBeenReRunChecked());
    assertTrue(simulation.isAssayGrouping());
    assertTrue(simulation.isBetweenGroups());
    assertTrue(simulation.isBusy());
    assertTrue(simulation.isValueInheriting());
    assertTrue(simulation.isWithinGroups());
    assertTrue(dummyPacingMaxTime.compareTo(simulation.getPacingMaxTime()) == 0);

    assertNotNull(simulation.toString());
  }

  @Test
  public void testInputDataRetrievingConstructor() {
    final long dummyFakeSimulationId = 1l;
    final String dummyCompoundIdentifier = "dummyCompoundIdentifier";
    final boolean dummyAssayGrouping = false;
    final boolean dummyValueInheriting = false;
    final boolean dummyBetweenGroups = false;
    final boolean dummyWithinGroups = false;
    final short dummyCellModelIdentifier = 0;
    final BigDecimal dummyPacingMaxTime = null;

    simulation = new Simulation(dummyFakeSimulationId, dummyCompoundIdentifier,
                                dummyAssayGrouping, dummyValueInheriting,
                                dummyBetweenGroups, dummyWithinGroups,
                                dummyCellModelIdentifier, dummyPacingMaxTime);
    assertSame(dummyFakeSimulationId, simulation.getId());
  }

  /**
   * Test the phase changing.
   */
  @Test
  public void testOperations() {
    /*
     * 1. Create a new simulation.
     */
    String dummyCompoundIdentifier = "dummyCompoundIdentifier";
    boolean dummyAssayGrouping = false;
    boolean dummyValueInheriting = false;
    boolean dummyBetweenGroups = false;
    boolean dummyWithinGroups = false;
    short dummyCellModelIdentifier = 0;
    BigDecimal dummyPacingMaxTime = null;

    simulation = new Simulation(dummyCompoundIdentifier, dummyAssayGrouping,
                                dummyValueInheriting, dummyBetweenGroups,
                                dummyWithinGroups, dummyCellModelIdentifier,
                                dummyPacingMaxTime);

    simulation.onCreate();

    assertNull(simulation.getCompleted());
    assertTrue(simulation.isBusy());
    assertEquals(dummyCompoundIdentifier, simulation.getCompoundIdentifier());

    assertNull(simulation.getLatestCheck());
    assertFalse(simulation.hasBeenReRunChecked());
    simulation.assignLatestCheck();
    assertNotNull(simulation.getLatestCheck());
    assertTrue(simulation.hasBeenReRunChecked());

    /*
     * 2. Reset system state - assign as 'busy'
     */
    simulation.resetSystemState();

    assertNull(simulation.getCompleted());
    assertNull(simulation.getLatestCheck());
    assertFalse(simulation.forcingReRun());
    assertFalse(simulation.forcingReset());
    assertTrue(simulation.isBusy());

    /*
     * 3. Reset system state - assign as 'free'
     */
    simulation.resetSystemState(true);

    assertNull(simulation.getCompleted());
    assertNull(simulation.getLatestCheck());
    assertFalse(simulation.forcingReRun());
    assertFalse(simulation.forcingReset());
    assertFalse(simulation.isBusy());

    /*
     * 4. Assign simulation as completed - this can't be done either as can only tranisition from 
     *    "busy" to "completed".
     */
    try {
      simulation.assignCompleted();
      fail("Cannot assign a 'free' simulation as 'completed'.");
    } catch (IllegalStateException e) {}

    /*
     * 5. Assign simulation as completed.
     */
    simulation.resetSystemState();
    simulation.assignCompleted();
    assertNotNull(simulation.getCompleted());
  }

  @Test
  public void testProcessTasks() {
    String dummyCompoundIdentifier = "dummyCompoundIdentifier";
    boolean dummyAssayGrouping = false;
    boolean dummyValueInheriting = false;
    boolean dummyBetweenGroups = false;
    boolean dummyWithinGroups = false;
    short dummyCellModelIdentifier = 0;
    BigDecimal dummyPacingMaxTime = null;

    simulation = new Simulation(dummyCompoundIdentifier, dummyAssayGrouping,
                                dummyValueInheriting, dummyBetweenGroups,
                                dummyWithinGroups, dummyCellModelIdentifier,
                                dummyPacingMaxTime);

    boolean dummyForceReRun = false;
    boolean dummyReset = false;

    assertFalse(simulation.forcingReRun());
    assertFalse(simulation.forcingReset());
    boolean changed = simulation.assignProcessTask(dummyForceReRun, dummyReset);
    assertFalse(simulation.forcingReRun());
    assertFalse(simulation.forcingReset());
    assertFalse(changed);

    dummyReset = true;
    changed = simulation.assignProcessTask(dummyForceReRun, dummyReset);
    assertFalse(simulation.forcingReRun());
    assertTrue(simulation.forcingReset());
    assertTrue(changed);

    dummyReset = false;
    dummyForceReRun = true;
    changed = simulation.assignProcessTask(dummyForceReRun, dummyReset);
    assertTrue(simulation.forcingReRun());
    assertFalse(simulation.forcingReset());
    assertTrue(changed);

    dummyReset = true;
    try {
      simulation.assignProcessTask(dummyForceReRun, dummyReset);
      fail("Should not permit both parameters to have a true value!");
    } catch (IllegalArgumentException e) {}

    assertTrue(changed);

    dummyForceReRun = false;
    assertTrue(simulation.forcingReRun());
    assertFalse(simulation.forcingReset());
    changed = simulation.assignProcessTask(dummyForceReRun, dummyReset);
    assertFalse(changed);

    dummyReset = false;
    dummyForceReRun = true;
    changed = simulation.assignProcessTask(dummyForceReRun, dummyReset);
    assertFalse(changed);
  }
}