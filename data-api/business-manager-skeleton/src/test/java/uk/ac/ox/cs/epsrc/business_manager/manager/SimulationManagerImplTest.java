/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.

  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.manager;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.dao.SimulationDAO;
import uk.ac.ox.cs.epsrc.business_manager.entity.Job;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.manager.JobManager;
import uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager;
import uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManagerImpl;

/**
 * Unit testing the simulations manager.
 *
 * @author geoff
 */
public class SimulationManagerImplTest {

  private BusinessManagerJMSTemplate mockJMSTemplate;
  private IMocksControl mocksControl;
  private InformationManager mockInformationManager;
  private JobManager mockJobManager;
  private SimulationDAO mockSimulationDAO;
  private SimulationManager simulationManager;

  private static final long dummySimulationId = 4l;

  @Before
  public void setUp() {
    simulationManager = new SimulationManagerImpl();

    mocksControl = createStrictControl();
    mockJMSTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    mockJobManager = mocksControl.createMock(JobManager.class);
    mockSimulationDAO = mocksControl.createMock(SimulationDAO.class);

    ReflectionTestUtils.setField(simulationManager, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockJMSTemplate);
    ReflectionTestUtils.setField(simulationManager, BusinessIdentifiers.COMPONENT_JOB_MANAGER,
                                 mockJobManager);
    ReflectionTestUtils.setField(simulationManager, BusinessIdentifiers.COMPONENT_INFORMATION_MANAGER,
                                 mockInformationManager);
    ReflectionTestUtils.setField(simulationManager, BusinessIdentifiers.COMPONENT_SIMULATION_DAO,
                                 mockSimulationDAO);
  }

  @Test
  public void testJobCompletedProcessing() {
    /*
     * 1. Emulate that simulation has not completed all jobs yet.
     */
    boolean dummySimulationCompleted = false;
    expect(mockJobManager.determineJobsCompletedForSimulation(dummySimulationId))
          .andReturn(dummySimulationCompleted);

    mocksControl.replay();

    boolean simulationCompleted = simulationManager.jobCompletedProcessing(dummySimulationId);

    mocksControl.verify();

    assertSame(dummySimulationCompleted, simulationCompleted);

    mocksControl.reset();

    /*
     * 2. Emulate that all simulation jobs completed.
     */
    dummySimulationCompleted = true;
    expect(mockJobManager.determineJobsCompletedForSimulation(dummySimulationId)) 
          .andReturn(dummySimulationCompleted);
    mockSimulationDAO.updateOnJobsCompletion(dummySimulationId);

    mocksControl.replay();

    simulationCompleted = simulationManager.jobCompletedProcessing(dummySimulationId);

    mocksControl.verify();

    assertSame(dummySimulationCompleted, simulationCompleted);
  }

  @Test
  public void testRetrievePerFrequencyConcentrations() {
    final List<Job> dummyJobs = new ArrayList<Job>();
    final Job mockJob1 = mocksControl.createMock(Job.class);
    final Job mockJob2 = mocksControl.createMock(Job.class);
    dummyJobs.add(mockJob1);
    dummyJobs.add(mockJob2);
    expect(mockJobManager.findJobsBySimulationId(dummySimulationId))
          .andReturn(dummyJobs);
    final Float dummyJob1PacingFreq1 = 3.4F;
    expect(mockJob1.getPacingFrequency()).andReturn(dummyJob1PacingFreq1);
    final Set<Float> dummyJob1CmpdConcs = new HashSet<Float>();
    final Float dummyJob1CmpdConc1 = 4.3F;
    final Float dummyJob1CmpdConc2 = 84.4F;
    dummyJob1CmpdConcs.add(dummyJob1CmpdConc1);
    dummyJob1CmpdConcs.add(dummyJob1CmpdConc2);
    expect(mockJob1.getCompoundConcentrations()).andReturn(dummyJob1CmpdConcs);
    final Float dummyJob2PacingFreq1 = 5.4F;
    expect(mockJob2.getPacingFrequency()).andReturn(dummyJob2PacingFreq1);
    final Set<Float> dummyJob2CmpdConcs = new HashSet<Float>();
    final Float dummyJob2CmpdConc1 = 5.3F;
    final Float dummyJob2CmpdConc2 = 43.4F;
    dummyJob2CmpdConcs.add(dummyJob2CmpdConc1);
    dummyJob2CmpdConcs.add(dummyJob2CmpdConc2);
    expect(mockJob2.getCompoundConcentrations()).andReturn(dummyJob2CmpdConcs);

    mocksControl.replay();

    final Map<BigDecimal, Set<BigDecimal>> perFreqConcs = simulationManager.retrievePerFrequencyConcentrations(dummySimulationId);

    mocksControl.verify();

    final BigDecimal dummyJob1PacingFreq1BD = new BigDecimal(dummyJob1PacingFreq1.toString());
    assertTrue(perFreqConcs.keySet().contains(dummyJob1PacingFreq1BD));
    assertTrue(perFreqConcs.get(dummyJob1PacingFreq1BD)
                           .contains(new BigDecimal(dummyJob1CmpdConc1.toString())));
    assertTrue(perFreqConcs.get(dummyJob1PacingFreq1BD)
                           .contains(new BigDecimal(dummyJob1CmpdConc2.toString())));
    final BigDecimal dummyJob2PacingFreq1BD = new BigDecimal(dummyJob2PacingFreq1.toString());
    assertTrue(perFreqConcs.keySet().contains(dummyJob2PacingFreq1BD));
    assertTrue(perFreqConcs.get(dummyJob2PacingFreq1BD)
                           .contains(new BigDecimal(dummyJob2CmpdConc1.toString())));
    assertTrue(perFreqConcs.get(dummyJob2PacingFreq1BD)
                           .contains(new BigDecimal(dummyJob2CmpdConc2.toString())));
  }

  @Test
  public void testSave() {
    final Simulation mockSimulation = mocksControl.createMock(Simulation.class);

    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockSimulationDAO.store(mockSimulation)).andReturn(mockSimulation);

    mocksControl.replay();

    final Simulation stored = simulationManager.save(mockSimulation);

    mocksControl.verify();

    assertSame(stored, mockSimulation);
  }

  @Test
  public void testSimulationTerminating() {
    mockSimulationDAO.updateOnJobsCompletion(dummySimulationId);

    mocksControl.replay();

    simulationManager.simulationTerminating(dummySimulationId);

    mocksControl.verify();
  }
}