/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.app_manager.manager;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.anyString;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.epsrc.business_manager.config.ConfigurationActionPotential;
import uk.ac.ox.cs.epsrc.business_manager.entity.GroupedInvocationInput;
import uk.ac.ox.cs.epsrc.business_manager.entity.IonChannelValues;
import uk.ac.ox.cs.epsrc.business_manager.entity.Job;
import uk.ac.ox.cs.epsrc.business_manager.entity.PIC50Data;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.manager.JobManager;
import uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager;
import uk.ac.ox.cs.epsrc.business_manager.value.object.PerJobConcentrationResultsVO;
import uk.ac.ox.cs.epsrc.business_manager.ws.app_manager.proxy.AppManagerProxy;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApplicationStatusRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApplicationStatusResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.AssociatedItem;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.BusinessDataUploadCompletedRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.Results;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllResultsResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.TypeCredibleIntervals;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 * Unit test the App Manager component manager interface implementation.
 *
 * @author geoff
 */
public class AppManagerManagerImplTest {

  private static final String invocationSource = "business-manager";

  private AppManagerProxy mockAppManagerProxy;
  private AppManagerManager appManagerManager;
  private Configuration mockConfiguration;
  private ConfigurationActionPotential mockConfigurationActionPotential;
  private BusinessManagerJMSTemplate mockJMSTemplate;
  private Float dummyPacingFrequency = 0.5F;
  private GroupedInvocationInput mockGroupedInvocationInput;
  private IMocksControl mocksControl;
  private Job mockJob;
  private JobManager mockJobManager;
  private static final Long dummyAppManagerId = 4l;
  private static final Long dummySimulationId = 1L;
  private Set<Float> dummyCompoundConcentrations = new HashSet<Float>();
  private Simulation mockSimulation;
  private SimulationManager mockSimulationManager;
  private String dummyAssayGroupName = "dummyAssayGroupName";

  @Before
  public void setUp() {
    appManagerManager = new AppManagerManagerImpl();

    mocksControl = createStrictControl();
    mockAppManagerProxy = mocksControl.createMock(AppManagerProxy.class);
    mockConfiguration = mocksControl.createMock(Configuration.class);
    mockConfigurationActionPotential = mocksControl.createMock(ConfigurationActionPotential.class);
    mockGroupedInvocationInput = mocksControl.createMock(GroupedInvocationInput.class);
    mockJMSTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    mockJob = mocksControl.createMock(Job.class);
    mockJobManager = mocksControl.createMock(JobManager.class);
    mockSimulation = mocksControl.createMock(Simulation.class);
    mockSimulationManager = mocksControl.createMock(SimulationManager.class);

    ReflectionTestUtils.setField(appManagerManager,
                                 BusinessIdentifiers.COMPONENT_APP_MANAGER_PROXY,
                                 mockAppManagerProxy);
    ReflectionTestUtils.setField(appManagerManager,
                                 BusinessIdentifiers.COMPONENT_CONFIGURATION,
                                 mockConfiguration);
    ReflectionTestUtils.setField(appManagerManager,
                                 BusinessIdentifiers.COMPONENT_CONFIGURATION_ACTIONPOTENTIAL,
                                 mockConfigurationActionPotential);
    ReflectionTestUtils.setField(appManagerManager,
                                 BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockJMSTemplate);
    ((AppManagerManagerImpl) appManagerManager).setJobManager(mockJobManager);
    ((AppManagerManagerImpl) appManagerManager).setSimulationManager(mockSimulationManager);
  }

  private void setCommonOptions() {
    final Short dummyDefaultPlasmaConcCount = Short.valueOf("30");
    expect(mockConfigurationActionPotential.getDefaultPlasmaConcCount())
          .andReturn(dummyDefaultPlasmaConcCount);
    final boolean dummyDefaultPlasmaConcLogScale = false;
    expect(mockConfigurationActionPotential.isDefaultPlasmaConcLogScale())
          .andReturn(dummyDefaultPlasmaConcLogScale);
    expect(mockJob.getPacingFrequency()).andReturn(dummyPacingFrequency);
    final Float dummyCompoundConcentration1 = Float.valueOf("0.001");
    final Float dummyCompoundConcentration2 = Float.valueOf("0.01");
    dummyCompoundConcentrations.add(dummyCompoundConcentration1);
    dummyCompoundConcentrations.add(dummyCompoundConcentration2);
    expect(mockJob.getCompoundConcentrations())
          .andReturn(dummyCompoundConcentrations);
    expect(mockJob.getGroupedInvocationInput())
          .andReturn(mockGroupedInvocationInput);
    expect(mockGroupedInvocationInput.getAssayGroupName())
          .andReturn(dummyAssayGroupName);
  }

  @Test
  public void testCreateRunRequestDefaultSaturationLevel() {
    setCommonOptions();

    final IonChannelValues mockIonChannelValues = mocksControl.createMock(IonChannelValues.class);
    final Set<IonChannelValues> dummyIonChannelValues = new HashSet<IonChannelValues>();
    dummyIonChannelValues.add(mockIonChannelValues);
    expect(mockGroupedInvocationInput.getIonChannelValues())
          .andReturn(dummyIonChannelValues);
    expect(mockSimulationManager.find(dummySimulationId))
          .andReturn(mockSimulation);
    final short dummyCellModelIdentifier = Short.valueOf("2").shortValue();
    expect(mockSimulation.getCellModelIdentifier())
          .andReturn(dummyCellModelIdentifier);
    final BigDecimal dummyPacingMaxTime = BigDecimal.ONE;
    expect(mockSimulation.getPacingMaxTime()).andReturn(dummyPacingMaxTime);
    final boolean dummyIsUsingDefaultCompoundConcentrations = false;
    expect(mockJob.isUsingConfigurationCompoundConcentrations())
          .andReturn(dummyIsUsingDefaultCompoundConcentrations);
    final String dummySourceAssayName = "dummySourceAssayName";
    expect(mockIonChannelValues.getSourceAssayName())
          .andReturn(dummySourceAssayName);
    final String dummyIonChannelName = IonChannel.CaV1_2.toString();
    expect(mockIonChannelValues.getIonChannelName())
          .andReturn(dummyIonChannelName);
    final PIC50Data mockPIC50Data = mocksControl.createMock(PIC50Data.class);
    final List<PIC50Data> dummyAllPIC50Data = new ArrayList<PIC50Data>();
    dummyAllPIC50Data.add(mockPIC50Data);
    expect(mockIonChannelValues.getIonChannelName())
          .andReturn(dummyIonChannelName);
    expect(mockIonChannelValues.getSourceAssayName())
          .andReturn(dummySourceAssayName);
    expect(mockIonChannelValues.getpIC50Data()).andReturn(dummyAllPIC50Data);
    final BigDecimal dummyPIC50 = BigDecimal.TEN;
    final BigDecimal dummyHill = BigDecimal.ONE;
    expect(mockPIC50Data.getValue()).andReturn(dummyPIC50);
    expect(mockPIC50Data.getHill()).andReturn(dummyHill);
    mockJMSTemplate.sendProgress(anyObject(OverallProgress.class));

    mocksControl.replay();

    ApPredictRunRequest request = appManagerManager.createRunRequest(dummySimulationId,
                                                                     mockJob);
    assertNotNull(request);
    AssociatedItem requestAssociatedItem = request.getICaL().getPIC50Data().get(0);
    assertTrue(dummyPIC50.compareTo(requestAssociatedItem.getC50()) == 0);
    assertTrue(dummyHill.compareTo(requestAssociatedItem.getHill()) == 0);
    assertNull(requestAssociatedItem.getSaturation());
    assertSame(invocationSource, request.getInvocationSource());
    final TypeCredibleIntervals typeCredibleIntervals = request.getCredibleIntervals();
    assertFalse(typeCredibleIntervals.isCalculateCredibleIntervals());
    assertNull(typeCredibleIntervals.getPercentiles());

    mocksControl.verify();

    mocksControl.reset();

    BigDecimal dummyDefaultSaturationLevel = new BigDecimal("55");
    ((AppManagerManagerImpl) appManagerManager).setDefaultSaturationLevel(dummyDefaultSaturationLevel);
    /* The default plasma values were assigned values during the previous run!
    expect(mockConfigurationActionPotential.getDefaultPlasmaConcCount())
          .andReturn(dummyDefaultPlasmaConcCount);
    expect(mockConfigurationActionPotential.isDefaultPlasmaConcLogScale())
          .andReturn(dummyDefaultPlasmaConcLogScale);
    */
    expect(mockJob.getPacingFrequency()).andReturn(dummyPacingFrequency);
    expect(mockJob.getCompoundConcentrations())
          .andReturn(dummyCompoundConcentrations);
    expect(mockJob.getGroupedInvocationInput())
          .andReturn(mockGroupedInvocationInput);
    expect(mockGroupedInvocationInput.getAssayGroupName())
          .andReturn(dummyAssayGroupName);
    expect(mockGroupedInvocationInput.getIonChannelValues())
          .andReturn(dummyIonChannelValues);
    expect(mockSimulationManager.find(dummySimulationId))
          .andReturn(mockSimulation);
    expect(mockSimulation.getCellModelIdentifier())
          .andReturn(dummyCellModelIdentifier);
    expect(mockSimulation.getPacingMaxTime()).andReturn(dummyPacingMaxTime);
    expect(mockJob.isUsingConfigurationCompoundConcentrations())
          .andReturn(dummyIsUsingDefaultCompoundConcentrations);
    expect(mockIonChannelValues.getSourceAssayName())
          .andReturn(dummySourceAssayName);
    expect(mockIonChannelValues.getIonChannelName())
          .andReturn(dummyIonChannelName).times(2);
    expect(mockIonChannelValues.getSourceAssayName())
          .andReturn(dummySourceAssayName);
    expect(mockIonChannelValues.getpIC50Data()).andReturn(dummyAllPIC50Data);
    expect(mockPIC50Data.getValue()).andReturn(dummyPIC50);
    expect(mockPIC50Data.getHill()).andReturn(dummyHill);
    mockJMSTemplate.sendProgress(anyObject(OverallProgress.class));

    mocksControl.replay();

    request = appManagerManager.createRunRequest(dummySimulationId, mockJob);
    assertNotNull(request);
    requestAssociatedItem = request.getICaL().getPIC50Data().get(0);
    assertTrue(dummyPIC50.compareTo(requestAssociatedItem.getC50()) == 0);
    assertTrue(dummyHill.compareTo(requestAssociatedItem.getHill()) == 0);
    assertTrue(dummyDefaultSaturationLevel.compareTo(requestAssociatedItem.getSaturation()) == 0);

    mocksControl.verify();
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testPersistResults() {
    final RetrieveAllResultsResponse mockResultsResponse = mocksControl.createMock(RetrieveAllResultsResponse.class);
    /*
     * No results, no warnings - simulation not completed.
     */
    expect(mockResultsResponse.getAppManagerId()).andReturn(dummyAppManagerId);
    final String dummyMessages = "dummyMessages";
    expect(mockResultsResponse.getMessages()).andReturn(dummyMessages);
    final String dummyDeltaAPD90PctileNames = "dummyDeltaAPD90PctileNames";
    expect(mockResultsResponse.getDeltaAPD90PercentileNames())
          .andReturn(dummyDeltaAPD90PctileNames);
    final List<Results> dummyResults = new ArrayList<Results>();
    expect(mockResultsResponse.getResults()).andReturn(dummyResults);
    final Capture<String> captureAppManagerId = newCapture();
    final Capture<String> captureMessages = newCapture();
    final Capture<String> captureDeltaAPD90PctileNames = newCapture();
    final Capture<Set<PerJobConcentrationResultsVO>> capturePJCResults = newCapture();
    expect(mockJobManager.persistResults(capture(captureAppManagerId),
                                         capture(captureMessages),
                                         capture(captureDeltaAPD90PctileNames),
                                         capture(capturePJCResults)))
          .andReturn(mockJob);
    expect(mockJob.getSimulationId()).andReturn(dummySimulationId);
    boolean dummySimulationCompleted = false;
    expect(mockSimulationManager.jobCompletedProcessing(dummySimulationId))
          .andReturn(dummySimulationCompleted);

    mocksControl.replay();

    BusinessDataUploadCompletedRequest request = appManagerManager.persistResults(mockResultsResponse);

    mocksControl.verify();

    assertSame(dummyAppManagerId, request.getAppManagerId());
    assertSame(dummyAppManagerId, Long.valueOf(captureAppManagerId.getValue()));
    assertSame(dummyMessages, captureMessages.getValue());
    assertTrue(capturePJCResults.getValue().isEmpty());

    mocksControl.reset();

    /*
     * No results, no warnings - simulation completed.
     */
    expect(mockResultsResponse.getAppManagerId()).andReturn(dummyAppManagerId);
    expect(mockResultsResponse.getMessages()).andReturn(dummyMessages);
    expect(mockResultsResponse.getDeltaAPD90PercentileNames())
          .andReturn(dummyDeltaAPD90PctileNames);
    expect(mockResultsResponse.getResults()).andReturn(dummyResults);
    expect(mockJobManager.persistResults(anyString(), anyString(), anyString(), 
                                         isA(Set.class)))
          .andReturn(mockJob);
    expect(mockJob.getSimulationId()).andReturn(dummySimulationId);
    dummySimulationCompleted = true;
    expect(mockSimulationManager.jobCompletedProcessing(dummySimulationId))
          .andReturn(dummySimulationCompleted);
    final Capture<OverallProgress> captureOverallProgress = newCapture();
    mockJMSTemplate.sendProgress(capture(captureOverallProgress));

    mocksControl.replay();

    request = appManagerManager.persistResults(mockResultsResponse);

    mocksControl.verify();

    final OverallProgress capturedOverallProgress = (OverallProgress) captureOverallProgress.getValue();
    assertSame(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertSame(InformationLevel.TRACE, capturedOverallProgress.getLevel());
    assertEquals(MessageKey.SYSTEM_TERMINATION_KEY.getBundleIdentifier(), capturedOverallProgress.getText());
  }

  @Test
  public void testRetrieveWorkload() {
    final ApplicationStatusResponse mockResponse = mocksControl.createMock(ApplicationStatusResponse.class);
    final Capture<ApplicationStatusRequest> captureRequest = newCapture();
    expect(mockAppManagerProxy.applicationStatusRequest(capture(captureRequest)))
          .andReturn(mockResponse);
    final String dummyStatus = "dummyStatus";
    expect(mockResponse.getStatus()).andReturn(dummyStatus);

    mocksControl.replay();

    final String workload = appManagerManager.retrieveWorkload();

    mocksControl.verify();

    assertEquals(dummyStatus, workload);
    final ApplicationStatusRequest capturedRequest = captureRequest.getValue();
    assertEquals(AppManagerManager.APP_MANAGER_WORKLOAD_IDENTIFIER,
                 capturedRequest.getData());
  }
}