/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit test the simulation request value object.
 *
 * @author geoff
 */
public class SimulationRequestVOTest {

  private String dummyCompoundIdentifier;
  private boolean dummyForceReRun;
  private boolean dummyReset;
  private boolean dummyAssayGrouping;
  private boolean dummyValueInheriting;
  private boolean dummyWithinGroups;
  private boolean dummyBetweenGroups;
  private short dummyCellModelIdentifier;
  private boolean dummyInputDataOnly;
  private BigDecimal dummyPacingMaxTime;

  @Before
  public void setUp() {
    dummyCompoundIdentifier = "dummyCompoundIdentifier";
    dummyForceReRun = true;
    dummyReset = false;
    dummyAssayGrouping = true;
    dummyValueInheriting = false;
    dummyWithinGroups = true;
    dummyBetweenGroups = false;
    dummyCellModelIdentifier = CellModelVO.MIN_MODEL_IDENTIFIER;
    dummyInputDataOnly = true;
    dummyPacingMaxTime = BigDecimal.ONE;
  }

  @Test
  public void testConstructor() {
    final SimulationRequestVO simReq = new SimulationRequestVO(dummyCompoundIdentifier,
                                                               dummyForceReRun,
                                                               dummyReset,
                                                               dummyAssayGrouping,
                                                               dummyValueInheriting,
                                                               dummyWithinGroups,
                                                               dummyBetweenGroups,
                                                               dummyCellModelIdentifier,
                                                               dummyPacingMaxTime,
                                                               dummyInputDataOnly);
    assertEquals(dummyCompoundIdentifier, simReq.getCompoundIdentifier());
    assertEquals(dummyForceReRun, simReq.isForceReRun());
    assertEquals(dummyReset, simReq.isReset());
    assertEquals(dummyAssayGrouping, simReq.isAssayGrouping());
    assertEquals(dummyValueInheriting, simReq.isValueInheriting());
    assertEquals(dummyWithinGroups, simReq.isWithinGroups());
    assertEquals(dummyBetweenGroups, simReq.isBetweenGroups());
    assertEquals(dummyCellModelIdentifier, simReq.getCellModelIdentifier());
    assertTrue(dummyPacingMaxTime.compareTo(simReq.getPacingMaxTime()) == 0);
    assertEquals(dummyInputDataOnly, simReq.isInputDataOnly());

    assertNotNull(simReq.toString());

    assertTrue(simReq.equals(simReq));
    final SimulationRequestVO simReq2 = simReq;
    assertTrue(simReq.equals(simReq2));

    dummyWithinGroups = !dummyWithinGroups;
    final SimulationRequestVO simReq3 = new SimulationRequestVO(dummyCompoundIdentifier,
                                                                dummyForceReRun,
                                                                dummyReset,
                                                                dummyAssayGrouping,
                                                                dummyValueInheriting,
                                                                dummyWithinGroups,
                                                                dummyBetweenGroups,
                                                                dummyCellModelIdentifier,
                                                                dummyPacingMaxTime,
                                                                dummyInputDataOnly);
    assertFalse(simReq.equals(simReq3));
  }
}