/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.information.provenance;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.AbstractProvenance;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Unit test the abstract provenance information.
 *
 * @author geoff
 */
public class AbstractProvenanceTest {

  private InformationLevel dummyInformationLevel;
  private String[] dummyArgs;
  private String dummyText;
  private Long dummySimulationId;

  private class TestAbstractProvenance extends AbstractProvenance {
    private static final long serialVersionUID = 1L;
    protected TestAbstractProvenance(Builder builder) {
      super(builder);
    }
  }

  @Before
  public void setUp() {
    dummyArgs = null;
    dummyInformationLevel = null;
    dummyText = null;
    dummySimulationId = null;
  }

  @Test
  public void testThreeArgConstructor() {
    dummyText = "dummyText";
    dummySimulationId = 3L;
    try {
      new AbstractProvenance.Builder(dummyInformationLevel, dummyText, dummySimulationId);
      fail("Should not permit the assignment of null information level in constructor.");
    } catch (IllegalArgumentException e) {}

    dummyInformationLevel = InformationLevel.TRACE;
    dummyText = null;
    try {
      new AbstractProvenance.Builder(dummyInformationLevel, dummyText, dummySimulationId);
      fail("Should not permit the assignment of null text in constructor.");
    } catch (IllegalArgumentException e) {}

    dummyInformationLevel = InformationLevel.TRACE;
    dummyText = "  ";
    try {
      new AbstractProvenance.Builder(dummyInformationLevel, dummyText, dummySimulationId);
      fail("Should not permit the assignment of blank text in constructor.");
    } catch (IllegalArgumentException e) {}

    dummyText = "dummyText";
    dummySimulationId = null;
    try {
      new AbstractProvenance.Builder(dummyInformationLevel, dummyText, dummySimulationId);
      fail("Should not permit the assignment of null simulationId.");
    } catch (IllegalArgumentException e) {}

    dummySimulationId = 3L;
    final AbstractProvenance.Builder builder = new AbstractProvenance.Builder(dummyInformationLevel,
                                                                              dummyText,
                                                                              dummySimulationId);
    final TestAbstractProvenance testProvenance = new TestAbstractProvenance(builder);
    assertEquals(dummyInformationLevel, testProvenance.getLevel());
    assertTrue(dummyText.equalsIgnoreCase(testProvenance.getText()));
    assertTrue(testProvenance.getArgs().isEmpty());
    assertEquals(dummySimulationId, testProvenance.getSimulationId());
    assertNotNull(testProvenance.toString());
  }

  @Test
  public void testFourArgConstructor() {
    dummyInformationLevel = InformationLevel.TRACE;
    dummyText = "dummyText";
    dummySimulationId = 3L;
    AbstractProvenance.Builder builder = new AbstractProvenance.Builder(dummyInformationLevel,
                                                                        dummyText, dummyArgs,
                                                                        dummySimulationId);
    TestAbstractProvenance testProvenance = new TestAbstractProvenance(builder);
    assertTrue(testProvenance.getArgs().isEmpty());

    dummyArgs = new String[] {};
    builder = new AbstractProvenance.Builder(dummyInformationLevel, dummyText, dummyArgs,
                                             dummySimulationId);
    testProvenance = new TestAbstractProvenance(builder);

    assertTrue(testProvenance.getArgs().isEmpty());

    final String arg1 = "fish";
    final String arg2 = "fish";
    final String arg3 = "chips";

    dummyArgs = new String[] { arg1, arg2, arg3 };
    builder = new AbstractProvenance.Builder(dummyInformationLevel, dummyText, dummyArgs,
                                             dummySimulationId);
    testProvenance = new TestAbstractProvenance(builder);

    assertEquals(3, testProvenance.getArgs().size());
    assertTrue(arg1.equalsIgnoreCase(testProvenance.getArgs().get(0)));
    assertTrue(arg3.equalsIgnoreCase(testProvenance.getArgs().get(2)));
  }
}