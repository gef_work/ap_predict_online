/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import static org.easymock.EasyMock.createControl;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata.ProcessedSiteDataVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.workflow.OutcomeVO;

/**
 * Test that the channel's processed data payload routing works.
 *
 * @author geoff
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( { "classpath:/META-INF/spring/ctx/integration/simulation/processing/ProcessedDataPayloadRoutingIT-context.xml" } )
public class ProcessedDataPayloadRoutingIT {

  @Autowired @Qualifier("processSimulation_channel_toPostProcessingRouter")
  MessageChannel toPostProcessingRouter;

  @Autowired @Qualifier("testPostProcessingRecorderChannel")
  QueueChannel toPostProcessingRecorderChannel;

  @Autowired @Qualifier("testNoSiteDataToProcessChannel")
  QueueChannel toNoSiteDataToProcess;

  @Test
  public void testWith() {
    final List<OutcomeVO> qsarResults = new ArrayList<OutcomeVO>();
    final IMocksControl mocksControl = createControl();
    final OutcomeVO mockOutcomeVO = mocksControl.createMock(OutcomeVO.class);

    qsarResults.add(mockOutcomeVO);
    // a results object full of nulls counts as a result!
    ProcessedSiteDataVO processedSiteData = new ProcessedSiteDataVO(null, qsarResults, null);
    Message<ProcessedSiteDataVO> inMessage = MessageBuilder.withPayload(processedSiteData).build();
    toPostProcessingRouter.send(inMessage);

    Message<?> outMessage1 = toPostProcessingRecorderChannel.receive(0);
    Message<?> outMessage2 = toNoSiteDataToProcess.receive(0);
    assertNotNull("Expecting an output message on recording channel", outMessage1);
    assertNull("Not expecting an output message on 'no site data' channel", outMessage2);
  }

  @Test
  public void testWithout() {
    ProcessedSiteDataVO processedSiteData = new ProcessedSiteDataVO(null, null, null);
    Message<ProcessedSiteDataVO> inMessage = MessageBuilder.withPayload(processedSiteData).build();
    toPostProcessingRouter.send(inMessage);

    Message<?> outMessage1 = toNoSiteDataToProcess.receive(0);
    Message<?> outMessage2 = toPostProcessingRecorderChannel.receive(0);
    assertNotNull("Expecting an output message", outMessage1);
    assertNull("Not expecting an output message on 'recording' channel", outMessage2);
  }

}