/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.business_manager.request_validator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;
import org.springframework.core.Ordered;

import uk.ac.ox.cs.epsrc.business_manager.ws.business_manager.request_validator.FundamentalCompoundIdentifiersValidator;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ObjectFactory;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProcessSimulationsRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.SchemaObjectBuilder;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidCompoundIdentifierException;

/**
 * Unit test the default compound names validator.
 *
 * @author geoff
 */
public class FundamentalCompoundIdentifiersValidatorTest {

  private final int defaultMaxCompoundIdentifierLength = FundamentalCompoundIdentifiersValidator.DEFAULT_MAX_COMPOUND_IDENTIFIER_LENGTH;

  private FundamentalCompoundIdentifiersValidator compoundIdentifiersValidator;

  private static final ObjectFactory objectFactory = new ObjectFactory();

  @Before
  public void setUp() {
    compoundIdentifiersValidator = new FundamentalCompoundIdentifiersValidator();
  }

  /**
   * Test that this validator will always be the first to be invoked.
   */
  @Test
  public void testOrder() {
    assertEquals(Ordered.HIGHEST_PRECEDENCE, compoundIdentifiersValidator.getOrder());
  }

  /**
   * Test the ability to adjust the default maximum compound length.
   */
  @Test
  public void testSettingDefaultMaximumCompoundIdentifierLength() throws InvalidCompoundIdentifierException {
    final ProcessSimulationsRequest processSimulationsRequest = SchemaObjectBuilder.createValidProcessSimulationsRequest();
    try {
      compoundIdentifiersValidator.validate(processSimulationsRequest);
    } catch (InvalidCompoundIdentifierException e) {
      fail("Should not have failed with a supposedly valid process simulations request!");
    }

    try {
      FundamentalCompoundIdentifiersValidator.setDefaultMaxCompoundIdentifierLength(0);
      fail("Should not have accepted an unrealistic default max compound name length to be assigned.");
    } catch (AssertionError e) { }

    FundamentalCompoundIdentifiersValidator.setDefaultMaxCompoundIdentifierLength(5);

    try {
      compoundIdentifiersValidator.validate(processSimulationsRequest);
      fail("Should not have considered a compound name length greater than the maximum allowed.");
    } catch (InvalidCompoundIdentifierException e) {
      // reset the default for other testing.
      FundamentalCompoundIdentifiersValidator.setDefaultMaxCompoundIdentifierLength(defaultMaxCompoundIdentifierLength);
    }
  }

  /**
   * Test processing fails if the request object is incomplete
   */
  @Test
  public void testValidationSuccessOnInvalidRequest() {
    ProcessSimulationsRequest processSimulationsRequest = null;

    processSimulationsRequest = objectFactory.createProcessSimulationsRequest();
    try {
      compoundIdentifiersValidator.validate(processSimulationsRequest);
      fail("Request validator should not permit a request with no compound names.");
    } catch (InvalidCompoundIdentifierException e) {
      //
    }

    processSimulationsRequest = SchemaObjectBuilder.createInvalidProcessSimulationsRequest();
    try {
      compoundIdentifiersValidator.validate(processSimulationsRequest);
      fail("Request validator should not permit a request with a compound names value with length exceeding the max. permitted");
    } catch (InvalidCompoundIdentifierException e) {
      //
    }
  }

  /**
   * Test that the validator complains if it doesn't recognize the class of the object containing the compound names. 
   */
  @Test(expected=UnsupportedOperationException.class)
  public void testContainingObjectClass() throws InvalidCompoundIdentifierException {
    final ProcessSimulationsRequest processSimulationsRequest = SchemaObjectBuilder.createValidProcessSimulationsRequest();
    compoundIdentifiersValidator.validate(processSimulationsRequest);

    final Object unspecifiedObjectRequest = new Object();
    compoundIdentifiersValidator.validate(unspecifiedObjectRequest);
  }
}