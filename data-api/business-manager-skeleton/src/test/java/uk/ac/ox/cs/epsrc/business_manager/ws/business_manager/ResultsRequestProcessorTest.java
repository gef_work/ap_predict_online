/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.business_manager;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.service.ResultsService;
import uk.ac.ox.cs.epsrc.business_manager.service.SimulationService;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.ConcentrationResultVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.JobDiagnosticsVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.JobResultsVO;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ResultsConcentrationStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ResultsJobDataStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ResultsRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ResultsResponse;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.RetrieveDiagnosticsRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.RetrieveDiagnosticsResponse;

/**
 * Unit test results request processor.
 *
 * @author geoff
 */
public class ResultsRequestProcessorTest {

  private IMocksControl mocksControl;
  private JobDiagnosticsVO mockJobDiagnostics;
  private long dummyJobId = 23l;
  private long dummySimulationId = 13l;
  private ResultsRequest mockResultsRequest;
  private ResultsRequestProcessor resultsRequestProcessor;
  private ResultsService mockResultsService;
  private RetrieveDiagnosticsRequest mockRetrieveDiagnosticsRequest;
  private SimulationService mockSimulationService;

  @Before
  public void setUp() {
    resultsRequestProcessor = new ResultsRequestProcessor();

    mocksControl = createStrictControl();

    mockResultsRequest = mocksControl.createMock(ResultsRequest.class);
    mockResultsService = mocksControl.createMock(ResultsService.class);
    mockSimulationService = mocksControl.createMock(SimulationService.class);

    ReflectionTestUtils.setField(resultsRequestProcessor,
                                 BusinessIdentifiers.COMPONENT_RESULTS_SERVICE,
                                 mockResultsService);
    ReflectionTestUtils.setField(resultsRequestProcessor,
                                 BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE,
                                 mockSimulationService);

    mockJobDiagnostics = mocksControl.createMock(JobDiagnosticsVO.class);
    mockRetrieveDiagnosticsRequest = mocksControl.createMock(RetrieveDiagnosticsRequest.class);
  }

  @Test
  public void testRetrieveDiagnostics() {
    expect(mockRetrieveDiagnosticsRequest.getJobId()).andReturn(dummyJobId);
    expect(mockResultsService.retrieveDiagnostics(dummyJobId))
          .andReturn(mockJobDiagnostics);
    final String dummyInfo = "dummyInfo";
    final String dummyOutput = "dummyOutput";
    expect(mockJobDiagnostics.getInfo()).andReturn(dummyInfo);
    expect(mockJobDiagnostics.getOutput()).andReturn(dummyOutput);

    mocksControl.replay();

    final RetrieveDiagnosticsResponse returnedResponse = resultsRequestProcessor.retrieveDiagnostics(mockRetrieveDiagnosticsRequest);

    mocksControl.verify();

    assertSame(dummyJobId, returnedResponse.getJobId());
    assertSame(dummyInfo, returnedResponse.getInfo());
    assertSame(dummyOutput, returnedResponse.getOutput());
  }

  @Test
  public void testRetrieveResults() {
    /*
     * 1. No job results
     */
    List<JobResultsVO> dummyJobResults = new ArrayList<JobResultsVO>();
    expect(mockResultsRequest.getSimulationId()).andReturn(dummySimulationId);
    expect(mockResultsService.retrieveResults(dummySimulationId))
          .andReturn(dummyJobResults);

    mocksControl.replay();

    ResultsResponse response = resultsRequestProcessor.retrieveResults(mockResultsRequest);

    mocksControl.verify();

    assertSame(0l, response.getSimulationId());
    assertTrue(response.getResultsJobDataStructure().isEmpty());

    mocksControl.reset();

    /*
     * 2. Job results
     */
    final JobResultsVO mockJobResultsVO = mocksControl.createMock(JobResultsVO.class);
    dummyJobResults.add(mockJobResultsVO);

    expect(mockResultsRequest.getSimulationId()).andReturn(dummySimulationId);
    expect(mockResultsService.retrieveResults(dummySimulationId))
          .andReturn(dummyJobResults);

    final long dummyJobId = 32l;
    final String dummyMessages = "dummyMessages";
    final String dummyDeltaAPD90PctileNames = "dummyDeltaAPD90PctileNames";
    final List<ConcentrationResultVO> dummyConcentrationResults = new ArrayList<ConcentrationResultVO>();
    final ConcentrationResultVO mockConcentrationResultVO = mocksControl.createMock(ConcentrationResultVO.class);
    dummyConcentrationResults.add(mockConcentrationResultVO);
    final Float dummyPacingFrequency = 4.4F;
    final String dummyAssayGroupName = "dummyAssayGroupName";
    final short dummyAssayGroupLevel = 2;

    expect(mockJobResultsVO.getJobId()).andReturn(dummyJobId);
    expect(mockJobResultsVO.getMessages()).andReturn(dummyMessages);
    expect(mockJobResultsVO.getDeltaAPD90PercentileNames())
          .andReturn(dummyDeltaAPD90PctileNames);
    expect(mockJobResultsVO.getConcentrationResults())
          .andReturn(dummyConcentrationResults);
    expect(mockJobResultsVO.getPacingFrequency())
          .andReturn(dummyPacingFrequency);
    expect(mockJobResultsVO.getAssayGroupName())
          .andReturn(dummyAssayGroupName);
    expect(mockJobResultsVO.getAssayGroupLevel())
          .andReturn(dummyAssayGroupLevel);

    final float dummyCompoundConcentration = 3.2f;
    final String dummyDeltaAPD90 = "dummyDeltaAPD90";
    final String dummyUpstrokeVelocity = "dummyUpstrokeVelocity";
    final String dummyTimes = "dummyTimes";
    final String dummyVoltages = "dummyVoltages";
    final String dummyQNet = "dummyQNet";

    expect(mockConcentrationResultVO.getCompoundConcentration())
          .andReturn(dummyCompoundConcentration);
    expect(mockConcentrationResultVO.getDeltaAPD90())
          .andReturn(dummyDeltaAPD90);
    expect(mockConcentrationResultVO.getUpstrokeVelocity())
          .andReturn(dummyUpstrokeVelocity);
    expect(mockConcentrationResultVO.getTimes()).andReturn(dummyTimes);
    expect(mockConcentrationResultVO.getVoltages()).andReturn(dummyVoltages);
    expect(mockConcentrationResultVO.getQNet()).andReturn(dummyQNet);

    mocksControl.replay();

    response = resultsRequestProcessor.retrieveResults(mockResultsRequest);

    mocksControl.verify();

    assertSame(0l, response.getSimulationId());
    final List<ResultsJobDataStructure> jobData = response.getResultsJobDataStructure();
    assertSame(1, jobData.size());
    final ResultsJobDataStructure jobDataStructure = jobData.get(0);
    assertSame(dummyJobId, jobDataStructure.getJobId());
    assertEquals(dummyPacingFrequency.floatValue(),
                 jobDataStructure.getJobPacingFrequency(), 0);
    assertEquals(dummyAssayGroupName, jobDataStructure.getJobAssayGroupName());
    assertSame(dummyAssayGroupLevel, jobDataStructure.getJobAssayGroupLevel());
    assertEquals(dummyMessages, jobDataStructure.getMessages());
    assertEquals(dummyDeltaAPD90PctileNames,
                 jobDataStructure.getDeltaAPD90PercentileNames());
    final List<ResultsConcentrationStructure> jobConcRslts = jobDataStructure.getResultsConcentrationStructure();
    assertSame(1, jobConcRslts.size());
    final ResultsConcentrationStructure rsltsConcStructure = jobConcRslts.get(0);
    assertEquals(dummyCompoundConcentration,
                 rsltsConcStructure.getCompoundConcentration(), 0);
    assertEquals(dummyDeltaAPD90, rsltsConcStructure.getDeltaAPD90());
    assertEquals(dummyUpstrokeVelocity, rsltsConcStructure.getUpstrokeVelocity());
    assertEquals(dummyTimes, rsltsConcStructure.getTimes());
    assertEquals(dummyVoltages, rsltsConcStructure.getVoltages());
    assertEquals(dummyQNet, rsltsConcStructure.getQNet());
  }
}