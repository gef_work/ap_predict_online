/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.manager;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.SimulationProcessingGateway;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.FundamentalProvenance;
import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.epsrc.business_manager.config.ConfigurationActionPotential;
import uk.ac.ox.cs.epsrc.business_manager.dao.SimulationDAO;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager;
import uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager;
import uk.ac.ox.cs.epsrc.business_manager.ws.app_manager.manager.AppManagerManager;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Simulation manager implementation processing unit testing.
 *
 * @author geoff
 */
public class SimulationManagerImplProcessingTest {

  private AppManagerManager mockAppManagerManager;
  private boolean dummyForcingReset;
  private BusinessManagerJMSTemplate mockJMSTemplate;
  private Configuration mockConfiguration;
  private ConfigurationActionPotential mockConfigurationActionPotential;
  private IMocksControl mocksControl;
  private InformationManager mockInformationManager;
  private JobManager mockJobManager;
  private static final long dummySimulationId = 3l;
  private Simulation mockSimulation;
  private SimulationDAO mockSimulationDAO;
  private SimulationManager simulationManager;
  private SimulationProcessingGateway mockSimulationProcessingGateway;
  private String dummyCompoundIdentifier;

  @Before
  public void setUp() {
    simulationManager = new SimulationManagerImpl();

    mocksControl = createStrictControl();
    mockAppManagerManager = mocksControl.createMock(AppManagerManager.class);
    mockJMSTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    mockConfiguration = mocksControl.createMock(Configuration.class);
    mockConfigurationActionPotential = mocksControl.createMock(ConfigurationActionPotential.class);
    mockInformationManager = mocksControl.createMock(InformationManager.class);
    mockJobManager = mocksControl.createMock(JobManager.class);
    mockSimulation = mocksControl.createMock(Simulation.class);
    mockSimulationDAO = mocksControl.createMock(SimulationDAO.class);
    mockSimulationProcessingGateway = mocksControl.createMock(SimulationProcessingGateway.class);

    ReflectionTestUtils.setField(simulationManager, BusinessIdentifiers.COMPONENT_APP_MANAGER_MANAGER,
                                 mockAppManagerManager);
    ReflectionTestUtils.setField(simulationManager, BusinessIdentifiers.COMPONENT_CONFIGURATION,
                                 mockConfiguration);
    ReflectionTestUtils.setField(simulationManager, BusinessIdentifiers.COMPONENT_CONFIGURATION_ACTIONPOTENTIAL,
                                 mockConfigurationActionPotential);
    ReflectionTestUtils.setField(simulationManager, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockJMSTemplate);
    ReflectionTestUtils.setField(simulationManager, BusinessIdentifiers.COMPONENT_JOB_MANAGER,
                                 mockJobManager);
    ReflectionTestUtils.setField(simulationManager, BusinessIdentifiers.COMPONENT_INFORMATION_MANAGER,
                                 mockInformationManager);
    ReflectionTestUtils.setField(simulationManager, BusinessIdentifiers.COMPONENT_SIMULATION_DAO,
                                 mockSimulationDAO);
    ReflectionTestUtils.setField(simulationManager, BusinessIdentifiers.COMPONENT_SIMULATION_PROCESSING_GATEWAY,
                                 mockSimulationProcessingGateway);

    dummyCompoundIdentifier = "dummyCompoundIdentifier";
  }

  @Test
  public void testForceReRun() {
    /*
     * 1. Force re-run when no running jobs (and not resetting)
     */
    expect(mockSimulation.getId()).andReturn(dummySimulationId);

    // >> resetSimulation()
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    final Set<String> dummyIdsToDelete = new HashSet<String>();
    expect(mockJobManager.purgeSimulationJobData(dummySimulationId)).andReturn(dummyIdsToDelete);
    dummyForcingReset = false;
    expect(mockSimulation.forcingReset()).andReturn(dummyForcingReset);
    // << resetSimulation()

    // >> purgeAndRun()
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockSimulation.getCompoundIdentifier()).andReturn(dummyCompoundIdentifier);
    expect(mockJobManager.purgeSimulationJobData(dummySimulationId)).andReturn(dummyIdsToDelete);
    mockSimulation.resetSystemState();
    //                  >> save()
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockSimulationDAO.store(mockSimulation)).andReturn(mockSimulation);
    //                  << save()
    mockInformationManager.purge(dummySimulationId);
    Capture<OverallProgress> captureOverallProgress = newCapture();
    mockJMSTemplate.sendProgress(capture(captureOverallProgress));
    Capture<FundamentalProvenance> captureFundamentalProvenance = newCapture();
    mockJMSTemplate.sendProvenance(capture(captureFundamentalProvenance));
    mockSimulationProcessingGateway.regularSimulation(mockSimulation);
    // << purgeAndRun()

    mocksControl.replay();

    Simulation processedSimulation = simulationManager.processingForceReRun(mockSimulation);

    mocksControl.verify();

    assertSame(mockSimulation, processedSimulation);

    OverallProgress capturedOverallProgress = (OverallProgress) captureOverallProgress.getValue();
    assertSame(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertSame(dummyCompoundIdentifier, capturedOverallProgress.getCompoundIdentifier());
    assertSame(InformationLevel.INFO, capturedOverallProgress.getLevel());
    assertEquals(MessageKey.RERUN_PROCESSING.getBundleIdentifier(),
                 capturedOverallProgress.getText());
    assertFalse(capturedOverallProgress.isTerminating());

    FundamentalProvenance capturedFundamentalProvenance = (FundamentalProvenance) captureFundamentalProvenance.getValue();
    assertSame(dummySimulationId, capturedFundamentalProvenance.getSimulationId());
    assertSame(dummyCompoundIdentifier, capturedFundamentalProvenance.getCompoundIdentifier());
    assertSame(InformationLevel.DEBUG, capturedFundamentalProvenance.getLevel());
    assertEquals(MessageKey.RERUN_PROCESSING.getBundleIdentifier(),
                 capturedFundamentalProvenance.getText());

    mocksControl.reset();

    /*
     * 2. Force re-run when running jobs (and not resetting)
     */
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    // >> resetSimulation()
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    dummyIdsToDelete.clear();
    final String dummyIdToDelete = "dummyIdToDelete";
    dummyIdsToDelete.add(dummyIdToDelete);
    expect(mockJobManager.purgeSimulationJobData(dummySimulationId)).andReturn(dummyIdsToDelete);
    final Set<String> deletedIds = new HashSet<String>();
    expect(mockAppManagerManager.deleteJobs(dummyIdsToDelete)).andReturn(deletedIds);
    // << resetSimulation()

    // >> purgeAndRun()
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockSimulation.getCompoundIdentifier()).andReturn(dummyCompoundIdentifier);
    expect(mockJobManager.purgeSimulationJobData(dummySimulationId)).andReturn(dummyIdsToDelete);
    mockSimulation.resetSystemState();
    //                  >> save()
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockSimulationDAO.store(mockSimulation)).andReturn(mockSimulation);
    //                  << save()
    mockInformationManager.purge(dummySimulationId);
    captureOverallProgress = newCapture();
    mockJMSTemplate.sendProgress(capture(captureOverallProgress));
    captureFundamentalProvenance = newCapture();
    mockJMSTemplate.sendProvenance(capture(captureFundamentalProvenance));
    mockSimulationProcessingGateway.regularSimulation(mockSimulation);
    // << purgeAndRun()

    mocksControl.replay();

    processedSimulation = simulationManager.processingForceReRun(mockSimulation);

    mocksControl.verify();

    assertSame(mockSimulation, processedSimulation);

    capturedOverallProgress = (OverallProgress) captureOverallProgress.getValue();
    assertSame(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertSame(dummyCompoundIdentifier, capturedOverallProgress.getCompoundIdentifier());
    assertSame(InformationLevel.INFO, capturedOverallProgress.getLevel());
    assertEquals(MessageKey.RERUN_PROCESSING.getBundleIdentifier(),
                 capturedOverallProgress.getText());
    assertFalse(capturedOverallProgress.isTerminating());

    capturedFundamentalProvenance = (FundamentalProvenance) captureFundamentalProvenance.getValue();
    assertSame(dummySimulationId, capturedFundamentalProvenance.getSimulationId());
    assertSame(dummyCompoundIdentifier, capturedFundamentalProvenance.getCompoundIdentifier());
    assertSame(InformationLevel.DEBUG, capturedFundamentalProvenance.getLevel());
    assertEquals(MessageKey.RERUN_PROCESSING.getBundleIdentifier(),
                 capturedFundamentalProvenance.getText());
  }

  @Test
  public void testForceReset() {
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockSimulation.getCompoundIdentifier()).andReturn(dummyCompoundIdentifier);

    mockSimulation.resetSystemState(true);
    // >> resetSimulation()
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    final Set<String> dummyIdsToDelete = new HashSet<String>();
    expect(mockJobManager.purgeSimulationJobData(dummySimulationId)).andReturn(dummyIdsToDelete);
    dummyForcingReset = false;
    expect(mockSimulation.forcingReset()).andReturn(dummyForcingReset);
    //                      >> save()
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockSimulationDAO.store(mockSimulation)).andReturn(mockSimulation);
    // << resetSimulation() << save()
    mockInformationManager.purge(dummySimulationId);

    Capture<OverallProgress> captureOverallProgress = newCapture();
    mockJMSTemplate.sendProgress(capture(captureOverallProgress));
    Capture<FundamentalProvenance> captureFundamentalProvenance = newCapture();
    mockJMSTemplate.sendProvenance(capture(captureFundamentalProvenance));
    mocksControl.replay();

    final Simulation processedSimulation = simulationManager.processingForceReset(mockSimulation);

    mocksControl.verify();

    assertSame(mockSimulation, processedSimulation);

    final OverallProgress capturedOverallProgress = (OverallProgress) captureOverallProgress.getValue();
    assertSame(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertSame(dummyCompoundIdentifier, capturedOverallProgress.getCompoundIdentifier());
    assertSame(InformationLevel.DEBUG, capturedOverallProgress.getLevel());
    assertEquals(MessageKey.RESET_PROCESSING.getBundleIdentifier(),
                 capturedOverallProgress.getText());
    assertTrue(capturedOverallProgress.isTerminating());

    final FundamentalProvenance capturedFundamentalProvenance = (FundamentalProvenance) captureFundamentalProvenance.getValue();
    assertSame(dummySimulationId, capturedFundamentalProvenance.getSimulationId());
    assertSame(dummyCompoundIdentifier, capturedFundamentalProvenance.getCompoundIdentifier());
    assertSame(InformationLevel.DEBUG, capturedFundamentalProvenance.getLevel());
    assertEquals(MessageKey.RESET_PROCESSING.getBundleIdentifier(),
                 capturedFundamentalProvenance.getText());
  }

  @Test
  public void testRegularNotCompleted() {
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    // * Not completed!
    expect(mockSimulation.getCompleted()).andReturn(null);
    final Capture<OverallProgress> captureOverallProgress = newCapture();
    mockJMSTemplate.sendProgress(capture(captureOverallProgress));
    mockSimulationProcessingGateway.regularSimulation(mockSimulation);

    mocksControl.replay();

    final Simulation processedSimulation = simulationManager.processingRegular(mockSimulation);

    mocksControl.verify();

    assertSame(mockSimulation, processedSimulation);

    final OverallProgress capturedOverallProgress = (OverallProgress) captureOverallProgress.getValue();
    assertNull(capturedOverallProgress.getCompoundIdentifier());
    assertSame(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertSame(InformationLevel.TRACE, capturedOverallProgress.getLevel());
    assertEquals("Simulation processing workflow initiated.", capturedOverallProgress.getText());
    assertFalse(capturedOverallProgress.isTerminating());
  }
}