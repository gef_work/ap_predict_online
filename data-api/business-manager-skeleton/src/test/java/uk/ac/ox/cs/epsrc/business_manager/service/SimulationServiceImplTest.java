/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.service;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedSimulationRequestVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.SimulationRequestVO;

/**
 * Simulation service implementation unit testing.
 *
 * @author geoff
 */
public class SimulationServiceImplTest {

  private IMocksControl mocksControl;
  private static final long dummySimulationId = 3l;
  private Simulation mockSimulation;
  private SimulationManager mockSimulationManager;
  private SimulationService simulationService;

  @Before
  public void setUp() {
    simulationService = new SimulationServiceImpl();

    mocksControl = createStrictControl();
    mockSimulation = mocksControl.createMock(Simulation.class);
    mockSimulationManager = mocksControl.createMock(SimulationManager.class);

    ReflectionTestUtils.setField(simulationService, BusinessIdentifiers.COMPONENT_SIMULATION_MANAGER,
                                 mockSimulationManager);
  }


  @Test
  public void testFindBySimulationId() {
    expect(mockSimulationManager.find(dummySimulationId)).andReturn(mockSimulation);

    mocksControl.replay();

    final Simulation returnedSimulation = simulationService.findBySimulationId(dummySimulationId);

    mocksControl.verify();

    assertSame(mockSimulation, returnedSimulation);
  }

  @Test
  public void testProcessSimulation() {
    /*
     * 1. Test forcing re-run
     */
    // >> findBySimulationId()
    expect(mockSimulationManager.find(dummySimulationId)).andReturn(mockSimulation);
    // << findBySimulationId()
    boolean dummyForceReRun = true;
    expect(mockSimulation.forcingReRun()).andReturn(dummyForceReRun);
    expect(mockSimulationManager.processingForceReRun(mockSimulation)).andReturn(mockSimulation);

    mocksControl.replay();

    Simulation processed = simulationService.processSimulation(dummySimulationId);

    mocksControl.verify();

    assertSame(mockSimulation, processed);

    mocksControl.reset();

    /*
     * 2. Test forcing reset.
     */
    // >> findBySimulationId()
    expect(mockSimulationManager.find(dummySimulationId)).andReturn(mockSimulation);
    // << findBySimulationId()
    dummyForceReRun = false;
    expect(mockSimulation.forcingReRun()).andReturn(dummyForceReRun);
    boolean dummyForceReset = true;
    expect(mockSimulation.forcingReset()).andReturn(dummyForceReset);
    expect(mockSimulationManager.processingForceReset(mockSimulation)).andReturn(mockSimulation);

    mocksControl.replay();

    processed = simulationService.processSimulation(dummySimulationId);

    mocksControl.verify();

    assertSame(mockSimulation, processed);

    mocksControl.reset();

    /*
     * 3. Test regular processing.
     */
    // >> findBySimulationId()
    expect(mockSimulationManager.find(dummySimulationId)).andReturn(mockSimulation);
    // << findBySimulationId()
    dummyForceReRun = false;
    expect(mockSimulation.forcingReRun()).andReturn(dummyForceReRun);
    dummyForceReset = false;
    expect(mockSimulation.forcingReset()).andReturn(dummyForceReset);
    expect(mockSimulationManager.processingRegular(mockSimulation)).andReturn(mockSimulation);

    mocksControl.replay();

    processed = simulationService.processSimulation(dummySimulationId);

    mocksControl.verify();

    assertSame(mockSimulation, processed);
  }

  @Test
  public void testProcessSimulationsRequest() {
    /*
     * 1. Empty verified requests collection.
     */
    final List<SimulationRequestVO> dummyVerifiedSimulationRequests = new ArrayList<SimulationRequestVO>();

    mocksControl.replay();

    List<ProcessedSimulationRequestVO> processed = simulationService.processSimulationsRequest(dummyVerifiedSimulationRequests);

    mocksControl.verify();

    assertTrue(processed.isEmpty());

    mocksControl.reset();

    /*
     * 2. Single verified request and simulation exists.
     */
    final SimulationRequestVO mockSimulationRequestVO = mocksControl.createMock(SimulationRequestVO.class);
    dummyVerifiedSimulationRequests.add(mockSimulationRequestVO);
    ProcessedSimulationRequestVO mockProcessedSimulationDataRequestVO = mocksControl.createMock(ProcessedSimulationRequestVO.class);
    boolean dummyIsInputDataOnly = false;
    expect(mockSimulationRequestVO.isInputDataOnly()).andReturn(dummyIsInputDataOnly);
    expect(mockSimulationManager.find(mockSimulationRequestVO)).andReturn(mockSimulation);
    expect(mockSimulationManager.requestProcessingExistingSimulation(mockSimulation,
                                                                     mockSimulationRequestVO))
                                .andReturn(mockProcessedSimulationDataRequestVO);

    mocksControl.replay();

    processed = simulationService.processSimulationsRequest(dummyVerifiedSimulationRequests);

    mocksControl.verify();

    assertSame(1, processed.size());
    assertSame(mockProcessedSimulationDataRequestVO, processed.get(0));

    mocksControl.reset();
 
    /*
     * 3. Single verified request and simulation doesn't exist.
     */
    expect(mockSimulationRequestVO.isInputDataOnly()).andReturn(dummyIsInputDataOnly);
    expect(mockSimulationManager.find(mockSimulationRequestVO)).andReturn(null);
    expect(mockSimulationManager.requestProcessingNewSimulation(mockSimulationRequestVO))
                                .andReturn(mockProcessedSimulationDataRequestVO);

    mocksControl.replay();

    processed = simulationService.processSimulationsRequest(dummyVerifiedSimulationRequests);

    mocksControl.verify();

    assertSame(1, processed.size());
    assertSame(mockProcessedSimulationDataRequestVO, processed.get(0));
  }

  @Test
  public void testUserInputValidation() {
    final String dummyCompoundIdentifier = "dummyCompoundIdentifier";
    final boolean dummyForceReRun = true;
    final boolean dummyReset = false;
    final boolean dummyAssayGrouping = true;
    final boolean dummyValueInheriting = false;
    final boolean dummyBetweenGroups = true;
    final boolean dummyWithinGroups = false;
    final short dummyCellModelIdentifier = 5;
    final BigDecimal dummyPacingMaxTime = BigDecimal.ONE;
    final boolean dummyInputDataOnly = true;

    final SimulationRequestVO mockSimulationRequestVO = mocksControl.createMock(SimulationRequestVO.class);
    expect(mockSimulationManager.userInputValidation(dummyCompoundIdentifier,
                                                     dummyForceReRun,
                                                     dummyReset,
                                                     dummyAssayGrouping,
                                                     dummyValueInheriting,
                                                     dummyBetweenGroups,
                                                     dummyWithinGroups,
                                                     dummyCellModelIdentifier,
                                                     dummyPacingMaxTime,
                                                     dummyInputDataOnly))
                                .andReturn(mockSimulationRequestVO);

    mocksControl.replay();

    final SimulationRequestVO validated = simulationService.userInputValidation(dummyCompoundIdentifier,
                                                                                dummyForceReRun,
                                                                                dummyReset,
                                                                                dummyAssayGrouping,
                                                                                dummyValueInheriting,
                                                                                dummyBetweenGroups,
                                                                                dummyWithinGroups,
                                                                                dummyCellModelIdentifier,
                                                                                dummyPacingMaxTime,
                                                                                dummyInputDataOnly);

    mocksControl.verify();

    assertSame(mockSimulationRequestVO, validated);
  }
}