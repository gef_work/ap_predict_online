/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws;

import java.io.StringReader;
import java.io.StringWriter;

import javax.sql.DataSource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.ws.MarshallingWebServiceInboundGateway;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.AfterTransaction;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.ws.context.DefaultMessageContext;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.pox.dom.DomPoxMessage;
import org.springframework.ws.pox.dom.DomPoxMessageFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import uk.ac.ox.cs.epsrc.business_manager.BusinessTestIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.util.DatabaseUtil;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ObjectFactory;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProcessSimulationsRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.SchemaObjectBuilder;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.SimulationDetail;

/**
 * Integration test the web service inbound gateway directly.
 *
 * @author Geoff Williams
 */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration( { "classpath:/META-INF/spring/ctx/appCtx.business.xml",
//                         "classpath:/META-INF/spring/ctx/data/business_manager/appCtx.database.business-manager.xml",
//                         "classpath:/META-INF/spring/ctx/integration/appCtx.int.xml",
//                         "classpath:/META-INF/spring/ctx/jms/appCtx.jms.ActiveMQ.xml"} )
//@TransactionConfiguration(transactionManager=BusinessTestIdentifiers.COMPONENT_TRANSACTION_MANAGER)
public class InboundGatewayDirectInvocationIT { //extends AbstractTransactionalJUnit4SpringContextTests {

  private Marshaller marshaller;

  @Autowired(required=true)
  private MarshallingWebServiceInboundGateway wsInboundGateway;

  @Autowired @Qualifier(BusinessTestIdentifiers.COMPONENT_BUSINESS_DATA_SOURCE)
  private DataSource dataSource;

  final static ObjectFactory objectFactory = new ObjectFactory();

  //@Before
  public void setUp() throws Exception {
    final JAXBContext jaxbContext = JAXBContext.newInstance(ProcessSimulationsRequest.class);
    marshaller = jaxbContext.createMarshaller();
  }

  //@AfterTransaction
  public void tearDown() {
    DatabaseUtil.resetSequence(dataSource, "sequence_pks_business_manager", "simulation_id", 1L);
  }

  @Test
  public void dummy() {}

  // Be careful as this invokes a complete processing run.
  //@Test
  public void testWSInboundGateway() throws Exception {
    final String dummyCompoundName = "DC1";

    final ProcessSimulationsRequest simulationRequest = objectFactory.createProcessSimulationsRequest();

    final SimulationDetail simulationDetail = SchemaObjectBuilder.createSimulationDetail(dummyCompoundName,
                                                                                         false, false);
    simulationRequest.getSimulationDetail().add(simulationDetail);

    final StringWriter stringWriter = new StringWriter();
    marshaller.marshal(simulationRequest, stringWriter);
    final String simulationRequestXml = stringWriter.toString();

    final DomPoxMessageFactory messageFactory = new DomPoxMessageFactory();
    final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    documentBuilderFactory.setNamespaceAware(true);
    final DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
    final Document document = documentBuilder.parse(new InputSource(new StringReader(simulationRequestXml)));
    final Transformer transformer = TransformerFactory.newInstance().newTransformer();
    final DomPoxMessage request = new DomPoxMessage(document, transformer, "text/xml");
    final MessageContext messageContext = new DefaultMessageContext(request, messageFactory);

    wsInboundGateway.invoke(messageContext);
  }
}