/*

  Original work: Copyright (c) 2015, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.newCapture;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.PreInvocationAssayValueProcessing;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress;
import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.epsrc.business_manager.entity.GroupedInvocationInput;
import uk.ac.ox.cs.epsrc.business_manager.entity.IonChannelValues;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.entity.SimulationAssay;
import uk.ac.ox.cs.epsrc.business_manager.service.SimulationService;
import uk.ac.ox.cs.epsrc.business_manager.value.object.PreInvocationProcessingVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;

/**
 * Pre-invocation assay value processing test.
 *
 * @author geoff
 */
public class PreInvocationAssayValueProcessingTest {

  private BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate;
  private Configuration mockConfiguration;
  private IMocksControl mocksControl;
  private final Long dummySimulationId = 1L;
  private PreInvocationAssayValueProcessing processing;
  private SimulationService mockSimulationService;

  private final Map<String, Short> dummyAssayGroupLevelByNameMap = new HashMap<String, Short>();
  private final Map<String, Short> dummyAssayLevelByAssayName = new HashMap<String, Short>();
  private final Map<Short, String> dummyAssayNameByAssayLevel = new HashMap<Short, String>();
  private final Map<Short, String> dummyGroupNameByGroupLevelMap = new HashMap<Short, String>();
  private final Short dummyAssayGroupLevel = 1;
  private final String dummyAssayGroupName = "dummyAssayGroupName";
  private final Short dummyAssayLevel = 2;
  private final String dummyAssayName = "dummyAssayName";

  @Before
  public void setUp() {
    processing = new PreInvocationAssayValueProcessing();
    mocksControl = createStrictControl();
    mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    mockConfiguration = mocksControl.createMock(Configuration.class);
    mockSimulationService = mocksControl.createMock(SimulationService.class);
    ReflectionTestUtils.setField(processing, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
    ReflectionTestUtils.setField(processing, BusinessIdentifiers.COMPONENT_CONFIGURATION,
                                 mockConfiguration);
    ReflectionTestUtils.setField(processing, BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE,
                                 mockSimulationService);

    dummyAssayGroupLevelByNameMap.clear();
    dummyAssayGroupLevelByNameMap.put(dummyAssayName, dummyAssayGroupLevel);
    dummyAssayLevelByAssayName.clear();
    dummyAssayLevelByAssayName.put(dummyAssayName, dummyAssayLevel);
    dummyAssayNameByAssayLevel.clear();
    dummyAssayNameByAssayLevel.put(dummyAssayLevel, dummyAssayName);
    dummyGroupNameByGroupLevelMap.clear();
    dummyGroupNameByGroupLevelMap.put(dummyAssayGroupLevel, dummyAssayGroupName);
  }

  @Test(expected=IllegalArgumentException.class)
  public void testFailsOnNullArgument() {
    ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    processing.preInvocationProcessing(dummyProcessingType, dummySimulationId,
                                       null);
  }

  @Test
  public void testGroupingAndInheritingIsOn() throws CloneNotSupportedException {
    final ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    final Simulation mockSimulation = mocksControl.createMock(Simulation.class);
    final PreInvocationProcessingVO mockPreInvocationProcessingVO = mocksControl.createMock(PreInvocationProcessingVO.class);

    expect(mockSimulationService.findBySimulationId(dummySimulationId)).andReturn(mockSimulation);
    expect(mockSimulation.isAssayGrouping()).andReturn(true);
    expect(mockSimulation.isValueInheriting()).andReturn(true);
    expect(mockSimulation.isBetweenGroups()).andReturn(true);
    expect(mockSimulation.isWithinGroups()).andReturn(true);

    final List<SimulationAssay> dummySimulationAssays = new ArrayList<SimulationAssay>();
    final SimulationAssay mockSimulationAssay = mocksControl.createMock(SimulationAssay.class);
    dummySimulationAssays.add(mockSimulationAssay);

    expect(mockPreInvocationProcessingVO.getSimulationAssays()).andReturn(dummySimulationAssays);

    // In "grouping" routine.
    expect(mockSimulationAssay.getAssayName()).andReturn(dummyAssayName);

    // In "retrieveAssayGroup" routine.
    expect(mockConfiguration.retrieveAssayGroupLevelByAssayName()).andReturn(dummyAssayGroupLevelByNameMap);

    // In "retrieveAssayLevel" routine.
    expect(mockConfiguration.retrieveAssayLevelByAssayName()).andReturn(dummyAssayLevelByAssayName);

    final SimulationAssay mockClonedSimulationAssay = mocksControl.createMock(SimulationAssay.class);
    expect(mockSimulationAssay.clone()).andReturn(mockClonedSimulationAssay);

    // Using LinkedHashSet here to guarantee insertion order for later test!
    final Set<IonChannelValues> dummyIonChannelValues = new LinkedHashSet<IonChannelValues>();
    final IonChannelValues mockIonChannelValues1 = mocksControl.createMock(IonChannelValues.class);
    final IonChannelValues mockIonChannelValues2 = mocksControl.createMock(IonChannelValues.class);
    dummyIonChannelValues.add(mockIonChannelValues1);
    dummyIonChannelValues.add(mockIonChannelValues2);
    expect(mockSimulationAssay.getIonChannelValues()).andReturn(dummyIonChannelValues);
    // provisionally return the same collection!
    expect(mockClonedSimulationAssay.getIonChannelValues()).andReturn(dummyIonChannelValues);

    expect(mockConfiguration.retrieveAssayGroupNameByGroupLevelMap())
          .andReturn(dummyGroupNameByGroupLevelMap);

    expect(mockClonedSimulationAssay.getIonChannelValues()).andReturn(dummyIonChannelValues);

    // Using isA because GroupedInvocationInput is created and referenced via "this".
    mockIonChannelValues1.setParent(isA(GroupedInvocationInput.class));
    mockIonChannelValues2.setParent(isA(GroupedInvocationInput.class));

    final Capture<Progress> dummyOverallProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress));

    expect(mockPreInvocationProcessingVO.getPerFrequencyConcentrations())
          .andReturn(new HashMap<BigDecimal, Set<BigDecimal>>());

    mocksControl.replay();

    processing.preInvocationProcessing(dummyProcessingType, dummySimulationId,
                                       mockPreInvocationProcessingVO);

    mocksControl.verify();
  }

  @Test
  public void testGroupingOffInheritingOn() throws CloneNotSupportedException {
    final ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    final Simulation mockSimulation = mocksControl.createMock(Simulation.class);
    final PreInvocationProcessingVO mockPreInvocationProcessingVO = mocksControl.createMock(PreInvocationProcessingVO.class);

    expect(mockSimulationService.findBySimulationId(dummySimulationId)).andReturn(mockSimulation);
    expect(mockSimulation.isAssayGrouping()).andReturn(false);
    expect(mockSimulation.isValueInheriting()).andReturn(true);
    expect(mockSimulation.isBetweenGroups()).andReturn(true);
    expect(mockSimulation.isWithinGroups()).andReturn(true);

    final List<SimulationAssay> dummySimulationAssays = new ArrayList<SimulationAssay>();
    final SimulationAssay mockSimulationAssay = mocksControl.createMock(SimulationAssay.class);
    dummySimulationAssays.add(mockSimulationAssay);

    expect(mockPreInvocationProcessingVO.getSimulationAssays()).andReturn(dummySimulationAssays);

    // In "grouping" routine.
    expect(mockSimulationAssay.getAssayName()).andReturn(dummyAssayName);

    // No call to retrieve assay group by assay name.

    // In "retrieveAssayLevel" routine.
    expect(mockConfiguration.retrieveAssayLevelByAssayName()).andReturn(dummyAssayLevelByAssayName);

    final SimulationAssay mockClonedSimulationAssay = mocksControl.createMock(SimulationAssay.class);
    expect(mockSimulationAssay.clone()).andReturn(mockClonedSimulationAssay);

    // Using LinkedHashSet here to guarantee insertion order for later test!
    final Set<IonChannelValues> dummyIonChannelValues = new LinkedHashSet<IonChannelValues>();
    final IonChannelValues mockIonChannelValues1 = mocksControl.createMock(IonChannelValues.class);
    final IonChannelValues mockIonChannelValues2 = mocksControl.createMock(IonChannelValues.class);
    dummyIonChannelValues.add(mockIonChannelValues1);
    dummyIonChannelValues.add(mockIonChannelValues2);
    expect(mockSimulationAssay.getIonChannelValues()).andReturn(dummyIonChannelValues);
    // provisionally return the same collection!
    expect(mockClonedSimulationAssay.getIonChannelValues()).andReturn(dummyIonChannelValues);

    // No call to retrieve assay group name by group level.

    expect(mockConfiguration.retrieveAssayNameByAssayLevel())
          .andReturn(dummyAssayNameByAssayLevel);

    expect(mockClonedSimulationAssay.getIonChannelValues()).andReturn(dummyIonChannelValues);

    // Using isA because GroupedInvocationInput is created and referenced via "this".
    mockIonChannelValues1.setParent(isA(GroupedInvocationInput.class));
    mockIonChannelValues2.setParent(isA(GroupedInvocationInput.class));

    final Capture<Progress> dummyOverallProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress));

    expect(mockPreInvocationProcessingVO.getPerFrequencyConcentrations())
          .andReturn(new HashMap<BigDecimal, Set<BigDecimal>>());

    mocksControl.replay();

    processing.preInvocationProcessing(dummyProcessingType, dummySimulationId,
                                       mockPreInvocationProcessingVO);

    mocksControl.verify();
  }
}