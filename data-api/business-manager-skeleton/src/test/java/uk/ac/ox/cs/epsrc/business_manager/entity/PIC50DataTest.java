/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 * 
 */
package uk.ac.ox.cs.epsrc.business_manager.entity;

import static org.easymock.EasyMock.createControl;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit test pIC50Data object.
 *
 * @author geoff
 */
public class PIC50DataTest {

  private IMocksControl mocksControl;
  private PIC50Data pIC50Data;

  @Before
  public void setUp() {
    mocksControl = createControl();
  }

  @Test
  public void testDefaultConstructor() {
    pIC50Data = new PIC50Data();
    assertNull(pIC50Data.getHill());
    assertNull(pIC50Data.getValue());
    assertFalse(pIC50Data.getOriginal());

    assertNotNull(pIC50Data.toString());
  }

  @Test
  public void testInitialisingConstructor() throws CloneNotSupportedException {
    BigDecimal dummyValue = null;
    BigDecimal dummyHill = null;
    boolean dummyOriginal = false;
    Integer dummyStrategyOrder = null;

    try {
      new PIC50Data(dummyValue, dummyHill, dummyOriginal, dummyStrategyOrder);
      fail("Should not permit a null pIC50 value to be assigned.");
    } catch (IllegalArgumentException e) {}

    dummyValue = BigDecimal.ONE;
    pIC50Data = new PIC50Data(dummyValue, dummyHill, dummyOriginal, dummyStrategyOrder);
    assertNull(pIC50Data.getHill());
    assertNotNull(pIC50Data.getValue());
    assertFalse(pIC50Data.getOriginal());
    assertNull(pIC50Data.getStrategyOrder());

    dummyHill = BigDecimal.TEN;
    dummyOriginal = true;
    dummyStrategyOrder = 5;

    pIC50Data = new PIC50Data(dummyValue, dummyHill, dummyOriginal, dummyStrategyOrder);
    assertSame(dummyHill, pIC50Data.getHill());
    assertSame(dummyValue, pIC50Data.getValue());
    assertTrue(pIC50Data.getOriginal());
    assertSame(dummyStrategyOrder, pIC50Data.getStrategyOrder());

    assertNotNull(pIC50Data.toString());

    final IonChannelValues mockIonChannelValues = mocksControl.createMock(IonChannelValues.class);
    pIC50Data.setIonChannelValues(mockIonChannelValues);

    final PIC50Data cloned = pIC50Data.clone();
    assertSame(dummyHill, cloned.getHill());
    assertSame(dummyValue, cloned.getValue());
    assertTrue(cloned.getOriginal());
  }

  @Test
  public void testEquality() {
    final BigDecimal dummyValue1 = BigDecimal.ONE;
    final BigDecimal dummyHill1 = BigDecimal.TEN;
    final boolean dummyOriginal1 = false;
    final Integer dummyStrategyOrder1 = 3;

    final PIC50Data pIC50Data1 = new PIC50Data(dummyValue1, dummyHill1, dummyOriginal1,
                                               dummyStrategyOrder1);

    BigDecimal dummyValue2 = BigDecimal.ONE;
    BigDecimal dummyHill2 = BigDecimal.TEN;
    boolean dummyOriginal2 = false;
    Integer dummyStrategyOrder2 = 3;

    PIC50Data pIC50Data2 = new PIC50Data(dummyValue2, dummyHill2, dummyOriginal2,
                                         dummyStrategyOrder2);

    assertTrue(pIC50Data1.equals(pIC50Data2));

    dummyValue2 = new BigDecimal("1.0000000000123");

    pIC50Data2 = new PIC50Data(dummyValue2, dummyHill2, dummyOriginal2, dummyStrategyOrder2);

    assertTrue(pIC50Data1.equals(pIC50Data2));

    dummyValue2 = new BigDecimal("1.000000000123");

    pIC50Data2 = new PIC50Data(dummyValue2, dummyHill2, dummyOriginal2, dummyStrategyOrder2);

    assertFalse(pIC50Data1.equals(pIC50Data2));

    dummyValue2 = BigDecimal.ONE;
    dummyHill2 = new BigDecimal("10.000000000010329");

    pIC50Data2 = new PIC50Data(dummyValue2, dummyHill2, dummyOriginal2, dummyStrategyOrder2);

    assertTrue(pIC50Data1.equals(pIC50Data2));

    dummyHill2 = new BigDecimal("10.00000000010329");

    pIC50Data2 = new PIC50Data(dummyValue2, dummyHill2, dummyOriginal2, dummyStrategyOrder2);

    assertFalse(pIC50Data1.equals(pIC50Data2));
  }
}