/*

  Copyright (c) 2005-2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.dao.jpa;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.dao.ResultsDAO;
import uk.ac.ox.cs.epsrc.business_manager.entity.GroupedInvocationInput;
import uk.ac.ox.cs.epsrc.business_manager.entity.IonChannelValues;
import uk.ac.ox.cs.epsrc.business_manager.entity.Job;
import uk.ac.ox.cs.epsrc.business_manager.entity.JobResult;
import uk.ac.ox.cs.epsrc.business_manager.entity.PIC50Data;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;

/**
 * Unit test the Results DAO implementation.
 *
 * @author geoff
 */
public class ResultsDAOImplTest {

  private EntityManager mockEntityManager;
  private IMocksControl mocksControl;
  private Query mockQuery;
  private ResultsDAO resultsDAO;

  @Before
  public void setUp() {
    resultsDAO = new ResultsDAOImpl();

    mocksControl = createStrictControl();
    mockEntityManager = mocksControl.createMock(EntityManager.class);
    mockQuery = mocksControl.createMock(Query.class);

    ReflectionTestUtils.setField(resultsDAO, "entityManager", mockEntityManager);
  }

  @Test
  public void testRetrieveInputValues() {
    final long dummySimulationId = 1l;
    final List<GroupedInvocationInput> retrievedInputValues = new ArrayList<GroupedInvocationInput>();
    final GroupedInvocationInput mockGroupedInvocationInput = mocksControl.createMock(GroupedInvocationInput.class);
    retrievedInputValues.add(mockGroupedInvocationInput);
    final Set<IonChannelValues> dummyIonChannelValuesCollection = new HashSet<IonChannelValues>();
    final IonChannelValues mockIonChannelValues = mocksControl.createMock(IonChannelValues.class);
    dummyIonChannelValuesCollection.add(mockIonChannelValues);
    final List<PIC50Data> dummyPIC50DataCollection = new ArrayList<PIC50Data>();
    dummyPIC50DataCollection.add(mocksControl.createMock(PIC50Data.class));

    expect(mockEntityManager.createNamedQuery(GroupedInvocationInput.QUERY_BY_SIMULATION_ID))
          .andReturn(mockQuery);
    expect(mockQuery.setParameter(Simulation.PROPERTY_SIMULATION_ID, dummySimulationId))
          .andReturn(mockQuery);
    expect(mockQuery.getResultList()).andReturn(retrievedInputValues);
    expect(mockGroupedInvocationInput.getIonChannelValues())
          .andReturn(dummyIonChannelValuesCollection);
    expect(mockIonChannelValues.getpIC50Data()).andReturn(dummyPIC50DataCollection);

    mocksControl.replay();

    final List<GroupedInvocationInput> returnedInputValues = resultsDAO.retrieveInputValues(dummySimulationId);

    mocksControl.verify();

    assertEquals(retrievedInputValues.size(), returnedInputValues.size());
    assertSame(retrievedInputValues.get(0), returnedInputValues.get(0));
  }

  @Test
  public void testRetrieveInputValuesReturnsEmptyCollectionOnNoResultException() {
    final long dummySimulationId = 1l;

    expect(mockEntityManager.createNamedQuery(GroupedInvocationInput.QUERY_BY_SIMULATION_ID))
          .andReturn(mockQuery);
    expect(mockQuery.setParameter(Simulation.PROPERTY_SIMULATION_ID, dummySimulationId))
          .andReturn(mockQuery);
    expect(mockQuery.getResultList()).andThrow(new NoResultException());

    mocksControl.replay();

    final List<GroupedInvocationInput> returnedInputValues = resultsDAO.retrieveInputValues(dummySimulationId);

    mocksControl.verify();

    assertTrue(returnedInputValues.isEmpty());

    mocksControl.reset();
  }

  @Test
  public void testRetrieveResults() {
    final long dummySimulationId = 1l;
    final List<Job> retrievedResults = new ArrayList<Job>();
    final Job mockJob1 = mocksControl.createMock(Job.class);
    retrievedResults.add(mockJob1);
    // Emulate the returned first Job having no JobResult objects.
    final Set<JobResult> mockJob1JobResults = new HashSet<JobResult>();

    expect(mockEntityManager.createNamedQuery(Job.QUERY_JOB_DATA_BY_SIMULATION_ID))
          .andReturn(mockQuery);
    expect(mockQuery.setParameter(Simulation.PROPERTY_SIMULATION_ID, dummySimulationId))
          .andReturn(mockQuery);
    expect(mockQuery.getResultList()).andReturn(retrievedResults);
    expect(mockJob1.getJobResults()).andReturn(mockJob1JobResults);

    mocksControl.replay();

    List<Job> returnedResults = resultsDAO.retrieveResults(dummySimulationId);

    mocksControl.verify();

    assertEquals(0, returnedResults.size());

    mocksControl.reset();

    final Job mockJob2 = mocksControl.createMock(Job.class);
    final Job mockJob3 = mocksControl.createMock(Job.class);
    retrievedResults.add(mockJob2);
    retrievedResults.add(mockJob3);
    // Emulate the returned second Job having some JobResult objects, but third not.
    final Set<JobResult> mockJob2JobResults = new HashSet<JobResult>();
    mockJob2JobResults.add(mocksControl.createMock(JobResult.class));
    mockJob2JobResults.add(mocksControl.createMock(JobResult.class));
    final Set<JobResult> mockJob3JobResults = new HashSet<JobResult>();

    expect(mockEntityManager.createNamedQuery(Job.QUERY_JOB_DATA_BY_SIMULATION_ID))
          .andReturn(mockQuery);
    expect(mockQuery.setParameter(Simulation.PROPERTY_SIMULATION_ID, dummySimulationId))
          .andReturn(mockQuery);
    expect(mockQuery.getResultList()).andReturn(retrievedResults);
    expect(mockJob1.getJobResults()).andReturn(mockJob1JobResults);
    expect(mockJob2.getJobResults()).andReturn(mockJob2JobResults);
    expect(mockJob3.getJobResults()).andReturn(mockJob3JobResults);

    mocksControl.replay();

    returnedResults = resultsDAO.retrieveResults(dummySimulationId);

    mocksControl.verify();

    assertEquals(1, returnedResults.size());
  }

  @Test
  public void testRetrieveResultsReturnsEmptyColletionOnNoResultException() {
    final long dummySimulationId = 1l;

    expect(mockEntityManager.createNamedQuery(Job.QUERY_JOB_DATA_BY_SIMULATION_ID))
          .andReturn(mockQuery);
    expect(mockQuery.setParameter(Simulation.PROPERTY_SIMULATION_ID, dummySimulationId))
          .andReturn(mockQuery);
    expect(mockQuery.getResultList()).andThrow(new NoResultException());

    mocksControl.replay();

    final List<Job> returnedResults = resultsDAO.retrieveResults(dummySimulationId);

    mocksControl.verify();

    assertTrue(returnedResults.isEmpty());
  }
}