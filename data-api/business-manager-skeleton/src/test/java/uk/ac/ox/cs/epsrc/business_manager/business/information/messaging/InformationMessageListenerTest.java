/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.information.messaging;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.fail;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.InformationMessageListener;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.Provenance;
import uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager;

/**
 * Unit test the information message listener.
 *
 * @author geoff
 */
public class InformationMessageListenerTest {

  private InformationManager mockInformationManager;
  private InformationMessageListener listener;
  private IMocksControl mocksControl;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockInformationManager = mocksControl.createMock(InformationManager.class);
    listener = new InformationMessageListener();
    ReflectionTestUtils.setField(listener, BusinessIdentifiers.COMPONENT_INFORMATION_MANAGER,
                                 mockInformationManager);
  }

  @Test
  public void testOnMessage() throws JMSException {
    Message mockMessage = null;
    listener.onMessage(mockMessage);

    mockMessage = (Message) mocksControl.createMock(MapMessage.class);
    try {
      listener.onMessage(mockMessage);
      fail("Should only be working with ObjectMessage objects.");
    } catch (UnsupportedOperationException e) {}

    mocksControl.reset();

    final ObjectMessage mockObjectMessage = (ObjectMessage) mocksControl.createMock(ObjectMessage.class);
    final String dummyInformation = "";
    expect(mockObjectMessage.getObject()).andReturn(dummyInformation);
    mocksControl.replay();
    try {
      listener.onMessage(mockObjectMessage);
      fail("ObjectMessage should only be of type Provenance/Progress/Purge.");
    } catch (UnsupportedOperationException e) {}

    mocksControl.verify();
    mocksControl.reset();

    final String dummyReason = "dummyReason";
    expect(mockObjectMessage.getObject()).andThrow(new JMSException(dummyReason));
    mocksControl.replay();
    try {
      listener.onMessage(mockObjectMessage);
      fail("Catch JMSException being thrown somewhere.");
    } catch (UnsupportedOperationException e) {}

    mocksControl.verify();
    mocksControl.reset();

    final Provenance mockProvenance = mocksControl.createMock(Provenance.class);
    expect(mockObjectMessage.getObject()).andReturn(mockProvenance);
    mockInformationManager.recordProvenance(mockProvenance);
    mocksControl.replay();
    listener.onMessage(mockObjectMessage);

    mocksControl.verify();
    mocksControl.reset();

    final Progress mockProgress = mocksControl.createMock(Progress.class);
    expect(mockObjectMessage.getObject()).andReturn(mockProgress);
    mockInformationManager.recordProgress(mockProgress);
    mocksControl.replay();
    listener.onMessage(mockObjectMessage);

    mocksControl.verify();
  }
}