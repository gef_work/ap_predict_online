/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.checker;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertSame;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.Ordered;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.checker.AdministratorDefinedChange;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Unit test administrator defined changes checker.
 *
 * @author geoff
 */
public class AdministratorDefinedChangeTest {

  private AdministratorDefinedChange administratorDefinedChange;
  private BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate;
  private IMocksControl mocksControl;
  private final Long dummySimulationId = 1L; 
  private Simulation mockSimulation;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    mockSimulation = mocksControl.createMock(Simulation.class);
  }

  @Test
  public void testConstructor() {
    boolean dummyForceRequires = true;
    String dummyReason = "dummyReason";
    administratorDefinedChange = new AdministratorDefinedChange(dummyForceRequires, dummyReason);
    ReflectionTestUtils.setField(administratorDefinedChange, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
    // These two are unaffected by constructor.
    assertEquals(Ordered.HIGHEST_PRECEDENCE, administratorDefinedChange.getOrder());
    assertEquals(MessageKey.SIMULATION_CHANGE_ADMINISTRATOR.getBundleIdentifier(),
                 administratorDefinedChange.getNature());

    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    mocksControl.replay();
    boolean requiresReRun = administratorDefinedChange.requiresReRun(mockSimulation);
    mocksControl.verify();

    assertSame(dummyForceRequires, requiresReRun);

    mocksControl.reset();

    dummyForceRequires = false;
    administratorDefinedChange = new AdministratorDefinedChange(dummyForceRequires, dummyReason);
    ReflectionTestUtils.setField(administratorDefinedChange, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    Capture<Progress> dummyOverallProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress));
    mocksControl.replay();
    requiresReRun = administratorDefinedChange.requiresReRun(mockSimulation);
    mocksControl.verify();

    assertSame(dummyForceRequires, requiresReRun);
    final OverallProgress capturedOverallProgress = (OverallProgress) dummyOverallProgress.getValue();
    assertEquals(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertSame(InformationLevel.DEBUG, capturedOverallProgress.getLevel());
    assertNotEquals(dummyReason, capturedOverallProgress.getText());
    assertEquals("No administrator defined change instruction", capturedOverallProgress.getText());
  }
}