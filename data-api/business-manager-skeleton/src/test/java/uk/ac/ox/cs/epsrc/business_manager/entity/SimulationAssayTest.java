/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 * 
 */
package uk.ac.ox.cs.epsrc.business_manager.entity;

import static org.easymock.EasyMock.createControl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.Set;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit test the simulation assay object.
 *
 * @author geoff
 */
public class SimulationAssayTest {

  private IMocksControl mocksControl;
  private SimulationAssay simulationAssay;

  @Before
  public void setUp() {
    mocksControl = createControl();
  }

  @Test
  public void testDefaultConstructor() throws CloneNotSupportedException {
    simulationAssay = new SimulationAssay();

    assertEquals(0, simulationAssay.getIonChannelValues().size());
    assertNotNull(simulationAssay.clone());
    assertNull(simulationAssay.getAssayName());
  }

  @Test
  public void testInitialisingConstructor() throws CloneNotSupportedException {
    Long dummySimulationId = null;
    String dummyAssayName = null;
    Set<IonChannelValues> dummyIonChannelValues = null;

    simulationAssay = new SimulationAssay(dummySimulationId, dummyAssayName, dummyIonChannelValues);

    assertEquals(0, simulationAssay.getIonChannelValues().size());
    assertNotNull(simulationAssay.clone());
    assertNull(simulationAssay.getAssayName());
    assertNull(simulationAssay.getSimulationId());

    dummySimulationId = 1L;
    dummyAssayName = "dummyAssayName";
    dummyIonChannelValues = new HashSet<IonChannelValues>();
    final IonChannelValues mockIonChannelValues1 = mocksControl.createMock(IonChannelValues.class);
    dummyIonChannelValues.add(mockIonChannelValues1);

    simulationAssay = new SimulationAssay(dummySimulationId, dummyAssayName, dummyIonChannelValues);
    assertTrue(simulationAssay.getIonChannelValues().contains(mockIonChannelValues1));
    // assertNotNull(simulationAssay.clone()); // clone() call on mock object returns null!
    assertEquals(dummyAssayName, simulationAssay.getAssayName());
    assertEquals(dummySimulationId, simulationAssay.getSimulationId());

    try {
      simulationAssay.appendIonChannelValues(null);
      fail("Should not permit the appending of a null IonChannelValues object!");
    } catch (IllegalArgumentException e) {}

    final IonChannelValues mockIonChannelValues2 = mocksControl.createMock(IonChannelValues.class);
    simulationAssay.appendIonChannelValues(mockIonChannelValues2);
    assertEquals(2, simulationAssay.getIonChannelValues().size());
  }
}
