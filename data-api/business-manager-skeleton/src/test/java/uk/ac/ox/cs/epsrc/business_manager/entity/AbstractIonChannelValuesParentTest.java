/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 * 
 */
package uk.ac.ox.cs.epsrc.business_manager.entity;

import static org.easymock.EasyMock.createStrictControl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Set;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit test AbstractIonChannelValuesParent object.
 *
 * @author geoff
 */
public class AbstractIonChannelValuesParentTest {

  private TestAbstractIonChannelValuesParent ionChannelValuesParent;
  private IMocksControl mocksControl;

  private class TestAbstractIonChannelValuesParent extends AbstractIonChannelValuesParent {
    private static final long serialVersionUID = 3581436496818463383L;
    protected TestAbstractIonChannelValuesParent() {
      super();
    }
    protected TestAbstractIonChannelValuesParent(final String parentName, final Long simulationId) {
      super(parentName, simulationId);
    }
  }

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
  }

  @Test
  public void testDefaultConstructor() {
    ionChannelValuesParent = new TestAbstractIonChannelValuesParent();
    assertTrue(ionChannelValuesParent.getIonChannelValues().isEmpty());
    assertNull(ionChannelValuesParent.getParentName());
    assertNull(ionChannelValuesParent.getSimulationId());

    assertTrue(ionChannelValuesParent.hashCode() > 0);
    assertNotNull(ionChannelValuesParent.toString());

    int hashCode = ionChannelValuesParent.hashCode();
    assertTrue(hashCode > 0);
  }

  @Test
  public void testInitialisingConstructor() {
    String dummyParentName = null;
    Long dummySimulationId = null;

    try {
      ionChannelValuesParent = new TestAbstractIonChannelValuesParent(dummyParentName, dummySimulationId);
      fail("Should not permit the assignment of an invalid parent name!");
    } catch (AssertionError e) {}

    int hashCode = 0;
    dummyParentName = "dummyParentName";
    ionChannelValuesParent = new TestAbstractIonChannelValuesParent(dummyParentName, dummySimulationId);
    assertTrue(ionChannelValuesParent.getIonChannelValues().isEmpty());
    assertEquals(dummyParentName, ionChannelValuesParent.getParentName());
    assertNull(ionChannelValuesParent.getSimulationId());

    ionChannelValuesParent.toString();
    assertFalse(hashCode == ionChannelValuesParent.hashCode());
    hashCode = ionChannelValuesParent.hashCode();

    dummySimulationId = 1L;
    ionChannelValuesParent = new TestAbstractIonChannelValuesParent(dummyParentName, dummySimulationId);
    assertTrue(ionChannelValuesParent.getIonChannelValues().isEmpty());
    assertEquals(dummyParentName, ionChannelValuesParent.getParentName());
    assertEquals(dummySimulationId, ionChannelValuesParent.getSimulationId());

    assertFalse(hashCode == ionChannelValuesParent.hashCode());
    hashCode = ionChannelValuesParent.hashCode();
    ionChannelValuesParent.toString();
  }

  @Test
  public void testAddingIonChannelValues() {
    ionChannelValuesParent = new TestAbstractIonChannelValuesParent();

    IonChannelValues dummyIonChannelValues = null;
    try {
      ionChannelValuesParent.addIonChannelValues(dummyIonChannelValues);
      fail();
    } catch (AssertionError e) {}

    final IonChannelValues mockIonChannelValues = mocksControl.createMock(IonChannelValues.class);

    mockIonChannelValues.setParent(ionChannelValuesParent);

    mocksControl.replay();

    ionChannelValuesParent.addIonChannelValues(mockIonChannelValues);

    mocksControl.verify();

    final Set<IonChannelValues> ionChannelValues = ionChannelValuesParent.getIonChannelValues();
    assertTrue(ionChannelValues.contains(mockIonChannelValues));

    try {
      ionChannelValues.remove(mockIonChannelValues);
      fail("Should not permit the removal of an ion channel values object");
    } catch (UnsupportedOperationException e) {}
  }
}