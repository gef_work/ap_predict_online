/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.manager;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.dao.ResultsDAO;
import uk.ac.ox.cs.epsrc.business_manager.entity.GroupedInvocationInput;
import uk.ac.ox.cs.epsrc.business_manager.entity.IonChannelValues;
import uk.ac.ox.cs.epsrc.business_manager.entity.Job;
import uk.ac.ox.cs.epsrc.business_manager.entity.JobResult;
import uk.ac.ox.cs.epsrc.business_manager.entity.PIC50Data;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.ConcentrationResultVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.GroupedValuesVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.IonChannelValuesVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.JobResultsVO;

/**
 * Unit test the ResultsManagerImpl object.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { ResultsManagerImpl.class, ConcentrationResultVO.class,
                   GroupedValuesVO.class, JobResultsVO.class } )
public class ResultsManagerImplTest {

  private IMocksControl mocksControl;
  private long dummySimulationId = 3l;
  private ResultsDAO mockResultsDAO;
  private ResultsManager resultsManager;

  @Before
  public void setUp() {
    resultsManager = new ResultsManagerImpl();

    mocksControl = createStrictControl();
    mockResultsDAO = mocksControl.createMock(ResultsDAO.class);

    ReflectionTestUtils.setField(resultsManager,
                                 BusinessIdentifiers.COMPONENT_RESULTS_DAO,
                                 mockResultsDAO);
  }

  @Test
  public void testRetrieveInputValues() throws Exception {
    /*
     * 1. No grouped invocation input retrieved from results DAO
     */
    // >> retrieveInputValuesEntites()
    final List<GroupedInvocationInput> dummyGroupedInvocationInputs = new ArrayList<GroupedInvocationInput>();
    expect(mockResultsDAO.retrieveInputValues(dummySimulationId))
          .andReturn(dummyGroupedInvocationInputs);
    // << retrieveInputValuesEntites()

    replayAll();
    mocksControl.replay();

    Set<GroupedValuesVO> retrievedInputValues = resultsManager.retrieveInputValues(dummySimulationId);

    verifyAll();
    mocksControl.verify();

    assertEquals(0, retrievedInputValues.size());

    resetAll();
    mocksControl.reset();

    /*
     * 2. A grouped invocation input is retrieved, but no ion channel values.
     */
    // >> retrieveInputValuesEntites()
    final GroupedInvocationInput mockGroupedInvocationInput = mocksControl.createMock(GroupedInvocationInput.class);
    dummyGroupedInvocationInputs.add(mockGroupedInvocationInput);
    expect(mockResultsDAO.retrieveInputValues(dummySimulationId))
          .andReturn(dummyGroupedInvocationInputs);
    // << retrieveInputValuesEntites()

    final short dummyAssayGroupLevel = 1;
    final String dummyAssayGroupName = "dummyAssayGroupName";
    final Set<IonChannelValues> dummyIonChannelValues = new HashSet<IonChannelValues>();

    expect(mockGroupedInvocationInput.getAssayGroupLevel())
          .andReturn(dummyAssayGroupLevel);
    expect(mockGroupedInvocationInput.getAssayGroupName())
          .andReturn(dummyAssayGroupName);
    expect(mockGroupedInvocationInput.getIonChannelValues())
          .andReturn(dummyIonChannelValues);
    final Capture<List<IonChannelValuesVO>> captureIonChannelVOs = newCapture();
    final GroupedValuesVO mockGroupedValues = mocksControl.createMock(GroupedValuesVO.class);
    expectNew(GroupedValuesVO.class, eq(dummyAssayGroupName),
                                     eq(dummyAssayGroupLevel),
                                     capture(captureIonChannelVOs))
             .andReturn(mockGroupedValues);

    replayAll();
    mocksControl.replay();

    retrievedInputValues = resultsManager.retrieveInputValues(dummySimulationId);

    verifyAll();
    mocksControl.verify();

    assertEquals(1, retrievedInputValues.size());
    assertTrue(captureIonChannelVOs.getValue().isEmpty());

    resetAll();
    mocksControl.reset();

    /*
     * 3. A grouped invocation input is retrieved, with ion channel values.
     */
    // >> retrieveInputValuesEntites()
    expect(mockResultsDAO.retrieveInputValues(dummySimulationId))
          .andReturn(dummyGroupedInvocationInputs);
    // << retrieveInputValuesEntites()

    expect(mockGroupedInvocationInput.getAssayGroupLevel())
          .andReturn(dummyAssayGroupLevel);
    expect(mockGroupedInvocationInput.getAssayGroupName())
          .andReturn(dummyAssayGroupName);
    final IonChannelValues mockIonChannelValues = mocksControl.createMock(IonChannelValues.class);
    dummyIonChannelValues.add(mockIonChannelValues);
    expect(mockGroupedInvocationInput.getIonChannelValues())
          .andReturn(dummyIonChannelValues);

    final String dummyIonChannelName = "dummyIonChannelName";
    final String dummySourceAssayName = "dummySourceAssayName";
    final List<PIC50Data> dummyPIC50Data = new ArrayList<PIC50Data>();
    final PIC50Data mockPIC50Data1 = mocksControl.createMock(PIC50Data.class);
    final PIC50Data mockPIC50Data2 = mocksControl.createMock(PIC50Data.class);
    dummyPIC50Data.add(mockPIC50Data1);
    dummyPIC50Data.add(mockPIC50Data2);

    expect(mockIonChannelValues.getIonChannelName())
          .andReturn(dummyIonChannelName);
    expect(mockIonChannelValues.getSourceAssayName())
          .andReturn(dummySourceAssayName);
    expect(mockIonChannelValues.getpIC50Data()).andReturn(dummyPIC50Data);

    final BigDecimal dummyPIC50Value1 = BigDecimal.TEN;
    final BigDecimal dummyPIC50Hill1 = BigDecimal.ONE;
    final boolean dummyPIC50Originals1 = false;
    final Integer dummyStrategyOrder1 = 2;

    expect(mockPIC50Data1.getValue()).andReturn(dummyPIC50Value1);
    expect(mockPIC50Data1.getHill()).andReturn(dummyPIC50Hill1);
    expect(mockPIC50Data1.getOriginal()).andReturn(dummyPIC50Originals1);
    expect(mockPIC50Data1.getStrategyOrder()).andReturn(dummyStrategyOrder1);

    final BigDecimal dummyPIC50Value2 = BigDecimal.ONE;
    final BigDecimal dummyPIC50Hill2 = BigDecimal.ZERO;
    final boolean dummyPIC50Originals2 = true;
    final Integer dummyStrategyOrder2 = 4;

    expect(mockPIC50Data2.getValue()).andReturn(dummyPIC50Value2);
    expect(mockPIC50Data2.getHill()).andReturn(dummyPIC50Hill2);
    expect(mockPIC50Data2.getOriginal()).andReturn(dummyPIC50Originals2);
    expect(mockPIC50Data2.getStrategyOrder()).andReturn(dummyStrategyOrder2);

    final Capture<String> capturePIC50s = newCapture();
    final Capture<String> captureHills = newCapture();
    final Capture<String> captureOriginals = newCapture();
    final Capture<String> captureStrategyOrders = newCapture();

    final IonChannelValuesVO mockIonChannelValuesVO = mocksControl.createMock(IonChannelValuesVO.class);
    expectNew(IonChannelValuesVO.class, eq(dummyIonChannelName),
                                        eq(dummySourceAssayName),
                                        capture(capturePIC50s),
                                        capture(captureHills),
                                        capture(captureOriginals),
                                        capture(captureStrategyOrders))
             .andReturn(mockIonChannelValuesVO);

    expectNew(GroupedValuesVO.class, eq(dummyAssayGroupName),
                                     eq(dummyAssayGroupLevel),
                                     capture(captureIonChannelVOs))
             .andReturn(mockGroupedValues);

    replayAll();
    mocksControl.replay();

    retrievedInputValues = resultsManager.retrieveInputValues(dummySimulationId);

    verifyAll();
    mocksControl.verify();

    assertSame(dummyGroupedInvocationInputs.size(), retrievedInputValues.size());
    assertSame(dummyIonChannelValues.size(),
               captureIonChannelVOs.getValue().size());
    assertEquals(dummyPIC50Value1.toString().concat(",")
                                            .concat(dummyPIC50Value2.toString()),
                 capturePIC50s.getValue());
    assertEquals(dummyPIC50Hill1.toString().concat(",")
                                           .concat(dummyPIC50Hill2.toString()),
                 captureHills.getValue());
    assertEquals(String.valueOf(dummyPIC50Originals1).concat(",")
                                                     .concat(String.valueOf(dummyPIC50Originals2)),
                 captureOriginals.getValue());
    assertEquals(dummyStrategyOrder1.toString().concat(",")
                                               .concat(dummyStrategyOrder2.toString()),
                 captureStrategyOrders.getValue());
  }

  @Test
  public void testRetrieveResults() throws Exception {
    /*
     * 1. No jobs retrieved from results DAO
     */
    final List<Job> dummyJobs = new ArrayList<Job>();
    expect(mockResultsDAO.retrieveResults(dummySimulationId))
          .andReturn(dummyJobs);

    mocksControl.replay();

    List<JobResultsVO> retrievedJobResults = resultsManager.retrieveResults(dummySimulationId);

    mocksControl.verify();
    assertEquals(0, retrievedJobResults.size());

    mocksControl.reset();

    /*
     * 2. Jobs retrieved with job results
     */

    mocksControl.resetToNice();

    final Job mockJob = mocksControl.createMock(Job.class);
    dummyJobs.add(mockJob);
    expect(mockResultsDAO.retrieveResults(dummySimulationId))
          .andReturn(dummyJobs);

    final long dummyJobId = 1l;
    final Float dummyPacingFrequency = null;
    final GroupedInvocationInput mockGroupedInvocationInput = mocksControl.createMock(GroupedInvocationInput.class);
    final String dummyAssayGroupName = null;
    final short dummyAssayGroupLevel = 1;
    final String dummyMessages = null;
    final String dummyDeltaAPD90PctileNames = null;

    expect(mockJob.getId()).andReturn(dummyJobId);
    expect(mockJob.getPacingFrequency()).andReturn(dummyPacingFrequency);
    expect(mockJob.getGroupedInvocationInput())
          .andReturn(mockGroupedInvocationInput);
    expect(mockGroupedInvocationInput.getAssayGroupName())
          .andReturn(dummyAssayGroupName);
    expect(mockGroupedInvocationInput.getAssayGroupLevel())
          .andReturn(dummyAssayGroupLevel);
    expect(mockJob.getMessages()).andReturn(dummyMessages);
    expect(mockJob.getDeltaAPD90PercentileNames())
          .andReturn(dummyDeltaAPD90PctileNames);

    final Set<JobResult> dummyJobResults = new HashSet<JobResult>();
    final JobResult mockJobResult1 = mocksControl.createMock(JobResult.class);
    final JobResult mockJobResult2 = mocksControl.createMock(JobResult.class);
    dummyJobResults.add(mockJobResult1);
    dummyJobResults.add(mockJobResult2);

    expect(mockJob.getJobResults()).andReturn(dummyJobResults);

    final float dummyCompoundConcentration1 = 1.0f;
    final String dummyDeltaAPD901 = "dummyDeltaAPD901";
    final String dummyUpstrokeVelocity1 = "dummyUpstrokeVelocity1";
    final String dummyTimes1 = "dummyTimes1";
    final String dummyVoltages1 = "dummyVoltages1";
    final String dummyQNet1 = "dummyQNet1";
    final float dummyCompoundConcentration2 = 2.0f;
    final String dummyDeltaAPD902 = "dummyDeltaAPD902";
    final String dummyUpstrokeVelocity2 = "dummyUpstrokeVelocity2";
    final String dummyTimes2 = "dummyTimes2";
    final String dummyVoltages2 = "dummyVoltages2";
    final String dummyQNet2 = "dummyQNet2";

    expect(mockJobResult1.getCompoundConcentration())
          .andReturn(dummyCompoundConcentration1);
    expect(mockJobResult1.getDeltaAPD90()).andReturn(dummyDeltaAPD901);
    expect(mockJobResult1.getUpstrokeVelocity()).andReturn(dummyUpstrokeVelocity1);
    expect(mockJobResult1.getTimes()).andReturn(dummyTimes1);
    expect(mockJobResult1.getVoltages()).andReturn(dummyVoltages1);
    expect(mockJobResult1.getQNet()).andReturn(dummyQNet1);
    final ConcentrationResultVO mockConcentrationResultVO1 = mocksControl.createMock(ConcentrationResultVO.class);
    expectNew(ConcentrationResultVO.class, dummyCompoundConcentration1,
                                           dummyTimes1, dummyVoltages1,
                                           dummyDeltaAPD901,
                                           dummyUpstrokeVelocity1, dummyQNet1)
             .andReturn(mockConcentrationResultVO1);
    expect(mockJobResult2.getCompoundConcentration())
          .andReturn(dummyCompoundConcentration2);
    expect(mockJobResult2.getDeltaAPD90()).andReturn(dummyDeltaAPD902);
    expect(mockJobResult2.getUpstrokeVelocity()).andReturn(dummyUpstrokeVelocity2);
    expect(mockJobResult2.getTimes()).andReturn(dummyTimes2);
    expect(mockJobResult2.getVoltages()).andReturn(dummyVoltages2);
    expect(mockJobResult2.getQNet()).andReturn(dummyQNet2);
    final ConcentrationResultVO mockConcentrationResultVO2 = mocksControl.createMock(ConcentrationResultVO.class);
    expectNew(ConcentrationResultVO.class, dummyCompoundConcentration2,
                                           dummyTimes2, dummyVoltages2,
                                           dummyDeltaAPD902,
                                           dummyUpstrokeVelocity2, dummyQNet2)
             .andReturn(mockConcentrationResultVO2);

    final Capture<List<ConcentrationResultVO>> captureConcResults = newCapture();
    final JobResultsVO mockJobResultsVO = mocksControl.createMock(JobResultsVO.class);
    expectNew(JobResultsVO.class, eq(dummyJobId), eq(dummyPacingFrequency),
                                  eq(dummyAssayGroupName),
                                  eq(dummyAssayGroupLevel), eq(dummyMessages),
                                  eq(dummyDeltaAPD90PctileNames),
                                  capture(captureConcResults))
             .andReturn(mockJobResultsVO);

    replayAll();
    mocksControl.replay();

    retrievedJobResults = resultsManager.retrieveResults(dummySimulationId);

    verifyAll();
    mocksControl.verify();

    assertSame(1, retrievedJobResults.size());
    final List<ConcentrationResultVO> capturedConcResults = captureConcResults.getValue();
    assertSame(dummyJobResults.size(), capturedConcResults.size());
  }
}