/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object.results;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

/**
 * Unit test per-concentration results value object.
 *
 * @author geoff
 */
public class ConcentrationResultsVOTest {

  @Test
  public void testConstructor() {
    float dummyCompoundConcentration = 0.0f;
    String dummyTimes = null;
    String dummyVoltages = null;
    String dummyDeltaAPD90 = null;
    String dummyUpstrokeVelocity = null;
    String dummyQNet = null;

    ConcentrationResultVO concRsltsVO = new ConcentrationResultVO(dummyCompoundConcentration,
                                                                  dummyTimes,
                                                                  dummyVoltages,
                                                                  dummyDeltaAPD90,
                                                                  dummyUpstrokeVelocity,
                                                                  dummyQNet);

    assertNotNull(concRsltsVO.toString());
    assertEquals(dummyCompoundConcentration,
                 concRsltsVO.getCompoundConcentration(), 0);
    assertNull(concRsltsVO.getTimes());
    assertNull(concRsltsVO.getVoltages());
    assertNull(concRsltsVO.getDeltaAPD90());
    assertNull(concRsltsVO.getUpstrokeVelocity());
    assertNull(concRsltsVO.getQNet());

    dummyCompoundConcentration = 0.5f;
    dummyTimes = "dummyTimes";
    dummyVoltages = "dummyVoltages";
    dummyDeltaAPD90 = "dummyDeltaAPD90";
    dummyUpstrokeVelocity = "dummyUpstrokeVelocity";
    dummyQNet = "dummyQNet";

    concRsltsVO = new ConcentrationResultVO(dummyCompoundConcentration,
                                            dummyTimes, dummyVoltages,
                                            dummyDeltaAPD90,
                                            dummyUpstrokeVelocity,
                                            dummyQNet);

    assertNotNull(concRsltsVO.toString());
    assertEquals(dummyCompoundConcentration,
                 concRsltsVO.getCompoundConcentration(), 0);
    assertEquals(dummyTimes, concRsltsVO.getTimes());
    assertEquals(dummyVoltages, concRsltsVO.getVoltages());
    assertEquals(dummyDeltaAPD90, concRsltsVO.getDeltaAPD90());
    assertEquals(dummyUpstrokeVelocity, concRsltsVO.getUpstrokeVelocity());
    assertEquals(dummyQNet, concRsltsVO.getQNet());
  }
}