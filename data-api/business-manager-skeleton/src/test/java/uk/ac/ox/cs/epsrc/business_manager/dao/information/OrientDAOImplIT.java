/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.dao.information;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.orientechnologies.orient.core.db.ODatabaseRecordThreadLocal;
import com.orientechnologies.orient.core.db.record.ODatabaseRecordTx;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.query.OSQLSynchQuery;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.dao.information.InformationDAO;
import uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager.INFORMATION_FORMAT;
import uk.ac.ox.cs.epsrc.business_manager.value.object.InformationVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * 
 *
 * @author geoff
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( { "classpath:/uk/ac/ox/cs/epsrc/business_manager/dao/information/OrientDAOImplIT-context.xml" } )
public class OrientDAOImplIT {

  // These values correspond to the private vars defined in OrientDAOImpl
  private static final String keyArgs = "args";
  private static final String keyCompoundIdentifier = "compoundIdentifier";
  private static final String keyJob = "job";
  private static final String keyGroupName = "groupName";
  private static final String keyGroupLevel = "groupLevel";
  private static final String keyJobId = "jobId";
  private static final String keyLevel = "level";
  private static final String keyPacingFrequency = "pacingFrequency";
  private static final String keyProgress = "progress";
  private static final String keySimulationId = "simulationId";
  private static final String keyText = "text";
  private static final String keyTimestamp = "timestamp";

  private static final String atRIDKey = "@rid";

  private Date dummyTimestamp;
  private INFORMATION_FORMAT dummyInformationFormat;
  private InformationLevel dummyInformationLevel;
  private List<String> dummyArgs;
  private long dummySimulationId;
  private String dummyCompoundIdentifier;
  private String dummyText;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_ORIENT_DAO)
  private InformationDAO informationDAO;

  @Before
  public void setUp() {
    dummyTimestamp = null;
    dummyInformationFormat = null;
    dummyInformationLevel = null;
    dummyArgs = null;
    dummyCompoundIdentifier = null;
    dummyText = null;
  }

  @After
  public void shutDown() {}

  @Test
  public void testProgressReadWrite() throws JSONException {
    dummySimulationId = 78l;
    dummyCompoundIdentifier = "dummyCompoundIdentifier";
    dummyTimestamp = new Date();
    dummyInformationLevel = InformationLevel.TRACE;
    dummyText = "dummyText1";
    dummyArgs = null;

    // Write the simulation/all progress and read back what's written.
    informationDAO.createProgressAll(dummyCompoundIdentifier, dummySimulationId, dummyTimestamp,
                                     dummyInformationLevel, dummyText, dummyArgs);

    dummyInformationFormat = INFORMATION_FORMAT.JSON;

    final InformationVO informationVO1 = informationDAO.retrieveProgress(dummySimulationId,
                                                                         dummyInformationFormat);
    assertTrue(informationVO1.hasInformation());
    assertEquals(Long.valueOf(dummySimulationId), informationVO1.getSimulationId());
    assertTrue(informationVO1.getAppManagerJobProgress().isEmpty());

    // e.g. {"@type":"d","@rid":"#13:0","@version":0,"@class":"All","compoundIdentifier":"dummyCompoundIdentifier",
    //       "simulationId":78,"progress":[{"@type":"d","@version":0,"timestamp":"1427204582136","level":"TRACE",
    //                                      "text":"dummyText1","args":null}],"@fieldTypes":"simulationId=l"}
    final JSONObject progressAll1 = new JSONObject(informationVO1.getInformation());
    assertEquals(dummyCompoundIdentifier, progressAll1.getString(keyCompoundIdentifier));
    assertEquals(dummySimulationId, progressAll1.getLong(keySimulationId));
    final JSONArray progressObject1 = progressAll1.getJSONArray(keyProgress);
    assertEquals(1, progressObject1.length());

    final Iterator<?> progressAllKeysIterator = progressAll1.keys();
    final Set<String> progressAllKeys = new HashSet<String>();
    while (progressAllKeysIterator.hasNext()) {
      progressAllKeys.add(progressAllKeysIterator.next().toString());
    }
    assertTrue(progressAllKeys.contains(keyProgress));
    assertTrue(progressAllKeys.contains(keyCompoundIdentifier));
    assertTrue(progressAllKeys.contains(keySimulationId));

    final JSONObject firstProgress = progressObject1.getJSONObject(0);
    final Iterator<?> progressTextKeysIterator = firstProgress.keys();
    final Set<String> progressTextKeys = new HashSet<String>();
    while (progressTextKeysIterator.hasNext()) {
      progressTextKeys.add(progressTextKeysIterator.next().toString());
    }
    // Confirm presence of values
    assertTrue(progressTextKeys.contains(keyTimestamp));
    assertTrue(progressTextKeys.contains(keyLevel));
    assertTrue(progressTextKeys.contains(keyArgs));
    assertTrue(progressTextKeys.contains(keyText));
    // Check values.
    assertEquals(dummyTimestamp, new Date(firstProgress.getLong(keyTimestamp)));
    assertTrue(dummyInformationLevel.compareTo(InformationLevel.valueOf(firstProgress.getString(keyLevel))) == 0);
    assertTrue(firstProgress.isNull(keyArgs));
    assertEquals(dummyText, firstProgress.getString(keyText));

    // Write another piece of simulation/all progress information.
    Date dummyTimestamp2 = new Date();
    InformationLevel dummyInformationLevel2 = InformationLevel.INFO;
    String dummyText2 = "dummyText2";
    List<String> dummyArgs2 = null;
    informationDAO.updateProgressAll(dummySimulationId, dummyTimestamp2, dummyInformationLevel2,
                                     dummyText2, dummyArgs2);

    final InformationVO informationVO2 = informationDAO.retrieveProgress(dummySimulationId,
                                                                         dummyInformationFormat);
    final JSONObject progressAll2 = new JSONObject(informationVO2.getInformation());
    assertEquals(dummyCompoundIdentifier, progressAll2.getString(keyCompoundIdentifier));
    assertEquals(dummySimulationId, progressAll2.getLong(keySimulationId));
    final JSONArray progressObject2 = progressAll2.getJSONArray(keyProgress);
    assertEquals(2, progressObject2.length());

    // Write a job progress
    final long dummyJob1JobId = 3498l;
    final String dummyJob1GroupName = "dummyJob1GroupName";
    final short dummyJob1GroupLevel = 3;
    final float dummyJob1PacingFrequency = 1.5f;
    final Date dummyJob1Timestamp = new Date();
    final InformationLevel dummyJob1InformationLevel = InformationLevel.WARN;
    final String dummyJob1Text = "dummyJob1Text";
    final List<String> dummyJob1Args = new ArrayList<String>();
    final String dummyJob1Arg1 = "dummyJob1ProgressArg1";
    final String dummyJob1Arg2 = "dummyJob1ProgressArg2";
    dummyJob1Args.add(dummyJob1Arg1);
    dummyJob1Args.add(dummyJob1Arg2);
    informationDAO.createProgressJob(dummySimulationId, dummyJob1JobId, dummyJob1GroupName,
                                     dummyJob1GroupLevel, dummyJob1PacingFrequency,
                                     dummyJob1Timestamp, dummyJob1InformationLevel,
                                     dummyJob1Text, dummyJob1Args);

    final InformationVO informationVO3 = informationDAO.retrieveProgress(dummySimulationId,
                                                                         dummyInformationFormat);
    /* {"@type":"d","@rid":"#13:0","@version":2,"@class":"All","compoundIdentifier":"dummyCompoundIdentifier",
     *  "simulationId":78,"progress":[{"@type":"d","@version":0,"timestamp":"1427213619044",
     *                                 "level":"TRACE","text":"dummyText1","args":null},
     *                                {"@type":"d","@version":0,"timestamp":"1427213619062",
     *                                 "level":"INFO","text":"dummyText2","args":null}],
     *  "job":[{"@type":"d","@rid":"#14:0","@version":0,"@class":"Job","jobId":3498,
     *          "groupName":"dummyJob1GroupName","groupLevel":3,"pacingFrequency":1.5,
     *          "progress":[{"@type":"d","@version":0,"timestamp":"1427213619070","level":"WARN",
     *                       "text":"dummyJob1Text","args":["dummyJob1ProgressArg1","dummyJob1ProgressArg2"]}],
     *          "@fieldTypes":"jobId=l,groupLevel=s,pacingFrequency=f"}],
     *  "@fieldTypes":"simulationId=l,job=e"}
     */
     final JSONObject progressAll3 = new JSONObject(informationVO3.getInformation());
     JSONArray jobProgresses = progressAll3.getJSONArray(keyJob);
     final JSONObject job1ProgressObject = jobProgresses.getJSONObject(0);
     final ORID job1ORID = new ORecordId(job1ProgressObject.getString(atRIDKey));

     final Iterator<?> job1ProgressKeysIterator = job1ProgressObject.keys();
     final Set<String> job1ProgressKeys = new HashSet<String>();
     while (job1ProgressKeysIterator.hasNext()) {
       job1ProgressKeys.add(job1ProgressKeysIterator.next().toString());
     }
     // Confirm presence of values.
     assertTrue(job1ProgressKeys.contains(keyJobId));
     assertTrue(job1ProgressKeys.contains(keyGroupName));
     assertTrue(job1ProgressKeys.contains(keyGroupLevel));
     assertTrue(job1ProgressKeys.contains(keyPacingFrequency));
     assertTrue(job1ProgressKeys.contains(keyProgress));
     // Confirm values
     assertEquals(dummyJob1JobId, job1ProgressObject.getLong(keyJobId));
     assertEquals(dummyJob1GroupName, job1ProgressObject.getString(keyGroupName));
     assertEquals(dummyJob1GroupLevel, job1ProgressObject.getInt(keyGroupLevel));
     assertEquals(dummyJob1PacingFrequency, job1ProgressObject.getDouble(keyPacingFrequency), 0.001);
     final JSONArray job1Progresses = job1ProgressObject.getJSONArray(keyProgress);
     final JSONObject job1FirstProgress = job1Progresses.getJSONObject(0);
     // Check values.
     assertEquals(dummyJob1Timestamp, new Date(job1FirstProgress.getLong(keyTimestamp)));
     assertTrue(dummyJob1InformationLevel.compareTo(InformationLevel.valueOf(job1FirstProgress.getString(keyLevel))) == 0);
     final JSONArray job1FirstProgressArgs = job1FirstProgress.getJSONArray(keyArgs);
     assertEquals(dummyJob1Args.size(), job1FirstProgressArgs.length());
     assertEquals(dummyJob1Arg1, job1FirstProgressArgs.getString(0));
     assertEquals(dummyJob1Arg2, job1FirstProgressArgs.getString(1));
     assertEquals(dummyJob1Text, job1FirstProgress.getString(keyText));

     // Write another job progress
     final long dummyJob2JobId = 3502l;
     final String dummyJob2GroupName = "dummyJob2GroupName";
     final short dummyJob2GroupLevel = 4;
     final float dummyJob2PacingFrequency = 1.75f;
     final Date dummyJob2Timestamp = new Date();
     final InformationLevel dummyJob2InformationLevel = InformationLevel.WARN;
     final String dummyJob2Text = "dummyJob2Text";
     final List<String> dummyJob2Args = new ArrayList<String>();
     final String dummyJob2Arg1 = "dummyJob2ProgressArg1";
     final String dummyJob2Arg2 = "dummyJob2ProgressArg2";
     dummyJob2Args.add(dummyJob2Arg1);
     dummyJob2Args.add(dummyJob2Arg2);
     informationDAO.createProgressJob(dummySimulationId, dummyJob2JobId, dummyJob2GroupName,
                                      dummyJob2GroupLevel, dummyJob2PacingFrequency,
                                      dummyJob2Timestamp, dummyJob2InformationLevel,
                                      dummyJob2Text, dummyJob2Args);
     final InformationVO informationVO4 = informationDAO.retrieveProgress(dummySimulationId,
                                                                          dummyInformationFormat);
     /* {"@type":"d","@rid":"#13:0","@version":3,"@class":"All","compoundIdentifier":"dummyCompoundIdentifier",
      *  "simulationId":78,"progress":[{"@type":"d","@version":0,"timestamp":"1427213619044",
      *                                 "level":"TRACE","text":"dummyText1","args":null},
      *                                {"@type":"d","@version":0,"timestamp":"1427213619062",
      *                                 "level":"INFO","text":"dummyText2","args":null}],
      *  "job":[{"@type":"d","@rid":"#14:0","@version":0,"@class":"Job","jobId":3498,
      *          "groupName":"dummyJob1GroupName","groupLevel":3,"pacingFrequency":1.5,
      *          "progress":[{"@type":"d","@version":0,"timestamp":"1427213619070","level":"WARN",
      *                       "text":"dummyJob1Text","args":["dummyJob1ProgressArg1","dummyJob1ProgressArg2"]}],
      *          "@fieldTypes":"jobId=l,groupLevel=s,pacingFrequency=f"},
      *         {"@type":"d","@rid":"#14:1","@version":0,"@class":"Job","jobId":3502,
      *          "groupName":"dummyJob2GroupName","groupLevel":4,"pacingFrequency":1.75,
      *          "progress":[{"@type":"d","@version":0,"timestamp":"1427213619079","level":"WARN",
      *                       "text":"dummyJob2Text","args":["dummyJob2ProgressArg1","dummyJob2ProgressArg2"]}],
      *          "@fieldTypes":"jobId=l,groupLevel=s,pacingFrequency=f"}],
      *  "@fieldTypes":"simulationId=l,job=e"}
      */
     final JSONObject progressAll4 = new JSONObject(informationVO4.getInformation());
     jobProgresses = progressAll4.getJSONArray(keyJob);
     JSONObject job2ProgressObject = jobProgresses.getJSONObject(1);
     final ORID job2ORID = new ORecordId(job2ProgressObject.getString(atRIDKey));
     final Iterator<?> job2ProgressKeysIterator = job2ProgressObject.keys();
     final Set<String> job2ProgressKeys = new HashSet<String>();
     while (job2ProgressKeysIterator.hasNext()) {
       job2ProgressKeys.add(job2ProgressKeysIterator.next().toString());
     }
     // Confirm presence of values.
     assertTrue(job2ProgressKeys.contains(keyJobId));
     assertTrue(job2ProgressKeys.contains(keyGroupName));
     assertTrue(job2ProgressKeys.contains(keyGroupLevel));
     assertTrue(job2ProgressKeys.contains(keyPacingFrequency));
     assertTrue(job2ProgressKeys.contains(keyProgress));
     // Confirm values.
     assertEquals(dummyJob2JobId, job2ProgressObject.getLong(keyJobId));
     assertEquals(dummyJob2GroupName, job2ProgressObject.getString(keyGroupName));
     assertEquals(dummyJob2GroupLevel, job2ProgressObject.getInt(keyGroupLevel));
     assertEquals(dummyJob2PacingFrequency, job2ProgressObject.getDouble(keyPacingFrequency), 0.001);

     // Try adding job progress
     final Date dummyJob2Timestamp2 = new Date();
     final InformationLevel dummyJob2InformationLevel2 = InformationLevel.ERROR;
     final String dummyJob2Text2 = "dummyJob2Text2";
     final List<String> dummyJob2Args2 = new ArrayList<String>();
     informationDAO.updateProgressJob(dummyJob2JobId, dummyJob2Timestamp2,
                                      dummyJob2InformationLevel2, dummyJob2Text2, dummyJob2Args2);
     final InformationVO informationVO5 = informationDAO.retrieveProgress(dummySimulationId,
                                                                          dummyInformationFormat);
     final JSONObject progressAll5 = new JSONObject(informationVO5.getInformation());
     jobProgresses = progressAll5.getJSONArray(keyJob);
     job2ProgressObject = jobProgresses.getJSONObject(1);
     final JSONArray job2Progresses = job2ProgressObject.getJSONArray(keyProgress);
     assertEquals(2, job2Progresses.length());
     final JSONObject job2SecondProgress = job2Progresses.getJSONObject(1);
     assertEquals(dummyJob2Timestamp2, new Date(job2SecondProgress.getLong(keyTimestamp)));
     assertTrue(dummyJob2InformationLevel2.compareTo(InformationLevel.valueOf(job2SecondProgress.getString(keyLevel))) == 0);
     final JSONArray job2SecondProgressArgs = job2SecondProgress.getJSONArray(keyArgs);
     assertEquals(dummyJob2Args2.size(), job2SecondProgressArgs.length());
     assertEquals(dummyJob2Text2, job2SecondProgress.getString(keyText));

     // Remove all the data
     informationDAO.purge(dummySimulationId);

     final InformationVO informationVO6 = informationDAO.retrieveProgress(dummySimulationId,
                                                                          dummyInformationFormat);
     assertTrue(informationVO6.getAppManagerJobProgress().isEmpty());
     assertEquals(InformationVO.EMPTY_ASSOC_ARRAY, informationVO6.getInformation());
     assertEquals(Long.valueOf(dummySimulationId), informationVO6.getSimulationId());

     // Make sure both job progress objects ahave been removed by purge
     String command = "traverse * from " + job1ORID;
     final ODatabaseRecordTx database = (ODatabaseRecordTx) ODatabaseRecordThreadLocal.INSTANCE.get();
     List<ODocument> traversed = database.command(new OSQLSynchQuery<ODocument>(command)).execute();
     assertTrue(traversed.isEmpty());
     command = "traverse * from " + job2ORID;
     traversed =  database.command(new OSQLSynchQuery<ODocument>(command)).execute();
     assertTrue(traversed.isEmpty());
  }

  @Test
  public void testWriteUpdate() {
    final Long simulationId = 1L;
    final String compoundIdentifier = "compoundIdentifier";

    final InformationLevel level = InformationLevel.TRACE;

    final Map<String, Object> rawDataSummaryId1 = new HashMap<String, Object>();
    rawDataSummaryId1.put("dummyKey", "dummyValue");
    final Map<String, Object> rawDataIndividualId1 = new HashMap<String, Object>();
    rawDataIndividualId1.put("dummyKey", "dummyValue");
    final Map<String, Object> rawDataIndividualId2 = new HashMap<String, Object>();
    rawDataIndividualId2.put("dummyKey", "dummyValue");

    informationDAO.writeFundamental(compoundIdentifier, simulationId, level, "writeGeneric.1",
                                    null);

    informationDAO.writeFundamental(null, simulationId, level, "writeBySimulationId.1", null);

    informationDAO.writeFundamental(null, simulationId, level, "writeBySimulationId.2", null);
  }

  @Test
  public void testWriteQuery() {
    dummySimulationId = 1l;

    dummyCompoundIdentifier = "dummyCompoundIdentifier";

    dummyInformationLevel = InformationLevel.TRACE;

    final Map<String, Object> dummySummaryRawData = new HashMap<String, Object>();
    dummySummaryRawData.put("dummySummaryKey", "dummySummaryValue");

    final Map<String, Object> dummyIndividualRawData = new HashMap<String, Object>();
    dummyIndividualRawData.put("dummyIndividualKey", "dummyIndividualValue");

    final Map<String, Object> dummyDoseResponseRawData = new HashMap<String, Object>();
    dummyDoseResponseRawData.put("dummyDoseResponseKey", "dummyDoseResponseValue");

    informationDAO.writeFundamental(dummyCompoundIdentifier, dummySimulationId,
                                    dummyInformationLevel, "writeGeneric.1", null);

    informationDAO.writeFundamental(null, dummySimulationId, dummyInformationLevel,
                                    "writeBySimulationId.1", null);

    informationDAO.writeFundamental(null, dummySimulationId, dummyInformationLevel,
                                    "writeBySimulationId.2", null);
  }
}