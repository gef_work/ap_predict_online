/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object.results;

import static org.easymock.EasyMock.createStrictControl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Test;

/**
 * Unit test the job results value object.
 *
 * @author geoff
 */
public class JobResultsVOTest {

  @Test
  public void testConstructor() {
    long dummyJobId = 5l;
    Float dummyPacingFrequency = null;
    String dummyAssayGroupName = null;
    short dummyAssayGroupLevel = 6;
    String dummyMessages = null;
    String dummyDeltaAPD90PctileNames = null;
    List<ConcentrationResultVO> dummyConcRslts = null;

    JobResultsVO jobResultsVO = new JobResultsVO(dummyJobId,
                                                 dummyPacingFrequency,
                                                 dummyAssayGroupName,
                                                 dummyAssayGroupLevel,
                                                 dummyMessages,
                                                 dummyDeltaAPD90PctileNames,
                                                 dummyConcRslts);

    assertNotNull(jobResultsVO.toString());
    assertSame(dummyJobId, jobResultsVO.getJobId());
    assertNull(jobResultsVO.getPacingFrequency());
    assertNull(jobResultsVO.getAssayGroupName());
    assertSame(dummyAssayGroupLevel, jobResultsVO.getAssayGroupLevel());
    assertNull(jobResultsVO.getMessages());
    assertNull(jobResultsVO.getDeltaAPD90PercentileNames());
    assertTrue(jobResultsVO.getConcentrationResults().isEmpty());

    dummyPacingFrequency = 4.2F;
    dummyAssayGroupName = "dummyAssayGroupName";
    dummyMessages = "dummyMessages";
    dummyDeltaAPD90PctileNames = "dummyDeltaAPD90PctileNames";
    final IMocksControl mocksControl = createStrictControl();
    final ConcentrationResultVO mockConcentrationResultVO = mocksControl.createMock(ConcentrationResultVO.class);
    dummyConcRslts = new ArrayList<ConcentrationResultVO>();
    dummyConcRslts.add(mockConcentrationResultVO);

    jobResultsVO = new JobResultsVO(dummyJobId, dummyPacingFrequency,
                                    dummyAssayGroupName, dummyAssayGroupLevel,
                                    dummyMessages, dummyDeltaAPD90PctileNames,
                                    dummyConcRslts);

    assertNotNull(jobResultsVO.toString());
    assertEquals(dummyPacingFrequency, jobResultsVO.getPacingFrequency());
    assertEquals(dummyAssayGroupName, jobResultsVO.getAssayGroupName());
    assertEquals(dummyMessages, jobResultsVO.getMessages());
    assertEquals(dummyDeltaAPD90PctileNames,
                 jobResultsVO.getDeltaAPD90PercentileNames());
    final List<ConcentrationResultVO> retrievedConcRslts = jobResultsVO.getConcentrationResults();
    assertSame(dummyConcRslts.size(), retrievedConcRslts.size());
    assertSame(mockConcentrationResultVO, retrievedConcRslts.get(0));

    try {
      retrievedConcRslts.clear();
      fail("Should not permit modification of returned concentration results collection!");
    } catch (UnsupportedOperationException e) {}
  }
}