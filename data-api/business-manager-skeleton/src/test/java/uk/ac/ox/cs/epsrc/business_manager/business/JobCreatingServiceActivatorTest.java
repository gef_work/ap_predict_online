/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.JobCreatingServiceActivator;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.JobProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.entity.GroupedInvocationInput;
import uk.ac.ox.cs.epsrc.business_manager.entity.Job;
import uk.ac.ox.cs.epsrc.business_manager.manager.JobManager;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;

/**
 * Test the job creating service activator.
 *
 * @author geoff
 */
public class JobCreatingServiceActivatorTest {

  private BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate;
  private IMocksControl mocksControl;
  private JobCreatingServiceActivator jobCreatingServiceActivator;
  private JobManager mockJobManager;
  private final Long dummySimulationId = 1L;
  private final ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    mockJobManager = mocksControl.createMock(JobManager.class);
    jobCreatingServiceActivator = new JobCreatingServiceActivator();
    ReflectionTestUtils.setField(jobCreatingServiceActivator, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
    ReflectionTestUtils.setField(jobCreatingServiceActivator, BusinessIdentifiers.COMPONENT_JOB_MANAGER,
                                 mockJobManager);
  }

  @Test
  public void testNoJobsCreated() {
    ProcessedDataVO mockProcessedDataVO = mocksControl.createMock(ProcessedDataVO.class);
    expect(mockJobManager.createAndStore(dummySimulationId.longValue(), mockProcessedDataVO))
          .andReturn(new HashSet<Job>());
    mocksControl.replay();
    Set<Job> jobs = jobCreatingServiceActivator.createJobs(dummyProcessingType, dummySimulationId,
                                                           mockProcessedDataVO);
    mocksControl.verify();
    assertTrue(jobs.isEmpty());
  }

  @Test
  public void testEmulateJobsCreated() {
    ProcessedDataVO mockProcessedDataVO = mocksControl.createMock(ProcessedDataVO.class);
    Job mockJob = mocksControl.createMock(Job.class);
    Long dummyJobId = 2L;
    GroupedInvocationInput mockGroupedInvocationInput = mocksControl.createMock(GroupedInvocationInput.class);
    String dummyAssayGroupName = "fakeAssayGroupName";
    Short dummyAssayGroupLevel = Short.valueOf("2");
    Float dummyPacingFrequency = 3F;
    final Set<Job> fakeJobs = new HashSet<Job>();
    fakeJobs.add(mockJob);
    expect(mockJobManager.createAndStore(dummySimulationId.longValue(), mockProcessedDataVO))
          .andReturn(fakeJobs);
    expect(mockJob.getId()).andReturn(dummyJobId);
    expect(mockJob.getGroupedInvocationInput()).andReturn(mockGroupedInvocationInput);
    expect(mockGroupedInvocationInput.getAssayGroupName()).andReturn(dummyAssayGroupName);
    expect(mockGroupedInvocationInput.getAssayGroupLevel()).andReturn(dummyAssayGroupLevel);
    expect(mockJob.getPacingFrequency()).andReturn(dummyPacingFrequency);

    Capture<Progress> dummyOverallProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress));

    Capture<Progress> dummyJobProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyJobProgress));

    mocksControl.replay();
    Set<Job> jobs = jobCreatingServiceActivator.createJobs(dummyProcessingType, dummySimulationId,
                                                           mockProcessedDataVO);
    mocksControl.verify();
    assertFalse(jobs.isEmpty());

    OverallProgress capturedOverallProgress = (OverallProgress) dummyOverallProgress.getValue();
    assertEquals(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertEquals(MessageKey.JOB_DATA_CREATION.getBundleIdentifier(),
                 capturedOverallProgress.getText());
    List<String> overallProgressArgs = capturedOverallProgress.getArgs();
    assertEquals(dummyAssayGroupName, overallProgressArgs.get(0));
    assertEquals(dummyPacingFrequency.toString(), overallProgressArgs.get(1));
    assertEquals(dummyJobId.toString(), overallProgressArgs.get(2));
    assertSame(InformationLevel.TRACE, capturedOverallProgress.getLevel());

    JobProgress capturedJobProgress = (JobProgress) dummyJobProgress.getValue();
    assertEquals(dummySimulationId, capturedJobProgress.getSimulationId());
    assertEquals(MessageKey.JOB_CREATED.getBundleIdentifier(), capturedJobProgress.getText());
    List<String> jobProgressArgs = capturedJobProgress.getArgs();
    assertEquals(dummyAssayGroupName, jobProgressArgs.get(0));
    assertEquals(dummyPacingFrequency.toString(), jobProgressArgs.get(1));
    assertSame(InformationLevel.TRACE, capturedJobProgress.getLevel());
    assertEquals(dummyJobId, capturedJobProgress.getJobId());
    assertEquals(dummyAssayGroupName, capturedJobProgress.getGroupName());
    assertEquals(dummyAssayGroupLevel, capturedJobProgress.getGroupLevel());
    assertEquals(dummyPacingFrequency, capturedJobProgress.getPacingFrequency());
  }
}