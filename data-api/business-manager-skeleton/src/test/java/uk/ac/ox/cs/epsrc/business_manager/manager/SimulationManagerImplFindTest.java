/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.manager;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertSame;

import java.math.BigDecimal;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.dao.SimulationDAO;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager;
import uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManagerImpl;
import uk.ac.ox.cs.epsrc.business_manager.value.object.SimulationRequestVO;

/**
 * Unit test simulation manager find operations.
 *
 * @author geoff
 */
public class SimulationManagerImplFindTest {

  private IMocksControl mocksControl;
  private Simulation mockSimulation;
  private SimulationDAO mockSimulationDAO;
  private SimulationManager simulationManager;

  private String dummyCompoundIdentifier;
  private Boolean dummyAssayGrouping;
  private Boolean dummyValueInheriting;
  private Boolean dummyBetweenGroups;
  private Boolean dummyWithinGroups;
  private Short dummyCellModelIdentifier;
  private BigDecimal dummyPacingMaxTime;

  private static final long dummySimulationId = 4l;

  @Before
  public void setUp() {
    simulationManager = new SimulationManagerImpl();

    mocksControl = createStrictControl();
    mockSimulation = mocksControl.createMock(Simulation.class);
    mockSimulationDAO = mocksControl.createMock(SimulationDAO.class);

    ReflectionTestUtils.setField(simulationManager, BusinessIdentifiers.COMPONENT_SIMULATION_DAO,
                                 mockSimulationDAO);
  }

  private void setStandardValues() {
    dummyCompoundIdentifier = "dummyCompoundIdentifier";
    dummyAssayGrouping = true;
    dummyValueInheriting = false;
    dummyBetweenGroups = true;
    dummyWithinGroups = false;
    dummyCellModelIdentifier = SimulationManagerImpl.anticipatedCellMLModelIdentifierMinValue;
    dummyPacingMaxTime = BigDecimal.TEN;
  }

  @Test
  public void testFindByAllProperties() {
    setStandardValues();

    expect(mockSimulationDAO.findByAllProperties(dummyCompoundIdentifier,
                                                 dummyAssayGrouping,
                                                 dummyValueInheriting,
                                                 dummyBetweenGroups,
                                                 dummyWithinGroups,
                                                 dummyCellModelIdentifier,
                                                 dummyPacingMaxTime))
                            .andReturn(mockSimulation);

    mocksControl.replay();

    final Simulation found = simulationManager.find(dummyCompoundIdentifier,
                                                    dummyAssayGrouping,
                                                    dummyValueInheriting,
                                                    dummyBetweenGroups,
                                                    dummyWithinGroups,
                                                    dummyCellModelIdentifier,
                                                    dummyPacingMaxTime);

    mocksControl.verify();

    assertSame(mockSimulation, found);
  }

  @Test
  public void testFindBySimulationId() {
    expect(mockSimulationDAO.findBySimulationId(dummySimulationId)).andReturn(mockSimulation);

    mocksControl.replay();

    final Simulation found = simulationManager.find(dummySimulationId);

    mocksControl.verify();

    assertSame(mockSimulation, found);
  }

  @Test
  public void testFindBySimulationRequest() {
    setStandardValues();

    final SimulationRequestVO mockSimulationRequest = mocksControl.createMock(SimulationRequestVO.class);
    expect(mockSimulationRequest.getCompoundIdentifier())
          .andReturn(dummyCompoundIdentifier);
    expect(mockSimulationRequest.isAssayGrouping())
          .andReturn(dummyAssayGrouping);
    expect(mockSimulationRequest.isValueInheriting())
          .andReturn(dummyValueInheriting);
    expect(mockSimulationRequest.isBetweenGroups())
          .andReturn(dummyBetweenGroups);
    expect(mockSimulationRequest.isWithinGroups())
          .andReturn(dummyWithinGroups);
    expect(mockSimulationRequest.getCellModelIdentifier())
          .andReturn(dummyCellModelIdentifier);
    expect(mockSimulationRequest.getPacingMaxTime())
          .andReturn(dummyPacingMaxTime);

    // >> find(...)
    expect(mockSimulationDAO.findByAllProperties(dummyCompoundIdentifier,
                                                 dummyAssayGrouping,
                                                 dummyValueInheriting,
                                                 dummyBetweenGroups,
                                                 dummyWithinGroups,dummyCellModelIdentifier,
                                                 dummyPacingMaxTime))
                            .andReturn(mockSimulation);
    // << find(...)

    mocksControl.replay();

    final Simulation found = simulationManager.find(mockSimulationRequest);

    mocksControl.verify();

    assertSame(mockSimulation, found);
  }
}