/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 * 
 */
package uk.ac.ox.cs.epsrc.business_manager.entity;

import static org.easymock.EasyMock.createStrictControl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit test GroupedInvocationInput object.
 *
 * @author geoff
 */
public class GroupedInvocationInputTest {

  private GroupedInvocationInput groupedInvocationInput;
  private IMocksControl mocksControl;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
  }

  @Test
  public void testDefaultConstructor() {
    groupedInvocationInput = new GroupedInvocationInput();

    assertNotNull(groupedInvocationInput.toString());
    assertNull(groupedInvocationInput.getAssayGroupLevel());
    assertNull(groupedInvocationInput.getAssayGroupName());
  }

  @Test
  public void testInitialisingConstructor() {
    final Long dummySimulationId = null;
    Short dummyGroupLevel = null;
    String dummyGroupName = null;
    Set<IonChannelValues> dummyIonChannelValues = null;

    groupedInvocationInput = new GroupedInvocationInput(dummySimulationId, dummyGroupLevel,
                                                        dummyGroupName, dummyIonChannelValues);

    assertNull(groupedInvocationInput.getAssayGroupLevel());
    assertNull(groupedInvocationInput.getAssayGroupName());
    assertTrue(groupedInvocationInput.getIonChannelValues().isEmpty());

    dummyGroupLevel = 0;
    dummyGroupName = "dummyGroupName";

    dummyIonChannelValues = new HashSet<IonChannelValues>();
    final IonChannelValues ionChannelValues1 = mocksControl.createMock(IonChannelValues.class);
    dummyIonChannelValues.add(ionChannelValues1);
    final IonChannelValues ionChannelValues2 = mocksControl.createMock(IonChannelValues.class);
    dummyIonChannelValues.add(ionChannelValues2);
    groupedInvocationInput = new GroupedInvocationInput(dummySimulationId, dummyGroupLevel,
                                                        dummyGroupName, dummyIonChannelValues);

    assertSame(dummyGroupLevel, groupedInvocationInput.getAssayGroupLevel());
    assertEquals(dummyGroupName, groupedInvocationInput.getAssayGroupName());
    assertEquals(2, groupedInvocationInput.getIonChannelValues().size());
    assertTrue(groupedInvocationInput.getIonChannelValues().contains(ionChannelValues1));
    assertTrue(groupedInvocationInput.getIonChannelValues().contains(ionChannelValues2));
  }
}