/*

  Original work: Copyright (c) 2015, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.PostProcessingDataRecorder;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.entity.SimulationAssay;
import uk.ac.ox.cs.epsrc.business_manager.manager.SimulationAssayManager;
import uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager;
import uk.ac.ox.cs.epsrc.business_manager.value.object.PreInvocationProcessingVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata.ProcessedSiteDataVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.workflow.OutcomeVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;

/**
 * Unit test the post-processing data recorder.
 *
 * @author geoff
 */
public class PostProcessingDataRecorderTest {

  private BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate;
  private IMocksControl mocksControl;
  private final Long dummySimulationId = 1L;
  private PostProcessingDataRecorder recorder;
  private SimulationAssayManager mockSimulationAssayManager;
  private SimulationManager mockSimulationManager;

  @Before
  public void setUp() {
    recorder = new PostProcessingDataRecorder();
    mocksControl = createStrictControl();
    mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    mockSimulationAssayManager = mocksControl.createMock(SimulationAssayManager.class);
    mockSimulationManager = mocksControl.createMock(SimulationManager.class);
    ReflectionTestUtils.setField(recorder, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
    ReflectionTestUtils.setField(recorder, BusinessIdentifiers.COMPONENT_SIMULATIONASSAY_MANAGER,
                                 mockSimulationAssayManager);
    ReflectionTestUtils.setField(recorder, BusinessIdentifiers.COMPONENT_SIMULATION_MANAGER,
                                 mockSimulationManager);
  }

  @Test(expected=IllegalArgumentException.class)
  public void testExceptionOnInvalidNullParameter() {
    ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    recorder.postProcessingDataRecording(dummyProcessingType, dummySimulationId, null);
  }

  @Test
  public void testPostProcessingDataRecordingRegularSimulation() {
    // Regular simulation, without experimental data in processed site data.
    ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    final ProcessedSiteDataVO mockProcessedSiteData = mocksControl.createMock(ProcessedSiteDataVO.class);

    final List<OutcomeVO> dummyQSARData = new ArrayList<OutcomeVO>();
    final List<OutcomeVO> dummyScreeningData = new ArrayList<OutcomeVO>();
    final List<SimulationAssay> dummyTransients = new ArrayList<SimulationAssay>();
    final List<SimulationAssay> dummyPersisted = new ArrayList<SimulationAssay>();

    expect(mockProcessedSiteData.getQSAR()).andReturn(dummyQSARData);
    expect(mockProcessedSiteData.getScreening()).andReturn(dummyScreeningData);
    expect(mockSimulationAssayManager.createTransient(dummySimulationId,
                                                      dummyQSARData,
                                                      dummyScreeningData))
          .andReturn(dummyTransients); 
    expect(mockSimulationAssayManager.persistSimulationAssays(dummyTransients))
          .andReturn(dummyPersisted);

    Capture<Progress> dummyOverallProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress));

    expect(mockProcessedSiteData.getExperimental()).andReturn(null);

    mocksControl.replay();

    PreInvocationProcessingVO preInvocationProcessing = recorder.postProcessingDataRecording(dummyProcessingType,
                                                                                             dummySimulationId,
                                                                                             mockProcessedSiteData);

    mocksControl.verify();

    assertTrue(preInvocationProcessing.getPerFrequencyConcentrations().isEmpty());
    assertTrue(preInvocationProcessing.getSimulationAssays().isEmpty());

    OverallProgress capturedOverallProgress = (OverallProgress) dummyOverallProgress.getValue();
    assertEquals(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertEquals(MessageKey.ASSAY_VALUES_PERSISTED.getBundleIdentifier(),
                 capturedOverallProgress.getText());
    assertTrue(capturedOverallProgress.getArgs().isEmpty());
    assertSame(InformationLevel.TRACE, capturedOverallProgress.getLevel());
  }

  @Test
  public void testPostProcessingSimulationTermination() {
    ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    final Long dummySimulationId = 2L;

    /*
     * Test 1 : Invalid null object.
     */
    ProcessedSiteDataVO mockProcessedSiteDataVO = null;

    try {
      recorder.postProcessingSimulationTermination(dummyProcessingType, dummySimulationId,
                                                   mockProcessedSiteDataVO);
      fail("Should not permit use of null processed site data VO");
    } catch (IllegalArgumentException e) {}

    /*
     * Test 2 : Invalid presence of site data.
     */
    mockProcessedSiteDataVO = mocksControl.createMock(ProcessedSiteDataVO.class);
    expect(mockProcessedSiteDataVO.isContainingSiteData()).andReturn(true);

    mocksControl.replay();

    try {
      recorder.postProcessingSimulationTermination(dummyProcessingType, dummySimulationId,
                                                   mockProcessedSiteDataVO);
      fail("Should not permit use of processed site data VO containing site data");
    } catch (IllegalArgumentException e) {}

    mocksControl.verify();

    mocksControl.reset();

    ProcessedSiteDataVO returnedProcessedSiteData = null;

    /*
     * Test 3 : Not input data gathering.
     */
    expect(mockProcessedSiteDataVO.isContainingSiteData()).andReturn(false);
    mockSimulationManager.simulationTerminating(dummySimulationId);

    mocksControl.replay();

    returnedProcessedSiteData = recorder.postProcessingSimulationTermination(dummyProcessingType,
                                                                             dummySimulationId,
                                                                             mockProcessedSiteDataVO);

    mocksControl.verify();

    assertSame(mockProcessedSiteDataVO, returnedProcessedSiteData);
  }
}