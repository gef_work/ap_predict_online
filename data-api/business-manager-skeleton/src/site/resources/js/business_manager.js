/**
 * Go back a page.
 */
function back() {
  window.history.go(-1);
}