/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.annotation.Header;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.service.SimulationService;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Initiate the simulation processing, i.e. these are the steps following on from the original 
 * simulations request processing!
 * 
 * @author geoff
 */
// see appCtx.sim.processRequest.xml
public class SimulationProcessingInitiator {

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JMS_TEMPLATE)
  private BusinessManagerJMSTemplate jmsTemplate;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE)
  private SimulationService simulationService;

  // Indicator of the number of entities updated.
  private static final Integer successfulSQLUpdateResponse = 1;

  private static final Log log = LogFactory.getLog(SimulationProcessingInitiator.class);

  /**
   * Initiate simulation processing.
 
   * @param simulationId Simulation identifier for processing.
   * @param updateResponse Response from the Spring Integration workflow's prior SQL update of Simulation. 
   * @throws IllegalArgumentException If response from prior Simulation SQL update failed.
   */
  public void initiateSimulationProcessing(final @Header(value=BusinessIdentifiers.SI_HDR_SIMULATION_ID,
                                                         required=true)
                                                 Long simulationId,
                                           final Integer updateResponse)
                                           throws IllegalArgumentException {
    if (updateResponse != successfulSQLUpdateResponse) {
      final String errorMessage = "Update failed.";
      log.error("~initiateSimulationProcessing() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    log.debug("~initiateSimulationProcessing() : Invoked for simulation id '" + simulationId + "'.");

    jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.TRACE,
                                                         MessageKey.SIMULATION_INITIATION.getBundleIdentifier(),
                                                         simulationId).build());

    simulationService.processSimulation(simulationId);
  }
}