/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.epsrc.business_manager.value.object.workflow.OutcomeVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ExperimentalDataHolder;

/**
 * Processed site data.
 *
 * @author geoff
 */
public class ProcessedSiteDataVO implements Serializable {

  private static final long serialVersionUID = -741154582490960538L;

  private final ExperimentalDataHolder experimental;
  private final List<OutcomeVO> qsar = new ArrayList<OutcomeVO>();
  private final List<OutcomeVO> screening = new ArrayList<OutcomeVO>();

  private static final Log log = LogFactory.getLog(ProcessedSiteDataVO.class);

  /**
   * Intialising constructor.
   *
   * @param experimental Experimental data.
   * @param qsar QSAR data.
   * @param screening Screening data.
   */
  public ProcessedSiteDataVO(final ExperimentalDataHolder experimental,
                             final List<OutcomeVO> qsar,
                             final List<OutcomeVO> screening) {
    log.debug("~ProcessedSiteDataVO() : Invoked.");

    this.experimental = experimental;
    if (qsar != null && !qsar.isEmpty()) {
      this.qsar.addAll(qsar);
    }
    if (screening != null && !screening.isEmpty()) {
      this.screening.addAll(screening);
    }
  }

  /**
   * Indicator as to whether there's site data been processed.
   * 
   * @return True if either experimental, QSAR or screening data has been retrieved.
   */
  public boolean isContainingSiteData() {
    return ((getExperimental() != null && getExperimental().containsData()) || 
            !getScreening().isEmpty() || !getQSAR().isEmpty());
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "ProcessedSiteDataVO [experimental=" + experimental + ", qsar="
        + qsar + ", screening=" + screening + "]";
  }

  /**
   * Retrieve the processed experimental data.
   * 
   * @return Processed experimental data (or {@code null} if none available).
   */
  public ExperimentalDataHolder getExperimental() {
    return experimental;
  }

  /**
   * Retrieve the processed screening data.
   * 
   * @return Unmodifiable collection of processed screening data (or empty collection if none
   *         available).
   */
  public List<OutcomeVO> getScreening() {
    return Collections.unmodifiableList(screening);
  }

  /**
   * Retrieve the processed QSAR data.
   * 
   * @return Unmodifiable collection of processed QSAR data (or empty collection if none available).
   */
  public List<OutcomeVO> getQSAR() {
    return Collections.unmodifiableList(qsar);
  }
}