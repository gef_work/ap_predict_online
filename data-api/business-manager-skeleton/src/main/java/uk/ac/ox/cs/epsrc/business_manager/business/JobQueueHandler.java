/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.Message;
import org.springframework.integration.MessageHeaders;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.QueueChannel;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Handle messages in the job queue.
 *
 * @author geoff
 */
public class JobQueueHandler {

  // JMS Template for sending provenance details.
  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JMS_TEMPLATE)
  private BusinessManagerJMSTemplate jmsTemplate;

  protected DirectChannel outputChannel;
  protected QueueChannel inputChannel;

  // defined in appCtx.appManager.xml
  private Map<String, String> appManagerWSExceptionCodes = new HashMap<String, String>();

  private static final Log log = LogFactory.getLog(JobQueueHandler.class);

  /**
   * Handle job messages.
   */
  public void handleJobMessages() {
    log.trace("~handleJobMessages() : Invoked.");
    final Message<?> message = inputChannel.receive(1000);
    if (message != null) {
      log.debug("~handleJobMessages() : Message received.");
      try {
        outputChannel.send(message);
      } catch (Exception e) {
        log.debug("~handleJobMessages() : Exception '" + e.getMessage() + "'.");
        final MessageHeaders messageHeaders = message.getHeaders();
        if (messageHeaders.containsKey(BusinessIdentifiers.SI_HDR_SIMULATION_ID)) {
          final Long simulationId = (Long) messageHeaders.get(BusinessIdentifiers.SI_HDR_SIMULATION_ID);
          final String rootCauseErrorMessage = ExceptionUtils.getRootCauseMessage(e);
          String messageKey = null;
          if (appManagerWSExceptionCodes.containsKey(rootCauseErrorMessage)) {
            messageKey = appManagerWSExceptionCodes.get(rootCauseErrorMessage);
            jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.WARN,
                                                                 messageKey, new String[] {},
                                                                 simulationId).build());
          } else {
            e.printStackTrace();
            jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.WARN,
                                                                 MessageKey.BUSINESS_MANAGER_UNRECOGNISED_PROBLEM.getBundleIdentifier(),
                                                                 new String[] { rootCauseErrorMessage },
                                                                 simulationId).build());
          }
        } else {
          e.printStackTrace();
          final String warnMessage = "TODO : No SI message header containing simulation id.";
          log.warn("~handleJobMessage() : " + warnMessage);
          System.out.println(warnMessage);
        }
      }
    } else {
      log.trace("~handleJobMessages() : No message received.");
    }
  }

  /**
   * @param appManagerWSExceptionCodes the appManagerWSExceptionCodes to set
   */
  @Value("#{appManagerWSExceptionCodes}")
  public void setAppManagerWSExceptionCodes(final Map<String, String> appManagerWSExceptionCodes) {
    this.appManagerWSExceptionCodes = appManagerWSExceptionCodes;
  }

  /**
   * Assign the input channel.
   * 
   * @param inputChannel Input channel to assign.
   */
  @Value("#{appManager_channel_toJobQueue}")
  public void setInputChannel(final QueueChannel inputChannel) {
    this.inputChannel = inputChannel;
  }

  /**
   * Assign the output channel.
   * 
   * @param outputChannel Output channel to assign.
   */
  @Value("#{appManager_channel_toOutboundGateway}")
  public void setOutputChannel(final DirectChannel outputChannel) {
    this.outputChannel = outputChannel;
  }
}