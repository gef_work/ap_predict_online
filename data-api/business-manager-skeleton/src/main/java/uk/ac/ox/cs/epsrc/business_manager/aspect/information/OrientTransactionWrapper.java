/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.aspect.information;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.business_manager.dao.information.OrientDAOImpl;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentPool;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.tx.OTransaction;

/**
 * Wraps write instructions to the Orient db with a transaction.
 *
 * @author geoff
 */
@Component
@Aspect
// See OrientArgChecker - This class starts a transaction after the args have been checked for validity.
@Order(2)
public class OrientTransactionWrapper {

  @Pointcut("execution(public * purge(..))")
  private void purgeOperation() {}

  @Pointcut("execution(public * create*(..))")
  private void anyPublicCreateOperation() {}

  @Pointcut("execution(public * retrieve*(..))")
  private void anyPublicRetrieveOperation() {}

  @Pointcut("execution(public * update*(..))")
  private void anyPublicUpdateOperation() {}

  @Pointcut("execution(public * write*(..))")
  private void anyPublicWriteOperation() {}

  // Use of the  + (plus) sign matches class (or a class that extends it)
  @Pointcut("within(uk.ac.ox.cs.epsrc.business_manager.dao.information.InformationDAO+)")
  private void inOrientProvenanceDAOImpl() {} 

  private static final Log log = LogFactory.getLog(OrientTransactionWrapper.class);

  /**
   * Create transaction for all r/w methods in the DAO.
   * 
   * @param pjp Wrapped method's join point.
   * @return Wrapped method's return object.
   * @throws Throwable
   */
  @Around("(anyPublicRetrieveOperation() || anyPublicUpdateOperation() ||" +
          " anyPublicWriteOperation() || anyPublicCreateOperation() || purgeOperation()) && " +
          "inOrientProvenanceDAOImpl()")
  private Object methodTransaction(final ProceedingJoinPoint pjp) throws Throwable {
    /* See : https://github.com/orientechnologies/orientdb/wiki/Java-Multi-Threading regarding 
             threadlocal and acquire.
       Acquire a database connection from the pool - if pool empty, caller will wait! */
    final ODatabaseDocumentTx db = ODatabaseDocumentPool.global().acquire(OrientDAOImpl.ORIENT_INAME,
                                                                          OrientDAOImpl.ORIENT_IUSERNAME,
                                                                          OrientDAOImpl.ORIENT_IUSERPASSWORD);
    Object methodReturned = null;
    String exceptionMessage = null;
    try {
      // Begin a new transaction
      db.begin(OTransaction.TXTYPE.OPTIMISTIC);
      // Call the original method.
      methodReturned = pjp.proceed();
      // After calling do the commit.
      db.commit();
    } catch (Exception e) {
      exceptionMessage = e.getMessage();
      db.rollback();
      log.warn("~methodTransaction() : Exception '" + e.getMessage() + "'.");
      e.printStackTrace();
    } finally {
      // Closes an opened database
      ODatabaseDocumentPool.global().release(db);
    }
    if (exceptionMessage != null) {
      throw new RuntimeException("Exception during Orient write process of: " + exceptionMessage);
    }
    return methodReturned;
  }
}