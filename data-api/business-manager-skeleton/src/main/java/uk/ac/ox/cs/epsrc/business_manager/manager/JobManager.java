/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.manager;

import java.util.List;
import java.util.Set;

import uk.ac.ox.cs.epsrc.business_manager.entity.Job;
import uk.ac.ox.cs.epsrc.business_manager.value.object.PerJobConcentrationResultsVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedDataVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.JobDiagnosticsVO;

/**
 * Job management interface.
 *
 * @author geoff
 */
public interface JobManager {

  /**
   * Assign an app manager identifier to the job.
   * 
   * @param jobId Job to update.
   * @param appManagerId App manager identifier.
   */
  public void assignAppManagerIdToJob(long jobId, String appManagerId);

  /**
   * Create and store jobs from processed data.
   * 
   * @param simulationId Simulation identifier.
   * @param processedData Processed site data.
   * @return Collection of pre-invocation objects.
   */
  public Set<Job> createAndStore(long simulationId, ProcessedDataVO processedData);

  /**
   * Determine if all jobs for a simulation have been completed.
   * 
   * @param simulationId Simulation identifier.
   * @return {@code true} if all jobs completed, otherwise {@code false}.
   */
  public boolean determineJobsCompletedForSimulation(long simulationId);

  /**
   * Retrieve the jobs associated with the simulation.
   * 
   * @param simulationId Simulation identifier.
   * @return Jobs associated with the simulation (or empty collection if non
   *         found).
   */
  public List<Job> findJobsBySimulationId(long simulationId);

  /**
   * Persist the job's diagnostics data.
   * 
   * @param appManagerId App manager identifier.
   * @param info Job information (optional).
   * @param output Job output (optional).
   * @throws IllegalArgumentException On invalid app manager id value.
   */
  public void persistDiagnostics(String appManagerId, String info, String output)
                                 throws IllegalArgumentException;

  /**
   * Persist the job's results data.
   * 
   * @param appManagerId App manager identifier.
   * @param messages Job messages (optional).
   * @param deltaAPD90PercentileNames Delta APD90 percentile names.
   * @param perJobConcentrationResultsVO Per-concentration results.
   * @return Job which the results are derived from, or null if no Job found.
   * @throws IllegalArgumentException On invalid app manager id value.
   */
  public Job persistResults(String appManagerId, String messages,
                            String deltaAPD90PercentileNames,
                            Set<PerJobConcentrationResultsVO> perJobConcentrationResultsVO)
                            throws IllegalArgumentException;

  /**
   * Remove all the jobs (and any dependent entites, as determined by
   * {@code CascadeType.REMOVE}) associated with the simulation, and where there
   * appears to be jobs still running, retrieve the app manager identities of
   * the latter.
   * 
   * @param simulationId Simulation identifier.
   * @return Collection of app manager identities of jobs which appear to be
   *         still running (i.e. no results yet persisted), or empty collection
   *         if none appear to be running.
   */
  public Set<String> purgeSimulationJobData(long simulationId);

  /**
   * Retrieve job diagnostics information.
   * 
   * @param jobId Job identifier.
   * @return Job diagnostics information -- potentially with no data, but not
   *         {@code null}.
   */
  public JobDiagnosticsVO retrieveDiagnostics(long jobId);
}