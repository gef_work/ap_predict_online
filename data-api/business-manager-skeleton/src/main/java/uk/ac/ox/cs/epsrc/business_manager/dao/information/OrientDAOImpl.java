/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.dao.information;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager.INFORMATION_FORMAT;
import uk.ac.ox.cs.epsrc.business_manager.value.object.InformationVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

import com.orientechnologies.orient.core.config.OGlobalConfiguration;
import com.orientechnologies.orient.core.db.ODatabaseRecordThreadLocal;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentPool;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.db.record.ODatabaseRecordTx;
import com.orientechnologies.orient.core.exception.OStorageException;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OSchema;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.query.OSQLSynchQuery;

/**
 * Provenance data access object implementation.
 * <p>
 * NOTE : This class should be wrapped by AOP classes, e.g. {@link uk.ac.ox.cs.epsrc.business_manager.aspect.information.OrientArgChecker}
 * which perform transaction management and argument validation.
 *
 * @author geoff
 */
@Component(BusinessIdentifiers.COMPONENT_ORIENT_DAO)
public class OrientDAOImpl implements InformationDAO {
  /*
   * Provenance persistence
   */
  private static final String classFundamental = "Fundamental";

  private static final String fieldNameCompoundIdentifier = "compoundIdentifier";
  private static final String fieldNameSimulationId = "simulationId";

  private static final String fieldNameProvenance = "provenance";
  private static final String fieldNameLevel = "level";
  private static final String fieldNameText = "text";
  private static final String fieldNameArgs = "args";

  /*
   * Progress persistence
   */
  private static final String classProgressAll = "All";
  private static final String classProgressJob = "Job";

  private static final String fieldNameProgress = "progress";
  private static final String fieldNameJob = "job";

  private static final String fieldNameTimestamp = "timestamp";

  private static final String fieldNameJobId = "jobId";
  private static final String fieldNameGroupName = "groupName";
  private static final String fieldNameGroupLevel = "groupLevel";
  private static final String fieldNamePacingFrequency = "pacingFrequency";

  public static final String ORIENT_INAME = "memory:live";
  public static final String ORIENT_IUSERNAME = "admin";
  public static final String ORIENT_IUSERPASSWORD = "admin";

  private static final String regex = "\\\"#\\d+:\\d+\\\"";
  private static final Pattern p = Pattern.compile(regex);

  private static final Log log = LogFactory.getLog(OrientDAOImpl.class);

  @PostConstruct
  protected void initialiseOrient() {
    final String methodName = prefConcat("initialiseOrient()");
    log.trace(methodName.concat(" : Invoked."));
    OGlobalConfiguration.LOG_CONSOLE_LEVEL.setValue("info");
    OGlobalConfiguration.LOG_FILE_LEVEL.setValue("info");

    // Default database pool maximum size
    OGlobalConfiguration.DB_POOL_MAX.setValue(200);
    // Time in ms of delay to check pending closed connections (checks for dirty conns and frees them)
    OGlobalConfiguration.SERVER_CHANNEL_CLEAN_DELAY.setValue(1000);
    // Dumps the configuration at application startup
    OGlobalConfiguration.ENVIRONMENT_DUMP_CFG_AT_STARTUP.setValue(true);

    ODatabaseDocumentTx database = null;
    try {
      try {
        log.trace(methodName.concat(" : About to acquire global [1]."));
        database = ODatabaseDocumentPool.global().acquire(ORIENT_INAME, ORIENT_IUSERNAME,
                                                          ORIENT_IUSERPASSWORD);
      } catch (OStorageException e) {
        log.trace(methodName.concat(" : Exception '" + e.getMessage() + "'."));
        log.trace(methodName.concat(" : Creating new database '" + ORIENT_INAME + "'."));
        new ODatabaseDocumentTx(ORIENT_INAME).create();
        log.trace(methodName.concat(" : About to acquire global [2]."));
        database = ODatabaseDocumentPool.global().acquire(ORIENT_INAME, ORIENT_IUSERNAME,
                                                          ORIENT_IUSERPASSWORD);
        final OSchema schema = database.getMetadata().getSchema();

        final OClass fundamentalClass = schema.createClass(classFundamental);

        fundamentalClass.createProperty(fieldNameCompoundIdentifier, OType.STRING).
                         setMandatory(true).setMin("1");
        fundamentalClass.createProperty(fieldNameSimulationId, OType.LONG);
        fundamentalClass.createProperty(fieldNameProvenance, OType.EMBEDDEDLIST);

        final OClass allProgressClass = schema.createClass(classProgressAll);
        final OClass jobProgressClass = schema.createClass(classProgressJob);

        // Represents all progress.
        allProgressClass.createProperty(fieldNameCompoundIdentifier, OType.STRING);
        allProgressClass.createProperty(fieldNameSimulationId, OType.LONG)
                        .setMandatory(true).setMin("1");
        allProgressClass.createProperty(fieldNameProgress, OType.EMBEDDEDLIST);
        allProgressClass.createProperty(fieldNameJob, OType.LINKSET, jobProgressClass);

        // Represents simulation jobs (usu. an assay grouping and freq object) progress
        jobProgressClass.createProperty(fieldNameJobId, OType.LONG);
        jobProgressClass.createProperty(fieldNameGroupName, OType.STRING);
        jobProgressClass.createProperty(fieldNameGroupLevel, OType.SHORT);
        jobProgressClass.createProperty(fieldNamePacingFrequency, OType.FLOAT);
        jobProgressClass.createProperty(fieldNameProgress, OType.EMBEDDEDLIST);

        schema.save();
      }
    } finally {
      if (database != null) {
        log.trace(methodName.concat(" : Closing database."));
        database.close();
      }
    }
  }

  // Various query strings.
  private static final String fundamentalBySimulationIdQuery;
  private static final String progressAllBySimulationIdQuery;
  private static final String progressJobByJobIdQuery;

  static {
    final String eq = " = :";
    final String selectEverythingFrom = "select * from ";
    final String where = " where ";
    fundamentalBySimulationIdQuery = selectEverythingFrom.concat(classFundamental)
                                     .concat(where)
                                     .concat(fieldNameSimulationId).concat(eq).concat(fieldNameSimulationId);
    progressJobByJobIdQuery = selectEverythingFrom.concat(classProgressJob).concat(where)
                              .concat(fieldNameJobId).concat(eq).concat(fieldNameJobId);
    progressAllBySimulationIdQuery = selectEverythingFrom.concat(classProgressAll)
                                     .concat(where)
                                     .concat(fieldNameSimulationId).concat(eq).concat(fieldNameSimulationId);
  }

  // Create the progress text.
  private ODocument createProgress(final String timestamp, final InformationLevel level,
                                   final String text, final List<String> args) {
    return new ODocument().field(fieldNameTimestamp, timestamp).field(fieldNameLevel, level)
                          .field(fieldNameText, text).field(fieldNameArgs, args, OType.EMBEDDEDLIST);
  }

  // Create the provenance text with args.
  private ODocument createProvenance(final InformationLevel level, final String text,
                                     final List<String> args) {
    final List<String> useArgs = new ArrayList<String>();
    if (args != null) {
      useArgs.addAll(args);
    }
    return new ODocument().field(fieldNameLevel, level).field(fieldNameText, text)
                          .field(fieldNameArgs, useArgs, OType.EMBEDDEDLIST);
  }

  // Create progress collection.
  private List<ODocument> createProgressCollection(final String timestamp,
                                                   final InformationLevel level, final String text,
                                                   final List<String> args) {
    final List<ODocument> newProgress = new ArrayList<ODocument>();
    newProgress.add(createProgress(timestamp, level, text, args));
    return newProgress;
  }

  // Create provenance collection with args.
  private List<ODocument> createProvenanceCollection(final InformationLevel level,
                                                     final String text,
                                                     final List<String> args) {
    final List<ODocument> newProvenances = new ArrayList<ODocument>();
    newProvenances.add(createProvenance(level, text, args));
    return newProvenances;
  }

  // Indicate if there's something representing a surrogate primary key
  private boolean hasValidId(final long value) {
    return (value > 0);
  }

  // Retrieve fundamental progress by system simulation id.
  private List<ODocument> queryFundamentalBySimulationId(final long simulationId) {
    final String methodName = "queryFundamentalBySimulationId()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    final boolean hasSimulationId = hasValidId(simulationId);
    if (!hasSimulationId) {
      final String error = "Simulation id must be provided to retrieve fundamental class objects from OrientDb.";
      log.error(identifyingLogPrefix.concat(error));
      System.out.println(error);
      return new ArrayList<ODocument>();
    }

    final Map<String, Object> params = new HashMap<String, Object>();
    params.put(fieldNameSimulationId, simulationId);
    final List<ODocument> found = new OSQLSynchQuery<ODocument>(fundamentalBySimulationIdQuery)
                                      .execute(params);
    log.trace(identifyingLogPrefix.concat("Found '" + found.size() + "' records."));
    return found;
  }

  // Retrieve the simulation/all progress by system simulation id.
  private List<ODocument> queryProgressAllBySimulationId(final long simulationId) {
    final String methodName = "queryProgressAllBySimulationId()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    final Map<String, Object> params = new HashMap<String, Object>();
    params.put(fieldNameSimulationId,  simulationId);
    final List<ODocument> found = new OSQLSynchQuery<ODocument>(progressAllBySimulationIdQuery)
                                      .execute(params);
    log.trace(identifyingLogPrefix.concat("Found '" + found.size() + "' records."));
    return found;
  }

  // Put quotes around an ORID
  private String quoteORID(final ORID orid) {
    return "\"".concat(orid.toString().concat("\""));
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProgressDAO#updateProgressJob(long, java.util.Date, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List)
   */
  @Override
  public void updateProgressJob(final long jobId, final Date timestamp, final InformationLevel level,
                                final String text, final List<String> args)
                                throws IllegalArgumentException, IllegalStateException {
    final String methodName = "updateProgressJob()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + jobId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat("Invoked."));
    if (!hasValidId(jobId) || timestamp == null || level == null || StringUtils.isBlank(text)) {
      final List<String> messages = new ArrayList<String>();
      if (!hasValidId(jobId)) messages.add("jobId '" + jobId + "'");
      if (timestamp == null) messages.add("timestamp '" + timestamp + "'");
      if (level == null) messages.add("level '" + level + "'");
      if (StringUtils.isBlank(text)) messages.add("text '" + text + "'");
      throwArgError(methodName, identifying,
                    "Invalid value in: ".concat(StringUtils.join(messages, ": ")));
    }

    final Map<String, Object> params = new HashMap<String, Object>();
    params.put(fieldNameJobId, jobId);
    final List<ODocument> found = new OSQLSynchQuery<ODocument>(progressJobByJobIdQuery).execute(params);
    final int foundCount = found.size();
    log.trace(identifyingLogPrefix.concat("Found '" + foundCount + "' records."));

    if (foundCount == 0) {
      throwStateError(methodName, identifying,
                      "No job progress data structure exists for job id '" + jobId + "'.");
    }
    if (foundCount > 1) {
      throwStateError(methodName, identifying,
                      "More than one job progress data structure exists for job id '" + jobId + "'.");
    }

    final ODocument existingJobProgress = found.iterator().next();
    final String timestampStr = Long.valueOf(timestamp.getTime()).toString();
    // Append the latest progress text to existing/nested texts. 
    appendProgress(existingJobProgress, timestampStr, level, text, args);
    log.trace(identifyingLogPrefix.concat("Job progress JSON '" + existingJobProgress.toJSON() + "'."));
  }

  // Add a progress to existing.
  private void appendProgress(final ODocument existingDocument, final String timestamp,
                              final InformationLevel level, final String text,
                              final List<String> args) {
    final List<ODocument> progress = existingDocument.field(fieldNameProgress);
    progress.add(createProgress(timestamp, level, text, args));
    existingDocument.save();
  }

  // Add a provenance to existing.
  private void appendProvenance(final ODocument existingDocument, final InformationLevel level,
                                final String text, final List<String> args) {
    final List<ODocument> provenances = existingDocument.field(fieldNameProvenance);
    provenances.add(createProvenance(level, text, args));
    existingDocument.save();
  }

  private String prefConcat(final String methodName) {
    return "~odb-".concat(methodName);
  }

  
  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.InformationDAO#purge(long)
   */
  @Override
  public void purge(final long simulationId) {
    final String methodName = "purge()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat("Invoked."));

    purge("progress", simulationId, queryProgressAllBySimulationId(simulationId));
    purge("provenance", simulationId, queryFundamentalBySimulationId(simulationId));
  }

  // Common code between purging different types of information.
  private void purge(final String informationType, final long simulationId,
                     final List<ODocument> information) {
    final String methodName = "purge(x3)";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    if (!information.isEmpty()) {
      final int informationCount = information.size();
      if (informationCount == 1) {
        final ODocument progressAll = information.get(0);
        for (final ODocument document : traversingQuery(progressAll.getIdentity())) {
          log.trace(identifyingLogPrefix.concat("Removing " + informationType + " '" + document.toJSON() + "'."));
          document.delete();
        }
      } else {
        throwStateError(methodName, identifying,
                        "There were '" + informationCount + "' " + informationType + " data documents for simulation id '" + simulationId + "'.");
      }
    }
  }

  // Build up the JSON representation of the traversed document data.
  private String recursiveJSONBuilder(String currentJSON, final Map<String, String> nestedJSON) {
    final Matcher matcher = p.matcher(currentJSON);
    // Are there any ORID identifier-like sequences, e.g. "#23:1" indicating linked documents?
    boolean ignoringParentORID = true;
    while (matcher.find()) {
      // Yes there was...
      // But we're not interested in the first one because it'll be the current @rid identifier!
      if (ignoringParentORID) {
        ignoringParentORID = false;
        continue;
      }

      // Fish out the matched "#23:1" (note the quotes)
      final String matchedORID = matcher.group();
      if (nestedJSON.containsKey(matchedORID)) {
        // Find the json string of the orient document to replace the "#23:1"
        final String jsonToBeInjected = nestedJSON.get(matchedORID);
        // First need to check the JSON toBeInjected itself for identifier-like sequences!
        currentJSON = currentJSON.replace(matchedORID, recursiveJSONBuilder(jsonToBeInjected, nestedJSON));
      }
    }

    return currentJSON;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProgressDAO#retrieveProgress(long, uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager.INFORMATION_FORMAT)
   */
  @Override
  public InformationVO retrieveProgress(final long simulationId, final INFORMATION_FORMAT format)
                                        throws IllegalArgumentException, IllegalStateException {
    final String methodName = "retrieveProgress()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat("Invoked."));

    final List<ODocument> progressAlls = queryProgressAllBySimulationId(simulationId);
    final int progressAllsCount = progressAlls.size();

    if (progressAllsCount == 0) {
      log.trace(identifyingLogPrefix.concat("No all progress structures found."));
      return new InformationVO(simulationId, InformationVO.EMPTY_ASSOC_ARRAY);
    }
    if (progressAllsCount > 1) {
      throwStateError(methodName, identifying, "More than one progress structure exists.");
    }

    final ODocument existingAll = progressAlls.iterator().next();
    final ORID existingAllORID = existingAll.getIdentity();

    // Store a collection of document JSONs keyed on a quote ORID value.
    final Map<String, String> connectedDocuments = new HashMap<String, String>();
    String rootJSON = null;
    // Traverse the retrieved documents
    for (final ODocument currentDocument : traversingQuery(existingAllORID)) {
      final ORID currentDocumentORID = currentDocument.getIdentity();
      if (currentDocumentORID != null) {
        final String currentJSON = currentDocument.toJSON();
        if (currentDocumentORID.equals(existingAllORID)) {
          // Set the current document of the simulation/all progress to be the root JSON.
          rootJSON = currentJSON;
        } else {
          // Document ORID different to the simulation/all progress, i.e. job progress 
          final String currentORIDQuoted = quoteORID(currentDocumentORID);
          connectedDocuments.put(currentORIDQuoted, currentJSON);
        }
      }
    }

    final StringBuffer output = new StringBuffer();
    if (rootJSON != null) {
      if (connectedDocuments.isEmpty()) {
        // If there were no job progress documents.
        output.append(rootJSON);
      } else {
        // If job progress documents encountered.
        final String allJSON = recursiveJSONBuilder(rootJSON, connectedDocuments);
        output.append(allJSON);
      }
    }

    return new InformationVO(simulationId, output.toString());
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProvenanceDAO#retrieveProvenance(long, uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager.INFORMATION_FORMAT)
   */
  @Override
  public InformationVO retrieveProvenance(final long simulationId, final INFORMATION_FORMAT format)
                                          throws IllegalArgumentException, IllegalStateException {
    final String methodName = "retrieveProgress()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat("Invoked."));

    // Retrieve the fundamental record which this individual belongs to.
    final List<ODocument> existingFundamentals = queryFundamentalBySimulationId(simulationId);
    assert (existingFundamentals.size() == 1) : "Should only be able to retrieve one fundamental for simulation id '" + simulationId + "'. Retrieved '" + existingFundamentals.size() + "'!";
    final ODocument existingFundamental = existingFundamentals.get(0);
    final ORID existingFundamentalORID = existingFundamental.getIdentity();

    // Store a collection of document JSONs keyed on a quote ORID value.
    final Map<String, String> connectedDocuments = new HashMap<String, String>();
    String rootJSON = null; 
    // Traverse the retrieved documents.
    for (final ODocument document : traversingQuery(existingFundamentalORID)) {
      final ORID orid = document.getIdentity();
      if (orid != null) {
        log.trace(identifyingLogPrefix.concat("Retrieving '" + orid.toString() + "' json of '" + document.toJSON() + "'."));
        final String json = document.toJSON();
        if (orid.equals(existingFundamentalORID)) {
          // Set the current document of the fundamental provenance to be the root JSON.
          rootJSON = json;
        } else {
          // Document ORID different to the fundamental, i.e. child provenance.
          final String currentORIDQuoted = quoteORID(orid);
          connectedDocuments.put(currentORIDQuoted, json);
        }
      }
    }

    final StringBuffer output = new StringBuffer();
    if (rootJSON != null) {
      if (connectedDocuments.isEmpty()) {
        // If there were no provenance documents.
        output.append(rootJSON);
      } else {
        // If provenance documents encountered.
        final String allJSON = recursiveJSONBuilder(rootJSON, connectedDocuments);
        output.append(allJSON);
      }
    }

    return new InformationVO(simulationId, output.toString());
  }

  // Frequently used arg error-throwing.
  private void throwArgError(final String method, final String identifying,
                             final String errorMessage) throws IllegalArgumentException {
    log.error(prefConcat(method).concat(" : ").concat(identifying).concat(" : ").concat(errorMessage));
    throw new IllegalArgumentException(errorMessage);
  }

  // Frequently used state error-throwing.
  private void throwStateError(final String method, final String identifying,
                               final String errorMessage) throws IllegalStateException {
    log.error(prefConcat(method).concat(" : ").concat(identifying).concat(" : ").concat(errorMessage));
    throw new IllegalStateException(errorMessage);
  }

  /*
   * Perform a document traversal based on the ORID value.
   * 
   * Search connected documents to the simulation/all progress document.
   * For example, for a simulation with jobs there would be a parent simulation/all progress
   *   document, as well as a job progress document. The simulation/all progress document would
   *   contain text such as ... "@rid":"#13:0" ... "job":["#14:0"] ... indicating a placeholder
   *   to the ORID identifier array of job documents, one of which would contain  ... "@rid":"#14:0".
   * This returns the parent as well as linked documents.
   * http://www.orientechnologies.com/docs/last/SQL-Traverse.html
   */
  private List<ODocument> traversingQuery(final ORID orid) {
    final String command = "traverse * from " + orid;
    final ODatabaseRecordTx database = (ODatabaseRecordTx) ODatabaseRecordThreadLocal.INSTANCE.get();
    return database.command(new OSQLSynchQuery<ODocument>(command)).execute();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProvenanceDAO#writeFundamental(java.lang.String, long, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List)
   */
  @Override
  public void writeFundamental(final String compoundIdentifier, final long simulationId,
                               final InformationLevel level, final String text,
                               final List<String> args) throws IllegalArgumentException,
                                                               IllegalStateException {
    final String methodName = "writeFundamental()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + compoundIdentifier + "]");
    identifyingSB.append("[" + simulationId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat(" : Invoked."));
    if (!hasValidId(simulationId) || level == null || text == null) {
      final List<String> messages = new ArrayList<String>();
      // No compound identifier is valid if we're going to append data
      if (!hasValidId(simulationId)) messages.add("Simulation id '" + simulationId + "'.");
      if (level == null) messages.add("Information level is '" + level + "'.");
      if (text == null) messages.add("Text is '" + text + "'.");
      throwArgError(methodName, identifying,
                    "Invalid value in: ".concat(StringUtils.join(messages, ": ")));
    }

    final List<ODocument> results = queryFundamentalBySimulationId(simulationId);
    final int resultCount = results.size();
    switch (resultCount) {
      case 0 :
        log.trace(identifyingLogPrefix.concat(" : Empty results."));
        if (StringUtils.isBlank(compoundIdentifier)) {
          throwArgError(methodName, identifying,
                        "Attempt to create a new '" + classFundamental + "' document denied as we need both compound identifier and simulation id.");
        } else {
          // We need both properties to create a new record.
          final ODocument newFundamental = new ODocument(classFundamental);
          newFundamental.field(fieldNameCompoundIdentifier, compoundIdentifier);
          newFundamental.field(fieldNameSimulationId, simulationId);
          newFundamental.field(fieldNameProvenance, createProvenanceCollection(level, text,
                                                                                       args));
          newFundamental.save();
        }
        break;
      case 1 :
        appendProvenance(results.get(0), level, text, args);
        break;
      default :
        throwStateError(methodName, identifying,
                        "Unexpected count of '" + resultCount  + "' records for class '" + classFundamental + "'.");
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProgressDAO#updateProgressAll(long, java.util.Date, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List)
   */
  @Override
  public void updateProgressAll(final long simulationId, final Date timestamp,
                                final InformationLevel level, final String text,
                                final List<String> args) throws IllegalArgumentException,
                                                                IllegalStateException,
                                                                UnsupportedOperationException {
    final String methodName = "updateProgressAll()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat("Invoked."));
    if (!hasValidId(simulationId) || timestamp == null || level == null ||
        StringUtils.isBlank(text)) {
      final List<String> messages = new ArrayList<String>();
      if (!hasValidId(simulationId)) messages.add("simulationId '" + simulationId + "'");
      if (timestamp == null) messages.add("timestamp '" + timestamp + "'");
      if (level == null) messages.add("level '" + level + "'");
      if (StringUtils.isBlank(text)) messages.add("text '" + text + "'");
      throwArgError(methodName, identifying,
                    "Invalid value in: ".concat(StringUtils.join(messages, ": ")));
    }

    final List<ODocument> results = queryProgressAllBySimulationId(simulationId);
    final int resultCount = results.size();
    final String timestampStr = Long.valueOf(timestamp.getTime()).toString();

    if (resultCount == 0) {
      throwStateError(methodName, identifying, "No simulation/all progress structure found.");
    }
    if (resultCount > 1) {
      throwStateError(methodName, identifying,
                      "More than one simulation/all progress structure found.");
    }
    appendProgress(results.iterator().next(), timestampStr, level, text, args);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProgressDAO#createProgressAll(java.lang.String, long, java.util.Date, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List)
   */
  @Override
  public void createProgressAll(final String compoundIdentifier, final long simulationId,
                                final Date timestamp, final InformationLevel level, final String text,
                                final List<String> args) throws IllegalArgumentException,
                                                                IllegalStateException {
    final String methodName = "createProgressAll()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + compoundIdentifier + "]");
    identifyingSB.append("[" + simulationId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat("Invoked."));
    if (StringUtils.isBlank(compoundIdentifier) || !hasValidId(simulationId) || timestamp == null ||
        level == null || StringUtils.isBlank(text)) {
      final List<String> messages = new ArrayList<String>();
      if (!hasValidId(simulationId)) messages.add("simulationId '" + simulationId + "'");
      if (timestamp == null) messages.add("timestamp '" + timestamp + "'");
      if (level == null) messages.add("level '" + level + "'");
      if (StringUtils.isBlank(text)) messages.add("text '" + text + "'");
      throwArgError(methodName, identifying,
                    "Invalid value in: ".concat(StringUtils.join(messages, ": ")));
    }

    final List<ODocument> results = queryProgressAllBySimulationId(simulationId);

    final int resultCount = results.size();
    final String timestampStr = Long.valueOf(timestamp.getTime()).toString();

    if (resultCount == 1) {
      throwStateError(methodName, identifying, "A simulation/all progress structure has been found.");
    }
    if (resultCount > 1) {
      throwStateError(methodName, identifying,
                      "More than one simulation/all progress structure found.");
    }
    log.trace(identifyingLogPrefix.concat("Empty results."));

    final ODocument newAll = new ODocument(classProgressAll);
    newAll.field(fieldNameCompoundIdentifier, compoundIdentifier);
    newAll.field(fieldNameSimulationId, simulationId);
    newAll.field(fieldNameProgress, createProgressCollection(timestampStr, level, text, args));
    newAll.save();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.ProgressDAO#createProgressJob(long, long, java.lang.String, short, float, java.util.Date, uk.ac.ox.cs.nc3rs.business_manager.api.value.InformationLevel, java.lang.String, java.util.List)
   */
  @Override
  public void createProgressJob(final long simulationId, final long jobId,
                                final String groupName, final short groupLevel,
                                final float pacingFrequency, final Date timestamp,
                                final InformationLevel level, final String text,
                                final List<String> args)
                                throws IllegalArgumentException, IllegalStateException {
    final String methodName = "createProgressJob()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    identifyingSB.append("[" + jobId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat("Invoked."));
    if (!hasValidId(simulationId) || !hasValidId(jobId) || StringUtils.isBlank(groupName) ||
        groupLevel < 0 || pacingFrequency < 0.0 || timestamp == null || level == null ||
            StringUtils.isBlank(text)) {
      final List<String> messages = new ArrayList<String>();
      if (!hasValidId(simulationId)) messages.add("simulationId '" + simulationId + "'");
      if (!hasValidId(jobId)) messages.add("jobId '" + jobId + "'");
      if (StringUtils.isBlank(groupName)) messages.add("groupName '" + groupName + "'");
      if (groupLevel < 0) messages.add("groupLevel '" + groupLevel + "'");
      if (pacingFrequency < 0.0) messages.add("pacingFrequency '" + pacingFrequency + "'");
      if (timestamp == null) messages.add("timestamp '" + timestamp + "'");
      if (level == null) messages.add("level '" + level + "'");
      if (StringUtils.isBlank(text)) messages.add("text '" + text + "'");
      throwArgError(methodName, identifying,
                    "Invalid value in: ".concat(StringUtils.join(messages, ": ")));
    }

    // Find simulation progress structure (should only be one!) to append new job progress to.
    final List<ODocument> results = queryProgressAllBySimulationId(simulationId);
    final int resultCount = results.size();

    if (resultCount == 0) {
      throwStateError(methodName, identifying,
                      "No simulation/all progress structure found.");
    }
    if (resultCount > 1) {
      throwStateError(methodName, identifying,
                      "More than one simulation/all progress structure found.");
    }

    // Create new job progress doc
    final ODocument newJob = new ODocument(classProgressJob);
    newJob.field(fieldNameJobId, jobId);
    newJob.field(fieldNameGroupName, groupName);
    newJob.field(fieldNameGroupLevel, groupLevel);
    newJob.field(fieldNamePacingFrequency, pacingFrequency);
    newJob.field(fieldNameProgress, createProgressCollection(Long.valueOf(timestamp.getTime()).toString(),
                                                             level, text, args));
    newJob.save();

    final ODocument existingAllProgress = results.get(0);
    final Set<ORID> existingJobClassIds = existingAllProgress.field(fieldNameJob);
    // create the link from the progress for the simulation to the new per-job progress.
    if (existingJobClassIds == null) {
      log.trace(identifyingLogPrefix.concat("Creating first job progress record link."));
      final Set<ORID> newJobClassIds = new HashSet<ORID>();
      newJobClassIds.add(newJob.getIdentity());
      existingAllProgress.field(fieldNameJob, newJobClassIds);
    } else {
      log.trace(identifyingLogPrefix.concat("Appending job to existing links."));
      existingJobClassIds.add(newJob.getIdentity());
    }
    existingAllProgress.save();

    log.trace(identifyingLogPrefix.concat("Job id#" + jobId + " JSON '" + newJob.toJSON() + "'."));
  }

  @PreDestroy
  protected void closeOrient() {
    log.trace("~odb-closeOrient() : Closing pool.");
    ODatabaseDocumentPool.global().close();
  }

  protected void dropOrient() {
    log.trace("~odb-dropOrient() : Dropping Orient.");
    final ODatabaseDocumentTx database = ODatabaseDocumentPool.global().acquire(ORIENT_INAME,
                                                                                ORIENT_IUSERNAME,
                                                                                ORIENT_IUSERPASSWORD);
    database.drop();
  }
}
