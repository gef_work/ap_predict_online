/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.information.provenance;

import org.apache.commons.lang3.StringUtils;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Fundamental/Generic provenance data.
 *
 * @author geoff
 */
public class FundamentalProvenance extends AbstractProvenance {

  private static final long serialVersionUID = 9078532707480594583L;

  private final String compoundIdentifier;

  public static class Builder extends AbstractProvenance.Builder {
    // Optional parameters
    private String compoundIdentifier = null;

    /**
     * Initialising constructor with minimum data.
     * <p>
     * If you're creating a new {@code FundamentalProvenance} object then you must chain the
     * {@link #onCreation(String)} operation.
     * 
     * @param level Provenance level.
     * @param text Provenance text.
     * @param simulationId Simulation Identifier.
     * @throws IllegalArgumentException See {@code super} constructor.
     */
    public Builder(final InformationLevel level, final String text, final Long simulationId)
                   throws IllegalArgumentException {
      super(level, text, simulationId);
    }

    /**
     * Initialising constructor with minimum data.
     * 
     * @param level Provenance level.
     * @param text Provenance text.
     * @param args Provenance args.
     * @param simulationId Simulation Identifier.
     * @throws IllegalArgumentException See {@code super} constructor.
     */
    public Builder(final InformationLevel level, final String text, final String[] args,
                   final Long simulationId) throws IllegalArgumentException {
      super(level, text, args, simulationId);
    }

    /**
     * Used when creating a {@code FundamentalProvenance} for the first time for a simulation.
     * 
     * @param compoundIdentifier Compound identifier.
     * @return Builder.
     * @throws IllegalArgumentException If {@link #compoundIdentifier} is blank.
     */
    public Builder onCreation(final String compoundIdentifier)
                              throws IllegalArgumentException {
      if (StringUtils.isBlank(compoundIdentifier)) {
        throw new IllegalArgumentException("A compound identifier must be provided on fundamental provenance creation.");
      }
      this.compoundIdentifier = compoundIdentifier;
      return this;
    }

    /**
     * Instruct the builder to build!
     * 
     * @return Newly built provenance data.
     */
    public FundamentalProvenance build() throws IllegalStateException {
      return new FundamentalProvenance(this);
    }
  }

  private FundamentalProvenance(final Builder builder) {
    super(builder);
    compoundIdentifier = builder.compoundIdentifier;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "FundamentalProvenance [compoundIdentifier=" + compoundIdentifier
        + "]";
  }

  /**
   * Retrieve the compound identifier.
   * 
   * @return The compound identifier, or {@code null} if not assigned.
   */
  public String getCompoundIdentifier() {
    return compoundIdentifier;
  }
}