/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.manager;

import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.Provenance;
import uk.ac.ox.cs.epsrc.business_manager.value.object.InformationVO;

/**
 * Interface to information (e.g. provenance, progress) activities.
 *
 * @author geoff
 */
public interface InformationManager {

  /**
   * Format of provenance output.
   */
  enum INFORMATION_FORMAT {
    JSON
  }

  /**
   * Purge information data from the system.
   * 
   * @param simulationId Simulation identifier.
   */
  void purge(long simulationId);

  /**
   * Record the progress information.
   * 
   * @param progress Progress information.
   */
  void recordProgress(Progress progress);

  /**
   * Record the provenance information.
   * 
   * @param provenance Provenance information.
   */
  void recordProvenance(Provenance provenance);

  /**
   * Retrieve progress data by simulation identifier. Note that this progress data is persisted
   * BusinessManager progress data, not AppManager WS progress data.
   * 
   * @param simulationId Simulation identifier. 
   * @return Progress data (or <code>null</code> if no data).
   */
  InformationVO retrieveProgress(long simulationId);

  /**
   * Retrieve provenance data by simulation identifier.
   * 
   * @param simulationId Simulation identifier. 
   * @return Provenance data (or <code>null</code> if no data).
   */
  InformationVO retrieveProvenance(long simulationId);

}