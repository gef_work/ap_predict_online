/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.aspect.information;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager.INFORMATION_FORMAT;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Test the args being passed to the OrientDAO implementation.
 *
 * @author geoff
 */
@Component
@Aspect
//See OrientTransactionWrapper - This class checks that the args are valid before starting a transaction.
@Order(2)
public class OrientArgChecker {

  @Pointcut("execution(public * retrieve*(..))")
  private void anyPublicRetrieveOperation() {}

  @Pointcut("execution(public * update*(..))")
  private void anyPublicUpdateOperation() {}

  // Use of the  + (plus) sign matches class (or a class that extends it)
  @Pointcut("within(uk.ac.ox.cs.epsrc.business_manager.dao.information.InformationDAO+)")
  private void inOrientProvenanceDAOImpl() {} 

  private static final Log log = LogFactory.getLog(OrientArgChecker.class);

  /**
   * Test the args passed to the retrieve operations to see if they are invalid.
   * 
   * @param joinPoint Intercepted method.
   * @throws IllegalArgumentException If either arg has an invalid value.
   */
  @Before("(anyPublicRetrieveOperation()) && inOrientProvenanceDAOImpl()")
  public void checkRetrieveMethodArgs(final JoinPoint joinPoint) throws IllegalArgumentException {
    final Object[] joinPointArgs = joinPoint.getArgs();

    final List<String> messages = new ArrayList<String>();

    for (int argIdx = 0; argIdx < joinPointArgs.length; argIdx++) {
      final Object joinPointObject = joinPointArgs[argIdx];
      switch (argIdx) {
        case 0 :
          final Long simulationId = (Long) joinPointObject;
          if (!hasValidId(simulationId)) {
            messages.add("simulationId '" + simulationId + "'");
          }
          break;
        case 1 :
          final INFORMATION_FORMAT format = (INFORMATION_FORMAT) joinPointObject;
          if (format == null) {
            messages.add("format '" + format + "'");
          }
          break;
        default :
          break;
      }
    }

    if (!messages.isEmpty()) {
      final String errorMessage = "Invalid value in ".concat(StringUtils.join(messages, ": "));
      log.error("~checkRetrieveMethodArgs() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }
  }

  /**
   * Test the first three args passed to the update operations to see if they are invalid.
   * 
   * @param joinPoint Intercepted method.
   * @throws IllegalArgumentException If either arg has an invalid value.
   */
  @Before("(anyPublicUpdateOperation()) && inOrientProvenanceDAOImpl()")
  public void checkUpdateMethodArgs(final JoinPoint joinPoint) throws IllegalArgumentException {
    final Object[] joinPointArgs = joinPoint.getArgs();

    final List<String> messages = new ArrayList<String>();

    for (int argIdx = 0; argIdx < joinPointArgs.length; argIdx++) {
      final Object joinPointObject = joinPointArgs[argIdx];
      switch (argIdx) {
        case 0 :
          final Long simulationId = (Long) joinPointObject;
          if (!hasValidId(simulationId)) {
            messages.add("Simulation id '" + simulationId + "'");
          }
          break;
        case 1 :
          if (joinPointObject instanceof String) {
            // Only test the provenance, not the progress (as they're timestamps)
            final String identifier = (String) joinPointObject;
            if (!hasStringValue(identifier)) {
              messages.add("Identifier  '" + identifier + "'");
            }
          }
          break;
        case 2 :
          final InformationLevel informationLevel = (InformationLevel) joinPointObject;
          if (informationLevel == null) {
            messages.add("Information level '" + informationLevel + "'");
          }
          break;
        case 3 :
          final String text = (String) joinPointObject;
          if (!hasStringValue(text)) {
            messages.add("Text  '" + text + "'");
          }
          break;
        default :
          break;
      }
    }

    if (!messages.isEmpty()) {
      final String errorMessage = "Invalid value in ".concat(StringUtils.join(messages, ": "));
      log.error("~checkUpdateMethodArgs() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }
  }

  // Indicate if there's something representing a string with content.
  private boolean hasStringValue(final String value) {
    return (value != null && value.trim().length() > 0);
  }

  // Indicate if there's something representing a surrogate primary key
  private boolean hasValidId(final Long value) {
    return (value != null && value > 0);
  }
}