/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.business_manager.request_validator;

import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;

import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProcessSimulationsRequest;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidCompoundIdentifierException;
import uk.ac.ox.cs.nc3rs.business_manager.api.request.validator.CompoundIdentifiersValidator;

/**
 * Ordered collection of validators to verify content of an incoming request to process
 * simulations.<br>
 * Components implementing {@link CompoundIdentifiersValidator} are Spring-injected and arranged in
 * an {@link org.springframework.core.Ordered} manner, e.g. {@link FundamentalCompoundIdentifiersValidator}. 
 *
 * @author geoff
 */
public class SimulationsRequestValidatorChain {

  // Find all Spring-configured CompoundIdentifiersValidator objects and inject them.
  @Autowired
  private List<CompoundIdentifiersValidator> compoundIdentifiersValidators;

  private static final Log log = LogFactory.getLog(SimulationsRequestValidatorChain.class);

  @PostConstruct
  private void initialiseChainSort() {
    Collections.sort(compoundIdentifiersValidators, AnnotationAwareOrderComparator.INSTANCE);
  }

  /**
   * Ordered validation of the simulations request.
   * 
   * @param processSimulationsRequest Non-null incoming simulations request.
   * @throws IllegalArgumentException If null request object received.
   * @throws InvalidCompoundIdentifierException If there request was invalid by virtue of invalid compound name(s).
   */
  public void simulationsRequestValidation(final ProcessSimulationsRequest processSimulationsRequest)
                                           throws IllegalArgumentException,
                                                  InvalidCompoundIdentifierException {
    log.debug("~simulationsRequestValidation() : Invoked.");
    if (processSimulationsRequest == null) {
      throw new IllegalArgumentException("Invalid attempt to invoke process simulations request validation using a null request object.");
    }

    if (compoundIdentifiersValidators != null) {
      for (final CompoundIdentifiersValidator compoundIdentifiersValidator : compoundIdentifiersValidators) {
        compoundIdentifiersValidator.validate(processSimulationsRequest);
      }
    }
  }
}