/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.business_manager;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager;
import uk.ac.ox.cs.epsrc.business_manager.service.SimulationService;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedSimulationRequestVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.SimulationRequestVO;
import uk.ac.ox.cs.epsrc.business_manager.ws.business_manager.request_validator.SimulationsRequestValidatorChain;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ObjectFactory;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProcessSimulationsRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProcessSimulationsResponse;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProcessSimulationsResponseStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.SimulationDetail;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.ConflictingRequestException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidCompoundIdentifierException;

/**
 * Process incoming WS JAXB objects representing simulation(s) requests and returns WS JAXB 
 * responses.
 * <p>
 * Calls validation of incoming request objects as well as converts objects from JAXB objects to 
 * internal structures for onward processing via the {@link SimulationManager},
 * converting responses back to JAXB objects for return WS responses.
 *
 * @author geoff
 */
@Component
public class SimulationsRequestProcessor {

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_CONFIGURATION)
  private Configuration configuration;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE)
  private SimulationService simulationService;

  private static SimulationsRequestValidatorChain validatorChain;

  // Makes web service JAXB objects available.
  private static final ObjectFactory objectFactory = new ObjectFactory();

  private static final Log log = LogFactory.getLog(SimulationsRequestProcessor.class);

  /** Default constructor */
  protected SimulationsRequestProcessor() {}

  /**
   * Initialising constructor.
   * 
   * @param simulationsRequestValidatorChain Validator chain to handle validation of incoming request.
   */
  public SimulationsRequestProcessor(final SimulationsRequestValidatorChain simulationsRequestValidatorChain) {
    validatorChain = simulationsRequestValidatorChain; 
  }

  /**
   * Process web service requests for simulations. Transfers the incoming JAXB request object into
   * an internal structure for onward processing.
   * 
   * @param incomingRequest Incoming web service simulation(s) request.
   * @return Response to request.
   * @throws ConflictingRequestException If more than one request for a compound which differ only
   *                                     in a non pIC50 value determining manner, i.e. differs only
   *                                     in forceReRun, reset and/or isInputDataOnly.
   * @throws InvalidCompoundIdentifierException If an invalid compound name is encountered.
   */
  public ProcessSimulationsResponse processSimulationsRequest(final ProcessSimulationsRequest incomingRequest)
                                                              throws ConflictingRequestException,
                                                                     InvalidCompoundIdentifierException {
    log.debug("~processSimulationsRequest() : Invoked.");

    // Make sure there are no invalid compound names in the incoming request
    validatorChain.simulationsRequestValidation(incomingRequest);

    // Make sure no duplicated compound names in incoming request.
    final List<SimulationRequestVO> verifiedSimulationRequests = new ArrayList<SimulationRequestVO>();
    for (final SimulationDetail simulationDetail : incomingRequest.getSimulationDetail()) {
      String compoundIdentifier = simulationDetail.getCompoundIdentifier();
      if (compoundIdentifier != null) {
        compoundIdentifier = compoundIdentifier.trim();
      }

      final SimulationRequestVO verifiedSimulationRequest = simulationService.userInputValidation(compoundIdentifier,
            simulationDetail.isForceReRun(), simulationDetail.isReset(),
            simulationDetail.isAssayGrouping(), simulationDetail.isValueInheriting(),
            simulationDetail.isBetweenGroups(), simulationDetail.isWithinGroups(),
            simulationDetail.getCellMLModelIdentifier(), simulationDetail.getPacingMaxTime(),
            simulationDetail.isInputDataOnly());

      if (verifiedSimulationRequests.contains(verifiedSimulationRequest)) {
        final String infoMessage = "Conflicting request submitted for compound identifier '" + compoundIdentifier + "'.";
        log.info("~processSimulationsRequest() : " + infoMessage);
        throw new ConflictingRequestException(infoMessage);
      } else {
        verifiedSimulationRequests.add(verifiedSimulationRequest);
      }
    }

    final ProcessSimulationsResponse outgoingResponse = objectFactory.createProcessSimulationsResponse();
    final List<ProcessSimulationsResponseStructure> simulationsResponses = outgoingResponse.getProcessSimulationsResponseStructure();

    final List<ProcessedSimulationRequestVO> processedRequests = new ArrayList<ProcessedSimulationRequestVO>(verifiedSimulationRequests.size());
    if (!verifiedSimulationRequests.isEmpty()) {
      processedRequests.addAll(simulationService.processSimulationsRequest(verifiedSimulationRequests));
    }

    for (final ProcessedSimulationRequestVO processedRequest : processedRequests) {
      final String compoundIdentifier = processedRequest.getCompoundIdentifier();
      final long simulationId = processedRequest.getSimulationId();

      final ProcessSimulationsResponseStructure simulationsResponseStructure = objectFactory.createProcessSimulationsResponseStructure();
      simulationsResponseStructure.setCompoundIdentifier(compoundIdentifier);
      simulationsResponseStructure.setSimulationId(simulationId);
      simulationsResponses.add(simulationsResponseStructure);
    }

    return outgoingResponse;
  }
}