/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object.results;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * {@link IonChannelValuesVO}'s, grouped by a group name and a group level.
 *
 * @author geoff
 */
public class GroupedValuesVO {

  private final String name;
  private final short level;
  private final List<IonChannelValuesVO> ionChannelValues = new ArrayList<IonChannelValuesVO>();

  /**
   * Initialising constructor.
   * 
   * @param name Grouping name.
   * @param level Grouping level.
   * @param ionChannelValues Ion channel values.
   */
  public GroupedValuesVO(final String name, final short level,
                         final List<IonChannelValuesVO> ionChannelValues) {
    this.name = name;
    this.level = level;
    if (ionChannelValues != null) {
      this.ionChannelValues.addAll(ionChannelValues);
    }
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "GroupedValuesVO [name=" + name + ", level=" + level
        + ", ionChannelValues=" + ionChannelValues + "]";
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + level;
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    GroupedValuesVO other = (GroupedValuesVO) obj;
    if (level != other.level)
      return false;
    if (name == null) {
      if (other.name != null)
        return false;
    } else if (!name.equals(other.name))
      return false;
    return true;
  }

  /**
   * Retrieve the grouped name.
   * 
   * @return Grouped name.
   */
  public String getName() {
    return name;
  }

  /**
   * Retrieve the grouped level.
   * 
   * @return Grouped level.
   */
  public short getLevel() {
    return level;
  }

  /**
   * (Unmodifiable) Collection of ion channel values for the grouping. 
   * 
   * @return Ion channel values for the grouping, or empty collection if none available.
   */
  public List<IonChannelValuesVO> getIonChannelValues() {
    return Collections.unmodifiableList(ionChannelValues);
  }
}