/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.entity.config;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

/**
 * Configuration record for a PC50 evalation strategy.
 *
 * @author geoff
 */
//second-level ehcache : see business_manager-ehcache.xml + business_manager-persistence.xml
/*
* NONSTRICT_READ_WRITE :
* Caches data that is sometimes updated without ever locking the cache.
* If concurrent access to an item is possible, this concurrency strategy makes no guarantee that
* the item returned from the cache is the latest version available in the database. Configure your
* cache timeout accordingly!
*/
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy.READ_ONLY)

@Entity
@NamedQueries({
  @NamedQuery(name=ConfigPC50EvaluationStrategy.QUERY_CONFIGPC50EVALUATIONSTRATEGY,
              query="SELECT configPC50EvaluationStrategy FROM ConfigPC50EvaluationStrategy AS configPC50EvaluationStrategy",
              hints = {@QueryHint(name = "org.hibernate.cacheable", value="true"),
                       @QueryHint(name = "org.hibernate.cacheRegion",
                                  value="uk.ac.ox.cs.epsrc.business_manager.entity.config.ConfigPC50EvaluationStrategy")})
})
@Table(uniqueConstraints = {
    @UniqueConstraint(columnNames = { "defaultInvocationOrder" },
                      name="default_invocation_order")
  })
public class ConfigPC50EvaluationStrategy implements Serializable {

  private static final long serialVersionUID = -7091793799758006420L;

  public static final String QUERY_CONFIGPC50EVALUATIONSTRATEGY = "configpc50evaluationstrategy.query";

  // Persistence identity (Surrogate primary key)
  @TableGenerator(name="configpc50evaluationstrategy_id_gen", table="sequence_pks_business_manager",
                  pkColumnName="pk_seq_name", pkColumnValue="configpc50evaluationstrategy_id",
                  valueColumnName="pk_seq_value", allocationSize=1)
  @Id @GeneratedValue(strategy=GenerationType.TABLE, generator="configpc50evaluationstrategy_id_gen")
  private Long id;

  @Column(insertable=true, nullable=false, updatable=false)
  private String description;

  @Column(insertable=true, nullable=false, updatable=false)
  private Integer defaultInvocationOrder;

  @Column(insertable=true, nullable=false, updatable=false)
  private Boolean defaultActive;

  /** Default constructor - do not use. */
  protected ConfigPC50EvaluationStrategy() {}

  /**
   * Initialising constructor.
   * 
   * @param description Strategy description.
   * @param defaultInvocationOrder Default invocation order.
   * @param defaultActive Indicator of whether the strategy should be active by default.
   */
  public ConfigPC50EvaluationStrategy(final String description, final Integer defaultInvocationOrder,
                                      final boolean defaultActive) {
    this.description = description;
    this.defaultInvocationOrder = defaultInvocationOrder;
    this.defaultActive = defaultActive;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "ConfigPC50EvaluationStrategy [id=" + id + ", description="
        + description + ", defaultInvocationOrder=" + defaultInvocationOrder
        + ", defaultActive=" + defaultActive + "]";
  }

  /**
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * @return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @return the defaultInvocationOrder
   */
  public Integer getDefaultInvocationOrder() {
    return defaultInvocationOrder;
  }

  /**
   * @return the defaultActive
   */
  public Boolean getDefaultActive() {
    return defaultActive;
  }
}