/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This {@link InputValueSource} object, containing a QSAR data input value (i.e. a pIC50) for an
 * <i>Ion Channel</i>, is created during post-processing aggregation (as opposed to some derived 
 * from <i>Screening</i> data workflow processing.
 *
 * @author geoff
 * @see SummaryIVS
 * @see IndividualIVS
 */
public class QSARIVS implements InputValueSource {

  private static final long serialVersionUID = 2447367394732537969L;

  private final BigDecimal pIC50;

  private static final Log log = LogFactory.getLog(QSARIVS.class);

  /**
   * Initialising constructor.
   * 
   * @param pIC50 pIC50 value.
   * @throws IllegalArgumentException If a {@code null} pIC50 value is passed as a parameter.
   */
  public QSARIVS(final BigDecimal pIC50) {
    if (pIC50 == null) {
      final String errorMessage = "A pIC50 must be passed to QSAR input value source constructor";
      log.error("QSARIVS() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }
    this.pIC50 = pIC50;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "QSARIVS [pIC50=" + pIC50 + "]";
  }

  /**
   * Retrieve the QSAR pIC50.
   * 
   * @return non-{@code null} pIC50 value.
   */
  public BigDecimal getpIC50() {
    return pIC50;
  }
}