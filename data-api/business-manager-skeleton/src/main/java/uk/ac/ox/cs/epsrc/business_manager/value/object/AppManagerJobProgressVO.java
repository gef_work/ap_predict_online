/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * App Manager job progress VO containing all the app manager statuses for the job.
 *
 * @author geoff
 */
public class AppManagerJobProgressVO implements Serializable {

  public class StatusVO {
    private final String timestamp;
    private final String level;
    private final String text;
    private final List<String> args = new ArrayList<String>();

    public StatusVO(final String timestamp, final String level, final String text) {
      this.timestamp = timestamp;
      this.level = level;
      this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
      return "StatusVO [timestamp=" + timestamp + ", level=" + level
          + ", text=" + text + ", args=" + args + "]";
    }

    /** @return the timestamp */
    public String getTimestamp() {
      return timestamp;
    }

    /** @return the level */
    public String getLevel() {
      return level;
    }

    /** @return the text */
    public String getText() {
      return text;
    }
  }

  private static final long serialVersionUID = 621609172113459366L;

  private final Long jobId;
  private final List<StatusVO> progress = new ArrayList<StatusVO>();

  /**
   * Initialising constructor.
   * 
   * @param jobId Job identifier.
   */
  public AppManagerJobProgressVO(final Long jobId) {
    this.jobId = jobId;
  }

  /**
   * @return the jobId
   */
  public Long getJobId() {
    return jobId;
  }

  /**
   * Retrieve the statuses collection (representing the job progress).
   * 
   * @return Statuses collection.
   */
  public List<StatusVO> getProgress() {
    return progress;
  }
}