/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.annotation.Header;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.epsrc.business_manager.entity.GroupedInvocationInput;
import uk.ac.ox.cs.epsrc.business_manager.entity.IonChannelValues;
import uk.ac.ox.cs.epsrc.business_manager.entity.PIC50Data;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.entity.SimulationAssay;
import uk.ac.ox.cs.epsrc.business_manager.service.SimulationService;
import uk.ac.ox.cs.epsrc.business_manager.value.object.PreInvocationProcessingVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;

/**
 * Generate assay values according to the assay grouping and assay inheriting preferences.
 *
 * @author geoff
 */
public class PreInvocationAssayValueProcessing {

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_CONFIGURATION)
  private Configuration configuration;

  // JMS Template for sending provenance details.
  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JMS_TEMPLATE)
  private BusinessManagerJMSTemplate jmsTemplate;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE)
  private SimulationService simulationService;

  private static final short groupingIsOffStrategy = 0;
  private static final short groupingIsOnStrategy = 1;

  private static final Short noInheritingStrategy = 0;
  private static final Short withinGroupInheritingStrategy = 1;
  private static final Short betweenGroupInheritingStrategy = 2;

  private static final Log log = LogFactory.getLog(PreInvocationAssayValueProcessing.class);

  /*
   * Perform grouping.
   */
  private Map<Short, Map<Short, SimulationAssay>> grouping(final short groupingStrategy,
                                                           final List<SimulationAssay> simulationAssays) {
    log.debug("~grouping() : Invoked.");

    /* k: assay group (or assay level if grouping is off),
       v: k: assay level (or 0 is grouping is off), v: simulation assay */
    final Map<Short, Map<Short, SimulationAssay>> grouped = new HashMap<Short, Map<Short, SimulationAssay>>();

    switch (groupingStrategy) {
      case groupingIsOffStrategy :
        log.debug("~grouping() : Grouping is off.");
        // order assays by assay level and consider the assay a group with an assigned dummy group identifier.
        for (final SimulationAssay simulationAssay : simulationAssays) {
          final String assayName = simulationAssay.getAssayName();
          final Short assayLevel = retrieveAssayLevel(assayName);

          final Map<Short, SimulationAssay> singleSimulationAssay = new TreeMap<Short, SimulationAssay>();
          singleSimulationAssay.put(assayLevel, simulationAssay); // one per assay in "group"

          // Grouping is off, just use the assay level as the group
          grouped.put(assayLevel, singleSimulationAssay);
        }
        break;
      case groupingIsOnStrategy :
        log.debug("~grouping() : Grouping is on.");
        // group assays according to group identifier, and order assays within group by assay level
        for (final SimulationAssay simulationAssay : simulationAssays) {
          final String assayName = simulationAssay.getAssayName();
          final Short assayGroupLevel = retrieveAssayGroupLevel(assayName);
          final Short assayLevel = retrieveAssayLevel(assayName);

          if (grouped.containsKey(assayGroupLevel)) {
            final Map<Short, SimulationAssay> existing = grouped.get(assayGroupLevel);
            existing.put(assayLevel, simulationAssay);
          } else {
            // we're using TreeMap as during inheritance processing we'll be inheriting by ordered assay level
            final Map<Short, SimulationAssay> orderedSimulationAssays = new TreeMap<Short, SimulationAssay>();
            orderedSimulationAssays.put(assayLevel, simulationAssay);
            grouped.put(assayGroupLevel, orderedSimulationAssays);
          }
        }

        break;
      default :
        final String errorMessage = "Could not determine the grouping strategy according to value '" + groupingStrategy + "'.";
        log.error("~grouping() : " + errorMessage);
        System.out.println(errorMessage);
        break;
    }

    return grouped;
  }

  /*
   * Perform inheriting.
   */
  private Map<Short, Map<Short, SimulationAssay>> inheriting(final Short[] inheritingStrategies,
                                                             final Map<Short, Map<Short, SimulationAssay>> grouped) {
    log.debug("~inheriting() : Invoked.");

    final Set<Short> strategyListing = new HashSet<Short>(Arrays.asList(inheritingStrategies));
    if (strategyListing.contains(noInheritingStrategy)) {
      if (strategyListing.size() > 1) {
        log.warn("~inheriting() : Inheriting strategy of 'no inheriting' is overriding. Other strategies ignored.");
      } else {
        log.debug("~inheriting() : Inheriting strategy of 'no inheriting'.");
      }

      /* We need to clone the business values, i.e. pIC50's + Hills, but not the relationships as 
         new ones will be assigned on creation of the GroupedInvocationInput */
      final Map<Short, Map<Short, SimulationAssay>> newGrouped = new TreeMap<Short, Map<Short, SimulationAssay>>();

      for (final Map.Entry<Short, Map<Short, SimulationAssay>> eachGrouped : grouped.entrySet()) {
        // May be either an assay level (if not grouping) or an assay group level (if grouping)
        final Short groupLevel = eachGrouped.getKey();
        final Map<Short, SimulationAssay> perGroupingLevel = eachGrouped.getValue();

        final Map<Short, SimulationAssay> newPerGroupingLevel = new TreeMap<Short, SimulationAssay>();
        newGrouped.put(groupLevel, newPerGroupingLevel);

        for (final Map.Entry<Short, SimulationAssay> eachGroupingLevel : perGroupingLevel.entrySet()) {
          final Short assayLevel = eachGroupingLevel.getKey();
          final SimulationAssay simulationAssay = eachGroupingLevel.getValue();

          SimulationAssay clonedSimulationAssay = null;
          try {
            log.debug("~inheriting() : Cloning the simulation assay.");
            clonedSimulationAssay = simulationAssay.clone();
          } catch (CloneNotSupportedException e) {
            final String errorMessage = "Exception '" + e.getMessage() + "' thrown cloning simulation assay";
            log.error("~inheriting() : " + errorMessage);
            throw new UnsupportedOperationException(errorMessage);
          }
          newPerGroupingLevel.put(assayLevel, clonedSimulationAssay);
        }
      }

      return newGrouped;
    } else {
      // Strategy is to inherit values.
      final boolean withinGroupInheriting = strategyListing.contains(withinGroupInheritingStrategy);
      final boolean betweenGroupInheriting = strategyListing.contains(betweenGroupInheritingStrategy);
      log.debug("~inheriting() : withinGroupInheriting '" + withinGroupInheriting + "', betweenGroupInheriting '" + betweenGroupInheriting + "'.");

      final Map<Short, Map<Short, SimulationAssay>> inherited = new TreeMap<Short, Map<Short, SimulationAssay>>();

      Set<IonChannelValues> previousIonChannelValuesCollection = new HashSet<IonChannelValues>();

      // k: group, v: group's per-assay ion channel values.
      for (final Map.Entry<Short, Map<Short, SimulationAssay>> eachGroupEntry : grouped.entrySet()) {
        // May be either an assay level (if not grouping) or an assay group level (if grouping)
        final Short groupLevel = eachGroupEntry.getKey();
        log.debug("~inheriting() : Checking group '" + groupLevel + "'");

        // Mapped using TreeMap, so ordered by assay level
        final Map<Short, SimulationAssay> perAssayLevel = eachGroupEntry.getValue();

        // Inheritance governed by ordering by assay level
        final Map<Short, SimulationAssay> inheritedSimulationAssay = new TreeMap<Short, SimulationAssay>(); 
        inherited.put(groupLevel, inheritedSimulationAssay);

        if (!betweenGroupInheriting) {
          log.debug("~inheriting() : Resetting previous assay ion channel values.");
          // Start with a blank sheet if we're in a new group and we're not inheriting from a prior
          previousIonChannelValuesCollection = new HashSet<IonChannelValues>();
        }

        // k: assay level, v: assay's ion channel's pIC50 data values. (pIC50, hill, etc)
        for (final Map.Entry<Short, SimulationAssay> eachAssayLevel : perAssayLevel.entrySet()) {
          final Short assayLevel = eachAssayLevel.getKey();
          log.debug("~inheriting() : Checking level '" + assayLevel + "'.");
          final SimulationAssay simulationAssay = eachAssayLevel.getValue();

          // For this assay level clone the assay's ion channel data.
          SimulationAssay clonedSimulationAssay = null;
          try {
            clonedSimulationAssay = simulationAssay.clone();
          } catch (CloneNotSupportedException e) {
            final String errorMessage = "Exception thrown when trying to clone a SimulationAssay: '" + e.getMessage() + "'.";
            log.error("~inheriting() : " + errorMessage);
            System.out.println(errorMessage);
            throw new UnsupportedOperationException(errorMessage);
          }
          // Replicate the current assay level's data into the inherited structure.
          inheritedSimulationAssay.put(assayLevel, clonedSimulationAssay);

          final Set<IonChannelValues> currentAssayIonChannelValues = simulationAssay.getIonChannelValues();

          if (withinGroupInheriting) {
            if (previousIonChannelValuesCollection.isEmpty()) {
              log.debug("~inheriting() : First assay in group.");
              /* First assay in group
                 1. assign cloned IonChannelValues objects to "previous" data holder, so that if
                    subsequently inherited the persistence ids won't be there. */
              previousIonChannelValuesCollection = clonedSimulationAssay.getIonChannelValues();
            } else {
              log.debug("~inheriting() : Not first assay in group.");
              /* Not first assay in group - add previous to cloned
                 1. traverse previously defined ion channel values for assay
                 2. if an ion channel value was defined in a previous assay but not in current assay
                    then append the ion channel data cloned assay data.
                 3. set cloned assay data to be previously defined. */

              // 1.
              for (final IonChannelValues eachPreviousIonChannelValues : previousIonChannelValuesCollection) {
                final String previousIonChannelName = eachPreviousIonChannelValues.getIonChannelName();
                // Not copying a reference to the collection as we're persisting a replication of the content!
                final List<PIC50Data> previousPIC50Data = eachPreviousIonChannelValues.retrieveClonedPIC50Data();
                final String sourceAssayName = eachPreviousIonChannelValues.getSourceAssayName();

                // 2.
                boolean ionChannelExistsInCurrent = false;
                for (final IonChannelValues currentIonChannelValues : currentAssayIonChannelValues) {
                  final String currentIonChannelName = currentIonChannelValues.getIonChannelName();
                  if (previousIonChannelName.equals(currentIonChannelName)) {
                    // previously defined ion channel found in current assay values... don't inherit
                    ionChannelExistsInCurrent = true;
                    break;
                  }
                }
                if (!ionChannelExistsInCurrent) {
                  log.debug("~inheriting() : Ion channel '" + previousIonChannelName + "' data being inherited.");
                  clonedSimulationAssay.appendIonChannelValues(new IonChannelValues(previousIonChannelName,
                                                                                    sourceAssayName,
                                                                                    previousPIC50Data));
                }
              }

              // 3.
              previousIonChannelValuesCollection = clonedSimulationAssay.getIonChannelValues();
            }
          }
        }
      }

      return inherited;
    }
  }

  /**
   * Process according to defined strategy choices.
   * 
   * @param processingType Processing type.
   * @param simulationId Simulation identifier.
   * @param preInvocationProcessing Processed data object.
   * @return Grouped and inherited data according to chosen strategies.
   * @throws IllegalArgumentException If null {@link PreInvocationProcessingVO} value passed.
   */
  public ProcessedDataVO preInvocationProcessing(final @Header(value=BusinessIdentifiers.SI_HDR_PROCESSING_TYPE,
                                                               required=true)
                                                       ProcessingType processingType,
                                                 final @Header(value=BusinessIdentifiers.SI_HDR_SIMULATION_ID,
                                                               required=true)
                                                       Long simulationId,
                                                 final PreInvocationProcessingVO preInvocationProcessing)
                                                 throws IllegalArgumentException {
    log.debug("~preInvocationProcessing() : Invoked.");
    if (preInvocationProcessing == null) {
      final String errorMessage = "Invalid use of null pre-invocation processing value object.";
      log.error("~preInvocationProcessing() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }
    final Simulation simulation = simulationService.findBySimulationId(simulationId);

    final short groupingStrategy = simulation.isAssayGrouping() ? groupingIsOnStrategy : groupingIsOffStrategy;
    final Short[] inheritingStrategies = new Short[2];
    int strategy_idx = 0;
    if (simulation.isValueInheriting()) {
      if (simulation.isBetweenGroups()) inheritingStrategies[strategy_idx++] = betweenGroupInheritingStrategy;
      if (simulation.isWithinGroups()) inheritingStrategies[strategy_idx++] = withinGroupInheritingStrategy;
    } else {
      inheritingStrategies[strategy_idx] = noInheritingStrategy;
    }

    final List<SimulationAssay> simulationAssays = preInvocationProcessing.getSimulationAssays();

    /* Task 1 : Group assay data according to grouping strategy.
                If grouping is "on" then the returned map key is on assay group, with value map
                  keyed on individual assay level.
                If grouping is "off" then map keyed on individual assay level, and only a single
                  value map object keyed on the same individual assay level */
    final Map<Short, Map<Short, SimulationAssay>> grouped = grouping(groupingStrategy,
                                                                     simulationAssays);

    // Task 2 : Assign values according to PreInvocationAssayValueAssignmentStrategy.
    final Map<Short, Map<Short, SimulationAssay>> inherited = inheriting(inheritingStrategies,
                                                                         grouped);

    // Task 3 : Transfer processed values to new value object for onward processing.
    // Processing beyond this point doesn't involve ordering by group or assay level
    final Set<GroupedInvocationInput> groupedAndInherited = new HashSet<GroupedInvocationInput>();

    // loop through the grouped assays and retrieve the last assay (representing the highest assay level) 
    for (final Map.Entry<Short, Map<Short, SimulationAssay>> eachInherited : inherited.entrySet()) {
      final Short groupLevel = eachInherited.getKey();
      final String groupName = retrieveGroupingName(groupingStrategy, groupLevel);
      final TreeMap<Short, SimulationAssay> assaysInGrouping = (TreeMap<Short, SimulationAssay>) eachInherited.getValue();

      final Map.Entry<Short, SimulationAssay> lastEntry = assaysInGrouping.lastEntry();
      //final Short highestAssayLevel = lastEntry.getKey();
      final SimulationAssay valuesAtHighestAssayLevel = lastEntry.getValue();
      final GroupedInvocationInput groupedInvocationInput = new GroupedInvocationInput(simulationId,
                                                                                       groupLevel,
                                                                                       groupName,
                                                                                       valuesAtHighestAssayLevel.getIonChannelValues());

      groupedAndInherited.add(groupedInvocationInput);
    }

    jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.TRACE,
                                                         MessageKey.ASSAY_VALUES_PROCESSED_PREINVOCATION.getBundleIdentifier(),
                                                         new String[] { }, simulationId).build());

    return new ProcessedDataVO(preInvocationProcessing.getPerFrequencyConcentrations(),
                               groupedAndInherited);
  }

  private Short retrieveAssayGroupLevel(final String assayName) {
    log.debug("~retrieveAssayGroup() : Invoked for '" + assayName + "'.");
    return configuration.retrieveAssayGroupLevelByAssayName().get(assayName);
  }

  private String retrieveGroupingName(final short groupingStrategy, final Short group) {
    log.debug("~retrieveGroupingName() : Invoked for group '" + group + "' and strategy '" + groupingStrategy + "'.");
    String groupingName = null;
    switch (groupingStrategy) {
      case groupingIsOnStrategy :
        groupingName = configuration.retrieveAssayGroupNameByGroupLevelMap().get(group);
        break;
      case groupingIsOffStrategy :
        groupingName = configuration.retrieveAssayNameByAssayLevel().get(group);
        break;
      default :
        final String errorMessage = "Unknown grouping strategy of '" + groupingStrategy + "' used.";
        log.error("~retrieveGroupingName() : " + errorMessage);
        throw new UnsupportedOperationException(errorMessage);
    }
    log.debug("~retrieveGroupingName() : Group '" + group + "' equated to name '" + groupingName + "'.");
    return groupingName;
  }

  private Short retrieveAssayLevel(final String assayName) {
    log.debug("~retrieveAssayLevel() : Invoked for assay name '" + assayName + "'.");
    return configuration.retrieveAssayLevelByAssayName().get(assayName);
  }
}