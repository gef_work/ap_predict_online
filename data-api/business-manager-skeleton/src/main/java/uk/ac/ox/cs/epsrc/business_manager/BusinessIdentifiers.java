/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Helper class for consistent cross-object naming.
 * 
 * @author Geoff
 */
public final class BusinessIdentifiers {

  /** System-wide data format */
  public static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss:SSS");

  /** Component - App Manager manager */
  public static final String COMPONENT_APP_MANAGER_MANAGER = "appManagerManager";
  /** Component name for the App Manager WS WSS4J security interceptor
      <p>
      See also (sample.)appCtx.ws.security-outgoing.xml */
  public static final String COMPONENT_APP_MANAGER_SERVICES_INTERCEPTOR = "wsAppManagerServicesInterceptor";
  /** Component - App Manager web services proxy */
  public static final String COMPONENT_APP_MANAGER_PROXY = "appManagerProxy";
  /** Component name for the business Manager WS WSS4J incoming gateway security interceptor
     <p>
     See also (sample.)appCtx.ws.security-incoming.xml */
  public static final String COMPONENT_BUS_MANAGER_INBOUND_GTWY_INTERCEPTOR = "wsInboundGatewayInterceptor";
  /** Component - Configuration settings */
  public static final String COMPONENT_CONFIGURATION = "configuration";
  /** Component - ApPredict configuration settings */
  public static final String COMPONENT_CONFIGURATION_ACTIONPOTENTIAL = "configurationActionPotential";
  /** Component - Config service */
  public static final String COMPONENT_CONFIG_SERVICE = "configService";
  /** Component - Information (provenance, progress) manager */
  public static final String COMPONENT_INFORMATION_MANAGER = "informationManager";
  /** Component - Information (provenance, progress) service */
  public static final String COMPONENT_INFORMATION_SERVICE = "informationService";
  /** Component - JMS template */
  public static final String COMPONENT_JMS_TEMPLATE = "jmsTemplate";
  /** Component - Job DAO */
  public static final String COMPONENT_JOB_DAO = "jobDAO";
  /** Component - Job manager */
  public static final String COMPONENT_JOB_MANAGER = "jobManager";
  /** Component - OrientDb DAO */
  public static final String COMPONENT_ORIENT_DAO = "orientDAO";
  /** Component - Simulation results DAO */
  public static final String COMPONENT_RESULTS_DAO = "resultsDAO";
  /** Component - Simulation results manager */
  public static final String COMPONENT_RESULTS_MANAGER = "resultsManager";
  /** Component - Simulation results service */
  public static final String COMPONENT_RESULTS_SERVICE = "resultsService";
  /** Component - Simulation DAO */
  public static final String COMPONENT_SIMULATION_DAO = "simulationDAO";
  /** Component - Simulation processing gateway */
  public static final String COMPONENT_SIMULATION_PROCESSING_GATEWAY = "simulationProcessingGateway";
  /** Component - Simulation manager */
  public static final String COMPONENT_SIMULATION_MANAGER = "simulationManager";
  /** Component - Simulation service */
  public static final String COMPONENT_SIMULATION_SERVICE = "simulationService";
  /** Component - SimulationAssay DAO */
  public static final String COMPONENT_SIMULATIONASSAY_DAO = "simulationAssayDAO";
  /** Component - SimulationAssay manager */
  public static final String COMPONENT_SIMULATIONASSAY_MANAGER = "simulationAssayManager";
  /** Component - Site DAO */
  public static final String COMPONENT_SITE_DAO = "siteDAO";
  /** Component - Web service inbound gateway */
  public static final String COMPONENT_WS_INBOUND_GATEWAY = "wsInboundGateway";

  /** Spring integration message header key : Job id. */
  public static final String SI_HDR_JOB_ID = "hdr_business_manager_jobId";
  /** Spring Integration message header key : Processing type. */
  public static final String SI_HDR_PROCESSING_TYPE = "hdr_business_manager_processingType";
  /** Spring Integration message header key : Progress message key. */
  public static final String SI_HDR_PROGRESS_MESSAGEKEY = "hdr_business_manager_progress_messagekey";
  /** Spring Integration message header key : ApPreDiCT run request object. */
  public static final String SI_HDR_RUN_REQUEST_OBJECT = "hdr_business_manager_run_request_object";
  /** Spring Integration message header key : Simulation id. */
  public static final String SI_HDR_SIMULATION_ID = "hdr_business_manager_simulationId";

  // making non-instanceable
  private BusinessIdentifiers() {}
}