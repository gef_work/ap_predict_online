/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.information.messaging;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.integration.annotation.Header;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata.ProcessedSiteDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.APIIdentifiers;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 * Transform incoming objects into information (provenance, progress) objects.
 *
 * @author geoff
 */
public class InformationTransformer {

  private static final Log log = LogFactory.getLog(InformationTransformer.class);

  /**
   * Convert processed site data which does not contain data to a progress report.
   * 
   * @param simulationId Simulation identifier.
   * @param compoundIdentifier Compound identifier.
   * @param messageKey Message key. 
   * @param processedSiteData Processed site data.
   * @return Progress report, or {@code null} if none available.
   * @throws IllegalArgumentException If the processed site data arg has a {@code null} value
   *                                  or contains site data, or if it's an inappropriate message key.
   */
  public Progress processedSiteDataToProgress(final @Header(BusinessIdentifiers.SI_HDR_SIMULATION_ID)
                                                    Long simulationId,
                                              final @Header(APIIdentifiers.SI_HDR_COMPOUND_IDENTIFIER)
                                                    String compoundIdentifier,
                                              final @Header(BusinessIdentifiers.SI_HDR_PROGRESS_MESSAGEKEY)
                                                    MessageKey messageKey,
                                              final ProcessedSiteDataVO processedSiteData)
                                              throws IllegalArgumentException {
    final String identifiedLogPrefix = "~processedSiteDataToProgress() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    if (processedSiteData == null) {
      final String errorMessage = "Invalid attempt to convert a null processed site data object to progress information.";
      log.error(identifiedLogPrefix.concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    if (processedSiteData.isContainingSiteData()) {
      final String errorMessage = "Invalid attempt to convert a site data object containing data to progress information.";
      log.error(identifiedLogPrefix.concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    Progress progress = null;
    switch (messageKey) {
      case SIMULATION_TERMINATING :
        log.debug(identifiedLogPrefix.concat("SI Tranformer - simulation terminating instruction."));
        progress = new OverallProgress.Builder(InformationLevel.WARN,
                                               messageKey.getBundleIdentifier(), true, simulationId)
                                      .build();
        break;
      default :
        final String errorMessage = "Inappropriate progress message key of '" + messageKey + "' used when converting site data to progress.";
        log.error(identifiedLogPrefix.concat(errorMessage));
        throw new IllegalArgumentException(errorMessage);
    }

    return progress;
  }

  /**
   * Convert simulation data to progress report.
   * 
   * @param simulationId Simulation identifier.
   * @param compoundIdentifier Compound identifier.
   * @param messageKey Message key.
   * @param simulation Simulation.
   * @return Progress report (or null if none available).
   * @throws IllegalArgumentException If not using an appropriate message key.
   */
  public Progress simulationToProgress(final @Header(BusinessIdentifiers.SI_HDR_SIMULATION_ID)
                                             Long simulationId,
                                       final @Header(APIIdentifiers.SI_HDR_COMPOUND_IDENTIFIER)
                                             String compoundIdentifier,
                                       final @Header(BusinessIdentifiers.SI_HDR_PROGRESS_MESSAGEKEY)
                                             MessageKey messageKey,
                                       final Simulation simulation)
                                       throws IllegalArgumentException {
    log.debug("~simulationToProgress() : Invoked.");

    Progress progress = null;
    switch (messageKey) {
      case DISTRIBUTING :
        progress = new OverallProgress.Builder(InformationLevel.TRACE,
                                               messageKey.getBundleIdentifier(), simulationId)
                                      .build();
        break;
      default :
        final String errorMessage = "Inappropriate progress message key of '" + messageKey + "' used when converting simulation to progress.";
        log.error("~simulationToProgress() : ".concat(errorMessage));
        throw new IllegalArgumentException(errorMessage);
    }
    return progress;
  }

  /**
   * Convert site data to progress report.
   * 
   * @param simulationId Simulation identifier.
   * @param compoundIdentifier Compound identifier.
   * @param messageKey Message key.
   * @param siteDataHolder Site data.
   * @return Progress report, or {@code null} if none available.
   * @throws IllegalArgumentException If a {@code null} site data holder arg is received, or 
   *                                  if an invalid message key is passed.
   */
  public Progress siteDataToProgress(final @Header(BusinessIdentifiers.SI_HDR_SIMULATION_ID)
                                           Long simulationId,
                                     final @Header(APIIdentifiers.SI_HDR_COMPOUND_IDENTIFIER)
                                           String compoundIdentifier,
                                     final @Header(BusinessIdentifiers.SI_HDR_PROGRESS_MESSAGEKEY)
                                           MessageKey messageKey,
                                     final SiteDataHolder siteDataHolder)
                                     throws IllegalArgumentException {
     log.debug("~siteDataToProgress() : Invoked.");

     if (siteDataHolder == null) {
       final String errorMessage = "Illegal attempt to convert a null site data object to progress.";
       log.error("~siteDataToProgress() : ".concat(errorMessage));
       throw new IllegalArgumentException(errorMessage);
     }

     final AssayVO assay = siteDataHolder.getAssay();
     final IonChannel ionChannel = siteDataHolder.getIonChannel();
     final Integer summaryRecordCount = siteDataHolder.getSummaryDataRecord(false) == null ? 0 : 1;
     final Integer individualRecordCount = siteDataHolder.getNonSummaryDataRecords().size();

     Progress progress = null;
     switch (messageKey) {
       case SITEDATA_BREAKDOWN :
         progress = new OverallProgress.Builder(InformationLevel.TRACE,
                                                messageKey.getBundleIdentifier(),
                                                new String[] { assay.getName(), ionChannel.toString(),
                                                               summaryRecordCount.toString(),
                                                               individualRecordCount.toString() },
                                                simulationId).build();
         break;
       default :
         final String errorMessage = "Inappropriate progress message key of '" + messageKey + "' used when converting site data to progress.";
         log.error("~siteDataToProgress() : " + errorMessage);
         throw new IllegalArgumentException(errorMessage);
     }

     return progress;
  }
}