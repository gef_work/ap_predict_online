/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.entity;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Objects extending this class hold relationships to {@link IonChannelValues} objects.<br>
 * For example the {@link GroupedInvocationInput} concrete class objects will hold the pIC50/Hill
 * ion channel values for grouped (i.e. Iwks/FLIPR/Barracuda) assay data, whereas the {@link SimulationAssay}
 * concrete class objects will hold the per-assay (i.e. ungrouped) pIC50/Hill ion channel values.<br>
 * In some cases, e.g. when the grouping is switched off, the two will be the same!
 *
 * @see GroupedInvocationInput
 * @see SimulationAssay
 * @author geoff
 */
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class AbstractIonChannelValuesParent implements Serializable {

  private static final long serialVersionUID = 1180983932453059771L;

  // Persistence identity (Surrogate primary key)
  @TableGenerator(name="ionchannelvaluesparent_id_gen", table="sequence_pks_business_manager",
                  pkColumnName="pk_seq_name", pkColumnValue="ionchannelvaluesparent_id",
                  valueColumnName="pk_seq_value", allocationSize=2)
  @Id @GeneratedValue(strategy=GenerationType.TABLE, generator="ionchannelvaluesparent_id_gen")
  private Long id;

  // Name of the assay or assay group.
  @Column(length=100, nullable=false, updatable=false)
  private String parentName;

  @Column(nullable=false, updatable=false)
  private Long simulationId;

  // HashSet means that order isn't preserved.
  // bidirectional AbstractIonChannelValuesParent [1] <-> [0..*] IonChannelValues
  @OneToMany(cascade={ CascadeType.PERSIST, CascadeType.REMOVE },
             fetch=FetchType.LAZY,
             mappedBy="parent")
  private Set<IonChannelValues> ionChannelValues = new HashSet<IonChannelValues>();

  @Transient
  private static final Log log = LogFactory.getLog(AbstractIonChannelValuesParent.class);

  /** <b>Do not invoke directly.</b> */
  protected AbstractIonChannelValuesParent() {}

  /**
   * Initialising constructor.
   * 
   * @param parentName Name of the IonChannelValues parent, e.g. assay name.
   * @param simulationId Simulation identifier.
   */
  protected AbstractIonChannelValuesParent(final String parentName, final Long simulationId) {
    //assert (StringUtils.isNotBlank(parentName)) : "An invalid empty parent name value used in constructor!";
    //assert (simulationId != null) : "An invalid null simulation id used in constructor";

    this.parentName = parentName;
    this.simulationId = simulationId;
  }

  /**
   * Bidirectional relationship honoured.
   * 
   * @param ionChannelValues IonChannelValues to add.
   */
  protected void addIonChannelValues(final IonChannelValues ionChannelValues) {
    assert (ionChannelValues != null) : "Invalid null-valued ionChannelValues argument supplied!";

    ionChannelValues.setParent(this);
    this.ionChannelValues.add(ionChannelValues);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result
        + ((parentName == null) ? 0 : parentName.hashCode());
    result = prime * result
        + ((simulationId == null) ? 0 : simulationId.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    AbstractIonChannelValuesParent other = (AbstractIonChannelValuesParent) obj;
    if (parentName == null) {
      if (other.parentName != null)
        return false;
    } else if (!parentName.equals(other.parentName))
      return false;
    if (simulationId == null) {
      if (other.simulationId != null)
        return false;
    } else if (!simulationId.equals(other.simulationId))
      return false;
    return true;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "AbstractIonChannelValuesParent [id="
        + id + ", parentName=" + parentName
        + ", simulationId=" + simulationId + ", ionChannelValues="
        + ionChannelValues + "]";
  }

  /**
   * Retrieve the ion channel values.
   * 
   * @return Unmodifiable ion channel values collection.
   */
  public Set<IonChannelValues> getIonChannelValues() {
    return Collections.unmodifiableSet(ionChannelValues);
  }

  /**
   * Retrieve the name of the assay or assay group.
   * <p>
   * The name returned will depend on the subclass, i.e. if {@link GroupedInvocationInput} the
   * name will be the name of the assay group, if {@link SimulationAssay} the name will be the
   * name of the assay.
   * 
   * @return Name of assay or assay group.
   */
  protected String getParentName() {
    return parentName;
  }

  /**
   * Retrieve the simulation identifier.
   * 
   * @return Simulation identifier.
   */
  public Long getSimulationId() {
    return simulationId;
  }
}