/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.manager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.dao.SimulationAssayDAO;
import uk.ac.ox.cs.epsrc.business_manager.entity.IonChannelValues;
import uk.ac.ox.cs.epsrc.business_manager.entity.PIC50Data;
import uk.ac.ox.cs.epsrc.business_manager.entity.SimulationAssay;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.InputValueSource;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.QSARIVS;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.RESTAPIIVS;
import uk.ac.ox.cs.epsrc.business_manager.value.object.workflow.OutcomeVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 * Implementation of the SimulationAssay manager.
 *
 * @author geoff
 */
// Note: Not read-only transactional methods by default!
@Component(BusinessIdentifiers.COMPONENT_SIMULATIONASSAY_MANAGER)
public class SimulationAssayManagerImpl implements SimulationAssayManager {

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_SIMULATIONASSAY_DAO)
  private SimulationAssayDAO simulationAssayDAO;

  // Spring property injection - spring.properties
  private BigDecimal defaultHillCoefficient;

  private static final Log log = LogFactory.getLog(SimulationAssayManagerImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.SimulationAssayManager#createTransient(boolean, long, java.util.List, java.util.List)
   */
  // Not transactional!
  @Override
  public List<SimulationAssay> createTransient(final long simulationId,
                                               final List<OutcomeVO> qsarData,
                                               final List<OutcomeVO> screeningData) {
    final String identifiedLogPrefix = "~createTransient() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    /* Combine the outcomes of the QSAR and screening data into a singular entity */
    final List<OutcomeVO> allOutcomes = new ArrayList<OutcomeVO>();
    if (qsarData != null && !qsarData.isEmpty()) {
      allOutcomes.addAll(qsarData);
    }
    if (screeningData != null && !screeningData.isEmpty()) {
      allOutcomes.addAll(screeningData);
    }

    /* Need to transfer the outcome data structure to the persistence graph structure.
       k1: assay name v1: k2: ion channel name v2: pIC50 collection */
    final Map<String, Map<String, List<PIC50Data>>> structure = new HashMap<String, Map<String, List<PIC50Data>>>();

    for (final OutcomeVO allOutcome : allOutcomes) {
      final List<InputValueSource> allInputValueSources = allOutcome.getInputValueSources();

      final List<PIC50Data> pIC50Data = new ArrayList<PIC50Data>();
      for (final InputValueSource inputValueSource : allInputValueSources) {
        if (inputValueSource instanceof QSARIVS) {
          final QSARIVS qsarIVS = (QSARIVS) inputValueSource;
          final BigDecimal pIC50 = qsarIVS.getpIC50();

          pIC50Data.add(new PIC50Data(pIC50, defaultHillCoefficient, true,
                                      null));
        } else if (inputValueSource instanceof RESTAPIIVS) {
          final RESTAPIIVS restApiIvs = (RESTAPIIVS) inputValueSource;
          final BigDecimal pIC50 = restApiIvs.getpIC50();
          final BigDecimal hillCoefficient = restApiIvs.getHillCoefficient();

          final boolean defaultToNotOriginal = false;
          final Integer defaultToNoStrategyOrder = null;
          pIC50Data.add(new PIC50Data(pIC50, hillCoefficient,
                                      defaultToNotOriginal,
                                      defaultToNoStrategyOrder));
        } else {
          final String errorMessage = "Unrecognised IVS (Input Value Source) of '" + inputValueSource.getClass() + "'.";
          log.error("~createTransient() : ".concat(errorMessage));
          throw new UnsupportedOperationException(errorMessage);
        }
      }

      final AssayVO assay = allOutcome.getAssay();
      final IonChannel ionChannel = allOutcome.getIonChannel();

      final String assayName = assay.getName();
      final String ionChannelName = ionChannel.name();

      if (structure.containsKey(assayName)) {
        log.debug(identifiedLogPrefix.concat("Structure for assay '" + assayName + "' exists."));
        final Map<String, List<PIC50Data>> subStructure = structure.get(assayName);
        if (subStructure.containsKey(ionChannelName)) {
          final String errorMessage = "Substructure (keyed on ion channel name of '" + ionChannelName + "') should not exist for assay '" + assayName + "'.";
          log.error(identifiedLogPrefix.concat(errorMessage));
        } else {
          log.debug(identifiedLogPrefix.concat("Adding substructure '" + ionChannelName + "' to structure '" + assayName + "'."));
          subStructure.put(ionChannelName, pIC50Data);
        }
      } else {
        log.debug(identifiedLogPrefix.concat("Adding assay '" + assayName + "' to structure with substructure '" + ionChannelName + "' included."));
        final Map<String, List<PIC50Data>> newSubStructure = new HashMap<String, List<PIC50Data>>();
        newSubStructure.put(ionChannelName, pIC50Data);
        structure.put(assayName, newSubStructure);
      }
    }

    final List<SimulationAssay> simulationAssays = new ArrayList<SimulationAssay>(structure.size());

    for (final Map.Entry<String, Map<String, List<PIC50Data>>> eachStructure : structure.entrySet()) {
      final String assayName = eachStructure.getKey();
      log.debug(identifiedLogPrefix.concat("Assay '" + assayName + "'."));
      final Map<String, List<PIC50Data>> subStructures = eachStructure.getValue();

      final Set<IonChannelValues> ionChannelValues = new HashSet<IonChannelValues>(subStructures.size());
      for (final Map.Entry<String, List<PIC50Data>> eachSubStructure : subStructures.entrySet()) {
        final String ionChannelName = eachSubStructure.getKey();
        log.debug(identifiedLogPrefix.concat("  Ion Channel '" + ionChannelName + "'."));

        final List<PIC50Data> pIC50Data = eachSubStructure.getValue();
        log.debug(identifiedLogPrefix.concat("  pIC50s '" + StringUtils.join(pIC50Data.toArray(), ",") + "'."));

        ionChannelValues.add(new IonChannelValues(ionChannelName, assayName,
                                                  pIC50Data));
      }

      final SimulationAssay simulationAssay = new SimulationAssay(simulationId,
                                                                  assayName,
                                                                  ionChannelValues);

      log.debug(identifiedLogPrefix.concat("Store '" + simulationAssay.toString() + "'."));
      simulationAssays.add(simulationAssay);
    }

    return simulationAssays;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.SimulationAssayManager#persistSimulationAssays(java.util.List)
   */
  @Override
  @Transactional(readOnly=false)
  public List<SimulationAssay> persistSimulationAssays(final List<SimulationAssay> transientSimulationAssays) {

    final List<SimulationAssay> simulationAssays = new ArrayList<SimulationAssay>(transientSimulationAssays.size());
    for (final SimulationAssay eachTransientSimulationAssay : transientSimulationAssays) {
      simulationAssays.add(simulationAssayDAO.store(eachTransientSimulationAssay));
    }

    return simulationAssays;
  }

  /**
   * Assign the default Hill coefficient value to use.
   * 
   * @param defaultHillCoefficient Default Hill coefficient to use.
   */
  @Value("${fdr.default_coefficient}")
  public void setDefaultHillCoefficient(final BigDecimal defaultHillCoefficient) {
    log.info("~setDefaultHillCoefficient() : Assigning default Hill coefficient '" + defaultHillCoefficient + "'.");
    this.defaultHillCoefficient = defaultHillCoefficient;
  }
}