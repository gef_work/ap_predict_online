/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Assay value entity.
 * <p>
 * The ion-channel-specific QSAR and screening values, e.g. hERG pIC50(s) and hERG Hill Coefficient(s),
 * derived from workflow processing are stored in this and linked entities, according to simulation
 * identifier and assay name.
 *
 * @author geoff
 */
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy.READ_ONLY,
                                 region = "uk.ac.ox.cs.epsrc.business_manager.entity.config.IonChannelValues")

@Entity
@Table(uniqueConstraints = {
  @UniqueConstraint(columnNames = {"jc_parentid", "ionChannelName"},
                    name="unique_parentid_ionchannelname")
})
public class IonChannelValues implements Cloneable, Serializable {

  private static final long serialVersionUID = -4613433862934332261L;

  // Persistence identity (Surrogate primary key)
  @TableGenerator(name="ionchannelvalues_id_gen", table="sequence_pks_business_manager",
                  pkColumnName="pk_seq_name", pkColumnValue="ionchannelvalues_id",
                  valueColumnName="pk_seq_value", allocationSize=5)
  @Id @GeneratedValue(strategy=GenerationType.TABLE, generator="ionchannelvalues_id_gen")
  private Long id;

  @Column(length=50, nullable=false, updatable=false)
  private String ionChannelName;

  // ArrayList used because order is preserved and different PIC50Data records may appear equal.
  // bidirectional IonChannelValues [1] <-> [0..*] PIC50Data
  @OneToMany(cascade={ CascadeType.PERSIST, CascadeType.REMOVE },
             fetch=FetchType.LAZY,
             mappedBy="ionChannelValues")
  private List<PIC50Data> pIC50Data = new ArrayList<PIC50Data>();

  // bidirectional AbstractIonChannelValuesParent [1..*] <-> [1] IonChannelValues
  @ManyToOne
  @JoinColumn(name="jc_parentid", insertable=true, nullable=false, updatable=false)
  @org.hibernate.annotations.ForeignKey(name="fk_ionchannelvalues_parentid")
  private AbstractIonChannelValuesParent parent;

  // see getSourceAssayName()
  @Column(nullable=false, updatable=false)
  private String sourceAssayName;

  @Transient
  private static final Log log = LogFactory.getLog(IonChannelValues.class);

  /** <b>Do not invoke directly.</b> */
  protected IonChannelValues() {}

  /**
   * Initialising constructor.
   * 
   * @param ionChannelName Ion channel name.
   * @param sourceAssayName Source assay name.
   * @param pIC50Data Ion channel's pIC50, etc data.
   */
  public IonChannelValues(final String ionChannelName, final String sourceAssayName,
                          final List<PIC50Data> pIC50Data) {
    log.debug("~IonChannelValues() : Invoked.");

    this.ionChannelName = ionChannelName;
    this.sourceAssayName = sourceAssayName;
    if (pIC50Data != null && !pIC50Data.isEmpty()) {
      for (final PIC50Data each_pIC50Data : pIC50Data) {
        add_pIC50Data(each_pIC50Data);
      }
    }
  }

  // bidirectional relationship honoured.
  private void add_pIC50Data(final PIC50Data pIC50Data) {
    pIC50Data.setIonChannelValues(this);
    this.pIC50Data.add(pIC50Data);
  }

  /**
   * This cloning process does not copy the persistence identifiers of objects, just the "business"
   * data. Bidirectional relationships are retained though.
   * 
   * @return Cloned object.
   */
  @Override
  protected IonChannelValues clone() throws CloneNotSupportedException {
    final List<PIC50Data> clonedPIC50Data = new ArrayList<PIC50Data>(pIC50Data.size());
    for (final PIC50Data eachPIC50Data : pIC50Data) {
      clonedPIC50Data.add(eachPIC50Data.clone());
    }
    return new IonChannelValues(ionChannelName, sourceAssayName, clonedPIC50Data);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result
        + ((ionChannelName == null) ? 0 : ionChannelName.hashCode());
    result = prime * result
        + ((sourceAssayName == null) ? 0 : sourceAssayName.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    IonChannelValues other = (IonChannelValues) obj;
    if (ionChannelName == null) {
      if (other.ionChannelName != null)
        return false;
    } else if (!ionChannelName.equals(other.ionChannelName))
      return false;
    if (sourceAssayName == null) {
      if (other.sourceAssayName != null)
        return false;
    } else if (!sourceAssayName.equals(other.sourceAssayName))
      return false;
    return true;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "IonChannelValues [id=" + id
        + ", ionChannelName=" + ionChannelName + ", pIC50Data=" + pIC50Data + ", sourceAssayName="
        + sourceAssayName + "]";
  }

  /**
   * Retrieve the cloned pIC50 data ({@link PIC50Data#clone()}) for this object and return it as
   * an unmodifiable collection.
   * 
   * @return Replicated/cloned pIC50 data.
   */
  public List<PIC50Data> retrieveClonedPIC50Data() {
    final List<PIC50Data> replicated = new ArrayList<PIC50Data>(pIC50Data.size());
    for (final PIC50Data eachPIC50Data : pIC50Data) {
      try {
        replicated.add(eachPIC50Data.clone());
      } catch (CloneNotSupportedException e) {}
    }
    return Collections.unmodifiableList(replicated);
  }

  /**
   * Assign a parent.
   * 
   * @param parent The parent to assign.
   */
  public void setParent(final AbstractIonChannelValuesParent parent) {
    /* SimulationAssay's clone() method invokes SimulationAssay's constructor which assigns 
       IonChannelValues to SimulationAssay, thus assigning the "parent" property on this object.
       To enable an IonChannelValues to be assigned to a different object, e.g. a 
       GroupedInvocationInput, the check below ensures that this object has been derived from
       a cloning operation (which wouldn't have copied the ionChannelValuesId) */
    if (this.parent != null && this.id != null) {
      final String errorMessage = "Cannot reassign an AbstractIonChannelValuesParent to a IonChannelValues.";
      log.error("~setParent() : " + errorMessage);
      throw new UnsupportedOperationException(errorMessage);
    }
    this.parent = parent;
  }

  /**
   * @return the ionChannelName
   */
  public String getIonChannelName() {
    return ionChannelName;
  }

  /**
   * @return the pIC50Data
   */
  public List<PIC50Data> getpIC50Data() {
    return Collections.unmodifiableList(pIC50Data);
  }

  /**
   * Indicate the assay source of the pIC50 data. In the case of {@link SimulationAssay} objects the
   * source assay name will be the name of the assay which recorded the data. In the case of
   * {@link GroupedInvocationInput} the source assay name may be derived from an inherited assay, 
   * i.e. A grouped PX/Qpatch CaV1.2 value may have a IonChannelValues object with a source assay
   * name of Quattro_FLIPR, indicating the data's origin. 
   * 
   * @return The assay name of the source of the pIC50 data.
   */
  public String getSourceAssayName() {
    return sourceAssayName;
  }
}