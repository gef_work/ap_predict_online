/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.information.progress;

import org.apache.commons.lang3.StringUtils;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Job progress details.
 *
 * @author geoff
 */
public class JobProgress extends AbstractProgress {

  private static final long serialVersionUID = -4738951082698418650L;

  private final Long jobId;
  private String groupName;
  private Short groupLevel;
  private Float pacingFrequency;

  public static class Builder extends AbstractProgress.Builder {
    // Optional parameters
    private final Long jobId;
    private String groupName = null;
    private Short groupLevel = null;
    private Float pacingFrequency = null;

    /**
     * Initialising constructor with minimum data. Default is non-terminating progress.
     * <p>
     * If you're creating the first {@code JobProgress} for this simulationId/jobId then chain the
     * {@link #onCreation(String, Short, Float)} function.
     * 
     * @param level Progress level.
     * @param text Progress text.
     * @param simulationId Simulation identifier.
     * @param jobId Job identifier.
     * @throws IllegalArgumentException See {@code super} constructor, or if {@code jobId} is
     *                                  {@code null}.
     */
    public Builder(final InformationLevel level, final String text, final Long simulationId,
                   final Long jobId) {
      super(level, text, simulationId);
      if (jobId == null) {
        throw new IllegalArgumentException("A jobId must be provided on job progress creation.");
      }
      this.jobId = jobId;
    }

    /**
     * Initialising constructor with optional args.
     * <p>
     * If you're creating the first {@code JobProgress} for this simulationId/jobId then chain the
     * {@link #onCreation(String, Short, Float)} function.
     * 
     * @param level Progress level.
     * @param text Progress text.
     * @param args Progress args.
     * @param simulationId Simulation identifier.
     * @param jobId Job identifier.
     * @throws IllegalArgumentException See {@code super} constructor, or if {@code jobId} is
     *                                  {@code null}.
     */
    public Builder(final InformationLevel level, final String text, final String[] args,
                   final Long simulationId, final Long jobId) {
      super(level, text, args, simulationId);
      if (jobId == null) {
        throw new IllegalArgumentException("A jobId must be provided on job progress creation.");
      }
      this.jobId = jobId;
    }

    /**
     * Used when creating a {@code JobProgress} for the first time.
     * 
     * @param groupName Group name.
     * @param groupLevel Group level.
     * @param pacingFrequency Simulation pacing frequency.
     * @return Builder.
     * @throws IllegalArgumentException If {@code groupName} is blank, or if other args are
     *                                  {@code null}.
     */
    public Builder onCreation(final String groupName, final Short groupLevel,
                              final Float pacingFrequency) throws IllegalArgumentException {
      if (StringUtils.isBlank(groupName) || groupLevel == null || pacingFrequency == null) {
        throw new IllegalArgumentException("A group name, level and pacing frequency must be provided on job progress creation.");
      }
      this.groupName = groupName;
      this.groupLevel = groupLevel;
      this.pacingFrequency = pacingFrequency;
      super.setOnCreation(true);
      return this;
    }

    /**
     * Instruct the builder to build!
     * 
     * @return Newly built progress data.
     */
    public JobProgress build() {
      return new JobProgress(this);
    }
  }

  /**
   * @param builder
   */
  private JobProgress(final Builder builder) {
    super(builder);
    jobId = builder.jobId;
    groupName = builder.groupName;
    groupLevel = builder.groupLevel;
    pacingFrequency = builder.pacingFrequency;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "JobProgress [jobId=" + jobId + ", groupName=" + groupName
        + ", groupLevel=" + groupLevel + ", pacingFrequency=" + pacingFrequency
        + "]";
  }

  /**
   * Retrieve the job identifier.
   * 
   * @return Job identifier.
   */
  public Long getJobId() {
    return jobId;
  }

  /**
   * Retrieve the group name, or the assay name is the simulation specified no grouping.
   * 
   * @return Group (or assay) name, or {@code null} if not assigned.
   */
  public String getGroupName() {
    return groupName;
  }

  /**
   * Retrieve the group level. This could be the assay level if the simulation specified no
   * grouping.
   * 
   * @return The group (or assay) level, or {@code null} if not assigned.
   */
  public Short getGroupLevel() {
    return groupLevel;
  }

  /**
   * Retrieve the pacing frequency.
   * 
   * @return The pacing frequency, or {@code null} if not assigned.
   */
  public Float getPacingFrequency() {
    return pacingFrequency;
  }
}