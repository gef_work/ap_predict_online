/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.config;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.value.object.CellModelVO;

/**
 * Configuration options for ApPredict invocations.
 * 
 * @author geoff
 */
// Values declared in config/appCtx.config.actionPotential.site.xml and
//                    config/appCtx.config.cellModels.site.xml
@Component(BusinessIdentifiers.COMPONENT_CONFIGURATION_ACTIONPOTENTIAL)
public class ConfigurationActionPotential {

  private static final BigDecimal illegalMaxPctileVal = new BigDecimal("100");

  // Holds relationships between the pacing frequencies and their compound concentrations.
  private final Map<BigDecimal, Set<BigDecimal>> defaultPerFrequencyConcentrations =
                new HashMap<BigDecimal, Set<BigDecimal>>();

  // Holds the CellML models.
  private final List<CellModelVO> cellModels = new ArrayList<CellModelVO>();

  // Holds the default Delta APD90 credible interval percentiles.
  private Set<BigDecimal> defaultCredibleIntervalPctiles = new TreeSet<BigDecimal>();

  private short defaultModelIdentifier;
  private final BigDecimal defaultPacingMaxTime;
  private final short defaultPlasmaConcCount;
  private final boolean defaultPlasmaConcLogScale;

  private static final Log log = LogFactory.getLog(ConfigurationActionPotential.class);

  /**
   * Initialising constructor.
   * 
   * @param defaultPacingMaxTime Default maximum pacing time (in minutes).
   * @param defaultPlasmaConcCount Default plasma concentration intermediate point count.
   * @param defaultPlasmaConcLogScale Default plasma concentration log scale.
   * @throws IllegalArgumentException If default maximum pacing time is specified as &lt;= 0.
   */
  public ConfigurationActionPotential(final BigDecimal defaultPacingMaxTime,
                                      final short defaultPlasmaConcCount,
                                      final boolean defaultPlasmaConcLogScale)
                                      throws IllegalArgumentException {
    log.info("~ConfigurationActionPotential() : Default Maximum Pacing Time (in minutes) '" + defaultPacingMaxTime + "'.");
    log.info("~ConfigurationActionPotential() : Default Plasma Conc Count '" + defaultPlasmaConcCount + "'.");
    log.info("~ConfigurationActionPotential() : Default Plasma Conc Log Scale '" + defaultPlasmaConcLogScale + "'.");
    if (!validPacingMaxTime(defaultPacingMaxTime)) {
      throw new IllegalArgumentException("Default Maximum Pacing Time must be > 0");
    }
    this.defaultPacingMaxTime = defaultPacingMaxTime;
    this.defaultPlasmaConcCount = defaultPlasmaConcCount;
    this.defaultPlasmaConcLogScale = defaultPlasmaConcLogScale;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "ConfigurationActionPotential [defaultPerFrequencyConcentrations="
        + defaultPerFrequencyConcentrations + ", cellModels=" + cellModels
        + ", defaultCredibleIntervalPctiles="
        + defaultCredibleIntervalPctiles + ", defaultModelIdentifier="
        + defaultModelIdentifier + ", defaultPacingMaxTime="
        + defaultPacingMaxTime + ", defaultPlasmaConcCount="
        + defaultPlasmaConcCount + ", defaultPlasmaConcLogScale="
        + defaultPlasmaConcLogScale + "]";
  }


  /**
   * Retrieve the CellML models.
   * 
   * @return Unmodifiable and ordered (by model identifier) collection (of min. size of 1) of
   *         CellML models.
   */
  public List<CellModelVO> getCellModels() {
    return Collections.unmodifiableList(cellModels);
  }

  /**
   * Retrieve the default Delta APD90 credible interval percentiles.
   * 
   * @return Default Delta APD90 credible interval percentiles.
   */
  public Set<BigDecimal> getDefaultCredibleIntervalPctiles() {
    return Collections.unmodifiableSet(defaultCredibleIntervalPctiles);
  }

  /**
   * Retrieve the default maximum pacing time (in minutes).
   * 
   * @return Default maximum pacing time, or {@code null} if not assigned.
   */
  public BigDecimal getDefaultPacingMaxTime() {
    return defaultPacingMaxTime;
  }

  /**
   * Retrieve the default per-frequency compound concentration values. There may be
   * different compound concentrations per frequency, however if there are too many compound
   * concentrations in a simulation the resultant client interface chart may appear congested!
   * <p>
   * For example, the frequency 0.5Hz may have compound concentrations of 0.01, 0.03, .. 100µM.
   * 
   * @return Unmodifiable collection of default per-frequency compound concentrations.
   */
  public Map<BigDecimal, Set<BigDecimal>> getDefaultPerFrequencyConcentrations() {
    return Collections.unmodifiableMap(defaultPerFrequencyConcentrations);
  }

  /**
   * Retrieve the default model identifier.
   * <p>
   * For example, the value {@code 1} has historically represented the Shannon model.
   * 
   * @return The default model identifier.
   * @throws IllegalStateException If the CellML models have not been configured.
   */
  public short getDefaultModelIdentifier() throws IllegalStateException {
    if (getCellModels().isEmpty()) {
      final String errorMessage = "Indeterminable default model identifier as no CellML models configured.";
      log.error("~getDefaultModelIdentifier() : ".concat(errorMessage));
      throw new IllegalStateException(errorMessage);
    }
    return defaultModelIdentifier;
  }

  /**
   * Retrieve the default plasma concentration count.
   * 
   * @return Default plasma concentration count.
   */
  public short getDefaultPlasmaConcCount() {
    return defaultPlasmaConcCount;
  }

  /**
   * Retrieve the default plasma concentration log scale flag.
   * 
   * @return True if to use log scale for plasma concentrations, otherwise false.
   */
  public boolean isDefaultPlasmaConcLogScale() {
    return defaultPlasmaConcLogScale;
  }

  /**
   * Assign the default Delta APD90 credible interval percentiles.
   * 
   * @param defaultCredibleIntervalPctiles The default Delta APD90 credible
   *                                       interval percentiles to set.
   */
  @Resource(name="defaultCredibleIntervalPercentiles")
  public void setDefaultCredibleIntervalPctiles(final Set<BigDecimal> defaultCredibleIntervalPctiles)
                                                throws IllegalArgumentException {
    if (defaultCredibleIntervalPctiles == null ||
        defaultCredibleIntervalPctiles.isEmpty()) {
      final String errorMessage = "Default Delta APD90 credible interval percentiles must be assigned in appCtx.config.actionPotential.site.xml.";
      log.error("~setdefaultCredibleIntervalPctiles() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }
    for (final BigDecimal credibleIntervalPctile : defaultCredibleIntervalPctiles) {
      if (credibleIntervalPctile == null ||
          credibleIntervalPctile.compareTo(BigDecimal.ZERO) <= 0 ||
          credibleIntervalPctile.compareTo(illegalMaxPctileVal) >= 0) {
        throw new IllegalArgumentException("A credible interval percentile value of '" + credibleIntervalPctile + "' is not valid!");
      } else {
        this.defaultCredibleIntervalPctiles.add(credibleIntervalPctile);
      }
    }
  }

  /**
   * Assign the default experimental per-frequency concentrations.
   * 
   * @param defaultPerFrequencyConcentrations Populated default experimental per-frequency
   *                                          concentrations.
   * @throws IllegalArgumentException If argument is {@code null} or empty.
   */
  // see Tips section @ http://docs.spring.io/spring/docs/3.0.x/spring-framework-reference/html/beans.html#beans-autowired-annotation-qualifiers
  @Resource(name="defaultPerFrequencyConcentrations")
  public void setDefaultPerFrequencyConcentrations(final Map<BigDecimal, Set<BigDecimal>> defaultPerFrequencyConcentrations)
                                                   throws IllegalArgumentException {
    if (defaultPerFrequencyConcentrations == null ||
        defaultPerFrequencyConcentrations.isEmpty()) {
      final String errorMessage = "Default per-frequency concentrations must be assigned in appCtx.config.actionPotential.site.xml.";
      log.error("~setDefaultPerFrequencyConcentrations() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    this.defaultPerFrequencyConcentrations.putAll(defaultPerFrequencyConcentrations);
  }

  /**
   * Verify that the maximum pacing time is a valid value.
   * <p>
   * A valid value is considered as being one of the following :
   * <ul>
   *   <li>{@code null}</li>
   *   <li>A positive non-zero decimal</li>
   * </ul>
   * 
   * @param pacingMaxTime Maximum pacing time to verify.
   * @return {@code true} if a valid value, otherwise {@code false}.
   */
  public static boolean validPacingMaxTime(final BigDecimal pacingMaxTime) {
    boolean valid = false;
    if (pacingMaxTime == null) {
      valid = true;
    } else {
      if (pacingMaxTime.compareTo(BigDecimal.ZERO) > 0) {
        valid = true;
      }
    }
    return valid;
  }

  /**
   * Assign the default CellML models.
   * 
   * @param configuredCellModels Configured (i.e. must be at least one value) CellML models.
   * @throws IllegalArgumentException If no configured CellML models are provided, or if a duplicate
   *                                  model identifier encountered.
   */
  @Autowired(required=true)
  protected void setCellModels(final Set<CellModelVO> configuredCellModels)
                               throws IllegalArgumentException {
    if (configuredCellModels == null || configuredCellModels.isEmpty()) {
      final String errorMessage = "CellML models must be assigned in appCtx.config.cellModels.site.xml.";
      log.error("~setCellModels() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    boolean defaultAssigned = false;
    final Set<Short> identifiersAssigned = new HashSet<Short>();
    for (final CellModelVO cellModel : configuredCellModels) {
      if (cellModel == null) {
        final String warnMessage = "Illegal null value representing a CellML model.. Ignoring!";
        log.warn("~setCellModels() : ".concat(warnMessage));
        continue;
      }
      final Short modelIdentifier = Short.valueOf(cellModel.getIdentifier());
      if (identifiersAssigned.contains(modelIdentifier)) {
        final String errorMessage = "CellML models with duplicate identifiers have been assigned!";
        log.error("~setCellModels() : ".concat(errorMessage));
        throw new IllegalArgumentException(errorMessage);
      } else {
        identifiersAssigned.add(modelIdentifier);
      }

      cellModels.add(cellModel);

      log.info("~setCellModels() : Configuring '" + cellModel.toString() + "'.");
      if (cellModel.isDefaultModel()) {
        if (defaultAssigned) {
          final String warnMessage = "There are more than one CellML models defined as being the default!";
          log.warn("~setCellModels() : ".concat(warnMessage));
        }
        defaultModelIdentifier = modelIdentifier;
        defaultAssigned = true;
      }
    }

    if (cellModels.isEmpty()) {
      final String errorMessage = "No CellML models could be configured.";
      log.error("~setCellModels() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    if (!defaultAssigned) {
      final String warnMessage = "No CellML model has been defined as the default! Assigning first as the default";
      log.warn("~setCellModels() : ".concat(warnMessage));
      defaultModelIdentifier = cellModels.get(0).getIdentifier();
    }

    Collections.sort(cellModels);
  }
}