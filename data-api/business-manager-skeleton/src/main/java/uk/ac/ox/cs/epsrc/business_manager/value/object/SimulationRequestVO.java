/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object;

import java.math.BigDecimal;

/**
 * Value object holding <b>quality-controlled</b> (i.e. verified and standardised) data 
 * representing a user's simulation request, e.g. :
 * <ul>
 *   <li>Compound identifier</li>
 *   <li>Force a simulation re-run</li>
 *   <li>Inheritance strategies</li>
 *   <li>etc.</li>
 * </ul>
 *
 * @author geoff
 */
public class SimulationRequestVO {

  private final String compoundIdentifier;
  private final boolean forceReRun;
  private final boolean reset;
  private final boolean assayGrouping;
  private final boolean valueInheriting;
  private final boolean withinGroups;
  private final boolean betweenGroups;
  private final short cellModelIdentifier;
  private final boolean inputDataOnly;
  private final BigDecimal pacingMaxTime; 

  /**
   * Initialising constructor being passed verified and standardised parameters.
   * 
   * @param compoundIdentifier Compound identifier.
   * @param forceReRun {@code true} if simulation is to be forcefully re-run.
   * @param reset {@code true} if simulation is to be reset.
   * @param assayGrouping {@code true} if input value assay grouping is to occur.
   * @param valueInheriting {@code true} if input value value inheriting is to occur.
   * @param withinGroups {@code true} if input value inheriting within assay groups (if assay grouping!) is to occur.
   * @param betweenGroups {@code true} if input value inheriting between assay groups (if assay grouping!) is to occur.
   * @param cellModelIdentifier CellML Model identifier.
   * @param pacingMaxTime Maximum pacing time (in minutes).
   * @param inputDataOnly Whether the simulation request is for input data only.
   */
  public SimulationRequestVO(final String compoundIdentifier,
                             final boolean forceReRun, final boolean reset,
                             final boolean assayGrouping,
                             final boolean valueInheriting,
                             final boolean withinGroups,
                             final boolean betweenGroups,
                             final short cellModelIdentifier,
                             final BigDecimal pacingMaxTime,
                             final boolean inputDataOnly) {
    this.compoundIdentifier = compoundIdentifier;
    this.forceReRun = forceReRun;
    this.reset = reset;
    this.assayGrouping = assayGrouping;
    this.valueInheriting = valueInheriting;
    this.withinGroups = withinGroups;
    this.betweenGroups = betweenGroups;
    this.cellModelIdentifier = cellModelIdentifier;
    this.pacingMaxTime = pacingMaxTime;
    this.inputDataOnly = inputDataOnly;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "SimulationRequestVO [compoundIdentifier=" + compoundIdentifier
        + ", forceReRun=" + forceReRun + ", reset=" + reset
        + ", assayGrouping=" + assayGrouping + ", valueInheriting="
        + valueInheriting + ", withinGroups=" + withinGroups
        + ", betweenGroups=" + betweenGroups + ", inputDataOnly=" + inputDataOnly + "]";
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + (assayGrouping ? 1231 : 1237);
    result = prime * result + (betweenGroups ? 1231 : 1237);
    result = prime * result + cellModelIdentifier;
    result = prime * result
        + ((compoundIdentifier == null) ? 0 : compoundIdentifier.hashCode());
    result = prime * result + (forceReRun ? 1231 : 1237);
    result = prime * result + (inputDataOnly ? 1231 : 1237);
    result = prime * result + (reset ? 1231 : 1237);
    result = prime * result + (valueInheriting ? 1231 : 1237);
    result = prime * result + (withinGroups ? 1231 : 1237);
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    SimulationRequestVO other = (SimulationRequestVO) obj;
    if (assayGrouping != other.assayGrouping)
      return false;
    if (betweenGroups != other.betweenGroups)
      return false;
    if (cellModelIdentifier != other.cellModelIdentifier)
      return false;
    if (compoundIdentifier == null) {
      if (other.compoundIdentifier != null)
        return false;
    } else if (!compoundIdentifier.equals(other.compoundIdentifier))
      return false;
    if (forceReRun != other.forceReRun)
      return false;
    if (inputDataOnly != other.inputDataOnly)
      return false;
    if (reset != other.reset)
      return false;
    if (valueInheriting != other.valueInheriting)
      return false;
    if (withinGroups != other.withinGroups)
      return false;
    return true;
  }

  /**
   * Retrieve the CellML Model identifier.
   * 
   * @return CellML Model identifier.
   */
  public short getCellModelIdentifier() {
    return cellModelIdentifier;
  }

  /**
   * @return the compoundIdentifier
   */
  public String getCompoundIdentifier() {
    return compoundIdentifier;
  }

  /**
   * Retrieve the force re-run flag.
   * 
   * @return {@code true} if forcing re-run, otherwise {@code false}.
   */
  public boolean isForceReRun() {
    return forceReRun;
  }

  /**
   * Retrieve the reset flag.
   * 
   * @return {@code true} if resetting, otherwise {@code false}.
   */
  public boolean isReset() {
    return reset;
  }

  /**
   * @return the assayGrouping
   */
  public boolean isAssayGrouping() {
    return assayGrouping;
  }

  /**
   * @return the valueInheriting
   */
  public boolean isValueInheriting() {
    return valueInheriting;
  }

  /**
   * @return the withinGroups
   */
  public boolean isWithinGroups() {
    return withinGroups;
  }

  /**
   * @return the betweenGroups
   */
  public boolean isBetweenGroups() {
    return betweenGroups;
  }

  /**
   * Retrieve the maximum pacing time (in minutes).
   * 
   * @return Maximum pacing time (in minutes), or {@code null} if not specified.
   */
  public BigDecimal getPacingMaxTime() {
    return pacingMaxTime;
  }

  /**
   * Retrieve flag to indicate whether the user has request to run input data only retrieval.
   * 
   * @return {@code true} if user seeking input data only, otherwise {@code false} (i.e. a normal
   *         simulation run.
   */
  public boolean isInputDataOnly() {
    return inputDataOnly;
  }
}