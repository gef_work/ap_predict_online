/*

  Original work: Copyright (c) 2015, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.dao.information;

import java.util.List;

import uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager.INFORMATION_FORMAT;
import uk.ac.ox.cs.epsrc.business_manager.value.object.InformationVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Interface to provenance data access object.
 *
 * @author geoff
 */
public interface ProvenanceDAO {

  /** Prefix used in provenance data to represent presence of dose-response data. */
  // If this value changes it will need to be reflected in the client provenance.js javascript.
  public static final String DOSE_RESPONSE_PREFIX = "dr_";

  /**
   * Retrieve the provenance for the compound.
   * 
   * @param simulationId Simulation identifier.
   * @param format Format of the returned provenance.
   * @return Provenance value object for compound, otherwise null if none found.
   * @throws IllegalArgumentException If an invalid simulation identifier or a <code>null</code>
   *                                  format value.
   * @throws IllegalStateException If there is more than one progress structure found for the
   *                               supplied <code>simulationId</code>.
   */
  InformationVO retrieveProvenance(long simulationId, INFORMATION_FORMAT format)
                                   throws IllegalArgumentException, IllegalStateException;

  /**
   * Write fundamental provenance data.
   * <p>
   * The first fundamental provenance text structure creation must be accompanied by both a 
   * compound identifier and a simulation id. Thereafter only simulation id should be used.
   * 
   * @param compoundIdentifier Compound identifier.
   * @param simulationId Simulation identifier.
   * @param level Provenance level.
   * @param text Provenance text.
   * @param args Provenance text arguments, {@code null} or empty if none available.
   * @throws IllegalArgumentException If the first fundamental provenance is not accompanied by
   *                                  both {@code compoundIdentifier} and {@code simulationId}.
   * @throws IllegalStateException If the database contains an irregular data structure.
   */
  void writeFundamental(String compoundIdentifier, long simulationId, InformationLevel level,
                        String text, List<String> args) throws IllegalArgumentException,
                                                               IllegalStateException;

}