/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object.results;

/**
 * Value object holding csv format pIC50, Hill, originality, source and strategy values for an ion
 * channel.
 *
 * @author geoff
 */
public class IonChannelValuesVO {

  private final String name;
  private final String pIC50s;
  private final String hills;
  private final String originals;
  private final String sourceAssayName;
  private final String strategyOrders;

  /**
   * Initialising constructor.
   * 
   * @param name Ion channel name.
   * @param sourceAssayName Source assay name.
   * @param pIC50s All pIC50 values (CSV format).
   * @param hills All Hill values (CSV format).
   * @param originals Original data indicators (CSV format).
   * @param strategyOrders pIC50 evaluation strategy default invocation orders (where applicable)
   *                       (CSV format).
   */
  public IonChannelValuesVO(final String name, final String sourceAssayName, final String pIC50s,
                            final String hills, final String originals, final String strategyOrders) {
    this.name = name;
    this.sourceAssayName = sourceAssayName;
    this.pIC50s = pIC50s;
    this.hills = hills;
    this.originals = originals;
    this.strategyOrders = strategyOrders;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "IonChannelValuesVO [name=" + name + ", pIC50s=" + pIC50s
        + ", hills=" + hills + ", originals=" + originals
        + ", sourceAssayName=" + sourceAssayName + ", strategyOrders="
        + strategyOrders + "]";
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @return the pIC50s
   */
  public String getpIC50s() {
    return pIC50s;
  }

  /**
   * @return the hills
   */
  public String getHills() {
    return hills;
  }

  /**
   * @return the sourceAssayName
   */
  public String getSourceAssayName() {
    return sourceAssayName;
  }

  /**
   * @return the originals
   */
  public String getOriginals() {
    return originals;
  }

  /**
   * @return the strategyOrders
   */
  public String getStrategyOrders() {
    return strategyOrders;
  }
}