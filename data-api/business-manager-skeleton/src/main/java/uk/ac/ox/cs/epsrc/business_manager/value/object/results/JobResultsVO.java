/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object.results;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Per-job results for the compound concentrations.
 *
 * @author geoff
 */
public class JobResultsVO {

  private final long jobId;
  private final Float pacingFrequency;
  private final String assayGroupName;
  private final short assayGroupLevel;
  private final String messages;
  private final String deltaAPD90PercentileNames;
  
  private final List<ConcentrationResultVO> concentrationResults = new ArrayList<ConcentrationResultVO>();

  /**
   * Initialising constructor.
   * 
   * @param jobId Job identifier.
   * @param pacingFrequency Pacing frequency.
   * @param assayGroupName Assay (or Assay Group) name.
   * @param assayGroupLevel Assay (or Assay Group) level.
   * @param messages Job-generated messages.
   * @param deltaAPD90PercentileNames Delta APD90 percentile names (as CSV).
   * @param concentrationResults Per-concentration results.
   */
  public JobResultsVO(final long jobId, final Float pacingFrequency, final String assayGroupName,
                      final short assayGroupLevel, final String messages,
                      final String deltaAPD90PercentileNames,
                      final List<ConcentrationResultVO> concentrationResults) {
    this.jobId = jobId;
    this.messages = messages;
    this.deltaAPD90PercentileNames = deltaAPD90PercentileNames;
    this.pacingFrequency = pacingFrequency;
    this.assayGroupLevel = assayGroupLevel;
    this.assayGroupName = assayGroupName;
    if (concentrationResults != null) {
      this.concentrationResults.addAll(concentrationResults);
    }
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "JobResultsVO [jobId=" + jobId + ", pacingFrequency="
        + pacingFrequency + ", assayGroupName=" + assayGroupName
        + ", assayGroupLevel=" + assayGroupLevel + ", messages=" + messages
        + ", deltaAPD90PercentileNames=" + deltaAPD90PercentileNames
        + ", concentrationResults=" + concentrationResults + "]";
  }

  /**
   * @return the jobId
   */
  public long getJobId() {
    return jobId;
  }

  /**
   * @return the pacingFrequency
   */
  public Float getPacingFrequency() {
    return pacingFrequency;
  }

  /**
   * @return the assayGroupName
   */
  public String getAssayGroupName() {
    return assayGroupName;
  }

  /**
   * @return the assayGroupLevel
   */
  public short getAssayGroupLevel() {
    return assayGroupLevel;
  }

  /**
   * @return the messages
   */
  public String getMessages() {
    return messages;
  }

  /**
   * Retrieve the Delta APD90 percentile names (as a CSV).
   * 
   * @return Delta APD90 percentile names (or {@code null} if none available, 
   *         representing a legacy database structure).
   */
  public String getDeltaAPD90PercentileNames() {
    return deltaAPD90PercentileNames;
  }

  /**
   * @return the concentrationResults
   */
  public List<ConcentrationResultVO> getConcentrationResults() {
    return Collections.unmodifiableList(concentrationResults);
  }
}