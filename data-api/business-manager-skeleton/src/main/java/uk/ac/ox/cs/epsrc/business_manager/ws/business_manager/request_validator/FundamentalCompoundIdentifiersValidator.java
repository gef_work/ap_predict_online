/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.business_manager.request_validator;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.business_manager.ws.business_manager.HandlerUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidCompoundIdentifierException;
import uk.ac.ox.cs.nc3rs.business_manager.api.request.validator.CompoundIdentifiersValidator;

/**
 * Most basic of compound identifier validation, testing :
 * <ul>
 *   <li>Length of identifier.</li>
 * </ul>
 *
 * @author geoff
 */
@Component
public class FundamentalCompoundIdentifiersValidator implements CompoundIdentifiersValidator, Ordered {

  /** Reasonable(?) default maximum compound identifier length. */
  protected static int DEFAULT_MAX_COMPOUND_IDENTIFIER_LENGTH = 50;

  private static final Log log = LogFactory.getLog(FundamentalCompoundIdentifiersValidator.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.request.validator.CompoundIdentifiersValidator#validate(java.lang.Object)
   */
  @Override
  public void validate(final Object requestObject) throws InvalidCompoundIdentifierException {
    log.debug("~validate() : Invoked.");

    final List<String> rawCompoundIdentifiers = new ArrayList<String>(HandlerUtil.retrieveOrderedRawCompoundIdentifiers(requestObject));

    final String problem = RequestValidatorUtil.genericStringCollectionValidator(new LinkedHashSet<String>(rawCompoundIdentifiers),
                                                                                 DEFAULT_MAX_COMPOUND_IDENTIFIER_LENGTH,
                                                                                 0, "compound identifier");
    if (problem != null) {
      throw new InvalidCompoundIdentifierException(problem);
    }
  }

  /* (non-Javadoc)
   * @see org.springframework.core.Ordered#getOrder()
   */
  @Override
  public int getOrder() {
    // Ensure that this validation will run first.
    return Ordered.HIGHEST_PRECEDENCE;
  }

  /**
   * Assign a new default max compound identifier length.
   * 
   * @param defaultMaxCompoundIdentifierLength New default max compound identifier length. 
   */
  public static void setDefaultMaxCompoundIdentifierLength(final int defaultMaxCompoundIdentifierLength) {
    assert (defaultMaxCompoundIdentifierLength > 0) : "Inappropriate attempt to set a default maximum compound identifier length to '" + defaultMaxCompoundIdentifierLength + "'.";
    if (FundamentalCompoundIdentifiersValidator.DEFAULT_MAX_COMPOUND_IDENTIFIER_LENGTH != defaultMaxCompoundIdentifierLength) {
      log.info("~setDefaultMaxCompoundIdentifierLength() : Assigning a new default maximum compound identifier length of '" + defaultMaxCompoundIdentifierLength + "'.");
    }
    FundamentalCompoundIdentifiersValidator.DEFAULT_MAX_COMPOUND_IDENTIFIER_LENGTH = defaultMaxCompoundIdentifierLength;
  }
}