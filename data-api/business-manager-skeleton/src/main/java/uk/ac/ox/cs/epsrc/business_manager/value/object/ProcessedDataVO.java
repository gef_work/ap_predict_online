/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.epsrc.business_manager.entity.GroupedInvocationInput;

/**
 * Pre- job creation object holding processed data.
 *
 * @author geoff
 */
public class ProcessedDataVO implements Serializable {

  private static final long serialVersionUID = 284205275580065767L;

  // e.g. k: 0.5Hz, v: [0, 0.001, 0.003, 0.01, 0.03, 0.1, ..] 
  private final Map<BigDecimal, Set<BigDecimal>> perFrequencyConcentrations =
                new HashMap<BigDecimal, Set<BigDecimal>>();

  // Grouped (and maybe inherited) data
  // e.g. highest per-grouping assay ion channel pIC50, etc values
  private final Set<GroupedInvocationInput> processedData = new HashSet<GroupedInvocationInput>();
 
  private static final Log log = LogFactory.getLog(ProcessedDataVO.class);

  /**
   * Initialising constructor.
   * 
   * @param perFrequencyConcentrations Per-frequency compound concentrations, or empty collection if
   *                                   none available, e.g. no experimental data.
   * @param processedData Grouped and inherited assay / ion channel data.
   */
  public ProcessedDataVO(final Map<BigDecimal, Set<BigDecimal>> perFrequencyConcentrations,
                         final Set<GroupedInvocationInput> processedData) {
    log.debug("~ProcessedDataVO() : Invoked.");

    this.perFrequencyConcentrations.putAll(perFrequencyConcentrations);
    this.processedData.addAll(processedData);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "ProcessedDataVO [perFrequencyConcentrations="
        + perFrequencyConcentrations + ", processedData="
        + processedData + "]";
  }

  /**
   * Retrieve the per-frequency compound concentrations.
   * 
   * @return Unmodifiable collection of per-frequency compound concentrations.
   */
  public Map<BigDecimal, Set<BigDecimal>> getPerFrequencyConcentrations() {
    return Collections.unmodifiableMap(perFrequencyConcentrations);
  }

  /**
   * Retrieve the processed (persistence entities!) data.
   * 
   * @return Unmodifiable collection of grouped invocation input values objects.
   */
  public Set<GroupedInvocationInput> getProcessedData() {
    return Collections.unmodifiableSet(processedData);
  }
}