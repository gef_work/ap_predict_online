/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.manager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.dao.ResultsDAO;
import uk.ac.ox.cs.epsrc.business_manager.entity.GroupedInvocationInput;
import uk.ac.ox.cs.epsrc.business_manager.entity.IonChannelValues;
import uk.ac.ox.cs.epsrc.business_manager.entity.Job;
import uk.ac.ox.cs.epsrc.business_manager.entity.JobResult;
import uk.ac.ox.cs.epsrc.business_manager.entity.PIC50Data;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.ConcentrationResultVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.GroupedValuesVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.IonChannelValuesVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.JobResultsVO;

/**
 * Implementation to simulation results management.
 *
 * @author geoff
 */
@Component(BusinessIdentifiers.COMPONENT_RESULTS_MANAGER)
@Transactional(readOnly=true)
public class ResultsManagerImpl implements ResultsManager {

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_RESULTS_DAO)
  private ResultsDAO resultsDAO;

  private static final Log log = LogFactory.getLog(ResultsManagerImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.ResultsManager#retrieveInputValues(long)
   */
  @Override
  public Set<GroupedValuesVO> retrieveInputValues(final long simulationId) {
    final String identifiedLogPrefix = "~retrieveInputValues() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    final Set<GroupedValuesVO> groupedValues = new HashSet<GroupedValuesVO>();

    /*
     * Switch from entities to value objects.
     */
    final List<GroupedInvocationInput> groupedInvocationInputs = retrieveInputValuesEntities(simulationId);
    for (final GroupedInvocationInput eachGroupedInvocationInput : groupedInvocationInputs) {
      final Short assayGroupLevel = eachGroupedInvocationInput.getAssayGroupLevel();
      final String assayGroupName = eachGroupedInvocationInput.getAssayGroupName();
      log.debug(identifiedLogPrefix.concat("Assay group name '" + assayGroupName + "', and level '" + assayGroupLevel + "'."));

      final List<IonChannelValuesVO> ionChannelVOs = new ArrayList<IonChannelValuesVO>();

      for (final IonChannelValues ionChannelValues : eachGroupedInvocationInput.getIonChannelValues()) {
        final String ionChannelName = ionChannelValues.getIonChannelName();
        final String sourceAssayName = ionChannelValues.getSourceAssayName();
        final List<PIC50Data> allPIC50Data = ionChannelValues.getpIC50Data();

        final int pIC50DataCount = allPIC50Data.size();
        final List<BigDecimal> allPIC50s = new ArrayList<BigDecimal>(pIC50DataCount);
        final List<BigDecimal> allHills = new ArrayList<BigDecimal>(pIC50DataCount);
        final List<Boolean> allOriginals = new ArrayList<Boolean>(pIC50DataCount);
        final List<Integer> allStrategyOrders = new ArrayList<Integer>(pIC50DataCount);
        for (final PIC50Data pIC50Data : allPIC50Data) {
          allPIC50s.add(pIC50Data.getValue());
          allHills.add(pIC50Data.getHill());
          allOriginals.add(pIC50Data.getOriginal());
          allStrategyOrders.add(pIC50Data.getStrategyOrder());
        }
        final String pIC50s = StringUtils.join(allPIC50s, ",");
        final String hills = StringUtils.join(allHills, ",");
        final String originals = StringUtils.join(allOriginals, ",");
        final String strategyOrders = StringUtils.join(allStrategyOrders, ",");
        ionChannelVOs.add(new IonChannelValuesVO(ionChannelName, sourceAssayName,
                                                 pIC50s, hills, originals,
                                                 strategyOrders));
      }

      groupedValues.add(new GroupedValuesVO(assayGroupName, assayGroupLevel,
                                            ionChannelVOs));
    }

    return groupedValues;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.ResultsManager#retrieveInputValuesEntities(long)
   */
  @Override
  public List<GroupedInvocationInput> retrieveInputValuesEntities(final long simulationId) {
    final String identifiedLogPrefix = "~retrieveInputValuesEntities() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    return resultsDAO.retrieveInputValues(simulationId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.ResultsManager#retrieveResults(long)
   */
  @Override
  public List<JobResultsVO> retrieveResults(final long simulationId) {
    final String identifiedLogPrefix = "~retrieveResults() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    final List<JobResultsVO> jobResults = new ArrayList<JobResultsVO>();

    final List<Job> jobs = resultsDAO.retrieveResults(simulationId);
    log.debug(identifiedLogPrefix.concat("Has '" + jobs.size() + "' results."));
    for (final Job job : jobs) {
      final long jobId = job.getId();

      final Float pacingFrequency = (Float) job.getPacingFrequency();
      // These may be assay or assay group names/levels.
      final GroupedInvocationInput groupedInvocationInput = job.getGroupedInvocationInput();
      final String assayGroupName = groupedInvocationInput.getAssayGroupName();
      final short assayGroupLevel = groupedInvocationInput.getAssayGroupLevel();
      final String messages = job.getMessages();
      final String deltaAPD90PercentileNames = job.getDeltaAPD90PercentileNames();

      final List<ConcentrationResultVO> concentrationResults = new ArrayList<ConcentrationResultVO>();
      for (final JobResult jobResult : job.getJobResults()) {
        final float compoundConcentration = jobResult.getCompoundConcentration();
        final String deltaAPD90 = jobResult.getDeltaAPD90();
        final String upstrokeVelocity = jobResult.getUpstrokeVelocity();
        final String times = jobResult.getTimes();
        final String voltages = jobResult.getVoltages();
        final String qNet = jobResult.getQNet();
        final ConcentrationResultVO concentrationResult = new ConcentrationResultVO(compoundConcentration,
                                                                                    times, voltages,
                                                                                    deltaAPD90,
                                                                                    upstrokeVelocity,
                                                                                    qNet);
        concentrationResults.add(concentrationResult);
      }
      jobResults.add(new JobResultsVO(jobId, pacingFrequency, assayGroupName,
                                      assayGroupLevel, messages,
                                      deltaAPD90PercentileNames,
                                      concentrationResults));
    }

    return jobResults;
  }
}