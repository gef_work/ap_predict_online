/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object;

/**
 * Value object containing the results of a simulation job for each of the compound concentrations.
 *
 * @author geoff
 */
public class PerJobConcentrationResultsVO {

  private final String compoundConcentration;
  private final String times;
  private final String voltages;
  private final String deltaAPD90;
  private final String qNet;

  /**
   * Initialising constructor.
   * 
   * @param compoundConcentration Compound concentration.
   * @param times AP times.
   * @param voltages AP voltages.
   * @param deltaAPD90 Delta APD90.
   * @param qNet qNet (optional).
   */
  public PerJobConcentrationResultsVO(final String compoundConcentration,
                                      final String times, final String voltages,
                                      final String deltaAPD90,
                                      final String qNet) {
    this.compoundConcentration = compoundConcentration;
    this.times = times;
    this.voltages = voltages;
    this.deltaAPD90 = deltaAPD90;
    this.qNet = qNet;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "PerJobConcentrationResultsVO [compoundConcentration="
        + compoundConcentration + ", times=" + times + ", voltages=" + voltages
        + ", deltaAPD90=" + deltaAPD90 + ", qNet=" + qNet + "]";
  }

  /**
   * Retrieve the compound concentration.
   * 
   * @return Compound concentration (in µM).
   */
  public String getCompoundConcentration() {
    return compoundConcentration;
  }

  /**
   * Retrieve the voltage trace times.
   * 
   * @return Voltage trace times (CSV format).
   */
  public String getTimes() {
    return times;
  }

  /**
   * Retrieve the voltage trace voltages.
   * 
   * @return Voltage trace voltages (CSV format).
   */
  public String getVoltages() {
    return voltages;
  }

  /**
   * Retrieve the Delta APD90 value(s).
   * 
   * @return Delta APD90 value(s) (potentially CSV format).
   */
  public String getDeltaAPD90() {
    return deltaAPD90;
  }

  /**
   * Retrieve the (optional) qNet value.
   * 
   * @return qNet value (C/F) (or {@code null} if not calculated).
   */
  public String getQNet() {
    return qNet;
  }
}