/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.app_manager.proxy;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApplicationStatusRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApplicationStatusResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllStatusesRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllStatusesResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.SimulationDeleteRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.SimulationDeleteResponse;

/**
 * Proxy to App Manager WS invocations.
 *
 * @author geoff
 */
public class AppManagerProxyImpl extends WebServiceGatewaySupport implements AppManagerProxy {

  // appCtx.ws.security-outgoing.xml
  @Autowired(required=false)
  @Qualifier(BusinessIdentifiers.COMPONENT_APP_MANAGER_SERVICES_INTERCEPTOR)
  private ClientInterceptor wsAppManagerServicesInterceptor;

  private static final Log log = LogFactory.getLog(AppManagerProxyImpl.class);

  @PostConstruct
  private void postConstruct() {
    final ClientInterceptor[] wsClientInterceptors = { wsAppManagerServicesInterceptor };

    this.setInterceptors(wsClientInterceptors);
    for (int idx = 0; idx < wsClientInterceptors.length; idx++) {
      log.info("~AppManagerProxyImpl() : Interceptor '" + wsClientInterceptors[idx] + "' assigned to outbound.");
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.ws.app_manager.proxy.AppManagerProxy#applicationStatusRequest(uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApplicationStatusRequest)
   */
  @Override
  public ApplicationStatusResponse applicationStatusRequest(final ApplicationStatusRequest statusRequest) {
    log.debug("~applicationStatusRequest() : Invoked.");

    return (ApplicationStatusResponse) getWebServiceTemplate().marshalSendAndReceive(statusRequest);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.ws.app_manager.proxy.AppManagerProxy#deleteSimulation(uk.ac.ox.cs.nc3rs.app_manager.ws._1.SimulationDeleteRequest)
   */
  @Override
  public SimulationDeleteResponse deleteSimulation(final SimulationDeleteRequest deleteRequest) {
    log.debug("~deleteSimulation() : Invoked.");

    return (SimulationDeleteResponse) getWebServiceTemplate().marshalSendAndReceive(deleteRequest);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.ws.app_manager.proxy.AppManagerProxy#retrieveJobProgress(uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllStatusesRequest)
   */
  @Override
  public RetrieveAllStatusesResponse retrieveJobProgress(final RetrieveAllStatusesRequest statusesRequest) {
    log.debug("~retrieveJobProgress() : Invoked.");

    return (RetrieveAllStatusesResponse) getWebServiceTemplate().marshalSendAndReceive(statusesRequest);
  }
}