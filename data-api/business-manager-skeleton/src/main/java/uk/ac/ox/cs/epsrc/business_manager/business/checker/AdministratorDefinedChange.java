/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.checker;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.Ordered;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Application administrator instruction. By default the order of this check is assigned the
 * highest precedence. 
 *
 * @author geoff
 */
public class AdministratorDefinedChange extends AbstractChangeChecker {

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JMS_TEMPLATE)
  private BusinessManagerJMSTemplate jmsTemplate;

  private final boolean forceRequires;
  private static final String defaultReason = "Administrator-defined re-run";
  private final String reason;

  private static final Log log = LogFactory.getLog(AdministratorDefinedChange.class);

  /**
   * Initialising constructor.
   * 
   * @param forceRequires True if administrator wishes to force a re-run.
   * @param reason Reason for forcing (defaults to an empty string if no forcing required).
   */
  public AdministratorDefinedChange(final boolean forceRequires, final String reason) {
    super(Ordered.HIGHEST_PRECEDENCE);
    this.forceRequires = forceRequires;
    final String useReason = (reason == null) ? defaultReason : reason;
    this.reason = forceRequires ? useReason : "";
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.business.checker.ChangeChecker#getNature()
   */
  @Override
  public String getNature() {
    return MessageKey.SIMULATION_CHANGE_ADMINISTRATOR.getBundleIdentifier();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.business.checker.ChangeChecker#requiresReRun(uk.ac.ox.cs.epsrc.business_manager.entity.Simulation)
   */
  @Override
  public boolean requiresReRun(final Simulation simulation) {
    final long simulationId = simulation.getId();
    final String identifiedLogPrefix = "~requiresReRun() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    if (forceRequires) {
      log.info(identifiedLogPrefix.concat(reason));
    } else {
      jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.DEBUG,
                                                           "No administrator defined change instruction",
                                                           simulationId).build());
    }

    return forceRequires;
  }
}