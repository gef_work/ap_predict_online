/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.manager;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.SimulationProcessingGateway;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.FundamentalProvenance;
import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.epsrc.business_manager.config.ConfigurationActionPotential;
import uk.ac.ox.cs.epsrc.business_manager.dao.SimulationDAO;
import uk.ac.ox.cs.epsrc.business_manager.entity.Job;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedSimulationRequestVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.SimulationRequestVO;
import uk.ac.ox.cs.epsrc.business_manager.ws.app_manager.manager.AppManagerManager;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Implementation of the simulations manager.
 *
 * @author geoff
 */
@Component(BusinessIdentifiers.COMPONENT_SIMULATION_MANAGER)
@Transactional(readOnly=true)
public class SimulationManagerImpl implements SimulationManager {

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_APP_MANAGER_MANAGER)
  private AppManagerManager appManagerManager;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_CONFIGURATION)
  private Configuration configuration;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_CONFIGURATION_ACTIONPOTENTIAL)
  private ConfigurationActionPotential configurationActionPotential;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_INFORMATION_MANAGER)
  private InformationManager informationManager;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JMS_TEMPLATE)
  private BusinessManagerJMSTemplate jmsTemplate;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JOB_MANAGER)
  private JobManager jobManager;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_SIMULATION_DAO)
  private SimulationDAO simulationDAO;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_SIMULATION_PROCESSING_GATEWAY)
  private SimulationProcessingGateway simulationProcessingGateway;

  protected static final short anticipatedCellMLModelIdentifierMinValue = 1;

  private static final boolean sendProgressTerminationKey = true;
  private static final String cross = MessageKey.MARK_CROSS.getBundleIdentifier();
  private static final String tick = MessageKey.MARK_TICK.getBundleIdentifier();

  private static final Log log = LogFactory.getLog(SimulationManagerImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager#find(long)
   */
  @Override
  public Simulation find(final long simulationId) {
    log.debug("~find() : [" + simulationId + "] : Invoked.");

    return simulationDAO.findBySimulationId(simulationId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager#find(uk.ac.ox.cs.epsrc.business_manager.value.object.SimulationRequestVO)
   */
  @Override
  public Simulation find(final SimulationRequestVO simulationRequest) {
    final String compoundIdentifier = simulationRequest.getCompoundIdentifier();
    log.debug("~find(SimulationRequestVO) : [" + compoundIdentifier + "] : Invoked.");

    final boolean assayGrouping = simulationRequest.isAssayGrouping();
    final boolean valueInheriting = simulationRequest.isValueInheriting();
    final boolean betweenGroups = simulationRequest.isBetweenGroups();
    final boolean withinGroups = simulationRequest.isWithinGroups();

    final short cellModelIdentifier = simulationRequest.getCellModelIdentifier();

    final BigDecimal pacingMaxTime = simulationRequest.getPacingMaxTime();

    // Check to see if simulation for compound id already started at some point?
    return find(compoundIdentifier, assayGrouping, valueInheriting,
                betweenGroups, withinGroups, cellModelIdentifier, pacingMaxTime);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager#find(java.lang.String, boolean, boolean, boolean, boolean, short, java.math.BigDecimal)
   */
  @Override
  public Simulation find(final String compoundIdentifier,
                         final boolean assayGrouping,
                         final boolean valueInheriting,
                         final boolean betweenGroups,
                         final boolean withinGroups,
                         final short cellModelIdentifier,
                         final BigDecimal pacingMaxTime) {
    log.debug("~find(...) : [" + compoundIdentifier + "] : Invoked.");

    return simulationDAO.findByAllProperties(compoundIdentifier, assayGrouping,
                                             valueInheriting, betweenGroups,
                                             withinGroups, cellModelIdentifier,
                                             pacingMaxTime);
  }

  // Check to see if the latest re-run check or results creation was outside the quiet time.
  private boolean hasExpired(final Date checkDate) {
    log.debug("~hasExpired() : Invoked.");
    if (checkDate == null) {
      return false;
    }

    // Convert default quiet period value from minutes to milliseconds.
    final Timestamp expireAt = new Timestamp(checkDate.getTime() +
                                             (configuration.getDefaultQuietPeriod() * 60 * 1000));
    final Date now = new Timestamp(Calendar.getInstance().getTime().getTime());
    return (expireAt.compareTo(now) < 1);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager#jobCompletedProcessing(long)
   */
  @Override
  @Transactional(readOnly=false)
  public boolean jobCompletedProcessing(final long simulationId) {
    final String identifiedLogPrefix = "~jobCompletedProcessing() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    boolean simulationCompleted = false;
    if (jobManager.determineJobsCompletedForSimulation(simulationId)) {
      simulationDAO.updateOnJobsCompletion(simulationId);
      simulationCompleted = true;
    } else {
      log.debug(identifiedLogPrefix.concat("Jobs remaining for simulation."));
    }

    return simulationCompleted;
  }

  // Make simulation available for SQL-selection.
  private Simulation makeAvailableForProcessingSelection(final Simulation simulation) {
    final Long simulationId = simulation.getId();
    final String identifiedLogPrefix = "~makeAvailableForProcessingSelection() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    simulation.makeAvailableForProcessing();
    return save(simulation);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager#processingForceReRun(uk.ac.ox.cs.epsrc.business_manager.entity.Simulation)
   */
  @Override
  @Transactional(readOnly=false)
  public Simulation processingForceReRun(final Simulation simulation) {
    final Long simulationId = simulation.getId();
    final String identifiedLogPrefix = "~processingForceReset() : [" + simulationId + "] : ";
    final String message = "** Rerun processing ** : Simulation '" + simulationId + "' to be re-run.";
    log.debug(identifiedLogPrefix.concat(message));

    final Simulation resetSimulation = resetSimulation(simulation);
    purgeAndRun(resetSimulation, MessageKey.RERUN_PROCESSING.getBundleIdentifier(),
                new String[] { simulationId.toString() });

    return resetSimulation;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager#processingForceReset(uk.ac.ox.cs.epsrc.business_manager.entity.Simulation)
   */
  @Override
  @Transactional(readOnly=false)
  public Simulation processingForceReset(final Simulation simulation) {
    final Long simulationId = simulation.getId();
    final String identifiedLogPrefix = "~processingForceReset() : [" + simulationId + "] : ";
    final String message = "** Reset processing ** : Simulation '" + simulationId + "' to be reset.";
    log.debug(identifiedLogPrefix.concat(message));

    final String compoundIdentifier = simulation.getCompoundIdentifier();

    simulation.resetSystemState(true);
    final Simulation resetSimulation = save(resetSimulation(simulation));
    informationManager.purge(simulationId);

    /*
     * Create a new progress and provenance as we're re-using the original simulation id so if
     * re-submitted it'll be considered an existing simulation (so expects progress and provenance).
     */

    // This is a terminating progress (which needs to follow the information purge).
    jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.DEBUG,
                                                         MessageKey.RESET_PROCESSING.getBundleIdentifier(),
                                                         new String[] { simulationId.toString() },
                                                         true, simulationId)
                                                .onCreation(compoundIdentifier)
                                                .build());

    jmsTemplate.sendProvenance(new FundamentalProvenance.Builder(InformationLevel.DEBUG,
                                                                 MessageKey.RESET_PROCESSING.getBundleIdentifier(),
                                                                 new String[] { simulationId.toString() },
                                                                 simulationId)
                                                        .onCreation(compoundIdentifier)
                                                        .build());
    return resetSimulation;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager#processingRegular(uk.ac.ox.cs.epsrc.business_manager.entity.Simulation)
   */
  @Override
  @Transactional(readOnly=false)
  public Simulation processingRegular(Simulation simulation) {
    final Long simulationId = simulation.getId();
    final String identifiedLogPrefix = "~processingRegular() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    final Date completed = simulation.getCompleted();
    if (completed == null) {
      final String message = "Simulation processing workflow initiated.";
      log.debug(identifiedLogPrefix.concat(message));
      jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.TRACE,
                                                           message,
                                                           new String[] { },
                                                           simulationId)
                                                  .build());

      simulationProcessingGateway.regularSimulation(simulation);
    } else {
      final boolean hasBeenReRunChecked = simulation.hasBeenReRunChecked();
      final boolean latestCheckExpired = hasExpired(simulation.getLatestCheck());
      final boolean resultsExpired = hasExpired(completed);

      final List<String> args = new ArrayList<String>();
      args.add(resultsExpired ? tick : cross);
      args.add(hasBeenReRunChecked ? tick : cross);
      args.add(latestCheckExpired ? tick : cross);

      boolean runChangeChecks = false;
      if ((!hasBeenReRunChecked && resultsExpired) ||
          (hasBeenReRunChecked && latestCheckExpired)) {
        runChangeChecks = true;
      }

      boolean reRunRequired = false;
      if (runChangeChecks) {
        simulation.assignLatestCheck();
        /*
         * Change checking no longer taking place - if results have expired, re-run!
         */
        args.add(MessageKey.SIMULATION_CHANGE_ADMINISTRATOR.getBundleIdentifier());
        reRunRequired = true;
      }

      final String[] useArgs = args.toArray(new String[0]);
      if (reRunRequired) {
        purgeAndRun(simulation, MessageKey.RERUN_REQUIRED.getBundleIdentifier(), useArgs);
      } else {
        simulation.assignStateNotBusy();
        simulation = save(simulation);

        // Note this is terminating overall progress.
        jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.DEBUG,
                                                             MessageKey.RERUN_NOT_REQUIRED.getBundleIdentifier(),
                                                             useArgs, true,
                                                             simulationId)
                                                    .build());
      }
    }

    return simulation;
  }


  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager#requestProcessingExistingSimulation(uk.ac.ox.cs.epsrc.business_manager.entity.Simulation, uk.ac.ox.cs.epsrc.business_manager.value.object.SimulationRequestVO)
   */
  @Override
  @Transactional(readOnly=false)
  public ProcessedSimulationRequestVO requestProcessingExistingSimulation(Simulation simulation,
                                                                          final SimulationRequestVO simulationRequest) {
    final Long simulationId = simulation.getId();
    final String identifiedLogPrefix = "~requestProcessingExistingSimulation() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    final boolean forceReRun = simulationRequest.isForceReRun();
    final boolean reset = simulationRequest.isReset();

    final String compoundIdentifier = simulation.getCompoundIdentifier();

    log.debug(identifiedLogPrefix.concat("Simulation with requested properties (for compound identifier '" + compoundIdentifier + "') already exists."));
    jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.DEBUG,
                                                         MessageKey.SIMULATION_ALREADY_EXISTS.getBundleIdentifier(),
                                                         simulationId).build());

    final boolean simulationIsFree = !simulation.isBusy();

    if (forceReRun || reset) {
      if (reset && simulationIsFree) {
        final String message = "Ignoring request to reset a 'free' simulation.";
        log.debug(identifiedLogPrefix.concat(message));

        jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.TRACE,
                                                             message,
                                                             sendProgressTerminationKey,
                                                             simulationId)
                                                    .build());
      } else if (simulation.assignProcessTask(forceReRun, reset)) {
        // Simulation is 'free' or 'busy', and processTask has been modified - requires processing.
        jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.DEBUG,
                                                             MessageKey.SIMULATION_CHANGED.getBundleIdentifier(),
                                                             simulationId)
                                                    .build());

        simulation = makeAvailableForProcessingSelection(simulation);
      } else {
        final String message = "Ignoring request to re-run or reset simulation as no change imposed.";
        log.debug(identifiedLogPrefix.concat(message));

        // Don't terminate progress if simulation is busy
        final boolean terminateProgress = simulationIsFree ? sendProgressTerminationKey : false;
        jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.TRACE,
                                                             message,
                                                             terminateProgress,
                                                             simulationId)
                                                    .build());
      }
    } else { // Not forcing re-run or reset.
      if (simulationIsFree) {
        final String message = "Setting flag for simulation to be processed.";
        log.debug(identifiedLogPrefix.concat(message));

        simulation = makeAvailableForProcessingSelection(simulation);

        jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.DEBUG,
                                                             message,
                                                             simulationId)
                                                    .build());
      } else {
        final String message = "Simulation is busy.";
        log.debug(identifiedLogPrefix.concat(message));

        // Don't terminate progress as the simulation is either doing something (or about to).
        jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.TRACE,
                                                             message,
                                                             simulationId)
                                                    .build());
      }
    }

    return new ProcessedSimulationRequestVO(compoundIdentifier, simulationId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager#requestProcessingNewSimulation(uk.ac.ox.cs.epsrc.business_manager.value.object.SimulationRequestVO)
   */
  @Override
  @Transactional(readOnly=false)
  public ProcessedSimulationRequestVO requestProcessingNewSimulation(final SimulationRequestVO simulationRequest) {
    final String compoundIdentifier = simulationRequest.getCompoundIdentifier();
    log.debug("~requestProcessingNewSimulation() : Simulation doesn't exist for compound identifier '" + compoundIdentifier + "' with specified properties. Creating a new one.");

    final boolean assayGrouping = simulationRequest.isAssayGrouping();
    final boolean valueInheriting = simulationRequest.isValueInheriting();
    final boolean betweenGroups = simulationRequest.isBetweenGroups();
    final boolean withinGroups = simulationRequest.isWithinGroups();

    final short cellModelIdentifier = simulationRequest.getCellModelIdentifier();

    final BigDecimal pacingMaxTime = simulationRequest.getPacingMaxTime();

    final Simulation simulation = save(new Simulation(compoundIdentifier,
                                                      assayGrouping,
                                                      valueInheriting,
                                                      betweenGroups,
                                                      withinGroups,
                                                      cellModelIdentifier,
                                                      pacingMaxTime));

    final Long simulationId = simulation.getId();

    // Provenance written to associate the simulation id with the compound identifier in provenance data.
    jmsTemplate.sendProvenance(new FundamentalProvenance.Builder(InformationLevel.TRACE,
                                                                 MessageKey.SIMULATION_IDENTIFIER_CREATED.getBundleIdentifier(),
                                                                 new String[] { simulationId.toString() },
                                                                 simulationId)
                                                        .onCreation(compoundIdentifier)
                                                        .build());

    jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.TRACE,
                                                         MessageKey.SIMULATION_IDENTIFIER_CREATED.getBundleIdentifier(),
                                                         new String[] { simulationId.toString() },
                                                         simulationId)
                                                .onCreation(compoundIdentifier).build());

    return new ProcessedSimulationRequestVO(compoundIdentifier, simulationId);
  }

  /**
   * Purge existing and run it as a new simulation.
   * <p>
   * This will
   * <ul>
   *   <li>Call {@link JobManager#purgeSimulationJobData(long)} to purge simulation job data</li>
   *   <li>Call {@link Simulation#resetSystemState()}</li>
   *   <li>Save the simulation</li>
   *   <li>Call {@link InformationManager#purge(long)} to remove information</li>
   *   <li>Create new {@linkplain OverallProgress} and {@linkplain FundamentalProvenance} messages</li>
   *   <li>Call {@link SimulationProcessingGateway#regularSimulation(Simulation)}</li>
   * </ul>
   * 
   * @param simulation
   * @param message
   */
  private void purgeAndRun(Simulation simulation, final String bundleIdentifier,
                           final String[] args) {
    final long simulationId = simulation.getId();
    final String identifiedLogPrefix = "~purgeAndRun() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked for '" + simulation.toString() + "'."));

    final String compoundIdentifier = simulation.getCompoundIdentifier();

    // Ignore any returned app manager ids.
    jobManager.purgeSimulationJobData(simulationId);

    simulation.resetSystemState();
    simulation = save(simulation);

    informationManager.purge(simulationId);

    // Need to recreate progress following an information purge.
    jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.INFO,
                                                         bundleIdentifier,
                                                         args, simulationId)
                                                .onCreation(compoundIdentifier)
                                                .build());

    // Need to recreate the provenance following an information purge.
    jmsTemplate.sendProvenance(new FundamentalProvenance.Builder(InformationLevel.DEBUG,
                                                                 bundleIdentifier, args,
                                                                 simulationId)
                                                        .onCreation(compoundIdentifier)
                                                        .build());

    simulationProcessingGateway.regularSimulation(simulation);
  }

  /**
   * Reset (aka "stop") a simulation.
   * <p>
   * This will ...
   * <ul>
   *   <li>Call JobManager to purge simulation job data</li>
   *   <li>Call AppManagerManager to destroy running jobs (if any)</li>
   * </ul>
   * 
   * @param simulation Simulation to reset.
   * @return Reset simulation.
   */
  private Simulation resetSimulation(final Simulation simulation) throws IllegalStateException {
    final Long simulationId = simulation.getId();
    final String logPrefix = "~resetSimulation() : [" + simulationId + "] : ";
    log.debug(logPrefix.concat("Invoked."));

    // NOTE: Job entities have CascadeType.REMOVE for JobResult and GroupedInvocationInput entities.
    final Set<String> appMgrIdsToDelete = jobManager.purgeSimulationJobData(simulationId);
    if (appMgrIdsToDelete.isEmpty()) {
      if (simulation.forcingReset()) {
        // Resetting a simulation should only be possible when jobs are running! 
        log.warn(logPrefix.concat("Attempting to reset simulation but no running jobs."));
      }
    } else {
      // Remove running jobs from app manager.
      final int toDeleteCount = appMgrIdsToDelete.size();
      final Set<String> deleted = appManagerManager.deleteJobs(appMgrIdsToDelete);
      final int deletedCount = deleted.size();

      final String message = "'" + toDeleteCount + "' job(s) to delete, with '" + deletedCount + "' job(s) deleted.";
      if (toDeleteCount != deletedCount) {
        log.warn(logPrefix.concat(message));
      } else {
        log.debug(logPrefix.concat(message));
      }
    }

    return simulation;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager#retrievePerFrequencyConcentrations(long)
   */
  @Override
  public Map<BigDecimal, Set<BigDecimal>> retrievePerFrequencyConcentrations(final long simulationId) {
    final String identifiedLogPrefix = "~retrievePerFrequencyConcentrations() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    final Map<BigDecimal, Set<BigDecimal>> perFrequencyConcentrations = new TreeMap<BigDecimal, Set<BigDecimal>>();
    for (final Job job : jobManager.findJobsBySimulationId(simulationId)) {
      final Float pacingFrequency = job.getPacingFrequency();
      final Set<Float> compoundConcentrations = job.getCompoundConcentrations();

      final Set<BigDecimal> newCompoundConcentrations = new TreeSet<BigDecimal>();
      for (final Float compoundConcentration : compoundConcentrations) {
        newCompoundConcentrations.add(new BigDecimal(compoundConcentration.toString()));
      }
      perFrequencyConcentrations.put(new BigDecimal(pacingFrequency.toString()),
                                     newCompoundConcentrations);
    }

    return Collections.unmodifiableMap(perFrequencyConcentrations);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager#save(uk.ac.ox.cs.epsrc.business_manager.entity.Simulation)
   */
  @Override
  @Transactional(readOnly=false)
  public Simulation save(final Simulation simulation) {
    final String identifiedLogPrefix = "~save() : [" + simulation.getId() + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    return simulationDAO.store(simulation);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager#simulationTerminating(long)
   */
  @Override
  @Transactional(readOnly=false)
  public void simulationTerminating(final long simulationId) {
    final String identifiedLogPrefix = "~simulationTerminating() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    // Not writing to jmsTemplate in this method because SI channelling sorting that out.
    simulationDAO.updateOnJobsCompletion(simulationId);
  }


  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager#userInputValidation(java.lang.String, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Short, java.math.BigDecimal, java.lang.Boolean)
   */
  @Override
  public SimulationRequestVO userInputValidation(final String compoundIdentifier,
                                                 final Boolean forceReRun,
                                                 final Boolean reset,
                                                 final Boolean assayGrouping,
                                                 final Boolean valueInheriting,
                                                 final Boolean betweenGroups,
                                                 final Boolean withinGroups,
                                                 final Short cellModelIdentifier,
                                                 final BigDecimal pacingMaxTime,
                                                 final Boolean inputDataOnly) throws IllegalArgumentException {
    log.debug("~userInputValidation() : Invoked.");

    // Place the default configuration values into the simulation processing object if not user-defined. 

    final boolean useForceReRun = forceReRun == null ? configuration.isDefaultForceReRun() : forceReRun;
    boolean useReset = reset == null ? Configuration.DEFAULT_FORCE_RESET : reset;
    if (useForceReRun) {
      log.info("~userInputValidation() : Forcing a re-run overrules a forcing reset request!");
      useReset = false;
    }

    final boolean useAssayGrouping = assayGrouping == null ? configuration.isDefaultAssayGrouping() :
                                                             assayGrouping;
    final boolean useValueInheriting = valueInheriting == null ? configuration.isDefaultValueInheriting() :
                                                                 valueInheriting;
    final boolean useWithinGroups = withinGroups == null ? configuration.isDefaultWithinGroups() :
                                                           withinGroups;
    final boolean useBetweenGroups = betweenGroups == null ? configuration.isDefaultBetweenGroups() :
                                                             betweenGroups;

    short useCellModelIdentifier;
    if (cellModelIdentifier == null) {
      useCellModelIdentifier = configurationActionPotential.getDefaultModelIdentifier();
    } else {
      if (cellModelIdentifier < anticipatedCellMLModelIdentifierMinValue) {
        throw new IllegalArgumentException("User requested a CellML Model identifier value of '" + cellModelIdentifier + "' which is currently not handled!");
      } else {
        useCellModelIdentifier = cellModelIdentifier;
      }
    }

    final boolean useInputDataOnly = inputDataOnly == null ? false : inputDataOnly;

    if (!ConfigurationActionPotential.validPacingMaxTime(pacingMaxTime)) {
      throw new IllegalArgumentException("User requested an illegal maximum pacing time of '" + pacingMaxTime.toPlainString() + "'.");
    }

    final SimulationRequestVO verifiedSimualtionRequest = new SimulationRequestVO(compoundIdentifier,
                                                                                  useForceReRun,
                                                                                  useReset,
                                                                                  useAssayGrouping,
                                                                                  useValueInheriting,
                                                                                  useWithinGroups,
                                                                                  useBetweenGroups,
                                                                                  useCellModelIdentifier,
                                                                                  pacingMaxTime,
                                                                                  useInputDataOnly);
    log.debug("~userInputValidation() : Create verified request object '" + verifiedSimualtionRequest.toString() + "'.");

    return verifiedSimualtionRequest;
  }
}