/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.app_manager.manager;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.annotation.PostConstruct;

import org.apache.commons.collections.map.MultiKeyMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.integration.annotation.Header;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.message.GenericMessage;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.JobProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.epsrc.business_manager.config.ConfigurationActionPotential;
import uk.ac.ox.cs.epsrc.business_manager.entity.GroupedInvocationInput;
import uk.ac.ox.cs.epsrc.business_manager.entity.IonChannelValues;
import uk.ac.ox.cs.epsrc.business_manager.entity.Job;
import uk.ac.ox.cs.epsrc.business_manager.entity.PIC50Data;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.manager.JobManager;
import uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager;
import uk.ac.ox.cs.epsrc.business_manager.value.object.AppManagerJobProgressVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.PerJobConcentrationResultsVO;
import uk.ac.ox.cs.epsrc.business_manager.ws.app_manager.proxy.AppManagerProxy;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ResultsReadyNotificationRequest;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.AssaySpread;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApplicationStatusRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApplicationStatusResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.AssociatedItem;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.BusinessDataUploadCompletedRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ChannelData;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ObjectFactory;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.Results;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllResultsRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllResultsResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllStatusesRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllStatusesResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveDiagnosticsRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveDiagnosticsResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.SimulationDeleteRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.SimulationDeleteResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.StatusRecord;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.TypeCredibleIntervals;

/**
 * Implementation of the App Manager operations.
 *
 * @author geoff
 */
@Component(BusinessIdentifiers.COMPONENT_APP_MANAGER_MANAGER)
@DependsOn(BusinessIdentifiers.COMPONENT_CONFIGURATION)
public class AppManagerManagerImpl implements AppManagerManager {

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_APP_MANAGER_PROXY)
  private AppManagerProxy appManagerProxy;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_CONFIGURATION)
  private Configuration configuration;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_CONFIGURATION_ACTIONPOTENTIAL)
  private ConfigurationActionPotential configurationActionPotential;

  // JMS Template for sending provenance details.
  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JMS_TEMPLATE)
  private BusinessManagerJMSTemplate jmsTemplate;

  // Avoid circular dependencies by using setter
  private JobManager jobManager;

  // Avoid circular dependencies by using setter
  private SimulationManager simulationManager;

  // ApPredict/App Manager invocation source. Value as defined in WS WSDL.
  private static final String invocationSource = "business-manager";

  // Used if a warning has been issued that a job is being queued because App Manager at capacity.
  private static final Vector<Long> capacityWarnedJobs = new Vector<Long>();

  private static final ObjectFactory objectFactory = new ObjectFactory();

  // Spring property injection - spring.properties
  private String appManagerCapacityIndicator;
  private String appManagerDefaultProgressPrefix;  
  private SimpleDateFormat appManagerStatusDateFormat;
  private BigDecimal defaultHillCoefficient;
  private BigDecimal defaultSaturationLevel;
  private String defaultCredibleIntervalPctiles;

  // defined in appCtx.appManager.xml
  private Map<String, String> appManagerCodes = new HashMap<String, String>();
  // defined in appCtx.appManager.xml
  private Map<String, String> apPredictCodes = new HashMap<String, String>();
  // defined in appCtx.appManager.xml
  // Queue channel for returning ApPreDiCT invocation objects on if they were rejected due to capacity constraints.
  private QueueChannel atCapacityQueueChannel;

  // Holds confidence interval spread values k: [1]IonChannel, [2]AssayName v: AssaySpread 
  private static final MultiKeyMap spreadHolder = new MultiKeyMap();
  // Holds the credible interval percentiles.

  // Runtime assignment
  private static Short defaultPlasmaConcCount;
  private static Boolean defaultPlasmaConcLogScale;

  private static final Log log = LogFactory.getLog(AppManagerManagerImpl.class);

  @PostConstruct
  private void initialiseSpreadHolder() {
    final Set<AssayVO> assays = configuration.getAssays();
    if (assays.isEmpty()) {
      final String warnMessage = "Attempting app manager manager initialisation without any assays defined!";
      log.warn("~initialiseSpreadHolder() : ".concat(warnMessage));
    }
    for (final AssayVO assay : assays) {
      for (final AssaySpread assaySpread : assay.getAssaySpreads()) {
        spreadHolder.put(assaySpread.getIonChannel(), assay.getName(), assaySpread);
      }
    }
    final StringBuffer credibleIntervalPctiles = new StringBuffer();
    for (final BigDecimal credibleIntervalPctile : configurationActionPotential.getDefaultCredibleIntervalPctiles()) {
      credibleIntervalPctiles.append(credibleIntervalPctile.toPlainString()).append(" ");
    }
    defaultCredibleIntervalPctiles = credibleIntervalPctiles.toString().trim();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.service.ws.AppManagerService#createDiagnosticsRequest(uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ResultsReadyNotificationRequest)
   */
  @Override
  public RetrieveDiagnosticsRequest createDiagnosticsRequest(final ResultsReadyNotificationRequest notificationRequest) {
    log.debug("~createDiagnosticsRequest() : Invoked.");

    final Long appManagerId = notificationRequest.getAppManagerId();

    final RetrieveDiagnosticsRequest diagnosticsRequest = objectFactory.createRetrieveDiagnosticsRequest();
    diagnosticsRequest.setAppManagerId(appManagerId);

    return diagnosticsRequest;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.service.ws.AppManagerService#createRunRequest(java.lang.Long, uk.ac.ox.cs.epsrc.business_manager.entity.Job)
   */
  @Override
  public ApPredictRunRequest createRunRequest(final @Header(value=BusinessIdentifiers.SI_HDR_SIMULATION_ID,
                                                            required=true)
                                                    Long simulationId,
                                              final Job job) {
    log.debug("~createRunRequest() : Invoked.");

    // One-time assignment of default simulation options.
    if (defaultPlasmaConcCount == null || defaultPlasmaConcLogScale == null) {
      defaultPlasmaConcCount = configurationActionPotential.getDefaultPlasmaConcCount();
      defaultPlasmaConcLogScale = configurationActionPotential.isDefaultPlasmaConcLogScale();

      log.info("~createRunRequest() : Initialising PlasmaConcCount='" + defaultPlasmaConcCount + "', " +
                                                  "PlasmaConcLogScale='" + defaultPlasmaConcLogScale + "'.");
    }

    final Float pacingFrequency = job.getPacingFrequency();
    final String concentrations = StringUtils.join(job.getCompoundConcentrations(), ", ");

    final GroupedInvocationInput groupedInvocationInput = job.getGroupedInvocationInput();
    final String assayGroupName = groupedInvocationInput.getAssayGroupName();
    final Set<IonChannelValues> ionChannelValues = groupedInvocationInput.getIonChannelValues();

    final ApPredictRunRequest request = objectFactory.createApPredictRunRequest();
    final Simulation simulation = simulationManager.find(simulationId);
    request.setModelIdentifier(simulation.getCellModelIdentifier());
    request.setPacingMaxTime(simulation.getPacingMaxTime());
    request.setPlasmaConcLogScale(defaultPlasmaConcLogScale);

    request.setPacingFreq(new BigDecimal(pacingFrequency));
    request.setPlasmaConcsAssigned(concentrations);
    // Don't use the default plasma concentrations count unless using experimentally-derived compound concentrations.
    if (!job.isUsingConfigurationCompoundConcentrations()) {
      request.setPlasmaConcCount(defaultPlasmaConcCount);
    }

    /*
     * Pre-processing.
     * Check each source assay and ion channel to determine whether all the data for the grouping
     * has spread data.
     */
    boolean allSourceAssayAndIonChannelsSpreadable = true;

    for (final IonChannelValues eachIonChannelValues : ionChannelValues) {
      final String sourceAssayName = eachIonChannelValues.getSourceAssayName();
      final IonChannel ionChannel = IonChannel.valueOf(eachIonChannelValues.getIonChannelName());
      if (!spreadHolder.containsKey(ionChannel, sourceAssayName)) {
        log.debug("~createRunRequest() : No presence for source assay name '" + sourceAssayName + "', ion channel '" + ionChannel + "' indicates not spreadable for assay group '" + assayGroupName + "'");
        allSourceAssayAndIonChannelsSpreadable = false;
        break;
      }
    }

    /*
     * Pre-processing.
     * If all the source assay and ion channel data has spread data then check if all assigned Hill
     * values are all estimated with default values assigned.
     * If all Hill values are estimated as default values then prepare to ignore them -- which will
     * force ApPredict to use default Hill values in any spread calculations.
     */
    final Set<String> ignorableDefaultedHillIonChannels = new HashSet<String>();
    if (allSourceAssayAndIonChannelsSpreadable) {
      for (final IonChannelValues eachIonChannelValues : ionChannelValues) {
        boolean originalOrEstimatedNonOne = false;

        final List<PIC50Data> allPIC50Data = eachIonChannelValues.getpIC50Data();
        for (final PIC50Data pIC50Data : allPIC50Data) {
          final BigDecimal hill = pIC50Data.getHill();
          final boolean isOriginal = pIC50Data.getOriginal();
          if (isOriginal || 
              (!isOriginal && (hill != null && BigDecimal.ONE.compareTo(hill) != 0))) {
            // 1. Original value OR
            // 2. Estimated value AND Hill defined as non-default
            // ... so it's not ignorable.
            originalOrEstimatedNonOne = true;
            break;
          }
        }

        if (!originalOrEstimatedNonOne) {
          // If everything's estimated, and if all estimated values are 1...
          ignorableDefaultedHillIonChannels.add(eachIonChannelValues.getIonChannelName());
        }
      }
    }

    boolean setCredibleIntervals = false;
    for (final IonChannelValues eachIonChannelValues : ionChannelValues) {
      final String ionChannelName = eachIonChannelValues.getIonChannelName();
      final String sourceAssayName = eachIonChannelValues.getSourceAssayName();
      final List<PIC50Data> allPIC50Data = eachIonChannelValues.getpIC50Data();

      final ChannelData channelData = objectFactory.createChannelData();

      final boolean ignoreHillsForIonChannel = ignorableDefaultedHillIonChannels.contains(ionChannelName);

      for (final PIC50Data pIC50Data : allPIC50Data) {
        final AssociatedItem associatedItem = objectFactory.createAssociatedItem();
        final BigDecimal pIC50 = pIC50Data.getValue();
        associatedItem.setC50(pIC50);
        if (ignoreHillsForIonChannel) {
          // All Hill values for this assay / ion channel were estimated to be the default value.
          log.warn("~createRunRequest() : [" + sourceAssayName + "][" + ionChannelName + "] : Assigning Hill of null for '" + pIC50 + "'.");
          associatedItem.setHill(null);
        } else {
          final BigDecimal hill = pIC50Data.getHill();
          associatedItem.setHill(hill == null ? defaultHillCoefficient : hill);
        }
        associatedItem.setSaturation(defaultSaturationLevel);

        channelData.getPIC50Data().add(associatedItem);
      }

      final IonChannel currentIonChannel = IonChannel.valueOf(ionChannelName);

      if (allSourceAssayAndIonChannelsSpreadable) {
        // Assign an assay spread of whichever assay generated the value.
        final AssaySpread assaySpread = (AssaySpread) spreadHolder.get(currentIonChannel,
                                                                       sourceAssayName);
        if (assaySpread != null) {
          setCredibleIntervals = true;
          channelData.setC50Spread(assaySpread.getSpreadPIC50());
          if (!ignoreHillsForIonChannel) {
            channelData.setHillSpread(assaySpread.getSpreadHill());
          }
        }
      }

      switch (currentIonChannel) {
        case CaV1_2 :
          request.setICaL(channelData);
          break;
        case hERG :
          request.setIKr(channelData);
          break;
        case KCNQ1 :
          request.setIKs(channelData);
          break;
        case NaV1_5 :
          request.setINa(channelData);
          break;
        default :
          log.error("~createRunRequest() : Unrecognized ion channel current of '" + currentIonChannel + "'.");
          break;
      }
    }

    final TypeCredibleIntervals typeCredibleIntervals = objectFactory.createTypeCredibleIntervals();
    if (setCredibleIntervals) {
      typeCredibleIntervals.setCalculateCredibleIntervals(true);
      typeCredibleIntervals.setPercentiles(defaultCredibleIntervalPctiles);
    } else {
      typeCredibleIntervals.setCalculateCredibleIntervals(false);
      typeCredibleIntervals.setPercentiles(null);
    }
    request.setCredibleIntervals(typeCredibleIntervals);

    request.setInvocationSource(invocationSource);

    jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.TRACE,
                                                         MessageKey.AP_PREDICT_PREPROCESSING.getBundleIdentifier(),
                                                         new String[] { assayGroupName,
                                                                        pacingFrequency.toString(),
                                                                        concentrations },
                                                         simulationId).build());

    return request;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.ws.app_manager.manager.AppManagerManager#deleteJobs(java.util.Set)
   */
  @Override
  public Set<String> deleteJobs(final Set<String> appManagerIds) throws IllegalArgumentException {
    log.debug("~deleteJobs() : Invoked with app manager identities '" + appManagerIds + "'.");

    if (appManagerIds == null) {
      final String errorMessage = "Instruction to delete simulations from app manager contained no ids!";
      log.error("~deleteJobs() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    final Set<String> deleted = new HashSet<String>(appManagerIds.size());
    for (final String appManagerId : appManagerIds) {
      final SimulationDeleteRequest deleteRequest = objectFactory.createSimulationDeleteRequest();
      deleteRequest.setAppManagerId(Long.valueOf(appManagerId));

      final SimulationDeleteResponse deleteResponse = appManagerProxy.deleteSimulation(deleteRequest);
      deleted.add(appManagerId + " = " + deleteResponse.getResponse());
    }

    return deleted;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.ws.app_manager.AppManagerService#handleRunResponse(java.lang.Long, java.lang.Long, uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunRequest, uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunResponse)
   */
  @Override
  public void handleRunResponse(final @Header(value=BusinessIdentifiers.SI_HDR_JOB_ID,
                                              required=true)
                                      Long jobId,
                                final @Header(value=BusinessIdentifiers.SI_HDR_SIMULATION_ID,
                                              required=true)
                                      Long simulationId,
                                final @Header(value=BusinessIdentifiers.SI_HDR_RUN_REQUEST_OBJECT,
                                              required=true)
                                      ApPredictRunRequest apPredictRunRequest,
                                final ApPredictRunResponse response) {
    log.debug("~handleRunResponse() : Invoked.");

    final String responseCode = response.getResponse().getResponseCode();
    final Long appManagerId = response.getResponse().getAppManagerId();

    if (appManagerId != null) {
      jobManager.assignAppManagerIdToJob(jobId, String.valueOf(appManagerId));

      jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.TRACE,
                                                           MessageKey.APP_MANAGER_RUN_REQUEST_RESPONSE.getBundleIdentifier(),
                                                           new String[] { jobId.toString(),
                                                                          appManagerId.toString() },
                                                           simulationId).build());
    } else {
      boolean situationHandled = false;
      final String messageKey = appManagerCodes.get(responseCode);
      if (messageKey != null) {
        if (appManagerCapacityIndicator.equals(messageKey)) {
          log.debug("~handleRunResponse() : App Manager indicates that operating at capacity.");

          final Map<String, Object> messageHeader = new HashMap<String, Object>();
          messageHeader.put(BusinessIdentifiers.SI_HDR_JOB_ID, jobId);
          messageHeader.put(BusinessIdentifiers.SI_HDR_SIMULATION_ID, simulationId);
          messageHeader.put(BusinessIdentifiers.SI_HDR_RUN_REQUEST_OBJECT, apPredictRunRequest);

          atCapacityQueueChannel.send(new GenericMessage<ApPredictRunRequest>(apPredictRunRequest,
                                                                              messageHeader));
          log.debug("~handleRunResponse() : Returned original request to queue channel.");

          if (!capacityWarnedJobs.contains(jobId)) {
            // the app manager code should translate to a message key code.
            jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.INFO,
                                                                 messageKey,
                                                                 new String[] { jobId.toString() },
                                                                 simulationId).build());
            jmsTemplate.sendProgress(new JobProgress.Builder(InformationLevel.INFO,
                                                             messageKey,
                                                             new String[] { jobId.toString() },
                                                             simulationId, jobId).build());
            capacityWarnedJobs.add(jobId);
          }
          situationHandled = true;

        } else {
          // the app manager code should translate to a message key code.
          jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.WARN,
                                                               messageKey, new String[] {},
                                                               simulationId).build());
        }
      } else {
        jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.WARN,
                                                             MessageKey.APP_MANAGER_UNRECOGNISED_RESPONSE.getBundleIdentifier(),
                                                             new String[] { messageKey },
                                                             simulationId).build());
      }
      if (!situationHandled) {
        final String errorMessage = "Cannot handle a response of '" + responseCode + "' from the App Manager. Please contact the developer!";
        log.error("~handleRunResponse() : " + errorMessage); 
        jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.WARN,
                                                             errorMessage, new String[] {},
                                                             simulationId).build());
      }
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.service.ws.AppManagerService#persistDiagnostics(uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveDiagnosticsResponse)
   */
  @Override
  @Transactional(readOnly=false)
  public RetrieveAllResultsRequest persistDiagnostics(final RetrieveDiagnosticsResponse diagnosticsResponse) {
    log.debug("~persistDiagnostics() : Invoked.");

    final long appManagerId = diagnosticsResponse.getAppManagerId();
    final String info = diagnosticsResponse.getInfo();
    final String output = diagnosticsResponse.getOutput();

    jobManager.persistDiagnostics(String.valueOf(appManagerId), info, output);

    final RetrieveAllResultsRequest allResultsRequest = objectFactory.createRetrieveAllResultsRequest();
    allResultsRequest.setAppManagerId(appManagerId);

    return allResultsRequest;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.service.ws.AppManagerService#persistResults(uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllResultsResponse)
   */
  @Override
  @Transactional(readOnly=false)
  public BusinessDataUploadCompletedRequest persistResults(final RetrieveAllResultsResponse resultsResponse) {
    log.debug("~persistResults() : Invoked.");
    final long appManagerId = resultsResponse.getAppManagerId();
    final String messages = resultsResponse.getMessages();
    final String deltaAPD90PercentileNames = resultsResponse.getDeltaAPD90PercentileNames();
    final List<Results> results = resultsResponse.getResults();

    final Set<String> warningMessageKeys = new HashSet<String>();
    final Set<PerJobConcentrationResultsVO> perJobConcentrationResultsVO = new HashSet<PerJobConcentrationResultsVO>(results.size());
    for (final Results eachResults : results) {
      // Delta APD90 columns can contain message codes if there's been a calculation problem.
      final String deltaAPD90 = eachResults.getDeltaAPD90();
      if (apPredictCodes.containsKey(deltaAPD90)) {
        warningMessageKeys.add(apPredictCodes.get(deltaAPD90));
      }

      final String compoundConcentration = eachResults.getReference().toPlainString();
      final String times = eachResults.getTimes();
      final String voltages = eachResults.getVoltages();
      final String qNet = eachResults.getQNet();

      perJobConcentrationResultsVO.add(new PerJobConcentrationResultsVO(compoundConcentration,
                                                                        times,
                                                                        voltages,
                                                                        deltaAPD90,
                                                                        qNet));
    }

    final Job job = jobManager.persistResults(String.valueOf(appManagerId),
                                              messages,
                                              deltaAPD90PercentileNames,
                                              perJobConcentrationResultsVO);

    // If all simulation jobs are completed, assign simulation state accordingly
    final Long simulationId = job.getSimulationId();
    final boolean simulationCompleted = simulationManager.jobCompletedProcessing(simulationId);
    if (simulationCompleted) {

      try {
        // Sleep momentarily. Postpones UI otherwise halting querying for results!
        Thread.sleep(3000);
      } catch (InterruptedException e) {}

      jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.TRACE,
                                                           MessageKey.SYSTEM_TERMINATION_KEY.getBundleIdentifier(),
                                                           new String[] { },
                                                           simulationId)
                                                  .build());
    }
 
    if (!warningMessageKeys.isEmpty()) {
      for (final String warningMessageKey : warningMessageKeys) {
        // the ApPredict code should translate to a message key code.
        jmsTemplate.sendProgress(new JobProgress.Builder(InformationLevel.WARN,
                                                         warningMessageKey,
                                                         new String[] {},
                                                         simulationId,
                                                         job.getId()).build());
      }
    }

    // notify the app manager that it can post-process the simulation job's data.
    final BusinessDataUploadCompletedRequest completedNotificationRequest = objectFactory.createBusinessDataUploadCompletedRequest();
    completedNotificationRequest.setAppManagerId(appManagerId);

    return completedNotificationRequest;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.ws.app_manager.AppManagerService#retrieveJobProgress(uk.ac.ox.cs.epsrc.business_manager.value.AppManagerJobProgressVO, java.lang.String)
   */
  @Override
  public void retrieveJobProgress(final AppManagerJobProgressVO jobProgress,
                                  final Long appManagerId) {
    log.debug("~retrieveJobProgress() : Invoked.");

    final RetrieveAllStatusesRequest statusesRequest = objectFactory.createRetrieveAllStatusesRequest();
    statusesRequest.setAppManagerId(appManagerId);

    // Call the AppManager WS.
    final RetrieveAllStatusesResponse statusesResponse = appManagerProxy.retrieveJobProgress(statusesRequest);
    final List<StatusRecord> statusRecords = statusesResponse.getStatusRecord();

    for (final StatusRecord statusRecord : statusRecords) {
      final String entered = statusRecord.getEntered();
      Date timestamp = null;
      try {
        timestamp = appManagerStatusDateFormat.parse(entered);
      } catch (NumberFormatException e) {
        // SimpleDateFormat parsing isn't threadsafe. Valid dates, e.g. '2017:07:09 12:44:59' were generating NFEs!
        // https://stackoverflow.com/questions/21017502/numberformatexception-while-parsing-date
        log.warn("~retrieveJobProgress() : NumberFormatException from App manager status date of '" + entered + "' using '" + appManagerStatusDateFormat + "'.");
        timestamp = new Date();
      } catch (ParseException e) {
        log.warn("~retrieveJobProgress() : ParseException from App manager status date of '" + entered + "' using '" + appManagerStatusDateFormat + "'.");
        timestamp = new Date();
      }

      final String level = statusRecord.getLevel();
      InformationLevel informationLevel = null;
      if ("T".equalsIgnoreCase(level)) {
        informationLevel = InformationLevel.TRACE;
      } else if ("D".equalsIgnoreCase(level)) {
        informationLevel = InformationLevel.DEBUG;
      } else if ("I".equalsIgnoreCase(level)) {
        informationLevel = InformationLevel.INFO;
      } else if ("W".equalsIgnoreCase(level)) {
        informationLevel = InformationLevel.WARN;
      } else if ("E".equalsIgnoreCase(level)) {
        informationLevel = InformationLevel.ERROR;
      } else if ("F".equalsIgnoreCase(level)) {
        informationLevel = InformationLevel.FATAL;
      } else {
        final String errorMessage = "Unrecognized status level of '" + level + "' retrieved from app manager for id '" + appManagerId + "'.";
        log.error("~retrieveJobProgress() : " + errorMessage);
      }

      String statusText = statusRecord.getStatus();
      if (statusText != null && statusText.contains(appManagerDefaultProgressPrefix)) {
        /* Assign the status level of progress information to INFO */
        statusText = statusText.replace(appManagerDefaultProgressPrefix, "").trim();
        informationLevel = InformationLevel.INFO;
      }
      if (timestamp != null && informationLevel != null) {
        // fill up the object used to return the progress data from the routine.
        jobProgress.getProgress().add(jobProgress.new StatusVO(new Long(timestamp.getTime()).toString(),
                                                               informationLevel.toString(),
                                                               statusText));
      }
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.ws.app_manager.service.AppManagerService#retrieveWorkload()
   */
  @Override
  public String retrieveWorkload() {
    log.debug("~retrieveWorkload() : Invoked.");

    final ApplicationStatusRequest applicationStatusRequest = objectFactory.createApplicationStatusRequest();
    applicationStatusRequest.setData(APP_MANAGER_WORKLOAD_IDENTIFIER);

    final ApplicationStatusResponse applicationStatusResponse = appManagerProxy.applicationStatusRequest(applicationStatusRequest);

    return applicationStatusResponse.getStatus();
  }

  /**
   * Assign the App Manager capacity indicator.
   * 
   * @param appManagerCapacityIndicator The App Manager capacity indicator.
   */
  @Value("${app_manager.capacity_indicator}")
  public void setAppManagerCapacityIndicator(final String appManagerCapacityIndicator) {
    log.info("~setAppManagerCapacityIndicator() : Assigned value '" + appManagerCapacityIndicator + "'.");
    this.appManagerCapacityIndicator = appManagerCapacityIndicator;
  }

  /**
   * (Spring-injected) App Manager codes.
   * 
   * @param appManagerCodes App Manager codes.
   */
  // see appCtx.appManager.xml
  @Value("#{appManagerCodes}")
  public void setAppManagerCodes(final Map<String, String> appManagerCodes) {
    this.appManagerCodes.clear();
    if (appManagerCodes != null) {
      for (final Map.Entry<String, String> appManagerCode : appManagerCodes.entrySet()) {
        log.info("~setAppManagerCodes() : Key '" + appManagerCode.getKey() + "', value '" + appManagerCode.getValue() + "'.");
      }
      this.appManagerCodes.putAll(appManagerCodes);
    } else {
      log.warn("~setAppManagerCodes() : No App manager codes assigned.");
    }
  }

  /**
   * Assign the default App Manager progress prefix.
   * 
   * @param appManagerDefaultProgressPrefix Default progress prefix being used.
   */
  // Spring property injection
  @Value("${app_manager.progress_pfx}")
  public void setAppManagerDefaultProgressPrefix(final String appManagerDefaultProgressPrefix) {
    log.info("~setApPredictCodes() : Default App manager progress prefix '" + appManagerDefaultProgressPrefix + "'.");
    this.appManagerDefaultProgressPrefix = appManagerDefaultProgressPrefix;
  }

  /**
   * Assign the App manager status date format.
   * 
   * @param appManagerStatusDateFormat App manager status date format.
   */
  // Spring property injection. See spring.properties.
  @Value("${app_manager.status_date_format}")
  public void setAppManagerStatusDateFormat(final String appManagerStatusDateFormat) {
    log.info("~setAppManagerStatusDateFormat() : Expecting App manager status dates in format '" + appManagerStatusDateFormat  +"'.");
    this.appManagerStatusDateFormat = new SimpleDateFormat(appManagerStatusDateFormat);
  }

  /**
   * (Spring-injected) ApPredict codes.
   * 
   * @param apPredictCodes ApPredict codes (for when the simulation run resulted in a message).
   */
  // See appCtx.appManager.xml
  @Value("#{apPredictCodes}")
  public void setApPredictCodes(final Map<String, String> apPredictCodes) {
    this.apPredictCodes.clear();
    if (apPredictCodes != null) {
      for (final Map.Entry<String, String> apPredictCode : apPredictCodes.entrySet()) {
        log.info("~setApPredictCodes() : Key '" + apPredictCode.getKey() + "', value '" + apPredictCode.getValue() + "'.");
      }
      this.apPredictCodes.putAll(apPredictCodes);
    } else {
      log.warn("~setApPredictCodes() : No ApPredict codes assigned.");
    }
  }

  /**
   * (Spring-injected) Queue channel for re-sending invocation requests on. 
   * 
   * @param atCapacityQueueChannel Queue channel for sending invocation requests on.
   */
  // See appCtx.appManager.xml
  @Value("#{appManager_channel_toJobQueue}")
  public void setAtCapacityQueueChannel(final QueueChannel atCapacityQueueChannel) {
    this.atCapacityQueueChannel = atCapacityQueueChannel;
  }

  /**
   * Assign the default Hill coefficient value to use.
   * 
   * @param defaultHillCoefficient Default Hill coefficient to use in dose-response fitting.
   */
  // String property injection. See spring.properties.
  @Value("${fdr.default_coefficient}")
  public void setDefaultHillCoefficient(final BigDecimal defaultHillCoefficient) {
    log.info("~setDefaultHillCoefficient() : Assigning default Hill coefficient '" + defaultHillCoefficient + "'.");
    this.defaultHillCoefficient = defaultHillCoefficient;
  }

  /**
   * Assign the default Saturation Level (%) to use. Would probably be 0 (zero).
   * 
   * @param defaultSaturationLevel Default Saturation Level.
   */
  // Spring property injection. See spring.properties.
  @Value("${app_manager.default_saturation_level}")
  public void setDefaultSaturationLevel(final BigDecimal defaultSaturationLevel) {
    if (defaultSaturationLevel == null) {
      throw new NullPointerException("You must assign a default saturation level!");
    }
    log.info("~setDefaultSaturationLevel() : Assigning default Saturation Level '" + defaultSaturationLevel + "'.");
    this.defaultSaturationLevel = defaultSaturationLevel;
  }

  /**
   * Assign the job manager.
   * 
   * @param jobManager Job manager to assign.
   */
  // Setter used to avoid circular dependencies
  @Autowired
  public void setJobManager(final JobManager jobManager) {
    this.jobManager = jobManager;
  }

  /**
   * Assign the simulation manager.
   * 
   * @param simulationManager Simulation manager to assign.
   */
  // Setter used to avoid circular dependencies
  @Autowired
  public void setSimulationManager(final SimulationManager simulationManager) {
    this.simulationManager = simulationManager;
  }
}