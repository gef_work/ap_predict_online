/*

  Original work: Copyright (c) 2015, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.service;

import java.math.BigDecimal;
import java.util.List;

import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedSimulationRequestVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.SimulationRequestVO;

/**
 * Interface to simulation services.
 *
 * @author geoff
 */
public interface SimulationService {

  /**
   * Retrieve a simulation with the specified id.
   * 
   * @param simulationId Persistence simulation id.
   * @return Simulation with requested simulation id, or null if not found.
   */
  Simulation findBySimulationId(long simulationId);

  /**
   * Process a simulation.
   * <p>
   * Whereas {@link #processSimulationsRequest(List)} handles the incoming simulation request and
   * makes some early decisions about which processing is appropriate, {@link #processSimulation(long)}
   * is a second phase processing which is invoked once the simulation has been SQL-selected.
   * 
   * @param simulationId Persistence identifier of the simulation to process.
   * @return Processed simulation.
   */
  Simulation processSimulation(long simulationId);

  /**
   * Process the incoming collection of value objects representing a client request to process 
   * a simulation (or simulations).
   * 
   * @param verifiedSimulationRequests Value object collection containing details of objects to run
   *                                   simulations on.
   * @return Collection of value objects containing the incoming compound names and their associated
   *         processing-related properties (e.g. inheritance strategies, PC50 evaluation strategies).
   */
  List<ProcessedSimulationRequestVO> processSimulationsRequest(List<SimulationRequestVO> verifiedSimulationRequests);

  /**
   * Validate user input and/or assign default values then return a validated
   * object if no problems, otherwise throw the appropriate exception.
   * 
   * @param compoundIdentifier Compound identifier.
   * @param forceReRun Force re-run process.
   * @param reset Reset process.
   * @param assayGrouping Whether simulation enacts assay grouping.
   * @param valueInheriting Whether simulation enacts value inheriting.
   * @param betweenGroups Whether simulation enacts value inheriting between groups.
   * @param withinGroups Whether simulation enacts value inheriting withing groups.
   * @param cellModelIdentifier CellML Model identifier.
   * @param pacingMaxTime Maximum pacing time (in minutes).
   * @param inputDataOnly Whether the simulation request is for input data only.
   * @return Simulation request value object.
   * @throws IllegalArgumentException If illegal properties are received, e.g. a -ve max.
   *                                  pacing time or -ve cell model identifier.
   */
  SimulationRequestVO userInputValidation(final String compoundIdentifier,
                                          final Boolean forceReRun,
                                          final Boolean reset,
                                          final Boolean assayGrouping,
                                          final Boolean valueInheriting,
                                          final Boolean betweenGroups,
                                          final Boolean withinGroups,
                                          final Short cellModelIdentifier,
                                          final BigDecimal pacingMaxTime,
                                          final Boolean inputDataOnly)
                                          throws IllegalArgumentException;
}