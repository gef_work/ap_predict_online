/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedSimulationRequestVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.SimulationRequestVO;

/**
 * Implementation of the simulation service.
 *
 * @author geoff
 */
@Component(BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE)
public class SimulationServiceImpl implements SimulationService {

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_SIMULATION_MANAGER)
  private SimulationManager simulationManager;

  private static final Log log = LogFactory.getLog(SimulationServiceImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.service.SimulationService#findBySimulationId(long)
   */
  @Override
  public Simulation findBySimulationId(final long simulationId) {
    final String identifiedLogPrefix = "~findBySimulationId() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    return simulationManager.find(simulationId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.service.SimulationService#processSimulation(long)
   */
  @Override
  // See ctx/integration/simulation/appCtx.sim.processRequest.xml
  // Simulations arriving here will have had requestProcessed=true and processStatus="busy"
  public Simulation processSimulation(final long simulationId) {
    final String identifiedLogPrefix = "~processSimulation() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    Simulation simulation = findBySimulationId(simulationId);

    if (simulation.forcingReRun()) {
      simulation = simulationManager.processingForceReRun(simulation);
    } else if (simulation.forcingReset()) {
      simulation = simulationManager.processingForceReset(simulation);
    } else {
      simulation = simulationManager.processingRegular(simulation);
    }

    return simulation;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.service.SimulationService#processSimulationsRequest(java.util.List)
   */
  // Most likely to arrive here after ws/business_manager/SimulationsRequestProcessor invocation
  @Override
  public List<ProcessedSimulationRequestVO> processSimulationsRequest(final List<SimulationRequestVO> verifiedSimulationRequests) {
    log.debug("~processSimulationsRequest() : ".concat("Invoked."));

    final List<ProcessedSimulationRequestVO> requestProcessed = new ArrayList<ProcessedSimulationRequestVO>(verifiedSimulationRequests.size());
    for (final SimulationRequestVO verifiedSimulationRequest : verifiedSimulationRequests) {
      if (verifiedSimulationRequest.isInputDataOnly()) {
        throw new UnsupportedOperationException("Input data gathering operation not available in business-manager-skeleton");
      } else {
        final Simulation simulation = simulationManager.find(verifiedSimulationRequest);
        final boolean simulationExists = (simulation != null);

        if (simulationExists) {
          requestProcessed.add(simulationManager.requestProcessingExistingSimulation(simulation,
                                                                                     verifiedSimulationRequest));
        } else {
          requestProcessed.add(simulationManager.requestProcessingNewSimulation(verifiedSimulationRequest));
        }
      }
    }

    return requestProcessed;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.service.SimulationService#userInputValidation(java.lang.String, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Short, java.math.BigDecimal, java.lang.Boolean)
   */
  @Override
  public SimulationRequestVO userInputValidation(final String compoundIdentifier,
                                                 final Boolean forceReRun,
                                                 final Boolean reset,
                                                 final Boolean assayGrouping,
                                                 final Boolean valueInheriting,
                                                 final Boolean betweenGroups,
                                                 final Boolean withinGroups,
                                                 final Short cellModelIdentifier,
                                                 final BigDecimal pacingMaxTime,
                                                 final Boolean inputDataOnly)
                                                 throws IllegalArgumentException {
    log.debug("~userInputValidation() : Cmpd#[" + compoundIdentifier + "] : Invoked.");

    return simulationManager.userInputValidation(compoundIdentifier, forceReRun,
                                                 reset, assayGrouping,
                                                 valueInheriting, betweenGroups,
                                                 withinGroups,
                                                 cellModelIdentifier,
                                                 pacingMaxTime, inputDataOnly);
  }
}