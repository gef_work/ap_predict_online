/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * The results of simulation jobs held as a per- compound concentration object.<br>
 * There may be, for example, 12 of these JobResults (concentrations) for a <code>Simulation</code>
 * 's (per-pacing frequency) <code>Job</code>, e.g. for concentrations 0.0, 0.001, 0.003 ... 33.0,
 * 100.0.
 *
 * @author geoff
 */
@Entity
@Table(uniqueConstraints = {
  @UniqueConstraint(columnNames = { "jc_jobid", "compoundConcentration" },
                    name="unique_jobid_cmpdconc")
})
public class JobResult {

  // Persistence identity (Surrogate primary key)
  @TableGenerator(name="jobresult_id_gen", table="sequence_pks_business_manager",
                  pkColumnName="pk_seq_name", pkColumnValue="jobresult_id",
                  valueColumnName="pk_seq_value", allocationSize=5)
  @Id @GeneratedValue(strategy=GenerationType.TABLE, generator="jobresult_id_gen")
  private Long id;

  // bidirectional Job [1..*] <-> [1] JobResult
  @ManyToOne
  @JoinColumn(name="jc_jobid", insertable=true, nullable=false, updatable=false)
  @org.hibernate.annotations.ForeignKey(name="fk_jobresult_jobid")
  private Job job;

  @Column(insertable=true, nullable=false, updatable=false)
  private Float compoundConcentration;

  /* Sometimes a delta APD90 value may not be possible to calculate, in which
     case a string such as "NoActionPotential_2,NoActionPotential_2,NoActionPotential_2"
     may be written to this column (if spreads are used)! */
  @Column(insertable=true, length=1000, nullable=true, updatable=false)
  private String deltaAPD90;

  @Column(insertable=true, length=50, nullable=true, updatable=false)
  private String upstrokeVelocity;

  // String of non-quoted csv times
  @Lob
  @Column(insertable=true, length=16384, nullable=false, updatable=false)
  private String times;

  // String of non-quoted csv voltages
  @Lob
  @Column(insertable=true, length=16384, nullable=false, updatable=false)
  private String voltages;

  @Column(insertable=true, length=1000, nullable=true, updatable=false)
  private String qNet;

  @Transient
  private static final Log log = LogFactory.getLog(JobResult.class);

  /** <b>Do not invoke directly.</b> */
  protected JobResult() {}

  /**
   * Initialising constructor.
   * 
   * @param job Parent job (to establish bidirectional relationship).
   * @param compoundConcentration Compound concentration.
   * @param deltaAPD90 Delta APD90 value.
   * @param upstrokeVelocity Upstroke velocity.
   * @param times AP times.
   * @param voltages AP voltages.
   * @param qNet (Optional) qNet.
   */
  public JobResult(final Job job, final Float compoundConcentration,
                   final String deltaAPD90, final String upstrokeVelocity,
                   final String times, final String voltages, final String qNet) {
    log.debug("~JobResult() : Invoked.");

    // establish bi-directional relationship
    job.addJobResult(this);
    this.job = job;

    this.compoundConcentration = compoundConcentration;
    this.deltaAPD90 = deltaAPD90;
    this.upstrokeVelocity = upstrokeVelocity;
    this.times = times;
    this.voltages = voltages;
    this.qNet = qNet;
  }


  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "JobResult [id=" + id + ", job=" + job + ", compoundConcentration="
        + compoundConcentration + ", deltaAPD90=" + deltaAPD90
        + ", upstrokeVelocity=" + upstrokeVelocity + ", times=" + times
        + ", voltages=" + voltages + ", qNet=" + qNet + "]";
  }

  /**
   * Retrieve the (surrogate primary) key job result identifier.
   * 
   * @return Job result identifier.
   */
  public Long getId() {
    return id;
  }

  /**
   * @return the job
   */
  public Job getJob() {
    return job;
  }

  /**
   * @return the compoundConcentration
   */
  public Float getCompoundConcentration() {
    return compoundConcentration;
  }

  /**
   * @return the deltaAPD90
   */
  public String getDeltaAPD90() {
    return deltaAPD90;
  }

  /**
   * @return the upstrokeVelocity
   */
  public String getUpstrokeVelocity() {
    return upstrokeVelocity;
  }

  /**
   * @return the times
   */
  public String getTimes() {
    return times;
  }

  /**
   * @return the voltages
   */
  public String getVoltages() {
    return voltages;
  }

  /**
   * Retrieve the qNet data.
   * 
   * @return qNet data, or {@code null} if not calculated.
   */
  public String getQNet() {
    return qNet;
  }
}
