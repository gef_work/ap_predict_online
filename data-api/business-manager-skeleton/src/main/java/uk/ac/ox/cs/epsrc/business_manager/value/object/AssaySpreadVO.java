/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object;

import java.math.BigDecimal;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.AssaySpread;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 * Confidence interval spread value object.
 * 
 * @author geoff
 */
public class AssaySpreadVO implements AssaySpread {

  private static final long serialVersionUID = 125110128880455836L;

  private final IonChannel ionChannel;
  private final BigDecimal IC50;
  private final BigDecimal pIC50;
  private final BigDecimal hill; 

  /**
   * Initialising constructor.
   * 
   * @param ionChannel Ion channel.
   * @param IC50 IC50 spread.
   * @param pIC50 pIC50 spread.
   * @param hill Hill spread.
   */
  public AssaySpreadVO(final IonChannel ionChannel, final BigDecimal IC50, final BigDecimal pIC50,
                       final BigDecimal hill) {
    if (ionChannel == null) {
      throw new IllegalStateException("Assay spread must have an ion channel value assigned");
    }
    this.ionChannel = ionChannel;
    this.IC50 = IC50;
    this.pIC50 = pIC50;
    this.hill = hill;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "AssaySpreadVO [ionChannel=" + ionChannel + ", IC50=" + IC50
        + ", pIC50=" + pIC50 + ", hill=" + hill + "]";
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.AssaySpread#getIonChannel()
   */
  @Override
  public IonChannel getIonChannel() {
    return ionChannel;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.AssaySpread#getSpreadHill()
   */
  @Override
  public BigDecimal getSpreadHill() {
    return hill;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.AssaySpread#getSpreadIC50()
   */
  @Override
  public BigDecimal getSpreadIC50() {
    return IC50;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.AssaySpread#getSpreadPIC50()
   */
  @Override
  public BigDecimal getSpreadPIC50() {
    return pIC50;
  }
}