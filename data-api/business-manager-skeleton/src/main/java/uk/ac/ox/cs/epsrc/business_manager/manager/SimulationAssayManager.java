/*

  Original work: Copyright (c) 2015, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.manager;

import java.util.List;

import uk.ac.ox.cs.epsrc.business_manager.entity.SimulationAssay;
import uk.ac.ox.cs.epsrc.business_manager.value.object.workflow.OutcomeVO;

/**
 * Interface to SimulationAssay manager implementation.
 *
 * @author geoff
 */
public interface SimulationAssayManager {

  /**
   * Create transient {@link SimulationAssay}s from the <i>QSAR</i> and/or
   * <i>Screening</i> data.
   * 
   * @param simulationId Simulation identifier.
   * @param qsarData <i>QSAR</i> data.
   * @param screeningData <i>Screening</i> data.
   * @return Transient simulation assays objects.
   */
  List<SimulationAssay> createTransient(long simulationId,
                                        List<OutcomeVO> qsarData,
                                        List<OutcomeVO> screeningData);

  /**
   * Persist {@link SimulationAssay} (and dependent) objects from the <i>Screening</i> data.
   * 
   * @param transientSimulationAssays Transient <tt>SimulationAssay</tt> collection.
   * @return The persisted <tt>SimulationAssay</tt> objects (one per assay).
   */
  public List<SimulationAssay> persistSimulationAssays(List<SimulationAssay> transientSimulationAssays);
}