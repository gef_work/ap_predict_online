/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.dao;

import java.util.List;
import java.util.Set;

import uk.ac.ox.cs.epsrc.business_manager.entity.Job;

/**
 * Job data access object.
 *
 * @author geoff
 */
public interface JobDAO {

  /**
   * Assign an app manager identifier to the job.
   * 
   * @param jobId Job to update.
   * @param appManagerId App manager identifier.
   */
  void assignAppManagerIdToJob(long jobId, String appManagerId);

  /**
   * Retrieve the job with the specified id.
   * 
   * @param jobId Persistence job id.
   * @return Job with requested id, or {@code null} if not found.
   */
  Job findByJobId(long jobId);

  /**
   * Find the Job which has the App manager identifier specified.
   * 
   * @param appManagerId App manager identifier.
   * @return Job assigned with App manager identifier, otherwise {@code null} if not found.
   */
  Job findJobByAppManagerId(String appManagerId);

  /**
   * Retrieve the jobs associated with the simulation.
   * 
   * @param simulationId Simulation identifier.
   * @return Jobs associated with the simulation, or empty collection if none found.
   */
  List<Job> findJobsBySimulationId(long simulationId);

  /**
   * Retrieve the count of the incomplete jobs for a simulation.
   * 
   * @param simulationId Simulation identifier.
   * @return Count of incomplete jobs.
   */
  long incompleteJobsForSimulationCount(long simulationId);

  /**
   * Remove the specified jobs.
   * 
   * @param jobs Jobs to remove.
   * @return {@code true} if jobs removed, otherwise {@code false}.
   */
  boolean removeJobs(List<Job> jobs);

  /**
   * Save a Job.
   * 
   * @param job Job to save.
   */
  void save(Job job);

  /**
   * Persist the values to be submitted for job invocation.
   * 
   * @param jobs Value holding objects.
   */
  void storeJobs(Set<Job> jobs);

  /**
   * Persist the job results.
   * 
   * @param job Job containing new results to store.
   * @param assignCompleted Assign the job as being completed (or not).
   */
  void storeJobResults(Job job, boolean assignCompleted);
}