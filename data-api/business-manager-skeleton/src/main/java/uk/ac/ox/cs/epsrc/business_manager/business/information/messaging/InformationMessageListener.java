/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.information.messaging;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.Provenance;
import uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager;

/**
 * Listener for JMS messages.
 *
 * @author geoff
 */
public class InformationMessageListener implements MessageListener {

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_INFORMATION_MANAGER)
  private InformationManager informationManager;

  private static final Log log = LogFactory.getLog(InformationMessageListener.class);

  /* (non-Javadoc)
   * @see javax.jms.MessageListener#onMessage(javax.jms.Message)
   */
  @Override
  public void onMessage(final Message message) {
    log.trace("~onMessage() : Invoked vvv '" + Thread.currentThread().getName() + "'.");
    if (message != null) {
      if (message instanceof ObjectMessage) {
        log.trace("~onMessage() : Message received.");
        final ObjectMessage objectMessage = (ObjectMessage) message;
        try {
          final Object nestedObject = objectMessage.getObject();
          if (nestedObject instanceof Provenance) {
            final Provenance provenance = (Provenance) nestedObject;
            log.trace("~onMessage() : Message object is provenance.");
            informationManager.recordProvenance(provenance);
          } else if (nestedObject instanceof Progress) {
            final Progress progress = (Progress) nestedObject;
            log.trace("~onMessage() : Message object is progress.");
            informationManager.recordProgress(progress);
          } else {
            final String error = "TODO : Unknown serialized object used in messaging '" + nestedObject.toString() + "'.";
            log.error("~onMessage() : " + error);
            System.out.println(error);
            throw new UnsupportedOperationException(error);
          }
        } catch (JMSException e) {
          final String error = "JMS Exception '" + e.getMessage() + "'.";
          log.error(error);
          System.out.println(error);
          throw new UnsupportedOperationException(error);
        }
      } else {
        final String error = "Unrecognised Message type of '" + message.getClass() + "' received.";
        log.error(error);
        System.out.println(error);
        throw new UnsupportedOperationException(error);
      }
    } else {
      final String debug = "Message was null";
      log.error("~onMessage() : " + debug);
      System.out.println(debug);
    }
  }
}