<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:jdbc="http://www.springframework.org/schema/jdbc"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.2.xsd
                           http://www.springframework.org/schema/jdbc http://www.springframework.org/schema/jdbc/spring-jdbc-3.2.xsd
                           http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-3.2.xsd">

  <description>
    <![CDATA[
    Business manager main Spring database-related configuration context.
    ]]>
  </description>

  <bean id="businessTransactionManager"
        autowire="byName"
        class="org.springframework.orm.jpa.JpaTransactionManager">
    <description>
      <![CDATA[
      PlatformTransactionManager implementation for a single JPA EntityManagerFactory. Binds a
        JPA EntityManager from the specified factory to the thread, potentially allowing for one
        thread-bound EntityManager per factory. SharedEntityManagerCreator and JpaTemplate are
        aware of thread-bound entity managers and participate in such transactions automatically.
        Using either is required for JPA access code supporting this transaction management 
        mechanism.
      ]]> 
    </description>
    <property name="entityManagerFactory" ref="businessEntityManagerFactory" />
    <property name="dataSource" ref="businessDataSource" />
  </bean>

  <tx:annotation-driven transaction-manager="businessTransactionManager" />

  <!-- Note: Place nested <beans> definitions at the end of the xml -->

  <beans profile="business_manager_embedded">
    <description>
      <![CDATA[
      Spring profile for embedded databases.
      ]]>
    </description>

    <bean id="businessEntityManagerFactory"
          autowire="byName"
          class="org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean">
      <property name="dataSource" ref="businessDataSource" />
      <property name="jpaVendorAdapter">
        <bean class="org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter" />
      </property>
      <!-- Using an unconventional location (i.e. not META-INF/persistence.xml) to emphasise usage -->
      <property name="persistenceXmlLocation"
                value="classpath:META-INF/data/business_manager-persistence.embedded.xml" />
    </bean>

    <jdbc:embedded-database id="businessDataSource">
      <jdbc:script location="classpath:org/springframework/integration/jdbc/schema-hsqldb.sql" />
    </jdbc:embedded-database>
  </beans>

  <beans profile="business_manager_mysql,business_manager_oracle10g">
    <description>
      <![CDATA[
      Spring profile for MySQL or Oracle databases.
      ]]>
    </description>

    <bean id="businessDataSource"
          class="org.springframework.jdbc.datasource.DriverManagerDataSource">
      <description>
        <![CDATA[
        Bean which uses the specified driver class to communicate with the underlying persistence/
        database.
        ]]>
      </description>
      <property name="driverClassName" value="${business_manager.database.driverClassName:appCtx.database.business_manager.xml_unassigned}" />
      <property name="url" value="${business_manager.database.url:appCtx.database.business_manager.xml_unassigned}" />
      <property name="username" value="${business_manager.database.username:appCtx.database.business_manager.xml_unassigned}" />
      <property name="password" value="${business_manager.database.password:appCtx.database.business_manager.xml_unassigned}" />
    </bean>
  </beans>

  <beans profile="business_manager_mysql">
    <description>
      <![CDATA[
      Spring profile for MySQL databases.
      ]]>
    </description>

    <bean id="businessEntityManagerFactory"
          autowire="byName"
          class="org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean">
      <description>
        <![CDATA[
        FactoryBean that creates a JPA EntityManagerFactory according to JPA's standard container
          bootstrap contract.
        ]]>
      </description>
      <property name="dataSource" ref="businessDataSource" />
      <property name="jpaVendorAdapter">
        <bean class="org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter" />
      </property>
      <!-- Using an unconventional location (i.e. not META-INF/persistence.xml) to emphasise usage -->
      <property name="persistenceXmlLocation"
                value="classpath:META-INF/data/business_manager-persistence.xml" />
    </bean>

    <jdbc:initialize-database data-source="businessDataSource" ignore-failures="DROPS">
      <jdbc:script location="classpath:org/springframework/integration/jdbc/schema-drop-mysql.sql" />
      <jdbc:script location="classpath:META-INF/sql/schema-mysql-business-manager.sql" />
    </jdbc:initialize-database>
  </beans>

  <beans profile="business_manager_oracle10g">
    <description>
      <![CDATA[
      Spring profile for Oracle 10g databases.
      ]]>
    </description>

    <bean id="businessEntityManagerFactory"
          autowire="byName"
          class="org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean">
      <description>
        <![CDATA[
        FactoryBean that creates a JPA EntityManagerFactory according to JPA's standard container
          bootstrap contract.
        ]]>
      </description>
      <property name="dataSource" ref="businessDataSource" />
      <property name="jpaVendorAdapter">
        <bean class="org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter" />
      </property>
      <!-- Using an unconventional location (i.e. not META-INF/persistence.xml) to emphasise usage -->
      <property name="persistenceXmlLocation"
                value="classpath:META-INF/data/business_manager-persistence.oracle10g.xml" />
    </bean>

    <jdbc:initialize-database data-source="businessDataSource" ignore-failures="DROPS">
      <jdbc:script location="classpath:org/springframework/integration/jdbc/schema-drop-oracle10g.sql" />
      <jdbc:script location="classpath:org/springframework/integration/jdbc/schema-oracle10g.sql" />
    </jdbc:initialize-database>
  </beans>

</beans>