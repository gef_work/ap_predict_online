#!/usr/bin/perl

#  Copyright (c) 2015, University of Oxford.
#  All rights reserved.
#
#  University of Oxford means the Chancellor, Masters and Scholars of the
#  University of Oxford, having an administrative office at Wellington
#  Square, Oxford OX1 2JD, UK.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are met:
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#   * Neither the name of the University of Oxford nor the names of its
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
#  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
#  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
#  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# An HTTP 500 (Internal Server Error) could be generated by *at least* the following :
#   1. Incorrect WSS username and password.
#   2. Server is using a self-signed certificate ("certificate verify failed") 
#
# Usually switching to using the SOAP::LITE 'trace' option and re-running provides
#   additional detail. In the case of certificate verification failure uncomment
#   the 'BEGIN' instruction below.
# Another source of information is the server logs.

#BEGIN { $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0 }

use strict;
use warnings;
#use Data::Dumper;
use JSON qw ( decode_json );
use Getopt::Long;
use Pod::Usage;
use SOAP::Lite;
#use SOAP::Lite qw ( trace );
use Digest::SHA1;
use MIME::Base64;
use Text::CSV_XS;

my @args;

my $wsdl,
my $cmpd_number;
my $file = "assay_input.csv";
my $assay_grouping = (1 == 1);
my $value_inheriting = (1 == 1);
my $value_inheriting_between_groups = (1 == 1);
my $value_inheriting_within_groups = (1 == 1);
my $dose_response_fitting = "IC50_ONLY";
my $dose_response_fitting_rounding = (1 == 1);
my $dose_response_fitting_hill_max;
my $dose_response_fitting_hill_min;
my $strategies = "0,1,2,3,4,5,6";
my $help = (1 == 0);
my $verbose = (1 == 0);

GetOptions( 'w|wsdl=s' => \$wsdl,
            'c|cmpd=s' => \$cmpd_number,
            'f|file=s' => \$file,
            'ag|assay_grouping' => \$assay_grouping,
            'vi|value_inheriting' => \$value_inheriting,
            'vibg|value_inheriting_between_groups' => \$value_inheriting_between_groups,
            'viwg|value_inheriting_within_groups' => \$value_inheriting_within_groups,
            'drf|dose_response_fitting=s' => \$dose_response_fitting,
            'drfr|dose_response_fitting_rounding' => \$dose_response_fitting_rounding,
            'drfhmax|dose_response_fitting_hill_max' => \$dose_response_fitting_hill_max,
            'drfhmin|dose_response_fitting_hill_min' => \$dose_response_fitting_hill_min,
            'p|pic50_strategies=s' => \$strategies,
            'h|help' => \$help,
            'v|verbose' => \$verbose);

pod2usage(1) if $help;

if ($verbose) {
  print "\n";
  print "Input breakdown :\n";
  print "\n";
  print "  w|wsdl                                  : $wsdl\n";
  if ($cmpd_number) {
    print "  c|cmpd                                  : $cmpd_number\n";
  }
  print "  f|file                                  : $file\n";
  print "  ag|assay_grouping                       : $assay_grouping\n";
  print "  vi|value_inheriting                     : $value_inheriting\n";
  print "  vibg|value_inheriting_between_groups    : $value_inheriting_between_groups\n";
  print "  viwg|value_inheriting_within_groups     : $value_inheriting_within_groups\n";
  print "  drf|dose_response_fitting               : $dose_response_fitting\n";
  print "  drfr|dose_response_fitting_rounding     : $dose_response_fitting_rounding\n";
  if ($dose_response_fitting_hill_max) {
    print "  drfhmax|dose_response_fitting_hill_max  : $dose_response_fitting_hill_max\n";
  }
  if ($dose_response_fitting_hill_min) {
    print "  drfhmin|dose_response_fitting_hill_min  : $dose_response_fitting_hill_min\n";
  }
  print "  p|pic50_strategies                      : $strategies\n";
  print "  h|help                                  : $help\n";
  print "  v|verbose                               : $verbose\n";
  print "\n";
  print "  If you wish to view the SOAP correspondance in details use :\n";
  print "    use SOAP::Lite +trace;\n";
  print "\n";
}

if ((!$wsdl) || !($cmpd_number)) {
  print "\n";
  print "The following are the minimum paramters to be specified :\n";
  print "  1. WSDL <-w|--wsdl>, e.g. http://localhost:18080/site_business-0.0.2/bws/\n";
  print "  2. Compound identifier <-c|--cmpd>, e.g. cmpd_100\n";
  print "\n";
  print "Enter '-h' for help\n";
  print "\n";
  exit;
}

my $soap = SOAP::Lite -> uri ('http://www.cs.ox.ac.uk/epsrc/business_manager/ws/1')
                      -> proxy ($wsdl)
                      -> on_fault (sub {
                                     my ($soap, $result) = @_;
                                     if (ref $result) {
                                       print "result\n";
                                     } else {
                                       if ($soap->transport->code == 500) {
                                         die "\nHTTP 500 (Internal Server Error) response returned!\nRead the comments at the top of this script!\n\n";
                                       } else {
                                         die $soap->transport->code."\n";
                                       }
                                     }
                                   });

# <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
#   <SOAP-ENV:Header/>
#   <SOAP-ENV:Body>
#     <ns2:ProcessSimulationsRequest xmlns:ns2="http://www.cs.ox.ac.uk/epsrc/business_manager/ws/1">
#       <ns2:SimulationDetail AssayGrouping="true" BetweenGroups="true" DoseResponseFitting="IC50_ONLY"
#                             DoseResponseFittingHillMax="1.0" DoseResponseFittingHillMin="0.0"
#                             DoseResponseFittingRounding="true" ForceReRun="false" Reset="false"
#                             Strategies="0,1,2,3,4,5,6" ValueInheriting="true" WithinGroups="true">
#         <ns2:CompoundIdentifier>cmpd2</ns2:CompoundIdentifier>
#       </ns2:SimulationDetail>
#     </ns2:ProcessSimulationsRequest>
#   </SOAP-ENV:Body>
# </SOAP-ENV:Envelope>

#my $username = "<your WSS username>";
#my $password = "<your WSS password>";

my $nonce = create_generator("random_text", int(1000*rand()));

my $timestamp = timestamp();

my $passwordDigest =  Digest::SHA1::sha1( $nonce . $timestamp . $password );
my $passwordHash = MIME::Base64::encode_base64($passwordDigest, "");

my $nonceHash = MIME::Base64::encode_base64($nonce, "");

my $securityHeader = SOAP::Header->name("wsse:Security")
                                 ->attr({'mustUnderstand'=>1,
                                         'xmlns:wsse'=>'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'});
my $userToken = SOAP::Header->name("wsse:UsernameToken" =>
                                   \SOAP::Header->value(
                                     SOAP::Header->name('wsse:Username')
                                                 ->value($username)
                                                 ->type(''),
                                     SOAP::Header->name('wsse:Password')
                                                 ->value($passwordHash)
                                                 ->type('')
                                                 ->attr({'Type'=>
                                                  'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest'}),
                                     SOAP::Header->name('wsse:Nonce')
                                                 ->value($nonceHash)
                                                 ->type('')
                                                 ->attr({'EncodingType'=>
                                                  'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary'}),
                                     SOAP::Header->name('wsu:Created')
                                                 ->value($timestamp)
                                                 ->type('')))
                          ->attr({'xmlns:wsu'=>'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'});

my $simulation_detail = SOAP::Data->new();
$simulation_detail->name('SimulationDetail');
$simulation_detail->attr( { AssayGrouping => 'false', 'InputDataOnly' => 'true' } );

my $compound_identifier = SOAP::Data->new();
$compound_identifier->name('CompoundIdentifier');
$compound_identifier->value($cmpd_number);

$simulation_detail->value(\$compound_identifier);

my $response = $soap->ProcessSimulationsRequest($securityHeader->value(\$userToken),
                                                $simulation_detail);

if ($response->fault) {
  print "** FAULT ** : ".$response->faultstring()."\n";
} else {
# <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
#   <SOAP-ENV:Header/>
#   <SOAP-ENV:Body>
#     <ns2:ProcessSimulationsResponse xmlns:ns2="http://www.cs.ox.ac.uk/epsrc/business_manager/ws/1">
#       <ns2:ProcessSimulationsResponseStructure>
#         <ns2:CompoundIdentifier>cmpd1</ns2:CompoundIdentifier>
#         <ns2:SimulationId>-1</ns2:SimulationId>
#         <ns2:InputData>
# { "experimentalPerFrequencyConcentrations" : { "0.500" : [ 0.000, 0.300, 1.000, 3.000, 10.000 ] },
#   "processedData" : [ { "simulationId" : -99,
#                         "ionChannelValues" : [ { "ionChannelName" : "NaV1_5",
#                                                  "pIC50Data" : [ { "value" : 4.200, "hill" : null } ],
#                                                  "sourceAssayName" : "QSAR"  },
#                                                { "ionChannelName" : "CaV1_2",
#                                                  "pIC50Data" : [ { "value" : 4.100, "hill" : null } ],
#                                                  "sourceAssayName" : "QSAR" } ],
#                         "assayGroupLevel" : 0,
#                         "assayGroupName" : "QSAR"
#                       },
#                       { "simulationId" : -99,
#                         "ionChannelValues" : [ .. 
#                                                { "ionChannelName" : "CaV1_2",
#                                                  "pIC50Data" : [ { "value" : 4, "hill" : null }, { "value" : 4, "hill" : null } ],
#                                                  "sourceAssayName" : "Quattro_FLIPR" },
#                                                { "ionChannelName" : "KCNQ1", ....

#  print Dumper $response->result, "\n";

  open my $tsv_file, ">>", $file or die "Opening file ($file) error: ".$!."\n";

  my $input_data = $response->valueof("//InputData");
  my $json = decode_json($input_data);

  my $experimental_per_frequency_concentrations = $json->{'experimentalPerFrequencyConcentrations'};
  foreach my $frequency (keys %$experimental_per_frequency_concentrations) {
    my @concentrations = @{$experimental_per_frequency_concentrations->{ $frequency }};
    my $concs = join('', map { "[$_]" } @concentrations);
    if ($verbose) {
      print "\n";
      print "$frequency  : $concs\n";
    }
  }

  # Ordered by equivalent appearance on the client screen
  my @ordered_ion_channel_names = ( "hERG", "CaV1_2", "NaV1_5", "KCNQ1" );
  my @processed_data = @{$json->{'processedData'}};

  # Sort the processed data by Group Level
  my @processed_sorted_by_group_level = sort { $a->{'assayGroupLevel'} <=> $b->{'assayGroupLevel'} } @processed_data;
  foreach (@processed_sorted_by_group_level) {
    my $each_processed_data = $_;

    my $simulation_id = $each_processed_data->{'simulationId'};
    my @ion_channel_values = @{$each_processed_data->{'ionChannelValues'}};
    # Group level, e.g. 0, 1, 2, 3
    my $group_level = $each_processed_data->{'assayGroupLevel'};
    # Group name, e.g. Iwks/FLIPR/Barracuda
    my $group_name = $each_processed_data->{'assayGroupName'};

    if ($verbose) {
      print "\n";
      print "[$group_level] $group_name :\n";
      print "\n";
    }
    my @tsv_line = ();
    push(@tsv_line, $cmpd_number);
    push(@tsv_line, $group_level);
    push(@tsv_line, $group_name);

    my %values = ();
    foreach (@ion_channel_values) {
      my $each_ion_channel_value = $_;

      # Ion channel names : NaV1_5, CaV1_2, hERG, KCNQ1
      my $ion_channel_name = $each_ion_channel_value->{'ionChannelName'};
      my @pIC50Data = @{$each_ion_channel_value->{'pIC50Data'}};
      my $source_assay_name = $each_ion_channel_value->{'sourceAssayName'};

      if ($verbose) {
        print "  $ion_channel_name values [src: $source_assay_name]\n";
      }

      my @pIC50sArray = ();
      foreach (@pIC50Data) {
        my $each_pIC50Data = $_;

        my $value = $each_pIC50Data->{'value'};
        # Assign default Hill Coefficient of "1" if not assigned.
        my $hill = $each_pIC50Data->{'hill'} || "1";

        if ($verbose) {
          print "    $value : $hill\n";
        }
        push(@pIC50sArray, "$value|$hill");
      }
      my $pIC50s = join(",", @pIC50sArray);

      $values{$ion_channel_name} = $pIC50s;
    }

    foreach (@ordered_ion_channel_names) {
      my $ordered_ion_channel_name = $_;
      # Assign "Not Available" no pIC50s for Ion Channel.
      my $line = $values{$ordered_ion_channel_name} || "N/A";
      push(@tsv_line, "$line");
    }

    # create the TSV effect
    my $line = join("\t", @tsv_line);
    print $tsv_file "$line\n";
  }

  close $tsv_file or die "Closing file ($file) error: ".$!."\n";

  print "** SUCCESS **\n";
  print "** Output written to $file **\n";

}

sub timestamp {
    my ($sec,$min,$hour,$mday,$mon,$year,undef,undef,undef) = gmtime(time);
    $mon++;
    $year = $year + 1900;
    return sprintf("%04d-%02d-%02dT%02d:%02d:%02dZ",$year,$mon,$mday,$hour,$min,$sec);
}

sub create_generator {
    my ($name,$start_with) = @_;
    my $i = $start_with;
    return sub {  $name . ++$i; };
}

__END__

=head1 NAME

Using this program.

=head1 SYNOPSIS

<prog> [options]

 Options:

   -w | --wsdl
   -c | --cmpd
   -f | --file
   -ag | --assay_grouping
   -vi | --value_inheriting
   -vibg | --value_inheriting_between_groups
   -viwg | --value_inheriting_within_groups
   -drf | --dose_response_fitting
   -drfr | --dose_response_fitting_rounding
   -drfhmax | --dose_response_fitting_hill_max
   -drfhmin | --dose_response_fitting_hill_min
   -p | --pic50_strategies
   -h | --help
   -v | --verbose
  
=head1 OPTIONS

=over 8

=item B<-w|--wsdl>

Business Manager web service interface URL, e.g. http://localhost:18080/site_business-0.0.2/bws/

Default value: None - This value must be provided.

=item B<-c|--cmpd>

Compound identifier.

Default value: None - This value must be provided.

=item B<-f|--file>

Output file for *appending* the results to.

Default value: assay_input.csv

=item B<-ag|--assay_grouping>

Flag to indicate if assay grouping is to be switched on.

Default value: true

=item B<-vi|--value_inheriting>

Flag to indicate if input values are to be inherited between and or
within assay groups. If grouping is switched off (which will render
the following redundant : -vibg|--value_inheriting_between_groups,
-viwg|--value-inheriting-within-groups) inheritence will apply to 
the assays rather than assay groups.

Default value: true

=item -B<-vibg|--value_inheriting_between_groups>

Flag to indicate if input values are inherited between assay groups.
This flag is redundant if <-vi|--value-inheriting> is not true.

Default value: true

=item -B<-viwg|--value_inheriting_within_groups>

Flag to indicate if input values are inherited within assay groups.
This flag is redundant if <-vi|--value-inheriting> is not true.

Default value: true

=item -B<-drf|--dose_response_fitting>

Dose-Response fitting strategy. The value has historically been
one of two values, IC50_ONLY or IC50_AND_HILL.
For the latest values the WSDL to the Dose-Response fitting should
be the first source of information (assuming they're updated!)

Default value: IC50_ONLY

=item -B<-drfr|--dose_response_fitting_rounding>

Flag to indicate if Dose-Response fitted results are to be 
rounded to pIC50 0 (zero) if the fitting returns a value which
would result in a negative pIC50.

Default value: true

=item -B<-drfhmax|--dose_response_fitting_hill_max>

If a Dose-Response fitting strategy involving Hill Coefficients 
has been selected then the value assigned here will constrain the
maximum Hill Coefficient value to use.

No default value 

=item -B<-drfhmin|--dose_response_fitting_hill_min>

If a Dose-Response fitting strategy involving Hill Coefficients
has been selected then the value assigned here will constrain the
minimum Hill Coefficient value to use.

No default value

=item -B<-p|--pic50_strategies>

PIC50 evaluation strategies to use.
The strategy numbers are defined by the site as part of the site's 
configuration whereby the application will attempt to determine 
the pIC50 to use via a number of numbered methods, e.g. looking 
for an equality modifier in summary data records, and/or using the
collection of dose-response values derived from the screening 
results.

Default value: 0,1,2,3,4,5,6

=back

=head1 DESCRIPTION

B<This program> will query a business manager web service to retrieve 
simulation input values based on the requested properties.

=cut
