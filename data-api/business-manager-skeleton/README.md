# Action Potential prediction -- Skeleton Business Manager

Reduced functionality `business-manager`. The idea being that rather than have
a full-functionality `business-manager` doing all things like dose-response-fitting,
pIC50 evaluation strategies, experimental data retrieval, etc, instead a site
provides a "data API" (a bit like `rest-data-api`) which does all that work.

The overlaid `site-business-rest` will actually do the call out to the "data API".