/*

  Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Nottingham nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.nottingham.mathematics.rest_data_api.manager;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.w3c.dom.NodeList;

import uk.ac.nottingham.mathematics.rest_data_api.exception.RestCallException;
import uk.ac.nottingham.mathematics.rest_data_api.util.Util;

/**
 *
 *
 * @author geoff
 */
@Component("restManager")
public class RestManagerImpl implements RestManager {

  private static final String myNamespace = "";
  private static final String myNamespaceURI = "http://www.cs.ox.ac.uk/epsrc/business_manager/ws/1";
  private static final String soapAction = "ProcessSimulationsRequest";

  @Value("${idg.urlhost}")
  private String idgUrlHost;

  @Value("${idg.urlpath}")
  private String idgUrlPath;

  @Value("${idg.username}")
  private String idgUsername;

  @Value("${idg.password}")
  private String idgPassword;

  @Value("${idg.nonce}")
  private String idgNonce;

  @Value("${idg.created}")
  private String idgCreated;

  @Value("${idg.connectTimeout}")
  private int idgConnectTimeout;

  @Value("${idg.readTimeout}")
  private int idgReadTimeout;

  @Value("${idg.pc50EvaluationStrategies}")
  private String pc50EvaluationStrategies;

  /* (non-Javadoc)
   * @see uk.ac.nottingham.mathematics.rest_data_api.manager.RestManager#callIdg(java.lang.String)
   */
  public String callIdg(final String id) throws RestCallException {
    String idgResponse = null;
    try {
      final SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
      final SOAPConnection soapConnection = soapConnectionFactory.createConnection();
      // https://stackoverflow.com/questions/9536616/setting-socket-read-timeout-with-javax-xml-soap-soapconnection
      final URL endpoint = new URL(new URL(idgUrlHost), idgUrlPath,
        new URLStreamHandler() {
          @Override
          protected URLConnection openConnection(final URL url)
                                                 throws IOException {
            final URL target = new URL(url.toString());
            final URLConnection connection = target.openConnection();
            connection.setConnectTimeout(idgConnectTimeout);
            connection.setReadTimeout(idgReadTimeout);
            return(connection);
          }
        });

      final SOAPMessage soapResponse = soapConnection.call(createSOAPMessage(soapAction, id),
                                                           endpoint);

      final SOAPBody soapBody = soapResponse.getSOAPBody();
      final NodeList inputDataNodeList = soapBody.getElementsByTagNameNS(myNamespaceURI,
                                                                         "InputData");
      soapConnection.close();

      if (inputDataNodeList == null) {
        throw new RestCallException("No 'InputData' tag in WS IDG response [1] : '" + soapBody.toString() + "'");
      } else if (inputDataNodeList.item(0) == null) {
        // Perhaps an indication that this has timed out waiting for response.
        // Try increasing appCtx.sim.processSimulation.xml inputDataGathering
        // reply-timeout value (filter.properties + rebuild, or change + restart!!)!
        throw new RestCallException("No 'InputData' tag in WS IDG response [2] : '" + soapBody.toString() + "'");
      } else {
        idgResponse = inputDataNodeList.item(0).getTextContent();
      }

    } catch (SOAPException soapException) {
      final Throwable soapExceptionCause = soapException.getCause();
      if (soapExceptionCause != null) {
        final Throwable subCause = soapExceptionCause.getCause();
        if (subCause != null) {
          final String fullUrl = idgUrlHost + idgUrlPath;
          if (subCause instanceof SocketTimeoutException) {
            throw new RestCallException("Socket timeout connecting to " + fullUrl);
          } else if (subCause instanceof ConnectException) {
            throw new RestCallException("Connection timeout connecting to " + fullUrl);
          }
        }
      }
      soapException.printStackTrace();
      throw new RestCallException("IDG WS communication exception!");
    } catch (Exception e) {
      e.printStackTrace();
      throw new RestCallException(e.getMessage());
    }

    if (idgResponse == null) {
      throw new RestCallException("IDG responded with a null value");
    } else {
      if (Util.isValidJSON(idgResponse)) {
        JSONArray processedDataArray = null;
        try {
          final JSONObject jsonObject = new JSONObject(idgResponse);
          processedDataArray = jsonObject.getJSONArray("processedData");
        } catch (JSONException e) {
          throw new RestCallException("IDG response was not JSON [1] : '" + idgResponse + "'");
        }
        if (processedDataArray.length() == 0) {
          throw new RestCallException("No data found for specified compound"); 
        }
      } else {
        throw new RestCallException("IDG response was not JSON [2] : '" + idgResponse + "'");
      }
    }

    return idgResponse;
  }

  private SOAPMessage createSOAPMessage(final String soapAction,
                                        final String id) throws Exception {
    final MessageFactory messageFactory = MessageFactory.newInstance();
    final SOAPMessage soapMessage = messageFactory.createMessage();

    final SOAPPart soapPart = soapMessage.getSOAPPart();

    final SOAPEnvelope envelope = soapPart.getEnvelope();

    final SOAPHeader header = envelope.getHeader();
    final SOAPElement security = header.addChildElement("Security", "wsse",
                                                        "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
    final SOAPElement usernameToken = security.addChildElement("UsernameToken",
                                                               "wsse");
    usernameToken.addAttribute(new QName("xmlns:wsu"),
                               "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");

    final SOAPElement username = usernameToken.addChildElement("Username",
                                                               "wsse");
    username.addTextNode(idgUsername);

    final SOAPElement password = usernameToken.addChildElement("Password",
                                                               "wsse");
    password.setAttribute("Type",
                          "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest");
    password.addTextNode(idgPassword);

    final SOAPElement nonce = usernameToken.addChildElement("Nonce", "wsse");
    nonce.setAttribute("EncodingType",
                       "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary");
    nonce.addTextNode(idgNonce);

    final SOAPElement created = usernameToken.addChildElement("Created", "wsu");
    created.addTextNode(idgCreated);

    envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);

    final SOAPBody soapBody = envelope.getBody();
    final SOAPElement soapBodyElem = soapBody.addChildElement("ProcessSimulationsRequest",
                                                              myNamespace);
    final SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("SimulationDetail",
                                                                   myNamespace);
    soapBodyElem1.addAttribute(QName.valueOf("InputDataOnly"), "true");

    soapBodyElem1.addAttribute(QName.valueOf("AssayGrouping"), "false");
    soapBodyElem1.addAttribute(QName.valueOf("ValueInheriting"), "false");
    //soapBodyElem1.addAttribute(QName.valueOf("BetweenGroups"), "false");
    //soapBodyElem1.addAttribute(QName.valueOf("WithinGroups"), "false");

    soapBodyElem1.addAttribute(QName.valueOf("DoseResponseFittingStrategy"), "IC50_ONLY");
    soapBodyElem1.addAttribute(QName.valueOf("DoseResponseFittingHillMinMax"), "false");
    //soapBodyElem1.addAttribute(QName.valueOf("DoseResponseFittingHillMax"), "");
    //soapBodyElem1.addAttribute(QName.valueOf("DoseResponseFittingHillMin"), "");
    soapBodyElem1.addAttribute(QName.valueOf("DoseResponseFittingRounding"), "true");

    soapBodyElem1.addAttribute(QName.valueOf("PC50EvaluationStrategies"), pc50EvaluationStrategies);

    //soapBodyElem1.addAttribute(QName.valueOf("ForceReRun"), "false");
    //soapBodyElem1.addAttribute(QName.valueOf("Reset"), "false");
    //soapBodyElem1.addAttribute(QName.valueOf("PacingMaxTime"), "false");
    //soapBodyElem1.addAttribute(QName.valueOf("CellMLModelIdentifier"), "false");

    final SOAPElement soapBodyElem2 = soapBodyElem1.addChildElement("CompoundIdentifier",
                                                                    myNamespace);
    soapBodyElem2.addTextNode(id);

    soapMessage.saveChanges();

    return soapMessage;
  }
}