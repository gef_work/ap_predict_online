/*

  Derived from https://www.baeldung.com/spring-boot-custom-error-page

 */
package uk.ac.nottingham.mathematics.rest_data_api.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @see https://www.baeldung.com/spring-boot-custom-error-page
 */
@Controller
public class CustomErrorController implements ErrorController {

  public CustomErrorController() {}

  @GetMapping(value = "/error")
  public String handleError(HttpServletRequest request) {
    final Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
    System.out.println("~handleError() : " + status);
    if (status != null) {
      Integer statusCode = Integer.valueOf(status.toString());

      if (statusCode == HttpStatus.NOT_FOUND.value()) {
        return "error-404";
      }
    }

    return "error";
  }

  /* (non-Javadoc)
   * @see org.springframework.boot.web.servlet.error.ErrorController#getErrorPath()
   */
  public String getErrorPath() {
    return "/error";
  }

}