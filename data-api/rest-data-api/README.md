# Action Potential prediction -- API to `business-manager` Input Data Gathering

A [Sprint Boot](https://spring.io/projects/spring-boot) API to invoke the input data gathering
process of `business-manager`.

Basically it calls the `business-manager` web service with an "input data gathering" request.