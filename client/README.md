# Action Potential prediction -- Web Interface Client

This is the code for running the AP-Portal client web interface, i.e. the part that deals with
browser presentation.

In essence `client` performs the following tasks :

  1. Presents the user with the opportunity to enter a compound identifier in the `client`
     interface and click "Submit".
  1. `client` calls `site-business` to retrieve and process the compound data.
  1. Polls `site-business` until simulation results are ready.
  1. Displays available simulation results for a compound.

## Installation

Please see either of the following :

  1. http://apportal.readthedocs.io/en/latest/installation/components/client/index.html
  1. ap_predict_online/docs/AP-Portal/RtD/_build/html/installation/components/client/index.html