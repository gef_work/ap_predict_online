/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.value.config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 * Object containing default configuration values.
 *
 * @author geoff
 */
public class BusinessManagerConfigurationsVO {

  private final boolean defaultForceReRun;
  private final boolean defaultReset;

  private final boolean defaultAssayGrouping;
  private final boolean defaultValueInheriting;
  private final boolean defaultBetweenGroups;
  private final boolean defaultWithinGroups;

  private final short defaultMinInformationLevel;
  private final short defaultMaxInformationLevel;

  private final List<PC50EvaluationStrategyConfigVO> defaultPC50EvaluationStrategies = new ArrayList<PC50EvaluationStrategyConfigVO>();
  private final Map<Short, String> assays = new HashMap<Short, String>();
  private final Map<Short, String> assayGroups = new HashMap<Short, String>();
  private final Map<Short, Vector<String>> ionChannels = new HashMap<Short, Vector<String>>();
  private final List<CellModelVO> cellModels = new ArrayList<CellModelVO>();

  private final int defaultSimulationRequestProcessingPolling;

  private DefaultDoseResponseFittingConfigurationsVO doseResponseConfigurations;

  /**
   * Initialising constructor based on BusinessManager configuration settings.
   * 
   * @param defaultForceReRun Default simulation force re-run value.
   * @param defaultReset Default simulation reset value.
   * @param defaultAssayGrouping Default assay grouping option.
   * @param defaultValueInheriting Default value inheriting option.
   * @param defaultBetweenGroups Default value inheriting between groups option.
   * @param defaultWithinGroups Default value inheriting within groups option.
   * @param defaultMinInformationLevel Default minimum information level.
   * @param defaultMaxInformationLevel Default maximum information level.
   * @param defaultPC50EvaluationStrategies Default PC50 evaluation strategies.
   * @param assays Assays.
   * @param assayGroups Assay groups.
   * @param ionChannels Ion channels.
   * @param doseResponseConfigurations Dose response fitting configurations (or {@code null} if none
   *                                   available).
   * @param cellModels CellML models.
   * @param defaultSimulationRequestProcessingPolling Default simulation request processing polling
   *                                                  period (in ms).
   */
  public BusinessManagerConfigurationsVO(final boolean defaultForceReRun,
                                         final boolean defaultReset,
                                         final boolean defaultAssayGrouping,
                                         final boolean defaultValueInheriting,
                                         final boolean defaultBetweenGroups,
                                         final boolean defaultWithinGroups,
                                         final short defaultMinInformationLevel,
                                         final short defaultMaxInformationLevel,
                                         final List<PC50EvaluationStrategyConfigVO> defaultPC50EvaluationStrategies,
                                         final Map<Short, String> assays,
                                         final Map<Short, String> assayGroups,
                                         final Map<Short, Vector<String>> ionChannels,
                                         final DefaultDoseResponseFittingConfigurationsVO doseResponseConfigurations,
                                         final List<CellModelVO> cellModels,
                                         final int defaultSimulationRequestProcessingPolling) {
    this.defaultForceReRun = defaultForceReRun;
    this.defaultReset = defaultReset;
    this.defaultAssayGrouping = defaultAssayGrouping;
    this.defaultValueInheriting = defaultValueInheriting;
    this.defaultBetweenGroups = defaultBetweenGroups;
    this.defaultWithinGroups = defaultWithinGroups;

    this.defaultMinInformationLevel = defaultMinInformationLevel;
    this.defaultMaxInformationLevel = defaultMaxInformationLevel;

    if (defaultPC50EvaluationStrategies != null) {
      this.defaultPC50EvaluationStrategies.addAll(defaultPC50EvaluationStrategies);
    }

    if (assays != null) {
      this.assays.putAll(assays);
    }
    if (assayGroups != null) {
      this.assayGroups.putAll(assayGroups);
    }
    if (ionChannels != null) {
      this.ionChannels.putAll(ionChannels);
    }
    this.doseResponseConfigurations = doseResponseConfigurations;

    if (cellModels != null) {
      this.cellModels.addAll(cellModels);
    }

    this.defaultSimulationRequestProcessingPolling = defaultSimulationRequestProcessingPolling;
  }

  /**
   * @return the defaultForceReRun
   */
  public boolean isDefaultForceReRun() {
    return defaultForceReRun;
  }

  /**
   * @return the defaultReset
   */
  public boolean isDefaultReset() {
    return defaultReset;
  }

  /**
   * @return the defaultAssayGrouping
   */
  public boolean isDefaultAssayGrouping() {
    return defaultAssayGrouping;
  }

  /**
   * @return the defaultValueInheriting
   */
  public boolean isDefaultValueInheriting() {
    return defaultValueInheriting;
  }

  /**
   * @return the defaultBetweenGroups
   */
  public boolean isDefaultBetweenGroups() {
    return defaultBetweenGroups;
  }

  /**
   * @return the defaultWithinGroups
   */
  public boolean isDefaultWithinGroups() {
    return defaultWithinGroups;
  }

  /**
   * @return the defaultMinInformationLevel
   */
  public short getDefaultMinInformationLevel() {
    return defaultMinInformationLevel;
  }

  /**
   * @return the defaultMaxInformationLevel
   */
  public short getDefaultMaxInformationLevel() {
    return defaultMaxInformationLevel;
  }

  /**
   * @return the defaultPC50EvaluationStrategies
   */
  public List<PC50EvaluationStrategyConfigVO> getDefaultPC50EvaluationStrategies() {
    return Collections.unmodifiableList(defaultPC50EvaluationStrategies);
  }

  /**
   * @return the assays
   */
  public Map<Short, String> getAssays() {
    return Collections.unmodifiableMap(assays);
  }

  /**
   * @return the assayGroups
   */
  public Map<Short, String> getAssayGroups() {
    return Collections.unmodifiableMap(assayGroups);
  }

  /**
   * @return the ionChannels
   */
  public Map<Short, Vector<String>> getIonChannels() {
    return Collections.unmodifiableMap(ionChannels);
  }

  /**
   * Retrieve the Dose-Response fitting configurations.
   * 
   * @return Dose-Response fitting configurations (or {@code null} if none available).
   */
  public DefaultDoseResponseFittingConfigurationsVO getDoseResponseConfigurations() {
    return doseResponseConfigurations;
  }

  /**
   * Retrieve the CellML models.
   * 
   * @return Unmodifiable collection (min. size of 1) of CellML models (ordered by model identifier).
   */
  public List<CellModelVO> getCellModels() {
    return Collections.unmodifiableList(cellModels);
  }

  /**
   * Retrieve the default simulation request processing polling period.
   * 
   * @return Default simulation request processing polling period (in ms).
   */
  public int getDefaultSimulationRequestProcessingPolling() {
    return defaultSimulationRequestProcessingPolling;
  }
}