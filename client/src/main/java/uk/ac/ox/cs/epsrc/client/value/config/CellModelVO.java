/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.value.config;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;

/**
 * Value object representing a CellML model.
 *
 * @author geoff
 */
// This is a replica of the CellModelVO in the business-manager component
public class CellModelVO implements Comparable<CellModelVO> {

  /** Minimum identifier value */
  protected static final short minModelIdentifier = 1;

  private final short identifier;
  private final String name;
  private final String description;
  private final String cellmlURL;
  private final String paperURL;
  private final boolean defaultModel;
  private final BigDecimal pacingMaxTime;

  /**
   * Initialising constructor.
   * 
   * @param identifier Model identifier, e.g. 1, 2, 3 (as passed to ApPredict)
   * @param name Model name, e.g. Shannon.
   * @param description Model description, e.g. Rabbit model.
   * @param cellmlURL CellML URL.
   * @param paperURL URL of research paper.
   * @param defaultModel Default model indicator.
   * @param pacingMaxTime Maximum pacing time in minutes.
   * @throws IllegalArgumentException If inappropriate model identifier
   *                                  ({@linkplain CellModelVO#minModelIdentifier}), or 
   *                                  no name provided.
   */
  public CellModelVO(final short identifier, final String name, final String description,
                     final String cellmlURL, final String paperURL, final boolean defaultModel,
                     final BigDecimal pacingMaxTime) {
    if (identifier < minModelIdentifier) {
      throw new IllegalArgumentException("Model identifier '" + identifier + "' value must be >= '" + minModelIdentifier + ".");
    }
    if (StringUtils.isBlank(name)) {
      throw new IllegalArgumentException("Model name must be provided.");
    }
    this.identifier = identifier;
    this.name = name;
    this.description = description;
    this.cellmlURL = cellmlURL;
    this.paperURL = paperURL;
    this.defaultModel = defaultModel;
    this.pacingMaxTime = pacingMaxTime;
  }

  /* (non-Javadoc)
   * @see java.lang.Comparable#compareTo(java.lang.Object)
   */
  @Override
  public int compareTo(final CellModelVO otherModel) {
    return this.identifier - otherModel.getIdentifier();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + identifier;
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    CellModelVO other = (CellModelVO) obj;
    if (identifier != other.identifier)
      return false;
    return true;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "CellModelVO [identifier=" + identifier + ", name=" + name
        + ", description=" + description + ", cellmlURL=" + cellmlURL
        + ", paperURL=" + paperURL + ", defaultModel=" + defaultModel
        + ", pacingMaxTime=" + pacingMaxTime + "]";
  }

  /**
   * Retrieve the model identier.
   * 
   * @return Model identifier
   */
  public short getIdentifier() {
    return identifier;
  }

  /**
   * Retrieve the model name.
   * 
   * @return Model name.
   */
  public String getName() {
    return name;
  }

  /**
   * Retrieve a description of the model.
   * 
   * @return Model description (or null if none provided).
   */
  public String getDescription() {
    return description;
  }

  /**
   * Retrieve a URL of the CellML repos.
   * 
   * @return CellML repository URL (or null if none provided).
   */
  public String getCellmlURL() {
    return cellmlURL;
  }

  /**
   * Retrieve a URL of a paper associated with the model.
   * 
   * @return Publication URL (or null if none provided).
   */
  public String getPaperURL() {
    return paperURL;
  }

  /**
   * Retrieve a flag to indicate if this is the default model to use.
   * 
   * @return True if this model is the default for invocation, otherwise false.
   */
  public boolean isDefaultModel() {
    return defaultModel;
  }

  /**
   * Retrieve the maximum pacing time in minutes.
   * 
   * @return Max. pacing time (in minutes), or {@code null} is not provided.
   */
  public BigDecimal getPacingMaxTime() {
    return pacingMaxTime;
  }
}