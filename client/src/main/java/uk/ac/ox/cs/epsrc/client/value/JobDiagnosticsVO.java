/*

  Copyright (c) 2016, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.value;

/**
 * Job diagnostics information value object.
 * <p>
 * For this information to appear the simulation's job must have at least been invoked by an 
 * app-manager ApPredict invocation and the job ended and have had the two items of data below
 * persisted.
 *
 * @author geoff
 */
public class JobDiagnosticsVO {

  private final String info;
  private final String output;

  /**
   * Initialising constructor.
   * 
   * @param info ApPredict invocation information, e.g. command line.
   * @param output ApPredict output.
   */
  public JobDiagnosticsVO(final String info, final String output) {
    this.info = info;
    this.output = output;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "JobDiagnosticsVO [info=" + info + ", output=" + output + "]";
  }

  /**
   * Retrieve job information, e.g. command line.
   * 
   * @return Job information, or {@code null} if non available.
   */
  public String getInfo() {
    return info;
  }

  /**
   * Retrieve job output, e.g. whatever ApPredict has sent to stdout/stderr.
   * 
   * @return Job output, or {@code null} if non available.
   */
  public String getOutput() {
    return output;
  }
}