/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import uk.ac.ox.cs.epsrc.client.ClientIdentifiers;
import uk.ac.ox.cs.epsrc.client.service.ClientService;
import uk.ac.ox.cs.epsrc.client.value.config.AllConfigurations;

/**
 * MVC Controller handling requests to extruder segments of html.
 *
 * @author geoff
 */
@Controller
public class Extruder {

  private static final String pathVarPageName = "pageName";

  @Autowired @Qualifier(ClientIdentifiers.COMPONENT_CLIENT_SERVICE)
  private ClientService clientService;

  private static final Log log = LogFactory.getLog(Extruder.class);

  /**
   * Retrieves a page for display in the extruder.
   * 
   * @param pageName Name of view to return.
   * @param model MVC model.
   * @return (Part of the) Path to jsp page.
   */
  @RequestMapping(value=ClientIdentifiers.URL_PREFIX_EXTRUDER + "{" + pathVarPageName + "}",
                  method=RequestMethod.GET)
  public String retrieveExtruderView(final @PathVariable(pathVarPageName)
                                           String pageName,
                                     final Model model) {
    log.debug("~retrieveExtruder() : Invoked '" + pageName + "'.");
    // TODO: This is happening on each extruder section view! could make session-bound but would have to
    //   enforce client session expiration if configuration defaults changed in business manager.
    final AllConfigurations allConfigurations = clientService.retrieveDefaultConfigurations();

    if ("cellModels".equals(pageName)) {
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_CELL_MODELS,
                         allConfigurations.retrieveCellModels());
    } else if ("doseResponseFitting".equals(pageName)) {
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_DOSE_RESPONSE_FITTING,
                         allConfigurations.retrieveDoseResponseFitting());
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_DOSE_RESPONSE_FITTING_ROUNDED,
                         allConfigurations.isDoseResponseFittingRounded());
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_DOSE_RESPONSE_FITTING_HILL_MAX,
                         allConfigurations.retrieveDoseResponseFittingHillMax());
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_DOSE_RESPONSE_FITTING_HILL_MIN,
                         allConfigurations.retrieveDoseResponseFittingHillMin());
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_HILL_FITTING_STRATEGY,
                         allConfigurations.retrieveHillFittingStrategy());
    } else if ("groupingInheritance".equals(pageName)) {
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_ASSAY_GROUPING,
                         allConfigurations.isAssayGrouping());
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_VALUE_INHERITING,
                         allConfigurations.isValueInheriting());
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_BETWEEN_GROUPS,
                         allConfigurations.isBetweenGroups());
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_WITHIN_GROUPS,
                         allConfigurations.isWithinGroups());
    } else if ("informationLevel".equals(pageName)) {
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_MAX_INFORMATION_LEVEL,
                         allConfigurations.retrieveDefaultMaxInformationLevel());
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_MIN_INFORMATION_LEVEL,
                         allConfigurations.retrieveDefaultMinInformationLevel());
    } else if ("resetReRun".equals(pageName)) {
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_FORCE_RERUN,
                         allConfigurations.isForceReRun());
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_RESET,
                         allConfigurations.isReset());
    } else if ("strategies".equals(pageName)) {
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_PC50_EVALUATION_STRATEGIES,
                         allConfigurations.retrievePC50EvaluationStrategies());
    } else if ("units".equals(pageName)) {
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_C50_UNITS,
                         allConfigurations.retrieveC50Units());
    }

    return ClientIdentifiers.URL_PREFIX_EXTRUDER + pageName;
  }
}