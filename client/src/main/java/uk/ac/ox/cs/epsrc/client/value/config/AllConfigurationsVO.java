/*

  Copyright (c) 2016, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.value.config;

import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.epsrc.client.value.type.InputValueDisplay;

/**
 * Object holding all configuration options.
 *
 * @author geoff
 */
public class AllConfigurationsVO implements AllConfigurations {

  private BusinessManagerConfigurationsVO businessManagerConfigurations;
  private ClientConfigurationsVO clientConfigurations;

  private static final Log log = LogFactory.getLog(AllConfigurationsVO.class);

  /**
   * Initialising constructor.
   * 
   * @param businessManagerConfigurations Business manager configurations.
   * @param clientConfigurations Client configurations.
   */
  public AllConfigurationsVO(final BusinessManagerConfigurationsVO businessManagerConfigurations,
                             final ClientConfigurationsVO clientConfigurations) {
    this.businessManagerConfigurations = businessManagerConfigurations;
    this.clientConfigurations = clientConfigurations;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.value.config.AllConfigurations#isAssayGrouping()
   */
  @Override
  public boolean isAssayGrouping() {
    log.debug("~isAssayGrouping() : Invoked.");

    return businessManagerConfigurations.isDefaultAssayGrouping();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.value.config.AllConfigurations#isBetweenGroups()
   */
  @Override
  public boolean isBetweenGroups() {
    log.debug("~isBetweenGroups() : Invoked.");

    return businessManagerConfigurations.isDefaultBetweenGroups();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.value.config.AllConfigurations#isDoseResponseFittingRounded()
   */
  @Override
  public boolean isDoseResponseFittingRounded() {
    log.debug("~isDoseResponseFittingRounded() : Invoked.");

    return businessManagerConfigurations.getDoseResponseConfigurations().isDefaultDoseResponseFittingRounded();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.value.config.AllConfigurations#isForceReRun()
   */
  @Override
  public boolean isForceReRun() {
    log.debug("~isForceReRun() : Invoked.");

    return businessManagerConfigurations.isDefaultForceReRun();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.value.config.AllConfigurations#isReset()
   */
  @Override
  public boolean isReset() {
    log.debug("~isReset() : Invoked.");

    return businessManagerConfigurations.isDefaultReset();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.value.config.AllConfigurations#isValueInheriting()
   */
  @Override
  public boolean isValueInheriting() {
    log.debug("~isValueInheriting() : Invoked.");

    return businessManagerConfigurations.isDefaultValueInheriting();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.value.config.AllConfigurations#isWithinGroups()
   */
  @Override
  public boolean isWithinGroups() {
    log.debug("~isWithinGroups() : Invoked.");

    return businessManagerConfigurations.isDefaultWithinGroups();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.transfer.AllConfigurations#retrieveC50Units()
   */
  @Override
  public Map<String, String> retrieveC50Units() {
    log.debug("~retrieveC50Units() : Invoked.");

    return clientConfigurations.getC50Units();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.transfer.AllConfigurations#retrieveCellModels()
   */
  @Override
  public List<CellModelVO> retrieveCellModels() {
    log.debug("~retrieveCellModels() : Invoked.");

    return businessManagerConfigurations.getCellModels();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.value.config.AllConfigurations#retrieveDoseResponseFitting()
   */
  @Override
  public String retrieveDoseResponseFitting() {
    log.debug("~retrieveDoseResponseFitting() : Invoked.");

    return businessManagerConfigurations.getDoseResponseConfigurations().getDefaultDoseResponseFitting();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.value.config.AllConfigurations#retrieveDoseResponseFittingHillMax()
   */
  @Override
  public float retrieveDoseResponseFittingHillMax() {
    log.debug("~retrieveDoseResponseFittingHillMax() : Invoked.");

    return businessManagerConfigurations.getDoseResponseConfigurations().getDefaultDoseResponseFittingHillMax();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.value.config.AllConfigurations#retrieveDoseResponseFittingHillMin()
   */
  @Override
  public float retrieveDoseResponseFittingHillMin() {
    log.debug("~retrieveDoseResponseFittingHillMin() : Invoked.");

    return businessManagerConfigurations.getDoseResponseConfigurations().getDefaultDoseResponseFittingHillMin();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.transfer.AllConfigurations#retrieveDefaultMaxInformationLevel()
   */
  @Override
  public short retrieveDefaultMaxInformationLevel() {
    log.debug("~retrieveDefaultMaxInformationLevel() : Invoked.");

    return businessManagerConfigurations.getDefaultMaxInformationLevel();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.transfer.AllConfigurations#retrieveDefaultMinInformationLevel()
   */
  @Override
  public short retrieveDefaultMinInformationLevel() {
    log.debug("~retrieveDefaultMinInformationLevel() : Invoked.");

    return businessManagerConfigurations.getDefaultMinInformationLevel();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.transfer.AllConfigurations#retrieveInputValueDisplay()
   */
  @Override
  public InputValueDisplay retrieveInputValueDisplay() {
    log.debug("~retrieveInputValueDisplay() : Invoked.");

    return clientConfigurations.getInputValueDisplay();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.transfer.AllConfigurations#retrieveIonChannels()
   */
  @Override
  public Map<Short, Vector<String>> retrieveIonChannels() {
    log.debug("~retrieveIonChannels() : Invoked.");

    return businessManagerConfigurations.getIonChannels();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.value.config.AllConfigurations#retrieveHillFittingStrategy()
   */
  @Override
  public Map<String, Boolean> retrieveHillFittingStrategy() {
    log.debug("~retrieveHillFittingStrategy() : Invoked.");

    return businessManagerConfigurations.getDoseResponseConfigurations().getDrfStrategyHillFitting();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.value.config.AllConfigurations#retrievePC50EvaluationStrategies()
   */
  @Override
  public List<PC50EvaluationStrategyConfigVO> retrievePC50EvaluationStrategies() {
    log.debug("~retrievePC50EvaluationStrategies() : Invoked.");

    return businessManagerConfigurations.getDefaultPC50EvaluationStrategies();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.value.config.AllConfigurations#retrieveSimulationRequestProcessingPolling()
   */
  public int retrieveSimulationRequestProcessingPolling() {
    log.debug("~retrieveSimulationRequestProcessingPolling() : Invoked.");

    return businessManagerConfigurations.getDefaultSimulationRequestProcessingPolling();
  }
}