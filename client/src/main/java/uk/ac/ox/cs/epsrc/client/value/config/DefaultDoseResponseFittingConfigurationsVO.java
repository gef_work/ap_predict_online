/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.value.config;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Default Dose-Response fitting configurations value object.
 *
 * @author geoff
 */
public class DefaultDoseResponseFittingConfigurationsVO {

  private final Map<String, Boolean> drfStrategyHillFitting = new HashMap<String, Boolean>();

  private final String defaultDoseResponseFitting;
  private final boolean defaultDoseResponseFittingRounded;
  private final float defaultDoseResponseFittingHillMax;
  private final float defaultDoseResponseFittingHillMin;

  private static final Log log = LogFactory.getLog(DefaultDoseResponseFittingConfigurationsVO.class);

  /**
   * Initialising constructor.
   * 
   * @param drfStrategyHillFitting Dose-Response strategies and whether they're Hill-fitting.
   * @param defaultDoseResponseFitting Default IC50_ONLY or IC50_AND_HILL for DoseResponse fitting.
   * @param defaultDoseResponseFittingRounded Default rounding.
   * @param defaultDoseResponseFittingHillMin Default minimum Hill Coefficient.
   * @param defaultDoseResponseFittingHillMax Default maximum Hill Coefficient.
   */
  public DefaultDoseResponseFittingConfigurationsVO(final Map<String, Boolean> drfStrategyHillFitting,
                                                    final String defaultDoseResponseFitting,
                                                    final boolean defaultDoseResponseFittingRounded,
                                                    final float defaultDoseResponseFittingHillMin,
                                                    final float defaultDoseResponseFittingHillMax) {
    if (drfStrategyHillFitting != null) {
      this.drfStrategyHillFitting.putAll(drfStrategyHillFitting);
    }
    this.defaultDoseResponseFitting = defaultDoseResponseFitting;
    this.defaultDoseResponseFittingRounded = defaultDoseResponseFittingRounded;
    this.defaultDoseResponseFittingHillMax = defaultDoseResponseFittingHillMax;
    this.defaultDoseResponseFittingHillMin = defaultDoseResponseFittingHillMin;

    log.debug("~DefaultDoseResponseFittingConfigurationsVO() : '" + this.toString() + "'.");
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "DefaultDoseResponseFittingConfigurationsVO [drfStrategyHillFitting="
        + drfStrategyHillFitting
        + ", defaultDoseResponseFitting="
        + defaultDoseResponseFitting
        + ", defaultDoseResponseFittingRounded="
        + defaultDoseResponseFittingRounded
        + ", defaultDoseResponseFittingHillMax="
        + defaultDoseResponseFittingHillMax
        + ", defaultDoseResponseFittingHillMin="
        + defaultDoseResponseFittingHillMin + "]";
  }

  /**
   * Retrieve dose-response fitting strategies and whether they're Hill-fitting.
   * 
   * @return Dose-Response fitting strategies and whether they're Hill-fitting.
   */
  public Map<String, Boolean> getDrfStrategyHillFitting() {
    return Collections.unmodifiableMap(drfStrategyHillFitting);
  }

  /**
   * @return the defaultDoseResponseFitting
   */
  public String getDefaultDoseResponseFitting() {
    return defaultDoseResponseFitting;
  }

  /**
   * @return the defaultDoseResponseFittingRounded
   */
  public boolean isDefaultDoseResponseFittingRounded() {
    return defaultDoseResponseFittingRounded;
  }

  /**
   * @return the defaultDoseResponseFittingHillMax
   */
  public float getDefaultDoseResponseFittingHillMax() {
    return defaultDoseResponseFittingHillMax;
  }

  /**
   * @return the defaultDoseResponseFittingHillMin
   */
  public float getDefaultDoseResponseFittingHillMin() {
    return defaultDoseResponseFittingHillMin;
  }
}