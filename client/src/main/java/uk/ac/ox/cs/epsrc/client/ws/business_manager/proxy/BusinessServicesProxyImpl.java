/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.ws.business_manager.proxy;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.soap.client.SoapFaultClientException;

import uk.ac.ox.cs.compbio.client_shared.api.exception.DownstreamCommunicationException;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.AppManagerWorkloadRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.AppManagerWorkloadResponse;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.DefConAssayGroupStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.DefConAssayStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.DefConCellModelStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.DefConDRFStrategyHillFitting;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.DefConDoseResponseFittingStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.DefConIonChannelStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.DefConPC50EvaluationStrategyStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.DefConRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.DefConResponse;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.ExperimentalResultsPerConcentration;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.ExperimentalResultsPerFrequency;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.ExperimentalResultsRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.ExperimentalResultsResponse;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.InputValues;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.InputValuesRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.InputValuesResponse;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.InputValuesStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.ObjectFactory;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.ProcessSimulationsRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.ProcessSimulationsResponse;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.ProcessSimulationsResponseStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.ProgressRecordStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.ProgressRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.ProgressResponse;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.ProvenanceRecordStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.ProvenanceRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.ProvenanceResponse;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.ResultsConcentrationStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.ResultsJobDataStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.ResultsRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.ResultsResponse;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.RetrieveDiagnosticsRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.RetrieveDiagnosticsResponse;
import uk.ac.ox.cs.epsrc.business_manager.ws._1.SimulationDetail;
import uk.ac.ox.cs.epsrc.client.ClientIdentifiers;
import uk.ac.ox.cs.epsrc.client.exception.BusinessManagerException;
import uk.ac.ox.cs.epsrc.client.exception.ClientException;
import uk.ac.ox.cs.epsrc.client.value.AllProgressVO;
import uk.ac.ox.cs.epsrc.client.value.ExperimentalDataVO;
import uk.ac.ox.cs.epsrc.client.value.JobDiagnosticsVO;
import uk.ac.ox.cs.epsrc.client.value.ResultsVO;
import uk.ac.ox.cs.epsrc.client.value.SimulationResponseVO;
import uk.ac.ox.cs.epsrc.client.value.config.BusinessManagerConfigurationsVO;
import uk.ac.ox.cs.epsrc.client.value.config.CellModelVO;
import uk.ac.ox.cs.epsrc.client.value.config.DefaultDoseResponseFittingConfigurationsVO;
import uk.ac.ox.cs.epsrc.client.value.config.PC50EvaluationStrategyConfigVO;

/**
 * JAXB marshalling gateway to the business manager web service.
 *
 * @author geoff
 */
// defined in appCtx.ws.xml
public class BusinessServicesProxyImpl extends WebServiceGatewaySupport
                                       implements BusinessServicesProxy {

  // appCtx.ws.security-outgoing.xml
  @Autowired(required=false)
  @Qualifier(ClientIdentifiers.COMPONENT_BUS_MANAGER_SERVICES_INTERCEPTOR)
  private ClientInterceptor wsBusManagerServicesInterceptor;

  private static final ObjectFactory objectFactory = new ObjectFactory();

  private static final Log log = LogFactory.getLog(BusinessServicesProxyImpl.class);

  @PostConstruct
  private void postConstruct() {
    final ClientInterceptor[] wsClientInterceptors = { wsBusManagerServicesInterceptor };

    this.setInterceptors(wsClientInterceptors);
    for (int idx = 0; idx < wsClientInterceptors.length; idx++) {
      log.info("~BusinessServicesProxyImpl() : Interceptor '" + wsClientInterceptors[idx] + "' assigned for outbound.");
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.ws.business_manager.proxy.BusinessServicesProxy#queryBusinessManager(java.lang.String, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.String, java.lang.String, java.lang.Boolean, java.lang.Boolean, java.lang.Float, java.lang.Float, java.lang.Short, java.math.BigDecimal, java.lang.String)
   */
  @Override
  public SimulationResponseVO queryBusinessManager(final String compoundIdentifier,
                                                   final Boolean forceReRun,
                                                   final Boolean reset,
                                                   final Boolean assayGrouping,
                                                   final Boolean valueInheriting,
                                                   final Boolean withinGroups,
                                                   final Boolean betweenGroups,
                                                   final String pC50EvaluationStrategies,
                                                   final String doseResponseFittingStrategy,
                                                   final Boolean doseResponseFittingRounding,
                                                   final Boolean doseResponseFittingHillMinMax,
                                                   final Float doseResponseFittingHillMax,
                                                   final Float doseResponseFittingHillMin,
                                                   final Short cellModelIdentifier,
                                                   final BigDecimal pacingMaxTime,
                                                   final String userId)
                                                   throws BusinessManagerException,
                                                          ClientException {
    log.debug("~queryBusinessManager() : Invoked for compound identifier '" + compoundIdentifier + "'.");

    final SimulationDetail simulationDetail = objectFactory.createSimulationDetail();
    simulationDetail.setCompoundIdentifier(compoundIdentifier);
    simulationDetail.setForceReRun(forceReRun);
    simulationDetail.setReset(reset);
    simulationDetail.setAssayGrouping(assayGrouping);
    simulationDetail.setBetweenGroups(betweenGroups);
    simulationDetail.setValueInheriting(valueInheriting);
    simulationDetail.setWithinGroups(withinGroups);
    simulationDetail.setPC50EvaluationStrategies(pC50EvaluationStrategies);
    simulationDetail.setDoseResponseFittingStrategy(doseResponseFittingStrategy);
    simulationDetail.setDoseResponseFittingRounding(doseResponseFittingRounding);
    simulationDetail.setDoseResponseFittingHillMinMax(doseResponseFittingHillMinMax);
    simulationDetail.setDoseResponseFittingHillMax(doseResponseFittingHillMax);
    simulationDetail.setDoseResponseFittingHillMin(doseResponseFittingHillMin);
    simulationDetail.setCellMLModelIdentifier(cellModelIdentifier);
    simulationDetail.setPacingMaxTime(pacingMaxTime);
    simulationDetail.setUserId(userId);

    final ProcessSimulationsRequest request = objectFactory.createProcessSimulationsRequest();
    request.getSimulationDetail().add(simulationDetail);

    ProcessSimulationsResponse response = null;
    try {
      response = (ProcessSimulationsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    } catch (SoapFaultClientException e) {
      final String message = e.getMessage();
      log.info("~queryBusinessManager() : Soap fault '" + message + "'.");
      throw new BusinessManagerException(message);
    } catch (WebServiceIOException e) {
      final String message = e.getMessage();
      log.error("~queryBusinessManager() : WS IO fault '" + message + "'.");
      throw new ClientException("Application Web Service communication error! [Original cause is '" + message + "']");
    }

    final List<ProcessSimulationsResponseStructure> structures = response.getProcessSimulationsResponseStructure();
    SimulationResponseVO simulationResponse = null;
    Long simulationId = null;
    if (structures == null || structures.size() < 1) {
      final String message = "Business manager responded with a null/empty response for compound identifier '" + compoundIdentifier + "'.";
      log.info("~queryBusinessManager() : " + message);
      throw new BusinessManagerException(message);
    } else if (structures.size() > 1) {
      final String message = "Business manager responded with more than one record for compound identifier '" + compoundIdentifier + "'.";
      log.info("~queryBusinessManager() : " + message);
      throw new BusinessManagerException(message);
    } else {
      final ProcessSimulationsResponseStructure responseStructure = structures.get(0);
      simulationResponse = new SimulationResponseVO(responseStructure.getCompoundIdentifier(),
                                                    responseStructure.getSimulationId());
    }
    log.debug("~queryBusinessManager() : Compound identifiers '" + compoundIdentifier + "' associated with simulation id '" + simulationId + "'.");

    return simulationResponse;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.ws.business_manager.proxy.BusinessServicesProxy#retrieveAppManagerWorkload()
   */
  public String retrieveAppManagerWorkload() throws DownstreamCommunicationException {
    log.debug("~retrieveAppManagerWorkload() : Invoked.");

    final AppManagerWorkloadRequest request = objectFactory.createAppManagerWorkloadRequest();

    final AppManagerWorkloadResponse response = (AppManagerWorkloadResponse) getWebServiceTemplate().marshalSendAndReceive(request);

    return response.getWorkload();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.manager.BusinessServicesProxy#retrieveDefaultConfigurations()
   */
  @Override
  public BusinessManagerConfigurationsVO retrieveDefaultConfigurations() {
    log.debug("~retrieveDefaultConfigurations() : Invoked.");

    final DefConRequest defaultsRequest = objectFactory.createDefConRequest();

    final DefConResponse defaultsResponse = (DefConResponse) getWebServiceTemplate().marshalSendAndReceive(defaultsRequest);

    final boolean defaultForceReRun = defaultsResponse.isDefaultForceReRun();
    final boolean defaultReset = defaultsResponse.isDefaultReset();

    final boolean defaultAssayGrouping = defaultsResponse.isDefaultAssayGrouping();
    final boolean defaultValueInheriting = defaultsResponse.isDefaultValueInheriting();
    final boolean defaultBetweenGroups = defaultsResponse.isDefaultBetweenGroups();
    final boolean defaultWithinGroups = defaultsResponse.isDefaultWithinGroups();

    final short defaultMinInformationLevel = defaultsResponse.getDefaultMinInformationLevel();
    final short defaultMaxInformationLevel = defaultsResponse.getDefaultMaxInformationLevel();

    final List<PC50EvaluationStrategyConfigVO> defaultPC50EvaluationStrategies = new ArrayList<PC50EvaluationStrategyConfigVO>();

    final List<DefConPC50EvaluationStrategyStructure> strategies = defaultsResponse.getDefConPC50EvaluationStrategyStructure();
    for (final DefConPC50EvaluationStrategyStructure eachStrategy : strategies) {
      final Long id = eachStrategy.getId();
      final String description = eachStrategy.getDescription();
      final int defaultInvocationOrder = eachStrategy.getDefaultInvocationOrder();
      final Boolean defaultActive = eachStrategy.isDefaultActive();

      final PC50EvaluationStrategyConfigVO defaultPC50EvaluationStrategy = new PC50EvaluationStrategyConfigVO(id,
                                                                                                              description,
                                                                                                              defaultInvocationOrder,
                                                                                                              defaultActive);
      defaultPC50EvaluationStrategies.add(defaultPC50EvaluationStrategy);
    }

    final List<DefConAssayStructure> assayStructures = defaultsResponse.getDefConAssayStructure();
    final Map<Short, String> assays = new HashMap<Short, String>();
    for (final DefConAssayStructure assayStructure : assayStructures) {
      final Short level = new Short(assayStructure.getLevel());
      final String name = assayStructure.getName();
      assays.put(level, name);
    }

    final List<DefConAssayGroupStructure> assayGroupStructures = defaultsResponse.getDefConAssayGroupStructure();
    final Map<Short, String> assayGroups = new HashMap<Short, String>();
    for (final DefConAssayGroupStructure assayGroupStructure : assayGroupStructures) {
      final Short level = new Short(assayGroupStructure.getLevel());
      final String name = assayGroupStructure.getName();
      assayGroups.put(level, name);
    }

    final List<DefConIonChannelStructure> ionChannelStructures = defaultsResponse.getDefConIonChannelStructure();
    final Map<Short, Vector<String>> ionChannels = new HashMap<Short, Vector<String>>();
    for (final DefConIonChannelStructure ionChannelStructure : ionChannelStructures) {
      final Short displayOrder = ionChannelStructure.getDisplayOrder();
      final String name = ionChannelStructure.getName();
      final String description = ionChannelStructure.getDescription();
      final Vector<String> ionChannelData = new Vector<String>();
      ionChannelData.add(name);
      ionChannelData.add(description);
      ionChannels.put(displayOrder, ionChannelData);
    }

    final DefConDoseResponseFittingStructure doseResponseStructure = defaultsResponse.getDefConDoseResponseFittingStructure();
    DefaultDoseResponseFittingConfigurationsVO doseResponseConfigurations = null;
    if (doseResponseStructure != null) {
      if (doseResponseStructure.getDefConDRFStrategyHillFitting() != null && 
          !doseResponseStructure.getDefConDRFStrategyHillFitting().isEmpty()) {
        final Map<String, Boolean> drfStrategyHillFittings = new HashMap<String, Boolean>();
        for (final DefConDRFStrategyHillFitting drfStrategyHillFitting : doseResponseStructure.getDefConDRFStrategyHillFitting()) {
          final String drfStrategy = drfStrategyHillFitting.getStrategy();
          final Boolean isStrategyHillFitting = drfStrategyHillFitting.isHillFitting();
          drfStrategyHillFittings.put(drfStrategy, isStrategyHillFitting);
        }
        doseResponseConfigurations = 
            new DefaultDoseResponseFittingConfigurationsVO(drfStrategyHillFittings,
                                                           doseResponseStructure.getDefaultDoseResponseFittingStrategy(),
                                                           doseResponseStructure.isDefaultDoseResponseFittingRounded(),
                                                           doseResponseStructure.getDefaultDoseResponseFittingHillMin(),
                                                           doseResponseStructure.getDefaultDoseResponseFittingHillMax());
      }
    }

    final List<DefConCellModelStructure> cellModelStructures = defaultsResponse.getDefConCellModelStructure();
    final List<CellModelVO> cellModels = new ArrayList<CellModelVO>();
    if (cellModelStructures != null && !cellModelStructures.isEmpty()) {
      for (final DefConCellModelStructure cellModelStructure : cellModelStructures) {
        final CellModelVO cellModel = new CellModelVO(cellModelStructure.getIdentifier(),
                                                      cellModelStructure.getName(),
                                                      cellModelStructure.getDescription(),
                                                      cellModelStructure.getCellMLURL(),
                                                      cellModelStructure.getPaperURL(),
                                                      cellModelStructure.isDefault(),
                                                      cellModelStructure.getPacingMaxTime());
        cellModels.add(cellModel);
      }
    }

    final int defaultSimulationRequestProcessingPolling = defaultsResponse.getDefaultSimulationRequestProcessingPolling();

    final BusinessManagerConfigurationsVO defaultConfigurations = new BusinessManagerConfigurationsVO(defaultForceReRun,
                                                                                      defaultReset,
                                                                                      defaultAssayGrouping,
                                                                                      defaultValueInheriting,
                                                                                      defaultBetweenGroups,
                                                                                      defaultWithinGroups,
                                                                                      defaultMinInformationLevel,
                                                                                      defaultMaxInformationLevel,
                                                                                      defaultPC50EvaluationStrategies,
                                                                                      assays,
                                                                                      assayGroups,
                                                                                      ionChannels,
                                                                                      doseResponseConfigurations,
                                                                                      cellModels,
                                                                                      defaultSimulationRequestProcessingPolling);

    log.debug("~retrieveDefaultConfigurations() : Created " + defaultConfigurations.toString() + "'.");

    return defaultConfigurations;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.ws.business_manager.proxy.BusinessServicesProxy#retrieveExperimentalData(long, java.lang.String)
   */
  @Override
  public ExperimentalDataVO retrieveExperimentalData(final long simulationId,
                                                     final String userId) {
    log.debug("~retrieveExperimentalData() : [" + simulationId + "][" + userId + "]");

    final ExperimentalResultsRequest experimentalResultsRequest = objectFactory.createExperimentalResultsRequest();
    experimentalResultsRequest.setSimulationId(simulationId);
    experimentalResultsRequest.setUserId(userId);

    final ExperimentalResultsResponse experimentalResultsResponse = (ExperimentalResultsResponse) getWebServiceTemplate().
                                                                                                  marshalSendAndReceive(experimentalResultsRequest);

    final ExperimentalDataVO experimentalData = new ExperimentalDataVO();
    for (ExperimentalResultsPerFrequency perFrequency : experimentalResultsResponse.getExperimentalResultsPerFrequency()) {
      final String frequencyHz = perFrequency.getFrequencyHz();
      for (ExperimentalResultsPerConcentration perConcentration : perFrequency.getExperimentalResultsPerConcentration()) {
        experimentalData.addExperimentalValues(frequencyHz, perConcentration.getPlasmaConc(),
                                               perConcentration.getQtPctChangeMean(),
                                               perConcentration.getQtPctChangeSEM());
      }
    }

    return experimentalData;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.ws.business_manager.proxy.BusinessServicesProxy#retrieveJobDiagnostics(long)
   */
  @Override
  public JobDiagnosticsVO retrieveJobDiagnostics(final long jobId) {
    log.debug("~retrieveJobDiagnostics() : Invoked for job id '" + jobId + "'.");

    final RetrieveDiagnosticsRequest retrieveDiagnosticsRequest = objectFactory.createRetrieveDiagnosticsRequest();
    retrieveDiagnosticsRequest.setJobId(jobId);

    final RetrieveDiagnosticsResponse retrieveDiagnosticsResponse = (RetrieveDiagnosticsResponse) getWebServiceTemplate().
                                                                                                  marshalSendAndReceive(retrieveDiagnosticsRequest);

    final long returnedJobId = retrieveDiagnosticsResponse.getJobId();

    String info = "";
    String output = "";

    if (returnedJobId == jobId) {
      info = retrieveDiagnosticsResponse.getInfo();
      output = retrieveDiagnosticsResponse.getOutput();
    } else {
      log.error("~retrieveJobDiagnostics() : Requested diagnostics for id '" + jobId + "', but was returned those for '" + returnedJobId + "'.");
    }

    return new JobDiagnosticsVO(info, output);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.manager.BusinessServicesProxy#retrieveSimulationInputValues(long)
   */
  @Override
  public List<Map<String, Object>> retrieveSimulationInputValues(final long simulationId) {
    log.debug("~retrieveSimulationInputValues() : Invoked for simulation id '" + simulationId + "'.");

    final InputValuesRequest inputValuesRequest = objectFactory.createInputValuesRequest();
    inputValuesRequest.setSimulationId(simulationId);

    final InputValuesResponse inputValuesResponse = (InputValuesResponse) getWebServiceTemplate().
                                                                          marshalSendAndReceive(inputValuesRequest);

    final List<Map<String, Object>> onwardGroups = new ArrayList<Map<String, Object>>();

    final List<InputValues> allInputValues = inputValuesResponse.getInputValues();
    if (allInputValues != null && !allInputValues.isEmpty()) {
      for (final InputValues inputValues : allInputValues) {
        final Map<String, Object> onwardGroup = new HashMap<String, Object>();

        final String groupName = inputValues.getGroupName();
        onwardGroup.put(ClientIdentifiers.KEY_INPUTVAL_GROUP_NAME, groupName);
        final Short groupLevel = inputValues.getGroupLevel();
        onwardGroup.put(ClientIdentifiers.KEY_INPUTVAL_GROUP_LEVEL, groupLevel);

        log.debug("~retrieveSimulationInputValues() : Input values for '" + groupName + "', level '" + groupLevel + "'.");
  
        final List<Map<String, String>> onwardInputValues = new ArrayList<Map<String, String>>();
  
        final List<InputValuesStructure> allInputValuesStructures = inputValues.getInputValuesStructure();
        for (final InputValuesStructure inputValuesStructure : allInputValuesStructures) {
          final String ionChannelName = inputValuesStructure.getIonChannelName();
          final String sourceAssayName = inputValuesStructure.getSourceAssayName();
          final String pIC50s = inputValuesStructure.getPIC50S();
          final String hills = inputValuesStructure.getHills();
          final String originals = inputValuesStructure.getOriginals();
          final String strategyOrders = inputValuesStructure.getStrategyOrders();
  
          final Map<String, String> onwardIonChannelValues = new HashMap<String, String>();
          onwardIonChannelValues.put(ClientIdentifiers.KEY_INPUTVAL_ION_CHANNEL_NAME, ionChannelName);
          onwardIonChannelValues.put(ClientIdentifiers.KEY_INPUTVAL_SOURCE_ASSAY_NAME, sourceAssayName);
          onwardIonChannelValues.put(ClientIdentifiers.KEY_INPUTVAL_PIC50S, pIC50s);
          onwardIonChannelValues.put(ClientIdentifiers.KEY_INPUTVAL_HILLS, hills);
          onwardIonChannelValues.put(ClientIdentifiers.KEY_INPUTVAL_ORIGINALS, originals);
          onwardIonChannelValues.put(ClientIdentifiers.KEY_INPUTVAL_STRATEGY_ORDERS, strategyOrders);
  
          onwardInputValues.add(onwardIonChannelValues);
        }
        onwardGroup.put(ClientIdentifiers.KEY_INPUTVAL_INPUT_VALUES, onwardInputValues);
        onwardGroups.add(onwardGroup);
      }
    } else {
      log.debug("~retrieveSimulationInputValues() : No input values data returned.");
    }

    return onwardGroups;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.manager.BusinessServicesProxy#retrieveSimulationProgress(long)
   */
  @Override
  public AllProgressVO retrieveSimulationProgress(final long simulationId) {
    log.debug("~retrieveSimulationProgress() : Invoked for simulation id '" + simulationId + "'.");

    final ProgressRequest progressRequest = objectFactory.createProgressRequest();
    progressRequest.getSimulationIds().add(simulationId);

    final ProgressResponse progressResponse = (ProgressResponse) getWebServiceTemplate().
                                                                 marshalSendAndReceive(progressRequest);

    // TODO : Handle multiple simulation progress data return.

    AllProgressVO allProgress = null;
    for (final ProgressRecordStructure progressRecordStructure : progressResponse.getProgressRecordStructure()) {
      final String businessManagerProgress = progressRecordStructure.getBusinessManagerProgress();
      final List<String> appManagerJobProgresses = progressRecordStructure.getAppManagerJobProgress();
      if (businessManagerProgress == null) {
        log.info("~retrieveSimulationProgress() : No business manager general progress retrieved for '" + simulationId + "'.");
      } else {
        allProgress = new AllProgressVO(businessManagerProgress, appManagerJobProgresses);
      }
    }

    return allProgress;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.manager.BusinessServicesProxy#retrieveSimulationProvenance(long)
   */
  @Override
  public String retrieveSimulationProvenance(final long simulationId) {
    log.debug("~retrieveSimulationProvenance() : Invoked for simulation id '" + simulationId + "'.");

    final ProvenanceRequest provenanceRequest = objectFactory.createProvenanceRequest();
    provenanceRequest.getSimulationIds().add(simulationId);

    final ProvenanceResponse provenanceResponse = (ProvenanceResponse) getWebServiceTemplate().marshalSendAndReceive(provenanceRequest);

    String provenance = null;
    for (final ProvenanceRecordStructure provenanceRecordStructure : provenanceResponse.getProvenanceRecordStructure()) {
      provenance = provenanceRecordStructure.getProvenance();
    }

    return provenance;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.ws.business_manager.proxy.BusinessServicesProxy#retrieveSimulationResults(long)
   */
  public ResultsVO retrieveSimulationResults(final long simulationId) {
    final String identifyingLogPrefix = "~retrieveSimulationResults() : [" + simulationId + "] : ";
    log.debug(identifyingLogPrefix.concat("Invoked."));

    final ResultsRequest resultsRequest = objectFactory.createResultsRequest();
    resultsRequest.setSimulationId(simulationId);

    // Client's call-out to business manager web services.
    final ResultsResponse resultsResponse = (ResultsResponse) getWebServiceTemplate().marshalSendAndReceive(resultsRequest);

    final Set<Map<String, Object>> results = new HashSet<Map<String, Object>>();
    boolean containingQNetData = false;

    final List<ResultsJobDataStructure> jobResultStructures = resultsResponse.getResultsJobDataStructure();
    for (final ResultsJobDataStructure jobResultStructure : jobResultStructures) {
      final Map<String, Object> jobMap = new HashMap<String, Object>();

      final long jobId = jobResultStructure.getJobId();
      final float jobPacingFrequency = jobResultStructure.getJobPacingFrequency();
      final String jobAssayGroupName = jobResultStructure.getJobAssayGroupName();
      final short jobAssayGroupLevel = jobResultStructure.getJobAssayGroupLevel();

      final String messages = jobResultStructure.getMessages();
      final String deltaAPD90PercentileNames = jobResultStructure.getDeltaAPD90PercentileNames();

      jobMap.put(ClientIdentifiers.KEY_RESULTS_JOB_ID, jobId);
      jobMap.put(ClientIdentifiers.KEY_RESULTS_JOB_PACING_FREQUENCY, jobPacingFrequency);
      jobMap.put(ClientIdentifiers.KEY_RESULTS_JOB_ASSAYGROUP_NAME, jobAssayGroupName);
      jobMap.put(ClientIdentifiers.KEY_RESULTS_JOB_ASSAYGROUP_LEVEL, jobAssayGroupLevel);

      if (messages != null && messages.length() > 0) {
        jobMap.put(ClientIdentifiers.KEY_RESULTS_MESSAGES, messages);
      }
      if (deltaAPD90PercentileNames != null) {
        /* While this is read in on a per-job basis, the %ile names (if there
           are any) will never differ between a simulation's jobs as there's
           only one ApPredict generating uniform %ile values! */
        jobMap.put(ClientIdentifiers.KEY_RESULTS_DELTA_APD90_PERCENTILE_NAMES,
                   deltaAPD90PercentileNames);
      }

      final List<ResultsConcentrationStructure> concentrationStructures = jobResultStructure.getResultsConcentrationStructure();

      // TreeMap ensures ordering by compound concentration.
      final Map<Float, Map<String, Object>> orderedConcentrations = new TreeMap<Float, Map<String, Object>>();

      for (final ResultsConcentrationStructure concentrationStructure : concentrationStructures) {
        final float compoundConcentration = concentrationStructure.getCompoundConcentration();
        final String subPrefix = "[" + jobId + "][" + compoundConcentration + "] : ";
        log.debug(identifyingLogPrefix.concat(subPrefix.concat("Retrieved.")));

        final String deltaAPD90 = concentrationStructure.getDeltaAPD90();
        final String upstrokeVelocity = concentrationStructure.getUpstrokeVelocity();
        final String times = concentrationStructure.getTimes();
        final String voltages = concentrationStructure.getVoltages();
        final String qNet = concentrationStructure.getQNet();
        if (!containingQNetData && !StringUtils.isBlank(qNet)) {
          log.debug(identifyingLogPrefix.concat(subPrefix.concat("Has qNet data.")));
          containingQNetData = true;
        }

        final Map<String, Object> concentrationData = new HashMap<String, Object>();
        concentrationData.put(ClientIdentifiers.KEY_RESULTS_DELTA_APD90, deltaAPD90);
        concentrationData.put(ClientIdentifiers.KEY_RESULTS_UPSTROKE_VELOCITY, upstrokeVelocity);
        concentrationData.put(ClientIdentifiers.KEY_RESULTS_TIMES, times);
        concentrationData.put(ClientIdentifiers.KEY_RESULTS_VOLTAGES, voltages);
        concentrationData.put(ClientIdentifiers.KEY_RESULTS_QNET, qNet);

        orderedConcentrations.put(compoundConcentration, concentrationData);
      }
      jobMap.put(ClientIdentifiers.KEY_RESULTS_CONCENTRATION_DATA, orderedConcentrations);

      results.add(jobMap);
    }

    return new ResultsVO(results, containingQNetData);
  }
}