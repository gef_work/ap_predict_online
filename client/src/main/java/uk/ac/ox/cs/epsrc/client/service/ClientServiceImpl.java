/*

  Original work: Copyright (c) 2015, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.service;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.client.ClientIdentifiers;
import uk.ac.ox.cs.epsrc.client.exception.BusinessManagerException;
import uk.ac.ox.cs.epsrc.client.exception.ClientException;
import uk.ac.ox.cs.epsrc.client.manager.BusinessManagerManager;
import uk.ac.ox.cs.epsrc.client.manager.ConfigurationManager;
import uk.ac.ox.cs.epsrc.client.value.AllProgressVO;
import uk.ac.ox.cs.epsrc.client.value.ExperimentalDataVO;
import uk.ac.ox.cs.epsrc.client.value.InputValuesVO;
import uk.ac.ox.cs.epsrc.client.value.JobDiagnosticsVO;
import uk.ac.ox.cs.epsrc.client.value.ResultsVO;
import uk.ac.ox.cs.epsrc.client.value.SimulationResponseVO;
import uk.ac.ox.cs.epsrc.client.value.config.AllConfigurations;
import uk.ac.ox.cs.epsrc.client.value.config.AllConfigurationsVO;

/**
 * Client's service implementation.
 *
 * @author geoff
 */
@Component(ClientIdentifiers.COMPONENT_CLIENT_SERVICE)
public class ClientServiceImpl implements ClientService {

  @Autowired @Qualifier(ClientIdentifiers.COMPONENT_BUSINESS_MANAGER_MANAGER)
  private BusinessManagerManager businessManagerManager;

  @Autowired @Qualifier(ClientIdentifiers.COMPONENT_CONFIGURATION_MANAGER)
  private ConfigurationManager configurationManager;

  private static final Log log = LogFactory.getLog(ClientServiceImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.service.ClientService#queryBusinessManager(java.lang.String, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.String, java.lang.String, java.lang.Boolean, java.lang.Boolean, java.lang.Float, java.lang.Float, java.lang.Short, java.math.BigDecimal, java.lang.String)
   */
  @Override
  public SimulationResponseVO queryBusinessManager(final String compoundIdentifier,
                                                   final Boolean forceReRun,
                                                   final Boolean reset,
                                                   final Boolean assayGrouping,
                                                   final Boolean valueInheriting,
                                                   final Boolean withinGroups,
                                                   final Boolean betweenGroups,
                                                   final String strategies,
                                                   final String doseResponseFitting,
                                                   final Boolean doseResponseFittingRounding,
                                                   final Boolean doseResponseFittingHillMinMax,
                                                   final Float doseResponseFittingHillMax,
                                                   final Float doseResponseFittingHillMin,
                                                   final Short cellModelIdentifier,
                                                   final BigDecimal pacingMaxTime,
                                                   final String userId)
                                                   throws BusinessManagerException,
                                                          ClientException {
    log.debug("~queryBusinessManager() : Invoked.");

    return businessManagerManager.queryBusinessManager(compoundIdentifier,
                                                       forceReRun, reset,
                                                       assayGrouping,
                                                       valueInheriting,
                                                       withinGroups,
                                                       betweenGroups, strategies,
                                                       doseResponseFitting,
                                                       doseResponseFittingRounding,
                                                       doseResponseFittingHillMinMax,
                                                       doseResponseFittingHillMax,
                                                       doseResponseFittingHillMin,
                                                       cellModelIdentifier,
                                                       pacingMaxTime, userId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.service.ClientService#retrieveDefaultConfigurations()
   */
  @Override
  public AllConfigurations retrieveDefaultConfigurations() {
    log.debug("~retrieveDefaultConfigurations() : Invoked.");

    return new AllConfigurationsVO(businessManagerManager.retrieveDefaultConfigurations(),
                                   configurationManager.retrieveDefaultConfigurations());
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.service.ClientService#retrieveExperimentalData(long, java.lang.String)
   */
  @Override
  public ExperimentalDataVO retrieveExperimentalData(final long simulationId,
                                                     final String userId) {
    log.debug("~retrieveExperimentalData() : [" + simulationId + "][" + userId + "]");

    return businessManagerManager.retrieveExperimentalData(simulationId, userId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.service.ClientService#retrieveJobDiagnostics(long)
   */
  @Override
  public JobDiagnosticsVO retrieveJobDiagnostics(final long jobId) {
    log.debug("~retrieveJobDiagnostics() : Invoked for job id '" + jobId + "'.");

    return businessManagerManager.retrieveJobDiagnostics(jobId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.service.ClientService#retrieveSimulationInputValues(long)
   */
  @Override
  public InputValuesVO retrieveSimulationInputValues(final long simulationId) {
    log.debug("~retrieveSimulationInputValues() : Invoked for simulation id '" + simulationId + "'.");

    return businessManagerManager.retrieveSimulationInputValues(simulationId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.service.ClientService#retrieveSimulationProgress(long)
   */
  @Override
  public AllProgressVO retrieveSimulationProgress(final long simulationId) {
    log.debug("~retrieveSimulationProgress() : Invoked for simulation id '" + simulationId + "'.");

    return businessManagerManager.retrieveSimulationProgress(simulationId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.service.ClientService#retrieveSimulationProvenance(long)
   */
  @Override
  public String retrieveSimulationProvenance(final long simulationId) {
    log.debug("~retrieveSimulationProvenance() : Invoked for simulation id '" + simulationId + "'.");

    return businessManagerManager.retrieveSimulationProvenance(simulationId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.service.ClientService#retrieveSimulationProvenance(long, java.lang.String, java.lang.String)
   */
  @Override
  public String retrieveSimulationProvenance(final long simulationId, final String assayName,
                                             final String ionChannelName) {
    log.debug("~retrieveSimulationProvenance() : Invoked for simulation id '" + simulationId + "', '" + assayName + "', '" + ionChannelName + "'.");

    return businessManagerManager.retrieveSimulationProvenance(simulationId, assayName,
                                                               ionChannelName);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.service.ClientService#retrieveSimulationResults(long)
   */
  @Override
  public ResultsVO retrieveSimulationResults(final long simulationId) {
    log.debug("~retrieveSimulationResults() : Invoked for simulation id '" + simulationId + "'.");

    return businessManagerManager.retrieveSimulationResults(simulationId);
  }
}
