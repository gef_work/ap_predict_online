/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.util;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.ox.cs.epsrc.client.ClientIdentifiers;

/**
 * Utility class for provenance fiddling.
 *
 * @author geoff
 */
public class ProvenanceUtil {

  private static final Log log = LogFactory.getLog(ProvenanceUtil.class);

  /**
   * Replace the following texts :
   * <ul>
   *   <li><code>summaryData</code></li>
   *   <li><code>individualData</code></li>
   *   <li><code>doseResponseData</code></li>
   * </ul>
   * ... with the text <code>children</code> so that the d3 nests on this. 
   *  
   * @param origProvenance Original provenance string.
   * @return Revised provenance string.
   */
  public static String adjustJSON(final String origProvenance) {
    log.debug("~adjustJSON() : Invoked.");

    String adjusted = null;
    try {
      final JSONObject json = new JSONObject(origProvenance);
      process(json);
      adjusted = json.toString();
    } catch (JSONException e) {
      log.warn("~adjustJSON() : '" + e.getMessage() + "'.");
      e.printStackTrace();
    }

    return adjusted;
  }

  private static void process(final JSONObject json) throws JSONException {
    Iterator<?> keys = json.keys();

    final Set<String> keySet = new HashSet<String>();
    while (keys.hasNext()) {
      keySet.add((String) keys.next());
    }

    final boolean hasSummary = keySet.contains(ClientIdentifiers.KEY_INFORMATION_SUMMARY_DATA);
    final boolean hasIndividual = keySet.contains(ClientIdentifiers.KEY_INFORMATION_INDIVIDUAL_DATA);
    final boolean hasDoseResponse = keySet.contains(ClientIdentifiers.KEY_INFORMATION_DOSE_RESPONSE_DATA);

    if (!hasSummary && !hasIndividual && !hasDoseResponse) {
      return;
    }

    if (hasSummary && hasIndividual) {
      /* In this situation replace the content of the summary and individual with a singular child
         containing processed content. */
      final JSONArray summaryData = (JSONArray) json.get(ClientIdentifiers.KEY_INFORMATION_SUMMARY_DATA);
      final JSONArray individualData = (JSONArray) json.get(ClientIdentifiers.KEY_INFORMATION_INDIVIDUAL_DATA);
      processArray(summaryData);
      processArray(individualData);
      json.remove(ClientIdentifiers.KEY_INFORMATION_SUMMARY_DATA);
      json.remove(ClientIdentifiers.KEY_INFORMATION_INDIVIDUAL_DATA);
      json.put(ClientIdentifiers.KEY_INFORMATION_CHILDREN, concatArray(summaryData, individualData));
    } else if (hasSummary) {
      // Rename the summaryData key with the children key (and process the content).
      final JSONArray summaryData = (JSONArray) json.get(ClientIdentifiers.KEY_INFORMATION_SUMMARY_DATA);
      processArray(summaryData);
      json.remove(ClientIdentifiers.KEY_INFORMATION_SUMMARY_DATA);
      json.put(ClientIdentifiers.KEY_INFORMATION_CHILDREN, summaryData);
    } else if (hasIndividual) {
      // Rename the individualData key with the children key (and process the content).
      final JSONArray individualData = (JSONArray) json.get(ClientIdentifiers.KEY_INFORMATION_INDIVIDUAL_DATA);
      processArray(individualData);
      json.remove(ClientIdentifiers.KEY_INFORMATION_INDIVIDUAL_DATA);
      json.put(ClientIdentifiers.KEY_INFORMATION_CHILDREN, individualData);
    } else if (hasDoseResponse) {
      // Simply rename the doseResponseData key.
      final JSONArray doseResponseData = (JSONArray) json.get(ClientIdentifiers.KEY_INFORMATION_DOSE_RESPONSE_DATA);
      json.put(ClientIdentifiers.KEY_INFORMATION_CHILDREN, doseResponseData);
      json.remove(ClientIdentifiers.KEY_INFORMATION_DOSE_RESPONSE_DATA);
    } else {
      //
    }
  }

  // JSONArray concatenation.
  private static JSONArray concatArray(final JSONArray... arrays) throws JSONException {
    JSONArray result = new JSONArray();
    for (JSONArray array : arrays) {
      for (int i = 0; i < array.length(); i++) {
        result.put(array.get(i));
      }
    }
    return result;
  }

  // If we've picked out a JSONArray then look at the objects therein and process them if necessary.
  private static void processArray(final JSONArray jsonArray) throws JSONException {
    for (int i = 0; i < jsonArray.length(); i++) {
      final Object arrayObj = jsonArray.get(i);
      if (arrayObj instanceof JSONObject) {
        process((JSONObject) arrayObj);
      }
    }
  }
}
