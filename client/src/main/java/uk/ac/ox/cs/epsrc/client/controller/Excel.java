/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.epsrc.client.ClientIdentifiers;
import uk.ac.ox.cs.epsrc.client.controller.util.ControllerUtil;
import uk.ac.ox.cs.epsrc.client.service.ClientService;

/**
 * Controller returning Excel views.
 *
 * @author geoff
 */
@Controller
public class Excel {

  protected static final String errorInvalidSimulationId = "Invalid simulation identifier provided";

  @Autowired @Qualifier(ClientIdentifiers.COMPONENT_CLIENT_SERVICE)
  private ClientService clientService;

  private static final Log log = LogFactory.getLog(Excel.class);

  /**
   * Generate an excel view.
   * 
   * @param compoundIdentifier Compound identifier.
   * @param simulationId simulation identifier.
   * @param model MVC model.
   * @return Bean view name.
   */
  @RequestMapping(value=ClientIdentifiers.URL_PREFIX_EXCEL,
                  method=RequestMethod.GET)
  public String generateExcelView(final @RequestParam(value=ClientIdentifiers.PARAM_NAME_COMPOUND_IDENTIFIER,
                                                      required=true)
                                        String compoundIdentifier,
                                  final @RequestParam(value=ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID,
                                                      required=true)
                                        String simulationId,
                                  final Model model) {
    log.debug("~generateExcelView() : Invoked for simulation id '" + simulationId + "'.");

    if (!ControllerUtil.validSimulationId(simulationId)) {
      log.info("~generateExcelView() : Invalid simulation id of '" + simulationId + "' provided!");
      model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                         errorInvalidSimulationId);
    } else {
      final long useSimulationId = Long.valueOf(simulationId);
      model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                         useSimulationId);
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_COMPOUND_IDENTIFIER,
                         compoundIdentifier);

      final Authentication authentication = SecurityContextHolder.getContext()
                                                                 .getAuthentication();
      final String userId = authentication.getName();

      try {
        // Data
        model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_ION_CHANNELS,
                           clientService.retrieveDefaultConfigurations().retrieveIonChannels());
        model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_INPUT_VALUES,
                           clientService.retrieveSimulationInputValues(useSimulationId));
        model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                           clientService.retrieveSimulationResults(useSimulationId));
        model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_EXPERIMENTAL_DATA,
                           clientService.retrieveExperimentalData(useSimulationId,
                                                                  userId));
      } catch (Exception e) {
        final String errorMessage = e.getMessage();
        log.warn("~generateExcelView() : Exception from Business Manager service '" + errorMessage + "'.");
        model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                           errorMessage);
      }
    }

    return ClientIdentifiers.VIEW_NAME_EXCEL;
  }
}