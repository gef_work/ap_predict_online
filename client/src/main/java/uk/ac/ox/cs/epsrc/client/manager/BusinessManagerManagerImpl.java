/*

  Original work: Copyright (c) 2015, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.manager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.client.ClientIdentifiers;
import uk.ac.ox.cs.epsrc.client.exception.BusinessManagerException;
import uk.ac.ox.cs.epsrc.client.exception.ClientException;
import uk.ac.ox.cs.epsrc.client.util.ProvenanceUtil;
import uk.ac.ox.cs.epsrc.client.value.AllProgressVO;
import uk.ac.ox.cs.epsrc.client.value.ExperimentalDataVO;
import uk.ac.ox.cs.epsrc.client.value.InputValuesVO;
import uk.ac.ox.cs.epsrc.client.value.JobDiagnosticsVO;
import uk.ac.ox.cs.epsrc.client.value.ResultsVO;
import uk.ac.ox.cs.epsrc.client.value.SimulationResponseVO;
import uk.ac.ox.cs.epsrc.client.value.config.BusinessManagerConfigurationsVO;
import uk.ac.ox.cs.epsrc.client.ws.business_manager.proxy.BusinessServicesProxy;

/**
 * Implementation of business manager manager interface.
 *
 * @author geoff
 */
@Component(ClientIdentifiers.COMPONENT_BUSINESS_MANAGER_MANAGER)
public class BusinessManagerManagerImpl implements BusinessManagerManager {

  @Autowired @Qualifier(ClientIdentifiers.COMPONENT_BUSINESS_SERVICES_PROXY) 
  private BusinessServicesProxy businessServicesProxy;

  private static final Log log = LogFactory.getLog(BusinessManagerManagerImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.manager.BusinessManagerManager#queryBusinessManager(java.lang.String, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.Boolean, java.lang.String, java.lang.String, java.lang.Boolean, java.lang.Boolean, java.lang.Float, java.lang.Float, java.lang.Short, java.math.BigDecimal, java.lang.String)
   */
  @Override
  public SimulationResponseVO queryBusinessManager(final String compoundIdentifier,
                                                   final Boolean forceReRun,
                                                   final Boolean reset,
                                                   final Boolean assayGrouping,
                                                   final Boolean valueInheriting,
                                                   final Boolean withinGroups,
                                                   final Boolean betweenGroups,
                                                   final String strategies,
                                                   final String doseResponseFitting,
                                                   final Boolean doseResponseFittingRounding,
                                                   final Boolean doseResponseFittingHillMinMax,
                                                   final Float doseResponseFittingHillMax,
                                                   final Float doseResponseFittingHillMin,
                                                   final Short cellModelIdentifier,
                                                   final BigDecimal pacingMaxTime,
                                                   final String userId)
                                                   throws BusinessManagerException,
                                                          ClientException {
    log.debug("~queryBusinessManager() : Invoked.");

    return businessServicesProxy.queryBusinessManager(compoundIdentifier,
                                                      forceReRun, reset,
                                                      assayGrouping,
                                                      valueInheriting,
                                                      withinGroups,
                                                      betweenGroups, strategies,
                                                      doseResponseFitting,
                                                      doseResponseFittingRounding,
                                                      doseResponseFittingHillMinMax,
                                                      doseResponseFittingHillMax,
                                                      doseResponseFittingHillMin,
                                                      cellModelIdentifier,
                                                      pacingMaxTime, userId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.manager.BusinessManagerManager#retrieveDefaultConfigurations()
   */
  @Override
  public BusinessManagerConfigurationsVO retrieveDefaultConfigurations() {
    log.debug("~retrieveDefaultConfigurations() : Invoked.");

    return businessServicesProxy.retrieveDefaultConfigurations();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.manager.BusinessManagerManager#retrieveExperimentalData(long, java.lang.String)
   */
  @Override
  public ExperimentalDataVO retrieveExperimentalData(final long simulationId,
                                                     final String userId) {
    log.debug("~retrieveExperimentalData() : [" + simulationId + "][" + userId + "]");

    return businessServicesProxy.retrieveExperimentalData(simulationId, userId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.manager.BusinessManagerManager#retrieveJobDiagnostics(long)
   */
  @Override
  public JobDiagnosticsVO retrieveJobDiagnostics(long jobId) {
    log.debug("~retrieveJobDiagnostics() : Invoked for job id '" + jobId + "'.");

    return businessServicesProxy.retrieveJobDiagnostics(jobId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.manager.BusinessManagerManager#retrieveSimulationInputValues(long)
   */
  @Override
  public InputValuesVO retrieveSimulationInputValues(final long simulationId) {
    final List<Map<String, Object>> simulationInputValues = businessServicesProxy.retrieveSimulationInputValues(simulationId);
    final InputValuesVO inputValues = new InputValuesVO(simulationInputValues);

    return inputValues;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.manager.BusinessManagerManager#retrieveSimulationProgress(long)
   */
  @Override
  public AllProgressVO retrieveSimulationProgress(final long simulationId) {
    log.debug("~retrieveSimulationProgress() : Invoked for simulation id '" + simulationId + "'.");

    return businessServicesProxy.retrieveSimulationProgress(simulationId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.manager.BusinessManagerManager#retrieveSimulationProvenance(long)
   */
  @Override
  public String retrieveSimulationProvenance(final long simulationId) {
    log.debug("~retrieveSimulationProvenance() : Invoked for simulation id '" + simulationId + "'.");

    String provenance = businessServicesProxy.retrieveSimulationProvenance(simulationId);
    if (provenance != null) {
      provenance = ProvenanceUtil.adjustJSON(provenance);
    }

    return provenance;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.manager.BusinessManagerManager#retrieveSimulationProvenance(long, java.lang.String, java.lang.String)
   */
  @Override
  public String retrieveSimulationProvenance(final long simulationId, final String assayName,
                                             final String ionChannelName) {
    log.debug("~retrieveSimulationProvenance() : Invoked for simulation id '" + simulationId + "', '" + assayName + "', '" + ionChannelName + "'.");

    String provenance = businessServicesProxy.retrieveSimulationProvenance(simulationId);
    if (provenance != null) {

      JSONObject jObject = null;
      try {
        jObject = new JSONObject(provenance.trim());
      } catch (JSONException e) {
        log.debug("~retrieveSimulationProvenance() : '" + e.getMessage() + "'.");
        e.printStackTrace();
      }

      final List<JSONObject> fished = new ArrayList<JSONObject>();
      if (jObject != null) {
        try {
          traverseJSON(jObject, fished, assayName, ionChannelName, 0);
          log.debug("~retrieveSimulationProvenance() : Fished '" + fished.size() + "' objects.");
        } catch (JSONException e) {
          log.error("~retrieveSimulationProvenance() : Exception '" + e.getMessage() + "' when traversing JSON.");
          e.printStackTrace();
        }
        if (fished.isEmpty()) {
          provenance = ProvenanceUtil.adjustJSON(provenance);
          log.debug("~retrieveSimulationProvenance() : Nothing fished.");
        } else {
          if (fished.size() == 1) {
            // Only one JSON object (probably representing a single summary or individual).
            log.debug("~retrieveSimulationProvenance() : Something fished - single item.");
            provenance = ProvenanceUtil.adjustJSON(fished.get(0).toString());
          } else {
            // Need to create a "parent" for the children!
            log.debug("~retrieveSimulationProvenance() : Something fished - multiple children!");
            final JSONObject parent = new JSONObject();
            try {
              // Generate fake provenance
              final JSONObject fakeProvenance = new JSONObject();
              fakeProvenance.put("text", "Placeholder node");
              fakeProvenance.put("level", "TRACE");
              final JSONArray fakeProvenanceArray = new JSONArray();
              fakeProvenanceArray.put(fakeProvenance);
              parent.put("provenance", fakeProvenanceArray);

              final JSONArray children = new JSONArray();
              for (final JSONObject fish : fished) {
                children.put(new JSONObject(ProvenanceUtil.adjustJSON(fish.toString())));
              }
              parent.put("@rid", "#tmp:tmp");
              parent.put("children", children);
            } catch (JSONException e) {
              final String errorMessage = "Exception generated during placeholder parent JSON construction '" + e.getMessage() + "'";
              log.error("~retrieveSimulationProvenance() : " + errorMessage);
              e.printStackTrace();
            }

            provenance = ProvenanceUtil.adjustJSON(parent.toString());
          }
        }
      }
    }

    log.debug("~retrieveSimulationProvenance() : Provenace is '" + provenance + "'.");

    return provenance;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.client.manager.BusinessManagerManager#retrieveSimulationResults(long)
   */
  @Override
  public ResultsVO retrieveSimulationResults(final long simulationId) {
    log.debug("~retrieveSimulationResults() : [" + simulationId + "] : Invoked.");

    return businessServicesProxy.retrieveSimulationResults(simulationId);
  }

  // A List<JSONObject> is used as there may not be a summary object, only individual objects for
  //   a particular assay and ion channel.
  private void traverseJSON(final JSONObject jObject, final List<JSONObject> fished,
                            final String assayName, final String ionChannelName, int level)
                            throws JSONException {
    level++;
    log.debug("~traverseJSON() : '" + level + "' Called.");
    Iterator<?> keys = jObject.keys();

    final Set<String> keySet = new HashSet<String>();
    while (keys.hasNext()) {
      keySet.add((String) keys.next());
    }
    log.debug("~traverseJSON() : '" + level + "' Keys '" + keySet + "'.");

    boolean found = false;
    if (keySet.contains("assayName") && keySet.contains("ionChannelName")) {
      // TODO : Could use the getString() functions!
      final String testAssayName = (String) jObject.get("assayName");
      final String testIonChannelName = (String) jObject.get("ionChannelName");

      if (testAssayName.equalsIgnoreCase(assayName) && 
          testIonChannelName.equalsIgnoreCase(ionChannelName)) {
        // Current JSON object corresponds to the assay and ion channel we're interested in.
        log.debug("~traverseJSON() : Fished @ '" + level + "'.");
        found = true;
        fished.add(jObject);
      }
    }
    if (!found) {
      // Current JSON object is not of interest, traverse nested JSON objects though.
      log.debug("~traverseJSON() : Not fished! Cast again .. ");
      for (final String key : keySet) {
        final Object object = jObject.get(key);
        if (object instanceof JSONObject) {
          log.debug("~traverseJSON() : '" + level + "' Value @ key '" + key + "' is JSONObject.");
          traverseJSON((JSONObject) object, fished, assayName, ionChannelName, level);
        } else if (object instanceof JSONArray) {
          log.debug("~traverseJSON() : '" + level + "' Value @ key '" + key + "' is JSONArray.");
          final JSONArray nestedArr = (JSONArray) object;
          if (nestedArr.length() > 0) {
            log.debug("~traverseJSON() : '" + level + "' JSONArray is length '" + nestedArr.length() + "'.");
            for (int i = 0; i < nestedArr.length(); i++) {
              final Object nestedObj = nestedArr.get(i);
              if (nestedObj instanceof JSONObject) {
                log.debug("~traverseJSON() : '" + level + "' JSONArray obj at '" + i + "' is JSONObject.");
                traverseJSON((JSONObject) nestedObj, fished, assayName, ionChannelName, level);
              } else if (nestedObj instanceof JSONArray) {
                log.debug("~traverseJSON() : '" + level + "' JSONArray obj at '" + i + "' is JSONArray.");
              } else {
                log.debug("~traverseJSON() : '" + level + "' JSONArray obj at '" + i + "' is '" + nestedObj.getClass() + "'.");
              }
            }
          } else {
            log.debug("~traverseJSON() : '" + level + "' Empty nested JSONArray.");
          }
        } else {
          log.debug("~traverseJSON() : '" + level + "' Value @ key '" + key + "' is '" + object.getClass() + "'.");
        }
      }
    }
  }
}