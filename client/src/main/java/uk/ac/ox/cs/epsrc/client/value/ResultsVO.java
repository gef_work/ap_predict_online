/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.value;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Value object for returning simulation results data.
 *
 * @author geoff
 */
public class ResultsVO {

  private final Set<Map<String, Object>> results = new HashSet<Map<String, Object>>();
  private final boolean containingQNetData;

  /**
   * Initialising constructor.
   * 
   * @param results Per-job collection of simulation results.
   * @param containingQNetData {@code true} if any qNet results calculated,
   *                           otherwise {@code false}.
   */
  public ResultsVO(final Set<Map<String, Object>> results,
                   final boolean containingQNetData) {
    if (results != null) {
      this.results.addAll(results);
    }
    this.containingQNetData = containingQNetData;
  }


  /**
   * Retrieve indicator to show if qNet calculations present in results data.
   * 
   * @return {@code true} if qNet calculations within.
   */
  public boolean isContainingQNetData() {
    return containingQNetData;
  }


  /**
   * Simulation results.
   * 
   * @return Unmodifiable simulation results (empty if none available).
   */
  public Set<Map<String, Object>> getResults() {
    return Collections.unmodifiableSet(results);
  }
}