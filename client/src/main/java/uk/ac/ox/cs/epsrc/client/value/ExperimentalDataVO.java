/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.value;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Experimental data (if any) associated with a compound.
 *
 * @author geoff
 */
public class ExperimentalDataVO {

  /** Key field - Experimental concentration (used in jsp pages) */
  public static final String KEY_EXPERIMENTAL_CONC = "experimental_conc";
  /** Key field - Experimental % Change Mean (used in jsp pages) */
  public static final String KEY_EXPERIMENTAL_PCT_CHANGE_MEAN = "experimental_pct_change_mean";
  /** Key field - Experimental % Change SEM (used in jsp pages) */
  public static final String KEY_EXPERIMENTAL_PCT_CHANGE_SEM = "experimental_pct_change_sem";

  // k: frequency (Hz), v: collection of per-concentration results.
  private Map<Float, List<Map<String, String>>> experimentalData = new TreeMap<Float, List<Map<String, String>>>();

  private static final Log log = LogFactory.getLog(ExperimentalDataVO.class);

  /**
   * Add per-frequency experimental for simulation. 
   * 
   * @param frequencyHz Frequency.
   * @param concentration Compound concentration.
   * @param qtPctChangeMean Pct change mean value at frequency and concentration.
   * @param qtPctChangeSEM Pct change SEM value at frequency and concentration.
   */
  /* A simulation optionally holds experimental data taken at potentially a number of frequencies (
   * e.g. 0.5, 1, 2) and concentrations (e.g. 0.1, 0.3, 1.0, 10.0, 30,0).
   */
  public void addExperimentalValues(final String frequencyHz, final String concentration,
                                    final String qtPctChangeMean, final String qtPctChangeSEM) {
    log.trace("~addExperimentalValues() : Invoked for frequency '" + frequencyHz + "' and concentration '" + concentration + "'");
    final Float floatFrequencyHz = new Float(frequencyHz);

    final Map<String, String> experimentalValues = new HashMap<String, String>(3);
    experimentalValues.put(KEY_EXPERIMENTAL_CONC, concentration);
    experimentalValues.put(KEY_EXPERIMENTAL_PCT_CHANGE_MEAN, qtPctChangeMean);
    experimentalValues.put(KEY_EXPERIMENTAL_PCT_CHANGE_SEM, qtPctChangeSEM);

    // Note: Experimental data keyed on frequency.
    if (experimentalData.containsKey(floatFrequencyHz)) {
      experimentalData.get(floatFrequencyHz).add(Collections.unmodifiableMap(experimentalValues));
    } else {
      final List<Map<String, String>> newValues = new ArrayList<Map<String, String>>();
      newValues.add(Collections.unmodifiableMap(experimentalValues));
      experimentalData.put(floatFrequencyHz, newValues);
    }
  }

  /**
   * Retreive experimental results.
   * <p>
   * The underlying {@code Map} is of type {@code TreeMap} and hence the Frequency keys are sorted.
   * 
   * @return (Partially unmodifiable) Experimental results (if available for compound).
   */
  public Map<Float, List<Map<String, String>>> getExperimentalData() {
    return Collections.unmodifiableMap(experimentalData);
  }
}