/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.config;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.client.ClientIdentifiers;
import uk.ac.ox.cs.epsrc.client.value.type.InputValueDisplay;

/**
 * Client configuration options.
 *
 * @author geoff
 */
// Values declared in config/appCtx.config.site.xml
@Component(ClientIdentifiers.COMPONENT_CONFIGURATION)
public class Configuration {

  private final Map<String, String> c50Units = new LinkedHashMap<String, String>();
  private final InputValueDisplay inputValueDisplay;

  private static final Log log = LogFactory.getLog(Configuration.class);

  /**
   * Initialising constructor.
   * 
   * @param inputValueDisplay Input value display option.
   * @throws IllegalArgumentException If a {@code null} input value display option chosen.
   */
  public Configuration(final InputValueDisplay inputValueDisplay) throws IllegalArgumentException {

    if (inputValueDisplay == null) {
      throw new IllegalArgumentException("An input value display option must be specified");
    }
    this.inputValueDisplay = inputValueDisplay;
  }

  @PostConstruct
  private void postConstruct() {
    log.info("~postConstruct() : '" + this.toString() + "'.");
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Configuration [c50Units=" + c50Units + ", inputValueDisplay="
        + inputValueDisplay + "]";
  }

  /**
   * Assign the C50 units.
   * 
   * @param c50Units Context-defined C50 units.
   */
  // see Tips section @ http://docs.spring.io/spring/docs/3.0.x/spring-framework-reference/html/beans.html#beans-autowired-annotation-qualifiers
  @Resource(name="c50Units")
  public void setC50Units(final Map<String, String> c50Units) {
    assert (this.c50Units.isEmpty()) : "Inappropriate attempt to reassign config C50Units."; 
    if (c50Units != null) {
      this.c50Units.putAll(c50Units);
    }
  }

  /**
   * Retrieve the configured C50 unit options.
   * 
   * @return (Unmodifiable) configured C50 unit options.
   */
  public Map<String, String> getC50Units() {
    return Collections.unmodifiableMap(c50Units);
  }

  /**
   * Retrieve the input value display choice.
   * 
   * @return Input value display choice.
   */
  public InputValueDisplay getInputValueDisplay() {
    return inputValueDisplay;
  }
}