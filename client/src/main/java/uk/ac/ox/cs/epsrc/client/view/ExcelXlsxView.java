/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.view.document.AbstractXlsxView;
import uk.ac.ox.cs.epsrc.client.ClientIdentifiers;
import uk.ac.ox.cs.epsrc.client.value.ExperimentalDataVO;
import uk.ac.ox.cs.epsrc.client.value.InputValuesVO;
import uk.ac.ox.cs.epsrc.client.value.ResultsVO;

/**
 * MVC {@code View} for displaying simulation input data and results in Excel.
 *
 * @author geoff
 */
// referenced in ????.properties, e.g. views.properties (name depends on ResourceBundleViewResolver basename in appCtx.view.xml).
public class ExcelXlsxView extends AbstractXlsxView {

  private static final String headerConcentration = "Concentration (µM)";
  private static final String headerDeltaAPD = "ΔAPD90 (%)";
  private static final String headerFreqency = "Hz";
  private static final String headerExperimentalQT = "Experimental_QT";
  private static final String headerQNet = "qNet (C/F)";

  private static final String headerSuffixName = " Name";
  private static final String headerSuffixLevel = " Level";
  private static final String headerAssayOrGroup = " Assay (or Assay Group)";
  private static final String headerSuffixPIC50s = " pIC50(s)";
  private static final String headerSuffixHills = " Hill(s)";
  private static final String headerSuffixAssaySource = "Assay source";
  private static final String headerTitleMembraneVoltage = "Membrane Voltage (mV)";
  private static final String headerTitleTime = "Time (ms)";
  private static final String sheetTitleAssayInputValuesSheet = "Assay Input Values";
  private static final String sheetTitlePctChange = "% Change";
  private static final String sheetTitleQNet = "qNet";
  private static final String sheetTitleVoltageTracesPerConcentration = "Voltage Traces (per-conc)";

  private static final String tmpKeyDelimiter = "~";
  // Place all the column titles in this row
  private static final short headerRowNumber = 0;
  // Row number to start data value entries.
  private static final short dataRowNumberStartAt = headerRowNumber + 1;
  // Row number to start % change results (now that %ile names appear in first row!)
  private static final short pctChangeDataRowNumberStartAt = dataRowNumberStartAt + 1;
  // Number of columns containing assay (or assay group) information
  private static final short numAssayInfoColumns = 2;
  // Currently showing three columns per ion channel (pIC50(s), Hill(s), Assay value source).
  private static final short columnsPerIonChannel = 3;

  private static final Log log = LogFactory.getLog(ExcelXlsxView.class);

  private class QNetResultVO {
    private final String columnTitle;
    private final Map<Float, String> qNetValues = new TreeMap<Float, String>();
    private QNetResultVO(final String columnTitle,
                         final Map<Float, String> qNetValues) {
      this.columnTitle = columnTitle;
      this.qNetValues.putAll(qNetValues);
    }
    private String getColumnTitle() {
      return this.columnTitle;
    }
    private Map<Float, String> getQNetValues() {
      return this.qNetValues;
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.compbio.client_shared.view.document.AbstractXlsView#buildExcelDocument(java.util.Map, org.apache.poi.ss.usermodel.Workbook, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
   */
  @SuppressWarnings("unchecked")
  @Override
  protected void buildExcelDocument(final Map<String, Object> model,
                                    final Workbook workbook,
                                    final HttpServletRequest request,
                                    final HttpServletResponse response)
                                    throws Exception {
    log.debug("~buildExcelDocument() : Invoked.");

    try {
      final InputValuesVO allInputValuesData = (InputValuesVO) model.get(ClientIdentifiers.MODEL_ATTRIBUTE_INPUT_VALUES);
      final ResultsVO allResultsData = (ResultsVO) model.get(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS);
      final ExperimentalDataVO allExperimentalData = (ExperimentalDataVO) model.get(ClientIdentifiers.MODEL_ATTRIBUTE_EXPERIMENTAL_DATA);

      if (allInputValuesData == null && allResultsData == null &&
          allExperimentalData == null) {
        final String message = "No input values or results available";
        log.warn("~buildExcelDocument() : " + message + ".");
        createEmptyDocument(workbook, "NoDataAvailable", message);
      } else {
        if (allInputValuesData != null) {
          loadInputValueSheet(workbook, allInputValuesData,
                              (Map<Short, Vector<String>>) model.get(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_ION_CHANNELS));
        }

        // Place the concentrations from results and/or experimental data into a collection
        final Set<Float> sortedUniqueConcentrations = retrieveAllSortedConcentrations(allResultsData,
                                                                                      allExperimentalData);

        final List<QNetResultVO> qNetResults = new ArrayList<QNetResultVO>(); 
        if (!sortedUniqueConcentrations.isEmpty()) {
          loadPctChangeSheet(workbook, allResultsData, allExperimentalData,
                             sortedUniqueConcentrations, qNetResults);
        }

        if (!qNetResults.isEmpty()) {
          loadQNetSheet(workbook, sortedUniqueConcentrations, qNetResults);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      final String message = "A data processing problem has occurred. Please advise the portal developer";
      log.error("~buildExcelDocument() : " + message + ". '" + e.getMessage() + "'");
      final int sheetCount = workbook.getNumberOfSheets();
      for (int sheetIdx = 0; sheetIdx < sheetCount; sheetIdx++) {
        try {
          workbook.removeSheetAt(sheetIdx);
        } catch (IndexOutOfBoundsException ioobe) {
          // Seems to get in a sheet count pickle if exception was generated whilst building workbook!
        }
      }
      createEmptyDocument(workbook, "ProcessingError", message);
    }
  }

  private void loadQNetSheet(final Workbook workbook,
                             final Set<Float> sortedUniqueConcentrations,
                             final List<QNetResultVO> qNetResults) {
    log.debug("~loadQNetSheet() : Invoked.");
    final List<Float> sortedListConcentrations = new ArrayList<Float>(sortedUniqueConcentrations);

    final Sheet qNetSheet = workbook.createSheet(sheetTitleQNet);

    // Create the header row
    final Row headerRow = loadConcentrationColumn(qNetSheet,
                                                      sortedListConcentrations);

    short qNetColNum = 1;
    for (final QNetResultVO qNetResult: qNetResults) {
      headerRow.createCell(qNetColNum)
               .setCellValue(new XSSFRichTextString(qNetResult.getColumnTitle()));

      for (final Map.Entry<Float, String> qNetValueME : qNetResult.getQNetValues().entrySet()) {
        final Float compoundConcentration = qNetValueME.getKey();
        final String qNet = qNetValueME.getValue();

        final short qNetRowNum = retrievePctChgDataRowNo(sortedListConcentrations,
                                                         compoundConcentration);
        final Row qNetRow = qNetSheet.getRow(qNetRowNum);
        assert (qNetRow != null) : "qNet row must have been created already when loading conc. column!";

        final Cell qNetCell = qNetRow.createCell(qNetColNum);
        try {
          final Float qNetValue = Float.valueOf(qNet);
          qNetCell.setCellValue(qNetValue);
        } catch (NumberFormatException e) {
          qNetCell.setCellValue(new XSSFRichTextString(qNet));
        }
      }

      qNetColNum++;
    }
  }

  // Create an empty document if there's no data or a problem.
  private void createEmptyDocument(final Workbook workbook,
                                   final String sheetName, final String message) {
    final Sheet sheet = workbook.createSheet(sheetName);
    final Row row = sheet.createRow(headerRowNumber);
    row.createCell((short) 0).setCellValue(new XSSFRichTextString(message));
  }

  // Load Input Value data into workbook sheet
  private void loadInputValueSheet(final Workbook workbook,
                                   final InputValuesVO allInputValuesData,
                                   final Map<Short, Vector<String>> ionChannels) {
    log.debug("~loadInputValueSheet() : Invoked.");
    // Determine the default configured ordering of ion channel columns in displays 
    final Map<String, Short> ionChannelColumnOrdering = new HashMap<String, Short>();
    // k: display order, v: v[0] name; v[1] description.
    for (final Map.Entry<Short, Vector<String>> eachIonChannel : ionChannels.entrySet()) {
      final Short displayOrder = eachIonChannel.getKey();
      final Vector<String> ionChannelData = eachIonChannel.getValue();
      final String ionChannelName = ionChannelData.get(0);
      ionChannelColumnOrdering.put(ionChannelName, displayOrder);
    }

    final Sheet assayInputValuesSheet = workbook.createSheet(sheetTitleAssayInputValuesSheet);

    // Create the header row
    final Row headerRow = assayInputValuesSheet.createRow(headerRowNumber);
    short colNum = 0;
    headerRow.createCell(colNum++).setCellValue(new XSSFRichTextString(headerAssayOrGroup.concat(headerSuffixName)));
    headerRow.createCell(colNum++).setCellValue(new XSSFRichTextString(headerAssayOrGroup.concat(headerSuffixLevel)));
    for (short ionChannelIdx = 0; ionChannelIdx < ionChannels.size(); ionChannelIdx++) { 
      final String ionChannelName = ionChannels.get(ionChannelIdx).get(0);
      headerRow.createCell(colNum++).setCellValue(new XSSFRichTextString(ionChannelName.concat(headerSuffixPIC50s)));
      headerRow.createCell(colNum++).setCellValue(new XSSFRichTextString(ionChannelName.concat(headerSuffixHills)));
      headerRow.createCell(colNum++).setCellValue(new XSSFRichTextString(ionChannelName.concat(headerSuffixAssaySource)));
    }

    // Sort input values according to group level, e.g. QSAR at the top
    final List<Map<String, Object>> allGroupsInputValuesData = allInputValuesData.getInputValues();
    final Map<Short, Map<String, Object>> sortedInputValuesData = new TreeMap<Short, Map<String, Object>>();
    for (final Map<String, Object> eachGroupInputValuesData : allGroupsInputValuesData) {
      final Short groupLevel = (Short) eachGroupInputValuesData.get(ClientIdentifiers.KEY_INPUTVAL_GROUP_LEVEL);
      sortedInputValuesData.put(groupLevel, eachGroupInputValuesData);
    }

    // Now traverse sorted input values by level and populate sheet with data.
    short rowNum = dataRowNumberStartAt;
    for (final Map.Entry<Short, Map<String, Object>> eachSortedInputValuesData : sortedInputValuesData.entrySet()) {
      final Map<String, Object> eachGroupInputValuesData = eachSortedInputValuesData.getValue();

      final Short groupLevel = (Short) eachGroupInputValuesData.get(ClientIdentifiers.KEY_INPUTVAL_GROUP_LEVEL);
      final String groupName = (String) eachGroupInputValuesData.get(ClientIdentifiers.KEY_INPUTVAL_GROUP_NAME);
      final Row dataRow = assayInputValuesSheet.createRow(rowNum++);
      dataRow.createCell((short) 0).setCellValue(new XSSFRichTextString(groupName));
      dataRow.createCell((short) 1).setCellValue(groupLevel);

      @SuppressWarnings("unchecked")
      final List<Map<String, Object>> eachGroupAllInputValues = (List<Map<String, Object>>) eachGroupInputValuesData.get(ClientIdentifiers.KEY_INPUTVAL_INPUT_VALUES);
      // Loop through the ion channel pIC50s/Hills, etc.
      for (final Map<String, Object> eachInputValues : eachGroupAllInputValues) {
        final String ionChannelName = (String) eachInputValues.get(ClientIdentifiers.KEY_INPUTVAL_ION_CHANNEL_NAME);
        final String sourceAssayName = (String) eachInputValues.get(ClientIdentifiers.KEY_INPUTVAL_SOURCE_ASSAY_NAME);
        final String pIC50s = (String) eachInputValues.get(ClientIdentifiers.KEY_INPUTVAL_PIC50S);
        final String hills = (String) eachInputValues.get(ClientIdentifiers.KEY_INPUTVAL_HILLS);

        final short displayOrder = ionChannelColumnOrdering.get(ionChannelName);
        final short startColumnNum = (short) (numAssayInfoColumns + (displayOrder * columnsPerIonChannel));
   
        dataRow.createCell((short) (startColumnNum + 0)).setCellValue(new XSSFRichTextString(pIC50s));
        dataRow.createCell((short) (startColumnNum + 1)).setCellValue(new XSSFRichTextString(hills));
        dataRow.createCell((short) (startColumnNum + 2)).setCellValue(new XSSFRichTextString(sourceAssayName));
      }
    }
  }

  private Row loadConcentrationColumn(final Sheet sheet,
                                          final List<Float> sortedListConcentrations) {

    final short concColNum = (short) 0;
    final Row headerRow = sheet.createRow(headerRowNumber);
    headerRow.createCell(concColNum)
             .setCellValue(new XSSFRichTextString(headerConcentration));
    sheet.createRow(dataRowNumberStartAt);
    short rowNum = pctChangeDataRowNumberStartAt;
    for (final Float concentration : sortedListConcentrations) {
      final Row concValueRow = sheet.createRow(rowNum++);
      concValueRow.createCell(concColNum)
                  .setCellValue(new Double(concentration.toString()));
    }

    return headerRow;
  }

  // Load Result's Pct Change / Experimental data into workbook sheet.
  @SuppressWarnings("unchecked")
  private void loadPctChangeSheet(final Workbook workbook,
                                  final ResultsVO allResultsData,
                                  final ExperimentalDataVO allExperimentalData,
                                  final Set<Float> sortedUniqueConcentrations,
                                  final List<QNetResultVO> qNetResults) {
    log.debug("~loadPctChangeSheet() : Invoked.");

    final List<Float> sortedListConcentrations = new ArrayList<Float>(sortedUniqueConcentrations);

    // % Change data sheet.
    final Sheet pctChangeSheet = workbook.createSheet(sheetTitlePctChange);
    final Row pctChangeHeaderRow = loadConcentrationColumn(pctChangeSheet,
                                                               sortedListConcentrations);

    // Voltage traces (per concentration) sheet.
    final Sheet vtPerConcSheet = workbook.createSheet(sheetTitleVoltageTracesPerConcentration);

    short pctChangeColNum = 1;
    if (allResultsData != null) {
      /* Transfer the results data from the per-pacing frequency / assay group level / assay group
         name structure to a TreeMap keyed on <assay group level>~<frequency>~<assay group name>.
         This will order the results data through natural sorting by group level then pacing frequency. */
      final Map<String, Map<Float, Map<String, Object>>> sortedData = new TreeMap<String, Map<Float, Map<String, Object>>>();

      final Set<Map<String, Object>> allResults = allResultsData.getResults();
      for (final Map<String, Object> eachResults : allResults) {
        /* eachResults structure is ...
           { concentrationData : {
               0.0 : {
                       deltaAPD90       : 0,
                       times            : -5,0,0.1,0.2,..
                       voltages         : -85.4171, -85.4169, -80.2578, -75.1608,..
                       upstrokeVelocity : null,
                       qNet             : 0.06
                     },
               0.001 : {
                         deltaAPD90      : 0.00015937,
                         times           : ..
                         voltages        : ..
                         upstrokeVelocty : null,
                         qNet             : 0.06
                       },
               ..
             },
             jobId : 13,
             jobPacingFrequency : 1.0,
             jobAssayGroupName : QSAR,
             jobAssayGroupLevel : 0,
             deltaAPD90PercentileNames : delta_APD90
           }
         */
        //final long jobId = (Long) eachResults.get(ClientIdentifiers.KEY_RESULTS_JOB_ID);
        final Float jobPacingFrequency = (Float) eachResults.get(ClientIdentifiers.KEY_RESULTS_JOB_PACING_FREQUENCY);
        final String jobAssayGroupName = (String) eachResults.get(ClientIdentifiers.KEY_RESULTS_JOB_ASSAYGROUP_NAME);
        final Short jobAssayGroupLevel = (Short) eachResults.get(ClientIdentifiers.KEY_RESULTS_JOB_ASSAYGROUP_LEVEL);
        final String jobDeltaAPD90PctileNames = (String) eachResults.get(ClientIdentifiers.KEY_RESULTS_DELTA_APD90_PERCENTILE_NAMES);
        final String jpfStr = jobPacingFrequency.toString();
        final String jaglStr = jobAssayGroupLevel.toString();
        // e.g. combinedKey = 0~1.0~QSAR, therefore sortedData sorted by group level, then frequency 
        final String combinedKey = jaglStr.concat(tmpKeyDelimiter).concat(jpfStr)
                                          .concat(tmpKeyDelimiter).concat(jobAssayGroupName)
                                          .concat(tmpKeyDelimiter).concat(jobDeltaAPD90PctileNames);

        // concentrationData corresponds to the structure above.
        final Map<Float, Map<String, Object>> concentrationData = new TreeMap<Float, Map<String, Object>>();
        concentrationData.putAll((Map<Float, Map<String, Object>>) eachResults.get(ClientIdentifiers.KEY_RESULTS_CONCENTRATION_DATA));

        sortedData.put(combinedKey, concentrationData);
      }

      short vtNextAssayFreqRowNo = headerRowNumber;

      /* Iterate through the sorted results data and populate the Excel cells. */
      for (final Map.Entry<String, Map<Float, Map<String, Object>>> eachSortedData : sortedData.entrySet()) {
        final String combinedKey = eachSortedData.getKey();
        final String[] components = combinedKey.split(tmpKeyDelimiter);
        // final String jobAssayGroupLevel = components[0];
        final String jobPacingFrequency = components[1];
        final String jobAssayGroupName = components[2];
        final String jobDeltaAPD90PctileNames = components[3];

        // % Change row title.
        final String pctChangeColumnTitle = jobAssayGroupName + " " + headerDeltaAPD + "@" + jobPacingFrequency + headerFreqency;
        pctChangeHeaderRow.createCell(pctChangeColNum).setCellValue(new XSSFRichTextString(pctChangeColumnTitle));

        // % Change APD90 % change names
        pctChangeSheet.getRow(dataRowNumberStartAt).createCell(pctChangeColNum)
                      .setCellValue(new XSSFRichTextString(jobDeltaAPD90PctileNames));

        // Voltage Trace (per concentration) row title
        final String vtPerConcTitle = jobAssayGroupName.concat("@").concat(jobPacingFrequency).concat(headerFreqency);
        final Row vtPerConcRow = vtPerConcSheet.createRow(vtNextAssayFreqRowNo++);
        vtPerConcRow.createCell((short) 0).setCellValue(new XSSFRichTextString(vtPerConcTitle));

        final Map<Float, Map<String, Object>> concentrationData = eachSortedData.getValue();

        // Keeps a record of the maximum number of result rows across all concentrations for the assay/freq
        short vtMaxCurrentRow = 0;
        // Keeps a record of which column to start at.
        short vtStartAtColNo = 0;

        // qNet data.
        final Map<Float, String> qNetPerConcValues = new TreeMap<Float, String>();

        for (final Map.Entry<Float, Map<String, Object>> eachConcentrationData : concentrationData.entrySet()) {
          final float concentration = eachConcentrationData.getKey();

          // For each concentration start at the top!
          short vtCurrentRowNo = vtNextAssayFreqRowNo;
          Row vtCurrentRow = vtPerConcSheet.getRow(vtCurrentRowNo);
          if (vtCurrentRow == null) {
            vtCurrentRow = vtPerConcSheet.createRow(vtCurrentRowNo);
          }
          // Write the concentration text.
          vtCurrentRow.createCell(vtStartAtColNo).setCellValue(new XSSFRichTextString("Conc ".concat(Float.valueOf(concentration).toString())));
          vtCurrentRowNo++;
          vtCurrentRow = vtPerConcSheet.getRow(vtCurrentRowNo);
          if (vtCurrentRow == null) {
            vtCurrentRow = vtPerConcSheet.createRow(vtCurrentRowNo);
          }
          final short vtTimesColNo = vtStartAtColNo;
          final short vtVoltagesColNo = (short) (vtTimesColNo + 1);

          vtCurrentRow.createCell(vtTimesColNo).setCellValue(new XSSFRichTextString(headerTitleTime));
          vtCurrentRow.createCell(vtVoltagesColNo).setCellValue(new XSSFRichTextString(headerTitleMembraneVoltage));
          vtCurrentRowNo++;

          /*
           * Populate % change data cell. As determined by the row of the concentration.
           */
          final short pctChangeDataRowNumber = retrievePctChgDataRowNo(sortedListConcentrations,
                                                                       concentration);
          final Row pctChangeDataRow = pctChangeSheet.getRow(pctChangeDataRowNumber);
          final Map<String, Object> compoundConcentrationValues = (Map<String, Object>) eachConcentrationData.getValue();
          final String deltaAPD90 = (String) compoundConcentrationValues.get(ClientIdentifiers.KEY_RESULTS_DELTA_APD90);

          //final String upstrokeVelocity = (String) compoundConcentrationValues.get(ClientIdentifiers.KEY_RESULTS_UPSTROKE_VELOCITY);

          // Display % change
          final Cell pctChangeCell = pctChangeDataRow.createCell(pctChangeColNum);
          try {
            final Double numericDeltaAPD90 = Double.valueOf(deltaAPD90);
            // It's a single, numeric value, probably not spread data.
            pctChangeCell.setCellValue(numericDeltaAPD90);
          } catch (NumberFormatException e) {
            // Assume that it's either spread data or a message indicator (e.g. no action potential)
            pctChangeCell.setCellValue(new XSSFRichTextString(deltaAPD90));
          }

          /*
           * Populate voltage traces sheet.
           */
          final String resultsTimes = (String) compoundConcentrationValues.get(ClientIdentifiers.KEY_RESULTS_TIMES);
          final String resultsVoltages = (String) compoundConcentrationValues.get(ClientIdentifiers.KEY_RESULTS_VOLTAGES);

          if (resultsTimes != null && resultsVoltages != null) {
            final String[] times = resultsTimes.split(",");
            final String[] voltages = resultsVoltages.split(",");

            final int timePoints = times.length;
            for (int idx = 0; idx < timePoints; idx++) {
              final Double time = Double.valueOf(times[idx]);
              final BigDecimal voltage = new BigDecimal(voltages[idx]);

              vtCurrentRow = vtPerConcSheet.getRow(vtCurrentRowNo);
              if (vtCurrentRow == null) {
                vtCurrentRow = vtPerConcSheet.createRow(vtCurrentRowNo);
              }
              vtCurrentRowNo++;

              vtCurrentRow.createCell(vtTimesColNo).setCellValue(time);
              vtCurrentRow.createCell(vtVoltagesColNo).setCellValue(voltage.doubleValue());
            }
          }

          // Increment by 3 provides for an additional padding column between concentrations.
          vtStartAtColNo += 3;

          if (vtCurrentRowNo > vtMaxCurrentRow) {
            // Keep a record of the highest number of result rows across the concentrations.
            vtMaxCurrentRow = vtCurrentRowNo;
          }

          final String qNet = (String) compoundConcentrationValues.get(ClientIdentifiers.KEY_RESULTS_QNET);
          if (!StringUtils.isBlank(qNet)) {
            qNetPerConcValues.put(concentration, qNet);
          }
        }
        if (!qNetPerConcValues.isEmpty()) {
          final String qNetColumnTitle = jobAssayGroupName + " " + headerQNet + "@" + jobPacingFrequency + headerFreqency;
          qNetResults.add(new QNetResultVO(qNetColumnTitle, qNetPerConcValues));
        }

        pctChangeColNum++;
        // Assign the next assay/freq row start point as highest result row + 1 (for a padding row).
        vtNextAssayFreqRowNo = (short) (vtMaxCurrentRow + 1);
      }
    }

    // Now append experimental data to the % Change data.
    if (allExperimentalData != null) {
      // Note that underlying Map is of type TreeMap, hence already sorted by frequency.
      final Map<Float, List<Map<String, String>>> allExperimental = allExperimentalData.getExperimentalData();
      for (final Map.Entry<Float, List<Map<String, String>>> eachExperimental : allExperimental.entrySet()) {
        final Float frequencyHz = eachExperimental.getKey();

        final String columnTitle = headerExperimentalQT.concat("@" + frequencyHz + headerFreqency);
        pctChangeHeaderRow.createCell(pctChangeColNum).setCellValue(new XSSFRichTextString(columnTitle));

        final List<Map<String, String>> experimentalValues = eachExperimental.getValue();
        for (final Map<String, String> eachExperimentalValues : experimentalValues) {
          final String compoundConcentration = (String) eachExperimentalValues.get(ExperimentalDataVO.KEY_EXPERIMENTAL_CONC);

          final String pctChangeMean = (String) eachExperimentalValues.get(ExperimentalDataVO.KEY_EXPERIMENTAL_PCT_CHANGE_MEAN);
          // final String pctChangeSEM = (String) eachExperimentalValues.get(ExperimentalDataVO.KEY_EXPERIMENTAL_PCT_CHANGE_SEM);

          final short pctChangeDataRowNumber = retrievePctChgDataRowNo(sortedListConcentrations,
                                                                       new Float(compoundConcentration));
          final Row experimentalQTDataRow = pctChangeSheet.getRow(pctChangeDataRowNumber);
          if (pctChangeMean != null) {
            experimentalQTDataRow.createCell(pctChangeColNum).setCellValue(new Double(pctChangeMean));
          }
        }
        pctChangeColNum++;
      }
    }
  }

  // Retrieve all the concentrations (sorted by value) used in experimental or simulation runs.
  private Set<Float> retrieveAllSortedConcentrations(final ResultsVO allResultsData,
                                                     final ExperimentalDataVO allExperimentalData) {
    final Set<Float> allSortedConcentrations = new TreeSet<Float>();

    // Retrieve concentrations from the results (if any).
    final Set<Map<String, Object>> allResults = allResultsData.getResults();
    for (final Map<String, Object> eachResults : allResults) {
      @SuppressWarnings("unchecked")
      final Map<Float, Map<String, Object>> concentrationData = (Map<Float, Map<String, Object>>) eachResults.get(ClientIdentifiers.KEY_RESULTS_CONCENTRATION_DATA);
      for (final Map.Entry<Float, Map<String, Object>> eachConcentrationData : concentrationData.entrySet()) {
        allSortedConcentrations.add(eachConcentrationData.getKey());
      }
    }

    // Retrieve concentrations from the experimental data (if any).
    final Map<Float, List<Map<String, String>>> allExperimental = allExperimentalData.getExperimentalData();
    for (final Map.Entry<Float, List<Map<String, String>>> eachExperimental : allExperimental.entrySet()) {
      final List<Map<String, String>> experimentalValues = eachExperimental.getValue();

      for (final Map<String, String> eachExperimentalValues : experimentalValues) {
        final String concentration = (String) eachExperimentalValues.get(ExperimentalDataVO.KEY_EXPERIMENTAL_CONC);
        allSortedConcentrations.add(new Float(concentration));
      }
    }

    return allSortedConcentrations;
  }

  // Determine % change row to place the data, based on the concentration amount.
  private short retrievePctChgDataRowNo(final List<Float> sortedListConcentrations,
                                        final Float compoundConcentration) {
    final short locationInList = (short) sortedListConcentrations.indexOf(compoundConcentration);
    return (short) (pctChangeDataRowNumberStartAt + locationInList);
  }
}