/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.value;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Holding object for all progress information.
 * <ul>
 *   <li>Business Manager overall and potentially some job progress.</li>
 *   <li>App Manager job progress (if available).</li>
 * </ul>
 *
 * @author geoff
 */
public class AllProgressVO {

  private String businessManagerProgress;
  private List<String> appManagerJobProgress = new ArrayList<String>();

  /**
   * Initialising constructor.
   * <p>
   * If there is no general progress available then don't create this object.
   * </p>
   * 
   * @param businessManagerProgress JSON-format overall simulation progress (must be provided).
   * @param appManagerJobProgress JSON-format collection of simulation job-specific progress (or 
   *                              null if none available).
   * @throws IllegalArgumentException If null argument passed for general progress.
   */
  public AllProgressVO(final String businessManagerProgress, final List<String> appManagerJobProgress) {
    if (businessManagerProgress == null) {
      throw new IllegalArgumentException("Business Manager general progress must be assigned");
    }

    this.businessManagerProgress = businessManagerProgress;
    if (appManagerJobProgress != null) {
      this.appManagerJobProgress.addAll(appManagerJobProgress);
    }
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "AllProgressVO [businessManagerProgress=" + businessManagerProgress
        + ", appManagerJobProgress=" + appManagerJobProgress + "]";
  }

  /**
   * Retrieve the business manager general (perhaps including job) progress.
   * 
   * @return JSON-format business manager general progress.
   */
  public String getBusinessManagerProgress() {
    return businessManagerProgress;
  }

  /**
   * Retrieve the app manager job progress.
   * 
   * @return Collection of JSON-format app manager job progress (or empty collection if none available).
   */
  public List<String> getAppManagerJobProgress() {
    return Collections.unmodifiableList(appManagerJobProgress);
  }
}