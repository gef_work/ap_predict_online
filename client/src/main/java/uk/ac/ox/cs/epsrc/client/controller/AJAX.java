/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.NoSuchMessageException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.epsrc.client.ClientIdentifiers;
import uk.ac.ox.cs.epsrc.client.exception.BusinessManagerException;
import uk.ac.ox.cs.epsrc.client.exception.ClientException;
import uk.ac.ox.cs.epsrc.client.service.ClientService;
import uk.ac.ox.cs.epsrc.client.value.AllProgressVO;
import uk.ac.ox.cs.epsrc.client.value.ExperimentalDataVO;
import uk.ac.ox.cs.epsrc.client.value.InputValuesVO;
import uk.ac.ox.cs.epsrc.client.value.ResultsVO;
import uk.ac.ox.cs.epsrc.client.value.SimulationResponseVO;

/**
 * MVC Controller responding to AJAX requests from the client.
 * <p>
 * This controller returns a bean name which will be detected by the appropriate
 * view resolver. In this case the MVC Model (probably containing a Java object)
 * will be converted into JSON format for the client to render using javascript.
 *
 * @author geoff
 */
@Controller
public class AJAX implements MessageSourceAware {

  protected static final String DEFAULT_EMPTY_JSON = "{}";

  private static final int maxUntranslated = 100;
  private static final Queue<Object> untranslated = new LinkedList<Object>();
  private static final String pathVarCompoundIdentifier = "compoundIdentifier";

  @Autowired @Qualifier(ClientIdentifiers.COMPONENT_CLIENT_SERVICE)
  private ClientService clientService;

  // Spring-assigned
  private MessageSource messageSource;

  private static final Log log = LogFactory.getLog(AJAX.class);

  /**
   * Bean to place into the return MVC model.
   * 
   * @author geoff
   */
  public class JSON implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String json;
    private final String exception;

    /**
     * Initialising constructor.
     * <p>
     * Generally either json or an exception message would be provided, not both.
     * </p>
     * 
     * @param json JSON text.
     * @param exception Exception message if there was a problem getting the JSON.
     */
    public JSON(final String json, final String exception) {
      this.json = json;
      this.exception = exception;
    }

    /** Retrieve the JSON.
     * 
     * @return JSON-format text (or null if not assigned).
     */
    public String getJson() {
      return json;
    }

    /**
     * Retrieve the exception message.
     * 
     * @return Exception message (or null if not assigned).
     */
    public String getException() {
      return exception;
    }
  }

  private void translateText(final JSONObject progressObject, final Locale locale)
                             throws JSONException {
    final String text = progressObject.getString(ClientIdentifiers.KEY_INFORMATION_TEXT);
    final List<Object> textArgs = new ArrayList<Object>();
    if (progressObject.has(ClientIdentifiers.KEY_INFORMATION_ARGS)) {
      final JSONArray args = progressObject.getJSONArray(ClientIdentifiers.KEY_INFORMATION_ARGS);
      if (args != null && args.length() > 0) {
        for (int argIdx = 0; argIdx < args.length(); argIdx++) {
          textArgs.add(args.get(argIdx));
        }
      }
    }
    final String useText = queryMessageSource(text, textArgs.toArray(), locale);
    progressObject.put(ClientIdentifiers.KEY_INFORMATION_TEXT, useText);
  }

  private String queryMessageSource(final String messageKey, final Object[] args, final Locale locale) {
    Object[] useArgs = null;
    if (args != null && args.length > 0) {

      useArgs = new Object[args.length];

      for (int argIdx = 0; argIdx < args.length; argIdx++) {
        final Object thisObject = args[argIdx];
        final String thisArg = thisObject.toString();
        log.debug("~queryMessageSource() : Checking arg '" + thisArg + "'.");
        final String argText = queryMessageSource(thisArg, null, locale);
        if (!thisArg.equals(argText)) {
          useArgs[argIdx] = argText;
        } else {
          useArgs[argIdx] = thisObject;
        }
      }
    }

    String useText = messageKey;
    // avoid unnecessary queries of the message source
    if (untranslated.contains(messageKey)) {
      log.trace("~queryMessageSource() : '" + messageKey + "' already untranslated... ignoring.");
    } else {
      try {
        useText = messageSource.getMessage(messageKey, useArgs, locale);
      } catch (NoSuchMessageException e) {
        if (untranslated.size() > maxUntranslated) {
          final Object evicted = untranslated.remove();
          log.debug("~queryMessageSource() : Popping '" + evicted.toString() + "' from stack.");
        }
        log.debug("~queryMessageSource() : Pushing '" + messageKey + "' to stack.");
        untranslated.add(messageKey);
      }
    }
    return useText;
  }

  /**
   * Retrieve job diagnostics information.
   * 
   * @param jobIdParameter Job identifier.
   * @param locale Locale.
   * @param model MVC model.
   * @return View bean name.
   */
  @RequestMapping(value=ClientIdentifiers.URL_PREFIX_AJAX_JOB_DIAGNOSTICS + "{" + ClientIdentifiers.PARAM_NAME_JOB_ID + "}",
                  method=RequestMethod.GET)
  public String retrieveJobDiagnostics(final @PathVariable(ClientIdentifiers.PARAM_NAME_JOB_ID)
                                             String jobIdParameter,
                                       final Locale locale,
                                       final Model model) {
    log.debug("~retrieveJobDiagnostics() : Invoked.");

    try {
      final long jobId = Long.valueOf(jobIdParameter).longValue();
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_JOB_DIAGNOSTICS,
                         clientService.retrieveJobDiagnostics(jobId));
    } catch (Exception e) {
      final Object[] args = new Object[] { "Job diagnostics for '" + jobIdParameter + "'",
                                           e.getMessage() };
      final String errorMessage = queryMessageSource(ClientIdentifiers.MSG_BUNDLE_ID_ERROR_RETRIEVING,
                                                     args, locale);
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_JOB_DIAGNOSTICS,
                         new JSON(null, errorMessage));
      log.error("~retrieveJobDiagnostics() : " + errorMessage);
    }

    return ClientSharedIdentifiers.VIEW_NAME_JSON;
  }

  /**
   * Retrieve experimental data for the simulation;
   * 
   * @param simulationId Simulation identifier.
   * @param locale Locale.
   * @param model MVC model.
   * @return View bean name.
   */
  @RequestMapping(value=ClientIdentifiers.URL_PREFIX_AJAX_EXPERIMENTAL + "{" + ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID + "}",
                  method=RequestMethod.GET)
  public String retrieveExperimental(final @PathVariable(ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID)
                                           String simulationId,
                                     final Locale locale,
                                     final Model model) {
    log.debug("~retrieveExperimental() : Invoked.");

    final Authentication authentication = SecurityContextHolder.getContext()
                                                               .getAuthentication();
    final String userId = authentication.getName();

    try {
      final ExperimentalDataVO experimentalData = clientService.retrieveExperimentalData(Long.valueOf(simulationId),
                                                                                         userId);
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_EXPERIMENTAL_DATA, experimentalData);
    } catch (Exception e) {
      final Object[] args = new Object[] { ClientIdentifiers.MSG_BUNDLE_ID_EXPERIMENTAL,
                                           e.getMessage() };
      final String errorMessage = queryMessageSource(ClientIdentifiers.MSG_BUNDLE_ID_ERROR_RETRIEVING,
                                                     args, locale);
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_EXPERIMENTAL_DATA,
                         new JSON(null, errorMessage));
      log.error("~retrieveExperimental() : " + errorMessage);
    }

    return ClientSharedIdentifiers.VIEW_NAME_JSON;
  }

  /**
   * Retrieve simulation input values;
   * 
   * @param simulationId Simulation identifier.
   * @param locale Locale.
   * @param model MVC model.
   * @return View bean name.
   */
  @RequestMapping(value=ClientIdentifiers.URL_PREFIX_AJAX_INPUTVALUES + "{" + ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID + "}",
                  method=RequestMethod.GET)
  public String retrieveInputValues(final @PathVariable(ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID)
                                          String simulationId,
                                    final Locale locale,
                                    final Model model) {
    log.debug("~retrieveInputValues() : Invoked.");

    try {
      final InputValuesVO inputValues = clientService.retrieveSimulationInputValues(Long.valueOf(simulationId));
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_INPUT_VALUES, inputValues);
    } catch (Exception e) {
      final Object[] args = new Object[] { ClientIdentifiers.MSG_BUNDLE_ID_INPUT_VALUES,
                                           e.getMessage() };
      final String errorMessage = queryMessageSource(ClientIdentifiers.MSG_BUNDLE_ID_ERROR_RETRIEVING,
                                                     args, locale);
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_INPUT_VALUES,
                         new JSON(null, errorMessage));
      log.error("~retrieveInputValues() : " + errorMessage);
    }

    return ClientSharedIdentifiers.VIEW_NAME_JSON;
  }

  /**
   * Retrieve whatever progress information (Business Manager - general and job, App Manager - job only)
   * is available for the simulation.
   * <p>
   * This routine will take all the business manager progress data and append any job-specific app
   * manager data to the corresponding business manager job progress data. Typically the business
   * manager job progress data will be related to job creation (i.e. "Job 1 created"), whilst app
   * manager progress data would be expected to be the physical progress of a running simulation 
   * (i.e. "49% complete").
   * </p>
   * 
   * @param simulationId Simulation identifier.
   * @param locale Locale.
   * @param model MVC model.
   * @return View bean name.
   */
  @RequestMapping(value=ClientIdentifiers.URL_PREFIX_AJAX_PROGRESS + "{" + ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID + "}",
                  method=RequestMethod.GET)
  public String retrieveProgress(final @PathVariable(ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID)
                                       String simulationId,
                                 final Locale locale,
                                 final Model model) {
    log.debug("~retrieveProgress() : Invoked for simulation id '" + simulationId + "'.");
    try {
      final AllProgressVO allProgress = clientService.retrieveSimulationProgress(Long.valueOf(simulationId));

      JSONObject simulationProgress = new JSONObject(DEFAULT_EMPTY_JSON);

      if (allProgress != null && !allProgress.getBusinessManagerProgress().isEmpty()) {
        log.debug("~retrieveProgress() : " + allProgress.toString());
        simulationProgress = new JSONObject(allProgress.getBusinessManagerProgress());
        final JSONArray overallProgress = simulationProgress.getJSONArray(ClientIdentifiers.KEY_INFORMATION_PROGRESS);
        for (int i = 0; i < overallProgress.length(); i++) {
          // convert 
          translateText(overallProgress.getJSONObject(i), locale);
        }

        JSONArray businessManagerJobProgress = null;
        try {
          // This job data was generated by the Business Manager
          businessManagerJobProgress = simulationProgress.getJSONArray(ClientIdentifiers.KEY_INFORMATION_PROGRESS_JOBS);
        } catch (JSONException e) {
          log.debug("~retrieveProgress() : No job progress data available yet.");
        }
        if (businessManagerJobProgress != null) {
          log.debug("~retrieveProgress() : Business Manager job progress available.");
          final Map<Long, JSONArray> appManagerJobProgressHolder= new HashMap<Long, JSONArray>();
          // This job data was generated by the App Manager - temporarily store it based on job id
          final List<String> appManagerJobProgress = allProgress.getAppManagerJobProgress();
          if (!appManagerJobProgress.isEmpty()) {
            for (final String eachAppManagerJobProgress : appManagerJobProgress) {
              final JSONObject eachAppManagerJobObject = new JSONObject(eachAppManagerJobProgress);
              final Long appJobId = eachAppManagerJobObject.getLong(ClientIdentifiers.KEY_INFORMATION_JOB_ID);
              final JSONArray appJobProgress = eachAppManagerJobObject.getJSONArray(ClientIdentifiers.KEY_INFORMATION_PROGRESS);
              if (appJobProgress.length() > 0) {
                appManagerJobProgressHolder.put(appJobId, appJobProgress);
              }
            }
          }

          // Traverse the Business Manager job progress and translate
          for (int i = 0; i < businessManagerJobProgress.length(); i++) {
            log.debug("~retrieveProgress() : '" + i + "' retrieved '" + businessManagerJobProgress.getJSONObject(i) + "'.");
            final JSONObject eachBusinessJobObject = businessManagerJobProgress.getJSONObject(i);
            final Long businessJobId = eachBusinessJobObject.getLong(ClientIdentifiers.KEY_INFORMATION_JOB_ID);
            // Use ptr to Business Manager job progress to append any stored App Manager job progress
            final JSONArray businessJobProgress = eachBusinessJobObject.getJSONArray(ClientIdentifiers.KEY_INFORMATION_PROGRESS);
            for (int j = 0; j < businessJobProgress.length(); j++) {
              translateText(businessJobProgress.getJSONObject(j), locale);
            }
            // Append any App Manager job progress to the Business Manager job progress
            final JSONArray heldAppManagerJobProgress = appManagerJobProgressHolder.get(businessJobId);
            if (heldAppManagerJobProgress != null) {
              // Note: Reverse order appending!
              for (int k = heldAppManagerJobProgress.length() - 1; k > -1; k--) {
                businessJobProgress.put(heldAppManagerJobProgress.getJSONObject(k));
              }
            }
          }
        } else {
          assert(allProgress.getAppManagerJobProgress().isEmpty()) : "No Business Manager job progress data but App Manager job progress data found!";
        }
        log.debug("~retrieveProgress() : '" + simulationProgress.toString() + "'.");
      } else {
        // Hmm! Doesn't seem to be any progress yet.
      }

      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_PROGRESS_OVERALL,
                         new JSON(simulationProgress.toString(), null));

    } catch (Exception e) {
      final String errorMessage = "Simulation '" + simulationId + "' progress retrieval/processing error: '" + e.getMessage() + "'.";
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_PROGRESS_OVERALL, new JSON(null, errorMessage));
      log.error("~retrieveProgress() : " + errorMessage);
    }

    return ClientSharedIdentifiers.VIEW_NAME_JSON;
  }

  /**
   * Retrieve simulation input data provenance.
   * 
   * @param simulationId Simulation identifier.
   * @param locale Locale.
   * @param model MVC model.
   * @return View bean name.
   */
  @RequestMapping(value=ClientIdentifiers.URL_PREFIX_AJAX_PROVENANCE + "{" + ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID + "}",
                  method=RequestMethod.GET)
  public String retrieveProvenance(final @PathVariable(ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID)
                                         String simulationId,
                                   final Locale locale,
                                   final Model model) {
    log.debug("~retrieveProvenance() : Invoked for simulation id '" + simulationId + "'");

    try {
      String provenance = clientService.retrieveSimulationProvenance(Long.valueOf(simulationId));

      if (provenance != null) {
        final JSONObject provenanceJSON = new JSONObject(provenance);
        if (provenanceJSON.has(ClientIdentifiers.KEY_INFORMATION_PROVENANCE)) {
          final JSONArray level1ProvenanceArray = provenanceJSON.getJSONArray(ClientIdentifiers.KEY_INFORMATION_PROVENANCE);
          for (int i = 0; i < level1ProvenanceArray.length(); i++) {
            translateText(level1ProvenanceArray.getJSONObject(i), locale);
          }
          if (provenanceJSON.has(ClientIdentifiers.KEY_INFORMATION_CHILDREN)) {
            processProvenanceInChildren(provenanceJSON.getJSONArray(ClientIdentifiers.KEY_INFORMATION_CHILDREN),
                                        locale);
          }
          provenance = provenanceJSON.toString();
        } else {
          final String warnMessage = "Could not find provenance key in data!";
          log.warn("~retrieveProvenance() : ".concat(warnMessage).concat(" : ").concat(provenance));
          throw new UnsupportedOperationException(warnMessage);
        }
      }
      log.debug("~retrieveProvenance() : Retrieved provenance '" + provenance + "'.");
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_PROVENANCE, new JSON(provenance, null));
    } catch (Exception e) {
      final String errorMessage = "Exception derived from simulation provenance retrieval '" + e.getMessage() + "'.";
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_PROVENANCE, new JSON(null, errorMessage));
      log.error("~retrieveProvenance() : " + errorMessage);
    }

    return ClientSharedIdentifiers.VIEW_NAME_JSON;
  }

  private void processProvenanceInChildren(final JSONArray children, final Locale locale)
                                           throws JSONException {
    for (int i = 0; i < children.length(); i++) {
      final JSONObject childObject = children.getJSONObject(i);
      if (childObject.has(ClientIdentifiers.KEY_INFORMATION_PROVENANCE)) {
        final JSONArray childProvenanceArray = childObject.getJSONArray(ClientIdentifiers.KEY_INFORMATION_PROVENANCE);
        for (int j = 0; j < childProvenanceArray.length(); j++) {
          translateText(childProvenanceArray.getJSONObject(j), locale);
        }
      }
      if (childObject.has(ClientIdentifiers.KEY_INFORMATION_CHILDREN)) {
        processProvenanceInChildren(childObject.getJSONArray(ClientIdentifiers.KEY_INFORMATION_CHILDREN),
                                                 locale);
      }
    }
  }

  /**
   * Retrieve simulation input data provenance.
   * 
   * @param simulationId Simulation identifier.
   * @param assayName Assay name.
   * @param ionChannelName Ion channel name.
   * @param locale Locale.
   * @param model MVC model.
   * @return View bean name.
   */
  @RequestMapping(value=ClientIdentifiers.URL_PREFIX_AJAX_PROVENANCE + "{" + ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID + "}"
                                                                     + "/{" + ClientIdentifiers.PARAM_NAME_ASSAY + "}"
                                                                     + "/{" + ClientIdentifiers.PARAM_NAME_ION_CHANNEL + "}",
                  method=RequestMethod.GET)
  public String retrieveProvenance(final @PathVariable(ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID)
                                         String simulationId,
                                   final @PathVariable(ClientIdentifiers.PARAM_NAME_ASSAY)
                                         String assayName,
                                   final @PathVariable(ClientIdentifiers.PARAM_NAME_ION_CHANNEL)
                                         String ionChannelName,
                                   final Locale locale,
                                   final Model model) {
    log.debug("~retrieveProvenance() : Invoked for simulation id '" + simulationId + "', '" + assayName + "', '" + ionChannelName + "'.");

    try {
      final String provenance = clientService.retrieveSimulationProvenance(Long.valueOf(simulationId),
                                                                           assayName,
                                                                           ionChannelName);
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_PROVENANCE,
                         new JSON(provenance, null));
    } catch (Exception e) {
      final String errorMessage = "Exception derived from simulation provenance retrieval '" + e.getMessage() + "'.";
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_PROVENANCE,
                         new JSON(null, errorMessage));
      log.error("~retrieveProvenance() : " + errorMessage);
    }

    return ClientSharedIdentifiers.VIEW_NAME_JSON;
  }

  
  /**
   * Retrieve simulation results;
   * 
   * @param simulationId Simulation identifier.
   * @param locale Locale.
   * @param model MVC model.
   * @return View bean name.
   */
  @RequestMapping(value=ClientIdentifiers.URL_PREFIX_AJAX_RESULTS + "{" + ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID + "}",
                  method=RequestMethod.GET)
  public String retrieveResults(final @PathVariable(ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID)
                                      String simulationId,
                                final Locale locale,
                                final Model model) {
    log.debug("~retrieveResults() : Invoked.");

    try {
      final ResultsVO results = clientService.retrieveSimulationResults(Long.valueOf(simulationId));
      model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                         results);
      model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_QNET_AVAILABLE,
                         results.isContainingQNetData());
    } catch (Exception e) {
      final Object[] args = new Object[] { ClientIdentifiers.MSG_BUNDLE_ID_RESULTS,
                                           e.getMessage() };
      final String errorMessage = queryMessageSource(ClientIdentifiers.MSG_BUNDLE_ID_ERROR_RETRIEVING,
                                                     args, locale);
      model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                         new JSON(null, errorMessage));
      log.error("~retrieveResults() : " + errorMessage);
    }

    return ClientSharedIdentifiers.VIEW_NAME_JSON;
  }

  /**
   * Simulation run request for identified compound.
   * 
   * @param compoundIdentifier Compound identifier.
   * @param forceReRun True if forcing a simulation re-run, otherwise false.
   * @param reset True if forcing a simulation reset, otherwise false.
   * @param assayGrouping True if assays are to be grouped, otherwise false.
   * @param valueInheriting True if values are to be inherited (betw. assays or assay groups), otherwise false.
   * @param withinGroups True if values are to be inherited within groups, otherwise false.
   * @param betweenGroups True if values are to be inherited between groups, otherwise false.
   * @param strategies pIC50 evaluation strategy.
   * @param doseResponseFitting True if dose-response fitting required, otherwise false.
   * @param doseResponseFittingRounding True if dose-response fitted data is to be rounded, otherwise false.
   * @param doseResponseFittingHillMinMax True if dose-response fitting Hill min/max value assignment.
   * @param doseResponseFittingHillMax If dose-response fitting, the maximum Hill Coefficient value.
   * @param doseResponseFittingHillMin If dose-response fitting, the minimum Hill Coefficient value.
   * @param cellModelIdentifier CellML Model identifier.
   * @param pacingMaxTime Maximum pacing time.
   * @param locale Locale.
   * @param model MVC model.
   * @return View bean name.
   */
  @RequestMapping(value=ClientIdentifiers.URL_PREFIX_AJAX_SIMULATION + "{" + pathVarCompoundIdentifier + "}",
                  method=RequestMethod.POST)
  public String runSimulation(final @PathVariable(pathVarCompoundIdentifier)
                                    String compoundIdentifier,
                              final @RequestParam(value=ClientIdentifiers.PARAM_NAME_FORCE_RERUN,
                                                  required=false)
                                    Boolean forceReRun,
                              final @RequestParam(value=ClientIdentifiers.PARAM_NAME_RESET,
                                                  required=false)
                                    Boolean reset,
                              final @RequestParam(value=ClientIdentifiers.PARAM_NAME_ASSAY_GROUPING,
                                                  required=false)
                                    Boolean assayGrouping,
                              final @RequestParam(value=ClientIdentifiers.PARAM_NAME_VALUE_INHERITING,
                                                  required=false)
                                    Boolean valueInheriting,
                              final @RequestParam(value=ClientIdentifiers.PARAM_NAME_WITHIN_GROUPS,
                                                  required=false)
                                    Boolean withinGroups,
                              final @RequestParam(value=ClientIdentifiers.PARAM_NAME_BETWEEN_GROUPS,
                                                  required=false)
                                    Boolean betweenGroups,
                              final @RequestParam(value=ClientIdentifiers.PARAM_NAME_STRATEGIES,
                                                  required=false)
                                    String strategies,
                              final @RequestParam(value=ClientIdentifiers.PARAM_NAME_DOSE_RESPONSE_FITTING,
                                                  required=false)
                                    String doseResponseFitting,
                              final @RequestParam(value=ClientIdentifiers.PARAM_NAME_DOSE_RESPONSE_FITTING_ROUNDING,
                                                  required=false)
                                    Boolean doseResponseFittingRounding,
                              final @RequestParam(value=ClientIdentifiers.PARAM_NAME_DOSE_RESPONSE_FITTING_HILL_MINMAX,
                                                  required=false)
                                    Boolean doseResponseFittingHillMinMax,
                              final @RequestParam(value=ClientIdentifiers.PARAM_NAME_DOSE_RESPONSE_FITTING_HILL_MAX,
                                                  required=false)
                                    Float doseResponseFittingHillMax,
                              final @RequestParam(value=ClientIdentifiers.PARAM_NAME_DOSE_RESPONSE_FITTING_HILL_MIN,
                                                  required=false)
                                    Float doseResponseFittingHillMin,
                              final @RequestParam(value=ClientIdentifiers.PARAM_NAME_CELL_MODEL_IDENTIFIER,
                                                  required=false)
                                    Short cellModelIdentifier,
                              final @RequestParam(value=ClientIdentifiers.PARAM_NAME_PACING_MAX_TIME,
                                                  required=false)
                                    BigDecimal pacingMaxTime,
                              final Locale locale,
                              final Model model) {
    log.debug("~runSimulation() : Invoked for compound identifier '" + compoundIdentifier + "' forceReRun '" +
              forceReRun + "' reset '" + reset + "' assayGrouping '" + assayGrouping + "' valueInheriting '" +
              valueInheriting + "' withinGroups '" + withinGroups + "' betweenGroups '" + betweenGroups +
              "' strategies '" + strategies + "' doseResponseFitting '" + doseResponseFitting + 
              "' doseResponseFittingRounding '" + doseResponseFittingRounding +
              "' doseResponseFittingHillMinMax '" + doseResponseFittingHillMinMax +
              "' doseResponseFittingHillMax '" + doseResponseFittingHillMax +
              "' doseResponseFittingHillMin '" + doseResponseFittingHillMin +
              "' cellModelIdentifier '" + cellModelIdentifier +
              "' pacingMaxTime '" + pacingMaxTime + "'.");

    model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_COMPOUND_IDENTIFIER,
                       compoundIdentifier);

    if (compoundIdentifier != null) {
      final Authentication authentication = SecurityContextHolder.getContext()
                                                                 .getAuthentication();
      final String userId = authentication.getName();

      try {
        final SimulationResponseVO simulationResponse =
              clientService.queryBusinessManager(compoundIdentifier, forceReRun,
                                                 reset, assayGrouping,
                                                 valueInheriting, withinGroups,
                                                 betweenGroups, strategies,
                                                 doseResponseFitting,
                                                 doseResponseFittingRounding,
                                                 doseResponseFittingHillMinMax,
                                                 doseResponseFittingHillMax,
                                                 doseResponseFittingHillMin,
                                                 cellModelIdentifier,
                                                 pacingMaxTime, userId);
        model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                           simulationResponse.getSimulationId());
      } catch (BusinessManagerException e) {
        model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR, e.getMessage());
      } catch (ClientException e) {
        model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR, e.getMessage());
      }
    }

    return ClientSharedIdentifiers.VIEW_NAME_JSON;
  }

  /* (non-Javadoc)
   * @see org.springframework.context.MessageSourceAware#setMessageSource(org.springframework.context.MessageSource)
   */
  @Override
  public void setMessageSource(final MessageSource messageSource) {
    this.messageSource = messageSource;
  }
}