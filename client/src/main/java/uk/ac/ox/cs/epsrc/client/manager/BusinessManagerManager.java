/*

  Original work: Copyright (c) 2015, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.manager;

import java.math.BigDecimal;

import uk.ac.ox.cs.epsrc.client.exception.BusinessManagerException;
import uk.ac.ox.cs.epsrc.client.exception.ClientException;
import uk.ac.ox.cs.epsrc.client.value.AllProgressVO;
import uk.ac.ox.cs.epsrc.client.value.ExperimentalDataVO;
import uk.ac.ox.cs.epsrc.client.value.InputValuesVO;
import uk.ac.ox.cs.epsrc.client.value.JobDiagnosticsVO;
import uk.ac.ox.cs.epsrc.client.value.ResultsVO;
import uk.ac.ox.cs.epsrc.client.value.SimulationResponseVO;
import uk.ac.ox.cs.epsrc.client.value.config.BusinessManagerConfigurationsVO;

/**
 * Manager interface to business manager operations.
 *
 * @author geoff
 */
public interface BusinessManagerManager {

  /**
   * Query the business manager with the specified compound identifier.
   * 
   * @param compoundIdentifier Compound identifier.
   * @param forceReRun Force simulation re-run.
   * @param reset Reset the simulation.
   * @param assayGrouping Assay grouping indicator.
   * @param valueInheriting Value inheriting indicator.
   * @param withinGroups Value inheriting with groups indicator.
   * @param betweenGroups Value inheriting between groups indicator.
   * @param strategies Chosen PC50 evaluation strategies.
   * @param doseResponseFitting Dose-response fitting option, e.g. IC50_ONLY.
   * @param doseResponseFittingRounding Whether rounding of value to pIC50
   *                                    {@code 0} (zero) should occur.
   * @param doseResponseFittingHillMinMax Whether Hill min/max assignment should
   *                                      occur.
   * @param doseResponseFittingHillMax Max. Hill Coefficient value.
   * @param doseResponseFittingHillMin Min. Hill Coefficient value.
   * @param cellModelIdentifier CellML Model identifier.
   * @param pacingMaxTime Maximum pacing time (in minutes).
   * @param userId User identifier.
   * @return Simulation response value object.
   * @throws BusinessManagerException If the business manager croaked.
   * @throws ClientException If the client croaked.
   */
  SimulationResponseVO queryBusinessManager(String compoundIdentifier,
                                            Boolean forceReRun,
                                            Boolean reset, Boolean assayGrouping,
                                            Boolean valueInheriting,
                                            Boolean withinGroups,
                                            Boolean betweenGroups,
                                            String strategies,
                                            String doseResponseFitting,
                                            Boolean doseResponseFittingRounding,
                                            Boolean doseResponseFittingHillMinMax,
                                            Float doseResponseFittingHillMax,
                                            Float doseResponseFittingHillMin,
                                            Short cellModelIdentifier,
                                            BigDecimal pacingMaxTime,
                                            String userId)
                                            throws BusinessManagerException,
                                                   ClientException;

  /**
   * Retrieve all default configuration options.
   * 
   * @return Default configuration options.
   */
  BusinessManagerConfigurationsVO retrieveDefaultConfigurations();

  /**
   * Retrieve the experimental data associated with the compound.
   * 
   * @param simulationId Simulation identifier.
   * @param userId (Optional) User identifier.
   * @return Experimental data value object.
   */
  ExperimentalDataVO retrieveExperimentalData(long simulationId, String userId);

  /**
   * Retrieve job diagnostics information.
   * 
   * @param jobId Job identifier.
   * @return Job diagnostics information value object -- potentially no data within, but not
   *         {@code null}.
   */
  JobDiagnosticsVO retrieveJobDiagnostics(long jobId);

  /**
   * Retrieve the input values for a simulation.
   * 
   * @param simulationId Simulation identifier.
   * @return Input values for the simulation.
   */
  InputValuesVO retrieveSimulationInputValues(long simulationId);

  /**
   * Retrieve the progress for a simulation.
   * 
   * @param simulationId Simulation identifier.
   * @return Progress for the simulation and it's jobs (or null if none available).
   */
  AllProgressVO retrieveSimulationProgress(long simulationId);

  /**
   * Retrieve the provenance for a simulation.
   * 
   * @param simulationId Simulation identifier.
   * @return Provenance for the simulation (or null if none available).
   */
  String retrieveSimulationProvenance(long simulationId);

  /**
   * Retrieve the provenance for individual data denoted by assay and ion channel name in a
   * simulation
   * 
   * @param simulationId Simulation identifier.
   * @param assayName Assay name.
   * @param ionChannelName Ion channel name.
   * @return Provenance for the simulation (or null if none available).
   */
  String retrieveSimulationProvenance(long simulationId, String assayName, String ionChannelName);

  /**
   * Retrieve the results for a simulation.
   * 
   * @param simulationId Simulation identifier.
   * @return Results for the simulation.
   */
  ResultsVO retrieveSimulationResults(long simulationId);

}