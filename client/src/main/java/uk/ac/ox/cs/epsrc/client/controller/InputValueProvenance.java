/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.epsrc.client.ClientIdentifiers;

/**
 * MVC Controller handling provenance display.
 *
 * @author geoff
 */
@Controller
public class InputValueProvenance {

  private static final Log log = LogFactory.getLog(InputValueProvenance.class);

  /**
   * Handle request to view provenance for a simulation.
   * 
   * @param simulationId Simulation identifier.
   * @param informationLevelMin Minimum information level.
   * @param informationLevelMax Maximum information level.
   * @param model MVC model.
   * @return InputValueProvenance view name.
   */
  @RequestMapping(method=RequestMethod.GET,
                  value="/" + ClientIdentifiers.JSP_PROVENANCE +
                        "/{" + ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID + "}" +
                        "/{" + ClientIdentifiers.PARAM_NAME_INFORMATION_LEVEL_MIN + "}" +
                        "/{" + ClientIdentifiers.PARAM_NAME_INFORMATION_LEVEL_MAX + "}")
  public String handleProvenance(final @PathVariable(ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID)
                                       Long simulationId,
                                 final @PathVariable(ClientIdentifiers.PARAM_NAME_INFORMATION_LEVEL_MIN)
                                       int informationLevelMin,
                                 final @PathVariable(ClientIdentifiers.PARAM_NAME_INFORMATION_LEVEL_MAX)
                                       int informationLevelMax,
                                 final Model model) {
    log.debug("~handleProvenance() : Invoked for simulation id '" + simulationId + "'.");

    if (simulationId != null) {
      model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                         simulationId);
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_MIN_INFORMATION_LEVEL,
                         informationLevelMin);
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_MAX_INFORMATION_LEVEL,
                         informationLevelMax);
    }

    return ClientIdentifiers.JSP_PROVENANCE;
  }

  /**
   * Handle request to view provenance for a simulation.
   * 
   * @param simulationId Simulation identifier.
   * @param assay Assay.
   * @param ionChannel Ion channel.
   * @param informationLevelMin Minimum information level.
   * @param informationLevelMax Maximum information level.
   * @param model MVC model.
   * @return InputValueProvenance view name.
   */
  @RequestMapping(method=RequestMethod.GET,
                  value="/" + ClientIdentifiers.JSP_PROVENANCE +
                        "/{" + ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID + "}" +
                        "/{" + ClientIdentifiers.PARAM_NAME_ASSAY + "}" +
                        "/{" + ClientIdentifiers.PARAM_NAME_ION_CHANNEL + "}" +
                        "/{" + ClientIdentifiers.PARAM_NAME_INFORMATION_LEVEL_MIN + "}" +
                        "/{" + ClientIdentifiers.PARAM_NAME_INFORMATION_LEVEL_MAX + "}")
  public String handleProvenance(final @PathVariable(ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID)
                                       Long simulationId,
                                 final @PathVariable(ClientIdentifiers.PARAM_NAME_ASSAY)
                                       String assay,
                                 final @PathVariable(ClientIdentifiers.PARAM_NAME_ION_CHANNEL)
                                       String ionChannel,
                                 final @PathVariable(ClientIdentifiers.PARAM_NAME_INFORMATION_LEVEL_MIN)
                                       int informationLevelMin,
                                 final @PathVariable(ClientIdentifiers.PARAM_NAME_INFORMATION_LEVEL_MAX)
                                       int informationLevelMax,
                                 final Model model) {
    log.debug("~handleProvenance() : Invoked for simulation id '" + simulationId + "', '" + assay + "', '" + ionChannel + "'.");

    if (simulationId != null) {
      model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                         simulationId);
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_MIN_INFORMATION_LEVEL,
                         informationLevelMin);
      model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_MAX_INFORMATION_LEVEL,
                         informationLevelMax);
      if (assay != null && ionChannel != null) {
        model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_ASSAY, assay);
        model.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_ION_CHANNEL,
                           ionChannel);
      }
    }

    return ClientIdentifiers.JSP_PROVENANCE;
  }
}