/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.value.config;

import java.util.List;
import java.util.Map;
import java.util.Vector;

import uk.ac.ox.cs.epsrc.client.value.type.InputValueDisplay;

/**
 * Interface to object holding both client and business-manager configuration options.
 *
 * @author geoff
 */
public interface AllConfigurations {

  /**
   * Is there assay grouping?
   * 
   * @return {@code true} if assay grouping, otherwise {@code false}.
   */
  boolean isAssayGrouping();

  /**
   * Is there input value inheriting between assay groups?
   * 
   * @return {@code true} if input value inheriting between assay groups, otherwise {@code false}.
   */
  boolean isBetweenGroups();

  /**
   * Is there dose-response fitting being rounded?
   * 
   * @return {@code true} if dose-response fitting rounding to 0 (zero), otherwise {@code false}.
   */
  boolean isDoseResponseFittingRounded();

  /**
   * Forcing a simulation re-run?
   * 
   * @return {@code true} if forcing a simulation re-run, otherwise {@code false}.
   */
  boolean isForceReRun();

  /**
   * Resetting a simulation?
   * 
   * @return {@code true} if resetting a simulation, otherwise {@code false}.
   */
  boolean isReset();

  /**
   * Is there input value inheriting?
   * 
   * @return {@code true} if input value inheriting, otherwise {@code false}.
   */
  boolean isValueInheriting();

  /**
   * Is there input value inheriting within assay groups?
   * 
   * @return {@code true} if input value inheriting within assay groups, otherwise {@code false}.
   */
  boolean isWithinGroups();

  /**
   * Retrieve the default configured C50 units, e.g. pIC50, IC50 M.
   * <p>
   * This is a client configuration property.
   * 
   * @return Default configured C50 units.
   */
  Map<String, String> retrieveC50Units();

  /**
   * Retrieve the default CellML models.
   * <p>
   * This is a business-manager configuration property.
   * 
   * @return Default configured CellML models.
   */
  List<CellModelVO> retrieveCellModels();

  /**
   * Retrieve the default maximum information level.
   * 
   * @return Default maximum information level.
   */
  short retrieveDefaultMaxInformationLevel();

  /**
   * Retrieve the default minimum information level.
   * 
   * @return Default minimum information level.
   */
  short retrieveDefaultMinInformationLevel();

  /**
   * Indicator of dose-response fitting strategy, e.g. IC50_ONLY, IC50_AND_HILL.
   * 
   * @return Dose-response fitting strategy.
   */
  String retrieveDoseResponseFitting();

  /**
   * Retrieve the maximum Hill value (when dose-response fitting).
   * 
   * @return Maximum Hill value.
   */
  float retrieveDoseResponseFittingHillMax();

  /**
   * Retrieve the minimum Hill value (when dose-response fitting).
   * 
   * @return Minimum Hill value.
   */
  float retrieveDoseResponseFittingHillMin();

  /**
   * Retrieve the Hill fitting strategies.
   * 
   * @return Hill fitting strategies.
   */
  Map<String, Boolean> retrieveHillFittingStrategy();

  /**
   * Retrieve the default configured input value display.
   * <p>
   * This is a client configuration property.
   * 
   * @return Default configured input value display.
   */
  InputValueDisplay retrieveInputValueDisplay();

  /**
   * Retrieve the default ion channels.
   * 
   * @return Default ion channels.
   */
  Map<Short, Vector<String>> retrieveIonChannels();

  /**
   * Retrieve the pIC50 evaluation strategies.
   * 
   * @return pIC50 evaluation strategies.
   */
  List<PC50EvaluationStrategyConfigVO> retrievePC50EvaluationStrategies();

  /**
   * Retrieve the simulation request processing polling period (in ms).
   * 
   * @return The simulation request processing polling period (in ms).
   */
  int retrieveSimulationRequestProcessingPolling();
}