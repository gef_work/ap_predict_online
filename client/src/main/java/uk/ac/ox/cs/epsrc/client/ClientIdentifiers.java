/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client;

/**
 * Client identifiers.
 *
 * @author geoff
 */
public class ClientIdentifiers {

  /** Component name for the Business Manager WS WSS4J security interceptor
      <p>
      See also (sample.)appCtx.ws.security-outgoing.xml */
  public static final String COMPONENT_BUS_MANAGER_SERVICES_INTERCEPTOR = "wsBusManagerServicesInterceptor";
  /** Component for Business Manager's web services proxy. */
  public static final String COMPONENT_BUSINESS_SERVICES_PROXY = "businessServicesProxy";
  public static final String COMPONENT_BUSINESS_MANAGER_MANAGER = "businessManagerManager";
  public static final String COMPONENT_CLIENT_SERVICE = "clientService";
  public static final String COMPONENT_CONFIGURATION = "configuration";
  public static final String COMPONENT_CONFIGURATION_MANAGER = "configurationManager";

  /** These two key values correspond to the data structure returned by the AJAX controller */
  public static final String KEY_JSON = "json";
  public static final String KEY_EXCEPTION = "exception";

  /** ExperimentalDataVO class property */
  public static final String KEY_EXPERIMENTAL_DATA = "experimentalData";

  /** D3 allows recursive processing by labeling elements generically as "children" */
  public static final String KEY_INFORMATION_CHILDREN = "children";
  /** These are references to NoSQL-defined vars (other element definitions occur in javascript.jsp) */
  public static final String KEY_INFORMATION_ARGS = "args";
  public static final String KEY_INFORMATION_DOSE_RESPONSE_DATA = "doseResponseData";
  public static final String KEY_INFORMATION_INDIVIDUAL_DATA = "individualData";
  public static final String KEY_INFORMATION_LEVEL = "level";
  public static final String KEY_INFORMATION_SUMMARY_DATA = "summaryData";
  public static final String KEY_INFORMATION_JOB_ID = "jobId";
  public static final String KEY_INFORMATION_PROGRESS_JOBS = "job";
  public static final String KEY_INFORMATION_PROGRESS = "progress";
  public static final String KEY_INFORMATION_PROVENANCE = "provenance";
  public static final String KEY_INFORMATION_TEXT = "text";

  public static final String KEY_INPUTVAL_GROUP_LEVEL = "groupLevel";
  public static final String KEY_INPUTVAL_GROUP_NAME = "groupName";
  public static final String KEY_INPUTVAL_HILLS = "hills";
  public static final String KEY_INPUTVAL_ORIGINALS = "originals";
  public static final String KEY_INPUTVAL_STRATEGY_ORDERS = "strategyOrders";
  /** This value corresponds to the InputValuesVO property "inputValues" */
  public static final String KEY_INPUTVAL_INPUT_VALUES = "inputValues";
  public static final String KEY_INPUTVAL_ION_CHANNEL_NAME = "ionChannelName";
  public static final String KEY_INPUTVAL_PIC50S = "pIC50s";
  public static final String KEY_INPUTVAL_SOURCE_ASSAY_NAME = "sourceAssayName";

  public static final String KEY_RESULTS_CONCENTRATION_DATA = "concentrationData";
  /** ResultsVO class property */
  public static final String KEY_RESULTS_DATA = "results";
  public static final String KEY_RESULTS_DELTA_APD90 = "deltaAPD90";
  public static final String KEY_RESULTS_DELTA_APD90_PERCENTILE_NAMES = "deltaAPD90PercentileNames";
  public static final String KEY_RESULTS_JOB_ID = "jobId";
  public static final String KEY_RESULTS_JOB_PACING_FREQUENCY = "jobPacingFrequency";
  public static final String KEY_RESULTS_JOB_ASSAYGROUP_NAME = "jobAssayGroupName";
  public static final String KEY_RESULTS_JOB_ASSAYGROUP_LEVEL = "jobAssayGroupLevel";
  public static final String KEY_RESULTS_MESSAGES = "messages";
  public static final String KEY_RESULTS_QNET = "qNet";
  public static final String KEY_RESULTS_TIMES = "times";
  public static final String KEY_RESULTS_UPSTROKE_VELOCITY = "upstrokeVelocity";
  public static final String KEY_RESULTS_VOLTAGES = "voltages";

  /** These keys are used within controllers, not in jsp */
  public static final String MSG_BUNDLE_ID_ERROR_RETRIEVING = "error.exception_retrieving";
  public static final String MSG_BUNDLE_ID_EXPERIMENTAL = "index.experimental";
  public static final String MSG_BUNDLE_ID_INPUT_VALUES = "inputvalues.input_values";
  public static final String MSG_BUNDLE_ID_RESULTS = "index.results";

  public static final String MODEL_ATTRIBUTE_ASSAY = "ma_assay";
  public static final String MODEL_ATTRIBUTE_COMPOUND_IDENTIFIER = "ma_compound_identifier";
  public static final String MODEL_ATTRIBUTE_CONFIG_ASSAY_GROUPING = "ma_config_assay_grouping";
  public static final String MODEL_ATTRIBUTE_CONFIG_BETWEEN_GROUPS = "ma_config_between_groups";
  public static final String MODEL_ATTRIBUTE_CONFIG_CELL_MODELS = "ma_config_cell_models";
  public static final String MODEL_ATTRIBUTE_CONFIG_DOSE_RESPONSE_FITTING = "ma_config_drf";
  public static final String MODEL_ATTRIBUTE_CONFIG_DOSE_RESPONSE_FITTING_ROUNDED = "ma_config_drf_rounded";
  public static final String MODEL_ATTRIBUTE_CONFIG_DOSE_RESPONSE_FITTING_HILL_MAX = "ma_config_drf_hill_max";
  public static final String MODEL_ATTRIBUTE_CONFIG_DOSE_RESPONSE_FITTING_HILL_MIN = "ma_config_drf_hill_min";
  public static final String MODEL_ATTRIBUTE_CONFIG_FORCE_RERUN = "ma_config_force_rerun";
  public static final String MODEL_ATTRIBUTE_CONFIG_HILL_FITTING_STRATEGY = "ma_config_hill_fitting_strategy";
  public static final String MODEL_ATTRIBUTE_CONFIG_ION_CHANNELS = "ma_config_ion_channels";
  public static final String MODEL_ATTRIBUTE_CONFIG_INPUT_VALUE_DISPLAY = "ma_config_input_value_display";
  public static final String MODEL_ATTRIBUTE_CONFIG_MAX_INFORMATION_LEVEL = "ma_config_max_info_level";
  public static final String MODEL_ATTRIBUTE_CONFIG_MIN_INFORMATION_LEVEL = "ma_config_min_info_level";
  public static final String MODEL_ATTRIBUTE_CONFIG_PC50_EVALUATION_STRATEGIES = "ma_config_pc50_eval_strategies";
  public static final String MODEL_ATTRIBUTE_CONFIG_RESET = "ma_config_reset";
  public static final String MODEL_ATTRIBUTE_CONFIG_SIM_REQ_POLLING = "ma_config_sim_req_polling";
  public static final String MODEL_ATTRIBUTE_CONFIG_VALUE_INHERITING = "ma_config_value_inheriting";
  public static final String MODEL_ATTRIBUTE_CONFIG_WITHIN_GROUPS = "ma_config_within_groups";
  public static final String MODEL_ATTRIBUTE_EXCEPTION = "ma_exception";
  public static final String MODEL_ATTRIBUTE_EXPERIMENTAL_DATA = "ma_experimental_data";
  public static final String MODEL_ATTRIBUTE_ION_CHANNEL = "ma_ion_channel";
  public static final String MODEL_ATTRIBUTE_INPUT_VALUES = "ma_input_values";
  public static final String MODEL_ATTRIBUTE_JOB_DIAGNOSTICS = "ma_job_diagnostics";
  public static final String MODEL_ATTRIBUTE_MAILER_CONFIGURED = "ma_mailer_configured";
  public static final String MODEL_ATTRIBUTE_PROGRESS_OVERALL = "ma_progress_overall";
  public static final String MODEL_ATTRIBUTE_PROVENANCE = "ma_provenance";
  public static final String MODEL_ATTRIBUTE_PUBLIC_RECAPTCHA_KEY = "ma_public_recaptcha_key";
  public static final String MODEL_ATTRIBUTE_CONFIG_C50_UNITS = "ma_c50_units";

  // These are slightly different in that they are not tiles-related names, they are jsp names.
  public static final String JSP_PROVENANCE = "provenance";

  // Update tiles.xml if any of these properties change.
  public static final String PAGE_DEFAULT_ERROR = "page_default_error";
  public static final String PAGE_INDEX = "page_index";

  public static final String PARAM_NAME_ASSAY = "assay";
  public static final String PARAM_NAME_ASSAY_GROUPING = "assayGrouping";
  public static final String PARAM_NAME_BETWEEN_GROUPS = "betweenGroups";
  public static final String PARAM_NAME_CELL_MODEL_IDENTIFIER = "cellModelIdentifier";
  public static final String PARAM_NAME_COMPOUND_IDENTIFIER = "compoundIdentifier";
  public static final String PARAM_NAME_DOSE_RESPONSE_FITTING = "doseResponseFitting";
  public static final String PARAM_NAME_DOSE_RESPONSE_FITTING_ROUNDING = "doseResponseFittingRounding";
  /** Parameter expressing the user's intention to assign min. and max. Hill values */
  public static final String PARAM_NAME_DOSE_RESPONSE_FITTING_HILL_MINMAX = "doseResponseFittingHillMinMax";
  public static final String PARAM_NAME_DOSE_RESPONSE_FITTING_HILL_MAX = "doseResponseFittingHillMax";
  public static final String PARAM_NAME_DOSE_RESPONSE_FITTING_HILL_MIN = "doseResponseFittingHillMin";
  public static final String PARAM_NAME_EMAIL_ADDRESS = "email_address";
  public static final String PARAM_NAME_FORCE_RERUN = "forceReRun";
  public static final String PARAM_NAME_ION_CHANNEL = "ionChannel";
  public static final String PARAM_NAME_INFORMATION_LEVEL_MAX = "informationLevelMax";
  public static final String PARAM_NAME_INFORMATION_LEVEL_MIN = "informationLevelMin";
  public static final String PARAM_NAME_JOB_ID = "jobId";
  public static final String PARAM_NAME_PACING_MAX_TIME = "pacingMaxTime";
  public static final String PARAM_NAME_RESET = "reset";
  public static final String PARAM_NAME_STRATEGIES = "strategies";
  public static final String PARAM_NAME_VALUE_INHERITING = "valueInheriting";
  public static final String PARAM_NAME_WITHIN_GROUPS = "withinGroups";

  public static final String URL_PREFIX_AJAX_JOB_DIAGNOSTICS = "ajax/jobdiagnostics/";
  public static final String URL_PREFIX_AJAX_EXPERIMENTAL = "ajax/experimental/";
  public static final String URL_PREFIX_AJAX_INPUTVALUES = "ajax/inputvalues/";
  public static final String URL_PREFIX_AJAX_PROGRESS = "ajax/progress/";
  public static final String URL_PREFIX_AJAX_PROVENANCE = "ajax/provenance/";
  public static final String URL_PREFIX_AJAX_RESULTS = "ajax/results/";
  public static final String URL_PREFIX_AJAX_SIMULATION = "ajax/simulation/";
  public static final String URL_PREFIX_EXCEL = "excel/";
  public static final String URL_PREFIX_EXTRUDER = "extruder/";

  public static final String VIEW_NAME_EXCEL = "viewExcel";

  public static final String VIEW_MAIN = "";

  private ClientIdentifiers() {}

}