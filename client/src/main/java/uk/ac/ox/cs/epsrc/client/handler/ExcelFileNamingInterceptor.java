/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import uk.ac.ox.cs.epsrc.client.ClientIdentifiers;

/**
 * Interceptor used to spot Excel requests, create a file name and set header.
 *
 * @author geoff
 */
// Referenced in appCtx.view.xml
public class ExcelFileNamingInterceptor extends HandlerInterceptorAdapter {

  private static final Log log = LogFactory.getLog(ExcelFileNamingInterceptor.class);

  /* (non-Javadoc)
   * @see org.springframework.web.servlet.handler.HandlerInterceptorAdapter#postHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object, org.springframework.web.servlet.ModelAndView)
   */
  @Override
  public void postHandle(final HttpServletRequest request,
                         final HttpServletResponse response,
                         final Object handler, final ModelAndView modelAndView)
                         throws Exception {
    log.debug("~postHandle() : Invoked.");

    final String servletPath = request.getServletPath();
    if (servletPath != null && servletPath.contains(ClientIdentifiers.URL_PREFIX_EXCEL)) {
      log.debug("~postHandle() : It's an Excel view!");

      String compoundIdentifier = (String) modelAndView.getModel()
                                                       .get(ClientIdentifiers.MODEL_ATTRIBUTE_COMPOUND_IDENTIFIER);
      if (compoundIdentifier != null) {
        compoundIdentifier = compoundIdentifier.replace(" ", "");
      }

      final String fileName = "AP-Portal_" + compoundIdentifier;
      response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".xlsx");
    }
  }
}