/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.value.config;

/**
 * PC50 evaulation strategy config structure.
 *
 * @author geoff
 */
public class PC50EvaluationStrategyConfigVO {

  private final Long id;
  private final String description;
  private final int defaultInvocationOrder;
  private final Boolean defaultActive;

  /**
   * Initialising constructor.
   * 
   * @param id Strategy identifier.
   * @param description Strategy description.
   * @param defaultInvocationOrder Default invocation order.
   * @param defaultActive Whether the strategy is active by default.
   */
  public PC50EvaluationStrategyConfigVO(final Long id, final String description,
                                        final int defaultInvocationOrder,
                                        final Boolean defaultActive) {
    this.id = id;
    this.description = description;
    this.defaultInvocationOrder = defaultInvocationOrder;
    this.defaultActive = defaultActive;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "PC50EvaluationStrategyConfigVO [id=" + id + ", description="
        + description + ", defaultInvocationOrder=" + defaultInvocationOrder
        + ", defaultActive=" + defaultActive + "]";
  }

  /**
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * @return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @return the defaultInvocationOrder
   */
  public int getDefaultInvocationOrder() {
    return defaultInvocationOrder;
  }

  /**
   * @return the defaultActive
   */
  public Boolean getDefaultActive() {
    return defaultActive;
  }
}