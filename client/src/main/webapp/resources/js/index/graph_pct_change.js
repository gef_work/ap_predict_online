var id_pct_change_prefix = 'id_pct_change_';
var pct_change_choices_trace_id = 'pct_change_choices_trace';
var pct_change_clear_selection_trace_id = 'pct_change_clear_selection_trace';
var pct_change_default_text_id = 'pct_change_default_text';
var pct_change_overview_trace_id = 'pct_change_overview_trace';
var pct_change_placeholder_trace_id = 'pct_change_placeholder_trace';

/**
 * Set the % change graph to its original state
 */
function reset_pct_change_display() {
  /* Reset the graph */
  var default_pct_change_text = jQuery('#' + pct_change_default_text_id).clone();
  default_pct_change_text.removeAttr('id');
  default_pct_change_text.show();
  DIV_RESULTS_TABBED.html(default_pct_change_text);
  DIV_RESULTS_TABBED.addClass('graph_placeholder_trace');

  /* Reset the events */
  jQuery('#' + pct_change_clear_selection_trace_id).off();
  jQuery('#' + pct_change_placeholder_trace_id).off();
  jQuery('#' + pct_change_overview_trace_id).off();

  /* Reset the choices */
  jQuery('#' + pct_change_choices_trace_id).empty();

  /* Remove any job warning messages div parents */
  jQuery('div[id^=messages_]').each(function() { jQuery(this).parent().remove(); });

  cs_graph_pctiles_to_show = [];
}

var min_pct_change = -30.0;
var max_pct_change = 30.0;

/**
 * Determine if the % change value is within configured min/max values (used to limit the 
 * % change axis extent).
 * 
 * @param pct_change % Change value.
 * @return true if pct change value is within the configured min/max values, otherwise false.
 */
function is_within_minmax(pct_change) {
  return pct_change >= min_pct_change && pct_change <= max_pct_change;
}

var pct_change_choice_container;
var confidence_checkbox;

/**
 * Show the % change graph based in the content of the js var PCT_CHANGE_DATASETS (usually 
 * populated in results.js).
 */
function show_pct_change_graph(has_qnet) {
  if (Object.size(PCT_CHANGE_DATASETS) == 0) {
    return;
  }

  var pct_change_data = [];
  jQuery.each(PCT_CHANGE_DATASETS, function(key, val) {
    pct_change_data.push(jQuery.extend(true, {}, val));
  });
  pct_change_data = cs_sort_array_on_group_level_and_pacing_frequency(pct_change_data);

  var all_ticks = [ [-2.0, '0.01'],
                    [-1.523, '0.03'],
                    [-1.0, '0.1'],
                    [-0.523, '0.3'],
                    [0.0 , '1.0'],
                    [0.477 , '3.0'],
                    [1.0 , '10.0'],
                    [1.477 , '30.0'],
                    [2.0 , '100.0'],
                    [2.477 , '300.0'],
                    [3.0 , '1000.0']
                  ];
  var log_conc_min = cs_round_3dp(3.0); 
  var log_conc_max = cs_round_3dp(-2.0);
  var set_minimum_y_axis = true;
  /* Traverse the Delta APD90 % change data to ..
     1) Determine whether we need to display the minimum Y-axis (Delta APD90 % change) data points
        (e.g. -30 <= % change <= +30) or whether there are values outside the minimum range, and,
     2) Set min and max log concentration values to determine if specific X-axis ticks are 
       required or not. */
  jQuery.each(pct_change_data, function(key, val) {
    var existing_data = val.data;
    var confidence_data = val.confidenceData;
    jQuery.each(existing_data, function(key, val) {
      var concentration = val[0];
      var pct_change = val[1];

      concentration = cs_round_3dp(concentration * 1.0);
      if (concentration > log_conc_max) log_conc_max = concentration;
      if (concentration < log_conc_min) log_conc_min = concentration;

      /* Once unset, ignore any further data. */
      if (set_minimum_y_axis) {
        /* If there's confidence data then take that into consideration */
        if (confidence_data) {
          var spreads = pct_change.split(',');
          jQuery.each(spreads, function() {
            var each_spread_value = this;
            if (jQuery.isNumeric(each_spread_value) &&
                !is_within_minmax(each_spread_value)) {
              /* It's outside the configured min/max */
              set_minimum_y_axis = false;
              return false;
            }
          });
        } else {
          if (jQuery.isNumeric(pct_change)) {
            set_minimum_y_axis = is_within_minmax(pct_change);
          }
        }
      }
    });
  });

  var specific_y_ticks;
  if (set_minimum_y_axis) {
    specific_y_ticks = [ [ min_pct_change, '-30'],
                         [ -20.0, '-20'],
                         [ -10.0, '-10'],
                         [ 0.0, '0'],
                         [ 10.0, '10'],
                         [ 20.0, '20'],
                         [ max_pct_change, '30']
                       ];
  }

  var specific_ticks = [];
  jQuery.each(all_ticks, function() {
    var this_tick_log = cs_round_3dp(this[0] * 1.0);
    var this_tick_conc = this[1] * 1.0;
    if (this_tick_log >= log_conc_min && this_tick_log <= log_conc_max) {
      specific_ticks.push([this_tick_log, this_tick_conc]);
    }
  });

  var overview_options = {
          legend: { 
                    show: true,
                    container: $('#pct_change_overview_legend_trace')
                   },
          series: {
                    lines: {
                             show: true, lineWidth: 1
                            },
                    shadowSize: 0
                  },
          xaxis: { ticks: 4 },
          yaxis: { ticks: 4 },
          grid: { color: '#999' },
          selection: { mode: 'xy' }
  };

  function getDefaultMainPanelOptions() {
    return jQuery.extend(true,
                         cs_plot_options(PLOT_PCT_CHANGE),
                         { yaxis : { ticks: specific_y_ticks },
                           xaxis : { ticks: specific_ticks } } );
  };

  var main_options = getDefaultMainPanelOptions();

  var pct_change_colour_idx = 0;
  var labeled_confidence_interval_data = {};
  $.each(pct_change_data, function(key, val) {
    /* Hard-code color indices to prevent them from shifting as things are turned on/off */
    val.color = pct_change_colour_idx;
    ++pct_change_colour_idx;

    if (val.confidenceData) {
      if (val.deltaAPD90PctileNames === undefined) {
        alert('graph_pct_change.js : val.deltaAPD90PctileNames === undefined');
      }
      cs_graph_percentiles.pre_process_pctiles(val, labeled_confidence_interval_data);
    }
  });

  /* plot the original graph now and retain a holder for it. */
  var plot = cs_fplot($('#' + pct_change_placeholder_trace_id), pct_change_data, main_options);
  /* plot the overview graph and legend and retain a holder for it.*/
  var overview = $.plot($('#pct_change_overview_trace'), pct_change_data, overview_options);
  /* Make the plot overview retain the original axes, even when different data is plotted.*/
  overview_options = $.extend(true, {}, overview_options, {
                                                            xaxis: {
                                                                     min: overview.getAxes().xaxis.min,
                                                                     max: overview.getAxes().xaxis.max
                                                                   },
                                                            yaxis: { 
                                                                     min: overview.getAxes().yaxis.min,
                                                                     max: overview.getAxes().yaxis.max
                                                                   }
                                                          });
  /* insert checkboxes */
  pct_change_choice_container = $('#' + pct_change_choices_trace_id);
  pct_change_choice_container.text('');
  $.each(pct_change_data, function(key, val) {
    var pct_change_div = jQuery('<div />');
    var plot_label = val.label;

    var plot_colour = val.color;
    var plot_messages = val.messages;

    /* class legendColorBox defined in jquery.flot.js in function insertLegend() */
    var legendColorBox = $('#pct_change_overview_legend_trace .legendColorBox:eq(' + plot_colour + ') div div');

    /* moz wasn't happy using border-color, which ie didn't mind! */
    var plot_colour = legendColorBox.css('border-left-color');
    var input_id = id_pct_change_prefix + key;

    var input_checkbox = jQuery('<input />').attr( { type: 'checkbox', name: key, checked: 'checked',
                                                     id: input_id } );
    var new_span = jQuery('<span />').css('background-color', plot_colour).html('&nbsp;&nbsp;');
    var input_label = jQuery('<label />').attr( { 'for': input_id } ).text(plot_label);

    pct_change_div.append(input_checkbox);
    pct_change_div.append('&nbsp;');
    pct_change_div.append(new_span);
    /* show action potential radio button if not experimental */
    if (plot_label.indexOf(EXPERIMENTAL_LABEL) < 0) {
      pct_change_div.append('&nbsp;&nbsp;');
      var actionpotential_radio = jQuery('<input />').attr( { type: 'radio',
                                                              name: 'actionpotential_radio',
                                                              title: TEXT_SHOW_ACTIONPOTENTIAL,
                                                              value: plot_label } );
      pct_change_div.append(actionpotential_radio);
    }
    pct_change_div.append('&nbsp;&nbsp;');
    pct_change_div.append(input_label);
    /* show message view buttons if there's any to display for plot */
    if (plot_messages) {
      var message_button_id = 'msgbutton_' + key;
      var message_div_id = 'messages_' + key;

      var message_button = jQuery('<button />').attr( { id: message_button_id } ).text(TEXT_VIEW_MESSAGES);
      var message_div = jQuery('<div />').attr( { id: message_div_id, title: plot_label } );
      message_div.append(jQuery('<pre />').text(plot_messages));

      pct_change_div.append('&nbsp;&nbsp;');
      pct_change_div.append(message_button);
      pct_change_div.append(message_div);
    }
    pct_change_choice_container.append(pct_change_div);
  });

  /* There's no guarantee that these two values will ever be defined, e.g. if only showing QSAR! */
  var confidence_checkbox;
  var pctile_span;

  /* Append a checkbox if there's confidence interval data available. */
  if (Object.size(labeled_confidence_interval_data) > 0) {
    var pct_change_div = jQuery('<div />');
    var elements = {};
    cs_graph_pct_change.append_pctile_checkboxes(elements);
    confidence_checkbox = elements.confidence_checkbox;
    pctile_span = elements.pctile_span;

    pct_change_div.append(TEXT_SHOW_CONFIDENCE + '&nbsp;');
    pct_change_div.append(confidence_checkbox);
    pct_change_div.append('&nbsp;');
    pct_change_div.append(pctile_span);
    pctile_span.hide();
    pct_change_choice_container.append(pct_change_div);
  }

  /* now connect the two - this method sets the "selection" ranges on both plots, and replots the main plot. */
  $('#' + pct_change_placeholder_trace_id).bind('plotselected', function (event, ranges) {
    /* clamp the zooming to prevent eternal zoom */
    if (ranges.xaxis.to - ranges.xaxis.from < 0.00001)
      ranges.xaxis.to = ranges.xaxis.from + 0.00001;
    if (ranges.yaxis.to - ranges.yaxis.from < 0.00001)
      ranges.yaxis.to = ranges.yaxis.from + 0.00001;
    main_options = $.extend(true, {}, main_options, {
                                                      xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to },
                                                      yaxis: { min: ranges.yaxis.from, max: ranges.yaxis.to }
                                                    });
    overview.setSelection(ranges, true);
    plotAccordingToChoicesAndZooming();
  });

  /* This method just calls the method on the main plot above. */
  $('#pct_change_overview_trace').bind('plotselected', function (event, ranges) {
    plot.setSelection(ranges);
  });

  /* Gets called for all interactive data plotting. */
  function plotAccordingToChoicesAndZooming() {
    if (confidence_checkbox !== undefined) {
      confidence_checkbox.is(':checked') ? pctile_span.show() : pctile_span.hide();
    }

    cs_graph_pctiles_to_show = [];
    /* Update the %ile percents to show, based on those with the boxes currently checked */
    pct_change_choice_container.find('input.' + cs_graph_pctile_checkbox_class + ':checked')
                               .each(function() {
      cs_graph_pctiles_to_show.push(jQuery(this).val());
    });

    /* 'data' is the section of 'pct_change_data' which will be plotted forthwith. */
    var data = [];
    var show_confidence = (pctile_span !== undefined && pctile_span.is(':visible'));
    pct_change_choice_container.find('input:checked').each(function () {
                                                 /* 'name' is just the numeric index of the checked input */
                                                 var key = $(this).attr('name');
                                                 if (key && pct_change_data[key]) {
                                                   data.push(pct_change_data[key]);
                                                   var ds_data = pct_change_data[key];
                                                   var label = ds_data.label;
                                                   /* Also display any confidence data. */
                                                   if (show_confidence) {
                                                     /* 'key' is 'lower_id' and 'upper_id' values */
                                                     jQuery.each(labeled_confidence_interval_data,
                                                                 function(upper_lower_id, upper_lower_data) {
                                                       /* Only show relevant confidence data, e.g.
                                                          label starting 'Quattro,FLIPR,Barracuda@0.5 (Hz)' */
                                                       if (upper_lower_id.indexOf(label) == 0) {
                                                         var pctile = upper_lower_data.pctile;
                                                         if (jQuery.inArray(pctile, cs_graph_pctiles_to_show) != -1) {
                                                           data.push(labeled_confidence_interval_data[upper_lower_id]);
                                                         }
                                                       }
                                                     });
                                                   }
                                                 }
                                               });
    if (data.length > 0) {
      plot = cs_fplot($('#' + pct_change_placeholder_trace_id), data, main_options);
      ranges = { xaxis: { from: plot.getAxes().xaxis.min, to: plot.getAxes().xaxis.max},
                 yaxis: { from: plot.getAxes().yaxis.min, to: plot.getAxes().yaxis.max} };
      overview = $.plot($('#pct_change_overview_trace'), data, overview_options);
      overview.setSelection(ranges, true);
    }

    if (has_qnet) {
      qnet_display_c.qnet_plot_according_to_choices_and_zooming();
    }
  }

  /* Show/Hide confidence data? Checkboxes */
  pct_change_choice_container.find('input:checkbox')
                             .click(plotAccordingToChoicesAndZooming);

  /* Reset Graph button */
  $('#' + pct_change_clear_selection_trace_id).click(function() {
    if ($('#' + pct_change_placeholder_trace_id).is(':visible')) {
      /* Let the main plot choose its own axes again */
      main_options = getDefaultMainPanelOptions();

      plotAccordingToChoicesAndZooming();
      overview.clearSelection();
    } else {
      qnet_display_c.qnet_main_options = qnet_display_c.qnet_get_default_main_panel_options();
      qnet_display_c.qnet_reset_everything();
    }
  });

  var previous_point = null;
  var fixed_decimal_place = 3;
  $('#' + pct_change_placeholder_trace_id).bind('plothover', function (event, pos, item) {
    previous_point = cs_process_plothover( { plot: plot,
                                             pos: pos,
                                             item: item,
                                             previous_point: previous_point,
                                             fixed_decimal_place: fixed_decimal_place,
                                             inversing: true,
                                             x: { element: $('#point_val_x'),
                                                  label: TEXT_CONC,
                                                  units: '&micro;M' },
                                             y: { element: $('#point_val_y'),
                                                  label: TEXT_DELTA_APD90,
                                                  units: '%' } } );
  });

  /* leave the hiding until last (otherwise flot may complain */
  $('#pct_change_overview_trace').hide();
  $('.graph_pct_change_overview_legend_trace').hide();
}
