/**
 * Add a class to an element if there's no warning/error classes already.
 * 
 * @param progress_element jQuery object representing element to add class to.
 * @param class_name Name of class to add.
 */
function add_alternate_class(progress_element, class_name) {
  if (!progress_element.hasClass('level_WARN') && !progress_element.hasClass('level_ERROR')) {
    progress_element.addClass(class_name);
  }
}

/**
 * Generate the flag span element text.
 *
 * @param text Text to prepend the flag image with.
 * @param flag_id Flag identifier.
 * @return Complete text, e.g. QSAR@0.5Hz + flag in span element text.
 */
function generate_tab_title(text, flag_id) {
  var span = jQuery('<span />').attr( { 'id' : flag_id } )
                               .addClass('ui-inline-icon ui-icon-flag ui-incline-neutral flag_style monospace border_5')
                               .html('&nbsp;');
  return text + ' ' + span.prop('outerHTML');
}

/**
 * Retrieve a textual element id for a flag.
 *
 * @return Textual flag identifier.
 */
function get_flag_id(id) {
  return PFX_FLAG + id;
}

/**
 * Retrieve a textual element id for job progress.
 *
 * @return Textual job progress identifier.
 */
function get_job_progress_id(id) {
  return PFX_PROGRESS_JOB + id;
}

/**
 * Retrieve the progress div jQuery element.
 *
 * @return Progress div jQuery element.
 */
function get_progress_div() {
  return jQuery('#progress');
}

/**
 * Process the AJAX response
 *
 * @param simulation_id Simulation identifier.
 * @param response AJAX progress response.
 */
function process_progress_response(simulation_id, response) {

  var make_progress_call = false;

  var response_obj = response[MODEL_ATTRIBUTE_PROGRESS_OVERALL];
  if (response_obj != undefined) {
    /* defined in AJAXController inner class */
    var exception = response_obj[KEY_EXCEPTION];
    if (exception != undefined) {
      show_js_error(exception);
    } else {
      /* defined in AJAXController inner class */
      var json = response_obj[KEY_JSON];
      if (json != undefined) {
        if (json == "{}") {
          make_progress_call = true;
        } else {
          var progress_all;
          try {
            progress_all = jQuery.parseJSON(json);
          } catch (e) {
            show_js_error('progress.js#process_progress_response(simulation_id, response) : Caught ' + e.message);
          }
          if (progress_all != undefined) {
            show_progress(progress_all);
            if (!progress_overall_completed) {
              make_progress_call = true;
            }
          }
        }
      } else {
        show_js_error('Could not find \'' + KEY_JSON + '\' in ajax progress response.');
      }
    }
  } else {
    show_js_error('Could not find \'' + MODEL_ATTRIBUTE_PROGRESS_OVERALL + '\' in ajax progress response!');
  }
  if (!make_progress_call) {
    clear_interval('p', simulation_id);
  }
}

/**
 * Show/Hide information (progress/provenance) data according to level, but specifically related
 * to displaying progress data.
 *
 * @param selected_min Minimum level to show.
 * @param selected_max Maximum level to show.
 */
function show_hide_level_progress(selected_min, selected_max) {
  show_hide_level(selected_min, selected_max);

  /* Highlight alternate displayed lines */
  jQuery('div[id^=progress_]').each(function() {
    var div = jQuery(this);
    div.find('span.monospace').each(function() {
      var span = jQuery(this);
      span.removeClass('row-normal');
      span.removeClass('row-highlight');
    });

    /* TODO : If progress is processed but no progress is visible because the tabs have not been
              been expanded, then no classes are added! */
    div.find('span.monospace:visible:even').each(function() {
      add_alternate_class(jQuery(this), 'row-highlight');
    });

    div.find('span.monospace:visible:odd').each(function() {
      add_alternate_class(jQuery(this), 'row-normal');
    });

    /* TODO : Trying to update the tab title with progress!
    var div_id = div.attr('id');
    vartitle = jQuery('#' + div_id + ' span.monospace:first').text();
    jQuery('a[href=#' + div_id + ']').attr('title', title);
    */
  });
}

/**
 * Reset the progress data.
 */
function reset_progress() {
  get_progress_div().empty();
}

/**
 * Controlling function to show progress data.
 *
 * @param progress_all All progress data (includes overall and job if assigned).
 */
function show_progress(progress_all) {
  var progress_overall;
  var progress_jobs;

  if (progress_all[KEY_INFORMATION_PROGRESS] != undefined) {
    progress_overall = progress_all[KEY_INFORMATION_PROGRESS];
  }
  if (progress_all[KEY_INFORMATION_PROGRESS_JOBS] != undefined) {
    progress_jobs = progress_all[KEY_INFORMATION_PROGRESS_JOBS];
    /* Need to sort the job progress data by grouping level and pacing frequency.
       Note that as we're prepending job tabs after the overall sorting is ordered "descending"! */
    progress_jobs = cs_sort_array_on_group_level_and_pacing_frequency(progress_jobs);

    /* populate the global job_details var */
    jQuery.each(progress_jobs, function() {
      var progress_job = this;
      var job_id = progress_job[KEY_INFORMATION_JOB_ID];
      if (!has_job_details(job_id)) {
        cs_assign_job_details(job_id, progress_job);
      }
    });
  }

  create_layout(progress_overall, progress_jobs);

  show_hide_level_progress(LEVEL_MIN, LEVEL_MAX);
}

/**
 * Determine the layout of the progress data in tabbed format.
 *
 * @param progress_overall Overall progress data, or undefined if none available.
 * @param progress_jobs Job progress data, or undefined if none available.
 */
function create_layout(progress_overall, progress_jobs) {
  if (progress_overall != undefined) {
    var on_first_display = false;
    var progress_tabs;

    /* 1. Overall progress */
    var progress_overall_id = 'progress_overall';
    var progress_overall_jQuery_id = '#' + progress_overall_id;
    var progress_overall_div = jQuery(progress_overall_jQuery_id);

    if (document.getElementById(progress_overall_id)) {
      /* Overall progress has previously been processed */
      var sub_div = jQuery('#sub_div');
      progress_tabs = sub_div.tabs('widget');

      /* empty the existing content */
      progress_overall_div.empty();
    } else {
      on_first_display = true;
      /* Create a new div for overall progress */
      var progress_div = get_progress_div();
      /* Using a sub div to allow destruction and recreation of div element */
      var sub_div = jQuery('#sub_div');
      if (sub_div) {
        sub_div.remove();
      }
      sub_div = jQuery('<div />').attr('id', 'sub_div');
      progress_div.append(sub_div);
      sub_div.append(jQuery('<ul />'));

      progress_tabs = sub_div.tabs({ collapsible: true, active: false });

      var overall_flag_id = get_flag_id(progress_overall_id);

      progress_tabs.tabs('add', progress_overall_jQuery_id,
                         generate_tab_title(progress_overall_title, overall_flag_id), 0);
      progress_overall_div = jQuery(progress_overall_jQuery_id);
      progress_overall_div.addClass('cs_block_span');
    }

    /* Clear redundant existing job layout, i.e. if resetting a simulation */
    var latest_job_ids = [];
    if (progress_jobs != undefined) {
      jQuery.each(progress_jobs, function() {
        var job_id = this[KEY_INFORMATION_JOB_ID];
        latest_job_ids.push(cs_id_as_key(job_id));
      });
    }
    var obsolete_tab_idxs = [];
    jQuery('div[id^=' + PFX_PROGRESS_JOB + ']').each(function(idx, div) {
      var visible_job_div_id = jQuery(div).attr('id');
      var visible_job_id = visible_job_div_id.replace(PFX_PROGRESS_JOB, '');
      /* If currently visible isn't in latest then remove */
      if (jQuery.inArray(visible_job_id, latest_job_ids) == -1) {
        /* Remove the job's details from the global var */
        if (has_job_details(visible_job_id)) {
          remove_job_details(visible_job_id);
        }
        /* Add 1 because the overall progress is the first tab! */
        var tab_idx = idx + 1;
        obsolete_tab_idxs.push(tab_idx);
      }
    });

    /* Remove from the end (descending numerically) */
    obsolete_tab_idxs.sort(function(a,b) { return (b-a); });
    jQuery.each(obsolete_tab_idxs, function() {
      var obsolete_tab_idx = this;
      progress_tabs.tabs('remove', obsolete_tab_idx);
    });

    /* 2. Job progress */
    if (progress_jobs != undefined) {
      jQuery.each(progress_jobs, function() {
        var progress_job = this;
        var job_id = progress_job[KEY_INFORMATION_JOB_ID];

        var progress_id = get_job_progress_id(job_id);
        var progress_tabs_id = '#' + progress_id;
        var job_div = jQuery(progress_tabs_id);
        if (document.getElementById(progress_id)) {
          /* Use an existing tab */
          job_div.empty();
        } else {
          /* Create a new job tab */
          var progress_title = retrieve_job_title(job_id);
          var job_flag_id = get_flag_id(progress_id);

          progress_tabs.tabs('add', progress_tabs_id,
                             generate_tab_title(progress_title, job_flag_id));
          job_div = jQuery(progress_tabs_id);
          job_div.addClass('cs_block_span');
        }
      });
      progress_tabs.tabs('refresh');
    }

    var has_progress_jobs = progress_jobs != undefined;

    populate_progress(progress_overall_div, progress_overall, true);

    if (has_progress_jobs) {
      jQuery.each(progress_jobs, function() {
        var progress_job = this;

        var job_id = progress_job[KEY_INFORMATION_JOB_ID];
        var progress_id = get_job_progress_id(job_id);
        var progress_tabs_id = '#' + progress_id;
        var job_div = jQuery(progress_tabs_id);
        populate_progress(job_div, progress_job[KEY_INFORMATION_PROGRESS], false);
      });
    }

    if (on_first_display) {
      jQuery('#progress div[aria-hidden=false]').attr({ 'aria-hidden': 'true', 'aria-expanded': 'false' }).hide();
      jQuery('#progress .ui-tabs-active').each(function(idx, el) {
        jQuery(el).removeClass('ui-tabs-active');
        jQuery(el).removeClass('ui-state-active');
      });

      jQuery('span[id^=' + PFX_FLAG + PFX_PROGRESS_JOB + ']').each(function(idx, el) {
        jQuery(el).mousedown(function(ev) {
          if (ev.which == 3) {
            /* Capture a right-mouse click to display diagnostics! */
            var flag_id = jQuery(this).attr('id');
            var job_id = flag_id.replace(PFX_FLAG + PFX_PROGRESS_JOB, '');
            ajax_for_job_diagnostics(job_id);
          }
        });
      });
    }
  } else {
    var sub_div = jQuery('#sub_div');
    if (sub_div) {
      sub_div.remove();
    }
  }

  show_flags();
}

/**
 * Display tab message tooltips when progress reports something interesting.
 */
function show_flags() {
  /* FLAGGED holds arrays of tab ids which have had progress information warnings flagged */
  var sim_id_key = cs_id_as_key(CURRENT_SIMULATION_ID);
  if (FLAGGED[sim_id_key] == undefined) {
    FLAGGED[sim_id_key] = [];
  }
  var flagged_tabs = FLAGGED[sim_id_key];

  /* If there are any non-green flags then pop up some messages */
  jQuery('span.ui-icon-flag').filter('.ui-inline-amber,.ui-inline-red').each(function(idx, el) {
    var span = jQuery(el);

    /* aria-controls holds 'progress_overall', 'progress_job_1', etc.. */
    var closest_li = span.closest('li');
    var closest_li_id = closest_li.attr('aria-controls');
    if (jQuery.inArray(closest_li_id, flagged_tabs) !== -1) {
      /* Already flagged - ignore */
      return;
    } else {
      flagged_tabs.push(closest_li_id);
    }

    /* Create the warning display and display it momentarily above the progress tab. */
    var border_class = 'border-amber';
    var arrow_class = 'arrow-amber';
    if (span.hasClass('ui-inline-red')) {
      border_class = 'border-red';
      arrow_class = 'arrow-red';
    }
    var id = span.attr('id');
    var position = closest_li.position();
    var content = 'Tab contains important information.<br /><br />Click on tab to view (if necessary).';

    var tt_id = 'tt_' + id;
    jQuery('<div />').css({ 'left': position.left + 35,
                            'top': position.top + 45 })
                     .addClass('tab-tooltip cs_rounded_5 ' + tt_id + ' ' + border_class)
                     .html(content)
                     .appendTo('body')
                     .fadeIn(750);
    /* Put a little arrow beneath the message. */
    jQuery('<div />').addClass(tt_id + ' arrow-down ' + arrow_class)
                     .appendTo(jQuery('.' + tt_id));
    /* Make it flash once. */
    jQuery('.' + tt_id).animate( { opacity: 0 }, 300, "linear", function() {
      jQuery(this).animate( { opacity:1 } , 300 );
    });

    /* Remove the warning automatically. */
    setTimeout(function() {
      var disappear_in = 10000;
      jQuery('.' + tt_id).fadeOut(disappear_in);
      setTimeout(function() { jQuery('.' + tt_id).remove(); }, disappear_in);
    }, 3500);
  });
}

/**
 * Read the progress data records and display.
 * 
 * @param div Div element to populate.
 * @param progress_data Progress data to add.
 * @param is_overall True if it's overall progress data, otherwise false.
 */
function populate_progress(div, progress_data, is_overall) {
  if (progress_data != undefined) {
    div.empty();
    div.addClass('progress');
    var div_id = div.attr('id');
    var flag_id = get_flag_id(div_id);

    var progress_line_total_count = progress_data.length;
    var progress_line_count = 0;
    var worst_level = 0;
    jQuery.each(progress_data, function() {
      var progress_line = this;
      progress_line_count++;

      var timestamp = +progress_line[KEY_INFORMATION_TIMESTAMP];
      var js_date = new Date(timestamp);
      if (js_date != undefined) {
        js_date = js_date.toLocaleString();
      } else {
        js_date = '';
      }
      var level = progress_line[KEY_INFORMATION_LEVEL];
      var text = progress_line[KEY_INFORMATION_TEXT];
      var remove_spinner = false;
      if (text != undefined) {
        var last_line = (progress_line_total_count == progress_line_count);
        if (last_line) {
          if (text == SYSTEM_TERMINATION_KEY) {
            text = '';
            remove_spinner = true;
          }
          if (is_overall) {
            jQuery('#spinner_progress').html(swap_highlighted(text));
          }
        }
      } else {
        text = '';
        show_js_error('progress text was not defined');
      }

      if (text != '') {
        var level_class = PFX_LEVEL_CLASS + level;
        var span_class = 'monospace ' + level_class;
        var progress_span = jQuery('<span>').html('[' + js_date + '] ' + swap_highlighted(text))
                                            .addClass(span_class);
        div.prepend(progress_span);
      }
      if (is_overall && remove_spinner) {
        progress_overall_completed = true;
        jQuery('#spinner').hide();
      }

      if (level != undefined) {
        var this_level = jQuery.inArray(level, LEVEL_SEQ);
        if (this_level > worst_level) {
          worst_level = this_level;
        }
      }
    });

    var flag_icon = jQuery('#' + flag_id);
    /* remove any existing classes */
    flag_icon.removeClass('ui-inline-red');
    flag_icon.removeClass('ui-inline-amber');
    flag_icon.removeClass('ui-inline-green');
    flag_icon.removeClass('ui-inline-neutral');
    var flag_class;
    switch (worst_level) {
      case 0 :
      case 1 :
      case 2 :
        flag_class = 'ui-inline-green';
        break;
      case 3 :
        flag_class = 'ui-inline-amber';
        break;
      default :
        flag_class = 'ui-inline-red';
    }
    flag_icon.addClass(flag_class);
  }
}
