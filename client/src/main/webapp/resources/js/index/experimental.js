/**
 * Process the AJAX response to retrieving experimental data.
 * Loads experimental data into a var which % Change graph processing can display.
 * 
 * @param response AJAX experimental data response.
 */
function process_experimental_response(response) {
  var experimental_idx = 0;
  EXPERIMENTAL_DATASETS = [];

  var ma_experimental_obj = response[MODEL_ATTRIBUTE_EXPERIMENTAL_DATA];
  if (ma_experimental_obj != undefined) {
    var experimental_data = ma_experimental_obj[KEY_EXPERIMENTAL_DATA];
    if (experimental_data != undefined) {
      jQuery.each(experimental_data, function(frequency_Hz, exp_valuesatconc_arr) {
        var has_sem_values = false;
        var exp_pct_change_plotdata = [];

        /* For each concentration (at a particular frequency) create a plot point */
        jQuery.each(exp_valuesatconc_arr, function() {
          var exp_concentration = this[KEY_EXPERIMENTAL_CONC] * 1;
          if (exp_concentration >= MINIMUM_PCT_CHANGE_CONCENTRATION) {
            var pct_change_mean = this[KEY_EXPERIMENTAL_PCT_CHANGE_MEAN];
            var pct_change_sem = this[KEY_EXPERIMENTAL_PCT_CHANGE_SEM];
            if (pct_change_mean != undefined) {
              exp_pct_change_plotdata.push( [ cs_log_10(exp_concentration),
                                              pct_change_mean, pct_change_sem ]);
              if (pct_change_sem != undefined) {
                has_sem_values = true;
              }
            }
          }
        });

        /* Sort the plot points by concentration */
        exp_pct_change_plotdata.sort(function(a, b) { return a[0] - b[0]; });
        /* New experimental plot point collection for a particular frequency */
        var use_points = { show: true };
        if (has_sem_values) {
          use_points = { show: true,
                         errorbars: "y",
                         yerr: { show: true, upperCap: "-", lowerCap: "-", radius: 5 }
                       };
        }

        /* Assign -ve group level to ensure sorting by group level and pacing frequency of all
           pct change datasets (including experimental datasets) is accurate. */
        EXPERIMENTAL_DATASETS[experimental_idx++] = {
          label: EXPERIMENTAL_LABEL + DELIMITER + frequency_Hz + " Hz",
          groupLevel: -1,
          pacingFrequency: frequency_Hz,
          data: exp_pct_change_plotdata,
          lines: { show: false },
          points: use_points,
          dashes: { show: true },
          shadowSize: 0
        };
      });
    } else {
      show_js_error('Could not find \'' + KEY_EXPERIMENTAL_DATA + '\' in ajax experimental data response!');
    }
  } else {
    show_js_error('Could not find \'' + MODEL_ATTRIBUTE_EXPERIMENTAL_DATA + '\' in ajax experimental data response!');
  }

  /* sort the experimental datasets by label (which is effectively ordered by frequency) */
  if (experimental_idx > 0) {
    EXPERIMENTAL_DATASETS.sort(function(a, b) {
      return alpha_sort(a.label, b.label);
    });
  }
}
