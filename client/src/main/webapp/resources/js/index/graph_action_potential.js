var ap_choices_trace_id = 'ap_choices_trace';
var ap_clear_selection_trace_id = 'ap_clear_selection_trace';
var ap_default_text_id = 'ap_default_text';
var ap_placeholder_trace_id = 'ap_placeholder_trace';

/* Set the AP graph and choices to its original state */
function reset_ap_display() {
  var default_ap_text = jQuery('#' + ap_default_text_id).clone();
  default_ap_text.removeAttr('id');
  default_ap_text.show();
  jQuery('#' + ap_placeholder_trace_id).html(default_ap_text);

  /* Reset the events */
  jQuery('#' + ap_clear_selection_trace_id).off();

  /* Reset the choices */
  jQuery('#' + ap_choices_trace_id).empty();
}

function show_ap_graph(simulation_name) {
  var ap_colour_idx = 0;

  var ap_dataset_subset = [];
  var control_dataset = {};

  if (simulation_name) {
    /* e.g. QSAR@0.5Hz, PatchXpress@1.0Hz */
    var plot_idx_subset = 0;
    var control = false;
    $.each(AP_DATASETS, function(key, val) {
      var full_label = val.label;
      if (full_label.indexOf(simulation_name) == 0) {
        ap_dataset_subset[plot_idx_subset++] = {
          label: full_label,
          data: val.data
        };
      } else if ((full_label == CONTROL_LABEL) && !control) {
        control = true;
        control_dataset = {
          label: full_label,
          data: val.data
        };
      }
    });
    ap_dataset_subset.splice(0, 0, control_dataset);
  }

  var main_options = getDefaultMainPanelOptions();

  function getDefaultMainPanelOptions () {
    return cs_plot_options(PLOT_AP);
  };

  var overview_options = {
                           legend: { 
                                     show: true,
                                     container: $('#ap_overview_legend_trace')
                                    },
                           series: {
                                     lines: {
                                              show: true, lineWidth: 1
                                             },
                                     shadowSize: 0
                                   },
                           xaxis: { ticks: 4 },
                           yaxis: { ticks: 4},
                           grid: { color: '#999' },
                           selection: { mode: 'xy' }
  };

  /* hard-code color indices to prevent them from shifting as things are turned on/off */
  $.each(ap_dataset_subset, function(key, val) {
    val.color = ap_colour_idx;
    ++ap_colour_idx;
  });

  /* plot the original graph now and retain a holder for it.*/
  var plot = $.plot($('#' + ap_placeholder_trace_id), ap_dataset_subset, main_options);
  /* plot the overview graph and legend and retain a holder for it.*/
  var overview = $.plot($('#ap_overview_trace'), ap_dataset_subset, overview_options);
  /* Make the plot overview retain the original axes, even when different data is plotted.*/
  overview_options = $.extend(true, {}, overview_options, {
                                                            xaxis: {
                                                                     min: overview.getAxes().xaxis.min,
                                                                     max: overview.getAxes().xaxis.max
                                                                   },
                                                            yaxis: { 
                                                                     min: overview.getAxes().yaxis.min,
                                                                     max: overview.getAxes().yaxis.max
                                                                   }
                                                          });
  /* insert checkboxes */
  var choiceContainer = $('#' + ap_choices_trace_id);
  choiceContainer.text('');
  $.each(ap_dataset_subset, function(key, val) {
    /* class legendColorBox defined in jquery.flot.js in function insertLegend() */
    var legendColorBox = $('#ap_overview_legend_trace .legendColorBox:eq(' + val.color + ') div div');
    var new_span = $('<span>&nbsp;&nbsp;</span>');
    /* moz wasn't happy using border-color, which ie didn't mind! */
    var colour = legendColorBox.css('border-left-color');
    new_span.css('background-color', colour);
    choiceContainer.append('<input type="checkbox"\
                                   name="' + key + '" \
                                   checked="checked" \
                                   id="id_ap_' + key + '" />&nbsp;');
    choiceContainer.append(new_span);
    choiceContainer.append('&nbsp;<label for="id_ap_' + key + '"> ' + val.label + '</label>');
    choiceContainer.append('<br />');
  });

  /* now connect the two - this method sets the "selection" ranges on both plots, and replots the main plot. */
  $('#' + ap_placeholder_trace_id).bind("plotselected", function (event, ranges) {
    /* clamp the zooming to prevent eternal zoom */
    if (ranges.xaxis.to - ranges.xaxis.from < 0.00001)
      ranges.xaxis.to = ranges.xaxis.from + 0.00001;
    if (ranges.yaxis.to - ranges.yaxis.from < 0.00001)
      ranges.yaxis.to = ranges.yaxis.from + 0.00001;
    main_options = $.extend(true, {}, main_options, {
                                                      xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to },
                                                      yaxis: { min: ranges.yaxis.from, max: ranges.yaxis.to }
                                                    });
    overview.setSelection(ranges, true);
    plotAccordingToChoicesAndZooming();
  });

  /* This method just calls the method on the main plot above. */
  $('#ap_overview_trace').bind("plotselected", function (event, ranges) {
    plot.setSelection(ranges);
  });

  /* Gets called for all interactive data plotting. */
  function plotAccordingToChoicesAndZooming() {
    /* 'data' is the section of 'ap_dataset_subset' which will be plotted at the moment. */
    var data = [];
    choiceContainer.find("input:checked").each(function () {
                                                 var key = $(this).attr("name");
                                                 if (key && ap_dataset_subset[key]) data.push(ap_dataset_subset[key]);
                                               });
    if (data.length > 0) {
      plot = $.plot($('#' + ap_placeholder_trace_id), data, main_options);
      ranges = { xaxis: { from: plot.getAxes().xaxis.min, to: plot.getAxes().xaxis.max},
                 yaxis: { from: plot.getAxes().yaxis.min, to: plot.getAxes().yaxis.max} };
      overview = $.plot($('#ap_overview_trace'), data, overview_options);
      overview.setSelection(ranges,true);
    }
  }

  /* If inputs are detected on the checkboxes fire off this method: */
  choiceContainer.find("input").click(plotAccordingToChoicesAndZooming);

  /* Reset button */
  $('#ap_clear_selection_trace').click(resetEverything);

  /* Reset method. */
  function resetEverything() {
    /* Check all the boxes. */
    choiceContainer.find("input").each(function () {
                                         if ($(this).attr("checked") == false) {
                                           $(this).click();
                                         }
                                       });
    /* Let the main plot choose its own axes again */
    main_options = getDefaultMainPanelOptions();

    plotAccordingToChoicesAndZooming();
    overview.clearSelection();
  }

  var previous_point = null;
  $('#' + ap_placeholder_trace_id).bind('plothover', function (event, pos, item) {
    if (typeof pos.y != 'undefined') {
      if (item) {
        if (previous_point != item.datapoint) {
          previous_point = item.datapoint;

          $('#tooltip').remove();
          var x = item.datapoint[0],
              y = item.datapoint[1].toFixed(2);

          var contents = '[' + item.series.label + '] : ' + 
                         GRAPH_LABEL_TIME + ' ' + x + GRAPH_LABEL_MS + ' : ' +
                         GRAPH_LABEL_MEMBRANE_VOLTAGE + ' ' + y + GRAPH_LABEL_MV;
          show_tooltip(item.pageX, item.pageY, contents);
        }
      } else {
        $('#tooltip').remove();
        previous_point = null;
      }
    }
  });

  /* leave the hiding until last (otherwise flot may complain */
  $('#ap_overview_trace').hide();
  $('.graph_ap_overview_legend_trace').hide();
}