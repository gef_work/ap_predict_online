var input_data_received = false;
var input_data_table_row_id = 'input_data_table_row';

/**
 * Open the provenance window.
 * 
 * @param url URL to call.
 */
function open_provenance_window(url) {
  window.open(url, 'provenance_window', 'height=840,width=1350,scrollbars=yes,resizable=yes');
}

/**
 * Reset the input values display.
 * 
 * @return Empty input values display element.
 */
function reset_input_values() {
  var base_row = jQuery('#' + input_data_table_row_id);
  base_row.siblings().remove();
  return base_row;
}

/**
 * Process the AJAX input values response.
 * 
 * @param response AJAX response.
 */
function process_input_values_response(response) {

  var ma_response_obj = response[MODEL_ATTRIBUTE_INPUT_VALUES];

  if (ma_response_obj != undefined) {
    var input_values = ma_response_obj[KEY_INPUTVAL_INPUT_VALUES];
    if (input_values != undefined && Object.size(input_values) > 0) {
      /* Set the global variable to indicate that we have input value data */
      input_data_received = true;

      /* Sort the incoming input values according to the group level.
         Note that the group level may be the assay level if the grouping was switched off! */
      input_values.sort(function(a, b) {
        var group_level_a = +a[KEY_INPUTVAL_GROUP_LEVEL];
        var group_level_b = +b[KEY_INPUTVAL_GROUP_LEVEL];
        return (group_level_a - group_level_b);
      });

      var base_row = reset_input_values();

      /* TODO : Improve the accuracy of testing for non-QSAR */
      var has_non_qsar = false;
      var probable_QSAR_level = 0;
      jQuery.each(input_values, function(index, all_input_values) {
        var group_level = all_input_values[KEY_INPUTVAL_GROUP_LEVEL];
        var group_name = all_input_values[KEY_INPUTVAL_GROUP_NAME];
        var group_input_values = all_input_values[KEY_INPUTVAL_INPUT_VALUES];

        var new_row = base_row.clone();
        /* group name is in leftmost column */
        new_row.find('td:eq(0)').text(group_name);
        new_row.removeAttr('id');
        new_row.removeAttr('style');

        new_row.addClass('dr-table-firstrow rich-table-firstrow');
        new_row.find('td').addClass('dr-table-cell rich-table-cell');

        var is_probably_QSAR = ((group_level * 1) == probable_QSAR_level);
        place_values_in_row(new_row, group_input_values, is_probably_QSAR);
        base_row.parent(':last').append(new_row);

        if (!has_non_qsar && !is_probably_QSAR) {
          has_non_qsar = true;
        }
      });

      base_row.siblings().filter(':even').addClass('row-highlight');
      base_row.siblings().filter(':odd').addClass('row-normal');

      if (has_non_qsar && SHOW_PROVENANCE) {
        jQuery('.trigger_provenance').show();
      }
      /* */
      jQuery('.trigger_input_values').show();
    } else {
      /* On first call there's unlikely to be input values until all input data is computed */
    }
  } else {
    show_js_error('Could not find \'' + MODEL_ATTRIBUTE_INPUT_VALUES + '\' in ajax input values response!');
  }
}

/**
 * Place the pIC50s and whatever else (i.e. raw data display image) into an input values row.
 * 
 * @param row Row to place values in.
 * @param values Values for placement.
 * @param is_probably_QSAR True if QSAR assay data, otherwise false.
 */
function place_values_in_row(row, values, is_probably_QSAR) {
  jQuery.each(values, function(index, ion_channel_values) {
    var ion_channel_name = ion_channel_values[KEY_INPUTVAL_ION_CHANNEL_NAME];
    var source_assay_name = ion_channel_values[KEY_INPUTVAL_SOURCE_ASSAY_NAME];
    var pic50s = ion_channel_values[KEY_INPUTVAL_PIC50S];
    var hills = ion_channel_values[KEY_INPUTVAL_HILLS];
    var originals = ion_channel_values[KEY_INPUTVAL_ORIGINALS];
    var strategy_orders = ion_channel_values[KEY_INPUTVAL_STRATEGY_ORDERS];

    /* Find column for ion channel */
    var idx = jQuery.inArray(ion_channel_name, DEFAULT_ION_CHANNEL_COLUMN_ORDER);
    if (idx < 0) {
      /* debug('ion channel name ' + ion_channel_name + ' not found in columns'); */
    } else {
      var allowing_for_groupname_column = idx + 1;
      var column = row.find('td:eq(' + allowing_for_groupname_column + ')');
      display_input_value(column, pic50s, hills, originals, source_assay_name, strategy_orders);
      if (!is_probably_QSAR && SHOW_PROVENANCE) {
        var image_id = guidGenerator();
        var new_image = jQuery('<img />').attr({ 'id' : image_id, 
                                                 'src': CONTENT_DISPLAY_LOCATION,
                                                 'title': 'View assay/ion channel input data detail. (Opens/Reuses a separate window)'})
                                         .css('float', 'right');
        jQuery('body').on('click', '#' + image_id, function() {
          var url = JSP_PROVENANCE + CURRENT_SIMULATION_ID + '/' + source_assay_name + '/' + 
                    ion_channel_name + "/" + LEVEL_MIN + "/" + LEVEL_MAX;
          open_provenance_window(url);
        });
        column.append(new_image);
      }
    }
  });
}

/**
 * The input value and other information to display.
 * 
 * @param element Element to display the information in.
 * @param pic50s pIC50s for the ion channel / assay.
 * @param hills Hill Coefficients for the ion channel / assay.
 * @param originals Indicator whether the value used was as per the raw data. 
 * @param source_assay_name Name of the assay which generated the pIC50s/Hills
 * @param strategy_orders pIC50 evaluation strategy default invocation orders (if applicable).
 */
function display_input_value(element, pic50s, hills, originals, source_assay_name,
                             strategy_orders) {
  var pIC50_array = pic50s.split(',');
  var hills_array = hills.split(',');
  var originals_array = originals.split(',');
  var strategy_orders_array = strategy_orders.split(',');

  element.empty();
  element.append(create_span(pIC50_array, hills_array, originals_array, source_assay_name,
                             strategy_orders_array, 'unit_c50_pIC50'));
  element.append(create_span(pIC50_array, hills_array, originals_array, source_assay_name,
                             strategy_orders_array, 'unit_c50_IC50_uM'));
  element.append(create_span(pIC50_array, hills_array, originals_array, source_assay_name,
                             strategy_orders_array, 'unit_c50_IC50_nM'));
}

/**
 * Create the element which will show the (p)IC50s according to whatever unit the user chooses.
 * 
 * @param pIC50_array Array of pIC50 business manager values.
 * @param hills_array Array of Hill Coefficient values.
 * @param originals_array Array of indicators of whether the pIC50 was dose-response fitted or not.
 * @param source_assay_name Name of source assay.
 * @param strategy_orders_array Array of pIC50 evaluation strategy default invocation orders (if 
 *                              applicable).
 * @param unit_class The CSS class corresponding to the (p)IC50 unit.
 * @returns Created element.
 */
function create_span(pIC50_array, hills_array, originals_array, source_assay_name,
                     strategy_orders_array, unit_class) {
  var name;
  var convert;
  switch (unit_class) {
    case 'unit_c50_pIC50':
      name = 'pIC50';
      convert = function(value) {
        return value;
      };
      break;
    case 'unit_c50_IC50_uM':
      name = 'IC50 (µM)';
      convert = function(value) {
        return pIC50_to_IC50(value, MOLAR_UNIT_uM);
      };
      break;
    case 'unit_c50_IC50_nM':
      name = 'IC50 (nM)';
      convert = function(value) {
        return pIC50_to_IC50(value, MOLAR_UNIT_nM);
      };
      break;
    default :
      alert();
      return;
  }

  var sum = 0;
  var sum_idx = 0;

  var iv_table = jQuery('<table />');
  var table_cols = [name, 'Hill', TEXT_STRATEGY];
  var header_row = jQuery('<tr />');
  iv_table.append(header_row);
  jQuery.each(table_cols, function(idx) {
    var column_text = jQuery('<span />').css( { 'text-decoration' : 'underline' })
                                        .text(table_cols[idx]);
    header_row.append(jQuery('<th />').append('&nbsp;').append(column_text).append('&nbsp;'));
  });

  for (var idx = 0; idx < pIC50_array.length; idx++) {
    var pIC50 = pIC50_array[idx];
    /* In the breakdown data show a direct conversion from the original pIC50 value. */
    var each_c50 = cs_round_3dp(convert(pIC50));
    var each_hill = cs_round_3dp(hills_array[idx]);
    var each_original = originals_array[idx];
    var each_strategy_order = strategy_orders_array[idx];

    if (MODEL_ATTRIBUTE_CONFIG_INPUT_VALUE_DISPLAY == INPUT_VALUE_DISPLAY_EQUALITY_MODIFIER) {
      if (each_original == 'true') {
        sum += +pIC50;
        sum_idx++;
      }
    } else if (MODEL_ATTRIBUTE_CONFIG_INPUT_VALUE_DISPLAY == INPUT_VALUE_DISPLAY_ANY_MODIFIER) {
      sum += +pIC50;
      sum_idx++;
    }

    var description = '';
    /* If it's QSAR data it's not going to have a pIC50 evaluation strategy order value */
    if (each_strategy_order != '') {
      var strategy_key = get_object_key(each_strategy_order);
      description = strategy_objects[strategy_key].description;
    }

    var display_class = (each_original == 'true') ? 'cs_bold' : 'cs_italic';
    iv_table.append(jQuery('<tr />').append(jQuery('<td />').addClass(display_class + ' iv_table_padded')
                                                            .html(each_c50))
                                    .append(jQuery('<td />').addClass(display_class + ' iv_table_padded')
                                                            .html(each_hill))
                                    .append(jQuery('<td />').css( { 'font-size' : 'x-small' } )
                                                            .html(description)));
  }
  header_row.siblings().filter(':even').addClass('row-highlight');

  var span = jQuery('<span />');

  /* Mean is the mean of the original pIC50 values (derived from equality modifier) converted to
     desired units. */
  var mean = (sum_idx > 0) ? cs_round_3dp(convert(sum / sum_idx)) : 'n/a';

  span.text(mean + ' (' + source_assay_name + ')')
      .attr( { 'title': iv_table.html() } )
      .css( { 'float': 'left' } )
      .addClass(unit_class);
  return span;
}
