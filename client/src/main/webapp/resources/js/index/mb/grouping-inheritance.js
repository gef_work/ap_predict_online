/* grouping and inheritance vars */
var grp_on_off;
var inh_on_off;
var inh_within;
var inh_between;
var inh_options;

var grp_state;
var inh_state;
var inh_btw_state;
var inh_wth_state;

/* show/hide the value inheritance options according to the current state */
function display_group_inherit() {
  grp_state = grp_on_off.is(':checked');
  inh_state = inh_on_off.is(':checked');
  inh_btw_state = inh_between.is(':checked');
  inh_wth_state = inh_within.is(':checked');

  if (inh_state && grp_state) {
    /* if grouping and inheriting allow user to specify nature of grouping */
    inh_options.show();
  } else {
    inh_options.hide();
  }
}
