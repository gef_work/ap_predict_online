/* pc50 evaluation strategy vars */
var pc50_sortable;
var strategy_objects;
var strategies_state;

var STATEVAR_KEY_ACTIVE = 'active';
var STATEVAR_KEY_OBJECT_KEY = 'object_key';

/* Pre- job submission create the collection of active workflow strategy objects */
function get_active_strategies() {
  var active_strategies = [];
  if (strategies_state != undefined) {
    jQuery.each(strategies_state, function(index, value) {
      var state = value;
      var object_key = state[STATEVAR_KEY_OBJECT_KEY];
      var active = state[STATEVAR_KEY_ACTIVE];
      var strategy_object = strategy_objects[object_key];

      var id = strategy_object['id'];
      if (active) {
        active_strategies.push(id);
      }
    });
    if (active_strategies.length < 1) {
      show_js_error('At least one workflow strategy must be active!');
    }
  } else {
    /* use the default workflow */
  }

  if (active_strategies.length < 1) {
    return;
  } else {
    return active_strategies.join();
  }
}
