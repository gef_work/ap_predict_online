/* Force re-run and reset js vars used in views/extruder/resetReRun.jsp */
var selector_rerun;
var selector_reset;
var force_rerun_state;
var reset_state;

/**
 * Reset the force re-run selection.
 */
function reset_force_rerun() {
  if (reset_state) {
    selector_reset.prop('checked', false);
    reset_state = false;
  }
}
/**
 * Reset the reset selection.
 */
function reset_reset() {
  if (force_rerun_state) {
    selector_rerun.prop('checked', false);
    force_rerun_state = false;
  }
}