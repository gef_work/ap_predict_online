/*
 * qNet display for client.
 */
var qnet_display_c = Object.create(qnet_display);

qnet_display_c.qnet_plot_according_to_choices_and_zooming = function() {
  var self = this;

  var pct_change_confidence_checkbox = jQuery('#pct_change_confidence_checkbox');
  var pccc_checked = pct_change_confidence_checkbox.is(':checked');

  var selected_labels = [];
  pct_change_choice_container.find('input[id^=id_pct_change_]:checked')
                             .each(function() {
    /* These 'Source' checkboxes are checked */
    var id = $(this).attr('id');
    var label_el = $('label[for=' + id + ']');
    /* Label text, e.g. PatchXpress,Qpatch@0.5 (Hz) */
    var label = label_el.text();

    selected_labels.push(label);
  });

  var data = [];
  if (selected_labels.length > 0) {
    $.each(self.qnet_data, function(key, val) {
      var data_label = val.label;
      var data_id = val.id;

      if (typeof data_label !== 'undefined') {
        /* Regular assay/freq qNet data */
        if (jQuery.inArray(data_label, selected_labels) != -1) {
          data.push(val);
        }
      } else {
        /* %ile data */
        var relevant = false;
        $.each(selected_labels, function(idx, selected_label) {
          if (data_id.startsWith(selected_label)) {
            relevant = true;
            return false;
          }
        });
        if (relevant && pccc_checked) {
          if (jQuery.inArray(val.pctile, cs_graph_pctiles_to_show) != -1) {
            data.push(val);
          }
        }
      }
    });
  }
  if (data.length > 0) {
    self.qnet_plot = cs_fplot($('#' + self.qnet_placeholder_trace_id), data,
                              self.qnet_main_options);
  }
}