/**
 * Determine the relative alphabetical values.
 * 
 * @param a_val First value.
 * @param b_val Second value.
 * @return 1 if a > b, 0 if a==b, -1 if a < b.
 */
function alpha_sort(a_val, b_val) {
  return ((a_val < b_val) ? -1 : ((a_val > b_val) ? 1 : 0));
}

/**
 * ActionPotential per-compound concentration label.<br />
 * If the compound concentration is 0 (zero) then the control label is used.
 * 
 * @param compound_concentration Compound concentration.
 * @param job_title Job title, e.g. Barracuda@0.5 (Hz).
 * @return Label, e.g. Barracuda@0.5 (Hz) @ 1.0 (uM).
 */
function create_ap_label(compound_concentration, job_title) {
  if (compound_concentration == 0) {
    return CONTROL_LABEL;
  } else {
    return job_title + DELIMITER + compound_concentration + MICRO_TEXT;
  }
}

/**
 * Process the AJAX results response.
 * 
 * @param response AJAX response.
 */
function process_results_response(response) {

  var new_data = false;

  /* 
   * Step 1 : If no experimental data has been assigned so far then look for some in the 
   *          EXPERIMENTAL_DATASETS global var.
   *          If there is some in the EXPERIMENTAL_DATASETS var, add it to the PCT_CHANGE_DATASETS
   *          global var.
   */
  if (PCT_CHANGE['experimental'] != undefined) {
    /* Experimental data has already been assigned */
  } else {
    /* Load the experimental datasets (if there are any) */
    var experimental_dataset_count = Object.size(EXPERIMENTAL_DATASETS);
    if (experimental_dataset_count > 0) {
      new_data = true;
      PCT_CHANGE['experimental'] = true;
      for (var idx = 0; idx < experimental_dataset_count; idx++) {
        PCT_CHANGE_DATASETS.push(EXPERIMENTAL_DATASETS[idx]);
      }
    }
  }

  /*
   * Step 2 :  Determine if there is any new results data for simulation jobs just arrived.
   *           If there is, add it to the PCT_CHANGE_DATASETS global var and refresh display.
   */
  var ma_results_obj = response[MODEL_ATTRIBUTE_RESULTS];
  var has_qnet = false;

  if (ma_results_obj != undefined) {
    var ma_qnet_available = response[MODEL_ATTRIBUTE_QNET_AVAILABLE]
    has_qnet = (ma_qnet_available != undefined && ma_qnet_available);

    var results_data = ma_results_obj[KEY_RESULTS_DATA];
    if (results_data != undefined) {
      var delta_apd90_datasets = [];
      jQuery.each(results_data, function(index, job_map) {
        var job_id = job_map[KEY_RESULTS_JOB_ID];
        var job_id_key = cs_id_as_key(job_id);
        if (PCT_CHANGE[job_id_key] != undefined) {
          /* This data has already been recorded. Ignore. */
        } else {
          var delta_apd90_plotdata = [];

          var messages = job_map[KEY_RESULTS_MESSAGES];
          var delta_apd90_pctile_names = job_map[KEY_RESULTS_DELTA_APD90_PERCENTILE_NAMES];
          var concentration_data = job_map[KEY_RESULTS_CONCENTRATION_DATA];

          var job_title = retrieve_job_title(job_id);

          var has_confidence_intervals = false;

          if (job_title != undefined) {
            PCT_CHANGE[job_id_key] = true;

            var qnet_plotdata = [];

            /* Data is arriving in order of increasing compound concentration */
            jQuery.each(concentration_data, function(compound_concentration, per_concentration_data) {
              var delta_apd90 = per_concentration_data[KEY_RESULTS_DELTA_APD90];
              /* var upstroke_velocity = per_concentration_data[KEY_RESULTS_UPSTROKE_VELOCITY]; */
              var split_times = per_concentration_data[KEY_RESULTS_TIMES].split(',');
              var split_voltages = per_concentration_data[KEY_RESULTS_VOLTAGES].split(',');
              var qnet = per_concentration_data[KEY_RESULTS_QNET];

              /* Step 2.1: Action Potential */
              var ap_plotdata = [];
              jQuery.each(split_times, function(index, time) {
                time = +time;
                ap_plotdata.push( [ time, split_voltages[index] ] );
              });
              AP_DATASETS.push({
                'label': create_ap_label(compound_concentration, job_title),
                'data': ap_plotdata
              });

              /* Step 2.2: % Change/Delta APD90
                           Some delta_apd90 values aren't numeric (in which case it's a message of
                           some sort) */
              var messages_found = false;
              if (!jQuery.isNumeric(delta_apd90)) {
                if (delta_apd90.indexOf(',')) {
                  /* Looks like we could have confidence interval spreads */
                  var potentialSpreadValues = delta_apd90.split(',');
                  jQuery.each(potentialSpreadValues, function(index, potentialSpreadValue) {
                    if (!jQuery.isNumeric(potentialSpreadValue)) {
                      messages_found = true;
                      return;
                    }
                  });
                  if (!messages_found) {
                    has_confidence_intervals = true;
                  }
                } else {
                  /* Assume message of some sort in Delta APD90 field */
                  messages_found = true;
                }
              }

              if ((!messages_found) && (compound_concentration >= MINIMUM_PCT_CHANGE_CONCENTRATION)) {
                delta_apd90_plotdata.push( [ cs_log_10(compound_concentration), delta_apd90 ]);
              }

              /* Step 2.3: qNet processing. */
              if (qnet != undefined && (compound_concentration >= MINIMUM_PCT_CHANGE_CONCENTRATION)) {
                qnet_plotdata.push( [ cs_log_10(compound_concentration), qnet ] );
              }
            });

            if (has_qnet) {
              QNET_DATASETS.push( {
                'label': job_title,
                'groupLevel': cs_retrieve_group_level(job_id),
                'pacingFrequency': cs_retrieve_pacing_frequency(job_id),
                'messages': messages,
                'deltaAPD90PctileNames': delta_apd90_pctile_names,
                'data': qnet_plotdata,
                'lines': { 'show': true },
                'points': { 'show': true },
                'dashes': { 'show': false },
                'shadowSize': 1,
                'confidenceData': has_confidence_intervals,
                'jobId': job_id
              });
            }

            /* group level and pacing frequency are used for ordering the datasets */
            delta_apd90_datasets.push({
              'label': job_title,
              'groupLevel': cs_retrieve_group_level(job_id),
              'pacingFrequency': cs_retrieve_pacing_frequency(job_id),
              'messages': messages,
              'deltaAPD90PctileNames': delta_apd90_pctile_names,
              'data': delta_apd90_plotdata,
              'lines': { 'show': true },
              'points': { 'show': true },
              'dashes': { 'show': false },
              'shadowSize': 1,
              'confidenceData': has_confidence_intervals,
              'jobId': job_id
            });
          } else {
            /* debug('No job title, so no results processing.'); */
          }
        }
      });

      if (delta_apd90_datasets.length > 0) {
        new_data = true;
        jQuery.each(delta_apd90_datasets, function(index, delta_apd90_dataset) {
          PCT_CHANGE_DATASETS.push(delta_apd90_dataset);
        });
      }
    }
  }

  if (new_data) {
    reset_pct_change_display();

    show_top_graph_area(has_qnet);
    show_pct_change_graph(has_qnet);
    if (has_qnet) {
      qnet_display_c.show_qnet_graph('client');
    }
    /* dialogs used when messages available */
    set_dialogs();

    /* If there are % change values of any kind, show the export form */
    jQuery('.trigger_pct_change_data').show();

    /* register the change event on the the pct change actionpotential radio buttons */
    jQuery('input:radio[name=actionpotential_radio]').change(function() {
      var changed = jQuery(this);
      if (changed.is(':checked')) {
        show_ap_graph(changed.val());
      }
    });

    if (progress_overall_completed) {
      var pct_change_count = Object.size(PCT_CHANGE_DATASETS);
      var experimental_count = Object.size(EXPERIMENTAL_DATASETS);
      if (pct_change_count - experimental_count == 1) {
        /* Emulate the selection to view AP-trace if only one % change simulation */
        jQuery('input:radio[name=actionpotential_radio]').click();
      }
    }
  } else {
    /* No new data.. move along please! */
  }
}

/**
 * Create the jQuery dialogs when Delta-APD90 results have messages to show.<br />
 * The function sweeps the document for specifically-labeled buttons and creates the dialogs as
 * well as assigning click event listeners.
 */
function set_dialogs() {
  jQuery('button[id^=msgbutton_]').each(function() {
    var button_obj = jQuery(this);
    var msgbutton_id = button_obj.attr('id');
    var messages_id = 'messages_' + msgbutton_id.replace('msgbutton_', '');
    var messages_obj = jQuery('div[id=' + messages_id + ']');
    messages_obj.dialog({
      autoOpen: false,
      show: {
        effect: 'blind',
        duration: 100
      },
      hide: {},
      width: 500,
      position: {
        my: 'left top',
        at: 'left bottom',
        of: button_obj
      }
    });
  });

  jQuery('button[id^=msgbutton_]').click(function() {
    var msgbutton_id = jQuery(this).attr('id');
    var messages_id = 'messages_' + msgbutton_id.replace('msgbutton_', '');
    jQuery('div[id=' + messages_id + ']').dialog('open');
  });
}

/**
 * Display the DeltaAPD90 (and optionally qNet) plots.
 * 
 * @param has_qnet <code>true</code> if qNet data available, otherwise <code>false</code>.
 */
function show_top_graph_area(has_qnet) {
  /* If tabs are already on display then destroy them. */
  if (DIV_RESULTS_TABBED.hasClass('ui-tabs')) {
    DIV_RESULTS_TABBED.tabs('destroy');
  }
  DIV_RESULTS_TABBED.empty();

  var pct_change = jQuery('<div />').attr( { 'id' : pct_change_placeholder_trace_id } )
                                    .addClass('graph_placeholder_trace');

  if (has_qnet) {
    var ul = jQuery('<ul />');
    var lis = [];
    lis.push(jQuery('<li />').append(jQuery('<a />').attr( { 'href' : '#tabA' } )
                                                    .text( 'Δ APD90 vs. Conc' )));
    lis.push(jQuery('<li />').append(jQuery('<a />').attr( { 'href' : '#tabB' } )
                                                    .text( 'qNet vs. Conc' )));
    var divs = [];
    divs.push(jQuery('<div />').attr( { 'id' : 'tabA' } ).append(pct_change));

    var qnet = jQuery('<div />').attr( { 'id' : 'qnet_placeholder_trace' } )
                                .addClass('graph_placeholder_trace');
    divs.push(jQuery('<div />').attr( { 'id' : 'tabB' } ).append(qnet));

    jQuery.each(lis, function() {
      ul.append(this);
    });
    DIV_RESULTS_TABBED.append(ul);

    jQuery.each(divs, function() {
      DIV_RESULTS_TABBED.append(this);
    })

    DIV_RESULTS_TABBED.tabs();
  } else {
    DIV_RESULTS_TABBED.append(pct_change);
  }
  DIV_RESULTS_TABBED.removeClass('graph_placeholder_trace');
}

/**
 * Show the x, y % change values on the % change chart.
 * 
 * @param x X coordinate.
 * @param y Y coordinate.
 * @param content Tooltip content to show.
 */
function show_tooltip(x, y, content) {
  jQuery('<div />').attr({ 'id' : 'tooltip' })
                   .css({ 'top': y + 5, 'left': x + 5 })
                   .addClass('graph_pct_change_tooltip')
                   .html(content)
                   .appendTo('body')
                   .fadeIn(200);
}