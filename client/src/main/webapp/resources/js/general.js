/* Used predominantly for debugging purposes. */
var intervalsSet = [];

/**
 * Clear setInterval events (by id) specified in the global object.
 * 
 * @param excluding_current <code>true</code> if to exclude setInterval events associated with 
 *                          current simulation, otherwise <code>false</code> to clear all known
 *                          (including for current simulation).
 */
function clear_all(excluding_current) {
  for (var existing_simulation_id in INTERVALS) {
    var is_current = (existing_simulation_id == CURRENT_SIMULATION_ID);

    if (!(is_current && excluding_current)) {
      INTERVALS[existing_simulation_id].clear_all();
      delete INTERVALS[existing_simulation_id];
    }
  }
}

/**
 * Clear all known current setInterval events.
 */
function clear_all_intervals() {
  clear_all(false);
}

/**
 * Clear all timeouts.
 */
function clear_all_timeouts() {
  jQuery.each(TIMEOUTS, function(idx, timeout_id) {
    clearTimeout(timeout_id);
  })
}

/**
 * Called each time an interval is set (for input_values, progress and results) once a simulation
 * run request has successfully returned.
 * 
 * @param purpose, e.g. 'i', 'p', 'r' (for input_values, progress and results respectively).
 * @param interval_id Newly set interval identifier.
 */
function set_interval(purpose, interval_id) {
  intervalsSet.push(interval_id);
  /* Step 1: Clear all intervals from all simulations except the current (may be overkill!) */
  clear_all(true);

  /* Step 2: Create new INTERVALS entry for current sim. if necessary. */
  var sim_ints = INTERVALS[CURRENT_SIMULATION_ID];

  if (sim_ints === undefined) {
    sim_ints = {
      'i': [],
      'p': [],
      'r': [],
      clear_all: function() {
                   for (var purpose in this) {
                     if (this[purpose] instanceof Array) {
                       clear_intervals(this[purpose]);
                     }
                   };
                 }
    };
    INTERVALS[CURRENT_SIMULATION_ID] = sim_ints;
  }

  /* Push new interval to new array */
  var purpose_ints = sim_ints[purpose];
  if (purpose_ints.length != 0) {
    /* Should not occur! Current simulation purpose already had a setIntervals call!
    var message = 'set_interval(' + purpose + ',' + interval_id + ') existing purpose_ints ' + JSON.stringify(purpose_ints);
    alert(message);
    debug(message);
    */
    clear_intervals(purpose_ints);
    sim_ints[purpose] = [];
    purpose_ints = sim_ints[purpose];
  }

  purpose_ints.push(interval_id);
}

/**
 * Clear all the specified intervals.
 * @param interval_ids Identifiers of intervals to clear.
 */
function clear_intervals(interval_ids) {
  jQuery.each(interval_ids, function(idx, interval_id) {
    clearInterval(interval_id);
    if (jQuery.inArray(interval_id, intervalsSet) == -1) {
      /* Should not occur! An interval id to clear isn't the id store (i.e. perhaps not previously set or already removed!
      var message = 'clear_intervals(' + interval_ids + ') interval id ' + interval_id + ' not previously set or already removed!!';
      alert(message);
      debug(message);
      */
    } else {
      intervalsSet.splice(jQuery.inArray(interval_id, intervalsSet), 1 );
    }
  });
}

/**
 * Clear the interval associated with the specified purpose for the specified simulation.
 * 
 * @param purpose, e.g. 'i', 'p', 'r' (for input_values, progress and results respectively).
 * @param simulation_id Simulation identifier.
 * @returns
 */
function clear_interval(purpose, simulation_id) {
  var sim_ints = INTERVALS[simulation_id];
  if (sim_ints !== undefined) {
    var purpose_ints = sim_ints[purpose];
    if (purpose_ints !== undefined) {
      if (purpose_ints.length != 1) {
        /* Should only be 1
        var message = 'clear_interval(' + purpose + ',' + simulation_id + ') purpose_ints ' + JSON.stringify(purpose_ints);
        alert(message);
        debug(message);
        */
      }
      clear_intervals(purpose_ints);
      sim_ints[purpose] = [];
    }
  }
}

/**
 * GUID generator.<br />
 * http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript
 * 
 * @return GUID
 */
function guidGenerator() {
  var S4 = function() {
    return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
  };
  return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}


/**
 * Retrieve indicator of whether job details exist.
 * 
 * @param job_id Job identifier.
 * @return True if job details exist for the identified job.
 */
function has_job_details(job_id) {
  return (JOB_DETAILS[cs_id_as_key(job_id)] != undefined);
}

/**
 * Remove job details for a job identifier.
 * 
 *  @param job_id Job identifier.
 */
function remove_job_details(job_id)  {
  var key = cs_id_as_key(job_id);
  /* debug('deleting job id ' + key + ' from job details'); */
  delete JOB_DETAILS[key];
}

/**
 * Reset the following datasets used in graph display :
 * <ul>
 *   <li>AP_DATASETS</li>
 *   <li>QNET_DATASETS</li>
 *   <li>PCT_CHANGE_DATASETS</li>
 *   <li>EXPERIMENTAL_DATASETS (optional)</li>
 * </ul>
 * 
 * @param reset_experimental Indicator to reset experimental data or not.
 */
function reset_graph_datasets(reset_experimental) {
  AP_DATASETS = [];
  if (reset_experimental) {
    /* when results are processed the experimental data is re-read. */
    EXPERIMENTAL_DATASETS = [];
  }
  PCT_CHANGE_DATASETS = [];
  QNET_DATASETS = [];
}

/**
 * Reset the client display to empty.
 */
function reset_display() {
  hide_js_error();
  cs_hide_message();
  cs_hide_message_error();
  jQuery('.trigger_provenance').hide();
  jQuery('.trigger_input_values').hide();
  jQuery('.trigger_pct_change_data').hide();

  reset_progress();

  reset_graph_datasets(true);
  reset_ap_display();
  reset_pct_change_display();
  reset_input_values();
}

/**
 * Retrieve a job title for the job.
 * 
 * @param job_id Job identifier.
 * @return Job title, e.g. <code>PatchXpress,Qpatch@0.5 (Hz)</code>
 */
function retrieve_job_title(job_id) {
  var key = cs_id_as_key(job_id);
  var job_detail = JOB_DETAILS[key];
  if (job_detail != undefined) {
    var group_name = job_detail[KEY_SHARED_GROUP_NAME];
    var pacing_frequency = cs_retrieve_pacing_frequency(job_id);

    return group_name + '@' + pacing_frequency + ' (Hz)';
  } else {
    /* debug('No job_detail for job_id ' + job_id + ' when retrieving job title.'); */
  }
}

/**
 * Show/Hide information (progress/provenance) data according to level.
 * 
 * @param selected_min Minimum level to show.
 * @param selected_max Maximum level to show.
 */
function show_hide_level(selected_min, selected_max) {
  for (var level_idx = 0; level_idx <= 5; level_idx++) {
    var group = jQuery('.' + PFX_LEVEL_CLASS + LEVEL_SEQ[level_idx]);
    var show = (level_idx >= LEVEL_MIN && level_idx <= LEVEL_MAX) ? true : false;
    if (show) {
      group.show();
    } else {
      group.hide();
    }
  }
}

/**
 * Replace pipe characters used in progress and provenance information with a {@code span} element.
 * 
 * @param text Progress/provenance text.
 * @returns Modified text with pipe char replaced with a span.
 */
function swap_highlighted(text) {
  return text.replace(/\|(.+?)\|/g, function($0, $1) {
           return '<span class="progress_highlight">\'' + $1 + '\'</span>';
         });
}

var MOLAR_UNIT_M = 'M';
var MOLAR_UNIT_uM = 'uM';
var MOLAR_UNIT_nM = 'nM';

/**
 * Convert from a pIC50 value to an IC50 value of the specified units.
 * 
 * @param pIC50 pIC50 value.
 * @returns IC50 value (or nothing if the requested units are not recognised).
 */
function pIC50_to_IC50(pIC50, molar_unit) {
  var multiple = Math.pow(10.0, (-1.0 * pIC50));
  if (molar_unit == MOLAR_UNIT_uM) {
    return multiple *= 1000000;
  } else if (molar_unit == MOLAR_UNIT_nM) {
    return multiple *= 1000000000;
  } else {
    alert('Sorry! Molar unit ' + molar_unit + ' not currently available.');
  }
}
