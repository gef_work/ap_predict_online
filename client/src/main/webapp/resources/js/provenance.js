/* The following are derived from property definitions in BusinessManager OrientDAOImpl.java */
var ASSAY_NAME = 'assayName';
var CLASS_DOSE_RESPONSE = 'DoseResponse';
var CLASS_INDIVIDUAL = 'Individual';
var CLASS_SUMMARY = 'Summary';
var DOSE_IN_UM = 'dose_in_uM';
var DOSE_RESPONSE = 'doseResponse';
var INDIVIDUAL_PIC50 = 'individual_pIC50';
var INDIVIDUAL_RAW_HILL = 'individual_rawHill';
var INDIVIDUAL_WORKFLOW_HILL = 'individual_workflowHill';
var ION_CHANNEL_NAME = 'ionChannelName';
var PROVENANCE = 'provenance';
var SUMMARY_PIC50S = 'summary_pIC50s';
/* The following are derived from property definitions in BusinessManager ProvenanceDAO.java */
var PREFIX_DR = 'dr_';
/* The following are derived from OrientDb nomenclature. */
var AT_CLASS = '@class';
var AT_RID = '@rid';
/* General vars */
var UP = 'UP';
var DOWN = 'DOWN';
var ON = 'ON';
var OFF = 'OFF';

var CHART_ID_HILL = 'hill';
var DATA_ID = 'id';
var DATA_DOSE = 'dose';
var DATA_POINTS = 'points';
var DATA_RESPONSE = 'response';

/* information level vars */
var LEVEL_SEQ = [ 'TRACE', 'DEBUG', 'INFO', 'WARN', 'ERROR' ];

// http://mbostock.github.io/d3/talk/20111018/tree.html
/* The m[3] value below (e.g. 100) represents indentation from left of base node */
var m = [20, 10, 20, 100],
    w = 950 - m[1] - m[3],
    h = 800 - m[0] - m[2],
    i = 0,
    root;

/* These variables are set on jQuery(document).ready ... */
var tree, diagonal, vis;

/**
 * AJAX simulation provenance request.
 * 
 * @param URL to invoke (simulation id included).
 */
function ajax_for_provenance(provenance_url) {
  jQuery('#provenance').empty();
  jQuery('#dose_response').empty();

  jQuery.ajax({
    url      : provenance_url,
    async    : true,
    type     : 'GET',
    data     : { },
    dataType : 'json',
    timeout  : 10000,
    success  : function(response) {
                 process_provenance_response(response);
               },
    error    : function(xhr, status, error) {
                 if (error != undefined) {
                   if (error == 'timeout') {
                     setTimeout(function() { ajax_for_provenance(provenance_url); }, 1110);
                   } else {
                     alert('ajax provenance error ' + error);
                   }
                 }
               }
  });
}

/**
 * Load all the JSON data into the svg.
 * 
 * @param json Provenance data in JSON format.
 */
function load(json) {
  /* debug('** Data loaded from AJAX! **'); */
  try {
    root = JSON.parse(json);
  } catch (e) {
    alert("provenance.js#load(json) : Caught '" + e.message + "' for '" + json + "'.");
  }
  if (root != undefined) {
    root.x0 = h / 2;
    root.y0 = 0;

    if (root.children != undefined) {
      /* Initialize the display to show only the root node. */
      root.children.forEach(toggleAll);
    } else {
      /* This can occur if no children, e.g. pIC50 derived from solitary summary record. */
    }
    toggle(root);
    update(root);
  }
}

/**
 * Process the AJAX response.
 * 
 * @param response AJAX response.
 */
function process_provenance_response(response) {
  /* debug_object(response); */
  var response_obj = response[MODEL_ATTRIBUTE_PROVENANCE];
  if (response_obj != undefined) {
    /* defined in AJAXController inner class */
    var exception = response_obj[KEY_EXCEPTION];
    if (exception != undefined) {
      show_js_error(exception);
    } else {
      /* defined in AJAXController inner class */
      var json = response_obj[KEY_JSON];
      if (json != undefined) {
        load(json);
      } else {
        show_js_error('Could not find \'' + KEY_JSON + '\' in ajax provenance response.');
      }
    }
  } else {
    show_js_error('Could not find \'' + MODEL_ATTRIBUTE_PROVENANCE + '\' in ajax provanence response!');
  }
}

/**
 * 
 * @param source
 */
function update(source) {
  var duration = d3.event && d3.event.altKey ? 5000 : 900;

  // Compute the new tree layout.
  var nodes = tree.nodes(root).reverse();

  // Normalize for fixed-depth.
  nodes.forEach(function(d) { d.y = d.depth * 250; });

  // Update the nodes
  var node = vis.selectAll("g.node")
                .data(nodes, function(d) { return d.id || (d.id = ++i); });

  // Enter any new nodes at the parent's previous position.
  var nodeEnter = node.enter().append("svg:g")
                      .attr("class", "node")
                      .attr("transform", function(d) {
                         return "translate(" + source.y0 + "," + source.x0 + ")"; })
                      .on("click", function(d) {
                                     jQuery('#msg').show();
                                     /* Visually insignificant delay to enable show() to take effect. */
                                     setTimeout(function() {
                                       toggle(d); update(d);
                                     }, 25)
                                   });

  nodeEnter.append("svg:circle")
           .attr("r", 1e-6)
           .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; })
           .on("mouseover", function(d) {
                 var pos = d3.mouse(jQuery('#svg_area')[0]);
                 var x = pos[0] + 20;
                 var y = pos[1] + 20;
                 var provenance_id = create_provenance_id(d[AT_RID]);
                 /* On occasions provenance data is left hanging during transitions - hide them. */
                 jQuery('div[id^=prov_]').hide();
                 jQuery('#' + provenance_id).css({ 'top': y + 'px', 'left': x + 'px', 'position': 'absolute' })
                                            .fadeIn(600);
                 show_chart(d);
                 show_raw_data(d);
               })
           .on("mouseout", function(d) {
                 var provenance_id = create_provenance_id(d[AT_RID]);
                 jQuery('#' + provenance_id).hide();
               });

  nodeEnter.append("svg:text")
           .attr("x", -10)
           .attr("y", -15)
           .attr("dy", ".35em")
           .attr("text-anchor", "middle")
           .text(function(d) { assign_stuff(d);
                               show_hide_level(LEVEL_MIN, LEVEL_MAX);
                               return find_name_for_node(d); } )
           .style({"fill-opacity": 1e-6, "font-family": "monospace",
                   "font-size": "11px" });

  // Transition nodes to their new position.
  var nodeUpdate = node.transition()
                       .duration(duration)
                       .attr("transform", function(d) {
                         return "translate(" + d.y + "," + d.x + ")"; });

  nodeUpdate.select("circle")
            .attr("r", 4.5)
            .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

  nodeUpdate.select("text")
            .style("fill-opacity", 1);

  // Transition exiting nodes to the parent's new position.
  var nodeExit = node.exit().transition()
                     .duration(duration)
                     .attr("transform", function(d) {
                       return "translate(" + source.y + "," + source.x + ")"; })
                     .remove();

  nodeExit.select("circle")
          .attr("r", 1e-6);

  nodeExit.select("text")
          .style("fill-opacity", 1e-6);

  // Update the links
  var link = vis.selectAll("path.link")
                .data(tree.links(nodes), function(d) { return d.target.id; });

  // Enter any new links at the parent's previous position.
  link.enter().insert("svg:path", "g")
      .attr("class", "link")
      .attr("d", function(d) {
        var o = {x: source.x0, y: source.y0};
        return diagonal({source: o, target: o}); })
      .transition()
      .duration(duration)
      .attr("d", diagonal);

  // Transition links to their new position.
  link.transition()
      .duration(duration)
      .attr("d", diagonal);

  // Transition exiting nodes to the parent's new position.
  link.exit().transition()
             .duration(duration)
             .attr("d", function(d) {
               var o = {x: source.x, y: source.y};
               return diagonal({source: o, target: o}); })
             .remove();

  // Stash the old positions for transition.
  nodes.forEach(function(d) {
    d.x0 = d.x;
    d.y0 = d.y;
  });

  if (jQuery('#msg').is(':visible')) {
    jQuery('#msg').hide();
  }
}
// Toggle children.
function toggle(d) {
  if (d.children) {
    d._children = d.children;
    d.children = null;
  } else {
    d.children = d._children;
    d._children = null;
  }
}

function toggleAll(d) {
  if (d.children) {
    d.children.forEach(toggleAll);
    toggle(d);
  }
}

/**
 * Create a textual value of the dose-response identifier based on the "@rid" value.
 * 
 * @param at_rid The value of the "@rid" json key.
 * @returns Textual dose-response identifier, e.g. "dr_9_0".
 */
function create_doseresp_holding_element_id(at_rid) {
  return 'dr_data_parent' + convert_rid(at_rid);
}

/**
 * Create a textual value for the provenance identifier based on the "@rid" value.
 * 
 * @param at_rid The value of the "@rid" json key.
 * @returns Textual provenance identifier, e.g. "prov_9_0".
 */
function create_provenance_id(at_rid) {
  return 'prov_' + convert_rid(at_rid);
}

function assign_stuff(node) {
  var at_rid = node[AT_RID];
  if (at_rid) {
    var provenance_id = create_provenance_id(at_rid);
    if (!document.getElementById(provenance_id)) {
      if (node[PROVENANCE]) {
        assign_provenance(node, provenance_id);
      }
    }
    var at_class = node[AT_CLASS];
    if (at_class == CLASS_INDIVIDUAL) {
      /* Holding element id, e.g. dr_data_parent_11_3 */
      var holding_el_id = create_doseresp_holding_element_id(at_rid);
      if (document.getElementById(holding_el_id) == null) {
        /* e.g. (node, dr_data_parent_11_3, _11_3) */
        assign_doseresp(node, holding_el_id, convert_rid(at_rid)); 
      }
    }
  }
}

/**
 * Look for available dose-response data for each of the dose-response provenance data and create 
 * structures for subsequent graphical display.
 * 
 * @param node Node (potentially, i.e. may not be any data) containing the dose-response data for
 *             different provenance sources.
 * @param holding_el_id Identifier of element holding the dose-response data.
 * @param converted_rid Part of the key identifying the dose-response data child element of holder.
 */
function assign_doseresp(node, holding_el_id, converted_rid) {
  var dose_response_holders = {};
  for (var property in node) {
    if (node.hasOwnProperty(property)) {
      /* If the property name starts with "dr_" its value contains dose-response data. */
      if (property.indexOf(PREFIX_DR) == 0) {
        dose_response_holders[property] = node[property];
      }
    }
  }

  /* Object.keys is ECMA 5 (so need IE > 8) */
  if (Object.keys(dose_response_holders).length > 0) {
    var parent_doseresp_div = jQuery('<div>').attr('id', holding_el_id);
    Object.keys(dose_response_holders).forEach(function (key) {
      /* New element id, e.g. dr_fs_maxresp_11_14*/
      var new_element_id = key + converted_rid;
      create_drf_data_div(dose_response_holders[key], new_element_id, parent_doseresp_div);
    });
    jQuery('#dose_response').append(parent_doseresp_div);
  }
}

/**
 * If there are dose-responses supplied, create a provenance-specific, identified parent 
 * <code>div</code> element in which to embed <code>span</code> elements representing individual 
 * dose-response values.
 * 
 * @param dose_responses Dose-response pairing(s), or undefined if none available.
 * @param new_element_id Provenance-specific identifier for new <code>div</code>.
 * @param parent_doseresp_div Parent <code>div</code> for any newly-created provenance child 
 *                            <code>div</code> elements.
 */
function create_drf_data_div(dose_responses, new_element_id, parent_doseresp_div) {
  if (dose_responses) {
    var new_div = jQuery('<div>').attr('id', new_element_id);
    parent_doseresp_div.append(create_spans(dose_responses, new_div));
  }
}

/**
 * For each of the dose-responses for a particular provenance source (e.g. nearest sub-50) create a
 * structure of <code>span</code> elements within a new <code>div</code> element.
 * <p />
 * These structures are read in by the {@link show_chart} function.
 * 
 * @param dose_responses Dose-response pairing(s).
 * @param new_div New <code>div</code> element.
 * @returns New <code>div</code> element with new content.
 */
function create_spans(dose_responses, new_div) {
  jQuery.each(dose_responses, function(key, val) {
    var span_pair = jQuery('<span>').addClass('pair');
    var span_dose = jQuery('<span>').text(key).addClass('dose');
    var span_resp = jQuery('<span>').text(val).addClass('resp');
    jQuery('<br>').appendTo(span_pair);
    span_pair.append(span_dose);
    span_pair.append(span_resp);
    new_div.append(span_pair);
  });
  return new_div;
}

/**
 * Create the display which pops up how the data was derived when mouseover'ing a node.
 * 
 * @param node Target node.
 * @param provenance_id Provenance identifier.
 */
function assign_provenance(node, provenance_id) {
  var new_div = jQuery('<div>').attr('id', provenance_id)
                               .css({ 'display': 'none',
                                      'box-shadow': '3px 3px 2px steelblue' })
                               .addClass('block_span cs_rounded_5');
  jQuery.each(node[PROVENANCE], function() {
    new_div.append(jQuery('<span>').html(swap_highlighted(this[KEY_TEXT]))
                                   .addClass(PFX_LEVEL_CLASS + this[KEY_LEVEL]));
  });
  jQuery('#provenance').append(new_div);
}

/**
 * Convert the html-unfriendly characters in the <code>@rid</code> value with something else.
 *  
 * @param at_rid Original <code>@rid</code> value.
 * @returns HTML-friendly value.
 */
function convert_rid(at_rid) {
  at_rid = at_rid.replace('#', '_');
  at_rid = at_rid.replace(':', '_');
  return at_rid;
}

function find_name_for_node(node) {
  /* The node field names correspond to the field name values in business manager's
     OrientProvenanceDAOImpl */
  var at_class = node[AT_CLASS];
  var ret_val = 'N/A';

  if (at_class == 'Fundamental') {
    ret_val = node['compoundName'];
  } else if (at_class == CLASS_SUMMARY) {
    var pIC50_data = '';
    if (node[SUMMARY_PIC50S]) {
      var provisional_pIC50_data = '[' + node[SUMMARY_PIC50S];
      var avgHillCoeff = '';
      if (node['summary_avgHillCoeff']) {
        avgHillCoeff = node['summary_avgHillCoeff'];
        provisional_pIC50_data += ',' + avgHillCoeff;
      }
      provisional_pIC50_data += '] (pIC50(s))';
      pIC50_data = provisional_pIC50_data;
    }
    var assay_ion_ch = '';
    if (node[ASSAY_NAME] && node[ION_CHANNEL_NAME]) {
      assay_ion_ch = node[ASSAY_NAME] + ':' + node[ION_CHANNEL_NAME] + '] ';
    }
    ret_val = '[' + at_class.substr(0,1) + ':' + assay_ion_ch + pIC50_data;
  } else if (at_class == CLASS_INDIVIDUAL) {
    var pIC50_data = '';
    if (node[INDIVIDUAL_PIC50]) {
      var pIC50 = node[INDIVIDUAL_PIC50];
      var workflow_hill = node[INDIVIDUAL_WORKFLOW_HILL];
      var raw_hill = node[INDIVIDUAL_RAW_HILL];
      /* debug('raw ' + raw_hill + ' fitted ' + workflow_hill); */
      var use_hill = workflow_hill || raw_hill;
      pIC50_data = '[' + pIC50.substr(0,5) + '/' + use_hill + '] (pIC50,Hill)';
    }
    var prefix = '[' + at_class.substr(0,1);
    if (node[ASSAY_NAME] && node[ION_CHANNEL_NAME]) {
      prefix += ':' + node[ASSAY_NAME] + ':' + node[ION_CHANNEL_NAME];
    }
    prefix += '] ';
    ret_val = prefix + pIC50_data;
  } else if (at_class == CLASS_DOSE_RESPONSE) {
    var dose_response_pair = '';
    if (node[DOSE_IN_UM] && node[DOSE_RESPONSE]) {
      var dose_in_uM = node[DOSE_IN_UM];
      dose_response_pair = ' [' + dose_in_uM.substr(0,5) + ',' + node[DOSE_RESPONSE] + '] (µM,%)';
    }
    ret_val = '[D-R]' + dose_response_pair;
  }

  return ret_val;
}

var chart, chart_div_w, chart_div_h;
var ch_margin, ch_width, ch_height;
var ch_x, ch_y, ch_xAxis, ch_yAxis, ch_line, ch_svg;

var concentration_points = [];

/**
 * Populate the concentration (dose) axis points for the Hill curve display.
 */
function initialise_concentration_points() {
  for (var idx = -3; idx <= 3; idx+=0.05) {
    concentration_points.push(Math.pow(10, idx));
  }
}

/**
 * Calculate the response for the specified concentration, IC50 and Hill coefficient.
 * 
 * @param concentration Concentration.
 * @param IC50 IC50 value.
 * @param hill Hill coefficient.
 * @returns Calculated response.
 */
function compute_response_for_hill(concentration, IC50, hill) {
  var response;

  var a = concentration/IC50;
  var b = Math.pow(a, hill);
  var c = 1 + b;
  var d = 1/c;
  var e = 100*d;
  response = 100-e;

  return response;
}

function chart_display(on_off) {
  if (on_off == OFF) {
    /* Clear any existing chart display. */
    jQuery('#chart').find('svg').find('g').empty();
    jQuery('#chart_info').show();
  } else {
    jQuery('#chart_info').hide();
  }
}
/**
 * Display the dose-response data in a chart.
 * <p />
 * The dose-response data itself (if available) has been marked up in the document via the 
 * {@link create_spans} function and will be referenced using the <code>@rid</code> value.
 * 
 * @param node Node containing <code>@rid</code>, pIC50, fitted hill, raw hill data.
 */
function show_chart(node) {
  var at_rid = node[AT_RID];
  if (at_rid != undefined) {
    var doseresp_id = create_doseresp_holding_element_id(at_rid);
    /* Look for an element in the document containing the dose-response data for this dose-response
         id (derived from the @rid value in provenance). */ 
    if (document.getElementById(doseresp_id) != null) { 
      var dose_resp_div = jQuery('#' + doseresp_id);
      var sub_elements = dose_resp_div.find('div');

      if (sub_elements) {
        chart_display(OFF);

        var data = [];

        /* First load in the dose-response data */
        jQuery.each(sub_elements, function() {
          var data_element_id = jQuery(this).attr('id');
          var data_unit = {};
          data_unit[DATA_ID] = data_element_id;

          var points = [];
          /* span.{pair,dose,resp} are defined in 'create_spans' function */
          jQuery.each(jQuery('#' + data_element_id).find('span.pair'), function() {
            var pair = jQuery(this);
            var dose = pair.find('span.dose').text();
            var response = pair.find('span.resp').text();

            var data_pair = {};
            data_pair[DATA_DOSE] = +dose;
            data_pair[DATA_RESPONSE] = +response;

            points.push(data_pair);
          });
          data_unit[DATA_POINTS] = points;
          if (points.length > 0) {
            data.push(data_unit);
          }
        });

        var individual_pIC50 = node[INDIVIDUAL_PIC50];
        /* No pIC50 value would be expected in some circumstances, e.g. data marked as invalid. */
        if (individual_pIC50 != undefined) {
          var workflow_hill = node[INDIVIDUAL_WORKFLOW_HILL];
          var raw_hill = node[INDIVIDUAL_RAW_HILL];
          var hill = workflow_hill || raw_hill;

          var individual_IC50 = pIC50_to_IC50(individual_pIC50, MOLAR_UNIT_uM);

          var hill_unit = {};
          hill_unit[DATA_ID] = CHART_ID_HILL;
          /* Now load in the hill curve */
          var hill_points = [];
          for (var conc_idx = 0; conc_idx < concentration_points.length; conc_idx++) {

            // compute response
            var hill_point = {};

            var concentration = concentration_points[conc_idx];
            var response = compute_response_for_hill(concentration, individual_IC50, hill);
            hill_point[DATA_DOSE] = concentration;
            hill_point[DATA_RESPONSE] = response;

            hill_points.push(hill_point);
          }
          hill_unit[DATA_POINTS] = hill_points;
          if (hill_points.length > 0) {
            data.push(hill_unit);
          } else {
            alert('no data 2');
          }
        }

        load_chart(data);
      }
    } else {
      /* No dose-response data for current node - clear any existing chart display. */
      chart_display(OFF);
    }
  }
}

/**
 * Determine whether a new value exceeds the existing value in the specified direction. 
 * 
 * @param existing_val Existing value.
 * @param new_val New value.
 * @param way Direction.
 * @returns Existing value if new value doesn't exceed it in specified direction, otherwise the new
 *          value.
 */
function assign_val(existing_val, new_val, way) {
  var ret_val;
  if (existing_val === undefined) {
    ret_val = new_val;
  } else {
    if (way == UP) {
      if (existing_val < new_val) {
        ret_val = new_val;
      } else {
        ret_val = existing_val;
      }
    } else {
      if (existing_val > new_val) {
        ret_val = new_val;
      } else {
        ret_val = existing_val;
      }
    }
  }

  return ret_val;
}

/**
 * Determine axis ranges and then make call to plot the chart.
 * 
 * @param data Potentially all Hill and dose-response data.
 */
function load_chart(data) {

  chart_display(ON);

  var x_from;
  var x_to;
  var y_from = -10.0;  /* -10% response. */
  var y_to = 100.0;    /* 100% response. */

  /* Traverse the data points and determine the range of values being referenced. */
  data.forEach(function(d) {
    /*var id = d[DATA_ID];*/
    var points = d[DATA_POINTS];
    var x_vals = d3.extent(points, function(d) { return d[DATA_DOSE]; });
    var y_vals = d3.extent(points, function(d) { return d[DATA_RESPONSE]; });
    x_from = assign_val(x_from, x_vals[0], DOWN);
    x_to = assign_val(x_to, x_vals[1], UP);
    y_from = assign_val(y_from, y_vals[0], DOWN);
    y_to = assign_val(y_to, y_vals[1], UP);
  });

  var x_extent = [];
  var y_extent = [];
  x_extent.push(x_from);
  x_extent.push(x_to);
  y_extent.push(y_from);
  y_extent.push(y_to);

  ch_x.domain(x_extent);
  ch_y.domain(y_extent);

  data.forEach(function(d) {
    var id = d[DATA_ID];
    var points = d[DATA_POINTS];
    display_chart(points, id);
  });
}

/**
 * Display raw data.
 * 
 * @param data Raw data to display.
 */
function show_raw_data(data) {
  var raw_data = data['rawData'];
  if (raw_data != undefined) {
    var raw_json = jQuery.parseJSON(raw_data);
    var json_keys = [];
    jQuery.each(raw_json, function(key, val) {
      json_keys.push(key);
    });
    json_keys.sort();
    var parent_div = jQuery('#raw_data');
    parent_div.empty();
    var table = jQuery('<table>').css( { 'margin': '3px', 'width': '98%' } );
    for (var i = 0; i < json_keys.length; i++) {
      var json_key = json_keys[i];
      var val = raw_json[json_key];

      var row = jQuery('<tr>');
      row.append(jQuery('<td>').text(json_key));
      if (jQuery.trim(val) == '') {
        val = '&nbsp;';
        row.append(jQuery('<td>').html(val));
      } else {
        row.append(jQuery('<td>').text(val));
      }
      table.append(row);
    }
    parent_div.append(table);
    parent_div.find('table tr:even').addClass('row-highlight');
    parent_div.find('table td').css({'border-top': 'solid #eee 1px',
                                     'border-bottom': 'solid #eee 1px',
                                     'padding-left': '2px' });
  }
}

/**
 * Display the SVG.
 * 
 * @param data Chart data points.
 * @param chart_id Identifier.
 */
function display_chart(data, chart_id) {

  /* Hill data appears differently. */
  var radius = chart_id == CHART_ID_HILL ? 1.0 : 3.0;

  ch_svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + ch_height + ")")
        .call(ch_xAxis)
        .append("text")
        .attr("x", 100)
        .attr("y", 30)
        .attr("text-anchor", "start")
        .text("Dose (µM)")
        .style({"font-family": "monospace", "font-size": "11px" });

  ch_svg.append("g")
        .attr("class", "y axis")
        .call(ch_yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("x", -150)
        .attr("y", -40)
        .attr("dy", ".71em")
        .attr("text-anchor", "start")
        .text("Response (%)")
        .style({"font-family": "monospace", "font-size": "11px" });

  /* Using class to colour the dose-response and hill points (via svg "fill") */
  ch_svg.selectAll("dot")
        .data(data)
        .enter().append("circle")
        .attr("class", chart_id)
        .attr("r", radius)
        .attr("cx", function(d) { return ch_x(d[DATA_DOSE]); })
        .attr("cy", function(d) { return ch_y(d[DATA_RESPONSE]); });
}
