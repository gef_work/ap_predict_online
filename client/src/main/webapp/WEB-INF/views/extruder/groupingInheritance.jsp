<%@taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">
  /* <![CDATA] */
  if (grp_state != undefined) {
  } else {
    /* default assay grouping state when page is first shown */
<c:choose>
  <c:when test="${ma_config_assay_grouping}">
    grp_state = true;
  </c:when>
  <c:otherwise>
    grp_state = false;
  </c:otherwise>
</c:choose>
  }

  if (inh_state != undefined) {
  } else {
    /* default value inheritance state when page is first shown */
<c:choose>
  <c:when test="${ma_config_value_inheriting}">
    inh_state = true;
  </c:when>
  <c:otherwise>
    inh_state = false;
  </c:otherwise>
</c:choose>
  }

  if (inh_btw_state != undefined) {
  } else {
    /* default between group value inheritance state when page is first shown */
<c:choose>
  <c:when test="${ma_config_between_groups}">
    inh_btw_state = true;
  </c:when>
  <c:otherwise>
    inh_btw_state = false;
  </c:otherwise>
</c:choose>
  }

  if (inh_wth_state != undefined) {
  } else {
    /* default within groups value inheritance state when page is first shown */
<c:choose>
  <c:when test="${ma_config_within_groups}">
    inh_wth_state = true;
  </c:when>
  <c:otherwise>
    inh_wth_state = false;
  </c:otherwise>
</c:choose>
  }
  /* ]]> */
</script>

<div class="text" style="height: 60px;">
  <div class="cs_float_left">
    <div class="cs_clearing">
      <label for="grouping_on_off"
             title="<spring:message code="extruder.grouping_on_off_desc" />"><spring:message code="extruder.grouping_on_off" /></label>
      <span class="cs_float_right">
        <input id="grouping_on_off"
               name="grouping"
               type="checkbox" />
      </span>
    </div>
  </div>
  <div class="cs_float_left" style="margin-left: 5px;">
    <div class="cs_clearing">
      <label for="inheritance_on_off"
             title="<spring:message code="extruder.inheritance_on_off_desc" />"><spring:message code="extruder.inheritance_on_off" /></label>
      <span class="cs_float_right">
        <input id="inheritance_on_off"
               type="checkbox" />
      </span>
    </div>
    <div class="cs_clearing inheritance_option">
      <label for="inheritance_within"
             title="<spring:message code="extruder.inheritance_within_groups_desc" />">&middot; <spring:message code="extruder.inheritance_within_groups" /></label>
      <span class="cs_float_right">
        <input id="inheritance_within"
               name="within"
               type="checkbox" />
      </span>
    </div>
    <div class="cs_clearing inheritance_option">
      <label for="inheritance_between"
             title="<spring:message code="extruder.inheritance_between_groups_desc" />">&middot; <spring:message code="extruder.inheritance_between_groups" /></label>
      <span class="cs_float_right">
        <input id="inheritance_between"
               name="between"
               type="checkbox" />
      </span>
    </div>
  </div>
</div>

<script type="text/javascript">
  /* <![CDATA] */
  jQuery(document).ready(
    function() {
      grp_on_off = jQuery('#grouping_on_off');
      inh_on_off = jQuery('#inheritance_on_off');
      inh_within = jQuery('#inheritance_within');
      inh_between = jQuery('#inheritance_between');

      inh_options = jQuery('.inheritance_option');

      (grp_state != undefined && !grp_state) ? grp_on_off.prop('checked', false) : grp_on_off.prop('checked', true);
      (inh_state != undefined && !inh_state) ? inh_on_off.prop('checked', false) : inh_on_off.prop('checked', true);
      (inh_btw_state != undefined && !inh_btw_state) ? inh_between.prop('checked', false) : inh_between.prop('checked', true);
      (inh_wth_state != undefined && !inh_wth_state) ? inh_within.prop('checked', false) : inh_within.prop('checked', true);

      display_group_inherit();

      grp_on_off.change(function() { display_group_inherit(); });
      inh_on_off.change(function() {
        if (inh_on_off.is(':checked')) {
          inh_between.prop('checked', true);
          inh_within.prop('checked', true);
        } else {
          inh_between.prop('checked', false);
          inh_within.prop('checked', false);
        }
        display_group_inherit();
      });
      inh_between.change(function() { display_group_inherit(); });
      inh_within.change(function() { display_group_inherit(); });
    });
  /* ]]> */
</script>