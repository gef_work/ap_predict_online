<%@taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">
  /* <![CDATA] */
  /* ]]> */
</script>

<div class="text" style="height: 110px;">
  <div class="cs_float_left">
    <div class="cs_clearing">
      <spring:message code="extruder.cellModel" /> :
      <select id="cell_models" />
      <div id="model_info" style="margin-top: 10px;" />
    </div>
  </div>
</div>

<script type="text/javascript">
  /* <![CDATA] */
  jQuery(document).ready(
    function() {
      var cell_models_el = jQuery('#cell_models');
      var model_info_el = jQuery('#model_info');

      /**
       * Append the CellML information corresponding to the model.
       *
       * @param model Model to provide information.
       */
      function create_model_info(model) {
        model_info_el.empty();
        model_info_el.append(jQuery('<p />').html(model.description));
        var link_cellml = jQuery('<a />').attr( { 'href': model.urlCellML,
                                                  'title': 'CellML URL',
                                                  'target': 'blank' } )
                                         .css( { 'color': 'white' } )
                                         .text('CellML URL');
        var link_paper = jQuery('<a />').attr( { 'href': model.urlPaper,
                                                 'title': 'Paper URL',
                                                 'target': 'blank' } )
                                        .css( { 'color': 'white' } )
                                        .text('Paper URL');
        var pacing_max_time_text = (model.pacingMaxTime != '') ?
                                   model.pacingMaxTime : '<spring:message code="cs_general.unlimited" />';
        var pacing_max_time = jQuery('<span />').text(pacing_max_time_text);
        var para1 = jQuery('<p />');
        para1.append(link_cellml);
        para1.append('&nbsp;&nbsp;');
        para1.append(link_paper);
        var para2 = jQuery('<p />');
        para2.append('<spring:message code="cs_simulation.maximum_pacing_time" />: ');
        para2.append(pacing_max_time);
        para2.append(' (mins)');

        model_info_el.append(para1);
        model_info_el.append(para2);
      }

      /* Traverse the CellML models and highlight a previously selected one or otherwise show a default */
      jQuery.each(MODELS, function(model_id, model) {
        var option_el = jQuery('<option />').attr( { 'value': model_id } )
                                            .text(model.name);
        /* Check the current model identifier against the state variable. */
        if (model_id == MODEL_IDENTIFIER_STATE) {
          option_el.attr( { 'selected': 'selected' } );
          create_model_info(model);
        }
        cell_models_el.append(option_el);
      });

      /* If someone selects a different CellML model then update information section and record
         the selected model identifier in the state variable. */
      cell_models_el.change(function(e) {
        MODEL_IDENTIFIER_STATE = cell_models_el.val();
        create_model_info(MODELS[cell_models_el.val() + '']);
      });
    }
  );
  /* ]]> */
</script>