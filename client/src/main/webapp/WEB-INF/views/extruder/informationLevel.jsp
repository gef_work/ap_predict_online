<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="text" style="height: 60px;">
  <span title="<spring:message code="extruder.move_level_sliders" />">
    <spring:message code="extruder.displaying_levels" />
  </span> : <span id="information_level"></span>
  <div class="level">
    <div>(<spring:message code="cs_general.min" />)</div>
    <div style="width: 50%" id="slider-range"></div>
    <div>(<spring:message code="cs_general.max" />)</div>
  </div>
</div>

<script type="text/javascript">
  /* <![CDATA] */
  jQuery(document).ready(
    function() {
      jQuery('#slider-range').slider({
        range: true,
        min: 0,
        max: 4,
        step: 1,
        values: [ LEVEL_MIN, LEVEL_MAX ],
        slide: function(event, ui) {
          LEVEL_MIN = ui.values[0];
          LEVEL_MAX = ui.values[1];
          jQuery('#information_level').text(LEVEL_SEQ[LEVEL_MIN] + ' - ' + LEVEL_SEQ[LEVEL_MAX]);

          show_hide_level_progress(LEVEL_MIN, LEVEL_MAX);
        }
      });

      jQuery('div.level').css({ 'padding' : '10px 0px' });
      jQuery('div.level>div').css({ 'float' : 'left', 'padding' : '0px 10px' });
      var start_min = jQuery('#slider-range').slider('values', 0);
      var start_max = jQuery('#slider-range').slider('values', 1);
      jQuery('#information_level').text(LEVEL_SEQ[start_min] + ' - ' + LEVEL_SEQ[start_max]);
    });
  /* ]]> */
</script>
