<%@taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="uk.ac.ox.cs.epsrc.client.ClientIdentifiers" %>
<div class="text title"><span class="label"><spring:message code="extruder.configuration_options" /></span></div>
<div class="voice {panel:'<%= ClientIdentifiers.URL_PREFIX_EXTRUDER %>cellModels'}"><span class="label subtitle"><spring:message code="extruder.cellModels" /></span></div>
<div class="voice {panel:'<%= ClientIdentifiers.URL_PREFIX_EXTRUDER %>groupingInheritance'}"><span class="label subtitle"><spring:message code="extruder.grouping_inheritance" /></span></div>
<div class="voice {panel:'<%= ClientIdentifiers.URL_PREFIX_EXTRUDER %>strategies'}"><span class="label subtitle"><spring:message code="extruder.strategies" /></span></div>
<div class="voice {panel:'<%= ClientIdentifiers.URL_PREFIX_EXTRUDER %>doseResponseFitting'}"><span class="label subtitle"><spring:message code="extruder.dose_response_fitting" /></span></div>
<div class="voice {panel:'<%= ClientIdentifiers.URL_PREFIX_EXTRUDER %>units'}"><span class="label subtitle"><spring:message code="extruder.units" /></span></div>
<sec:authorize access="hasAnyRole('ROLE_POWER_USER')">
<div class="voice {panel:'<%= ClientIdentifiers.URL_PREFIX_EXTRUDER %>resetReRun'}"><span class="label subtitle"><spring:message code="extruder.reset_forcererun" /></span></div>
</sec:authorize>
<div class="voice {panel:'<%= ClientIdentifiers.URL_PREFIX_EXTRUDER %>informationLevel'}"><span class="label subtitle"><spring:message code="extruder.information_level" /></span></div>