<%@taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn"     uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="uk.ac.ox.cs.epsrc.client.ClientIdentifiers" %>
<script type="text/javascript">
  /* <![CDATA] */
  var PFX_STRATEGIES_CHECKBOX = 'strategies_chkbox_';

  function get_checkbox_id(id) {
    return PFX_STRATEGIES_CHECKBOX + id;
  }

  if (strategies_state != undefined) {
  } else {
    /* default strategies state when page is first shown */
    strategies_state = [];
<c:forEach items="${ma_config_pc50_eval_strategies}"
           var="pc50EvalStrategy"
           varStatus="pc50_eval_strategy">
    var state_object = {};
    state_object[STATEVAR_KEY_OBJECT_KEY] = get_object_key('<c:out value="${pc50EvalStrategy.id}" />');
    state_object[STATEVAR_KEY_ACTIVE] = <c:out value="${pc50EvalStrategy.defaultActive}" />;
    strategies_state[<c:out value="${pc50_eval_strategy.index}" />] = state_object;
</c:forEach>
  }

  /* display the workflow objects (as found in strategies_state) as a ul element */
  function display_strategies(strategies_state) {
    pc50_sortable.empty();
    var max_display_len = 27;
    jQuery.each(strategies_state, function(index, value) {
      var state = strategies_state[index];

      var object_key = state[STATEVAR_KEY_OBJECT_KEY];
      var active = state[STATEVAR_KEY_ACTIVE];

      var strategy_object = strategy_objects[object_key];
      var id = strategy_object['id'];
      var description = strategy_object['description'];
      var defaultInvocationOrder = strategy_object['defaultInvocationOrder'];

      var li = jQuery('<li />').attr({ 'id': get_object_key(id) });
      var li_text = '[' + defaultInvocationOrder + ']&nbsp;&nbsp;';

      var input = jQuery('<input />').attr({ 'id': get_checkbox_id(id), 'type': 'checkbox', 'class': 'cs_float_right' });
      if (active) {
        input.attr({ 'checked' : 'checked' });
      }
      var span;
      if (description.length > max_display_len) {
        span = jQuery('<span />').addClass('cs_no_display').text(description);
        li_text += description.substring(0, max_display_len) + ' ..';
        li.addClass('truncated_desc');
      } else {
        li_text += description;
      }
      li.html(li_text);
      if (span != undefined) li.append(span);
      li.append(input);

      pc50_sortable.append(li)
    }); 
  }
  /* ]]> */
</script>

<div class="text">
  <div style="margin: 5px;"><spring:message code="extruder.strategies_drag_drop" /> :</div>
  <div>
    <div style="margin: 2px 0px; border: solid 1px gray;">
      <span class="cs_float_left"
            title="<spring:message code="extruder.strategies_dflt_inv_order" />">[<spring:message code="extruder.strategies_dflt_inv_order_short" />]</span>
      <span class="cs_clearing" style="padding-left: 50px;"
            title="<spring:message code="extruder.strategies_strategy_desc" />"><spring:message code="cs_general.description" /></span>
      <span class="cs_float_right"
            title="<spring:message code="extruder.strategies_strategy_act_disact" />"><spring:message code="extruder.strategies_strategy_active" /></span>
    </div>
    <ul id="pc50_sortable" style="border: solid 1px gray;"></ul>
  </div>
  <div id="strategy_description"></div>
</div>

<script type="text/javascript">
  /* <![CDATA] */
  jQuery(document).ready(
    function() {
      pc50_sortable = jQuery('#pc50_sortable');
      display_strategies(strategies_state);
      pc50_sortable.sortable({
        placeholder: 'ui-state-highlight'
      });
      pc50_sortable.disableSelection();
      pc50_sortable.on('sortupdate', function(event, ui) {
        /* This is kicked off on a change in sequence on drag-and-drop completion.
           It stores the current state (but doesn't save state if there's been no drag-and-drop!) */
        strategies_state = [];
        var idx = 0;
        pc50_sortable.find('li').each(function() {
          var each_li = jQuery(this);
          var active = each_li.find('input[id^=' + PFX_STRATEGIES_CHECKBOX + ']').is(':checked');

          var object_key = each_li.attr('id');
          var state_object = {};
          state_object[STATEVAR_KEY_OBJECT_KEY] = object_key;
          state_object[STATEVAR_KEY_ACTIVE] = active;
          strategies_state[idx++] = state_object;
        });
      });

      jQuery('.truncated_desc').mouseover(function() {
        var full_description = jQuery(this).find('span').text();
        jQuery('#strategy_description').html('<spring:message code="cs_general.description" /> :<p>"' + full_description + '"</p>');
      }).mouseout(function() {
        jQuery('#strategy_description').html('&nbsp;');
      });

      /* Need to keep a record in the state variable of when a checkbox is clicked */
      jQuery('input[id^=' + PFX_STRATEGIES_CHECKBOX + ']').change(function() {
        var checkbox = jQuery(this);
        /* find the workflow surr. pri. key in order to determine the object key */
        var chkbox_id = checkbox.attr('id');
        var id = chkbox_id.replace(PFX_STRATEGIES_CHECKBOX, '');
        var object_key = get_object_key(id);

        /* loop through the workflow states until we hit this checkbox's state holder */
        jQuery.each(strategies_state, function(index, value) {
          var state = value;
          if (state[STATEVAR_KEY_OBJECT_KEY] == object_key) {
            var active = checkbox.is(':checked');
            state[STATEVAR_KEY_ACTIVE] = active;
            return false;
          }
        });
      });
    }
  );

  /* ]]> */
</script>
