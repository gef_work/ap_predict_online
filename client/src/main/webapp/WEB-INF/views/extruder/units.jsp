<%@taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">
  /* <![CDATA] */
  /* ]]> */
</script>

<div class="text" style="height: 30px;">
  <div class="cs_float_left">
    <div class="cs_clearing">
      <spring:message code="extruder.c50_values" /> :
      <select id="c50_units" />
    </div>
  </div>
</div>

<script type="text/javascript">
  /* <![CDATA] */
  jQuery(document).ready(
    function() {
      var c50_units_el = jQuery('#c50_units');
      /* Traverse the C50 units and highlight a previously selected one or otherwise show a default */
      jQuery.each(C50_UNITS, function(each_c50_class, each_c50_value) {
        var option_el = jQuery('<option />').attr( { 'value': each_c50_class } )
                                            .text(each_c50_value);
        if (C50_UNIT_STATE == each_c50_class) {
          option_el.attr( { 'selected': 'selected' } );
        }
        c50_units_el.append(option_el);
      });

      /* If someone selects a different C50 unit then update page. */
      c50_units_el.change(function(e) {
        show_c50_unit(c50_units_el.val());
      });
    }
  );
  /* ]]> */
</script>