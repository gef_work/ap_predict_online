<%@taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">
  /* <![CDATA] */
  if (force_rerun_state != undefined) {
  } else {
    /* default simulation force re-run when page is first shown */
<c:choose>
  <c:when test="${ma_config_force_rerun}">
    force_rerun_state = true;
  </c:when>
  <c:otherwise>
    force_rerun_state = false;
  </c:otherwise>
</c:choose>
  }

  if (reset_state != undefined) {
  } else {
    /* default simulation reset when page is first shown */
<c:choose>
  <c:when test="${ma_config_reset}">
    reset_state = true;
  </c:when>
  <c:otherwise>
    reset_state = false;
  </c:otherwise>
</c:choose>
  }

  /* ]]> */
</script>

<div class="text" style="height: 45px;">
  <div class="cs_float_left">
    <div class="cs_clearing">
      <label for="force_rerun_on_off"
             title="<spring:message code="extruder.force_rerun_on_off_desc" />"><spring:message code="extruder.force_rerun_on_off" /></label>
      <span class="cs_float_right">
        <input id="force_rerun_on_off"
               name="force_rerun"
               type="checkbox" />
      </span>
    </div>
  </div>
  <div class="cs_float_right" style="margin-right: 5px;">
    <div class="cs_clearing">
      <label for="reset_on_off"
             title="<spring:message code="extruder.reset_on_off_desc" />"><spring:message code="extruder.reset_on_off" /></label>
      <span class="cs_float_right">
        <input id="reset_on_off"
               name="reset"
               type="checkbox" />
      </span>
    </div>
  </div>
  <div class="cs_clearing">
    <spring:message code="extruder.options_experimental" htmlEscape="false" />.
  </div>
</div>

<script type="text/javascript">
  /* <![CDATA] */

  jQuery(document).ready(
    function() {
      /* js vars defined in resources/js/index/mb/reset-rerun.js */
      selector_rerun = jQuery('#force_rerun_on_off');
      selector_reset = jQuery('#reset_on_off');

      /* Set initial select option appearance */
      (force_rerun_state != undefined && force_rerun_state) ? selector_rerun.prop('checked', true) : selector_rerun.prop('checked', false); 
      (reset_state != undefined && reset_state) ? selector_reset.prop('checked', true) : selector_reset.prop('checked', false); 

      /* Listen for change */
      selector_rerun.change(function() {
        force_rerun_state = selector_rerun.is(':checked');
      });
      selector_reset.change(function() {
        reset_state = selector_reset.is(':checked');
      });
    });
  /* ]]> */
</script>