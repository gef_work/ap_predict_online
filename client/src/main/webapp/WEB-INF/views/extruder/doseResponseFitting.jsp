<%@taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">
  /* <![CDATA] */
  <%-- By default set the state values to default configured values --%>
  if (typeof drf_rounding_state === 'undefined') {
    drf_rounding_state = <c:out value="${ma_config_drf_rounded}" />;
  }
  if (typeof drf_hill_min_state === 'undefined') {
    drf_hill_min_state = '<c:out value="${ma_config_drf_hill_min}" />';
  }
  if (typeof drf_hill_max_state === 'undefined') {
    drf_hill_max_state = '<c:out value="${ma_config_drf_hill_max}" />';
  }
  if (typeof drf_strategy_state === 'undefined') {
    drf_strategy_state = '<c:out value="${ma_config_drf}" />';
  }
<c:forEach items="${ma_config_hill_fitting_strategy}"
           var="drfStrategyHillFitting">
  drf_strategy_hill_fitting['<c:out value="${drfStrategyHillFitting.key}" />'] = <c:out value="${drfStrategyHillFitting.value}" />;
</c:forEach>
  /* ]]> */
</script>
<div class="text" style="height: 110px;">
<c:choose>
  <c:when test="${not empty ma_config_hill_fitting_strategy}">
  <table>
    <tr>
      <td><spring:message code="extruder.strategy" /></td>
      <td> : </td>
      <td>
        <select id="fitting_option"
                name="doseResponseFitting">
  <c:forEach items="${ma_config_hill_fitting_strategy}"
             var="drfStrategyHillFitting">
          <option value="<c:out value="${drfStrategyHillFitting.key}" />"><c:out value="${drfStrategyHillFitting.key}" /></option>
  </c:forEach>
        </select>
      </td>
    </tr>
    <tr id="hill_minmax_row">
      <td>
        <label for="hill_minmax"
               title="">Hill Min/Max?</label>
      </td>
      <td> : </td>
      <td>
        <input id="hill_minmax"
               type="checkbox" />
      </td>
    </tr>
    <tr id="hill_row">
      <td><spring:message code="extruder.hill_coefficient" /> </td>
      <td> : </td>
      <td>
        <label for="hill_min"
               title="<spring:message code="extruder.hill_min_desc" />"><spring:message code="cs_general.min" /></label>
        <input id="hill_min"
               maxlength="4"
               name="doseResponseFittingHillMin"
               size="4" />
        &nbsp;
        <label for="hill_max"
               title="<spring:message code="extruder.hill_max_desc" />"><spring:message code="cs_general.max" /></label>
        <input id="hill_max"
               maxlength="4"
               name="doseResponseFittingHillMax"
               size="4" />
      </td>
    </tr>
    <tr>
      <td>
        <label for="rounding_on_off"
               title="<spring:message code="extruder.rounding_on_off_desc" />"><spring:message code="extruder.rounding_on_off" /></label>
      </td>
      <td> : </td>
      <td>
        <input id="rounding_on_off"
               name="doseResponseFittingRounded"
               type="checkbox" />
      </td>
    </tr>
  </table>
  </c:when>
  <c:otherwise>
    No Dose-Response fitting performed.
  </c:otherwise>
</c:choose>
</div>

<script type="text/javascript">
  /* <![CDATA] */
  jQuery(document).ready(
    function() {
      var rounding_on_off_el = jQuery('#rounding_on_off');
      var hill_min_el = jQuery('#hill_min');
      var hill_max_el = jQuery('#hill_max');
      var fitting_option_el = jQuery('#fitting_option');
      var hill_minmax_el = jQuery('#hill_minmax');

      /* These vars are defined and used in dose-response-fitting.js */
      hill_row_el = jQuery('#hill_row');
      hill_minmax_row_el = jQuery('#hill_minmax_row');

      if (drf_rounding_state != undefined && !drf_rounding_state) {
        rounding_on_off_el.prop('checked', false);
      } else {
        rounding_on_off_el.prop('checked', true);
      }

      if (drf_strategy_state != undefined) {
        fitting_option_el.val(drf_strategy_state);
        show_hill_minmax_by_option();
      }
      if (drf_hill_minmax_state != undefined) {
        hill_minmax_el.prop('checked', drf_hill_minmax_state);
      }

      rounding_on_off_el.change(function() {
        drf_rounding_state = rounding_on_off_el.is(':checked');
      });

      /* React to change of dose-response-fitting strategy. */
      fitting_option_el.change(function() {
        drf_strategy_state = fitting_option_el.val();
        show_hill_minmax_by_option();
      });

      /* If Hill min/max element changes show/hide the values row. */
      hill_minmax_el.change(function() {
        drf_hill_minmax_state = hill_minmax_el.is(':checked');
        show_hill_row(hill_minmax_row_el.is(':visible'));
      });

      hill_min_el.val(drf_hill_min_state);
      hill_max_el.val(drf_hill_max_state);

      hill_min_el.focusout(function() { drf_hill_min_state = hill_min_el.val(); });
      hill_max_el.focusout(function() { drf_hill_max_state = hill_max_el.val(); });
    });
  /* ]]> */
</script>