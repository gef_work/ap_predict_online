<%@page session="false" %>
<%
  response.setHeader("Expires", "Sat, 6 May 1995 12:00:00");
  response.setHeader("Cache-control", "private, no-cache, no-store, must-revalidate");
  response.setHeader("Cache-control", "post-check=0, precheck=0");
  response.setHeader("Pragma", "no-cache");
  response.setDateHeader("Expires", 0);

  response.setCharacterEncoding("utf-8");
  response.setContentType("text/html;charset=UTF-8");
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="uk.ac.ox.cs.epsrc.client.ClientIdentifiers" %>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
<c:choose>
  <c:when test="${not empty ma_assay && not empty ma_ion_channel}">
    <title><spring:message code="provenance.input_data" /> : <c:out value="${ma_assay}" />/<c:out value="${ma_ion_channel}" /></title>
  </c:when>
  <c:otherwise>
    <title><spring:message code="provenance.page_title_default" /></title>
  </c:otherwise>
</c:choose>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/jquery-1.8.3.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/ui/jquery-ui-1.9.2.custom.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/d3/d3.v3.min.js" />" charset="utf-8"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/general.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/provenance.js" />"></script>
    <link href="<c:url value="/resources/css/client.css" />" media="all" rel="stylesheet" type="text/css" />
    <link href="<c:url value="/resources/css/client_provenance.css" />" media="all" rel="stylesheet" type="text/css" />
    <link href="<c:url value="/resources/css/site/client-shared-site.css" />" media="all" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
      /* <![CDATA] */
      var KEY_JSON = '<%= ClientIdentifiers.KEY_JSON %>';
      var KEY_EXCEPTION = '<%= ClientIdentifiers.KEY_EXCEPTION %>';
      var KEY_TEXT = '<%= ClientIdentifiers.KEY_INFORMATION_TEXT %>';
      var KEY_LEVEL = '<%= ClientIdentifiers.KEY_INFORMATION_LEVEL %>';

      var PFX_LEVEL_CLASS = 'level_';

      var MODEL_ATTRIBUTE_PROVENANCE = '<%= ClientIdentifiers.MODEL_ATTRIBUTE_PROVENANCE %>';
      var URL_PREFIX_AJAX_PROVENANCE = '${pageContext.request.contextPath}/<%= ClientIdentifiers.URL_PREFIX_AJAX_PROVENANCE %>';

      /* Default min information level when page is first shown */
      var LEVEL_MIN = '${ma_config_min_info_level}' * 1;
      /* Default max information level when page is first shown */
      var LEVEL_MAX = '${ma_config_max_info_level}' * 1;

      jQuery(document).ready(
        function() {
          initialise_concentration_points();

          chart = jQuery('#chart');
          chart_div_w = chart.width();
          chart_div_h = chart.height();

          ch_margin = {top: 20, right: 20, bottom: 35, left: 50 };
          ch_width = chart_div_w - ch_margin.left - ch_margin.right;
          ch_height = chart_div_h - ch_margin.top - ch_margin.bottom;

          ch_x = d3.scale.log()
//                   .domain([1,1000])
                   .range([0, ch_width]);

          ch_y = d3.scale.linear()
                   .range([ch_height, 0]);

          ch_xAxis = d3.svg.axis()
                       .scale(ch_x)
                       .orient("bottom")
                       .ticks(6, 1);

          ch_yAxis = d3.svg.axis()
                       .scale(ch_y)
                       .orient("left");

          ch_line = d3.svg.line()
                      .x(function(d) { return ch_x(d.dose); })
                      .y(function(d) { return ch_y(d.response); });

          ch_svg = d3.select("#chart").append("svg")
                     .attr("width", ch_width + ch_margin.left + ch_margin.right)
                     .attr("height", ch_height + ch_margin.top + ch_margin.bottom)
                     .append("g")
                     .attr("transform", "translate(" + ch_margin.left + "," + ch_margin.top + ")");

          tree = d3.layout.tree()
                   .size([h, w]);

          diagonal = d3.svg.diagonal()
                       .projection(function(d) { return [d.y, d.x]; });

          vis = d3.select("#svg_area").append("svg:svg")
                  .attr("width", w + m[1] + m[3])
                  .attr("height", h + m[0] + m[2])
                  .append("svg:g")
                  .attr("transform", "translate(" + m[3] + "," + m[0] + ")");

          /* Make the AJAX call for provenance */
<c:choose>
  <c:when test="${not empty ma_assay && not empty ma_ion_channel}">
          ajax_for_provenance(URL_PREFIX_AJAX_PROVENANCE + '<c:out value='${ma_simulation_id}' />/<c:out value='${ma_assay}' />/<c:out value='${ma_ion_channel}' />');
  </c:when>
  <c:otherwise>
          ajax_for_provenance(URL_PREFIX_AJAX_PROVENANCE + <c:out value='${ma_simulation_id}' />);
  </c:otherwise>
</c:choose>
          show_hide_level(LEVEL_MIN, LEVEL_MAX);
        }
      );
      /* ]]> */
    </script>
  </head>
  <body>
    <div id="svg">
      <div>
        <span class="bordered cs_rounded_5 white" style="padding: 1px;"><spring:message code="provenance.info_svg_area" /></span>
        <span id="msg" style="display: none;"><spring:message code="cs_general.processing" /> ....</span>
        <div id="svg_area" class="bordered cs_rounded_5 white">
        </div>
        <div id="chart" class="bordered cs_rounded_5 white">
          <span id="chart_info" style="padding: 5px;"><spring:message code="provenance.info_chart" /></div>
        </div>
        <div id="raw_data" class="bordered cs_rounded_5 white">
          <span style="padding: 5px"><spring:message code="provenance.info_raw_data" /></span>
        </div>
      </div>
      <div id="provenance"></div>
      <div id="dose_response" style="display: none;"></div>
    </div>
  </body>
</html>
