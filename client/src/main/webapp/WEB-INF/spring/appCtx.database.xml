<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd">

  <description>
    <![CDATA[
    client entity and transaction management beans.
    ]]>
  </description>

  <bean id="clientEntityManagerFactory"
        autowire="byName"
        class="org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean">
    <description>
      <![CDATA[
      FactoryBean that creates a JPA EntityManagerFactory according to JPA's standard container
        bootstrap contract. This is the most powerful way to set up a shared JPA
        EntityManagerFactory in a Spring application context; the EntityManagerFactory can then be
        passed to JPA-based DAOs via dependency injection. Note that switching to a JNDI lookup or
        to a LocalEntityManagerFactoryBean  definition is just a matter of configuration!
      As with LocalEntityManagerFactoryBean, configuration settings are usually read in from a 
        META-INF/persistence.xml config file, residing in the class path, according to the general
        JPA configuration contract. However, this FactoryBean is more flexible in that you can 
        override the location of the persistence.xml file, specify the JDBC DataSources to link 
        to, etc. Furthermore, it allows for pluggable class instrumentation through Spring's 
        LoadTimeWeaver abstraction, instead of being tied to a special VM agent specified on JVM
        startup.
      Internally, this FactoryBean parses the persistence.xml file itself and creates a 
        corresponding PersistenceUnitInfo object (with further configuration merged in, such as
        JDBC DataSources and the Spring LoadTimeWeaver), to be passed to the chosen JPA 
        PersistenceProvider. This corresponds to a local JPA container with full support for the
        standard JPA container contract.
      The exposed EntityManagerFactory object will implement all the interfaces of the underlying
        native EntityManagerFactory returned by the PersistenceProvider, plus the 
        EntityManagerFactoryInfo interface which exposes additional metadata as assembled by this
        FactoryBean.
      ]]>
    </description>
    <property name="dataSource" ref="clientSharedDataSource" />
    <property name="jpaVendorAdapter">
      <bean class="org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter" />
    </property>
    <!-- Using an unconventional location (i.e. not META-INF/persistence.xml) to emphasise usage -->
    <property name="persistenceXmlLocation"
              value="classpath:META-INF/data/client-persistence.xml" />
  </bean>

  <bean id="clientTransactionManager"
        autowire="byName"
        class="org.springframework.orm.jpa.JpaTransactionManager">
    <description>
      <![CDATA[
      PlatformTransactionManager implementation for a single JPA EntityManagerFactory. Binds a
        JPA EntityManager from the specified factory to the thread, potentially allowing for one
        thread-bound EntityManager per factory. SharedEntityManagerCreator and JpaTemplate are
        aware of thread-bound entity managers and participate in such transactions automatically.
        Using either is required for JPA access code supporting this transaction management 
        mechanism.
      This transaction manager is appropriate for applications that use a single JPA
        EntityManagerFactory for transactional data access. JTA (usually through 
        JtaTransactionManager) is necessary for accessing multiple transactional resources within
        the same transaction. Note that you need to configure your JPA provider accordingly in 
        order to make it participate in JTA transactions.
      This transaction manager also supports direct DataSource access within a transaction (i.e. 
        plain JDBC code working with the same DataSource). This allows for mixing services which
        access JPA and services which use plain JDBC (without being aware of JPA)! Application 
        code needs to stick to the same simple Connection lookup pattern as with 
        DataSourceTransactionManager (i.e. DataSourceUtils.getConnection(javax.sql.DataSource) or
        going through a TransactionAwareDataSourceProxy). Note that this requires a 
        vendor-specific JpaDialect to be configured.
      Note: To be able to register a DataSource's Connection for plain JDBC code, this instance
        needs to be aware of the DataSource (setDataSource(javax.sql.DataSource)). The given 
        DataSource should obviously match the one used by the given EntityManagerFactory. This
        transaction manager will autodetect the DataSource used as known connection factory of the
        EntityManagerFactory, so you usually don't need to explicitly specify the "dataSource" 
        property.
      On JDBC 3.0, this transaction manager supports nested transactions via JDBC 3.0 Savepoints.
        The AbstractPlatformTransactionManager.setNestedTransactionAllowed(boolean)
        "nestedTransactionAllowed"} flag defaults to "false", though, as nested transactions will
        just apply to the JDBC Connection, not to the JPA EntityManager and its cached objects. 
        You can manually set the flag to "true" if you want to use nested transactions for JDBC
        access code which participates in JPA transactions (provided that your JDBC driver 
        supports Savepoints). Note that JPA itself does not support nested transactions! Hence,
        do not expect JPA access code to semantically participate in a nested transaction.
      ]]> 
    </description>
    <property name="entityManagerFactory" ref="clientEntityManagerFactory" />
    <property name="dataSource" ref="clientSharedDataSource" />
  </bean>

  <tx:annotation-driven transaction-manager="clientTransactionManager" />

</beans>


