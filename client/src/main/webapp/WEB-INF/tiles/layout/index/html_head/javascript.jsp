<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers,
                uk.ac.ox.cs.epsrc.client.ClientIdentifiers,
                uk.ac.ox.cs.epsrc.client.value.ExperimentalDataVO,
                uk.ac.ox.cs.epsrc.client.value.type.InputValueDisplay" %>
<sec:csrfMetaTags />
<c:set var="input_value_display_any_modifier" value="<%= InputValueDisplay.meanOfpIC50FromAnyModifier %>" />
<c:set var="input_value_display_equality_modifier" value="<%= InputValueDisplay.meanOfpIC50FromEqualityModifierOnly %>" />
    <script type="text/javascript">
      /* <![CDATA] */
      "use strict";

      var KEY_JSON = '<%= ClientIdentifiers.KEY_JSON %>';
      var KEY_EXCEPTION = '<%= ClientIdentifiers.KEY_EXCEPTION %>';

      var KEY_EXPERIMENTAL_DATA = '<%= ClientIdentifiers.KEY_EXPERIMENTAL_DATA %>';
      var KEY_EXPERIMENTAL_CONC = '<%= ExperimentalDataVO.KEY_EXPERIMENTAL_CONC %>';
      var KEY_EXPERIMENTAL_PCT_CHANGE_MEAN = '<%= ExperimentalDataVO.KEY_EXPERIMENTAL_PCT_CHANGE_MEAN %>';
      var KEY_EXPERIMENTAL_PCT_CHANGE_SEM = '<%= ExperimentalDataVO.KEY_EXPERIMENTAL_PCT_CHANGE_SEM %>';

      var KEY_INPUTVAL_GROUP_LEVEL = '<%= ClientIdentifiers.KEY_INPUTVAL_GROUP_LEVEL %>';
      var KEY_INPUTVAL_GROUP_NAME = '<%= ClientIdentifiers.KEY_INPUTVAL_GROUP_NAME %>';
      var KEY_INPUTVAL_HILLS = '<%= ClientIdentifiers.KEY_INPUTVAL_HILLS %>';
      var KEY_INPUTVAL_INPUT_VALUES = '<%= ClientIdentifiers.KEY_INPUTVAL_INPUT_VALUES %>';
      var KEY_INPUTVAL_ION_CHANNEL_NAME = '<%= ClientIdentifiers.KEY_INPUTVAL_ION_CHANNEL_NAME %>';
      var KEY_INPUTVAL_ORIGINALS = '<%= ClientIdentifiers.KEY_INPUTVAL_ORIGINALS %>';
      var KEY_INPUTVAL_PIC50S = '<%= ClientIdentifiers.KEY_INPUTVAL_PIC50S %>';
      var KEY_INPUTVAL_SOURCE_ASSAY_NAME = '<%= ClientIdentifiers.KEY_INPUTVAL_SOURCE_ASSAY_NAME %>';
      var KEY_INPUTVAL_STRATEGY_ORDERS = '<%= ClientIdentifiers.KEY_INPUTVAL_STRATEGY_ORDERS %>';

      /* these values are defined in the NoSQL definitions (OrientDAOImpl.java) */
      var KEY_INFORMATION_LEVEL = 'level';
      var KEY_INFORMATION_TIMESTAMP = 'timestamp';

      var KEY_INFORMATION_JOB_ID = '<%= ClientIdentifiers.KEY_INFORMATION_JOB_ID %>';
      var KEY_INFORMATION_PROGRESS = '<%= ClientIdentifiers.KEY_INFORMATION_PROGRESS %>';
      var KEY_INFORMATION_PROGRESS_JOBS = '<%= ClientIdentifiers.KEY_INFORMATION_PROGRESS_JOBS %>';
      var KEY_INFORMATION_TEXT = '<%= ClientIdentifiers.KEY_INFORMATION_TEXT %>';

      /* shared between information data and JOB_DETAILS data */
      var KEY_SHARED_GROUP_NAME = 'groupName';
      var KEY_SHARED_GROUP_LEVEL = 'groupLevel'; 
      var KEY_SHARED_PACING_FREQUENCY = 'pacingFrequency';

      var KEY_RESULTS_JOB_ID = '<%= ClientIdentifiers.KEY_RESULTS_JOB_ID %>';
      var KEY_RESULTS_MESSAGES = '<%= ClientIdentifiers.KEY_RESULTS_MESSAGES %>';
      var KEY_RESULTS_DELTA_APD90_PERCENTILE_NAMES = '<%= ClientIdentifiers.KEY_RESULTS_DELTA_APD90_PERCENTILE_NAMES %>';
      var KEY_RESULTS_CONCENTRATION_DATA = '<%= ClientIdentifiers.KEY_RESULTS_CONCENTRATION_DATA %>';
      var KEY_RESULTS_DATA = '<%= ClientIdentifiers.KEY_RESULTS_DATA %>';
      var KEY_RESULTS_DELTA_APD90 = '<%= ClientIdentifiers.KEY_RESULTS_DELTA_APD90 %>';
      var KEY_RESULTS_UPSTROKE_VELOCITY = '<%= ClientIdentifiers.KEY_RESULTS_UPSTROKE_VELOCITY %>';
      var KEY_RESULTS_TIMES = '<%= ClientIdentifiers.KEY_RESULTS_TIMES %>';
      var KEY_RESULTS_VOLTAGES = '<%= ClientIdentifiers.KEY_RESULTS_VOLTAGES %>';
      var KEY_RESULTS_QNET = '<%= ClientIdentifiers.KEY_RESULTS_QNET %>';

      var MODEL_ATTRIBUTE_EXPERIMENTAL_DATA = '<%= ClientIdentifiers.MODEL_ATTRIBUTE_EXPERIMENTAL_DATA %>';
      var MODEL_ATTRIBUTE_CONFIG_INPUT_VALUE_DISPLAY = '<c:out value="${ma_config_input_value_display}" />';
      var MODEL_ATTRIBUTE_CONFIG_SIM_REQ_POLLING = <c:out value="${ma_config_sim_req_polling}" />;
      var MODEL_ATTRIBUTE_INPUT_VALUES = '<%= ClientIdentifiers.MODEL_ATTRIBUTE_INPUT_VALUES %>';
      var MODEL_ATTRIBUTE_PROGRESS_OVERALL = '<%= ClientIdentifiers.MODEL_ATTRIBUTE_PROGRESS_OVERALL %>';
      var MODEL_ATTRIBUTE_QNET_AVAILABLE = '<%= ClientSharedIdentifiers.MODEL_ATTRIBUTE_QNET_AVAILABLE %>';
      var MODEL_ATTRIBUTE_RESULTS = '<%= ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS %>';

      var INPUT_VALUE_DISPLAY_ANY_MODIFIER = '<c:out value="${input_value_display_any_modifier}" />';
      var INPUT_VALUE_DISPLAY_EQUALITY_MODIFIER = '<c:out value="${input_value_display_equality_modifier}" />';

      var JSP_PROVENANCE = '<%= ClientIdentifiers.JSP_PROVENANCE %>/';

      var DIV_RESULTS_TABBED;

      /* information progress */
      var PFX_FLAG = 'flag_';
      var PFX_LEVEL_CLASS = 'level_';
      var PFX_PROGRESS_JOB = 'progress_job_';

      var URL_PREFIX_AJAX_EXPERIMENTAL = '${pageContext.request.contextPath}/<%= ClientIdentifiers.URL_PREFIX_AJAX_EXPERIMENTAL %>';
      var URL_PREFIX_AJAX_INPUTVALUES = '${pageContext.request.contextPath}/<%= ClientIdentifiers.URL_PREFIX_AJAX_INPUTVALUES %>';
      var URL_PREFIX_AJAX_JOB_DIAGNOSTICS = '${pageContext.request.contextPath}/<%= ClientIdentifiers.URL_PREFIX_AJAX_JOB_DIAGNOSTICS %>';
      var URL_PREFIX_AJAX_PROGRESS = '${pageContext.request.contextPath}/<%= ClientIdentifiers.URL_PREFIX_AJAX_PROGRESS %>';
      var URL_PREFIX_AJAX_PROVENANCE = '${pageContext.request.contextPath}/<%= ClientIdentifiers.URL_PREFIX_AJAX_PROVENANCE %>';
      var URL_PREFIX_AJAX_RESULTS = '${pageContext.request.contextPath}/<%= ClientIdentifiers.URL_PREFIX_AJAX_RESULTS %>';
      var URL_PREFIX_AJAX_SIMULATION = '${pageContext.request.contextPath}/<%= ClientIdentifiers.URL_PREFIX_AJAX_SIMULATION %>';

      var CURRENT_SIMULATION_ID;

      /* job details */
      var JOB_DETAILS = {};

      var MICRO_TEXT = ' [&micro;M]';
      var CONTROL_LABEL = '0 ' + MICRO_TEXT;
      var DELIMITER = ' @ ';

      var AP_DATASETS = [];
      var EXPERIMENTAL_DATASETS = [];
      var PCT_CHANGE_DATASETS = [];
      var QNET_DATASETS = [];

      <%-- GRAPH_LABEL_? require javascript escaping in case someone uses a ',",\, etc. char in the text
           (but no html escaping because it's going directly into a js string for output) --%>
      var GRAPH_LABEL_CONC_UM = '<spring:message code="cs_results.graphlabel_conc_um" htmlEscape="false" javaScriptEscape="true" />';
      var GRAPH_LABEL_PCT_CHANGE = '<spring:message code="cs_results.graphlabel_delta_apd90" htmlEscape="false" javaScriptEscape="true" />';
      var GRAPH_LABEL_QNET = '<spring:message code="cs_results.graphlabel_qnet" htmlEscape="false" javaScriptEscape="true" />';
      var GRAPH_LABEL_TIME = '<spring:message code="cs_results.graphlabel_time" htmlEscape="false" javaScriptEscape="true" />';
      var GRAPH_LABEL_MS = '<spring:message code="cs_results.graphlabel_ms" htmlEscape="false" javaScriptEscape="true" />';
      var GRAPH_LABEL_MEMBRANE_VOLTAGE = '<spring:message code="cs_results.graphlabel_membrane_voltage" htmlEscape="false" javaScriptEscape="true" />';
      var GRAPH_LABEL_MV = '<spring:message code="cs_results.graphlabel_mv" htmlEscape="false" javaScriptEscape="true" />';
      var GRAPH_LABEL_TIME_MS = GRAPH_LABEL_TIME + ' (' + GRAPH_LABEL_MS + ')';
      var GRAPH_LABEL_MEMBRANE_VOLTAGE_MV = GRAPH_LABEL_MEMBRANE_VOLTAGE + ' (' + GRAPH_LABEL_MV + ')';
      var TEXT_STRATEGY = '<spring:message code="inputvalues.strategy" htmlEscape="false" javaScriptEscape="true" />';
      var TEXT_AT_A_CONCENTRATION_OF = '<spring:message code="cs_results.at_a_conc_of" htmlEscape="false" javaScriptEscape="true" />';
      var TEXT_CONC = '<spring:message code="cs_results.conc" htmlEscape="false" javaScriptEscape="true" />';
      var TEXT_DELTA_APD90 = '<spring:message code="cs_results.delta_apd90" htmlEscape="false" javaScriptEscape="true" />';
      var TEXT_HAS_CHANGED = '<spring:message code="cs_results.has_changed" htmlEscape="false" javaScriptEscape="true" />';
      var TEXT_QNET = '<spring:message code="cs_results.qnet" htmlEscape="false" javaScriptEscape="true" />';
      var TEXT_SHOW_ACTIONPOTENTIAL = '<spring:message code="index.show_actionpotential" htmlEscape="false" javaScriptEscape="true" />';
      var TEXT_SHOW_CONFIDENCE = '<spring:message code="cs_results.show_confidence" htmlEscape="false" javaScriptEscape="true" />';
      var TEXT_VIEW_MESSAGES = '<spring:message code="cs_results.view_messages" htmlEscape="false" javaScriptEscape="true" />';

      var EXPERIMENTAL_LABEL = 'Experimental QT';
      /* Don't show pct change plots for concentrations below this value */
      var MINIMUM_PCT_CHANGE_CONCENTRATION = 0.01;

      /* default min information level when page is first shown */
      var LEVEL_MIN = '${ma_config_min_info_level}' * 1;
      /* default max information level when page is first shown */
      var LEVEL_MAX = '${ma_config_max_info_level}' * 1;

      var DEFAULT_ION_CHANNEL_COLUMN_ORDER = [];
      var ION_CHANNELS = {};
<c:if test="${not empty ma_config_ion_channels}">
      /* {0=[hERG, hERG], 1=[CaV1_2, Calcium 1.2], 2=[NaV1_5, Sodium 1.5], 3=[KCNQ1, KCNQ1]} */
      var config_ion_channels = ${ma_config_ion_channels};
      for (var key in config_ion_channels) {
        var name = config_ion_channels[key][0];
        var desc = config_ion_channels[key][1];
        ION_CHANNELS[name] = desc;
        DEFAULT_ION_CHANNEL_COLUMN_ORDER.push(name);
      }
</c:if>

<c:if test="${not empty ma_config_cell_models}">
      var MODELS = {};
  <c:forEach items="${ma_config_cell_models}"
             var="cellml_model">
    <c:set var="model_name">${fn:replace(cellml_model.name, "'", "\\'")}</c:set>
      MODELS['<c:out value="${cellml_model.identifier}" />'] = 
        { 'id' : '<c:out value="${cellml_model.identifier}" />',
          'name' : '${model_name}',
          'description' : '<c:out value="${cellml_model.description}" />',
          'urlCellML' : '<c:out value="${cellml_model.cellmlURL}" />',
          'urlPaper' : '<c:out value="${cellml_model.paperURL}" />',
          'defaultModel' : <c:out value="${cellml_model.defaultModel}" />,
          'pacingMaxTime' : '<c:out value="${cellml_model.pacingMaxTime}" />' };
    <c:if test="${cellml_model.defaultModel}">
      var MODEL_IDENTIFIER_STATE = '<c:out value="${cellml_model.identifier}" />';
    </c:if>
  </c:forEach>
</c:if>

      var PFX_STRATEGIES = 'strategies_';
      function get_object_key(id) {
        return PFX_STRATEGIES + id;
      }
      var strategy_objects = {};

<c:forEach items="${ma_config_pc50_eval_strategies}"
           var="pc50EvalStrategy"
           varStatus="pc50_eval_strategy">
      strategy_objects[get_object_key('<c:out value="${pc50EvalStrategy.id}" />')] = 
                                     { 'id' : '<c:out value="${pc50EvalStrategy.id}" />',
                                       'defaultInvocationOrder': '<c:out value="${pc50EvalStrategy.defaultInvocationOrder}" />',
                                       'description': '<c:out value="${pc50EvalStrategy.description}" />',
                                       'defaultActive': <c:out value="${pc50EvalStrategy.defaultActive}" /> };
</c:forEach>

      var progress_overall_title = '<spring:message code="progress.overall" />';
      var spinner_location = '<c:url value="/resources/img/spinner.gif" />';
      var CONTENT_DISPLAY_LOCATION = '<c:url value="/resources/img/displayContent.png" />';

      Object.size = function(obj) {
        var size = 0;
        var key;
        for (key in obj) {
          if (obj.hasOwnProperty(key)) size++;
        }
        return size;
      };

      var PCT_CHANGE = {};
      var FLAGGED = {};

      /* "." defined by progress.system_termination_key in progress.properties i18n bundle */
      var SYSTEM_TERMINATION_KEY = '.';
      /* Indicator of whether overall progress has finished arriving (i.e. simulation end) */
      var progress_overall_completed = false;

      var C50_UNITS = {};
<c:forEach items="${ma_c50_units}"
           var="c50_unit"
           varStatus="status">
  <c:if test="${status.index == 0}">
      var C50_UNIT_STATE = '<c:out value="${c50_unit.key}" />';
  </c:if>
      C50_UNITS['<c:out value="${c50_unit.key}" />'] = '<c:out value="${c50_unit.value}" />';
</c:forEach>
      var SHOW_PROVENANCE = true;
      /* ]]> */
    </script>
    <!--[if lte IE 8]>
    <script type="text/javascript">
      /* <![CDATA] */
      /* IE8 and less doesn't handle svg, and the r2d3 and aight javascript alternatives didn't
         seem to help either - so don't show provenance if IE8 or less. */
      var SHOW_PROVENANCE = false;
      /* ]]> */
    </script>
    <![endif]-->

    <script type="text/javascript" src="<c:url value="/resources/js/jquery/validate/jquery.validate-1.14.0.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/general.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/flot/0.8.1/jquery.flot.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/flot/0.8.1/jquery.flot.canvas.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/flot/jquery.flot.axislabels.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/flot/jquery.flot.dashes.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/flot/jquery.flot.fillbetween.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/flot/0.8.1/jquery.flot.errorbars.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/flot/0.8.1/jquery.flot.selection.js" />"></script>

    <script type="text/javascript" src="<c:url value="/resources/js/various/base64.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/various/canvas2image.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/flot/jquery.flot.saveAsImage.js" />"></script>

    <script type="text/javascript" src="<c:url value="/resources/js/cs-flotting.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/cs-qnet.js" />"></script>

    <script type="text/javascript" src="<c:url value="/resources/js/index/experimental.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/index/graph_action_potential.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/index/graph_pct_change.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/index/graph_qnet.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/index/input-values.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/index/mb/dose-response-fitting.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/index/mb/grouping-inheritance.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/index/mb/pc50-evaluation-strategies.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/index/mb/reset-rerun.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/index/progress.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/index/results.js" />"></script>

    <script type="text/javascript" src="<c:url value="/resources/js/mb/jquery.hoverIntent.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/mb/jquery.mb.flipText.js" />"></script>
<c:if test="${not empty ma_config_ion_channels}">
    <script type="text/javascript" src="<c:url value="/resources/js/mb/mbExtruder.js" />"></script>
</c:if>
    <script type="text/javascript" src="<c:url value="/resources/js/provenance.js" />"></script>

    <!--[if lte IE 8]><script type="text/javascript"
                             src="<c:url value="/resources/js/jquer/flot/0.8.1/excanvas.min.js" />"></script><![endif]-->
    <script type="text/javascript">
      /* <![CDATA] */

      var compound_identifiers = [];
      var call_counter = { 'experimental' : 0 };

      var TIMEOUTS = [];
      var INTERVALS = {};

      /**
       * Remove diagnostics information.
       */
      function clear_diagnostics() {
        jQuery('pre[id^=diagnostics_info_]').remove();
        jQuery('pre[id^=diagnostics_output_]').remove();
      }

      /**
       * AJAX job diagnostics info retrieve and display.
       *
       * @param job_id Job identifier.
       */
      function ajax_for_job_diagnostics(job_id) {
        var info_id = 'diagnostics_info_' + job_id;
        var output_id = 'diagnostics_output_' + job_id;

        var displayed = document.getElementById(info_id);
        clear_diagnostics();
        if (displayed) {
          /* Convention is toggle display by clicking on same flag */
          return;
        }

        jQuery.ajax({
          url: URL_PREFIX_AJAX_JOB_DIAGNOSTICS + job_id,
          async: true,
          type: 'GET',
          success: function(response) {
            var job_diagnostics = response['<%= ClientIdentifiers.MODEL_ATTRIBUTE_JOB_DIAGNOSTICS %>'];
            if (job_diagnostics !== undefined) {
              var info = job_diagnostics.info;
              if (info !== undefined && info != null) {
                var regexp = /<command_line>(.*)<\/command_line>/g;
                var match = regexp.exec(info);
                var cmd = match[1];
 
                var pre1 = jQuery('<pre />').attr( { 'id' : info_id } )
                                            .css( { opacity : 0.5 } )
                                            .text(cmd);
                jQuery('body').append(pre1);
              }

              var output = job_diagnostics.output;
              if (output !== undefined && output != null) {
                var pre2 = jQuery('<pre />').attr( { 'id' : output_id } )
                                            .css( { opacity : 0.5 } )
                                            .text(output);
                jQuery('body').append(pre2);
              }
            }
          },
          error: function(xhr, status, error) {
            if (error != undefined) {
              show_js_error(error);
            }
          }
        });
      }

      /**
       * AJAX simulation run request
       *
       * @param compound_identifier Compound identifier.
       */
      function ajax_for_simulation(compound_identifier) {
        var token = $("meta[name='_csrf']").attr('content');
        var header = $("meta[name='_csrf_header']").attr('content');

        // TODO : http://docs.spring.io/spring-security/site/docs/4.0.x/reference/html/csrf.html#csrf-include-csrf-token-ajax
        $(document).ajaxSend(function(e, xhr, options) {
          xhr.setRequestHeader(header, token);
        });

        if (jQuery.inArray(compound_identifier, compound_identifiers) < 0 ) {
          compound_identifiers.push(compound_identifier);
          jQuery('#compound_identifier').autocomplete({
            source: compound_identifiers
          });
        }

        jQuery('#spinner').show();
        clear_diagnostics();
        display_simulation_settings();
        jQuery('#js_debug').empty();
        clear_all_intervals();
        clear_all_timeouts();

        jQuery.ajax({
          url: URL_PREFIX_AJAX_SIMULATION + compound_identifier,
          async: false,   // Important! This is NOT asynchronous to ensure sequential responses!
          type: 'POST',
          data: { '<%= ClientIdentifiers.PARAM_NAME_FORCE_RERUN %>': force_rerun_state,
                  '<%= ClientIdentifiers.PARAM_NAME_RESET %>': reset_state,
                  '<%= ClientIdentifiers.PARAM_NAME_ASSAY_GROUPING %>': grp_state,
                  '<%= ClientIdentifiers.PARAM_NAME_VALUE_INHERITING %>': inh_state,
                  '<%= ClientIdentifiers.PARAM_NAME_WITHIN_GROUPS %>': inh_wth_state,
                  '<%= ClientIdentifiers.PARAM_NAME_BETWEEN_GROUPS %>': inh_btw_state,
                  '<%= ClientIdentifiers.PARAM_NAME_STRATEGIES %>': get_active_strategies(),
                  '<%= ClientIdentifiers.PARAM_NAME_DOSE_RESPONSE_FITTING %>': drf_strategy_state,
                  '<%= ClientIdentifiers.PARAM_NAME_DOSE_RESPONSE_FITTING_ROUNDING %>': drf_rounding_state,
                  '<%= ClientIdentifiers.PARAM_NAME_DOSE_RESPONSE_FITTING_HILL_MINMAX %>': drf_hill_minmax_state,
                  '<%= ClientIdentifiers.PARAM_NAME_DOSE_RESPONSE_FITTING_HILL_MAX %>': drf_hill_max_state,
                  '<%= ClientIdentifiers.PARAM_NAME_DOSE_RESPONSE_FITTING_HILL_MIN %>': drf_hill_min_state,
                  '<%= ClientIdentifiers.PARAM_NAME_CELL_MODEL_IDENTIFIER %>': MODEL_IDENTIFIER_STATE,
                  '<%= ClientIdentifiers.PARAM_NAME_PACING_MAX_TIME %>': MODELS[MODEL_IDENTIFIER_STATE].pacingMaxTime },
          dataType: 'json',
          success: function(response) {
            var compound_identifier = response['<%= ClientIdentifiers.MODEL_ATTRIBUTE_COMPOUND_IDENTIFIER %>'];
            var title = '<spring:message code="index.title" /> ' + compound_identifier;
            if (navigator.appVersion.indexOf('MSIE') == -1) {
              jQuery('title').text(title);
            }
            jQuery('#cs_top_header_title').text(title);

            reset_display();

            var message_error = response['<%= ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR %>'];
            if (message_error != undefined) {
              cs_show_message_error(message_error);
              return;
            }

            CURRENT_SIMULATION_ID = response['<%= ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID %>'];

            call_counter.experimental = 0;
            PCT_CHANGE = {};
            input_data_received = false;

            progress_overall_completed = false;

            var random_number_1 = 221;
            var random_number_2 = 111;
            var random_1_sec_wait = 1000;

            var timeout_progress = MODEL_ATTRIBUTE_CONFIG_SIM_REQ_POLLING + random_number_1;
            var timeout_input_values = timeout_progress + random_number_2;
            var timeout_experimental = timeout_input_values + random_number_2;
            var timeout_results = timeout_experimental + random_number_2;

            var interval_progress = random_1_sec_wait + random_number_1;
            var interval_input_values = interval_progress + random_number_2;
            var interval_results = interval_input_values + random_number_2;

            TIMEOUTS.push(setTimeout(function() {
              ajax_for_progress(CURRENT_SIMULATION_ID);
              var interval_id = setInterval(function() {
                ajax_for_progress(CURRENT_SIMULATION_ID);
              }, interval_progress);
              set_interval('p', interval_id);
            }, timeout_progress));

            TIMEOUTS.push(setTimeout(function() {
              ajax_for_input_values(CURRENT_SIMULATION_ID);
              var interval_id = setInterval(function() {
                ajax_for_input_values(CURRENT_SIMULATION_ID);
              }, interval_input_values);
              set_interval('i', interval_id);
            }, timeout_input_values));

            TIMEOUTS.push(setTimeout(function() {
              ajax_for_experimental(CURRENT_SIMULATION_ID)
            }, timeout_experimental));

            TIMEOUTS.push(setTimeout(function() {
              ajax_for_results(CURRENT_SIMULATION_ID);
              var interval_id = setInterval(function() {
                ajax_for_results(CURRENT_SIMULATION_ID);
              }, interval_results);
              set_interval('r', interval_id);
            }, timeout_results));
          },
          error: function(xhr, status, error) {
            if (error != undefined) {
              show_js_error(error);
            }
          }  
        });

        /* After a single simulation reset/re-run request switch the selectors off (to prevent
           continued reset/re-run submissions) */
        reset_force_rerun();
        reset_reset();
      }

      /* AJAX progress request for a simulation */
      function ajax_for_progress(simulation_id) {
        if (simulation_id != CURRENT_SIMULATION_ID) {
          /* Dont process if call arrives from a non-current simulation's setInterval/setTimeout */
          clear_interval('p', simulation_id);
          return;
        }

        jQuery.ajax({
          url: URL_PREFIX_AJAX_PROGRESS + simulation_id,
          async: true,
          type: 'GET',
          data: {},
          dataType: 'json',
          success: function(response) {
                     if (simulation_id != CURRENT_SIMULATION_ID) {
                       /* Dont process if response is from a previous simulation. */
                       return;
                     }
                     process_progress_response(simulation_id, response);
                   },
          error: function(xhr, status, error) {
                   var make_progress_call = false;
                   if (error != undefined) {
                     if (error == 'timeout') {
                       show_js_error('<spring:message code="error.timeout" /> <spring:message code="progress.retrieval_timeout" />.');
                       make_progress_call = true;
                     } else if (error == 'Service Unavailable') {
                       /* happens if someone's switched the business manager off while the ajax is polling! */
                       show_js_error('<spring:message code="error.service_unavailable" />');
                     } else if (error == '') {
                       /* happens if someone's switched the web server off while the ajax is polling! */
                       show_js_error('<spring:message code="error.website_unavailable" />');
                     } else {
                       show_js_error('<spring:message code="error.sorry" />' + error);
                     }
                   }
                   if (!make_progress_call) {
                     clear_interval('p', simulation_id);
                   }
                 }
        });
      }

      /**
       * AJAX experimental data request for a simulation.
       *
       * @param simulation_id Simulation identifier.
       */
      function ajax_for_experimental(simulation_id) {
        if (simulation_id != CURRENT_SIMULATION_ID) {
          /* Dont process if call arrives from a non-current simulation's setInterval/setTimeout */
          return;
        }
        if (call_counter.experimental > 2) {
          /* Don't call again if we've already called twice */
          return;
        }
        call_counter.experimental++;

        jQuery.ajax({
          url: URL_PREFIX_AJAX_EXPERIMENTAL + simulation_id,
          async: true,
          type: 'GET',
          data: {},
          dataType: 'json',
          success: function(response) {
                     if (simulation_id != CURRENT_SIMULATION_ID) {
                       /* Dont process if response is from a previous simulation. */
                       return;
                     }
                     process_experimental_response(response);
                   },
          error: function(xhr, status, error) {
                   if (error != undefined) {
                     show_js_error('ajax experimental error ' + error);
                     if (error == 'timeout') {
                       if (Object.size(EXPERIMENTAL_DATASETS) == 0) {
                         ajax_for_experimental(simulation_id);
                       }
                     }
                   }
                 }
        });
      }

      /* AJAX results request for a simulation */
      function ajax_for_results(simulation_id) {
        if (simulation_id != CURRENT_SIMULATION_ID) {
          /* Dont process if call arrives from a non-current simulation's setInterval/setTimeout */
          clear_interval('r', simulation_id);
          return;
        }
        jQuery.ajax({
          url: URL_PREFIX_AJAX_RESULTS + simulation_id,
          async: true,
          type: 'GET',
          data: {},
          dataType: 'json',
          success: function(response) {
                     if (simulation_id != CURRENT_SIMULATION_ID) {
                       /* Dont process if response is from a previous simulation. */
                       return;
                     }

                     process_results_response(response);
                     var make_results_call = false;
                     if (!progress_overall_completed) {
                       make_results_call = true;
                     }
                     if (!make_results_call) {
                       clear_interval('r', simulation_id);
                     }
                   },
          error: function(xhr, status, error) {
                   var make_results_call = false;
                   if (error != undefined) {
                     show_js_error('ajax results error ' + error);
                     if (error == 'timeout') {
                       make_results_call = true;
                     }
                   }
                   if (!make_results_call) {
                     clear_interval('r', simulation_id);
                   }
                 }
        });
      }

      /**
       * AJAX input values request for a simulation.
       *
       * @param simulation_id Simulation identifier.
       */
      function ajax_for_input_values(simulation_id) {
        if (simulation_id != CURRENT_SIMULATION_ID) {
          /* Dont process if call arrives from a non-current simulation's setInterval/setTimeout */
          clear_interval('i', simulation_id);
          return;
        }
        jQuery.ajax({
          url: URL_PREFIX_AJAX_INPUTVALUES + simulation_id,
          async: true,
          type: 'GET',
          data: {},
          dataType: 'json',
          success: function(response) {
                     if (simulation_id != CURRENT_SIMULATION_ID) {
                       /* Dont process if response is from a previous simulation. */
                       return;
                     }

                     process_input_values_response(response);
                     var make_input_values_call = false;
                     if (input_data_received) {
                       show_c50_unit(C50_UNIT_STATE);
                     } else {
                       if (!progress_overall_completed) {
                         make_input_values_call = true;
                       }
                     }
                     if (!make_input_values_call) {
                       clear_interval('i', simulation_id);
                     }
                   },
          error: function(xhr, status, error) {
                   if (error != undefined) {
                     show_js_error('ajax input values error ' + error);
                   }
                 }
        });
      }

      /**
       * Display the settings for current simulation.
       */
      function display_simulation_settings() {
        var simulation_settings = jQuery('<span />').css( { 'font-weight' : 'bold' } )
                                                    .html(' <spring:message code="extruder.cellModel" />');
        var pacing_max_time = jQuery('<span />').css( { 'font-weight' : 'bold' } )
                                                .html(' <spring:message code="cs_simulation.maximum_pacing_time" />');

        var model_chosen = MODELS[MODEL_IDENTIFIER_STATE];

        simulation_settings.after(' : ' + model_chosen.name + ' (' + model_chosen.description + ') ');
        var pacing_max_time_text = (model_chosen.pacingMaxTime != '') ?
        model_chosen.pacingMaxTime : '<spring:message code="cs_general.unlimited" />';
        pacing_max_time.after(' : ' + pacing_max_time_text + ' (mins) ');

        simulation_settings.after(pacing_max_time);

        jQuery('#simulation_settings').css( { 'font-size' : 'x-small' } )
                                      .html(simulation_settings);
      }

      jQuery(document).ready(
        function() {
          DIV_RESULTS_TABBED = jQuery('#results_tabbed');

          reset_display();

          var simulation_run_form = jQuery('#simulation_run_form');
          simulation_run_form.validate({
            rules: {
              <%= ClientIdentifiers.PARAM_NAME_COMPOUND_IDENTIFIER %>: {
                required: true,
                minlength: 2
              }
            },
            messages: {
              <%= ClientIdentifiers.PARAM_NAME_COMPOUND_IDENTIFIER %> : {
                required: '<spring:message code="index.form_required" />',
                minlength: '<spring:message code="index.form_min_length" arguments="2" />'
              }
            }
          });

          /* Show/hide progress/provenance based on the information level */
          show_hide_level_progress(LEVEL_MIN, LEVEL_MAX);

          /* Place the ion channel headers into input values table */
          var header = jQuery('#input_data_table_header');
          jQuery.each(DEFAULT_ION_CHANNEL_COLUMN_ORDER, function(index, ion_channel_name) {
            var th = jQuery('<th />').addClass('dr-table-headercell rich-table-headercell')
                                     .attr('title', ION_CHANNELS[ion_channel_name])
                                     .text(ion_channel_name);
            header.append(th);
          });

          /* Simulation form submission event handler */
          simulation_run_form.submit(function(event) {
            var compound_identifier = jQuery('#compound_identifier').val();
            if (!compound_identifier || /^\s*$/.test(compound_identifier)) {
              event.preventDefault();
              return false;
            }
            ajax_for_simulation(compound_identifier);
            event.preventDefault();
          });

          /* Excel report form submission */
          jQuery('#export_form').submit(function(event) {
            var compound_identifier = jQuery('#compound_identifier').val();
            jQuery('input:hidden[name=<%= ClientIdentifiers.PARAM_NAME_COMPOUND_IDENTIFIER %>]').val(compound_identifier);
            jQuery('input:hidden[name=<%= ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID %>]').val(CURRENT_SIMULATION_ID);
          });

<c:if test="${not empty ma_config_ion_channels}">
          /* configuration data received, so show extruder */
          jQuery("#extruder").buildMbExtruder({
            position:"right",
            top:2,
            width:300,
            extruderOpacity:.8,
            textOrientation:"tb",
            onExtOpen:function(){},
            onExtContentLoad:function(){},
            onExtClose:function(){}
          });
          jQuery.fn.changeLabel=function(text) {
            jQuery(this).find(".flapLabel").html(text);
            jQuery(this).find(".flapLabel").mbFlipText();
          };
</c:if>

          jQuery('#input_data_button').click(function() {
            var url = JSP_PROVENANCE + CURRENT_SIMULATION_ID + "/" + LEVEL_MIN + "/" + LEVEL_MAX;
            open_provenance_window(url);
          });
        }
      );

      /**
       * Show the C50 values in the selected units.
       * <p />
       * Also stores the currently visible unit class in global var (so extruder page knows which
       *   select option to consider as selected).
       * 
       * @param Class of unit to show.
       */
      function show_c50_unit(selected_c50_class) {
        jQuery.each(C50_UNITS, function(each_c50_class, each_c50_value) {
          if (each_c50_class != selected_c50_class) {
            jQuery('.' + each_c50_class).hide();
          }
        });
        jQuery('.' + selected_c50_class).show();
        C50_UNIT_STATE = selected_c50_class;
      }

      /* ]]> */
    </script>
