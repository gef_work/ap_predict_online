<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <link href="<c:url value="/resources/css/extruder/extruder.css" />" media="all" rel="stylesheet" type="text/css" />
    <link href="<c:url value="/resources/css/client_graph.css" />" media="all" rel="stylesheet" type="text/css" />
    <link href="<c:url value="/resources/css/client_input-values.css" />" media="all" rel="stylesheet" type="text/css" />
    <style type="text/css">
      .inline_form { display: inline-block; }
      /* the 'error' class is used by the jquery.validate plugin */
      .error {
         color: #f00;
         padding-left: 2px;
         padding-right: 2px;
       }
       .ui-inline-neutral { background-image: url(<c:url value="/resources/css/smoothness/images/ui-icons_222222_256x240.png" />); }
       .ui-inline-red { background-image: url(<c:url value="/resources/img/smoothness/ui-icons_ff0000_256x240.png" />); }
       .ui-inline-amber { background-image: url(<c:url value="/resources/img/smoothness/ui-icons_ff9000_256x240.png" />); }
       .ui-inline-green { background-image: url(<c:url value="/resources/img/smoothness/ui-icons_00ff00_256x240.png" />); }
    </style>
    <!--[if lte IE 8]>
    <style type="text/css">
      .inline_form {
        display: inline;
        margin-right: 2px;
      }
    </style>
    <![endif]--> 
