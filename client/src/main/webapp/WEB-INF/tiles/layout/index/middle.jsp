<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers,
                uk.ac.ox.cs.epsrc.client.ClientIdentifiers,
                uk.ac.ox.cs.epsrc.client.value.type.InputValueDisplay" %>
<c:set var="source_name"><spring:message code="cs_results.source" /></c:set>
<c:set var="input_value_display_any_modifier" value="<%= InputValueDisplay.meanOfpIC50FromAnyModifier %>" />
<c:set var="input_value_display_equality_modifier" value="<%= InputValueDisplay.meanOfpIC50FromEqualityModifierOnly %>" />
<div class="control_panel">
  <span><spring:message code="site.compound_identifier" /></span> :
  <form class="inline_form"
        id="simulation_run_form">
    <input id="compound_identifier"
           name="<%= ClientIdentifiers.PARAM_NAME_COMPOUND_IDENTIFIER %>" 
           size="20"
           type="text" />
    <input name="<spring:message code="index.submit" />"
           type="submit"
           value="<spring:message code="index.submit" />" />
  </form>
  <span id="spinner" style="display: none;">
    <img src="<c:url value="/resources/img/spinner.gif" />" />
    <sup id="spinner_progress"></sup>
  </span>
</div>

<c:if test="${not empty ma_config_ion_channels}">
<div id="extruder" class="{title: 'v ', url:'<%= ClientIdentifiers.URL_PREFIX_EXTRUDER %>mainPanel'}"></div>
</c:if>

<div id="progress"></div>

<%-- % Change section --%>
<div id="graph_area" class="cs_rounded_5">
  <div>
    <span id="simulation_settings" style="padding: 2px;"></span>
    <table>
      <tr>
        <td>
          <span style="font-size: x-small;"
                class="trigger_pct_change_data"><spring:message code="cs_results.zoom_graph" /></span>
          <div id="results_tabbed" class="graph_placeholder_trace">
          </div>
        </td>
        <td>
          <b><c:out value="${source_name}" /></b>
          <div id="pct_change_choices_trace"></div>
          <p />
          <p />&nbsp;&nbsp;
          <input id="pct_change_clear_selection_trace"
                 type="button" class="graph_input trigger_pct_change_data"
                 value="<spring:message code="cs_results.reset_graph" />" style="display: none;"/>
          <p />
          <div class="input_data_div cs_rounded_5">
            <span><spring:message code="inputvalues.title_start" htmlEscape="false" /></span>
            <span class="unit_c50_pIC50" style="display: none;">pIC50 (-log(M))</span>
            <span class="unit_c50_IC50_uM" style="display: none;">IC50 (&micro;M)</span>
            <span class="unit_c50_IC50_nM" style="display: none;">IC50 (nM)</span>
<c:choose>
  <c:when test="${ma_config_input_value_display == input_value_display_equality_modifier}">
            <spring:message code="inputvalues.title_end_equality" htmlEscape="false" /> :
  </c:when>
  <c:when test="${ma_config_input_value_display == input_value_display_any_modifier}">
            <spring:message code="inputvalues.title_end_any" htmlEscape="false" /> :
  </c:when>
  <c:otherwise>
            !Unrecognised input value display configuration option of '<c:out value="${ma_config_input_value_display}" />'!
  </c:otherwise>
</c:choose>
            <table id="input_data_table"
                   class="dr-table rich-table">
              <thead class="dr-table-head">
                <tr id="input_data_table_header" class="dr-table-header rich-table-header">
                  <th class="dr-table-headercell rich-table-headercell"><spring:message code="index.assay_or_group" /></th>
                </tr>
              </thead>
              <tbody>
                <tr id="input_data_table_row" style="display: none;">
                  <td>&nbsp;</td>
                  <td><spring:message code="index.not_available" /></td>
                  <td><spring:message code="index.not_available" /></td>
                  <td><spring:message code="index.not_available" /></td>
                  <td><spring:message code="index.not_available" /></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div style="margin-top: 10px;">
            <input id="input_data_button"
                   title="<spring:message code="index.provenance_title" />"
                   type="button"
                   class="trigger_provenance"
                   value="<spring:message code="index.provenance" />" />
            <form action="<%= ClientIdentifiers.URL_PREFIX_EXCEL %>"
                  id="export_form"
                  method="get"
                  target="_blank"
                  class="inline_form trigger_input_values trigger_pct_change_data"
                  style="display: none;">
              <input type="hidden"
                     name="<%= ClientIdentifiers.PARAM_NAME_COMPOUND_IDENTIFIER %>" />
              <input type="hidden"
                     name="<%= ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID %>" />
              <input type="submit"
                     title="<spring:message code="cs_results.export_as_xls_title" />"
                     value="<spring:message code="cs_results.export_as_xls" />"/>
            </form>
          </div>
          <table style="margin-top: 10px;">
            <tr><td><spring:message code="cs_results.conc" />.&nbsp;</td><td id="point_val_x">0</td><td>&nbsp;&micro;M</td></tr>
            <tr><td><spring:message code="cs_results.change" />&nbsp;</td><td id="point_val_y">0</td><td>&nbsp;%</td></tr>
          </table>
          <%-- overview graph (which may be hidden!) --%>
          <div id="pct_change_overview_trace"
               class="graph_overview_trace graph_colours"></div>
          <%-- colour coordinates --%>
          <p id="pct_change_overview_legend_trace"
             class="graph_pct_change_overview_legend_trace"></p>
        </td>
      </tr>
    </table>
  </div>
  <%-- ActionPotential section --%>
  <div>
    <br />
    <table>
      <tr>
        <td>
          <span style="font-size: x-small;"
                class="trigger_pct_change_data"><spring:message code="cs_results.zoom_graph" /></span>
          <div id="ap_placeholder_trace"
               class="graph_placeholder_trace">
          </div>
        </td>
        <td>
          <%-- tickboxes and reset --%>
          <b><spring:message code="cs_results.simulation_and_concentration" /> (&micro;M)</b>
          <div id="ap_choices_trace"></div>
          <p />
          <p />&nbsp;&nbsp;
          <input id="ap_clear_selection_trace"
                 type="button" class="graph_input trigger_pct_change_data"
                 value="<spring:message code="cs_results.reset_graph" />" style="display: none;"/>
          <%-- overview graph --%>
          <p />
          <div id="ap_overview_trace"
               class="graph_overview_trace graph_colours"></div>
          <%-- color coordinates --%>
          <p id="ap_overview_legend_trace"
             class="graph_ap_overview_legend_trace"></p>
        </td>
      </tr>
    </table>
  </div>

  <ul id="pct_change_default_text"
      style="display: none">
    <li><spring:message code="index.advice_pct_change_1" /></li>
    <li><spring:message code="index.advice_pct_change_2" /></li>
    <li><spring:message code="index.advice_pct_change_3" /></li>
  </ul>

  <ul id="ap_default_text"
      style="display: none">
    <li><spring:message code="index.advice_ap_1" arguments="${source_name}" /></li>
  </ul>
</div>