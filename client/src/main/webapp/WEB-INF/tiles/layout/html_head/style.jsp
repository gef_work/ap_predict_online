<%@taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
    <link href="<c:url value="/resources/css/client.css" />" 
          rel="stylesheet"
          type="text/css" />
    <link href="<c:url value="/resources/css/client-shared.css" />" 
          rel="stylesheet"
          type="text/css" />
    <link href="<c:url value="/resources/css/site/client-shared-site.css" />" 
          rel="stylesheet"
          type="text/css" />
    <link href="<c:url value="/resources/css/smoothness/jquery-ui-1.9.2.custom.min.css" />" 
          rel="stylesheet"
          type="text/css" />