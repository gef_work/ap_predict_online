<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <script src="<c:url value="/resources/js/jquery/jquery-1.8.3.min.js" />"
            type="text/javascript"></script>
    <script src="<c:url value="/resources/js/jquery/ui/jquery-ui-1.9.2.custom.min.js" />"
            type="text/javascript"></script>
    <script type="text/javascript">
     /* <![CDATA] */

     var debug_div;
     function debug(text) {
       var span = jQuery('<span>').text(text);
       debug_div.prepend(span);
     }
     function debug_object(incoming) {
      for (var key in incoming) {
         var val = incoming[key];
         debug('\'' + key + '\' = \'' + val + '\'');
         /*
         for (var prop in val) {
           if (val.hasOwnProperty(prop)) {
             debug('key ' + key + ' val ' + val + ' prop ' + prop);
           }
         }
         */
       }
     }

     var js_error_div;
     /* Hide javascript errors */
     function hide_js_error() {
       js_error_div.hide();
     }
     /* Show javascript errors */
     function show_js_error(text) {
       js_error_div.text(text).show();
       setTimeout(function() { hide_js_error(); }, 2000);
     }

     jQuery(document).tooltip();

     jQuery(document).ready(
       function() {
         debug_div = jQuery('#js_debug');
         js_error_div = jQuery('#js_error');
       }
     );

     /* ]]> */
    </script>
