/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.value.config;

import static org.easymock.EasyMock.createStrictControl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit test the business manager configurations value object.
 *
 * @author geoff
 */
public class BusinessManagerConfigurationsVOTest {

  private boolean dummyDefaultForceReRun;
  private boolean dummyDefaultReset;
  private boolean dummyDefaultAssayGrouping;
  private boolean dummyDefaultValueInheriting;
  private boolean dummyDefaultBetweenGroups;
  private boolean dummyDefaultWithinGroups;
  private BusinessManagerConfigurationsVO bmc;
  private DefaultDoseResponseFittingConfigurationsVO mockDoseResponseConfigurations;
  private IMocksControl mocksControl;
  private int dummyDefaultSimulationRequestProcessingPolling;
  private List<CellModelVO> dummyCellModels;
  private List<PC50EvaluationStrategyConfigVO> dummyDefaultPC50EvaluationStrategies;
  private Map<Short, String> dummyAssays;
  private Map<Short, String> dummyAssayGroups;
  private Map<Short, Vector<String>> dummyIonChannels;
  private short dummyDefaultMinInformationLevel;
  private short dummyDefaultMaxInformationLevel;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
  }

  @Test
  public void testConstructor() {
    bmc = new BusinessManagerConfigurationsVO(dummyDefaultForceReRun, dummyDefaultReset,
                                              dummyDefaultAssayGrouping, dummyDefaultValueInheriting,
                                              dummyDefaultBetweenGroups, dummyDefaultWithinGroups,
                                              dummyDefaultMinInformationLevel,
                                              dummyDefaultMaxInformationLevel,
                                              dummyDefaultPC50EvaluationStrategies, dummyAssays,
                                              dummyAssayGroups, dummyIonChannels,
                                              mockDoseResponseConfigurations, dummyCellModels,
                                              dummyDefaultSimulationRequestProcessingPolling);
    assertNotNull(bmc.getAssayGroups());
    assertNotNull(bmc.getAssays());
    assertNotNull(bmc.getCellModels());
    assertEquals(0, bmc.getDefaultMaxInformationLevel());
    assertEquals(0, bmc.getDefaultMinInformationLevel());
    assertNotNull(bmc.getDefaultPC50EvaluationStrategies());
    assertEquals(0, bmc.getDefaultSimulationRequestProcessingPolling());
    assertNull(bmc.getDoseResponseConfigurations());
    assertNotNull(bmc.getIonChannels());
    assertFalse(bmc.isDefaultAssayGrouping());
    assertFalse(bmc.isDefaultBetweenGroups());
    assertFalse(bmc.isDefaultForceReRun());
    assertFalse(bmc.isDefaultReset());
    assertFalse(bmc.isDefaultValueInheriting());
    assertFalse(bmc.isDefaultWithinGroups());

    dummyDefaultForceReRun = true;
    dummyDefaultReset = true;
    dummyDefaultAssayGrouping = true;
    dummyDefaultValueInheriting = true;
    dummyDefaultBetweenGroups = true;
    dummyDefaultWithinGroups = true;
    mockDoseResponseConfigurations = mocksControl.createMock(DefaultDoseResponseFittingConfigurationsVO.class);
    dummyDefaultSimulationRequestProcessingPolling = 3;
    dummyCellModels = new ArrayList<CellModelVO>();
    final CellModelVO mockCellModelVO = mocksControl.createMock(CellModelVO.class);
    dummyCellModels.add(mockCellModelVO);
    dummyDefaultPC50EvaluationStrategies = new ArrayList<PC50EvaluationStrategyConfigVO>();
    final PC50EvaluationStrategyConfigVO mockPC50EvaluationStrategyConfigVO = mocksControl.createMock(PC50EvaluationStrategyConfigVO.class);
    dummyDefaultPC50EvaluationStrategies.add(mockPC50EvaluationStrategyConfigVO);
    dummyAssays = new HashMap<Short, String>();
    final Short dummyAssayKey = 4;
    final String dummyAssayValue = "dummyAssayValue";
    dummyAssays.put(dummyAssayKey, dummyAssayValue);
    dummyAssayGroups = new HashMap<Short, String>();
    final Short dummyAssayGroupKey = 5;
    final String dummyAssayGroupValue = "dummyAssayGroupValue";
    dummyAssayGroups.put(dummyAssayGroupKey, dummyAssayGroupValue);
    final Short dummyIonChannelsKey = 6;
    final Vector<String> dummyIonChannelsValues = new Vector<String>();
    final String dummyIonChannelsValue = "dummyIonChannelsValueValue";
    dummyIonChannelsValues.add(dummyIonChannelsValue);
    dummyIonChannels = new HashMap<Short, Vector<String>>();
    dummyIonChannels.put(dummyIonChannelsKey, dummyIonChannelsValues);
    dummyDefaultMaxInformationLevel = 7;
    dummyDefaultMinInformationLevel = 8;

    bmc = new BusinessManagerConfigurationsVO(dummyDefaultForceReRun, dummyDefaultReset,
                                              dummyDefaultAssayGrouping, dummyDefaultValueInheriting,
                                              dummyDefaultBetweenGroups, dummyDefaultWithinGroups,
                                              dummyDefaultMinInformationLevel,
                                              dummyDefaultMaxInformationLevel,
                                              dummyDefaultPC50EvaluationStrategies, dummyAssays,
                                              dummyAssayGroups, dummyIonChannels,
                                              mockDoseResponseConfigurations, dummyCellModels,
                                              dummyDefaultSimulationRequestProcessingPolling);
    assertEquals(dummyAssayGroups, bmc.getAssayGroups());
    assertEquals(dummyAssays, bmc.getAssays());
    assertEquals(dummyCellModels, bmc.getCellModels());
    assertEquals(dummyDefaultMaxInformationLevel, bmc.getDefaultMaxInformationLevel());
    assertEquals(dummyDefaultMinInformationLevel, bmc.getDefaultMinInformationLevel());
    assertEquals(dummyDefaultPC50EvaluationStrategies, bmc.getDefaultPC50EvaluationStrategies());
    assertEquals(dummyDefaultSimulationRequestProcessingPolling,
                 bmc.getDefaultSimulationRequestProcessingPolling());
    assertNotNull(bmc.getDoseResponseConfigurations());
    assertEquals(dummyIonChannels, bmc.getIonChannels());
    assertTrue(bmc.isDefaultAssayGrouping());
    assertTrue(bmc.isDefaultBetweenGroups());
    assertTrue(bmc.isDefaultForceReRun());
    assertTrue(bmc.isDefaultReset());
    assertTrue(bmc.isDefaultValueInheriting());
    assertTrue(bmc.isDefaultWithinGroups());
  }
  
}