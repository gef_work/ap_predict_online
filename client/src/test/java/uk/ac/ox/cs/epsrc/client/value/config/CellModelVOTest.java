/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.value.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import org.junit.Test;

/**
 * Unit test CellML model value object.
 *
 * @author geoff
 */
//This is a replica of the CellModelVOTest in the business-manager component
public class CellModelVOTest {

  @Test
  public void testConstructor() {
    short dummyIdentifier = 0;
    String dummyName = null;
    String dummyDescription = null;
    String dummyCellMLURL = null;
    String dummyPaperURL = null;
    boolean dummyDefaultModel = false;
    BigDecimal dummyPacingMaxTime = null;

    CellModelVO cellModel = null;
    try {
      cellModel = new CellModelVO(dummyIdentifier, dummyName, dummyDescription, dummyCellMLURL,
                                  dummyPaperURL, dummyDefaultModel, dummyPacingMaxTime);
      fail("Should not permit an identifier value less than '" + CellModelVO.minModelIdentifier + "'.");
    } catch (IllegalArgumentException e) {}

    dummyIdentifier = 1;
    try {
      cellModel = new CellModelVO(dummyIdentifier, dummyName, dummyDescription, dummyCellMLURL,
                                  dummyPaperURL, dummyDefaultModel, dummyPacingMaxTime);
      fail("Should not permit an empty name.");
    } catch (IllegalArgumentException e) {}

    dummyName = "dummyName";
    cellModel = new CellModelVO(dummyIdentifier, dummyName, dummyDescription, dummyCellMLURL,
                                dummyPaperURL, dummyDefaultModel, dummyPacingMaxTime);
    assertSame(dummyIdentifier, cellModel.getIdentifier());
    assertEquals(dummyName, cellModel.getName());
    assertNull(cellModel.getDescription());
    assertNull(cellModel.getCellmlURL());
    assertNull(cellModel.getPaperURL());
    assertNull(cellModel.getPacingMaxTime());
    assertFalse(cellModel.isDefaultModel());
    assertNotNull(cellModel.toString());

    dummyDescription = "dummyDescription";
    dummyCellMLURL = "dummyCellMLURL";
    dummyPaperURL = "dummyPaperURL";
    dummyDefaultModel = true;
    dummyPacingMaxTime = BigDecimal.TEN;
    cellModel = new CellModelVO(dummyIdentifier, dummyName, dummyDescription, dummyCellMLURL,
                                dummyPaperURL, dummyDefaultModel, dummyPacingMaxTime);

    assertSame(dummyIdentifier, cellModel.getIdentifier());
    assertEquals(dummyName, cellModel.getName());
    assertSame(dummyDescription, cellModel.getDescription());
    assertSame(dummyCellMLURL, cellModel.getCellmlURL());
    assertSame(dummyPaperURL, cellModel.getPaperURL());
    assertTrue(cellModel.isDefaultModel());
    assertSame(dummyPacingMaxTime, cellModel.getPacingMaxTime());
  }

  @Test
  public void testCompareTo() {
    short dummyIdentifier = 1;
    String dummyName = "dummyName";
    String dummyDescription = null;
    String dummyCellMLURL = null;
    String dummyPaperURL = null;
    boolean dummyDefaultModel = false;
    BigDecimal dummyPacingMaxTime = null;

    final CellModelVO cellModel1 = new CellModelVO(dummyIdentifier, dummyName, dummyDescription,
                                                   dummyCellMLURL, dummyPaperURL, dummyDefaultModel,
                                                   dummyPacingMaxTime);

    final CellModelVO cellModel2 = new CellModelVO(dummyIdentifier, dummyName, dummyDescription,
                                                   dummyCellMLURL, dummyPaperURL, dummyDefaultModel,
                                                   dummyPacingMaxTime);

    assertTrue(cellModel1.compareTo(cellModel2) == 0);
    assertTrue(cellModel1.equals(cellModel2));

    dummyIdentifier = 2;
    final CellModelVO cellModel3 = new CellModelVO(dummyIdentifier, dummyName, dummyDescription,
                                                   dummyCellMLURL, dummyPaperURL, dummyDefaultModel,
                                                   dummyPacingMaxTime);
    assertFalse(cellModel1.compareTo(cellModel3) == 0);
    assertFalse(cellModel1.equals(cellModel3));
  }
}