/*

  Original work: Copyright (c) 2015, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.service;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

import java.io.IOException;
import java.math.BigDecimal;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.client.ClientIdentifiers;
import uk.ac.ox.cs.epsrc.client.exception.BusinessManagerException;
import uk.ac.ox.cs.epsrc.client.exception.ClientException;
import uk.ac.ox.cs.epsrc.client.manager.BusinessManagerManager;
import uk.ac.ox.cs.epsrc.client.manager.ConfigurationManager;
import uk.ac.ox.cs.epsrc.client.service.ClientService;
import uk.ac.ox.cs.epsrc.client.service.ClientServiceImpl;
import uk.ac.ox.cs.epsrc.client.value.AllProgressVO;
import uk.ac.ox.cs.epsrc.client.value.ExperimentalDataVO;
import uk.ac.ox.cs.epsrc.client.value.InputValuesVO;
import uk.ac.ox.cs.epsrc.client.value.JobDiagnosticsVO;
import uk.ac.ox.cs.epsrc.client.value.ResultsVO;
import uk.ac.ox.cs.epsrc.client.value.SimulationResponseVO;
import uk.ac.ox.cs.epsrc.client.value.config.AllConfigurations;

/**
 * Unit test the Business Manager service implementation.
 *
 * @author geoff
 */
public class ClientServiceImplTest {

  private BusinessManagerManager mockBusinessManagerManager;
  private ClientService clientService;
  private ConfigurationManager mockConfigurationManager;
  private IMocksControl mocksControl;
  private JobDiagnosticsVO mockJobDiagnostics;
  private long dummyJobId = 4l;
  private long dummySimulationId = 3l;
  private final String dummyUserId = null;

  @Before
  public void setUp() throws IOException {
    clientService = new ClientServiceImpl();

    mocksControl = createStrictControl();
    mockBusinessManagerManager = mocksControl.createMock(BusinessManagerManager.class);
    mockConfigurationManager = mocksControl.createMock(ConfigurationManager.class);
    ReflectionTestUtils.setField(clientService, ClientIdentifiers.COMPONENT_BUSINESS_MANAGER_MANAGER,
                                 mockBusinessManagerManager);
    ReflectionTestUtils.setField(clientService, ClientIdentifiers.COMPONENT_CONFIGURATION_MANAGER,
                                 mockConfigurationManager);
    mockJobDiagnostics = mocksControl.createMock(JobDiagnosticsVO.class);
  }

  @Test
  public void testQueryBusinessManager() throws BusinessManagerException,
                                                ClientException {
    final String dummyCompoundIdentifier = "dummyCompoundIdentifier";
    final Boolean dummyForceReRun = Boolean.FALSE;
    final Boolean dummyReset = Boolean.TRUE;
    final Boolean dummyAssayGrouping = Boolean.FALSE;
    final Boolean dummyValueInheriting = Boolean.TRUE;
    final Boolean dummyWithinGroups = Boolean.FALSE;
    final Boolean dummyBetweenGroups = Boolean.TRUE;
    final String dummyStrategies = "dummyStrategies";
    final String dummyDoseResponseFitting = "dummyDoseResponseFitting";
    final Boolean dummydoseResponseFittingRounding = Boolean.FALSE;
    final Boolean dummyDoseResponseFittingHillMinMax = Boolean.TRUE;
    final Float dummyDoseResponseFittingHillMax = 1.1F;
    final Float dummyDoseResponseFittingHillMin = 2.2F;
    final Short dummyCellModelIdentifier = 1;
    final BigDecimal dummyPacingMaxTime = BigDecimal.TEN;
    final String dummyUserId = "dummyUserId";

    expect(mockBusinessManagerManager.queryBusinessManager(dummyCompoundIdentifier,
                                                           dummyForceReRun,
                                                           dummyReset,
                                                           dummyAssayGrouping,
                                                           dummyValueInheriting,
                                                           dummyWithinGroups,
                                                           dummyBetweenGroups,
                                                           dummyStrategies,
                                                           dummyDoseResponseFitting,
                                                           dummydoseResponseFittingRounding,
                                                           dummyDoseResponseFittingHillMinMax,
                                                           dummyDoseResponseFittingHillMax,
                                                           dummyDoseResponseFittingHillMin,
                                                           dummyCellModelIdentifier,
                                                           dummyPacingMaxTime,
                                                           dummyUserId))
          .andReturn(null);

    mocksControl.replay();

    SimulationResponseVO simulationResponse = clientService.queryBusinessManager(dummyCompoundIdentifier,
                                                                                 dummyForceReRun,
                                                                                 dummyReset,
                                                                                 dummyAssayGrouping,
                                                                                 dummyValueInheriting,
                                                                                 dummyWithinGroups,
                                                                                 dummyBetweenGroups,
                                                                                 dummyStrategies,
                                                                                 dummyDoseResponseFitting,
                                                                                 dummydoseResponseFittingRounding,
                                                                                 dummyDoseResponseFittingHillMinMax,
                                                                                 dummyDoseResponseFittingHillMax,
                                                                                 dummyDoseResponseFittingHillMin,
                                                                                 dummyCellModelIdentifier,
                                                                                 dummyPacingMaxTime,
                                                                                 dummyUserId);

    mocksControl.verify();

    assertNull(simulationResponse);
  }

  @Test
  public void testRetrieveDefaultConfigurations() {
    expect(mockBusinessManagerManager.retrieveDefaultConfigurations())
          .andReturn(null);
    expect(mockConfigurationManager.retrieveDefaultConfigurations())
          .andReturn(null);

    mocksControl.replay();

    AllConfigurations defaultConfigurations = clientService.retrieveDefaultConfigurations();

    mocksControl.verify();

    assertNotNull(defaultConfigurations);
  }

  @Test
  public void testRetrieveExperimentalData() {
    expect(mockBusinessManagerManager.retrieveExperimentalData(dummySimulationId,
                                                               dummyUserId))
          .andReturn(null);

    mocksControl.replay();

    ExperimentalDataVO experimentalData = clientService.retrieveExperimentalData(dummySimulationId,
                                                                                 dummyUserId);

    mocksControl.verify();

    assertNull(experimentalData);
  }

  @Test
  public void testRetrieveJobDiagnostics() {
    expect(mockBusinessManagerManager.retrieveJobDiagnostics(dummyJobId))
          .andReturn(mockJobDiagnostics);

    mocksControl.replay();

    final JobDiagnosticsVO returnedJobDiagnostics = clientService.retrieveJobDiagnostics(dummyJobId);

    mocksControl.verify();

    assertSame(mockJobDiagnostics, returnedJobDiagnostics);
  }

  @Test
  public void testRetrieveSimulationInputValues() {
    expect(mockBusinessManagerManager.retrieveSimulationInputValues(dummySimulationId))
          .andReturn(null);

    mocksControl.replay();

    InputValuesVO inputValues = clientService.retrieveSimulationInputValues(dummySimulationId);

    mocksControl.verify();

    assertNull(inputValues);
  }

  @Test
  public void testRetrieveSimulationProgress() {
    expect(mockBusinessManagerManager.retrieveSimulationProgress(dummySimulationId))
          .andReturn(null);

    mocksControl.replay();

    AllProgressVO progress = clientService.retrieveSimulationProgress(dummySimulationId);

    mocksControl.verify();

    assertNull(progress);
  }

  @Test
  public void testRetrievesimulationProvenance1Arg() {
    expect(mockBusinessManagerManager.retrieveSimulationProvenance(dummySimulationId))
          .andReturn(null);

    mocksControl.replay();

    String provenance = clientService.retrieveSimulationProvenance(dummySimulationId);

    mocksControl.verify();

    assertNull(provenance);
  }

  @Test
  public void testRetrievesimulationProvenance3Arg() {
    final String dummyAssayName = "dummyAssayName";
    final String dummyIonChannelName = "dummyIonChannelName";

    expect(mockBusinessManagerManager.retrieveSimulationProvenance(dummySimulationId,
                                                                   dummyAssayName,
                                                                   dummyIonChannelName))
          .andReturn(null);

    mocksControl.replay();

    String provenance = clientService.retrieveSimulationProvenance(dummySimulationId,
                                                                   dummyAssayName,
                                                                   dummyIonChannelName);

    mocksControl.verify();

    assertNull(provenance);
  }

  @Test
  public void testRetrieveSimulationResults() {
    expect(mockBusinessManagerManager.retrieveSimulationResults(dummySimulationId))
          .andReturn(null);

    mocksControl.replay();

    ResultsVO results = clientService.retrieveSimulationResults(dummySimulationId);

    mocksControl.verify();

    assertNull(results);
  }
}