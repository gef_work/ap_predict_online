/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.controller;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.io.IOUtils;
import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.Model;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.epsrc.client.ClientIdentifiers;
import uk.ac.ox.cs.epsrc.client.controller.AJAX;
import uk.ac.ox.cs.epsrc.client.controller.AJAX.JSON;
import uk.ac.ox.cs.epsrc.client.exception.BusinessManagerException;
import uk.ac.ox.cs.epsrc.client.exception.ClientException;
import uk.ac.ox.cs.epsrc.client.service.ClientService;
import uk.ac.ox.cs.epsrc.client.value.AllProgressVO;
import uk.ac.ox.cs.epsrc.client.value.ExperimentalDataVO;
import uk.ac.ox.cs.epsrc.client.value.InputValuesVO;
import uk.ac.ox.cs.epsrc.client.value.JobDiagnosticsVO;
import uk.ac.ox.cs.epsrc.client.value.ResultsVO;
import uk.ac.ox.cs.epsrc.client.value.SimulationResponseVO;

/**
 * Unit test the AJAX Controller.
 * 
 * @author geoff
 */
public class AJAXTest {

  private AJAX ajaxController;
  private Authentication mockAuthentication;
  private ClientService mockClientService;
  private IMocksControl mocksControl;
  private JobDiagnosticsVO mockJobDiagnostics;
  private Locale locale;
  private MessageSource mockMessageSource;
  private Model mockModel;
  private SecurityContext mockSecurityContext;

  private static final String dummyJobIdStr = "2";
  private static final long dummyJobId = Long.valueOf(dummyJobIdStr).longValue();
  private static final String dummySimulationIdStr = "1";
  private static final long dummySimulationId = Long.valueOf(dummySimulationIdStr).longValue();
  private static final String dummyAssayName = "fakeAssayName";
  private static final String dummyIonChannelName = "fakeIonChannelName";
  private static final String dummyExceptionString = "fake exception";

  private static String dummyJSONBusMgrProgressNoJobs;

  private static final String dummyJob1BusMgrText1 = "j1BMt1";
  private static String dummyJSONBusMgrProgressWithJobs;

  private static final String dummyJob1AppMgrText1 = "j1AMt1";

  private static final List<String> dummyJSONAppMgrEmptyProgress = new ArrayList<String>();
  private static final List<String> dummyJSONAppMgrJobProgress = new ArrayList<String>();

  private static final String dummyUserId = null;

  static {
    try {
      // Business Manager generated overall {"progress":[{"text":"overall_1","args":[]}]} */
      final JSONObject overallProgress = new JSONObject().put(ClientIdentifiers.KEY_INFORMATION_TEXT,
                                                              "overall_1");
      overallProgress.put(ClientIdentifiers.KEY_INFORMATION_ARGS, new JSONArray());
      final JSONObject progressNoJobs =  new JSONObject().put(ClientIdentifiers.KEY_INFORMATION_PROGRESS,
                                                              new JSONArray().put(overallProgress));
      dummyJSONBusMgrProgressNoJobs = progressNoJobs.toString();

      /* Business Manager generated overall + job progress, e.g.
         {"progress":[{"text":"overall_1","args":[]}],"job":[{"jobId":1,"progress":[{"text":"j1BMt1","args":[]}]}]} */
      final JSONObject jobProgressObj = new JSONObject();
      jobProgressObj.put(ClientIdentifiers.KEY_INFORMATION_TEXT, dummyJob1BusMgrText1);
      jobProgressObj.put(ClientIdentifiers.KEY_INFORMATION_ARGS, new JSONArray());

      final JSONObject jobObject1 = new JSONObject();
      jobObject1.put(ClientIdentifiers.KEY_INFORMATION_JOB_ID, 1);
      jobObject1.put(ClientIdentifiers.KEY_INFORMATION_PROGRESS, new JSONArray().put(jobProgressObj));
      final JSONArray jobArray1 = new JSONArray().put(jobObject1);

      final JSONObject progressWithJobs = new JSONObject().put(ClientIdentifiers.KEY_INFORMATION_PROGRESS,
                                                               new JSONArray().put(overallProgress))
                                                          .put(ClientIdentifiers.KEY_INFORMATION_PROGRESS_JOBS,
                                                               jobArray1);
      dummyJSONBusMgrProgressWithJobs = progressWithJobs.toString();

      // Empty app manager generated progress, e.g. {"jobId":1,"progress":[]}
      final JSONObject appMgrEmptyJobProgress = new JSONObject();
      appMgrEmptyJobProgress.put(ClientIdentifiers.KEY_INFORMATION_JOB_ID, 1);
      appMgrEmptyJobProgress.put(ClientIdentifiers.KEY_INFORMATION_PROGRESS, new JSONArray());
      dummyJSONAppMgrEmptyProgress.add(appMgrEmptyJobProgress.toString());

      // App Manager generated progress, e.g. {"jobId":1,"progress":[{"text":"j1AMt1","args":[]}]}
      final JSONObject appMgrProgressObj = new JSONObject();
      appMgrProgressObj.put(ClientIdentifiers.KEY_INFORMATION_TEXT, dummyJob1AppMgrText1);
      appMgrProgressObj.put(ClientIdentifiers.KEY_INFORMATION_ARGS, new JSONArray());

      final JSONObject appMgrJobProgress = new JSONObject();
      appMgrJobProgress.put(ClientIdentifiers.KEY_INFORMATION_JOB_ID, 1);
      appMgrJobProgress.put(ClientIdentifiers.KEY_INFORMATION_PROGRESS, new JSONArray().put(appMgrProgressObj));
      dummyJSONAppMgrJobProgress.add(appMgrJobProgress.toString());

    } catch (JSONException e) {
      e.printStackTrace();
    }

    
  }

  @Before
  public void setUp() {
    ajaxController = new AJAX();
    mocksControl = createControl();
    mockClientService = mocksControl.createMock(ClientService.class);
    ReflectionTestUtils.setField(ajaxController,
                                 ClientIdentifiers.COMPONENT_CLIENT_SERVICE,
                                 mockClientService);
    mockModel = mocksControl.createMock(Model.class);
    mockMessageSource = mocksControl.createMock(MessageSource.class);
    mockJobDiagnostics = mocksControl.createMock(JobDiagnosticsVO.class);
    mockSecurityContext = mocksControl.createMock(SecurityContext.class);
    mockAuthentication = mocksControl.createMock(Authentication.class);
    ajaxController.setMessageSource(mockMessageSource);

    locale = new Locale("en");
  }

  @Test
  public void testJobDiagnosticsRetrieval() {
    expect(mockClientService.retrieveJobDiagnostics(dummyJobId))
          .andReturn(mockJobDiagnostics);
    expect(mockModel.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_JOB_DIAGNOSTICS,
                                  mockJobDiagnostics))
          .andReturn(mockModel);

    mocksControl.replay();

    final String viewName = ajaxController.retrieveJobDiagnostics(dummyJobIdStr, locale, mockModel);

    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);
  }

  @Test
  public void testJobDiagnosticsRetrievalExceptionCatch() {
    expect(mockClientService.retrieveJobDiagnostics(dummyJobId))
          .andThrow(new UnsupportedOperationException(dummyExceptionString));

    expect(mockMessageSource.getMessage(isA(String.class), anyObject(Object[].class),
                                        isA(Locale.class)))
          .andThrow(new NoSuchMessageException("")).anyTimes();

    // Catch what's being placed into the model
    final Capture<String> modelMessageKey = newCapture();
    final Capture<AJAX.JSON> modelAttrArgs = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey), capture(modelAttrArgs)))
          .andReturn(mockModel);

    mocksControl.replay();

    String viewName = ajaxController.retrieveJobDiagnostics(dummyJobIdStr, locale, mockModel);

    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);

    // Verify the content of the AJAX.JSON being returned is good
    assertEquals(ClientIdentifiers.MODEL_ATTRIBUTE_JOB_DIAGNOSTICS, modelMessageKey.getValue());
    assertEquals(ClientIdentifiers.MSG_BUNDLE_ID_ERROR_RETRIEVING,
                 modelAttrArgs.getValue().getException());
  }

  @Test
  public void testJSONClass() {
    String dummyJSON = null;
    String dummyException = null;

    JSON json = ajaxController.new JSON(dummyJSON, dummyException);
    assertNull(json.getJson());
    assertNull(json.getException());

    dummyJSON = "jsonStr";
    dummyException = "exceptionStr";
    json = ajaxController.new JSON(dummyJSON, dummyException);
    assertTrue(dummyJSON.equals(json.getJson()));
    assertTrue(dummyException.equals(json.getException()));
  }

  @Test
  public void testExperimentalRetrieval() {
    final ExperimentalDataVO dummyExperimentalDataVO = null;

    SecurityContextHolder.setContext(mockSecurityContext);
    expect(mockSecurityContext.getAuthentication())
          .andReturn(mockAuthentication);
    expect(mockAuthentication.getName()).andReturn(dummyUserId);

    expect(mockClientService.retrieveExperimentalData(dummySimulationId,
                                                      dummyUserId))
          .andReturn(dummyExperimentalDataVO);
    expect(mockModel.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_EXPERIMENTAL_DATA,
                                  dummyExperimentalDataVO))
          .andReturn(mockModel);
    mocksControl.replay();
    final String viewName = ajaxController.retrieveExperimental(dummySimulationIdStr, locale,
                                                                mockModel);
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);
  }

  @Test
  public void testExperimentalRetrievalExceptionCatch() {
    SecurityContextHolder.setContext(mockSecurityContext);
    expect(mockSecurityContext.getAuthentication())
          .andReturn(mockAuthentication);
    expect(mockAuthentication.getName()).andReturn(dummyUserId);

    expect(mockClientService.retrieveExperimentalData(dummySimulationId,
                                                      dummyUserId))
          .andThrow(new UnsupportedOperationException(dummyExceptionString));

    // Ignore testing the message source querying.
    expect(mockMessageSource.getMessage(isA(String.class), anyObject(Object[].class),
                                        isA(Locale.class)))
          .andThrow(new NoSuchMessageException("")).anyTimes();

    // Catch what's being placed into the model
    final Capture<String> modelMessageKey = newCapture();
    final Capture<AJAX.JSON> modelAttrArgs = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey), capture(modelAttrArgs)))
          .andReturn(mockModel);

    mocksControl.replay();
    final String viewName = ajaxController.retrieveExperimental(dummySimulationIdStr, locale,
                                                                mockModel);
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);

    // Verify the content of the AJAX.JSON being returned is good
    assertEquals(ClientIdentifiers.MODEL_ATTRIBUTE_EXPERIMENTAL_DATA, modelMessageKey.getValue());
    assertEquals(ClientIdentifiers.MSG_BUNDLE_ID_ERROR_RETRIEVING,
                 modelAttrArgs.getValue().getException());
  }

  @Test
  public void testInputValuesRetrieval() {
    final InputValuesVO dummyInputValuesVO = null;

    expect(mockClientService.retrieveSimulationInputValues(dummySimulationId))
          .andReturn(dummyInputValuesVO);
    expect(mockModel.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_INPUT_VALUES,
                                  dummyInputValuesVO))
          .andReturn(mockModel);
    mocksControl.replay();
    final String viewName = ajaxController.retrieveInputValues(dummySimulationIdStr, locale, mockModel);
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);
  }

  @Test
  public void testInputValuesRetrievalExceptionCatch() {
    expect(mockClientService.retrieveSimulationInputValues(dummySimulationId))
          .andThrow(new UnsupportedOperationException(dummyExceptionString));
    // Ignore testing the message source querying.
    expect(mockMessageSource.getMessage(isA(String.class), anyObject(Object[].class),
                                        isA(Locale.class)))
          .andThrow(new NoSuchMessageException("")).anyTimes();

    // Catch what's being placed into the model
    final Capture<String> modelMessageKey = newCapture();
    final Capture<AJAX.JSON> modelAttrArgs = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey), capture(modelAttrArgs)))
          .andReturn(mockModel);

    mocksControl.replay();
    final String viewName = ajaxController.retrieveInputValues(dummySimulationIdStr, locale, mockModel);
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);

    // Verify the content of the AJAX.JSON being returned is good
    assertEquals(ClientIdentifiers.MODEL_ATTRIBUTE_INPUT_VALUES, modelMessageKey.getValue());
    assertEquals(ClientIdentifiers.MSG_BUNDLE_ID_ERROR_RETRIEVING,
                 modelAttrArgs.getValue().getException());
  }

  @Test
  public void testProgressRetrievalOnNoProgressData() {
    final AllProgressVO dummyAllProgressVO = null;

    expect(mockClientService.retrieveSimulationProgress(dummySimulationId))
          .andReturn(dummyAllProgressVO);

    // Catch what's being placed into the model
    final Capture<String> modelMessageKey = newCapture();
    final Capture<AJAX.JSON> modelAttrArgs = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey), capture(modelAttrArgs)))
          .andReturn(mockModel);

    mocksControl.replay();
    final String viewName = ajaxController.retrieveProgress(dummySimulationIdStr, locale, mockModel);
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);

    // Verify the content of the AJAX.JSON being returned is good
    assertEquals(ClientIdentifiers.MODEL_ATTRIBUTE_PROGRESS_OVERALL, modelMessageKey.getValue());
    assertEquals(AJAX.DEFAULT_EMPTY_JSON, modelAttrArgs.getValue().getJson());
  }

  @Test
  public void testProgressRetrieval() throws JSONException {
    // Situation 1: Only Business Manager general (no job) progress data.
    AllProgressVO dummyAllProgressVO = new AllProgressVO(dummyJSONBusMgrProgressNoJobs,
                                                         null);

    expect(mockClientService.retrieveSimulationProgress(dummySimulationId))
          .andReturn(dummyAllProgressVO);
    // Ignore testing the message source querying.
    expect(mockMessageSource.getMessage(isA(String.class),
                                        anyObject(Object[].class),
                                        isA(Locale.class)))
          .andThrow(new NoSuchMessageException("")).anyTimes();

    // Catch what's being placed into the model
    Capture<String> modelMessageKey = newCapture();
    Capture<AJAX.JSON> modelAttrArgs = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey),
                                  capture(modelAttrArgs)))
          .andReturn(mockModel);

    mocksControl.replay();
    String viewName = ajaxController.retrieveProgress(dummySimulationIdStr,
                                                      locale, mockModel);
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);

    // Verify the content of the AJAX.JSON being returned is good
    assertEquals(ClientIdentifiers.MODEL_ATTRIBUTE_PROGRESS_OVERALL,
                 modelMessageKey.getValue());
    assertEquals(dummyJSONBusMgrProgressNoJobs, modelAttrArgs.getValue().getJson());

    mocksControl.reset();

    // Situation 2: Only Business Manager general and job progress data.
    dummyAllProgressVO = new AllProgressVO(dummyJSONBusMgrProgressWithJobs,
                                           null);

    expect(mockClientService.retrieveSimulationProgress(dummySimulationId))
          .andReturn(dummyAllProgressVO);
    // Ignore testing the message source querying.
    expect(mockMessageSource.getMessage(isA(String.class),
                                        anyObject(Object[].class),
                                        isA(Locale.class)))
          .andThrow(new NoSuchMessageException("")).anyTimes();

    // Catch what's being placed into the model
    modelMessageKey = newCapture();
    modelAttrArgs = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey),
                                  capture(modelAttrArgs)))
          .andReturn(mockModel);

    mocksControl.replay();
    viewName = ajaxController.retrieveProgress(dummySimulationIdStr, locale,
                                               mockModel);
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);

    // Verify the content of the AJAX.JSON being returned is good
    assertEquals(ClientIdentifiers.MODEL_ATTRIBUTE_PROGRESS_OVERALL, modelMessageKey.getValue());
    // Verify that the returned JSON-format string has the right content
    JSONObject jsonObject = new JSONObject(modelAttrArgs.getValue().getJson());
    String job1Text1 = jsonObject.getJSONArray(ClientIdentifiers.KEY_INFORMATION_PROGRESS_JOBS)
                                 .getJSONObject(0)
                                 .getJSONArray(ClientIdentifiers.KEY_INFORMATION_PROGRESS)
                                 .getJSONObject(0)
                                 .getString(ClientIdentifiers.KEY_INFORMATION_TEXT);
    assertEquals(dummyJob1BusMgrText1, job1Text1);

    mocksControl.reset();

    // Situation 3: Both Business Manager-derived general and job progress, now with empty App Manager job data.
    dummyAllProgressVO = new AllProgressVO(dummyJSONBusMgrProgressWithJobs,
                                           dummyJSONAppMgrEmptyProgress);
    expect(mockClientService.retrieveSimulationProgress(dummySimulationId))
          .andReturn(dummyAllProgressVO);
    // Ignore testing the message source querying.
    expect(mockMessageSource.getMessage(isA(String.class),
                                        anyObject(Object[].class),
                                        isA(Locale.class)))
          .andThrow(new NoSuchMessageException("")).anyTimes();

    // Catch what's being placed into the model
    modelMessageKey = newCapture();
    modelAttrArgs = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey),
                                  capture(modelAttrArgs)))
          .andReturn(mockModel);

    mocksControl.replay();
    viewName = ajaxController.retrieveProgress(dummySimulationIdStr, locale,
                                               mockModel);
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);

    // Verify the content of the AJAX.JSON being returned is good
    assertEquals(ClientIdentifiers.MODEL_ATTRIBUTE_PROGRESS_OVERALL, modelMessageKey.getValue());
    // Verify that the returned JSON-format string has the right content
    jsonObject = new JSONObject(modelAttrArgs.getValue().getJson());
    //{"progress":[{"text":"overall_1","args":[]}],"job":[{"progress":[{"text":"j1BMt1","args":[]}],"jobId":1}]}
    JSONArray job1JSONArr = jsonObject.getJSONArray(ClientIdentifiers.KEY_INFORMATION_PROGRESS_JOBS)
                                      .getJSONObject(0)
                                      .getJSONArray(ClientIdentifiers.KEY_INFORMATION_PROGRESS);
    job1Text1 = job1JSONArr.getJSONObject(0).getString(ClientIdentifiers.KEY_INFORMATION_TEXT);
    assertEquals(dummyJob1BusMgrText1, job1Text1);
    assertTrue(1 == job1JSONArr.length());

    mocksControl.reset();

    // Situation 4: Both Business Manager-derived general and job progress, now with App Manager job data.
    dummyAllProgressVO = new AllProgressVO(dummyJSONBusMgrProgressWithJobs,
                                           dummyJSONAppMgrJobProgress);

    expect(mockClientService.retrieveSimulationProgress(dummySimulationId))
          .andReturn(dummyAllProgressVO);
    // Ignore testing the message source querying.
    expect(mockMessageSource.getMessage(isA(String.class),
                                        anyObject(Object[].class),
                                        isA(Locale.class)))
          .andThrow(new NoSuchMessageException("")).anyTimes();

    // Catch what's being placed into the model
    modelMessageKey = newCapture();
    modelAttrArgs = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey),
                                  capture(modelAttrArgs)))
          .andReturn(mockModel);

    mocksControl.replay();
    viewName = ajaxController.retrieveProgress(dummySimulationIdStr, locale,
                                               mockModel);
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);

    // Verify the content of the AJAX.JSON being returned is good
    assertEquals(ClientIdentifiers.MODEL_ATTRIBUTE_PROGRESS_OVERALL,
                 modelMessageKey.getValue());
    // Verify that the returned JSON-format string has the right content
    jsonObject = new JSONObject(modelAttrArgs.getValue().getJson());
    job1JSONArr = jsonObject.getJSONArray(ClientIdentifiers.KEY_INFORMATION_PROGRESS_JOBS)
                            .getJSONObject(0)
                            .getJSONArray(ClientIdentifiers.KEY_INFORMATION_PROGRESS);
    job1Text1 = job1JSONArr.getJSONObject(0).getString(ClientIdentifiers.KEY_INFORMATION_TEXT);
    final String job1Text2 = job1JSONArr.getJSONObject(1).getString(ClientIdentifiers.KEY_INFORMATION_TEXT);

    assertEquals(dummyJob1BusMgrText1, job1Text1);
    assertEquals(dummyJob1AppMgrText1, job1Text2);
  }

  @Test
  public void testProgressRetrievalExceptionCatch() {
    expect(mockClientService.retrieveSimulationProgress(dummySimulationId))
          .andThrow(new UnsupportedOperationException(dummyExceptionString));
    // Ignore testing the message source querying.
    expect(mockMessageSource.getMessage(isA(String.class),
                                        anyObject(Object[].class),
                                        isA(Locale.class)))
          .andThrow(new NoSuchMessageException("")).anyTimes();

    // Catch what's being placed into the model
    final Capture<String> modelMessageKey = newCapture();
    final Capture<AJAX.JSON> modelAttrArgs = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey),
                                  capture(modelAttrArgs)))
          .andReturn(mockModel);

    mocksControl.replay();
    final String viewName = ajaxController.retrieveProgress(dummySimulationIdStr,
                                                            locale, mockModel);
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);

    // Verify the content of the AJAX.JSON being returned is good
    assertEquals(ClientIdentifiers.MODEL_ATTRIBUTE_PROGRESS_OVERALL,
                 modelMessageKey.getValue());
    assertNotNull(modelAttrArgs.getValue().getException());
  }

  @Test
  public void testProvenanceRetrievalBySimulationIdProcessingJSON() {
    final String origProvenance = getFile("provenance1.json");
    expect(mockClientService.retrieveSimulationProvenance(dummySimulationId))
          .andReturn(origProvenance);
    final Capture<String> modelMessageKey = newCapture();
    final Capture<AJAX.JSON> modelAttrArgs = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey),
                                  capture(modelAttrArgs)))
          .andReturn(mockModel);
    expect(mockMessageSource.getMessage(isA(String.class),
                                        anyObject(Object[].class),
                                        isA(Locale.class)))
          .andThrow(new NoSuchMessageException("")).anyTimes();
    mocksControl.replay();
    final String viewName = ajaxController.retrieveProvenance(dummySimulationIdStr,
                                                              locale, mockModel);
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);

    // Verify the content of the AJAX.JSON being returned is good
    assertEquals(ClientIdentifiers.MODEL_ATTRIBUTE_PROVENANCE,
                 modelMessageKey.getValue());
    System.out.println(new JSONObject(modelAttrArgs.getValue()).toString());
  }

  @Test
  public void testProvenanceRetrievalBySimulationId() {
    expect(mockClientService.retrieveSimulationProvenance(dummySimulationId))
          .andReturn(null);
    final Capture<String> modelMessageKey = newCapture();
    final Capture<AJAX.JSON> modelAttrArgs = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey),
                                  capture(modelAttrArgs)))
          .andReturn(mockModel);
    mocksControl.replay();
    final String viewName = ajaxController.retrieveProvenance(dummySimulationIdStr,
                                                              locale, mockModel);
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);

    // Verify the content of the AJAX.JSON being returned is good
    assertEquals(ClientIdentifiers.MODEL_ATTRIBUTE_PROVENANCE,
                 modelMessageKey.getValue());
  }

  @Test
  public void testProvenanceRetrievalBySimulationIdExceptionCatch() {
    expect(mockClientService.retrieveSimulationProvenance(dummySimulationId))
          .andThrow(new UnsupportedOperationException(dummyExceptionString));
    // Ignore testing the message source querying.
    expect(mockMessageSource.getMessage(isA(String.class),
                                        anyObject(Object[].class),
                                        isA(Locale.class)))
          .andThrow(new NoSuchMessageException("")).anyTimes();

    // Catch what's being placed into the model
    final Capture<String> modelMessageKey = newCapture();
    final Capture<AJAX.JSON> modelAttrArgs = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey), capture(modelAttrArgs)))
          .andReturn(mockModel);

    mocksControl.replay();
    final String viewName = ajaxController.retrieveProvenance(dummySimulationIdStr,
                                                              locale, mockModel);
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);

    // Verify the content of the AJAX.JSON being returned is good
    assertEquals(ClientIdentifiers.MODEL_ATTRIBUTE_PROVENANCE, modelMessageKey.getValue());
    assertNotNull(modelAttrArgs.getValue().getException());
  }

  @Test
  public void testProvenanceRetrievalBySimIdAssayIonChannel() {
    expect(mockClientService.retrieveSimulationProvenance(dummySimulationId,
                                                          dummyAssayName,
                                                          dummyIonChannelName))
                                     .andReturn(null);
    final Capture<String> modelMessageKey = newCapture();
    final Capture<AJAX.JSON> modelAttrArgs = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey), capture(modelAttrArgs)))
          .andReturn(mockModel);
    mocksControl.replay();
    final String viewName = ajaxController.retrieveProvenance(dummySimulationIdStr,
                                                              dummyAssayName,
                                                              dummyIonChannelName,
                                                              locale, mockModel);
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);

    // Verify the content of the AJAX.JSON being returned is good
    assertEquals(ClientIdentifiers.MODEL_ATTRIBUTE_PROVENANCE,
                 modelMessageKey.getValue());
  }

  @Test
  public void testProvenanceRetrievalBySimIdAssayIonChannelExceptionCatch() {
    expect(mockClientService.retrieveSimulationProvenance(dummySimulationId,
                                                          dummyAssayName,
                                                          dummyIonChannelName))
          .andThrow(new UnsupportedOperationException(dummyExceptionString));
    // Ignore testing the message source querying.
    expect(mockMessageSource.getMessage(isA(String.class),
                                        anyObject(Object[].class),
                                        isA(Locale.class)))
          .andThrow(new NoSuchMessageException("")).anyTimes();

    // Catch what's being placed into the model
    final Capture<String> modelMessageKey = newCapture();
    final Capture<AJAX.JSON> modelAttrArgs = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey),
                                  capture(modelAttrArgs)))
                    .andReturn(mockModel);

    mocksControl.replay();
    final String viewName = ajaxController.retrieveProvenance(dummySimulationIdStr,
                                                              dummyAssayName,
                                                              dummyIonChannelName,
                                                              locale, mockModel);
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);

    // Verify the content of the AJAX.JSON being returned is good
    assertEquals(ClientIdentifiers.MODEL_ATTRIBUTE_PROVENANCE,
                 modelMessageKey.getValue());
    assertNotNull(modelAttrArgs.getValue().getException());
  }

  @Test
  public void testResultsRetrieval() {
    /*
     * Return results value object.
     */
    final ResultsVO mockResultsVO = mocksControl.createMock(ResultsVO.class);
    expect(mockClientService.retrieveSimulationResults(dummySimulationId))
          .andReturn(mockResultsVO);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                                  mockResultsVO))
          .andReturn(mockModel);
    boolean dummyIsContainingQNetData = true;
    expect(mockResultsVO.isContainingQNetData())
          .andReturn(dummyIsContainingQNetData);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_QNET_AVAILABLE,
                                  dummyIsContainingQNetData))
          .andReturn(mockModel);

    mocksControl.replay();

    final String viewName = ajaxController.retrieveResults(dummySimulationIdStr,
                                                           locale, mockModel);

    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);
  }

  @Test
  public void testResultsRetrievalExceptionCatch() {
    expect(mockClientService.retrieveSimulationResults(dummySimulationId))
          .andThrow(new UnsupportedOperationException(dummyExceptionString));
    // Ignore testing the message source querying.
    expect(mockMessageSource.getMessage(isA(String.class),
                                        anyObject(Object[].class),
                                        isA(Locale.class)))
          .andThrow(new NoSuchMessageException("")).anyTimes();

    // Catch what's being placed into the model
    final Capture<String> modelMessageKey = newCapture();
    final Capture<AJAX.JSON> modelAttrArgs = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey),
                                  capture(modelAttrArgs)))
          .andReturn(mockModel);

    mocksControl.replay();
    final String viewName = ajaxController.retrieveResults(dummySimulationIdStr,
                                                           locale, mockModel);
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);

    // Verify the content of the AJAX.JSON being returned is good
    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                 modelMessageKey.getValue());
    assertNotNull(modelAttrArgs.getValue().getException());
  }

  @Test
  public void testSimulationRun() throws BusinessManagerException,
                                         ClientException {
    /*
     * Emulate lack of compound identifier. 
     */
    String dummyCompoundIdentifier = null;
    final Boolean dummyForceReRun = Boolean.TRUE;
    final Boolean dummyReset = Boolean.TRUE;
    final Boolean dummyAssayGrouping = Boolean.TRUE;
    final Boolean dummyValueInheriting = Boolean.TRUE;
    final Boolean dummyWithinGroups = Boolean.TRUE;
    final Boolean dummyBetweenGroups = Boolean.TRUE;
    final String dummyStrategies = "";
    final String dummyDoseResponseFitting = "";
    final Boolean dummyDoseResponseFittingRounding = Boolean.TRUE;
    final Boolean dummyDoseResponseFittingHillMinMax = Boolean.TRUE;
    final Float dummyDoseResponseFittingHillMin = 0.5f;
    final Float dummyDoseResponseFittingHillMax = 2.0f;
    final Short dummyCellModelIdentifier = Short.valueOf("1");
    final BigDecimal dummyPacingMaxTime = BigDecimal.TEN;
    final String dummyUserId = "dummyUserId";

    final Capture<String> modelMessageKey = newCapture();
    final Capture<Object> modelAttrArgs = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey),
                                  capture(modelAttrArgs)))
          .andReturn(mockModel);

    mocksControl.replay();

    String viewName = ajaxController.runSimulation(dummyCompoundIdentifier,
                                                   dummyForceReRun,
                                                   dummyReset, dummyAssayGrouping,
                                                   dummyValueInheriting,
                                                   dummyWithinGroups,
                                                   dummyBetweenGroups,
                                                   dummyStrategies,
                                                   dummyDoseResponseFitting,
                                                   dummyDoseResponseFittingRounding,
                                                   dummyDoseResponseFittingHillMinMax,
                                                   dummyDoseResponseFittingHillMax,
                                                   dummyDoseResponseFittingHillMin,
                                                   dummyCellModelIdentifier,
                                                   dummyPacingMaxTime,
                                                   locale, mockModel);

    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);
    assertEquals(ClientIdentifiers.MODEL_ATTRIBUTE_COMPOUND_IDENTIFIER,
                 modelMessageKey.getValue());

    mocksControl.reset();

    /*
     * Emulate submission of a compound identifier.
     */
    dummyCompoundIdentifier = "fakeCompoundIdentifier";
    expect(mockModel.addAttribute(capture(modelMessageKey),
                                  capture(modelAttrArgs)))
          .andReturn(mockModel);

    SecurityContextHolder.setContext(mockSecurityContext);
    expect(mockSecurityContext.getAuthentication())
          .andReturn(mockAuthentication);
    expect(mockAuthentication.getName()).andReturn(dummyUserId);

    final SimulationResponseVO mockSimulationResponseVO = mocksControl.createMock(SimulationResponseVO.class);
    expect(mockClientService.queryBusinessManager(dummyCompoundIdentifier,
                                                  dummyForceReRun, dummyReset,
                                                  dummyAssayGrouping,
                                                  dummyValueInheriting,
                                                  dummyWithinGroups,
                                                  dummyBetweenGroups,
                                                  dummyStrategies,
                                                  dummyDoseResponseFitting,
                                                  dummyDoseResponseFittingRounding,
                                                  dummyDoseResponseFittingHillMinMax,
                                                  dummyDoseResponseFittingHillMax,
                                                  dummyDoseResponseFittingHillMin,
                                                  dummyCellModelIdentifier,
                                                  dummyPacingMaxTime,
                                                  dummyUserId))
          .andReturn(mockSimulationResponseVO);
    expect(mockSimulationResponseVO.getSimulationId())
          .andReturn(dummySimulationId);
    final Capture<String> modelMessageKey2 = newCapture();
    final Capture<Object> modelAttrArgs2 = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey2),
                                  capture(modelAttrArgs2)))
          .andReturn(mockModel);

    mocksControl.replay();

    viewName = ajaxController.runSimulation(dummyCompoundIdentifier,
                                            dummyForceReRun, dummyReset,
                                            dummyAssayGrouping,
                                            dummyValueInheriting,
                                            dummyWithinGroups,
                                            dummyBetweenGroups, dummyStrategies,
                                            dummyDoseResponseFitting,
                                            dummyDoseResponseFittingRounding,
                                            dummyDoseResponseFittingHillMinMax,
                                            dummyDoseResponseFittingHillMax,
                                            dummyDoseResponseFittingHillMin,
                                            dummyCellModelIdentifier,
                                            dummyPacingMaxTime, locale,
                                            mockModel);

    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);
    assertEquals(ClientIdentifiers.MODEL_ATTRIBUTE_COMPOUND_IDENTIFIER,
                 modelMessageKey.getValue());
    assertEquals(dummyCompoundIdentifier, modelAttrArgs.getValue());
    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                 modelMessageKey2.getValue());
    assertEquals(dummySimulationId, modelAttrArgs2.getValue());
  }

  @Test
  public void testSimulationRunExceptionCatch() throws BusinessManagerException,
                                                       ClientException {
    final String dummyCompoundIdentifier = "fakeCompoundIdentifier";
    final Boolean dummyForceReRun = Boolean.TRUE;
    final Boolean dummyReset = Boolean.TRUE;
    final Boolean dummyAssayGrouping = Boolean.TRUE;
    final Boolean dummyValueInheriting = Boolean.TRUE;
    final Boolean dummyWithinGroups = Boolean.TRUE;
    final Boolean dummyBetweenGroups = Boolean.TRUE;
    final String dummyStrategies = "";
    final String dummyDoseResponseFitting = "";
    final Boolean dummyDoseResponseFittingRounding = Boolean.TRUE;
    final Boolean dummyDoseResponseFittingHillMinMax = Boolean.TRUE;
    final Float dummyDoseResponseFittingHillMin = 0.5f;
    final Float dummyDoseResponseFittingHillMax = 2.0f;
    final Short dummyCellModelIdentifier = Short.valueOf("1");
    final BigDecimal dummyPacingMaxTime = BigDecimal.TEN;
    final String dummyUserId = "dummyUserId";

    Capture<String> modelMessageKey = newCapture();
    Capture<Object> modelAttrArgs = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey),
                                  capture(modelAttrArgs)))
          .andReturn(mockModel);

    SecurityContextHolder.setContext(mockSecurityContext);
    expect(mockSecurityContext.getAuthentication())
          .andReturn(mockAuthentication);
    expect(mockAuthentication.getName()).andReturn(dummyUserId);

    // Throw the Business Manager exception
    expect(mockClientService.queryBusinessManager(dummyCompoundIdentifier,
                                                  dummyForceReRun, dummyReset,
                                                  dummyAssayGrouping,
                                                  dummyValueInheriting,
                                                  dummyWithinGroups,
                                                  dummyBetweenGroups,
                                                  dummyStrategies,
                                                  dummyDoseResponseFitting,
                                                  dummyDoseResponseFittingRounding,
                                                  dummyDoseResponseFittingHillMinMax,
                                                  dummyDoseResponseFittingHillMax,
                                                  dummyDoseResponseFittingHillMin,
                                                  dummyCellModelIdentifier,
                                                  dummyPacingMaxTime,
                                                  dummyUserId))
          .andThrow(new BusinessManagerException(dummyExceptionString));
    Capture<String> modelMessageKey2 = newCapture();
    Capture<Object> modelAttrArgs2 = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey2),
                                  capture(modelAttrArgs2)))
          .andReturn(mockModel);
    mocksControl.replay();
    String viewName = ajaxController.runSimulation(dummyCompoundIdentifier,
                                                   dummyForceReRun, dummyReset,
                                                   dummyAssayGrouping,
                                                   dummyValueInheriting,
                                                   dummyWithinGroups,
                                                   dummyBetweenGroups,
                                                   dummyStrategies,
                                                   dummyDoseResponseFitting,
                                                   dummyDoseResponseFittingRounding,
                                                   dummyDoseResponseFittingHillMinMax,
                                                   dummyDoseResponseFittingHillMax,
                                                   dummyDoseResponseFittingHillMin,
                                                   dummyCellModelIdentifier,
                                                   dummyPacingMaxTime,
                                                   locale, mockModel);
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);
    assertEquals(ClientIdentifiers.MODEL_ATTRIBUTE_COMPOUND_IDENTIFIER,
                 modelMessageKey.getValue());
    assertEquals(dummyCompoundIdentifier, modelAttrArgs.getValue());
    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                 modelMessageKey2.getValue());
    assertEquals(dummyExceptionString, modelAttrArgs2.getValue());

    mocksControl.reset();

    modelMessageKey = newCapture();
    modelAttrArgs = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey),
                                  capture(modelAttrArgs)))
          .andReturn(mockModel);

    expect(mockSecurityContext.getAuthentication())
          .andReturn(mockAuthentication);
    expect(mockAuthentication.getName()).andReturn(dummyUserId);

    expect(mockClientService.queryBusinessManager(dummyCompoundIdentifier,
                                                  dummyForceReRun, dummyReset,
                                                  dummyAssayGrouping,
                                                  dummyValueInheriting,
                                                  dummyWithinGroups,
                                                  dummyBetweenGroups,
                                                  dummyStrategies,
                                                  dummyDoseResponseFitting,
                                                  dummyDoseResponseFittingRounding,
                                                  dummyDoseResponseFittingHillMinMax,
                                                  dummyDoseResponseFittingHillMax,
                                                  dummyDoseResponseFittingHillMin,
                                                  dummyCellModelIdentifier,
                                                  dummyPacingMaxTime,
                                                  dummyUserId))
          .andThrow(new ClientException(dummyExceptionString));
    modelMessageKey2 = newCapture();
    modelAttrArgs2 = newCapture();
    expect(mockModel.addAttribute(capture(modelMessageKey2), capture(modelAttrArgs2)))
          .andReturn(mockModel);
    mocksControl.replay();
    viewName = ajaxController.runSimulation(dummyCompoundIdentifier,
                                            dummyForceReRun,
                                            dummyReset, dummyAssayGrouping,
                                            dummyValueInheriting,
                                            dummyWithinGroups,
                                            dummyBetweenGroups, dummyStrategies,
                                            dummyDoseResponseFitting,
                                            dummyDoseResponseFittingRounding,
                                            dummyDoseResponseFittingHillMinMax,
                                            dummyDoseResponseFittingHillMax,
                                            dummyDoseResponseFittingHillMin,
                                            dummyCellModelIdentifier,
                                            dummyPacingMaxTime,
                                            locale, mockModel);
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);
    assertEquals(ClientIdentifiers.MODEL_ATTRIBUTE_COMPOUND_IDENTIFIER,
                 modelMessageKey.getValue());
    assertEquals(dummyCompoundIdentifier, modelAttrArgs.getValue());
    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                 modelMessageKey2.getValue());
    assertEquals(dummyExceptionString, modelAttrArgs2.getValue());
  }

  private String getFile(final String fileName) {
    String result = "";
    try {
      result = IOUtils.toString(this.getClass().getResourceAsStream(fileName));
    } catch (IOException e) {
      e.printStackTrace();
    }
    return result;
  }
}