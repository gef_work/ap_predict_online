/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.controller;

import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.easymock.IMocksControl;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.Model;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.epsrc.client.ClientIdentifiers;
import uk.ac.ox.cs.epsrc.client.controller.Default;
import uk.ac.ox.cs.epsrc.client.service.ClientService;
import uk.ac.ox.cs.epsrc.client.value.config.AllConfigurations;
import uk.ac.ox.cs.epsrc.client.value.config.CellModelVO;
import uk.ac.ox.cs.epsrc.client.value.config.PC50EvaluationStrategyConfigVO;
import uk.ac.ox.cs.epsrc.client.value.type.InputValueDisplay;

/**
 * Unit test the default controller.
 *
 * @author geoff
 */
public class DefaultTest {

  private AllConfigurations mockAllConfigurations;
  private Default defaultController;
  private ClientService mockClientService;
  private IMocksControl mocksControl;
  private Model mockModel;

  private static final String dummyExceptionString = "fake exception";

  @Before
  public void setUp() {
    defaultController = new Default();

    mocksControl = createControl();
    mockClientService = mocksControl.createMock(ClientService.class);
    ReflectionTestUtils.setField(defaultController,
                                 ClientIdentifiers.COMPONENT_CLIENT_SERVICE,
                                 mockClientService);

    mockAllConfigurations = mocksControl.createMock(AllConfigurations.class);
    mockModel = mocksControl.createMock(Model.class);
  }

  @Test
  public void testDefaultHandling() {
    final short dummyDefaultMinInformationLevel = Short.valueOf("1");
    final short dummyDefaultMaxInformationLevel = Short.valueOf("2");
    final Map<Short, Vector<String>> dummyIonChannels = null;
    final List<CellModelVO> dummyCellModels = new ArrayList<CellModelVO>();
    final CellModelVO mockCellModelVO = mocksControl.createMock(CellModelVO.class);
    dummyCellModels.add(mockCellModelVO);
    final Map<String, String> dummyC50Units = new HashMap<String, String>();
    final InputValueDisplay dummyInputValueDisplay = InputValueDisplay.meanOfpIC50FromAnyModifier;
    final List<PC50EvaluationStrategyConfigVO> dummyPC50EvaluationStrategies = null;
    final int dummySimulationRequestProcessingPolling = Integer.valueOf("3");

    expect(mockClientService.retrieveDefaultConfigurations())
          .andReturn(mockAllConfigurations);
    expect(mockAllConfigurations.retrieveDefaultMinInformationLevel())
          .andReturn(dummyDefaultMinInformationLevel);
    expect(mockModel.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_MIN_INFORMATION_LEVEL,
                                  dummyDefaultMinInformationLevel))
          .andReturn(mockModel);
    expect(mockAllConfigurations.retrieveDefaultMaxInformationLevel())
          .andReturn(dummyDefaultMaxInformationLevel);
    expect(mockModel.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_MAX_INFORMATION_LEVEL,
                                  dummyDefaultMaxInformationLevel))
          .andReturn(mockModel);
    expect(mockAllConfigurations.retrieveIonChannels())
          .andReturn(dummyIonChannels);
    expect(mockModel.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_ION_CHANNELS,
                                  new JSONObject(dummyIonChannels).toString()))
          .andReturn(mockModel);
    expect(mockAllConfigurations.retrieveC50Units())
          .andReturn(dummyC50Units);
    expect(mockModel.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_C50_UNITS,
                                  dummyC50Units))
          .andReturn(mockModel);
    expect(mockAllConfigurations.retrieveCellModels())
          .andReturn(dummyCellModels);
    expect(mockModel.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_CELL_MODELS,
                                  dummyCellModels))
          .andReturn(mockModel);
    expect(mockAllConfigurations.retrieveInputValueDisplay())
          .andReturn(dummyInputValueDisplay);
    expect(mockModel.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_INPUT_VALUE_DISPLAY,
                                  dummyInputValueDisplay))
          .andReturn(mockModel);
    expect(mockAllConfigurations.retrievePC50EvaluationStrategies())
          .andReturn(dummyPC50EvaluationStrategies);
    expect(mockModel.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_PC50_EVALUATION_STRATEGIES,
                                  dummyPC50EvaluationStrategies))
          .andReturn(mockModel);
    expect(mockAllConfigurations.retrieveSimulationRequestProcessingPolling())
          .andReturn(dummySimulationRequestProcessingPolling);
    expect(mockModel.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_SIM_REQ_POLLING,
                                  dummySimulationRequestProcessingPolling))
          .andReturn(mockModel);

    mocksControl.replay();

    final String viewName = defaultController.handleDefault(mockModel);

    mocksControl.verify();

    assertEquals(ClientIdentifiers.PAGE_INDEX, viewName);
  }

  @Test
  public void testDefaultHandlingExceptionCatch() {
    expect(mockClientService.retrieveDefaultConfigurations())
          .andThrow(new UnsupportedOperationException(dummyExceptionString));
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                                  dummyExceptionString))
          .andReturn(mockModel);

    mocksControl.replay();
    String viewName = defaultController.handleDefault(mockModel);
    mocksControl.verify();

    assertEquals(ClientIdentifiers.PAGE_INDEX, viewName);
  }
}