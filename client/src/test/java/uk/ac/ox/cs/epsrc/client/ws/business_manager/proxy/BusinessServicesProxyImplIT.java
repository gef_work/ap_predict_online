/*

  Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.ws.business_manager.proxy;

import static org.junit.Assert.assertEquals;
import static org.springframework.ws.test.client.RequestMatchers.*;
import static org.springframework.ws.test.client.ResponseCreators.*;

import java.math.BigDecimal;

import javax.xml.transform.Source;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.xml.transform.StringSource;

import uk.ac.ox.cs.epsrc.client.exception.BusinessManagerException;
import uk.ac.ox.cs.epsrc.client.exception.ClientException;
import uk.ac.ox.cs.epsrc.client.value.SimulationResponseVO;

import org.springframework.ws.test.client.MockWebServiceServer;

/**
 * Business services proxy implementation integration test.
 *
 * @author geoff
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( { "classpath:/uk/ac/ox/cs/epsrc/client/ws/business_manager/proxy/appCtx.ws.xml",
                         "classpath:/uk/ac/ox/cs/epsrc/client/ws/business_manager/proxy/appCtx.ws.security-outgoing.xml" } )
public class BusinessServicesProxyImplIT {

  @Autowired
  private BusinessServicesProxyImpl businessServicesProxyImpl;

  private MockWebServiceServer mockServer;

  private String dummyCompoundIdentifier;
  private Boolean dummyForceReRun;
  private Boolean dummyReset;
  private Boolean dummyAssayGrouping;
  private Boolean dummyValueInheriting;
  private Boolean dummyWithinGroups;
  private Boolean dummyBetweenGroups;
  private String dummyPC50EvaluationStrategies;
  private String dummyDoseResponseFittingStrategy;
  private Boolean dummyDoseResponseFittingRounding;
  private Boolean dummyDoseResponseFittingHillMinMax;
  private Float dummyDoseResponseFittingHillMax;
  private Float dummyDoseResponseFittingHillMin;
  private Short dummyCellModelIdentifier;
  private BigDecimal dummyPacingMaxTime;
  private String dummyUserId;

  private static String request = "<ns2:ProcessSimulationsRequest xmlns:ns2=\"http://www.cs.ox.ac.uk/epsrc/business_manager/ws/1\"><ns2:SimulationDetail CellMLModelIdentifier=\"%d\" UserId=\"%s\"><ns2:CompoundIdentifier>%s</ns2:CompoundIdentifier></ns2:SimulationDetail></ns2:ProcessSimulationsRequest>";
  private static String response = "<ns2:ProcessSimulationsResponse xmlns:ns2=\"http://www.cs.ox.ac.uk/epsrc/business_manager/ws/1\"><ns2:ProcessSimulationsResponseStructure><ns2:CompoundIdentifier>%s</ns2:CompoundIdentifier><ns2:SimulationId>%d</ns2:SimulationId></ns2:ProcessSimulationsResponseStructure></ns2:ProcessSimulationsResponse>";

  @Before
  public void setUp() {
    mockServer = MockWebServiceServer.createServer(businessServicesProxyImpl);

    dummyCompoundIdentifier = "dummyCompoundIdentifier";
    dummyCellModelIdentifier = 2;
    dummyUserId = "dummyUserId";
  }

  /**
   * Verify that the User Id is being assigned.
   */
  @Test
  public void testQueryBusinessManagerWithUserId() throws BusinessManagerException,
                                                          ClientException {

    final Long dummySimulationId = 1L;
    String fakeRequest = String.format(request, dummyCellModelIdentifier,
                                       dummyUserId, dummyCompoundIdentifier);
    String fakeResponse = String.format(response,  dummyCompoundIdentifier,
                                        dummySimulationId);

    Source expectedRequestPayload = 
           new StringSource(fakeRequest);
    Source responsePayload =
           new StringSource(fakeResponse);

    mockServer.expect(payload(expectedRequestPayload))
              .andRespond(withPayload(responsePayload));

    SimulationResponseVO simulationResponseVO = 
      businessServicesProxyImpl.queryBusinessManager(dummyCompoundIdentifier,
                                                     dummyForceReRun, dummyReset,
                                                     dummyAssayGrouping,
                                                     dummyValueInheriting,
                                                     dummyWithinGroups,
                                                     dummyBetweenGroups,
                                                     dummyPC50EvaluationStrategies,
                                                     dummyDoseResponseFittingStrategy,
                                                     dummyDoseResponseFittingRounding,
                                                     dummyDoseResponseFittingHillMinMax,
                                                     dummyDoseResponseFittingHillMax,
                                                     dummyDoseResponseFittingHillMin,
                                                     dummyCellModelIdentifier,
                                                     dummyPacingMaxTime,
                                                     dummyUserId);

    assertEquals(dummyCompoundIdentifier,
                 simulationResponseVO.getCompoundIdentifier());
    assertEquals(dummySimulationId, simulationResponseVO.getSimulationId());

    mockServer.verify();
  }
}