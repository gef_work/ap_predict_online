/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import uk.ac.ox.cs.epsrc.client.ClientIdentifiers;
import uk.ac.ox.cs.epsrc.client.util.ProvenanceUtil;

/**
 * Test the provenance utility class.
 *
 * @author geoff
 */
public class ProvenanceUtilTest {

  // See BusinessManager OrientDAOImpl.java
  private static final String assayNameKey = "assayName";
  private static final String ionChannelKey = "ionChannelName";

  // See BusinessManager API AssayUtil.java
  private static final String assayManualPatch = "Manual Patch";
  private static final String assayPatchXpress = "PatchXpress";
  private static final String assayQpatch = "Qpatch";
  private static final String assayQuattroFLIPR = "Quattro_FLIPR";
  // See BusinessManager API IonChannel.java
  private static final String ionChannelCaV = "CaV1_2";
  private static final String ionChannelHERG = "hERG";
  private static final String ionChannelKCNQ1 = "KCNQ1";
  private static final String ionChannelNaV = "NaV1_5";

  @Test
  public void testJSONAdjustProvenance1() {
    final String origProvenance = getFile("provenance1.json");
    final String adjustedProvenance = ProvenanceUtil.adjustJSON(origProvenance);

    try {
      final JSONObject json = new JSONObject(adjustedProvenance);
      final JSONArray childrenLevel1 = (JSONArray) json.get(ClientIdentifiers.KEY_INFORMATION_CHILDREN);
      assertEquals(12, childrenLevel1.length());
      for (int i = 0; i < childrenLevel1.length(); i++) {
        final JSONObject childObj = (JSONObject) childrenLevel1.get(i);
        // @rid is an identifier created/used by OrientDb
        final String rid = (String) childObj.get("@rid");
        if (rid.equals("#11:57")) {
          checkLevel1Data(childObj, 5, assayPatchXpress, ionChannelHERG);
        } else if (rid.equals("#11:58")) {
          checkLevel1Data(childObj, 1, assayPatchXpress, ionChannelHERG);
        } else if (rid.equals("#11:63")) {
          checkLevel1Data(childObj, 1, assayPatchXpress, ionChannelNaV);
        } else if (rid.equals("#11:64")) {
          checkLevel1Data(childObj, 1, assayPatchXpress, ionChannelNaV);
        } else if (rid.equals("#11:65")) {
          checkLevel1Data(childObj, 1, assayPatchXpress, ionChannelNaV);
        } else if (rid.equals("#11:68")) {
          checkLevel1Data(childObj, 1, assayPatchXpress, ionChannelCaV);
        } else if (rid.equals("#11:73")) {
          checkLevel1Data(childObj, 5, assayQpatch, ionChannelHERG);
        } else if (rid.equals("#11:74")) {
          checkLevel1Data(childObj, 0, assayManualPatch, ionChannelHERG);
        } else if (rid.equals("#10:12")) {
          checkLevel1Data(childObj, 2, assayQuattroFLIPR, ionChannelKCNQ1);
          // Summary data having IndividualData children
          final JSONArray childrenLevel2 = (JSONArray) childObj.get(ClientIdentifiers.KEY_INFORMATION_CHILDREN);
          checkLevel2Data(childrenLevel2, new String[] { "#11:59", "#11:60" });
        } else if (rid.equals("#10:13")) {
          checkLevel1Data(childObj, 2, assayQuattroFLIPR, ionChannelCaV);
          // Summary data having IndividualData children
          final JSONArray childrenLevel2 = (JSONArray) childObj.get(ClientIdentifiers.KEY_INFORMATION_CHILDREN);
          checkLevel2Data(childrenLevel2, new String[] { "#11:61", "#11:62" });
        } else if (rid.equals("#10:14")) {
          checkLevel1Data(childObj, 2, assayQuattroFLIPR, ionChannelNaV);
          // Summary data having IndividualData children
          final JSONArray childrenLevel2 = (JSONArray) childObj.get(ClientIdentifiers.KEY_INFORMATION_CHILDREN);
          checkLevel2Data(childrenLevel2, new String[] { "#11:66", "#11:67" });
        } else if (rid.equals("#10:15")) {
          checkLevel1Data(childObj, 4, assayQuattroFLIPR, ionChannelHERG);
          // Summary data having IndividualData children
          final JSONArray childrenLevel2 = (JSONArray) childObj.get(ClientIdentifiers.KEY_INFORMATION_CHILDREN);
          checkLevel2Data(childrenLevel2, new String[] { "#11:69", "#11:70", "#11:71", "#11:72" });
        } else {
          fail("Unrecognised @rid value of '" + rid + "' encountered.");
        }
      }

    } catch (JSONException e) {
      e.printStackTrace();
      fail("Exception encountered processing JSON.");
    }
  }

  private void checkLevel1Data(final JSONObject testObject, final int childrenLength,
                               final String assayName, final String ionChannelName)
                               throws JSONException {
    JSONArray childrenData = null;
    if (childrenLength == 0) {
      try {
        testObject.get(ClientIdentifiers.KEY_INFORMATION_CHILDREN);
        fail("Should not be able to find children if none expected!.");
      } catch (JSONException e) {}
    } else {
      childrenData = (JSONArray) testObject.get(ClientIdentifiers.KEY_INFORMATION_CHILDREN);
      assertEquals(childrenLength, childrenData.length());
    }

    final String actualAssayName = (String) testObject.get(assayNameKey);
    final String actualIonChannelName = (String) testObject.get(ionChannelKey);

    assertTrue(assayName.equals(actualAssayName));
    assertTrue(ionChannelName.equals(actualIonChannelName));
  }

  private void checkLevel2Data(final JSONArray testArray, final String[] rids) 
                               throws JSONException {
    final Set<String> checkFor = new HashSet<String>(Arrays.asList(rids));
    int level2ObjCount = 0;
    for (int i = 0; i < testArray.length(); i++) {
      final JSONObject level2Obj = (JSONObject) testArray.get(i);
      final String rid = (String) level2Obj.get("@rid");
      assertTrue(checkFor.contains(rid));
      level2ObjCount++;
    }
    assertEquals(level2ObjCount, checkFor.size());
  }

  private String getFile(final String fileName) {
    String result = "";
    try {
      result = IOUtils.toString(this.getClass().getResourceAsStream(fileName));
    } catch (IOException e) {
      e.printStackTrace();
    }
    return result;
  }
}