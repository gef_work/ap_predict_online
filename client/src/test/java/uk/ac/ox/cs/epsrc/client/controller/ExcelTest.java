/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.controller;

import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.Model;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.epsrc.client.ClientIdentifiers;
import uk.ac.ox.cs.epsrc.client.controller.Excel;
import uk.ac.ox.cs.epsrc.client.service.ClientService;
import uk.ac.ox.cs.epsrc.client.value.config.AllConfigurations;

/**
 * Unit test the Excel-generating controller.
 *
 * @author geoff
 */
public class ExcelTest {

  private AllConfigurations mockAllConfigurations;
  private Authentication mockAuthentication;
  private Excel excelController;
  private ClientService mockClientService;
  private IMocksControl mocksControl;
  private Model mockModel;
  private SecurityContext mockSecurityContext;

  private static final String dummyCompoundIdentifier = "fakeCompoundIdentifier";
  private static final String dummySimulationId = "1";
  private static final String dummyExceptionString = "fake exception";
  private static final String dummyUserId = null;

  @Before
  public void setUp() {
    excelController = new Excel();

    mocksControl = createControl();
    mockClientService = mocksControl.createMock(ClientService.class);
    ReflectionTestUtils.setField(excelController,
                                 ClientIdentifiers.COMPONENT_CLIENT_SERVICE,
                                 mockClientService);

    mockAllConfigurations = mocksControl.createMock(AllConfigurations.class);
    mockModel = mocksControl.createMock(Model.class);
    mockSecurityContext = mocksControl.createMock(SecurityContext.class);
    mockAuthentication = mocksControl.createMock(Authentication.class);
  }

  @Test
  public void testExcelViewGenerationInvalidSimulationId() {
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                                  Excel.errorInvalidSimulationId))
          .andReturn(mockModel);

    mocksControl.replay();
    final String viewName = excelController.generateExcelView(dummyCompoundIdentifier,
                                                              "fakeSimulationId",
                                                              mockModel);
    mocksControl.verify();

    assertEquals(ClientIdentifiers.VIEW_NAME_EXCEL, viewName);
  }

  @Test
  public void testExcelViewGeneration() {
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  Long.valueOf(dummySimulationId)))
          .andReturn(mockModel);
    expect(mockModel.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_COMPOUND_IDENTIFIER,
                                  dummyCompoundIdentifier))
          .andReturn(mockModel);
    SecurityContextHolder.setContext(mockSecurityContext);
    expect(mockSecurityContext.getAuthentication())
          .andReturn(mockAuthentication);
    expect(mockAuthentication.getName()).andReturn(dummyUserId);

    expect(mockClientService.retrieveDefaultConfigurations())
          .andReturn(mockAllConfigurations);
    final Map<Short, Vector<String>> dummyIonChannels = new HashMap<Short, Vector<String>>();
    expect(mockAllConfigurations.retrieveIonChannels())
          .andReturn(dummyIonChannels);
    expect(mockModel.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_CONFIG_ION_CHANNELS,
                                  dummyIonChannels))
          .andReturn(mockModel);
    expect(mockClientService.retrieveSimulationInputValues(Long.valueOf(dummySimulationId)))
          .andReturn(null);
    expect(mockModel.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_INPUT_VALUES,
                                  null))
          .andReturn(mockModel);
    expect(mockClientService.retrieveSimulationResults(Long.valueOf(dummySimulationId)))
          .andReturn(null);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                                  null))
          .andReturn(mockModel);
    expect(mockClientService.retrieveExperimentalData(Long.valueOf(dummySimulationId),
                                                      dummyUserId))
          .andReturn(null);
    expect(mockModel.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_EXPERIMENTAL_DATA,
                                  null))
          .andReturn(mockModel);

    mocksControl.replay();
    final String viewName = excelController.generateExcelView(dummyCompoundIdentifier,
                                                              dummySimulationId,
                                                              mockModel);
    mocksControl.verify();

    assertEquals(ClientIdentifiers.VIEW_NAME_EXCEL, viewName);
  }

  @Test
  public void testExcelViewGenerationExceptionCatch() {
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  Long.valueOf(dummySimulationId)))
          .andReturn(mockModel);
    expect(mockModel.addAttribute(ClientIdentifiers.MODEL_ATTRIBUTE_COMPOUND_IDENTIFIER,
                                  dummyCompoundIdentifier))
          .andReturn(mockModel);
    SecurityContextHolder.setContext(mockSecurityContext);
    expect(mockSecurityContext.getAuthentication())
          .andReturn(mockAuthentication);
    expect(mockAuthentication.getName()).andReturn(dummyUserId);

    expect(mockClientService.retrieveDefaultConfigurations())
          .andThrow(new UnsupportedOperationException(dummyExceptionString));
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                                  dummyExceptionString))
          .andReturn(mockModel);

    mocksControl.replay();
    final String viewName = excelController.generateExcelView(dummyCompoundIdentifier,
                                                              dummySimulationId,
                                                              mockModel);
    mocksControl.verify();

    assertEquals(ClientIdentifiers.VIEW_NAME_EXCEL, viewName);
  }
}