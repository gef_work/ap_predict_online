/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.client.manager;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.io.IOException;
import java.math.BigDecimal;

import org.apache.commons.io.IOUtils;
import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.client.ClientIdentifiers;
import uk.ac.ox.cs.epsrc.client.exception.BusinessManagerException;
import uk.ac.ox.cs.epsrc.client.exception.ClientException;
import uk.ac.ox.cs.epsrc.client.util.ProvenanceUtil;
import uk.ac.ox.cs.epsrc.client.value.JobDiagnosticsVO;
import uk.ac.ox.cs.epsrc.client.value.ResultsVO;
import uk.ac.ox.cs.epsrc.client.value.SimulationResponseVO;
import uk.ac.ox.cs.epsrc.client.ws.business_manager.proxy.BusinessServicesProxy;

/**
 * Unit test the business manager implementation.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { ProvenanceUtil.class } )
public class BusinessManagerManagerImplTest {
  private BusinessManagerManager businessManagerManager;
  private BusinessServicesProxy mockBusinessServicesProxy;
  private IMocksControl mocksControl;
  private JobDiagnosticsVO mockJobDiagnosticsVO;
  private long dummyJobId = 2l;
  private long dummySimulationId = 5l;
  private SimulationResponseVO mockSimulationResponseVO;

  private static String dummyJSON;

  @Before
  public void setUp() throws IOException {
    mocksControl = createStrictControl();
    businessManagerManager = new BusinessManagerManagerImpl();
    mockBusinessServicesProxy = mocksControl.createMock(BusinessServicesProxy.class);
    ReflectionTestUtils.setField(businessManagerManager, ClientIdentifiers.COMPONENT_BUSINESS_SERVICES_PROXY,
                                 mockBusinessServicesProxy);

    mockJobDiagnosticsVO = mocksControl.createMock(JobDiagnosticsVO.class);
    mockSimulationResponseVO = mocksControl.createMock(SimulationResponseVO.class);

    if (dummyJSON == null) {
      dummyJSON = IOUtils.toString(this.getClass().getResourceAsStream("provenance.json"));
    }
  }

  @Test
  public void testQueryBusinessManager() throws BusinessManagerException,
                                                ClientException {
    final String dummyCompoundIdentifier = "dummyCompoundIdentifier";
    final Boolean dummyForceReRun = Boolean.FALSE;
    final Boolean dummyReset = Boolean.TRUE;
    final Boolean dummyAssayGrouping = Boolean.FALSE;
    final Boolean dummyValueInheriting = Boolean.TRUE;
    final Boolean dummyWithinGroups = Boolean.FALSE;
    final Boolean dummyBetweenGroups = Boolean.TRUE;
    final String dummyStrategies = "dummyStrategies";
    final String dummyDoseResponseFitting = "dummyDoseResponseFitting";
    final Boolean dummydoseResponseFittingRounding = Boolean.FALSE;
    final Boolean dummyDoseResponseFittingHillMinMax = Boolean.TRUE;
    final Float dummyDoseResponseFittingHillMax = 1.1F;
    final Float dummyDoseResponseFittingHillMin = 2.2F;
    final Short dummyCellModelIdentifier = 1;
    final BigDecimal dummyPacingMaxTime = BigDecimal.TEN;
    final String dummyUserId = "dummyUserId";

    expect(mockBusinessServicesProxy.queryBusinessManager(dummyCompoundIdentifier,
                                                          dummyForceReRun,
                                                          dummyReset,
                                                          dummyAssayGrouping,
                                                          dummyValueInheriting,
                                                          dummyWithinGroups,
                                                          dummyBetweenGroups,
                                                          dummyStrategies,
                                                          dummyDoseResponseFitting,
                                                          dummydoseResponseFittingRounding,
                                                          dummyDoseResponseFittingHillMinMax,
                                                          dummyDoseResponseFittingHillMax,
                                                          dummyDoseResponseFittingHillMin,
                                                          dummyCellModelIdentifier,
                                                          dummyPacingMaxTime,
                                                          dummyUserId))
          .andReturn(mockSimulationResponseVO);

    mocksControl.replay();

    final SimulationResponseVO returned = businessManagerManager.queryBusinessManager(dummyCompoundIdentifier,
                                                                                      dummyForceReRun,
                                                                                      dummyReset,
                                                                                      dummyAssayGrouping,
                                                                                      dummyValueInheriting,
                                                                                      dummyWithinGroups,
                                                                                      dummyBetweenGroups,
                                                                                      dummyStrategies,
                                                                                      dummyDoseResponseFitting,
                                                                                      dummydoseResponseFittingRounding,
                                                                                      dummyDoseResponseFittingHillMinMax,
                                                                                      dummyDoseResponseFittingHillMax,
                                                                                      dummyDoseResponseFittingHillMin,
                                                                                      dummyCellModelIdentifier,
                                                                                      dummyPacingMaxTime,
                                                                                      dummyUserId);

    mocksControl.verify();

    assertSame(mockSimulationResponseVO, returned);
  }

  @Test
  public void testRetrieveIndividualProvenance() {
    final String dummyAssayName = "PatchXpress";
    final String dummyIonChannelName = "CaV1_2";

    expect(mockBusinessServicesProxy.retrieveSimulationProvenance(dummySimulationId))
          .andReturn(dummyJSON);
    mockStatic(ProvenanceUtil.class);
    Capture<String> captureProvenance = newCapture();

    expect(ProvenanceUtil.adjustJSON(capture(captureProvenance)))
          .andReturn("");

    replayAll();
    mocksControl.replay();

    final String provenance = businessManagerManager.retrieveSimulationProvenance(dummySimulationId,
                                                                                  dummyAssayName,
                                                                                  dummyIonChannelName);

    verifyAll();
    mocksControl.verify();

    assertEquals("{ 'a': 1, 'b': 2 }", captureProvenance.getValue());
    assertNotNull(provenance);
  }

  @Test
  public void testRetrieveJobDiagnostics() {
    expect(mockBusinessServicesProxy.retrieveJobDiagnostics(dummyJobId))
          .andReturn(mockJobDiagnosticsVO);

    mocksControl.replay();

    final JobDiagnosticsVO returnedJobDiagnostics = businessManagerManager.retrieveJobDiagnostics(dummyJobId);

    mocksControl.verify();

    assertSame(returnedJobDiagnostics, mockJobDiagnosticsVO);
  }

  @Test
  public void testRetrieveSimulationProvenance() {
    final String dummyJSON = "{\"a\":\"1\"}";
    expect(mockBusinessServicesProxy.retrieveSimulationProvenance(dummySimulationId))
          .andReturn(dummyJSON);
    mockStatic(ProvenanceUtil.class);
    expect(ProvenanceUtil.adjustJSON(dummyJSON)).andReturn(dummyJSON);

    replayAll();
    mocksControl.replay();

    final String provenance = businessManagerManager.retrieveSimulationProvenance(dummySimulationId);

    verifyAll();
    mocksControl.verify();

    assertEquals(dummyJSON, provenance);
  }

  @Test
  public void testRetrieveSimulationResults() {
    final ResultsVO mockResultsVO = mocksControl.createMock(ResultsVO.class);
    expect(mockBusinessServicesProxy.retrieveSimulationResults(dummySimulationId))
          .andReturn(mockResultsVO);

    mocksControl.replay();

    final ResultsVO resultsVO = businessManagerManager.retrieveSimulationResults(dummySimulationId);

    mocksControl.verify();

    assertSame(mockResultsVO, resultsVO);
  }
}