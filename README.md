# Action Potential prediction (`ApPredict`) online - The `AP-Portal`.

** Update July 2023 ** : `app-manager`, `client-parent`, `client-shared` and `client-direct`
are being significantly updated in the [update](https://bitbucket.org/gef_work/ap_predict_online/branch/update)
branch to the very latest library versions (e.g. jakarta, Spring 6), Thymeleaf, and Java 17. These are personal endeavours
and **not** part of active `AP-Nimbus` development.

** IMPORTANT ** : This code is generally no longer actively developed - whilst some changes will
continue to be made, activity has transferred to `AP-Nimbus` at https://github.com/CardiacModelling/ap-nimbus .

A public version of this portal **was** hosted at the University of Nottingham (legacy URL: https&#58;//cardiac.nottingham.ac.uk/ActionPotential/),
but it's been switched off and replaced with [AP-Nimbus](https://cardiac.nottingham.ac.uk/ActionPotentialPortal/)

The components below belong to the `AP-Portal` which is used to determine ion channel
IC50/pIC50/Hill Coefficient values which
[ApPredict](https://github.com/Chaste/ApPredict "ApPredict source home") processes.
The `ApPredict` simulation input values and subsequent results are displayed via the `AP-Portal`'s
web interface and can be exported.

Getting the components below working is therefore only part of the journey, `ApPredict` installation
is the first part! There are example installation processes of `ApPredict` from source on a 
[minimal Centos 6.5](https://bitbucket.org/gef_work/ap_predict_online/src/master/docs/ApPredict/install/x86_64/CentOS/6.5/minimal/)
(or [6.8](https://bitbucket.org/gef_work/ap_predict_online/src/master/docs/ApPredict/install/x86_64/CentOS/6.8/minimal/),
[7.3](https://bitbucket.org/gef_work/ap_predict_online/src/master/docs/ApPredict/install/x86_64/CentOS/7.3/minimal/)) or
a [minimal Fedora 20 MATE Live](https://bitbucket.org/gef_work/ap_predict_online/src/master/docs/ApPredict/install/x86_64/Fedora/20/MATE_Live/).
Please note as of 25th Oct. 2019 the `ApPredict` build scripts for CentOS version 7.3 (which
were also ok for 7.6 and 7.7) will no longer attempt to use the latest `Chaste` "develop" branch
because the [Chaste](https://chaste.cs.ox.ac.uk/) developers decided on 20th Jun. 2019 to 
[switch to C++14 compiler flags](https://github.com/Chaste/Chaste/commit/9778de6c95c354810d4b3a0cbb17709879eda7ae)
which are not available in CentOS 7.x. As such the build scripts are being changed today to check out
[the prior commit](https://github.com/Chaste/Chaste/commit/fa6617cd23373091eb9d215714fe6ec411cdbaab)

## Documentation :

Technical documentation : https://apportal.readthedocs.io/ .

User/Scientific documentation : https://cardiac.nottingham.ac.uk/ActionPotentialPortal/ .

## Repository content :

### `client-direct` web interface.

Java servlet web interface which talks directly (via web services) to the App Manager.  

### `client` web interface.

Java servlet web interface which talks to the Business Manager web service.

### `client-parent` build system configuration.

Maven build system `pom.xml` file for `client-shared` use.

### `client-shared` shared functionality between `client` and `client-direct`.

Shared Java code and configurations used by the `client` and `client-direct` components.

### `business-manager-api` API.

Java API to Business Manager operations.

### `business-manager` generic processing web service.

Java web service which provides generic processing of assay and QSAR ion channel data, e.g. handles summary and individual
(full-curve) data, invokes dose-response fitting (if necessary), in order to generate IC50/pIC50 values for compounds. Data
is supplied to the Business Manager via site-specific Business Manager API implementations.  
Also contains a prototype Perl script for invoking the Business Manager from, for example, a command-line.

### `site-business` demo site-specific processing.

Demonstration code (of Business Manager API implementations) representing a theoretical entity which is generating assay,
QSAR and experimental data, and how this site would process and present its own data to the Business Manager for processing.

### `app-manager` application-invocating web service.

Java web service which invokes the simulation software, `ApPredict`, monitors its progress, and returns the results to the
calling web service (usually `client-direct` or `site-business`).

### `dose-response-manager` dose-response fitting web service.

Java web service which runs the dose-response fitting code to determine IC50 values from dose-response collections and
returns the results to the calling web service (usually the `business-manager`).

### `dose-response-jni` dose-response fitting JNI.

Means by which the Java Dose-Response Manager can invoke the C++ code used to compute Dose-Response fitting curves.

### `data-api` Non-integral Data API work.

Not an integral component of ap-predict-online -- this activity is for emulating a Data API as part
of an effort to encourage sites towards developing their own APIs for ion channel data retrieval.

### Deployment diagram.

![Deployment diagram](https://bitbucket.org/gef_work/ap_predict_online/raw/master/deployment_diagram.png "Deployment diagram")

### Simplified Workflow diagram.

![Simplifed Workflow diagram](https://bitbucket.org/gef_work/ap_predict_online/raw/master/simplified_workflow_diagram.png "Simplified Workflow diagram")

### Simplified `business-manager` Workflow diagram.

![Simplied business-manager Workflow diagram](https://bitbucket.org/gef_work/ap_predict_online/raw/master/overall-business-logic-generic.png "Simplified business-manager Workflow diagram")

## Code development resources :

No longer maintained.

## Related activities :

https://github.com/Chaste/ApPredict - ApPredict - Action Potential Prediction software.

https://www.cs.ox.ac.uk/chaste/ - CHASTE - Cancer, Heart and Soft Tissue Environment.

https://scrambler.cs.ox.ac.uk/ - Cardiac Electrophysiology Web Lab

## Earlier related project work and funding :

2017-2018 : Pharmaceutical company funding.

2016-2017 : [EPSRC](https://www.epsrc.ac.uk/) (Impact Acceleration Account)

2015-2016 : [2020 Science project](https://www.cs.ox.ac.uk/projects/twenty/)

2014-2015 : [EPSRC](https://www.epsrc.ac.uk/) (Impact Acceleration Account)

2013-2014 : [NC3Rs](https://www.nc3rs.org.uk/)

2011-2012 : [EPSRC](https://www.epsrc.ac.uk/) (Knowledge Transfer Secondment)

2008-2011 : [EU preDiCT project](https://www.cs.ox.ac.uk/projects/preDiCT/) - [Source code](http://sourceforge.net/projects/predictvre/)

2005-2007 : [Integrative Biology-VRE](https://www.cs.ox.ac.uk/projects/integrativebiology/) - [Source code](https://sourceforge.net/projects/ibvre/)
