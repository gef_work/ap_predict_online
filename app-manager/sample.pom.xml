<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

  <modelVersion>4.0.0</modelVersion>

  <groupId>uk.ac.ox.cs.nc3rs</groupId>
  <artifactId>app_manager</artifactId>
  <!-- If the version is changed then also update filter.properties! -->
  <version>0.0.4</version>
  <packaging>war</packaging>

  <name>NC3Rs App Manager</name>
  <url>http://www.cs.ox.ac.uk/</url>
  <description>Application Manager for NC3Rs.</description>

  <developers>
    <developer>
      <id>geoff</id>
      <name>Geoff Williams</name>
      <email>geoff.williams@cs.ox.ac.uk</email>
      <roles>
        <role>developer</role>
      </roles>
    </developer>
  </developers>

  <issueManagement>
    <system>Bitbucket issue tracker</system>
    <url>https://bitbucket.org/gef_work/ap_predict_online/issues</url>
  </issueManagement>

  <licenses>
    <license>
      <name>BSD 3-Clause License</name>
      <url>http://opensource.org/licenses/BSD-3-Clause</url>
    </license>
  </licenses>

  <mailingLists>
    <mailingList>
      <post>geoff.williams@cs.ox.ac.uk</post>
    </mailingList>
  </mailingLists>

  <organization>
    <name>Oxford University Department of Computer Science</name>
    <url>http://www.cs.ox.ac.uk</url>
  </organization>

  <scm>
    <url>https://bitbucket.org/gef_work/ap_predict_online</url>
  </scm>

  <repositories>
    <!-- Required for com.sun.xml.wsit jars -->
    <repository>
      <id>jboss.repository.releases.public</id>
      <name>JBoss Repo Public</name>
      <url>http://repository.jboss.org/nexus/content/groups/public-jboss</url>
    </repository>
  </repositories>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <java-version>1.6</java-version>

    <cglib-version>2.2.2</cglib-version>
    <com.sun.xml.stream>1.0.1</com.sun.xml.stream>
    <commons-collections-version>3.2.1</commons-collections-version>
    <cxf.version>2.4.2</cxf.version>
    <javax.servlet-version>2.5</javax.servlet-version>
    <javax.xml.bind-version>2.0</javax.xml.bind-version>
    <easymock-version>3.4</easymock-version>
    <junit.version>4.11</junit.version>
    <log4j-version>1.2.17</log4j-version>
    <org.apache.commons.dbcp2-version>2.0.1</org.apache.commons.dbcp2-version>
    <org.apache.commons.lang3-version>3.0.1</org.apache.commons.lang3-version>
    <org.apache.commons.vfs2-version>2.0</org.apache.commons.vfs2-version>
    <org.hsqldb-version>2.3.0</org.hsqldb-version>
    <org.jasypt.spring31-version>1.9.2</org.jasypt.spring31-version>
    <org.springframework-version>3.2.12.RELEASE</org.springframework-version>
    <org.springframework.integration-version>3.0.1.RELEASE</org.springframework.integration-version>
    <org.springframework.ws-version>2.1.3.RELEASE</org.springframework.ws-version>
    <org.aspectj-version>1.6.9</org.aspectj-version>
    <powermock-version>1.6.6</powermock-version>
  </properties>

  <dependencies>
    <!-- COMPILE -->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-webmvc</artifactId>
      <version>${org.springframework-version}</version>
      <scope>compile</scope>
    </dependency>
    <!-- PROVIDED -->
    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>servlet-api</artifactId>
      <version>${javax.servlet-version}</version>
      <scope>provided</scope>
    </dependency>
    <!-- RUNTIME -->
    <dependency>
      <!-- https://jira.springsource.org/browse/SWS-752 -->
      <groupId>com.sun.xml.stream</groupId>
      <artifactId>sjsxp</artifactId>
      <version>${com.sun.xml.stream}</version>
    </dependency>
    <dependency>
      <groupId>log4j</groupId>
      <artifactId>log4j</artifactId>
      <version>${log4j-version}</version>
      <exclusions>
        <exclusion>
          <groupId>javax.mail</groupId>
          <artifactId>mail</artifactId>
        </exclusion>
        <exclusion>
          <groupId>javax.jms</groupId>
          <artifactId>jms</artifactId>
        </exclusion>
        <exclusion>
          <groupId>com.sun.jdmk</groupId>
          <artifactId>jmxtools</artifactId>
        </exclusion>
        <exclusion>
          <groupId>com.sun.jmx</groupId>
          <artifactId>jmxri</artifactId>
        </exclusion>
      </exclusions>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-dbcp2</artifactId>
      <version>${org.apache.commons.dbcp2-version}</version>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
      <version>${org.apache.commons.lang3-version}</version>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-vfs2</artifactId>
      <version>${org.apache.commons.vfs2-version}</version>
    </dependency>
    <dependency>
      <groupId>org.aspectj</groupId>
      <artifactId>aspectjrt</artifactId>
      <version>${org.aspectj-version}</version>
    </dependency> 
    <dependency>
      <groupId>org.hsqldb</groupId>
      <artifactId>hsqldb</artifactId>
      <version>${org.hsqldb-version}</version>
    </dependency>
    <dependency>
      <groupId>org.jasypt</groupId>
      <artifactId>jasypt-spring31</artifactId>
      <version>${org.jasypt.spring31-version}</version>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-orm</artifactId>
      <version>${org.springframework-version}</version>
    </dependency>
    <dependency>
      <groupId>org.springframework.integration</groupId>
      <artifactId>spring-integration-file</artifactId>
      <version>${org.springframework.integration-version}</version>
    </dependency>
    <dependency>
      <groupId>org.springframework.integration</groupId>
      <artifactId>spring-integration-jdbc</artifactId>
      <version>${org.springframework.integration-version}</version>
    </dependency>
    <dependency>
      <groupId>org.springframework.integration</groupId>
      <artifactId>spring-integration-stream</artifactId>
      <version>${org.springframework.integration-version}</version>
    </dependency>
    <dependency>
      <groupId>org.springframework.integration</groupId>
      <artifactId>spring-integration-ws</artifactId>
      <version>${org.springframework.integration-version}</version>
    </dependency>
    <dependency>
      <groupId>org.springframework.ws</groupId>
      <artifactId>spring-ws-security</artifactId>
      <version>${org.springframework.ws-version}</version>
    </dependency>
    <dependency>
      <groupId>javax.xml.bind</groupId>
      <artifactId>jaxb-api</artifactId>
      <version>${javax.xml.bind-version}</version>
    </dependency>
    <dependency>
      <groupId>cglib</groupId>
      <artifactId>cglib-nodep</artifactId>
      <version>${cglib-version}</version>
    </dependency>
    <!-- TEST -->
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>${junit.version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.easymock</groupId>
      <artifactId>easymock</artifactId>
      <version>${easymock-version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.powermock</groupId>
      <artifactId>powermock-module-junit4</artifactId>
      <version>${powermock-version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.powermock</groupId>
      <artifactId>powermock-api-easymock</artifactId>
      <version>${powermock-version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-test</artifactId>
      <version>${org.springframework-version}</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.springframework.ws</groupId>
      <artifactId>spring-ws-test</artifactId>
      <version>${org.springframework.ws-version}</version>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <profiles>
    <profile>
      <id>mvn-dev-profile</id>
      <build>
        <plugins>
        </plugins>
      </build>
    </profile>
  </profiles>

  <build>
    <filters>
      <!-- The following copy the (non-Spring) property values into xml files on maven build.  -->
      <filter>src/properties/filter.properties</filter>
      <filter>src/properties/database/database.filter.properties</filter>
    </filters>

    <resources>
      <resource>
        <directory>src/main/resources</directory>
        <filtering>true</filtering>
      </resource>
    </resources>

    <plugins>
      <plugin>
        <groupId>org.apache.cxf</groupId>
        <artifactId>cxf-codegen-plugin</artifactId>
        <version>${cxf.version}</version>
        <executions>
          <execution>
            <phase>generate-sources</phase>
            <configuration>
              <sourceRoot>src/main/java</sourceRoot>
              <wsdlOptions>
                <wsdlOption>
                  <wsdl>src/main/resources/META-INF/wsdl/business_services.wsdl</wsdl>
                </wsdlOption>
              </wsdlOptions>
            </configuration>
            <goals>
              <goal>wsdl2java</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-antrun-plugin</artifactId>
        <version>1.6</version>
        <executions>
          <execution>
            <id>create_database_properties</id>
            <phase>generate-resources</phase>
            <goals>
              <goal>run</goal>
            </goals>
            <configuration>
              <target>
                <!-- The following file is used by spring's context:property-placeholder
                     for assigning property values on application start-up. -->
                <concat destfile="src/main/resources/META-INF/properties/app_manager.properties"
                        fixlastline="yes">
                  <fileset dir="src/properties/" includes="spring.properties" />
                  <fileset dir="src/properties/database/" includes="${deploy.env}.database.${deploy.db_vendor}.properties" />
                </concat>
              </target>
            </configuration>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-checkstyle-plugin</artifactId>
        <version>2.10</version>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-clean-plugin</artifactId>
        <version>2.5</version>
        <configuration>
          <filesets>
            <fileset>
              <directory>logs</directory>
              <includes>
                <include>*.log</include>
              </includes>
              <followSymlinks>false</followSymlinks>
            </fileset>
          </filesets>
        </configuration>
      </plugin>

      <plugin>
        <artifactId>maven-resources-plugin</artifactId>
        <version>2.5</version>
        <executions>
          <execution>
            <id>copy-resources</id>
            <phase>validate</phase>
            <goals>
              <goal>copy-resources</goal>
            </goals>
            <configuration>
              <outputDirectory>src/main/resources/META-INF/properties</outputDirectory>
              <overwrite>true</overwrite>
              <resources>
                <resource>
                  <directory>src/properties</directory>
                  <includes>
                    <include>env.properties</include>
                  </includes>
                </resource>
              </resources>
            </configuration>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.jvnet.jaxb2.maven2</groupId>
        <artifactId>maven-jaxb2-plugin</artifactId>
        <version>0.8.3</version>
        <executions>
          <execution>
            <phase>generate-sources</phase>
            <goals>
              <goal>generate</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <extension>true</extension>
          <args>
            <arg>-enableIntrospection</arg>
          </args>
          <!-- doesn't seem to work..
          <enableIntrospection>true</enableIntrospection>
          -->
          <generateDirectory>src/main/java</generateDirectory>
          <generatePackage>uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb</generatePackage>
          <schemaDirectory>src/main/resources/META-INF/schema</schemaDirectory>
          <includeSchemas>
            <includeSchema>*.xsd</includeSchema>
          </includeSchemas>
          <strict>true</strict>
          <cleanPackageDirectories>true</cleanPackageDirectories>
          <verbose>true</verbose>
        </configuration>
      </plugin>


      <plugin>
        <!-- Configuration options :
               http://maven.apache.org/surefire/maven-failsafe-plugin/integration-test-mojo.html -->
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-failsafe-plugin</artifactId>
        <version>2.13</version>
        <executions>
          <execution>
            <goals>
              <goal>integration-test</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <!-- Value set via :
                 1. mvn -Dspring.profiles.active=dev ...
                 2. Eclipse > Run > Run Configurations > JRE, VM Arguments : -Dspring.profiles.active = dev -->
          <argLine>-Dspring.profiles.active=${spring.profiles.active}</argLine>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <version>2.10.3</version>
        <configuration>
          <stylesheetfile>${basedir}/src/main/javadoc/stylesheet.css</stylesheetfile>
          <show>public</show>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-source-plugin</artifactId>
        <version>2.2.1</version>
        <executions>
          <execution>
            <id>attach-sources</id>
            <phase>verify</phase>
            <goals>
              <goal>jar-no-fork</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>surefire-report-maven-plugin</artifactId>
        <version>2.14.1</version>
      </plugin>

      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>cobertura-maven-plugin</artifactId>
        <version>2.5.2</version>
        <configuration>
          <instrumentation>
            <excludes>
              <exclude>uk/ac/ox/cs/nc3rs/app_manager/AppIdentifiers.class</exclude>
              <!-- Don't test WSDL-generated code of the Business Manager web service -->
              <exclude>uk/ac/ox/cs/epsrc/business_manager/**/*.class</exclude>
            </excludes>
          </instrumentation>
          <check>
            <!-- See: http://www.artima.com/forums/flat.jsp?forum=106&thread=204677 -->
            <branchRate>50</branchRate>
            <lineRate>70</lineRate>
            <haltOnFailure>false</haltOnFailure>
            <totalBranchRate>75</totalBranchRate>
            <totalLineRate>70</totalLineRate>
            <packageLineRate>70</packageLineRate>
            <packageBranchRate>50</packageBranchRate>
            <regexes>
              <regex>
                <pattern>uk.ac.ox.cs.nc3rs.app_manager.dao.*</pattern>
                <branchRate>50</branchRate>
                <lineRate>75</lineRate>
              </regex>
            </regexes>
          </check>
        </configuration>
        <executions>
          <execution>
            <id>cobertura-clean</id>
            <phase>pre-site</phase>
            <goals>
              <goal>clean</goal>
            </goals>
          </execution>
          <execution>
            <id>cobertura-instrument</id>
            <phase>site</phase>
            <goals>
              <goal>cobertura</goal>
            </goals>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.0</version>
        <configuration>
          <source>1.6</source>
          <target>1.6</target>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-project-info-reports-plugin</artifactId>
        <version>2.8</version>
        <configuration>
          <dependencyLocationsEnabled>false</dependencyLocationsEnabled>
        </configuration>
      </plugin>
    </plugins>
  </build>

  <reporting>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <version>2.10.3</version>
        <reportSets>
          <reportSet>
            <id>html</id>
            <reports>
              <report>javadoc</report>
              <!-- <report>test-javadoc</report> -->
            </reports>
          </reportSet>
        </reportSets>
        <configuration>
          <excludePackageNames>uk.ac.ox.cs.epsrc.business_manager.ws._1:uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb</excludePackageNames>
        </configuration>
        <!--
        <configuration>
          <additionalparam>-Xdoclint:none</additionalparam>
        </configuration>
        -->
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-project-info-reports-plugin</artifactId>
        <version>2.8</version>
        <configuration>
          <dependencyLocationsEnabled>false</dependencyLocationsEnabled>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>cobertura-maven-plugin</artifactId>
        <version>2.5.2</version>
      </plugin>
    </plugins>
  </reporting>
</project>