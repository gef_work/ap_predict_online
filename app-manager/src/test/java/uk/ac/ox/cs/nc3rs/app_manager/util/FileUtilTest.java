/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.util;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.createMock;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.mockStaticPartial;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Scanner;

import javax.activation.DataHandler;

import org.apache.commons.io.IOUtils;
import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.util.FileUtil.AlphabeticComparator;
import uk.ac.ox.cs.nc3rs.app_manager.util.FileUtil.DirFilter;
import uk.ac.ox.cs.nc3rs.app_manager.value.DiagnosticsVO;

/**
 * Unit test the file utilities class.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ File.class, FileOutputStream.class, FileUtil.class,
                  IOUtils.class })
public class FileUtilTest {

  private static final String defaultTmpFilePrefix = "portal_";
  private static final String defaultTmpFileSuffix = ".tmpfile";
  private static final String diagnosticsInfoName = "VRE_INFO";
  private static final String diagnosticsOutputName = "VRE_OUTPUT";
  private static final String diagnosticsFilesRegex = "VRE_(INFO|OUTPUT)\\.\\d+.*";

  private static final long dummyAppManagerId = 7l;
  private IMocksControl mocksControl;
  private File mockFile;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockFile = mocksControl.createMock(File.class);
  }

  @Test
  public void testDelete() {
    /*
     * Null file
     */
    mocksControl.replay();

    FileUtil.delete(null);

    mocksControl.verify();

    mocksControl.reset();

    /*
     * File doesn't exist.
     */
    final String dummyFileName = "dummyFileName";
    expect(mockFile.getName()).andReturn(dummyFileName);
    expect(mockFile.exists()).andReturn(false);

    mocksControl.replay();

    FileUtil.delete(mockFile);

    mocksControl.verify();

    mocksControl.reset();

    /*
     * File exists, no list files, didn't delete
     */
    expect(mockFile.getName()).andReturn(dummyFileName);
    expect(mockFile.exists()).andReturn(true);
    expect(mockFile.listFiles()).andReturn(new File[] {});
    expect(mockFile.delete()).andReturn(false);

    mocksControl.replay();

    FileUtil.delete(mockFile);

    mocksControl.verify();

    mocksControl.reset();

    /*
     * File exists, list files (including subdir), and could delete
     */
    
    expect(mockFile.getName()).andReturn(dummyFileName);
    expect(mockFile.exists()).andReturn(true);
    final File mockFileListedFile = mocksControl.createMock(File.class);
    final File mockFileListedDir = mocksControl.createMock(File.class);

    expect(mockFile.listFiles()).andReturn(new File[] { mockFileListedFile, 
                                                        mockFileListedDir });
    final String dummyMockFileListedFileName = "dummyMockFileListedFileName";
    expect(mockFileListedFile.getName()).andReturn(dummyMockFileListedFileName);
    expect(mockFileListedFile.isDirectory()).andReturn(false);
    expect(mockFileListedFile.delete()).andReturn(true);
    final String dummyMockFileListedDirName = "dummyMockFileListedDirName";
    expect(mockFileListedDir.getName()).andReturn(dummyMockFileListedDirName);
    expect(mockFileListedDir.isDirectory()).andReturn(true);
    // >> delete()
    expect(mockFileListedDir.getName()).andReturn(dummyMockFileListedDirName);
    expect(mockFileListedDir.exists()).andReturn(true);
    final File mockFileListedDirFile = mocksControl.createMock(File.class);
    expect(mockFileListedDir.listFiles()).andReturn(new File[] { mockFileListedDirFile });
    final String dummyMockFileListedDirFileName = "dummyMockFileListedDirFileName";
    expect(mockFileListedDirFile.getName()).andReturn(dummyMockFileListedDirFileName);
    expect(mockFileListedDirFile.isDirectory()).andReturn(false);
    // Pretend couldn't delete subdir file!
    expect(mockFileListedDirFile.delete()).andReturn(false);
    expect(mockFileListedDir.delete()).andReturn(true);
    // << delete()
    expect(mockFile.delete()).andReturn(true);

    mocksControl.replay();

    FileUtil.delete(mockFile);

    mocksControl.verify();
  }

  @Test
  public void testRetrieveDiagnostics() throws Exception {
    /*
     * No sought files listed
     */
    mockStaticPartial(FileUtil.class, "retrieveJobDirectory",
                                      "retrieveFiles");

    final String dummyBaseDir = "dummyBaseDir";
    final String dummyJobDirectory = "dummyJobDirectory";
    expect(FileUtil.retrieveJobDirectory(dummyBaseDir, dummyAppManagerId))
          .andReturn(dummyJobDirectory);
    Capture<DirFilter> captureDirFilter = newCapture();
    Capture<AlphabeticComparator> captureAlphabeticComparator = newCapture();
    Capture<String> captureJobDirectory = newCapture();
    expect(FileUtil.retrieveFiles(capture(captureDirFilter),
                                  capture(captureAlphabeticComparator),
                                  capture(captureJobDirectory),
                                  eq(false)))
          .andReturn(new String[] {});

    replayAll();
    mocksControl.replay();

    DiagnosticsVO retrieved = FileUtil.retrieveDiagnostics(dummyAppManagerId,
                                                           dummyBaseDir);

    verifyAll();
    mocksControl.verify();

    assertNull(retrieved.getInfo());
    assertNull(retrieved.getOutput());
    DirFilter capturedDirFilter = captureDirFilter.getValue();
    assertEquals(diagnosticsFilesRegex, capturedDirFilter.getPattern().toString());
    String capturedJobDirectory = captureJobDirectory.getValue();
    assertEquals(dummyJobDirectory, capturedJobDirectory);

    resetAll();
    mocksControl.reset();

    /*
     * Sought file listed, but scanner throws exception
     */
    expect(FileUtil.retrieveJobDirectory(dummyBaseDir, dummyAppManagerId))
          .andReturn(dummyJobDirectory);
    String dummyVREFileName = "dummyVREFileName";
    String[] dummyVREFileList = new String[] { dummyVREFileName };
    expect(FileUtil.retrieveFiles(isA(DirFilter.class),
                                  isA(AlphabeticComparator.class),
                                  isA(String.class), eq(false)))
          .andReturn(dummyVREFileList);
    // As per code!
    String dummyVREFilePath = dummyJobDirectory.concat(dummyVREFileName);
    final File mockVREFile = mocksControl.createMock(File.class);
    expectNew(File.class, dummyVREFilePath).andReturn(mockVREFile);
    final String dummyFileNotFoundException = "dummyFileNotFoundException";
    expectNew(Scanner.class, mockVREFile)
             .andThrow(new FileNotFoundException(dummyFileNotFoundException));

    replayAll();
    mocksControl.replay();

    retrieved = FileUtil.retrieveDiagnostics(dummyAppManagerId, dummyBaseDir);

    verifyAll();
    mocksControl.verify();

    assertNull(retrieved.getInfo());
    assertNull(retrieved.getOutput());

    resetAll();
    mocksControl.reset();

    /*
     * Sought file listed, VRE info file empty.
     */
    expect(FileUtil.retrieveJobDirectory(dummyBaseDir, dummyAppManagerId))
          .andReturn(dummyJobDirectory);
    dummyVREFileName = diagnosticsInfoName.concat("dummy");
    dummyVREFileList = new String[] { dummyVREFileName };
    expect(FileUtil.retrieveFiles(isA(DirFilter.class),
                                  isA(AlphabeticComparator.class),
                                  isA(String.class), eq(false)))
          .andReturn(dummyVREFileList);
    // As per code!
    dummyVREFilePath = dummyJobDirectory.concat(dummyVREFileName);
    expectNew(File.class, dummyVREFilePath).andReturn(mockVREFile);
    final Scanner mockScanner = createMock(Scanner.class);
    expectNew(Scanner.class, mockVREFile).andReturn(mockScanner);
    expect(mockScanner.hasNextLine()).andReturn(false);
    mockScanner.close();

    replayAll();
    mocksControl.replay();

    retrieved = FileUtil.retrieveDiagnostics(dummyAppManagerId, dummyBaseDir);

    verifyAll();
    mocksControl.verify();

    assertNull(retrieved.getInfo());
    assertNull(retrieved.getOutput());

    resetAll();
    mocksControl.reset();

    /*
     * Sought file listed, VRE info file content processing.
     */
    expect(FileUtil.retrieveJobDirectory(dummyBaseDir, dummyAppManagerId))
          .andReturn(dummyJobDirectory);
    expect(FileUtil.retrieveFiles(isA(DirFilter.class),
                                  isA(AlphabeticComparator.class),
                                  isA(String.class), eq(false)))
          .andReturn(dummyVREFileList);
    expectNew(File.class, dummyVREFilePath).andReturn(mockVREFile);
    expectNew(Scanner.class, mockVREFile).andReturn(mockScanner);
    expect(mockScanner.hasNextLine()).andReturn(true);
    final String dummyLine1 = "dummyLine1";
    expect(mockScanner.nextLine()).andReturn(dummyLine1);
    expect(mockScanner.hasNextLine()).andReturn(true);
    final String dummyLine2 = "dummyLine2";
    expect(mockScanner.nextLine()).andReturn(dummyLine2);
    expect(mockScanner.hasNextLine()).andReturn(false);
    mockScanner.close();

    replayAll();
    mocksControl.replay();

    retrieved = FileUtil.retrieveDiagnostics(dummyAppManagerId, dummyBaseDir);

    verifyAll();
    mocksControl.verify();

    assertEquals(dummyLine1.concat("\n").concat(dummyLine2).concat("\n"),
                 retrieved.getInfo());
    assertNull(retrieved.getOutput());

    resetAll();
    mocksControl.reset();

    /*
     * Sought file listed, VRE output file content processing.
     */
    expect(FileUtil.retrieveJobDirectory(dummyBaseDir, dummyAppManagerId))
          .andReturn(dummyJobDirectory);
    dummyVREFileName = diagnosticsOutputName.concat("dummy");
    dummyVREFileList = new String[] { dummyVREFileName };
    expect(FileUtil.retrieveFiles(isA(DirFilter.class),
                                  isA(AlphabeticComparator.class),
                                  isA(String.class), eq(false)))
          .andReturn(dummyVREFileList);
    // As per code!
    dummyVREFilePath = dummyJobDirectory.concat(dummyVREFileName);
    expectNew(File.class, dummyVREFilePath).andReturn(mockVREFile);
    expectNew(Scanner.class, mockVREFile).andReturn(mockScanner);
    expect(mockScanner.hasNextLine()).andReturn(true);
    expect(mockScanner.nextLine()).andReturn(dummyLine1);
    expect(mockScanner.hasNextLine()).andReturn(true);
    expect(mockScanner.nextLine()).andReturn(dummyLine2);
    expect(mockScanner.hasNextLine()).andReturn(false);
    mockScanner.close();

    replayAll();
    mocksControl.replay();

    retrieved = FileUtil.retrieveDiagnostics(dummyAppManagerId, dummyBaseDir);

    verifyAll();
    mocksControl.verify();

    assertNull(retrieved.getInfo());
    assertEquals(dummyLine1.concat("\n").concat(dummyLine2).concat("\n"),
                 retrieved.getOutput());

    resetAll();
    mocksControl.reset();

    /*
     * Sought file listed, unrecognised file name!
     */
    expect(FileUtil.retrieveJobDirectory(dummyBaseDir, dummyAppManagerId))
          .andReturn(dummyJobDirectory);
    dummyVREFileName = "dummyVREFileName";
    dummyVREFileList = new String[] { dummyVREFileName };
    expect(FileUtil.retrieveFiles(isA(DirFilter.class),
                                  isA(AlphabeticComparator.class),
                                  isA(String.class), eq(false)))
          .andReturn(dummyVREFileList);
    // As per code!
    dummyVREFilePath = dummyJobDirectory.concat(dummyVREFileName);
    expectNew(File.class, dummyVREFilePath).andReturn(mockVREFile);
    expectNew(Scanner.class, mockVREFile).andReturn(mockScanner);
    expect(mockScanner.hasNextLine()).andReturn(true);
    expect(mockScanner.nextLine()).andReturn(dummyLine1);
    expect(mockScanner.hasNextLine()).andReturn(true);
    expect(mockScanner.nextLine()).andReturn(dummyLine2);
    expect(mockScanner.hasNextLine()).andReturn(false);
    mockScanner.close();

    replayAll();
    mocksControl.replay();

    try {
      retrieved = FileUtil.retrieveDiagnostics(dummyAppManagerId, dummyBaseDir);
      fail("Should not processed unrecognised VRE file name!");
    } catch (UnsupportedOperationException e) {
      assertTrue(e.getMessage().contains(diagnosticsInfoName));
      assertTrue(e.getMessage().contains(diagnosticsOutputName));
    }

    verifyAll();
    mocksControl.verify();
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testRetrieveFiles() throws Exception {
    final DirFilter mockDirFilter = mocksControl.createMock(DirFilter.class);
    final AlphabeticComparator mockAlphabeticComparator = mocksControl.createMock(AlphabeticComparator.class);
    final String dummyDirectory = "dummyDirectory";
    boolean dummyOrdered = false;

    /*
     * Fail if directory arg has a file name passed.
     */
    final File mockFile = mocksControl.createMock(File.class);
    expectNew(File.class, dummyDirectory).andReturn(mockFile);
    expect(mockFile.isDirectory()).andReturn(false);

    replayAll();
    mocksControl.replay();

    try {
      FileUtil.retrieveFiles(mockDirFilter, mockAlphabeticComparator,
                             dummyDirectory, dummyOrdered);
      fail("Expecting directory name as method arg!");
    } catch (UnsupportedOperationException e) {
      assertTrue(e.getMessage().contains(dummyDirectory));
    }

    verifyAll();
    mocksControl.verify();

    resetAll();
    mocksControl.reset();

    /*
     * Directory name passed as arg, listing not ordered.
     */
    expectNew(File.class, dummyDirectory).andReturn(mockFile);
    expect(mockFile.isDirectory()).andReturn(true);
    String[] dummyListing = new String[] { "dummyListing1", "dummyListing2" };
    expect(mockFile.list(mockDirFilter)).andReturn(dummyListing);

    replayAll();
    mocksControl.replay();

    String[] retrieved = FileUtil.retrieveFiles(mockDirFilter,
                                                mockAlphabeticComparator,
                                                dummyDirectory, dummyOrdered);

    verifyAll();
    mocksControl.verify();

    assertSame(2, retrieved.length);

    resetAll();
    mocksControl.reset();

    /*
     * Directory name passed as arg, listing ordered.
     */
    dummyOrdered = true;
    expectNew(File.class, dummyDirectory).andReturn(mockFile);
    expect(mockFile.isDirectory()).andReturn(true);
    expect(mockFile.list(mockDirFilter)).andReturn(dummyListing);
    mockStatic(Arrays.class);
    Arrays.sort(dummyListing, mockAlphabeticComparator);

    replayAll();
    mocksControl.replay();

    retrieved = FileUtil.retrieveFiles(mockDirFilter, mockAlphabeticComparator,
                                       dummyDirectory, dummyOrdered);

    verifyAll();
    mocksControl.verify();

    assertSame(2, retrieved.length);
  }

  @Test
  public void testRetrieveJobDirectory() {
    final String dummyBaseDir = "dummyBaseDir";
    String retrieved = FileUtil.retrieveJobDirectory(dummyBaseDir,
                                                     dummyAppManagerId);
    assertEquals(dummyBaseDir + dummyAppManagerId + AppIdentifiers.FILE_SEPARATOR,
                 retrieved);
  }

  @Test
  public void testWriteFileToTmp() throws Exception {
    /*
     * Default file suffix.
     */
    final DataHandler mockDataHandler = mocksControl.createMock(DataHandler.class);
    final InputStream mockInputStream = mocksControl.createMock(InputStream.class);
    expect(mockDataHandler.getInputStream()).andReturn(mockInputStream);
    final File mockFile = mocksControl.createMock(File.class);
    mockStatic(File.class);
    expect(File.createTempFile(defaultTmpFilePrefix, defaultTmpFileSuffix))
          .andReturn(mockFile);
    final FileOutputStream mockFileOutputStream = mocksControl.createMock(FileOutputStream.class);
    expectNew(FileOutputStream.class, mockFile).andReturn(mockFileOutputStream);
    final long dummyBytesCopied = 4l;
    mockStatic(IOUtils.class);
    expect(IOUtils.copyLarge(mockInputStream, mockFileOutputStream))
          .andReturn(dummyBytesCopied);
    IOUtils.closeQuietly(mockFileOutputStream);
    IOUtils.closeQuietly(mockInputStream);

    replayAll();
    mocksControl.replay();

    File file = FileUtil.writeFileToTmp(mockDataHandler, null);

    verifyAll();
    mocksControl.verify();

    assertEquals(mockFile, file);

    resetAll();
    mocksControl.reset();

    /*
     * Specified file suffix.
     */
    expect(mockDataHandler.getInputStream()).andReturn(mockInputStream);
    final String dummyTmpFileSuffix = "dummyTmpFileSuffix";
    expect(File.createTempFile(defaultTmpFilePrefix, dummyTmpFileSuffix))
          .andReturn(mockFile);
    expectNew(FileOutputStream.class, mockFile).andReturn(mockFileOutputStream);
    expect(IOUtils.copyLarge(mockInputStream, mockFileOutputStream))
          .andReturn(dummyBytesCopied);
    IOUtils.closeQuietly(mockFileOutputStream);
    IOUtils.closeQuietly(mockInputStream);

    replayAll();
    mocksControl.replay();

    file = FileUtil.writeFileToTmp(mockDataHandler, dummyTmpFileSuffix);

    verifyAll();
    mocksControl.verify();

  }
}