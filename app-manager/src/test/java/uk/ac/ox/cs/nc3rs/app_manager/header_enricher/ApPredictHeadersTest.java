/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.header_enricher;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO;
import uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAOImpl;
import uk.ac.ox.cs.nc3rs.app_manager.util.SystemUtil;
import uk.ac.ox.cs.nc3rs.app_manager.value.ApPredictVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.SimulationInfoVO;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.SetProcessStatusRequest;

/**
 * Unit test the Spring Integration message headers assignment.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ SystemUtil.class })
public class ApPredictHeadersTest {

  private AppManagerDAO mockAppManagerDAO;
  private ApPredictHeaders apPredictHeaders;
  private IMocksControl mocksControl;
  private static final long dummyAppManagerId = 3l;

  @Before
  public void setUp() {
    apPredictHeaders = new ApPredictHeaders();

    mocksControl = createStrictControl();
    mockAppManagerDAO = mocksControl.createMock(AppManagerDAO.class);

    ReflectionTestUtils.setField(apPredictHeaders, AppIdentifiers.COMPONENT_JDBC_APP_MANAGER_DAO,
                                 mockAppManagerDAO);
  }

  @Test
  public void testSetAppManagerIdHeader() {
    /*
     * 1. No id in app manager record!
     */
    final Map<String, Object> dummyAppManagerRecord = new HashMap<String, Object>();

    try {
      apPredictHeaders.setAppManagerIdHeader(dummyAppManagerRecord);
      fail("Should fail if can't find identifier.");
    } catch (IllegalStateException e) {
      assertTrue(e.getMessage().contains("No column named"));
    }

    /*
     * 2. Null value found.
     */
    dummyAppManagerRecord.put(AppManagerDAOImpl.AC_APP_MANAGER_ID, null);

    try {
      apPredictHeaders.setAppManagerIdHeader(dummyAppManagerRecord);
      fail("Should fail if can't find identifier.");
    } catch (IllegalStateException e) {
      assertTrue(e.getMessage().contains("was null"));
    }

    /*
     * 3. Found, but wrong class!
     */
    final Float dummyAppManagerIdFloat = 4.5f;
    dummyAppManagerRecord.put(AppManagerDAOImpl.AC_APP_MANAGER_ID,
                              dummyAppManagerIdFloat);

    try {
      apPredictHeaders.setAppManagerIdHeader(dummyAppManagerRecord);
      fail("Should fail if can't find identifier.");
    } catch (IllegalStateException e) {
      assertTrue(e.getMessage().contains("BigDecimal or Long"));
    }

    /*
     * 4. Found a long.
     */
    dummyAppManagerRecord.put(AppManagerDAOImpl.AC_APP_MANAGER_ID,
                              Long.valueOf(dummyAppManagerId));

    Long retrieved = apPredictHeaders.setAppManagerIdHeader(dummyAppManagerRecord);

    assertSame(dummyAppManagerId, retrieved);

    /*
     * 5. Found a BigDecimal.
     */
    final BigDecimal dummyAppManagerIdBD = BigDecimal.TEN;
    dummyAppManagerRecord.put(AppManagerDAOImpl.AC_APP_MANAGER_ID,
                              dummyAppManagerIdBD);

    retrieved = apPredictHeaders.setAppManagerIdHeader(dummyAppManagerRecord);

    assertSame(dummyAppManagerIdBD.longValue(), retrieved);
  }

  @Test
  public void testSetApPredictVOHeader() {
    /*
     * 1. No ApPredictVO in app manager record!
     */
    final Map<String, Object> dummyAppManagerRecord = new HashMap<String, Object>();

    try {
      apPredictHeaders.setApPredictVOHeader(dummyAppManagerRecord);
      fail("Should fail if can't find ApPredictVO.");
    } catch (IllegalStateException e) {
      assertTrue(e.getMessage().contains("No column named"));
    }

    /*
     * 2. Null value.
     */
    dummyAppManagerRecord.put(AppManagerDAOImpl.AC_A_OBJECT_VALUE, null);

    try {
      apPredictHeaders.setApPredictVOHeader(dummyAppManagerRecord);
      fail("Should fail if can't find ApPredictVO.");
    } catch (IllegalStateException e) {
      assertTrue(e.getMessage().contains("was null!"));
    }

    /*
     * 3. Value found.
     */
    dummyAppManagerRecord.put(AppManagerDAOImpl.AC_A_OBJECT_VALUE,
                              new byte[] { });

    mockStatic(SystemUtil.class);
    final ApPredictVO mockAPPredictVO = mocksControl.createMock(ApPredictVO.class);
    expect(SystemUtil.apPredictDeserialize(isA(byte[].class)))
          .andReturn(mockAPPredictVO);

    replayAll();

    final ApPredictVO retrieved = apPredictHeaders.setApPredictVOHeader(dummyAppManagerRecord);

    verifyAll();

    assertSame(mockAPPredictVO, retrieved);
  }

  @Test
  public void testSimulationInfo() {
    final SetProcessStatusRequest mockProcessStatusRequest = mocksControl.createMock(SetProcessStatusRequest.class);
    expect(mockProcessStatusRequest.getAppManagerId()).andReturn(dummyAppManagerId);
    final SimulationInfoVO mockSimulationInfo = mocksControl.createMock(SimulationInfoVO.class);
    expect(mockAppManagerDAO.retrieveSimulationInfo(dummyAppManagerId))
          .andReturn(mockSimulationInfo);

    mocksControl.replay();

    final SimulationInfoVO retrieved = apPredictHeaders.setSimulationInfo(mockProcessStatusRequest);

    mocksControl.verify();

    assertSame(mockSimulationInfo, retrieved);
  }
}