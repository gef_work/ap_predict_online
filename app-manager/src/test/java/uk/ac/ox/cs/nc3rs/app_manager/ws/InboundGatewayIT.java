/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.ws;

import java.io.StringReader;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.ws.MarshallingWebServiceInboundGateway;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ws.context.DefaultMessageContext;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.pox.dom.DomPoxMessage;
import org.springframework.ws.pox.dom.DomPoxMessageFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;

/**
 * Integration test the inbound gateway.
 *
 * @author Geoff Williams
 */
@ContextConfiguration(locations = { "classpath:META-INF/spring/ctx/appCtx.business.xml",
                                    "classpath:META-INF/spring/ctx/data/appCtx.database.xml",
                                    "classpath:META-INF/spring/ctx/integration/appCtx.int.xml",
                                    "classpath:META-INF/spring/ctx/ws/appCtx.ws.security-incoming.xml",
                                    "classpath:META-INF/spring/ctx/ws/appCtx.ws.security-outgoing.xml",
                                    "classpath:META-INF/spring/ctx/ws/appCtx.ws.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class InboundGatewayIT {

  @Autowired(required=true)
  private MarshallingWebServiceInboundGateway gateway;

  /*
  @Test
  public void testSendAndReceive() throws Exception {
    String xml = "<ws:InvokeRequest xmlns:ws=\"http://www.cs.ox.ac.uk/nc3rs/app_manager/ws/1\">" +
                 "  <ws:InvoRequest>apple</ws:InvoRequest>" +
                 "</ws:InvokeRequest>";

    DomPoxMessageFactory messageFactory = new DomPoxMessageFactory();
    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    documentBuilderFactory.setNamespaceAware(true);
    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
    Document document = documentBuilder.parse(new InputSource(new StringReader(xml)));
    Transformer transformer = TransformerFactory.newInstance().newTransformer();
    DomPoxMessage request = new DomPoxMessage(document, transformer, "text/xml");
    MessageContext messageContext = new DefaultMessageContext(request, messageFactory);
    gateway.invoke(messageContext);
    Object reply = messageContext.getResponse().getPayloadSource();
    assertThat(reply, is(DOMSource.class));
    DOMSource replySource = (DOMSource) reply;
    Element element = (Element) replySource.getNode().getFirstChild();
    //assertThat(element.getTagName(), equalTo("InvokeResponse"));
  }
  */

  @Test
  public void testSomething() {
    
  }

  //@Test
  public void testPubSub() throws Exception {
    String xml = "<ws:ApPredictRunRequest xmlns:ws=\"http://www.cs.ox.ac.uk/nc3rs/app_manager/ws/1\">" +
                 "  <ws:ModelIdentifier>3</ws:ModelIdentifier>" +
                 "  <ws:PacingFreq>0.5</ws:PacingFreq>" +
                 "  <ws:IC50Cal>15600</ws:IC50Cal>" +
                 "  <ws:IC50Herg>300</ws:IC50Herg>" +
                 "  <ws:IC50Na>16600</ws:IC50Na>" +
                 "  <ws:PlasmaConcs>100</ws:PlasmaConcs>" +
                 "  <ws:PlasmaConcCount>1</ws:PlasmaConcCount>" +
                 "  <ws:PlasmaConcLogScale>true</ws:PlasmaConcLogScale>" +
                 "</ws:ApPredictRunRequest>";

    DomPoxMessageFactory messageFactory = new DomPoxMessageFactory();
    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    documentBuilderFactory.setNamespaceAware(true);
    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
    Document document = documentBuilder.parse(new InputSource(new StringReader(xml)));
    Transformer transformer = TransformerFactory.newInstance().newTransformer();
    DomPoxMessage request = new DomPoxMessage(document, transformer, "text/xml");
    MessageContext messageContext = new DefaultMessageContext(request, messageFactory);
    gateway.invoke(messageContext);
    gateway.invoke(messageContext);
    Thread.sleep(5000);
    //Object reply = messageContext.getResponse().getPayloadSource();
    //assertThat(reply, is(DOMSource.class));
    //DOMSource replySource = (DOMSource) reply;
    //Element element = (Element) replySource.getNode().getFirstChild();
    //assertThat(element.getTagName(), equalTo("InvokeResponse"));
  }
}
