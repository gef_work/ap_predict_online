/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.value;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

/**
 * Unit test the voltage_results.dat data value object.
 *
 * @author geoff
 */
public class VoltageResultsVOTest {

  @Test
  public void testEverything() {
    String dummyCmpdConc = null;
    String dummyUpstrokeVelocity = null;
    String dummyPeakVm = null;
    String dummyAPD50 = null;
    String dummyAPD90 = null;
    String dummyDeltaAPD90 = null;

    VoltageResultsVO results = new VoltageResultsVO(dummyCmpdConc,
                                                    dummyUpstrokeVelocity,
                                                    dummyPeakVm, dummyAPD50,
                                                    dummyAPD90, dummyDeltaAPD90);

    assertNull(results.getCmpdConc());
    assertNull(results.getUpstrokeVelocity());
    assertNull(results.getPeakVm());
    assertNull(results.getAPD50());
    assertNull(results.getAPD90());
    assertNull(results.getDeltaAPD90());

    dummyCmpdConc = "dummyCmpdConc";
    dummyUpstrokeVelocity = "dummyUpstrokeVelocity";
    dummyPeakVm = "dummyPeakVm";
    dummyAPD50 = "dummyAPD50";
    dummyAPD90 = "dummyAPD90";
    dummyDeltaAPD90 = "dummyDeltaAPD90";

    results = new VoltageResultsVO(dummyCmpdConc, dummyUpstrokeVelocity,
                                   dummyPeakVm, dummyAPD50, dummyAPD90,
                                   dummyDeltaAPD90);

    assertEquals(dummyCmpdConc, results.getCmpdConc());
    assertEquals(dummyUpstrokeVelocity, results.getUpstrokeVelocity());
    assertEquals(dummyPeakVm, results.getPeakVm());
    assertEquals(dummyAPD50, results.getAPD50());
    assertEquals(dummyAPD90, results.getAPD90());
    assertEquals(dummyDeltaAPD90, results.getDeltaAPD90());
  }
}