/*

  Copyright (c) 2016, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.value;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

/**
 *
 *
 * @author geoff
 */
public class RequestPlasmaDataTest {

  private RequestPlasmaData requestPlasmaData;

  @Before
  public void setUp() {
    requestPlasmaData = new RequestPlasmaData();
  }

  @Test
  public void testUninitialised() {
    assertNull(requestPlasmaData.getPlasmaConcCount());
    assertNull(requestPlasmaData.getPlasmaConcHigh());
    assertNull(requestPlasmaData.getPlasmaConcLogScale());
    assertNull(requestPlasmaData.getPlasmaConcLow());
    assertNull(requestPlasmaData.getPlasmaConcs());
    assertFalse(requestPlasmaData.isConcCountAssigned());
    assertFalse(requestPlasmaData.isConcLogScaleAssigned());
    assertFalse(requestPlasmaData.isRangeAssigned());
    try {
      requestPlasmaData.validate();
      fail("Should have failed on range not being assigned.");
    } catch (IllegalStateException e) {}
  }

  @Test
  public void testConcCountAssignment() {
    assertFalse(requestPlasmaData.isConcCountAssigned());
    Short dummyPlasmaConcCount = null;
    // Plasma conc count is considered assigned, even if a null value has been passed!
    requestPlasmaData.setPlasmaConcCount(dummyPlasmaConcCount);
    assertTrue(requestPlasmaData.isConcCountAssigned());
    assertNull(requestPlasmaData.getPlasmaConcCount());
    try {
      requestPlasmaData.setPlasmaConcCount(dummyPlasmaConcCount);
      fail("Should not permit reassignment of plasma concentration count");
    } catch (IllegalArgumentException e) {}

    requestPlasmaData = new RequestPlasmaData();
    dummyPlasmaConcCount = 1;
    requestPlasmaData.setPlasmaConcCount(dummyPlasmaConcCount);
    assertTrue(requestPlasmaData.isConcCountAssigned());
    assertEquals(dummyPlasmaConcCount, requestPlasmaData.getPlasmaConcCount());
  }

  @Test
  public void testConcLogScaleAssigned() {
    assertFalse(requestPlasmaData.isConcLogScaleAssigned());
    Boolean dummyPlasmaConcLogScale = null;
    // Plasma conc log scale is considered assigned, even if a null value has been passed!
    requestPlasmaData.setPlasmaConcLogScale(dummyPlasmaConcLogScale);
    assertTrue(requestPlasmaData.isConcLogScaleAssigned());
    assertNull(requestPlasmaData.getPlasmaConcLogScale());
    try {
      requestPlasmaData.setPlasmaConcLogScale(dummyPlasmaConcLogScale);
      fail("Should not permit reassignment of plasma conc log scale");
    } catch (IllegalArgumentException e) {}

    requestPlasmaData = new RequestPlasmaData();
    dummyPlasmaConcLogScale = Boolean.TRUE;
    requestPlasmaData.setPlasmaConcLogScale(dummyPlasmaConcLogScale);
    assertTrue(requestPlasmaData.isConcLogScaleAssigned());
    assertEquals(dummyPlasmaConcLogScale, requestPlasmaData.getPlasmaConcLogScale());
    requestPlasmaData = new RequestPlasmaData();

    dummyPlasmaConcLogScale = Boolean.FALSE;
    requestPlasmaData.setPlasmaConcLogScale(dummyPlasmaConcLogScale);
    assertTrue(requestPlasmaData.isConcLogScaleAssigned());
    assertEquals(dummyPlasmaConcLogScale, requestPlasmaData.getPlasmaConcLogScale());
  }

  @Test
  public void testSettingRangePlasmaConcs() {
    String dummyPlasmaConcs = null;
    BigDecimal dummyPlasmaConcHigh = null;
    BigDecimal dummyPlasmaConcLow = null;
    try {
      requestPlasmaData.setRange(dummyPlasmaConcs, dummyPlasmaConcHigh, dummyPlasmaConcLow);
      fail("Should insist on at least either plasma concentrations or a high concentration");
    } catch (IllegalArgumentException e) {}

    dummyPlasmaConcs = "";
    try {
      requestPlasmaData.setRange(dummyPlasmaConcs, dummyPlasmaConcHigh, dummyPlasmaConcLow);
      fail("Should insist on at least either plasma concentrations or a high concentration");
    } catch (IllegalArgumentException e) {}

    dummyPlasmaConcs = "1";
    requestPlasmaData.setRange(dummyPlasmaConcs, dummyPlasmaConcHigh, dummyPlasmaConcLow);
    requestPlasmaData.validate(); // Should not throw IllegalStateException!
    assertEquals(dummyPlasmaConcs, requestPlasmaData.getPlasmaConcs());

    try {
      requestPlasmaData.setRange(dummyPlasmaConcs, dummyPlasmaConcHigh, dummyPlasmaConcLow);
      fail("Should not allow reassignment of range");
    } catch (IllegalArgumentException e) {}

    requestPlasmaData = new RequestPlasmaData();

    dummyPlasmaConcs = "1,2,3";
    requestPlasmaData.setRange(dummyPlasmaConcs, dummyPlasmaConcHigh, dummyPlasmaConcLow);
    assertEquals(dummyPlasmaConcs, requestPlasmaData.getPlasmaConcs());

    requestPlasmaData = new RequestPlasmaData();

    dummyPlasmaConcs = "3,2,1";
    requestPlasmaData.setRange(dummyPlasmaConcs, dummyPlasmaConcHigh, dummyPlasmaConcLow);
    assertEquals("1,2,3", requestPlasmaData.getPlasmaConcs());
  }

  @Test
  public void testSettingRangeHighLow() {
    String dummyPlasmaConcs = null;
    BigDecimal dummyPlasmaConcHigh = null;
    BigDecimal dummyPlasmaConcLow = null;

    dummyPlasmaConcHigh = new BigDecimal("-1");
    try {
      requestPlasmaData.setRange(dummyPlasmaConcs, dummyPlasmaConcHigh, dummyPlasmaConcLow);
      fail("Should not permit assignment of negative high plasma conc.");
    } catch (IllegalArgumentException e) {}

    requestPlasmaData = new RequestPlasmaData();

    dummyPlasmaConcHigh = BigDecimal.ZERO;
    requestPlasmaData.setRange(dummyPlasmaConcs, dummyPlasmaConcHigh, dummyPlasmaConcLow);
    assertTrue(dummyPlasmaConcHigh.compareTo(requestPlasmaData.getPlasmaConcHigh()) == 0);
    assertNull(requestPlasmaData.getPlasmaConcLow());

    requestPlasmaData = new RequestPlasmaData();

    dummyPlasmaConcHigh = BigDecimal.ONE;
    requestPlasmaData.setRange(dummyPlasmaConcs, dummyPlasmaConcHigh, dummyPlasmaConcLow);
    assertTrue(dummyPlasmaConcHigh.compareTo(requestPlasmaData.getPlasmaConcHigh()) == 0);
    assertNull(requestPlasmaData.getPlasmaConcLow());

    requestPlasmaData = new RequestPlasmaData();

    dummyPlasmaConcHigh = BigDecimal.ONE;
    dummyPlasmaConcLow = new BigDecimal("-1");
    try {
      requestPlasmaData.setRange(dummyPlasmaConcs, dummyPlasmaConcHigh, dummyPlasmaConcLow);
      fail("Should not permit assignment of negative low plasma conc.");
    } catch (IllegalArgumentException e) {}

    requestPlasmaData = new RequestPlasmaData();

    dummyPlasmaConcLow = BigDecimal.TEN;
    try {
      requestPlasmaData.setRange(dummyPlasmaConcs, dummyPlasmaConcHigh, dummyPlasmaConcLow);
      fail("Should not permit assignment of low concentration value higher than the high value");
    } catch (IllegalArgumentException e) {}

    requestPlasmaData = new RequestPlasmaData();

    dummyPlasmaConcLow = BigDecimal.ZERO;
    requestPlasmaData.setRange(dummyPlasmaConcs, dummyPlasmaConcHigh, dummyPlasmaConcLow);
    assertTrue(dummyPlasmaConcLow.compareTo(requestPlasmaData.getPlasmaConcLow()) == 0);
  }

}