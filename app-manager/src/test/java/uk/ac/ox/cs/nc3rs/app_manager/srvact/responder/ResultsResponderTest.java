/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.responder;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO;
import uk.ac.ox.cs.nc3rs.app_manager.domain.PKResult;
import uk.ac.ox.cs.nc3rs.app_manager.domain.SimulationResult;
import uk.ac.ox.cs.nc3rs.app_manager.value.AllSimulationDomainResultsVO;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.PKResults;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.Results;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.RetrieveAllResultsRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.RetrieveAllResultsResponse;

/**
 * Unit test the simulation results responder.
 *
 * @author geoff
 */
public class ResultsResponderTest {

  private static final long dummyAppManagerId = 4l;

  private AppManagerDAO mockAppManagerDAO;
  private IMocksControl mocksControl;
  private ResultsResponder resultsResponder;
  private RetrieveAllResultsRequest mockRetrieveAllResultsRequest;

  @Before
  public void setUp() {
    resultsResponder = new ResultsResponder();

    mocksControl = createStrictControl();
    mockAppManagerDAO = mocksControl.createMock(AppManagerDAO.class);
    mockRetrieveAllResultsRequest = mocksControl.createMock(RetrieveAllResultsRequest.class);

    ReflectionTestUtils.setField(resultsResponder,
                                 AppIdentifiers.COMPONENT_JDBC_APP_MANAGER_DAO,
                                 mockAppManagerDAO);
  }

  @Test
  public void testHandleRequest() {
    /*
     * No simulation messages nor simulation results present.
     */
    expect(mockRetrieveAllResultsRequest.getAppManagerId())
          .andReturn(dummyAppManagerId);
    String dummyMessages = null;
    expect(mockAppManagerDAO.retrieveSimulationMessages(dummyAppManagerId))
          .andReturn(dummyMessages);
    final AllSimulationDomainResultsVO mockAllSimulationDomainResults = mocksControl.createMock(AllSimulationDomainResultsVO.class);
    expect(mockAppManagerDAO.retrieveAllSimulationResults(dummyAppManagerId))
          .andReturn(mockAllSimulationDomainResults);
    final String dummyDeltaAPD90PctileNames = "dummyDeltaAPD90PctileNames";
    expect(mockAllSimulationDomainResults.getDeltaAPDPercentileNames())
          .andReturn(dummyDeltaAPD90PctileNames);
    expect(mockAllSimulationDomainResults.getSimulationResults())
          .andReturn(new ArrayList<SimulationResult>());
    expect(mockAllSimulationDomainResults.getPkResults())
          .andReturn(new ArrayList<PKResult>());

    mocksControl.replay();

    RetrieveAllResultsResponse response = resultsResponder.handleRequest(mockRetrieveAllResultsRequest);

    mocksControl.verify();

    assertSame(dummyAppManagerId, response.getAppManagerId());
    assertNull(response.getMessages());
    assertTrue(response.getResults().isEmpty());
    assertEquals(dummyDeltaAPD90PctileNames,
                 response.getDeltaAPD90PercentileNames());

    mocksControl.reset();

    /*
     * Simulation messages but no simulation results present.
     */
    expect(mockRetrieveAllResultsRequest.getAppManagerId())
          .andReturn(dummyAppManagerId);
    dummyMessages = "dummyMessages";
    expect(mockAppManagerDAO.retrieveSimulationMessages(dummyAppManagerId))
          .andReturn(dummyMessages);
    expect(mockAppManagerDAO.retrieveAllSimulationResults(dummyAppManagerId))
          .andReturn(mockAllSimulationDomainResults);
    expect(mockAllSimulationDomainResults.getDeltaAPDPercentileNames())
          .andReturn(dummyDeltaAPD90PctileNames);
    expect(mockAllSimulationDomainResults.getSimulationResults())
          .andReturn(new ArrayList<SimulationResult>());
    expect(mockAllSimulationDomainResults.getPkResults())
          .andReturn(new ArrayList<PKResult>());

    mocksControl.replay();

    response = resultsResponder.handleRequest(mockRetrieveAllResultsRequest);

    mocksControl.verify();

    assertSame(dummyAppManagerId, response.getAppManagerId());
    assertSame(dummyMessages, response.getMessages());
    assertTrue(response.getResults().isEmpty());

    mocksControl.reset();

    /*
     * Simulation messages and simulation results present.
     */
    expect(mockRetrieveAllResultsRequest.getAppManagerId())
          .andReturn(dummyAppManagerId);
    expect(mockAppManagerDAO.retrieveSimulationMessages(dummyAppManagerId))
          .andReturn(dummyMessages);
    // Simulation results.
    expect(mockAppManagerDAO.retrieveAllSimulationResults(dummyAppManagerId))
          .andReturn(mockAllSimulationDomainResults);
    final SimulationResult mockSimulationResult1 = mocksControl.createMock(SimulationResult.class);
    final SimulationResult mockSimulationResult2 = mocksControl.createMock(SimulationResult.class);
    final List<SimulationResult> dummySimulationResults = new ArrayList<SimulationResult>();
    dummySimulationResults.add(mockSimulationResult1);
    dummySimulationResults.add(mockSimulationResult2);
    expect(mockAllSimulationDomainResults.getDeltaAPDPercentileNames())
          .andReturn(dummyDeltaAPD90PctileNames);
    expect(mockAllSimulationDomainResults.getSimulationResults())
          .andReturn(dummySimulationResults);
    final BigDecimal dummyCmpdConc1 = BigDecimal.ONE;
    final String dummyTimes1 = "dummyTimes1";
    final String dummyVoltages1 = "dummyVoltages1";
    final String dummyAPD901 = "dummyAPD901";
    final String dummyDeltaAPD901 = "dummyDeltaAPD901";
    final String dummyQNet1 = "dummyQNet1";
    expect(mockSimulationResult1.getCmpdConc()).andReturn(dummyCmpdConc1);
    expect(mockSimulationResult1.getTimes()).andReturn(dummyTimes1);
    expect(mockSimulationResult1.getVoltages()).andReturn(dummyVoltages1);
    expect(mockSimulationResult1.getAPD90()).andReturn(dummyAPD901);
    expect(mockSimulationResult1.getDeltaAPD90()).andReturn(dummyDeltaAPD901);
    expect(mockSimulationResult1.getQNet()).andReturn(dummyQNet1);
    final BigDecimal dummyCmpdConc2 = BigDecimal.TEN;
    final String dummyTimes2 = "dummyTimes2";
    final String dummyVoltages2 = "dummyVoltages2";
    final String dummyAPD902 = "dummyAPD902";
    final String dummyDeltaAPD902 = "dummyDeltaAPD902";
    final String dummyQNet2 = "dummyQNet2";
    expect(mockSimulationResult2.getCmpdConc()).andReturn(dummyCmpdConc2);
    expect(mockSimulationResult2.getTimes()).andReturn(dummyTimes2);
    expect(mockSimulationResult2.getVoltages()).andReturn(dummyVoltages2);
    expect(mockSimulationResult2.getAPD90()).andReturn(dummyAPD902);
    expect(mockSimulationResult2.getDeltaAPD90()).andReturn(dummyDeltaAPD902);
    expect(mockSimulationResult2.getQNet()).andReturn(dummyQNet2);
    // PK results
    final PKResult mockPKResult1 = mocksControl.createMock(PKResult.class);
    final PKResult mockPKResult2 = mocksControl.createMock(PKResult.class);
    final List<PKResult> dummyPKResults = new ArrayList<PKResult>();
    dummyPKResults.add(mockPKResult1);
    dummyPKResults.add(mockPKResult2);
    expect(mockAllSimulationDomainResults.getPkResults())
          .andReturn(dummyPKResults);
    final BigDecimal dummyTimepoint1 = BigDecimal.ZERO;
    final String dummyAPD90s1 = "dummyAPD90s1";
    expect(mockPKResult1.getTimepoint()).andReturn(dummyTimepoint1);
    expect(mockPKResult1.getApd90s()).andReturn(dummyAPD90s1);
    final BigDecimal dummyTimepoint2 = BigDecimal.TEN;
    final String dummyAPD90s2 = "dummyAPD90s2";
    expect(mockPKResult2.getTimepoint()).andReturn(dummyTimepoint2);
    expect(mockPKResult2.getApd90s()).andReturn(dummyAPD90s2);

    mocksControl.replay();

    response = resultsResponder.handleRequest(mockRetrieveAllResultsRequest);

    mocksControl.verify();

    assertSame(dummyAppManagerId, response.getAppManagerId());
    assertSame(dummyMessages, response.getMessages());
    final List<Results> results = response.getResults();
    assertEquals(2, results.size());
    final Results results1 = results.get(0);
    assertTrue(dummyCmpdConc1.compareTo(results1.getReference()) == 0);
    assertEquals(dummyTimes1, results1.getTimes());
    assertEquals(dummyVoltages1, results1.getVoltages());
    assertEquals(dummyAPD901, results1.getAPD90());
    assertEquals(dummyDeltaAPD901, results1.getDeltaAPD90());
    assertEquals(dummyQNet1, results1.getQNet());
    final Results results2 = results.get(1);
    assertTrue(dummyCmpdConc2.compareTo(results2.getReference()) == 0);
    assertEquals(dummyTimes2, results2.getTimes());
    assertEquals(dummyVoltages2, results2.getVoltages());
    assertEquals(dummyAPD902, results2.getAPD90());
    assertEquals(dummyDeltaAPD902, results2.getDeltaAPD90());
    assertEquals(dummyQNet2, results2.getQNet());
    final List<PKResults> pkResults = response.getPKResults();
    assertEquals(2, pkResults.size());
    final PKResults pkResults1 = pkResults.get(0);
    assertTrue(dummyTimepoint1.compareTo(pkResults1.getTimepoint()) == 0);
    assertEquals(dummyAPD90s1, pkResults1.getAPD90S());
    final PKResults pkResults2 = pkResults.get(1);
    assertTrue(dummyTimepoint2.compareTo(pkResults2.getTimepoint()) == 0);
    assertEquals(dummyAPD90s2, pkResults2.getAPD90S());
  }
}