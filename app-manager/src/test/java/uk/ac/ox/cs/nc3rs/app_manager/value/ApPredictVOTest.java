/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the names of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.value;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;
import java.util.Set;

import org.junit.Test;

import uk.ac.ox.cs.nc3rs.app_manager.value.ApPredictVO.CHANNEL_CURRENT;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.AssociatedItem;

/**
 * Unit test ApPredict value object.
 *
 * @author geoff
 */
public class ApPredictVOTest extends AbstractApPredictVOTest {

  @Test
  public void testCellMLFile() throws Exception {
    final String dummyCellMLFileName = "dummyCellMLFileName";
    final String dummyCellMLFileLocation = "dummyCellMLFileLocation";
    // For simplicity (i.e. to avoid concs data assignment), it's a PK sim!
    dummyPKFileLocation = "dummyPKFileLocation";

    expect(mockFileCellML.getName()).andReturn(dummyCellMLFileName);
    expect(mockFileCellML.getAbsolutePath()).andReturn(dummyCellMLFileLocation);
    expect(mockRequest.getPacingFreq()).andReturn(null);
    expect(mockRequest.getPacingMaxTime()).andReturn(null);
    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    noChannelData();

    replayAll();
    mocksControl.replay();

    final ApPredictVO apPredictVO = new ApPredictVO(mockRequest,
                                                    dummyPKFileLocation,
                                                    mockFileCellML);

    verifyAll();
    mocksControl.verify();

    assertEquals(dummyCellMLFileName, apPredictVO.getCellMLFileName());
    assertEquals(dummyCellMLFileLocation, apPredictVO.getCellMLFileLocation());
    assertTrue(apPredictVO.isPKSimulation());
  }

  @Test
  public void testCredibleIntervals() throws Exception {
    /*
     * No credible intervals in request.
     */
    expect(mockRequest.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockRequest.getPacingFreq()).andReturn(null);
    expect(mockRequest.getPacingMaxTime()).andReturn(null);
    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    noChannelData();
    extractPlasmaData();

    replayAll();
    mocksControl.replay();

    ApPredictVO apPredictVO = new ApPredictVO(mockRequest, dummyPKFileLocation,
                                              null);

    verifyAll();
    mocksControl.verify();

    assertNull(apPredictVO.getCredibleIntervalPercentiles());
    assertFalse(apPredictVO.isCredibleIntervals());

    resetAll();
    mocksControl.reset();

    /*
     * Credible intervals in request, but don't calculate instruction.
     */
    expect(mockRequest.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockRequest.getPacingFreq()).andReturn(null);
    expect(mockRequest.getPacingMaxTime()).andReturn(null);
    expect(mockRequest.getCredibleIntervals()).andReturn(mockCredibleIntervals);
    dummyCalculateCredibleIntervals = false;
    expect(mockCredibleIntervals.getCalculateCredibleIntervals())
          .andReturn(dummyCalculateCredibleIntervals);
    noChannelData();
    extractPlasmaData();

    replayAll();
    mocksControl.replay();

    apPredictVO = new ApPredictVO(mockRequest, dummyPKFileLocation, null);

    verifyAll();
    mocksControl.verify();

    assertNull(apPredictVO.getCredibleIntervalPercentiles());
    assertFalse(apPredictVO.isCredibleIntervals());

    resetAll();
    mocksControl.reset();

    /*
     * Credible intervals in request, calculate instruction.
     */
    expect(mockRequest.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockRequest.getPacingFreq()).andReturn(null);
    expect(mockRequest.getPacingMaxTime()).andReturn(null);
    expect(mockRequest.getCredibleIntervals()).andReturn(mockCredibleIntervals);
    dummyCalculateCredibleIntervals = true;
    expect(mockCredibleIntervals.getCalculateCredibleIntervals())
          .andReturn(dummyCalculateCredibleIntervals);
    dummyPctiles = "dummyPctiles";
    expect(mockCredibleIntervals.getPercentiles()).andReturn(dummyPctiles);

    noChannelData();
    extractPlasmaData();

    replayAll();
    mocksControl.replay();

    apPredictVO = new ApPredictVO(mockRequest, dummyPKFileLocation, null);

    verifyAll();
    mocksControl.verify();

    assertEquals(dummyPctiles, apPredictVO.getCredibleIntervalPercentiles());
    assertTrue(apPredictVO.isCredibleIntervals());
  }

  @Test
  public void testManyAssociatedItemsForICaL() throws Exception {
    dummyPacingMaxTime = BigDecimal.TEN;
    dummyPacingFrequency = BigDecimal.ONE;

    preparePacing();
    expect(mockRequest.getCredibleIntervals()).andReturn(mockCredibleIntervals);
    dummyCalculateCredibleIntervals = true;
    expect(mockCredibleIntervals.getCalculateCredibleIntervals())
          .andReturn(dummyCalculateCredibleIntervals);
    dummyPctiles = "dummyPctiles";
    expect(mockCredibleIntervals.getPercentiles()).andReturn(dummyPctiles);
    // >> extractChannelData()
    // ICaL associated item 1
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    final AssociatedItem mockAssociatedItemICaL2 = mocksControl.createMock(AssociatedItem.class);
    dummyIC50DataICaL.add(mockAssociatedItemICaL);
    dummyIC50DataICaL.add(mockAssociatedItemICaL2);

    expect(mockChannelDataICaL.getIC50Data()).andReturn(dummyIC50DataICaL);     // <-- IC50 ICaL data
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(null);
    dummyC50SpreadICaL = dummyArbitrarySpread;
    expect(mockChannelDataICaL.getC50Spread()).andReturn(dummyC50SpreadICaL);   // <-- IC spread is good
    dummyHillSpreadICaL = dummyArbitrarySpread;
    expect(mockChannelDataICaL.getHillSpread()).andReturn(dummyHillSpreadICaL); // <-- Hill spread is good
    dummyAssociatedItemICaLC50 = dummyArbitraryIC50;                            // <-- IC50 is good
    expect(mockAssociatedItemICaL.getC50())
          .andReturn(dummyAssociatedItemICaLC50);
    dummyAssociatedItemICaLHill = dummyArbitraryHill;                           // <-- Hill is good
    expect(mockAssociatedItemICaL.getHill())
          .andReturn(dummyAssociatedItemICaLHill);
    dummyAssociatedItemICaLSaturation = dummyArbitrarySaturation;               // <-- Saturation is good
    expect(mockAssociatedItemICaL.getSaturation())
          .andReturn(dummyAssociatedItemICaLSaturation);
    dummyAssociatedDataICaL.add(mockAssociatedDataICaL);
    expectNew(AssociatedData.class, (BigDecimal) null,
                                    dummyAssociatedItemICaLC50,
                                    dummyAssociatedItemICaLHill,
                                    dummyAssociatedItemICaLSaturation)
             .andReturn(mockAssociatedDataICaL);

    // ICaL associated item 2
    expect(mockAssociatedItemICaL2.getC50())
          .andReturn(dummyAssociatedItemICaLC50);
    dummyAssociatedItemICaLHill = dummyArbitraryHill;                           // <-- Hill is good
    expect(mockAssociatedItemICaL2.getHill())
          .andReturn(dummyAssociatedItemICaLHill);
    dummyAssociatedItemICaLSaturation = dummyArbitrarySaturation;               // <-- Saturation is good
    expect(mockAssociatedItemICaL2.getSaturation())
          .andReturn(dummyAssociatedItemICaLSaturation);
    final AssociatedData mockAssociatedDataICaL2 = mocksControl.createMock(AssociatedData.class);
    dummyAssociatedDataICaL.add(mockAssociatedDataICaL2);
    expectNew(AssociatedData.class, (BigDecimal) null,
                                    dummyAssociatedItemICaLC50,
                                    dummyAssociatedItemICaLHill,
                                    dummyAssociatedItemICaLSaturation)
             .andReturn(mockAssociatedDataICaL2);
    
    // << extractChannelData()
    expectNew(RequestChannelData.class, dummyAssociatedDataICaL,
                                        dummyC50SpreadICaL,
                                        dummyHillSpreadICaL)
             .andReturn(mockRequestChannelDataICaL);
    expect(mockRequest.getIKr()).andReturn(null);
    expect(mockRequest.getIK1()).andReturn(null);
    expect(mockRequest.getIKs()).andReturn(null);
    expect(mockRequest.getIto()).andReturn(null);
    expect(mockRequest.getINa()).andReturn(null);
    expect(mockRequest.getINaL()).andReturn(null);

    extractPlasmaData();

    mocksControl.replay();
    replayAll();

    new ApPredictVO(mockRequest, dummyPKFileLocation, null);

    mocksControl.verify();
    verifyAll();
  }

  @Test
  public void testManyChannelDataWithoutSpreads() throws Exception {
    dummyPacingMaxTime = BigDecimal.TEN;
    dummyPacingFrequency = BigDecimal.ONE;

    preparePacing();
    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    // ICaL
    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    dummyIC50DataICaL.add(mockAssociatedItemICaL);
    expect(mockChannelDataICaL.getIC50Data()).andReturn(dummyIC50DataICaL);     // <-- IC50 ICaL data
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(null);
    expect(mockChannelDataICaL.getC50Spread()).andReturn(null);                 // <-- No IC spread
    expect(mockChannelDataICaL.getHillSpread()).andReturn(null);                // <-- No Hill spread
    dummyAssociatedItemICaLC50 = dummyArbitraryIC50;                            // <-- IC50 is good
    expect(mockAssociatedItemICaL.getC50())
          .andReturn(dummyAssociatedItemICaLC50);
    expect(mockAssociatedItemICaL.getHill()).andReturn(null);                   // <-- No Hill
    dummyAssociatedItemICaLSaturation = dummyArbitrarySaturation;               // <-- Saturation is good
    expect(mockAssociatedItemICaL.getSaturation())
          .andReturn(dummyAssociatedItemICaLSaturation);
    dummyAssociatedDataICaL.add(mockAssociatedDataICaL);
    expectNew(AssociatedData.class, (BigDecimal) null,
                                    dummyAssociatedItemICaLC50,
                                    dummyAssociatedItemICaLHill,
                                    dummyAssociatedItemICaLSaturation)
             .andReturn(mockAssociatedDataICaL);
    // << extractChannelData()
    expectNew(RequestChannelData.class, dummyAssociatedDataICaL,
                                        dummyC50SpreadICaL,
                                        dummyHillSpreadICaL)
             .andReturn(mockRequestChannelDataICaL);

    // IKr
    // >> extractChannelData()
    expect(mockRequest.getIKr()).andReturn(mockChannelDataIKr);
    dummyPIC50DataIKr.add(mockAssociatedItemIKr);
    expect(mockChannelDataIKr.getIC50Data()).andReturn(null);
    expect(mockChannelDataIKr.getPIC50Data()).andReturn(dummyPIC50DataIKr);     // <-- pIC50 IKr data
    expect(mockChannelDataIKr.getC50Spread()).andReturn(dummyC50SpreadIKr);     // <-- No IC spread
    expect(mockChannelDataIKr.getHillSpread()).andReturn(dummyHillSpreadIKr);   // <-- No Hill spread
    dummyAssociatedItemIKrC50 = dummyArbitraryPIC50;                            // <-- pIC50 is good
    expect(mockAssociatedItemIKr.getC50())
          .andReturn(dummyAssociatedItemIKrC50);
    expect(mockAssociatedItemIKr.getHill()).andReturn(null);                    // <-- No Hill
    dummyAssociatedItemIKrSaturation = dummyArbitrarySaturation;                // <-- Saturation is good
    expect(mockAssociatedItemIKr.getSaturation())
          .andReturn(dummyAssociatedItemIKrSaturation);
    dummyAssociatedDataIKr.add(mockAssociatedDataIKr);
    expectNew(AssociatedData.class, dummyAssociatedItemIKrC50, 
                                    (BigDecimal) null,
                                    dummyAssociatedItemIKrHill,
                                    dummyAssociatedItemIKrSaturation)
             .andReturn(mockAssociatedDataIKr);
    // << extractChannelData()
    expectNew(RequestChannelData.class, dummyAssociatedDataIKr,
                                        dummyC50SpreadIKr,
                                        dummyHillSpreadIKr)
             .andReturn(mockRequestChannelDataIKr);
    expect(mockRequest.getIK1()).andReturn(null);
    expect(mockRequest.getIKs()).andReturn(null);
    expect(mockRequest.getIto()).andReturn(null);
    expect(mockRequest.getINa()).andReturn(null);
    expect(mockRequest.getINaL()).andReturn(null);

    extractPlasmaData();

    mocksControl.replay();
    replayAll();

    final ApPredictVO apPredictVO =  new ApPredictVO(mockRequest, dummyPKFileLocation, null);

    mocksControl.verify();
    verifyAll();

    assertSame(dummyModelIdentifier, apPredictVO.getModelIdentifier());
    assertTrue(apPredictVO.hasMaxPacingTime());
    assertTrue(apPredictVO.hasPacingFrequency());
    assertFalse(apPredictVO.isCredibleIntervals());
    assertSame(mockRequestChannelDataICaL,
               apPredictVO.retrieveChannelData(CHANNEL_CURRENT.ICaL));
    assertSame(mockRequestChannelDataIKr,
               apPredictVO.retrieveChannelData(CHANNEL_CURRENT.IKr));
    assertNull(apPredictVO.retrieveChannelData(CHANNEL_CURRENT.IK1));
    assertNull(apPredictVO.retrieveChannelData(CHANNEL_CURRENT.IKs));
    assertNull(apPredictVO.retrieveChannelData(CHANNEL_CURRENT.INa));
    assertNull(apPredictVO.retrieveChannelData(CHANNEL_CURRENT.INaL));
    assertNull(apPredictVO.retrieveChannelData(CHANNEL_CURRENT.Ito));
    assertNull(apPredictVO.getPKFileLocation());
    assertSame(mockRequestPlasmaData, apPredictVO.getPlasma());
    assertFalse(apPredictVO.isPKSimulation());
    assertNotNull(apPredictVO.toString());

  }

  @Test
  public void testManyChannelDataWithSpreads() throws Exception {
    dummyPacingMaxTime = BigDecimal.TEN;
    dummyPacingFrequency = BigDecimal.ONE;

    preparePacing();
    expect(mockRequest.getCredibleIntervals()).andReturn(mockCredibleIntervals);
    dummyCalculateCredibleIntervals = true;
    expect(mockCredibleIntervals.getCalculateCredibleIntervals())
          .andReturn(dummyCalculateCredibleIntervals);
    dummyPctiles = "dummyPctiles";
    expect(mockCredibleIntervals.getPercentiles()).andReturn(dummyPctiles);
    // ICaL
    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    dummyIC50DataICaL.add(mockAssociatedItemICaL);
    expect(mockChannelDataICaL.getIC50Data()).andReturn(dummyIC50DataICaL);     // <-- IC50 ICaL data
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(null);
    dummyC50SpreadICaL = dummyArbitrarySpread;
    expect(mockChannelDataICaL.getC50Spread()).andReturn(dummyC50SpreadICaL);   // <-- IC spread is good
    dummyHillSpreadICaL = dummyArbitrarySpread;
    expect(mockChannelDataICaL.getHillSpread()).andReturn(dummyHillSpreadICaL); // <-- Hill spread is good
    dummyAssociatedItemICaLC50 = dummyArbitraryIC50;                            // <-- IC50 is good
    expect(mockAssociatedItemICaL.getC50())
          .andReturn(dummyAssociatedItemICaLC50);
    dummyAssociatedItemICaLHill = dummyArbitraryHill;                           // <-- Hill is good
    expect(mockAssociatedItemICaL.getHill())
          .andReturn(dummyAssociatedItemICaLHill);
    dummyAssociatedItemICaLSaturation = dummyArbitrarySaturation;               // <-- Saturation is good
    expect(mockAssociatedItemICaL.getSaturation())
          .andReturn(dummyAssociatedItemICaLSaturation);
    dummyAssociatedDataICaL.add(mockAssociatedDataICaL);
    expectNew(AssociatedData.class, (BigDecimal) null,
                                    dummyAssociatedItemICaLC50,
                                    dummyAssociatedItemICaLHill,
                                    dummyAssociatedItemICaLSaturation)
             .andReturn(mockAssociatedDataICaL);
    // << extractChannelData()
    expectNew(RequestChannelData.class, dummyAssociatedDataICaL,
                                        dummyC50SpreadICaL,
                                        dummyHillSpreadICaL)
             .andReturn(mockRequestChannelDataICaL);

    // IKr
    // >> extractChannelData()
    expect(mockRequest.getIKr()).andReturn(mockChannelDataIKr);
    dummyPIC50DataIKr.add(mockAssociatedItemIKr);
    expect(mockChannelDataIKr.getIC50Data()).andReturn(null);
    expect(mockChannelDataIKr.getPIC50Data()).andReturn(dummyPIC50DataIKr);     // <-- pIC50 IKr data
    dummyC50SpreadIKr = dummyArbitrarySpread;
    expect(mockChannelDataIKr.getC50Spread()).andReturn(dummyC50SpreadIKr);     // <-- IC spread is good
    dummyHillSpreadIKr = dummyArbitrarySpread;
    expect(mockChannelDataIKr.getHillSpread()).andReturn(dummyHillSpreadIKr);   // <-- Hill spread is good
    dummyAssociatedItemIKrC50 = dummyArbitraryPIC50;                            // <-- pIC50 is good
    expect(mockAssociatedItemIKr.getC50())
          .andReturn(dummyAssociatedItemIKrC50);
    dummyAssociatedItemIKrHill = dummyArbitraryHill;                            // <-- Hill is good
    expect(mockAssociatedItemIKr.getHill())
          .andReturn(dummyAssociatedItemIKrHill);
    dummyAssociatedItemIKrSaturation = dummyArbitrarySaturation;                // <-- Saturation is good
    expect(mockAssociatedItemIKr.getSaturation())
          .andReturn(dummyAssociatedItemIKrSaturation);
    dummyAssociatedDataIKr.add(mockAssociatedDataIKr);
    expectNew(AssociatedData.class, dummyAssociatedItemIKrC50, 
                                    (BigDecimal) null,
                                    dummyAssociatedItemIKrHill,
                                    dummyAssociatedItemIKrSaturation)
             .andReturn(mockAssociatedDataIKr);
    // << extractChannelData()
    expectNew(RequestChannelData.class, dummyAssociatedDataIKr,
                                        dummyC50SpreadIKr,
                                        dummyHillSpreadIKr)
             .andReturn(mockRequestChannelDataIKr);
    expect(mockRequest.getIK1()).andReturn(null);
    expect(mockRequest.getIKs()).andReturn(null);
    expect(mockRequest.getIto()).andReturn(null);
    expect(mockRequest.getINa()).andReturn(null);
    expect(mockRequest.getINaL()).andReturn(null);

    extractPlasmaData();

    mocksControl.replay();
    replayAll();

    final ApPredictVO apPredictVO =  new ApPredictVO(mockRequest,
                                                     dummyPKFileLocation, null);

    mocksControl.verify();
    verifyAll();

    assertSame(dummyModelIdentifier, apPredictVO.getModelIdentifier());
    assertTrue(apPredictVO.hasMaxPacingTime());
    assertTrue(apPredictVO.hasPacingFrequency());
    assertTrue(apPredictVO.isCredibleIntervals());
    assertSame(mockRequestChannelDataICaL,
               apPredictVO.retrieveChannelData(CHANNEL_CURRENT.ICaL));
    assertSame(mockRequestChannelDataIKr,
               apPredictVO.retrieveChannelData(CHANNEL_CURRENT.IKr));
    assertNull(apPredictVO.retrieveChannelData(CHANNEL_CURRENT.IK1));
    assertNull(apPredictVO.retrieveChannelData(CHANNEL_CURRENT.IKs));
    assertNull(apPredictVO.retrieveChannelData(CHANNEL_CURRENT.INa));
    assertNull(apPredictVO.retrieveChannelData(CHANNEL_CURRENT.INaL));
    assertNull(apPredictVO.retrieveChannelData(CHANNEL_CURRENT.Ito));
    assertNull(apPredictVO.getPKFileLocation());
    assertSame(mockRequestPlasmaData, apPredictVO.getPlasma());
    assertFalse(apPredictVO.isPKSimulation());
    assertNotNull(apPredictVO.toString());
  }

  @Test
  public void testMinimumData() throws Exception {
    expect(mockRequest.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockRequest.getPacingFreq()).andReturn(null);
    expect(mockRequest.getPacingMaxTime()).andReturn(null);
    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    noChannelData();
    extractPlasmaData();

    replayAll();
    mocksControl.replay();

    final ApPredictVO apPredictVO = new ApPredictVO(mockRequest,
                                                    dummyPKFileLocation, null);

    verifyAll();
    mocksControl.verify();

    assertSame(dummyModelIdentifier, apPredictVO.getModelIdentifier());
    assertFalse(apPredictVO.hasMaxPacingTime());
    assertFalse(apPredictVO.hasPacingFrequency());
    assertFalse(apPredictVO.isCredibleIntervals());
    assertNull(apPredictVO.retrieveChannelData(CHANNEL_CURRENT.ICaL));
    assertNull(apPredictVO.retrieveChannelData(CHANNEL_CURRENT.IK1));
    assertNull(apPredictVO.retrieveChannelData(CHANNEL_CURRENT.IKr));
    assertNull(apPredictVO.retrieveChannelData(CHANNEL_CURRENT.IKs));
    assertNull(apPredictVO.retrieveChannelData(CHANNEL_CURRENT.INa));
    assertNull(apPredictVO.retrieveChannelData(CHANNEL_CURRENT.INaL));
    assertNull(apPredictVO.retrieveChannelData(CHANNEL_CURRENT.Ito));
    assertNull(apPredictVO.getPKFileLocation());
    assertSame(mockRequestPlasmaData, apPredictVO.getPlasma());
    assertFalse(apPredictVO.isPKSimulation());
    assertNotNull(apPredictVO.toString());
  }

  @Test(expected=IllegalArgumentException.class)
  public void testMinimumPlasmaDataNotSatisfied() throws Exception {
    expect(mockRequest.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockRequest.getPacingFreq()).andReturn(null);
    expect(mockRequest.getPacingMaxTime()).andReturn(null);
    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    noChannelData();

    // >> extractPlasmaData()
    expectNew(RequestPlasmaData.class).andReturn(mockRequestPlasmaData);
    expect(mockRequest.getPlasmaConcCount()).andReturn(null);
    expect(mockRequest.getPlasmaConcLogScale()).andReturn(null);
    expect(mockRequest.getPlasmaConcsHiLo()).andReturn(null);
    final String dummyPlasmaConcsAssigned = null;
    expect(mockRequest.getPlasmaConcsAssigned())
          .andReturn(dummyPlasmaConcsAssigned);
    final BigDecimal dummyPlasmaConcHigh = null;
    final BigDecimal dummyPlasmaConcLow = null;
    mockRequestPlasmaData.setRange(dummyPlasmaConcsAssigned, dummyPlasmaConcHigh,
                                   dummyPlasmaConcLow);
    expectLastCall().andThrow(new IllegalArgumentException());                  // emulate setRange() exception 
    // << extractPlasmaData()

    replayAll();
    mocksControl.replay();

    try {
      new ApPredictVO(mockRequest, dummyPKFileLocation, null);
      fail("Should be throwing an IllegalArgumentException!");
    } catch (IllegalStateException e) {}

    verifyAll();
    mocksControl.verify();
  }

  @Test
  public void testNullHillOKOnlyIfNoHillSpread() throws Exception {
    preparePacing();
    expect(mockRequest.getCredibleIntervals()).andReturn(mockCredibleIntervals);
    dummyCalculateCredibleIntervals = true;
    expect(mockCredibleIntervals.getCalculateCredibleIntervals())
          .andReturn(dummyCalculateCredibleIntervals);
    dummyPctiles = "dummyPctiles";
    expect(mockCredibleIntervals.getPercentiles()).andReturn(dummyPctiles);
    // ICaL
    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    dummyIC50DataICaL.add(mockAssociatedItemICaL);
    expect(mockChannelDataICaL.getIC50Data()).andReturn(dummyIC50DataICaL);     // <-- IC50 ICaL data
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(null);
    dummyC50SpreadICaL = dummyArbitrarySpread;
    expect(mockChannelDataICaL.getC50Spread()).andReturn(dummyC50SpreadICaL);   // <-- IC spread is good
    expect(mockChannelDataICaL.getHillSpread()).andReturn(dummyHillSpreadICaL); // <-- No Hill spread
    dummyAssociatedItemICaLC50 = dummyArbitraryIC50;                            // <-- IC50 is good
    expect(mockAssociatedItemICaL.getC50())
          .andReturn(dummyAssociatedItemICaLC50);
    expect(mockAssociatedItemICaL.getHill())
          .andReturn(dummyAssociatedItemICaLHill);                              // <-- No Hill
    dummyAssociatedItemICaLSaturation = dummyArbitrarySaturation;               // <-- Saturation is good
    expect(mockAssociatedItemICaL.getSaturation())
          .andReturn(dummyAssociatedItemICaLSaturation);
    dummyAssociatedDataICaL.add(mockAssociatedDataICaL);
    expectNew(AssociatedData.class, (BigDecimal) null,
                                    dummyAssociatedItemICaLC50,
                                    dummyAssociatedItemICaLHill,
                                    dummyAssociatedItemICaLSaturation)
             .andReturn(mockAssociatedDataICaL);
    // << extractChannelData()
    expectNew(RequestChannelData.class, dummyAssociatedDataICaL,
                                        dummyC50SpreadICaL,
                                        dummyHillSpreadICaL)
             .andReturn(mockRequestChannelDataICaL);
    expect(mockRequest.getIKr()).andReturn(null);
    expect(mockRequest.getIK1()).andReturn(null);
    expect(mockRequest.getIKs()).andReturn(null);
    expect(mockRequest.getIto()).andReturn(null);
    expect(mockRequest.getINa()).andReturn(null);
    expect(mockRequest.getINaL()).andReturn(null);

    extractPlasmaData();

    mocksControl.replay();
    replayAll();

    new ApPredictVO(mockRequest, dummyPKFileLocation, null);

    mocksControl.verify();
    verifyAll();
  }

  @Test
  public void testPKFile() throws Exception {
    dummyPKFileLocation = "dummyPKFileLocation";
    expect(mockRequest.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockRequest.getPacingFreq()).andReturn(null);
    expect(mockRequest.getPacingMaxTime()).andReturn(null);
    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    noChannelData();

    replayAll();
    mocksControl.replay();

    final ApPredictVO apPredictVO = new ApPredictVO(mockRequest,
                                                    dummyPKFileLocation, null);

    verifyAll();
    mocksControl.verify();

    assertEquals(dummyPKFileLocation, apPredictVO.getPKFileLocation());
    assertTrue(apPredictVO.isPKSimulation());
  }

  @Test
  public void testPlasmaConcsFullData() throws Exception {
    dummyModelIdentifier = 1;
    expect(mockRequest.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockRequest.getPacingFreq()).andReturn(null);
    expect(mockRequest.getPacingMaxTime()).andReturn(null);
    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    noChannelData();

    // >> extractPlasmaData()
    expectNew(RequestPlasmaData.class).andReturn(mockRequestPlasmaData);
    final Short dummyPlasmaConcCount = 3;
    expect(mockRequest.getPlasmaConcCount())
          .andReturn(dummyPlasmaConcCount).times(2);
    mockRequestPlasmaData.setPlasmaConcCount(dummyPlasmaConcCount);
    final Boolean dummyPlasmaConcLogScale = Boolean.TRUE;
    expect(mockRequest.getPlasmaConcLogScale())
          .andReturn(dummyPlasmaConcLogScale).times(2);
    mockRequestPlasmaData.setPlasmaConcLogScale(dummyPlasmaConcLogScale);
    expect(mockRequest.getPlasmaConcsHiLo()).andReturn(mockPlasmaConcsHiLo);
    final String dummyPlasmaConcsAssigned = null;
    expect(mockRequest.getPlasmaConcsAssigned())
          .andReturn(dummyPlasmaConcsAssigned);
    final BigDecimal dummyPlasmaConcHigh = BigDecimal.TEN;
    expect(mockPlasmaConcsHiLo.getHigh()).andReturn(dummyPlasmaConcHigh);
    final BigDecimal dummyPlasmaConcLow = BigDecimal.ZERO;
    expect(mockPlasmaConcsHiLo.getLow()).andReturn(dummyPlasmaConcLow);
    mockRequestPlasmaData.setRange(dummyPlasmaConcsAssigned, dummyPlasmaConcHigh,
                                   dummyPlasmaConcLow);
    mockRequestPlasmaData.validate();
    // << extractPlasmaData()

    replayAll();
    mocksControl.replay();

    final ApPredictVO apPredictVO = new ApPredictVO(mockRequest,
                                                    dummyPKFileLocation, null);


    verifyAll();
    mocksControl.verify();

    assertFalse(apPredictVO.isPKSimulation());
  }

  @Test
  public void testRetrieveRemovableFileLocations() throws Exception {
    /*
     * Not a PK simulation, and no CellML file location.
     */
    expect(mockRequest.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockRequest.getPacingFreq()).andReturn(null);
    expect(mockRequest.getPacingMaxTime()).andReturn(null);
    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    noChannelData();
    extractPlasmaData();

    replayAll();
    mocksControl.replay();

    ApPredictVO apPredictVO = new ApPredictVO(mockRequest, dummyPKFileLocation,
                                              null);
    Set<String> removableFiles = apPredictVO.retrieveRemovableFileLocations();

    verifyAll();
    mocksControl.verify();

    assertTrue(removableFiles.isEmpty());

    resetAll();
    mocksControl.reset();

    /*
     * PK simulation, and no CellML file location.
     */
    dummyPKFileLocation = "dummyPKFileLocation";
    expect(mockRequest.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockRequest.getPacingFreq()).andReturn(null);
    expect(mockRequest.getPacingMaxTime()).andReturn(null);
    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    noChannelData();

    replayAll();
    mocksControl.replay();

    apPredictVO = new ApPredictVO(mockRequest, dummyPKFileLocation, null);
    removableFiles = apPredictVO.retrieveRemovableFileLocations();

    verifyAll();
    mocksControl.verify();

    assertSame(1, removableFiles.size());
    assertTrue(removableFiles.contains(dummyPKFileLocation));

    resetAll();
    mocksControl.reset();

    /*
     * PK simulation, with CellML file location.
     */
    final String dummyCellMLFileName = "dummyCellMLFileName";
    final String dummyCellMLFileLocation = "dummyCellMLFileLocation";
    expect(mockFileCellML.getName()).andReturn(dummyCellMLFileName);
    expect(mockFileCellML.getAbsolutePath()).andReturn(dummyCellMLFileLocation);
    expect(mockRequest.getPacingFreq()).andReturn(null);
    expect(mockRequest.getPacingMaxTime()).andReturn(null);
    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    noChannelData();

    replayAll();
    mocksControl.replay();

    apPredictVO = new ApPredictVO(mockRequest, dummyPKFileLocation,
                                  mockFileCellML);
    removableFiles = apPredictVO.retrieveRemovableFileLocations();

    verifyAll();
    mocksControl.verify();

    assertSame(2, removableFiles.size());
    assertTrue(removableFiles.contains(dummyPKFileLocation));
    assertTrue(removableFiles.contains(dummyCellMLFileLocation));
  }
}