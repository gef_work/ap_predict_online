/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.postrun.reader;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.easymock.EasyMock.partialMockBuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO;
import uk.ac.ox.cs.nc3rs.app_manager.util.FileUtil;
import uk.ac.ox.cs.nc3rs.app_manager.value.SimulationResultsVO;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.SetProcessStatusRequest;

/**
 * Unit test the PK data file reader implementation.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { ResultsPKReader.class, FileUtil.class } )
public class ResultsPKReaderTest {

  private static final String fileNamePKResults = "pkpd_results.txt";

  private AppManagerDAO mockAppManagerDAO;
  private IMocksControl mocksControl;
  private static final long dummyAppManagerId = 3l;
  private ResultsPKReader mockResultsPKReader;
  private SetProcessStatusRequest mockSetProcessStatusRequest;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockAppManagerDAO = mocksControl.createMock(AppManagerDAO.class);
    mockResultsPKReader = partialMockBuilder(ResultsPKReader.class)
                                            .addMockedMethod("retrieveResultFilesDir")
                                            .addMockedMethod("generalDataLoading")
                                            .createMock(mocksControl);

    mockSetProcessStatusRequest = mocksControl.createMock(SetProcessStatusRequest.class);
    ReflectionTestUtils.setField(mockResultsPKReader,
                                 AppIdentifiers.COMPONENT_JDBC_APP_MANAGER_DAO,
                                 mockAppManagerDAO);
  }

  @Test
  public void testResultsPKReader() throws Exception {
    /*
     * No PK file found (at which point croak!).
     */
    expect(mockSetProcessStatusRequest.getAppManagerId())
          .andReturn(dummyAppManagerId);
    final String dummyResultsFileDir = "dummyResultsFileDir";
    expect(mockResultsPKReader.retrieveResultFilesDir(dummyAppManagerId))
          .andReturn(dummyResultsFileDir);
    final List<SimulationResultsVO> dummySimulationResults = new ArrayList<SimulationResultsVO>();
    final String dummyDeltaAPD90PercentileNames = "dummyDeltaAPD90PercentileNames";
    expect(mockResultsPKReader.generalDataLoading(dummyAppManagerId,
                                                  dummyResultsFileDir,
                                                  dummySimulationResults))
          .andReturn(dummyDeltaAPD90PercentileNames);
    final String dummyFileNotFoundException = "dummyFileNotFoundException";
    dummyResultsFileDir.concat(fileNamePKResults);
    final Capture<String> capturePKResultsFilePath = newCapture();
    final File mockPKResultsFile = mocksControl.createMock(File.class);
    expectNew(File.class, new Class[] { String.class },
                          capture(capturePKResultsFilePath))
             .andReturn(mockPKResultsFile);
    mockStatic(FileUtil.class);
    expect(FileUtil.fileLineReader(mockPKResultsFile, true, true, true))
          .andThrow(new FileNotFoundException(dummyFileNotFoundException));

    replayAll();
    mocksControl.replay();

    try {
      mockResultsPKReader.resultsPKReader(mockSetProcessStatusRequest);
      fail("Should not accept failure to read PK results file!");
    } catch (UnsupportedOperationException e) {
    }

    verifyAll();
    mocksControl.verify();

    assertEquals(dummyResultsFileDir.concat(fileNamePKResults),
                 capturePKResultsFilePath.getValue());

    resetAll();
    mocksControl.reset();

    /*
     * Read PK file.
     */
    expect(mockSetProcessStatusRequest.getAppManagerId())
          .andReturn(dummyAppManagerId);
    expect(mockResultsPKReader.retrieveResultFilesDir(dummyAppManagerId))
          .andReturn(dummyResultsFileDir);
    expect(mockResultsPKReader.generalDataLoading(dummyAppManagerId,
                                                  dummyResultsFileDir,
                                                  dummySimulationResults))
          .andReturn(dummyDeltaAPD90PercentileNames);
    expectNew(File.class, new Class[] { String.class },
                          capture(capturePKResultsFilePath))
             .andReturn(mockPKResultsFile);

    final List<String> dummyPKResultsLines = new ArrayList<String>();
    // Create the PK file results lines
    final String dummyLine1Timepoint = "dummyLine1Timepoint";
    final String dummyLine1APD90_1 = "dummyLine1APD90_1";
    final String dummyLine1APD90_2 = "dummyLine1APD90_2";
    final List<String> pkRsltsLine1 = new ArrayList<String>();
    pkRsltsLine1.add(dummyLine1Timepoint);
    pkRsltsLine1.add(dummyLine1APD90_1);
    pkRsltsLine1.add(dummyLine1APD90_2);
    final String dummyPKResultsValuesLine1 = StringUtils.join(pkRsltsLine1,
                                                              AbstractRunResultsReader.tab);

    final String dummyLine2Timepoint = "dummyLine2Timepoint";
    final String dummyLine2APD90_1 = "dummyLine2APD90_1";
    final String dummyLine2APD90_2 = "dummyLine2APD90_2";
    final List<String> pkRsltsLine2 = new ArrayList<String>();
    pkRsltsLine2.add(dummyLine2Timepoint);
    pkRsltsLine2.add(dummyLine2APD90_1);
    pkRsltsLine2.add(dummyLine2APD90_2);
    final String dummyPKResultsValuesLine2 = StringUtils.join(pkRsltsLine2,
                                                              AbstractRunResultsReader.tab);
    dummyPKResultsLines.add(dummyPKResultsValuesLine1);
    dummyPKResultsLines.add(dummyPKResultsValuesLine2);
    expect(FileUtil.fileLineReader(mockPKResultsFile, true, true, true))
          .andReturn(dummyPKResultsLines);


    final Capture<Map<String, String>> capturePKResultsValues = newCapture();
    mockAppManagerDAO.saveSimulationResults(eq(dummyAppManagerId),
                                            eq(dummySimulationResults),
                                            capture(capturePKResultsValues),
                                            eq(dummyDeltaAPD90PercentileNames));

    replayAll();
    mocksControl.replay();

    final SetProcessStatusRequest request = mockResultsPKReader.resultsPKReader(mockSetProcessStatusRequest);

    verifyAll();
    mocksControl.verify();

    final Map<String, String> capturedPKResultsValues = capturePKResultsValues.getValue();
    assertTrue(capturedPKResultsValues.size() == 2);
    assertTrue(capturedPKResultsValues.containsKey(dummyLine1Timepoint));
    assertTrue(capturedPKResultsValues.containsKey(dummyLine2Timepoint));
    assertEquals(dummyLine1APD90_1.concat(",").concat(dummyLine1APD90_2),
                 capturedPKResultsValues.get(dummyLine1Timepoint));
    assertEquals(dummyLine2APD90_1.concat(",").concat(dummyLine2APD90_2),
                 capturedPKResultsValues.get(dummyLine2Timepoint));
    assertSame(mockSetProcessStatusRequest, request);
  }
}