/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the names of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.value;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.powermock.api.easymock.PowerMock.expectNew;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.ApPredictRunRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.AssociatedItem;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.ChannelData;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.TypeCredibleIntervals;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.TypePlasmaConcsHiLo;

/**
 * Abstract testing class for {@link ApPredictVO}.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { ApPredictVO.class, AssociatedData.class,
                   RequestChannelData.class, RequestPlasmaData.class } )
public abstract class AbstractApPredictVOTest {

  protected static final String dummyPlasmaConcsAssigned = "1";
  protected static final BigDecimal dummyArbitraryHill = new BigDecimal("0.76");
  protected static final BigDecimal dummyArbitraryIC50 = new BigDecimal("1001.34");
  protected static final BigDecimal dummyArbitraryPIC50 = new BigDecimal("4.5");
  protected static final BigDecimal dummyArbitrarySaturation = new BigDecimal("48.3");
  protected static final BigDecimal dummyArbitrarySpread = new BigDecimal("0.32");

  protected ApPredictRunRequest mockRequest;
  protected AssociatedData mockAssociatedDataICaL;
  protected AssociatedData mockAssociatedDataIKr;
  protected AssociatedData mockAssociatedDataIK1;
  protected AssociatedItem mockAssociatedItemICaL;
  protected AssociatedItem mockAssociatedItemIKr;
  protected AssociatedItem mockAssociatedItemIK1;
  protected BigDecimal dummyAssociatedItemICaLC50;
  protected BigDecimal dummyAssociatedItemIKrC50;
  protected BigDecimal dummyAssociatedItemICaLHill;
  protected BigDecimal dummyAssociatedItemIKrHill;
  protected BigDecimal dummyAssociatedItemICaLSaturation;
  protected BigDecimal dummyAssociatedItemIKrSaturation;
  protected BigDecimal dummyC50SpreadICaL;
  protected BigDecimal dummyC50SpreadIKr;
  protected BigDecimal dummyHillSpreadICaL;
  protected BigDecimal dummyHillSpreadIKr;
  protected BigDecimal dummyPacingFrequency;
  protected BigDecimal dummyPacingMaxTime;
  protected Boolean dummyCalculateCredibleIntervals;
  protected ChannelData mockChannelDataICaL;
  protected ChannelData mockChannelDataIKr;
  protected ChannelData mockChannelDataIK1;
  protected File mockFileCellML;
  protected IMocksControl mocksControl;
  protected List<AssociatedData> dummyAssociatedDataICaL;
  protected List<AssociatedData> dummyAssociatedDataIKr;
  protected List<AssociatedItem> dummyIC50DataICaL;
  protected List<AssociatedItem> dummyIC50DataIKr;
  protected List<AssociatedItem> dummyPIC50DataICaL;
  protected List<AssociatedItem> dummyPIC50DataIKr;
  protected RequestChannelData mockRequestChannelDataICaL;
  protected RequestChannelData mockRequestChannelDataIKr;
  protected RequestPlasmaData mockRequestPlasmaData;
  protected Short dummyModelIdentifier;
  protected String dummyPctiles;
  protected String dummyPKFileLocation;
  protected TypeCredibleIntervals mockCredibleIntervals;
  protected TypePlasmaConcsHiLo mockPlasmaConcsHiLo;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockAssociatedItemICaL = mocksControl.createMock(AssociatedItem.class);
    mockAssociatedItemIKr = mocksControl.createMock(AssociatedItem.class);
    mockAssociatedItemIK1 = mocksControl.createMock(AssociatedItem.class);
    mockChannelDataICaL = mocksControl.createMock(ChannelData.class);
    mockChannelDataIKr = mocksControl.createMock(ChannelData.class);
    mockChannelDataIK1 = mocksControl.createMock(ChannelData.class);
    mockPlasmaConcsHiLo = mocksControl.createMock(TypePlasmaConcsHiLo.class);
    mockRequest = mocksControl.createMock(ApPredictRunRequest.class);
    mockRequestPlasmaData = mocksControl.createMock(RequestPlasmaData.class);
    mockAssociatedDataICaL = mocksControl.createMock(AssociatedData.class);
    mockAssociatedDataIKr = mocksControl.createMock(AssociatedData.class);
    mockAssociatedDataIK1 = mocksControl.createMock(AssociatedData.class);
    mockRequestChannelDataICaL = mocksControl.createMock(RequestChannelData.class);
    mockRequestChannelDataIKr = mocksControl.createMock(RequestChannelData.class);
    mockFileCellML = mocksControl.createMock(File.class);
    mockCredibleIntervals = mocksControl.createMock(TypeCredibleIntervals.class);

    dummyAssociatedDataICaL = new ArrayList<AssociatedData>();
    dummyAssociatedDataIKr = new ArrayList<AssociatedData>();
    dummyAssociatedItemICaLC50 = null;
    dummyAssociatedItemIKrC50 = null;
    dummyAssociatedItemICaLHill = null;
    dummyAssociatedItemIKrHill = null;
    dummyAssociatedItemICaLSaturation = null;
    dummyAssociatedItemIKrSaturation = null;
    dummyC50SpreadICaL = null;
    dummyHillSpreadICaL = null;
    dummyModelIdentifier = 99;
    dummyPacingFrequency = null;
    dummyPacingMaxTime = null;
    dummyIC50DataICaL = new ArrayList<AssociatedItem>();
    dummyIC50DataIKr = new ArrayList<AssociatedItem>();
    dummyPIC50DataICaL = new ArrayList<AssociatedItem>();
    dummyPIC50DataIKr = new ArrayList<AssociatedItem>();
    dummyPKFileLocation = null;
    dummyCalculateCredibleIntervals = null;
    dummyPctiles = null;
  }

  protected void extractPlasmaData() throws Exception {
    // >> extractPlasmaData()
    expectNew(RequestPlasmaData.class).andReturn(mockRequestPlasmaData);
    expect(mockRequest.getPlasmaConcCount()).andReturn(null);
    expect(mockRequest.getPlasmaConcLogScale()).andReturn(null);
    expect(mockRequest.getPlasmaConcsHiLo()).andReturn(null);
    expect(mockRequest.getPlasmaConcsAssigned())
          .andReturn(dummyPlasmaConcsAssigned);
    final BigDecimal dummyPlasmaConcHigh = null;
    final BigDecimal dummyPlasmaConcLow = null;
    mockRequestPlasmaData.setRange(dummyPlasmaConcsAssigned,
                                   dummyPlasmaConcHigh,
                                   dummyPlasmaConcLow);
    mockRequestPlasmaData.validate();
    // << extractPlasmaData()
  }

  protected void noChannelData() {
    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(null);
    expect(mockRequest.getIKr()).andReturn(null);
    expect(mockRequest.getIK1()).andReturn(null);
    expect(mockRequest.getIKs()).andReturn(null);
    expect(mockRequest.getIto()).andReturn(null);
    expect(mockRequest.getINa()).andReturn(null);
    expect(mockRequest.getINaL()).andReturn(null);
    // << extractChannelData()
  }

  protected void preparePacing() {
    expect(mockRequest.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockRequest.getPacingFreq()).andReturn(dummyPacingFrequency);
    expect(mockRequest.getPacingMaxTime()).andReturn(dummyPacingMaxTime);
  }
}