/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.postrun.reader;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertSame;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO;
import uk.ac.ox.cs.nc3rs.app_manager.util.FileUtil;
import uk.ac.ox.cs.nc3rs.app_manager.value.DiagnosticsVO;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.SetProcessStatusRequest;

/**
 * Unit test the post simulation run diagnostics reader.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { FileUtil.class } )
public class PostSimulationRunDiagnosticsReaderTest {

  private static final long dummyAppManagerId = 3l;
  private AppManagerDAO mockAppManagerDAO;
  private IMocksControl mocksControl;
  private PostSimulationRunDiagnosticsReader reader;
  private SetProcessStatusRequest mockRequest;

  @Before
  public void setUp() {
    reader = new PostSimulationRunDiagnosticsReader();

    mocksControl = createStrictControl();
    mockAppManagerDAO = mocksControl.createMock(AppManagerDAO.class);
    mockRequest = mocksControl.createMock(SetProcessStatusRequest.class);

    ReflectionTestUtils.setField(reader, AppIdentifiers.COMPONENT_JDBC_APP_MANAGER_DAO,
                                 mockAppManagerDAO);
  }

  @Test
  public void testPostSimulationRunDiagnostics() {
    /**
     * No diagnostics scenario
     */
    expect(mockRequest.getAppManagerId()).andReturn(dummyAppManagerId);
    mockStatic(FileUtil.class);
    expect(FileUtil.retrieveDiagnostics(dummyAppManagerId, null)).andReturn(null);

    replayAll();
    mocksControl.replay();

    SetProcessStatusRequest retrieved = reader.postSimulationRunDiagnostics(mockRequest);

    verifyAll();
    mocksControl.verify();

    assertSame(mockRequest, retrieved);

    resetAll();
    mocksControl.reset();

    /*
     * Diagnostics available
     */
    expect(mockRequest.getAppManagerId()).andReturn(dummyAppManagerId);
    mockStatic(FileUtil.class);
    final DiagnosticsVO mockDiagnosticsVO = mocksControl.createMock(DiagnosticsVO.class);
    expect(FileUtil.retrieveDiagnostics(dummyAppManagerId, null)).andReturn(mockDiagnosticsVO);
    final String dummyInfo = "dummyInfo";
    expect(mockDiagnosticsVO.getInfo()).andReturn(dummyInfo);
    final String dummyOutput = "dummyOutput";
    expect(mockDiagnosticsVO.getOutput()).andReturn(dummyOutput);
    mockAppManagerDAO.saveSimulationDiagnostics(dummyAppManagerId, dummyInfo, dummyOutput);

    replayAll();
    mocksControl.replay();

    retrieved = reader.postSimulationRunDiagnostics(mockRequest);

    verifyAll();
    mocksControl.verify();

    assertSame(mockRequest, retrieved);
  }

  @Test
  public void testSetBaseDir() {
    /**
     * No diagnostics + no base dir set scenario
     */
    expect(mockRequest.getAppManagerId()).andReturn(dummyAppManagerId);
    mockStatic(FileUtil.class);
    expect(FileUtil.retrieveDiagnostics(dummyAppManagerId, null)).andReturn(null);

    replayAll();
    mocksControl.replay();

    reader.postSimulationRunDiagnostics(mockRequest);

    verifyAll();
    mocksControl.verify();

    resetAll();
    mocksControl.reset();

    /**
     * No diagnostics + base dir set scenario
     */
    final String dummyBaseDir = "dummyBaseDir";
    reader.setBaseDir(dummyBaseDir);
    expect(mockRequest.getAppManagerId()).andReturn(dummyAppManagerId);
    mockStatic(FileUtil.class);
    expect(FileUtil.retrieveDiagnostics(dummyAppManagerId, dummyBaseDir)).andReturn(null);

    replayAll();
    mocksControl.replay();

    reader.postSimulationRunDiagnostics(mockRequest);

    verifyAll();
    mocksControl.verify();
  }
}