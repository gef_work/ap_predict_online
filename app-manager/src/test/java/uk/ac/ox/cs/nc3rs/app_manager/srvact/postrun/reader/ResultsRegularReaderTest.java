/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.postrun.reader;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.partialMockBuilder;
import static org.junit.Assert.assertSame;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO;
import uk.ac.ox.cs.nc3rs.app_manager.value.SimulationResultsVO;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.SetProcessStatusRequest;

/**
 * Unit test the reading of regular simulation results.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ ResultsRegularReader.class, Scanner.class })
public class ResultsRegularReaderTest {

  private static final long dummyAppManagerId = 3l;

  private AppManagerDAO mockAppManagerDAO;
  private IMocksControl mocksControl;
  private ResultsRegularReader mockResultsRegularReader;
  private SetProcessStatusRequest mockSetProcessStatusRequest;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockAppManagerDAO = mocksControl.createMock(AppManagerDAO.class);
    mockResultsRegularReader = partialMockBuilder(ResultsRegularReader.class)
                                                 .addMockedMethod("retrieveResultFilesDir")
                                                 .addMockedMethod("generalDataLoading")
                                                 .createMock(mocksControl);

    mockSetProcessStatusRequest = mocksControl.createMock(SetProcessStatusRequest.class);
    ReflectionTestUtils.setField(mockResultsRegularReader,
                                 AppIdentifiers.COMPONENT_JDBC_APP_MANAGER_DAO,
                                 mockAppManagerDAO);
  }

  @Test
  public void testResultsPKReader() throws Exception {
    expect(mockSetProcessStatusRequest.getAppManagerId())
          .andReturn(dummyAppManagerId);
    final String dummyResultsFileDir = "dummyResultsFileDir";
    expect(mockResultsRegularReader.retrieveResultFilesDir(dummyAppManagerId))
          .andReturn(dummyResultsFileDir);
    final List<SimulationResultsVO> dummySimulationResults = new ArrayList<SimulationResultsVO>();
    final String dummyDeltaAPD90PercentileNames = "dummyDeltaAPD90PercentileNames";
    expect(mockResultsRegularReader.generalDataLoading(dummyAppManagerId,
                                                       dummyResultsFileDir,
                                                       dummySimulationResults))
          .andReturn(dummyDeltaAPD90PercentileNames);
    mockAppManagerDAO.saveSimulationResults(dummyAppManagerId,
                                            dummySimulationResults, null,
                                            dummyDeltaAPD90PercentileNames);

    replayAll();
    mocksControl.replay();

    final SetProcessStatusRequest request = mockResultsRegularReader.resultsRegularReader(mockSetProcessStatusRequest);

    verifyAll();
    mocksControl.verify();

    assertSame(mockSetProcessStatusRequest, request);
  }
}