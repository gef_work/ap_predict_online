/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.dao;

import static org.easymock.EasyMock.anyLong;
import static org.easymock.EasyMock.anyString;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.captureLong;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.isNull;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.domain.PKResult;
import uk.ac.ox.cs.nc3rs.app_manager.domain.RunStatus;
import uk.ac.ox.cs.nc3rs.app_manager.domain.SimulationResult;
import uk.ac.ox.cs.nc3rs.app_manager.value.AllSimulationDomainResultsVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.ApPredictVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.DiagnosticsVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.SimulationInfoVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.SimulationResultsVO;

/**
 * Unit test the app manager dao.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ AppManagerDAOImpl.class, GeneratedKeyHolder.class,
                  PreparedStatementCreator.class, RunStatus.class })
public class AppManagerDAOImplTest {

  private AppManagerDAO appManagerDAO;
  private IMocksControl mocksControl;
  private JdbcTemplate mockAppTemplate;
  private long dummyAppManagerId = 4l;

  @Before
  public void setUp() {
    appManagerDAO = new AppManagerDAOImpl();

    mocksControl = createStrictControl();
    mockAppTemplate = mocksControl.createMock(JdbcTemplate.class);
    ReflectionTestUtils.setField(appManagerDAO, AppIdentifiers.COMPONENT_JDBC_APP_TEMPLATE,
                                 mockAppTemplate);
  }

  @Test
  public void testAddStatus() {
    /*
     * Insert doesn't throw a data access exception.
     */
    final RunStatus mockRunStatus = mocksControl.createMock(RunStatus.class);

    expect(mockRunStatus.getId()).andReturn(dummyAppManagerId);
    // >> retrieveSimulationMessages()
    final String dummyMessages = "dummyMessages";
    Capture<String> captureSelectSQL = newCapture();
    Capture<Class<String>> captureSelectRequiredType = newCapture();
    Capture<Long> captureSelectArg1 = newCapture();
    expect(mockAppTemplate.queryForObject(capture(captureSelectSQL),
                                          capture(captureSelectRequiredType),
                                          captureLong(captureSelectArg1)))
          .andReturn(dummyMessages);
    // << retrieveSimulationMessages()
    final String dummyLevel = "dummyLevel";
    expect(mockRunStatus.getLevel()).andReturn(dummyLevel);
    final String dummyStatus = "dummyStatus";
    expect(mockRunStatus.getStatus()).andReturn(dummyStatus);
    Capture<String> captureInsertSQL = newCapture();
    Capture<Long> captureInsertArg1 = newCapture();
    Capture<String> captureInsertArg2 = newCapture();
    Capture<String> captureInsertArg3 = newCapture();
    final int dummyUpdateInt = 2;
    expect(mockAppTemplate.update(capture(captureInsertSQL),
                                  captureLong(captureInsertArg1),
                                  capture(captureInsertArg2),
                                  capture(captureInsertArg3)))
          .andReturn(dummyUpdateInt);

    mocksControl.replay();

    appManagerDAO.addStatus(mockRunStatus);

    mocksControl.verify();

    String capturedSelectSQL = captureSelectSQL.getValue();
    assertEquals("SELECT MESSAGES FROM APP_MANAGER WHERE APP_MANAGER_ID=?", capturedSelectSQL);
    assertSame(dummyAppManagerId, captureSelectArg1.getValue());

    String capturedInsertSQL = captureInsertSQL.getValue();
    assertEquals("INSERT INTO RUN_STATUS (APP_MANAGER_ID,RSLEVEL,STATUS_TEXT) VALUES (?,?,?)",
                 capturedInsertSQL);
    assertSame(dummyAppManagerId, captureInsertArg1.getValue());
    assertEquals(dummyLevel, captureInsertArg2.getValue());
    assertEquals(dummyStatus, captureInsertArg3.getValue());

    mocksControl.reset();

    /*
     * Insert throws no data access exception.
     */
    expect(mockRunStatus.getId()).andReturn(dummyAppManagerId);
    // >> retrieveSimulationMessages()
    captureSelectSQL = newCapture();
    captureSelectRequiredType = newCapture();
    captureSelectArg1 = newCapture();
    expect(mockAppTemplate.queryForObject(capture(captureSelectSQL),
                                          capture(captureSelectRequiredType),
                                          captureLong(captureSelectArg1)))
          .andReturn(dummyMessages);
    // << retrieveSimulationMessages()
    expect(mockRunStatus.getLevel()).andReturn(dummyLevel);
    expect(mockRunStatus.getStatus()).andReturn(dummyStatus);
    captureInsertSQL = newCapture();
    captureInsertArg1 = newCapture();
    captureInsertArg2 = newCapture();
    captureInsertArg3 = newCapture();
    expect(mockAppTemplate.update(capture(captureInsertSQL),
                                  captureLong(captureInsertArg1),
                                  capture(captureInsertArg2),
                                  capture(captureInsertArg3)))
          .andThrow(new DataIntegrityViolationException("fakeDataIntegrityViolationException"));

    mocksControl.replay();

    appManagerDAO.addStatus(mockRunStatus);

    mocksControl.verify();

    capturedSelectSQL = captureSelectSQL.getValue();
    assertEquals("SELECT MESSAGES FROM APP_MANAGER WHERE APP_MANAGER_ID=?", capturedSelectSQL);
    assertSame(dummyAppManagerId, captureSelectArg1.getValue());

    capturedInsertSQL = captureInsertSQL.getValue();
    assertEquals("INSERT INTO RUN_STATUS (APP_MANAGER_ID,RSLEVEL,STATUS_TEXT) VALUES (?,?,?)",
                 capturedInsertSQL);
    assertSame(dummyAppManagerId, captureInsertArg1.getValue());
    assertEquals(dummyLevel, captureInsertArg2.getValue());
    assertEquals(dummyStatus, captureInsertArg3.getValue());
  }

  @Test
  public void testCreateManagerRecord() {
    // Too tricky!
  }

  @Test
  public void testPurgeExistingSimulation() {
    final int[] dummyUpdateInts = new int[] {};
    final Capture<String[]> captureBUArgs = newCapture();

    expect(mockAppTemplate.batchUpdate(capture(captureBUArgs)))
          .andReturn(dummyUpdateInts);


    mocksControl.replay();

    appManagerDAO.purgeExistingSimulation(dummyAppManagerId);

    mocksControl.verify();

    final String[] capturedBUArgs = captureBUArgs.getValue();
    assertEquals("DELETE  FROM APP_MANAGER WHERE APP_MANAGER_ID='4'", capturedBUArgs[0]);
    assertEquals("DELETE  FROM RUN_STATUS WHERE APP_MANAGER_ID='4'", capturedBUArgs[1]);
    assertEquals("DELETE  FROM SIMULATION_RESULTS WHERE APP_MANAGER_ID='4'", capturedBUArgs[2]);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testRetrieveAllSimulationResults() throws Exception {
    final List<SimulationResult> dummySimulationResults = new ArrayList<SimulationResult>();

    final Capture<String> captureSelectSQL1 = newCapture();
    final Capture<Object[]> captureSelectArgs1 = newCapture();
    final Capture<RowMapper<SimulationResult>> captureSelectRowMapper1 = newCapture();
    expect(mockAppTemplate.query(capture(captureSelectSQL1),
                                 capture(captureSelectArgs1),
                                 capture(captureSelectRowMapper1)))
          .andReturn(dummySimulationResults);

    final List<PKResult> dummyPKResults = new ArrayList<PKResult>();
    final Capture<String> captureSelectSQL2 = newCapture();
    final Capture<Object[]> captureSelectArgs2 = newCapture();
    final Capture<RowMapper<PKResult>> captureSelectRowMapper2 = newCapture();
    expect(mockAppTemplate.query(capture(captureSelectSQL2),
                                 capture(captureSelectArgs2),
                                 capture(captureSelectRowMapper2)))
          .andReturn(dummyPKResults);

    final String dummyDeltaAPD90PctileNames = "dummyDeltaAPD90PctileNames";
    final Capture<String> captureSelectSQL3 = newCapture();
    @SuppressWarnings("rawtypes")
    final Capture<Class> captureSelectRequiredType3 = newCapture();
    // It's a varargs param, i.e. Object...!
    final Capture<Long> captureSelectArgs3 = newCapture();
    expect(mockAppTemplate.queryForObject(capture(captureSelectSQL3),
                                          capture(captureSelectRequiredType3),
                                          captureLong(captureSelectArgs3)))
          .andReturn(dummyDeltaAPD90PctileNames);

    final AllSimulationDomainResultsVO mockAllSimulationDomainResultsVO = mocksControl.createMock(AllSimulationDomainResultsVO.class);
    expectNew(AllSimulationDomainResultsVO.class, dummyPKResults,
                                                  dummySimulationResults,
                                                  dummyDeltaAPD90PctileNames)
             .andReturn(mockAllSimulationDomainResultsVO);

    replayAll();
    mocksControl.replay();

    final AllSimulationDomainResultsVO retrieved = appManagerDAO.retrieveAllSimulationResults(dummyAppManagerId);

    verifyAll();
    mocksControl.verify();

    assertSame(mockAllSimulationDomainResultsVO, retrieved);

    assertEquals("SELECT * FROM SIMULATION_RESULTS WHERE APP_MANAGER_ID=?",
                 captureSelectSQL1.getValue());
    assertSame(dummyAppManagerId, captureSelectArgs1.getValue()[0]);
    assertNotNull(captureSelectRowMapper1.getValue());

    assertEquals("SELECT * FROM PK_RESULTS WHERE APP_MANAGER_ID=?",
                 captureSelectSQL2.getValue());
    assertSame(dummyAppManagerId, captureSelectArgs2.getValue()[0]);
    assertNotNull(captureSelectRowMapper2.getValue());

    assertEquals("SELECT DELTA_APD90_PERCENTILE_NAMES FROM APP_MANAGER WHERE APP_MANAGER_ID=?",
                 captureSelectSQL3.getValue());
    assertSame(dummyAppManagerId, captureSelectArgs3.getValue());
    assertSame(String.class, captureSelectRequiredType3.getValue());
  }

  @Test
  public void testRetrieveRunStatusOperations() {
    final List<RunStatus> dummyRunStatuses = new ArrayList<RunStatus>();

    final Capture<String> captureSelectSQL = newCapture();
    final Capture<Object[]> captureSelectArgs = newCapture();
    final Capture<RowMapper<RunStatus>> captureSelectRowMapper = newCapture();

    expect(mockAppTemplate.query(capture(captureSelectSQL),
                                 capture(captureSelectArgs),
                                 capture(captureSelectRowMapper)))
          .andReturn(dummyRunStatuses);

    mocksControl.replay();

    final List<RunStatus> retrieved = appManagerDAO.retrieveAllStatuses(dummyAppManagerId);

    mocksControl.verify();

    final String capturedSelectSQL = captureSelectSQL.getValue();
    assertEquals("SELECT * FROM RUN_STATUS WHERE APP_MANAGER_ID=? ORDER BY ENTERED_TS DESC",
                 capturedSelectSQL);
    final Object[] capturedSelectArgs = captureSelectArgs.getValue();
    assertSame(dummyAppManagerId, capturedSelectArgs[0]);
    final RowMapper<RunStatus> capturedSelectRowMapper = captureSelectRowMapper.getValue();
    assertNotNull(capturedSelectRowMapper);
    assertSame(dummyRunStatuses, retrieved);

    mocksControl.reset();

    /*
     * Verify retrieval of latest status.
     */
    final RunStatus mockRunStatus1 = mocksControl.createMock(RunStatus.class);
    final RunStatus mockRunStatus2 = mocksControl.createMock(RunStatus.class);
    dummyRunStatuses.add(mockRunStatus1);
    dummyRunStatuses.add(mockRunStatus2);
    expect(mockAppTemplate.query(capture(captureSelectSQL),
                                 capture(captureSelectArgs),
                                 capture(captureSelectRowMapper)))
                          .andReturn(dummyRunStatuses);

    mocksControl.replay();

    final RunStatus latest = appManagerDAO.retrieveLatestStatus(dummyAppManagerId);

    mocksControl.verify();

    // Even though in this example the second mock was added last, the first
    // of the mocks (i.e. courtesy of get(0)) was returned.
    assertSame(mockRunStatus1, latest);
  }

  @Test
  public void testRetrieveObjectValue() {
    final Capture<String> captureSelectSQL = newCapture();
    final Capture<Object[]> captureArgs = newCapture();
    final Capture<RowMapper<ApPredictVO>> captureRowMapper = newCapture();

    final ApPredictVO mockApPredictVO = mocksControl.createMock(ApPredictVO.class);
    expect(mockAppTemplate.queryForObject(capture(captureSelectSQL),
                                          capture(captureArgs),
                                          capture(captureRowMapper)))
          .andReturn(mockApPredictVO);

    mocksControl.replay();

    final ApPredictVO retrieved = appManagerDAO.retrieveObjectValue(dummyAppManagerId);

    mocksControl.verify();

    assertEquals("SELECT OBJECT_VALUE FROM APP_MANAGER WHERE APP_MANAGER_ID=?",
                 captureSelectSQL.getValue());
    assertSame(dummyAppManagerId, captureArgs.getValue()[0]);
    assertSame(mockApPredictVO, retrieved);
  }

  @Test
  public void testRetrieveDiagnostics() {
    /*
     * Retrieve a null diagnostics value object.
     */
    Capture<String> captureSelectSQL = newCapture();
    Capture<Object[]> captureSelectArgs = newCapture();
    Capture<RowMapper<DiagnosticsVO>> captureSelectRowMapper = newCapture();

    expect(mockAppTemplate.queryForObject(capture(captureSelectSQL),
                                          capture(captureSelectArgs),
                                          capture(captureSelectRowMapper)))
          .andReturn(null);

    mocksControl.replay();

    DiagnosticsVO retrieved = appManagerDAO.retrieveDiagnostics(dummyAppManagerId);

    mocksControl.verify();

    assertNull(retrieved);

    mocksControl.reset();

    /*
     * Retrieval throws an exception.
     */
    captureSelectSQL = newCapture();
    captureSelectArgs = newCapture();
    captureSelectRowMapper = newCapture();

    expect(mockAppTemplate.queryForObject(capture(captureSelectSQL),
                                          capture(captureSelectArgs),
                                          capture(captureSelectRowMapper)))
          .andThrow(new DataIntegrityViolationException("fakeDataIntegrityViolationException"));

    mocksControl.replay();

    retrieved = appManagerDAO.retrieveDiagnostics(dummyAppManagerId);

    mocksControl.verify();

    assertNull(retrieved);

    mocksControl.reset();

    /*
     * Retrieval retrieves diagnostics (but containing null data).
     */
    captureSelectSQL = newCapture();
    captureSelectArgs = newCapture();
    captureSelectRowMapper = newCapture();

    DiagnosticsVO mockDiagnosticsVO = mocksControl.createMock(DiagnosticsVO.class);
    expect(mockAppTemplate.queryForObject(capture(captureSelectSQL),
                                          capture(captureSelectArgs),
                                          capture(captureSelectRowMapper)))
          .andReturn(mockDiagnosticsVO);
    expect(mockDiagnosticsVO.getInfo()).andReturn(null);
    expect(mockDiagnosticsVO.getOutput()).andReturn(null);

    mocksControl.replay();

    retrieved = appManagerDAO.retrieveDiagnostics(dummyAppManagerId);

    mocksControl.verify();

    assertNull(retrieved);

    mocksControl.reset();

    /*
     * Retrieval retrieves diagnostics (containing data).
     */
    captureSelectSQL = newCapture();
    captureSelectArgs = newCapture();
    captureSelectRowMapper = newCapture();

    mockDiagnosticsVO = mocksControl.createMock(DiagnosticsVO.class);
    expect(mockAppTemplate.queryForObject(capture(captureSelectSQL),
                                          capture(captureSelectArgs),
                                          capture(captureSelectRowMapper)))
          .andReturn(mockDiagnosticsVO);
    expect(mockDiagnosticsVO.getInfo()).andReturn(null);
    final String dummyOutput = "dummyOutput";
    expect(mockDiagnosticsVO.getOutput()).andReturn(dummyOutput);

    mocksControl.replay();

    retrieved = appManagerDAO.retrieveDiagnostics(dummyAppManagerId);

    mocksControl.verify();

    assertSame(mockDiagnosticsVO, retrieved);

    mocksControl.reset();

    /*
     * Retrieval retrieves diagnostics (containing data).
     */
    captureSelectSQL = newCapture();
    captureSelectArgs = newCapture();
    captureSelectRowMapper = newCapture();

    mockDiagnosticsVO = mocksControl.createMock(DiagnosticsVO.class);
    expect(mockAppTemplate.queryForObject(capture(captureSelectSQL),
                                          capture(captureSelectArgs),
                                          capture(captureSelectRowMapper)))
          .andReturn(mockDiagnosticsVO);
    final String dummyInfo = "dummyInfo";
    expect(mockDiagnosticsVO.getInfo()).andReturn(dummyInfo);

    mocksControl.replay();

    retrieved = appManagerDAO.retrieveDiagnostics(dummyAppManagerId);

    mocksControl.verify();

    assertSame(mockDiagnosticsVO, retrieved);
  }

  @Test
  public void testRetrieveSimulationProcessId() {
    /*
     * Retrieval throws an exception.
     */
    Capture<String> captureSelectSQL = newCapture();
    Capture<Class<String>> captureSelectClass = newCapture();
    Capture<Long> captureSelectArg = newCapture();

    expect(mockAppTemplate.queryForObject(capture(captureSelectSQL),
                                          capture(captureSelectClass),
                                          captureLong(captureSelectArg)))
          .andThrow(new DataIntegrityViolationException("fakeDataIntegrityViolationException"));

    mocksControl.replay();

    String retrieved = appManagerDAO.retrieveSimulationProcessId(dummyAppManagerId);

    mocksControl.verify();

    String capturedSelectSQL = captureSelectSQL.getValue();
    assertEquals("SELECT PROCESS_ID FROM APP_MANAGER WHERE APP_MANAGER_ID=?",
                 capturedSelectSQL);
    Long capturedSelectArg = captureSelectArg.getValue();
    assertSame(dummyAppManagerId, capturedSelectArg);
    Class<String> capturedSelectClass = captureSelectClass.getValue();
    assertNotNull(capturedSelectClass);
    assertNull(retrieved);

    mocksControl.reset();

    /*
     * Retrieve returns simulation process id.
     */
    captureSelectSQL = newCapture();
    captureSelectClass = newCapture();
    captureSelectArg = newCapture();

    final String dummyProcessId = "dummyProcessId";
    expect(mockAppTemplate.queryForObject(capture(captureSelectSQL),
                                          capture(captureSelectClass),
                                          captureLong(captureSelectArg)))
          .andReturn(dummyProcessId);

    mocksControl.replay();

    retrieved = appManagerDAO.retrieveSimulationProcessId(dummyAppManagerId);

    mocksControl.verify();

    capturedSelectSQL = captureSelectSQL.getValue();
    assertEquals("SELECT PROCESS_ID FROM APP_MANAGER WHERE APP_MANAGER_ID=?",
                 capturedSelectSQL);
    capturedSelectArg = captureSelectArg.getValue();
    assertSame(dummyAppManagerId, capturedSelectArg);
    capturedSelectClass = captureSelectClass.getValue();
    assertNotNull(capturedSelectClass);
    assertEquals(dummyProcessId, retrieved);
  }

  @Test
  public void testSaveSimulationDiagnostics() {
    final Capture<String> captureUpdateSQL = newCapture();
    final Capture<String> captureUpdateArg1 = newCapture();
    final Capture<String> captureUpdateArg2 = newCapture();
    final Capture<Long> captureUpdateArg3 = newCapture();

    final int dummyUpdateInt = 2;
    expect(mockAppTemplate.update(capture(captureUpdateSQL),
                                  capture(captureUpdateArg1),
                                  capture(captureUpdateArg2),
                                  captureLong(captureUpdateArg3)))
          .andReturn(dummyUpdateInt);

    mocksControl.replay();

    final String dummyInfo = "dummyInfo";
    final String dummyOutput = "dummyOutput";

    appManagerDAO.saveSimulationDiagnostics(dummyAppManagerId, dummyInfo, dummyOutput);

    mocksControl.verify();

    final String capturedUpdateSQL = captureUpdateSQL.getValue();
    assertEquals("UPDATE APP_MANAGER SET INFO=?,OUTPUT=? WHERE APP_MANAGER_ID=?", capturedUpdateSQL);
    final String capturedUpdateArg1 = captureUpdateArg1.getValue();
    assertSame(dummyInfo, capturedUpdateArg1);
    final String capturedUpdateArg2 = captureUpdateArg2.getValue();
    assertSame(dummyOutput, capturedUpdateArg2);
    final Long capturedUpdateArg3 = captureUpdateArg3.getValue();
    assertSame(dummyAppManagerId, capturedUpdateArg3);
  }

  @Test
  public void testRetrieveSimulationInfo() {
    /*
     * Retrieval throws an exception
     */
    Capture<String> captureSelectSQL = newCapture();
    Capture<Object[]> captureSelectArgs = newCapture();
    Capture<RowMapper<SimulationInfoVO>> captureSelectRowMapper = newCapture();

    expect(mockAppTemplate.queryForObject(capture(captureSelectSQL),
                                          capture(captureSelectArgs),
                                          capture(captureSelectRowMapper)))
          .andThrow(new DataIntegrityViolationException("fakeDataIntegrityViolationException"));

    mocksControl.replay();

    SimulationInfoVO retrieved = appManagerDAO.retrieveSimulationInfo(dummyAppManagerId);

    mocksControl.verify();

    String capturedSelectSQL = captureSelectSQL.getValue();
    assertEquals("SELECT INVOCATION_SOURCE,SIMULATION_TYPE FROM APP_MANAGER WHERE APP_MANAGER_ID=?",
                 capturedSelectSQL);
    Object[] capturedSelectArgs = captureSelectArgs.getValue();
    assertSame(dummyAppManagerId, capturedSelectArgs[0]);
    RowMapper<SimulationInfoVO> capturedSelectRowMapper = captureSelectRowMapper.getValue();
    assertNotNull(capturedSelectRowMapper);
    assertNull(retrieved);

    mocksControl.reset();

    /*
     * Retrieve returns simulation info.
     */
    captureSelectSQL = newCapture();
    captureSelectArgs = newCapture();
    captureSelectRowMapper = newCapture();

    final SimulationInfoVO mockSimulationInfo = mocksControl.createMock(SimulationInfoVO.class);
    expect(mockAppTemplate.queryForObject(capture(captureSelectSQL),
                                          capture(captureSelectArgs),
                                          capture(captureSelectRowMapper)))
          .andReturn(mockSimulationInfo);

    mocksControl.replay();

    retrieved = appManagerDAO.retrieveSimulationInfo(dummyAppManagerId);

    mocksControl.verify();

    capturedSelectSQL = captureSelectSQL.getValue();
    assertEquals("SELECT INVOCATION_SOURCE,SIMULATION_TYPE FROM APP_MANAGER WHERE APP_MANAGER_ID=?",
                 capturedSelectSQL);
    capturedSelectArgs = captureSelectArgs.getValue();
    assertSame(dummyAppManagerId, capturedSelectArgs[0]);
    capturedSelectRowMapper = captureSelectRowMapper.getValue();
    assertNotNull(capturedSelectRowMapper);
    assertSame(mockSimulationInfo, retrieved);
  }

  @Test
  public void testSaveSimulationMessages() throws Exception {
    final Capture<String> captureUpdateSQL = newCapture();
    final Capture<String> captureUpdateArg1 = newCapture();
    final Capture<Long> captureUpdateArg2 = newCapture();

    final int dummyUpdateInt1 = 5;
    expect(mockAppTemplate.update(capture(captureUpdateSQL),
                                  capture(captureUpdateArg1),
                                  captureLong(captureUpdateArg2)))
          .andReturn(dummyUpdateInt1);
    final RunStatus mockRunStatus = mocksControl.createMock(RunStatus.class);
    expectNew(RunStatus.class, dummyAppManagerId, null, RunStatus.WARN_PREFIX,
                               "Messages generated for simulation")
             .andReturn(mockRunStatus);
    // >> addStatus()
    expect(mockRunStatus.getId()).andReturn(dummyAppManagerId);
    //                >> retrieveSimulationMessages()
    final String dummyMessages = "dummyMessages";
    final Capture<String> captureSelectSQL = newCapture();
    final Capture<Class<String>> captureSelectRequiredType = newCapture();
    final Capture<Long> captureSelectArg1 = newCapture();
    expect(mockAppTemplate.queryForObject(capture(captureSelectSQL),
                                          capture(captureSelectRequiredType),
                                          captureLong(captureSelectArg1)))
          .andReturn(dummyMessages);
    //                << retrieveSimulationMessages()
    final String dummyLevel = "dummyLevel";
    expect(mockRunStatus.getLevel()).andReturn(dummyLevel);
    final String dummyStatus = "dummyStatus";
    expect(mockRunStatus.getStatus()).andReturn(dummyStatus);
    final Capture<String> captureInsertSQL = newCapture();
    final Capture<Long> captureInsertArg1 = newCapture();
    final Capture<String> captureInsertArg2 = newCapture();
    final Capture<String> captureInsertArg3 = newCapture();
    final int dummyUpdateInt2 = 2;
    expect(mockAppTemplate.update(capture(captureInsertSQL),
                                  captureLong(captureInsertArg1),
                                  capture(captureInsertArg2),
                                  capture(captureInsertArg3)))
          .andReturn(dummyUpdateInt2);
    // << addStatus()

    replayAll();
    mocksControl.replay();

    appManagerDAO.saveSimulationMessages(dummyAppManagerId, dummyMessages);

    verifyAll();
    mocksControl.verify();

    final String capturedUpdateSQL = captureUpdateSQL.getValue();
    assertEquals("UPDATE APP_MANAGER SET MESSAGES=? WHERE APP_MANAGER_ID=?", capturedUpdateSQL);
    final String capturedUpdateArg1 = captureUpdateArg1.getValue();
    assertSame(dummyMessages, capturedUpdateArg1);
    final Long capturedUpdateArg2 = captureUpdateArg2.getValue();
    assertSame(dummyAppManagerId, capturedUpdateArg2);
  }

  @Test
  public void testSaveSimulationProcessId() {
    final Capture<String> captureUpdateSQL = newCapture();
    final Capture<String> captureUpdateArg1 = newCapture();
    final Capture<Long> captureUpdateArg2 = newCapture();

    final int dummyUpdateInt = 5;
    expect(mockAppTemplate.update(capture(captureUpdateSQL),
                                  capture(captureUpdateArg1),
                                  captureLong(captureUpdateArg2)))
          .andReturn(dummyUpdateInt);

    mocksControl.replay();

    final String dummyProcessId = "dummyProcessId";
    appManagerDAO.saveSimulationProcessId(dummyAppManagerId, dummyProcessId);

    mocksControl.verify();

    final String capturedUpdateSQL = captureUpdateSQL.getValue();
    assertEquals("UPDATE APP_MANAGER SET PROCESS_ID=? WHERE APP_MANAGER_ID=?", capturedUpdateSQL);
    final String capturedUpdateArg1 = captureUpdateArg1.getValue();
    assertSame(dummyProcessId, capturedUpdateArg1);
    final Long capturedUpdateArg2 = captureUpdateArg2.getValue();
    assertSame(dummyAppManagerId, capturedUpdateArg2);
  }

  @Test
  public void testSaveSimulationResults() throws Exception {
    final List<SimulationResultsVO> dummySimulationResults = new ArrayList<SimulationResultsVO>();
    final Map<String, String> dummyPKResults = new HashMap<String, String>();

    /*
     * No Delta APD90 pctile names (or PK results).
     */
    String dummyDeltaAPD90PercentileNames = null;

    Capture<String> captureBatchUpdateSQL = newCapture();
    final int[] dummyUpdateInts = new int[] {};
    expect(mockAppTemplate.batchUpdate(capture(captureBatchUpdateSQL),
                                       isA(BatchPreparedStatementSetter.class)))
          .andReturn(dummyUpdateInts);

    final RunStatus mockRunStatus = mocksControl.createMock(RunStatus.class);
    Capture<Long> captureRSId = newCapture();
    Capture<String> captureRSTimestamp = newCapture();
    Capture<String> captureRSLevel = newCapture();
    Capture<String> captureRSStatus = newCapture();
    expectNew(RunStatus.class, captureLong(captureRSId),
                               capture(captureRSTimestamp),
                               capture(captureRSLevel),
                               capture(captureRSStatus))
             .andReturn(mockRunStatus);
    // Not interested in testing any of the following here!
    // >> addStatus()
    expect(mockRunStatus.getId()).andReturn(dummyAppManagerId);
    //                >> retrieveSimulationMessages()
    expect(mockAppTemplate.queryForObject(anyString(), eq(String.class),
                                          anyLong()))
          .andThrow(new EmptyResultDataAccessException(1));
    // << addStatus() << retrieveSimulationMessages()

    replayAll();
    mocksControl.replay();

    appManagerDAO.saveSimulationResults(dummyAppManagerId,
                                        dummySimulationResults, dummyPKResults,
                                        dummyDeltaAPD90PercentileNames);

    verifyAll();
    mocksControl.verify();

    assertEquals("INSERT INTO SIMULATION_RESULTS (APP_MANAGER_ID,REFERENCE,AP_TIMES,AP_VOLTAGES,APD90,DELTA_APD90,QNET) VALUES (?,?,?,?,?,?,?)",
                 captureBatchUpdateSQL.getValue());

    assertEquals(Long.valueOf(dummyAppManagerId), captureRSId.getValue());
    assertNull(captureRSTimestamp.getValue());
    assertSame(RunStatus.DEBUG_PREFIX, captureRSLevel.getValue());
    assertTrue(captureRSStatus.getValue()
                              .contains(String.valueOf(dummyAppManagerId)));

    resetAll();
    mocksControl.reset();

    /*
     * Now with Delta APD90 pctile names (but still no PK results).
     */
    dummyDeltaAPD90PercentileNames = "dummyDeltaAPD90PercentileNames";

    captureBatchUpdateSQL = newCapture();
    expect(mockAppTemplate.batchUpdate(anyString(),
                                       isA(BatchPreparedStatementSetter.class)))
          .andReturn(dummyUpdateInts);

    final Capture<String> captureUpdateSQL = newCapture();
    final Capture<String> captureDeltaAPD90PctileNames = newCapture();
    final Capture<Long> captureAppManagerId = newCapture();
    final Integer dummyUpdateInt = 2;
    expect(mockAppTemplate.update(capture(captureUpdateSQL),
                                  capture(captureDeltaAPD90PctileNames),
                                  captureLong(captureAppManagerId)))
          .andReturn(dummyUpdateInt);

    expectNew(RunStatus.class, anyLong(), isNull(), anyString(), anyString())
             .andReturn(mockRunStatus);
    // Still not interested in testing any of the following here!
    // >> addStatus()
    expect(mockRunStatus.getId()).andReturn(dummyAppManagerId);
    //                >> retrieveSimulationMessages()
    expect(mockAppTemplate.queryForObject(anyString(), eq(String.class),
                                          anyLong()))
          .andThrow(new EmptyResultDataAccessException(1));
    // << addStatus() << retrieveSimulationMessages()

    replayAll();
    mocksControl.replay();

    appManagerDAO.saveSimulationResults(dummyAppManagerId,
                                        dummySimulationResults, dummyPKResults,
                                        dummyDeltaAPD90PercentileNames);

    verifyAll();
    mocksControl.verify();

    assertEquals("UPDATE APP_MANAGER SET DELTA_APD90_PERCENTILE_NAMES=? WHERE APP_MANAGER_ID=?",
                 captureUpdateSQL.getValue());
    assertEquals(dummyDeltaAPD90PercentileNames,
                 captureDeltaAPD90PctileNames.getValue());
    assertEquals(Long.valueOf(dummyAppManagerId),
                 captureAppManagerId.getValue());
  }
}