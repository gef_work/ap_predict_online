/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.prerun;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.io.IOException;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import uk.ac.ox.cs.nc3rs.app_manager.util.FileUtil;
import uk.ac.ox.cs.nc3rs.app_manager.value.ApPredictVO;

/**
 * Unit test the ApPredict run pre-processor.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ ApPredictRunPreProcessor.class, FileUtil.class,
                  Runtime.class })
public class ApPredictRunPreProcessorTest {

  private static final String prepareScript = "./prepare.sh";

  private ApPredictRunPreProcessor apPredictRunPreProcessor;
  private ApPredictVO mockApPredictVO;
  private boolean dummyHasCellMLData;
  private boolean dummyIsPKSimulation;
  private IMocksControl mocksControl;
  private static final int dummyAppManagerId = 4;
  private Process mockProcess;
  private Runtime mockRuntime;
  private static final String dummyBaseDir = "dummyBaseDir";
  private String dummyCellMLFileName;
  private String dummyCellMLFileLocation;
  private String dummyPKFileLocation;
  private static final String dummyJobDir = "dummyJobDir";

  @Before
  public void setUp() {
    apPredictRunPreProcessor = new ApPredictRunPreProcessor();

    mocksControl = createStrictControl();

    mockApPredictVO = mocksControl.createMock(ApPredictVO.class);
    mockProcess = mocksControl.createMock(Process.class);
    mockRuntime = mocksControl.createMock(Runtime.class);

    dummyHasCellMLData = false;
    dummyIsPKSimulation = false;
    dummyCellMLFileName = null;
    dummyCellMLFileLocation = null;
    dummyPKFileLocation = null;
  }

  @Test
  public void testPreProcessApPredict() throws IOException, InterruptedException {
    apPredictRunPreProcessor.setBaseDir(dummyBaseDir);

    /*
     * Note: Assuming running on Linux system, i.e. AppIdentifiers calls to 
     *       static properties are not mocked.
     */

    /*
     * No CellML file and not a PK simulation.
     */
    mockStatic(FileUtil.class);
    expect(FileUtil.retrieveJobDirectory(dummyBaseDir, dummyAppManagerId))
          .andReturn(dummyJobDir);
    expect(mockApPredictVO.hasCellMLData()).andReturn(dummyHasCellMLData);
    expect(mockApPredictVO.isPKSimulation()).andReturn(dummyIsPKSimulation);
    mockStatic(Runtime.class);
    expect(Runtime.getRuntime()).andReturn(mockRuntime);
    Capture<String[]> capturePreparerArgs = newCapture();
    expect(mockRuntime.exec(capture(capturePreparerArgs)))
          .andReturn(mockProcess);
    final int dummyExitValue = 4;
    expect(mockProcess.waitFor()).andReturn(dummyExitValue);

    replayAll();
    mocksControl.replay();

    String returned = apPredictRunPreProcessor.preProcessApPredict(dummyAppManagerId,
                                                                   mockApPredictVO);

    verifyAll();
    mocksControl.verify();

    String[] capturedPreparerArgs = capturePreparerArgs.getValue();
    assertSame(5, capturedPreparerArgs.length);
    assertEquals(prepareScript, capturedPreparerArgs[0]);
    assertEquals(dummyJobDir, capturedPreparerArgs[1]);
    assertEquals("", capturedPreparerArgs[2]);
    assertEquals("", capturedPreparerArgs[3]);
    assertEquals("", capturedPreparerArgs[4]);
    assertNotNull(returned);

    resetAll();
    mocksControl.reset();

    /*
     * CellML file present, not a PK simulation.
     */
    dummyCellMLFileName = "dummyCellMLFileName";
    dummyCellMLFileLocation = "dummyCellMLFileLocation";

    expect(FileUtil.retrieveJobDirectory(dummyBaseDir, dummyAppManagerId))
          .andReturn(dummyJobDir);
    dummyHasCellMLData = true;
    expect(mockApPredictVO.hasCellMLData()).andReturn(dummyHasCellMLData);
    expect(mockApPredictVO.getCellMLFileName()).andReturn(dummyCellMLFileName);
    expect(mockApPredictVO.getCellMLFileLocation())
          .andReturn(dummyCellMLFileLocation);
    expect(mockApPredictVO.isPKSimulation()).andReturn(dummyIsPKSimulation);
    expect(Runtime.getRuntime()).andReturn(mockRuntime);
    expect(mockRuntime.exec(capture(capturePreparerArgs)))
          .andReturn(mockProcess);
    expect(mockProcess.waitFor()).andReturn(dummyExitValue);

    replayAll();
    mocksControl.replay();

    returned = apPredictRunPreProcessor.preProcessApPredict(dummyAppManagerId,
                                                            mockApPredictVO);


    verifyAll();
    mocksControl.verify();

    capturedPreparerArgs = capturePreparerArgs.getValue();
    assertSame(5, capturedPreparerArgs.length);
    assertEquals(prepareScript, capturedPreparerArgs[0]);
    assertEquals(dummyJobDir, capturedPreparerArgs[1]);
    assertEquals(dummyCellMLFileName, capturedPreparerArgs[2]);
    assertEquals(dummyCellMLFileLocation, capturedPreparerArgs[3]);
    assertEquals("", capturedPreparerArgs[4]);

    resetAll();
    mocksControl.reset();

    /*
     * CellML file not present, PK simulation.
     */
    dummyPKFileLocation = "dummyPKFileLocation";
    expect(FileUtil.retrieveJobDirectory(dummyBaseDir, dummyAppManagerId))
          .andReturn(dummyJobDir);
    dummyHasCellMLData = false;
    expect(mockApPredictVO.hasCellMLData()).andReturn(dummyHasCellMLData);
    dummyIsPKSimulation = true;
    expect(mockApPredictVO.isPKSimulation()).andReturn(dummyIsPKSimulation);
    expect(mockApPredictVO.getPKFileLocation()).andReturn(dummyPKFileLocation);
    expect(Runtime.getRuntime()).andReturn(mockRuntime);
    expect(mockRuntime.exec(capture(capturePreparerArgs)))
          .andReturn(mockProcess);
    expect(mockProcess.waitFor()).andReturn(dummyExitValue);

    replayAll();
    mocksControl.replay();

    returned = apPredictRunPreProcessor.preProcessApPredict(dummyAppManagerId,
                                                            mockApPredictVO);


    verifyAll();
    mocksControl.verify();

    capturedPreparerArgs = capturePreparerArgs.getValue();
    assertSame(5, capturedPreparerArgs.length);
    assertEquals(prepareScript, capturedPreparerArgs[0]);
    assertEquals(dummyJobDir, capturedPreparerArgs[1]);
    assertEquals("", capturedPreparerArgs[2]);
    assertEquals("", capturedPreparerArgs[3]);
    assertEquals(dummyPKFileLocation, capturedPreparerArgs[4]);

    resetAll();
    mocksControl.reset();

    /*
     * CellML file present, PK simulation.
     */
    expect(FileUtil.retrieveJobDirectory(dummyBaseDir, dummyAppManagerId))
          .andReturn(dummyJobDir);
    dummyHasCellMLData = true;
    expect(mockApPredictVO.hasCellMLData()).andReturn(dummyHasCellMLData);
    expect(mockApPredictVO.getCellMLFileName()).andReturn(dummyCellMLFileName);
    expect(mockApPredictVO.getCellMLFileLocation())
          .andReturn(dummyCellMLFileLocation);
    expect(mockApPredictVO.isPKSimulation()).andReturn(dummyIsPKSimulation);
    expect(mockApPredictVO.getPKFileLocation()).andReturn(dummyPKFileLocation);
    expect(Runtime.getRuntime()).andReturn(mockRuntime);
    expect(mockRuntime.exec(capture(capturePreparerArgs)))
          .andReturn(mockProcess);
    expect(mockProcess.waitFor()).andReturn(dummyExitValue);

    replayAll();
    mocksControl.replay();

    returned = apPredictRunPreProcessor.preProcessApPredict(dummyAppManagerId,
                                                            mockApPredictVO);


    verifyAll();
    mocksControl.verify();

    capturedPreparerArgs = capturePreparerArgs.getValue();
    assertSame(5, capturedPreparerArgs.length);
    assertEquals(prepareScript, capturedPreparerArgs[0]);
    assertEquals(dummyJobDir, capturedPreparerArgs[1]);
    assertEquals(dummyCellMLFileName, capturedPreparerArgs[2]);
    assertEquals(dummyCellMLFileLocation, capturedPreparerArgs[3]);
    assertEquals(dummyPKFileLocation, capturedPreparerArgs[4]);
  }
}