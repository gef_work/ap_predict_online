/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.postrun.reader;

import static org.easymock.EasyMock.anyString;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.newCapture;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO;
import uk.ac.ox.cs.nc3rs.app_manager.util.FileUtil;
import uk.ac.ox.cs.nc3rs.app_manager.util.FileUtil.AlphabeticComparator;
import uk.ac.ox.cs.nc3rs.app_manager.util.FileUtil.DirFilter;
import uk.ac.ox.cs.nc3rs.app_manager.value.QNetResultsVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.SimulationResultsVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.VoltageResultsVO;

/**
 * Unit test the abstract run results reader class.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { AbstractRunResultsReader.class, File.class, FileUtil.class,
                   QNetResultsVO.class, SimulationResultsVO.class,
                   VoltageResultsVO.class } )
public class AbstractRunResultsReaderTest {

  private class FakeRunResultsReader extends AbstractRunResultsReader {}

  private static final String fileNameMessages = "messages.txt";
  private static final String fileNameVoltageResults = "voltage_results.dat";
  private static final String apPredictVoltageTraceRegex = "^.*conc_\\d+.*_voltage_trace\\.dat$";

  private AppManagerDAO mockAppManagerDAO;
  private FakeRunResultsReader fakeRunResultsReader;
  private IMocksControl mocksControl;
  private List<SimulationResultsVO> dummySimulationResults;
  private long dummyAppManagerId = 4l;
  private String dummyBaseDir;
  private String dummyResultFilesDir;

  @Before
  public void setUp() {
    fakeRunResultsReader = new FakeRunResultsReader();

    mocksControl = createStrictControl();
    mockAppManagerDAO = mocksControl.createMock(AppManagerDAO.class);

    ReflectionTestUtils.setField(fakeRunResultsReader,
                                 AppIdentifiers.COMPONENT_JDBC_APP_MANAGER_DAO,
                                 mockAppManagerDAO);

    dummyBaseDir = "dummyBaseDir";
    dummyResultFilesDir = "dummyResultFilesDir";
    dummySimulationResults = new ArrayList<SimulationResultsVO>();
    fakeRunResultsReader.setBaseDir(dummyBaseDir);
  }

  @Test
  public void testGettersSetters() {
    assertSame(mockAppManagerDAO, fakeRunResultsReader.getAppManagerDAO());
    assertEquals(dummyBaseDir, fakeRunResultsReader.getBaseDir());
  }

  @Test
  public void testGeneralDataLoading() throws Exception {
    /*
     * No files found (including no voltages results - at which point croak!).
     */
    // >> persistMessages()
    final Capture<String> captureMessagesFilePath = newCapture();
    final File mockMessagesFile = mocksControl.createMock(File.class);
    expectNew(File.class, new Class[] { String.class },
                          capture(captureMessagesFilePath))
             .andReturn(mockMessagesFile);
    mockStatic(FileUtil.class);
    final List<String> dummyMessageLines = new ArrayList<String>();
    expect(FileUtil.fileLineReader(mockMessagesFile, true, false, false))
          .andReturn(dummyMessageLines);
    // << persistMessages()
  
    // >> readVoltageResults()
    final Capture<String> captureVoltageResultsFilePath = newCapture();
    final File mockVoltageResultsFile = mocksControl.createMock(File.class);
    expectNew(File.class, new Class[] { String.class },
                          capture(captureVoltageResultsFilePath))
             .andReturn(mockVoltageResultsFile);
    final String dummyFileNotFoundException = "dummyFileNotFoundException";
    expect(FileUtil.fileLineReader(mockVoltageResultsFile, true, false, true))
             .andThrow(new FileNotFoundException(dummyFileNotFoundException));
    // << readVoltageResults()

    replayAll();
    mocksControl.replay();

    try {
      fakeRunResultsReader.generalDataLoading(dummyAppManagerId,
                                              dummyResultFilesDir,
                                              dummySimulationResults);
      fail("Should have thrown an exception when it couldn't find voltagesShould not happen!");
    } catch (UnsupportedOperationException e) {}

    verifyAll();
    mocksControl.verify();

    final String capturedMessagesFilePath = captureMessagesFilePath.getValue();
    assertEquals(dummyResultFilesDir.concat(fileNameMessages),
                 capturedMessagesFilePath);
    final String capturedVoltageResultsFilePath = captureVoltageResultsFilePath.getValue();
    assertEquals(dummyResultFilesDir.concat(fileNameVoltageResults),
                 capturedVoltageResultsFilePath);

    resetAll();
    mocksControl.reset();

    /*
     * All files found.
     */
    // >> persistMessages()
    expectNew(File.class, new Class[] { String.class }, anyString())
             .andReturn(mockMessagesFile);
    final String dummyMessage1 = "dummyMessage1";
    final String dummyMessage2 = "dummyMessage2";
    dummyMessageLines.add(dummyMessage1);
    dummyMessageLines.add(dummyMessage2);
    expect(FileUtil.fileLineReader(mockMessagesFile, true, false, false))
          .andReturn(dummyMessageLines);
    final Capture<String> captureMessages = newCapture();
    mockAppManagerDAO.saveSimulationMessages(eq(dummyAppManagerId),
                                             capture(captureMessages));
    // << persistMessages()

    // >> readVoltageResults()
    expectNew(File.class, new Class[] { String.class }, anyString())
             .andReturn(mockVoltageResultsFile);
    // Prepare voltage results header line
    final String dummyHdrConcentration = "dummyHdrConcentration";
    final String dummyHdrUpstrokeVelocity = "dummyHdrUpstrokeVelocity";
    final String dummyHdrPeakVm = "dummyHdrPeakVm";
    final String dummyHdrAPD50 = "dummyHdrAPD50";
    final String dummyHdrAPD90 = "dummyHdrAPD90";
    final String dummyHdrDeltaAPD90 = "dummyHdrDeltaAPD90";
    final List<String> vrLine1 = new ArrayList<String>();
    vrLine1.add(dummyHdrConcentration);
    vrLine1.add(dummyHdrUpstrokeVelocity);
    vrLine1.add(dummyHdrPeakVm);
    vrLine1.add(dummyHdrAPD50);
    vrLine1.add(dummyHdrAPD90);
    vrLine1.add(dummyHdrDeltaAPD90);
    final String dummyVoltageResultsLine1 = StringUtils.join(vrLine1,
                                                             AbstractRunResultsReader.tab);
    // Prepare first results line of voltage results
    final String dummyConcentration = "dummyConcentration";
    final String dummyUpstrokeVelocity = "dummyUpstrokeVelocity";
    final String dummyPeakVm = "dummyPeakVm";
    final String dummyAPD50 = "dummyAPD50";
    final String dummyAPD90 = "dummyAPD90";
    final String dummyDeltaAPD90 = "dummyDeltaAPD90";
    final List<String> vrLine2 = new ArrayList<String>();
    vrLine2.add(dummyConcentration);
    vrLine2.add(dummyUpstrokeVelocity);
    vrLine2.add(dummyPeakVm);
    vrLine2.add(dummyAPD50);
    vrLine2.add(dummyAPD90);
    vrLine2.add(dummyDeltaAPD90);
    final String dummyVoltageResultsLine2 = StringUtils.join(vrLine2,
                                                             AbstractRunResultsReader.tab);
 
    final List<String> dummyVoltageResultsLines = new ArrayList<String>();
    dummyVoltageResultsLines.add(dummyVoltageResultsLine1);
    dummyVoltageResultsLines.add(dummyVoltageResultsLine2);
    expect(FileUtil.fileLineReader(mockVoltageResultsFile, true, false, true))
          .andReturn(dummyVoltageResultsLines);

    final VoltageResultsVO mockVoltageResultsVO = mocksControl.createMock(VoltageResultsVO.class);
    expectNew(VoltageResultsVO.class, dummyHdrConcentration,
                                      dummyHdrUpstrokeVelocity,
                                      dummyHdrPeakVm, dummyHdrAPD50,
                                      dummyHdrAPD90, dummyHdrDeltaAPD90)
             .andReturn(mockVoltageResultsVO);
    expectNew(VoltageResultsVO.class, dummyConcentration, dummyUpstrokeVelocity,
                                      dummyPeakVm, dummyAPD50, dummyAPD90,
                                      dummyDeltaAPD90)
             .andReturn(mockVoltageResultsVO);
    // << readVoltageResults()

    // >> readQNet()
    final File mockQNetResultsFile = mocksControl.createMock(File.class);
    expectNew(File.class, new Class[] { String.class }, anyString())
             .andReturn(mockQNetResultsFile);
    final List<String> dummyQNetResultsLines = new ArrayList<String>();
    final String dummyQNet = "dummyQNet";
    final List<String> qNetLine1 = new ArrayList<String>();
    qNetLine1.add(dummyConcentration);
    qNetLine1.add(dummyQNet);
    final String dummyQNetResultsLine1 = StringUtils.join(qNetLine1,
                                                          AbstractRunResultsReader.tab);
    dummyQNetResultsLines.add(dummyQNetResultsLine1);
    expect(FileUtil.fileLineReader(mockQNetResultsFile, true, true, false))
          .andReturn(dummyQNetResultsLines);
    final QNetResultsVO mockQNetResultsVO = mocksControl.createMock(QNetResultsVO.class);
    expectNew(QNetResultsVO.class, dummyConcentration, dummyQNet)
             .andReturn(mockQNetResultsVO);
    // << readQNet()

    // >> readConcentrationVoltages()
    final String dummyConcVoltagesFileName = "conc_".concat(dummyConcentration)
                                                    .concat("_voltage_trace.dat");
    final String[] dummyFileList = new String[] { dummyConcVoltagesFileName };
    final Capture<DirFilter> captureDirFilter = newCapture();
    mockStatic(FileUtils.class);
    expect(FileUtil.retrieveFiles(capture(captureDirFilter),
                                  isA(AlphabeticComparator.class),
                                  eq(dummyResultFilesDir), eq(true)))
          .andReturn(dummyFileList);
    final Capture<String> captureConcVoltagesFilePath = newCapture();
    final File mockConcVoltagesFile = mocksControl.createMock(File.class);
    expectNew(File.class, new Class[] { String.class },
                          capture(captureConcVoltagesFilePath))
             .andReturn(mockConcVoltagesFile);

    // Emulate Time(ms) vs. Membrance Voltage(mV)
    final String dummyTime1 = "-5";
    final String dummyMembraneVoltage1 = "-85.4172";
    final String dummyVoltageTraceLine1 = dummyTime1.concat(AbstractRunResultsReader.tab)
                                                    .concat(dummyMembraneVoltage1);
    final String dummyTime2 = "0";
    final String dummyMembraneVoltage2 = "-85.4176";
    final String dummyVoltageTraceLine2 = dummyTime2.concat(AbstractRunResultsReader.tab)
                                                    .concat(dummyMembraneVoltage2);
    final List<String> dummyVoltageTraceLines = new ArrayList<String>();
    dummyVoltageTraceLines.add(dummyVoltageTraceLine1);
    dummyVoltageTraceLines.add(dummyVoltageTraceLine2);
    expect(FileUtil.fileLineReader(mockConcVoltagesFile, true, true, true))
          .andReturn(dummyVoltageTraceLines);
    // << readConcentrationVoltages()

    expect(mockQNetResultsVO.getqNet()).andReturn(dummyQNet);
    expect(mockVoltageResultsVO.getAPD90()).andReturn(dummyAPD90);
    expect(mockVoltageResultsVO.getDeltaAPD90()).andReturn(dummyDeltaAPD90);
    final SimulationResultsVO mockSimulationResultsVO = mocksControl.createMock(SimulationResultsVO.class);
    expectNew(SimulationResultsVO.class, dummyConcentration,
                                         dummyTime1.concat(",").concat(dummyTime2),
                                         dummyMembraneVoltage1.concat(",").concat(dummyMembraneVoltage2),
                                         dummyAPD90, dummyDeltaAPD90, dummyQNet)
             .andReturn(mockSimulationResultsVO);

    replayAll();
    mocksControl.replay();

    final String pctileNames = fakeRunResultsReader.generalDataLoading(dummyAppManagerId,
                                                                       dummyResultFilesDir,
                                                                       dummySimulationResults);

    verifyAll();
    mocksControl.verify();

    assertEquals(dummyMessage1.concat("\n").concat(dummyMessage2),
                 captureMessages.getValue());

    assertEquals(Pattern.compile(apPredictVoltageTraceRegex).toString(),
                 captureDirFilter.getValue().getPattern().toString());

    assertEquals(dummyResultFilesDir.concat(dummyConcVoltagesFileName),
                 captureConcVoltagesFilePath.getValue());

    assertEquals(dummyHdrDeltaAPD90, pctileNames);
  }

  @Test
  public void testRetrieveResultFilesDir() {
    final String dummyJobDirectory = dummyBaseDir.concat("/");
    mockStatic(FileUtil.class);
    expect(FileUtil.retrieveJobDirectory(dummyBaseDir, dummyAppManagerId))
          .andReturn(dummyJobDirectory);

    replayAll();
    mocksControl.replay();

    final String retrieved = fakeRunResultsReader.retrieveResultFilesDir(dummyAppManagerId);

    verifyAll();
    mocksControl.verify();

    assertEquals(dummyJobDirectory.concat(AppIdentifiers.AP_PREDICT_OUTPUT_BASE),
                 retrieved);
  }
}