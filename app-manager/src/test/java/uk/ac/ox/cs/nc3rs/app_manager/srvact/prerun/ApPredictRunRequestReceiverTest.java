/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.prerun;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.expectLastCall;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.io.File;
import java.io.IOException;

import javax.activation.DataHandler;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.business.Running;
import uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO;
import uk.ac.ox.cs.nc3rs.app_manager.util.FileUtil;
import uk.ac.ox.cs.nc3rs.app_manager.value.ApPredictVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.type.InvocationSource;
import uk.ac.ox.cs.nc3rs.app_manager.value.type.SimulationType;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.ApPredictRunRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.ApPredictRunResponse;

/**
 * Unit test the ApPredict run request receiver.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ ApPredictRunRequestReceiver.class, ApPredictVO.class,
                  FileUtil.class, Running.class })
public class ApPredictRunRequestReceiverTest {

  private static final long dummyAppManagerId = 5l;
  private static final String businessManagerInvocationSource = InvocationSource.BUSINESS_MANAGER.getAlternatives()[0];
  private static final String cellMLSuffix = ".cellml";

  private AppManagerDAO mockAppManagerDAO;
  private ApPredictRunRequest mockApPredictRunRequest;
  private ApPredictRunRequestReceiver receiver;
  private File mockFileCellML;
  private File mockFilePK;
  private IMocksControl mocksControl;

  @Before
  public void setUp() {
    receiver = new ApPredictRunRequestReceiver();

    mocksControl = createStrictControl();
    mockAppManagerDAO = mocksControl.createMock(AppManagerDAO.class);
    mockApPredictRunRequest = mocksControl.createMock(ApPredictRunRequest.class);
    mockFileCellML = mocksControl.createMock(File.class);
    mockFilePK = mocksControl.createMock(File.class);

    ReflectionTestUtils.setField(receiver,
                                 AppIdentifiers.COMPONENT_JDBC_APP_MANAGER_DAO,
                                 mockAppManagerDAO);
  }

  @Test
  public void testHandleApPredictRunRequest() throws Exception {
    /*
     * No CellML or PK file, unlimited capacity, 'other' invocation source, regular simulation.
     */
    expect(mockApPredictRunRequest.getCellMLFile()).andReturn(null);
    expect(mockApPredictRunRequest.getPKFile()).andReturn(null);
    final ApPredictVO mockApPredictVO = mocksControl.createMock(ApPredictVO.class);
    String dummyPKFileLocation = null;
    expectNew(ApPredictVO.class, mockApPredictRunRequest, dummyPKFileLocation,
                                 (File) null)
             .andReturn(mockApPredictVO);
    String dummyInvocationSource = "dummyInvocationSource";
    expect(mockApPredictRunRequest.getInvocationSource())
          .andReturn(dummyInvocationSource);
    boolean dummyIsPKSimulation = false;
    expect(mockApPredictVO.isPKSimulation()).andReturn(dummyIsPKSimulation);
    // >> alreadyAtCapacity()
    // returns false as unlimited invocations if env invocation limit not defined
    // << alreadyAtCapacity()
    expect(mockAppManagerDAO.createManagerRecord(mockApPredictVO,
                                                 InvocationSource.OTHER,
                                                 SimulationType.REGULAR))
          .andReturn(dummyAppManagerId);
    mockStatic(Running.class);
    Running.addToRunning(dummyAppManagerId);
    expectLastCall();

    replayAll();
    mocksControl.replay();

    ApPredictRunResponse received = receiver.handleApPredictRequest(mockApPredictRunRequest);

    verifyAll();
    mocksControl.verify();

    assertSame(dummyAppManagerId, received.getResponse().getAppManagerId());
    assertNull(received.getResponse().getResponseCode());

    resetAll();
    mocksControl.reset();

    /*
     * No CellML, unlimited capacity, business-manager invocation source, pk
     * simulation - exception reading PK file.
     */
    expect(mockApPredictRunRequest.getCellMLFile()).andReturn(null);
    final DataHandler mockDataHandler = mocksControl.createMock(DataHandler.class);
    expect(mockApPredictRunRequest.getPKFile()).andReturn(mockDataHandler);
    final String dummyIOException = "dummyIOException";
    mockStatic(FileUtil.class);
    expect(FileUtil.writeFileToTmp(mockDataHandler, null))
          .andThrow(new IOException(dummyIOException));

    replayAll();
    mocksControl.replay();

    try {
      received = receiver.handleApPredictRequest(mockApPredictRunRequest);
      fail("Should not accept impossibility to write PK file to disk!");
    } catch (IllegalStateException e) {
      assertTrue(e.getMessage().contains(dummyIOException));
    }

    verifyAll();
    mocksControl.verify();

    resetAll();
    mocksControl.reset();

    /*
     * No CellML, unlimited capacity, business-manager invocation source, pk
     * simulation with PK write ok.
     */
    expect(mockApPredictRunRequest.getCellMLFile()).andReturn(null);
    expect(mockApPredictRunRequest.getPKFile()).andReturn(mockDataHandler);
    mockStatic(FileUtil.class);
    expect(FileUtil.writeFileToTmp(mockDataHandler, null))
          .andReturn(mockFilePK);
    dummyPKFileLocation = "dummyPKFileLocation";
    expect(mockFilePK.getAbsolutePath()).andReturn(dummyPKFileLocation);
    expectNew(ApPredictVO.class, mockApPredictRunRequest, dummyPKFileLocation,
                                (File) null)
             .andReturn(mockApPredictVO);
    dummyInvocationSource = businessManagerInvocationSource;
    expect(mockApPredictRunRequest.getInvocationSource())
          .andReturn(dummyInvocationSource);
    dummyIsPKSimulation = true;
    expect(mockApPredictVO.isPKSimulation()).andReturn(dummyIsPKSimulation);
    // >> alreadyAtCapacity()
    // returns false as unlimited invocations if env invocation limit not defined
    // << alreadyAtCapacity()
    expect(mockAppManagerDAO.createManagerRecord(mockApPredictVO,
                                                 InvocationSource.BUSINESS_MANAGER,
                                                 SimulationType.PK))
          .andReturn(dummyAppManagerId);
    Running.addToRunning(dummyAppManagerId);
    expectLastCall();

    replayAll();
    mocksControl.replay();

    received = receiver.handleApPredictRequest(mockApPredictRunRequest);

    verifyAll();
    mocksControl.verify();

    assertSame(dummyAppManagerId, received.getResponse().getAppManagerId());
    assertNull(received.getResponse().getResponseCode());

    resetAll();
    mocksControl.reset();

    /*
     * CellML present - problem writing it to tmp.
     */
    expect(mockApPredictRunRequest.getCellMLFile()).andReturn(mockDataHandler);
    expect(FileUtil.writeFileToTmp(mockDataHandler, cellMLSuffix))
                   .andThrow(new IOException(dummyIOException));

    replayAll();
    mocksControl.replay();

    try {
       receiver.handleApPredictRequest(mockApPredictRunRequest);
       fail("Should have caught the IOException!");
    } catch (IllegalStateException e) {}

    verifyAll();
    mocksControl.verify();

    resetAll();
    mocksControl.reset();

    /*
     * CellML present - no problem writing to tmp.
     */
    expect(mockApPredictRunRequest.getCellMLFile()).andReturn(mockDataHandler);
    expect(FileUtil.writeFileToTmp(mockDataHandler, cellMLSuffix))
                   .andReturn(mockFileCellML);
    expect(mockApPredictRunRequest.getPKFile()).andReturn(null);
    expectNew(ApPredictVO.class, mockApPredictRunRequest, null, mockFileCellML)
             .andReturn(mockApPredictVO);
    expect(mockApPredictRunRequest.getInvocationSource())
          .andReturn(dummyInvocationSource);
    expect(mockApPredictVO.isPKSimulation()).andReturn(dummyIsPKSimulation);
    // >> alreadyAtCapacity()
    // returns false as unlimited invocations if env invocation limit not defined
    // << alreadyAtCapacity()
    expect(mockAppManagerDAO.createManagerRecord(mockApPredictVO,
                                                 InvocationSource.BUSINESS_MANAGER,
                                                 SimulationType.PK))
          .andReturn(dummyAppManagerId);
    mockStatic(Running.class);
    Running.addToRunning(dummyAppManagerId);
    expectLastCall();

    replayAll();
    mocksControl.replay();

    received = receiver.handleApPredictRequest(mockApPredictRunRequest);

    verifyAll();
    mocksControl.verify();

  }


  @Test
  public void testSetEnvInvocationLimit() throws Exception {
    // Can't do this as static Running class interferes with above.
    /*
     * Not at capacity, 'other' invocation source, regular simulation.
    receiver.setEnvInvocationLimit(2);
  
    final ApPredictVO mockApPredictVO = mocksControl.createMock(ApPredictVO.class);
    expectNew(ApPredictVO.class, mockApPredictRunRequest).andReturn(mockApPredictVO);
    String dummyInvocationSource = "dummyInvocationSource";
    expect(mockApPredictRunRequest.getInvocationSource()).andReturn(dummyInvocationSource);
    boolean dummyIsPKSimulation = false;
    expect(mockApPredictVO.isPKSimulation()).andReturn(dummyIsPKSimulation);
    // >> alreadyAtCapacity()
    int dummyRunningCount = 1;
    mockStatic(Running.class);
    expect(Running.count()).andReturn(dummyRunningCount);
    // << alreadyAtCapacity()
    expect(mockAppManagerDAO.createManagerRecord(mockApPredictVO, InvocationSource.OTHER,
                                                 SimulationType.REGULAR))
          .andReturn(dummyAppManagerId);
    Running.addToRunning(dummyAppManagerId);
    expectLastCall();
  
    replayAll();
    mocksControl.replay();
  
    ApPredictRunResponse received = receiver.handleApPredictRequest(mockApPredictRunRequest);
  
    verifyAll();
    mocksControl.verify();
  
    assertSame(dummyAppManagerId, received.getResponse().getAppManagerId());
    assertNull(received.getResponse().getResponseCode());
  
    resetAll();
    mocksControl.reset();
     */
  }
}