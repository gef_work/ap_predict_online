/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.value;

import static org.easymock.EasyMock.createStrictControl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Test;

import uk.ac.ox.cs.nc3rs.app_manager.domain.PKResult;
import uk.ac.ox.cs.nc3rs.app_manager.domain.SimulationResult;

/**
 *
 *
 * @author geoff
 */
public class AllSimulationDomainResultsVOTest {

  @Test
  public void testConstructor() {
    AllSimulationDomainResultsVO resultsVO = new AllSimulationDomainResultsVO(null,
                                                                              null,
                                                                              null);
    assertNull(resultsVO.getDeltaAPDPercentileNames());
    assertTrue(resultsVO.getPkResults().isEmpty());
    assertTrue(resultsVO.getSimulationResults().isEmpty());

    final IMocksControl mocksControl = createStrictControl();
    final PKResult mockPKResult1 = mocksControl.createMock(PKResult.class);
    final PKResult mockPKResult2 = mocksControl.createMock(PKResult.class);
    final SimulationResult mockSimulationResult = mocksControl.createMock(SimulationResult.class);

    final List<PKResult> dummyPKResults = new ArrayList<PKResult>();
    dummyPKResults.add(mockPKResult1);
    dummyPKResults.add(mockPKResult2);
    final List<SimulationResult> dummySimulationResults = new ArrayList<SimulationResult>();
    dummySimulationResults.add(mockSimulationResult);
    final String dummyDeltaAPD90PctileName = "dummyDeltaAPD90PctileName";

    resultsVO = new AllSimulationDomainResultsVO(dummyPKResults,
                                                 dummySimulationResults,
                                                 dummyDeltaAPD90PctileName);

    assertSame(dummyPKResults.size(), resultsVO.getPkResults().size());
    assertSame(1, resultsVO.getSimulationResults().size());
    assertEquals(dummyDeltaAPD90PctileName,
                 resultsVO.getDeltaAPDPercentileNames());
  }
}
