/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.postrun;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.Selectors;
import org.apache.commons.vfs2.VFS;
import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO;
import uk.ac.ox.cs.nc3rs.app_manager.value.ApPredictVO;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.BusinessDataUploadCompletedRequest;

/**
 * Unit test the post simulation run data removal.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { VFS.class } )
public class PostSimulationRunDataRemoverTest {

  private static final long dummyAppManagerId = 5l;

  private AppManagerDAO mockAppManagerDAO;
  private BusinessDataUploadCompletedRequest mockBusinessDataUploadCompletedRequest;
  private IMocksControl mocksControl;
  private PostSimulationRunDataRemover postSimulationRunDataRemover;

  @Before
  public void setUp() {
    postSimulationRunDataRemover = new PostSimulationRunDataRemover();

    mocksControl = createStrictControl();
    mockAppManagerDAO = mocksControl.createMock(AppManagerDAO.class);
    mockBusinessDataUploadCompletedRequest = mocksControl.createMock(BusinessDataUploadCompletedRequest.class);

    ReflectionTestUtils.setField(postSimulationRunDataRemover,
                                 AppIdentifiers.COMPONENT_JDBC_APP_MANAGER_DAO,
                                 mockAppManagerDAO);
  }

  @Test
  public void testRemoveSimulationData() throws FileSystemException {
    /*
     * No ApPredictVO object retrieved.
     */
    expect(mockBusinessDataUploadCompletedRequest.getAppManagerId())
          .andReturn(dummyAppManagerId);
    expect(mockAppManagerDAO.retrieveObjectValue(dummyAppManagerId))
          .andReturn(null);
    mockAppManagerDAO.purgeExistingSimulation(dummyAppManagerId);

    mocksControl.replay();

    postSimulationRunDataRemover.removeSimulationData(mockBusinessDataUploadCompletedRequest);

    mocksControl.verify();

    mocksControl.reset();

    /*
     * ApPredictVO object retrieved, not files to remove.
     */
    expect(mockBusinessDataUploadCompletedRequest.getAppManagerId())
          .andReturn(dummyAppManagerId);
    final ApPredictVO mockApPredictVO = mocksControl.createMock(ApPredictVO.class);
    expect(mockAppManagerDAO.retrieveObjectValue(dummyAppManagerId))
          .andReturn(mockApPredictVO);
    final Set<String> dummyRemovableFiles = new HashSet<String>();
    expect(mockApPredictVO.retrieveRemovableFileLocations())
          .andReturn(dummyRemovableFiles);
    mockAppManagerDAO.purgeExistingSimulation(dummyAppManagerId);

    mocksControl.replay();

    postSimulationRunDataRemover.removeSimulationData(mockBusinessDataUploadCompletedRequest);

    mocksControl.verify();

    mocksControl.reset();

    /*
     * ApPredictVO has a file for removal - VFS exception thrown.
     */
    expect(mockBusinessDataUploadCompletedRequest.getAppManagerId())
          .andReturn(dummyAppManagerId);
    expect(mockAppManagerDAO.retrieveObjectValue(dummyAppManagerId))
          .andReturn(mockApPredictVO);
    // Returning a file location for removal.
    final String dummyFileLocation1 = "dummyFileLocation1";
    dummyRemovableFiles.add(dummyFileLocation1);
    expect(mockApPredictVO.retrieveRemovableFileLocations())
          .andReturn(dummyRemovableFiles);

    // >> removeFiles()
    final FileSystemManager mockFileSystemManager = mocksControl.createMock(FileSystemManager.class);
    mockStatic(VFS.class);
    expect(VFS.getManager()).andReturn(mockFileSystemManager);
    final String dummyFileSystemException = "dummyFileSystemException";
    expect(mockFileSystemManager.resolveFile(dummyFileLocation1))
          .andThrow(new FileSystemException(dummyFileSystemException));
    // << removeFiles()

    mockAppManagerDAO.purgeExistingSimulation(dummyAppManagerId);

    replayAll();
    mocksControl.replay();

    postSimulationRunDataRemover.removeSimulationData(mockBusinessDataUploadCompletedRequest);

    verifyAll();
    mocksControl.verify();

    resetAll();
    mocksControl.reset();

    /*
     * ApPredictVO has files for removal, no exceptions thrown.
     */
    expect(mockBusinessDataUploadCompletedRequest.getAppManagerId())
          .andReturn(dummyAppManagerId);
    expect(mockAppManagerDAO.retrieveObjectValue(dummyAppManagerId))
          .andReturn(mockApPredictVO);
    final String dummyFileLocation2 = "dummyFileLocation2";
    dummyRemovableFiles.add(dummyFileLocation2);
    expect(mockApPredictVO.retrieveRemovableFileLocations())
          .andReturn(dummyRemovableFiles);

    // >> removeFiles()
    expect(VFS.getManager()).andReturn(mockFileSystemManager);
    final FileObject mockFileObject = mocksControl.createMock(FileObject.class);
    final Capture<String> captureFileLocation1 = newCapture();
    expect(mockFileSystemManager.resolveFile(capture(captureFileLocation1)))
          .andReturn(mockFileObject);
    final int dummyDeletedCount = 4;
    expect(mockFileObject.delete(Selectors.SELECT_SELF))
          .andReturn(dummyDeletedCount);
    final Capture<String> captureFileLocation2 = newCapture();
    expect(mockFileSystemManager.resolveFile(capture(captureFileLocation2)))
          .andReturn(mockFileObject);
    expect(mockFileObject.delete(Selectors.SELECT_SELF))
          .andReturn(dummyDeletedCount);
    // << removeFiles()

    mockAppManagerDAO.purgeExistingSimulation(dummyAppManagerId);

    replayAll();
    mocksControl.replay();

    postSimulationRunDataRemover.removeSimulationData(mockBusinessDataUploadCompletedRequest);

    verifyAll();
    mocksControl.verify();

    final String capturedFileLocation1 = captureFileLocation1.getValue();
    final String capturedFileLocation2 = captureFileLocation2.getValue();

    assertNotNull(capturedFileLocation1);
    assertNotNull(capturedFileLocation2);
    assertNotEquals(capturedFileLocation1, capturedFileLocation2);
  }
}