/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.value;

import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;

import org.junit.Test;

/**
 * Unit test ApPredict value object error state processing.
 *
 * @author geoff
 */
public class ApPredictVOErrorStateTest extends AbstractApPredictVOTest {

  @Test
  public void testErrorState1() {
    /*
     * Null IC50 and pIC50 associated items.
     */
    preparePacing();

    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    expect(mockChannelDataICaL.getIC50Data()).andReturn(null);
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(null);
    // << extractChannelData()

    mocksControl.replay();

    try {
      new ApPredictVO(mockRequest, dummyPKFileLocation, null);
      fail("Should be throwing IllegalStateException!");
    } catch (IllegalStateException e) {
      assertTrue(ApPredictVO.ERROR_STATE_1.equals(e.getMessage()));
    }

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Empty IC50 and pIC50 associated items.
     */
    preparePacing();

    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    expect(mockChannelDataICaL.getIC50Data()).andReturn(dummyIC50DataICaL);
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(dummyPIC50DataICaL);
    // << extractChannelData()

    mocksControl.replay();

    try {
      new ApPredictVO(mockRequest, dummyPKFileLocation, null);
      fail("Should be throwing IllegalStateException!");
    } catch (IllegalStateException e) {
      assertTrue(ApPredictVO.ERROR_STATE_1.equals(e.getMessage()));
    }

    mocksControl.verify();
  }

  @Test
  public void testErrorState2() {
    /*
     * Both IC50 and pIC50 associated items.
     */
    preparePacing();

    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    dummyIC50DataICaL.add(mockAssociatedItemICaL);
    expect(mockChannelDataICaL.getIC50Data()).andReturn(dummyIC50DataICaL);
    dummyPIC50DataICaL.add(mockAssociatedItemICaL);
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(dummyPIC50DataICaL);
    // << extractChannelData()

    mocksControl.replay();

    try {
      new ApPredictVO(mockRequest, dummyPKFileLocation, null);
      fail("Should be throwing IllegalStateException!");
    } catch (IllegalStateException e) {
      assertTrue(ApPredictVO.ERROR_STATE_2.equals(e.getMessage()));
    }

    mocksControl.verify();
  }

  @Test
  public void testErrorState3() {
    /*
     * Invalid value -ve IC50
     * --ic50-cal -0.1 --hill-cal 0.76 --saturation-cal 48.3
     */
    preparePacing();
    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    dummyIC50DataICaL.add(mockAssociatedItemICaL);
    expect(mockChannelDataICaL.getIC50Data()).andReturn(dummyIC50DataICaL);     // <-- IC50 data
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(null);
    expect(mockChannelDataICaL.getC50Spread()).andReturn(null);                 // <-- No IC spread
    expect(mockChannelDataICaL.getHillSpread()).andReturn(null);                // <-- No Hill spread
    dummyAssociatedItemICaLC50 = new BigDecimal("-0.1");
    expect(mockAssociatedItemICaL.getC50())
          .andReturn(dummyAssociatedItemICaLC50);                               // <-- IC50 is invalid -ve!
    expect(mockAssociatedItemICaL.getHill()).andReturn(dummyArbitraryHill);     // <-- Hill is good
    expect(mockAssociatedItemICaL.getSaturation())
          .andReturn(dummyArbitrarySaturation);                                 // <-- Saturation is good 
    // << extractChannelData()

    mocksControl.replay();

    try {
      new ApPredictVO(mockRequest, dummyPKFileLocation, null);
      fail("Should be throwing IllegalStateException!");
    } catch (IllegalStateException e) {
      assertTrue(ApPredictVO.ERROR_STATE_3.equals(e.getMessage()));
    }

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Invalid (lack of) pIC50
     * --hill-cal 0.76 --saturation-cal 48.3
     */
    preparePacing();

    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    dummyPIC50DataICaL.add(mockAssociatedItemICaL);
    expect(mockChannelDataICaL.getIC50Data()).andReturn(null);
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(dummyPIC50DataICaL);   // <-- pIC50 data
    expect(mockChannelDataICaL.getC50Spread()).andReturn(null);                 // <-- No IC spread
    expect(mockChannelDataICaL.getHillSpread()).andReturn(null);                // <-- No Hill spread
    expect(mockAssociatedItemICaL.getC50()).andReturn(null);                    // <-- pIC50 is invalid null!
    expect(mockAssociatedItemICaL.getHill()).andReturn(dummyArbitraryHill);     // <-- Hill is good
    expect(mockAssociatedItemICaL.getSaturation())
          .andReturn(dummyArbitrarySaturation);                                 // <-- saturation is good
    // << extractChannelData()

    mocksControl.replay();

    try {
      new ApPredictVO(mockRequest, dummyPKFileLocation, null);
      fail("Should be throwing IllegalStateException!");
    } catch (IllegalStateException e) {
      assertTrue(ApPredictVO.ERROR_STATE_3.equals(e.getMessage()));
    }

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Invalid (lack of) IC50
     */
    preparePacing();

    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    expect(mockChannelDataICaL.getIC50Data()).andReturn(dummyIC50DataICaL);     // <-- IC50 data
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(null);
    expect(mockChannelDataICaL.getC50Spread()).andReturn(null);                 // <-- No IC spread
    expect(mockChannelDataICaL.getHillSpread()).andReturn(null);                // <-- No Hill spread
    expect(mockAssociatedItemICaL.getC50()).andReturn(null);                    // <-- IC50 is invalid null!
    expect(mockAssociatedItemICaL.getHill()).andReturn(dummyArbitraryHill);     // <-- Hill is good
    expect(mockAssociatedItemICaL.getSaturation())
          .andReturn(dummyArbitrarySaturation);                                 // <-- Saturation is good 
    // << extractChannelData()

    mocksControl.replay();

    try {
      new ApPredictVO(mockRequest, dummyPKFileLocation, null);
      fail("Should be throwing IllegalStateException!");
    } catch (IllegalStateException e) {
      assertTrue(ApPredictVO.ERROR_STATE_3.equals(e.getMessage()));
    }

    mocksControl.verify();
  }

  @Test
  public void testErrorState4() {
    /*
     * Invalid 0 value Hill.
     * --pic50-cal 4.5 --hill-cal 0
     */
    preparePacing();
    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    expect(mockChannelDataICaL.getIC50Data()).andReturn(null);
    dummyPIC50DataICaL.add(mockAssociatedItemICaL);
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(dummyPIC50DataICaL);   // <-- pIC50 data
    expect(mockChannelDataICaL.getC50Spread()).andReturn(null);                 // <-- No IC spread
    expect(mockChannelDataICaL.getHillSpread()).andReturn(null);                // <-- No Hill spread
    expect(mockAssociatedItemICaL.getC50()).andReturn(dummyArbitraryPIC50);     // <-- pIC50 is good
    expect(mockAssociatedItemICaL.getHill()).andReturn(BigDecimal.ZERO);        // <-- Hill is invalid!
    expect(mockAssociatedItemICaL.getSaturation())
          .andReturn(dummyArbitrarySaturation);                                 // <-- Saturation is good 
    // << extractChannelData()

    mocksControl.replay();

    try {
      new ApPredictVO(mockRequest, dummyPKFileLocation, null);
      fail("Should be throwing IllegalStateException!");
    } catch (IllegalStateException e) {
      assertTrue(ApPredictVO.ERROR_STATE_4.equals(e.getMessage()));
    }

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Hill spread provided, but no Hill value.
     * --ic50-cal 1001.34 --pic50-spread-cal 0.32 --hill-spread-cal 0.32 --saturation-cal 48.3 --credible-intervals
     */
    preparePacing();

    expect(mockRequest.getCredibleIntervals()).andReturn(mockCredibleIntervals);
    dummyCalculateCredibleIntervals = true;
    expect(mockCredibleIntervals.getCalculateCredibleIntervals())
          .andReturn(dummyCalculateCredibleIntervals);
    dummyPctiles = "dummyPctiles";
    expect(mockCredibleIntervals.getPercentiles()).andReturn(dummyPctiles);
    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    dummyIC50DataICaL.add(mockAssociatedItemICaL);
    expect(mockChannelDataICaL.getIC50Data()).andReturn(dummyIC50DataICaL);     // <-- IC50 data
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(null);
    dummyC50SpreadICaL = dummyArbitrarySpread;
    expect(mockChannelDataICaL.getC50Spread()).andReturn(dummyC50SpreadICaL);   // <-- IC spread is good
    dummyHillSpreadICaL = dummyArbitrarySpread;
    expect(mockChannelDataICaL.getHillSpread()).andReturn(dummyHillSpreadICaL); // <-- Hill spread is good
    dummyAssociatedItemICaLC50 = dummyArbitraryIC50;                            // <-- IC50 is good
    expect(mockAssociatedItemICaL.getC50())
          .andReturn(dummyAssociatedItemICaLC50);
    expect(mockAssociatedItemICaL.getHill())
          .andReturn(dummyAssociatedItemICaLHill);                              // <-- No Hill
    dummyAssociatedItemICaLSaturation = dummyArbitrarySaturation;               // <-- Saturation is good
    expect(mockAssociatedItemICaL.getSaturation())
          .andReturn(dummyAssociatedItemICaLSaturation);
 
    mocksControl.replay();
    replayAll();

    try {
      new ApPredictVO(mockRequest, dummyPKFileLocation, null);
      fail("Should be throwing IllegalStateException!");
    } catch (IllegalStateException e) {
      assertTrue(ApPredictVO.ERROR_STATE_4.equals(e.getMessage()));
    }

    mocksControl.verify();
    verifyAll();
  }

  @Test
  public void testErrorState5() {
    /*
     * Invalid null value saturation.
     */
    preparePacing();
    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    expect(mockChannelDataICaL.getIC50Data()).andReturn(null);
    dummyPIC50DataICaL.add(mockAssociatedItemICaL);
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(dummyPIC50DataICaL);   // <-- pIC50 data
    expect(mockChannelDataICaL.getC50Spread()).andReturn(null);                 // <-- No IC spread
    expect(mockChannelDataICaL.getHillSpread()).andReturn(null);                // <-- No Hill spread
    expect(mockAssociatedItemICaL.getC50()).andReturn(dummyArbitraryPIC50);     // <-- pIC50 is good
    expect(mockAssociatedItemICaL.getHill()).andReturn(dummyArbitraryHill);     // <-- Hill is good
    expect(mockAssociatedItemICaL.getSaturation()).andReturn(null);             // <-- Saturation is invalid null
    // << extractChannelData()

    mocksControl.replay();

    try {
      new ApPredictVO(mockRequest, dummyPKFileLocation, null);
      fail("Should be throwing IllegalStateException!");
    } catch (IllegalStateException e) {
      assertTrue(ApPredictVO.ERROR_STATE_5.equals(e.getMessage()));
    }

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Invalid -ve value saturation.
     * --pic50-cal 4.5 --hill-cal 0.76 --saturation-cal -0.00001
     */
    preparePacing();
    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    expect(mockChannelDataICaL.getIC50Data()).andReturn(null);
    dummyPIC50DataICaL.add(mockAssociatedItemICaL);
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(dummyPIC50DataICaL);   // <-- pIC50 data
    expect(mockChannelDataICaL.getC50Spread()).andReturn(null);                 // <-- No IC spread
    expect(mockChannelDataICaL.getHillSpread()).andReturn(null);                // <-- No Hill spread
    expect(mockAssociatedItemICaL.getC50()).andReturn(dummyArbitraryPIC50);     // <-- pIC50 is good
    expect(mockAssociatedItemICaL.getHill()).andReturn(dummyArbitraryHill);     // <-- Hill is good
    expect(mockAssociatedItemICaL.getSaturation())
          .andReturn(new BigDecimal("-0.00001"));                               // <-- Saturation is invalid -ve 
    // << extractChannelData()

    mocksControl.replay();

    try {
      new ApPredictVO(mockRequest, dummyPKFileLocation, null);
      fail("Should be throwing IllegalStateException!");
    } catch (IllegalStateException e) {
      assertTrue(ApPredictVO.ERROR_STATE_5.equals(e.getMessage()));
    }

    mocksControl.verify();
  }

  @Test
  public void testErrorState6() {
    dummyModelIdentifier = 0;
    expect(mockRequest.getModelIdentifier()).andReturn(dummyModelIdentifier);

    mocksControl.replay();

    try {
      new ApPredictVO(mockRequest, dummyPKFileLocation, null);
      fail("Should be throwing IllegalStateException!");
    } catch (IllegalStateException e) {
      assertTrue(ApPredictVO.ERROR_STATE_6.equals(e.getMessage()));
    }

    mocksControl.verify();
  }

  //@Test
  public void testErrorState7() throws Exception {
    /*
     * Hill for one channel but not another.
     * --pic50-cal 4.5 --hill-cal 0.76 --saturation-cal 48.3 --pic50-herg 4.5 --saturation-herg 48.3
     */
    dummyPacingMaxTime = BigDecimal.TEN;
    dummyPacingFrequency = BigDecimal.ONE;

    preparePacing();
    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    // ICaL
    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    dummyIC50DataICaL.add(mockAssociatedItemICaL);
    expect(mockChannelDataICaL.getIC50Data()).andReturn(dummyIC50DataICaL);     // <-- IC50 ICaL data
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(null);
    expect(mockChannelDataICaL.getC50Spread()).andReturn(null);                 // <-- No IC spread
    expect(mockChannelDataICaL.getHillSpread()).andReturn(null);                // <-- No Hill spread
    dummyAssociatedItemICaLC50 = dummyArbitraryIC50;                            // <-- IC50 is good
    expect(mockAssociatedItemICaL.getC50())
          .andReturn(dummyAssociatedItemICaLC50);
    dummyAssociatedItemICaLHill = dummyArbitraryHill;                           // <-- Hill is good
    expect(mockAssociatedItemICaL.getHill())
          .andReturn(dummyAssociatedItemICaLHill);
    dummyAssociatedItemICaLSaturation = dummyArbitrarySaturation;               // <-- Saturation is good
    expect(mockAssociatedItemICaL.getSaturation())
          .andReturn(dummyAssociatedItemICaLSaturation);
    dummyAssociatedDataICaL.add(mockAssociatedDataICaL);
    expectNew(AssociatedData.class, (BigDecimal) null,
                                    dummyAssociatedItemICaLC50,
                                    dummyAssociatedItemICaLHill,
                                    dummyAssociatedItemICaLSaturation)
             .andReturn(mockAssociatedDataICaL);
    // << extractChannelData()
    expectNew(RequestChannelData.class, dummyAssociatedDataICaL,
                                        dummyC50SpreadICaL,
                                        dummyHillSpreadICaL)
             .andReturn(mockRequestChannelDataICaL);

    // IKr
    // >> extractChannelData()
    expect(mockRequest.getIKr()).andReturn(mockChannelDataIKr);
    dummyPIC50DataIKr.add(mockAssociatedItemIKr);
    expect(mockChannelDataIKr.getIC50Data()).andReturn(null);
    expect(mockChannelDataIKr.getPIC50Data()).andReturn(dummyPIC50DataIKr);     // <-- pIC50 IKr data
    expect(mockChannelDataIKr.getC50Spread()).andReturn(dummyC50SpreadIKr);     // <-- No IC spread
    expect(mockChannelDataIKr.getHillSpread()).andReturn(dummyHillSpreadIKr);   // <-- No Hill spread
    dummyAssociatedItemIKrC50 = dummyArbitraryPIC50;                            // <-- pIC50 is good
    expect(mockAssociatedItemIKr.getC50())
          .andReturn(dummyAssociatedItemIKrC50);
    expect(mockAssociatedItemIKr.getHill()).andReturn(null);                    // <-- No Hill
    dummyAssociatedItemIKrSaturation = dummyArbitrarySaturation;                // <-- Saturation is good
    expect(mockAssociatedItemIKr.getSaturation())
          .andReturn(dummyAssociatedItemIKrSaturation);
    dummyAssociatedDataIKr.add(mockAssociatedDataIKr);
    expectNew(AssociatedData.class, dummyAssociatedItemIKrC50, 
                                    (BigDecimal) null,
                                    dummyAssociatedItemIKrHill,
                                    dummyAssociatedItemIKrSaturation)
             .andReturn(mockAssociatedDataIKr);
    // << extractChannelData()

    mocksControl.replay();
    replayAll();

    try {
      new ApPredictVO(mockRequest, dummyPKFileLocation, null);
      fail("Should be throwing IllegalStateException!");
    } catch (IllegalStateException e) {
      assertTrue(ApPredictVO.ERROR_STATE_7.equals(e.getMessage()));
    }

    mocksControl.verify();
    verifyAll();
  }

  //@Test
  public void testErrorStateCI1() throws Exception {
    /*
     * C50, Hill and C50 spread for ICaL but no spread for Hill.
     * --pic50-cal 4.5 --pic50-spread-cal 0.32 --hill-cal 0.76 --credible-intervals
     */
    preparePacing();
    expect(mockRequest.getCredibleIntervals()).andReturn(mockCredibleIntervals);
    dummyCalculateCredibleIntervals = true;
    expect(mockCredibleIntervals.getCalculateCredibleIntervals())
          .andReturn(dummyCalculateCredibleIntervals);
    dummyPctiles = "dummyPctiles";
    expect(mockCredibleIntervals.getPercentiles()).andReturn(dummyPctiles);

    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    dummyIC50DataICaL.add(mockAssociatedItemICaL);
    expect(mockChannelDataICaL.getIC50Data()).andReturn(dummyIC50DataICaL);
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(null);
    expect(mockChannelDataICaL.getC50Spread()).andReturn(dummyArbitrarySpread); // <-- IC spread is good
    expect(mockChannelDataICaL.getHillSpread()).andReturn(dummyHillSpreadICaL); // <-- No Hill spread
    dummyAssociatedItemICaLC50 = dummyArbitraryIC50;                            // <-- IC50 is good
    expect(mockAssociatedItemICaL.getC50())
          .andReturn(dummyAssociatedItemICaLC50);
    dummyAssociatedItemICaLHill = dummyArbitraryHill;                           // <-- Hill is good
    expect(mockAssociatedItemICaL.getHill())
          .andReturn(dummyAssociatedItemICaLHill);
    dummyAssociatedItemICaLSaturation = dummyArbitrarySaturation;               // <-- Saturation is good
    expect(mockAssociatedItemICaL.getSaturation())
          .andReturn(dummyAssociatedItemICaLSaturation);
    dummyAssociatedDataICaL.add(mockAssociatedDataICaL);
    expectNew(AssociatedData.class, (BigDecimal) null,
                                    dummyAssociatedItemICaLC50,
                                    dummyAssociatedItemICaLHill,
                                    dummyAssociatedItemICaLSaturation)
             .andReturn(mockAssociatedDataICaL);
    // << extractChannelData()

    mocksControl.replay();
    replayAll();

    try {
      new ApPredictVO(mockRequest, dummyPKFileLocation, null);
      fail("Should be throwing IllegalStateException!");
    } catch (IllegalStateException e) {
      //assertTrue(ApPredictVO.ERROR_STATE_CI_1.equals(e.getMessage()));
    }

    mocksControl.verify();
    verifyAll();
  }

  @Test
  public void testErrorStateCI2() {
    /*
     * Credible intervals requested, but none provided for an ion channel
     * --ic50-cal 1001.34 --hill-cal 0.76 --credible-intervals
     */
    preparePacing();
    expect(mockRequest.getCredibleIntervals()).andReturn(mockCredibleIntervals);
    dummyCalculateCredibleIntervals = true;
    expect(mockCredibleIntervals.getCalculateCredibleIntervals())
          .andReturn(dummyCalculateCredibleIntervals);
    dummyPctiles = "dummyPctiles";
    expect(mockCredibleIntervals.getPercentiles()).andReturn(dummyPctiles);

    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    dummyIC50DataICaL.add(mockAssociatedItemICaL);
    expect(mockChannelDataICaL.getIC50Data()).andReturn(dummyIC50DataICaL);     // <-- IC50 data
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(null);
    expect(mockChannelDataICaL.getC50Spread()).andReturn(dummyC50SpreadICaL);   // <-- No IC spread
    expect(mockChannelDataICaL.getHillSpread()).andReturn(dummyHillSpreadICaL); // <-- No Hill spread
    // << extractChannelData()

    mocksControl.replay();

    try {
      new ApPredictVO(mockRequest, dummyPKFileLocation, null);
      fail("Should be throwing IllegalStateException!");
    } catch (IllegalStateException e) {
      assertTrue(ApPredictVO.ERROR_STATE_CI_2.equals(e.getMessage()));
    }

    mocksControl.verify();
  }

  @Test
  public void testErrorStateCI3() throws Exception {
    /*
     * No IC spread when IC, Hill and Hill spread provided. 
     * --ic50-cal 1001.34 --hill-cal 0.76 --hill-spread-cal 0.32 --credible-intervals
     */
    preparePacing();
    expect(mockRequest.getCredibleIntervals()).andReturn(mockCredibleIntervals);
    dummyCalculateCredibleIntervals = true;
    expect(mockCredibleIntervals.getCalculateCredibleIntervals())
          .andReturn(dummyCalculateCredibleIntervals);
    dummyPctiles = "dummyPctiles";
    expect(mockCredibleIntervals.getPercentiles()).andReturn(dummyPctiles);

    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    dummyIC50DataICaL.add(mockAssociatedItemICaL);
    expect(mockChannelDataICaL.getIC50Data()).andReturn(dummyIC50DataICaL);     // <-- IC50 data
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(null);
    expect(mockChannelDataICaL.getC50Spread()).andReturn(dummyC50SpreadICaL);   // <-- No IC spread
    dummyHillSpreadICaL = dummyArbitrarySpread;
    expect(mockChannelDataICaL.getHillSpread()).andReturn(dummyHillSpreadICaL); // <-- Hill spread is good
    dummyAssociatedItemICaLC50 = dummyArbitraryIC50;                            // <-- IC50 is good
    expect(mockAssociatedItemICaL.getC50())
          .andReturn(dummyAssociatedItemICaLC50);
    dummyAssociatedItemICaLHill = dummyArbitraryHill;                           // <-- Hill is good
    expect(mockAssociatedItemICaL.getHill())
          .andReturn(dummyAssociatedItemICaLHill);
    dummyAssociatedItemICaLSaturation = dummyArbitrarySaturation;               // <-- Saturation is good
    expect(mockAssociatedItemICaL.getSaturation())
          .andReturn(dummyAssociatedItemICaLSaturation);
    dummyAssociatedDataICaL.add(mockAssociatedDataICaL);
    expectNew(AssociatedData.class, (BigDecimal) null,
                                    dummyAssociatedItemICaLC50,
                                    dummyAssociatedItemICaLHill,
                                    dummyAssociatedItemICaLSaturation)
             .andReturn(mockAssociatedDataICaL);

    mocksControl.replay();
    replayAll();

    try {
      new ApPredictVO(mockRequest, dummyPKFileLocation, null);
      fail("Should be throwing IllegalStateException!");
    } catch (IllegalStateException e) {
      assertTrue(ApPredictVO.ERROR_STATE_CI_3.equals(e.getMessage()));
    }

    mocksControl.verify();
    verifyAll();
  }

  @Test
  public void testErrorStateCI4() {
    /*
     * Hill spread supplied but credible intervals not requested.
     * --hill-spread-cal 0.32
     */
    preparePacing();
    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    expect(mockChannelDataICaL.getIC50Data()).andReturn(null);
    dummyPIC50DataICaL.add(mockAssociatedItemICaL);
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(dummyPIC50DataICaL);   // <-- pIC50 data
    expect(mockChannelDataICaL.getC50Spread()).andReturn(null);                 // <-- No IC spread
    dummyHillSpreadICaL = dummyArbitrarySpread;
    expect(mockChannelDataICaL.getHillSpread())
          .andReturn(dummyHillSpreadICaL);                                      // <-- Hill spread is good (but irrelevant!)

    mocksControl.replay();

    try {
      new ApPredictVO(mockRequest, dummyPKFileLocation, null);
      fail("Should be throwing IllegalStateException!");
    } catch (IllegalStateException e) {
      assertTrue(ApPredictVO.ERROR_STATE_CI_4.equals(e.getMessage()));
    }

    mocksControl.verify();

    mocksControl.reset();

    /*
     * IC spread supplied but credible intervals not requested.
     * --pic50-cal 4.5 --pic50-spread-cal 0.32 --hill-spread-cal 0.32
     */
    preparePacing();
    expect(mockRequest.getCredibleIntervals()).andReturn(null);
    // >> extractChannelData()
    expect(mockRequest.getICaL()).andReturn(mockChannelDataICaL);
    expect(mockChannelDataICaL.getIC50Data()).andReturn(null);
    expect(mockChannelDataICaL.getPIC50Data()).andReturn(dummyPIC50DataICaL);   // <-- pIC50 data
    dummyC50SpreadICaL = dummyArbitrarySpread;
    expect(mockChannelDataICaL.getC50Spread()).andReturn(dummyC50SpreadICaL);   // <-- pIC50 spread is good (but irrelevant!)

    mocksControl.replay();

    try {
      new ApPredictVO(mockRequest, dummyPKFileLocation, null);
      fail("Should be throwing IllegalStateException!");
    } catch (IllegalStateException e) {
      assertTrue(ApPredictVO.ERROR_STATE_CI_4.equals(e.getMessage()));
    }

    mocksControl.verify();
  }

  @Test
  public void testErrorStateCI5() {
    /*
     * Credible intervals to be calculated, but no Delta APD90 %iles specified.
     */
    preparePacing();
    expect(mockRequest.getCredibleIntervals()).andReturn(mockCredibleIntervals);
    dummyCalculateCredibleIntervals = true;
    expect(mockCredibleIntervals.getCalculateCredibleIntervals())
          .andReturn(dummyCalculateCredibleIntervals);
    expect(mockCredibleIntervals.getPercentiles()).andReturn(dummyPctiles);

    mocksControl.replay();

    try {
      new ApPredictVO(mockRequest, dummyPKFileLocation, null);
      fail("Should be throwing IllegalStateException!");
    } catch (IllegalStateException e) {
      assertTrue(ApPredictVO.ERROR_STATE_CI_5.equals(e.getMessage()));
    }

    mocksControl.verify();
  }
}