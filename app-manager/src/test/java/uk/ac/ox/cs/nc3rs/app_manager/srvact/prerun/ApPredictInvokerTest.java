/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.prerun;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import uk.ac.ox.cs.nc3rs.app_manager.util.FileUtil;
import uk.ac.ox.cs.nc3rs.app_manager.value.ApPredictVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.ApPredictVO.CHANNEL_CURRENT;
import uk.ac.ox.cs.nc3rs.app_manager.value.AssociatedData;
import uk.ac.ox.cs.nc3rs.app_manager.value.RequestChannelData;
import uk.ac.ox.cs.nc3rs.app_manager.value.RequestPlasmaData;

/**
 * Unit test the ApPredict invoker.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ FileUtil.class, FileUtils.class, File.class,
                  ApPredictInvoker.class })
public class ApPredictInvokerTest {

  private static final String argCellML = "--cellml";
  private static final String argCredibleIntervals = " --credible-intervals ";
  private static final String argModel = " --model ";
  private static final String argPacingFreq = " --pacing-freq ";
  private static final String argPacingMaxTime = " --pacing-max-time ";
  private static final String argPKFile = " --pkpd-file ";
  private static final String argPlasmaConcCount = " --plasma-conc-count ";
  private static final String argPlasmaConcHigh = " --plasma-conc-high ";
  private static final String argPlasmaConcLogScale = " --plasma-conc-logscale ";
  private static final String argPlasmaConcLow = " --plasma-conc-low ";
  private static final String argPlasmaConcs = " --plasma-concs ";
  private static final String argPrefixHill = "--hill-";
  private static final String argPrefixHillSpread = "--hill-spread-";
  private static final String argPrefixIC50 = "--ic50-";
  private static final String argPrefixPIC50 = "--pic50-";
  private static final String argPrefixIC50Spread = "--ic50-spread-";
  private static final String argPrefixPIC50Spread = "--pic50-spread-";
  private static final String argPrefixSaturation = "--saturation-";

  private static final String valueFalse = Boolean.FALSE.toString();
  private static final String valueTrue = Boolean.TRUE.toString();

  private static final String apPredictShell = "ApPredict.sh";
  private static final String localRunner = "./local_runner.sh";

  private ApPredictInvoker apPredictInvoker;
  private ApPredictVO mockApPredictVO;
  private boolean dummyHasCellMLData;
  private boolean dummyHasPacingFrequency;
  private boolean dummyHasPacingMaxTime;
  private boolean dummyIsCredibleIntervals;
  private IMocksControl mocksControl;
  private long dummyAppManagerId = 3l;
  private Process mockProcess;
  private RequestPlasmaData mockRequestPlasmaData;
  private Runtime mockRuntime;
  private short dummyModelIdentifier;
  private String dummyJobDirectory;
  private String dummyCellMLFileName;
  private String dummyCredibleIntervalPctiles;

  @Before
  public void setUp() {
    apPredictInvoker = new ApPredictInvoker();
    mocksControl = createStrictControl();
    mockApPredictVO = mocksControl.createMock(ApPredictVO.class);
    mockProcess = mocksControl.createMock(Process.class);
    mockRequestPlasmaData = mocksControl.createMock(RequestPlasmaData.class);
    mockRuntime = mocksControl.createMock(Runtime.class);

    dummyJobDirectory = "dummyJobDirectory";
    dummyModelIdentifier = 2;
    dummyHasCellMLData = false;
    dummyHasPacingFrequency = false;
    dummyHasPacingMaxTime = false;
    dummyIsCredibleIntervals = false;
    dummyCredibleIntervalPctiles = "dummyCredibleIntervalPctiles"; 
  }

  @Test
  public void testInvokeApPredictMinimumArguments() throws IOException {
    expect(mockApPredictVO.hasCellMLData()).andReturn(dummyHasCellMLData);
    expect(mockApPredictVO.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockApPredictVO.hasPacingFrequency())
          .andReturn(dummyHasPacingFrequency);
    expect(mockApPredictVO.hasMaxPacingTime()).andReturn(dummyHasPacingMaxTime);
    for (final CHANNEL_CURRENT channel : CHANNEL_CURRENT.values()) {
      expect(mockApPredictVO.retrieveChannelData(channel)).andReturn(null);
    }
    expect(mockApPredictVO.isCredibleIntervals())
          .andReturn(dummyIsCredibleIntervals);
    mockStatic(FileUtil.class);
    expect(FileUtil.retrieveJobDirectory(null, dummyAppManagerId))
          .andReturn(dummyJobDirectory);

    expect(mockApPredictVO.isPKSimulation()).andReturn(false);
    expect(mockApPredictVO.getPlasma()).andReturn(mockRequestPlasmaData);
    boolean dummyPlasmaDataRangeAssigned = false;
    expect(mockRequestPlasmaData.isRangeAssigned())
          .andReturn(dummyPlasmaDataRangeAssigned);
    boolean dummyPlasmaDataConcCountAssigned = false;
    expect(mockRequestPlasmaData.isConcCountAssigned())
          .andReturn(dummyPlasmaDataConcCountAssigned);
    boolean dummyPlasmaDataConcLogScaleAssigned = false;
    expect(mockRequestPlasmaData.isConcLogScaleAssigned())
          .andReturn(dummyPlasmaDataConcLogScaleAssigned);

    mockRuntime = mocksControl.createMock(Runtime.class);
    mockStatic(Runtime.class);
    expect(Runtime.getRuntime()).andReturn(mockRuntime);
    mockProcess = mocksControl.createMock(Process.class);
    final Capture<String[]> captureLocalRunnerArgs = newCapture();
    expect(mockRuntime.exec(capture(captureLocalRunnerArgs)))
          .andReturn(mockProcess);

    replayAll();
    mocksControl.replay();

    final String outcome = apPredictInvoker.invokeApPredict(dummyAppManagerId,
                                                            mockApPredictVO);

    verifyAll();
    mocksControl.verify();

    assertEquals("Invoked './local_runner.sh'", outcome);
    final String[] capturedLocalRunnerArgs = (String[]) captureLocalRunnerArgs.getValue();
    assertEquals(localRunner, capturedLocalRunnerArgs[0]);
    assertEquals(dummyJobDirectory, capturedLocalRunnerArgs[1]);
    assertEquals(String.valueOf(dummyAppManagerId), capturedLocalRunnerArgs[2]);
    final String commandLine = capturedLocalRunnerArgs[3];
    assertTrue(commandLine.startsWith(apPredictShell.concat(" ")));
    assertTrue(commandLine.contains(argModel.concat(String.valueOf(dummyModelIdentifier)).concat(" ")));
    assertTrue(commandLine.contains(argPlasmaConcLogScale.concat(valueTrue).concat(" ")));
    assertFalse(commandLine.contains(argCredibleIntervals));
    assertFalse(commandLine.contains(argPacingFreq));
    assertFalse(commandLine.contains(argPacingMaxTime));
    assertFalse(commandLine.contains(argPlasmaConcCount));
    assertFalse(commandLine.contains(argPlasmaConcHigh));
    assertFalse(commandLine.contains(argPlasmaConcLow));
    assertFalse(commandLine.contains(argPlasmaConcs));
    assertFalse(commandLine.contains(argPrefixHill));
    assertFalse(commandLine.contains(argPrefixHillSpread));
    assertFalse(commandLine.contains(argPrefixIC50));
    assertFalse(commandLine.contains(argPrefixPIC50));
    assertFalse(commandLine.contains(argPrefixIC50Spread));
    assertFalse(commandLine.contains(argPrefixPIC50Spread));
    assertFalse(commandLine.contains(argPrefixSaturation));
    assertFalse(commandLine.contains(argPKFile));
    assertFalse(commandLine.contains(argCellML));
  }

  @Test
  public void testInvokeApPredictOptionalHills() throws IOException {
    expect(mockApPredictVO.hasCellMLData()).andReturn(dummyHasCellMLData);
    expect(mockApPredictVO.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockApPredictVO.hasPacingFrequency())
          .andReturn(dummyHasPacingFrequency);
    expect(mockApPredictVO.hasMaxPacingTime()).andReturn(dummyHasPacingMaxTime);

    BigDecimal dummyPIC50hERG1 = new BigDecimal("1.1");
    BigDecimal dummyPIC50hERG2 = new BigDecimal("2.2");
    BigDecimal dummyIC50hERG1 = new BigDecimal("3.3");
    BigDecimal dummyIC50hERG2 = new BigDecimal("4.4");
    BigDecimal dummyHillhERG1 = null;
    BigDecimal dummyHillhERG2 = null;
    BigDecimal dummySaturationhERG1 = new BigDecimal("5.5");
    BigDecimal dummySaturationhERG2 = new BigDecimal("6.6");
    BigDecimal dummySpreadC50hERG = new BigDecimal("7.7");

    for (final CHANNEL_CURRENT channel : CHANNEL_CURRENT.values()) {
      if (channel.compareTo(CHANNEL_CURRENT.IKr) == 0) {
        RequestChannelData mockRequestChannelData = mocksControl.createMock(RequestChannelData.class);
        expect(mockApPredictVO.retrieveChannelData(channel))
              .andReturn(mockRequestChannelData);
        List<AssociatedData> dummyAssociatedData = new ArrayList<AssociatedData>();
        final AssociatedData mockAssociatedData1 = mocksControl.createMock(AssociatedData.class);
        final AssociatedData mockAssociatedData2 = mocksControl.createMock(AssociatedData.class);
        dummyAssociatedData.add(mockAssociatedData1);
        dummyAssociatedData.add(mockAssociatedData2);
        expect(mockRequestChannelData.getAssociatedData())
              .andReturn(dummyAssociatedData);

        expect(mockAssociatedData1.getpIC50()).andReturn(dummyPIC50hERG1);
        // This effectively gets ignored!
        expect(mockAssociatedData1.getIC50()).andReturn(dummyIC50hERG1);
        expect(mockAssociatedData1.getHill()).andReturn(dummyHillhERG1);
        expect(mockAssociatedData1.getSaturation())
              .andReturn(dummySaturationhERG1);
        expect(mockAssociatedData2.getpIC50()).andReturn(dummyPIC50hERG2);
        // This effectively gets ignored!
        expect(mockAssociatedData2.getIC50()).andReturn(dummyIC50hERG2);
        expect(mockAssociatedData2.getHill()).andReturn(dummyHillhERG2);
        expect(mockAssociatedData2.getSaturation())
              .andReturn(dummySaturationhERG2);
  
        expect(mockRequestChannelData.getSpreadC50())
              .andReturn(dummySpreadC50hERG);
      } else {
        expect(mockApPredictVO.retrieveChannelData(channel)).andReturn(null);
      }
    }

    dummyIsCredibleIntervals = true;
    expect(mockApPredictVO.isCredibleIntervals())
          .andReturn(dummyIsCredibleIntervals);
    expect(mockApPredictVO.getCredibleIntervalPercentiles())
          .andReturn(dummyCredibleIntervalPctiles);
    mockStatic(FileUtil.class);
    expect(FileUtil.retrieveJobDirectory(null, dummyAppManagerId))
          .andReturn(dummyJobDirectory);

    expect(mockApPredictVO.isPKSimulation()).andReturn(false);
    expect(mockApPredictVO.getPlasma()).andReturn(mockRequestPlasmaData);
    boolean dummyPlasmaDataRangeAssigned = false;
    expect(mockRequestPlasmaData.isRangeAssigned())
          .andReturn(dummyPlasmaDataRangeAssigned);
    boolean dummyPlasmaDataConcCountAssigned = false;
    expect(mockRequestPlasmaData.isConcCountAssigned())
          .andReturn(dummyPlasmaDataConcCountAssigned);
    boolean dummyPlasmaDataConcLogScaleAssigned = false;
    expect(mockRequestPlasmaData.isConcLogScaleAssigned())
          .andReturn(dummyPlasmaDataConcLogScaleAssigned);

    mockRuntime = mocksControl.createMock(Runtime.class);
    mockStatic(Runtime.class);
    expect(Runtime.getRuntime()).andReturn(mockRuntime);
    mockProcess = mocksControl.createMock(Process.class);
    final Capture<String[]> captureLocalRunnerArgs = newCapture();
    expect(mockRuntime.exec(capture(captureLocalRunnerArgs)))
          .andReturn(mockProcess);

    replayAll();
    mocksControl.replay();

    apPredictInvoker.invokeApPredict(dummyAppManagerId, mockApPredictVO);

    verifyAll();
    mocksControl.verify();

    final String[] capturedLocalRunnerArgs = (String[]) captureLocalRunnerArgs.getValue();

    assertEquals(localRunner, capturedLocalRunnerArgs[0]);
    assertEquals(dummyJobDirectory, capturedLocalRunnerArgs[1]);
    assertEquals(String.valueOf(dummyAppManagerId), capturedLocalRunnerArgs[2]);
    final String commandLine = capturedLocalRunnerArgs[3];
    assertTrue(commandLine.startsWith(apPredictShell.concat(" ")));
    assertTrue(commandLine.contains(argModel.concat(String.valueOf(dummyModelIdentifier)).concat(" ")));
    assertTrue(commandLine.contains(argPlasmaConcLogScale.concat(valueTrue).concat(" ")));
    assertTrue(commandLine.contains(argCredibleIntervals));
    assertTrue(commandLine.contains(argCredibleIntervals.concat(dummyCredibleIntervalPctiles)));
    assertFalse(commandLine.contains(argPacingFreq));
    assertFalse(commandLine.contains(argPacingMaxTime));
    assertFalse(commandLine.contains(argPlasmaConcCount));
    assertFalse(commandLine.contains(argPlasmaConcHigh));
    assertFalse(commandLine.contains(argPlasmaConcLow));
    assertFalse(commandLine.contains(argPlasmaConcs));
    assertFalse(commandLine.contains(argPrefixHill.concat(CHANNEL_CURRENT.IKr.getInvocationArg()).concat(" ")));
    assertFalse(commandLine.contains(argPrefixHillSpread.concat(CHANNEL_CURRENT.IKr.getInvocationArg()).concat(" ")));
    assertFalse(commandLine.contains(argPrefixIC50));
    assertTrue(commandLine.contains(argPrefixPIC50.concat(CHANNEL_CURRENT.IKr.getInvocationArg()).concat(" ").concat(dummyPIC50hERG1.toPlainString()).concat(" ").concat(dummyPIC50hERG2.toPlainString()).concat(" ")));
    assertFalse(commandLine.contains(argPrefixIC50Spread));
    assertTrue(commandLine.contains(argPrefixPIC50Spread.concat(CHANNEL_CURRENT.IKr.getInvocationArg()).concat(" ").concat(dummySpreadC50hERG.toPlainString()).concat(" ")));
    assertTrue(commandLine.contains(argPrefixSaturation.concat(CHANNEL_CURRENT.IKr.getInvocationArg()).concat(" ").concat(dummySaturationhERG1.toPlainString()).concat(" ").concat(dummySaturationhERG2.toPlainString()).concat(" ")));
    assertFalse(commandLine.contains(argPKFile));
  }

  @Test
  public void testInvokeApPredictWithCellMLData() throws IOException {
    dummyHasCellMLData = true;
    expect(mockApPredictVO.hasCellMLData()).andReturn(dummyHasCellMLData);
    dummyCellMLFileName = "dummyCellMLFileName";
    expect(mockApPredictVO.getCellMLFileName()).andReturn(dummyCellMLFileName);
    expect(mockApPredictVO.hasPacingFrequency())
          .andReturn(dummyHasPacingFrequency);
    expect(mockApPredictVO.hasMaxPacingTime()).andReturn(dummyHasPacingMaxTime);
    for (final CHANNEL_CURRENT channel : CHANNEL_CURRENT.values()) {
      expect(mockApPredictVO.retrieveChannelData(channel)).andReturn(null);
    }
    expect(mockApPredictVO.isCredibleIntervals())
          .andReturn(dummyIsCredibleIntervals);
    mockStatic(FileUtil.class);
    expect(FileUtil.retrieveJobDirectory(null, dummyAppManagerId))
          .andReturn(dummyJobDirectory);

    expect(mockApPredictVO.isPKSimulation()).andReturn(false);
    expect(mockApPredictVO.getPlasma()).andReturn(mockRequestPlasmaData);
    boolean dummyPlasmaDataRangeAssigned = false;
    expect(mockRequestPlasmaData.isRangeAssigned())
          .andReturn(dummyPlasmaDataRangeAssigned);
    boolean dummyPlasmaDataConcCountAssigned = false;
    expect(mockRequestPlasmaData.isConcCountAssigned())
          .andReturn(dummyPlasmaDataConcCountAssigned);
    boolean dummyPlasmaDataConcLogScaleAssigned = false;
    expect(mockRequestPlasmaData.isConcLogScaleAssigned())
          .andReturn(dummyPlasmaDataConcLogScaleAssigned);

    mockRuntime = mocksControl.createMock(Runtime.class);
    mockStatic(Runtime.class);
    expect(Runtime.getRuntime()).andReturn(mockRuntime);
    mockProcess = mocksControl.createMock(Process.class);
    final Capture<String[]> captureLocalRunnerArgs = newCapture();
    expect(mockRuntime.exec(capture(captureLocalRunnerArgs)))
          .andReturn(mockProcess);

    replayAll();
    mocksControl.replay();

    final String outcome = apPredictInvoker.invokeApPredict(dummyAppManagerId,
                                                            mockApPredictVO);

    verifyAll();
    mocksControl.verify();

    assertEquals("Invoked './local_runner.sh'", outcome);
    final String[] capturedLocalRunnerArgs = (String[]) captureLocalRunnerArgs.getValue();
    assertEquals(localRunner, capturedLocalRunnerArgs[0]);
    assertEquals(dummyJobDirectory, capturedLocalRunnerArgs[1]);
    assertEquals(String.valueOf(dummyAppManagerId), capturedLocalRunnerArgs[2]);
    final String commandLine = capturedLocalRunnerArgs[3];
    assertTrue(commandLine.startsWith(apPredictShell.concat(" ")));
    assertFalse(commandLine.contains(argModel));
    assertTrue(commandLine.contains(argPlasmaConcLogScale.concat(valueTrue).concat(" ")));
    assertFalse(commandLine.contains(argCredibleIntervals));
    assertFalse(commandLine.contains(argPacingFreq));
    assertFalse(commandLine.contains(argPacingMaxTime));
    assertFalse(commandLine.contains(argPlasmaConcCount));
    assertFalse(commandLine.contains(argPlasmaConcHigh));
    assertFalse(commandLine.contains(argPlasmaConcLow));
    assertFalse(commandLine.contains(argPlasmaConcs));
    assertFalse(commandLine.contains(argPrefixHill));
    assertFalse(commandLine.contains(argPrefixHillSpread));
    assertFalse(commandLine.contains(argPrefixIC50));
    assertFalse(commandLine.contains(argPrefixPIC50));
    assertFalse(commandLine.contains(argPrefixIC50Spread));
    assertFalse(commandLine.contains(argPrefixPIC50Spread));
    assertFalse(commandLine.contains(argPrefixSaturation));
    assertFalse(commandLine.contains(argPKFile));
    assertTrue(commandLine.contains(argCellML));
  }

  @Test
  public void testInvokeApPredictWithIC50Data() throws IOException {
    expect(mockApPredictVO.hasCellMLData()).andReturn(dummyHasCellMLData);
    expect(mockApPredictVO.getModelIdentifier())
          .andReturn(dummyModelIdentifier);
    expect(mockApPredictVO.hasPacingFrequency())
          .andReturn(dummyHasPacingFrequency);
    expect(mockApPredictVO.hasMaxPacingTime()).andReturn(dummyHasPacingMaxTime);

    BigDecimal dummyIC50 = new BigDecimal("1.1");
    BigDecimal dummyHill = new BigDecimal("2.2");
    BigDecimal dummySaturation = new BigDecimal("3.3");
    BigDecimal dummySpreadC50 = new BigDecimal("4.4");
    BigDecimal dummySpreadHill = new BigDecimal("5.5");
    for (final CHANNEL_CURRENT channel : CHANNEL_CURRENT.values()) {
      if (channel.compareTo(CHANNEL_CURRENT.ICaL) == 0) {
        RequestChannelData mockRequestChannelData = mocksControl.createMock(RequestChannelData.class);
        expect(mockApPredictVO.retrieveChannelData(channel))
              .andReturn(mockRequestChannelData);
        List<AssociatedData> dummyAssociatedData = new ArrayList<AssociatedData>();
        AssociatedData mockAssociatedData = mocksControl.createMock(AssociatedData.class);
        dummyAssociatedData.add(mockAssociatedData);
  
        expect(mockRequestChannelData.getAssociatedData())
              .andReturn(dummyAssociatedData);
        expect(mockAssociatedData.getpIC50()).andReturn(null);
        expect(mockAssociatedData.getIC50()).andReturn(dummyIC50);
        expect(mockAssociatedData.getHill()).andReturn(dummyHill);
        expect(mockAssociatedData.getSaturation()).andReturn(dummySaturation);
  
        expect(mockRequestChannelData.getSpreadC50()).andReturn(dummySpreadC50);
        expect(mockRequestChannelData.getSpreadHill())
              .andReturn(dummySpreadHill);
      } else {
        expect(mockApPredictVO.retrieveChannelData(channel)).andReturn(null);
      }
    }

    dummyIsCredibleIntervals = true;
    expect(mockApPredictVO.isCredibleIntervals())
          .andReturn(dummyIsCredibleIntervals);
    expect(mockApPredictVO.getCredibleIntervalPercentiles())
          .andReturn(dummyCredibleIntervalPctiles);
    mockStatic(FileUtil.class);
    expect(FileUtil.retrieveJobDirectory(null, dummyAppManagerId))
          .andReturn(dummyJobDirectory);

    expect(mockApPredictVO.isPKSimulation()).andReturn(false);
    expect(mockApPredictVO.getPlasma()).andReturn(mockRequestPlasmaData);
    boolean dummyPlasmaDataRangeAssigned = false;
    expect(mockRequestPlasmaData.isRangeAssigned())
          .andReturn(dummyPlasmaDataRangeAssigned);
    boolean dummyPlasmaDataConcCountAssigned = false;
    expect(mockRequestPlasmaData.isConcCountAssigned())
          .andReturn(dummyPlasmaDataConcCountAssigned);
    boolean dummyPlasmaDataConcLogScaleAssigned = false;
    expect(mockRequestPlasmaData.isConcLogScaleAssigned())
          .andReturn(dummyPlasmaDataConcLogScaleAssigned);


    mockRuntime = mocksControl.createMock(Runtime.class);
    mockStatic(Runtime.class);
    expect(Runtime.getRuntime()).andReturn(mockRuntime);
    mockProcess = mocksControl.createMock(Process.class);
    final Capture<String[]> captureLocalRunnerArgs = newCapture();
    expect(mockRuntime.exec(capture(captureLocalRunnerArgs)))
          .andReturn(mockProcess);

    replayAll();
    mocksControl.replay();

    apPredictInvoker.invokeApPredict(dummyAppManagerId, mockApPredictVO);

    verifyAll();
    mocksControl.verify();

    final String[] capturedLocalRunnerArgs = (String[]) captureLocalRunnerArgs.getValue();

    assertEquals(localRunner, capturedLocalRunnerArgs[0]);
    assertEquals(dummyJobDirectory, capturedLocalRunnerArgs[1]);
    assertEquals(String.valueOf(dummyAppManagerId), capturedLocalRunnerArgs[2]);
    final String commandLine = capturedLocalRunnerArgs[3];
    assertTrue(commandLine.startsWith(apPredictShell.concat(" ")));
    assertTrue(commandLine.contains(argModel.concat(String.valueOf(dummyModelIdentifier)).concat(" ")));
    assertTrue(commandLine.contains(argPlasmaConcLogScale.concat(valueTrue).concat(" ")));
    assertTrue(commandLine.contains(argCredibleIntervals));
    assertFalse(commandLine.contains(argPacingFreq));
    assertFalse(commandLine.contains(argPacingMaxTime));
    assertFalse(commandLine.contains(argPlasmaConcCount));
    assertFalse(commandLine.contains(argPlasmaConcHigh));
    assertFalse(commandLine.contains(argPlasmaConcLow));
    assertFalse(commandLine.contains(argPlasmaConcs));
    assertTrue(commandLine.contains(argPrefixHill.concat(CHANNEL_CURRENT.ICaL.getInvocationArg()).concat(" ").concat(dummyHill.toPlainString()).concat(" ")));
    assertTrue(commandLine.contains(argPrefixHillSpread.concat(CHANNEL_CURRENT.ICaL.getInvocationArg()).concat(" ").concat(dummySpreadHill.toPlainString()).concat(" ")));
    assertTrue(commandLine.contains(argPrefixIC50.concat(CHANNEL_CURRENT.ICaL.getInvocationArg()).concat(" ").concat(dummyIC50.toPlainString()).concat(" ")));
    assertFalse(commandLine.contains(argPrefixPIC50));
    assertTrue(commandLine.contains(argPrefixIC50Spread.concat(CHANNEL_CURRENT.ICaL.getInvocationArg()).concat(" ").concat(dummySpreadC50.toPlainString()).concat(" ")));
    assertFalse(commandLine.contains(argPrefixPIC50Spread));
    assertTrue(commandLine.contains(argPrefixSaturation.concat(CHANNEL_CURRENT.ICaL.getInvocationArg()).concat(" ").concat(dummySaturation.toPlainString()).concat(" ")));
    assertFalse(commandLine.contains(argPKFile));
    assertFalse(commandLine.contains(argCellML));
  }

  @Test
  public void testInvokeApPredictWithIC50DataNoSpreads() throws IOException {
    expect(mockApPredictVO.hasCellMLData()).andReturn(dummyHasCellMLData);
    expect(mockApPredictVO.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockApPredictVO.hasPacingFrequency())
          .andReturn(dummyHasPacingFrequency);
    expect(mockApPredictVO.hasMaxPacingTime()).andReturn(dummyHasPacingMaxTime);

    BigDecimal dummyIC50 = new BigDecimal("1.1");
    BigDecimal dummyHill = new BigDecimal("2.2");
    BigDecimal dummySaturation = new BigDecimal("3.3");
    for (final CHANNEL_CURRENT channel : CHANNEL_CURRENT.values()) {
      if (channel.compareTo(CHANNEL_CURRENT.ICaL) == 0) {
        RequestChannelData mockRequestChannelData = mocksControl.createMock(RequestChannelData.class);
        expect(mockApPredictVO.retrieveChannelData(channel))
              .andReturn(mockRequestChannelData);
        List<AssociatedData> dummyAssociatedData = new ArrayList<AssociatedData>();
        AssociatedData mockAssociatedData = mocksControl.createMock(AssociatedData.class);
        dummyAssociatedData.add(mockAssociatedData);

        expect(mockRequestChannelData.getAssociatedData())
              .andReturn(dummyAssociatedData);
        expect(mockAssociatedData.getpIC50()).andReturn(null);
        // This effectively gets ignored!
        expect(mockAssociatedData.getIC50()).andReturn(dummyIC50);
        expect(mockAssociatedData.getHill()).andReturn(dummyHill);
        expect(mockAssociatedData.getSaturation()).andReturn(dummySaturation);
        expect(mockRequestChannelData.getSpreadC50()).andReturn(null);
        expect(mockRequestChannelData.getSpreadHill()).andReturn(null);
      } else {
        expect(mockApPredictVO.retrieveChannelData(channel)).andReturn(null);
      }
    }

    expect(mockApPredictVO.isCredibleIntervals())
          .andReturn(dummyIsCredibleIntervals);
    mockStatic(FileUtil.class);
    expect(FileUtil.retrieveJobDirectory(null, dummyAppManagerId))
          .andReturn(dummyJobDirectory);

    expect(mockApPredictVO.isPKSimulation()).andReturn(false);
    expect(mockApPredictVO.getPlasma()).andReturn(mockRequestPlasmaData);
    boolean dummyPlasmaDataRangeAssigned = false;
    expect(mockRequestPlasmaData.isRangeAssigned())
          .andReturn(dummyPlasmaDataRangeAssigned);
    boolean dummyPlasmaDataConcCountAssigned = false;
    expect(mockRequestPlasmaData.isConcCountAssigned())
          .andReturn(dummyPlasmaDataConcCountAssigned);
    boolean dummyPlasmaDataConcLogScaleAssigned = false;
    expect(mockRequestPlasmaData.isConcLogScaleAssigned())
          .andReturn(dummyPlasmaDataConcLogScaleAssigned);

    mockRuntime = mocksControl.createMock(Runtime.class);
    mockStatic(Runtime.class);
    expect(Runtime.getRuntime()).andReturn(mockRuntime);
    mockProcess = mocksControl.createMock(Process.class);
    final Capture<String[]> captureLocalRunnerArgs = newCapture();
    expect(mockRuntime.exec(capture(captureLocalRunnerArgs)))
          .andReturn(mockProcess);

    replayAll();
    mocksControl.replay();

    apPredictInvoker.invokeApPredict(dummyAppManagerId, mockApPredictVO);

    verifyAll();
    mocksControl.verify();

    final String[] capturedLocalRunnerArgs = (String[]) captureLocalRunnerArgs.getValue();

    assertEquals(localRunner, capturedLocalRunnerArgs[0]);
    assertEquals(dummyJobDirectory, capturedLocalRunnerArgs[1]);
    assertEquals(String.valueOf(dummyAppManagerId), capturedLocalRunnerArgs[2]);
    final String commandLine = capturedLocalRunnerArgs[3];
    assertTrue(commandLine.startsWith(apPredictShell.concat(" ")));
    assertTrue(commandLine.contains(argModel.concat(String.valueOf(dummyModelIdentifier)).concat(" ")));
    assertTrue(commandLine.contains(argPlasmaConcLogScale.concat(valueTrue).concat(" ")));
    assertFalse(commandLine.contains(argCredibleIntervals));
    assertFalse(commandLine.contains(argPacingFreq));
    assertFalse(commandLine.contains(argPacingMaxTime));
    assertFalse(commandLine.contains(argPlasmaConcCount));
    assertFalse(commandLine.contains(argPlasmaConcHigh));
    assertFalse(commandLine.contains(argPlasmaConcLow));
    assertFalse(commandLine.contains(argPlasmaConcs));
    assertTrue(commandLine.contains(argPrefixHill.concat(CHANNEL_CURRENT.ICaL.getInvocationArg()).concat(" ").concat(dummyHill.toPlainString()).concat(" ")));
    assertFalse(commandLine.contains(argPrefixHillSpread));
    assertTrue(commandLine.contains(argPrefixIC50.concat(CHANNEL_CURRENT.ICaL.getInvocationArg()).concat(" ").concat(dummyIC50.toPlainString()).concat(" ")));
    assertFalse(commandLine.contains(argPrefixPIC50));
    assertFalse(commandLine.contains(argPrefixIC50Spread));
    assertFalse(commandLine.contains(argPrefixPIC50Spread));
    assertTrue(commandLine.contains(argPrefixSaturation.concat(CHANNEL_CURRENT.ICaL.getInvocationArg()).concat(" ").concat(dummySaturation.toPlainString()).concat(" ")));
    assertFalse(commandLine.contains(argPKFile));
    assertFalse(commandLine.contains(argCellML));
  }

  @Test
  public void testInvokeApPredictWithPacingData() throws IOException {
    expect(mockApPredictVO.hasCellMLData()).andReturn(dummyHasCellMLData);
    expect(mockApPredictVO.getModelIdentifier()).andReturn(dummyModelIdentifier);
    dummyHasPacingFrequency = true;
    expect(mockApPredictVO.hasPacingFrequency())
          .andReturn(dummyHasPacingFrequency);
    BigDecimal dummyPacingFrequency = BigDecimal.ONE;
    expect(mockApPredictVO.getPacingFreq()).andReturn(dummyPacingFrequency);
    dummyHasPacingMaxTime = true;
    expect(mockApPredictVO.hasMaxPacingTime()).andReturn(dummyHasPacingMaxTime);
    BigDecimal dummyPacingMaxTime = BigDecimal.TEN;
    expect(mockApPredictVO.getPacingMaxTime()).andReturn(dummyPacingMaxTime);

    for (final CHANNEL_CURRENT channel : CHANNEL_CURRENT.values()) {
      expect(mockApPredictVO.retrieveChannelData(channel)).andReturn(null);
    }

    expect(mockApPredictVO.isCredibleIntervals())
          .andReturn(dummyIsCredibleIntervals);
    mockStatic(FileUtil.class);
    expect(FileUtil.retrieveJobDirectory(null, dummyAppManagerId))
          .andReturn(dummyJobDirectory);

    expect(mockApPredictVO.isPKSimulation()).andReturn(false);
    expect(mockApPredictVO.getPlasma()).andReturn(mockRequestPlasmaData);
    boolean dummyPlasmaDataRangeAssigned = false;
    expect(mockRequestPlasmaData.isRangeAssigned())
          .andReturn(dummyPlasmaDataRangeAssigned);
    boolean dummyPlasmaDataConcCountAssigned = false;
    expect(mockRequestPlasmaData.isConcCountAssigned())
          .andReturn(dummyPlasmaDataConcCountAssigned);
    boolean dummyPlasmaDataConcLogScaleAssigned = false;
    expect(mockRequestPlasmaData.isConcLogScaleAssigned())
          .andReturn(dummyPlasmaDataConcLogScaleAssigned);

    mockRuntime = mocksControl.createMock(Runtime.class);
    mockStatic(Runtime.class);
    expect(Runtime.getRuntime()).andReturn(mockRuntime);
    mockProcess = mocksControl.createMock(Process.class);
    final Capture<String[]> captureLocalRunnerArgs = newCapture();
    expect(mockRuntime.exec(capture(captureLocalRunnerArgs)))
          .andReturn(mockProcess);

    replayAll();
    mocksControl.replay();

    apPredictInvoker.invokeApPredict(dummyAppManagerId, mockApPredictVO);

    verifyAll();
    mocksControl.verify();

    final String[] capturedLocalRunnerArgs = (String[]) captureLocalRunnerArgs.getValue();

    assertEquals(localRunner, capturedLocalRunnerArgs[0]);
    assertEquals(dummyJobDirectory, capturedLocalRunnerArgs[1]);
    assertEquals(String.valueOf(dummyAppManagerId), capturedLocalRunnerArgs[2]);
    final String commandLine = capturedLocalRunnerArgs[3];
    assertTrue(commandLine.startsWith(apPredictShell.concat(" ")));
    assertTrue(commandLine.contains(argModel.concat(String.valueOf(dummyModelIdentifier)).concat(" ")));
    assertTrue(commandLine.contains(argPlasmaConcLogScale.concat(valueTrue).concat(" ")));
    assertFalse(commandLine.contains(argCredibleIntervals));
    assertTrue(commandLine.contains(argPacingFreq.concat(dummyPacingFrequency.toPlainString().concat(" "))));
    assertTrue(commandLine.contains(argPacingMaxTime.concat(dummyPacingMaxTime.toPlainString().concat(" "))));
    assertFalse(commandLine.contains(argPlasmaConcCount));
    assertFalse(commandLine.contains(argPlasmaConcHigh));
    assertFalse(commandLine.contains(argPlasmaConcLow));
    assertFalse(commandLine.contains(argPlasmaConcs));
    assertFalse(commandLine.contains(argPrefixHill));
    assertFalse(commandLine.contains(argPrefixHillSpread));
    assertFalse(commandLine.contains(argPrefixIC50));
    assertFalse(commandLine.contains(argPrefixPIC50));
    assertFalse(commandLine.contains(argPrefixIC50Spread));
    assertFalse(commandLine.contains(argPrefixPIC50Spread));
    assertFalse(commandLine.contains(argPrefixSaturation));
    assertFalse(commandLine.contains(argPKFile));
    assertFalse(commandLine.contains(argCellML));
  }

  @Test
  public void testInvokeApPredictWithPIC50Data() throws IOException {
    expect(mockApPredictVO.hasCellMLData()).andReturn(dummyHasCellMLData);
    expect(mockApPredictVO.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockApPredictVO.hasPacingFrequency())
          .andReturn(dummyHasPacingFrequency);
    expect(mockApPredictVO.hasMaxPacingTime()).andReturn(dummyHasPacingMaxTime);

    BigDecimal dummyPIC50 = BigDecimal.ZERO;
    BigDecimal dummyIC50 = new BigDecimal("1.1");
    BigDecimal dummyHill = new BigDecimal("2.2");
    BigDecimal dummySaturation = new BigDecimal("3.3");
    BigDecimal dummySpreadC50 = new BigDecimal("4.4");
    BigDecimal dummySpreadHill = new BigDecimal("5.5");

    for (final CHANNEL_CURRENT channel : CHANNEL_CURRENT.values()) {
      if (channel.compareTo(CHANNEL_CURRENT.IKr) == 0) {
        RequestChannelData mockRequestChannelData = mocksControl.createMock(RequestChannelData.class);
        expect(mockApPredictVO.retrieveChannelData(channel))
              .andReturn(mockRequestChannelData);
        List<AssociatedData> dummyAssociatedData = new ArrayList<AssociatedData>();
        AssociatedData mockAssociatedData = mocksControl.createMock(AssociatedData.class);
        dummyAssociatedData.add(mockAssociatedData);
  
        expect(mockRequestChannelData.getAssociatedData())
              .andReturn(dummyAssociatedData);
        expect(mockAssociatedData.getpIC50()).andReturn(dummyPIC50);
        // This effectively gets ignored!
        expect(mockAssociatedData.getIC50()).andReturn(dummyIC50);
        expect(mockAssociatedData.getHill()).andReturn(dummyHill);
        expect(mockAssociatedData.getSaturation()).andReturn(dummySaturation);
  
        expect(mockRequestChannelData.getSpreadC50()).andReturn(dummySpreadC50);
        expect(mockRequestChannelData.getSpreadHill())
              .andReturn(dummySpreadHill);
      } else {
        expect(mockApPredictVO.retrieveChannelData(channel)).andReturn(null);
      }
    }

    dummyIsCredibleIntervals = true;
    expect(mockApPredictVO.isCredibleIntervals())
          .andReturn(dummyIsCredibleIntervals);
    expect(mockApPredictVO.getCredibleIntervalPercentiles())
          .andReturn(dummyCredibleIntervalPctiles);
    mockStatic(FileUtil.class);
    expect(FileUtil.retrieveJobDirectory(null, dummyAppManagerId))
          .andReturn(dummyJobDirectory);

    expect(mockApPredictVO.isPKSimulation()).andReturn(false);
    expect(mockApPredictVO.getPlasma()).andReturn(mockRequestPlasmaData);
    boolean dummyPlasmaDataRangeAssigned = false;
    expect(mockRequestPlasmaData.isRangeAssigned())
          .andReturn(dummyPlasmaDataRangeAssigned);
    boolean dummyPlasmaDataConcCountAssigned = false;
    expect(mockRequestPlasmaData.isConcCountAssigned())
          .andReturn(dummyPlasmaDataConcCountAssigned);
    boolean dummyPlasmaDataConcLogScaleAssigned = false;
    expect(mockRequestPlasmaData.isConcLogScaleAssigned())
          .andReturn(dummyPlasmaDataConcLogScaleAssigned);

    mockRuntime = mocksControl.createMock(Runtime.class);
    mockStatic(Runtime.class);
    expect(Runtime.getRuntime()).andReturn(mockRuntime);
    mockProcess = mocksControl.createMock(Process.class);
    final Capture<String[]> captureLocalRunnerArgs = newCapture();
    expect(mockRuntime.exec(capture(captureLocalRunnerArgs)))
          .andReturn(mockProcess);

    replayAll();
    mocksControl.replay();

    apPredictInvoker.invokeApPredict(dummyAppManagerId, mockApPredictVO);

    verifyAll();
    mocksControl.verify();

    final String[] capturedLocalRunnerArgs = (String[]) captureLocalRunnerArgs.getValue();

    assertEquals(localRunner, capturedLocalRunnerArgs[0]);
    assertEquals(dummyJobDirectory, capturedLocalRunnerArgs[1]);
    assertEquals(String.valueOf(dummyAppManagerId), capturedLocalRunnerArgs[2]);
    final String commandLine = capturedLocalRunnerArgs[3];
    assertTrue(commandLine.startsWith(apPredictShell.concat(" ")));
    assertTrue(commandLine.contains(argModel.concat(String.valueOf(dummyModelIdentifier)).concat(" ")));
    assertTrue(commandLine.contains(argPlasmaConcLogScale.concat(valueTrue).concat(" ")));
    assertTrue(commandLine.contains(argCredibleIntervals));
    assertFalse(commandLine.contains(argPacingFreq));
    assertFalse(commandLine.contains(argPacingMaxTime));
    assertFalse(commandLine.contains(argPlasmaConcCount));
    assertFalse(commandLine.contains(argPlasmaConcHigh));
    assertFalse(commandLine.contains(argPlasmaConcLow));
    assertFalse(commandLine.contains(argPlasmaConcs));
    assertTrue(commandLine.contains(argPrefixHill.concat(CHANNEL_CURRENT.IKr.getInvocationArg()).concat(" ").concat(dummyHill.toPlainString()).concat(" ")));
    assertTrue(commandLine.contains(argPrefixHillSpread.concat(CHANNEL_CURRENT.IKr.getInvocationArg()).concat(" ").concat(dummySpreadHill.toPlainString()).concat(" ")));
    assertFalse(commandLine.contains(argPrefixIC50));
    assertTrue(commandLine.contains(argPrefixPIC50.concat(CHANNEL_CURRENT.IKr.getInvocationArg()).concat(" ").concat(dummyPIC50.toPlainString()).concat(" ")));
    assertFalse(commandLine.contains(argPrefixIC50Spread));
    assertTrue(commandLine.contains(argPrefixPIC50Spread.concat(CHANNEL_CURRENT.IKr.getInvocationArg()).concat(" ").concat(dummySpreadC50.toPlainString()).concat(" ")));
    assertTrue(commandLine.contains(argPrefixSaturation.concat(CHANNEL_CURRENT.IKr.getInvocationArg()).concat(" ").concat(dummySaturation.toPlainString()).concat(" ")));
    assertFalse(commandLine.contains(argPKFile));
    assertFalse(commandLine.contains(argCellML));
  }

  @Test
  public void testInvokeApPredictWithPKFile() throws Exception {
    expect(mockApPredictVO.hasCellMLData()).andReturn(dummyHasCellMLData);
    expect(mockApPredictVO.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockApPredictVO.hasPacingFrequency())
          .andReturn(dummyHasPacingFrequency);
    expect(mockApPredictVO.hasMaxPacingTime()).andReturn(dummyHasPacingMaxTime);
    for (final CHANNEL_CURRENT channel : CHANNEL_CURRENT.values()) {
      expect(mockApPredictVO.retrieveChannelData(channel)).andReturn(null);
    }

    expect(mockApPredictVO.isCredibleIntervals())
          .andReturn(dummyIsCredibleIntervals);
    mockStatic(FileUtil.class);
    expect(FileUtil.retrieveJobDirectory(null, dummyAppManagerId))
          .andReturn(dummyJobDirectory);

    expect(mockApPredictVO.isPKSimulation()).andReturn(true);

    mockRuntime = mocksControl.createMock(Runtime.class);
    mockStatic(Runtime.class);
    expect(Runtime.getRuntime()).andReturn(mockRuntime);
    mockProcess = mocksControl.createMock(Process.class);
    Capture<String[]> captureLocalRunnerArgs = newCapture();
    expect(mockRuntime.exec(capture(captureLocalRunnerArgs)))
          .andReturn(mockProcess);

    replayAll();
    mocksControl.replay();

    String outcome = apPredictInvoker.invokeApPredict(dummyAppManagerId,
                                                      mockApPredictVO);

    verifyAll();
    mocksControl.verify();

    assertEquals("Invoked './local_runner.sh'", outcome);
    String[] capturedLocalRunnerArgs = (String[]) captureLocalRunnerArgs.getValue();
    assertEquals(localRunner, capturedLocalRunnerArgs[0]);
    assertEquals(dummyJobDirectory, capturedLocalRunnerArgs[1]);
    assertEquals(String.valueOf(dummyAppManagerId), capturedLocalRunnerArgs[2]);
    String commandLine = capturedLocalRunnerArgs[3];
    assertTrue(commandLine.startsWith(apPredictShell.concat(" ")));
    assertTrue(commandLine.contains(argModel.concat(String.valueOf(dummyModelIdentifier)).concat(" ")));
    assertFalse(commandLine.contains(argPlasmaConcLogScale.concat(valueTrue).concat(" ")));
    assertFalse(commandLine.contains(argCredibleIntervals));
    assertFalse(commandLine.contains(argPacingFreq));
    assertFalse(commandLine.contains(argPacingMaxTime));
    assertFalse(commandLine.contains(argPlasmaConcCount));
    assertFalse(commandLine.contains(argPlasmaConcHigh));
    assertFalse(commandLine.contains(argPlasmaConcLow));
    assertFalse(commandLine.contains(argPlasmaConcs));
    assertFalse(commandLine.contains(argPrefixHill));
    assertFalse(commandLine.contains(argPrefixHillSpread));
    assertFalse(commandLine.contains(argPrefixIC50));
    assertFalse(commandLine.contains(argPrefixPIC50));
    assertFalse(commandLine.contains(argPrefixIC50Spread));
    assertFalse(commandLine.contains(argPrefixPIC50Spread));
    assertFalse(commandLine.contains(argPrefixSaturation));
    assertTrue(commandLine.contains(argPKFile));
    assertFalse(commandLine.contains(argCellML));
  }

  @Test
  public void testInvokeApPredictWithPlasmaData() throws IOException {
    expect(mockApPredictVO.hasCellMLData()).andReturn(dummyHasCellMLData);
    expect(mockApPredictVO.getModelIdentifier())
          .andReturn(dummyModelIdentifier);
    dummyHasPacingFrequency = true;
    expect(mockApPredictVO.hasPacingFrequency())
          .andReturn(dummyHasPacingFrequency);
    BigDecimal dummyPacingFrequency = BigDecimal.ONE;
    expect(mockApPredictVO.getPacingFreq()).andReturn(dummyPacingFrequency);
    dummyHasPacingMaxTime = true;
    expect(mockApPredictVO.hasMaxPacingTime()).andReturn(dummyHasPacingMaxTime);
    BigDecimal dummyPacingMaxTime = BigDecimal.TEN;
    expect(mockApPredictVO.getPacingMaxTime()).andReturn(dummyPacingMaxTime);

    for (final CHANNEL_CURRENT channel : CHANNEL_CURRENT.values()) {
      expect(mockApPredictVO.retrieveChannelData(channel)).andReturn(null);
    }

    expect(mockApPredictVO.isCredibleIntervals())
          .andReturn(dummyIsCredibleIntervals);
    mockStatic(FileUtil.class);
    expect(FileUtil.retrieveJobDirectory(null, dummyAppManagerId))
          .andReturn(dummyJobDirectory);

    expect(mockApPredictVO.isPKSimulation()).andReturn(false);
    expect(mockApPredictVO.getPlasma()).andReturn(mockRequestPlasmaData);
    boolean dummyPlasmaDataRangeAssigned = true;
    expect(mockRequestPlasmaData.isRangeAssigned())
          .andReturn(dummyPlasmaDataRangeAssigned);
    String dummyPlasmaConcs = "0,10,100";
    expect(mockRequestPlasmaData.getPlasmaConcs())
          .andReturn(dummyPlasmaConcs).times(2);
    boolean dummyPlasmaDataConcCountAssigned = true;
    expect(mockRequestPlasmaData.isConcCountAssigned())
          .andReturn(dummyPlasmaDataConcCountAssigned);
    Short dummyPlasmaConcCount = 4;
    expect(mockRequestPlasmaData.getPlasmaConcCount())
          .andReturn(dummyPlasmaConcCount);
    boolean dummyPlasmaDataConcLogScaleAssigned = true;
    expect(mockRequestPlasmaData.isConcLogScaleAssigned())
          .andReturn(dummyPlasmaDataConcLogScaleAssigned);
    boolean dummyPlasmaConcLogScale = false;
    expect(mockRequestPlasmaData.getPlasmaConcLogScale())
          .andReturn(dummyPlasmaConcLogScale);

    mockRuntime = mocksControl.createMock(Runtime.class);
    mockStatic(Runtime.class);
    expect(Runtime.getRuntime()).andReturn(mockRuntime);
    mockProcess = mocksControl.createMock(Process.class);
    final Capture<String[]> captureLocalRunnerArgs = newCapture();
    expect(mockRuntime.exec(capture(captureLocalRunnerArgs)))
          .andReturn(mockProcess);

    replayAll();
    mocksControl.replay();

    apPredictInvoker.invokeApPredict(dummyAppManagerId, mockApPredictVO);

    verifyAll();
    mocksControl.verify();

    String[] capturedLocalRunnerArgs = (String[]) captureLocalRunnerArgs.getValue();

    assertEquals(localRunner, capturedLocalRunnerArgs[0]);
    assertEquals(dummyJobDirectory, capturedLocalRunnerArgs[1]);
    assertEquals(String.valueOf(dummyAppManagerId), capturedLocalRunnerArgs[2]);
    String commandLine = capturedLocalRunnerArgs[3];
    assertTrue(commandLine.startsWith(apPredictShell.concat(" ")));
    assertTrue(commandLine.contains(argModel.concat(String.valueOf(dummyModelIdentifier)).concat(" ")));
    assertTrue(commandLine.contains(argPlasmaConcLogScale.concat(valueFalse).concat(" ")));
    assertFalse(commandLine.contains(argCredibleIntervals));
    assertTrue(commandLine.contains(argPacingFreq.concat(dummyPacingFrequency.toPlainString().concat(" "))));
    assertTrue(commandLine.contains(argPacingMaxTime.concat(dummyPacingMaxTime.toPlainString().concat(" "))));
    assertTrue(commandLine.contains(argPlasmaConcCount.concat(dummyPlasmaConcCount.toString())));
    assertFalse(commandLine.contains(argPlasmaConcHigh));
    assertFalse(commandLine.contains(argPlasmaConcLow));
    assertTrue(commandLine.contains(argPlasmaConcs.concat("0 10 100").concat(" ")));
    assertFalse(commandLine.contains(argPrefixHill));
    assertFalse(commandLine.contains(argPrefixHillSpread));
    assertFalse(commandLine.contains(argPrefixIC50));
    assertFalse(commandLine.contains(argPrefixPIC50));
    assertFalse(commandLine.contains(argPrefixIC50Spread));
    assertFalse(commandLine.contains(argPrefixPIC50Spread));
    assertFalse(commandLine.contains(argPrefixSaturation));
    assertFalse(commandLine.contains(argPKFile));
    assertFalse(commandLine.contains(argCellML));

    resetAll();
    mocksControl.reset();

    expect(mockApPredictVO.hasCellMLData()).andReturn(dummyHasCellMLData);
    expect(mockApPredictVO.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockApPredictVO.hasPacingFrequency())
          .andReturn(dummyHasPacingFrequency);
    expect(mockApPredictVO.getPacingFreq()).andReturn(dummyPacingFrequency);
    expect(mockApPredictVO.hasMaxPacingTime()).andReturn(dummyHasPacingMaxTime);
    expect(mockApPredictVO.getPacingMaxTime()).andReturn(dummyPacingMaxTime);

    for (final CHANNEL_CURRENT channel : CHANNEL_CURRENT.values()) {
      expect(mockApPredictVO.retrieveChannelData(channel)).andReturn(null);
    }

    expect(mockApPredictVO.isCredibleIntervals())
          .andReturn(dummyIsCredibleIntervals);
    expect(FileUtil.retrieveJobDirectory(null, dummyAppManagerId))
          .andReturn(dummyJobDirectory);

    expect(mockApPredictVO.isPKSimulation()).andReturn(false);
    expect(mockApPredictVO.getPlasma()).andReturn(mockRequestPlasmaData);
    expect(mockRequestPlasmaData.isRangeAssigned())
          .andReturn(dummyPlasmaDataRangeAssigned);
    expect(mockRequestPlasmaData.getPlasmaConcs()).andReturn(null);
    BigDecimal dummyPlasmaConcHigh = BigDecimal.TEN;
    expect(mockRequestPlasmaData.getPlasmaConcHigh())
          .andReturn(dummyPlasmaConcHigh);
    BigDecimal dummyPlasmaConcLow = BigDecimal.ONE;
    expect(mockRequestPlasmaData.getPlasmaConcLow())
          .andReturn(dummyPlasmaConcLow).times(2);
    expect(mockRequestPlasmaData.isConcCountAssigned())
          .andReturn(dummyPlasmaDataConcCountAssigned);
    expect(mockRequestPlasmaData.getPlasmaConcCount())
          .andReturn(dummyPlasmaConcCount);
    expect(mockRequestPlasmaData.isConcLogScaleAssigned())
          .andReturn(dummyPlasmaDataConcLogScaleAssigned);
    dummyPlasmaConcLogScale = true;
    expect(mockRequestPlasmaData.getPlasmaConcLogScale())
          .andReturn(dummyPlasmaConcLogScale);

    expect(Runtime.getRuntime()).andReturn(mockRuntime);
    expect(mockRuntime.exec(capture(captureLocalRunnerArgs)))
          .andReturn(mockProcess);

    replayAll();
    mocksControl.replay();

    apPredictInvoker.invokeApPredict(dummyAppManagerId, mockApPredictVO);

    verifyAll();
    mocksControl.verify();

    capturedLocalRunnerArgs = (String[]) captureLocalRunnerArgs.getValue();

    assertEquals(localRunner, capturedLocalRunnerArgs[0]);
    assertEquals(dummyJobDirectory, capturedLocalRunnerArgs[1]);
    assertEquals(String.valueOf(dummyAppManagerId), capturedLocalRunnerArgs[2]);
    commandLine = capturedLocalRunnerArgs[3];
    assertTrue(commandLine.startsWith(apPredictShell.concat(" ")));
    assertTrue(commandLine.contains(argModel.concat(String.valueOf(dummyModelIdentifier)).concat(" ")));
    assertTrue(commandLine.contains(argPlasmaConcLogScale.concat(valueTrue).concat(" ")));
    assertFalse(commandLine.contains(argCredibleIntervals));
    assertTrue(commandLine.contains(argPacingFreq.concat(dummyPacingFrequency.toPlainString().concat(" "))));
    assertTrue(commandLine.contains(argPacingMaxTime.concat(dummyPacingMaxTime.toPlainString().concat(" "))));
    assertTrue(commandLine.contains(argPlasmaConcCount.concat(dummyPlasmaConcCount.toString())));
    assertTrue(commandLine.contains(argPlasmaConcHigh.concat(dummyPlasmaConcHigh.toPlainString().concat(" "))));
    assertTrue(commandLine.contains(argPlasmaConcLow.concat(dummyPlasmaConcLow.toPlainString().concat(" "))));
    assertFalse(commandLine.contains(argPlasmaConcs));
    assertFalse(commandLine.contains(argPrefixHill));
    assertFalse(commandLine.contains(argPrefixHillSpread));
    assertFalse(commandLine.contains(argPrefixIC50));
    assertFalse(commandLine.contains(argPrefixPIC50));
    assertFalse(commandLine.contains(argPrefixIC50Spread));
    assertFalse(commandLine.contains(argPrefixPIC50Spread));
    assertFalse(commandLine.contains(argPrefixSaturation));
    assertFalse(commandLine.contains(argPKFile));

    resetAll();
    mocksControl.reset();

    expect(mockApPredictVO.hasCellMLData()).andReturn(dummyHasCellMLData);
    expect(mockApPredictVO.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockApPredictVO.hasPacingFrequency())
          .andReturn(dummyHasPacingFrequency);
    expect(mockApPredictVO.getPacingFreq()).andReturn(dummyPacingFrequency);
    expect(mockApPredictVO.hasMaxPacingTime()).andReturn(dummyHasPacingMaxTime);
    expect(mockApPredictVO.getPacingMaxTime()).andReturn(dummyPacingMaxTime);

    for (final CHANNEL_CURRENT channel : CHANNEL_CURRENT.values()) {
      expect(mockApPredictVO.retrieveChannelData(channel)).andReturn(null);
    }

    expect(mockApPredictVO.isCredibleIntervals())
          .andReturn(dummyIsCredibleIntervals);
    expect(FileUtil.retrieveJobDirectory(null, dummyAppManagerId))
          .andReturn(dummyJobDirectory);

    expect(mockApPredictVO.isPKSimulation()).andReturn(false);
    expect(mockApPredictVO.getPlasma()).andReturn(mockRequestPlasmaData);
    expect(mockRequestPlasmaData.isRangeAssigned())
          .andReturn(dummyPlasmaDataRangeAssigned);
    expect(mockRequestPlasmaData.getPlasmaConcs()).andReturn(null);
    expect(mockRequestPlasmaData.getPlasmaConcHigh())
          .andReturn(dummyPlasmaConcHigh);
    expect(mockRequestPlasmaData.getPlasmaConcLow()).andReturn(null);
    expect(mockRequestPlasmaData.isConcCountAssigned())
          .andReturn(dummyPlasmaDataConcCountAssigned);
    expect(mockRequestPlasmaData.getPlasmaConcCount())
          .andReturn(dummyPlasmaConcCount);
    expect(mockRequestPlasmaData.isConcLogScaleAssigned())
          .andReturn(dummyPlasmaDataConcLogScaleAssigned);
    dummyPlasmaConcLogScale = true;
    expect(mockRequestPlasmaData.getPlasmaConcLogScale())
          .andReturn(dummyPlasmaConcLogScale);

    expect(Runtime.getRuntime()).andReturn(mockRuntime);
    expect(mockRuntime.exec(capture(captureLocalRunnerArgs)))
          .andReturn(mockProcess);

    replayAll();
    mocksControl.replay();

    apPredictInvoker.invokeApPredict(dummyAppManagerId, mockApPredictVO);

    verifyAll();
    mocksControl.verify();

    capturedLocalRunnerArgs = (String[]) captureLocalRunnerArgs.getValue();

    assertEquals(localRunner, capturedLocalRunnerArgs[0]);
    assertEquals(dummyJobDirectory, capturedLocalRunnerArgs[1]);
    assertEquals(String.valueOf(dummyAppManagerId), capturedLocalRunnerArgs[2]);
    commandLine = capturedLocalRunnerArgs[3];
    assertTrue(commandLine.startsWith(apPredictShell.concat(" ")));
    assertTrue(commandLine.contains(argModel.concat(String.valueOf(dummyModelIdentifier)).concat(" ")));
    assertTrue(commandLine.contains(argPlasmaConcLogScale.concat(valueTrue).concat(" ")));
    assertFalse(commandLine.contains(argCredibleIntervals));
    assertTrue(commandLine.contains(argPacingFreq.concat(dummyPacingFrequency.toPlainString().concat(" "))));
    assertTrue(commandLine.contains(argPacingMaxTime.concat(dummyPacingMaxTime.toPlainString().concat(" "))));
    assertTrue(commandLine.contains(argPlasmaConcCount.concat(dummyPlasmaConcCount.toString())));
    assertTrue(commandLine.contains(argPlasmaConcHigh.concat(dummyPlasmaConcHigh.toPlainString().concat(" "))));
    assertFalse(commandLine.contains(argPlasmaConcLow));
    assertFalse(commandLine.contains(argPlasmaConcs));
    assertFalse(commandLine.contains(argPrefixHill));
    assertFalse(commandLine.contains(argPrefixHillSpread));
    assertFalse(commandLine.contains(argPrefixIC50));
    assertFalse(commandLine.contains(argPrefixPIC50));
    assertFalse(commandLine.contains(argPrefixIC50Spread));
    assertFalse(commandLine.contains(argPrefixPIC50Spread));
    assertFalse(commandLine.contains(argPrefixSaturation));
    assertFalse(commandLine.contains(argPKFile));
  }

  @Test
  public void testSetBaseDir() throws IOException {
    final String dummyBaseDir = "dummyBaseDir";

    expect(mockApPredictVO.hasCellMLData()).andReturn(dummyHasCellMLData);
    expect(mockApPredictVO.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockApPredictVO.hasPacingFrequency())
          .andReturn(dummyHasPacingFrequency);
    expect(mockApPredictVO.hasMaxPacingTime()).andReturn(dummyHasPacingMaxTime);
    for (final CHANNEL_CURRENT channel : CHANNEL_CURRENT.values()) {
      expect(mockApPredictVO.retrieveChannelData(channel)).andReturn(null);
    }

    expect(mockApPredictVO.isCredibleIntervals())
          .andReturn(dummyIsCredibleIntervals);
    mockStatic(FileUtil.class);
    expect(FileUtil.retrieveJobDirectory(null, dummyAppManagerId))
          .andReturn(dummyJobDirectory);

    expect(mockApPredictVO.isPKSimulation()).andReturn(false);
    expect(mockApPredictVO.getPlasma()).andReturn(mockRequestPlasmaData);
    boolean dummyPlasmaDataRangeAssigned = false;
    expect(mockRequestPlasmaData.isRangeAssigned())
          .andReturn(dummyPlasmaDataRangeAssigned);
    boolean dummyPlasmaDataConcCountAssigned = false;
    expect(mockRequestPlasmaData.isConcCountAssigned())
          .andReturn(dummyPlasmaDataConcCountAssigned);
    boolean dummyPlasmaDataConcLogScaleAssigned = false;
    expect(mockRequestPlasmaData.isConcLogScaleAssigned()) 
          .andReturn(dummyPlasmaDataConcLogScaleAssigned);

    mockRuntime = mocksControl.createMock(Runtime.class);
    mockStatic(Runtime.class);
    expect(Runtime.getRuntime()).andReturn(mockRuntime);
    mockProcess = mocksControl.createMock(Process.class);
    Capture<String[]> captureLocalRunnerArgs = newCapture();
    expect(mockRuntime.exec(capture(captureLocalRunnerArgs)))
          .andReturn(mockProcess);

    replayAll();
    mocksControl.replay();

    String outcome = apPredictInvoker.invokeApPredict(dummyAppManagerId,
                                                      mockApPredictVO);

    verifyAll();
    mocksControl.verify();

    assertEquals("Invoked './local_runner.sh'", outcome);
    String[] capturedLocalRunnerArgs = (String[]) captureLocalRunnerArgs.getValue();
    assertEquals(localRunner, capturedLocalRunnerArgs[0]);
    assertEquals(dummyJobDirectory, capturedLocalRunnerArgs[1]);
    assertEquals(String.valueOf(dummyAppManagerId), capturedLocalRunnerArgs[2]);
    String commandLine = capturedLocalRunnerArgs[3];
    assertTrue(commandLine.startsWith(apPredictShell.concat(" ")));
    assertFalse(commandLine.contains(dummyBaseDir));

    resetAll();
    mocksControl.reset();

    /*
     * Assign dummy base dir
     */
    apPredictInvoker.setBaseDir(dummyBaseDir);

    expect(mockApPredictVO.hasCellMLData()).andReturn(dummyHasCellMLData);
    expect(mockApPredictVO.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockApPredictVO.hasPacingFrequency())
          .andReturn(dummyHasPacingFrequency);
    expect(mockApPredictVO.hasMaxPacingTime()).andReturn(dummyHasPacingMaxTime);
    for (final CHANNEL_CURRENT channel : CHANNEL_CURRENT.values()) {
      expect(mockApPredictVO.retrieveChannelData(channel)).andReturn(null);
    }

    expect(mockApPredictVO.isCredibleIntervals())
          .andReturn(dummyIsCredibleIntervals);
    expect(FileUtil.retrieveJobDirectory(dummyBaseDir, dummyAppManagerId))
          .andReturn(dummyJobDirectory);

    expect(mockApPredictVO.isPKSimulation()).andReturn(false);
    expect(mockApPredictVO.getPlasma()).andReturn(mockRequestPlasmaData);
    expect(mockRequestPlasmaData.isRangeAssigned())
          .andReturn(dummyPlasmaDataRangeAssigned);
    expect(mockRequestPlasmaData.isConcCountAssigned())
          .andReturn(dummyPlasmaDataConcCountAssigned);
    expect(mockRequestPlasmaData.isConcLogScaleAssigned())
          .andReturn(dummyPlasmaDataConcLogScaleAssigned);

    mockRuntime = mocksControl.createMock(Runtime.class);
    expect(Runtime.getRuntime()).andReturn(mockRuntime);
    mockProcess = mocksControl.createMock(Process.class);
    captureLocalRunnerArgs = newCapture();
    expect(mockRuntime.exec(capture(captureLocalRunnerArgs)))
          .andReturn(mockProcess);

    replayAll();
    mocksControl.replay();

    outcome = apPredictInvoker.invokeApPredict(dummyAppManagerId,
                                               mockApPredictVO);

    verifyAll();
    mocksControl.verify();

    assertEquals("Invoked './local_runner.sh'", outcome);
    capturedLocalRunnerArgs = (String[]) captureLocalRunnerArgs.getValue();
    assertEquals(localRunner, capturedLocalRunnerArgs[0]);
    assertEquals(dummyJobDirectory, capturedLocalRunnerArgs[1]);
    assertEquals(String.valueOf(dummyAppManagerId), capturedLocalRunnerArgs[2]);
    commandLine = capturedLocalRunnerArgs[3];
    assertTrue(commandLine.startsWith(apPredictShell.concat(" ")));
    // Still doesn't contain the assigned directory however FileUtil.retrieveJobDirectory(..) is the test!
    assertFalse(commandLine.contains(dummyBaseDir));
  }
}