/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Test;

/**
 * Unit test the simulation result value object.
 *
 * @author geoff
 */
public class SimulationResultTest {

  @Test
  public void testEverything() {
    long dummyAppManagerId = 4l;
    BigDecimal dummyCmpdConc = null;
    String dummyTimes = null;
    String dummyVoltages = null;
    String dummyAPD90 = null;
    String dummyDeltaAPD90 = null;
    String dummyQNet = null;

    SimulationResult simulationResult = new SimulationResult(dummyAppManagerId,
                                                             dummyCmpdConc,
                                                             dummyTimes,
                                                             dummyVoltages,
                                                             dummyAPD90,
                                                             dummyDeltaAPD90,
                                                             dummyQNet);

    assertSame(dummyAppManagerId, simulationResult.getAppManagerId());
    assertNull(simulationResult.getCmpdConc());
    assertNull(simulationResult.getTimes());
    assertNull(simulationResult.getVoltages());
    assertNull(simulationResult.getAPD90());
    assertNull(simulationResult.getDeltaAPD90());
    assertNull(simulationResult.getQNet());
    assertNotNull(simulationResult.toString());

    dummyCmpdConc = BigDecimal.ONE;
    dummyTimes = "dummyTimes";
    dummyVoltages = "dummyVoltages";
    dummyAPD90 = "dummyAPD90";
    dummyDeltaAPD90 = "dummyDeltaAPD90";
    dummyQNet = "dummyQNet";

    simulationResult = new SimulationResult(dummyAppManagerId, dummyCmpdConc,
                                            dummyTimes, dummyVoltages,
                                            dummyAPD90, dummyDeltaAPD90,
                                            dummyQNet);

    assertSame(dummyAppManagerId, simulationResult.getAppManagerId());
    assertTrue(dummyCmpdConc.compareTo(simulationResult.getCmpdConc()) == 0);
    assertEquals(dummyTimes, simulationResult.getTimes());
    assertEquals(dummyVoltages, simulationResult.getVoltages());
    assertEquals(dummyAPD90, simulationResult.getAPD90());
    assertEquals(dummyDeltaAPD90, simulationResult.getDeltaAPD90());
    assertEquals(dummyQNet, simulationResult.getQNet());
    assertNotNull(simulationResult.toString());
  }
}