/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.util;

import java.io.IOException;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.app_manager.exception.MessageKeys;
import uk.ac.ox.cs.nc3rs.app_manager.value.ActionOutcomeVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.ApPredictVO;

/**
 * Utility class for performing system processes.
 * 
 * @author geoff
 */
public class SystemUtil {

  private static final String defaultKillCommand = "/bin/kill";
  private static final String defaultKillFlag = "-9";

  /** Instructions to invoke on the system */
  public static enum INSTRUCTION {
    DELETE
  }

  private static final Log log = LogFactory.getLog(SystemUtil.class);

  /**
   * Deserialize the {@link ApPredictVO} from byte[] to an object.
   *  
   * @param apPredictByteArray {@link ApPredictVO} as {@code byte[]}.
   * @return Deserialized {@literal ApPredictVO} object.
   */
  public static ApPredictVO apPredictDeserialize(final byte[] apPredictByteArray) {
    return (ApPredictVO) SerializationUtils.deserialize(apPredictByteArray);
  }

  /**
   * Serialize the {@link ApPredictVO} object to a byte[].
   * 
   * @param apPredictVO {@literal ApPredictVO} object to serialize.
   * @return Serialized {@code byte[]} representation of object.
   */
  public static byte[] apPredictSerialize(final ApPredictVO apPredictVO) {
    return SerializationUtils.serialize(apPredictVO);
  }

  /**
   * Make a system call.
   * 
   * @param processId Process identifier.
   * @param instruction System instruction to undertake.
   * @return Action outcome.
   */
  public static ActionOutcomeVO makeSystemCall(final String processId, final INSTRUCTION instruction) {
    log.debug("~makeSystemCall() : Instruction '" + instruction + "' requested for process id '" + processId + "'.");

    ActionOutcomeVO outcome = null;

    switch (instruction) {
      case DELETE:
        final String[] args = { defaultKillCommand, defaultKillFlag, processId };
        final String invocation = StringUtils.join(args, " ");
        log.debug("~makeSystemCall() : Invoking delete command '" + invocation + "'.");

        final Runtime runtime = Runtime.getRuntime();
        try {
          runtime.exec(args);
          outcome = new ActionOutcomeVO(true, MessageKeys.DELETE_SUCCESS);
        } catch (IOException ioe) {
          final String message = "Invocation of kill command failed with message '" + ioe.getMessage() + "'";
          log.error("~makeSystemCall() : " + message);
          outcome = new ActionOutcomeVO(false, message);
        }

        break;
      default:
        throw new UnsupportedOperationException("Unknown system instruction '" + instruction + "'.");
    }
 
    return outcome;
  }
}