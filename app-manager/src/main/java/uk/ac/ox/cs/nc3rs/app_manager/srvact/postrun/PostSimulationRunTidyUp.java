/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.postrun;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.Selectors;
import org.apache.commons.vfs2.VFS;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.app_manager.util.FileUtil;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.SetProcessStatusRequest;

/**
 * Tidy up after simulation run.
 *
 * @author Geoff Williams
 */
@Component
public class PostSimulationRunTidyUp {

  private String baseDir;

  private static final Log log = LogFactory.getLog(PostSimulationRunTidyUp.class);

  /**
   * Remove the filesytem directory after a simulation run.
   * 
   * @param setProcessStatusRequest Incoming SOAP request.
   * @return Textual string containing summary of tidy up work.
   */
  @ServiceActivator
  public String postSimulationRunTidyUp(final SetProcessStatusRequest setProcessStatusRequest) {
    log.debug("~int.postSimulationRunTidyUp() : Invoked.");
    log.trace("~int.postSimulationRunTidyUp() : Thread '" + Thread.currentThread().getName() + "'.");

    final long appManagerId = setProcessStatusRequest.getAppManagerId();

    final String host = setProcessStatusRequest.getHost();
    final String processId = setProcessStatusRequest.getProcessId();
    final String source = setProcessStatusRequest.getSource();
    final String status = setProcessStatusRequest.getStatus();
    final String statusCode = setProcessStatusRequest.getStatusCode();

    final String jobDirectory = FileUtil.retrieveJobDirectory(baseDir, appManagerId);

    try {
      final FileSystemManager fileSystemManager = VFS.getManager();
      final FileObject fileObject = fileSystemManager.resolveFile(jobDirectory);
      log.debug("~int.postSimulationRunTidyUp() : About to delete '" + jobDirectory + "'.");
      fileObject.delete(Selectors.SELECT_ALL);
    } catch (FileSystemException e) {
      log.warn("~int.postSimulationRunTidyUp() : FS Exception during file deletion : '" + e.getMessage() + "'.");
    }

    final StringBuffer details = new StringBuffer();
    details.append("Host '" + host + "', ");
    details.append("Process Id '" + processId + "', ");
    details.append("App Manager Id '" + appManagerId + "', ");
    details.append("Source '" + source + "', ");
    details.append("Status '" + status + "', ");
    details.append("Status Code '" + statusCode + "', ");
    final String summary = details.toString();
    log.debug("~int.postSimulationRunTidyUp() : Summary '" + summary + "'.");

    return summary;
  }

  /**
   * Assign the base directory, e.g. /home/me/run_here/
   * 
   * @param baseDir Base directory.
   */
  // Spring-injected.
  @Value("${base.dir}")
  public void setBaseDir(final String baseDir) {
    this.baseDir = baseDir;
  }
}