/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.value;

import java.io.Serializable;

import uk.ac.ox.cs.nc3rs.app_manager.value.type.InvocationSource;
import uk.ac.ox.cs.nc3rs.app_manager.value.type.SimulationType;

/**
 * Value object storing useful info.
 *
 * @author geoff
 */
public class SimulationInfoVO implements Serializable {

  private static final long serialVersionUID = 1L;

  private final InvocationSource invocationSource;
  private final SimulationType simulationType;

  /**
   * Initialising constructor.
   * 
   * @param invocationSource Simulation invocation component source.
   * @param simulationType Simulation type, e.g. PK.
   */
  public SimulationInfoVO(final InvocationSource invocationSource,
                          final SimulationType simulationType) {
    this.invocationSource = invocationSource;
    this.simulationType = simulationType;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "SimulationInfoVO [invocationSource=" + invocationSource
        + ", simulationType=" + simulationType + "]";
  }

  /**
   * Retrieve the simulation invocation source.
   * 
   * @return Simulation invocation source.
   */
  public InvocationSource getInvocationSource() {
    return invocationSource;
  }

  /**
   * Retrieve the simulation type.
   * 
   * @return Simulation type.
   */
  public SimulationType getSimulationType() {
    return simulationType;
  }

  
}