/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.value;

import java.util.ArrayList;
import java.util.List;

import uk.ac.ox.cs.nc3rs.app_manager.domain.PKResult;
import uk.ac.ox.cs.nc3rs.app_manager.domain.SimulationResult;

/**
 * Value object for retrieving all simulation domain objects.
 *
 * @author geoff
 */
public class AllSimulationDomainResultsVO {

  private final List<PKResult> pkResults = new ArrayList<PKResult>();
  private final List<SimulationResult> simulationResults = new ArrayList<SimulationResult>();
  private final String deltaAPDPercentileNames;

  /**
   * Initialising constructor.
   * 
   * @param pkResults Optional PKPD results.
   * @param simulationResults Simulation results.
   * @param deltaAPD90PercentileNames CSV-format Delta APD90 percentile names.
   */
  public AllSimulationDomainResultsVO(final List<PKResult> pkResults,
                                      final List<SimulationResult> simulationResults,
                                      final String deltaAPD90PercentileNames) {
    if (pkResults != null) {
      this.pkResults.addAll(pkResults);
    }
    if (simulationResults != null) {
      this.simulationResults.addAll(simulationResults);
    }
    this.deltaAPDPercentileNames = deltaAPD90PercentileNames;
  }

  /**
   * Retrieve the DeltaAPD90 percentile names.
   * 
   * @return Delta APD90 percentile names.
   */
  public String getDeltaAPDPercentileNames() {
    return deltaAPDPercentileNames;
  }

  /**
   * Retrieve the PKPD results.
   * 
   * @return PKPD results, or empty collection if none available.
   */
  public List<PKResult> getPkResults() {
    return pkResults;
  }

  /**
   * Retrieve the simulation results.
   * 
   * @return Simulation results, or empty collection if none available.
   */
  public List<SimulationResult> getSimulationResults() {
    return simulationResults;
  }
}