/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.ws.app_manager.proxy;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.ObjectFactory;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.SetProcessStatusRequest;

/**
 * Web service proxy for App Manager services.
 * <p>
 * App Manager talks Web Services to itself to assign process statuses.
 * 
 * @author geoff
 */
public class AppServicesProxyImpl extends WebServiceGatewaySupport implements AppServicesProxy {

  // appCtx.ws.security-outgoing.xml
  @Autowired(required=false)
  @Qualifier(AppIdentifiers.COMPONENT_APP_MANAGER_SERVICES_INTERCEPTOR)
  private ClientInterceptor wsAppManagerServicesInterceptor;

  private static final ObjectFactory objectFactory = new ObjectFactory();

  private static final Log log = LogFactory.getLog(AppServicesProxyImpl.class);

  @PostConstruct
  private void postConstruct() {
    final ClientInterceptor[] wsClientInterceptors = { wsAppManagerServicesInterceptor };

    this.setInterceptors(wsClientInterceptors);
    for (int idx = 0; idx < wsClientInterceptors.length; idx++) {
      log.info("~AppServicesProxyImpl() : Interceptor '" + wsClientInterceptors[idx] + "' assigned for outbound.");
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.app_manager.service.AppServicesProxy#assignProcessStatus(int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
   */
  @Override
  public void assignProcessStatus(final long appManagerId, final String status,
                                  final String statusCode, final String host, final String source,
                                  final String processId) {
    log.debug("assignProcessStatus(..) : Invoked");

    final SetProcessStatusRequest processStatusRequest = objectFactory.createSetProcessStatusRequest();
    processStatusRequest.setAppManagerId(appManagerId);
    processStatusRequest.setHost(host);
    processStatusRequest.setProcessId(processId);
    processStatusRequest.setSource(source);
    processStatusRequest.setStatusCode(statusCode);
    processStatusRequest.setStatus(status);

    getWebServiceTemplate().marshalSendAndReceive(processStatusRequest);
  }
}