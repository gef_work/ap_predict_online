/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.value;

/**
 * Value object used to store input from voltage_results.dat file.
 * 
 * @author Geoff Williams
 */
public class VoltageResultsVO {

  private final String cmpdConc;
  private final String upstrokeVelocity;
  private final String peakVm;
  private final String apd50;
  private final String apd90;
  private final String deltaAPD90;

  /**
   * (Non-validating) initialising constructor.
   * 
   * @param cmpdConc The concentration value (in µM).
   * @param upstrokeVelocity Upstroke velocity.
   * @param peakVm Peak Vm.
   * @param apd50 APD50.
   * @param apd90 APD90.
   * @param deltaAPD90 Delta APD90.
   */
  public VoltageResultsVO(final String cmpdConc, final String upstrokeVelocity,
                          final String peakVm, final String apd50,
                          final String apd90, final String deltaAPD90) {
    this.cmpdConc = cmpdConc;
    this.upstrokeVelocity = upstrokeVelocity;
    this.peakVm = peakVm;
    this.apd50 = apd50;
    this.apd90 = apd90;
    this.deltaAPD90 = deltaAPD90;
  }

  /**
   * Retrieve the compound concentration value.
   * 
   * @return Compound concentration (in µM).
   */
  public String getCmpdConc() {
    return cmpdConc;
  }

  /**
   * Retrieve the upstroke velocity.
   * 
   * @return The upstroke velocity.
   */
  public String getUpstrokeVelocity() {
    return upstrokeVelocity;
  }

  /**
   * Retrieve the peak Vm.
   * 
   * @return The peak Vm.
   */
  public String getPeakVm() {
    return peakVm;
  }

  /**
   * Retrieve the APD50 value.
   * 
   * @return APD50 value.
   */
  public String getAPD50() {
    return apd50;
  }

  /**
   * Retrieve the APD90 value.
   * 
   * @return APD90 value.
   */
  public String getAPD90() {
    return apd90;
  }

  /**
   * Retrieve the Delta APD90 value.
   * 
   * @return The Delta APD90.
   */
  public String getDeltaAPD90() {
    return deltaAPD90;
  }
}