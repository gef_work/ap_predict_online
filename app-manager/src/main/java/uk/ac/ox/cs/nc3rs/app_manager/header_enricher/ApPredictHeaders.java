/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the names of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.header_enricher;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO;
import uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAOImpl;
import uk.ac.ox.cs.nc3rs.app_manager.util.SystemUtil;
import uk.ac.ox.cs.nc3rs.app_manager.value.ApPredictVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.SimulationInfoVO;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.SetProcessStatusRequest;

/**
 * Place various object retrieved from the database into the channel message headers.
 *
 * @author Geoff Williams
 */
@Component
public class ApPredictHeaders {

  @Autowired @Qualifier(AppIdentifiers.COMPONENT_JDBC_APP_MANAGER_DAO)
  private AppManagerDAO appManagerDAO;

  private static final Log log = LogFactory.getLog(ApPredictHeaders.class);

  /**
   * Place the AppManager identifier into the channel message headers.
   * 
   * @param appManagerRecord Query record.
   * @return App manager identifier.
   * @throws IllegalStateException If can't determine identifier.
   */
  public Long setAppManagerIdHeader(final Map<String, Object> appManagerRecord)
                                    throws IllegalStateException {
    log.debug("~int.setAppManagerIdHeader() : Invoked.");

    Long appManagerId = null;
    String errorReason = null;
    if (appManagerRecord.containsKey(AppManagerDAOImpl.AC_APP_MANAGER_ID)) {
      final Object provisionalAppManagerId = appManagerRecord.get(AppManagerDAOImpl.AC_APP_MANAGER_ID);
      if (provisionalAppManagerId != null) {
        if (provisionalAppManagerId instanceof BigDecimal) {
          /* Oracle seems to read in the primary key as a BigDecimal. */
          appManagerId = ((BigDecimal) provisionalAppManagerId).longValue();
        } else if (provisionalAppManagerId instanceof Long) {
          /* MySQL prefers a long primary key. */
          appManagerId = (Long) provisionalAppManagerId;
        } else {
          errorReason = AppManagerDAOImpl.AC_APP_MANAGER_ID.concat(" is expected to be either a BigDecimal or Long value.");
        }
      } else {
        errorReason = AppManagerDAOImpl.AC_APP_MANAGER_ID.concat(" was null!");
      }
    } else {
      errorReason = "No column named '" + AppManagerDAOImpl.AC_APP_MANAGER_ID + "' found in query results.";
    }

    if (appManagerId == null) {
      log.warn("~int.setAppManagerIdHeader() : ".concat(errorReason));
      throw new IllegalStateException(errorReason);
    }
    log.debug("~int.setAppManagerIdHeader() : [" + appManagerId + "] : Set in message headers.");

    return appManagerId;
  }

  /**
   * Place the {@linkplain ApPredictVO} value object into the channel message
   * headers.
   * 
   * @param appManagerRecord Query record.
   * @return {@linkplain ApPredictVO} value object.
   * @throws IllegalStateException If can't determine {@linkplain ApPredictVO}.
   */
  public ApPredictVO setApPredictVOHeader(final Map<String, Object> appManagerRecord)
                                          throws IllegalStateException {
    log.debug("~int.setApPredictVOHeader() : Invoked.");

    ApPredictVO apPredictVO = null;
    String errorReason = null;
    if (appManagerRecord.containsKey(AppManagerDAOImpl.AC_A_OBJECT_VALUE)) {
      final Object provisionalObjectValue = appManagerRecord.get(AppManagerDAOImpl.AC_A_OBJECT_VALUE);
      if (provisionalObjectValue != null) {
        apPredictVO = SystemUtil.apPredictDeserialize((byte[]) provisionalObjectValue);
      } else {
        errorReason = AppManagerDAOImpl.AC_A_OBJECT_VALUE.concat(" was null!");
      }
    } else {
      errorReason = "No column named '" + AppManagerDAOImpl.AC_A_OBJECT_VALUE + "' found in query results.";
    }

    if (apPredictVO == null) {
      log.warn("~int.setApPredictVOHeader() : ".concat(errorReason));
      throw new IllegalStateException(errorReason);
    }
    log.debug("~int.setApPredictVOHeader() : Deserialized '" + apPredictVO.toString() + "'.");

    return apPredictVO;
  }

  /**
   * Place the simulation information value object into the channel message headers.
   * 
   * @param setProcessStatusRequest Incoming request.
   * @return Simulation information value object (or {@code null} if not available).
   */
  public SimulationInfoVO setSimulationInfo(final SetProcessStatusRequest setProcessStatusRequest) {
    log.debug("~int.setSimulationInfo() : Invoked.");

    return appManagerDAO.retrieveSimulationInfo(setProcessStatusRequest.getAppManagerId());
  }
}