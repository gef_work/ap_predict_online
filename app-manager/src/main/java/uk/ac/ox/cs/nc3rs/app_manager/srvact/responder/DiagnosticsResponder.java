/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.responder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO;
import uk.ac.ox.cs.nc3rs.app_manager.util.FileUtil;
import uk.ac.ox.cs.nc3rs.app_manager.value.DiagnosticsVO;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.ObjectFactory;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.RetrieveDiagnosticsRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.RetrieveDiagnosticsResponse;

/**
 * Respond to request with the application's diagnostics data.
 * <p>
 * The first source to check is in the database (which should be populated if the simulation has
 * finished and the data not yet retrieved), failing that the local filesystem (if the simulation is
 * currently running).
 *
 * @author Geoff Williams
 */
@Component
public class DiagnosticsResponder {

  @Autowired @Qualifier(AppIdentifiers.COMPONENT_JDBC_APP_MANAGER_DAO)
  private AppManagerDAO appManagerDAO;

  private String baseDir;

  private static final ObjectFactory objectFactory = new ObjectFactory();

  private static final Log log = LogFactory.getLog(DiagnosticsResponder.class);

  /**
   * Respond to the request for diagnostics information.
   * 
   * @param retrieveDiagnosticsRequest Incoming SOAP request.
   * @return SOAP response for onward processing.
   */
  @ServiceActivator
  public RetrieveDiagnosticsResponse retrieveDiagnostics(final RetrieveDiagnosticsRequest retrieveDiagnosticsRequest) {
    log.debug("~int.retrieveDiagnostics() : Invoked.");
    log.trace("~int.retrieveDiagnostics() : Thread '" + Thread.currentThread().getName() + "'.");

    final long appManagerId = retrieveDiagnosticsRequest.getAppManagerId();

    DiagnosticsVO diagnostics = null;

    diagnostics = appManagerDAO.retrieveDiagnostics(appManagerId);
    if (diagnostics == null) {
      log.trace("~int.retrieveDiagnostics() : No persisted diagnostics, retrieving from filesystem.");
      diagnostics = FileUtil.retrieveDiagnostics(appManagerId, baseDir);
    }

    final RetrieveDiagnosticsResponse retrieveDiagnosticsResponse = objectFactory.createRetrieveDiagnosticsResponse();
    retrieveDiagnosticsResponse.setAppManagerId(appManagerId);
    retrieveDiagnosticsResponse.setInfo(diagnostics.getInfo());
    retrieveDiagnosticsResponse.setOutput(diagnostics.getOutput());

    return retrieveDiagnosticsResponse;
  }

  /**
   * Assign the base directory, e.g. /home/me/run_here/
   * 
   * @param baseDir Base directory.
   */
  // Spring-injected.
  @Value("${base.dir}")
  public void setBaseDir(final String baseDir) {
    this.baseDir = baseDir;
  }
}