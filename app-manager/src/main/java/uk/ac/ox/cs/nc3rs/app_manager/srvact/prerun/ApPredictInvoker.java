/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.prerun;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.annotation.Header;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.util.FileUtil;
import uk.ac.ox.cs.nc3rs.app_manager.value.ApPredictVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.ApPredictVO.CHANNEL_CURRENT;
import uk.ac.ox.cs.nc3rs.app_manager.value.AssociatedData;
import uk.ac.ox.cs.nc3rs.app_manager.value.RequestPlasmaData;
import uk.ac.ox.cs.nc3rs.app_manager.value.RequestChannelData;

/**
 * ApPredict application invoker.
 *
 * @author Geoff Williams
 */
@Component
public class ApPredictInvoker extends AbstractChasteDerivativeInvoker {

  private static final String argCellML = "--cellml";
  private static final String argCredibleIntervals = "--credible-intervals";
  private static final String argModel = "--model";
  private static final String argPacingFreq = "--pacing-freq";
  private static final String argPacingMaxTime = "--pacing-max-time";
  private static final String argPKFile = "--pkpd-file";
  private static final String argPlasmaConcCount = "--plasma-conc-count";
  private static final String argPlasmaConcHigh = "--plasma-conc-high";
  private static final String argPlasmaConcLogScale = "--plasma-conc-logscale";
  private static final String argPlasmaConcLow = "--plasma-conc-low";
  private static final String argPlasmaConcs = "--plasma-concs";
  private static final String argPrefixHill = "--hill-";
  private static final String argPrefixHillSpread = "--hill-spread-";
  private static final String argPrefixIC50 = "--ic50-";
  private static final String argPrefixPIC50 = "--pic50-";
  private static final String argPrefixIC50Spread = "--ic50-spread-";
  private static final String argPrefixPIC50Spread = "--pic50-spread-";
  private static final String argPrefixSaturation = "--saturation-";

  private static final String _false = " ".concat(Boolean.FALSE.toString());
  private static final String _true = " ".concat(Boolean.TRUE.toString());

  private static final String localRunner = "./local_runner.sh";
  private static final String pkpdFileName = "pkpd.file";

  private String baseDir;

  private static final Log log = LogFactory.getLog(ApPredictInvoker.class);

  // Non-valued command-line arg appender
  private void appender(final StringBuffer commandLine, final String argIdentifier) {
    commandLine.append(argIdentifier).append(" ");
  }

  // Valued arg command-line appender
  private void appender(final StringBuffer commandLine, final String argIdentifier,
                        final String argValue) {
    appender(commandLine, argIdentifier);
    commandLine.append(argValue).append(" ");
  }

  /**
   * Invoke the ApPredict application.
   * 
   * @param appManagerId App Manager identifier.
   * @param apPredictVO ApPredict value object (derived from incoming WS request).
   * @return Textual representation of local runner arguments.
   */
  @ServiceActivator
  public String invokeApPredict(final @Header(AppIdentifiers.SI_HDR_APP_MANAGER_ID)
                                      long appManagerId,
                                final @Header(AppIdentifiers.SI_HDR_APPREDICT_VO)
                                      ApPredictVO apPredictVO) {
    log.debug("~int.invokeApPredict() : Invoked");

    final StringBuffer commandLine = new StringBuffer();
    commandLine.append("ApPredict.sh").append(" ");

    if (apPredictVO.hasCellMLData()) {
      appender(commandLine, argCellML, apPredictVO.getCellMLFileName());
    } else {
      appender(commandLine, argModel,
               apPredictVO.getModelIdentifier().toString());
    }

    if (apPredictVO.hasPacingFrequency()) {
      appender(commandLine, argPacingFreq,
               apPredictVO.getPacingFreq().toPlainString());
    }
    if (apPredictVO.hasMaxPacingTime()) {
      appender(commandLine, argPacingMaxTime,
               apPredictVO.getPacingMaxTime().toPlainString());
    }

    for (final CHANNEL_CURRENT channel : CHANNEL_CURRENT.values()) {
      final RequestChannelData ionChannelData = apPredictVO.retrieveChannelData(channel);
      if (ionChannelData == null) {
        log.debug("~int.invokeApPredict() : '" + appManagerId + "' - No data for channel '" + channel + "'.");
        continue;
      }
      log.debug("~int.invokeApPredict() : '" + appManagerId + "' - Data for channel '" + channel + "'.");

      final List<String> c50s = new ArrayList<String>();
      final List<String> hills = new ArrayList<String>();
      final List<String> saturations = new ArrayList<String>();
      boolean usingIC50 = true;
      for (final AssociatedData eachAssociation : ionChannelData.getAssociatedData()) {
        final BigDecimal pIC50 = eachAssociation.getpIC50();
        final BigDecimal IC50 = eachAssociation.getIC50();
        if (pIC50 != null) {
          usingIC50 = false;
          c50s.add(pIC50.toPlainString());
        } else {
          c50s.add(IC50.toPlainString());
        }

        final BigDecimal hill = eachAssociation.getHill();
        if (hill != null) {
          hills.add(hill.toPlainString());
        }

        final BigDecimal saturation = eachAssociation.getSaturation();
        saturations.add(saturation.toPlainString());
      }

      final boolean hillsPresent = !hills.isEmpty();

      final String channelSuffix = channel.getInvocationArg();

      final String c50String = StringUtils.join(c50s, " ");
      String hillString = null;
      if (hillsPresent) {
        hillString = StringUtils.join(hills, " ");
      }
      final String saturationString = StringUtils.join(saturations, " ");

      final String argC50Prefix = usingIC50 ? argPrefixIC50 : argPrefixPIC50;
      appender(commandLine, argC50Prefix.concat(channelSuffix), c50String);
      if (hillString != null) {
        appender(commandLine, argPrefixHill.concat(channelSuffix), hillString);
      }
      appender(commandLine, argPrefixSaturation.concat(channelSuffix),
               saturationString);

      final BigDecimal spreadC50 = ionChannelData.getSpreadC50();               // optional
      if (spreadC50 != null) {
        final String argPrefix = usingIC50 ? argPrefixIC50Spread : argPrefixPIC50Spread;
        appender(commandLine, argPrefix.concat(channelSuffix),
                 spreadC50.toPlainString());
      }

      if (hillString != null) {
        final BigDecimal spreadHill = ionChannelData.getSpreadHill();           // optional
        if (spreadHill != null) {
          appender(commandLine, argPrefixHillSpread.concat(channelSuffix),
                   spreadHill.toPlainString());
        }
      }
    }

    if (apPredictVO.isCredibleIntervals()) {
      appender(commandLine, argCredibleIntervals.concat(" ")
                                                .concat(apPredictVO.getCredibleIntervalPercentiles()));
    }

    final String jobDirectory = FileUtil.retrieveJobDirectory(baseDir,
                                                              appManagerId);

    if (apPredictVO.isPKSimulation()) {
      /* getPKFileLocation() not used - instead symlink to pkpdFileName in
         prepare.sh to avoid ApPredict.sh command using a local fs path (which
         causes problems when invoking ApPredict.sh from within docker! */
      appender(commandLine, argPKFile, pkpdFileName);
    } else {
      final RequestPlasmaData plasmaData = apPredictVO.getPlasma();
      if (plasmaData.isRangeAssigned()) {
        if (plasmaData.getPlasmaConcs() != null) {
          final String[] concsArr = plasmaData.getPlasmaConcs().split(",");
          appender(commandLine, argPlasmaConcs, StringUtils.join(concsArr, " "));
        } else {
          appender(commandLine, argPlasmaConcHigh, 
                   plasmaData.getPlasmaConcHigh().toPlainString());
          if (plasmaData.getPlasmaConcLow() != null) {
            appender(commandLine, argPlasmaConcLow,
                     plasmaData.getPlasmaConcLow().toPlainString());
          }
        }
      }
      if (plasmaData.isConcCountAssigned()) {
        appender(commandLine, argPlasmaConcCount,
                 Short.valueOf(plasmaData.getPlasmaConcCount()).toString());
      }
      if (plasmaData.isConcLogScaleAssigned() && !plasmaData.getPlasmaConcLogScale()) {
        /* Assign "--plasma-conc-logscale false" only if a value was assigned
             in WS call AND that assigned value was "false".
           Update app_manager.xsd documentation if this changes! */ 
        appender(commandLine, argPlasmaConcLogScale.concat(_false));
      } else {
        appender(commandLine, argPlasmaConcLogScale.concat(_true));
      }
    }

    final String fullCommandLine = commandLine.toString();

    log.debug("~int.invokeApPredict(String, long, ApPredictVO) : Command line '" + fullCommandLine + "'");

    final String[] localRunnerArgs = new String[4];
    if (AppIdentifiers.OS_NAME.equals(AppIdentifiers.LINUX)) {
      localRunnerArgs[0] = localRunner;
      localRunnerArgs[1] = jobDirectory;
      localRunnerArgs[2] = Long.valueOf(appManagerId).toString();
      localRunnerArgs[3] = fullCommandLine;

      final String invocation = StringUtils.join(localRunnerArgs, " ");
      log.debug("~int.invokeApPredict() : Command line '" + invocation + "'.");
      // TODO : Remove System.out.println.
      System.out.println("Command line :" + invocation);
    } else if (AppIdentifiers.OS_NAME.equals(AppIdentifiers.WINDOWS_XP)) {
      final String errorMessage = "Sorry! Cannot run Chaste (or derivative) on '" + AppIdentifiers.OS_NAME + "' systems!";
      log.fatal("~int.invokeChaste() : " + errorMessage);
    } else {
      final String errorMessage = "Unrecognised os.name property of '" + AppIdentifiers.OS_NAME + "'";
      log.fatal("~int.invokeChaste() : " + errorMessage);
    }

    return invoke(localRunnerArgs);
  }

  /**
   * Assign the base directory, e.g. /home/me/run_here/
   * 
   * @param baseDir Base directory.
   */
  // Spring-injected.
  @Value("${base.dir}")
  public void setBaseDir(final String baseDir) {
    this.baseDir = baseDir;
  }
}