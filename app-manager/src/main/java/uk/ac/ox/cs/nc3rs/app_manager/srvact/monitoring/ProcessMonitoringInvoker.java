/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.monitoring;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.business.ProcessMonitor;
import uk.ac.ox.cs.nc3rs.app_manager.business.ProgressMonitor;
import uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO;
import uk.ac.ox.cs.nc3rs.app_manager.domain.RunStatus;
import uk.ac.ox.cs.nc3rs.app_manager.srvact.systemprocess.ProcessDataFileManager;
import uk.ac.ox.cs.nc3rs.app_manager.util.FileUtil;

/**
 * Kick off the monitoring of system processes.
 *
 * @author Geoff Williams
 */
@Component
public class ProcessMonitoringInvoker implements ApplicationContextAware {

  private ApplicationContext applicationContext;

  @Autowired @Qualifier(AppIdentifiers.COMPONENT_JDBC_APP_MANAGER_DAO)
  private AppManagerDAO appManagerDAO;

  private String baseDir;
  private String monitorFrequency;

  private static final String APPREDICT_PROGRESS_FILE_RELATIVE_LOC = "testoutput/ApPredict_output/progress_status.txt";

  private static final Log log = LogFactory.getLog(ProcessMonitoringInvoker.class);

  /**
   * Kick off the monitoring of the system process and the ApPredict progress output.
   * 
   * @param processData Internal representation of process data file contents.
   * @return Text for stdout.
   */
  @ServiceActivator
  public String activateProcessMonitoring(final Map<String, String> processData) {
    log.debug("~int.activateProcessMonitoring() : Invoked.");
    log.trace("~int.activateProcessMonitoring() : Thread '" + Thread.currentThread().getName() + "'.");

    final String dataAppManagerId = processData.get(ProcessDataFileManager.PROCESS_INFO_APP_MANAGER_ID);
    final int appManagerId = new Integer(dataAppManagerId).intValue();
    final String processId = processData.get(ProcessDataFileManager.PROCESS_INFO_PROCESS_ID);

    final ProcessMonitor processMonitor = this.applicationContext.getBean(AppIdentifiers.COMPONENT_PROCESS_MONITOR,
                                                                          ProcessMonitor.class);
    processMonitor.setProcessId(processId);
    processMonitor.setAppManagerId(appManagerId);
    processMonitor.setSchedule(monitorFrequency);

    final String processMessage = "Process monitoring initiated";
    final RunStatus processStatusVO = new RunStatus(appManagerId, null, RunStatus.DEBUG_PREFIX,
                                                  processMessage);
    appManagerDAO.addStatus(processStatusVO);

    final String jobDirectory = FileUtil.retrieveJobDirectory(baseDir, appManagerId);
    final String progressFileName = jobDirectory.concat(APPREDICT_PROGRESS_FILE_RELATIVE_LOC);

    final ProgressMonitor progressMonitor = this.applicationContext.getBean(AppIdentifiers.COMPONENT_PROGRESS_MONITOR,
                                                                            ProgressMonitor.class);
    progressMonitor.setAppManagerId(appManagerId);
    progressMonitor.setProgressFileName(progressFileName);
    progressMonitor.setSchedule(monitorFrequency);

    final String progressMessage = "Progress monitoring initiated";
    final RunStatus progressStatusVO = new RunStatus(appManagerId, null, RunStatus.DEBUG_PREFIX,
                                                   progressMessage);
    appManagerDAO.addStatus(progressStatusVO);

    return "Process/Progress monitoring activated!\n";
  }

  /* (non-Javadoc)
   * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
   */
  @Override
  public void setApplicationContext(final ApplicationContext applicationContext)
                                    throws BeansException {
    this.applicationContext = applicationContext;
  }

  /**
   * Assign the baseDirectory, e.g. /home/me/run_here/
   * 
   * @param baseDir baseDirectory.
   */
  // Spring-injected.
  @Value("${base.dir}")
  public void setBaseDir(final String baseDir) {
    this.baseDir = baseDir;
  }

  /**
   * Assign the monitor frequency, e.g. 15 (seconds).
   * 
   * @param monitorFrequency Monitor frequency (in seconds).
   */
  @Value("${monitor.frequency}")
  public void setMonitorFrequency(final String monitorFrequency) {
    this.monitorFrequency = monitorFrequency;
  }
}