/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the names of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.value.type;

import java.util.Arrays;

/**
 * Component which generated the {@code app-manager} invocation request.
 *
 * @author geoff
 */
public enum InvocationSource {
  // These values are most likely defined in app_manager.xsd
  BUSINESS_MANAGER(new String[] { "business-manager" }),
  CLIENT_DIRECT(new String[] { "client-direct" }),
  OTHER();

  private final String[] alternatives;

  /**
   * Default constructor.
   * 
   * Initialises the invocation source with an empty array of alternative names.
   */
  InvocationSource() {
    alternatives = new String[] {};
  }

  /**
   * Initialising constructor.
   * 
   * @param alternatives Alternatives by which the enumerated value is refered to.
   */
  InvocationSource(final String[] alternatives) {
    this.alternatives = alternatives;
  }

  /**
   * Retrieve the enum value corresponding to the alternative, defaulting to the
   * default value of {@code InvocationSource#OTHER} if not found.
   * 
   * @param alternative Alternative to look for.
   * @return Corresponding enum if found, otherwise {@literal InvocationSource#OTHER}. 
   */
  public static InvocationSource findEnum(final String alternative) {
    InvocationSource foundInvocationSource = OTHER;

    for (final InvocationSource invocationSource : InvocationSource.values()) {
      if (invocationSource.inAlternatives(alternative)) {
        foundInvocationSource = invocationSource;
        break;
      }
    }

    return foundInvocationSource;
  }

  /**
   * Is the specified string an alternative for the invocation source.
   * 
   * @param alternative Alternative to check for.
   * @return {@code true} if an alternative, otherwise {@code false}.
   */
  public boolean inAlternatives(final String alternative) {
    if (alternative == null) {
      return false;
    }
    return (Arrays.asList(this.alternatives).contains(alternative));
  }

  /**
   * Retrieve the alternatives for the enumerated value.
   * 
   * @return Alternatives for the enumerated value, empty array if none available.
   */
  public String[] getAlternatives() {
    return this.alternatives;
  }
}