/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.responder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO;
import uk.ac.ox.cs.nc3rs.app_manager.domain.PKResult;
import uk.ac.ox.cs.nc3rs.app_manager.domain.SimulationResult;
import uk.ac.ox.cs.nc3rs.app_manager.value.AllSimulationDomainResultsVO;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.ObjectFactory;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.PKResults;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.Results;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.RetrieveAllResultsRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.RetrieveAllResultsResponse;

/**
 * Retrieve the simulation results for a particular app manager identifier.
 *
 * @author Geoff Williams
 */
@Component
public class ResultsResponder {

  @Autowired @Qualifier(AppIdentifiers.COMPONENT_JDBC_APP_MANAGER_DAO)
  private AppManagerDAO appManagerDAO;

  private final ObjectFactory objectFactory = new ObjectFactory();

  private static final Log log = LogFactory.getLog(ResultsResponder.class);

  /**
   * Query for simulation results for an app manager id.
   * 
   * @param retrieveAllResultsRequest SOAP request.
   * @return SOAP response.
   */
  @ServiceActivator
  public RetrieveAllResultsResponse handleRequest(final RetrieveAllResultsRequest retrieveAllResultsRequest) {
    log.debug("~int.handleRequest() : Invoked.");

    final long appManagerId = retrieveAllResultsRequest.getAppManagerId();

    final RetrieveAllResultsResponse retrieveAllResultsResponse = objectFactory.createRetrieveAllResultsResponse();
    retrieveAllResultsResponse.setAppManagerId(appManagerId);

    final String messages = appManagerDAO.retrieveSimulationMessages(appManagerId);
    if (messages != null) {
      retrieveAllResultsResponse.setMessages(messages);
    }

    final AllSimulationDomainResultsVO allSimulationResults = appManagerDAO.retrieveAllSimulationResults(appManagerId);

    retrieveAllResultsResponse.setDeltaAPD90PercentileNames(allSimulationResults.getDeltaAPDPercentileNames());

    for (final SimulationResult simulationResult : allSimulationResults.getSimulationResults()) {
      final Results results = objectFactory.createResults();

      results.setReference(simulationResult.getCmpdConc());
      results.setTimes(simulationResult.getTimes());
      results.setVoltages(simulationResult.getVoltages());
      results.setAPD90(simulationResult.getAPD90());
      results.setDeltaAPD90(simulationResult.getDeltaAPD90());
      results.setQNet(simulationResult.getQNet());

      retrieveAllResultsResponse.getResults().add(results);
    }

    for (final PKResult pkResult : allSimulationResults.getPkResults()) {
      final PKResults pkResults = objectFactory.createPKResults();

      pkResults.setTimepoint(pkResult.getTimepoint());
      pkResults.setAPD90S(pkResult.getApd90s());

      retrieveAllResultsResponse.getPKResults().add(pkResults);
    }


    return retrieveAllResultsResponse;
  }
} 