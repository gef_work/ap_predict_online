/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.postrun.reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.app_manager.util.FileUtil;
import uk.ac.ox.cs.nc3rs.app_manager.value.SimulationResultsVO;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.SetProcessStatusRequest;

/**
 * Persist the simulation results after the PK simulation has finished.
 *
 * @author Geoff Williams
 */
@Component
public class ResultsPKReader extends AbstractRunResultsReader {

  private static final String fileNamePKResults = "pkpd_results.txt";

  private static final Log log = LogFactory.getLog(ResultsPKReader.class);

  /**
   * Persist the PK simulation run results from the filesystem files.
   * 
   * @param setProcessStatusRequest Incoming SOAP status request.
   * @return (Unmodified) Incoming SOAP status request.
   */
  @ServiceActivator
  public SetProcessStatusRequest resultsPKReader(final SetProcessStatusRequest setProcessStatusRequest) {
    final long appManagerId = setProcessStatusRequest.getAppManagerId();
    final String logPrefix = "~int.resultsPKReader() : [" + appManagerId + "] : ";
    log.debug(logPrefix.concat("Invoked."));

    final String resultFilesDir = retrieveResultFilesDir(appManagerId);
    final List<SimulationResultsVO> simulationResults = new ArrayList<SimulationResultsVO>();
    final String deltaAPD90PercentileNames = generalDataLoading(appManagerId,
                                                                resultFilesDir,
                                                                simulationResults);

    /*
     * Read in the pkpd_results.txt
     */
    final String pkResultsFilePath = resultFilesDir.concat(fileNamePKResults);
    final File pkResultsFile = new File(pkResultsFilePath);

    final List<String> pkResultsLines = new ArrayList<String>();
    try {
      pkResultsLines.addAll(FileUtil.fileLineReader(pkResultsFile, true, true,
                                                    true));
    } catch (FileNotFoundException e) {
      final String errorMessage = "Failed to read PK file '" + pkResultsFilePath + "'";
      log.error(logPrefix.concat(errorMessage));
      throw new UnsupportedOperationException(errorMessage);
    }

    final Map<String, String> pkResultsValues = new HashMap<String, String>();
    for (final String pkResultsLine : pkResultsLines) {
      String timepoint = null;
      final List<String> apd90s = new ArrayList<String>();

      for (final String pkResultsColumn : pkResultsLine.split(tab)) {
        if (timepoint == null) {
          // First column is the timepoint.
          timepoint = pkResultsColumn;
        } else {
          // Remaining columns are APD90s.
          apd90s.add(pkResultsColumn);
        }
      }

      pkResultsValues.put(timepoint, StringUtils.join(apd90s, ","));
    }

    log.trace(logPrefix.concat("PK '" + pkResultsValues.toString() + "'."));

    getAppManagerDAO().saveSimulationResults(appManagerId, simulationResults,
                                             pkResultsValues,
                                             deltaAPD90PercentileNames);

    return setProcessStatusRequest;
  }
}