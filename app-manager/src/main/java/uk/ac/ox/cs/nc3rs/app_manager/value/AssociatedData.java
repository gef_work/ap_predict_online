/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.value;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Structure to enable consistent relationships between (p)IC50 values and their associated
 * (non-optional) Hill Coefficient values.<br>
 * One of pIC50 or IC50 will be set, not both or neither. Default is to use pIC50 if supplied and
 * discard IC50 if also assigned.
 *
 * @author geoff
 */
public class AssociatedData implements Serializable {

  private static final long serialVersionUID = -1441474652545631520L;

  private final BigDecimal pIC50;
  private final BigDecimal IC50;
  private final BigDecimal hill;
  private final BigDecimal saturation;

  /**
   * Initialising constructor.
   * <p>
   * A pIC50 or an IC50 value must be provided. If both are provided then the pIC50 value will take
   * precedence and the IC50 value will be discarded.
   * 
   * @param pIC50 pIC50 value.
   * @param IC50 IC50 value.
   * @param hill Hill coefficient, or {@code null} if not available.
   * @param saturation Saturation Level, or {@code null} if not available.
   * @throws IllegalArgumentException if neither a pIC50 or an IC50 value is provided.
   */
  public AssociatedData(final BigDecimal pIC50, final BigDecimal IC50, final BigDecimal hill,
                        final BigDecimal saturation) {
    if (pIC50 == null && IC50 == null) {
      throw new IllegalArgumentException("Either a pIC50 or an IC50 must be assigned to AssociatedData");
    }

    if (pIC50 != null) {
      this.pIC50 = pIC50;
      this.IC50 = null;
    } else {
      this.pIC50 = null;
      this.IC50 = IC50;
    }
    this.hill = hill;
    this.saturation = saturation;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "AssociatedData [pIC50=" + pIC50 + ", IC50=" + IC50 + ", hill=" + hill + ", saturation=" + saturation + "]";
  }

  /**
   * Retrieve the pIC50 value.
   * 
   * @return pIC50 value if assigned, otherwise {@code null}.
   */
  
  public BigDecimal getpIC50() {
    return pIC50;
  }

  /**
   * Retrieve the IC50 value.
   * 
   * @return IC50 value if assigned, otherwise {@code null}
   */
  public BigDecimal getIC50() {
    return IC50;
  }

  /** 
   * Retrieve the Hill coefficient.
   * 
   * @return Hill coefficient, or {@code null} if not assigned.
   */
  public BigDecimal getHill() {
    return hill;
  }

  /** 
   * Retrieve the saturation level.
   * 
   * @return Saturation level, or {@code null} if not assigned.
   */
  public BigDecimal getSaturation() {
    return saturation;
  }
}