/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the names of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import uk.ac.ox.cs.nc3rs.app_manager.domain.RunStatus;
import uk.ac.ox.cs.nc3rs.app_manager.value.AllSimulationDomainResultsVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.ApPredictVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.DiagnosticsVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.SimulationInfoVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.SimulationResultsVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.type.InvocationSource;
import uk.ac.ox.cs.nc3rs.app_manager.value.type.SimulationType;

/**
 * Interface to App Manager transactions.
 *
 * @author Geoff Williams
 */
public interface AppManagerDAO {

  /** Universal timestamp format. */
  public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");

  /**
   * Add a status record.
   * 
   * @param runStatus Status data to add.
   */
  public void addStatus(RunStatus runStatus);

  /**
   * Create an App manager record for a simulation run.
   * 
   * @param invocationObject Object for invocation.
   * @param invocationSource Invocation source, e.g. {@linkplain InvocationSource#CLIENT_DIRECT}.
   * @param simulationType Simulation type, e.g. {@linkplain SimulationType#PK}.
   * @return App manager identifier of new record.
   */
  public Long createManagerRecord(Object invocationObject, InvocationSource invocationSource,
                                  SimulationType simulationType);

  /**
   * Remove all evidence of a previous simulation from persistence.
   * 
   * @param appManagerId Application manager identifier.
   */
  public void purgeExistingSimulation(long appManagerId);

  /**
   * Retrieve the latest status data to arrive.
   * 
   * @param appManagerId App manager identifier.
   * @return Latest status data.
   */
  public RunStatus retrieveLatestStatus(long appManagerId);

  /**
   * Retrieve the originally invoked ApPredict object.
   * 
   * @param appManagerId App manager identifier.
   * @return Original ApPredict object, or {@code null} if not found.
   */
  public ApPredictVO retrieveObjectValue(long appManagerId);

  /**
   * Retrieve the diagnostics data.
   * 
   * @param appManagerId Application manager identifier.
   * @return Diagnostics data, or {@code null} if no data found.
   */
  public DiagnosticsVO retrieveDiagnostics(long appManagerId);

  /**
   * Retrieve the simulation results (e.g. the per-concentration results) for a
   * particular app manager id.
   * 
   * @param appManagerId App manager identifier.
   * @return All simulation results.
   */
  public AllSimulationDomainResultsVO retrieveAllSimulationResults(long appManagerId);

  /**
   * Retrieve all status data.
   * 
   * @param appManagerId App manager identifier.
   * @return All status data.
   */
  public List<RunStatus> retrieveAllStatuses(long appManagerId);

  /**
   * Retrieve simulation information.
   * 
   * @param appManagerId App manager identifier.
   * @return Simulation information (or {@code null} if none available).
   */
  public SimulationInfoVO retrieveSimulationInfo(long appManagerId);

  /**
   * Retrieve the messages generated by ApPredict.
   * <p>
   * Such messages are usually file-derived messages indicating failure to complete simulation 
   * normally, e.g. when concentrations cause a cell not to depolarize. 
   * 
   * @param appManagerId App manager identifier.
   * @return ApPredict generated messages.
   */
  public String retrieveSimulationMessages(long appManagerId);

  /**
   * Retrieve the process id which the simulation is running under.
   * 
   * @param appManagerId App manager identifier.
   * @return Process identifier (or {@code null} if no record identified by appManagerId).
   */
  public String retrieveSimulationProcessId(long appManagerId);

  /**
   * Persist the diagnostics information.
   * 
   * @param appManagerId App Manager identifier.
   * @param info Diagnostic information, e.g. how the app was invoked.
   * @param output Diagnostic output, e.g. output generated by process. 
   */
  public void saveSimulationDiagnostics(long appManagerId, String info, String output);

  /**
   * Save simulation messages.
   * 
   * @param appManagerId App manager identifier.
   * @param messages Simulation messages.
   */
  public void saveSimulationMessages(long appManagerId, String messages);

  /**
   * Save the simulation process identifier.
   * 
   * @param appManagerId App manager identifier.
   * @param processId Process identifier.
   */
  public void saveSimulationProcessId(long appManagerId, String processId);

  /**
   * Save the simulation results.
   * 
   * @param appManagerId App Manager identifier.
   * @param simulationResults Simulation results.
   * @param pkResults Optional PKPD results.
   * @param deltaAPD90PercentileNames CSV-format names of DeltaAPD90 percentile values.
   */
  public void saveSimulationResults(long appManagerId,
                                    List<SimulationResultsVO> simulationResults,
                                    Map<String, String> pkResults,
                                    String deltaAPD90PercentileNames);
}