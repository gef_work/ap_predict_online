/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager;

/**
 * System-wide component identifiers.
 *
 * @author Geoff Williams
 */
public class AppIdentifiers {

  public static final String OS_NAME = System.getProperty("os.name");
  public static final String FILE_SEPARATOR = System.getProperty("file.separator");

  public static final String LINUX = "Linux";
  public static final String WINDOWS_XP = "Windows XP";

  public static final String AP_PREDICT_OUTPUT_BASE = "testoutput/ApPredict_output/";

  /** Spring Integration message header for app manager identifier */
  public static final String SI_HDR_APP_MANAGER_ID = "app_manager_id";
  /** Spring Integration message header for ApPredict value object */
  public static final String SI_HDR_APPREDICT_VO = "ap_predict_vo";
  /** Spring Integration message header for process status */
  public static final String SI_HDR_PROCESS_STATUS = "process_status";
  /** Spring Integration message header for simulation info value object */
  public static final String SI_HDR_SIMULATION_INFO = "simulation_info";

  /** Component name for the App Manager WS WSS4J security interceptor
      <p>
      See also (sample.)appCtx.ws.security-outgoing.xml */
  public static final String COMPONENT_APP_MANAGER_SERVICES_INTERCEPTOR = "wsAppManagerServicesInterceptor";
  /** Component name for the App Manager WS WSS4J incoming gateway security interceptor
      <p>
      See also (sample.)appCtx.ws.security-incoming.xml */
  public static final String COMPONENT_APP_MANAGER_INBOUND_GTWY_INTERCEPTOR = "wsInboundGatewayInterceptor";
  /** Component name for app services proxy */
  public static final String COMPONENT_APP_SERVICES_PROXY = "appServicesProxy";
  /** Component name for the JDBC app manager DAO */
  public static final String COMPONENT_JDBC_APP_MANAGER_DAO = "appManagerDAO";
  /** Component name for the JDBC app template */
  public static final String COMPONENT_JDBC_APP_TEMPLATE = "appTemplate";
  /** Component name for process monitor */
  public static final String COMPONENT_PROCESS_MONITOR = "processMonitor";
  /** Component name for progress monitor */
  public static final String COMPONENT_PROGRESS_MONITOR = "progressMonitor";
  /** Component - Web service inbound gateway */
  public static final String COMPONENT_WS_INBOUND_GATEWAY = "wsInboundGateway";

  // Enforce noninstantiability with a private constructor
  private AppIdentifiers() {}
}