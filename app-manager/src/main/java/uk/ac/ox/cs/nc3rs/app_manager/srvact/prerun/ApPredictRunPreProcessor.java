/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.prerun;

import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.annotation.Header;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.util.FileUtil;
import uk.ac.ox.cs.nc3rs.app_manager.value.ApPredictVO;

/**
 * Do some pre-run preparation.
 *
 * @author Geoff Williams
 */
@Component
public class ApPredictRunPreProcessor extends GenericPreProcessor {

  private static final String prepareScript = "./prepare.sh";

  private String baseDir;

  private static final Log log = LogFactory.getLog(ApPredictRunPreProcessor.class);

  /**
   * Prepare the job directory via a local script run.
   * 
   * @param appManagerId App Manager identifier.
   * @param apPredictVO ApPredict object to run.
   * @return Ignore.
   */
  @ServiceActivator
  public String preProcessApPredict(final @Header(AppIdentifiers.SI_HDR_APP_MANAGER_ID)
                                          int appManagerId,
                                    final @Header(AppIdentifiers.SI_HDR_APPREDICT_VO)
                                          ApPredictVO apPredictVO) {
    log.debug("~int.preProcessApPredict() : Invoked");

    // First create the run environment
    final String jobDirectory = FileUtil.retrieveJobDirectory(baseDir,
                                                              appManagerId);

    final String[] preparerArgs = new String[] { "", "", "", "", "" };

    if (AppIdentifiers.OS_NAME.equals(AppIdentifiers.LINUX)) {
      preparerArgs[0] = prepareScript;
      preparerArgs[1] = jobDirectory;
      if (apPredictVO.hasCellMLData()) {
        /* Extra args needed to allow the script to symlink an absolute path
           for ApPredict to read CellML file. */
        preparerArgs[2] = apPredictVO.getCellMLFileName();
        preparerArgs[3] = apPredictVO.getCellMLFileLocation();
      }
      if (apPredictVO.isPKSimulation()) {
        /* Absolute path to PKPD file being symlinked to hardcoded file name. */
        preparerArgs[4] = apPredictVO.getPKFileLocation();
      }
    } else if (AppIdentifiers.OS_NAME.equals(AppIdentifiers.WINDOWS_XP)) {
      final String errorMessage = "Sorry! Cannot run scripts on '" + AppIdentifiers.OS_NAME + "' systems!";
      log.fatal("~int.preProcessApPredict() : " + errorMessage);
      throw new UnsupportedOperationException(errorMessage);
    } else {
      final String errorMessage = "Unrecognised os.name property of '" + AppIdentifiers.OS_NAME + "'";
      log.fatal("~int.preProcessApPredict() : " + errorMessage);
      throw new UnsupportedOperationException(errorMessage);
    }

    log.debug("~int.preProcessApPredict() : '" + Arrays.asList(preparerArgs) + "'.");
    final Runtime runtime = Runtime.getRuntime();
    try {
      log.debug("~int.preProcessApPredict() : Pre-execution of prepare.sh");
      final Process process = runtime.exec(preparerArgs);
      process.waitFor();
      log.debug("~int.preProcessApPredict() : Post-completion of prepare.sh");
    } catch (IOException ioe) {
      ioe.printStackTrace();
      final String errorMessage = "Preparing for ApPredict run using '" + prepareScript + "' failed with '" + ioe.getMessage() + "'";
      log.error("~int.preProcessApPredict() : " + errorMessage);
      throw new UnsupportedOperationException(errorMessage);
    } catch (InterruptedException e) {
      e.printStackTrace();
      final String errorMessage = "Preparing for ApPredict run using '" + prepareScript + "' failed with '" + e.getMessage() + "'";
      log.error("~int.preProcessApPredict() : " + errorMessage);
      throw new UnsupportedOperationException(errorMessage);
    }

    return "PreProcessed";
  }

  /**
   * Assign the base directory, e.g. /home/me/run_here/
   * 
   * @param baseDir Base directory.
   */
  // Spring-injected.
  @Value("${base.dir}")
  public void setBaseDir(final String baseDir) {
    this.baseDir = baseDir;
  }
}