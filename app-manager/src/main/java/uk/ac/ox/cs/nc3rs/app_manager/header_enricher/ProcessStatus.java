/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.header_enricher;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO;
import uk.ac.ox.cs.nc3rs.app_manager.domain.RunStatus;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.SetProcessStatusRequest;

/**
 *
 *
 * @author Geoff Williams
 */
@Component
public class ProcessStatus {

  @Autowired @Qualifier(AppIdentifiers.COMPONENT_JDBC_APP_MANAGER_DAO)
  private AppManagerDAO appManagerDAO;

  private static final Log log = LogFactory.getLog(ProcessStatus.class);

  public String setProcessStatusHeader(final SetProcessStatusRequest setProcessStatusRequest) {
    log.debug("~int.setProcessStatusHeader() : Invoked.");
    log.debug("~int.setProcessStatusHeader() : Thread '" + Thread.currentThread().getName() + "'.");

    final String statusCode = setProcessStatusRequest.getStatusCode();
    final long appManagerId = setProcessStatusRequest.getAppManagerId();
    final String status = setProcessStatusRequest.getStatus();

    assert statusCode != null && statusCode.length() > 0 : "Received an invalid instruction to set process status using statusCode of '" + statusCode + "'";
    final String level = statusCode.substring(0, 1);
    log.debug("~int.setProcessStatusHeader() : Deriving status level of '" + level + "' from incoming process status code '" + statusCode + "'");
    final RunStatus runStatus = new RunStatus(appManagerId, null, level, status);
    appManagerDAO.addStatus(runStatus);

    return statusCode;
  }
}