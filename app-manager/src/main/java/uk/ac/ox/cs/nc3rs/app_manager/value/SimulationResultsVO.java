/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.value;

/**
 * Value Object for simulation results data (destined to be returned to the
 * calling component).
 * <p>
 * This data is usually derived from {@code voltage_results.dat} data (and the 
 * corresponding {@code VoltageResultsVO} content).
 * 
 * @author Geoff Williams
 */
public class SimulationResultsVO {
  private final String cmpdConc;
  private final String times;
  private final String voltages;
  private final String apd90;
  private final String deltaAPD90;
  private final String qNet;

  /**
   * Initialising constructor.
   * 
   * @param cmpdConc The compound concentration value (in µM).
   * @param times Action potential times.
   * @param voltages Action potential voltages.
   * @param apd90 APD90.
   * @param deltaAPD90 Delta APD90.
   * @param qNet qNet data.
   */
  public SimulationResultsVO(final String cmpdConc, final String times,
                             final String voltages, final String apd90,
                             final String deltaAPD90, final String qNet) {
    this.cmpdConc = cmpdConc;
    this.times = times;
    this.voltages = voltages;
    this.apd90 = apd90;
    this.deltaAPD90 = deltaAPD90;
    this.qNet = qNet;
  }

  /**
   * Retrieve the compound concentration value.
   * 
   * @return Compound concentration value (in µM).
   */
  public String getCmpdConc() {
    return cmpdConc;
  }

  /**
   * Retrieve the voltage trace times.
   * 
   * @return Voltage trace times.
   */
  public String getTimes() {
    return times;
  }

  /**
   * Retrieve the voltage trace voltages.
   * 
   * @return Voltage trace voltages.
   */
  public String getVoltages() {
    return voltages;
  }

  /**
   * Retrieve the APD90 value.
   *  
   * @return The APD90 value.
   */
  public String getAPD90() {
    return apd90;
  }
  /**
   * Retrieve the Delta APD90 value.
   *  
   * @return The delta APD90 value.
   */
  public String getDeltaAPD90() {
    return deltaAPD90;
  }

  /**
   * Retrieve the qNet value.
   * 
   * @return The qNet value (or {@code null} if not provided).
   */
  public String getQNet() {
    return qNet;
  }
}