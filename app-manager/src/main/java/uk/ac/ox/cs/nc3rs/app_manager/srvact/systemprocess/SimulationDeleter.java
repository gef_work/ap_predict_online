/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.systemprocess;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.business.Running;
import uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO;
import uk.ac.ox.cs.nc3rs.app_manager.exception.MessageKeys;
import uk.ac.ox.cs.nc3rs.app_manager.srvact.postrun.PostSimulationRunTidyUp;
import uk.ac.ox.cs.nc3rs.app_manager.util.SystemUtil;
import uk.ac.ox.cs.nc3rs.app_manager.util.SystemUtil.INSTRUCTION;
import uk.ac.ox.cs.nc3rs.app_manager.value.ActionOutcomeVO;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.ObjectFactory;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.SetProcessStatusRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.SimulationDeleteRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.SimulationDeleteResponse;

/**
 * Service activator to handle the deletion of a simulation, including :
 * <ul>
 *   <li>Killing of process</li>
 *   <li>Removal of persisted data</li>
 * </ul>
 * 
 * @author geoff
 */
@Component
public class SimulationDeleter {

  @Autowired @Qualifier(AppIdentifiers.COMPONENT_JDBC_APP_MANAGER_DAO)
  private AppManagerDAO appManagerDAO;

  @Autowired @Qualifier("postSimulationRunTidyUp")
  private PostSimulationRunTidyUp postSimulationRunTidyUp;

  private static final ObjectFactory objectFactory = new ObjectFactory();

  private static final Log log = LogFactory.getLog(SimulationDeleter.class);

  @ServiceActivator
  public SimulationDeleteResponse simulationDelete(final SimulationDeleteRequest simulationDeleteRequest) {
    log.debug("~int.simulationDelete() : Invoked.");

    final long appManagerId = new Long(simulationDeleteRequest.getAppManagerId()).longValue();

    final String processId = appManagerDAO.retrieveSimulationProcessId(appManagerId);
    log.debug("~int.simulationDelete() : Process id '" + processId + "' retrieved for '" + appManagerId + "'.");

    String message = "";
    if (processId != null) {
      final ActionOutcomeVO outcome = SystemUtil.makeSystemCall(processId, INSTRUCTION.DELETE);
      message = outcome.getInformation();
      if (outcome.isSuccess()) {
        // This doesn't kill the monitor!
        appManagerDAO.purgeExistingSimulation(appManagerId);
        Running.removeFromRunning(appManagerId);
        final SetProcessStatusRequest setProcessStatusRequest = objectFactory.createSetProcessStatusRequest();
        setProcessStatusRequest.setAppManagerId(appManagerId);

        // This is normally invoked via spring integration after a successful simulation run.
        postSimulationRunTidyUp.postSimulationRunTidyUp(setProcessStatusRequest);
      }
    } else {
      message = MessageKeys.DELETE_NOT_FOUND;
      log.warn("~int.simulationDelete() : " + message);
    }

    final SimulationDeleteResponse simulationDeleteResponse = objectFactory.createSimulationDeleteResponse();
    simulationDeleteResponse.setResponse(message);

    return simulationDeleteResponse;
  }
}