/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the names of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.value;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.app_manager.value.type.ic.ICType;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.ApPredictRunRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.AssociatedItem;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.ChannelData;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.TypeCredibleIntervals;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.TypePlasmaConcsHiLo;

/**
 * Value object for an ApPredict run.
 *
 * @author Geoff Williams
 */
public class ApPredictVO implements Serializable {

  /**
   * Data error - No data structure (neither IC50 nor pIC50) supplied.
   */
  public static final String ERROR_STATE_1 = "Ion channel data must be accompanied by either IC50 or pIC50 data";
  /**
   * Data error - Both pIC50 data and IC50 data structures supplied.
   */
  public static final String ERROR_STATE_2 = "Both pIC50 data and IC50 data cannot be supplied for an ion channel";
  /**
   * Data error - An undeclared, empty or invalid IC50 or pIC50 value supplied.
   * <p>
   * <b>Invalid</b>: {@code --ic50-cal -0.1 --hill-cal 0.76 --saturation-cal 48.3}<br>
   * <b>Invalid</b>: {@code --hill-cal 0.76 --saturation-cal 48.3}
   */
  public static final String ERROR_STATE_3 = "An invalid (perhaps undeclared) pIC50 or IC50 value was encountered";
  /**
   * Data error - An invalid (perhaps undeclared) Hill value supplied. In the 
   * second example below the {@code null} Hill value is only allowed when
   * there is no Hill spread value, because {@code null} Hill values are
   * deemed to represent estimated default-valued Hills which should not have
   * spread calculated.
   * <p>
   * <b>Invalid</b>: {@code --pic50-cal 4.5 --hill-cal 0}<br>
   * <b>Invalid</b>: {@code --ic50-cal 1001.34 --pic50-spread-cal 0.32 --hill-spread-cal 0.32 --saturation-cal 48.3 --credible-intervals}
   */
  public static final String ERROR_STATE_4 = "An invalid (perhaps undeclared) Hill value was encountered";
  /**
   * Data error - An invalid (perhaps undeclared) saturation value supplied.
   * <p>
   * <b>Invalid</b>: {@code --pic50-cal 4.5 --hill-cal 0.76 --saturation-cal}<br>
   * <b>Invalid</b>: {@code --pic50-cal 4.5 --hill-cal 0.76 --saturation-cal -0.00001}
   */
  public static final String ERROR_STATE_5 = "An invalid (perhaps undeclared) saturation value was encountered";
  /**
   * Data error - An invalid (perhaps undeclared) model identifier value supplied.
   */
  public static final String ERROR_STATE_6 = "An invalid (perhaps undeclared) model identifier value was encountered";
  /**
   * Data error - An inconsistent Hill declaration between channels supplied.
   * <p>
   * <b>Invalid</b>: {@code --pic50-cal 4.5 --hill-cal 0.76 --saturation-cal 48.3 --pic50-herg 4.5 --saturation-herg 48.3}
   */
  public static final String ERROR_STATE_7 = "An inconsistent Hill declaration between channels was encountered";

  /**
   * Confidence intervals error - (p)IC50 value + (p)IC50 spread + Hill value
   * provided, but no Hill spread.
   * <p>
   * <b>Invalid</b>: {@code --pic50-cal 4.5 --pic50-spread-cal 0.32 --hill-cal 0.76 --credible-intervals}.
   */
  //public static final String ERROR_STATE_CI_1 = "If a p(IC50) value with spread is provided, any corresponding Hill value must also have a spread";
  /**
   * Confidence intervals error - Credible intervals requested, but none
   * provided for an ion channel.
   * <p>
   * <b>Invalid</b>: {@code --ic50-cal 1001.34 --hill-cal 0.76 --credible-intervals}.
   */
  public static final String ERROR_STATE_CI_2 = "Credible intervals requested, but none provided for ion channel";
  /**
   * Confidence intervals error - (p)IC50 value + Hill value + Hill spread
   * provided, but no (p)IC50 spread provided.
   * <p>
   * <b>Invalid</b>: {@code --ic50-cal 1001.34 --hill-cal 0.76 --hill-spread-cal 0.32 --credible-intervals}.
   */
  public static final String ERROR_STATE_CI_3 = "If a Hill value has a spread, a (p)IC50 value must also have a spread";
  /**
   * Confidence intervals error - Credible intervals not requested but spread
   * values supplied.
   * <p>
   * <b>Invalid</b>: {@code --hill-spread-cal 0.32}.<br>
   * <b>Invalid</b>: {@code --pic50-cal 4.5 --pic50-spread-cal 0.32 --hill-spread-cal 0.32}
   */
  public static final String ERROR_STATE_CI_4 = "Credible intervals not requested but spread values supplied";

  /**
   * Confidence intervals error - Credible interval calculation requested but no
   * percentiles specified.
   * <p>
   * Prior to {@code ApPredict} being able to handle credible interval
   * percentile values (i.e. defaulting to the 95% pctile when 
   * {@code --credible-intervals} was specified on its own), we're now
   * insisting that the percentile values are supplied.
   * </p>
   * 
   * <b>Invalid</b>: {@code --credible-intervals}
   */
  public static final String ERROR_STATE_CI_5 = "Credible intervals calculations requested but percentiles not specified";

  public static final String UTF8_NAME = StandardCharsets.UTF_8.name();

  private static final long serialVersionUID = -1424767549035172670L;

  /**
   * Ion channel currents handled by ApPredict.
   */
  public enum CHANNEL_CURRENT {
    ICaL("cal"),
    IKr("herg"),
    IK1("ik1"),
    IKs("iks"),
    Ito("ito"),
    INa("na"),
    INaL("nal");
    private final String invocationArg;

    CHANNEL_CURRENT(final String invocationArg) {
      this.invocationArg = invocationArg;
    }

    public String getInvocationArg() {
      return invocationArg;
    }
  }

  private final RequestChannelData data_ICaL;
  private final RequestChannelData data_IKr;
  private final RequestChannelData data_IK1;
  private final RequestChannelData data_IKs;
  private final RequestChannelData data_Ito;
  private final RequestChannelData data_INa;
  private final RequestChannelData data_INaL;
  private final boolean credibleIntervals;
  private final String credibleIntervalPercentiles;
  private final Short modelIdentifier;
  private final BigDecimal pacingFreq;
  private final BigDecimal pacingMaxTime;
  private final RequestPlasmaData plasma;
  private final String pkFileLocation;
  private final String cellMLFileName;
  // Contains the absolute path
  private final String cellMLFileLocation;

  private short channelCount = 0;
  private short hillCount = 0;

  private static final Log log = LogFactory.getLog(ApPredictVO.class);

  /**
   * Initialising (and validating) constructor.
   * 
   * @param request Incoming run request.
   * @param pkFileLocation Optional PKPD file location.
   * @param cellMLFile Optional CellML file.
   * @throws IllegalStateException If invalid data is derived from the request.
   */
  public ApPredictVO(final ApPredictRunRequest request,
                     final String pkFileLocation, final File cellMLFile)
                     throws IllegalStateException {
    log.debug("~ApPredictVO() : Invoked.");

    Short useModelIdentifier = null;
    String useCellMLFileName = null;
    String useCellMLFileLocation = null;
    if (cellMLFile == null) {
      // Verify supplied request model identifier
      final short requestModelIdentifier = request.getModelIdentifier();
      if (requestModelIdentifier < 1) {
        log.info("~ApPredictVO() : ".concat(ERROR_STATE_6));
        throw new IllegalStateException(ERROR_STATE_6);
      }
      useModelIdentifier = requestModelIdentifier;
    } else {
      useCellMLFileName = cellMLFile.getName();
      useCellMLFileLocation = cellMLFile.getAbsolutePath();
    }

    modelIdentifier = useModelIdentifier;
    cellMLFileName = useCellMLFileName;
    cellMLFileLocation = useCellMLFileLocation;

    // Optional
    pacingFreq = request.getPacingFreq();
    // Optional
    pacingMaxTime = request.getPacingMaxTime();

    final TypeCredibleIntervals credibleIntervalsType = request.getCredibleIntervals();
    /* Must determine credible intervals presence prior to calling
       extractChannelData(..) (due to isCredibleIntervals() call). */
    if (credibleIntervalsType == null) {
      log.debug("~ApPredictVO() : Assigning a default 'false' value for credible intervals as no value received.");
      credibleIntervals = false;
      credibleIntervalPercentiles = null;
    } else {
      final boolean calculateCredibleIntervals = credibleIntervalsType.getCalculateCredibleIntervals();
      credibleIntervals = calculateCredibleIntervals;
      if (credibleIntervals) {
        final String pctiles = credibleIntervalsType.getPercentiles();
        if (StringUtils.isBlank(pctiles)) {
          log.info("~ApPredictVO() : ".concat(ERROR_STATE_CI_5));
          throw new IllegalStateException(ERROR_STATE_CI_5);
        }
        credibleIntervalPercentiles = pctiles;
      } else {
        credibleIntervalPercentiles = null;
      }
    }

    data_ICaL = extractChannelData(CHANNEL_CURRENT.ICaL, request);
    data_IKr = extractChannelData(CHANNEL_CURRENT.IKr, request);
    data_IK1 = extractChannelData(CHANNEL_CURRENT.IK1, request);
    data_IKs = extractChannelData(CHANNEL_CURRENT.IKs, request);
    data_Ito = extractChannelData(CHANNEL_CURRENT.Ito, request);
    data_INa = extractChannelData(CHANNEL_CURRENT.INa, request);
    data_INaL = extractChannelData(CHANNEL_CURRENT.INaL, request);

    if (pkFileLocation != null) {
      plasma = null;
      this.pkFileLocation = pkFileLocation;
    } else {
      plasma = extractPlasmaData(request);
      this.pkFileLocation = null;
    }
  }

  // Extract and validate the Ion Channel data from the run request.
  private RequestChannelData extractChannelData(final CHANNEL_CURRENT channel,
                                                final ApPredictRunRequest request)
                                                throws IllegalStateException,
                                                       UnsupportedOperationException {
    final String logPrefix = "~extractChannelData() : ";
    log.debug(logPrefix.concat("Invoked."));

    ChannelData channelData = null;
    switch (channel) {
      case ICaL :
        channelData = request.getICaL();
        break;
      case IKr :
        channelData = request.getIKr();
        break;
      case IK1 :
        channelData = request.getIK1();
        break;
      case IKs :
        channelData = request.getIKs();
        break;
      case Ito :
        channelData = request.getIto();
        break;
      case INa :
        channelData = request.getINa();
        break;
      case INaL :
        channelData = request.getINaL();
        break;
      default :
        log.error(logPrefix.concat("Invalid channel encountered '" + channel + "'."));
        break;
    }

    RequestChannelData requestChannelData = null;

    if (channelData != null) {
      channelCount++;

      final List<AssociatedItem> suppliedIC50Data = channelData.getIC50Data();
      final List<AssociatedItem> suppliedPIC50Data = channelData.getPIC50Data();

      final boolean presentIC50 = (suppliedIC50Data == null ||
                                   suppliedIC50Data.isEmpty()) ? false : true;
      final boolean presentPIC50 = (suppliedPIC50Data == null ||
                                    suppliedPIC50Data.isEmpty()) ? false : true;

      /* Ensure a 50% inhibitory concentration value for an ion channel. */
      if (!presentIC50 && !presentPIC50) {
        log.info(logPrefix.concat(ERROR_STATE_1));
        throw new IllegalStateException(ERROR_STATE_1);
      }

      /* Cannot supply both pIC50 and IC50 for an ion channel.
         ApPredict rejects duplicate arguments */
      if (presentIC50 && presentPIC50) {
        log.info(logPrefix.concat(ERROR_STATE_2));
        throw new IllegalStateException(ERROR_STATE_2);
      }

      // Populate collection to process from available 50% inhib. conc. data.
      final List<AssociatedItem> availableC50Data = new ArrayList<AssociatedItem>();
      ICType icType = null;
      if (presentPIC50) {
        icType = ICType.PIC50;
        availableC50Data.addAll(suppliedPIC50Data);
      } else {
        icType = ICType.IC50;
        availableC50Data.addAll(suppliedIC50Data);
      }

      final List<AssociatedData> associatedData = new ArrayList<AssociatedData>();

      final BigDecimal spreadC50 = channelData.getC50Spread();
      boolean hasSpreadC50 = false;
      if (spreadC50 != null) {
        if (!isCredibleIntervals()) {
          log.info(logPrefix.concat(ERROR_STATE_CI_4));
          throw new IllegalStateException(ERROR_STATE_CI_4);
        }
        hasSpreadC50 = true;
      }
      final BigDecimal spreadHill = channelData.getHillSpread();
      boolean hasSpreadHill = false;
      if (spreadHill != null) {
        if (!isCredibleIntervals()) {
          log.info(logPrefix.concat(ERROR_STATE_CI_4));
          throw new IllegalStateException(ERROR_STATE_CI_4);
        }
        hasSpreadHill = true;
      }

      if (isCredibleIntervals() && !hasSpreadC50 && !hasSpreadHill) {
        log.info(logPrefix.concat(ERROR_STATE_CI_2));
        throw new IllegalStateException(ERROR_STATE_CI_2);
      }

      boolean hillsSupplied = false;
      /* Read in the collection of (p)IC50, Hill and Saturation values for the 
           ion channel.
         e.g. --pic50-cal 2 4 --hill-cal 0.98 0.97 --saturation-cal 34 54. */
      for (final AssociatedItem eachSuppliedC50Data : availableC50Data) {
        final BigDecimal suppliedC50 = eachSuppliedC50Data.getC50();
        final BigDecimal suppliedHill = eachSuppliedC50Data.getHill();
        final BigDecimal suppliedSaturation = eachSuppliedC50Data.getSaturation();

        /* Insist that a valid inhib. conc value is supplied.
           ApPredict silently ignores ion channel Hill or saturation values
             where there's no (p)IC50. */ 
        if (!hasValidICValue(suppliedC50, icType)) {
          log.info("~extractChannelData() : ".concat(ERROR_STATE_3));
          throw new IllegalStateException(ERROR_STATE_3);
        }

        // Info: The default ApPredict Hill value is 1.0
        if (!hasValidHillValue(suppliedHill, hasSpreadHill)) {
          log.info("~extractChannelData() : ".concat(ERROR_STATE_4));
          throw new IllegalStateException(ERROR_STATE_4);
        }

        // Info: The default ApPredict saturation value is 0 (%).
        if (!hasValidSaturationValue(suppliedSaturation)) {
          log.info("~extractChannelData() : ".concat(ERROR_STATE_5));
          throw new IllegalStateException(ERROR_STATE_5);
        }

        if (suppliedHill != null) {
          hillsSupplied = true;
        }

        BigDecimal IC50 = null;
        BigDecimal pIC50 = null;
        switch (icType) {
          case IC50:
            IC50 = suppliedC50;
            break;
          case PIC50:
            pIC50 = suppliedC50;
            break;
          default:
            throw new UnsupportedOperationException("IC type '" + icType + "' handling not currently implemented!");
        }

        associatedData.add(new AssociatedData(pIC50, IC50, suppliedHill,
                                              suppliedSaturation));
      }

      if (hillsSupplied) {
        hillCount++;
        if (!hasSpreadC50 && hasSpreadHill) {
          // Missing spread value: If inhib. conc, Hill data and Hill spread, but no inhib. conc. spread.
          log.info("~ApPredictVO() : ".concat(ERROR_STATE_CI_3));
          throw new IllegalStateException(ERROR_STATE_CI_3);
        } else if (hasSpreadC50 && !hasSpreadHill) {
          // Missing spread value: If inhib. conc, inhib. conc. spread, and Hill data, but no Hill spread value.
          /*
           * As of Jan 24th '18 this is no longer enforced as we don't want to be
           * mandating client-direct users must be entering a Hill spread value
           * 
          log.info("~ApPredictVO() : ".concat(ERROR_STATE_CI_1));
          throw new IllegalStateException(ERROR_STATE_CI_1);
          */
        }
      }
      if (hillCount > 0 && hillCount != channelCount) {
        // Ensure that if Hill data is present, it's present for all channels.
        /*
         * As of Apr 27th '18 this is no longer enforced as there are genuine
         * situations whereby an ion channel may have all pIC50s and Hills
         * estimated and the estimated Hills are all of value '1' (ApPredict's
         * default Hill value). In such circumstances business-manager nullifies
         * the estimated Hill values as an indication to ApPredict to
         * respect the difference between a measured and an estimated Hill value
         * of 1 (most notably when determining confidence intervals).
         * 
        log.info("~ApPredictVO() : ".concat(ERROR_STATE_7));
        throw new IllegalStateException(ERROR_STATE_7);
         */
      }

      requestChannelData = new RequestChannelData(associatedData, spreadC50,
                                                  spreadHill);

      log.debug("~extractChannelData() : Created '" + requestChannelData + "'.");
    }

    return requestChannelData;
  }

  // Extract and validate the plasma data from the run request
  private RequestPlasmaData extractPlasmaData(final ApPredictRunRequest request)
                                              throws IllegalStateException {

    final RequestPlasmaData plasmaData = new RequestPlasmaData();

    // Optional value
    if (request.getPlasmaConcCount() != null) {
      plasmaData.setPlasmaConcCount(request.getPlasmaConcCount());
    }

    // Optional value
    if (request.getPlasmaConcLogScale() != null) {
      plasmaData.setPlasmaConcLogScale(request.getPlasmaConcLogScale());
    }

    final TypePlasmaConcsHiLo hiLo = request.getPlasmaConcsHiLo();
    plasmaData.setRange(request.getPlasmaConcsAssigned(),
                        hiLo == null ? null : hiLo.getHigh(),
                        hiLo == null ? null : hiLo.getLow());

    plasmaData.validate();

    return plasmaData;
  }

  /**
   * Indicator that there's all relevant CellML file data available.
   * 
   * @return {@code true} if all relevant data available, otherwise
   *         {@code false}.
   */
  public boolean hasCellMLData() {
    return (getCellMLFileLocation() != null && getCellMLFileName() != null);
  }

  /**
   * Indicator that there's a pacing frequency value been set.
   * 
   * @return Indicator of presence of pacing frequency.
   */
  public boolean hasPacingFrequency() {
    return (getPacingFreq() != null);
  }

  /**
   * Indicator that there's a pacing max time value assigned.
   * 
   * @return Indicator of presence of pacing max time.
   */
  public boolean hasMaxPacingTime() {
    return (getPacingMaxTime() != null);
  }

  /*
   * For some ion channels business-manager considers two scenarios for Hill
   *   values (business_manager.ws.app_manager.manager.AppManagerManagerImpl):
   * 1) Each Hill value is either an original value or if an estimated value, it
   *    is not equal to the default Hill value (of 1).
   * 2) Each Hill value is estimated to be the default Hill value (of 1).
   * 
   * In the case of (2), all Hill values are nullified and the Hill spread
   *   value is not provided.
   */
  private boolean hasValidHillValue(final BigDecimal hillValue,
                                    final boolean hasSpreadHill) {
    log.debug("~hasValidHillValue() : Invoked.");
    boolean valid = false;

    if (hillValue != null) {
      // Gary: Hill should be >0 (if Hill is 0 formula gives 50% block regardless of conc.)
      if (hillValue.compareTo(BigDecimal.ZERO) == 1) {
        valid = true;
      }
    } else {
      // null Hill value is considered valid only if no Hill spread defined.
      if (!hasSpreadHill) {
        valid = true;
      }
    }

    log.debug("~hasValidHillValue() : Hill value of '" + hillValue + "' (Hill spread? '" + hasSpreadHill + "') is " + (valid ? "" : "in") + "valid!");

    return valid;
  }

  private boolean hasValidICValue(final BigDecimal icValue, final ICType icType) {
    log.debug("~hasValidICValue() : Invoked.");
    boolean valid = false;

    if (icValue != null) {
      switch (icType) {
        case IC50 :
          // Gary: "IC50s shouldn't be less than zero"
          if (icValue.compareTo(BigDecimal.ZERO) >= 0) {
            valid = true;
          }
          break;
        case PIC50 :
          // Gary: "pIC50 can be anything"
          valid = true;
          break;
        default:
          throw new UnsupportedOperationException("IC validity checking for '" + icType + "' not currently implemented!");
      }
    } else {
      // Null value is considered invalid!
    }

    log.debug("~hasValidICValue() : IC value of '" + icValue + "' for '" + icType + "' is '" + (valid ? "" : "in") + "valid!");

    return valid;
  }

  private boolean hasValidSaturationValue(final BigDecimal saturation) {
    log.debug("~hasValidSaturationValue() : Invoked.");
    boolean valid = false;

    if (saturation != null) {
      // Gary: "saturation shouldn't be less than zero"
      if (saturation.compareTo(BigDecimal.ZERO) >= 0) {
        valid = true;
      }
    } else {
      // Null value is considered invalid!
    }

    log.debug("~hasValidSaturationValue() : saturation value of '" + saturation + "' is '" + (valid ? "" : "in") + "valid!");

    return valid;
  }

  /**
   * Flag to indicate if a PK simulation has been requested.
   * 
   * @return {@code true} if doing a PK simulation, otherwise {@code false}.
   */
  public boolean isPKSimulation() {
    return (!StringUtils.isBlank(getPKFileLocation()));
  }

  /**
   * Retrieve the ion channel data corresponding to the ion channel.
   * 
   * @param channel Ion channel.
   * @return Ion channel data (or null if not assigned).
   */
  public RequestChannelData retrieveChannelData(final CHANNEL_CURRENT channel) {
    RequestChannelData requestChannelData = null;
    switch (channel) {
      case ICaL :
        requestChannelData = data_ICaL;
        break;
      case IKr :
        requestChannelData = data_IKr;
        break;
      case IK1 :
        requestChannelData = data_IK1;
        break;
      case IKs :
        requestChannelData = data_IKs;
        break;
      case Ito :
        requestChannelData = data_Ito;
        break;
      case INa :
        requestChannelData = data_INa;
        break;
      case INaL :
        requestChannelData = data_INaL;
        break;
      default :
        break;
    }

    return requestChannelData;
  }

  /**
   * Retrieve a collection of all files which are due for deletion.
   * 
   * @return Collection of files for removal post- simulation run (or empty
   *         collection if none to remove).
   */
  public Set<String> retrieveRemovableFileLocations() {
    log.debug("~retrieveRemovableFileLocations() : Invoked.");

    final Set<String> removableFiles = new HashSet<String>();
    if (isPKSimulation()) {
      if (pkFileLocation != null) {
        removableFiles.add(pkFileLocation);
      }
    }
    if (cellMLFileLocation != null) {
      removableFiles.add(cellMLFileLocation);
    }

    return removableFiles;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "ApPredictVO [data_ICaL=" + data_ICaL + ", data_IKr=" + data_IKr
        + ", data_IK1=" + data_IK1 + ", data_IKs=" + data_IKs + ", data_Ito="
        + data_Ito + ", data_INa=" + data_INa + ", data_INaL=" + data_INaL
        + ", credibleIntervals="
        + credibleIntervals + ", credibleIntervalPercentiles="
        + credibleIntervalPercentiles + ", modelIdentifier=" + modelIdentifier
        + ", pacingFreq=" + pacingFreq + ", pacingMaxTime=" + pacingMaxTime
        + ", plasma=" + plasma + ", pkFileLocation=" + pkFileLocation
        + ", cellMLFileName=" + cellMLFileName + ", cellMLFileLocation="
        + cellMLFileLocation + ", channelCount=" + channelCount + ", hillCount="
        + hillCount + "]";
  }

  /**
   * Retrieve the model identifier.
   * 
   * @return Model identifier (or {@code null} if using CellML file).
   */
  public Short getModelIdentifier() {
    return modelIdentifier;
  }

  /**
   * Retrieve the pacing frequency.
   * 
   * @return Pacing frequency (or null if not assigned).
   */
  public BigDecimal getPacingFreq() {
    return pacingFreq;
  }

  /**
   * Retrieve the max pacing time.
   * 
   * @return The max pacing time (in minutes) (or null if not assigned). 
   */
  public BigDecimal getPacingMaxTime() {
    return pacingMaxTime;
  }

  /**
   * Retrieve the plasma data.
   * 
   * @return Plasma data.
   */
  public RequestPlasmaData getPlasma() {
    return plasma;
  }

  /**
   * Retrieve CellML file name.
   * 
   * @return CellML file name, or {@code null} if none available.
   */
  public String getCellMLFileName() {
    return cellMLFileName;
  }

  /**
   * Retrieve CellML file location.
   * 
   * @return CellML file location, or {@code null} if none available.
   */
  public String getCellMLFileLocation() {
    return cellMLFileLocation;
  }

  /**
   * Retrieve PKFile location.
   * 
   * @return PK file location, or {@code null} if none available.
   */
  public String getPKFileLocation() {
    return pkFileLocation;
  }

  /**
   * Retrieve flag to indicate if there are credible intervals / spreads
   * requested.
   * 
   * @return {@code true} if credible intervals requested, otherwise
   *         {@code false}.
   */
  public boolean isCredibleIntervals() {
    return credibleIntervals;
  }

  /**
   * Retrieve the credible interval percentile values.
   * 
   * @return Credible interval percentile values, or {@code null} if not
   *         requesting credible intervals.
   */
  public String getCredibleIntervalPercentiles() {
    return credibleIntervalPercentiles;
  }
}