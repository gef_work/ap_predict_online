/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.postrun.reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO;
import uk.ac.ox.cs.nc3rs.app_manager.util.FileUtil;
import uk.ac.ox.cs.nc3rs.app_manager.value.QNetResultsVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.SimulationResultsVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.VoltageResultsVO;

/**
 * Abstract simulation run results reader.
 *
 * @author geoff
 */
@Component()
public abstract class AbstractRunResultsReader {

  protected static final String tab = "\t";
  private static final String fileNameQNetResults = "q_net.txt";
  private static final String fileNameMessages = "messages.txt";
  private static final String fileNameVoltageResults = "voltage_results.dat";
  private static final String apPredictVoltageTraceRegex = "^.*conc_\\d+.*_voltage_trace\\.dat$";
  private static final String deltaAPD90PercentileUnits = "(%)";

  @Autowired @Qualifier(AppIdentifiers.COMPONENT_JDBC_APP_MANAGER_DAO)
  private AppManagerDAO appManagerDAO;

  private String baseDir;

  private static final Log log = LogFactory.getLog(AbstractRunResultsReader.class);

  /**
   * Read in simulation results data, persisting messages whilst transferring
   * other data for later persisting.
   * 
   * <ul>
   *   <li>Persist {@value #fileNameMessages} data</li>
   *   <li>Read {@value #fileNameVoltageResults} data</li>
   *   <li>Read concentration results file data</li>
   *   <li>Read (optional) {@value #fileNameQNetResults} data</li>
   * </ul>
   * 
   * @param appManagerId App Manager identifier.
   * @param resultFilesDir System directory holding results files.
   * @return CSV of delta APD90 percentile names.
   */
  protected String generalDataLoading(final long appManagerId,
                                      final String resultFilesDir,
                                      final List<SimulationResultsVO> simulationResults) {
    final String logPrefix = "~int.generalDataLoading() : [" + appManagerId + "] : ";
    log.debug(logPrefix.concat("Invoked."));

    // 1) Persist (i.e. write to db) discretionary messages file data
    persistMessages(appManagerId, resultFilesDir);

    /* 2) Read in voltage_results.dat data - keyed on concentration. */
    final Map<String, VoltageResultsVO> voltageResultsValues = new HashMap<String, VoltageResultsVO>();
    final String deltaAPD90PercentileNames = readVoltageResults(appManagerId,
                                                                resultFilesDir,
                                                                voltageResultsValues);

    /* 3) Read in *optional* q_net.txt data - keyed on concentration. */
    final Map<String, QNetResultsVO> qNetResultsValues = new HashMap<String, QNetResultsVO>();
    readQNet(appManagerId, resultFilesDir, qNetResultsValues);
    final boolean hasQNetData = !qNetResultsValues.isEmpty();

    /* 4) Read in conc_0.669433_voltage_trace.dat, etc., files - keyed on
          concentration. */
    final Map<String, Map<String, String>> voltageTraceResultsValues = new HashMap<String, Map<String, String>>();
    readConcentrationVoltages(appManagerId, resultFilesDir,
                              voltageTraceResultsValues);

    /*
    System.out.println(voltageResultsValues.keySet());
    System.out.println(voltageTraceResultsValues.keySet());
    if (!voltageResultsValues.keySet().equals(voltageTraceResultsValues.keySet())) {
      final String errorMessage = "Imbalance between concentrations recorded in voltage_results.dat and conc_X_voltage_trace.dat files";
      log.error(logPrefix.concat(errorMessage));
      throw new UnsupportedOperationException(errorMessage);
    }
    if (hasQNetData) {
      if (!qNetResultsValues.keySet().equals(voltageTraceResultsValues.keySet())) {
        final String errorMessage = "Imbalance between concentrations recorded in q_net.txt and conc_X_voltage_trace.dat files";
        log.error(logPrefix.concat(errorMessage));
        throw new UnsupportedOperationException(errorMessage);
      }
    }
    */

    /* 5) Merge all data into simulations results value objects. */
    for (final String concentration : voltageTraceResultsValues.keySet()) {
      final Map<String, String> voltageTraces = voltageTraceResultsValues.get(concentration);
      final VoltageResultsVO voltageResultsVO = voltageResultsValues.get(concentration);
      if (voltageResultsVO == null) {
        final String errorMessage = "A conc_X_voltage_trace.dat file exists yet no such conc in voltage_results.dat!";
        log.error(logPrefix.concat(errorMessage));
        throw new UnsupportedOperationException(errorMessage);
      }
      String qNetValues = null;
      if (hasQNetData) {
        qNetValues = qNetResultsValues.get(concentration).getqNet();
      }

      simulationResults.add(new SimulationResultsVO(concentration,
                                                    StringUtils.join(voltageTraces.keySet().toArray(), ","),
                                                    StringUtils.join(voltageTraces.values().toArray(), ","),
                                                    voltageResultsVO.getAPD90(),
                                                    voltageResultsVO.getDeltaAPD90(),
                                                    qNetValues));
    }

    return deltaAPD90PercentileNames;
  }

  /* Read concentration voltages files, e.g. {@code conc_0.669433_voltage_trace.dat}. */ 
  private void readConcentrationVoltages(final long appManagerId,
                                         final String resultFilesDir,
                                         final Map<String, Map<String, String>> voltageTraceResultsValues) {
    final String logPrefix = "~int.readConcentrationVoltages() : [" + appManagerId + "] : ";
    log.debug(logPrefix.concat("Invoked."));

    final FileUtil fileUtil = new FileUtil();
    final String[] fileList = FileUtil.retrieveFiles(fileUtil.new DirFilter(apPredictVoltageTraceRegex),
                                                     fileUtil.new AlphabeticComparator(),
                                                     resultFilesDir, true);

    // Read in the concentration voltage traces
    for (final String resultsFileName : Arrays.asList(fileList)) {
      final String resultsFilePath = resultFilesDir.concat(resultsFileName);
      log.debug(logPrefix.concat("Results file path '" + resultsFilePath + "'."));
      final File resultsFile = new File(resultsFilePath);

      final List<String> voltageTraceLines = new ArrayList<String>();
      try {
        voltageTraceLines.addAll(FileUtil.fileLineReader(resultsFile, true,
                                                         true, true));
      } catch (FileNotFoundException e) {
        final String errorMessage = "Failed to read file '" + resultsFilePath + "'";
        log.error(logPrefix.concat(errorMessage));
        throw new UnsupportedOperationException(errorMessage);
      }

      final Map<String, String> voltageTraces = new LinkedHashMap<String, String>();
      for (final String voltageTraceLine : voltageTraceLines) {
        if (voltageTraceLine.length() > 0) {
          final String[] voltageTraceColumns = voltageTraceLine.split(tab);
          voltageTraces.put(voltageTraceColumns[0].trim(),
                            voltageTraceColumns[1].trim());
        }
      }

      // Named, e.g. conc_0_voltage_trace.dat
      final String[] components = resultsFileName.split("_");
      final String concentration = components[1];

      voltageTraceResultsValues.put(concentration, voltageTraces);
    }
  }

  /* Read (*optional*) qNet results file, e.g. q_net.txt. */
  private void readQNet(final long appManagerId, final String resultFilesDir,
                        final Map<String, QNetResultsVO> qNetResultsValues) {
    final String logPrefix = "~int.readQNet() : [" + appManagerId + "] : ";
    log.debug(logPrefix.concat("Invoked."));

    /*
     * Read in the optional q_net.txt results file.
     */
    final String qNetResultsFilePath = resultFilesDir.concat(fileNameQNetResults);
    final File qNetResultsFile = new File(qNetResultsFilePath);

    final List<String> qNetResultsLines = new ArrayList<String>();
    try {
      qNetResultsLines.addAll(FileUtil.fileLineReader(qNetResultsFile, true,
                                                      true, false));
    } catch (FileNotFoundException e) {}

    if (!qNetResultsLines.isEmpty()) {
      for (final String qNetResultsLine : qNetResultsLines) {
        final String[] columns = qNetResultsLine.split(tab);
        final String concentration = columns[0];
        final String qNetValues = columns[1];

        qNetResultsValues.put(concentration, new QNetResultsVO(concentration,
                                                               qNetValues));
      }
    }
  }

  /* Read voltage results file data, probably as determined by content of 
     {@value #fileNameVoltageResults}. */
  private String readVoltageResults(final long appManagerId,
                                    final String resultFilesDir,
                                    final Map<String, VoltageResultsVO> voltageResultsValues) {
    final String logPrefix = "~int.readVoltageResults() : [" + appManagerId + "] : ";
    log.debug(logPrefix.concat("Invoked."));

    final String voltageResultsFilePath = resultFilesDir.concat(fileNameVoltageResults);
    final File voltageResultsFile = new File(voltageResultsFilePath);

    final List<String> voltageResultsLines = new ArrayList<String>();
    try {
      voltageResultsLines.addAll(FileUtil.fileLineReader(voltageResultsFile,
                                                         true, false, true));
    } catch (FileNotFoundException e) {
      final String errorMessage = "Failed to read file '" + voltageResultsFilePath + "'";
      log.error(logPrefix.concat(errorMessage));
      throw new UnsupportedOperationException(errorMessage);
    }

    String deltaAPD90PercentileNames = null;
    for (final String voltageResultsLine : voltageResultsLines) {
      if (!StringUtils.isBlank(voltageResultsLine)) {

        final String[] voltageResultsColumns = voltageResultsLine.split(tab);
        final String concentration = voltageResultsColumns[0];
        final String upstrokeVelocity = voltageResultsColumns[1];
        final String peakVm = voltageResultsColumns[2];
        final String apd50 = voltageResultsColumns[3];
        final String apd90 = voltageResultsColumns[4];
        final String deltaAPD90 = voltageResultsColumns[5];

        if (deltaAPD90PercentileNames == null) {
        /*
        if (deltaAPD90PercentileNames == null &&
            deltaAPD90.contains(deltaAPD90PercentileUnits)) {
        */
          deltaAPD90PercentileNames = deltaAPD90.replace(deltaAPD90PercentileUnits, "")
                                                .trim();

          log.debug(logPrefix.concat("DeltaAPD90 column titled '" + deltaAPD90PercentileNames + "'."));
        }

        voltageResultsValues.put(concentration,
                                 new VoltageResultsVO(concentration,
                                                      upstrokeVelocity,
                                                      peakVm, apd50, apd90,
                                                      deltaAPD90));
      }
    }

    return deltaAPD90PercentileNames;
  }

  /* Transfer discretionary messages data (from {@value #fileNameMessages}) to
     db. */
  private void persistMessages(final long appManagerId,
                               final String resultFilesDir) {
    final String logPrefix = "~int.persistMessages() : [" + appManagerId + "] : ";
    log.debug(logPrefix.concat("Invoked."));

    final String messagesFilePath = resultFilesDir.concat(fileNameMessages);
    final File messagesFile = new File(messagesFilePath);

    final List<String> messages = new ArrayList<String>();
    try {
      messages.addAll(FileUtil.fileLineReader(messagesFile, true, false, false));
    } catch (FileNotFoundException e) {}

    if (!messages.isEmpty()) {
      final String allMessages = StringUtils.join(messages, '\n');
      if (!StringUtils.isBlank(allMessages)) {
        getAppManagerDAO().saveSimulationMessages(appManagerId, allMessages);
      }
    }
  }

  /**
   * Retrieve the result files directory.
   * 
   * @param appManagerId App manager identifier.
   * @return Directory holding all the results files.
   */
  protected String retrieveResultFilesDir(final long appManagerId) {
    final String appManagerDir = FileUtil.retrieveJobDirectory(getBaseDir(),
                                                               appManagerId);
    final StringBuffer resultFilesSB = new StringBuffer();
    resultFilesSB.append(appManagerDir);
    resultFilesSB.append(AppIdentifiers.AP_PREDICT_OUTPUT_BASE);

    return resultFilesSB.toString();
  }
  /**
   * Retrieve the app manager DAO.
   * 
   * @return App manager DAO.
   */
  protected AppManagerDAO getAppManagerDAO() {
    return appManagerDAO;
  }

  /**
   * Retrieve the base directory.
   * 
   * @return The base directory.
   */
  protected String getBaseDir() {
    return baseDir;
  }


  /**
   * Assign the base directory, e.g. /home/me/run_here/
   * 
   * @param baseDir Base directory.
   */
  // Spring-injected.
  @Value("${base.dir}")
  public void setBaseDir(final String baseDir) {
    this.baseDir = baseDir;
  }
}