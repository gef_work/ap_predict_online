/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.value;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Value object holding all the Ion Channel data which has arrived on a web service request to run 
 * ApPredict.
 *
 * @author geoff
 */
public class RequestChannelData implements Serializable {

  private static final long serialVersionUID = 7491029827740215015L;

  private final List<AssociatedData> associatedData = new ArrayList<AssociatedData>();
  private final BigDecimal spreadC50;
  private final BigDecimal spreadHill;

  /**
   * Initialising constructor (for when no confidence interval spread data is available).
   * <p>
   * The associated data must contain one or the other of pIC50 or IC50 values.
   * 
   * @param associatedData Collection (at least one) of related (p)IC50s and Hills.
   */
  public RequestChannelData(final List<AssociatedData> associatedData) {
    this(associatedData, null, null);
  }

  /**
   * Initialising constructor.
   * <p>
   * The associated data must contain one or the other of pIC50 or IC50 values.
   *
   * @param associatedData Collection (at least one) of related (p)IC50s and Hills.
   * @param spreadC50 Confidence interval spread for C50s (or {@code null} if not applicable).
   * @param spreadHill Confidence interval spread for Hill coefficients (or {@code null} if not
   *                   applicable).
   * @throws IllegalStateException if no {@linkplain #associatedData} is provided.
   */
  public RequestChannelData(final List<AssociatedData> associatedData, final BigDecimal spreadC50,
                            final BigDecimal spreadHill) throws IllegalStateException {
    if (associatedData == null || associatedData.isEmpty()) {
      throw new IllegalStateException("(p)IC50 values must be provided for RequestChannelData objects");
    }

    short IC50Count = (short) 0;
    short pIC50Count = (short) 0;

    // Validate content 
    for (final AssociatedData eachItem : associatedData) {
      final BigDecimal IC50 = eachItem.getIC50();
      if (IC50 != null) {
        IC50Count++;
      }
      final BigDecimal pIC50 = eachItem.getpIC50();
      if (pIC50 != null) {
        pIC50Count++;
      }
      if (IC50Count > 0 && pIC50Count > 0) {
        throw new IllegalArgumentException("RequestChannelData has received associated data in both IC50 and pIC50 format");
      }
    }

    this.associatedData.addAll(associatedData);
    this.spreadC50 = spreadC50;
    this.spreadHill = spreadHill;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "RequestChannelData [associatedData=" + associatedData
        + ", spreadC50=" + spreadC50 + ", spreadHill=" + spreadHill + "]";
  }

  /**
   * Retrieve the associated data items (of which there will be at least one).
   * <p>
   * The data will be either IC50(s) or pIC50(s) and associated Hill(s).
   * 
   * @return Associated data items.
   */
  public List<AssociatedData> getAssociatedData() {
    return Collections.unmodifiableList(associatedData);
  }

  /**
   * Retrieve the confidence interval spread for (p)IC50. 
   * 
   * @return (p)IC50 confidence interval spread (or null if not assigned).
   */
  public BigDecimal getSpreadC50() {
    return spreadC50;
  }

  /**
   * Retrieve the Hill confidence interval spread.
   * 
   * @return Hill confidence interval spread (or null if not assigned).
   */
  public BigDecimal getSpreadHill() {
    return spreadHill;
  }
}