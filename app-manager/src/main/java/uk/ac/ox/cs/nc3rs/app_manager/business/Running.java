/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.business;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Object to store a collection of running simulations.
 * 
 * @author Geoff Williams
 */
public final class Running {

  private static final List<Long> running = new CopyOnWriteArrayList<Long>();

  private static final Log log = LogFactory.getLog(Running.class);

  /**
   * Add a new simulation to the collection of those running.
   * 
   * @param appManagerId App manager identifier (identifying the running simulation).
   */
  public static void addToRunning(final Long appManagerId) {
    log.debug("~addToRunning() : Adding identifier '" + appManagerId + "' to collection.");

    running.add(appManagerId);

    for (final Long identifier : running) {
      log.trace("~addToRunning() : Contains '" + identifier + "'.");
    }
  }

  /**
   * Retrieve a count of the number of running simulations.
   * 
   * @return Count of number of running simulations.
   */
  public static int count() {
    return running.size();
  }

  /**
   * Remove the app manager identifier from the list of running simulations. 
   * 
   * @param appManagerId App manger identifier.
   */
  public static void removeFromRunning(final Long appManagerId) {
    log.debug("~removeFromRunning() : Removing identifier '" + appManagerId + "' from collection.");

    running.remove(appManagerId);

    for (final Long identifier : running) {
      log.trace("~removeFromRunning() : Contains '" + identifier + "'.");
    }
  }
}