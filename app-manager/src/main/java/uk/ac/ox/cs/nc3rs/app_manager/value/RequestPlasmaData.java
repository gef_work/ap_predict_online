/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.value;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;

/**
 * Value object holding all the plasma/compound data which has arrived on a web
 * service request to run ApPredict.
 * 
 * @author geoff
 */
public class RequestPlasmaData implements Serializable {

  private static final long serialVersionUID = 2947798687864882648L;

  private Boolean concCountAssigned = false;
  private Boolean concLogScaleAssigned = false;
  private Boolean rangeAssigned = false;

  private Short plasmaConcCount;
  private BigDecimal plasmaConcHigh;
  private BigDecimal plasmaConcLow;
  private String plasmaConcs;
  private Boolean plasmaConcLogScale;

  /**
   * Indicate if the plasma data has concentration count value has been set.
   * 
   * @return {@code true} if concentration count value was assigned (even if it was a {@code null}
   *         assignment!), otherwise {@code false}.
   */
  public boolean isConcCountAssigned() {
    return concCountAssigned;
  }

  /**
   * Indicate if the plasma data has the concentration log scale value has been set.
   * 
   * @return {@code true} if the concentration log scale value was assigned (even if it was a 
   *         {@code null} assignment!), otherwise {@code false}.
   */
  public boolean isConcLogScaleAssigned() {
    return concLogScaleAssigned;
  }

  /**
   * Indicate if the plasma data has a concentration range value has been set.
   * 
   * @return {@code true} if concentration range was assigned (even if it was a {@code null}
   *         assignment!), otherwise {@code false}.
   */
  public boolean isRangeAssigned() {
    return rangeAssigned;
  }

  /**
   * Assign optional plasma concentration count.
   * 
   * @param plasmaConcCount Plasma concentration count.
   * @throws IllegalArgumentException if plasma concentration count already assigned.
   */
  public void setPlasmaConcCount(final Short plasmaConcCount) throws IllegalArgumentException {
    if (isConcCountAssigned()) {
      throw new IllegalArgumentException("Cannot reassign plasma concentration count");
    }
    concCountAssigned = true;
    this.plasmaConcCount = plasmaConcCount;
  }

  /**
   * Assign optional plasma concentration log scale.
   * 
   * @param plasmaConcLogScale Plasma concentration log scale.
   * @throws IllegalArgumentException if plasma concentration log scale already assigned.
   */
  public void setPlasmaConcLogScale(final Boolean plasmaConcLogScale) {
    if (isConcLogScaleAssigned()) {
      throw new IllegalArgumentException("Cannot reassign plasma concentration log scale");
    }
    concLogScaleAssigned = true;
    this.plasmaConcLogScale = plasmaConcLogScale;
  }

  /**
   * Assign the range of concentrations to test against.
   * <p>
   * Expecting one of :
   * <ul>
   *   <li>{@linkplain #plasmaConcs}</li>
   *   <li>{@linkplain #plasmaConcHigh} (and optionally {@linkplain #plasmaConcLow})</li>
   * </ul>
   * <p>
   * If {@link #plasmaConcs} is specified then other parameters are silently ignored.
   * 
   * @param plasmaConcs CSV string of concentrations.
   * @param plasmaConcHigh Highest plasma concentration.
   * @param plasmaConcLow Lowest plasma concentration.
   * @throws IllegalArgumentException if attempting to reassign a range.
   * @throws IllegalArgumentException if neither plasma concentrations or highest plasma
   *                                  concentration is specified.
   * @throws IllegalArgumentException if a high concentration is lower than a low concentration.
   * @throws IllegalArgumentException if a high concentration is negative.
   */
  public void setRange(final String plasmaConcs, final BigDecimal plasmaConcHigh,
                       final BigDecimal plasmaConcLow) throws IllegalArgumentException {
    if (isRangeAssigned()) {
      throw new IllegalArgumentException("Cannot reassign plasma concentration range values");
    }

    if (plasmaConcs != null && plasmaConcs.trim().length() > 0) {
      // Converting the CSV string to sorted numerical
      final Set<String> concentrations = new HashSet<String>();
      concentrations.addAll(Arrays.asList(plasmaConcs.trim().split(",")));
      final Set<BigDecimal> sorted = new TreeSet<BigDecimal>();
      for (final String concentration : concentrations) {
        sorted.add(new BigDecimal(concentration.trim()));
      }
      this.plasmaConcs = StringUtils.join(sorted, ",");
    } else {
      if (plasmaConcHigh == null) {
        throw new IllegalArgumentException("Either plasma concentrations or highest plasma concentration is required");
      }
      if (plasmaConcLow != null) {
        if (plasmaConcLow.compareTo(plasmaConcHigh) > 0) {
          throw new IllegalArgumentException("Cannot assign a low concentration value greater than the high concentration");
        }
        if (plasmaConcLow.compareTo(BigDecimal.ZERO) < 0) {
          throw new IllegalArgumentException("Cannot have a negative low concentration value");
        }
      }
      if (plasmaConcHigh.compareTo(BigDecimal.ZERO) < 0) {
        throw new IllegalArgumentException("Cannot have a negative high concentration value");
      }

      this.plasmaConcHigh = plasmaConcHigh;
      this.plasmaConcLow = plasmaConcLow;
    }

    rangeAssigned = true;
  }

  /**
   * Validate the state of the object.
   * 
   * @throws IllegalStateException If a plasma concentration range isn't assigned.
   */
  public void validate() throws IllegalStateException {
    if (!isRangeAssigned()) {
      throw new IllegalStateException("A range of plasma concentration is required");
    }
  }

  /**
   * Retrieve the plasma concentration count.
   * 
   * @return Plasma concentration count if assigned, otherwise {@code null}.
   */
  public Short getPlasmaConcCount() {
    return plasmaConcCount;
  }

  /**
   * Retrieve the highest plasma concentration value.
   * 
   * @return Highest plasma concentration value if assigned, otherwise {@code null}.
   */
  public BigDecimal getPlasmaConcHigh() {
    return plasmaConcHigh;
  }

  /**
   * Retrieve the lowest plasma concentration value.
   * 
   * @return Lowest plasma concentration value if assigned, otherwise {@code null}.
   */
  public BigDecimal getPlasmaConcLow() {
    return plasmaConcLow;
  }

  /**
   * Retrieve plasma concentrations in CSV format.
   * 
   * @return Plasma concentrations if assigned, otherwise {@code null}.
   */
  public String getPlasmaConcs() {
    return plasmaConcs;
  }

  /**
   * Indicator of whether a specified plasma conc log scale is true or false.
   * 
   * @return {@code true} if plasma conc log scale wanted, otherwise {@code false}.
   */
  public Boolean getPlasmaConcLogScale() {
    return plasmaConcLogScale;
  }
}
