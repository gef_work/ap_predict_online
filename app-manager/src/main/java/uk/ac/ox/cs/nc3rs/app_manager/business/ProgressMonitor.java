/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.business;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.ws.app_manager.proxy.AppServicesProxy;

/**
 * Progress monitor which periodically reads the file created by ApPredict and
 * sends a SOAP message containing the progress info to the app. 
 *
 * @author Geoff Williams
 */
@Component(AppIdentifiers.COMPONENT_PROGRESS_MONITOR)
@Scope("prototype")
public class ProgressMonitor {

  @Autowired @Qualifier(AppIdentifiers.COMPONENT_APP_SERVICES_PROXY)
  private AppServicesProxy appServices;

  private Timer daemonTimer;
  private int appManagerId;
  private String progressFileName;
  private int fileNotFoundCount = 0;
  private List<String> linesRead = new ArrayList<String>();

  private static final int MAX_WARNINGS_COUNT = 12;
  private static final String PROGRESS_PREFIX = "PROG: ";
  // Referenced also by client-direct value/StatusVO.java
  private static final String progressFileNotFound = " Progress file not found - ";
  private static final String STATUS_CODE = "D10";

  private static final Log log = LogFactory.getLog(ProgressMonitor.class);

  ProgressMonitor() {
    this.daemonTimer = new Timer(true);
  }

  private class QueryTask extends TimerTask {
    public void run() {
      log.debug("~run() : Invoked. Looking for '" + progressFileName + "'");

      FileReader fileReader = null;
      try {
        fileReader = new FileReader(progressFileName);
        log.debug("~run() : ProgressFile '" + progressFileName + "' found");
      } catch (final FileNotFoundException fileNotFoundException) {
        /* To be expected when simulation has ended successfully, although if
           simulation ended prematurely it's worth investigating. If the latter,
           check for the presence of a VRE_OUTPUT.<app-manager id> file in the
           application run directory! */
        fileNotFoundCount++;
        log.debug("~run() : ProgressFile '" + progressFileName + "' not found - attempt '" + fileNotFoundCount + "'");

        String status = PROGRESS_PREFIX;
        if (fileNotFoundCount == MAX_WARNINGS_COUNT) {
          status = status.concat(progressFileNotFound).concat("waiting exhausted... apparent failure to run ApPredict!");
          log.debug("~run() : About to cancel the timer");
          daemonTimer.cancel();
        } else if (fileNotFoundCount < MAX_WARNINGS_COUNT) {
          final int countDown = MAX_WARNINGS_COUNT - fileNotFoundCount + 1;
          status = status.concat(progressFileNotFound).concat("counting down .. " + countDown);
        }
        log.debug("~run() : ".concat(STATUS_CODE).concat(" status to set '" + status + "'"));
        appServices.assignProcessStatus(appManagerId, status, STATUS_CODE,
                                        "Host", "Source", "processId");
      }

      if (fileReader != null) {
        final Scanner scanner = new Scanner(fileReader);
        final List<String> unReadLines = new ArrayList<String>();

        if (scanner != null) {
          final List<String> fixedLinesRead = Collections.unmodifiableList(linesRead);
          try {
            while(scanner.hasNextLine()) {
              final String line = scanner.nextLine();
              if (!fixedLinesRead.contains(line)) {
                final String statusLine = PROGRESS_PREFIX.concat(line);
                unReadLines.add(statusLine);
                linesRead.add(line);
              }
            }
          } finally {
            log.trace("~run() : Closing scanner.");
            scanner.close();
          }
        }
        try {
          log.trace("~run() : Closing filereader.");
          fileReader.close();
        } catch (IOException e) {
          log.warn("~run() : Exception closing FileReader '" + e.getMessage() + "'.");
          e.printStackTrace();
        }
        if (!unReadLines.isEmpty()) {
          for (final String unReadLine : unReadLines) {
            appServices.assignProcessStatus(appManagerId, unReadLine, STATUS_CODE, "Host",
                "Source", "processId");
          }
        }
      }
    }
  }

  /**
   * @param appManagerId the appManagerId to set
   */
  public void setAppManagerId(int appManagerId) {
    this.appManagerId = appManagerId;
  }

  public void setSchedule(final String monitorFrequency) {
    this.daemonTimer.schedule(new QueryTask(), 2000,
                              new Integer(monitorFrequency) * 1000);
  }

  /**
   * @param progressFileName the progressFileName to set
   */
  public void setProgressFileName(final String progressFileName) {
    this.progressFileName = progressFileName;
  }
}