/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the names of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import javax.activation.DataHandler;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.value.DiagnosticsVO;

/**
 * File utility class.
 *
 * @author Geoff Williams
 */
public class FileUtil {

  private static final String defaultTmpFilePrefix = "portal_";
  private static final String defaultTmpFileSuffix = ".tmpfile";
  private static final String diagnosticsInfoName = "VRE_INFO";
  private static final String diagnosticsOutputName = "VRE_OUTPUT";
  private static final String diagnosticsFilesRegex = "VRE_(INFO|OUTPUT)\\.\\d+.*";

  private static final Log log = LogFactory.getLog(FileUtil.class);

  public class DirFilter implements FilenameFilter {
    private Pattern pattern;
    public DirFilter(final String regex) {
      pattern = Pattern.compile(regex);
    }
    public boolean accept(final File dir, final String name) {
      return pattern.matcher(new File(name).getName()).matches();
    }
    public Pattern getPattern() {
      return pattern;
    }
  }

  @SuppressWarnings("rawtypes")
  public class AlphabeticComparator implements Comparator {
    public int compare(Object o1, Object o2) {
      String s1 = (String) o1;
      String s2 = (String) o2;
      return s1.toLowerCase().compareTo(s2.toLowerCase());
    }
  }

  /**
   * Delete a file (or recursively delete a directory).
   * 
   * @param file File object pointing to a file or directory.
   */
  public static void delete(final File file) {
    if (file == null) return;
    final String fileName = file.getName();
    log.trace("~int.delete() : Incoming '" + fileName + "'.");

    if (file.exists()) {
      for(final File listedFile : file.listFiles()) {
        final String listedFileName = listedFile.getName();
        if(listedFile.isDirectory()) {
          delete(listedFile);
        } else {
          log.trace("~int.delete() : Delete file '" + listedFileName + "'.");
          if (!listedFile.delete()) {
            log.warn("~int.delete() : Could not delete '" + listedFileName + "'.");
          }
        }
      }

      if (!file.delete()) {
        log.warn("~int.delete() : Could not delete '" + fileName + "'.");
      }
    }
  }

  /**
   * Read a file into an ordered collection of lines.
   * 
   * @param file File to read.
   * @param ignoreBlanks {@code true} if not to include blank lines.
   * @param ignoreFirstLine {@code true} if not to include first (blank or 
   *                        non-blank) line read in.
   * @param throwFileNotFound {@code true} if to throw
   *                          {@code FileNotFoundException}
   * @return Ordered collection of file lines.
   * @throws FileNotFoundException If file not found and 
   *                               {@code throwFileNotFound} is {@code true}.
   */
  public static List<String> fileLineReader(final File file,
                                            final boolean ignoreBlanks,
                                            final boolean ignoreFirstLine,
                                            final boolean throwFileNotFound)
                                            throws FileNotFoundException {
    final List<String> lines = new ArrayList<String>();

    Scanner scanner = null;
    if (throwFileNotFound) {
      scanner = new Scanner(file);
    } else {
      try {
        scanner = new Scanner(file);
      } catch (FileNotFoundException e) {
        // We're not throwing the exception if file not found.
        return lines;
      }
    }

    boolean firstLine = true;
    try {
      while (scanner.hasNextLine()) {
        final String line = scanner.nextLine();

        if (firstLine) {
          firstLine = false;
          if (ignoreFirstLine) {
            continue;
          }
        }

        if (StringUtils.isBlank(line)) {
          if (!ignoreBlanks) {
            lines.add(line);
          }
        } else {
          lines.add(line);
        }
      }
    } finally {
      scanner.close();
    }

    return lines;
  }

  /**
   * Retrieve diagnostics data from the filesystem.
   * 
   * @param appManagerId Application manager identifier.
   * @param baseDir Base directory.
   * @return Diagnostics data.
   */
  public static DiagnosticsVO retrieveDiagnostics(final long appManagerId,
                                                  final String baseDir) {
    log.debug("~retrieveDiagnostics() : Invoked for '" + appManagerId + "'.");
    final String jobDirectory = retrieveJobDirectory(baseDir, appManagerId);

    final FileUtil fileUtil = new FileUtil();
    final String[] vreFileList = retrieveFiles(fileUtil.new DirFilter(diagnosticsFilesRegex),
                                               fileUtil.new AlphabeticComparator(),
                                               jobDirectory, false);

    String info = null;
    String output = null;
    // Read in VRE info
    for (final String vreFileName : Arrays.asList(vreFileList)) {
      final String vreFilePath = jobDirectory.concat(vreFileName);
      log.debug("~retrieveDiagnostics(RetrieveDiagnosticsRequest) : VRE file path '" + vreFilePath + "'");
      final File vreFile = new File(vreFilePath);

      Scanner scanner = null;
      try {
        scanner = new Scanner(vreFile);
      } catch (FileNotFoundException fileNotFoundException) {
        final String infoMessage = "Failed to read discretionary file '" + vreFilePath + "'";
        log.info("~retrieveDiagnostics(RetrieveDiagnosticsRequest) : " + infoMessage);
      }
      if (scanner != null) {
        final StringBuffer vreText = new StringBuffer();
        while (scanner.hasNextLine()) {
          vreText.append(scanner.nextLine() + '\n');
        }
        scanner.close();

        if (vreText.length() > 0) {
          log.debug("~retrieveDiagnostics(RetrieveDiagnosticsRequest) : Content read in.");
          final String text = vreText.toString();
          if (vreFileName.startsWith(diagnosticsInfoName)) {
            info = text;
          } else if (vreFileName.startsWith(diagnosticsOutputName)) {
            output = text;
          } else {
            final String errorMessage = "Expecting one of '" + diagnosticsInfoName + "/" + diagnosticsOutputName + " file naming, instead got '" + vreFileName + "'";
            log.error("~retrieveDiagnostics(RetrieveDiagnosticsRequest) : " + errorMessage);
            throw new UnsupportedOperationException(errorMessage);
          }
        }
      }
    }

    return new DiagnosticsVO(info, output);
  }

  /**
   * Retrieve an array (potentially empty) of file names in '{@code directory}'
   * which match the specified filter '{@code dirFilter}', optionally 
   * '{@code ordered}' by the '{@code alphabeticComparator}'.
   *  
   * @param dirFilter Filter to apply to file names.
   * @param alphabeticComparator Ordering comparator.
   * @param directory Directory to list.
   * @param ordered {@code true} if to order by comparator, otherwise 
   *        {@code false}.
   * @return Array, potentially empty, of filtered file names. 
   */
  @SuppressWarnings("unchecked")
  public static String[] retrieveFiles(final DirFilter dirFilter,
                                       final AlphabeticComparator alphabeticComparator,
                                       final String directory,
                                       final boolean ordered) {
    log.debug("~retrieveFiles(..) : Determine files to retrieve");
    final File dir = new File(directory);
    if (!dir.isDirectory()) {
      final String errorMessage = "Directory '" + directory.toString() + "' isn't a directory!!";
      log.warn("~retrieveFiles(..) : " + errorMessage);
      throw new UnsupportedOperationException(errorMessage);
    }

    final String[] listing = dir.list(dirFilter);
    if (ordered) {
      Arrays.sort(listing, alphabeticComparator);
    }

    return listing;
  }

  /**
   * Retrieve job directory.
   * 
   * @param baseDir Base directory.
   * @param appManagerId App manager identifier.
   * @return Job directory (with trailing slash appended).
   */
  public static String retrieveJobDirectory(final String baseDir, final long appManagerId) {
    log.trace("~retrieveJobDirectory(String, String) : From '" + baseDir + "', '" + appManagerId + "'");

    final StringBuffer jobDirectory = new StringBuffer();
    jobDirectory.append(baseDir);
    jobDirectory.append(appManagerId);
    jobDirectory.append(AppIdentifiers.FILE_SEPARATOR);

    final String path = jobDirectory.toString();
    log.trace("~retrieveJobDirectory(String, String) : Job directory is '" + path + "'");

    return path;
  }

  /**
   * Write a file to the temporary directory.
   * 
   * @param dataHandler Data handler containing file content.
   * @param preferredSuffix Optional preferred file extension (defaults to
   *                        {@linkplain FileUtil#defaultTmpFileSuffix}).
   * @return Newly created file.
   * @throws IOException If there's a system problem handling the file!
   */
  public static File writeFileToTmp(final DataHandler dataHandler,
                                    final String preferredSuffix) 
                                    throws IOException {
    final InputStream inputStream = dataHandler.getInputStream();
    final String suffix = preferredSuffix == null ? defaultTmpFileSuffix :
                                                    preferredSuffix;
    final File tmpFile = File.createTempFile(defaultTmpFilePrefix, suffix);
    final OutputStream outputStream = new FileOutputStream(tmpFile);

    IOUtils.copyLarge(inputStream, outputStream);

    IOUtils.closeQuietly(outputStream);
    IOUtils.closeQuietly(inputStream);

    return tmpFile;
  }
}