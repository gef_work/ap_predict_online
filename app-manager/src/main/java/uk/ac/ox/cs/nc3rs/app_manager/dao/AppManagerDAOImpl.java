/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.dao;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.domain.PKResult;
import uk.ac.ox.cs.nc3rs.app_manager.domain.RunStatus;
import uk.ac.ox.cs.nc3rs.app_manager.domain.SimulationResult;
import uk.ac.ox.cs.nc3rs.app_manager.util.SystemUtil;
import uk.ac.ox.cs.nc3rs.app_manager.value.AllSimulationDomainResultsVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.ApPredictVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.DiagnosticsVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.SimulationInfoVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.SimulationResultsVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.type.InvocationSource;
import uk.ac.ox.cs.nc3rs.app_manager.value.type.SimulationType;

/**
 * Data Access Object which provides access to tall the databases which the 
 * application is expected to talk directly to.
 *
 * @author Geoff Williams
 */
@Repository(AppIdentifiers.COMPONENT_JDBC_APP_MANAGER_DAO)
@Transactional(readOnly=true)
public class AppManagerDAOImpl implements AppManagerDAO {

  @Autowired @Qualifier(AppIdentifiers.COMPONENT_JDBC_APP_TEMPLATE)
  private JdbcTemplate appTemplate;

  private static final String atAppManager = "APP_MANAGER";
  private static final String atPKResults = "PK_RESULTS";
  private static final String atRunStatus = "RUN_STATUS";
  private static final String atSimulationResults = "SIMULATION_RESULTS";

  public static final String AC_APP_MANAGER_ID = "APP_MANAGER_ID";

  public static final String AC_A_OBJECT_VALUE = "OBJECT_VALUE";
  public static final String AC_A_MESSAGES = "MESSAGES";
  public static final String AC_A_INFO = "INFO";
  public static final String AC_A_OUTPUT = "OUTPUT";
  public static final String AC_A_PROCESS_ID = "PROCESS_ID";
  private static final String acAInvocationSource = "INVOCATION_SOURCE";
  private static final String acASimulationType = "SIMULATION_TYPE";
  private static final String acADAPD90PercentileNames = "DELTA_APD90_PERCENTILE_NAMES";

  private static final String acRSEntered = "ENTERED_TS";
  private static final String acRSLevel = "RSLEVEL";
  private static final String acRSStatusText = "STATUS_TEXT";

  private static final String acSRReference = "REFERENCE";
  private static final String acSRApTimes = "AP_TIMES";
  private static final String acSRApVoltages = "AP_VOLTAGES";
  private static final String acSRAPD90 = "APD90";
  private static final String acSRDeltaAPD90 = "DELTA_APD90";
  private static final String acSRQNet = "QNET";

  private static final String acPKTimepoint = "TIMEPOINT";
  private static final String acPKAPD90s = "APD90S";

  private static final String[] appManagerColumnNames = new String[] { AC_A_OBJECT_VALUE, 
                                                                       acAInvocationSource,
                                                                       acASimulationType };

  private static final String[] statusColumnNames = new String[] { AC_APP_MANAGER_ID,
                                                                   acRSLevel,
                                                                   acRSStatusText};

  private static final String comma = ",";
  private static final String sqlDelete = "DELETE ";
  private static final String sqlFrom = " FROM ";
  private static final String sqlQM = "=?";
  private static final String sqlSelect = "SELECT ";
  private static final String sqlSet = " SET ";
  private static final String sqlUpdate = "UPDATE ";
  private static final String sqlWhere = " WHERE ";

  private static final String aDelete = 
    sqlDelete.concat(sqlFrom).concat(atAppManager)
             .concat(sqlWhere).concat(AC_APP_MANAGER_ID).concat("='%d'");

  private static final String aInsert = insertSQL(atAppManager, appManagerColumnNames);

  private static final String aSelectObjectValue = 
    sqlSelect.concat(AC_A_OBJECT_VALUE).concat(sqlFrom).concat(atAppManager)
             .concat(sqlWhere).concat(AC_APP_MANAGER_ID).concat(sqlQM);
  
  private static final String aSelectDiagnostics =
    sqlSelect.concat(AC_A_INFO).concat(comma).concat(AC_A_OUTPUT)
             .concat(sqlFrom).concat(atAppManager)
             .concat(sqlWhere).concat(AC_APP_MANAGER_ID).concat(sqlQM);

  private static final String aSelectMessages =
    sqlSelect.concat(AC_A_MESSAGES)
             .concat(sqlFrom).concat(atAppManager)
             .concat(sqlWhere).concat(AC_APP_MANAGER_ID).concat(sqlQM);

  private static final String aSelectProcessID =
    sqlSelect.concat(AC_A_PROCESS_ID)
             .concat(sqlFrom).concat(atAppManager)
             .concat(sqlWhere).concat(AC_APP_MANAGER_ID).concat(sqlQM);

  private static final String aSelectSimulationInfo =
    sqlSelect.concat(acAInvocationSource).concat(comma).concat(acASimulationType)
             .concat(sqlFrom).concat(atAppManager)
             .concat(sqlWhere).concat(AC_APP_MANAGER_ID).concat(sqlQM);

  private static final String aSelectDeltaAPD90PercentileNames =
      sqlSelect.concat(acADAPD90PercentileNames)
               .concat(sqlFrom).concat(atAppManager)
               .concat(sqlWhere).concat(AC_APP_MANAGER_ID).concat(sqlQM);

  private static final String aUpdateDeltaAPD90PercentileNames = 
    sqlUpdate.concat(atAppManager)
             .concat(sqlSet).concat(acADAPD90PercentileNames).concat(sqlQM)
             .concat(sqlWhere).concat(AC_APP_MANAGER_ID).concat(sqlQM);

  private static final String aUpdateDiagnostics =
    sqlUpdate.concat(atAppManager)
             .concat(sqlSet).concat(AC_A_INFO).concat(sqlQM).concat(comma).concat(AC_A_OUTPUT).concat(sqlQM)
             .concat(sqlWhere).concat(AC_APP_MANAGER_ID).concat(sqlQM);

  private static final String aUpdateMessages =
    sqlUpdate.concat(atAppManager)
             .concat(sqlSet).concat(AC_A_MESSAGES).concat(sqlQM)
             .concat(sqlWhere).concat(AC_APP_MANAGER_ID).concat(sqlQM);

  private static final String aUpdateProcessID = 
    sqlUpdate.concat(atAppManager)
             .concat(sqlSet).concat(AC_A_PROCESS_ID).concat(sqlQM)
             .concat(sqlWhere).concat(AC_APP_MANAGER_ID).concat(sqlQM);

  private static final String rsDelete =
    sqlDelete.concat(sqlFrom).concat(atRunStatus)
             .concat(sqlWhere).concat(AC_APP_MANAGER_ID).concat("='%d'");

  private static final String rsInsert = insertSQL(atRunStatus, statusColumnNames);

  private static final String rsSelectStatus = 
    sqlSelect.concat("*").concat(sqlFrom).concat(atRunStatus)
             .concat(sqlWhere).concat(AC_APP_MANAGER_ID).concat(sqlQM)
                              .concat(" ORDER BY ").concat(acRSEntered).concat(" DESC");

  private static final String srDelete =
    sqlDelete.concat(sqlFrom).concat(atSimulationResults)
             .concat(sqlWhere).concat(AC_APP_MANAGER_ID).concat("='%d'");

  private static final String srInsert =
    "INSERT INTO " + atSimulationResults +
    " (" + AC_APP_MANAGER_ID + comma + acSRReference + comma + acSRApTimes + comma
         + acSRApVoltages + comma + acSRAPD90 + comma + acSRDeltaAPD90 + comma
         + acSRQNet + ") VALUES (?,?,?,?,?,?,?)";

  private static final String srSelectSimResults = 
    sqlSelect.concat("*").concat(sqlFrom).concat(atSimulationResults)
             .concat(sqlWhere).concat(AC_APP_MANAGER_ID).concat(sqlQM);

  private static final String pkDelete =
      sqlDelete.concat(sqlFrom).concat(atPKResults)
               .concat(sqlWhere).concat(AC_APP_MANAGER_ID).concat("='%d'");

  private static final String pkInsert =
    "INSERT INTO " + atPKResults +
    " (" + AC_APP_MANAGER_ID + comma + acPKTimepoint + comma + acPKAPD90s + ")" +
    "  VALUES (?,?,?)";

  private static final String srSelectPKResults = 
      sqlSelect.concat("*").concat(sqlFrom).concat(atPKResults)
               .concat(sqlWhere).concat(AC_APP_MANAGER_ID).concat(sqlQM);

  private static final Log log = LogFactory.getLog(AppManagerDAOImpl.class);

  private static String insertSQL(final String table, final String[] columnNames) {
    final StringBuffer sql = new StringBuffer();
    sql.append("INSERT INTO ").append(table).append(" ").append(columnData(columnNames));
    return sql.toString();
  }

  private static String columnData(final String[] columnNames) {
    assert columnNames != null && columnNames.length > 0 : "Must be at least one column name";
    return columnNames(columnNames).concat(valuePlaceholders(columnNames.length));
  }

  private static String columnNames(final String[] columnNames) {
    final StringBuffer columnText = new StringBuffer();
    columnText.append("(").append(StringUtils.join(Arrays.asList(columnNames), comma)).append(")");
    return columnText.toString();
  }

  private static String valuePlaceholders(final int valueCount) {
    final StringBuffer valuesBuffer = new StringBuffer();
    valuesBuffer.append(" VALUES (?");
    final int valueLimit = valueCount - 1;
    for (int idx = 1; idx <= valueLimit; idx++) {
      valuesBuffer.append(",?");
    }
    valuesBuffer.append(")");
    return valuesBuffer.toString();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO#addStatus(uk.ac.ox.cs.nc3rs.app_manager.domain.RunStatus)
   */
  @Override
  @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
  public void addStatus(final RunStatus runStatus) {
    final long appManagerId = runStatus.getId();
    log.debug("~addStatus() : [" + appManagerId + "] : Invoked.");

    try {
      // Trigger DataAccessException if no record in APP_MANAGER table for identifier.
      retrieveSimulationMessages(appManagerId);

      // TODO : Find better way to ignore status requests once simulation data purged.
      appTemplate.update(rsInsert,
                         new Object[] { appManagerId, runStatus.getLevel(),
                                        runStatus.getStatus() });

    } catch (DataAccessException dae) {
      // Status reports (e.g. progress file can't be found) can arrive after simulation data purged.
      log.debug("~addStatus() : [" + appManagerId + "] : Assuming DataAccessException indicating simulation data purged."); 
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO#createManagerRecord(java.lang.Object, uk.ac.ox.cs.nc3rs.app_manager.value.type.InvocationSource, uk.ac.ox.cs.nc3rs.app_manager.value.type.SimulationType)
   */
  @Override
  @Transactional(readOnly=false)
  public Long createManagerRecord(final Object invocationObject,
                                  final InvocationSource invocationSource,
                                  final SimulationType simulationType) {
    log.debug("~createManagerRecord() : Invoked.");

    final KeyHolder keyHolder = new GeneratedKeyHolder();
    appTemplate.update(
      new PreparedStatementCreator() {
        public PreparedStatement createPreparedStatement(Connection connection)
                                                         throws SQLException {
          final PreparedStatement preparedStatement = connection.prepareStatement(aInsert,
                                                                                  new String[] { AC_APP_MANAGER_ID });
          preparedStatement.setObject(1, SystemUtil.apPredictSerialize((ApPredictVO) invocationObject));
          preparedStatement.setString(2, invocationSource == null ? null : invocationSource.toString());
          preparedStatement.setString(3, simulationType == null ? null : simulationType.toString());

          return preparedStatement;
        }
      },
      keyHolder);
    final long newId = keyHolder.getKey().longValue();
    log.debug("~createManagerRecord() : [" + newId + "] : Id created.");

    return newId;
  }

  // RowMapper for PKResult.
  private class PKResultRowInfoMapper implements RowMapper<PKResult> {
    public PKResult mapRow(final ResultSet resultSet, final int rowNumber) 
                           throws SQLException {
      final PKResult pkResult = new PKResult(resultSet.getLong(AC_APP_MANAGER_ID),
                                             resultSet.getBigDecimal(acPKTimepoint),
                                             resultSet.getString(acPKAPD90s));

      return pkResult;
    }
  }

  // RowMapper for SimulationResult.
  private class SimulationResultRowInfoMapper implements RowMapper<SimulationResult> {
    public SimulationResult mapRow(final ResultSet resultSet, final int rowNumber) 
                                   throws SQLException {
      /* In a HSQL environment stripTrailingZeros() makes the values look nicer
         except for the value 0, which is converted to 0E-32! */
      final SimulationResult simulationResult = new SimulationResult(resultSet.getLong(AC_APP_MANAGER_ID),
                                                                     resultSet.getBigDecimal(acSRReference)
                                                                              .stripTrailingZeros(),
                                                                     resultSet.getString(acSRApTimes),
                                                                     resultSet.getString(acSRApVoltages),
                                                                     resultSet.getString(acSRAPD90),
                                                                     resultSet.getString(acSRDeltaAPD90),
                                                                     resultSet.getString(acSRQNet));
      log.debug("~SimulationResultRowInfoMapper() : ".concat(simulationResult.toString()));

      return simulationResult;
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO#purgeExistingSimulation(long)
   */
  @Override
  @Transactional(readOnly=false)
  public void purgeExistingSimulation(final long appManagerId) {
    log.debug("~purgeExistingSimulation() : [" + appManagerId + "] : Invoked.");

    final String buADelete = String.format(aDelete, appManagerId);
    final String buRSDelete = String.format(rsDelete, appManagerId);
    final String buSRDelete = String.format(srDelete, appManagerId);
    final String buPKDelete = String.format(pkDelete, appManagerId);

    appTemplate.batchUpdate(new String[] { buADelete, buRSDelete, buSRDelete,
                                           buPKDelete });
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO#retrieveAllSimulationResults(long)
   */
  @Override
  public AllSimulationDomainResultsVO retrieveAllSimulationResults(final long appManagerId) {
    log.debug("~retrieveAllSimulationResults() : [" + appManagerId + "] : Invoked.");

    final List<SimulationResult> simulationResults = appTemplate.query(srSelectSimResults,
                                                                       new Object[] { appManagerId },
                                                                       new SimulationResultRowInfoMapper());

    final List<PKResult> pkResults = appTemplate.query(srSelectPKResults,
                                                       new Object[] { appManagerId },
                                                       new PKResultRowInfoMapper());

    final String deltaAPD90PercentileNames = appTemplate.queryForObject(aSelectDeltaAPD90PercentileNames,
                                                                        String.class,
                                                                        new Object[] { appManagerId });

    return new AllSimulationDomainResultsVO(pkResults, simulationResults,
                                            deltaAPD90PercentileNames);
  }

  private class StatusVORowInfoMapper implements RowMapper<RunStatus> {
    public RunStatus mapRow(final ResultSet resultSet, final int rowNumber)
                           throws SQLException {
      final RunStatus runStatus = new RunStatus(resultSet.getInt(AC_APP_MANAGER_ID),
                                                DATE_FORMAT.format(resultSet.getTimestamp(acRSEntered)),
                                                resultSet.getString(acRSLevel),
                                                resultSet.getString(acRSStatusText));

      return runStatus;
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO#retrieveAllStatuses(long)
   */
  @Override
  public List<RunStatus> retrieveAllStatuses(final long appManagerId) {
    log.debug("~retrieveAllStatuses() : [" + appManagerId + "] : Invoked.");

    final List<RunStatus> statuses = appTemplate.query(rsSelectStatus,
                                                       new Object[] { appManagerId },
                                                       new StatusVORowInfoMapper());

    log.debug("~retrieveAllStatuses() : Status count '" + statuses.size() + "'.");

    return statuses;
  }

  // RowMapper for DiagnosticsVO.
  private class DiagnosticsVORowInfoMapper implements RowMapper<DiagnosticsVO> {
    public DiagnosticsVO mapRow(final ResultSet resultSet, final int rowNumber) 
                                throws SQLException {
      final DiagnosticsVO diagnosticsVO = new DiagnosticsVO(resultSet.getString(AC_A_INFO),
                                                            resultSet.getString(AC_A_OUTPUT));

      return diagnosticsVO;
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO#retrieveDiagnostics(long)
   */
  @Override
  public DiagnosticsVO retrieveDiagnostics(final long appManagerId) {
    log.debug("~retrieveDiagnostics() : [" + appManagerId + "] : Invoked.");

    DiagnosticsVO diagnostics = null;
    try {
      diagnostics = appTemplate.queryForObject(aSelectDiagnostics,
                                               new Object[] { appManagerId },
                                               new DiagnosticsVORowInfoMapper());
    } catch (DataAccessException dae) {
      // probably not a problem, diagnostics data only persisted once simulation finishes.
      log.warn("~retrieveDiagnostics() : No record found for '" + appManagerId + "' : Message : " + dae.getMessage() + "'.");
    }

    if (diagnostics != null) {
      log.trace("~retrieveDiagnostics() : There was a record in the table... going to check for diagnostics data to use.");
      if (diagnostics.getInfo() == null && diagnostics.getOutput() == null) {
        log.debug("~retrieveDiagnostics() : No diagnostics data for '" + appManagerId + "'.");
        diagnostics = null;
      }
    }

    return diagnostics;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO#retrieveLatestStatus(long)
   */
  @Override
  public RunStatus retrieveLatestStatus(final long appManagerId) {
    log.debug("~retrieveLatestStatus() : : [" + appManagerId + "] : Invoked.");

    return retrieveAllStatuses(appManagerId).get(0);
  }

  private class ApPredictRowMapper implements RowMapper<ApPredictVO> {
    /* (non-Javadoc)
     * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
     */
    @Override
    public ApPredictVO mapRow(final ResultSet resultSet, final int rowNumber)
                              throws SQLException {
      final Blob apPredictBlob = resultSet.getBlob(AC_A_OBJECT_VALUE);
      return SystemUtil.apPredictDeserialize(apPredictBlob.getBytes(1, (int) apPredictBlob.length()));
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO#retrieveObjectValue(long)
   */
  @Override
  public ApPredictVO retrieveObjectValue(final long appManagerId) {
    log.debug("~retrieveObjectValue() : [" + appManagerId + "] : Invoked.");

    return appTemplate.queryForObject(aSelectObjectValue, 
                                      new Object[] { appManagerId },
                                      new ApPredictRowMapper());
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO#retrieveSimulationMessages(long)
   */
  @Override
  public String retrieveSimulationMessages(final long appManagerId) {
    log.debug("~retrieveSimulationMessages() : [" + appManagerId + "] : Invoked.");

    final String messages = appTemplate.queryForObject(aSelectMessages,
                                                       String.class,
                                                       new Object[] { appManagerId });
    log.trace("~retrieveSimulationMessages() : Messages '" + messages + "'");

    return messages;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO#retrieveSimulationProcessId(long)
   */
  @Override
  public String retrieveSimulationProcessId(final long appManagerId) {
    log.debug("~retrieveSimulationProcessId() : [" + appManagerId + "] : Invoked.");

    String processId = null;
    try {
      processId = appTemplate.queryForObject(aSelectProcessID, String.class,
                                             new Object[] {  appManagerId });
    } catch (DataAccessException e) {
      log.warn("~retrieveSimulationProcessId() : [" + appManagerId + "] : Exception retrieving process id '" + e.getMessage() + "'.");
    }

    return processId;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO#saveSimulationDiagnostics(long, java.lang.String, java.lang.String)
   */
  @Override
  @Transactional(readOnly=false)
  public void saveSimulationDiagnostics(final long appManagerId,
                                        final String info, final String output) {
    log.debug("~saveSimulationDiagnostics() : [" + appManagerId + "] : Invoked.");

    appTemplate.update(aUpdateDiagnostics,
                       new Object [] { info, output, appManagerId});

  }

  // RowMapper for SimulationInfoVO.
  private class SimulationInfoVORowInfoMapper implements RowMapper<SimulationInfoVO> {
    public SimulationInfoVO mapRow(final ResultSet resultSet,
                                   final int rowNumber) throws SQLException {
      final String invocationSourceStr = resultSet.getString(acAInvocationSource);
      final String simulationTypeStr = resultSet.getString(acASimulationType);

      final InvocationSource invocationSource = InvocationSource.valueOf(invocationSourceStr);
      final SimulationType simulationType = SimulationType.valueOf(simulationTypeStr);

      return new SimulationInfoVO(invocationSource, simulationType);
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO#retrieveSimulationInfo(long)
   */
  @Override
  public SimulationInfoVO retrieveSimulationInfo(final long appManagerId) {
    log.debug("~retrieveSimulationInfo() : [" + appManagerId + "] : Invoked.");

    SimulationInfoVO simulationInfo = null;
    try {
      simulationInfo = appTemplate.queryForObject(aSelectSimulationInfo,
                                                  new Object[] { appManagerId },
                                                  new SimulationInfoVORowInfoMapper());
    } catch (DataAccessException dae) {
      log.warn("~retrieveSimulationInfo() : [" + appManagerId + "] : No record found. Message : '" + dae.getMessage() + "'.");
    }

    return simulationInfo;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO#saveSimulationMessages(long, java.lang.String)
   */
  @Override
  @Transactional(readOnly=false)
  public void saveSimulationMessages(final long appManagerId,
                                     final String messages) {
    log.debug("~saveSimulationMessages() : [" + appManagerId + "] : Invoked.");

    appTemplate.update(aUpdateMessages,
                       new Object[] { messages, appManagerId });

    final String status = "Messages generated for simulation";
    final RunStatus runStatus = new RunStatus(appManagerId, null,
                                              RunStatus.WARN_PREFIX, status);
    addStatus(runStatus);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO#saveSimulationProcessId(long, java.lang.String)
   */
  @Override
  @Transactional(readOnly=false)
  public void saveSimulationProcessId(final long appManagerId,
                                      final String processId) {
    log.debug("~saveSimulationProcessId() : [" + appManagerId + "][" + processId + "] : Invoked.");

    appTemplate.update(aUpdateProcessID, new Object[] { processId,
                                                        appManagerId });
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO#saveSimulationResults(long, java.util.List, java.util.Map, java.lang.String)
   */
  @Override
  @Transactional(readOnly=false)
  public void saveSimulationResults(final long appManagerId,
                                    final List<SimulationResultsVO> simulationResults,
                                    final Map<String, String> pkResults,
                                    final String deltaAPD90PercentileNames) {
    final String identifiedLogPrefix = "~saveSimulationResults : [" + appManagerId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    final List<String> cmpdConcs = new ArrayList<String>();
    appTemplate.batchUpdate(srInsert, new BatchPreparedStatementSetter() {
      public int getBatchSize() {
        return simulationResults.size();
      }

      public void setValues(final PreparedStatement preparedStatement,
                            final int idx) throws SQLException {
        final SimulationResultsVO results = simulationResults.get(idx);
        preparedStatement.setLong(1, appManagerId);
        final String cmpdConc = results.getCmpdConc();
        cmpdConcs.add(cmpdConc);
        preparedStatement.setString(2, cmpdConc);
        preparedStatement.setString(3, results.getTimes());
        preparedStatement.setString(4, results.getVoltages());
        preparedStatement.setString(5, results.getAPD90());
        preparedStatement.setString(6, results.getDeltaAPD90());
        preparedStatement.setString(7, results.getQNet());
      }
    });

    if (deltaAPD90PercentileNames != null) {
      log.debug(identifiedLogPrefix.concat("Writing DeltaAPD90 %ile names."));
      appTemplate.update(aUpdateDeltaAPD90PercentileNames,
                         new Object[] { deltaAPD90PercentileNames,
                                        appManagerId });
    }

    if (pkResults != null && !pkResults.isEmpty()) {
      log.debug(identifiedLogPrefix.concat("Writing PK results."));

      final List<String> timepoints = new ArrayList<String>(pkResults.keySet());
      appTemplate.batchUpdate(pkInsert, new BatchPreparedStatementSetter() {
        public int getBatchSize() {
          return pkResults.size();
        }

        public void setValues(final PreparedStatement preparedStatement,
                              final int idx) throws SQLException {
          final String timepoint = timepoints.get(idx);
          final String apd90s = pkResults.get(timepoint);

          preparedStatement.setLong(1, appManagerId);
          preparedStatement.setString(2, timepoint);
          preparedStatement.setString(3, apd90s);
        }
      });
    }

    // Update status details.
    String restrictedCmpdConcs = StringUtils.join(cmpdConcs, comma);
    if (restrictedCmpdConcs.length() > 65535) {
      restrictedCmpdConcs = restrictedCmpdConcs.substring(0, 65531) + "...";
    }
    final String status = "Simulation results saved for '" + appManagerId + "'";
    final RunStatus runStatus = new RunStatus(appManagerId, null,
                                              RunStatus.DEBUG_PREFIX, status);
    addStatus(runStatus);
  }
}