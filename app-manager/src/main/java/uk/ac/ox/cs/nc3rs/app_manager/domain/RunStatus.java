/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.domain;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Status message.
 *
 * @author Geoff Williams
 */
public class RunStatus {

  /** "trace" status level */
  public static final String TRACE_PREFIX = "T";
  /** "debug" status level */
  public static final String DEBUG_PREFIX = "D";
  /** "info" status level */
  public static final String INFO_PREFIX = "I";
  /** "warn" status level */
  public static final String WARN_PREFIX = "W";
  /** "error" status level */
  public static final String ERROR_PREFIX = "E";
  /** "fatal" status level */
  public static final String FATAL_PREFIX = "F";

  private final long id;
  private final String timestamp;
  private final String status;
  private final String level;

  private static final Log log = LogFactory.getLog(RunStatus.class);

  /**
   * Initialising constructor.
   * 
   * @param id App Manager identifier.
   * @param timestamp Timestamp.
   * @param level Level, e.g. {@value #DEBUG_PREFIX}, as per
   *        {@linkplain RunStatus#DEBUG_PREFIX}
   * @param status Status message (restrict length to db field max)
   */
  public RunStatus(final long id, final String timestamp, final String level,
                   final String status) {
    this.id = id;
    this.timestamp = timestamp;
    this.level = level;
    this.status = status;
    log.debug("~RunStatus() : ".concat(toString()));
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "RunStatus [id=" + id + ", timestamp=" + timestamp + ", status="
        + status + ", level=" + level + "]";
  }

  /**
   * Retrieve the persistence identifier.
   * 
   * @return Persistence identifier.
   */
  public long getId() {
    return id;
  }

  /**
   * Retrieve the time stamp.
   * 
   * @return Timestamp.
   */
  public String getTimestamp() {
    return timestamp;
  }

  /**
   * Retrieve the status text.
   * 
   * @return The status text.
   */
  public String getStatus() {
    return status;
  }

  /**
   * Retrieve the status level, e.g. {@linkplain RunStatus#DEBUG_PREFIX}
   * (although use of static vars is not enforced!)
   * 
   * @return The status level.
   */
  public String getLevel() {
    return level;
  }
}