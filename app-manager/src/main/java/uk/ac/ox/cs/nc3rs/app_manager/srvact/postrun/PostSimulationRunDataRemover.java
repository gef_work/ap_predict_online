/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.postrun;

import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.Selectors;
import org.apache.commons.vfs2.VFS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO;
import uk.ac.ox.cs.nc3rs.app_manager.value.ApPredictVO;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.BusinessDataUploadCompletedRequest;

/**
 * Service activator which removes all evidence of a simulation having run,
 * including removing the temporary files (e.g. PK, CellML) if any used.
 * 
 * @author Geoff Williams
 */
@Component
public class PostSimulationRunDataRemover {

  @Autowired @Qualifier(AppIdentifiers.COMPONENT_JDBC_APP_MANAGER_DAO)
  private AppManagerDAO appManagerDAO;

  private static final Log log = LogFactory.getLog(PostSimulationRunDataRemover.class);

  /**
   * Remove the simulation data from the persisted records, and if there are
   * temporary files associated with the situation, remove those from the
   * file system too.
   * 
   * @param businessDataUploadCompletedRequest Incoming SOAP request.
   */
  @ServiceActivator
  public void removeSimulationData(final BusinessDataUploadCompletedRequest businessDataUploadCompletedRequest) {
    log.debug("~int.removeSimulationdata() : Invoked.");

    final long appManagerId = businessDataUploadCompletedRequest.getAppManagerId();

    final ApPredictVO apPredict = appManagerDAO.retrieveObjectValue(appManagerId);
    if (apPredict != null) {
      removeFiles(appManagerId, apPredict.retrieveRemovableFileLocations());
    }

    // Scrub persisted simulation data.
    appManagerDAO.purgeExistingSimulation(appManagerId);
  }

  private void removeFiles(final long appManagerId,
                           final Set<String> fileLocations) {
    final String logPrefix = "~removeFiles() : [" + appManagerId + "] : ";
    log.debug(logPrefix.concat("Invoked."));

    if (!fileLocations.isEmpty()) {
      try {
        final FileSystemManager fileSystemManager = VFS.getManager();
        for (final String fileLocation : fileLocations) {
          log.debug(logPrefix.concat("About to remove file at '" + fileLocation + "'."));
          final FileObject fileObject = fileSystemManager.resolveFile(fileLocation);
          fileObject.delete(Selectors.SELECT_SELF);
        }
      } catch (FileSystemException e) {
        // Assuming that it's not there so not concerned about it!
        log.warn(logPrefix.concat("FS Exception during file deletion : '" + e.getMessage() + "'."));
      }
    }
  }
}