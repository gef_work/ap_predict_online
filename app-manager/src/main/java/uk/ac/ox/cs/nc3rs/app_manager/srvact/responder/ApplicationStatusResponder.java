/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.responder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.app_manager.business.Running;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.ApplicationStatusRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.ApplicationStatusResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.ObjectFactory;

/**
 * Responder to a request for the application's status.
 * 
 * @author Geoff Williams
 */
@Component
public class ApplicationStatusResponder {

  private Integer envInvocationLimit;

  // As defined in the app_manager.xsd
  private static final String dataWorkload = "Workload";

  private static final ObjectFactory objectFactory = new ObjectFactory();

  private static final Log log = LogFactory.getLog(ApplicationStatusResponder.class);

  /**
   * Handle the incoming SOAP request object and provide appropriate response.
   * 
   * @param applicationStatusRequest Incoming SOAP request.
   * @return Outgoing SOAP response.
   */
  @ServiceActivator
  public ApplicationStatusResponse statusRespond(final ApplicationStatusRequest applicationStatusRequest) {
    log.debug("~int.statusRespond() : Invoked.");

    final String data = applicationStatusRequest.getData();

    final ApplicationStatusResponse applicationStatusResponse = objectFactory.createApplicationStatusResponse();

    String statusText = "Unknown workload data request of '" + data + "'";
    if (dataWorkload.equals(data)) {
      final int runningCount = Running.count();
      final String invocationLimit = Integer.valueOf(envInvocationLimit).toString();
      statusText = new Integer(runningCount).toString().concat(" of ").concat(invocationLimit).concat(" (max) processes");
    }

    applicationStatusResponse.setStatus(statusText);

    return applicationStatusResponse;
  }

  /**
   * Assign the invocation limit, e.g. 8
   * 
   * @param envInvocationLimit Invocation limit.
   */
  // Spring-injected.
  @Value("${invocation.limit}")
  public void setEnvInvocationLimit(final Integer envInvocationLimit) {
    this.envInvocationLimit = envInvocationLimit;
  }
}