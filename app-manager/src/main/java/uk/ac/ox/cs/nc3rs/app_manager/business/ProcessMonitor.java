/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.business;

import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.ws.app_manager.proxy.AppServicesProxy;

/**
 * Periodic checker of the native system process status.
 *
 * @author Geoff Williams
 */
@Component(AppIdentifiers.COMPONENT_PROCESS_MONITOR)
@Scope("prototype")
public class ProcessMonitor {

  @Autowired @Qualifier(AppIdentifiers.COMPONENT_APP_SERVICES_PROXY)
  private AppServicesProxy appServices;

  private Timer daemonTimer;
  private int appManagerId;
  private String processId;
  private String processFileName;

  public static final String PROCESS_PREFIX = "PROC: ";
  /** Process ended status code - referenced in appCtx.int.statusProcessing.xml */
  public static final String PROCESS_ENDED_STATUS_CODE = "D11";
  /** Process failed status code - referenced in appCtx.int.statusProcessing.xml */
  public static final String PROCESS_FAILED_STATUS_CODE = "E20";

  private static final Log log = LogFactory.getLog(ProcessMonitor.class);

  ProcessMonitor() {
    this.daemonTimer = new Timer(true);
  }

  //
  private class QueryTask extends TimerTask {
    public void run() {
      log.debug("~run() : Invoked. Looking for '" + processFileName + "'");

      FileReader fileReader = null;
      try {
        fileReader = new FileReader(processFileName);
      } catch (final FileNotFoundException fileNotFoundException) {
        log.debug("~run() : About to cancel the timer");
        daemonTimer.cancel();

        log.debug("~run() : File not found! Assuming processing finished - sending D11 to web service");
        final String status = PROCESS_PREFIX.concat(" _END");
        log.debug("~run() : " + PROCESS_ENDED_STATUS_CODE + " status to set '" + status + "'");
        final String statusCode = PROCESS_ENDED_STATUS_CODE;

        appServices.assignProcessStatus(appManagerId, status, statusCode,
                                        "Host", "Source", processId);
      }

      if (fileReader != null) {
        String status = null;
        final Scanner scanner = new Scanner(fileReader);
        if (scanner != null) {
          boolean foundState = false;
          try {
            while(scanner.hasNextLine() && !foundState) {
              final String trimmedLine = scanner.nextLine().trim();
              if (trimmedLine.startsWith("State:")) {
                final String[] lineContents = trimmedLine.split("\t");
                status = PROCESS_PREFIX.concat(" ").concat(lineContents[1]);
                foundState = true;
              }
            }
          } finally {
            log.trace("~run() : Closing scanner.");
            scanner.close();
          }

          if (!foundState) {
            final String warnMessage = "State not found in '" + processFileName + "'";
            log.warn("~run() : " + warnMessage);
            System.out.println(warnMessage);
          }
        }
        try {
          log.trace("~run() : Closing filereader.");
          fileReader.close();
        } catch (IOException e) {
          log.warn("~run() : Exception closing FileReader '" + e.getMessage() + "'.");
          e.printStackTrace();
        }
        if (status != null) {
          log.debug("~run() : D10 status to set '" + status + "'");
          final String statusCode = "D10";
          appServices.assignProcessStatus(appManagerId, status,
                                          statusCode, "Host", "Source", processId);
        }
      }
    }
  }

  /**
   * @param appManagerId the appManagerId to set
   */
  public void setAppManagerId(int appManagerId) {
    this.appManagerId = appManagerId;
  }

  /**
   * 
   * @param monitorFrequency Frequency of monitoring.
   */
  public void setSchedule(final String monitorFrequency) {
    this.daemonTimer.schedule(new QueryTask(), 2000,
                              new Integer(monitorFrequency) * 1000);
  }

  /**
   * @param processId the processId to set
   */
  public void setProcessId(String processId) {
    this.processId = processId;
    setProcessFileName("/proc/".concat(processId).concat("/status"));
  }

  /**
   * @param processFileName the processFileName to set
   */
  public void setProcessFileName(String processFileName) {
    this.processFileName = processFileName;
  }
}