/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.app_manager.srvact.prerun;

import java.io.File;
import java.io.IOException;

import javax.activation.DataHandler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.app_manager.AppIdentifiers;
import uk.ac.ox.cs.nc3rs.app_manager.business.Running;
import uk.ac.ox.cs.nc3rs.app_manager.dao.AppManagerDAO;
import uk.ac.ox.cs.nc3rs.app_manager.exception.MessageKeys;
import uk.ac.ox.cs.nc3rs.app_manager.util.FileUtil;
import uk.ac.ox.cs.nc3rs.app_manager.value.ApPredictVO;
import uk.ac.ox.cs.nc3rs.app_manager.value.type.InvocationSource;
import uk.ac.ox.cs.nc3rs.app_manager.value.type.SimulationType;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.ApPredictRunRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.ApPredictRunResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.ObjectFactory;
import uk.ac.ox.cs.nc3rs.app_manager.ws.schema.jaxb.ApPredictRunResponse.Response;

/**
 * Receive a SOAP object instruction to run an ApPredict simulation.
 *
 * @author Geoff Williams
 */
@Component
public class ApPredictRunRequestReceiver {

  @Autowired @Qualifier(AppIdentifiers.COMPONENT_JDBC_APP_MANAGER_DAO)
  private AppManagerDAO appManagerDAO;

  private Integer envInvocationLimit;

  private static final int unlimitedInvocations = 0;
  // ApPredict requires a CellML file to have a particular suffix!
  private static final String cellMLSuffix = ".cellml";

  private static int invocationLimit = -1;

  private final ObjectFactory objectFactory = new ObjectFactory();

  private static final Log log = LogFactory.getLog(ApPredictRunRequestReceiver.class);

  /**
   * Handle a request to run an ApPredict.
   *  
   * @param apPredictRunRequest SOAP request object.
   * @return SOAP response object.
   * @throws IllegalStateException If problems writing CellML or PK files to
   *                               disk.
   */
  @ServiceActivator
  public ApPredictRunResponse handleApPredictRequest(final ApPredictRunRequest apPredictRunRequest)
                                                     throws IllegalStateException {
    log.debug("~int.handleApPredictRequest() : Invoked.");

    // Store CellML File if present.
    File cellMLFile = null;
    final DataHandler cellMLFileDataHandler = apPredictRunRequest.getCellMLFile();
    if (cellMLFileDataHandler != null) {
      try {
        cellMLFile = FileUtil.writeFileToTmp(cellMLFileDataHandler,
                     cellMLSuffix);
      } catch (IOException e) {
        final String errorMessage = "CellML content from DataHandler : IOException '" + e.getMessage() + "'.";
        log.error("~int.handleApPredictRequest() : ".concat(errorMessage));
        throw new IllegalStateException(errorMessage);
      }
    }

    // Store PK File if present.
    String pkFileLocation = null;
    final DataHandler pkFileDataHandler = apPredictRunRequest.getPKFile();
    if (pkFileDataHandler != null) {
      try {
        pkFileLocation = FileUtil.writeFileToTmp(pkFileDataHandler, null)
                                 .getAbsolutePath();
      } catch (IOException e) {
        final String errorMessage = "PKFile content from DataHandler : IOException '" + e.getMessage() + "'.";
        log.error("~int.handleApPredictRequest() : ".concat(errorMessage));
        throw new IllegalStateException(errorMessage);
      }
    }

    // Transfer request data into a value object (and validate it in the process!)
    final ApPredictVO apPredictVO = new ApPredictVO(apPredictRunRequest,
                                                    pkFileLocation, cellMLFile);

    final InvocationSource invocationSource = InvocationSource.findEnum(apPredictRunRequest.getInvocationSource());

    SimulationType simulationType = SimulationType.REGULAR;
    if (apPredictVO.isPKSimulation()) {
      simulationType = SimulationType.PK;
    }

    final ApPredictRunResponse apPredictRunResponse = objectFactory.createApPredictRunResponse();
    final Response response = objectFactory.createApPredictRunResponseResponse();
    if (alreadyAtCapacity()) {
      // TODO : Response codes need to be understood/accessible by client.
      response.setResponseCode(MessageKeys.AT_CAPACITY);
    } else {
      final long appManagerId = appManagerDAO.createManagerRecord(apPredictVO,
                                                                  invocationSource,
                                                                  simulationType);
      Running.addToRunning(appManagerId);
      log.debug("~int.handleApPredictRequest() : App Manager Id '" + appManagerId + "' created.");
      response.setAppManagerId(appManagerId);
    }
    apPredictRunResponse.setResponse(response);

    return apPredictRunResponse;
  }

  private boolean alreadyAtCapacity() {
    // lazy initialisation poss. returning different object if multiple threads, but it's only a number.
    if (invocationLimit == -1) {
      invocationLimit = (envInvocationLimit == null) ? 0 : envInvocationLimit;
    }

    if (invocationLimit == unlimitedInvocations) {
      log.debug("~alreadyAtCapacity() : Unlimited invocations permitted via env.properties configuration.");
      return false;
    }

    final int runningCount = Running.count();
    boolean alreadyAtCapacity = false;
    if (runningCount >= invocationLimit) {
      log.debug("~alreadyAtCapacity() : '" + runningCount + "' currently running, limit is '" + invocationLimit + "'.");
      alreadyAtCapacity = true;
    }

    return alreadyAtCapacity;
  }

  /**
   * Assign the invocation limit, e.g. 8
   * 
   * @param envInvocationLimit Invocation limit.
   */
  // Spring-injected.
  @Value("${invocation.limit}")
  public void setEnvInvocationLimit(final Integer envInvocationLimit) {
    this.envInvocationLimit = envInvocationLimit;
  }
}