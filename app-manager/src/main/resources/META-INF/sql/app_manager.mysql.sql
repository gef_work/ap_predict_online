--DROP TABLE IF EXISTS APP_MANAGER;
--DROP TABLE IF EXISTS RUN_STATUS;
--DROP TABLE IF EXISTS SIMULATION_RESULTS;
--DROP TABLE IF EXISTS PK_RESULTS;

CREATE TABLE IF NOT EXISTS APP_MANAGER (
  APP_MANAGER_ID bigint not null auto_increment,
  OBJECT_VALUE mediumblob,
  /* 'text' === 64k */
  MESSAGES text,
  /* Status used by spring integration to detect new record in table */
  STATUS int default 0,
  INFO mediumtext,
  /* Can hold confidence interval loading data. 'mediumtext' === 16Mb */
  OUTPUT mediumtext,
  PROCESS_ID varchar(10),
  INVOCATION_SOURCE varchar(30),
  SIMULATION_TYPE varchar(30),
  DELTA_APD90_PERCENTILE_NAMES varchar(1000),
  primary key (APP_MANAGER_ID)
) ENGINE=InnoDB#

CREATE TABLE IF NOT EXISTS RUN_STATUS (
  APP_MANAGER_ID bigint,
  ENTERED_TS timestamp default current_timestamp not null,
  RSLEVEL char(1),
  STATUS_TEXT varchar(250),
  INDEX IDX_APP_MANAGER_ID (APP_MANAGER_ID)
) ENGINE=InnoDB#

CREATE TABLE IF NOT EXISTS SIMULATION_RESULTS (
  APP_MANAGER_ID bigint,
  REFERENCE varchar(50),
  AP_TIMES mediumtext,
  AP_VOLTAGES mediumtext,
  APD90 varchar(100),
  DELTA_APD90 varchar(1000),
  QNET varchar(1000),
  CONSTRAINT IDX_UNIQUE_REF_PER_ID UNIQUE (APP_MANAGER_ID, REFERENCE)
) ENGINE=InnoDB#

CREATE TABLE IF NOT EXISTS PK_RESULTS (
  APP_MANAGER_ID bigint,
  TIMEPOINT varchar(50),
  APD90S mediumtext,
  CONSTRAINT IDX_UNIQUE_TIMEPOINT_PER_ID UNIQUE (APP_MANAGER_ID, TIMEPOINT)
) ENGINE=InnoDB#

DROP PROCEDURE IF EXISTS upgrade_db#

CREATE PROCEDURE upgrade_db()
BEGIN
  SELECT CHARACTER_MAXIMUM_LENGTH INTO @apPlasmaConcLen
    FROM information_schema.columns
      WHERE table_schema = DATABASE()
        AND table_name='SIMULATION_RESULTS'
        AND column_name='AP_PLASMA_CONC';

  /* Previous table definitions set plasma conc len to 10. For such installations increase the size
     of existing columns. */
  IF @apPlasmaConcLen = 10 THEN
    ALTER TABLE SIMULATION_RESULTS MODIFY AP_PLASMA_CONC varchar(20);
    ALTER TABLE SIMULATION_RESULTS MODIFY AP_TIMES mediumtext;
    ALTER TABLE SIMULATION_RESULTS MODIFY AP_VOLTAGES mediumtext;
  END IF;

  IF NOT EXISTS (SELECT NULL
                   FROM information_schema.columns
                     WHERE table_schema = DATABASE()
                       AND table_name = 'APP_MANAGER'
                       AND column_name = 'INVOCATION_SOURCE') THEN
    ALTER TABLE APP_MANAGER ADD COLUMN INVOCATION_SOURCE varchar(30),
                            ADD COLUMN SIMULATION_TYPE varchar(30);
  END IF;

  IF NOT EXISTS (SELECT NULL
                   FROM information_schema.columns
                     WHERE table_schema = DATABASE()
                       AND table_name = 'SIMULATION_RESULTS'
                       AND column_name = 'REFERENCE') THEN
    ALTER TABLE SIMULATION_RESULTS DROP INDEX IDX_UNIQUE_CONC_PER_ID;
    ALTER TABLE SIMULATION_RESULTS CHANGE COLUMN AP_PLASMA_CONC REFERENCE varchar(20);
    ALTER TABLE SIMULATION_RESULTS ADD CONSTRAINT IDX_UNIQUE_REF_PER_ID UNIQUE (APP_MANAGER_ID, REFERENCE);
  END IF;

  IF NOT EXISTS (SELECT NULL
                   FROM information_schema.columns
                     WHERE table_schema = DATABASE()
                       AND table_name = 'SIMULATION_RESULTS'
                       AND column_name = 'APD90') THEN
    ALTER TABLE SIMULATION_RESULTS ADD COLUMN APD90 varchar(100) AFTER AP_VOLTAGES;
  END IF;

  SELECT CHARACTER_MAXIMUM_LENGTH INTO @referenceLen
    FROM information_schema.columns
      WHERE table_schema = DATABASE()
        AND table_name='SIMULATION_RESULTS'
        AND column_name='REFERENCE';

  /* Previous table definitions set reference len to 20. For such installations increase the size
     of existing column. */
  IF @referenceLen = 20 THEN
    ALTER TABLE SIMULATION_RESULTS MODIFY REFERENCE varchar(50);
  END IF;

  IF NOT EXISTS (SELECT NULL
                   FROM information_schema.columns
                     WHERE table_schema = DATABASE()
                       AND table_name = 'APP_MANAGER'
                       AND column_name = 'DELTA_APD90_PERCENTILE_NAMES') THEN
    ALTER TABLE APP_MANAGER ADD COLUMN DELTA_APD90_PERCENTILE_NAMES varchar(1000);
  END IF;

  SELECT CHARACTER_MAXIMUM_LENGTH INTO @deltaAPD90Len
    FROM information_schema.columns
      WHERE table_schema = DATABASE()
        AND table_name='SIMULATION_RESULTS'
        AND column_name='DELTA_APD90';

  /* Previous table definitions set DeltaAPD90 len to 100. For such installations increase the size
     of existing column. */
  IF @deltaAPD90Len = 100 THEN
    ALTER TABLE SIMULATION_RESULTS MODIFY DELTA_APD90 varchar(1000);
  END IF;

  IF NOT EXISTS (SELECT NULL
                   FROM information_schema.columns
                     WHERE table_schema = DATABASE()
                       AND table_name = 'RUN_STATUS'
                       AND column_name = 'RSLEVEL') THEN
    ALTER TABLE RUN_STATUS CHANGE LEVEL RSLEVEL char(1);
  END IF;

  IF NOT EXISTS (SELECT NULL
                   FROM information_schema.columns
                     WHERE table_schema = DATABASE()
                       AND table_name = 'SIMULATION_RESULTS'
                       AND column_name = 'QNET') THEN
    ALTER TABLE SIMULATION_RESULTS ADD COLUMN QNET varchar(1000) null;
  END IF;

  SELECT CHARACTER_MAXIMUM_LENGTH INTO @objectValueLen
    FROM information_schema.columns
      WHERE table_schema = DATABASE()
        AND table_name='APP_MANAGER'
        AND column_name='OBJECT_VALUE';

  /* Previous table definitions set OBJECT_VALUE to blob (i.e. length 65535).
     For such installations increase the size of existing column. */
  IF @objectValueLen = 65535 THEN
    ALTER TABLE APP_MANAGER MODIFY OBJECT_VALUE mediumblob;
  END IF;

  SELECT CHARACTER_MAXIMUM_LENGTH INTO @appManagerInfoLen
    FROM information_schema.columns
      WHERE table_schema = DATABASE()
        AND table_name='APP_MANAGER'
        AND column_NAME='INFO';

  /* Previous table definitions set INFO to text (i.e. length 65535).
     For such installations increase the size of existing column. */
  IF @appManagerInfoLen = 65535 THEN
    ALTER TABLE APP_MANAGER MODIFY INFO mediumtext;
  END IF;
END #

/* If you get "java.sql.SQLException: Thread stack overrun" problems then you may need to increase
   the thread_stack value to something like 256K in your MySQL config file, e.g. my.cnf */
CALL upgrade_db#
