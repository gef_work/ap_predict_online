drop trigger APP_MANAGER_TRIGGER#
drop sequence APP_MANAGER_ID_SEQ#
drop table APP_MANAGER cascade constraints#
drop table RUN_STATUS cascade constraints#
drop table SIMULATION_RESULTS cascade constraints#
drop table PK_RESULTS cascade constraints#

create table APP_MANAGER (
  APP_MANAGER_ID number(38) not null primary key,
  OBJECT_VALUE blob,
  MESSAGES clob,
  STATUS number(1,0) default 0,
  INFO clob,
  OUTPUT clob,
  PROCESS_ID nvarchar2(10),
  INVOCATION_SOURCE nvarchar2(30),
  SIMULATION_TYPE nvarchar2(30),
  DELTA_APD90_PERCENTILE_NAMES nvarchar2(1000)
)#
create sequence APP_MANAGER_ID_SEQ start with 1 increment by 1 nomaxvalue#
create or replace trigger APP_MANAGER_TRIGGER
  before insert on APP_MANAGER
  for each row
begin
  select APP_MANAGER_ID_SEQ.nextval into :new.APP_MANAGER_ID from dual;
end;#

create table RUN_STATUS (
  APP_MANAGER_ID number(38),
  ENTERED_TS date default sysdate,
  RSLEVEL nvarchar2(1),
  STATUS_TEXT nvarchar2(250)
)#
create index IDX_APP_MANAGER_ID on RUN_STATUS (APP_MANAGER_ID)#

create table SIMULATION_RESULTS (
  APP_MANAGER_ID number(38),
  REFERENCE nvarchar2(50),
  AP_TIMES clob,
  AP_VOLTAGES clob,
  APD90 nvarchar2(100),
  DELTA_APD90 nvarchar2(1000),
  QNET nvarchar2(1000),
  constraint IDX_UNIQUE_REF_PER_ID unique (APP_MANAGER_ID, REFERENCE)
)#

create table PK_RESULTS (
  APP_MANAGER_ID number(38),
  TIMEPOINT nvarchar2(50),
  APD90S clob,
  constraint IDX_UNIQUE_TIMEPOINT_PER_ID unique (APP_MANAGER_ID, TIMEPOINT)
)#
