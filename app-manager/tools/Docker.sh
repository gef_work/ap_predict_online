#!/bin/bash -e

container_dir=/home/appredict/apps/ApPredict
output_dir=`pwd`/testoutput

if [ -d "${output_dir}" ]; then
  rm -rf ${output_dir}/
fi

mkdir -p ${output_dir}

selinux_enforcing=false
if [ -x "$(command -v getenforce)" ] && $(getenforce | grep -q '^Enforcing$'); then
  selinux_enforcing=true
fi

if [ "${selinux_enforcing}" = true ]; then
  chcon -R -t svirt_sandbox_file_t ${output_dir}
fi

if [ -f CELLML_FILE_PATH ]; then
  if [ "${selinux_enforcing}" = true ]; then
    chcon -R -t svirt_sandbox_file_t CELLML_FILE_PATH
  fi
  cellml="-v CELLML_FILE_PATH:${container_dir}/CELLML_FILE_NAME:ro"
fi
if [ -f PKPD_FILE_PATH ]; then
  if [ "${selinux_enforcing}" = true ]; then
    chcon -R -t svirt_sandbox_file_t PKPD_FILE_PATH
  fi
  pkpd="-v PKPD_FILE_PATH:${container_dir}/pkpd.file:ro"
fi

#--------------------------------------------------------------------------------------------------#
# --rm                                                                                             #
#   Automatically clean up the container and remove the file system when container exits.          #
# -u `id -u`:`id -g`                                                                               #
#   Run the simulation as the host user id and group id.                                           #
#   This means that simulation output written to ${output_dir} will be as the invoking host user.  #
# ${cellml}                                                                                        #
#   If there's a dynamic CellML file then mount file as volume (read-only), otherwise do nothing.  #
# ${pkpd}                                                                                          #
#   If there's a PKPD file then mount file as volume (read-only), otherwise do nothing.            #
# -v ${output_dir}:${container_dir}/testoutput:Z                                                   #
#   Mount container's "testoutput" directory read-write on host fs as dir named in ${output_dir}.  #
#   This enables the container to write real-time simulation output to the hosting fs.             #
# -e "BASH_ENV=/home/appredict/.bashrc"                                                            #
#   Non-interactive mode of `docker run` means appredict's ~/.bashrc isn't read, so assignment of  #
#     BASH_ENV forces it to be read in (for assignment of LD_LIBRARY_PATH and PYTHONPATH).         #
# -w ${container_dir}                                                                              #
#   Overrule Dockerfile WORKDIR with this one (as ${container_dir} contains "_BINARY.arch" files). #
# centos-appredict:6bd9518_6ba68bb                                                                 #
#   Image to run.                                                                                  #
# ${container_dir}/ApPredict.sh "$@"                                                               #
#   Command to invoke within image's run container                                                 #
#--------------------------------------------------------------------------------------------------#

#           centos-appredict:6bd9518_6ba68bb.1 \
#           cardiacmodelling/appredict-with-emulators:0.0.1 \
docker run --rm \
           -u `id -u`:`id -g` \
           ${cellml} \
           ${pkpd} \
           -v ${output_dir}:${container_dir}/testoutput:Z \
           -w ${container_dir}/ \
           cardiacmodelling/appredict-with-emulators:0.0.4 \
           ${container_dir}/ApPredict.sh "$@"
#docker run --rm \
#           -u `id -u`:`id -g` \
#           ${cellml} \
#           ${pkpd} \
#           -v ${output_dir}:${container_dir}/testoutput:Z \
#           -e "BASH_ENV=/home/appredict/.bashrc" \
#           -e "CHASTE_TEST_OUTPUT=${container_dir}/testoutput" \
#           -w ${container_dir}/ \
#           centos-appredict:0f75df0_12cdbe7.2 \
#           ${container_dir}/ApPredict.sh "$@"
