#!/bin/bash -e

base=`pwd`
cmd="cp -iv"

${cmd} ${base}/src/main/resources/META-INF/spring/ctx/ws/sample.appCtx.ws.security-incoming.xml ${base}/src/main/resources/META-INF/spring/ctx/ws/appCtx.ws.security-incoming.xml
${cmd} ${base}/src/main/resources/META-INF/spring/ctx/ws/sample.appCtx.ws.security-outgoing.xml ${base}/src/main/resources/META-INF/spring/ctx/ws/appCtx.ws.security-outgoing.xml
${cmd} ${base}/src/properties/database/sample.database.filter.properties ${base}/src/properties/database/database.filter.properties
${cmd} /dev/null ${base}/src/properties/database/dev.database.embedded.properties
${cmd} ${base}/src/properties/database/sample.database.spring.properties ${base}/src/properties/database/dev.database.mysql.properties
${cmd} ${base}/src/properties/database/sample.database.spring.properties ${base}/src/properties/database/dev.database.oracle10g.properties
${cmd} ${base}/src/properties/sample.filter.properties ${base}/src/properties/filter.properties
${cmd} ${base}/src/properties/sample.spring.properties ${base}/src/properties/spring.properties
${cmd} ${base}/sample.pom.xml ${base}/pom.xml