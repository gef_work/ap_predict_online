#!/bin/bash -e

ARG_COUNT=$#
if [[ $ARG_COUNT -ne 4 ]] ; then
  echo ""
  echo "Usage : `basename $0` <destination_dir> <cellml_file_name> <cellml_absolute_filepath> <pkpd_file_absolute_filepath>"
  echo ""
  echo "      : Use \"\" if arg is not being provided, e.g., for no CellML file ..."
  echo "        `basename $0` /home/user/app_run/233/ \"\" \"\" /home/user/files/mypkpdfile"
  exit 8
else
  DESTINATION_DIR=$1
  CELLML_FILE_NAME=$2
  CELLML_FILE_PATH=$3
  PKPD_FILE_PATH=$4
fi

#
# Create the simulation run directory if it doesn't exist already.
#
if [ ! -d $DESTINATION_DIR ] ; then 
  mkdir -p $DESTINATION_DIR
fi

#
# Symlink to the executable wrapper (ApPredictInvoker.java has ApPredict.sh hardcoded!)
#
ln -s /home/chaste/apps/ApPredict/ApPredict.sh $DESTINATION_DIR/
ln -s /home/chaste/apps/ApPredict/*_BINARY.arch $DESTINATION_DIR/
if [ ! -z ${CELLML_FILE_PATH} ]; then
  ln -s ${CELLML_FILE_PATH} ${DESTINATION_DIR}/${CELLML_FILE_NAME}
fi
if [ ! -z ${PKPD_FILE_PATH} ]; then
  ln -s ${PKPD_FILE_PATH} ${DESTINATION_DIR}/pkpd.file
fi

exit 0