#!/bin/bash -e

container_dir=/home/appredict/apps/ApPredict
output_dir=`pwd`/testoutput

cellml_dynamic_dir=`pwd`/cellml/dynamic
cellml_build_dir_gccoptnative=`pwd`/cellml/build/gccoptnative
cellml_build_dir_gccopt=`pwd`/cellml/build/gccopt

if [ -d "${output_dir}" ]; then
  rm -rf ${output_dir}/
fi

mkdir -p ${output_dir}

selinux_enforcing=false
if [ -x "$(command -v getenforce)" ] && $(getenforce | grep -q '^Enforcing$'); then
  selinux_enforcing=true
fi

if [ "${selinux_enforcing}" = true ]; then
  chcon -R -t svirt_sandbox_file_t ${output_dir}
fi

if [ -f CELLML_FILE_PATH ]; then
  if [ "${selinux_enforcing}" = true ]; then
    chcon -R -t svirt_sandbox_file_t CELLML_FILE_PATH
  fi
  mkdir -p ${cellml_dynamic_dir} ${cellml_build_dir}

  cellml=",${cellml_dynamic_dir}:/home/appredict/build/Chaste/heart/dynamic"
  cellml="${cellml},${cellml_build_dir_gccopt}:/home/appredict/build/Chaste/heart/build/optimised/dynamic"
  # optimised_native is probably applicable for ApPredict built with GccOptNative!!
  cellml="${cellml},${cellml_build_dir_gccoptnative}:/home/appredict/build/Chaste/heart/build/optimised_native/dynamic"
  cellml="${cellml},CELLML_FILE_PATH:${container_dir}/$(basename CELLML_FILE_PATH)"
fi
if [ -f PKPD_FILE_PATH ]; then
  if [ "${selinux_enforcing}" = true ]; then
    chcon -R -t svirt_sandbox_file_t PKPD_FILE_PATH
  fi
  pkpd=",PKPD_FILE_PATH:${container_dir}/pkpd.file"
fi

#
# If you have installed a completely unprivileged Singularity then you may
# need to include the `-u` arg.
# See https://sylabs.io/guides/3.2/user-guide/cli/singularity_run.html
#
SINGULARITY_BIND="${output_dir}:${container_dir}/testoutput${cellml}${pkpd}" \
  singularity exec --pwd ${container_dir} appredict.img ./ApPredict.sh "$@"
