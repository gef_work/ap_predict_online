#!/bin/bash -e

ARG_COUNT=$#
if [[ $ARG_COUNT -ne 4 ]] ; then
  echo ""
  echo "Usage : `basename $0` <destination_dir> <cellml_file_name> <cellml_absolute_filepath> <pkpd_file_absolute_filepath>"
  echo ""
  echo "      : Use \"\" if arg is not being provided, e.g., for no CellML file ..."
  echo "        `basename $0` /home/user/app_run/233/ \"\" \"\" /home/user/files/mypkpdfile"
  exit 8
else
  DESTINATION_DIR=$1
  CELLML_FILE_NAME=$2
  CELLML_FILE_PATH=$3
  PKPD_FILE_PATH=$4
fi

#
# Create the simulation run directory if it doesn't exist already.
#
if [ ! -d $DESTINATION_DIR ] ; then 
  mkdir -p $DESTINATION_DIR
fi

#
# Note : Because ApPredictInvoker.java invokes a file named 'ApPredict.sh' in
#        the simulation run directory, in the case of Docker usage this 
#        ApPredict.sh file will contain docker commands which will ultimately
#        invoke the  `docker run ...` of a docker image (which will in turn
#        invoke the conventional, i.e. ApPredict.sh within the created
#        container).
#
cp -v Docker.sh ${DESTINATION_DIR}/ApPredict.sh

if [[ ! -z ${CELLML_FILE_PATH} && ! -z ${CELLML_FILE_NAME} ]]; then
  sed -i -- "s|CELLML_FILE_PATH|${CELLML_FILE_PATH}|g" ${DESTINATION_DIR}/ApPredict.sh
  sed -i -- "s|CELLML_FILE_NAME|${CELLML_FILE_NAME}|g" ${DESTINATION_DIR}/ApPredict.sh
  ln -s ${CELLML_FILE_PATH} ${DESTINATION_DIR}/${CELLML_FILE_NAME}
fi
if [ ! -z ${PKPD_FILE_PATH} ]; then
  sed -i -- "s|PKPD_FILE_PATH|${PKPD_FILE_PATH}|g" ${DESTINATION_DIR}/ApPredict.sh
  ln -s ${PKPD_FILE_PATH} ${DESTINATION_DIR}/pkpd.file
fi

exit 0