# Action Potential prediction -- Application Manager

This is the code for running an `ApPredict` executable via a command-line invocation.

In essence `app-manager` performs the following tasks :

  1. Receives a web service simulation invocation request from either `client-direct` or
     `site-business` containing all the parameters for an `ApPredict` simulation to run.
  1. Invokes an `ApPredict` simulation.
  1. Monitors the invoked simulation runn and reports back progress and results when polled.
  1. Tidies up after a successful simulation run.

## Installation

Please see either of the following :

  1. http://apportal.readthedocs.io/en/latest/installation/components/app-manager/index.html
  1. ap_predict_online/docs/AP-Portal/RtD/_build/html/installation/components/app-manager/index.html