/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.util;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import org.junit.Test;

import uk.ac.ox.cs.nc3rs.client_direct.value.IC50_UNIT;

/**
 * Unit test the converter utility.
 *
 * @author geoff
 */
public class ConverterUtilTest {

  @Test
  public void testIC50ToPIC50() {
    IC50_UNIT dummyIC50Unit = IC50_UNIT.µM;

    BigDecimal dummyIC50 = BigDecimal.ZERO;
    try {
      ConverterUtil.ic50ToPIC50(dummyIC50Unit, dummyIC50);
      fail("Should not permit convesion of IC50 of 0 (zero)!");
    } catch (IllegalArgumentException e) {}

    dummyIC50 = BigDecimal.ONE;
    BigDecimal pIC50 = ConverterUtil.ic50ToPIC50(dummyIC50Unit, dummyIC50);
    assertTrue(new BigDecimal("6").compareTo(pIC50) == 0);

    dummyIC50 = new BigDecimal("0.000001");
    dummyIC50Unit = IC50_UNIT.M;
    pIC50 = ConverterUtil.ic50ToPIC50(dummyIC50Unit, dummyIC50);
    assertTrue(new BigDecimal("6").compareTo(pIC50) == 0);

    dummyIC50 = new BigDecimal("1000");
    dummyIC50Unit = IC50_UNIT.nM;
    pIC50 = ConverterUtil.ic50ToPIC50(dummyIC50Unit, dummyIC50);
    assertTrue(new BigDecimal("6").compareTo(pIC50) == 0);
  }
}