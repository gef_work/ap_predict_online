/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.service;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.util.LinkedList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunRequest;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Job;
import uk.ac.ox.cs.nc3rs.client_direct.exception.AppManagerWSInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.NoConnectionException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.RunInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.StatusVO;
import uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.manager.AppManagerManager;
import uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.service.AppManagerService;
import uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.service.AppManagerServiceImpl;
import uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.util.AppManagerUtil;

/**
 * Unit test the app manager service implementation.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { AppManagerServiceImpl.class, AppManagerUtil.class } )
public class AppManagerServiceImplTest {

  private AppManagerManager mockAppManagerManager;
  private AppManagerService appManagerService;
  private IMocksControl mocksControl;
  private long dummyAppManagerId;
  private SimulationInputVO mockSimulationInputVO;
  private StatusVO mockStatusVO;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockAppManagerManager = mocksControl.createMock(AppManagerManager.class);

    appManagerService = new AppManagerServiceImpl();

    ReflectionTestUtils.setField(appManagerService, ClientDirectIdentifiers.COMPONENT_APP_MANAGER_MANAGER,
                                 mockAppManagerManager);

    dummyAppManagerId = 123l;
    mockSimulationInputVO = mocksControl.createMock(SimulationInputVO.class);
    mockStatusVO = mocksControl.createMock(StatusVO.class);
  }

  @Test
  public void testRetrievingLatestCompletedProgress() throws AppManagerWSInvocationException, NoConnectionException {
    final List<StatusVO> dummyProgress = new LinkedList<StatusVO>();
    // > retrieveProgress
    expect(mockAppManagerManager.retrieveProgress(dummyAppManagerId)).andReturn(dummyProgress);
    // < retrieveProgress

    mocksControl.replay();

    String returnedProgress = appManagerService.retrieveLatestCompletedProgress(dummyAppManagerId);

    mocksControl.verify();
    assertNull(returnedProgress);

    mocksControl.reset();

    // Add a status object as a single progress data
    dummyProgress.add(mockStatusVO);
    // > retrieveProgress
    expect(mockAppManagerManager.retrieveProgress(dummyAppManagerId)).andReturn(dummyProgress);
    // < retrieveProgress
    boolean dummyIsProgressStatus = true;
    boolean dummyIsProgressFileNotFoundStatus = false;
    expect(mockStatusVO.isProgressStatus()).andReturn(dummyIsProgressStatus);
    expect(mockStatusVO.isProgressFileNotFoundStatus()).andReturn(dummyIsProgressFileNotFoundStatus);
    final String dummyStatus = "dummyStatus";
    expect(mockStatusVO.getStatus()).andReturn(dummyStatus);

    mocksControl.replay();

    returnedProgress = appManagerService.retrieveLatestCompletedProgress(dummyAppManagerId);

    mocksControl.verify();
    assertSame(dummyStatus, returnedProgress);

    mocksControl.reset();

    // Emulate alternative progress status
    // > retrieveProgress
    expect(mockAppManagerManager.retrieveProgress(dummyAppManagerId)).andReturn(dummyProgress);
    // < retrieveProgress
    dummyIsProgressStatus = true;
    dummyIsProgressFileNotFoundStatus = true;
    expect(mockStatusVO.isProgressStatus()).andReturn(dummyIsProgressStatus);
    expect(mockStatusVO.isProgressFileNotFoundStatus()).andReturn(dummyIsProgressFileNotFoundStatus);

    mocksControl.replay();

    returnedProgress = appManagerService.retrieveLatestCompletedProgress(dummyAppManagerId);

    mocksControl.verify();
    assertNull(returnedProgress);

    mocksControl.reset();

    // Emulate alternative progress status
    // > retrieveProgress
    expect(mockAppManagerManager.retrieveProgress(dummyAppManagerId)).andReturn(dummyProgress);
    // < retrieveProgress
    dummyIsProgressStatus = false;
    expect(mockStatusVO.isProgressStatus()).andReturn(dummyIsProgressStatus);

    mocksControl.replay();

    returnedProgress = appManagerService.retrieveLatestCompletedProgress(dummyAppManagerId);

    mocksControl.verify();
    assertNull(returnedProgress);
  }

  @Test
  public void testRetrieveProgress() throws AppManagerWSInvocationException, NoConnectionException {
    final List<StatusVO> dummyStatuses = new LinkedList<StatusVO>();
    expect(mockAppManagerManager.retrieveProgress(dummyAppManagerId)).andReturn(dummyStatuses);

    mocksControl.replay();

    List<StatusVO> returnedStatuses = appManagerService.retrieveProgress(dummyAppManagerId);

    mocksControl.verify();
    assertSame(0, returnedStatuses.size());
    try {
      returnedStatuses.add(mockStatusVO);
      fail("Should not permit modification of the returned statuses collection");
    } catch (UnsupportedOperationException use) {}

    mocksControl.reset();

    // Add another status
    dummyStatuses.add(mockStatusVO);
    expect(mockAppManagerManager.retrieveProgress(dummyAppManagerId)).andReturn(dummyStatuses);

    mocksControl.replay();

    returnedStatuses = appManagerService.retrieveProgress(dummyAppManagerId);

    mocksControl.verify();
    assertSame(1, returnedStatuses.size());
  }

  @Test
  public void testRetrieveResults() throws AppManagerWSInvocationException, NoConnectionException {
    Job mockJob = null;
    expect(mockAppManagerManager.retrieveResults(dummyAppManagerId)).andReturn(mockJob);

    mocksControl.replay();

    Job returnedJob = appManagerService.retrieveResults(dummyAppManagerId);

    mocksControl.verify();
    assertNull(returnedJob);

    mocksControl.reset();

    mockJob = mocksControl.createMock(Job.class);
    expect(mockAppManagerManager.retrieveResults(dummyAppManagerId)).andReturn(mockJob);

    mocksControl.replay();

    returnedJob = appManagerService.retrieveResults(dummyAppManagerId);

    mocksControl.verify();
    assertSame(mockJob, returnedJob);
  }

  @Test
  public void testRetrieveWorkload() throws AppManagerWSInvocationException, NoConnectionException {
    final String dummyWorkload = "dummyWorkload";

    expect(mockAppManagerManager.retrieveWorkload()).andReturn(dummyWorkload);

    mocksControl.replay();

    final String returnedWorkload = appManagerService.retrieveWorkload();

    mocksControl.verify();
    assertSame(dummyWorkload, returnedWorkload);
  }

  @Test
  public void testRunSimulation() throws RunInvocationException, AppManagerWSInvocationException,
                                         NoConnectionException {
    final ApPredictRunRequest mockApPredictRunRequest = mocksControl.createMock(ApPredictRunRequest.class);
    mockStatic(AppManagerUtil.class);
    expect(AppManagerUtil.createRunRequest(mockSimulationInputVO)).andReturn(mockApPredictRunRequest);
    final long dummySimulationId = 98l;
    expect(mockAppManagerManager.runApPredict(mockApPredictRunRequest)).andReturn(dummySimulationId);

    replayAll();
    mocksControl.replay();

    final long returnedSimulationId = appManagerService.runSimulation(mockSimulationInputVO);

    verifyAll();
    mocksControl.verify();
    assertSame(dummySimulationId, returnedSimulationId);
  }

  @Test
  public void testSignalResultsUploaded() throws AppManagerWSInvocationException,
                                                 NoConnectionException {
    mockAppManagerManager.signalResultsUploaded(dummyAppManagerId);

    mocksControl.replay();

    appManagerService.signalResultsUploaded(dummyAppManagerId);

    mocksControl.verify();
  }
}