/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.manager;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.manager.CSConfigurationManager;
import uk.ac.ox.cs.compbio.client_shared.value.PortalFeature;
import uk.ac.ox.cs.compbio.client_shared.value.security.Role;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.config.Configuration;
import uk.ac.ox.cs.nc3rs.client_direct.manager.ConfigurationManagerImpl;
import uk.ac.ox.cs.nc3rs.client_direct.value.IonCurrent;
import uk.ac.ox.cs.nc3rs.client_direct.value.ModelVO;

/**
 * Unit test the configuration service implementation.
 *
 * @author geoff
 */
public class ConfigurationManagerImplTest {

  private ConfigurationManager configurationManager;
  private Configuration mockConfiguration;
  private CSConfigurationManager mockCSConfigurationManager;
  private IMocksControl mocksControl = createStrictControl();
  private ModelVO mockModel;

  @Before
  public void setUp() {
    configurationManager = new ConfigurationManagerImpl();
    mockConfiguration = mocksControl.createMock(Configuration.class);
    mockCSConfigurationManager = mocksControl.createMock(CSConfigurationManager.class);
    mockModel = mocksControl.createMock(ModelVO.class);
    ReflectionTestUtils.setField(configurationManager,
                                 ClientDirectIdentifiers.COMPONENT_CONFIGURATION,
                                 mockConfiguration);
    ReflectionTestUtils.setField(configurationManager,
                                 ClientSharedIdentifiers.COMPONENT_CLIENT_SHARED_CONFIGURATION_MANAGER,
                                 mockCSConfigurationManager);
  }

  @Test
  public void testRetrieveC50Units() {
    final Map<String, String> dummyC50Units = new HashMap<String, String>();
    dummyC50Units.put("dummyKey1", "dummyValue1");
    expect(mockConfiguration.getC50Units()).andReturn(dummyC50Units);

    mocksControl.replay();

    final Map<String, String> retrievedC50Units = configurationManager.retrieveC50Units();
    assertSame(dummyC50Units, retrievedC50Units);

    mocksControl.verify();
  }

  @Test
  public void testRetrieveCredibleIntervals() {
    final Set<BigDecimal> dummyCredibleIntervals = new HashSet<BigDecimal>();
    final BigDecimal dummyCredibleInterval = BigDecimal.TEN;
    dummyCredibleIntervals.add(dummyCredibleInterval);

    expect(mockConfiguration.getCredibleIntervalPctiles())
          .andReturn(dummyCredibleIntervals);

    mocksControl.replay();

    final Set<BigDecimal> retrievedCIs = configurationManager.retrieveCredibleIntervalPctiles();

    mocksControl.verify();

    assertSame(dummyCredibleIntervals, retrievedCIs);
  }

  @Test
  public void testRetrieveModelNames() {
    final Map<Short, String> dummyModelNames = new HashMap<Short, String>();
    dummyModelNames.put(Short.valueOf("2"), "dummyValue1");
    expect(mockConfiguration.getModelNames()).andReturn(dummyModelNames);

    mocksControl.replay();

    final Map<Short, String> retrievedModelNames = configurationManager.retrieveModelNames();

    mocksControl.verify();

    assertSame(dummyModelNames, retrievedModelNames);
  }

  @Test
  public void testRetrieveModels() {
    final List<ModelVO> dummyModels = new ArrayList<ModelVO>();
    dummyModels.add(mockModel);

    expect(mockConfiguration.getModels()).andReturn(dummyModels);

    mocksControl.replay();

    final List<ModelVO> retrievedModels = configurationManager.retrieveModels();

    mocksControl.verify();

    assertSame(dummyModels, retrievedModels);
  }

  @Test
  public void testRetrieveRequiredRoles() {
    final Set<Role> dummyRoles = new HashSet<Role>();
    final PortalFeature dummyPortalFeature = PortalFeature.UNCERTAINTIES;

    expect(mockCSConfigurationManager.retrieveRequiredRoles(dummyPortalFeature))
          .andReturn(dummyRoles);

    mocksControl.replay();

    final Set<Role> requiredRoles = configurationManager.retrieveRequiredRoles(dummyPortalFeature);

    mocksControl.verify();

    assertEquals(dummyRoles, requiredRoles);
  }

  @Test
  public void testRetrieveSpreads() {
    final Map<IonCurrent, BigDecimal> dummySpreads = new HashMap<IonCurrent, BigDecimal>();

    expect(mockConfiguration.getSpreads()).andReturn(dummySpreads);

    mocksControl.replay();

    final Map<IonCurrent, BigDecimal> retrieved = configurationManager.retrieveSpreads();

    mocksControl.verify();

    assertSame(dummySpreads, retrieved);
  }
}