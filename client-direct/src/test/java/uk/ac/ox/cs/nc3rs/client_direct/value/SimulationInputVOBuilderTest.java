/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.value;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;

import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.nc3rs.client_direct.config.Configuration;
import uk.ac.ox.cs.nc3rs.client_direct.util.ConverterUtil;

/**
 * Unit test of the simulation input value object.
 * <p>
 * Actually it's more of an integration test because the .build() operation
 * creates new {@link MeasurementObservationsVO} objects which are tested.
 * 
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { ConverterUtil.class } )
public class SimulationInputVOBuilderTest {

  private static final BigDecimal smallAmount = new BigDecimal("0.00000000000000000001");
  private static final String invalidICaLC50 = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_ICaL_C50);
  private static final String invalidICaLHill = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_ICaL_Hill);
  private static final String invalidICaLSaturation = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_ICaL_Saturation);
  private static final String invalidICaLSpread = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_ICaL_Spread);
  private static final String invalidIK1C50 = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_IK1_C50);
  private static final String invalidIK1Hill = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_IK1_Hill);
  private static final String invalidIK1Saturation = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_IK1_Saturation);
  private static final String invalidIKrC50 = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_IKr_C50);
  private static final String invalidIKrHill = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_IKr_Hill);
  private static final String invalidIKrSaturation = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_IKr_Saturation);
  private static final String invalidIKsC50 = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_IKs_C50);
  private static final String invalidIKsHill = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_IKs_Hill);
  private static final String invalidIKsSaturation = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_IKs_Saturation);
  private static final String invalidItoC50 = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_Ito_C50);
  private static final String invalidItoHill = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_Ito_Hill);
  private static final String invalidItoSaturation = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_Ito_Saturation);
  private static final String invalidINaC50 = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_INa_C50);
  private static final String invalidINaHill = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_INa_Hill);
  private static final String invalidINaSaturation = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_INa_Saturation);
  private static final String invalidINaLC50 = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_INaL_C50);
  private static final String invalidINaLHill = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_INaL_Hill);
  private static final String invalidINaLSaturation = SimulationInputVO.invalidConstruct(SimulationInputVO.IVN_INaL_Saturation);

  private static final BigDecimal defaultHillCoefficient = new BigDecimal(SimulationInputVO.DEFAULT_HILL_COEFFICIENT);
  private static final BigDecimal defaultSaturation = new BigDecimal(SimulationInputVO.DEFAULT_SATURATION);

  private IMocksControl mocksControl;
  private PortalFile mockCellMLFile;
  private PortalFile mockPKFile;
  private SimulationInputVO simulationInputVO;
  private String dummyC50Type;
  private String dummyCellMLFileName;
  private String dummyICaLC50, dummyICaLHill, dummyICaLSaturation,
                 dummyICaLSpread;
  private String dummyIK1C50, dummyIK1Hill, dummyIK1Saturation,
                 dummyIK1Spread;
  private String dummyIKrC50, dummyIKrHill, dummyIKrSaturation,
                 dummyIKrSpread;
  private String dummyIKsC50, dummyIKsHill, dummyIKsSaturation,
                 dummyIKsSpread;
  private String dummyItoC50, dummyItoHill, dummyItoSaturation,
                 dummyItoSpread;
  private String dummyINaC50, dummyINaHill, dummyINaSaturation,
                 dummyINaSpread;
  private String dummyINaLC50, dummyINaLHill, dummyINaLSaturation,
                 dummyINaLSpread;
  private String dummyIC50Units;
  private String dummyModelIdentifier;
  private String dummyNotes;
  private String dummyCIPctiles;
  private String dummyPacingFrequency, dummyPacingMaxTime;
  private String dummyPlasmaConcMax, dummyPlasmaConcMin;
  private String dummyPlasmaIntPtCount, dummyPlasmaIntPtLogScale;
  private String[] dummyMinPlasmaConcPoints;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();

    dummyModelIdentifier = "1";
    dummyC50Type = C50_TYPE.IC50.toString();
    dummyIC50Units = IC50_UNIT.µM.toString();
    dummyPlasmaConcMax = Configuration.RECOMMENDED_PLASMA_CONC_MAX.toString();
    dummyMinPlasmaConcPoints = new String[] { "1" };
    mockCellMLFile = null;
    mockPKFile = null;
  }

  @Test
  public void testCellML() {
    /*
     * Neither model identifier nor CellML file.
     */
    simulationInputVO = new SimulationInputVO.Builder().build();

    Collection<String> assignmentProblems = simulationInputVO.getAssignmentProblems();
    assertTrue(assignmentProblems.contains(SimulationInputVO.INVALID_CELLML));

    /*
     * Both model identifier and (unnamed) CellML file.
     */
    mockCellMLFile = mocksControl.createMock(PortalFile.class);
    expect(mockCellMLFile.getName()).andReturn(dummyCellMLFileName);

    mocksControl.replay();

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .cellMLFile(mockCellMLFile)
                                                       .build();

    mocksControl.verify();

    assignmentProblems = simulationInputVO.getAssignmentProblems();
    assertTrue(assignmentProblems.contains(SimulationInputVO.INVALID_CELLML));
    assertNotNull(simulationInputVO.getCellMLFile());
    assertNull(simulationInputVO.getCellMLFileName());

    mocksControl.reset();

    /*
     * Only a CellML file provided.
     */
    dummyCellMLFileName = "dummyCellMLFileName";
    expect(mockCellMLFile.getName()).andReturn(dummyCellMLFileName);

    mocksControl.replay();

    simulationInputVO = new SimulationInputVO.Builder().cellMLFile(mockCellMLFile)
                                                       .build();

    mocksControl.verify();

    assignmentProblems = simulationInputVO.getAssignmentProblems();
    assertFalse(assignmentProblems.contains(SimulationInputVO.INVALID_CELLML));
    assertSame(mockCellMLFile, simulationInputVO.getCellMLFile());
    assertEquals(dummyCellMLFileName, simulationInputVO.getCellMLFileName());
  }

  // Based on the interface defaulting the Hill and Saturation values to 1 and
  //   0 respectively.
  //@Test
  public void testDefaultParameterValuesFromInterface() throws Exception {
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(null)
                                                       .ic50Units(null)
                                                       .iCaLC50("")
                                                       .iCaLHill(SimulationInputVO.DEFAULT_HILL_COEFFICIENT)
                                                       .iCaLSaturation(SimulationInputVO.DEFAULT_SATURATION)
                                                       .iCaLSpread(null)
                                                       .iK1C50("")
                                                       .iK1Hill(SimulationInputVO.DEFAULT_HILL_COEFFICIENT)
                                                       .iK1Saturation(SimulationInputVO.DEFAULT_SATURATION)
                                                       .iK1Spread(null)
                                                       .iKrC50("")
                                                       .iKrHill(SimulationInputVO.DEFAULT_HILL_COEFFICIENT)
                                                       .iKrSaturation(SimulationInputVO.DEFAULT_SATURATION)
                                                       .iKrSpread(null)
                                                       .iKsC50("")
                                                       .iKsHill(SimulationInputVO.DEFAULT_HILL_COEFFICIENT)
                                                       .iKsSaturation(SimulationInputVO.DEFAULT_SATURATION)
                                                       .iKsSpread(null)
                                                       .iNaC50("")
                                                       .iNaHill(SimulationInputVO.DEFAULT_HILL_COEFFICIENT)
                                                       .iNaSaturation(SimulationInputVO.DEFAULT_SATURATION)
                                                       .iNaSpread(null)
                                                       .itoC50("")
                                                       .itoHill(SimulationInputVO.DEFAULT_HILL_COEFFICIENT)
                                                       .itoSaturation(SimulationInputVO.DEFAULT_SATURATION)
                                                       .itoSpread(null)
                                                       .notes(null)
                                                       .pacingFrequency(null)
                                                       .pacingMaxTime(null)
                                                       .plasmaConcMax(null)
                                                       .plasmaConcMin(null)
                                                       .plasmaIntPtCount(null)
                                                       .plasmaIntPtLogScale(null)
                                                       .build();

    final MeasurementObservationsVO iCaLCompoundObservations = simulationInputVO.getICaLObservations();
    assertNull(iCaLCompoundObservations.getC50());
    assertTrue(defaultHillCoefficient.compareTo(iCaLCompoundObservations.getHill()) == 0);
    assertTrue(defaultSaturation.compareTo(iCaLCompoundObservations.getSaturation()) == 0);
    assertNull(iCaLCompoundObservations.getSpread());
  }

  @Test
  public void testHandlingBlankData() {
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(null)
                                                       .c50Type(null)
                                                       .cellMLFile(null)
                                                       .ic50Units(null)
                                                       .iCaLC50(null).iCaLHill(null)
                                                       .iCaLSaturation(null)
                                                       .iCaLSpread(null)
                                                       .iK1C50(null).iK1Hill(null)
                                                       .iK1Saturation(null)
                                                       .iK1Spread(null)
                                                       .iKrC50(null).iKrHill(null)
                                                       .iKrSaturation(null)
                                                       .iKrSpread(null)
                                                       .iKsC50(null).iKsHill(null)
                                                       .iKsSaturation(null)
                                                       .iKsSpread(null)
                                                       .iNaC50(null).iNaHill(null)
                                                       .iNaSaturation(null)
                                                       .iNaSpread(null)
                                                       .itoC50(null).itoHill(null)
                                                       .itoSaturation(null)
                                                       .itoSpread(null)
                                                       .notes(null)
                                                       .pacingFrequency(null)
                                                       .pacingMaxTime(null)
                                                       .pkFile(null)
                                                       .plasmaConcMax(null)
                                                       .plasmaConcMin(null)
                                                       .plasmaIntPtCount(null)
                                                       .plasmaIntPtLogScale(null)
                                                       .build();
    assertNull(simulationInputVO.getCellMLFile());
    assertNull(simulationInputVO.getCellMLFileName());
    assertNull(simulationInputVO.getModelIdentifier());
    assertNull(simulationInputVO.getPkFile());
    final MeasurementObservationsVO iCaLCompoundObservations = simulationInputVO.getICaLObservations();
    assertNull(iCaLCompoundObservations.getC50());
    assertNull(iCaLCompoundObservations.getHill());
    assertNull(iCaLCompoundObservations.getSaturation());
    assertNull(iCaLCompoundObservations.getSpread());
  }

  @Test
  public void testNotes() {
    dummyNotes = "dummyNotes";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .notes(dummyNotes)
                                                       .build();
    assertEquals(dummyNotes, simulationInputVO.getNotes());

    // Test trimming
    final String textualNotes = "textualNotes";
    dummyNotes = "      ".concat(textualNotes).concat("      ");
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .notes(dummyNotes)
                                                       .build();
    assertEquals(textualNotes, simulationInputVO.getNotes());

    // Test trunacation on exceeding max length.
    dummyNotes = new String(new char[SimulationInputVO.MAX_NOTES_LENGTH + 5]).replace('\0', '-');
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .notes(dummyNotes)
                                                       .build();
    final String recordedNotes = simulationInputVO.getNotes();
    assertNotEquals(dummyNotes, recordedNotes);
    assertEquals(SimulationInputVO.MAX_NOTES_LENGTH, recordedNotes.length());
  }

  @Test
  public void testInvalidIC50ValueOf0() {
    dummyICaLC50 = "0";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iCaLC50(dummyICaLC50)
                                                       .plasmaConcMax(dummyPlasmaConcMax)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().contains(SimulationInputVO.INVALID_IC50_VALUE_OF_0));
  }

  @Test
  public void testNegativeC50Values() {
    dummyICaLC50 = "-0.5";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iCaLC50(dummyICaLC50)
                                                       .plasmaConcMax(dummyPlasmaConcMax)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().contains(SimulationInputVO.INVALID_NEGATIVE_IC50));
  }

  @Test
  public void testValidIC50DataEntry() {
    dummyICaLC50 = "50.0";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iCaLC50(dummyICaLC50)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    assertTrue(IC50_UNIT.valueOf(dummyIC50Units).compareTo(simulationInputVO.getIc50Units()) == 0);
  }

  @Test
  public void testValidPIC50DataEntry() {
    dummyC50Type = C50_TYPE.pIC50.toString();
    dummyICaLC50 = "5.0";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .iCaLC50(dummyICaLC50)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    assertNull(simulationInputVO.getIc50Units());
  }

  @Test
  public void testSuccessOnGoodData() {
    /*
     * First ensure that the minimum data requirements are satisfied
     */
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    assertSame(new Short(dummyModelIdentifier).shortValue(),
               simulationInputVO.getModelIdentifier());

    /*
     * Good pacing max time max
     */
    dummyPacingMaxTime = SimulationInputVO.MAX_PACING_MAX_TIME;
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .pacingMaxTime(dummyPacingMaxTime)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    assertTrue(new BigDecimal(dummyPacingMaxTime)
               .compareTo(simulationInputVO.getPacingMaxTime()) == 0);

    dummyPacingMaxTime = new BigDecimal(SimulationInputVO.MAX_PACING_MAX_TIME).subtract(smallAmount).toPlainString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .pacingMaxTime(dummyPacingMaxTime)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    assertTrue(new BigDecimal(dummyPacingMaxTime).compareTo(simulationInputVO.getPacingMaxTime()) == 0);

    /*
     * Good pacing max time min
     */
    dummyPacingMaxTime = new BigDecimal(SimulationInputVO.MIN_PACING_MAX_TIME).add(smallAmount).toPlainString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .pacingMaxTime(dummyPacingMaxTime)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    assertTrue(new BigDecimal(dummyPacingMaxTime).compareTo(simulationInputVO.getPacingMaxTime()) == 0);

    /*
     * Good pacing frequency max
     */
    dummyPacingFrequency = SimulationInputVO.MAX_PACING_FREQUENCY;
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .pacingFrequency(dummyPacingFrequency)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    assertEquals(new BigDecimal(dummyPacingFrequency), simulationInputVO.getPacingFrequency());

    final BigDecimal maxPacingFrequency = new BigDecimal(SimulationInputVO.MAX_PACING_FREQUENCY);
    dummyPacingFrequency = maxPacingFrequency.subtract(smallAmount).toPlainString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .pacingFrequency(dummyPacingFrequency)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    assertEquals(new BigDecimal(dummyPacingFrequency), simulationInputVO.getPacingFrequency());

    /*
     * Good pacing frequency min
     */
    final BigDecimal minPacingFrequency = new BigDecimal(SimulationInputVO.MIN_PACING_FREQUENCY);
    dummyPacingFrequency = minPacingFrequency.add(smallAmount).toPlainString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .pacingFrequency(dummyPacingFrequency)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    assertEquals(new BigDecimal(dummyPacingFrequency), simulationInputVO.getPacingFrequency());

    dummyPacingFrequency = SimulationInputVO.MIN_PACING_FREQUENCY;
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .pacingFrequency(dummyPacingFrequency)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    assertEquals(new BigDecimal(dummyPacingFrequency), simulationInputVO.getPacingFrequency());

    /*
     * Good ICaL
     */

    // Just the ICaL value.
    dummyICaLC50 = "5.0";
    final BigDecimal bdDummyICaLC50 = new BigDecimal(dummyICaLC50);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iCaLC50(dummyICaLC50)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    MeasurementObservationsVO iCaLObservations = simulationInputVO.getICaLObservations();
    assertTrue(bdDummyICaLC50.compareTo(iCaLObservations.getC50()) == 0);
    assertTrue(defaultHillCoefficient.compareTo(iCaLObservations.getHill()) == 0);
    assertTrue(defaultSaturation.compareTo(iCaLObservations.getSaturation()) == 0);
    assertNull(iCaLObservations.getSpread());

    // Include an ICaL Hill representing min. Hill coefficient.
    dummyICaLHill = new BigDecimal(SimulationInputVO.MIN_HILL_COEFFICIENT).toPlainString();
    BigDecimal bdDummyICaLHill = new BigDecimal(dummyICaLHill);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iCaLC50(dummyICaLC50)
                                                       .iCaLHill(dummyICaLHill)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iCaLObservations = simulationInputVO.getICaLObservations();
    assertTrue(bdDummyICaLC50.compareTo(iCaLObservations.getC50()) == 0);
    assertTrue(bdDummyICaLHill.compareTo(iCaLObservations.getHill()) == 0);
    assertTrue(defaultSaturation.compareTo(iCaLObservations.getSaturation()) == 0);
    assertNull(iCaLObservations.getSpread());

    // Include an ICaL Hill representing max. Hill coefficient.
    dummyICaLHill = new BigDecimal(SimulationInputVO.MAX_HILL_COEFFICIENT).toPlainString();
    bdDummyICaLHill = new BigDecimal(dummyICaLHill);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iCaLC50(dummyICaLC50)
                                                       .iCaLHill(dummyICaLHill)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iCaLObservations = simulationInputVO.getICaLObservations();
    assertTrue(bdDummyICaLC50.compareTo(iCaLObservations.getC50()) == 0);
    assertTrue(bdDummyICaLHill.compareTo(iCaLObservations.getHill()) == 0);
    assertTrue(defaultSaturation.compareTo(iCaLObservations.getSaturation()) == 0);
    assertNull(iCaLObservations.getSpread());

    // Include a saturation value (revert to default Hill)
    dummyICaLSaturation = "50.0";
    final BigDecimal bdDummyICaLSaturation = new BigDecimal(dummyICaLSaturation);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iCaLC50(dummyICaLC50)
                                                       .iCaLSaturation(dummyICaLSaturation)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iCaLObservations = simulationInputVO.getICaLObservations();
    assertTrue(bdDummyICaLC50.compareTo(iCaLObservations.getC50()) == 0);
    assertTrue(defaultHillCoefficient.compareTo(iCaLObservations.getHill()) == 0);
    assertTrue(bdDummyICaLSaturation.compareTo(iCaLObservations.getSaturation()) == 0);
    assertNull(iCaLObservations.getSpread());

    // Include a spread value (revert to default Saturation and Hill)
    dummyICaLSpread = new BigDecimal(SimulationInputVO.MIN_SPREAD_GT)
                                    .add(new BigDecimal("0.1")).toString();
    BigDecimal bdDummyICaLSpread = new BigDecimal(dummyICaLSpread);
    dummyCIPctiles = "dummyCIPctiles";

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iCaLC50(dummyICaLC50)
                                                       .iCaLSpread(dummyICaLSpread)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .credibleIntervalPctiles(dummyCIPctiles)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iCaLObservations = simulationInputVO.getICaLObservations();
    assertTrue(bdDummyICaLC50.compareTo(iCaLObservations.getC50()) == 0);
    assertTrue(defaultHillCoefficient.compareTo(iCaLObservations.getHill()) == 0);
    assertTrue(defaultSaturation.compareTo(iCaLObservations.getSaturation()) == 0);
    assertTrue(bdDummyICaLSpread.compareTo(iCaLObservations.getSpread()) == 0);

    dummyICaLSpread = SimulationInputVO.MAX_SPREAD;
    bdDummyICaLSpread = new BigDecimal(dummyICaLSpread);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iCaLC50(dummyICaLC50)
                                                       .iCaLSpread(dummyICaLSpread)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .credibleIntervalPctiles(dummyCIPctiles)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iCaLObservations = simulationInputVO.getICaLObservations();
    assertTrue(bdDummyICaLC50.compareTo(iCaLObservations.getC50()) == 0);
    assertTrue(defaultHillCoefficient.compareTo(iCaLObservations.getHill()) == 0);
    assertTrue(defaultSaturation.compareTo(iCaLObservations.getSaturation()) == 0);
    assertTrue(bdDummyICaLSpread.compareTo(iCaLObservations.getSpread()) == 0);

    /*
     * Good Ik1
     */
    dummyIK1C50 = "5.1";
    final BigDecimal bdDummyIK1C50 = new BigDecimal(dummyIK1C50);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iK1C50(dummyIK1C50)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    MeasurementObservationsVO iK1Observations = simulationInputVO.getIK1Observations();
    assertTrue(bdDummyIK1C50.compareTo(iK1Observations.getC50()) == 0);
    assertTrue(defaultHillCoefficient.compareTo(iK1Observations.getHill()) == 0);
    assertTrue(defaultSaturation.compareTo(iK1Observations.getSaturation()) == 0);
    assertNull(iK1Observations.getSpread());

    dummyIK1Hill = "0.51";
    final BigDecimal bdDummyIK1Hill = new BigDecimal(dummyIK1Hill);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iK1C50(dummyIK1C50)
                                                       .iK1Hill(dummyIK1Hill)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iK1Observations = simulationInputVO.getIK1Observations();
    assertTrue(bdDummyIK1C50.compareTo(iK1Observations.getC50()) == 0);
    assertTrue(bdDummyIK1Hill.compareTo(iK1Observations.getHill()) == 0);
    assertTrue(defaultSaturation.compareTo(iK1Observations.getSaturation()) == 0);
    assertNull(iK1Observations.getSpread());

    dummyIK1Saturation = "50.1";
    final BigDecimal bdDummyIK1Saturation = new BigDecimal(dummyIK1Saturation);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iK1C50(dummyIK1C50)
                                                       .iK1Hill(dummyIK1Hill)
                                                       .iK1Saturation(dummyIK1Saturation)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iK1Observations = simulationInputVO.getIK1Observations();
    assertTrue(bdDummyIK1C50.compareTo(iK1Observations.getC50()) == 0);
    assertTrue(bdDummyIK1Hill.compareTo(iK1Observations.getHill()) == 0);
    assertTrue(bdDummyIK1Saturation.compareTo(iK1Observations.getSaturation()) == 0);
    assertNull(iK1Observations.getSpread());

    dummyIK1Spread = SimulationInputVO.MAX_SPREAD;
    final BigDecimal bdDummyIK1Spread = new BigDecimal(dummyIK1Spread);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iK1C50(dummyIK1C50)
                                                       .iK1Hill(dummyIK1Hill)
                                                       .iK1Saturation(dummyIK1Saturation)
                                                       .iK1Spread(dummyIK1Spread)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .credibleIntervalPctiles(dummyCIPctiles)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iK1Observations = simulationInputVO.getIK1Observations();
    assertTrue(bdDummyIK1C50.compareTo(iK1Observations.getC50()) == 0);
    assertTrue(bdDummyIK1Hill.compareTo(iK1Observations.getHill()) == 0);
    assertTrue(bdDummyIK1Saturation.compareTo(iK1Observations.getSaturation()) == 0);
    assertTrue(bdDummyIK1Spread.compareTo(iK1Observations.getSpread()) == 0);

    /*
     * Good Ikr
     */
    dummyIKrC50 = "5.2";
    final BigDecimal bdDummyIKrC50 = new BigDecimal(dummyIKrC50);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iKrC50(dummyIKrC50)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    MeasurementObservationsVO iKrObservations = simulationInputVO.getIKrObservations();
    assertTrue(bdDummyIKrC50.compareTo(iKrObservations.getC50()) == 0);
    assertTrue(defaultHillCoefficient.compareTo(iKrObservations.getHill()) == 0);
    assertTrue(defaultSaturation.compareTo(iKrObservations.getSaturation()) == 0);
    assertNull(iKrObservations.getSpread());

    dummyIKrHill = "0.52";
    final BigDecimal bdDummyIKrHill = new BigDecimal(dummyIKrHill);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iKrC50(dummyIKrC50)
                                                       .iKrHill(dummyIKrHill)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iKrObservations = simulationInputVO.getIKrObservations();
    assertTrue(bdDummyIKrC50.compareTo(iKrObservations.getC50()) == 0);
    assertTrue(bdDummyIKrHill.compareTo(iKrObservations.getHill()) == 0);
    assertTrue(defaultSaturation.compareTo(iKrObservations.getSaturation()) == 0);
    assertNull(iKrObservations.getSpread());

    dummyIKrSaturation = "50.2";
    final BigDecimal bdDummyIKrSaturation = new BigDecimal(dummyIKrSaturation);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iKrC50(dummyIKrC50)
                                                       .iKrHill(dummyIKrHill)
                                                       .iKrSaturation(dummyIKrSaturation)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iKrObservations = simulationInputVO.getIKrObservations();
    assertTrue(bdDummyIKrC50.compareTo(iKrObservations.getC50()) == 0);
    assertTrue(bdDummyIKrHill.compareTo(iKrObservations.getHill()) == 0);
    assertTrue(bdDummyIKrSaturation.compareTo(iKrObservations.getSaturation()) == 0);
    assertNull(iKrObservations.getSpread());

    dummyIKrSpread = SimulationInputVO.MAX_SPREAD;
    final BigDecimal bdDummyIKrSpread = new BigDecimal(dummyIKrSpread);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iKrC50(dummyIKrC50)
                                                       .iKrHill(dummyIKrHill)
                                                       .iKrSaturation(dummyIKrSaturation)
                                                       .iKrSpread(dummyIKrSpread)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .credibleIntervalPctiles(dummyCIPctiles)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iKrObservations = simulationInputVO.getIKrObservations();
    assertTrue(bdDummyIKrC50.compareTo(iKrObservations.getC50()) == 0);
    assertTrue(bdDummyIKrHill.compareTo(iKrObservations.getHill()) == 0);
    assertTrue(bdDummyIKrSaturation.compareTo(iKrObservations.getSaturation()) == 0);
    assertTrue(bdDummyIKrSpread.compareTo(iKrObservations.getSpread()) == 0);

    /*
     * Good IKs
     */
    dummyIKsC50 = "5.3";
    final BigDecimal bdDummyIKsC50 = new BigDecimal(dummyIKsC50);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iKsC50(dummyIKsC50)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    MeasurementObservationsVO iKsObservations = simulationInputVO.getIKsObservations();
    assertTrue(bdDummyIKsC50.compareTo(iKsObservations.getC50()) == 0);
    assertTrue(defaultHillCoefficient.compareTo(iKsObservations.getHill()) == 0);
    assertTrue(defaultSaturation.compareTo(iKsObservations.getSaturation()) == 0);
    assertNull(iKsObservations.getSpread());

    dummyIKsHill = "0.53";
    final BigDecimal bdDummyIKsHill = new BigDecimal(dummyIKsHill);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iKsC50(dummyIKsC50)
                                                       .iKsHill(dummyIKsHill)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iKsObservations = simulationInputVO.getIKsObservations();
    assertTrue(bdDummyIKsC50.compareTo(iKsObservations.getC50()) == 0);
    assertTrue(bdDummyIKsHill.compareTo(iKsObservations.getHill()) == 0);
    assertTrue(defaultSaturation.compareTo(iKsObservations.getSaturation()) == 0);
    assertNull(iKsObservations.getSpread());

    dummyIKsSaturation = "50.3";
    final BigDecimal bdDummyIKsSaturation = new BigDecimal(dummyIKsSaturation);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iKsC50(dummyIKsC50)
                                                       .iKsHill(dummyIKsHill)
                                                       .iKsSaturation(dummyIKsSaturation)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iKsObservations = simulationInputVO.getIKsObservations();
    assertTrue(bdDummyIKsC50.compareTo(iKsObservations.getC50()) == 0);
    assertTrue(bdDummyIKsHill.compareTo(iKsObservations.getHill()) == 0);
    assertTrue(bdDummyIKsSaturation.compareTo(iKsObservations.getSaturation()) == 0);
    assertNull(iKsObservations.getSpread());

    dummyIKsSpread = SimulationInputVO.MAX_SPREAD;
    final BigDecimal bdDummyIKsSpread = new BigDecimal(dummyIKsSpread);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iKsC50(dummyIKsC50)
                                                       .iKsHill(dummyIKsHill)
                                                       .iKsSaturation(dummyIKsSaturation)
                                                       .iKsSpread(dummyIKsSpread)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .credibleIntervalPctiles(dummyCIPctiles)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iKsObservations = simulationInputVO.getIKsObservations();
    assertTrue(bdDummyIKsC50.compareTo(iKsObservations.getC50()) == 0);
    assertTrue(bdDummyIKsHill.compareTo(iKsObservations.getHill()) == 0);
    assertTrue(bdDummyIKsSaturation.compareTo(iKsObservations.getSaturation()) == 0);
    assertTrue(bdDummyIKsSpread.compareTo(iKsObservations.getSpread()) == 0);

    /*
     * Good Ito
     */
    dummyItoC50 = "5.4";
    final BigDecimal bdDummyItoC50 = new BigDecimal(dummyItoC50);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .itoC50(dummyItoC50)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    MeasurementObservationsVO itoObservations = simulationInputVO.getItoObservations();
    assertTrue(bdDummyItoC50.compareTo(itoObservations.getC50()) == 0);
    assertTrue(defaultHillCoefficient.compareTo(itoObservations.getHill()) == 0);
    assertTrue(defaultSaturation.compareTo(itoObservations.getSaturation()) == 0);
    assertNull(itoObservations.getSpread());

    dummyItoHill = "0.54";
    final BigDecimal bdDummyItoHill = new BigDecimal(dummyItoHill);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .itoC50(dummyItoC50)
                                                       .itoHill(dummyItoHill)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    itoObservations = simulationInputVO.getItoObservations();
    assertTrue(bdDummyItoC50.compareTo(itoObservations.getC50()) == 0);
    assertTrue(bdDummyItoHill.compareTo(itoObservations.getHill()) == 0);
    assertTrue(defaultSaturation.compareTo(itoObservations.getSaturation()) == 0);
    assertNull(itoObservations.getSpread());

    dummyItoSaturation = "50.4";
    final BigDecimal bdDummyItoSaturation = new BigDecimal(dummyItoSaturation);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .itoC50(dummyItoC50)
                                                       .itoHill(dummyItoHill)
                                                       .itoSaturation(dummyItoSaturation)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    itoObservations = simulationInputVO.getItoObservations();
    assertTrue(bdDummyItoC50.compareTo(itoObservations.getC50()) == 0);
    assertTrue(bdDummyItoHill.compareTo(itoObservations.getHill()) == 0);
    assertTrue(bdDummyItoSaturation.compareTo(itoObservations.getSaturation()) == 0);
    assertNull(itoObservations.getSpread());

    dummyItoSpread = SimulationInputVO.MAX_SPREAD;
    final BigDecimal bdDummyItoSpread = new BigDecimal(dummyItoSpread);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .itoC50(dummyItoC50)
                                                       .itoHill(dummyItoHill)
                                                       .itoSaturation(dummyItoSaturation)
                                                       .itoSpread(dummyItoSpread)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .credibleIntervalPctiles(dummyCIPctiles)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    itoObservations = simulationInputVO.getItoObservations();
    assertTrue(bdDummyItoC50.compareTo(itoObservations.getC50()) == 0);
    assertTrue(bdDummyItoHill.compareTo(itoObservations.getHill()) == 0);
    assertTrue(bdDummyItoSaturation.compareTo(itoObservations.getSaturation()) == 0);
    assertTrue(bdDummyItoSpread.compareTo(itoObservations.getSpread()) == 0);

    /*
     * Good INa
     */
    dummyINaC50 = "5.5";
    final BigDecimal bdDummyINaC50 = new BigDecimal(dummyINaC50);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iNaC50(dummyINaC50)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    MeasurementObservationsVO iNaObservations = simulationInputVO.getINaObservations();
    assertTrue(bdDummyINaC50.compareTo(iNaObservations.getC50()) == 0);
    assertTrue(defaultHillCoefficient.compareTo(iNaObservations.getHill()) == 0);
    assertTrue(defaultSaturation.compareTo(iNaObservations.getSaturation()) == 0);
    assertNull(iNaObservations.getSpread());

    dummyINaHill = "0.55";
    final BigDecimal bdDummyINaHill = new BigDecimal(dummyINaHill);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iNaC50(dummyINaC50)
                                                       .iNaHill(dummyINaHill)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iNaObservations = simulationInputVO.getINaObservations();
    assertTrue(bdDummyINaC50.compareTo(iNaObservations.getC50()) == 0);
    assertTrue(bdDummyINaHill.compareTo(iNaObservations.getHill()) == 0);
    assertTrue(defaultSaturation.compareTo(iNaObservations.getSaturation()) == 0);
    assertNull(iNaObservations.getSpread());

    dummyINaSaturation = "50.5";
    final BigDecimal bdDummyINaSaturation = new BigDecimal(dummyINaSaturation);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iNaC50(dummyINaC50)
                                                       .iNaHill(dummyINaHill)
                                                       .iNaSaturation(dummyINaSaturation)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iNaObservations = simulationInputVO.getINaObservations();
    assertTrue(bdDummyINaC50.compareTo(iNaObservations.getC50()) == 0);
    assertTrue(bdDummyINaHill.compareTo(iNaObservations.getHill()) == 0);
    assertTrue(bdDummyINaSaturation.compareTo(iNaObservations.getSaturation()) == 0);
    assertNull(iNaObservations.getSpread());

    dummyINaSpread = SimulationInputVO.MAX_SPREAD;
    final BigDecimal bdDummyINaSpread = new BigDecimal(dummyINaSpread);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iNaC50(dummyINaC50)
                                                       .iNaHill(dummyINaHill)
                                                       .iNaSaturation(dummyINaSaturation)
                                                       .iNaSpread(dummyINaSpread)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .credibleIntervalPctiles(dummyCIPctiles)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iNaObservations = simulationInputVO.getINaObservations();
    assertTrue(bdDummyINaC50.compareTo(iNaObservations.getC50()) == 0);
    assertTrue(bdDummyINaHill.compareTo(iNaObservations.getHill()) == 0);
    assertTrue(bdDummyINaSaturation.compareTo(iNaObservations.getSaturation()) == 0);
    assertTrue(bdDummyINaSpread.compareTo(iNaObservations.getSpread()) == 0);

    /*
     * Good INaL
     */
    dummyINaLC50 = "5.51";
    final BigDecimal bdDummyINaLC50 = new BigDecimal(dummyINaLC50);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iNaLC50(dummyINaLC50)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    MeasurementObservationsVO iNaLObservations = simulationInputVO.getINaLObservations();
    assertTrue(bdDummyINaLC50.compareTo(iNaLObservations.getC50()) == 0);
    assertTrue(defaultHillCoefficient.compareTo(iNaLObservations.getHill()) == 0);
    assertTrue(defaultSaturation.compareTo(iNaLObservations.getSaturation()) == 0);
    assertNull(iNaLObservations.getSpread());

    dummyINaLHill = "0.551";
    final BigDecimal bdDummyINaLHill = new BigDecimal(dummyINaLHill);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iNaLC50(dummyINaLC50)
                                                       .iNaLHill(dummyINaLHill)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iNaLObservations = simulationInputVO.getINaLObservations();
    assertTrue(bdDummyINaLC50.compareTo(iNaLObservations.getC50()) == 0);
    assertTrue(bdDummyINaLHill.compareTo(iNaLObservations.getHill()) == 0);
    assertTrue(defaultSaturation.compareTo(iNaLObservations.getSaturation()) == 0);
    assertNull(iNaLObservations.getSpread());

    dummyINaLSaturation = "50.51";
    final BigDecimal bdDummyINaLSaturation = new BigDecimal(dummyINaLSaturation);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iNaLC50(dummyINaLC50)
                                                       .iNaLHill(dummyINaLHill)
                                                       .iNaLSaturation(dummyINaLSaturation)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iNaLObservations = simulationInputVO.getINaLObservations();
    assertTrue(bdDummyINaLC50.compareTo(iNaLObservations.getC50()) == 0);
    assertTrue(bdDummyINaLHill.compareTo(iNaLObservations.getHill()) == 0);
    assertTrue(bdDummyINaLSaturation.compareTo(iNaLObservations.getSaturation()) == 0);
    assertNull(iNaLObservations.getSpread());

    dummyINaLSpread = SimulationInputVO.MAX_SPREAD;
    final BigDecimal bdDummyINaLSpread = new BigDecimal(dummyINaLSpread);

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iNaLC50(dummyINaLC50)
                                                       .iNaLHill(dummyINaLHill)
                                                       .iNaLSaturation(dummyINaLSaturation)
                                                       .iNaLSpread(dummyINaLSpread)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .credibleIntervalPctiles(dummyCIPctiles)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    iNaLObservations = simulationInputVO.getINaLObservations();
    assertTrue(bdDummyINaLC50.compareTo(iNaLObservations.getC50()) == 0);
    assertTrue(bdDummyINaLHill.compareTo(iNaLObservations.getHill()) == 0);
    assertTrue(bdDummyINaLSaturation.compareTo(iNaLObservations.getSaturation()) == 0);
    assertTrue(bdDummyINaLSpread.compareTo(iNaLObservations.getSpread()) == 0);

    /*
     * Good plasma concentration range
     */
    final BigDecimal maxPlasmaConc = Configuration.RECOMMENDED_PLASMA_CONC_MAX;
    final BigDecimal minPlasmaConc = Configuration.PLASMA_CONC_MIN;
    dummyPlasmaConcMin = minPlasmaConc.toPlainString();
    dummyPlasmaConcMax = maxPlasmaConc.toPlainString();
    dummyPlasmaIntPtLogScale = Boolean.TRUE.toString();
    dummyPlasmaIntPtCount = "8";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaConcMin(dummyPlasmaConcMin)
                                                       .plasmaConcMax(dummyPlasmaConcMax)
                                                       .plasmaIntPtLogScale(dummyPlasmaIntPtLogScale)
                                                       .plasmaIntPtCount(dummyPlasmaIntPtCount)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    assertTrue(new BigDecimal(dummyPlasmaConcMax).compareTo(simulationInputVO.getPlasmaConcMax()) == 0);
    assertTrue(new BigDecimal(dummyPlasmaConcMin).compareTo(simulationInputVO.getPlasmaConcMin()) == 0);
    assertEquals(Short.valueOf(dummyPlasmaIntPtCount),
                 simulationInputVO.getPlasmaIntPtCount());
    assertTrue(simulationInputVO.getPlasmaIntPtLogScale());
    assertEquals(InputOption.CONC_RANGE, simulationInputVO.retrieveInputOption());

    /*
     * Good plasma concentration points
     */
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    assertEquals(Arrays.asList(dummyMinPlasmaConcPoints),
                 simulationInputVO.getPlasmaConcPoints());
    assertEquals(InputOption.CONC_POINTS, simulationInputVO.retrieveInputOption());

    // Borderline case of count of points exceeding *current*(!) max permitted.
    final short exceedingMaxPointCount = SimulationInputVO.MAX_CONC_POINT_COUNT + 1;
    final String[] dummyPlasmaConcPoints = new String[exceedingMaxPointCount];
    for (short idx = 0; idx <= SimulationInputVO.MAX_CONC_POINT_COUNT; idx++) {
      dummyPlasmaConcPoints[idx] = String.valueOf(idx);
    }
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaConcPoints(dummyPlasmaConcPoints)
                                                       .build();
    assertFalse(simulationInputVO.getAssignmentProblems()
                .contains(SimulationInputVO.INVALID_PK_OR_CONCS));
    assertFalse(simulationInputVO.getAssignmentProblems()
               .contains(SimulationInputVO.INVALID_PLASMA_CONC_POINT));
    assertEquals(SimulationInputVO.MAX_CONC_POINT_COUNT,
                 simulationInputVO.getPlasmaConcPoints().size());
    assertEquals("0", simulationInputVO.getPlasmaConcPoints().get(0));
    assertEquals("19", simulationInputVO.getPlasmaConcPoints().get(19));

    // Exceeding *current*(!) max permitted, and removal of duplicates, and sorted.
    for (short idx = SimulationInputVO.MAX_CONC_POINT_COUNT; idx >= 0; idx--) {
      dummyPlasmaConcPoints[idx] = String.valueOf(idx);
    }
    dummyPlasmaConcPoints[1] = "0";
    dummyPlasmaConcPoints[2] = "0";
    dummyPlasmaConcPoints[3] = "0";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaConcPoints(dummyPlasmaConcPoints)
                                                       .build();
    assertFalse(simulationInputVO.getAssignmentProblems()
                .contains(SimulationInputVO.INVALID_PK_OR_CONCS));
    assertFalse(simulationInputVO.getAssignmentProblems()
               .contains(SimulationInputVO.INVALID_PLASMA_CONC_POINT));
    // Effectively [0, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
    assertEquals(18, simulationInputVO.getPlasmaConcPoints().size());
    assertEquals("0", simulationInputVO.getPlasmaConcPoints().get(0));
    assertEquals("20", simulationInputVO.getPlasmaConcPoints().get(17));

    // Exceeding *current*(!) max permitted, and removal of duplicates, sorted
    // and exp
    for (short idx = SimulationInputVO.MAX_CONC_POINT_COUNT; idx >= 0; idx--) {
      dummyPlasmaConcPoints[idx] = String.valueOf(idx+1);
    }
    dummyPlasmaConcPoints[1] = "1";
    dummyPlasmaConcPoints[2] = "1";
    dummyPlasmaConcPoints[3] = "1";
    dummyPlasmaConcPoints[4] = "4e3";
    dummyPlasmaConcPoints[5] = "5e2";
    dummyPlasmaConcPoints[6] = "2e-2";
    // Effectively [1, 1, 1, 1, 4e3, 5e2, 2e-2, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21]
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaConcPoints(dummyPlasmaConcPoints)
                                                       .build();
    assertFalse(simulationInputVO.getAssignmentProblems()
                .contains(SimulationInputVO.INVALID_PK_OR_CONCS));
    assertFalse(simulationInputVO.getAssignmentProblems()
               .contains(SimulationInputVO.INVALID_PLASMA_CONC_POINT));
    // Effectively [0.02, 1, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 500, 4000]
    assertEquals(18, simulationInputVO.getPlasmaConcPoints().size());
    assertEquals("0.02", simulationInputVO.getPlasmaConcPoints().get(0));
    assertEquals("4000", simulationInputVO.getPlasmaConcPoints().get(17));

    /*
     * Good PK file
     */
    mockPKFile = EasyMock.createNiceMock(PortalFile.class);
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .pkFile(mockPKFile)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    assertSame(mockPKFile, simulationInputVO.getPkFile());
    assertEquals(InputOption.PK, simulationInputVO.retrieveInputOption());

    /* Test everything */
    dummyPlasmaIntPtLogScale = "true";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iCaLC50(dummyICaLC50)
                                                       .iCaLHill(dummyICaLHill)
                                                       .iCaLSaturation(dummyICaLSaturation)
                                                       .iK1C50(dummyIK1C50)
                                                       .iK1Hill(dummyIK1Hill)
                                                       .iK1Saturation(dummyIK1Saturation)
                                                       .iKrC50(dummyIKrC50)
                                                       .iKrHill(dummyIKrHill)
                                                       .iKrSaturation(dummyIKrSaturation)
                                                       .iKsC50(dummyIKsC50)
                                                       .iKsHill(dummyIKsHill)
                                                       .iKsSaturation(dummyIKsSaturation)
                                                       .iNaC50(dummyINaC50)
                                                       .iNaHill(dummyINaHill)
                                                       .iNaSaturation(dummyINaSaturation)
                                                       .itoC50(dummyItoC50)
                                                       .itoHill(dummyItoHill)
                                                       .itoSaturation(dummyItoSaturation)
                                                       .notes(dummyNotes)
                                                       .pacingFrequency(dummyPacingFrequency)
                                                       .pacingMaxTime(dummyPacingMaxTime)
                                                       .plasmaConcMax(dummyPlasmaConcMax)
                                                       .plasmaConcMin(dummyPlasmaConcMin)
                                                       .plasmaIntPtCount(dummyPlasmaIntPtCount)
                                                       .plasmaIntPtLogScale(dummyPlasmaIntPtLogScale)
                                                       .build();
  }

  @Test
  public void testPKFile() {
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .build();

    assertNull(simulationInputVO.getPkFile());

    final PortalFile mockPortalFile = mocksControl.createMock(PortalFile.class);

    mocksControl.replay();

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .pkFile(mockPortalFile)
                                                       .build();

    mocksControl.verify();

    assertSame(mockPortalFile, simulationInputVO.getPkFile());
  }

  @Test
  public void testPIC50Retrieval() {
    mockStatic(ConverterUtil.class);

    // No (p)IC50 values provided.
    dummyModelIdentifier = "1";
    dummyC50Type = C50_TYPE.IC50.toString();
    dummyIC50Units = IC50_UNIT.µM.toString();
    dummyPlasmaConcMax = "100.0";

    replayAll();

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();

    verifyAll();

    assertNull(simulationInputVO.retrievePIC50ICaL());
    assertNull(simulationInputVO.retrievePIC50Ik1());
    assertNull(simulationInputVO.retrievePIC50Iks());
    assertNull(simulationInputVO.retrievePIC50Ikr());
    assertNull(simulationInputVO.retrievePIC50INa());
    assertNull(simulationInputVO.retrievePIC50INaL());
    assertNull(simulationInputVO.retrievePIC50Ito());

    resetAll();

    // pIC50 value provided.
    dummyC50Type = C50_TYPE.pIC50.toString();
    dummyICaLC50 = "5.0";

    replayAll();

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iCaLC50(dummyICaLC50)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();

    verifyAll();

    assertTrue(new BigDecimal(dummyICaLC50).compareTo(simulationInputVO.retrievePIC50ICaL()) == 0);

    resetAll();

    // IC50 value conversions.
    dummyC50Type = C50_TYPE.IC50.toString();
    dummyIC50Units = IC50_UNIT.M.toString();
    dummyICaLC50 = "0.000001";

    final BigDecimal fakeReturnedPIC50 = BigDecimal.TEN;
    expect(ConverterUtil.ic50ToPIC50(IC50_UNIT.M, new BigDecimal(dummyICaLC50)))
          .andReturn(fakeReturnedPIC50);

    replayAll();

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iCaLC50(dummyICaLC50)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    simulationInputVO.retrievePIC50ICaL();

    verifyAll();

    resetAll();

    dummyC50Type = C50_TYPE.IC50.toString();
    dummyIC50Units = IC50_UNIT.nM.toString();
    dummyICaLC50 = "100.0";

    expect(ConverterUtil.ic50ToPIC50(IC50_UNIT.nM, new BigDecimal(dummyICaLC50)))
          .andReturn(fakeReturnedPIC50);

    replayAll();

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iCaLC50(dummyICaLC50)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    simulationInputVO.retrievePIC50ICaL();

    verifyAll();

    resetAll();

    dummyC50Type = C50_TYPE.IC50.toString();
    dummyIC50Units = IC50_UNIT.µM.toString();
    dummyICaLC50 = "100";
    dummyIK1C50 = "10";
    dummyIKrC50 = "1";
    dummyIKsC50 = "0.1";
    dummyINaC50 = "0.01";
    dummyItoC50 = "0.001";
    dummyINaLC50 = "0.0001";

    expect(ConverterUtil.ic50ToPIC50(IC50_UNIT.µM, new BigDecimal(dummyICaLC50)))
          .andReturn(fakeReturnedPIC50);
    expect(ConverterUtil.ic50ToPIC50(IC50_UNIT.µM, new BigDecimal(dummyIK1C50)))
          .andReturn(fakeReturnedPIC50);
    expect(ConverterUtil.ic50ToPIC50(IC50_UNIT.µM, new BigDecimal(dummyIKrC50)))
          .andReturn(fakeReturnedPIC50);
    expect(ConverterUtil.ic50ToPIC50(IC50_UNIT.µM, new BigDecimal(dummyIKsC50)))
          .andReturn(fakeReturnedPIC50);
    expect(ConverterUtil.ic50ToPIC50(IC50_UNIT.µM, new BigDecimal(dummyINaC50)))
          .andReturn(fakeReturnedPIC50);
    expect(ConverterUtil.ic50ToPIC50(IC50_UNIT.µM, new BigDecimal(dummyINaLC50)))
          .andReturn(fakeReturnedPIC50);
    expect(ConverterUtil.ic50ToPIC50(IC50_UNIT.µM, new BigDecimal(dummyItoC50)))
          .andReturn(fakeReturnedPIC50);

    replayAll();

    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .ic50Units(dummyIC50Units)
                                                       .iCaLC50(dummyICaLC50)
                                                       .iK1C50(dummyIK1C50)
                                                       .iKrC50(dummyIKrC50)
                                                       .iKsC50(dummyIKsC50)
                                                       .iNaC50(dummyINaC50)
                                                       .iNaLC50(dummyINaLC50)
                                                       .itoC50(dummyItoC50)
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();
    simulationInputVO.retrievePIC50ICaL();
    simulationInputVO.retrievePIC50Ik1();
    simulationInputVO.retrievePIC50Ikr();
    simulationInputVO.retrievePIC50Iks();
    simulationInputVO.retrievePIC50INa();
    simulationInputVO.retrievePIC50INaL();
    simulationInputVO.retrievePIC50Ito();

    verifyAll();
  }

  @Test
  public void testProblemGenerationOnBadData() {
    /*
     * Bad model identifier
     */
    dummyModelIdentifier = "dummyModelIdentifier";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_MODEL_IDENTIFIER));
    assertNull(simulationInputVO.getModelIdentifier());
    assertTrue(simulationInputVO.hasAssignmentProblems());

    /*
     * Bad pacing max time max
     */
    dummyPacingMaxTime = "badPacingMaxTime";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .pacingMaxTime(dummyPacingMaxTime)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_PACING_MAX_TIME));
    assertSame(null, simulationInputVO.getPacingMaxTime());

    final BigDecimal maxPacingMaxTime = new BigDecimal(SimulationInputVO.MAX_PACING_MAX_TIME);
    dummyPacingMaxTime = maxPacingMaxTime.add(smallAmount).toPlainString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .pacingMaxTime(dummyPacingMaxTime)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_PACING_MAX_TIME));
    assertTrue(new BigDecimal(dummyPacingMaxTime)
                             .compareTo(simulationInputVO.getPacingMaxTime()) == 0);

    /*
     * Bad pacing max time min
     */
    dummyPacingMaxTime = SimulationInputVO.MIN_PACING_MAX_TIME;
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .pacingMaxTime(dummyPacingMaxTime)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_PACING_MAX_TIME));
    assertTrue(new BigDecimal(dummyPacingMaxTime)
                             .compareTo(simulationInputVO.getPacingMaxTime()) == 0);

    final BigDecimal minPacingMaxTime = new BigDecimal(SimulationInputVO.MIN_PACING_MAX_TIME);
    dummyPacingMaxTime = minPacingMaxTime.subtract(smallAmount).toPlainString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .pacingMaxTime(dummyPacingMaxTime)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_PACING_MAX_TIME));
    assertTrue(new BigDecimal(dummyPacingMaxTime)
                             .compareTo(simulationInputVO.getPacingMaxTime()) == 0);

    /*
     * Bad pacing frequency max
     */
    dummyPacingFrequency = "badPacingFrequency";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .pacingFrequency(dummyPacingFrequency)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_PACING_FREQUENCY));
    assertNull(simulationInputVO.getPacingFrequency());

    final BigDecimal maxPacingFrequency = new BigDecimal(SimulationInputVO.MAX_PACING_FREQUENCY);
    dummyPacingFrequency =  maxPacingFrequency.add(smallAmount).toPlainString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .pacingFrequency(dummyPacingFrequency)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().contains(SimulationInputVO.INVALID_PACING_FREQUENCY));
    assertTrue(new BigDecimal(dummyPacingFrequency)
                             .compareTo(simulationInputVO.getPacingFrequency()) == 0);

    /*
     * Bad pacing frequency min
     */
    final BigDecimal minPacingFrequency = new BigDecimal(SimulationInputVO.MIN_PACING_FREQUENCY);
    dummyPacingFrequency = minPacingFrequency.subtract(smallAmount).toPlainString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .pacingFrequency(dummyPacingFrequency)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_PACING_FREQUENCY));
    assertTrue(new BigDecimal(dummyPacingFrequency)
                             .compareTo(simulationInputVO.getPacingFrequency()) == 0);

    /*
     * No type assigned.
     */
    dummyICaLC50 = "60.0";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iCaLC50(dummyICaLC50)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_TYPE_NONASSIGNMENT));

    /*
     * IC50 assigned but no units
     */
    dummyICaLC50 = "6.0";
    dummyC50Type = C50_TYPE.IC50.toString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .iCaLC50(dummyICaLC50)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_UNIT_NONASSIGNMENT_IF_IC50));
 
    /*
     * Bad ICaL
     */
    dummyICaLC50 = "badICaLC50";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iCaLC50(dummyICaLC50)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidICaLC50));

    // For the first ion channel try a range of Hill values.
    dummyICaLHill = "badICaLHill";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iCaLHill(dummyICaLHill)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidICaLHill));

    dummyICaLHill = new BigDecimal(SimulationInputVO.MIN_HILL_COEFFICIENT)
                                  .subtract(smallAmount).toPlainString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iCaLHill(dummyICaLHill)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidICaLHill));

    dummyICaLHill = new BigDecimal(SimulationInputVO.MAX_HILL_COEFFICIENT)
                                  .add(smallAmount).toPlainString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iCaLHill(dummyICaLHill)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidICaLHill));

    dummyICaLSaturation = "badICaLSaturation";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iCaLSaturation(dummyICaLSaturation)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidICaLSaturation));

    dummyICaLSaturation = new BigDecimal(SimulationInputVO.MIN_SATURATION)
                                        .subtract(smallAmount).toPlainString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iCaLSaturation(dummyICaLSaturation)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidICaLSaturation));

    // Invalid spread values
    dummyICaLSpread = "badICaLSpread";
    dummyCIPctiles = "dummyCIPctiles";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iCaLSpread(dummyICaLSpread)
                                                       .credibleIntervalPctiles(dummyCIPctiles)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidICaLSpread));
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_SPREAD_PCTILES));

    // Invalid if not greater than min spread value
    dummyICaLSpread = SimulationInputVO.MIN_SPREAD_GT;
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iCaLSpread(dummyICaLSpread)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidICaLSpread));
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_SPREAD_PCTILES));

    dummyICaLSpread = new BigDecimal(SimulationInputVO.MAX_SPREAD)
                                    .add(smallAmount).toString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iCaLSpread(dummyICaLSpread)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidICaLSpread));
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_SPREAD_PCTILES));

    // Try assigning a valid Hill, saturation and spread without an IC50
    dummyICaLHill = "0.5";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iCaLHill(dummyICaLHill)
                                                       .build();
    final String invalidObservationICaL = SimulationInputVO.invalidObservationConstruct(IonCurrent.ICaL);
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidObservationICaL));

    dummyICaLSaturation = "0.5";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iCaLSaturation(dummyICaLSaturation)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidObservationICaL));

    dummyICaLSpread = SimulationInputVO.MAX_SPREAD;
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iCaLSpread(dummyICaLSpread)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidObservationICaL));
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_SPREAD_PCTILES));

    /*
     * Bad Ik1
     */
    dummyIK1C50 = "badIK1C50";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iK1C50(dummyIK1C50)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidIK1C50));
    assertNull(simulationInputVO.getIK1Observations().getC50());

    dummyIK1Hill = "badIK1Hill";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iK1Hill(dummyIK1Hill)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidIK1Hill));
    assertNull(simulationInputVO.getIK1Observations().getHill());

    dummyIK1Hill = "0.5";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iK1Hill(dummyIK1Hill)
                                                       .build();
    final String invalidObservationIK1 = SimulationInputVO.invalidObservationConstruct(IonCurrent.IK1);
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidObservationIK1));

    dummyIK1Saturation = "badIK1Saturation";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iK1Saturation(dummyIK1Saturation)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidIK1Saturation));
    assertNull(simulationInputVO.getIK1Observations().getSaturation());

    /*
     * Bad Ikr
     */
    dummyIKrC50 = "badIKrC50";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iKrC50(dummyIKrC50)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().contains(invalidIKrC50));
    assertNull(simulationInputVO.getIKrObservations().getC50());

    dummyIKrHill = "badIKrHill";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iKrHill(dummyIKrHill)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().contains(invalidIKrHill));
    assertNull(simulationInputVO.getIKrObservations().getHill());

    dummyIKrHill = "0.5";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iKrHill(dummyIKrHill)
                                                       .build();
    final String invalidObservationIKr = SimulationInputVO.invalidObservationConstruct(IonCurrent.IKr);
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidObservationIKr));

    dummyIKrSaturation = "badIKrSaturation";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iKrSaturation(dummyIKrSaturation)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidIKrSaturation));
    assertNull(simulationInputVO.getIKrObservations().getSaturation());

    /*
     * Bad Iks
     */
    dummyIKsC50 = "badIKsC50";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iKsC50(dummyIKsC50)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidIKsC50));
    assertNull(simulationInputVO.getIKsObservations().getC50());

    dummyIKsHill = "badIKsHill";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iKsHill(dummyIKsHill)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidIKsHill));
    assertNull(simulationInputVO.getIKsObservations().getHill());

    dummyIKsHill = "0.5";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iKsHill(dummyIKsHill)
                                                       .build();
    final String invalidObservationIKs = SimulationInputVO.invalidObservationConstruct(IonCurrent.IKs);
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidObservationIKs));

    dummyIKsSaturation = "badIKsSaturation";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iKsSaturation(dummyIKsSaturation)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidIKsSaturation));
    assertNull(simulationInputVO.getIKsObservations().getSaturation());

    /*
     * Bad Ito
     */
    dummyItoC50 = "badItoC50";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .itoC50(dummyItoC50)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().contains(invalidItoC50));
    assertNull(simulationInputVO.getItoObservations().getC50());

    dummyItoHill = "badItoHill";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .itoHill(dummyItoHill)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().contains(invalidItoHill));
    assertNull(simulationInputVO.getItoObservations().getHill());

    dummyItoHill = "0.5";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .itoHill(dummyItoHill)
                                                       .build();
    final String invalidObservationIto = SimulationInputVO.invalidObservationConstruct(IonCurrent.Ito);
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidObservationIto));

    dummyItoSaturation = "badItoSaturation";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .itoSaturation(dummyItoSaturation)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidItoSaturation));
    assertNull(simulationInputVO.getItoObservations().getSaturation());

    /*
     * Bad INa
     */
    dummyINaC50 = "badINaC50";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iNaC50(dummyINaC50)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().contains(invalidINaC50));
    assertNull(simulationInputVO.getINaObservations().getC50());

    dummyINaHill = "badINaHill";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iNaHill(dummyINaHill)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().contains(invalidINaHill));
    assertNull(simulationInputVO.getINaObservations().getHill());

    dummyINaHill = "0.5";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iNaHill(dummyINaHill)
                                                       .build();
    final String invalidObservationINa = SimulationInputVO.invalidObservationConstruct(IonCurrent.INa);
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidObservationINa));

    dummyINaSaturation = "badINaSaturation";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iNaSaturation(dummyINaSaturation)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidINaSaturation));
    assertNull(simulationInputVO.getINaObservations().getSaturation());

    /*
     * Bad INaL
     */
    dummyINaLC50 = "badINaLC50";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iNaLC50(dummyINaLC50)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().contains(invalidINaLC50));
    assertNull(simulationInputVO.getINaLObservations().getC50());

    dummyINaLHill = "badINaLHill";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iNaLHill(dummyINaLHill)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems().contains(invalidINaLHill));
    assertNull(simulationInputVO.getINaLObservations().getHill());

    dummyINaLHill = "0.5";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iNaLHill(dummyINaLHill)
                                                       .build();
    final String invalidObservationINaL = SimulationInputVO.invalidObservationConstruct(IonCurrent.INaL);
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidObservationINaL));

    dummyINaLSaturation = "badINaLSaturation";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iNaLSaturation(dummyINaLSaturation)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(invalidINaLSaturation));
    assertNull(simulationInputVO.getINaLObservations().getSaturation());

    /*
     * Bad C50 Type
     */
    dummyC50Type = "badC50Type";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_C50_TYPE));
    assertNull(simulationInputVO.getC50Type());

    dummyC50Type = C50_TYPE.IC50.toString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .build();
    assertFalse(simulationInputVO.getAssignmentProblems()
                                 .contains(SimulationInputVO.INVALID_C50_TYPE));
    assertSame(C50_TYPE.IC50, simulationInputVO.getC50Type());

    dummyC50Type = null;
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .c50Type(dummyC50Type)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.SPECIFY_C50_TYPE));
    assertNull(simulationInputVO.getC50Type());

    /*
     * Bad IC50 units
     */
    dummyIC50Units = "badC50Units";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .ic50Units(dummyIC50Units)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_IC50_UNITS));
    assertNull(simulationInputVO.getIc50Units());

    dummyIC50Units = IC50_UNIT.nM.toString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .ic50Units(dummyIC50Units)
                                                       .build();
    assertFalse(simulationInputVO.getAssignmentProblems()
                                 .contains(SimulationInputVO.INVALID_IC50_UNITS));
    assertSame(IC50_UNIT.nM, simulationInputVO.getIc50Units());

    dummyIC50Units = null;
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .ic50Units(dummyIC50Units)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
              .contains(SimulationInputVO.SPECIFY_IC50_UNITS));
    assertNull(simulationInputVO.getIc50Units());

    /*
     * Bad plasma conc max.
     */
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
              .contains(SimulationInputVO.INVALID_PK_OR_CONCS));
    try {
      simulationInputVO.retrieveInputOption();
      fail("Should not permit querying for undefined input option.");
    } catch (IllegalStateException e) {
      assertEquals("Indeterminable simulation type", e.getMessage());
    }

    dummyPlasmaConcMax = "badPlasmaConcMax";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaConcMax(dummyPlasmaConcMax)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
              .contains(SimulationInputVO.INVALID_PLASMA_CONC_MAX));
    assertNull(simulationInputVO.getPlasmaConcMax());

    final BigDecimal maxPlasmaConc = Configuration.RECOMMENDED_PLASMA_CONC_MAX;
    dummyPlasmaConcMax = maxPlasmaConc.add(smallAmount).toPlainString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaConcMax(dummyPlasmaConcMax)
                                                       .build();
    assertFalse(simulationInputVO.getAssignmentProblems()
               .contains(SimulationInputVO.INVALID_PLASMA_CONC_MAX));
    assertTrue(new BigDecimal(dummyPlasmaConcMax)
              .compareTo(simulationInputVO.getPlasmaConcMax()) == 0);

    dummyPlasmaConcMax = Configuration.RECOMMENDED_PLASMA_CONC_MAX.toString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaConcMax(dummyPlasmaConcMax)
                                                       .build();
    assertFalse(simulationInputVO.getAssignmentProblems()
                                 .contains(SimulationInputVO.INVALID_PLASMA_CONC_MAX));
    assertEquals(new BigDecimal(dummyPlasmaConcMax),
                 simulationInputVO.getPlasmaConcMax());

    dummyPlasmaConcMax = maxPlasmaConc.subtract(smallAmount).toPlainString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaConcMax(dummyPlasmaConcMax)
                                                       .build();
    assertFalse(simulationInputVO.getAssignmentProblems()
                                 .contains(SimulationInputVO.INVALID_PLASMA_CONC_MAX));
    assertEquals(new BigDecimal(dummyPlasmaConcMax),
                 simulationInputVO.getPlasmaConcMax());

    /*
     * Bad plasma conc min.
     */
    dummyPlasmaConcMin = "badPlasmaConcMin";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaConcMin(dummyPlasmaConcMin)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
              .contains(SimulationInputVO.INVALID_PLASMA_CONC_MIN));
    assertNull(simulationInputVO.getPlasmaConcMin());

    final BigDecimal minPlasmaConc = BigDecimal.ZERO;
    dummyPlasmaConcMin = minPlasmaConc.add(smallAmount).toPlainString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaConcMin(dummyPlasmaConcMin)
                                                       .build();
    assertFalse(simulationInputVO.getAssignmentProblems()
               .contains(SimulationInputVO.INVALID_PLASMA_CONC_MIN));
    assertEquals(new BigDecimal(dummyPlasmaConcMin),
                 simulationInputVO.getPlasmaConcMin());

    dummyPlasmaConcMin = Configuration.PLASMA_CONC_MIN.toString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaConcMin(dummyPlasmaConcMin)
                                                       .build();
    assertFalse(simulationInputVO.getAssignmentProblems()
               .contains(SimulationInputVO.INVALID_PLASMA_CONC_MIN));
    assertEquals(new BigDecimal(dummyPlasmaConcMin),
                 simulationInputVO.getPlasmaConcMin());

    dummyPlasmaConcMin = minPlasmaConc.subtract(smallAmount).toPlainString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaConcMin(dummyPlasmaConcMin)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_PLASMA_CONC_MIN));
    assertTrue(new BigDecimal(dummyPlasmaConcMin)
                             .compareTo(simulationInputVO.getPlasmaConcMin()) == 0);

    dummyPlasmaConcMax = minPlasmaConc.toPlainString();
    dummyPlasmaConcMin = maxPlasmaConc.toPlainString();
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaConcMin(dummyPlasmaConcMin)
                                                       .plasmaConcMax(dummyPlasmaConcMax)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_PLASMA_CONC_MIN_MAX));
    assertTrue(new BigDecimal(dummyPlasmaConcMax)
                             .compareTo(simulationInputVO.getPlasmaConcMax()) == 0);
    assertTrue(new BigDecimal(dummyPlasmaConcMin)
                             .compareTo(simulationInputVO.getPlasmaConcMin()) == 0);

    /*
     * Bad plasma intermediate point count.
     */
    dummyPlasmaIntPtCount = "badPlasmaConcIntPtCnt";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaIntPtCount(dummyPlasmaIntPtCount)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_PLASMA_INT_PT_COUNT));
    assertNull(simulationInputVO.getPlasmaIntPtCount());

    dummyPlasmaIntPtCount = "-1";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaIntPtCount(dummyPlasmaIntPtCount)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_NEGATIVE_PLASMA_INT_PT_CNT));
    assertEquals(Short.valueOf(dummyPlasmaIntPtCount),
                 simulationInputVO.getPlasmaIntPtCount());

    dummyPlasmaIntPtCount = String.valueOf(Short.valueOf(SimulationInputVO.MAX_PLASMA_INT_PT_CNT) + 1);
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaIntPtCount(dummyPlasmaIntPtCount)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_EXCESS_PLASMA_INT_PT_CNT));
    assertEquals(Short.valueOf(dummyPlasmaIntPtCount),
                 simulationInputVO.getPlasmaIntPtCount());

    /*
     * Bad plasma intermediate point log scale.
     */
    // Unspecified value generates a "true" for log scale.
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .build();
    assertFalse(simulationInputVO.getAssignmentProblems()
                                 .contains(SimulationInputVO.INVALID_PLASMA_INT_PT_LOG_SCALE));
    assertTrue(simulationInputVO.getPlasmaIntPtLogScale());

    dummyPlasmaIntPtLogScale = "badPlasmaIntPtLogScale";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaIntPtLogScale(dummyPlasmaIntPtLogScale)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
                                .contains(SimulationInputVO.INVALID_PLASMA_INT_PT_LOG_SCALE));
    assertTrue(simulationInputVO.getPlasmaIntPtLogScale());

    dummyPlasmaIntPtLogScale = "true";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaIntPtLogScale(dummyPlasmaIntPtLogScale)
                                                       .build();
    assertFalse(simulationInputVO.getAssignmentProblems()
                                 .contains(SimulationInputVO.INVALID_PLASMA_INT_PT_LOG_SCALE));
    assertTrue(simulationInputVO.getPlasmaIntPtLogScale());

    dummyPlasmaIntPtLogScale = "1";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaIntPtLogScale(dummyPlasmaIntPtLogScale)
                                                       .build();
    assertFalse(simulationInputVO.getAssignmentProblems()
                                 .contains(SimulationInputVO.INVALID_PLASMA_INT_PT_LOG_SCALE));
    assertTrue(simulationInputVO.getPlasmaIntPtLogScale());

    dummyPlasmaIntPtLogScale = "false";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaIntPtLogScale(dummyPlasmaIntPtLogScale)
                                                       .build();
    assertFalse(simulationInputVO.getAssignmentProblems()
                                 .contains(SimulationInputVO.INVALID_PLASMA_INT_PT_LOG_SCALE));
    assertFalse(simulationInputVO.getPlasmaIntPtLogScale());

    dummyPlasmaIntPtLogScale = "0";
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaIntPtLogScale(dummyPlasmaIntPtLogScale)
                                                       .build();
    assertFalse(simulationInputVO.getAssignmentProblems()
               .contains(SimulationInputVO.INVALID_PLASMA_INT_PT_LOG_SCALE));
    assertFalse(simulationInputVO.getPlasmaIntPtLogScale());

    /*
     * Bad plasma points
     */
    String[] dummyPlasmaConcPoints = null;
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaConcPoints(dummyPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
              .contains(SimulationInputVO.INVALID_PK_OR_CONCS));

    dummyPlasmaConcPoints = new String[] {};
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaConcPoints(dummyPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
              .contains(SimulationInputVO.INVALID_PK_OR_CONCS));

    dummyPlasmaConcPoints = new String[] { " " };
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaConcPoints(dummyPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
              .contains(SimulationInputVO.INVALID_PK_OR_CONCS));

    dummyPlasmaConcPoints = new String[] { " ", "dummyPlasmaConcPoint" };
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaConcPoints(dummyPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
              .contains(SimulationInputVO.INVALID_PLASMA_CONC_POINT));

    dummyPlasmaConcPoints = new String[] { " ", "-1" };
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .plasmaConcPoints(dummyPlasmaConcPoints)
                                                       .build();
    assertTrue(simulationInputVO.getAssignmentProblems()
              .contains(SimulationInputVO.INVALID_PK_OR_CONCS));
  }

  @Test
  public void testPIC50FromIC50Values() {
    final BigDecimal ic50_ICaL_M = new BigDecimal("0.0166");
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iCaLC50(ic50_ICaL_M.toPlainString())
                                                       .c50Type(C50_TYPE.IC50.toString())
                                                       .ic50Units(IC50_UNIT.M.toString())
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();

    final BigDecimal pIC50From_M = simulationInputVO.retrievePIC50ICaL();

    final BigDecimal ic50_ICaL_uM = new BigDecimal("16600");
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iCaLC50(ic50_ICaL_uM.toPlainString())
                                                       .c50Type(C50_TYPE.IC50.toString())
                                                       .ic50Units(IC50_UNIT.µM.toString())
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();

    final BigDecimal pIC50From_uM = simulationInputVO.retrievePIC50ICaL();

    assertEquals(pIC50From_M, pIC50From_uM);

    final BigDecimal ic50_ICaL_nM = new BigDecimal("16600000");
    simulationInputVO = new SimulationInputVO.Builder().modelIdentifier(dummyModelIdentifier)
                                                       .iCaLC50(ic50_ICaL_nM.toPlainString())
                                                       .c50Type(C50_TYPE.IC50.toString())
                                                       .ic50Units(IC50_UNIT.nM.toString())
                                                       .plasmaConcPoints(dummyMinPlasmaConcPoints)
                                                       .build();

    final BigDecimal pIC50From_nM = simulationInputVO.retrievePIC50ICaL();

    assertEquals(pIC50From_uM, pIC50From_nM);
  }
}