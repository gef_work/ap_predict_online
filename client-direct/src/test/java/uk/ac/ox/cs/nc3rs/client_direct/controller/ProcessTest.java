/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.Model;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.controller.util.SecurityUtil;
import uk.ac.ox.cs.compbio.client_shared.value.PortalFeature;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.controller.util.ControllerUtil;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation;
import uk.ac.ox.cs.nc3rs.client_direct.entity.SimulationInput;
import uk.ac.ox.cs.nc3rs.client_direct.exception.SimulationNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService;
import uk.ac.ox.cs.nc3rs.client_direct.value.IonCurrent;
import uk.ac.ox.cs.nc3rs.client_direct.value.ModelVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;

/**
 * Unit test the "Process" controller.
 * 
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { Process.class, ControllerUtil.class, SecurityUtil.class,
                   SimulationInputVO.class } )
public class ProcessTest {

  private ClientDirectService mockClientDirectService;
  private HttpSession mockHttpSession;
  private IMocksControl mocksControl;
  private Process processController;
  private Model mockModel;
  private SimulationInputVO mockSimulationInputVO;
  private String dummySimulationId;

  @Before
  public void setUp() {
    processController = new Process();

    mocksControl = createControl();
    mockClientDirectService = mocksControl.createMock(ClientDirectService.class);
    ReflectionTestUtils.setField(processController,
                                 ClientDirectIdentifiers.COMPONENT_CLIENT_DIRECT_SERVICE,
                                 mockClientDirectService);

    mockModel = mocksControl.createMock(Model.class);
    mockHttpSession = mocksControl.createMock(HttpSession.class);
    mockSimulationInputVO = mocksControl.createMock(SimulationInputVO.class);

    dummySimulationId = "dummySimulationId";
  }

  @Test
  public void testSetPropertiesForInput() {
    String dummyExceptionMessage = null;
    final String noCredibleIntervalPctilesConcatenation = "";

    /*
     *  1. Simulation input supplied. Unrestricted feature access.
     */
    final List<ModelVO> dummyModels = new ArrayList<ModelVO>();
    expect(mockClientDirectService.retrieveModels()).andReturn(dummyModels);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_MODELS,
                                  dummyModels))
          .andReturn(mockModel);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_INPUT_VALUES,
                                  mockSimulationInputVO))
          .andReturn(mockModel);

    boolean dummyAllowUncertainties = true;
    mockStatic(ControllerUtil.class);
    expect(ControllerUtil.allowFeature(PortalFeature.UNCERTAINTIES,
                                       mockClientDirectService))
          .andReturn(dummyAllowUncertainties); 
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SHOW_UNCERTAINTIES,
                                  dummyAllowUncertainties))
          .andReturn(mockModel);
    final Map<IonCurrent, BigDecimal> dummySpreads = new HashMap<IonCurrent, BigDecimal>();
    expect(mockClientDirectService.retrieveSpreads()).andReturn(dummySpreads);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_SPREADS,
                                  dummySpreads))
          .andReturn(mockModel);
    final Set<BigDecimal> dummyPctiles = new HashSet<BigDecimal>();
    expect(mockClientDirectService.retrieveCredibleIntervalPctiles())
          .andReturn(dummyPctiles);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_PCTILES,
                                  noCredibleIntervalPctilesConcatenation))
          .andReturn(mockModel);

    boolean dummyAllowDynamicCellML = true;
    expect(ControllerUtil.allowFeature(PortalFeature.DYNAMIC_CELLMLS,
                                       mockClientDirectService))
          .andReturn(dummyAllowDynamicCellML); 
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SHOW_DYNAMIC_CELLML,
                                  dummyAllowDynamicCellML))
          .andReturn(mockModel);
    final Capture<String> modelToken = newCapture();
    final Capture<String> sessionToken = newCapture();
    expect(mockModel.addAttribute(eq(ClientDirectIdentifiers.MODEL_ATTRIBUTE_TOKEN),
                                  capture(modelToken)))
          .andReturn(mockModel);
    mockHttpSession.setAttribute(eq(ClientDirectIdentifiers.SESSION_PARAM_TOKEN),
                                 capture(sessionToken));
    expectLastCall();

    replayAll();
    mocksControl.replay();

    Process.setPropertiesForInput(mockClientDirectService, mockSimulationInputVO,
                                  mockHttpSession, dummyExceptionMessage,
                                  mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(modelToken.getValue(), sessionToken.getValue());

    resetAll();
    mocksControl.reset();

    /*
     * 2. Repeat, but now try with an exception message.
     */
    dummyExceptionMessage = "dummyExceptionMessage";

    expect(mockClientDirectService.retrieveModels()).andReturn(dummyModels);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_MODELS,
                                  dummyModels))
          .andReturn(mockModel);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_INPUT_VALUES,
                                  mockSimulationInputVO))
          .andReturn(mockModel);

    expect(ControllerUtil.allowFeature(PortalFeature.UNCERTAINTIES,
                                       mockClientDirectService))
          .andReturn(dummyAllowUncertainties); 
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SHOW_UNCERTAINTIES,
                                  dummyAllowUncertainties))
          .andReturn(mockModel);
    expect(mockClientDirectService.retrieveSpreads()).andReturn(dummySpreads);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_SPREADS,
                                  dummySpreads))
          .andReturn(mockModel);
    expect(mockClientDirectService.retrieveCredibleIntervalPctiles())
          .andReturn(dummyPctiles);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_PCTILES,
                                  noCredibleIntervalPctilesConcatenation))
          .andReturn(mockModel);

    expect(ControllerUtil.allowFeature(PortalFeature.DYNAMIC_CELLMLS,
                                       mockClientDirectService))
          .andReturn(dummyAllowDynamicCellML);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SHOW_DYNAMIC_CELLML,
                                  dummyAllowDynamicCellML))
          .andReturn(mockModel);

    expect(mockModel.addAttribute(eq(ClientDirectIdentifiers.MODEL_ATTRIBUTE_TOKEN),
                                  capture(modelToken)))
          .andReturn(mockModel);
    mockHttpSession.setAttribute(eq(ClientDirectIdentifiers.SESSION_PARAM_TOKEN),
                                 capture(sessionToken));
    expectLastCall();
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                                  dummyExceptionMessage))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    Process.setPropertiesForInput(mockClientDirectService, mockSimulationInputVO,
                                  mockHttpSession, dummyExceptionMessage,
                                  mockModel);

    verifyAll();
    mocksControl.verify();

    resetAll();
    mocksControl.reset();

    /*
     * 3. Now try with no simulation input.
     */
    mockSimulationInputVO = null;

    expect(mockClientDirectService.retrieveModels()).andReturn(dummyModels);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_MODELS,
                                  dummyModels))
          .andReturn(mockModel);
    final Map<String, String> dummyC50Units = new HashMap<String, String>();
    expect(mockClientDirectService.retrieveC50Units()).andReturn(dummyC50Units);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_C50_UNITS,
                                  dummyC50Units))
          .andReturn(mockModel);

    expect(ControllerUtil.allowFeature(PortalFeature.UNCERTAINTIES,
                                       mockClientDirectService))
          .andReturn(dummyAllowUncertainties); 
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SHOW_UNCERTAINTIES,
                                  dummyAllowUncertainties))
          .andReturn(mockModel);
    expect(mockClientDirectService.retrieveSpreads()).andReturn(dummySpreads);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_SPREADS,
                                  dummySpreads))
          .andReturn(mockModel);
    expect(mockClientDirectService.retrieveCredibleIntervalPctiles())
          .andReturn(dummyPctiles);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_PCTILES,
                                  noCredibleIntervalPctilesConcatenation))
          .andReturn(mockModel);

    expect(ControllerUtil.allowFeature(PortalFeature.DYNAMIC_CELLMLS,
                                       mockClientDirectService))
          .andReturn(dummyAllowDynamicCellML);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SHOW_DYNAMIC_CELLML,
                                  dummyAllowDynamicCellML))
          .andReturn(mockModel);

    expect(mockModel.addAttribute(eq(ClientDirectIdentifiers.MODEL_ATTRIBUTE_TOKEN),
                                  capture(modelToken)))
          .andReturn(mockModel);
    mockHttpSession.setAttribute(eq(ClientDirectIdentifiers.SESSION_PARAM_TOKEN),
                                 capture(sessionToken));
    expectLastCall();
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                                  dummyExceptionMessage))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    Process.setPropertiesForInput(mockClientDirectService, mockSimulationInputVO,
                                  mockHttpSession, dummyExceptionMessage,
                                  mockModel);

    verifyAll();
    mocksControl.verify();

    resetAll();
    mocksControl.reset();

    /*
     * 4. Test deny feature access.
     */
    expect(mockClientDirectService.retrieveModels()).andReturn(dummyModels);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_MODELS,
                                  dummyModels))
          .andReturn(mockModel);
    expect(mockClientDirectService.retrieveC50Units()).andReturn(dummyC50Units);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_C50_UNITS,
                                  dummyC50Units))
          .andReturn(mockModel);

    dummyAllowUncertainties = false;
    expect(ControllerUtil.allowFeature(PortalFeature.UNCERTAINTIES,
                                       mockClientDirectService))
          .andReturn(dummyAllowUncertainties); 
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SHOW_UNCERTAINTIES,
                                  dummyAllowUncertainties))
          .andReturn(mockModel);

    dummyAllowDynamicCellML = false;
    expect(ControllerUtil.allowFeature(PortalFeature.DYNAMIC_CELLMLS,
                                       mockClientDirectService))
          .andReturn(dummyAllowDynamicCellML);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SHOW_DYNAMIC_CELLML,
           dummyAllowDynamicCellML)).andReturn(mockModel);
    expect(mockModel.addAttribute(eq(ClientDirectIdentifiers.MODEL_ATTRIBUTE_TOKEN),
                                  capture(modelToken)))
          .andReturn(mockModel);
    mockHttpSession.setAttribute(eq(ClientDirectIdentifiers.SESSION_PARAM_TOKEN),
                                 capture(sessionToken));
    expectLastCall();
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                                  dummyExceptionMessage))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    Process.setPropertiesForInput(mockClientDirectService, mockSimulationInputVO,
                                  mockHttpSession, dummyExceptionMessage,
                                  mockModel);

    verifyAll();
    mocksControl.verify();

    resetAll();
    mocksControl.reset();

    /*
     * 5. Test spread processing.
     */
    dummyExceptionMessage = null;
    dummyModels.clear();
    expect(mockClientDirectService.retrieveModels()).andReturn(dummyModels);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_MODELS,
                                  dummyModels))
          .andReturn(mockModel);
    mockSimulationInputVO = mocksControl.createMock(SimulationInputVO.class);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_INPUT_VALUES,
                                  mockSimulationInputVO))
          .andReturn(mockModel);

    dummyAllowUncertainties = true;
    expect(ControllerUtil.allowFeature(PortalFeature.UNCERTAINTIES,
                                       mockClientDirectService))
          .andReturn(dummyAllowUncertainties); 
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SHOW_UNCERTAINTIES,
                                  dummyAllowUncertainties))
          .andReturn(mockModel);
    final IonCurrent dummyIonCurrent1 = IonCurrent.IKr;
    final BigDecimal dummySpread1 = BigDecimal.TEN;
    final IonCurrent dummyIonCurrent2 = IonCurrent.IK1;
    final BigDecimal dummySpread2 = null;
    final IonCurrent dummyIonCurrent3 = IonCurrent.INa;
    final BigDecimal dummySpread3 = BigDecimal.ONE;
    dummySpreads.put(dummyIonCurrent1, dummySpread1);
    dummySpreads.put(dummyIonCurrent2, dummySpread2);
    dummySpreads.put(dummyIonCurrent3, dummySpread3);
    expect(mockClientDirectService.retrieveSpreads()).andReturn(dummySpreads);
    final Capture<Map<String, String>> captureSpreads = newCapture();
    expect(mockModel.addAttribute(eq(ClientDirectIdentifiers.MODEL_ATTRIBUTE_SPREADS),
                                  capture(captureSpreads)))
          .andReturn(mockModel);
    // Adding some credible interval percentiles!
    final BigDecimal dummyExpPctile1 = BigDecimal.ZERO;
    final BigDecimal dummyExpPctile2 = new BigDecimal("1E-5");
    dummyPctiles.add(dummyExpPctile1);
    dummyPctiles.add(dummyExpPctile2);
    expect(mockClientDirectService.retrieveCredibleIntervalPctiles())
          .andReturn(dummyPctiles);
    final Capture<String> captureConcatenatedPctiles = newCapture();
    expect(mockModel.addAttribute(eq(ClientDirectIdentifiers.MODEL_ATTRIBUTE_PCTILES),
                                  capture(captureConcatenatedPctiles)))
          .andReturn(mockModel);

    dummyAllowDynamicCellML = true;
    expect(ControllerUtil.allowFeature(PortalFeature.DYNAMIC_CELLMLS,
                                       mockClientDirectService))
          .andReturn(dummyAllowDynamicCellML);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SHOW_DYNAMIC_CELLML,
                                  dummyAllowDynamicCellML))
          .andReturn(mockModel);
    expect(mockModel.addAttribute(eq(ClientDirectIdentifiers.MODEL_ATTRIBUTE_TOKEN),
                                  capture(modelToken)))
          .andReturn(mockModel);
    mockHttpSession.setAttribute(eq(ClientDirectIdentifiers.SESSION_PARAM_TOKEN),
                                 capture(sessionToken));
    expectLastCall();

    replayAll();
    mocksControl.replay();

    Process.setPropertiesForInput(mockClientDirectService, mockSimulationInputVO,
                                  mockHttpSession, dummyExceptionMessage,
                                  mockModel);

    verifyAll();
    mocksControl.verify();

    final Map<String, String> capturedUISpreads = captureSpreads.getValue();
    assertEquals(3, capturedUISpreads.size());
    assertTrue(capturedUISpreads.containsKey(dummyIonCurrent1.toString()));
    assertTrue(capturedUISpreads.containsKey(dummyIonCurrent2.toString()));
    assertTrue(capturedUISpreads.containsKey(dummyIonCurrent3.toString()));
    assertEquals(capturedUISpreads.get(dummyIonCurrent1.toString()),
                 dummySpread1.toPlainString());
    assertNull(capturedUISpreads.get(dummyIonCurrent2.toString()));
    assertEquals(capturedUISpreads.get(dummyIonCurrent3.toString()),
                 dummySpread3.toPlainString());

    final String capturedConcatenatedPctiles = captureConcatenatedPctiles.getValue();
    final Set<String> capturedPctilesCollection = new HashSet<String>();
    capturedPctilesCollection.addAll(Arrays.asList(StringUtils.split(capturedConcatenatedPctiles, " ")));
    assertTrue(capturedPctilesCollection.contains(dummyExpPctile1.toPlainString()));
    assertTrue(capturedPctilesCollection.contains(dummyExpPctile2.toPlainString()));
  }

  @Test
  public void testSimulationNew() throws Exception {
    /*
     * Null simulation id, i.e. new simulation.
     */
    dummySimulationId = null;

    mockStatic(Process.class, Process.class.getMethod("setPropertiesForInput", 
                                                      ClientDirectService.class,
                                                      SimulationInputVO.class,
                                                      HttpSession.class,
                                                      String.class,
                                                      Model.class));

    Process.setPropertiesForInput(mockClientDirectService, null,
                                  mockHttpSession, null, mockModel);

    replayAll();
    mocksControl.replay();

    assertEquals(ClientDirectIdentifiers.PAGE_INPUT,
                 processController.simulationNew(dummySimulationId, mockModel,
                                                 mockHttpSession));

    verifyAll();
    mocksControl.verify();

    resetAll();
    mocksControl.reset();

    /*
     * Invalid simulation id 
     */
    dummySimulationId = "dummySimulationId";
    mockStatic(ControllerUtil.class);
    expect(ControllerUtil.validSimulationId(dummySimulationId))
          .andReturn(false);

    Process.setPropertiesForInput(mockClientDirectService, null,
                                  mockHttpSession, null, mockModel);

    replayAll();
    mocksControl.replay();

    assertEquals(ClientDirectIdentifiers.PAGE_INPUT,
                 processController.simulationNew(dummySimulationId, mockModel, mockHttpSession));

    verifyAll();
    mocksControl.verify();

    resetAll();
    mocksControl.reset();

    /*
     * Valid simulation id 
     */
    dummySimulationId = "2";
    mockStatic(ControllerUtil.class);
    expect(ControllerUtil.validSimulationId(dummySimulationId))
          .andReturn(true);
    final Long dummySimulationIdLong = Long.valueOf(dummySimulationId);
    final Simulation mockSimulation = mocksControl.createMock(Simulation.class);
    expect(mockClientDirectService.retrieveSimulation(dummySimulationIdLong))
          .andReturn(mockSimulation);
    final SimulationInput mockSimulationInput = mocksControl.createMock(SimulationInput.class);
    expect(mockSimulation.getSimulationInput()).andStubReturn(mockSimulationInput);
    final SimulationInputVO mockSimulationInputVO = mocksControl.createMock(SimulationInputVO.class);
    expectNew(SimulationInputVO.class, mockSimulationInput)
             .andStubReturn(mockSimulationInputVO);

    Process.setPropertiesForInput(mockClientDirectService, mockSimulationInputVO,
                                  mockHttpSession, null, mockModel);

    replayAll();
    mocksControl.replay();

    assertEquals(ClientDirectIdentifiers.PAGE_INPUT,
                 processController.simulationNew(dummySimulationId, mockModel, mockHttpSession));

    verifyAll();
    mocksControl.verify();

    resetAll();
    mocksControl.reset();

    /*
     * Valid simulation id but simulation not found.
     */
    mockStatic(ControllerUtil.class);
    expect(ControllerUtil.validSimulationId(dummySimulationId))
          .andReturn(true);
    final String dummySimulationNotFoundException = "dummySimulationNotFoundException";
    expect(mockClientDirectService.retrieveSimulation(dummySimulationIdLong))
          .andThrow(new SimulationNotFoundException(dummySimulationNotFoundException));

    Process.setPropertiesForInput(mockClientDirectService, null, mockHttpSession,
                                  null, mockModel);

    replayAll();
    mocksControl.replay();

    assertEquals(ClientDirectIdentifiers.PAGE_INPUT,
                 processController.simulationNew(dummySimulationId, mockModel, mockHttpSession));

    verifyAll();
    mocksControl.verify();
  }
}