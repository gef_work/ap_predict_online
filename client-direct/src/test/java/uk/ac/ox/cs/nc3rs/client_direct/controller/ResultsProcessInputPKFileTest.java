/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.io.IOException;
import java.io.InputStream;

import org.easymock.Capture;
import org.junit.Test;
import org.springframework.web.multipart.MultipartFile;

import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.compbio.client_shared.value.file.FILE_DATA_TYPE;
import uk.ac.ox.cs.compbio.client_shared.value.file.FileStoreActionOutcomeVO;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.value.InputOption;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;

/**
 * Unit test the results controller's input processing of the PK file.
 * 
 * @author geoff
 */
public class ResultsProcessInputPKFileTest extends AbstractResultsProcessInputTest {

  private void localPKFailingRepetitive(final SimulationInputVO mockSimulationInputVO,
                                        final String dummyOutcomeInformation) {
    expect(mockHttpSession.getAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN))
          .andReturn(dummyModelAssignedToken).times(2);
    mockHttpSession.removeAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN);
    expect(mockSimulationInputVO.hasAssignmentProblems()).andReturn(false);
    Process.setPropertiesForInput(mockClientDirectService, mockSimulationInputVO,
                                  mockHttpSession, dummyOutcomeInformation,
                                  mockModel);
  }

  @Test
  public void testSimulationInputAssignment() throws Exception {
    /*
     * No PK file submitted.
     */
    dummyPkOrConcs = InputOption.PK.toString();
    defaultPortalFeatureProcessing();
    mockMultipartPKFile = mocksControl.createMock(MultipartFile.class);

    // >> processMultipart()
    boolean dummyPKFileIsEmpty = true;
    expect(mockMultipartPKFile.isEmpty()).andReturn(dummyPKFileIsEmpty);

    final FileStoreActionOutcomeVO mockOutcomeVO = mocksControl.createMock(FileStoreActionOutcomeVO.class);
    expectNew(FileStoreActionOutcomeVO.class, (PortalFile) null, false, 
                                              "Error : PK processing specified but no PK file uploaded!")
             .andReturn(mockOutcomeVO);
    // << processMultipart()

    expect(mockOutcomeVO.isSuccess()).andReturn(false);
    final String dummyOutcomeInformation = "dummyInformation";
    expect(mockOutcomeVO.getInformation()).andReturn(dummyOutcomeInformation);

    SimulationInputVO mockSimulationInputVO = assignSimulationInputProperties(mockPKFile,
                                                                              null,
                                                                              false);

    mockStatic(Process.class);
    localPKFailingRepetitive(mockSimulationInputVO, dummyOutcomeInformation);

    replayAll();
    mocksControl.replay();

    String returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_INPUT, returnPage);

    resetAll();
    mocksControl.reset();

    /*
     * PK file submitted, but input stream retrieval throws exception.
     */
    dummyPKFileIsEmpty = false;
    defaultPortalFeatureProcessing();

    // >> processMultipart()
    expect(mockMultipartPKFile.isEmpty()).andReturn(dummyPKFileIsEmpty);
    final String dummyIOException = "dummyIOException";
    expect(mockMultipartPKFile.getInputStream())
          .andThrow(new IOException(dummyIOException));
    expectNew(FileStoreActionOutcomeVO.class, (PortalFile) null, false, 
                                              "Application Error : Error handling the input stream from PK file!")
             .andReturn(mockOutcomeVO);
    // << processMultipart()

    expect(mockOutcomeVO.isSuccess()).andReturn(false);
    expect(mockOutcomeVO.getInformation()).andReturn(dummyOutcomeInformation);

    mockSimulationInputVO = assignSimulationInputProperties(mockPKFile, null,
                                                            false);

    localPKFailingRepetitive(mockSimulationInputVO, dummyOutcomeInformation);

    replayAll();
    mocksControl.replay();

    returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_INPUT, returnPage);

    resetAll();
    mocksControl.reset();

    /*
     * PK file submitted, but input stream is null.
     */
    defaultPortalFeatureProcessing();

    // >> processMultipart()
    expect(mockMultipartPKFile.isEmpty()).andReturn(dummyPKFileIsEmpty);
    expect(mockMultipartPKFile.getInputStream()).andReturn(null);
    expectNew(FileStoreActionOutcomeVO.class, (PortalFile) null, false, 
                                              "Application Error : No PK file input stream available!")
             .andReturn(mockOutcomeVO);
    // << processMultipart()

    expect(mockOutcomeVO.isSuccess()).andReturn(false);
    expect(mockOutcomeVO.getInformation()).andReturn(dummyOutcomeInformation);

    mockSimulationInputVO = assignSimulationInputProperties(mockPKFile, null,
                                                            false);

    localPKFailingRepetitive(mockSimulationInputVO, dummyOutcomeInformation);

    replayAll();
    mocksControl.replay();

    returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_INPUT, returnPage);

    resetAll();
    mocksControl.reset();

    /*
     * PK file submitted, closing input stream throws exception.
     */
    defaultPortalFeatureProcessing();

    // >> processMultipart()
    expect(mockMultipartPKFile.isEmpty()).andReturn(dummyPKFileIsEmpty);
    final InputStream mockInputStream = mocksControl.createMock(InputStream.class);
    expect(mockMultipartPKFile.getInputStream()).andReturn(mockInputStream);
    final String dummyOriginalFilename = "dummyName";
    expect(mockMultipartPKFile.getOriginalFilename())
          .andReturn(dummyOriginalFilename);
    final String dummyUploader = "dummyUploader";
    expect(mockPrincipal.getName()).andReturn(dummyUploader);
    Capture<FILE_DATA_TYPE> captureFileDataType = newCapture();
    Capture<InputStream> captureInputStream = newCapture();
    Capture<String> captureOriginalFilename = newCapture();
    Capture<String> captureUploader = newCapture();
    final FileStoreActionOutcomeVO mockOutcome = mocksControl.createMock(FileStoreActionOutcomeVO.class);
    expect(mockClientDirectService.storeFile(capture(captureFileDataType),
                                             capture(captureInputStream),
                                             capture(captureOriginalFilename),
                                             capture(captureUploader)))
          .andReturn(mockOutcome);
    mockInputStream.close();
    expectLastCall().andThrow(new IOException(dummyIOException));
    // << processMultipart()

    final boolean dummyOutcomeSuccess = true;
    expect(mockOutcome.isSuccess()).andReturn(dummyOutcomeSuccess);
    mockPKFile = mocksControl.createMock(PortalFile.class);
    expect(mockOutcome.getPortalFile()).andReturn(mockPKFile);
    mockSimulationInputVO = assignSimulationInputProperties(mockPKFile, null,
                                                            false);

    latterTesting(mockSimulationInputVO);

    replayAll();
    mocksControl.replay();

    returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_RESULTS, returnPage);

    resetAll();
    mocksControl.reset();

    /*
     * PK file submitted, and input stream is available.
     */
    defaultPortalFeatureProcessing();

    // >> processMultipart()
    expect(mockMultipartPKFile.isEmpty()).andReturn(dummyPKFileIsEmpty);
    expect(mockMultipartPKFile.getInputStream()).andReturn(mockInputStream);
    expect(mockMultipartPKFile.getOriginalFilename())
          .andReturn(dummyOriginalFilename);
    expect(mockPrincipal.getName()).andReturn(dummyUploader);
    captureFileDataType = newCapture();
    captureInputStream = newCapture();
    captureOriginalFilename = newCapture();
    captureUploader = newCapture();
    expect(mockClientDirectService.storeFile(capture(captureFileDataType),
                                             capture(captureInputStream),
                                             capture(captureOriginalFilename),
                                             capture(captureUploader)))
          .andReturn(mockOutcome);
    mockInputStream.close();
    // << processMultipart()

    expect(mockOutcome.isSuccess()).andReturn(dummyOutcomeSuccess);
    mockPKFile = mocksControl.createMock(PortalFile.class);
    expect(mockOutcome.getPortalFile()).andReturn(mockPKFile);
    mockSimulationInputVO = assignSimulationInputProperties(mockPKFile, null,
                                                            false);

    latterTesting(mockSimulationInputVO);

    replayAll();
    mocksControl.replay();

    returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    final FILE_DATA_TYPE capturedFileDataType = captureFileDataType.getValue();
    final String capturedUploader = captureUploader.getValue();

    assertTrue(FILE_DATA_TYPE.PK.compareTo(capturedFileDataType) == 0);
    assertEquals(dummyUploader, capturedUploader);

    final String capturedOriginalFilename = captureOriginalFilename.getValue();
    assertEquals(dummyOriginalFilename, capturedOriginalFilename);

    assertEquals(ClientDirectIdentifiers.PAGE_RESULTS, returnPage);

    resetAll();
    mocksControl.reset();
  }
}