/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.manager;

import static org.easymock.EasyMock.anyBoolean;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.isA;

import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.acls.model.AccessControlEntry;
import org.springframework.security.acls.model.MutableAcl;
import org.springframework.security.acls.model.MutableAclService;
import org.springframework.security.acls.model.NotFoundException;
import org.springframework.security.acls.model.ObjectIdentity;
import org.springframework.security.acls.model.Permission;
import org.springframework.security.acls.model.Sid;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation;

/**
 * Unit test the permission manager implementation.
 *
 * @author geoff
 */
public class PermissionManagerImplTest {

  private IMocksControl mocksControl;
  private MutableAcl mockMutableAcl;
  private MutableAclService mockMutableAclService;
  private Permission mockPermission;
  private PermissionManager permissionManager;
  private Sid mockSid;

  @Before
  public void setUp() {
    permissionManager = new PermissionManagerImpl();

    // Using a strict control because it's a bit tricky inside permission manager.
    mocksControl = createStrictControl();
    mockMutableAclService = mocksControl.createMock(MutableAclService.class);

    ReflectionTestUtils.setField(permissionManager, "mutableAclService", mockMutableAclService);

    mockMutableAcl = mocksControl.createMock(MutableAcl.class);
    mockPermission = mocksControl.createMock(Permission.class);
    mockSid = mocksControl.createMock(Sid.class);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testAddPermission() {
    final Class<?> dummySecureObjectClass = Simulation.class;
    final long dummySecureObjectId = 1l;

    expect(mockMutableAclService.readAclById(isA(ObjectIdentity.class)))
          .andThrow(new NotFoundException(""));
    expect(mockMutableAclService.createAcl(isA(ObjectIdentity.class)))
          .andReturn(mockMutableAcl);
    expect(mockMutableAcl.isGranted(isA(List.class), isA(List.class),
                                    anyBoolean()))
          .andThrow(new NotFoundException(""));
    expect(mockMutableAcl.getEntries())
          .andReturn(new ArrayList<AccessControlEntry>());
    mockMutableAcl.insertAce(0, mockPermission, mockSid, true);
    expectLastCall().once();
    expect(mockMutableAclService.updateAcl(mockMutableAcl)).andReturn(mockMutableAcl);
    mocksControl.replay();

    permissionManager.addPermission(dummySecureObjectClass, dummySecureObjectId,
                                    mockSid, mockPermission);

    mocksControl.verify();
  }
}