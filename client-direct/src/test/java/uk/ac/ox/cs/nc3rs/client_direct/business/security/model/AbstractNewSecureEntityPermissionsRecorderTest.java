/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.business.security.model;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertSame;
import static org.powermock.api.easymock.PowerMock.mockStatic;

import org.aspectj.lang.ProceedingJoinPoint;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.acls.domain.PrincipalSid;
import org.springframework.security.acls.model.Permission;
import org.springframework.security.acls.model.Sid;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.compbio.client_shared.api.entity.SecuredEntity;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.aop.aspect.MethodInterceptingDomainObjectUtil;
import uk.ac.ox.cs.nc3rs.client_direct.business.security.util.GrantedAuthorityAndSidUtil;
import uk.ac.ox.cs.nc3rs.client_direct.business.security.util.PermissionUtil;
import uk.ac.ox.cs.nc3rs.client_direct.manager.PermissionManager;

/**
 * Unit test the abstract new secure entity permissions recorder.
 * 
 * @author geo
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { AbstractNewSecureEntityPermissionsRecorderTest.class,
                   MethodInterceptingDomainObjectUtil.class,
                   GrantedAuthorityAndSidUtil.class, PermissionUtil.class } )
public class AbstractNewSecureEntityPermissionsRecorderTest {

  class TestNewSecureEntityPermissionsRecorder extends AbstractNewSecureEntityPermissionsRecorder {
    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.client_direct.business.security.model.AbstractNewSecureEntityPermissionsRecorder#retrieveCreator(java.lang.Object)
     */
    @Override
    protected String retrieveCreator(Object storedObject) {
      return dummyCreator;
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.client_direct.business.security.model.AbstractNewSecureEntityPermissionsRecorder#retrievePersistenceId(java.lang.Object)
     */
    @Override
    protected Long retrievePersistenceId(Object interceptedObject) {
      return dummyPersistenceId;
    }
  }

  private IMocksControl mocksControl;
  private Long dummyPersistenceId = 23L;
  private PermissionManager mockPermissionManager;
  private ProceedingJoinPoint mockProceedingJoinPoint;
  private String dummyCreator = "dummyCreator";
  private TestNewSecureEntityPermissionsRecorder newSecureEntityPermissionsRecorder;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockPermissionManager = mocksControl.createMock(PermissionManager.class);
    mockProceedingJoinPoint = mocksControl.createMock(ProceedingJoinPoint.class);
    newSecureEntityPermissionsRecorder = new TestNewSecureEntityPermissionsRecorder();

    ReflectionTestUtils.setField(newSecureEntityPermissionsRecorder,
                                 ClientDirectIdentifiers.COMPONENT_PERMISSION_MANAGER,
                                 mockPermissionManager);
  }

  /*
   * In this situation we can only part-test the code as the persistence id returned by the 
   * concrete class TestNewSecureEntityPermissionsRecorder is constant so can't emulate scenario
   * where object persisted.
   */
  @Test
  public void testCreationAdviceNoNewPersistenceId() throws Throwable {
    mockStatic(MethodInterceptingDomainObjectUtil.class);
    final SecuredEntity mockSecuredEntity = mocksControl.createMock(SecuredEntity.class);
    expect(MethodInterceptingDomainObjectUtil.getDomainObjectInstance(isA(ProceedingJoinPoint.class),
                                                                      isA(Class.class)))
          .andReturn(mockSecuredEntity);
    final SecuredEntity mockStored = mocksControl.createMock(SecuredEntity.class);
    expect(mockProceedingJoinPoint.proceed()).andReturn(mockStored);

    mocksControl.replay();

    final Object returnedObject = newSecureEntityPermissionsRecorder.creationAdvice(mockProceedingJoinPoint);

    mocksControl.verify();
    assertSame(mockStored, returnedObject);
  }

  @Test
  public void testAdministrationPermissionToCreatorAssignment() {
    final Class<?> secureObjectClass = Integer.class;
    final Long secureObjectId = new Long(1);
    final String creator = "creator";
    final Sid principalSid = new PrincipalSid(creator);
    final Permission permission = PermissionUtil.retrieveAdministrationPermission();
    mockPermissionManager.addPermission(secureObjectClass, secureObjectId, principalSid, permission);
    expectLastCall().once();

    replay(mockPermissionManager);

    newSecureEntityPermissionsRecorder.assignAdministrationPermissionToCreator(secureObjectClass,
                                                                               secureObjectId,
                                                                               creator);

    verify(mockPermissionManager);
  }
}