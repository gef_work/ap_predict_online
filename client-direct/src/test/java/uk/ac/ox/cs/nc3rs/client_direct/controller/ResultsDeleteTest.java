/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller;

import static org.easymock.EasyMock.createStrictControl;
import static org.junit.Assert.assertEquals;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.security.Principal;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.Model;

import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService;

/**
 * Unit test the results controller's results deletion.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { Main.class } )
public class ResultsDeleteTest {

  private ClientDirectService mockClientDirectService;
  private IMocksControl mocksControl;
  private MessageSource mockMessageSource;
  private Model mockModel;
  private Principal mockPrincipal;
  private Results resultsController;

  @Before
  public void setUp() {
    resultsController = new Results();

    mocksControl = createStrictControl();
    mockClientDirectService = mocksControl.createMock(ClientDirectService.class);
    mockMessageSource = mocksControl.createMock(MessageSource.class);
    ReflectionTestUtils.setField(resultsController, "messageSource", mockMessageSource);
    ReflectionTestUtils.setField(resultsController, ClientDirectIdentifiers.COMPONENT_CLIENT_DIRECT_SERVICE,
                                 mockClientDirectService);

    mockModel = mocksControl.createMock(Model.class);
    mockPrincipal = mocksControl.createMock(Principal.class);
  }

  @Test
  public void testResultsDelete() {
    /*
     * Invalid simulation id.
     */
    String dummySimulationId = "apples";
    mockStatic(Main.class);
    Main.setModelPropertiesForMain(mockClientDirectService, mockPrincipal,
                                   mockModel, null);

    replayAll();
    mocksControl.replay();

    String page = resultsController.resultsDelete(dummySimulationId,
                                                  mockPrincipal, mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_MAIN, page);

    resetAll();
    mocksControl.reset();

    /*
     * Valid simulation id.
     */
    dummySimulationId = "1";

    mockClientDirectService.deleteSimulation(Long.valueOf(dummySimulationId));
    Main.setModelPropertiesForMain(mockClientDirectService, mockPrincipal,
                                   mockModel, null);

    replayAll();
    mocksControl.replay();

    page = resultsController.resultsDelete(dummySimulationId, mockPrincipal,
                                           mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_MAIN, page);
  }
}