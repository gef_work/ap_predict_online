/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import org.easymock.EasyMock;
import org.junit.Test;

/**
 * Unit test the Job PK Result entity.
 *
 * @author geoff
 */
public class JobPKResultTest {

  @Test
  public void testInitialisingConstructor() {
    BigDecimal dummyTimepoint = null;
    String dummyAPD90s = null;
    boolean dummyForTranslation = false;

    try {
      new JobPKResult(dummyTimepoint, dummyAPD90s, dummyForTranslation);
      fail("Should not permit null args in initialising constructor!");
    } catch (IllegalArgumentException e) {
      assertTrue(e.getMessage().contains("Cannot create a job PK"));
    }

    dummyTimepoint = BigDecimal.ONE;
    try {
      new JobPKResult(dummyTimepoint, dummyAPD90s, dummyForTranslation);
      fail("Should not permit null args in initialising constructor!");
    } catch (IllegalArgumentException e) {
      assertTrue(e.getMessage().contains("Cannot create a job PK"));
    }

    dummyAPD90s = "dummyAPD90s";
    JobPKResult jobPKResult = new JobPKResult(dummyTimepoint, dummyAPD90s,
                                                    dummyForTranslation);
    assertTrue(dummyTimepoint.compareTo(jobPKResult.getTimepoint()) == 0);
    assertEquals(dummyAPD90s, jobPKResult.getApd90s());
    assertFalse(jobPKResult.isForTranslation());

    dummyForTranslation = true;
    jobPKResult = new JobPKResult(dummyTimepoint, dummyAPD90s,
                                  dummyForTranslation);
    assertTrue(dummyTimepoint.compareTo(jobPKResult.getTimepoint()) == 0);
    assertEquals(dummyAPD90s, jobPKResult.getApd90s());
    assertTrue(jobPKResult.isForTranslation());
    assertTrue(jobPKResult.toString().contains(dummyAPD90s));
  }

  @Test
  public void testSetJob() {
    final BigDecimal dummyTimepoint = BigDecimal.TEN;
    final String dummyAPD90s = "dummyAPD90s";
    final boolean dummyForTranslation = false;

    final JobPKResult jobPKResult = new JobPKResult(dummyTimepoint, dummyAPD90s,
                                                    dummyForTranslation);

    final Job mockJob = EasyMock.createMock(Job.class);
    mockJob.addJobPKResult(jobPKResult);

    EasyMock.replay(mockJob);

    jobPKResult.setJob(mockJob);

    EasyMock.verify(mockJob);
  }
}