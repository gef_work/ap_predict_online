/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.value;

import static org.easymock.EasyMock.createStrictControl;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation;

/**
 * Unit test the simulation current results value object.
 *
 * @author geoff
 */
public class SimulationCurrentResultsVOTest {

  private IMocksControl mocksControl;
  private ResultsVO mockResultsVO;
  private Simulation mockSimulation;
  private SimulationCurrentResultsVO simulationCurrentResultsVO;
  private StatusVO mockStatusVO;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockResultsVO = mocksControl.createMock(ResultsVO.class);
    mockSimulation = mocksControl.createMock(Simulation.class);
    mockStatusVO = mocksControl.createMock(StatusVO.class);
  }

  @Test
  public void testConstructorSuccessOnNullParameters() {
    simulationCurrentResultsVO = new SimulationCurrentResultsVO(null, null, null);

    assertNotNull(simulationCurrentResultsVO.getProgress());
    assertNull(simulationCurrentResultsVO.getResults());
    assertNull(simulationCurrentResultsVO.getSimulation());
    assertNotNull(simulationCurrentResultsVO.toString());
    assertFalse(simulationCurrentResultsVO.hasResults());
  }

  @Test
  public void testConstructorSuccessOnNonNullParameters() {
    final List<StatusVO> fakeProgress = new ArrayList<StatusVO>();
    fakeProgress.add(mockStatusVO);

    mocksControl.replay();

    simulationCurrentResultsVO = new SimulationCurrentResultsVO(mockSimulation,
                                                                fakeProgress,
                                                                mockResultsVO);

    mocksControl.verify();

    assertSame(1, simulationCurrentResultsVO.getProgress().size());
    assertNotNull(simulationCurrentResultsVO.getResults());
    assertNotNull(simulationCurrentResultsVO.getSimulation());
    assertNotNull(simulationCurrentResultsVO.toString());
    assertTrue(simulationCurrentResultsVO.hasResults());

    mocksControl.reset();

    mocksControl.replay();

    try {
      simulationCurrentResultsVO.getProgress().add(mockStatusVO);
      fail("Should not allow modification of the progress data");
    } catch (UnsupportedOperationException use) {}
  }
}