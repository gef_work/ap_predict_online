/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.entity;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import uk.ac.ox.cs.nc3rs.client_direct.value.InputOption;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;

/**
 * Unit test a Simulation entity.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { Simulation.class, SimulationInputVO.class } )
public class SimulationTest {

  private IMocksControl mocksControl;
  private Job mockJob;
  private long dummyAppManagerId;
  private String dummyCreator = "dummyCreator";
  private Simulation simulation;
  private SimulationInput mockSimulationInput;
  private SimulationInputVO mockSimulationInputVO;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();

    mockJob = mocksControl.createMock(Job.class);
    mockSimulationInput = mocksControl.createMock(SimulationInput.class);
    mockSimulationInputVO = mocksControl.createMock(SimulationInputVO.class);

    dummyAppManagerId = 1l;
  }

  @Test
  public void testEquality() {
    simulation = new Simulation(mockJob, dummyCreator, mockSimulationInput);
    final Simulation anotherSimulation = new Simulation(mockJob, dummyCreator,
                                                        mockSimulationInput);
    assertTrue(simulation.equals(anotherSimulation));
    simulation.onCreate();
    simulation.assignCompleted();
    assertFalse(simulation.equals(anotherSimulation));
    assertNotNull(simulation.hashCode());
  }

  @Test
  public void testInitialisedSimulationInitialState() {
    expect(mockJob.getAppManagerId()).andReturn(dummyAppManagerId);

    mocksControl.replay();

    simulation = new Simulation(mockJob, dummyCreator, mockSimulationInput);
    final long appManagerId = simulation.getAppManagerId();

    mocksControl.verify();

    assertEquals(dummyAppManagerId, appManagerId);
    assertEquals(dummyCreator, simulation.getCreator());
    assertNull(simulation.getId());
    assertSame(mockJob, simulation.getJob());
    assertNull(simulation.getPersisted()); // This is javax internal callback assigned!
    assertSame(mockSimulationInput, simulation.getSimulationInput());
    assertFalse(simulation.isCompleted());
    try {
      simulation.isTakingTooLongToComplete();
      fail("Should not permit use of null 'persisted' property!");
    } catch (NullPointerException e) {}
    assertNotNull(simulation.toString());
  }

  @Test
  public void testInitialisingConstructorWithInvalidParameterValues() {
    try {
      new Simulation(null, null, null);
      fail("Should not accept null Job arg!");
    } catch (IllegalArgumentException e) {}
    try {
      new Simulation(mockJob, null, null);
      fail("Should not accept null Creator arg!");
    } catch (IllegalArgumentException e) {}
    try {
      new Simulation(mockJob, dummyCreator, null);
      fail("Should not accept null SimulationInput arg!");
    } catch (IllegalArgumentException e) {}
  }

  @Test
  public void testInputType() throws Exception {
    Simulation simulation = null;
    assertFalse(Simulation.hasConcsPointInput(simulation));
    assertFalse(Simulation.hasPKInput(simulation));
    assertFalse(Simulation.hasConcRangeInput(simulation));

    simulation = new Simulation();
    assertFalse(Simulation.hasConcsPointInput(simulation));
    assertFalse(Simulation.hasPKInput(simulation));
    assertFalse(Simulation.hasConcRangeInput(simulation));

    final Simulation mockSimulation = mocksControl.createMock(Simulation.class);
    expect(mockSimulation.retrieveInputOption())
          .andReturn(InputOption.CONC_POINTS).anyTimes();

    mocksControl.replay();

    assertTrue(Simulation.hasConcsPointInput(mockSimulation));
    assertFalse(Simulation.hasConcRangeInput(mockSimulation));
    assertFalse(Simulation.hasPKInput(mockSimulation));

    mocksControl.verify();

    mocksControl.reset();

    expect(mockSimulation.retrieveInputOption())
          .andReturn(InputOption.CONC_RANGE).anyTimes();

    mocksControl.replay();

    assertTrue(Simulation.hasConcRangeInput(mockSimulation));
    assertFalse(Simulation.hasConcsPointInput(mockSimulation));
    assertFalse(Simulation.hasPKInput(mockSimulation));

    mocksControl.verify();

    mocksControl.reset();

    expect(mockSimulation.retrieveInputOption())
          .andReturn(InputOption.PK).anyTimes();

    mocksControl.replay();

    assertFalse(Simulation.hasConcRangeInput(mockSimulation));
    assertFalse(Simulation.hasConcsPointInput(mockSimulation));
    assertTrue(Simulation.hasPKInput(mockSimulation));

    mocksControl.verify();

    mocksControl.reset();

    simulation = new Simulation(mockJob, dummyCreator, mockSimulationInput);

    replayAll();
    mocksControl.replay();

    final InputOption returned = simulation.retrieveInputOption();

    verifyAll();
    mocksControl.verify();

    assertNull(returned);
  }

  @Test
  public void testStateChanges() {
    simulation = new Simulation(mockJob, dummyCreator, mockSimulationInput);
    assertNull(simulation.getPersisted());
    simulation.onCreate();
    assertNotNull(simulation.getPersisted());
    assertFalse(simulation.isTakingTooLongToComplete());
    assertFalse(simulation.isCompleted());
    simulation.assignCompleted();
    assertTrue(simulation.isCompleted());
    assertFalse(simulation.isTakingTooLongToComplete());
  }

  @Test
  public void testWithin() throws Exception {
    /*
     * Test simulation input absence exception handling on transient simulation
     * input. 
     */
    simulation = new Simulation(mockJob, dummyCreator, mockSimulationInput);

    expect(mockSimulationInput.getId()).andReturn(null);

    mocksControl.replay();

    try {
      simulation.withinPostCompletionGracePeriod();
      fail("Should demand persisted SimulationInput property!");
    } catch (IllegalStateException e) {}

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Test simulation input ok, but simulation not completed.
     */
    simulation.onCreate();           // Must emulate @PrePersist first!
    final Long dummySimulationInputId = 5L; 
    expect(mockSimulationInput.getId()).andReturn(dummySimulationInputId);

    mocksControl.replay();

    boolean within = simulation.withinPostCompletionGracePeriod();

    mocksControl.verify();

    assertFalse(within);

    mocksControl.reset();

    /*
     * Test simulation input ok, simulation completed, input option is ...
     */
    simulation.assignCompleted();

    /* Can't test any further as simulation.id is null so retrieveInputOption
       always returns null! */

    /*
    expect(mockSimulationInput.getId()).andReturn(dummySimulationInputId);
 
    // >> retrieveInputOption()
    expectNew(SimulationInputVO.class, mockSimulationInput)
             .andReturn(mockSimulationInputVO);
    final InputOption dummyInputOption = InputOption.PK;
    expect(mockSimulationInputVO.retrieveInputOption())
          .andReturn(dummyInputOption);
    // << retrieveInputOption()

    replayAll();
    mocksControl.replay();

    within = simulation.withinPostCompletionGracePeriod();

    verifyAll();
    mocksControl.verify();

    */
  }
}