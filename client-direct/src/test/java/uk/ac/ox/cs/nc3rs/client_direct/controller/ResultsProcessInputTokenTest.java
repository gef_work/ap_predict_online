/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller;

import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import javax.servlet.http.HttpSession;

import org.junit.Test;
import org.springframework.context.NoSuchMessageException;

import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;

/**
 * Unit test the results controller's input processing token handling.
 * 
 * @author geoff
 */
public class ResultsProcessInputTokenTest extends AbstractResultsProcessInputTest {

  @Test
  public void testTokenMismatch() throws Exception {
    final String dummySessionToken = "";

    defaultPortalFeatureProcessing();
    assignSimulationInputProperties(null, null, false);

    // Test 1 - Session token has content, model token has empty string... so mismatch
    expect(mockHttpSession.getAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN)).
           andReturn(dummySessionToken).times(2);
    mockHttpSession.removeAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN);

    // anyTimes() used as no order of test execution and message queue system may have been populated.
    expect(mockMessageSource.getMessage(ClientDirectIdentifiers.RESUBMISSION_MSG_BUNDLE_ID,
                                        null, dummyLocale)).
           andThrow(new NoSuchMessageException(null)).anyTimes();

    mockStatic(Main.class);
    Main.setModelPropertiesForMain(mockClientDirectService, mockPrincipal, mockModel,
                                   ClientDirectIdentifiers.RESUBMISSION_MSG_BUNDLE_ID);

    replayAll();
    mocksControl.replay();

    String returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_MAIN, returnPage);
  }

  @Test
  public void testSessionToken() throws Exception {
    /*
     * Null HTTP session object.
     */
    mockHttpSession = null;
    defaultPortalFeatureProcessing();
    assignSimulationInputProperties(null, null, false);

    // anyTimes() used as no order of test execution and message queue system may have been populated.
    expect(mockMessageSource.getMessage(ClientDirectIdentifiers.RESUBMISSION_MSG_BUNDLE_ID,
                                        null, dummyLocale))
          .andThrow(new NoSuchMessageException(null)).anyTimes();
    mockStatic(Main.class);
    Main.setModelPropertiesForMain(mockClientDirectService, mockPrincipal,
                                   mockModel,
                                   ClientDirectIdentifiers.RESUBMISSION_MSG_BUNDLE_ID);

    replayAll();
    mocksControl.replay();

    String returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_MAIN, returnPage);

    resetAll();
    mocksControl.reset();

    /*
     * Session passed to method, but not containing a token.
     */
    defaultPortalFeatureProcessing();
    mockHttpSession = mocksControl.createMock(HttpSession.class);
    assignSimulationInputProperties(null, null, false);

    expect(mockHttpSession.getAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN))
          .andReturn(null);
    expect(mockMessageSource.getMessage(ClientDirectIdentifiers.RESUBMISSION_MSG_BUNDLE_ID,
                                        null, dummyLocale))
          .andThrow(new NoSuchMessageException(null)).anyTimes();
    Main.setModelPropertiesForMain(mockClientDirectService, mockPrincipal,
                                   mockModel,
                                   ClientDirectIdentifiers.RESUBMISSION_MSG_BUNDLE_ID);

    replayAll();
    mocksControl.replay();

    returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_MAIN, returnPage);
  }
}