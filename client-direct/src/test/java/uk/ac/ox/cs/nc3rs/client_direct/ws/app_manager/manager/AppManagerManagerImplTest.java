/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.manager;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunResponse.Response;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApplicationStatusRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApplicationStatusResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.BusinessDataUploadCompletedRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.PKResults;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.Results;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllResultsRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllResultsResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllStatusesRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllStatusesResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.StatusRecord;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Job;
import uk.ac.ox.cs.nc3rs.client_direct.entity.JobPKResult;
import uk.ac.ox.cs.nc3rs.client_direct.entity.JobResult;
import uk.ac.ox.cs.nc3rs.client_direct.exception.AppManagerWSInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.NoConnectionException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.RunInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.value.StatusVO;
import uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.manager.AppManagerManager;
import uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.manager.AppManagerManagerImpl;
import uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.proxy.AppManagerProxy;
import uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.service.AppManagerService;

/**
 * Unit test the App Manager manager implementation.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { AppManagerManagerImpl.class } )
public class AppManagerManagerImplTest {

  private ApplicationStatusResponse mockApplicationStatusResponse;
  private AppManagerManager appManagerManager;
  private AppManagerProxy mockAppManagerProxy;
  private ApPredictRunRequest mockApPredictRunRequest;
  private ApPredictRunResponse mockApPredictRunResponse;
  private IMocksControl mocksControl;
  private long dummyAppManagerId;
  private Response mockResponse;
  private RetrieveAllResultsResponse mockRetrieveAllResultsResponse;
  private RetrieveAllStatusesResponse mockRetrieveAllStatusesResponse;

  @Before
  public void setUp() {
    appManagerManager = new AppManagerManagerImpl();

    mocksControl = createStrictControl();
    mockAppManagerProxy = mocksControl.createMock(AppManagerProxy.class);

    ReflectionTestUtils.setField(appManagerManager, "appManagerProxy", 
                                 mockAppManagerProxy);

    dummyAppManagerId = 32l;
    mockApplicationStatusResponse = mocksControl.createMock(ApplicationStatusResponse.class);
    mockApPredictRunRequest = mocksControl.createMock(ApPredictRunRequest.class);
    mockApPredictRunResponse = mocksControl.createMock(ApPredictRunResponse.class);
    mockResponse = mocksControl.createMock(Response.class);
    mockRetrieveAllResultsResponse = mocksControl.createMock(RetrieveAllResultsResponse.class);
    mockRetrieveAllStatusesResponse = mocksControl.createMock(RetrieveAllStatusesResponse.class);
  }

  @Test
  public void testRetrieveProgress() throws Exception {
    /*
     * No status records from app manager proxy
     */
    final Capture<RetrieveAllStatusesRequest> captureRequest = newCapture();
    expect(mockAppManagerProxy.retrieveAllProgress(capture(captureRequest)))
          .andReturn(mockRetrieveAllStatusesResponse);
    final List<StatusRecord> dummyStatusRecords = new ArrayList<StatusRecord>();
    expect(mockRetrieveAllStatusesResponse.getStatusRecord())
          .andReturn(dummyStatusRecords);

    mocksControl.replay();

    List<StatusVO> returnedProgress = appManagerManager.retrieveProgress(dummyAppManagerId);

    mocksControl.verify();

    assertSame(0, returnedProgress.size());
    RetrieveAllStatusesRequest capturedRequest = captureRequest.getValue();
    assertSame(dummyAppManagerId, capturedRequest.getAppManagerId());

    mocksControl.reset();

    /*
     * Non-progress status record from app manager proxy 
     */
    final StatusRecord mockStatusRecord1 = mocksControl.createMock(StatusRecord.class);
    dummyStatusRecords.add(mockStatusRecord1);
    expect(mockAppManagerProxy.retrieveAllProgress(capture(captureRequest)))
          .andReturn(mockRetrieveAllStatusesResponse);
    expect(mockRetrieveAllStatusesResponse.getStatusRecord())
          .andReturn(dummyStatusRecords);
    final String dummyEntered1 = "dummyEntered1";
    final String dummyStatus1 = "dummyStatus1";
    final String dummyLevel1 = "dummyLevel1";
    expect(mockStatusRecord1.getEntered()).andReturn(dummyEntered1);
    expect(mockStatusRecord1.getStatus()).andReturn(dummyStatus1);
    expect(mockStatusRecord1.getLevel()).andReturn(dummyLevel1);
    final StatusVO mockStatusVO1 = mocksControl.createMock(StatusVO.class);
    expectNew(StatusVO.class, dummyEntered1, dummyStatus1, dummyLevel1)
             .andReturn(mockStatusVO1);
    final boolean dummyIsProgressStatus1 = false;
    expect(mockStatusVO1.isProgressStatus()).andReturn(dummyIsProgressStatus1);

    replayAll();
    mocksControl.replay();

    returnedProgress = appManagerManager.retrieveProgress(dummyAppManagerId);

    verifyAll();
    mocksControl.verify();

    assertSame(0, returnedProgress.size());

    resetAll();
    mocksControl.reset();

    /*
     * Non-progress and progress status record from app manager proxy 
     */
    final StatusRecord mockStatusRecord2 = mocksControl.createMock(StatusRecord.class);
    dummyStatusRecords.add(mockStatusRecord2);
    expect(mockAppManagerProxy.retrieveAllProgress(capture(captureRequest)))
          .andReturn(mockRetrieveAllStatusesResponse);
    expect(mockRetrieveAllStatusesResponse.getStatusRecord())
          .andReturn(dummyStatusRecords);
    expect(mockStatusRecord1.getEntered()).andReturn(dummyEntered1);
    expect(mockStatusRecord1.getStatus()).andReturn(dummyStatus1);
    expect(mockStatusRecord1.getLevel()).andReturn(dummyLevel1);
    expectNew(StatusVO.class, dummyEntered1, dummyStatus1, dummyLevel1)
             .andReturn(mockStatusVO1);
    expect(mockStatusVO1.isProgressStatus()).andReturn(dummyIsProgressStatus1);
    final StatusVO mockStatusVO2 = mocksControl.createMock(StatusVO.class);
    final String dummyEntered2 = "dummyEntered2";
    final String dummyStatus2 = "dummyStatus2";
    final String dummyLevel2 = "dummyLevel2";
    expect(mockStatusRecord2.getEntered()).andReturn(dummyEntered2);
    expect(mockStatusRecord2.getStatus()).andReturn(dummyStatus2);
    expect(mockStatusRecord2.getLevel()).andReturn(dummyLevel2);
    expectNew(StatusVO.class, dummyEntered2, dummyStatus2, dummyLevel2)
             .andReturn(mockStatusVO2);
    final boolean dummyIsProgressStatus2 = true;
    expect(mockStatusVO2.isProgressStatus()).andReturn(dummyIsProgressStatus2);

    replayAll();
    mocksControl.replay();

    returnedProgress = appManagerManager.retrieveProgress(dummyAppManagerId);

    verifyAll();
    mocksControl.verify();
    assertSame(1, returnedProgress.size());
    assertSame(mockStatusVO2, returnedProgress.get(0));
    try {
      returnedProgress.add(mockStatusVO2);
      fail("Should not permit modification of the returned progress.");
    } catch (UnsupportedOperationException e) {}
  }

  @Test
  public void testRetrieveResults() throws Exception {
    /*
     * No concentration results
     */
    final Capture<RetrieveAllResultsRequest> captureRequest = newCapture();
    expect(mockAppManagerProxy.retrieveAllResults(capture(captureRequest)))
          .andReturn(mockRetrieveAllResultsResponse);
    expect(mockRetrieveAllResultsResponse.getAppManagerId())
          .andReturn(dummyAppManagerId);
    final String dummyMessages = "dummyMessages";
    final String dummyDeltaAPD90PctileNames = "dummyDeltaAPD90PctileNames";
    List<Results> dummyResults = null;
    List<PKResults> dummyPKResults = null;
    expect(mockRetrieveAllResultsResponse.getMessages())
          .andReturn(dummyMessages);
    expect(mockRetrieveAllResultsResponse.getDeltaAPD90PercentileNames())
          .andReturn(dummyDeltaAPD90PctileNames);
    expect(mockRetrieveAllResultsResponse.getResults())
          .andReturn(dummyResults);
    expect(mockRetrieveAllResultsResponse.getPKResults())
          .andReturn(dummyPKResults);
    final Job mockJob = mocksControl.createMock(Job.class);
    expectNew(Job.class, dummyAppManagerId).andReturn(mockJob);
    mockJob.setMessages(dummyMessages);
    mockJob.setDeltaAPD90PercentileNames(dummyDeltaAPD90PctileNames);

    replayAll();
    mocksControl.replay();

    Job returnedJob = appManagerManager.retrieveResults(dummyAppManagerId);

    verifyAll();
    mocksControl.verify();

    assertSame(mockJob, returnedJob);
    final RetrieveAllResultsRequest capturedRequest = captureRequest.getValue();
    assertSame(dummyAppManagerId, capturedRequest.getAppManagerId());

    resetAll();
    mocksControl.reset();

    /*
     * Concentration and PK results
     */
    expect(mockAppManagerProxy.retrieveAllResults(capture(captureRequest)))
          .andReturn(mockRetrieveAllResultsResponse);
    expect(mockRetrieveAllResultsResponse.getAppManagerId())
          .andReturn(dummyAppManagerId);
    expect(mockRetrieveAllResultsResponse.getMessages())
          .andReturn(dummyMessages);
    expect(mockRetrieveAllResultsResponse.getDeltaAPD90PercentileNames())
          .andReturn(dummyDeltaAPD90PctileNames);
    // Regular results
    final Results mockResults = mocksControl.createMock(Results.class);
    dummyResults = new ArrayList<Results>();
    dummyResults.add(mockResults);
    expect(mockRetrieveAllResultsResponse.getResults())
          .andReturn(dummyResults);
    // PK results
    final PKResults mockPKResults = mocksControl.createMock(PKResults.class);
    dummyPKResults = new ArrayList<PKResults>();
    dummyPKResults.add(mockPKResults);
    expect(mockRetrieveAllResultsResponse.getPKResults())
          .andReturn(dummyPKResults);
    // Process regular
    expectNew(Job.class, dummyAppManagerId).andReturn(mockJob);
    mockJob.setMessages(dummyMessages);
    mockJob.setDeltaAPD90PercentileNames(dummyDeltaAPD90PctileNames);

    final BigDecimal dummyReference = BigDecimal.ONE;
    final String dummyTimes = "dummyTimes";
    final String dummyVoltages = "dummyVoltages";
    final String dummyAPD90 = "dummyAPD90";
    final String dummyDeltaAPD90 = "dummyDeltaAPD90";
    final String dummyQNet = "dummyQNet";
    expect(mockResults.getReference()).andReturn(dummyReference);
    expect(mockResults.getTimes()).andReturn(dummyTimes);
    expect(mockResults.getVoltages()).andReturn(dummyVoltages);
    expect(mockResults.getAPD90()).andReturn(dummyAPD90);
    expect(mockResults.getDeltaAPD90()).andReturn(dummyDeltaAPD90);
    expect(mockResults.getQNet()).andReturn(dummyQNet);

    final JobResult mockJobResult = mocksControl.createMock(JobResult.class);
    expectNew(JobResult.class, dummyReference, dummyTimes, dummyVoltages,
                               dummyAPD90, dummyDeltaAPD90, dummyQNet, false)
             .andReturn(mockJobResult);
    mockJob.addJobResult(mockJobResult);
    // Process PK
    final BigDecimal dummyTimepoint = BigDecimal.ONE;
    expect(mockPKResults.getTimepoint()).andReturn(dummyTimepoint);
    final String dummyAPD901 = "dummyAPD901";
    final String dummyAPD902 = "dummyAPD902";
    String dummyAPD90s = dummyAPD901.concat(",").concat(dummyAPD902);
    expect(mockPKResults.getAPD90S()).andReturn(dummyAPD90s);
    final JobPKResult mockJobPKResult = mocksControl.createMock(JobPKResult.class);
    expectNew(JobPKResult.class, dummyTimepoint, dummyAPD90s, false)
             .andReturn(mockJobPKResult);
    mockJob.addJobPKResult(mockJobPKResult);

    replayAll();
    mocksControl.replay();

    returnedJob = appManagerManager.retrieveResults(dummyAppManagerId);

    verifyAll();
    mocksControl.verify();

    assertSame(mockJob, returnedJob);

    resetAll();
    mocksControl.reset();
  }

  @Test
  public void testRetrieveWorkload() throws AppManagerWSInvocationException,
                                            NoConnectionException {
    final Capture<ApplicationStatusRequest> captureRequest = newCapture();
    expect(mockAppManagerProxy.applicationStatusRequest(capture(captureRequest)))
          .andReturn(mockApplicationStatusResponse);
    final String dummyStatus = "dummyStatus";
    expect(mockApplicationStatusResponse.getStatus()).andReturn(dummyStatus);

    mocksControl.replay();

    final String retrievedWorkload = appManagerManager.retrieveWorkload();

    mocksControl.verify();

    assertSame(dummyStatus, retrievedWorkload);
    final ApplicationStatusRequest capturedRequest = captureRequest.getValue();
    assertSame(AppManagerManager.APP_MANAGER_WORKLOAD_IDENTIFIER,
               capturedRequest.getData());
  }

  @Test
  public void testRunApPredict() throws AppManagerWSInvocationException,
                                        NoConnectionException,
                                        RunInvocationException {
    expect(mockAppManagerProxy.invokeRunRequest(mockApPredictRunRequest))
          .andReturn(mockApPredictRunResponse);
    expect(mockApPredictRunResponse.getResponse()).andReturn(mockResponse);
    final String dummyResponseCode = null;
    expect(mockResponse.getResponseCode()).andReturn(dummyResponseCode);
    final Long dummyAppManagerId = 1L;
    expect(mockResponse.getAppManagerId()).andReturn(dummyAppManagerId);

    mocksControl.replay();

    final long returnedAppManagerId = appManagerManager.runApPredict(mockApPredictRunRequest);

    mocksControl.verify();
    assertSame(returnedAppManagerId, dummyAppManagerId.longValue());
  }

  @Test(expected=RunInvocationException.class)
  public void testRunApPredictThrowsRunInvocationException1() 
              throws AppManagerWSInvocationException, NoConnectionException,
                     RunInvocationException {
    /**
     * Response code, but not in app manager codes (because none assigned).
     */
    expect(mockAppManagerProxy.invokeRunRequest(mockApPredictRunRequest))
          .andReturn(mockApPredictRunResponse);
    expect(mockApPredictRunResponse.getResponse()).andReturn(mockResponse);
    final String dummyResponseCode = "dummyResponseCode";
    expect(mockResponse.getResponseCode()).andReturn(dummyResponseCode);

    mocksControl.replay();

    appManagerManager.runApPredict(mockApPredictRunRequest);
  }

  @Test(expected=RunInvocationException.class)
  public void testRunApPredictThrowsRunInvocationException2() throws AppManagerWSInvocationException,
                                                                     NoConnectionException,
                                                                     RunInvocationException {
    /**
     * Response code, but not in app manager codes (assigned).
     */
    final Map<String, String> dummyAppManagerCodes = new HashMap<String, String>();
    final String dummyResponseCode = "dummyResponseCode";
    dummyAppManagerCodes.put(dummyResponseCode, "fish");
    ((AppManagerManagerImpl) appManagerManager).setAppManagerCodes(dummyAppManagerCodes);

    expect(mockAppManagerProxy.invokeRunRequest(mockApPredictRunRequest))
          .andReturn(mockApPredictRunResponse);
    expect(mockApPredictRunResponse.getResponse()).andReturn(mockResponse);
    expect(mockResponse.getResponseCode()).andReturn(dummyResponseCode);

    mocksControl.replay();

    appManagerManager.runApPredict(mockApPredictRunRequest);
  }

  @Test(expected=RunInvocationException.class)
  public void testRunApPredictThrowsRunInvocationException3() throws AppManagerWSInvocationException,
                                                                     NoConnectionException,
                                                                     RunInvocationException {
    /**
     * Response code, indicating operating at capacity.
     */
    final Map<String, String> dummyAppManagerCodes = new HashMap<String, String>();
    final String dummyResponseCode = "dummyResponseCode";
    dummyAppManagerCodes.put(dummyResponseCode,
                             AppManagerService.APP_MANAGER_CAPACITY_BUNDLE_IDENTIFIER);
    ((AppManagerManagerImpl) appManagerManager).setAppManagerCodes(dummyAppManagerCodes);

    expect(mockAppManagerProxy.invokeRunRequest(mockApPredictRunRequest))
          .andReturn(mockApPredictRunResponse);
    expect(mockApPredictRunResponse.getResponse()).andReturn(mockResponse);
    expect(mockResponse.getResponseCode()).andReturn(dummyResponseCode);

    mocksControl.replay();

    appManagerManager.runApPredict(mockApPredictRunRequest);
  }

  @Test(expected=RunInvocationException.class)
  public void testRunApPredictThrowsRunInvocationException4() throws AppManagerWSInvocationException,
                                                                     NoConnectionException,
                                                                     RunInvocationException {
    /**
     * No response code, no app manager id
     */
    expect(mockAppManagerProxy.invokeRunRequest(mockApPredictRunRequest))
          .andReturn(mockApPredictRunResponse);
    expect(mockApPredictRunResponse.getResponse()).andReturn(mockResponse);
    final String dummyResponseCode = null;
    expect(mockResponse.getResponseCode()).andReturn(dummyResponseCode);
    final Long dummyAppManagerId = null;
    expect(mockResponse.getAppManagerId()).andReturn(dummyAppManagerId);

    mocksControl.replay();

    appManagerManager.runApPredict(mockApPredictRunRequest);
  }

  @Test
  public void testSignalResultsUpload() throws AppManagerWSInvocationException,
                                               NoConnectionException {
    final Capture<BusinessDataUploadCompletedRequest> captureRequest = newCapture();
    mockAppManagerProxy.signalResultsUploaded(capture(captureRequest));

    mocksControl.replay();

    appManagerManager.signalResultsUploaded(dummyAppManagerId);

    mocksControl.verify();
    final BusinessDataUploadCompletedRequest capturedRequest = captureRequest.getValue();
    assertSame(dummyAppManagerId, capturedRequest.getAppManagerId());
  }
}