/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the names of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.value;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;
import java.util.Arrays;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.nc3rs.client_direct.entity.SimulationInput;

/**
 * Unit test the {@link SimulationInput} object constructor.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { SimulationInputVO.class, MeasurementObservationsVO.class } )
public class SimulationInputVOConstructorTest {

  private BigDecimal dummyPacingFrequency;
  private BigDecimal dummyPacingMaxTime;
  private BigDecimal dummyC50ICaL;
  private BigDecimal dummyHillICaL;
  private BigDecimal dummySaturationICaL;
  private BigDecimal dummySpreadICaL;
  private BigDecimal dummyC50IK1;
  private BigDecimal dummyHillIK1;
  private BigDecimal dummySaturationIK1;
  private BigDecimal dummySpreadIK1;
  private BigDecimal dummyC50IKr;
  private BigDecimal dummyHillIKr;
  private BigDecimal dummySaturationIKr;
  private BigDecimal dummySpreadIKr;
  private BigDecimal dummyC50IKs;
  private BigDecimal dummyHillIKs;
  private BigDecimal dummySaturationIKs;
  private BigDecimal dummySpreadIKs;
  private BigDecimal dummyC50Ito;
  private BigDecimal dummyHillIto;
  private BigDecimal dummySaturationIto;
  private BigDecimal dummySpreadIto;
  private BigDecimal dummyC50INa;
  private BigDecimal dummyHillINa;
  private BigDecimal dummySaturationINa;
  private BigDecimal dummySpreadINa;
  private BigDecimal dummyC50INaL;
  private BigDecimal dummyHillINaL;
  private BigDecimal dummySaturationINaL;
  private BigDecimal dummySpreadINaL;
  private BigDecimal dummyPlasmaConcMax;
  private BigDecimal dummyPlasmaConcMin;
  private boolean dummyPlasmaIntPtLogScale;
  private C50_TYPE dummyC50Type;
  private IC50_UNIT dummyIC50Unit;
  private IMocksControl mocksControl;
  private Long dummyId;
  private MeasurementObservationsVO mockMOICaL;
  private MeasurementObservationsVO mockMOIK1;
  private MeasurementObservationsVO mockMOIKr;
  private MeasurementObservationsVO mockMOIKs;
  private MeasurementObservationsVO mockMOIto;
  private MeasurementObservationsVO mockMOINa;
  private MeasurementObservationsVO mockMOINaL;
  private PortalFile dummyPKFile;
  private Short dummyModelIdentifier;
  private Short dummyPlasmaIntPtCount;
  private SimulationInput mockSimulationInput;
  private SimulationInputVO simulationInputVO;
  private String dummyCellMLFileName;
  private String dummyCIPctiles;
  private String dummyNotes;
  private String dummyPlasmaConcPoints;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockSimulationInput = mocksControl.createMock(SimulationInput.class);
    mockMOICaL = mocksControl.createMock(MeasurementObservationsVO.class);
    mockMOIK1 = mocksControl.createMock(MeasurementObservationsVO.class);
    mockMOIKr = mocksControl.createMock(MeasurementObservationsVO.class);
    mockMOIKs = mocksControl.createMock(MeasurementObservationsVO.class);
    mockMOIto = mocksControl.createMock(MeasurementObservationsVO.class);
    mockMOINa = mocksControl.createMock(MeasurementObservationsVO.class);
    mockMOINaL = mocksControl.createMock(MeasurementObservationsVO.class);

    dummyId = 1L;
    dummyModelIdentifier = 1;
    dummyPacingFrequency = new BigDecimal("0.5");
    dummyPacingMaxTime = new BigDecimal("3.0");
    dummyC50ICaL = new BigDecimal("5.0");
    dummyHillICaL = new BigDecimal("0.5");
    dummySaturationICaL = new BigDecimal("50.0");
    dummySpreadICaL = new BigDecimal("0.21");
    dummyC50IK1 = new BigDecimal("5.1");
    dummyHillIK1 = new BigDecimal("0.51");
    dummySaturationIK1 = new BigDecimal("50.1");
    dummySpreadIK1 = new BigDecimal("0.24");
    dummyC50IKr = new BigDecimal("5.2");
    dummyHillIKr = new BigDecimal("0.52");
    dummySaturationIKr = new BigDecimal("50.2");
    dummySpreadIKr = new BigDecimal("0.54");
    dummyC50IKs = new BigDecimal("5.3");
    dummyHillIKs = new BigDecimal("0.53");
    dummySaturationIKs = new BigDecimal("50.3");
    dummySpreadIKs = new BigDecimal("0.62");
    dummyC50INa = new BigDecimal("5.4");
    dummyHillINa = new BigDecimal("0.54");
    dummySaturationINa = new BigDecimal("50.4");
    dummySpreadINa = new BigDecimal("0.12");
    dummyC50INaL = new BigDecimal("5.41");
    dummyHillINaL = new BigDecimal("0.541");
    dummySaturationINaL = new BigDecimal("50.41");
    dummySpreadINaL = new BigDecimal("0.121");
    dummyC50Ito = new BigDecimal("5.5");
    dummyHillIto = new BigDecimal("0.55");
    dummySaturationIto = new BigDecimal("50.5");
    dummySpreadIto = new BigDecimal("0.33");

    dummyPlasmaConcMax = new BigDecimal("100.0");
    dummyPlasmaConcMin = new BigDecimal("0.0");
    dummyPlasmaIntPtCount = 2;
    dummyPlasmaIntPtLogScale = true;
    dummyC50Type = C50_TYPE.IC50;
    dummyIC50Unit = IC50_UNIT.M;
    dummyPlasmaConcPoints = "a b";
    dummyPKFile = null;
    dummyCellMLFileName = null;
    dummyNotes = "dummyNotes";
    dummyCIPctiles = "dummyCIPctiles";
  }

  @Test
  public void testConstructor() throws Exception {
    /*
     * Supply valid dummy values (but no PK file)
     */
    expect(mockSimulationInput.getId()).andReturn(dummyId);
    expect(mockSimulationInput.getModelIdentifier())
                              .andReturn(dummyModelIdentifier);
    expect(mockSimulationInput.getPacingFrequency())
                              .andReturn(dummyPacingFrequency);
    expect(mockSimulationInput.getPacingMaxTime()).andReturn(dummyPacingMaxTime);
    expect(mockSimulationInput.getC50ICaL()).andReturn(dummyC50ICaL);
    expect(mockSimulationInput.getHillICaL()).andReturn(dummyHillICaL);
    expect(mockSimulationInput.getSaturationICaL())
                              .andReturn(dummySaturationICaL);
    expect(mockSimulationInput.getSpreadICaL()).andReturn(dummySpreadICaL);
    expectNew(MeasurementObservationsVO.class, dummyC50ICaL, dummyHillICaL,
                                               dummySaturationICaL,
                                               dummySpreadICaL)
             .andReturn(mockMOICaL);
    expect(mockSimulationInput.getC50Ik1()).andReturn(dummyC50IK1);
    expect(mockSimulationInput.getHillIk1()).andReturn(dummyHillIK1);
    expect(mockSimulationInput.getSaturationIk1()).andReturn(dummySaturationIK1);
    expect(mockSimulationInput.getSpreadIk1()).andReturn(dummySpreadIK1);
    expectNew(MeasurementObservationsVO.class, dummyC50IK1, dummyHillIK1,
                                               dummySaturationIK1,
                                               dummySpreadIK1)
             .andReturn(mockMOIK1);
    expect(mockSimulationInput.getC50Iks()).andReturn(dummyC50IKs);
    expect(mockSimulationInput.getHillIks()).andReturn(dummyHillIKs);
    expect(mockSimulationInput.getSaturationIks()).andReturn(dummySaturationIKs);
    expect(mockSimulationInput.getSpreadIks()).andReturn(dummySpreadIKs);
    expectNew(MeasurementObservationsVO.class, dummyC50IKs, dummyHillIKs,
                                               dummySaturationIKs,
                                               dummySpreadIKs)
             .andReturn(mockMOIKs);
    expect(mockSimulationInput.getC50Ikr()).andReturn(dummyC50IKr);
    expect(mockSimulationInput.getHillIkr()).andReturn(dummyHillIKr);
    expect(mockSimulationInput.getSaturationIkr()).andReturn(dummySaturationIKr);
    expect(mockSimulationInput.getSpreadIkr()).andReturn(dummySpreadIKr);
    expectNew(MeasurementObservationsVO.class, dummyC50IKr, dummyHillIKr,
                                               dummySaturationIKr,
                                               dummySpreadIKr)
             .andReturn(mockMOIKr);
    expect(mockSimulationInput.getC50Ito()).andReturn(dummyC50Ito);
    expect(mockSimulationInput.getHillIto()).andReturn(dummyHillIto);
    expect(mockSimulationInput.getSaturationIto()).andReturn(dummySaturationIto);
    expect(mockSimulationInput.getSpreadIto()).andReturn(dummySpreadIto);
    expectNew(MeasurementObservationsVO.class, dummyC50Ito, dummyHillIto,
                                               dummySaturationIto,
                                               dummySpreadIto)
             .andReturn(mockMOIto);
    expect(mockSimulationInput.getC50INa()).andReturn(dummyC50INa);
    expect(mockSimulationInput.getHillINa()).andReturn(dummyHillINa);
    expect(mockSimulationInput.getSaturationINa()).andReturn(dummySaturationINa);
    expect(mockSimulationInput.getSpreadINa()).andReturn(dummySpreadINa);
    expectNew(MeasurementObservationsVO.class, dummyC50INa, dummyHillINa,
                                               dummySaturationINa,
                                               dummySpreadINa)
             .andReturn(mockMOINa);
    expect(mockSimulationInput.getC50INaL()).andReturn(dummyC50INaL);
    expect(mockSimulationInput.getHillINaL()).andReturn(dummyHillINaL);
    expect(mockSimulationInput.getSaturationINaL()).andReturn(dummySaturationINaL);
    expect(mockSimulationInput.getSpreadINaL()).andReturn(dummySpreadINaL);
    expectNew(MeasurementObservationsVO.class, dummyC50INaL, dummyHillINaL,
                                               dummySaturationINaL,
                                               dummySpreadINaL)
             .andReturn(mockMOINaL);
    boolean dummyHasNonC50Value = true;
    expect(mockMOICaL.hasNonC50Value()).andReturn(dummyHasNonC50Value);

    expect(mockSimulationInput.getC50Type()).andReturn(dummyC50Type);
    expect(mockSimulationInput.getIc50Units()).andReturn(dummyIC50Unit);
    expect(mockSimulationInput.getPlasmaConcMin()).andReturn(dummyPlasmaConcMin);
    expect(mockSimulationInput.getPlasmaConcMax()).andReturn(dummyPlasmaConcMax);
    expect(mockSimulationInput.getPlasmaIntPtCount())
          .andReturn(dummyPlasmaIntPtCount);
    expect(mockSimulationInput.isPlasmaIntPtLogScale())
          .andReturn(dummyPlasmaIntPtLogScale);
    expect(mockSimulationInput.getPlasmaConcPoints())
          .andReturn(dummyPlasmaConcPoints);
    expect(mockSimulationInput.getPkFile()).andReturn(dummyPKFile);
    expect(mockSimulationInput.getCellMLFileName())
          .andReturn(dummyCellMLFileName);
    expect(mockSimulationInput.getCredibleIntervalPctiles())
          .andReturn(dummyCIPctiles);
    expect(mockSimulationInput.getNotes()).andReturn(dummyNotes);

    replayAll();
    mocksControl.replay();

    simulationInputVO = new SimulationInputVO(mockSimulationInput);

    verifyAll();
    mocksControl.verify();

    assertEquals(dummyModelIdentifier, simulationInputVO.getModelIdentifier());
    assertTrue(dummyPacingFrequency.compareTo(simulationInputVO.getPacingFrequency()) == 0);
    assertTrue(dummyPacingMaxTime.compareTo(simulationInputVO.getPacingMaxTime()) == 0);
    assertSame(mockMOICaL, simulationInputVO.getICaLObservations());
    assertSame(mockMOIK1, simulationInputVO.getIK1Observations());
    assertSame(mockMOIKr, simulationInputVO.getIKrObservations());
    assertSame(mockMOIKs, simulationInputVO.getIKsObservations());
    assertSame(mockMOINa, simulationInputVO.getINaObservations());
    assertSame(mockMOINaL, simulationInputVO.getINaLObservations());
    assertSame(mockMOIto, simulationInputVO.getItoObservations());
    // Can't do the following as retrievePIC50??? returns a mock object! 
    //assertTrue(dummyC50ICaL.compareTo(simulationInputVO.retrievePIC50ICaL()) == 0);
    //assertTrue(dummyC50IK1.compareTo(simulationInputVO.retrievePIC50Ik1()) == 0);
    //assertTrue(dummyC50IKr.compareTo(simulationInputVO.retrievePIC50Ikr()) == 0);
    //assertTrue(dummyC50IKs.compareTo(simulationInputVO.retrievePIC50Iks()) == 0);
    //assertTrue(dummyC50INa.compareTo(simulationInputVO.retrievePIC50INa()) == 0);
    //assertTrue(dummyC50Ito.compareTo(simulationInputVO.retrievePIC50Ito()) == 0);
    assertTrue(dummyC50Type.compareTo(simulationInputVO.getC50Type()) == 0);
    assertTrue(dummyIC50Unit.compareTo(simulationInputVO.getIc50Units()) == 0);
    assertTrue(dummyPlasmaConcMax.compareTo(simulationInputVO.getPlasmaConcMax()) == 0);
    assertTrue(dummyPlasmaConcMin.compareTo(simulationInputVO.getPlasmaConcMin()) == 0);
    assertEquals(dummyPlasmaIntPtCount, simulationInputVO.getPlasmaIntPtCount());
    assertTrue(simulationInputVO.getPlasmaIntPtLogScale());
    assertEquals(Arrays.asList(dummyPlasmaConcPoints.split(" ")),
                 simulationInputVO.getPlasmaConcPoints());
    assertNull(simulationInputVO.getPkFile());
    assertEquals(dummyNotes, simulationInputVO.getNotes());
    assertNotNull(simulationInputVO.toString());
    assertTrue(simulationInputVO.isNonC50Value());
    assertTrue(simulationInputVO.getAssignmentProblems().isEmpty());
    // PK file was null, so next test was for conc points!
    assertEquals(InputOption.CONC_POINTS,
                 simulationInputVO.retrieveInputOption());
    assertFalse(simulationInputVO.hasAssignmentProblems());
    assertNull(simulationInputVO.getCellMLFile());
    assertNull(simulationInputVO.getCellMLFileName());
    assertEquals(dummyCIPctiles, simulationInputVO.getCredibleIntervalPctiles());

    resetAll();
    mocksControl.reset();

    /*
     * Now test with 
     * 1. No compound effects data.
     * 2. Empty plasma conc points.
     */
    expect(mockSimulationInput.getId()).andReturn(dummyId);
    expect(mockSimulationInput.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockSimulationInput.getPacingFrequency()).andReturn(dummyPacingFrequency);
    expect(mockSimulationInput.getPacingMaxTime()).andReturn(dummyPacingMaxTime);
    expect(mockSimulationInput.getC50ICaL()).andReturn(null);
    expect(mockSimulationInput.getHillICaL()).andReturn(null);
    expect(mockSimulationInput.getSaturationICaL()).andReturn(null);
    expect(mockSimulationInput.getSpreadICaL()).andReturn(null);
    expectNew(MeasurementObservationsVO.class, isNull(), isNull(), isNull(),
                                               isNull())
             .andReturn(mockMOICaL);
    expect(mockSimulationInput.getC50Ik1()).andReturn(null);
    expect(mockSimulationInput.getHillIk1()).andReturn(null);
    expect(mockSimulationInput.getSaturationIk1()).andReturn(null);
    expect(mockSimulationInput.getSpreadIk1()).andReturn(null);
    expectNew(MeasurementObservationsVO.class, isNull(), isNull(), isNull(),
                                               isNull())
             .andReturn(mockMOIK1);
    expect(mockSimulationInput.getC50Iks()).andReturn(null);
    expect(mockSimulationInput.getHillIks()).andReturn(null);
    expect(mockSimulationInput.getSaturationIks()).andReturn(null);
    expect(mockSimulationInput.getSpreadIks()).andReturn(null);
    expectNew(MeasurementObservationsVO.class, isNull(), isNull(), isNull(),
                                               isNull())
             .andReturn(mockMOIKs);
    expect(mockSimulationInput.getC50Ikr()).andReturn(null);
    expect(mockSimulationInput.getHillIkr()).andReturn(null);
    expect(mockSimulationInput.getSaturationIkr()).andReturn(null);
    expect(mockSimulationInput.getSpreadIkr()).andReturn(null);
    expectNew(MeasurementObservationsVO.class, isNull(), isNull(), isNull(),
                                               isNull())
             .andReturn(mockMOIKr);
    expect(mockSimulationInput.getC50Ito()).andReturn(null);
    expect(mockSimulationInput.getHillIto()).andReturn(null);
    expect(mockSimulationInput.getSaturationIto()).andReturn(null);
    expect(mockSimulationInput.getSpreadIto()).andReturn(null);
    expectNew(MeasurementObservationsVO.class, isNull(), isNull(), isNull(),
                                               isNull())
             .andReturn(mockMOIto);
    expect(mockSimulationInput.getC50INa()).andReturn(null);
    expect(mockSimulationInput.getHillINa()).andReturn(null);
    expect(mockSimulationInput.getSaturationINa()).andReturn(null);
    expect(mockSimulationInput.getSpreadINa()).andReturn(null);
    expectNew(MeasurementObservationsVO.class, isNull(), isNull(), isNull(),
                                               isNull())
             .andReturn(mockMOINa);
    expect(mockSimulationInput.getC50INaL()).andReturn(null);
    expect(mockSimulationInput.getHillINaL()).andReturn(null);
    expect(mockSimulationInput.getSaturationINaL()).andReturn(null);
    expect(mockSimulationInput.getSpreadINaL()).andReturn(null);
    expectNew(MeasurementObservationsVO.class, isNull(), isNull(), isNull(),
                                               isNull())
             .andReturn(mockMOINaL);
    dummyHasNonC50Value = false;
    expect(mockMOICaL.hasNonC50Value()).andReturn(dummyHasNonC50Value);
    expect(mockMOIKr.hasNonC50Value()).andReturn(dummyHasNonC50Value);
    expect(mockMOIK1.hasNonC50Value()).andReturn(dummyHasNonC50Value);
    expect(mockMOIKs.hasNonC50Value()).andReturn(dummyHasNonC50Value);
    expect(mockMOIto.hasNonC50Value()).andReturn(dummyHasNonC50Value);
    expect(mockMOINa.hasNonC50Value()).andReturn(dummyHasNonC50Value);
    expect(mockMOINaL.hasNonC50Value()).andReturn(dummyHasNonC50Value);
    expect(mockSimulationInput.getC50Type()).andReturn(dummyC50Type);
    expect(mockSimulationInput.getIc50Units()).andReturn(dummyIC50Unit);
    expect(mockSimulationInput.getPlasmaConcMin()).andReturn(dummyPlasmaConcMin);
    expect(mockSimulationInput.getPlasmaConcMax()).andReturn(dummyPlasmaConcMax);
    expect(mockSimulationInput.getPlasmaIntPtCount())
          .andReturn(dummyPlasmaIntPtCount);
    expect(mockSimulationInput.isPlasmaIntPtLogScale())
          .andReturn(dummyPlasmaIntPtLogScale);
    expect(mockSimulationInput.getPlasmaConcPoints()).andReturn("");
    expect(mockSimulationInput.getPkFile()).andReturn(dummyPKFile);
    expect(mockSimulationInput.getCellMLFileName())
          .andReturn(dummyCellMLFileName);
    expect(mockSimulationInput.getCredibleIntervalPctiles())
          .andReturn(dummyCIPctiles);
    expect(mockSimulationInput.getNotes()).andReturn(dummyNotes);

    replayAll();
    mocksControl.replay();

    simulationInputVO = new SimulationInputVO(mockSimulationInput);

    verifyAll();
    mocksControl.verify();

    assertTrue(dummyPlasmaConcMax.compareTo(simulationInputVO.getPlasmaConcMax()) == 0);
    assertTrue(dummyPlasmaConcMin.compareTo(simulationInputVO.getPlasmaConcMin()) == 0);
    assertEquals(dummyPlasmaIntPtCount, simulationInputVO.getPlasmaIntPtCount());
    assertTrue(simulationInputVO.getPlasmaIntPtLogScale());
    assertTrue(simulationInputVO.getPlasmaConcPoints().isEmpty());
    assertNull(simulationInputVO.getPkFile());
    assertEquals(InputOption.CONC_RANGE,
                 simulationInputVO.retrieveInputOption());
    assertFalse(simulationInputVO.hasAssignmentProblems());
    assertFalse(simulationInputVO.isNonC50Value());

    resetAll();
    mocksControl.reset();

    /*
     * Now test with 
     * 1. No compound effects data.
     * 2. Empty plasma conc points and range but a PK file.
     */
    dummyPKFile = mocksControl.createMock(PortalFile.class);

    expect(mockSimulationInput.getId()).andReturn(dummyId);
    expect(mockSimulationInput.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockSimulationInput.getPacingFrequency()).andReturn(dummyPacingFrequency);
    expect(mockSimulationInput.getPacingMaxTime()).andReturn(dummyPacingMaxTime);
    expect(mockSimulationInput.getC50ICaL()).andReturn(null);
    expect(mockSimulationInput.getHillICaL()).andReturn(null);
    expect(mockSimulationInput.getSaturationICaL()).andReturn(null);
    expect(mockSimulationInput.getSpreadICaL()).andReturn(null);
    expectNew(MeasurementObservationsVO.class, isNull(), isNull(), isNull(),
                                               isNull())
             .andReturn(mockMOICaL);
    expect(mockSimulationInput.getC50Ik1()).andReturn(null);
    expect(mockSimulationInput.getHillIk1()).andReturn(null);
    expect(mockSimulationInput.getSaturationIk1()).andReturn(null);
    expect(mockSimulationInput.getSpreadIk1()).andReturn(null);
    expectNew(MeasurementObservationsVO.class, isNull(), isNull(), isNull(),
                                               isNull())
             .andReturn(mockMOIK1);
    expect(mockSimulationInput.getC50Iks()).andReturn(null);
    expect(mockSimulationInput.getHillIks()).andReturn(null);
    expect(mockSimulationInput.getSaturationIks()).andReturn(null);
    expect(mockSimulationInput.getSpreadIks()).andReturn(null);
    expectNew(MeasurementObservationsVO.class, isNull(), isNull(), isNull(),
                                               isNull())
             .andReturn(mockMOIKs);
    expect(mockSimulationInput.getC50Ikr()).andReturn(null);
    expect(mockSimulationInput.getHillIkr()).andReturn(null);
    expect(mockSimulationInput.getSaturationIkr()).andReturn(null);
    expect(mockSimulationInput.getSpreadIkr()).andReturn(null);
    expectNew(MeasurementObservationsVO.class, isNull(), isNull(), isNull(),
                                               isNull())
             .andReturn(mockMOIKr);
    expect(mockSimulationInput.getC50Ito()).andReturn(null);
    expect(mockSimulationInput.getHillIto()).andReturn(null);
    expect(mockSimulationInput.getSaturationIto()).andReturn(null);
    expect(mockSimulationInput.getSpreadIto()).andReturn(null);
    expectNew(MeasurementObservationsVO.class, isNull(), isNull(), isNull(),
                                               isNull())
             .andReturn(mockMOIto);
    expect(mockSimulationInput.getC50INa()).andReturn(null);
    expect(mockSimulationInput.getHillINa()).andReturn(null);
    expect(mockSimulationInput.getSaturationINa()).andReturn(null);
    expect(mockSimulationInput.getSpreadINa()).andReturn(null);
    expectNew(MeasurementObservationsVO.class, isNull(), isNull(), isNull(),
                                               isNull())
             .andReturn(mockMOINa);
    expect(mockSimulationInput.getC50INaL()).andReturn(null);
    expect(mockSimulationInput.getHillINaL()).andReturn(null);
    expect(mockSimulationInput.getSaturationINaL()).andReturn(null);
    expect(mockSimulationInput.getSpreadINaL()).andReturn(null);
    expectNew(MeasurementObservationsVO.class, isNull(), isNull(), isNull(),
                                               isNull())
             .andReturn(mockMOINaL);
    dummyHasNonC50Value = false;
    expect(mockMOICaL.hasNonC50Value()).andReturn(dummyHasNonC50Value);
    expect(mockMOIKr.hasNonC50Value()).andReturn(dummyHasNonC50Value);
    expect(mockMOIK1.hasNonC50Value()).andReturn(dummyHasNonC50Value);
    expect(mockMOIKs.hasNonC50Value()).andReturn(dummyHasNonC50Value);
    expect(mockMOIto.hasNonC50Value()).andReturn(dummyHasNonC50Value);
    expect(mockMOINa.hasNonC50Value()).andReturn(dummyHasNonC50Value);
    expect(mockMOINaL.hasNonC50Value()).andReturn(dummyHasNonC50Value);
    expect(mockSimulationInput.getC50Type()).andReturn(dummyC50Type);
    expect(mockSimulationInput.getIc50Units()).andReturn(dummyIC50Unit);
    expect(mockSimulationInput.getPlasmaConcMin()).andReturn(null);
    expect(mockSimulationInput.getPlasmaConcMax()).andReturn(null);
    expect(mockSimulationInput.getPlasmaIntPtCount()).andReturn(null);
    expect(mockSimulationInput.isPlasmaIntPtLogScale()).andReturn(false);
    expect(mockSimulationInput.getPlasmaConcPoints()).andReturn("");
    expect(mockSimulationInput.getPkFile()).andReturn(dummyPKFile);
    expect(mockSimulationInput.getCellMLFileName())
          .andReturn(dummyCellMLFileName);
    expect(mockSimulationInput.getCredibleIntervalPctiles())
          .andReturn(dummyCIPctiles);
    expect(mockSimulationInput.getNotes()).andReturn(dummyNotes);

    replayAll();
    mocksControl.replay();

    simulationInputVO = new SimulationInputVO(mockSimulationInput);

    verifyAll();
    mocksControl.verify();

    assertNull(simulationInputVO.getPlasmaConcMax());
    assertNull(simulationInputVO.getPlasmaConcMin());
    assertNull(simulationInputVO.getPlasmaIntPtCount());
    assertFalse(simulationInputVO.getPlasmaIntPtLogScale());
    assertTrue(simulationInputVO.getPlasmaConcPoints().isEmpty());
    assertNotNull(simulationInputVO.getPkFile());
    assertEquals(InputOption.PK, simulationInputVO.retrieveInputOption());
    assertFalse(simulationInputVO.hasAssignmentProblems());
  }

  @Test(expected=IllegalArgumentException.class)
  public void testConstructorFailsOnTransient() {
    expect(mockSimulationInput.getId()).andReturn(null);

    mocksControl.replay();

    simulationInputVO = new SimulationInputVO(mockSimulationInput);

    mocksControl.verify();

    assertNull(simulationInputVO);
  }
}