/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import org.junit.Test;

import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;

/**
 * Unit test the feature processing.
 *
 * @author geoff
 */
public class ResultsProcessInputFeatureProcessingTest extends AbstractResultsProcessInputTest {

  @Test
  // Test the checkbox option setting.
  public void testSpreadsEnabledByUser() throws Exception {
    /*
     * Spreads enabled by user, and default feature processing allows feature
     * access. No spread values defined though, so SimulationInputVO.Builder
     * receives nulls.
     */
    dummyAllowUncertainties = true;
    dummySpreadsEnabled = Results.BOX_CHECKED_VALUE;

    defaultPortalFeatureProcessing();
    SimulationInputVO mockSimulationInput = assignSimulationInputProperties(null,
                                                                            null,
                                                                            true);

    latterTesting(mockSimulationInput);

    replayAll();
    mocksControl.replay();

    String returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_RESULTS, returnPage);

    resetAll();
    mocksControl.reset();

    /*
     * Spreads enabled by user, and default feature processing allows feature
     * access. Define some spread values and verify receipt by
     * SimulationInputVO.Builder.
     */
    dummyICaLSpread = "dummyICaLSpread";
    dummyIK1Spread = "dummyIK1Spread";
    dummyIKrSpread = "dummyIKrSpread";
    dummyIKsSpread = "dummyIKsSpread";
    dummyINaSpread = "dummyINaSpread";
    dummyItoSpread = "dummyItoSpread";

    defaultPortalFeatureProcessing();
    mockSimulationInput = assignSimulationInputProperties(null, null, true);

    latterTesting(mockSimulationInput);

    replayAll();
    mocksControl.replay();

    returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_RESULTS, returnPage);

    resetAll();
    mocksControl.reset();

    /*
     * Spreads not enabled by user, and default feature processing allows
     * feature access. Any spread values received should be nullified.
     */
    dummyAllowUncertainties = false;
    dummySpreadsEnabled = null;

    defaultPortalFeatureProcessing();
    mockSimulationInput = assignSimulationInputProperties(null, null, false);
    latterTesting(mockSimulationInput);

    replayAll();
    mocksControl.replay();

    returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_RESULTS, returnPage);

    resetAll();
    mocksControl.reset();
  }

  @Test
  // Test the call to the client-direct service.
  public void testRestrictedRoles() throws Exception {
    dummySpreadsEnabled = Results.BOX_CHECKED_VALUE;
    dummyICaLSpread = "dummyICaLSpread";
    dummyIK1Spread = "dummyIK1Spread";
    dummyIKrSpread = "dummyIKrSpread";
    dummyIKsSpread = "dummyIKsSpread";
    dummyINaSpread = "dummyINaSpread";
    dummyItoSpread = "dummyItoSpread";

    dummyAllowUncertainties = true;

    /*
     * Test user having restricted roles.
     */
    defaultPortalFeatureProcessing();
    SimulationInputVO mockSimulationInput = assignSimulationInputProperties(null,
                                                                            null, 
                                                                            true);
    latterTesting(mockSimulationInput);

    replayAll();
    mocksControl.replay();

    String returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_RESULTS, returnPage);

    resetAll();
    mocksControl.reset();

    /*
     * Test user not having restricted roles.
     */
    dummyAllowUncertainties = false;
    defaultPortalFeatureProcessing();
    mockSimulationInput = assignSimulationInputProperties(null, null, false);

    latterTesting(mockSimulationInput);

    replayAll();
    mocksControl.replay();

    returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_RESULTS, returnPage);

    resetAll();
    mocksControl.reset();

    /*
     * Test user not having restricted roles (but checkbox to use spreads
     * selected - so log a warn message!!).
     */
    dummySpreadsEnabled = Results.BOX_CHECKED_VALUE;
    defaultPortalFeatureProcessing();

    mockSimulationInput = assignSimulationInputProperties(null, null, false);

    latterTesting(mockSimulationInput);

    replayAll();
    mocksControl.replay();

    returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_RESULTS, returnPage);

    resetAll();
    mocksControl.reset();

    /*
     * Test user not being allowed to specify spreads, so any values arriving 
     * are nullified.
     */
    dummySpreadsEnabled = null;
    defaultPortalFeatureProcessing();

    mockSimulationInput = assignSimulationInputProperties(null, null, false);

    latterTesting(mockSimulationInput);

    replayAll();
    mocksControl.replay();

    returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_RESULTS, returnPage);
  }
}