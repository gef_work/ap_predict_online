/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.entity;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;
import java.util.Set;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Unit test the Job entity.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { Job.class } )
public class JobTest {

  private IMocksControl mocksControl;
  private Job job;
  private long dummyAppManagerId;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();

    dummyAppManagerId = 124l;
    job = new Job(dummyAppManagerId);
  }

  @Test
  public void testInitialisingConstructor() {
    assertSame(dummyAppManagerId, job.getAppManagerId());
    assertFalse(job.hasResults());
    assertFalse(job.hasPKResults());
    assertNull(job.getMessages());
    assertSame(0, job.getJobResults().size());
    assertSame(0, job.getJobPKResults().size());
    assertSame(0, job.retrieveJobResultCopies().size());
  }

  @Test
  public void testJobResults() {
    final JobResult mockJobResult = mocksControl.createMock(JobResult.class);

    mocksControl.replay();

    job.addJobResult(mockJobResult);

    mocksControl.verify();
  }

  @Test
  public void testJobPKResults() throws Exception {
    Set<JobPKResult> retrieved = job.retrieveJobPKResultCopies();
    assertTrue(retrieved.isEmpty());
    assertFalse(job.hasPKResults());

    /*
     * Add a new JobPKResult to the copies.
     */
    final JobPKResult mockJobPKResult = mocksControl.createMock(JobPKResult.class);

    mocksControl.replay();

    retrieved.add(mockJobPKResult);

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Try adding a JobPKResult the wrong way.
     */
    mocksControl.replay();

    try {
      job.getJobPKResults().add(mockJobPKResult);
      fail("Should not permit the direct modification of the original collection!");
    } catch (UnsupportedOperationException e) {}

    mocksControl.verify();

    assertFalse(job.hasPKResults());

    mocksControl.reset();

    /*
     * Try adding the right way.
     */
    mocksControl.replay();

    job.addJobPKResult(mockJobPKResult);

    mocksControl.verify();

    assertTrue(job.hasPKResults());

    mocksControl.reset();

    /*
     * Test the PK results copying when there's a PK result available. 
     */
    final BigDecimal dummyTimepoint = BigDecimal.ONE;
    expect(mockJobPKResult.getTimepoint()).andReturn(dummyTimepoint);
    final String dummyAPD90s = "dummyAPD90s";
    expect(mockJobPKResult.getApd90s()).andReturn(dummyAPD90s);
    final boolean dummyIsForTranslation = true;
    expect(mockJobPKResult.isForTranslation()).andReturn(dummyIsForTranslation);
    final JobPKResult mockCopiedJobPKResult = mocksControl.createMock(JobPKResult.class);
    expectNew(JobPKResult.class, dummyTimepoint, dummyAPD90s,
                                 dummyIsForTranslation)
             .andReturn(mockCopiedJobPKResult);

    replayAll();
    mocksControl.replay();

    retrieved = job.retrieveJobPKResultCopies();

    verifyAll();
    mocksControl.verify();

    assertSame(1, retrieved.size());
    assertTrue(retrieved.contains(mockCopiedJobPKResult));
  }

  @Test
  public void testRetrieveJobResultCopies() throws Exception {
    final JobResult mockJobResult = mocksControl.createMock(JobResult.class);
    job.addJobResult(mockJobResult);
    assertTrue(job.hasResults());

    final BigDecimal dummyReference = BigDecimal.TEN;
    final String dummyTimes = "dummyTimes";
    final String dummyVoltages = "dummyVoltages";
    final String dummyAPD90 = "dummyAPD90";
    final String dummyDeltaAPD90 = "dummyDeltaAPD90";
    final String dummyQNet = "dummyQNet";
    final boolean dummyForTranslation = true;

    expect(mockJobResult.getReference()).andReturn(dummyReference);
    expect(mockJobResult.getTimes()).andReturn(dummyTimes);
    expect(mockJobResult.getVoltages()).andReturn(dummyVoltages);
    expect(mockJobResult.getAPD90()).andReturn(dummyAPD90);
    expect(mockJobResult.getDeltaAPD90()).andReturn(dummyDeltaAPD90);
    expect(mockJobResult.getQNet()).andReturn(dummyQNet);
    expect(mockJobResult.getForTranslation()).andReturn(dummyForTranslation);

    final JobResult mockCopiedJobResult = mocksControl.createMock(JobResult.class);
    expectNew(JobResult.class, dummyReference, dummyTimes, dummyVoltages,
                               dummyAPD90, dummyDeltaAPD90, dummyQNet,
                               dummyForTranslation)
             .andReturn(mockCopiedJobResult);

    replayAll();
    mocksControl.replay();

    final Set<JobResult> returnedJobResultCopies = job.retrieveJobResultCopies();

    verifyAll();
    mocksControl.verify();

    assertSame(1, returnedJobResultCopies.size());
    assertTrue(returnedJobResultCopies.contains(mockCopiedJobResult));
  }

  @Test
  public void testSetDeltaAPD90PctileNames() {
    assertNull(job.getDeltaAPD90PercentileNames());

    String dummyDeltaAPD90PctileNames = null;

    job.setDeltaAPD90PercentileNames(dummyDeltaAPD90PctileNames);

    assertNull(job.getDeltaAPD90PercentileNames());

    dummyDeltaAPD90PctileNames = "dummyDeltaAPD90PctileNames";
    job.setDeltaAPD90PercentileNames(dummyDeltaAPD90PctileNames);

    assertEquals(dummyDeltaAPD90PctileNames, job.getDeltaAPD90PercentileNames());
  }

  @Test
  public void testSetMessages() {
    final String dummyMessages = "dummyMessages";

    job.setMessages(dummyMessages);

    assertTrue(job.toString().contains(dummyMessages));
  }
}