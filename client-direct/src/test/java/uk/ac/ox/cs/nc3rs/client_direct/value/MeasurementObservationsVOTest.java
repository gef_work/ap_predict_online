/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.value;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit test the {@link MeasurementObservationsVO} object.
 * <p>
 * Note that {@linkplain SimulationInputVOBuilderTest}, although listed as a
 * unit test, provisionally(*) behaves as an integration test as it tests the
 * {@linkplain MeasurementObservationsVO} object creation and value retrieval!
 *
 * (*) Until I work out how to unit test Builder pattern objects!
 * 
 * @author geoff
 */
public class MeasurementObservationsVOTest {

  private static final String str10 = BigDecimal.TEN.toString();
  
  private BigDecimal dummyC50;
  private BigDecimal dummyHill;
  private BigDecimal dummySaturation;
  private BigDecimal dummySpread;

  private MeasurementObservationsVO observations;

  @Before
  public void setUp() {
    dummyC50 = null;
    dummyHill = null;
    dummySaturation = null;
    dummySpread = null;
  }

  @Test
  public void testConstructor() {
    /*
     * Test C50 assignment.
     */
    observations = new MeasurementObservationsVO(dummyC50, dummyHill,
                                                 dummySaturation, dummySpread);
    assertNull(observations.getC50());
    assertNull(observations.getHill());
    assertNull(observations.getSaturation());
    assertNull(observations.getSpread());
    assertFalse(observations.hasNonC50Value());
    assertNotNull(observations.toString());

    dummyC50 = BigDecimal.TEN;

    observations = new MeasurementObservationsVO(dummyC50, dummyHill,
                                                 dummySaturation, dummySpread);
    assertTrue(dummyC50.compareTo(observations.getC50()) == 0);
    assertNull(observations.getHill());
    assertNull(observations.getSaturation());
    assertNull(observations.getSpread());
    assertFalse(observations.hasNonC50Value());

    /*
     * Test Hill assignment
     */
    dummyHill = BigDecimal.TEN;

    observations = new MeasurementObservationsVO(dummyC50, dummyHill,
                                                 dummySaturation, dummySpread);
    assertTrue(dummyC50.compareTo(observations.getC50()) == 0);
    assertTrue(dummyHill.compareTo(observations.getHill()) == 0);
    assertNull(observations.getSaturation());
    assertNull(observations.getSpread());
    assertTrue(observations.hasNonC50Value());

    dummyHill = SimulationInputVO.DEFAULT_HILL_COEFFICIENT_BD;

    observations = new MeasurementObservationsVO(dummyC50, dummyHill,
                                                 dummySaturation, dummySpread);
    assertTrue(dummyC50.compareTo(observations.getC50()) == 0);
    assertTrue(dummyHill.compareTo(observations.getHill()) == 0);
    assertNull(observations.getSaturation());
    assertNull(observations.getSpread());
    assertFalse(observations.hasNonC50Value());

    /*
     * Test saturation.
     */
    dummyHill = null;
    dummySaturation = BigDecimal.TEN;

    observations = new MeasurementObservationsVO(dummyC50, dummyHill,
                                                 dummySaturation, dummySpread);
    assertTrue(dummyC50.compareTo(observations.getC50()) == 0);
    assertNull(observations.getHill());
    assertTrue(dummySaturation.compareTo(observations.getSaturation()) == 0);
    assertNull(observations.getSpread());
    assertTrue(observations.hasNonC50Value());

    dummySaturation = SimulationInputVO.DEFAULT_SATURATION_BD;

    observations = new MeasurementObservationsVO(dummyC50, dummyHill,
                                                 dummySaturation, dummySpread);

    assertTrue(dummyC50.compareTo(observations.getC50()) == 0);
    assertNull(observations.getHill());
    assertTrue(dummySaturation.compareTo(observations.getSaturation()) == 0);
    assertNull(observations.getSpread());
    assertFalse(observations.hasNonC50Value());

    /*
     * Test spread.
     */
    dummySaturation = null;
    dummySpread = BigDecimal.ZERO;

    observations = new MeasurementObservationsVO(dummyC50, dummyHill,
                                                 dummySaturation, dummySpread);
    assertTrue(dummyC50.compareTo(observations.getC50()) == 0);
    assertNull(observations.getHill());
    assertNull(observations.getSaturation());
    assertTrue(dummySpread.compareTo(observations.getSpread()) == 0);
    assertTrue(observations.hasNonC50Value());

    dummySpread = null;

    observations = new MeasurementObservationsVO(dummyC50, dummyHill,
                                                 dummySaturation, dummySpread);
    assertTrue(dummyC50.compareTo(observations.getC50()) == 0);
    assertNull(observations.getHill());
    assertNull(observations.getSaturation());
    assertNull(observations.getSpread());
    assertFalse(observations.hasNonC50Value());
  }

  @Test
  public void testHasNonC50Value() {
    String dummyHill = null;
    String dummySaturation = null;
    String dummySpread = null;

    assertFalse(MeasurementObservationsVO.hasNonC50Value(dummyHill,
                                                         dummySaturation,
                                                         dummySpread));

    /*
     * Test Hill
     */
    dummyHill = SimulationInputVO.DEFAULT_HILL_COEFFICIENT;
    assertFalse(MeasurementObservationsVO.hasNonC50Value(dummyHill,
                                                         dummySaturation,
                                                         dummySpread));

    dummyHill = str10;
    assertTrue(MeasurementObservationsVO.hasNonC50Value(dummyHill,
                                                        dummySaturation,
                                                        dummySpread));

    /*
     * Test saturation.
     */
    dummyHill = null;
    dummySaturation = SimulationInputVO.DEFAULT_SATURATION;
    assertFalse(MeasurementObservationsVO.hasNonC50Value(dummyHill,
                                                         dummySaturation,
                                                         dummySpread));

    dummySaturation = str10;
    assertTrue(MeasurementObservationsVO.hasNonC50Value(dummyHill,
                                                        dummySaturation,
                                                        dummySpread));

    /*
     * Test spread
     */
    dummySaturation = null;
    dummySpread = str10;
    assertTrue(MeasurementObservationsVO.hasNonC50Value(dummyHill,
                                                        dummySaturation,
                                                        dummySpread));
  }

  @Test
  public void testValid() {
    boolean validity = MeasurementObservationsVO.valid(dummyC50, dummyHill,
                                                       dummySaturation,
                                                       dummySpread);
    assertTrue(validity);

    dummySaturation = BigDecimal.TEN;
    validity = MeasurementObservationsVO.valid(dummyC50, dummyHill,
                                               dummySaturation, dummySpread);
    assertFalse(validity);

    dummySaturation = SimulationInputVO.DEFAULT_SATURATION_BD;
    validity = MeasurementObservationsVO.valid(dummyC50, dummyHill,
                                               dummySaturation, dummySpread);
    assertTrue(validity);

    dummySaturation = null;
    dummyHill = BigDecimal.TEN;
    validity = MeasurementObservationsVO.valid(dummyC50, dummyHill,
                                               dummySaturation, dummySpread);
    assertFalse(validity);

    dummyHill = SimulationInputVO.DEFAULT_HILL_COEFFICIENT_BD;
    validity = MeasurementObservationsVO.valid(dummyC50, dummyHill,
                                               dummySaturation, dummySpread);
    assertTrue(validity);

    dummySpread = BigDecimal.ONE;
    validity = MeasurementObservationsVO.valid(dummyC50, dummyHill,
                                               dummySaturation, dummySpread);
    assertFalse(validity);

    dummyC50 = BigDecimal.ZERO;
    validity = MeasurementObservationsVO.valid(dummyC50, dummyHill,
                                               dummySaturation, dummySpread);
    assertTrue(validity);
  }
}