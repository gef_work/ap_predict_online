/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.value;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.value.ResultsVO.PerReferenceDataVO;

/**
 * Unit test the results value object.
 *
 * @author geoff
 */
public class ResultsVOTest {

  private ResultsVO dummyResultsVO;

  @Test
  public void testConstructor() {
    /*
     * null results as constructor arg
     */
    Map<String, Object> dummyResults = null;

    dummyResultsVO = new ResultsVO(dummyResults);

    assertNotNull(dummyResultsVO.getResults());
    assertSame(0, dummyResultsVO.getResults().size());
    assertFalse(dummyResultsVO.hasResults());
    assertFalse(dummyResultsVO.hasPerReferenceData());
    assertSame(0, dummyResultsVO.retrievePerReferenceData(true).size());
    assertSame(0, dummyResultsVO.retrievePerReferenceData(false).size());
    assertNotNull(dummyResultsVO.toString());

    /*
     * Empty Map as constructor arg.
     */
    dummyResults = new HashMap<String, Object>();

    dummyResultsVO = new ResultsVO(dummyResults);

    assertNotNull(dummyResultsVO.getResults());
    assertSame(0, dummyResultsVO.getResults().size());
    assertFalse(dummyResultsVO.hasResults());
    assertFalse(dummyResultsVO.hasPerReferenceData());
    assertSame(0, dummyResultsVO.retrievePerReferenceData(true).size());
    assertSame(0, dummyResultsVO.retrievePerReferenceData(false).size());

    /*
     * Test results reassignment attempt exception
     */
    try {
      dummyResultsVO.getResults().put("dummyKey", new Object());
      fail("Should not allow modification of the results");
    } catch (UnsupportedOperationException use) {}

    /*
     * Null results reference data.
     */
    dummyResults.clear();
    dummyResults.put(ClientDirectIdentifiers.KEY_RESULTS_REFERENCE_DATA, null);
    dummyResultsVO = new ResultsVO(dummyResults);
    assertNotNull(dummyResultsVO.getResults());
    // 1, because as per assigned!
    assertSame(dummyResults.size(), dummyResultsVO.getResults().size());
    assertTrue(dummyResultsVO.hasResults());
    assertFalse(dummyResultsVO.hasPerReferenceData());
    assertSame(0, dummyResultsVO.retrievePerReferenceData(true).size());
    assertSame(0, dummyResultsVO.retrievePerReferenceData(false).size());

    /*
     * Results reference data containing null key and value.
     */
    final Map<BigDecimal, Map<String, Object>> dummyReferenceData = new HashMap<BigDecimal, Map<String, Object>>();
    dummyReferenceData.put(null, new HashMap<String, Object>());
    dummyReferenceData.put(BigDecimal.TEN, null);
    dummyResults.put(ClientDirectIdentifiers.KEY_RESULTS_REFERENCE_DATA,
                    dummyReferenceData);
    dummyResultsVO = new ResultsVO(dummyResults);
    assertNotNull(dummyResultsVO.getResults());
    // 1, because as per assigned!
    assertSame(dummyResults.size(), dummyResultsVO.getResults().size());
    assertTrue(dummyResultsVO.hasResults());
    assertFalse(dummyResultsVO.hasPerReferenceData());
    assertSame(0, dummyResultsVO.retrievePerReferenceData(true).size());
    assertSame(0, dummyResultsVO.retrievePerReferenceData(false).size());

    /*
     * Emulate null concentration data.
     */
    dummyResults.clear();
    dummyReferenceData.clear();
    dummyResults.put(ClientDirectIdentifiers.KEY_RESULTS_REFERENCE_DATA,
                     dummyReferenceData);
    dummyResultsVO = new ResultsVO(dummyResults);
    assertNotNull(dummyResultsVO.getResults());
    assertSame(1, dummyResultsVO.getResults().size());
    assertTrue(dummyResultsVO.hasResults());
    assertFalse(dummyResultsVO.hasPerReferenceData());
    assertSame(0, dummyResultsVO.retrievePerReferenceData(true).size());
    assertSame(0, dummyResultsVO.retrievePerReferenceData(false).size());

    /*
     * Emulate null per-concentration data.
     */
    dummyResults.clear();
    dummyReferenceData.clear();
    BigDecimal dummyReference = BigDecimal.ONE;
    dummyReferenceData.put(dummyReference, null);
    dummyResults.put(ClientDirectIdentifiers.KEY_RESULTS_REFERENCE_DATA,
                     dummyReferenceData);

    dummyResultsVO = new ResultsVO(dummyResults);
    assertNotNull(dummyResultsVO.getResults());
    assertSame(1, dummyResultsVO.getResults().size());
    assertTrue(dummyResultsVO.hasResults());
    // False, because there wasn't actually any data!
    assertFalse(dummyResultsVO.hasPerReferenceData());
    assertSame(0, dummyResultsVO.retrievePerReferenceData(true).size());
    assertSame(0, dummyResultsVO.retrievePerReferenceData(false).size());

    /*
     * Emulate per-concentration data in reverse concentration order
     */
    dummyResults.clear();
    dummyReferenceData.clear();
    BigDecimal dummyReference1 = BigDecimal.TEN;
    final Map<String, Object> dr1Content = new HashMap<String, Object>();
    final String dummyFR1APD90 = new BigDecimal("3.5").toPlainString();
    final String dummyFR1DeltaAPD90 = new BigDecimal("3.6").toPlainString();
    final String dummyFR1Times = "dummyFR1Times";
    final String dummyFR1Voltages = "dummyFR1Voltages";

    dr1Content.put(ClientDirectIdentifiers.KEY_RESULTS_PERREF_APD90,
                   dummyFR1APD90);
    dr1Content.put(ClientDirectIdentifiers.KEY_RESULTS_PERREF_DELTA_APD90,
                   dummyFR1DeltaAPD90);
    dr1Content.put(ClientDirectIdentifiers.KEY_RESULTS_PERREF_TIMES,
                   dummyFR1Times);
    dr1Content.put(ClientDirectIdentifiers.KEY_RESULTS_PERREF_VOLTAGES,
                   dummyFR1Voltages);

    dummyReferenceData.put(dummyReference1, dr1Content);
    BigDecimal dummyReference2 = BigDecimal.ONE;
    final Map<String, Object> dr2Content = new HashMap<String, Object>();
    final String dummyFR2APD90 = new BigDecimal("5.5").toPlainString();
    final String dummyFR2DeltaAPD90 = new BigDecimal("5.6").toPlainString();
    final String dummyFR2Times = "dummyFR2Times";
    final String dummyFR2Voltages = "dummyFR2Voltages";

    dr2Content.put(ClientDirectIdentifiers.KEY_RESULTS_PERREF_APD90,
                   dummyFR2APD90);
    dr2Content.put(ClientDirectIdentifiers.KEY_RESULTS_PERREF_DELTA_APD90,
                   dummyFR2DeltaAPD90);
    dr2Content.put(ClientDirectIdentifiers.KEY_RESULTS_PERREF_TIMES,
                   dummyFR2Times);
    dr2Content.put(ClientDirectIdentifiers.KEY_RESULTS_PERREF_VOLTAGES,
                   dummyFR2Voltages);
    dummyReferenceData.put(dummyReference2, dr2Content);
    dummyResults.put(ClientDirectIdentifiers.KEY_RESULTS_REFERENCE_DATA,
                    dummyReferenceData);

    dummyResultsVO = new ResultsVO(dummyResults);
    assertNotNull(dummyResultsVO.getResults());
    assertSame(1, dummyResultsVO.getResults().size());
    assertTrue(dummyResultsVO.hasResults());
    assertTrue(dummyResultsVO.hasPerReferenceData());
    List<PerReferenceDataVO> sortedReferences = dummyResultsVO.retrievePerReferenceData(true);
    assertSame(2, sortedReferences.size());
    PerReferenceDataVO firstReference = sortedReferences.get(0);
    assertEquals(dummyReference2, firstReference.getReference());
    assertEquals(dummyFR2APD90, firstReference.retrieveAPD90());
    assertEquals(dummyFR2DeltaAPD90, firstReference.retrieveDeltaAPD90());
    assertEquals(dummyFR2Times, firstReference.retrieveVoltageTraceTimes());
    assertEquals(dummyFR2Voltages, firstReference.retrieveVoltageTraceVoltages());

    List<PerReferenceDataVO> unsortedReferences = dummyResultsVO.retrievePerReferenceData(false);
    assertSame(2, unsortedReferences.size());
    firstReference = unsortedReferences.get(0);
    assertEquals(dummyReference1, firstReference.getReference());
    assertEquals(dummyFR1APD90, firstReference.retrieveAPD90());
    assertEquals(dummyFR1DeltaAPD90, firstReference.retrieveDeltaAPD90());
    assertEquals(dummyFR1Times, firstReference.retrieveVoltageTraceTimes());
    assertEquals(dummyFR1Voltages, firstReference.retrieveVoltageTraceVoltages());
  }
}