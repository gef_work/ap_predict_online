/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.config;

import static org.easymock.EasyMock.createStrictControl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.nc3rs.client_direct.value.IonCurrent;
import uk.ac.ox.cs.nc3rs.client_direct.value.ModelVO;

/**
 * Unit test configuration.
 *
 * @author geoff
 */
public class ConfigurationTest {

  private static final BigDecimal illegalMaxPctileVal = new BigDecimal("100");

  private Configuration configuration;
  private IMocksControl mocksControl;
  private Map<IonCurrent, String> dummySpreads;
  private ModelVO mockModel;
  private Set<BigDecimal> dummyCIPctiles;
  private String dummyDefaultRecommendedPlasmaConcMax = "100";
  private String dummyDefaultPlasmaConcMin = "0";
  private String dummyRecommendedPlasmaConcMax;
  private String dummyPlasmaConcMin;

  @Before
  public void setUp() {
    configuration = new Configuration(dummyDefaultRecommendedPlasmaConcMax,
                                      dummyDefaultPlasmaConcMin,
                                      dummySpreads, dummyCIPctiles);

    mocksControl = createStrictControl();
    mockModel = mocksControl.createMock(ModelVO.class);

    dummyCIPctiles = null;
    dummyRecommendedPlasmaConcMax = null;
    dummySpreads = null;
    dummyPlasmaConcMin = null;
  }

  @Test(expected=IllegalArgumentException.class)
  public void testSetC50UnitsFailsOnEmptyArg() {
    configuration.setC50Units(new HashMap<String, String>());
  }

  @Test(expected=IllegalArgumentException.class)
  public void testSetC50UnitsFailsOnNullArg() {
    configuration.setC50Units(null);
  }

  @Test
  public void testConstructor() {
    try {
      new Configuration(dummyRecommendedPlasmaConcMax, dummyPlasmaConcMin,
                        dummySpreads, dummyCIPctiles);
      fail("Should not permit null plasma concentration assignment");
    } catch (IllegalArgumentException e) {}

    dummyRecommendedPlasmaConcMax = "100";
    dummyPlasmaConcMin = null;
    try {
      new Configuration(dummyRecommendedPlasmaConcMax, dummyPlasmaConcMin,
                        dummySpreads, dummyCIPctiles);
      fail("Should not permit null plasma concentration assignment");
    } catch (IllegalArgumentException e) {}

    dummyRecommendedPlasmaConcMax = "a";
    dummyPlasmaConcMin = "0";
    try {
      new Configuration(dummyRecommendedPlasmaConcMax, dummyPlasmaConcMin,
                        dummySpreads, dummyCIPctiles);
      fail("Should not permit non-numeric plasma concentration assignment");
    } catch (IllegalArgumentException e) {}

    dummyRecommendedPlasmaConcMax = "100";
    dummyPlasmaConcMin = "a";
    try {
      new Configuration(dummyRecommendedPlasmaConcMax, dummyPlasmaConcMin,
                        dummySpreads, dummyCIPctiles);
      fail("Should not permit non-numeric plasma concentration assignment");
    } catch (IllegalArgumentException e) {}

    dummyRecommendedPlasmaConcMax = "-1";
    dummyPlasmaConcMin = "0";
    try {
      new Configuration(dummyRecommendedPlasmaConcMax, dummyPlasmaConcMin,
                        dummySpreads, dummyCIPctiles);
      fail("Should not permit a negative maximum plasma concentration assignment");
    } catch (IllegalArgumentException e) {}

    dummyRecommendedPlasmaConcMax = "0";
    try {
      new Configuration(dummyRecommendedPlasmaConcMax, dummyPlasmaConcMin,
                        dummySpreads, dummyCIPctiles);
      fail("Should not permit a zero maximum plasma concentration assignment");
    } catch (IllegalArgumentException e) {}

    dummyRecommendedPlasmaConcMax = "5";
    dummyPlasmaConcMin = "10";
    try {
      new Configuration(dummyRecommendedPlasmaConcMax, dummyPlasmaConcMin,
                        dummySpreads, dummyCIPctiles);
      fail("Should not permit a minimum greater than a maximum plasma concentration assignment");
    } catch (IllegalArgumentException e) {}

    dummyRecommendedPlasmaConcMax = "10";
    dummyPlasmaConcMin = "10";
    try {
      new Configuration(dummyRecommendedPlasmaConcMax, dummyPlasmaConcMin,
                        dummySpreads, dummyCIPctiles);
      fail("Should not permit a minimum equal to a maximum plasma concentration assignment");
    } catch (IllegalArgumentException e) {}

    dummyRecommendedPlasmaConcMax = "10";
    dummyPlasmaConcMin = "0";

    new Configuration(dummyRecommendedPlasmaConcMax, dummyPlasmaConcMin,
                      dummySpreads, dummyCIPctiles);
    assertEquals(dummyRecommendedPlasmaConcMax,
                 Configuration.RECOMMENDED_PLASMA_CONC_MAX.toString());
    assertEquals(dummyPlasmaConcMin, Configuration.PLASMA_CONC_MIN.toString());
  }

  @Test
  public void testConstructorSpreadAssignment() {
    /*
     * 1. Null spreads arg
     */
    configuration = new Configuration(Configuration.RECOMMENDED_PLASMA_CONC_MAX.toString(),
                                      Configuration.PLASMA_CONC_MIN.toString(),
                                      dummySpreads, dummyCIPctiles);
    assertNotNull(configuration.getSpreads());
    assertEquals(0, configuration.getSpreads().size());

    // Try to modify retrieved collection
    try {
      configuration.getSpreads().put(IonCurrent.IKs, BigDecimal.ONE);
      fail("Should not allow modification of getter spread collection!");
    } catch (UnsupportedOperationException e) {}

    /*
     * 2. Empty spreads arg.
     */
    dummySpreads = new HashMap<IonCurrent, String>();

    configuration = new Configuration(Configuration.RECOMMENDED_PLASMA_CONC_MAX.toString(),
                                      Configuration.PLASMA_CONC_MIN.toString(),
                                      dummySpreads, dummyCIPctiles);
    assertEquals(0, configuration.getSpreads().size());
    assertTrue(configuration.getCredibleIntervalPctiles().isEmpty());

    /*
     * 3. Croak if spread entry (even a null-keyed one!) without any pctiles.
     */
    dummySpreads.clear();
    IonCurrent dummyIonCurrent1 = null;
    String dummySpread1 = BigDecimal.ONE.toString();
    dummySpreads.put(dummyIonCurrent1, dummySpread1);

    try {
      configuration = new Configuration(Configuration.RECOMMENDED_PLASMA_CONC_MAX.toString(),
                                        Configuration.PLASMA_CONC_MIN.toString(),
                                        dummySpreads, dummyCIPctiles);
      fail("Should fail if spread arg is supplied but no credible interval pctiles supplied!");
    } catch (IllegalArgumentException e) {}

    /*
     * 4. Test pctile value checking.
     */
    dummyCIPctiles = new HashSet<BigDecimal>();

    try {
      new Configuration(Configuration.RECOMMENDED_PLASMA_CONC_MAX.toString(),
                        Configuration.PLASMA_CONC_MIN.toString(),
                        dummySpreads, dummyCIPctiles);
      fail("Should not accept empty pctiles collection!");
    } catch (IllegalArgumentException e) {
      assertTrue(e.getMessage().contains("must be specified"));
    }

    dummyCIPctiles.add(null);

    try {
      new Configuration(Configuration.RECOMMENDED_PLASMA_CONC_MAX.toString(),
                        Configuration.PLASMA_CONC_MIN.toString(),
                        dummySpreads, dummyCIPctiles);
      fail("Should not accept null pctile value!");
    } catch (IllegalArgumentException e) {
      assertTrue(e.getMessage().contains("is not valid"));
    }

    dummyCIPctiles.clear();
    BigDecimal dummyCIPctile = BigDecimal.ZERO.subtract(BigDecimal.ONE);
    dummyCIPctiles.add(dummyCIPctile);

    try {
      new Configuration(Configuration.RECOMMENDED_PLASMA_CONC_MAX.toString(),
                        Configuration.PLASMA_CONC_MIN.toString(),
                        dummySpreads, dummyCIPctiles);
      fail("Should not accept -ve pctile!");
    } catch (IllegalArgumentException e) {
      assertTrue(e.getMessage().contains("is not valid"));
    }

    dummyCIPctiles.clear();
    dummyCIPctile = BigDecimal.ZERO;
    dummyCIPctiles.add(dummyCIPctile);

    try {
      new Configuration(Configuration.RECOMMENDED_PLASMA_CONC_MAX.toString(),
                        Configuration.PLASMA_CONC_MIN.toString(),
                        dummySpreads, dummyCIPctiles);
      fail("Should not accept 0 pctile!");
    } catch (IllegalArgumentException e) {
      assertTrue(e.getMessage().contains("is not valid"));
    }

    dummyCIPctiles.clear();
    dummyCIPctile = illegalMaxPctileVal;
    dummyCIPctiles.add(dummyCIPctile);

    try {
      new Configuration(Configuration.RECOMMENDED_PLASMA_CONC_MAX.toString(),
                        Configuration.PLASMA_CONC_MIN.toString(),
                        dummySpreads, dummyCIPctiles);
      fail("Should not accept illegal pctile value of '" + illegalMaxPctileVal + "'.!");
    } catch (IllegalArgumentException e) {
      assertTrue(e.getMessage().contains("is not valid"));
    }

    dummyCIPctiles.clear();
    dummyCIPctile = illegalMaxPctileVal.add(BigDecimal.ONE);
    dummyCIPctiles.add(dummyCIPctile);

    try {
      new Configuration(Configuration.RECOMMENDED_PLASMA_CONC_MAX.toString(),
                        Configuration.PLASMA_CONC_MIN.toString(),
                        dummySpreads, dummyCIPctiles);
      fail("Should not accept illegal pctile value exceeding '" + illegalMaxPctileVal + "'.!");
    } catch (IllegalArgumentException e) {
      assertTrue(e.getMessage().contains("is not valid"));
    }

    /*
     * 5. Try again with spread entry (even a null-keyed one!) but with pctiles.
     */
    dummyCIPctiles.clear();
    dummyCIPctile = illegalMaxPctileVal.subtract(BigDecimal.ONE);
    dummyCIPctiles.add(dummyCIPctile);
    configuration = new Configuration(Configuration.RECOMMENDED_PLASMA_CONC_MAX.toString(),
                                      Configuration.PLASMA_CONC_MIN.toString(),
                                      dummySpreads, dummyCIPctiles);
    assertEquals(0, configuration.getSpreads().size());
    assertSame(1, configuration.getCredibleIntervalPctiles().size());
    try {
      configuration.getCredibleIntervalPctiles().clear();
      fail("Should not permit modification of credible interval pctile collection!");
    } catch (UnsupportedOperationException e) {}

    /*
     * Try adding an blank-valued spread entry
     */
    dummySpreads.clear();
    dummyIonCurrent1 = IonCurrent.IKr;
    dummySpread1 = "";
    dummySpreads.put(dummyIonCurrent1, dummySpread1);

    configuration = new Configuration(Configuration.RECOMMENDED_PLASMA_CONC_MAX.toString(),
                                      Configuration.PLASMA_CONC_MIN.toString(),
                                      dummySpreads, dummyCIPctiles);
    assertEquals(1, configuration.getSpreads().size());

    /*
     * Try adding an illegal-valued spread entry
     */
    dummySpreads.clear();
    dummyIonCurrent1 = IonCurrent.IKr;
    dummySpread1 = "dummySpread1";
    dummySpreads.put(dummyIonCurrent1, dummySpread1);

    configuration = new Configuration(Configuration.RECOMMENDED_PLASMA_CONC_MAX.toString(),
                                      Configuration.PLASMA_CONC_MIN.toString(),
                                      dummySpreads, dummyCIPctiles);
    assertEquals(1, configuration.getSpreads().size());

    /*
     * Try adding valid spread entries.
     */
    dummySpreads.clear();
    dummyIonCurrent1 = IonCurrent.IKr;
    dummySpread1 = BigDecimal.ONE.toString();
    final IonCurrent dummyIonCurrent2 = IonCurrent.ICaL;
    final String dummySpread2 = BigDecimal.TEN.toString();
    dummySpreads.put(dummyIonCurrent1, dummySpread1);
    dummySpreads.put(dummyIonCurrent2, dummySpread2);

    configuration = new Configuration(Configuration.RECOMMENDED_PLASMA_CONC_MAX.toString(),
                                      Configuration.PLASMA_CONC_MIN.toString(),
                                      dummySpreads, dummyCIPctiles);

    final Map<IonCurrent, BigDecimal> retrieved = configuration.getSpreads();
    assertEquals(2, retrieved.size());
    assertEquals(dummySpread1, retrieved.get(dummyIonCurrent1).toString());
    assertEquals(dummySpread2, retrieved.get(dummyIonCurrent2).toString());
  }

  @Test
  public void testGetSetC50Units() {
    final String fakeKey1 = "fakeKey1";
    final String fakeValue1 = "fakeValue1";

    final Map<String, String> dummyC50Units = new HashMap<String, String>();
    dummyC50Units.put(fakeKey1, fakeValue1);
    configuration.setC50Units(dummyC50Units);

    final Map<String, String> assignedC50Units = configuration.getC50Units();

    assertSame(1, assignedC50Units.size());
    assertEquals(fakeValue1, assignedC50Units.get(fakeKey1));
    try {
      assignedC50Units.clear();
      fail("Should permit modification of C50 units");
    } catch (UnsupportedOperationException e) {}

    final String fakeKey2 = "fakeKey2";
    final String fakeValue2 = "fakeValue2";
    final Map<String, String> dummyC50Units2 = new HashMap<String, String>();
    dummyC50Units2.put(fakeKey2, fakeValue2);
    try {
      configuration.setC50Units(dummyC50Units2);
      fail("Should not permit reassignment of C50 units");
    } catch (IllegalArgumentException e) {}
  }

  @Test(expected=IllegalArgumentException.class)
  public void testSettingModelsFailsIfNonSupplied() {
    configuration.setModels(new HashSet<ModelVO>());
  }

  @Test
  public void testGetSetModelsSuccess() {
    final Set<ModelVO> fakeModels = new HashSet<ModelVO>();
    fakeModels.add(mockModel);

    mocksControl.replay();

    configuration.setModels(fakeModels);

    mocksControl.verify();

    final List<ModelVO> returnedModels = configuration.getModels();
    assertSame(1, returnedModels.size());
    assertSame(0, configuration.getModelNames().size());

    mocksControl.reset();

    mocksControl.replay();

    try {
      returnedModels.add(mockModel);
      fail("Should not allow modification of the Models once configured");
    } catch (UnsupportedOperationException use) {}
  }
}