/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.io.IOException;
import java.io.InputStream;

import org.easymock.Capture;
import org.junit.Test;
import org.springframework.web.multipart.MultipartFile;

import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.compbio.client_shared.value.file.FILE_DATA_TYPE;
import uk.ac.ox.cs.compbio.client_shared.value.file.FileStoreActionOutcomeVO;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.value.InputOption;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;

/**
 * Unit test {@link Results} controller's input processing of CellML files.
 *
 * @author geoff
 */
public class ResultsProcessInputCellMLFileTest extends AbstractResultsProcessInputTest {

  @Test
  public void testSimulationInputAssignment() throws Exception {
    /*
     * Dynamic CellML not allowed yet multipartFile supplied
     */
    dummyPkOrConcs = InputOption.CONC_POINTS.toString();
    mockMultipartCellMLFile = mocksControl.createMock(MultipartFile.class);
    defaultPortalFeatureProcessing();

    /* Controller assigns null to mockMultipartCellMLFile - mockCellMLFile
       on SimulationInputVO creation is null! */
    SimulationInputVO mockSimulationInputVO = assignSimulationInputProperties(mockPKFile,
                                                                              mockCellMLFile,
                                                                              false);

    latterTesting(mockSimulationInputVO);

    replayAll();
    mocksControl.replay();

    String returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_RESULTS, returnPage);

    resetAll();
    mocksControl.reset();

    /*
     * Dynamic CellML allowed - MultipartFile supplied but processing not 
     * successful.  
     */
    dummyAllowDynamicCellML = true;
    defaultPortalFeatureProcessing();

    // >> processMultipart()
    expect(mockMultipartCellMLFile.isEmpty()).andReturn(false);
    final String dummyIOException = "dummyIOException";
    expect(mockMultipartCellMLFile.getInputStream())
          .andThrow(new IOException(dummyIOException));
    Capture<PortalFile> capturePortalFile = newCapture();
    Capture<String> captureInformation = newCapture();
    FileStoreActionOutcomeVO mockOutcome = mocksControl.createMock(FileStoreActionOutcomeVO.class);
    expectNew(FileStoreActionOutcomeVO.class, capture(capturePortalFile),
                                              eq(false),
                                              capture(captureInformation))
             .andReturn(mockOutcome);
    // << processMultipart()
    expect(mockOutcome.isSuccess()).andReturn(false);
    String dummyInformation = "dummyInformation";
    expect(mockOutcome.getInformation()).andReturn(dummyInformation);

    dummyModelIdentifier = null;                                                // Nullified by controller
    mockSimulationInputVO = assignSimulationInputProperties(mockPKFile,
                                                            mockCellMLFile,
                                                            false);

    expect(mockHttpSession.getAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN))
          .andReturn(dummyModelAssignedToken).times(2);
    mockHttpSession.removeAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN);
    expect(mockSimulationInputVO.hasAssignmentProblems()).andReturn(false);
    mockStatic(Process.class);
    Process.setPropertiesForInput(mockClientDirectService, mockSimulationInputVO,
                                  mockHttpSession, dummyInformation,
                                  mockModel);

    replayAll();
    mocksControl.replay();

    returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    PortalFile capturedPortalFile = capturePortalFile.getValue();
    String capturedInformation = captureInformation.getValue();

    assertNull(capturedPortalFile);
    assertEquals("Application Error : Error handling the input stream from CELLML file!",
                 capturedInformation);
    assertEquals(ClientDirectIdentifiers.PAGE_INPUT, returnPage);

    resetAll();
    mocksControl.reset();

    /*
     * Dynamic CellML allowed - MultipartFile supplied and processing 
     * successful.  
     */
    dummyAllowDynamicCellML = true;
    defaultPortalFeatureProcessing();

    // >> processMultipart()
    expect(mockMultipartCellMLFile.isEmpty()).andReturn(false);
    final InputStream mockInputStream = mocksControl.createMock(InputStream.class);
    expect(mockMultipartCellMLFile.getInputStream())
          .andReturn(mockInputStream);
    final String dummyOriginalFileName = "dummyOriginalFilename";
    expect(mockMultipartCellMLFile.getOriginalFilename())
          .andReturn(dummyOriginalFileName);
    final String dummyUploader = "dummyUploader";
    expect(mockPrincipal.getName()).andReturn(dummyUploader);
    Capture<FILE_DATA_TYPE> captureFileDataType = newCapture();
    Capture<InputStream> captureInputStream = newCapture();
    Capture<String> captureOriginalFilename = newCapture();
    Capture<String> captureUploader = newCapture();
    expect(mockClientDirectService.storeFile(capture(captureFileDataType),
                                             capture(captureInputStream),
                                             capture(captureOriginalFilename),
                                             capture(captureUploader)))
          .andReturn(mockOutcome);
    mockInputStream.close();
    // << processMultipart()

    expect(mockOutcome.isSuccess()).andReturn(true);
    expect(mockOutcome.getPortalFile()).andReturn(mockCellMLFile);

    mockSimulationInputVO = assignSimulationInputProperties(mockPKFile,
                                                            mockCellMLFile,
                                                            false);

    latterTesting(mockSimulationInputVO);

    replayAll();
    mocksControl.replay();

    returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertTrue(FILE_DATA_TYPE.CELLML.compareTo(captureFileDataType.getValue()) == 0);
    assertSame(mockInputStream, captureInputStream.getValue());
    assertEquals(dummyOriginalFileName, captureOriginalFilename.getValue());
    assertEquals(dummyUploader, captureUploader.getValue());
    assertEquals(ClientDirectIdentifiers.PAGE_RESULTS, returnPage);
  }
}