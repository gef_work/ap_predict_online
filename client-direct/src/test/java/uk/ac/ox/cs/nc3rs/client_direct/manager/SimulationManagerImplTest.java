/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the names of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.manager;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityNotFoundException;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.dao.SimulationDAO;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Job;
import uk.ac.ox.cs.nc3rs.client_direct.entity.JobPKResult;
import uk.ac.ox.cs.nc3rs.client_direct.entity.JobResult;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation;
import uk.ac.ox.cs.nc3rs.client_direct.entity.SimulationInput;
import uk.ac.ox.cs.nc3rs.client_direct.exception.DataRetrievalException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.SimulationNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.value.C50_TYPE;
import uk.ac.ox.cs.nc3rs.client_direct.value.MeasurementObservationsVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.IC50_UNIT;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;

/**
 * Unit test the simualtion manager implementation.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { SimulationManagerImpl.class } )
public class SimulationManagerImplTest {

  private BigDecimal dummyPacingFrequency;
  private BigDecimal dummyPacingMaxTime;
  private BigDecimal dummyPIC50ICaL, dummyHillICaL, dummySaturationICaL,
                     dummySpreadICaL;
  private BigDecimal dummyPIC50IK1, dummyHillIK1, dummySaturationIK1,
                     dummySpreadIK1;
  private BigDecimal dummyPIC50IKr, dummyHillIKr, dummySaturationIKr,
                     dummySpreadIKr;
  private BigDecimal dummyPIC50IKs, dummyHillIKs, dummySaturationIKs,
                     dummySpreadIKs;
  private BigDecimal dummyPIC50INa, dummyHillINa, dummySaturationINa,
                     dummySpreadINa;
  private BigDecimal dummyPIC50INaL, dummyHillINaL, dummySaturationINaL,
                     dummySpreadINaL;
  private BigDecimal dummyPIC50Ito, dummyHillIto, dummySaturationIto,
                     dummySpreadIto;
  private BigDecimal dummyPlasmaConcMax;
  private BigDecimal dummyPlasmaConcMin;
  private boolean dummyPlasmaIntPtLogScale;
  private C50_TYPE dummyC50Type;
  private MeasurementObservationsVO mockCompoundObservationsICaL;
  private MeasurementObservationsVO mockCompoundObservationsIK1;
  private MeasurementObservationsVO mockCompoundObservationsIKr;
  private MeasurementObservationsVO mockCompoundObservationsIKs;
  private MeasurementObservationsVO mockCompoundObservationsINa;
  private MeasurementObservationsVO mockCompoundObservationsINaL;
  private MeasurementObservationsVO mockCompoundObservationsIto;
  private IC50_UNIT dummyIC50Unit;
  private IMocksControl mocksControl;
  private long dummySimulationId;
  private PortalFile dummyPKFile;
  private Simulation mockSimulation;
  private SimulationDAO mockSimulationDAO;
  private SimulationInput mockSimulationInput;
  private Short dummyPlasmaIntPtCount;
  private SimulationInputVO mockSimulationInputVO;
  private short dummyModelIdentifier;
  private String dummyCellMLFileName;
  private String dummyCIPctiles;
  private String dummyNotes;
  private List<String> dummyPlasmaConcPoints;

  private SimulationManager simulationManager;

  @Before
  public void setUp() {
    // Using a non-strict control because don't want to follow assignment order within simulation manager
    mocksControl = createStrictControl();
    mockSimulation = mocksControl.createMock(Simulation.class);
    mockSimulationDAO = mocksControl.createMock(SimulationDAO.class);
    mockSimulationInput = mocksControl.createMock(SimulationInput.class);
    mockSimulationInputVO = mocksControl.createMock(SimulationInputVO.class);
    mockCompoundObservationsICaL = mocksControl.createMock(MeasurementObservationsVO.class);
    mockCompoundObservationsIK1 = mocksControl.createMock(MeasurementObservationsVO.class);
    mockCompoundObservationsIKr = mocksControl.createMock(MeasurementObservationsVO.class);
    mockCompoundObservationsIKs = mocksControl.createMock(MeasurementObservationsVO.class);
    mockCompoundObservationsINa = mocksControl.createMock(MeasurementObservationsVO.class);
    mockCompoundObservationsINaL = mocksControl.createMock(MeasurementObservationsVO.class);
    mockCompoundObservationsIto = mocksControl.createMock(MeasurementObservationsVO.class);

    simulationManager = new SimulationManagerImpl();

    ReflectionTestUtils.setField(simulationManager, ClientDirectIdentifiers.COMPONENT_SIMULATION_DAO,
                                 mockSimulationDAO);

    dummySimulationId = 100l;
    dummyModelIdentifier = (short) 10;
    dummyC50Type = C50_TYPE.IC50;
    dummyIC50Unit = IC50_UNIT.µM;
    dummyPacingFrequency = new BigDecimal("0.5");
    dummyPacingMaxTime = new BigDecimal("2.0");
    dummyPIC50ICaL = new BigDecimal("4.5");
    dummyHillICaL = new BigDecimal("0.45");
    dummySaturationICaL = new BigDecimal("45.0");
    dummySpreadICaL = new BigDecimal("0.12");
    dummyPIC50IK1 = new BigDecimal("4.6");
    dummyHillIK1 = new BigDecimal("0.46");
    dummySaturationIK1 = new BigDecimal("46.0");
    dummySpreadIK1 = new BigDecimal("0.23");
    dummyPIC50IKr = new BigDecimal("4.7");
    dummyHillIKr = new BigDecimal("0.47");
    dummySaturationIKr = new BigDecimal("47.0");
    dummySpreadIKr = new BigDecimal("0.34");
    dummyPIC50IKs = new BigDecimal("4.8");
    dummyHillIKs = new BigDecimal("0.48");
    dummySaturationIKs = new BigDecimal("48.0");
    dummySpreadIKs = new BigDecimal("0.45");
    dummyPIC50INa = new BigDecimal("4.9");
    dummyHillINa = new BigDecimal("0.49");
    dummySaturationINa = new BigDecimal("49.0");
    dummySpreadINa = new BigDecimal("0.56");
    dummyPIC50INaL = new BigDecimal("4.91");
    dummyHillINaL = new BigDecimal("0.491");
    dummySaturationINaL = new BigDecimal("49.01");
    dummySpreadINaL = new BigDecimal("0.561");
    dummyPIC50Ito = new BigDecimal("5.0");
    dummyHillIto = new BigDecimal("0.5");
    dummySaturationIto = new BigDecimal("50.0");
    dummySpreadIto = new BigDecimal("0.67");
    dummyPlasmaIntPtCount = Short.valueOf("6");
    dummyPlasmaConcMax = new BigDecimal("100");
    dummyPlasmaConcMin = new BigDecimal("10");
    dummyPlasmaIntPtLogScale = true;
    dummyPlasmaConcPoints = new ArrayList<String>(
                                Arrays.asList(new String[] { "a", "b" } ));
    dummyPKFile = null;
    dummyCellMLFileName = "dummyCellMLFileName";
    dummyNotes = "dummyNotes";
  }

  // Using createStrictControl, ensures that SimulationInput object constructor args are called.
  @Test
  public void testCreateSimulation() throws Exception {
    final long dummyAppManagerId = 1L;
    final long dummySimulationId = 2L;
    final String dummyCreator = "dummyCreator";

    expect(mockSimulationInputVO.getICaLObservations())
          .andReturn(mockCompoundObservationsICaL);
    expect(mockSimulationInputVO.getIK1Observations())
          .andReturn(mockCompoundObservationsIK1);
    expect(mockSimulationInputVO.getIKrObservations())
          .andReturn(mockCompoundObservationsIKr);
    expect(mockSimulationInputVO.getIKsObservations())
          .andReturn(mockCompoundObservationsIKs);
    expect(mockSimulationInputVO.getINaObservations())
          .andReturn(mockCompoundObservationsINa);
    expect(mockSimulationInputVO.getINaLObservations())
          .andReturn(mockCompoundObservationsINaL);
    expect(mockSimulationInputVO.getItoObservations())
          .andReturn(mockCompoundObservationsIto);
    expect(mockCompoundObservationsICaL.getC50()).andReturn(dummyPIC50ICaL);
    expect(mockCompoundObservationsICaL.getHill()).andReturn(dummyHillICaL);
    expect(mockCompoundObservationsICaL.getSaturation())
          .andReturn(dummySaturationICaL);
    expect(mockCompoundObservationsICaL.getSpread()).andReturn(dummySpreadICaL);
    expect(mockCompoundObservationsIK1.getC50()).andReturn(dummyPIC50IK1);
    expect(mockCompoundObservationsIK1.getHill()).andReturn(dummyHillIK1);
    expect(mockCompoundObservationsIK1.getSaturation())
          .andReturn(dummySaturationIK1);
    expect(mockCompoundObservationsIK1.getSpread()).andReturn(dummySpreadIK1);
    expect(mockCompoundObservationsIKr.getC50()).andReturn(dummyPIC50IKr);
    expect(mockCompoundObservationsIKr.getHill()).andReturn(dummyHillIKr);
    expect(mockCompoundObservationsIKr.getSaturation())
          .andReturn(dummySaturationIKr);
    expect(mockCompoundObservationsIKr.getSpread()).andReturn(dummySpreadIKr);
    expect(mockCompoundObservationsIKs.getC50()).andReturn(dummyPIC50IKs);
    expect(mockCompoundObservationsIKs.getHill()).andReturn(dummyHillIKs);
    expect(mockCompoundObservationsIKs.getSaturation())
          .andReturn(dummySaturationIKs);
    expect(mockCompoundObservationsIKs.getSpread()).andReturn(dummySpreadIKs);
    expect(mockCompoundObservationsINa.getC50()).andReturn(dummyPIC50INa);
    expect(mockCompoundObservationsINa.getHill()).andReturn(dummyHillINa);
    expect(mockCompoundObservationsINa.getSaturation())
          .andReturn(dummySaturationINa);
    expect(mockCompoundObservationsINa.getSpread()).andReturn(dummySpreadINa);
    expect(mockCompoundObservationsINaL.getC50()).andReturn(dummyPIC50INaL);
    expect(mockCompoundObservationsINaL.getHill()).andReturn(dummyHillINaL);
    expect(mockCompoundObservationsINaL.getSaturation())
          .andReturn(dummySaturationINaL);
    expect(mockCompoundObservationsINaL.getSpread()).andReturn(dummySpreadINaL);
    expect(mockCompoundObservationsIto.getC50()).andReturn(dummyPIC50Ito);
    expect(mockCompoundObservationsIto.getHill()).andReturn(dummyHillIto);
    expect(mockCompoundObservationsIto.getSaturation())
          .andReturn(dummySaturationIto);
    expect(mockCompoundObservationsIto.getSpread()).andReturn(dummySpreadIto);
    expect(mockSimulationInputVO.getC50Type()).andReturn(dummyC50Type);
    expect(mockSimulationInputVO.getIc50Units()).andReturn(dummyIC50Unit);
    expect(mockSimulationInputVO.getModelIdentifier())
          .andReturn(dummyModelIdentifier);
    expect(mockSimulationInputVO.getPacingMaxTime())
          .andReturn(dummyPacingMaxTime);
    expect(mockSimulationInputVO.getPacingFrequency())
          .andReturn(dummyPacingFrequency);
    expect(mockSimulationInputVO.getPlasmaConcMax())
          .andReturn(dummyPlasmaConcMax);
    expect(mockSimulationInputVO.getPlasmaConcMin())
          .andReturn(dummyPlasmaConcMin);
    expect(mockSimulationInputVO.getPlasmaIntPtCount())
          .andReturn(dummyPlasmaIntPtCount);
    expect(mockSimulationInputVO.getPlasmaIntPtLogScale())
          .andReturn(dummyPlasmaIntPtLogScale);
    expect(mockSimulationInputVO.getPlasmaConcPoints())
          .andReturn(dummyPlasmaConcPoints);
    expect(mockSimulationInputVO.getPkFile()).andReturn(dummyPKFile);
    expect(mockSimulationInputVO.getCellMLFileName())
          .andReturn(dummyCellMLFileName);
    expect(mockSimulationInputVO.getCredibleIntervalPctiles())
          .andReturn(dummyCIPctiles);
    expect(mockSimulationInputVO.getNotes()).andReturn(dummyNotes);

    expectNew(SimulationInput.class, dummyPIC50ICaL, dummyHillICaL,
                                     dummySaturationICaL, dummySpreadICaL,
                                     dummyPIC50IK1, dummyHillIK1,
                                     dummySaturationIK1, dummySpreadIK1,
                                     dummyPIC50IKr, dummyHillIKr,
                                     dummySaturationIKr, dummySpreadIKr, 
                                     dummyPIC50IKs, dummyHillIKs,
                                     dummySaturationIKs, dummySpreadIKs,
                                     dummyPIC50INa, dummyHillINa,
                                     dummySaturationINa, dummySpreadINa,
                                     dummyPIC50INaL, dummyHillINaL,
                                     dummySaturationINaL, dummySpreadINaL,
                                     dummyPIC50Ito, dummyHillIto,
                                     dummySaturationIto, dummySpreadIto,
                                     dummyC50Type, dummyIC50Unit,
                                     dummyModelIdentifier, dummyPacingMaxTime,
                                     dummyPacingFrequency, dummyPlasmaConcMax,
                                     dummyPlasmaConcMin, dummyPlasmaIntPtCount,
                                     dummyPlasmaIntPtLogScale, "a b",
                                     dummyPKFile, dummyCellMLFileName,
                                     dummyCIPctiles, dummyNotes)
             .andReturn(mockSimulationInput);

    final Job mockJob = mocksControl.createMock(Job.class);
    expectNew(Job.class, dummyAppManagerId).andReturn(mockJob);
    expectNew(Simulation.class, mockJob, dummyCreator, mockSimulationInput)
             .andReturn(mockSimulation);

    expect(mockSimulationDAO.store(mockSimulation)).andReturn(mockSimulation);
    expect(mockSimulation.getId()).andReturn(dummySimulationId);

    replayAll();
    mocksControl.replay();

    final long returnedSimulationId = simulationManager.createSimulation(dummyAppManagerId,
                                                                         dummyCreator,
                                                                         mockSimulationInputVO);

    verifyAll();
    mocksControl.verify();

    assertSame(dummySimulationId, returnedSimulationId);
  }

  @Test
  public void testDeleteSimulation() {
    mockSimulationDAO.deleteSimulation(dummySimulationId);

    mocksControl.replay();

    simulationManager.deleteSimulation(dummySimulationId);

    mocksControl.verify();
  }

  @Test
  public void testRecordResults() {
    final Job mockAppManagerJob = mocksControl.createMock(Job.class);
    final long dummyAppManagerId = 4l;

    expect(mockAppManagerJob.getAppManagerId())
          .andReturn(dummyAppManagerId);
    expect(mockSimulationDAO.findBySimulationId(dummySimulationId))
          .andReturn(mockSimulation);
    final Job mockSimulationJob = mocksControl.createMock(Job.class);
    expect(mockSimulation.getJob()).andReturn(mockSimulationJob);
    final String dummyMessages = "dummyMessages";
    expect(mockAppManagerJob.getMessages()).andReturn(dummyMessages);
    mockSimulationJob.setMessages(dummyMessages);
    final String dummyDeltaAPD90PctileNames = "dummyDeltaAPD90PctileNames";
    expect(mockAppManagerJob.getDeltaAPD90PercentileNames())
          .andReturn(dummyDeltaAPD90PctileNames);
    mockSimulationJob.setDeltaAPD90PercentileNames(dummyDeltaAPD90PctileNames);

    final Set<JobResult> dummyJobResults = new HashSet<JobResult>();
    final JobResult mockJobResult = mocksControl.createMock(JobResult.class);
    dummyJobResults.add(mockJobResult);
    expect(mockAppManagerJob.retrieveJobResultCopies())
          .andReturn(dummyJobResults);
    mockJobResult.setJob(mockSimulationJob);

    final Set<JobPKResult> dummyJobPKResults = new HashSet<JobPKResult>();
    final JobPKResult mockJobPKResult = mocksControl.createMock(JobPKResult.class);
    dummyJobPKResults.add(mockJobPKResult);
    expect(mockAppManagerJob.retrieveJobPKResultCopies())
          .andReturn(dummyJobPKResults);
    mockJobPKResult.setJob(mockSimulationJob);

    expect(mockSimulationDAO.storeResults(mockSimulationJob)).andReturn(mockAppManagerJob);

    mocksControl.replay();

    final Job returnedJob = simulationManager.recordResults(dummySimulationId,
                                                            mockAppManagerJob);

    mocksControl.verify();
    assertSame(mockAppManagerJob, returnedJob);
  }

  @Test
  public void testRetrieveAllSimulations() {
    expect(mockSimulationDAO.findSimulations())
          .andReturn(new ArrayList<Simulation>());

    mocksControl.replay();

    final List<Simulation> returnedSimulations = simulationManager.retrieveAllSimulations();

    mocksControl.verify();
    assertNotNull(returnedSimulations);
    assertSame(0, returnedSimulations.size());
  }

  @Test
  public void testRetrieveResults() {
    expect(mockSimulationDAO.findBySimulationId(dummySimulationId))
          .andReturn(mockSimulation);
    final Job mockJob = mocksControl.createMock(Job.class);
    expect(mockSimulation.getJob()).andReturn(mockJob);
    mockSimulationDAO.avoidJobLazyInit(mockJob);

    mocksControl.replay();

    final Job returnedJob = simulationManager.retrieveResults(dummySimulationId);

    mocksControl.verify();
    assertSame(mockJob, returnedJob);
  }

  @Test
  public void testRetrieveSimulationSucceedsOnSimulationFind() 
              throws SimulationNotFoundException {
    /*
     * Test no PK file.
     */
    final long fakeSimulationId = 1L;
    expect(mockSimulationDAO.findBySimulationId(fakeSimulationId))
          .andReturn(mockSimulation);
    final SimulationInput mockSimulationInput = mocksControl.createMock(SimulationInput.class);
    expect(mockSimulation.getSimulationInput()).andReturn(mockSimulationInput);
    expect(mockSimulationInput.getPkFile()).andReturn(null);

    mocksControl.replay();

    Simulation returnedSimulation = simulationManager.retrieveSimulation(fakeSimulationId);

    mocksControl.verify();

    assertSame(mockSimulation, returnedSimulation);

    mocksControl.reset();

    /*
     * Test PK file.
     */
    expect(mockSimulationDAO.findBySimulationId(fakeSimulationId))
          .andReturn(mockSimulation);
    expect(mockSimulation.getSimulationInput()).andReturn(mockSimulationInput);
    final PortalFile mockPKFile = mocksControl.createMock(PortalFile.class);
    expect(mockSimulationInput.getPkFile()).andReturn(mockPKFile);
    expect(mockPKFile.getContent()).andReturn(null);

    mocksControl.replay();

    returnedSimulation = simulationManager.retrieveSimulation(fakeSimulationId);

    mocksControl.verify();

    assertSame(mockSimulation, returnedSimulation);
  }

  @Test(expected=SimulationNotFoundException.class)
  public void testRetrieveSimulationFailureOnNonexistentSimulationId()
              throws SimulationNotFoundException {
    final long fakeSimulationId = 1L;
    expect(mockSimulationDAO.findBySimulationId(fakeSimulationId))
          .andReturn(null);

    mocksControl.replay();

    simulationManager.retrieveSimulation(fakeSimulationId);

    mocksControl.verify();
  }

  @Test
  public void testRetrieveUserSimulations() throws DataRetrievalException {
    /*
     * Simulations found.
     */
    final String fakeUser = "user";
    final List<Simulation> dummyUserSimulations = new ArrayList<Simulation>();
    final Simulation mockSimulation = mocksControl.createMock(Simulation.class);
    dummyUserSimulations.add(mockSimulation);
    expect(mockSimulationDAO.findByUser(fakeUser))
          .andReturn(dummyUserSimulations);

    mocksControl.replay();

    List<Simulation> returnedSimulations = simulationManager.retrieveUserSimulations(fakeUser);

    mocksControl.verify();

    assertEquals(dummyUserSimulations, returnedSimulations);

    mocksControl.reset();

    /*
     * Data retrieval exception.
     */
    final String dummyEntityNotFoundException = "dummyEntityNotFoundException";
    expect(mockSimulationDAO.findByUser(fakeUser))
          .andThrow(new EntityNotFoundException(dummyEntityNotFoundException));

    mocksControl.replay();

    try {
      returnedSimulations = simulationManager.retrieveUserSimulations(fakeUser);
      fail("Should be throwing DataRetrievalException!!");
    } catch (DataRetrievalException e) {
    }

    mocksControl.verify();

    assertEquals(dummyUserSimulations, returnedSimulations);
  }

  @Test
  public void testSave() {
    expect(mockSimulationDAO.store(isA(Simulation.class)))
          .andReturn(mockSimulation);

    mocksControl.replay();

    final Simulation returnedSimulation = simulationManager.save(mockSimulation);

    mocksControl.verify();

    assertSame(mockSimulation, returnedSimulation);
  }
}