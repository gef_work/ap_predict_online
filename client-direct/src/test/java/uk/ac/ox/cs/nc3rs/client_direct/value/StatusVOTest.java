/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.value;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit test the Status value object.
 *
 * @author geoff
 */
public class StatusVOTest {

  private static final String appManagerCompletedText = "PROG: ..done!";
  private static final String appManagerPctCompletedText = "% completed";
  private static final String appManagerProgressFileNotFoundText = " Progress file not found - ";

  private StatusVO statusVO;
  private String dummyTimestamp = null;
  private String dummyStatus = null;
  private String dummyLevel = null;

  @Before
  public void setUp() {
    dummyTimestamp = null;
    dummyStatus = null;
    dummyLevel = null;
  }

  @Test
  public void testConstructorSuccessOnNullParameters() {
    statusVO = new StatusVO(dummyTimestamp, dummyStatus, dummyLevel);
    assertNotNull(statusVO.getTimestamp());
    assertNull(statusVO.getStatus());
    assertNull(statusVO.getLevel());
    assertFalse(statusVO.isProcessStatus());
    assertFalse(statusVO.isProgressStatus());
    assertNotNull(statusVO.toString());
  }

  @Test
  public void testConstructor() {
    dummyTimestamp = "dummyTimestamp";
    dummyStatus = "dummyStatus";
    dummyLevel = "dummyLevel";
    statusVO = new StatusVO(dummyTimestamp, dummyStatus, dummyLevel);
    assertEquals(dummyTimestamp, statusVO.getTimestamp());
    assertEquals(dummyStatus, statusVO.getStatus());
    assertEquals(dummyLevel, statusVO.getLevel());
    assertFalse(statusVO.isProcessStatus());
    assertFalse(statusVO.isProgressStatus());
    assertFalse(statusVO.isProgressFileNotFoundStatus());
  }

  @Test
  public void testKnownStatusContent() {
    dummyTimestamp = "dummyTimestamp";
    dummyStatus = StatusVO.TEXT_PROGRESS.concat(" dummyStatus");
    dummyLevel = "dummyLevel";
    statusVO = new StatusVO(dummyTimestamp, dummyStatus, dummyLevel);
    assertFalse(statusVO.isProcessStatus());
    assertTrue(statusVO.isProgressStatus());
    assertFalse(statusVO.isProgressFileNotFoundStatus());

    dummyStatus = StatusVO.TEXT_PROGRESS.concat(appManagerProgressFileNotFoundText);
    statusVO = new StatusVO(dummyTimestamp, dummyStatus, dummyLevel);
    assertFalse(statusVO.isProcessStatus());
    assertTrue(statusVO.isProgressStatus());
    assertTrue(statusVO.isProgressFileNotFoundStatus());

    dummyStatus = StatusVO.TEXT_PROCESS.concat(" dummyStatus");
    statusVO = new StatusVO(dummyTimestamp, dummyStatus, dummyLevel);
    assertTrue(statusVO.isProcessStatus());
    assertFalse(statusVO.isProgressStatus());
    assertFalse(statusVO.isProgressFileNotFoundStatus());
  }

  @Test
  public void testStatusRepresentsCompleted() {
    dummyStatus = StatusVO.TEXT_PROGRESS.concat(" dummyStatus");
    assertFalse(StatusVO.statusRepresentsCompleted(dummyStatus));

    dummyStatus = appManagerCompletedText;
    assertTrue(StatusVO.statusRepresentsCompleted(dummyStatus));
  }

  @Test
  public void testStatusesIndicateSimulationCompleted() {
    dummyTimestamp = "dummyTimestamp";
    dummyStatus = "dummyStatus";
    dummyLevel = "dummyLevel";

    /*
     * Null arg
     */
    Collection<StatusVO> dummyStatuses = null;
    assertFalse(StatusVO.statusesIndicatesSimulationCompleted(dummyStatuses));

    /*
     * Non-null statuses, containing null + others
     */
    dummyStatuses = new ArrayList<StatusVO>();
    assertFalse(StatusVO.statusesIndicatesSimulationCompleted(dummyStatuses));

    dummyStatuses.add(null);
    assertFalse(StatusVO.statusesIndicatesSimulationCompleted(dummyStatuses));

    dummyStatuses.add(new StatusVO(dummyTimestamp, null, dummyLevel));
    assertFalse(StatusVO.statusesIndicatesSimulationCompleted(dummyStatuses));

    dummyStatuses.add(new StatusVO(dummyTimestamp, dummyStatus, dummyLevel));
    assertFalse(StatusVO.statusesIndicatesSimulationCompleted(dummyStatuses));

    dummyStatuses.add(new StatusVO(dummyTimestamp, appManagerCompletedText,
                                   dummyLevel));
    assertTrue(StatusVO.statusesIndicatesSimulationCompleted(dummyStatuses));

    dummyStatuses.add(new StatusVO(dummyTimestamp, dummyStatus, dummyLevel));
    assertTrue(StatusVO.statusesIndicatesSimulationCompleted(dummyStatuses));

    /*
     * Reset to test for post-PKPD stuff 
     */
    dummyStatuses = new ArrayList<StatusVO>();
    assertFalse(StatusVO.statusesIndicatesSimulationCompleted(dummyStatuses));

    dummyStatuses.add(null);
    assertFalse(StatusVO.statusesIndicatesSimulationCompleted(dummyStatuses));

    dummyStatuses.add(new StatusVO(dummyTimestamp, null, dummyLevel));
    assertFalse(StatusVO.statusesIndicatesSimulationCompleted(dummyStatuses));

    dummyStatuses.add(new StatusVO(dummyTimestamp, dummyStatus, dummyLevel));
    assertFalse(StatusVO.statusesIndicatesSimulationCompleted(dummyStatuses));

    dummyStatuses.add(new StatusVO(dummyTimestamp,
                                   appManagerPctCompletedText,
                                   dummyLevel));
    assertFalse(StatusVO.statusesIndicatesSimulationCompleted(dummyStatuses));

    dummyStatuses.add(new StatusVO(dummyTimestamp,
                                   appManagerProgressFileNotFoundText,
                                   dummyLevel));
    assertFalse(StatusVO.statusesIndicatesSimulationCompleted(dummyStatuses));

    dummyStatuses.add(new StatusVO(dummyTimestamp, dummyStatus, dummyLevel));
    assertFalse(StatusVO.statusesIndicatesSimulationCompleted(dummyStatuses));

    dummyStatuses.add(new StatusVO(dummyTimestamp,
                                   appManagerPctCompletedText,
                                   dummyLevel));
    assertTrue(StatusVO.statusesIndicatesSimulationCompleted(dummyStatuses));
  }
}