/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the names of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.entity;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.nc3rs.client_direct.value.C50_TYPE;
import uk.ac.ox.cs.nc3rs.client_direct.value.MeasurementObservationsVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.IC50_UNIT;

/**
 * Unit test the simulation input entity.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { MeasurementObservationsVO.class } )
public class SimulationInputTest {

  private BigDecimal dummyPacingFrequency;
  private BigDecimal dummyPacingMaxTime;
  private BigDecimal dummyPIC50ICaL, dummyHillICaL, dummySaturationICaL,
                     dummySpreadICaL;
  private BigDecimal dummyPIC50IK1, dummyHillIK1, dummySaturationIK1,
                     dummySpreadIK1;
  private BigDecimal dummyPIC50IKr, dummyHillIKr, dummySaturationIKr,
                     dummySpreadIKr;
  private BigDecimal dummyPIC50IKs, dummyHillIKs, dummySaturationIKs,
                     dummySpreadIKs;
  private BigDecimal dummyPIC50INa, dummyHillINa, dummySaturationINa,
                     dummySpreadINa;
  private BigDecimal dummyPIC50INaL, dummyHillINaL, dummySaturationINaL,
                     dummySpreadINaL;
  private BigDecimal dummyPIC50Ito, dummyHillIto, dummySaturationIto,
                     dummySpreadIto;
  private BigDecimal dummyPlasmaConcMax;
  private BigDecimal dummyPlasmaConcMin;
  private boolean dummyPlasmaIntPtLogScale;
  private C50_TYPE dummyC50Type;
  private IC50_UNIT dummyIC50Unit;
  private IMocksControl mocksControl;
  private PortalFile mockPortalFile;
  private SimulationInput simulationInput;
  private Short dummyPlasmaIntPtCount;
  private Short dummyModelIdentifier;
  private String dummyCellMLFileName;
  private String dummyCredibleIntervalPctiles;
  private String dummyNotes;
  private String dummyPlasmaConcPoints;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
  }

  private SimulationInput createSimulationInput() {
    return new SimulationInput(dummyPIC50ICaL, dummyHillICaL,
                               dummySaturationICaL, dummySpreadICaL,
                               dummyPIC50IK1, dummyHillIK1,
                               dummySaturationIK1, dummySpreadIK1,
                               dummyPIC50IKr, dummyHillIKr,
                               dummySaturationIKr, dummySpreadIKr,
                               dummyPIC50IKs, dummyHillIKs,
                               dummySaturationIKs, dummySpreadIKs,
                               dummyPIC50INa, dummyHillINa,
                               dummySaturationINa, dummySpreadINa,
                               dummyPIC50INaL, dummyHillINaL,
                               dummySaturationINaL, dummySpreadINaL,
                               dummyPIC50Ito, dummyHillIto,
                               dummySaturationIto, dummySpreadIto,
                               dummyC50Type, dummyIC50Unit,
                               dummyModelIdentifier, dummyPacingMaxTime,
                               dummyPacingFrequency, dummyPlasmaConcMax,
                               dummyPlasmaConcMin, dummyPlasmaIntPtCount,
                               dummyPlasmaIntPtLogScale, dummyPlasmaConcPoints,
                               mockPortalFile, dummyCellMLFileName,
                               dummyCredibleIntervalPctiles, dummyNotes);
  }

  private void setValidValues() {
    dummyModelIdentifier = 10;
    dummyC50Type = C50_TYPE.IC50;
    dummyIC50Unit = IC50_UNIT.µM;
    dummyPacingFrequency = new BigDecimal("0.5");
    dummyPacingMaxTime = new BigDecimal("2.0");
    dummyPIC50ICaL = new BigDecimal("4.5");
    dummyHillICaL = new BigDecimal("0.45");
    dummySaturationICaL = new BigDecimal("45.0");
    dummySpreadICaL = new BigDecimal("0.11");
    dummyPIC50IK1 = new BigDecimal("4.6");
    dummyHillIK1 = new BigDecimal("0.46");
    dummySaturationIK1 = new BigDecimal("46.0");
    dummySpreadIK1 = new BigDecimal("0.22");
    dummyPIC50IKr = new BigDecimal("4.7");
    dummyHillIKr = new BigDecimal("0.47");
    dummySaturationIKr = new BigDecimal("47.0");
    dummySpreadIKr = new BigDecimal("0.33");
    dummyPIC50IKs = new BigDecimal("4.8");
    dummyHillIKs = new BigDecimal("0.48");
    dummySaturationIKs = new BigDecimal("48.0");
    dummySpreadIKs = new BigDecimal("0.44");
    dummyPIC50INa = new BigDecimal("4.9");
    dummyHillINa = new BigDecimal("0.49");
    dummySaturationINa = new BigDecimal("49.0");
    dummySpreadINa = new BigDecimal("0.55");
    dummyPIC50INaL = new BigDecimal("4.91");
    dummyHillINaL = new BigDecimal("0.491");
    dummySaturationINaL = new BigDecimal("49.1");
    dummySpreadINaL = new BigDecimal("0.551");
    dummyPIC50Ito = new BigDecimal("5.0");
    dummyHillIto = new BigDecimal("0.5");
    dummySaturationIto = new BigDecimal("50.0");
    dummySpreadIto = new BigDecimal("0.66");
    dummyPlasmaIntPtCount = Short.valueOf("6");
    dummyPlasmaConcMax = new BigDecimal("100");
    dummyPlasmaConcMin = new BigDecimal("10");
    dummyPlasmaIntPtLogScale = true;
    dummyPlasmaConcPoints = "dummyPlasmaConcPoints";
    mockPortalFile = mocksControl.createMock(PortalFile.class);
    dummyCellMLFileName = "dummyCellMLFileName";
    dummyCredibleIntervalPctiles = "dummyCredibleIntervalPctiles";
    dummyNotes = "dummyNotes";
  }

  @Test
  public void testIsCompoundEffects() {
    setValidValues();

    simulationInput = createSimulationInput();

    mockStatic(MeasurementObservationsVO.class);
    expect(MeasurementObservationsVO.hasNonC50Value(dummyHillICaL.toPlainString(),
                                                    dummySaturationICaL.toPlainString(),
                                                    dummySpreadICaL.toPlainString()))
          .andReturn(true);
    expect(MeasurementObservationsVO.hasNonC50Value(dummyHillIK1.toPlainString(),
                                                    dummySaturationIK1.toPlainString(),
                                                    dummySpreadIK1.toPlainString()))
          .andReturn(true);
    expect(MeasurementObservationsVO.hasNonC50Value(dummyHillIKr.toPlainString(),
                                                    dummySaturationIKr.toPlainString(),
                                                    dummySpreadIKr.toPlainString()))
          .andReturn(true);
    expect(MeasurementObservationsVO.hasNonC50Value(dummyHillIKs.toPlainString(),
                                                    dummySaturationIKs.toPlainString(),
                                                    dummySpreadIKs.toPlainString()))
          .andReturn(true);
    expect(MeasurementObservationsVO.hasNonC50Value(dummyHillINa.toPlainString(),
                                                    dummySaturationINa.toPlainString(),
                                                    dummySpreadINa.toPlainString()))
          .andReturn(true);
    expect(MeasurementObservationsVO.hasNonC50Value(dummyHillINaL.toPlainString(),
                                                    dummySaturationINaL.toPlainString(),
                                                    dummySpreadINaL.toPlainString()))
          .andReturn(true);
    expect(MeasurementObservationsVO.hasNonC50Value(dummyHillIto.toPlainString(),
                                                    dummySaturationIto.toPlainString(),
                                                    dummySpreadIto.toPlainString()))
          .andReturn(true);

    replayAll();
    // Not testing pk file mocking

    assertTrue(simulationInput.retrieveNonC50ICaL());
    assertTrue(simulationInput.retrieveNonC50IK1());
    assertTrue(simulationInput.retrieveNonC50IKr());
    assertTrue(simulationInput.retrieveNonC50IKs());
    assertTrue(simulationInput.retrieveNonC50INa());
    assertTrue(simulationInput.retrieveNonC50INaL());
    assertTrue(simulationInput.retrieveNonC50Ito());

    verifyAll();
  }

  @Test
  public void testInitialisingConstructorWithNullArgs() {
    dummyC50Type = null;
    dummyIC50Unit = null;
    dummyPacingFrequency = null;
    dummyPacingMaxTime = null;
    dummyPIC50ICaL = null;
    dummyHillICaL = null;
    dummySaturationICaL = null;
    dummyPIC50IK1 = null;
    dummyHillIK1 = null;
    dummySaturationIK1 = null;
    dummyPIC50IKr = null;
    dummyHillIKr = null;
    dummySaturationIKr = null;
    dummyPIC50IKs = null;
    dummyHillIKs = null;
    dummySaturationIKs = null;
    dummyPIC50INa = null;
    dummyHillINa = null;
    dummySaturationINa = null;
    dummyPIC50INaL = null;
    dummyHillINaL = null;
    dummySaturationINaL = null;
    dummyPIC50Ito = null;
    dummyHillIto = null;
    dummySaturationIto = null;
    dummyPlasmaIntPtCount = null;
    dummyPlasmaConcMax = null;
    dummyPlasmaConcMin = null;
    dummyNotes = null;

    mocksControl.replay();

    simulationInput = createSimulationInput();

    mocksControl.verify();

    assertNull(simulationInput.getId());
    assertNull(simulationInput.getC50ICaL());
    assertNull(simulationInput.getHillICaL());
    assertNull(simulationInput.getSaturationICaL());
    assertNull(simulationInput.getSpreadICaL());
    assertNull(simulationInput.getC50Ik1());
    assertNull(simulationInput.getHillIk1());
    assertNull(simulationInput.getSaturationIk1());
    assertNull(simulationInput.getSpreadIk1());
    assertNull(simulationInput.getC50Ikr());
    assertNull(simulationInput.getHillIkr());
    assertNull(simulationInput.getSaturationIkr());
    assertNull(simulationInput.getSpreadIkr());
    assertNull(simulationInput.getC50Iks());
    assertNull(simulationInput.getHillIks());
    assertNull(simulationInput.getSaturationIks());
    assertNull(simulationInput.getSpreadIks());
    assertNull(simulationInput.getC50INa());
    assertNull(simulationInput.getHillINa());
    assertNull(simulationInput.getSaturationINa());
    assertNull(simulationInput.getSpreadINa());
    assertNull(simulationInput.getC50INaL());
    assertNull(simulationInput.getHillINaL());
    assertNull(simulationInput.getSaturationINaL());
    assertNull(simulationInput.getSpreadINaL());
    assertNull(simulationInput.getC50Ito());
    assertNull(simulationInput.getHillIto());
    assertNull(simulationInput.getSaturationIto());
    assertNull(simulationInput.getSpreadIto());
    assertNull(simulationInput.getC50Type());
    assertNull(simulationInput.getIc50Units());
    assertNull(simulationInput.getModelIdentifier());
    assertNull(simulationInput.getPacingMaxTime());
    assertNull(simulationInput.getPacingFrequency());
    assertNull(simulationInput.getPlasmaConcMax());
    assertNull(simulationInput.getPlasmaConcMin());
    assertNull(simulationInput.getPlasmaIntPtCount());
    assertFalse(simulationInput.isPlasmaIntPtLogScale());
    assertNull(simulationInput.getPlasmaConcPoints());
    assertNull(simulationInput.getPkFile());
    assertNull(simulationInput.getPkFileName());
    assertNull(simulationInput.getCellMLFileName());
    assertNull(simulationInput.getCredibleIntervalPctiles());
    assertNull(simulationInput.getNotes());
    assertNotNull(simulationInput.toString());
  }

  @Test
  public void testInitialisingConstructorWithValidValues() {
    setValidValues();

    final String dummyPKFileName = "dummyPKFileName";
    expect(mockPortalFile.getName()).andReturn(dummyPKFileName);

    mocksControl.replay();

    simulationInput = createSimulationInput();

    mocksControl.verify();

    assertEquals(dummyModelIdentifier, simulationInput.getModelIdentifier());
    assertTrue(dummyPacingFrequency.compareTo(simulationInput.getPacingFrequency()) == 0);
    assertTrue(dummyPacingMaxTime.compareTo(simulationInput.getPacingMaxTime()) == 0);
    assertTrue(dummyPIC50ICaL.compareTo(simulationInput.getC50ICaL()) == 0);
    assertTrue(dummyHillICaL.compareTo(simulationInput.getHillICaL()) == 0);
    assertTrue(dummySaturationICaL.compareTo(simulationInput.getSaturationICaL()) == 0);
    assertTrue(dummySpreadICaL.compareTo(simulationInput.getSpreadICaL()) == 0);
    assertTrue(dummyPIC50IK1.compareTo(simulationInput.getC50Ik1()) == 0);
    assertTrue(dummyHillIK1.compareTo(simulationInput.getHillIk1()) == 0);
    assertTrue(dummySaturationIK1.compareTo(simulationInput.getSaturationIk1()) == 0);
    assertTrue(dummySpreadIK1.compareTo(simulationInput.getSpreadIk1()) == 0);
    assertTrue(dummyPIC50IKr.compareTo(simulationInput.getC50Ikr()) == 0);
    assertTrue(dummyHillIKr.compareTo(simulationInput.getHillIkr()) == 0);
    assertTrue(dummySaturationIKr.compareTo(simulationInput.getSaturationIkr()) == 0);
    assertTrue(dummySpreadIKr.compareTo(simulationInput.getSpreadIkr()) == 0);
    assertTrue(dummyPIC50IKs.compareTo(simulationInput.getC50Iks()) == 0);
    assertTrue(dummyHillIKs.compareTo(simulationInput.getHillIks()) == 0);
    assertTrue(dummySaturationIKs.compareTo(simulationInput.getSaturationIks()) == 0);
    assertTrue(dummySpreadIKs.compareTo(simulationInput.getSpreadIks()) == 0);
    assertTrue(dummyPIC50INa.compareTo(simulationInput.getC50INa()) == 0);
    assertTrue(dummyHillINa.compareTo(simulationInput.getHillINa()) == 0);
    assertTrue(dummySaturationINa.compareTo(simulationInput.getSaturationINa()) == 0);
    assertTrue(dummySpreadINa.compareTo(simulationInput.getSpreadINa()) == 0);
    assertTrue(dummyPIC50INaL.compareTo(simulationInput.getC50INaL()) == 0);
    assertTrue(dummyHillINaL.compareTo(simulationInput.getHillINaL()) == 0);
    assertTrue(dummySaturationINaL.compareTo(simulationInput.getSaturationINaL()) == 0);
    assertTrue(dummySpreadINaL.compareTo(simulationInput.getSpreadINaL()) == 0);
    assertTrue(dummyPIC50Ito.compareTo(simulationInput.getC50Ito()) == 0);
    assertTrue(dummyHillIto.compareTo(simulationInput.getHillIto()) == 0);
    assertTrue(dummySaturationIto.compareTo(simulationInput.getSaturationIto()) == 0);
    assertTrue(dummySpreadIto.compareTo(simulationInput.getSpreadIto()) == 0);
    assertTrue(dummyC50Type.compareTo(simulationInput.getC50Type()) == 0);
    assertTrue(dummyIC50Unit.compareTo(simulationInput.getIc50Units()) == 0);
    assertTrue(dummyPlasmaConcMax.compareTo(simulationInput.getPlasmaConcMax()) == 0);
    assertTrue(dummyPlasmaConcMin.compareTo(simulationInput.getPlasmaConcMin()) == 0);
    assertTrue(simulationInput.isPlasmaIntPtLogScale());
    assertSame(dummyPlasmaConcPoints, simulationInput.getPlasmaConcPoints());
    assertSame(mockPortalFile, simulationInput.getPkFile());
    assertEquals(dummyPKFileName, simulationInput.getPkFileName());
    assertEquals(dummyCellMLFileName, simulationInput.getCellMLFileName());
    assertEquals(dummyCredibleIntervalPctiles,
                 simulationInput.getCredibleIntervalPctiles());
    assertEquals(dummyNotes, simulationInput.getNotes());
    assertNotNull(simulationInput.toString());
  }
}