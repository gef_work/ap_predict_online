/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.service;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.compbio.client_shared.manager.FileManager;
import uk.ac.ox.cs.compbio.client_shared.value.PortalFeature;
import uk.ac.ox.cs.compbio.client_shared.value.file.FILE_DATA_TYPE;
import uk.ac.ox.cs.compbio.client_shared.value.file.FileStoreActionOutcomeVO;
import uk.ac.ox.cs.compbio.client_shared.value.security.Role;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Job;
import uk.ac.ox.cs.nc3rs.client_direct.entity.JobPKResult;
import uk.ac.ox.cs.nc3rs.client_direct.entity.JobResult;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation;
import uk.ac.ox.cs.nc3rs.client_direct.exception.AppManagerWSInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.DataRetrievalException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.NoConnectionException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.ResultsNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.RunInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.SimulationNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.manager.ConfigurationManager;
import uk.ac.ox.cs.nc3rs.client_direct.manager.SimulationManager;
import uk.ac.ox.cs.nc3rs.client_direct.value.InputOption;
import uk.ac.ox.cs.nc3rs.client_direct.value.IonCurrent;
import uk.ac.ox.cs.nc3rs.client_direct.value.ModelVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.ResultsVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationCurrentResultsVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.StatusVO;
import uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.service.AppManagerService;

/**
 * Unit test the client (direct) service interface implementation.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { ClientDirectServiceImpl.class, StatusVO.class,
                   ResultsVO.class } )
public class ClientDirectServiceImplTest {

  // Derived from StatusVO.
  private static final String appManagerCompletedText = "PROG: ..done!";

  private AppManagerService mockAppManagerService;
  private ClientDirectService clientDirectService;
  private ConfigurationManager mockConfigurationManager;
  private FileManager mockFileManager;
  private IMocksControl mocksControl;
  private long dummyAppManagerId = 2l;
  private long dummySimulationId = 3l;
  private ModelVO mockModel;
  private Simulation mockSimulation;
  private SimulationInputVO mockSimulationInputVO;
  private SimulationManager mockSimulationManager;

  @Before
  public void setUp() {
    clientDirectService = new ClientDirectServiceImpl();

    mocksControl = createStrictControl();
    mockAppManagerService = mocksControl.createMock(AppManagerService.class);
    mockConfigurationManager = mocksControl.createMock(ConfigurationManager.class);
    mockFileManager = mocksControl.createMock(FileManager.class);
    mockSimulationManager = mocksControl.createMock(SimulationManager.class);
    ReflectionTestUtils.setField(clientDirectService,
                                 ClientDirectIdentifiers.COMPONENT_APP_MANAGER_SERVICE,
                                 mockAppManagerService);
    ReflectionTestUtils.setField(clientDirectService,
                                 ClientDirectIdentifiers.COMPONENT_CONFIGURATION_MANAGER,
                                 mockConfigurationManager);
    ReflectionTestUtils.setField(clientDirectService,
                                 ClientSharedIdentifiers.COMPONENT_FILE_MANAGER,
                                 mockFileManager);
    ReflectionTestUtils.setField(clientDirectService,
                                 ClientDirectIdentifiers.COMPONENT_SIMULATION_MANAGER,
                                 mockSimulationManager);

    mockSimulation = mocksControl.createMock(Simulation.class);
    mockSimulationInputVO = mocksControl.createMock(SimulationInputVO.class);
  }

  @Test
  public void testDeleteSimulation() {
    mockSimulationManager.deleteSimulation(dummySimulationId);

    mocksControl.replay();

    clientDirectService.deleteSimulation(dummySimulationId);

    mocksControl.verify();
  }

  @Test
  public void testRetrieveAllFiles() {
    final List<PortalFile> dummyFiles = new ArrayList<PortalFile>();

    expect(mockFileManager.retrieveAllFiles()).andReturn(dummyFiles);

    mocksControl.replay();

    final List<PortalFile> retrieved = clientDirectService.retrieveAllFiles();

    mocksControl.verify();

    assertSame(dummyFiles, retrieved);
  }

  @Test
  public void testRetrieveAllSimulations() {
    expect(mockSimulationManager.retrieveAllSimulations())
          .andReturn(new ArrayList<Simulation>());

    mocksControl.replay();

    final List<Simulation> returnedSimulations = clientDirectService.retrieveAllSimulations();

    mocksControl.verify();

    assertNotNull(returnedSimulations);
    assertSame(0, returnedSimulations.size());
}

  @Test
  public void testRetrieveC50Units() {
    final Map<String, String> dummyC50Units = new HashMap<String, String>();
    dummyC50Units.put("dummyKey1", "dummyValue1");
    expect(mockConfigurationManager.retrieveC50Units()).andReturn(dummyC50Units);

    mocksControl.replay();

    final Map<String, String> retrievedC50Units = clientDirectService.retrieveC50Units();

    mocksControl.verify();

    assertSame(dummyC50Units, retrievedC50Units);
  }

  @Test
  public void testRetrieveCredibleIntervalPctiles() {
    final Set<BigDecimal> dummyCIPctiles = new HashSet<BigDecimal>();
    final BigDecimal dummyCIPctile = BigDecimal.TEN;
    dummyCIPctiles.add(dummyCIPctile);
    expect(mockConfigurationManager.retrieveCredibleIntervalPctiles())
          .andReturn(dummyCIPctiles);

    mocksControl.replay();

    final Set<BigDecimal> retrievedCIPctiles = clientDirectService.retrieveCredibleIntervalPctiles();

    mocksControl.verify();

    assertSame(dummyCIPctiles, retrievedCIPctiles);
  }

  @Test
  public void testRetrieveLatestProgressSuccessOnCompletion()
              throws SimulationNotFoundException,
                     AppManagerWSInvocationException, NoConnectionException {
    /*
     * Scenario 1 - Progress status represents completed.
     */
    expect(mockSimulationManager.retrieveSimulation(dummySimulationId))
          .andReturn(mockSimulation);
    expect(mockSimulation.isCompleted()).andReturn(false);
    expect(mockSimulation.getAppManagerId()).andReturn(dummyAppManagerId);
    String dummyProgress = appManagerCompletedText;
    expect(mockAppManagerService.retrieveLatestCompletedProgress(dummyAppManagerId))
          .andReturn(dummyProgress);
    mockStatic(StatusVO.class);
    expect(StatusVO.statusRepresentsCompleted(dummyProgress)).andReturn(true);
    mockSimulation.assignCompleted();
    expect(mockSimulationManager.save(mockSimulation)).andReturn(mockSimulation);

    replayAll();
    mocksControl.replay();

    String returnedProgress = clientDirectService.retrieveLatestProgress(dummySimulationId);

    verifyAll();
    mocksControl.verify();

    assertEquals(dummyProgress, returnedProgress);

    resetAll();
    mocksControl.reset();

    /*
     * Scenario 2 - Progress status doesn't represent completed.
     */
    expect(mockSimulationManager.retrieveSimulation(dummySimulationId))
          .andReturn(mockSimulation);
    expect(mockSimulation.isCompleted()).andReturn(false);
    expect(mockSimulation.getAppManagerId()).andReturn(dummyAppManagerId);
    expect(mockAppManagerService.retrieveLatestCompletedProgress(dummyAppManagerId))
          .andReturn(dummyProgress);
    expect(StatusVO.statusRepresentsCompleted(dummyProgress)).andReturn(false);

    replayAll();
    mocksControl.replay();

    returnedProgress = clientDirectService.retrieveLatestProgress(dummySimulationId);

    verifyAll();
    mocksControl.verify();

    assertEquals(dummyProgress, returnedProgress);
  }

  @Test
  public void testRetrieveLatestProgressSuccessOnFinished()
              throws SimulationNotFoundException,
                     AppManagerWSInvocationException, NoConnectionException {
    expect(mockSimulationManager.retrieveSimulation(dummySimulationId))
          .andReturn(mockSimulation);
    expect(mockSimulation.isCompleted()).andReturn(true);

    mocksControl.replay();

    final String returnedProgress = clientDirectService.retrieveLatestProgress(dummySimulationId);

    mocksControl.verify();

    assertEquals(ClientDirectService.PROGRESS_INDICATING_SIMULATION_HAS_FINISHED,
                 returnedProgress);
  }

  @Test
  public void testRetrieveLatestProgressSuccessOnSomeKindOfProblem()
              throws SimulationNotFoundException,
                     AppManagerWSInvocationException, NoConnectionException {
    expect(mockSimulationManager.retrieveSimulation(dummySimulationId))
          .andReturn(mockSimulation);
    expect(mockSimulation.isCompleted()).andReturn(false);
    expect(mockSimulation.getAppManagerId()).andReturn(dummyAppManagerId);
    expect(mockAppManagerService.retrieveLatestCompletedProgress(dummyAppManagerId))
          .andReturn(null);

    mocksControl.replay();

    final String returnedProgress = clientDirectService.retrieveLatestProgress(dummySimulationId);

    mocksControl.verify();

    assertEquals(ClientDirectService.PROGRESS_INDICATING_SOME_KIND_OF_PROBLEM, returnedProgress);
  }

  @Test
  public void testRetrieveModelNames() {
    final Map<Short, String> dummyModelNames = new HashMap<Short, String>();
    dummyModelNames.put(Short.valueOf("2"), "dummyValue1");
    expect(mockConfigurationManager.retrieveModelNames())
          .andReturn(dummyModelNames);

    mocksControl.replay();

    final Map<Short, String> retrievedModelNames = clientDirectService.retrieveModelNames();
    assertSame(dummyModelNames, retrievedModelNames);

    mocksControl.verify();
  }

  @Test
  public void testRetrieveModels() {
    final List<ModelVO> dummyModels = new ArrayList<ModelVO>();
    dummyModels.add(mockModel);

    expect(mockConfigurationManager.retrieveModels()).andReturn(dummyModels);

    mocksControl.replay();

    final List<ModelVO> retrievedModels = clientDirectService.retrieveModels();
    assertSame(dummyModels, retrievedModels);

    mocksControl.verify();
  }

  @Test
  public void testRetrieveRequiredRoles() {
    final Set<Role> dummyRoles = new HashSet<Role>();
    final PortalFeature dummyPortalFeature = PortalFeature.UNCERTAINTIES;

    expect(mockConfigurationManager.retrieveRequiredRoles(dummyPortalFeature))
          .andReturn(dummyRoles);

    mocksControl.replay();

    final Set<Role> requiredRoles = clientDirectService.retrieveRequiredRoles(dummyPortalFeature);

    mocksControl.verify();

    assertEquals(dummyRoles, requiredRoles);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testRetrieveSimulationResults() throws Exception {
    /*
     * Scenario 1 - Simulation complete, but no results found either in
     *              simulation manager or app manager (and not in grace period)
     */
    // >> retrieveSimulation(long)
    expect(mockSimulationManager.retrieveSimulation(dummySimulationId))
          .andReturn(mockSimulation);
    // << retrieveSimulation(long)
    expect(mockSimulation.getAppManagerId()).andReturn(dummyAppManagerId);
    boolean dummySimulationIsCompleted = true;
    expect(mockSimulation.isCompleted()).andReturn(dummySimulationIsCompleted);
    final Job mockSimulationManagerJob = mocksControl.createMock(Job.class);
    expect(mockSimulationManager.retrieveResults(dummySimulationId))
          .andReturn(mockSimulationManagerJob);
    boolean dummySimulationManagerJobHasResults = false;
    expect(mockSimulationManagerJob.hasResults())
          .andReturn(dummySimulationManagerJobHasResults);
    // See if app manager has job results!
    final Job mockAppManagerJob = mocksControl.createMock(Job.class);
    expect(mockAppManagerService.retrieveResults(dummyAppManagerId))
          .andReturn(mockAppManagerJob);
    boolean dummyAppManagerJobHasResults = false;
    expect(mockAppManagerJob.hasResults())
          .andReturn(dummyAppManagerJobHasResults);
    boolean dummyWithinPostCompletionGracePeriod = false;
    expect(mockSimulation.withinPostCompletionGracePeriod())
          .andReturn(dummyWithinPostCompletionGracePeriod);

    mocksControl.replay();

    try {
      clientDirectService.retrieveSimulationResults(dummySimulationId);
      fail("Should not accept a simulation being complete but no results found!");
    } catch (ResultsNotFoundException e) {
      //
    } catch (Exception e) {
      fail("Should not throw unexpected exception!");
    }

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Scenario 2 - Simulation complete, but no results found either in
     *              simulation manager or app manager (and in grace period)
     */
    // >> retrieveSimulation(long)
    expect(mockSimulationManager.retrieveSimulation(dummySimulationId))
          .andReturn(mockSimulation);
    // << retrieveSimulation(long)
    expect(mockSimulation.getAppManagerId()).andReturn(dummyAppManagerId);
    expect(mockSimulation.isCompleted()).andReturn(dummySimulationIsCompleted);
    expect(mockSimulationManager.retrieveResults(dummySimulationId))
          .andReturn(mockSimulationManagerJob);
    expect(mockSimulationManagerJob.hasResults())
          .andReturn(dummySimulationManagerJobHasResults);
    // See if app manager has job results!
    expect(mockAppManagerService.retrieveResults(dummyAppManagerId))
          .andReturn(mockAppManagerJob);
    expect(mockAppManagerJob.hasResults())
          .andReturn(dummyAppManagerJobHasResults);
    dummyWithinPostCompletionGracePeriod = true;
    expect(mockSimulation.withinPostCompletionGracePeriod())
          .andReturn(dummyWithinPostCompletionGracePeriod);
    Capture<Simulation> captureSimulation = newCapture();
    Capture<List<StatusVO>> captureProgress = newCapture();
    Capture<ResultsVO> captureResults = newCapture();
    SimulationCurrentResultsVO mockResults = mocksControl.createMock(SimulationCurrentResultsVO.class);
    expectNew(SimulationCurrentResultsVO.class, capture(captureSimulation),
                                                capture(captureProgress),
                                                capture(captureResults))
             .andReturn(mockResults);

    replayAll();
    mocksControl.replay();

    clientDirectService.retrieveSimulationResults(dummySimulationId);

    verifyAll();
    mocksControl.verify();

    Simulation capturedSimulation = captureSimulation.getValue();
    assertSame(mockSimulation, capturedSimulation);
    List<StatusVO> capturedProgress = captureProgress.getValue();
    assertTrue(capturedProgress.isEmpty());
    assertNull(captureResults.getValue());

    resetAll();
    mocksControl.reset();

    /*
     * Scenario 3 - Simulation complete, results found in app manager.
     */
    // >> retrieveSimulation()
    expect(mockSimulationManager.retrieveSimulation(dummySimulationId))
          .andReturn(mockSimulation);
    // << retrieveSimulation()
    expect(mockSimulation.getAppManagerId()).andReturn(dummyAppManagerId);
    expect(mockSimulation.isCompleted()).andReturn(dummySimulationIsCompleted);
    expect(mockSimulationManager.retrieveResults(dummySimulationId))
          .andReturn(mockSimulationManagerJob);
    // No results from simulation manager.
    expect(mockSimulationManagerJob.hasResults())
          .andReturn(dummySimulationManagerJobHasResults);
    // See if app manager has job results....
    expect(mockAppManagerService.retrieveResults(dummyAppManagerId))
          .andReturn(mockAppManagerJob);
    dummyAppManagerJobHasResults = true;
    expect(mockAppManagerJob.hasResults())
          .andReturn(dummyAppManagerJobHasResults);
    // ... yes is has, so persist them!
    final Job mockAppManagerDerivedJob = mocksControl.createMock(Job.class);
    expect(mockSimulationManager.recordResults(dummySimulationId,
                                               mockAppManagerJob))
          .andReturn(mockAppManagerDerivedJob);
    mockAppManagerService.signalResultsUploaded(dummyAppManagerId);

    expect(mockAppManagerDerivedJob.getAppManagerId())
          .andReturn(dummyAppManagerId);
    final String dummyMessages = "dummyMessages";
    expect(mockAppManagerDerivedJob.getMessages()).andReturn(dummyMessages);
    final String dummyDeltaAPD90PctileNames = "dummyDeltaAPD90PctileNames";
    expect(mockAppManagerDerivedJob.getDeltaAPD90PercentileNames())
          .andReturn(dummyDeltaAPD90PctileNames);
    final InputOption dummyInputOption = InputOption.CONC_POINTS;
    expect(mockSimulation.retrieveInputOption()).andReturn(dummyInputOption);
    // (Assume that copied JobResults from App Manager are appearing here!)
    final Set<JobResult> dummyCopiedJobResults = new HashSet<JobResult>();
    final JobResult mockCopiedJobResult = mocksControl.createMock(JobResult.class);
    dummyCopiedJobResults.add(mockCopiedJobResult);
    expect(mockAppManagerDerivedJob.getJobResults())
          .andReturn(dummyCopiedJobResults);
    final BigDecimal dummyReference = BigDecimal.TEN;
    expect(mockCopiedJobResult.getReference()).andReturn(dummyReference);
    final String dummyAPD90 = "dummyAPD90";
    expect(mockCopiedJobResult.getAPD90()).andReturn(dummyAPD90);
    final String dummyDeltaAPD90 = "dummyDeltaAPD90";
    expect(mockCopiedJobResult.getDeltaAPD90()).andReturn(dummyDeltaAPD90);
    final String dummyTimes = "dummyTimes";
    expect(mockCopiedJobResult.getTimes()).andReturn(dummyTimes);
    final String dummyVoltages = "dummyVoltages";
    expect(mockCopiedJobResult.getVoltages()).andReturn(dummyVoltages);
    final String dummyQNet = "dummyQNet";
    expect(mockCopiedJobResult.getQNet()).andReturn(dummyQNet);
    boolean dummyForTranslation = true;
    expect(mockCopiedJobResult.getForTranslation()).andReturn(dummyForTranslation);
    // Process the PK results
    // (Assume that copied JobResults from App Manager are appearing here!)
    expect(mockAppManagerDerivedJob.hasPKResults()).andReturn(true);
    final Set<JobPKResult> dummyCopiedJobPKResults = new HashSet<JobPKResult>();
    final JobPKResult mockCopiedJobPKResult = mocksControl.createMock(JobPKResult.class);
    dummyCopiedJobPKResults.add(mockCopiedJobPKResult);
    expect(mockAppManagerDerivedJob.getJobPKResults())
          .andReturn(dummyCopiedJobPKResults);
    final BigDecimal dummyTimepoint = BigDecimal.TEN;
    expect(mockCopiedJobPKResult.getTimepoint()).andReturn(dummyTimepoint);
    final String dummyAPD90s = "dummyAPD90s";
    expect(mockCopiedJobPKResult.getApd90s()).andReturn(dummyAPD90s);
    final boolean dummyIsForTranslation = false;
    expect(mockCopiedJobPKResult.isForTranslation())
          .andReturn(dummyIsForTranslation);

    Capture<Map<String, Object>> captureJobMap = newCapture();
    final ResultsVO mockResultsVO = mocksControl.createMock(ResultsVO.class);
    expectNew(ResultsVO.class, capture(captureJobMap)).andReturn(mockResultsVO);
    captureSimulation = newCapture();
    captureProgress = newCapture();
    captureResults = newCapture();
    expectNew(SimulationCurrentResultsVO.class, capture(captureSimulation),
                                                capture(captureProgress),
                                                capture(captureResults))
             .andReturn(mockResults);

    replayAll();
    mocksControl.replay();

    clientDirectService.retrieveSimulationResults(dummySimulationId);

    verifyAll();
    mocksControl.verify();

    capturedSimulation = captureSimulation.getValue();
    assertSame(mockSimulation, capturedSimulation);
    capturedProgress = captureProgress.getValue();
    assertTrue(capturedProgress.isEmpty());
    assertSame(mockResultsVO, captureResults.getValue());

    final Map<String, Object> capturedJobMap = captureJobMap.getValue();
    assertEquals(dummyAppManagerId,
                 capturedJobMap.get(ClientDirectIdentifiers.KEY_RESULTS_JOB_ID));
    assertEquals(dummyMessages,
                 capturedJobMap.get(ClientDirectIdentifiers.KEY_RESULTS_MESSAGES));
    assertEquals(dummyDeltaAPD90PctileNames,
                 capturedJobMap.get(ClientDirectIdentifiers.KEY_RESULTS_DELTA_APD90_PERCENTILE_NAMES));

    final Set<BigDecimal> retrievedResultsForTranslation = 
         (Set<BigDecimal>) capturedJobMap.get(ClientDirectIdentifiers.KEY_RESULTS_FOR_TRANSLATION);
    assertSame(1, retrievedResultsForTranslation.size());
    assertTrue(retrievedResultsForTranslation.contains(dummyReference));

    final Map<BigDecimal, Map<String, Object>> keyedOnReference =
         (Map<BigDecimal, Map<String, Object>>) capturedJobMap.get(ClientDirectIdentifiers.KEY_RESULTS_REFERENCE_DATA);
    assertSame(1, keyedOnReference.size());
    final Map<String, Object> referenceData = keyedOnReference.get(dummyReference);
    assertSame(dummyAPD90,
               referenceData.get(ClientDirectIdentifiers.KEY_RESULTS_PERREF_APD90));
    assertSame(dummyDeltaAPD90,
               referenceData.get(ClientDirectIdentifiers.KEY_RESULTS_PERREF_DELTA_APD90));
    assertSame(dummyTimes,
               referenceData.get(ClientDirectIdentifiers.KEY_RESULTS_PERREF_TIMES));
    assertSame(dummyVoltages,
               referenceData.get(ClientDirectIdentifiers.KEY_RESULTS_PERREF_VOLTAGES));
    assertSame(dummyQNet,
               referenceData.get(ClientDirectIdentifiers.KEY_RESULTS_PERREF_QNET));

    resetAll();
    mocksControl.reset();

    /*
     * Scenario 4 - Simulation complete, results found in simulation manager.
     */
    expect(mockSimulationManager.retrieveSimulation(dummySimulationId))
          .andReturn(mockSimulation);
    // << retrieveSimulation(long)
    expect(mockSimulation.getAppManagerId()).andReturn(dummyAppManagerId);
    expect(mockSimulation.isCompleted()).andReturn(dummySimulationIsCompleted);
    expect(mockSimulationManager.retrieveResults(dummySimulationId))
          .andReturn(mockSimulationManagerJob);
    // Simulation manager job has results.
    dummySimulationManagerJobHasResults = true;
    expect(mockSimulationManagerJob.hasResults())
          .andReturn(dummySimulationManagerJobHasResults);
    expect(mockSimulationManagerJob.getAppManagerId())
          .andReturn(dummyAppManagerId);
    expect(mockSimulationManagerJob.getMessages()).andReturn(dummyMessages);
    expect(mockSimulationManagerJob.getDeltaAPD90PercentileNames())
          .andReturn(dummyDeltaAPD90PctileNames);
    expect(mockSimulation.retrieveInputOption()).andReturn(dummyInputOption);
    final Set<JobResult> dummyJobResults = new HashSet<JobResult>();
    final JobResult mockJobResult = mocksControl.createMock(JobResult.class);
    dummyJobResults.add(mockJobResult);
    expect(mockSimulationManagerJob.getJobResults()).andReturn(dummyJobResults);
    expect(mockJobResult.getReference()).andReturn(dummyReference);
    expect(mockJobResult.getAPD90()).andReturn(dummyAPD90);
    expect(mockJobResult.getDeltaAPD90()).andReturn(dummyDeltaAPD90);
    expect(mockJobResult.getTimes()).andReturn(dummyTimes);
    expect(mockJobResult.getVoltages()).andReturn(dummyVoltages);
    expect(mockJobResult.getQNet()).andReturn(dummyQNet);
    dummyForTranslation = false;
    expect(mockJobResult.getForTranslation()).andReturn(dummyForTranslation);
    // Simulation manager job has no PK results.
    expect(mockSimulationManagerJob.hasPKResults()).andReturn(false);

    captureJobMap = newCapture();
    expectNew(ResultsVO.class, capture(captureJobMap)).andReturn(mockResultsVO);
    captureSimulation = newCapture();
    captureProgress = newCapture();
    captureResults = newCapture();
    expectNew(SimulationCurrentResultsVO.class, capture(captureSimulation),
                                                capture(captureProgress),
                                                capture(captureResults))
             .andReturn(mockResults);

    replayAll();
    mocksControl.replay();

    clientDirectService.retrieveSimulationResults(dummySimulationId);

    verifyAll();
    mocksControl.verify();
  }

  @Test
  public void testRetrieveSimulationResultsSuccessOnJustFinishedSimulation()
              throws Exception {
    expect(mockSimulationManager.retrieveSimulation(dummySimulationId))
          .andReturn(mockSimulation);
    expect(mockSimulation.getAppManagerId()).andReturn(dummyAppManagerId);
    expect(mockSimulation.isCompleted()).andReturn(false);
    final List<StatusVO> dummyProgress = new ArrayList<StatusVO>();
    final StatusVO mockStatus = mocksControl.createMock(StatusVO.class);
    dummyProgress.add(mockStatus);
    expect(mockAppManagerService.retrieveProgress(dummyAppManagerId))
          .andReturn(dummyProgress);
    mockStatic(StatusVO.class);
    expect(StatusVO.statusesIndicatesSimulationCompleted(dummyProgress))
          .andReturn(true);
    mockSimulation.assignCompleted();
    expect(mockSimulationManager.save(mockSimulation)).andReturn(mockSimulation);
    Capture<Simulation> captureSimulation = newCapture();
    Capture<List<StatusVO>> captureProgress = newCapture();
    Capture<ResultsVO> captureResults = newCapture();
    SimulationCurrentResultsVO mockResults = mocksControl.createMock(SimulationCurrentResultsVO.class);
    expectNew(SimulationCurrentResultsVO.class, capture(captureSimulation),
                                                capture(captureProgress),
                                                capture(captureResults))
             .andReturn(mockResults);

    replayAll();
    mocksControl.replay();

    clientDirectService.retrieveSimulationResults(dummySimulationId);

    verifyAll();
    mocksControl.verify();

    Simulation capturedSimulation = captureSimulation.getValue();
    assertSame(mockSimulation, capturedSimulation);
    List<StatusVO> capturedProgress = captureProgress.getValue();
    assertFalse(capturedProgress.isEmpty());
    assertNull(captureResults.getValue());
  }

  @Test
  public void testRetrieveSimulationResultsSuccessOnUnfinishedSimulation()
              throws Exception {
    /*
     * Scenario 1 - Progress status indicates simulation incomplete but no 
     *              progress available.
     */
    expect(mockSimulationManager.retrieveSimulation(dummySimulationId))
          .andReturn(mockSimulation);
    expect(mockSimulation.getAppManagerId()).andReturn(dummyAppManagerId);
    expect(mockSimulation.isCompleted()).andReturn(false);
    final List<StatusVO> dummyProgress = new ArrayList<StatusVO>();
    expect(mockAppManagerService.retrieveProgress(dummyAppManagerId))
          .andReturn(dummyProgress);
    Capture<Simulation> captureSimulation = newCapture();
    Capture<List<StatusVO>> captureProgress = newCapture();
    Capture<ResultsVO> captureResults = newCapture();
    SimulationCurrentResultsVO mockResults = mocksControl.createMock(SimulationCurrentResultsVO.class);
    expectNew(SimulationCurrentResultsVO.class, capture(captureSimulation),
                                                capture(captureProgress),
                                                capture(captureResults))
             .andReturn(mockResults);

    replayAll();
    mocksControl.replay();

    clientDirectService.retrieveSimulationResults(dummySimulationId);

    verifyAll();
    mocksControl.verify();

    Simulation capturedSimulation = captureSimulation.getValue();
    assertSame(mockSimulation, capturedSimulation);
    List<StatusVO> capturedProgress = captureProgress.getValue();
    assertTrue(capturedProgress.isEmpty());
    assertNull(captureResults.getValue());

    resetAll();
    mocksControl.reset();

    /*
     * Scenario 2 - Progress status indicates simulation incomplete with progress.
     */
    expect(mockSimulationManager.retrieveSimulation(dummySimulationId))
          .andReturn(mockSimulation);
    expect(mockSimulation.getAppManagerId()).andReturn(dummyAppManagerId);
    expect(mockSimulation.isCompleted()).andReturn(false);
    final StatusVO mockStatus = mocksControl.createMock(StatusVO.class);
    dummyProgress.add(mockStatus);
    expect(mockAppManagerService.retrieveProgress(dummyAppManagerId))
          .andReturn(dummyProgress);
    mockStatic(StatusVO.class);
    expect(StatusVO.statusesIndicatesSimulationCompleted(dummyProgress))
          .andReturn(false);
    expectNew(SimulationCurrentResultsVO.class, capture(captureSimulation),
                                                capture(captureProgress),
                                                capture(captureResults))
             .andReturn(mockResults);

    replayAll();
    mocksControl.replay();

    clientDirectService.retrieveSimulationResults(dummySimulationId);

    verifyAll();
    mocksControl.verify();

    capturedSimulation = captureSimulation.getValue();
    assertSame(mockSimulation, capturedSimulation);
    capturedProgress = captureProgress.getValue();
    assertFalse(capturedProgress.isEmpty());
    assertSame(mockStatus, capturedProgress.get(0));
    assertNull(captureResults.getValue());
  }

  @Test
  public void testRetrieveSimulationSuccessOnFound()
              throws SimulationNotFoundException {

    expect(mockSimulationManager.retrieveSimulation(dummySimulationId))
          .andReturn(mockSimulation);

    mocksControl.replay();

    final Simulation returnedSimulation = clientDirectService.retrieveSimulation(dummySimulationId);

    mocksControl.verify();

    assertNotNull(returnedSimulation);
  }

  @Test(expected=SimulationNotFoundException.class)
  public void testRetrieveSimulationThrowsExceptionIfNotFound()
              throws SimulationNotFoundException {
    expect(mockSimulationManager.retrieveSimulation(dummySimulationId))
          .andThrow(new SimulationNotFoundException());

    mocksControl.replay();

    clientDirectService.retrieveSimulation(dummySimulationId);

    mocksControl.verify();
  }

  @Test
  public void testRetrieveSpreads() {
    final Map<IonCurrent, BigDecimal> dummySpreads = new HashMap<IonCurrent, BigDecimal>();

    expect(mockConfigurationManager.retrieveSpreads()).andReturn(dummySpreads);

    mocksControl.replay();

    final Map<IonCurrent, BigDecimal> retrieved = clientDirectService.retrieveSpreads();

    mocksControl.verify();

    assertSame(dummySpreads, retrieved);
  }

  @Test
  public void testRetrieveUserSimulations() throws DataRetrievalException {
    final String dummyUser = "dummyUser";
    expect(mockSimulationManager.retrieveUserSimulations(dummyUser))
          .andReturn(new ArrayList<Simulation>());

    mocksControl.replay();

    final List<Simulation> returnedSimulations = clientDirectService.retrieveUserSimulations(dummyUser);

    mocksControl.verify();

    assertSame(0, returnedSimulations.size());
  }

  @Test
  public void testSimulationRun() throws RunInvocationException,
                                         AppManagerWSInvocationException,
                                         NoConnectionException {
    final String dummyInitiator = "dummyInitiator";

    expect(mockAppManagerService.runSimulation(mockSimulationInputVO))
          .andReturn(dummyAppManagerId);
    expect(mockSimulationManager.createSimulation(dummyAppManagerId,
                                                  dummyInitiator,
                                                  mockSimulationInputVO))
          .andReturn(dummySimulationId);

    mocksControl.replay();

    final long returnedSimulationId = clientDirectService.runSimulation(mockSimulationInputVO,
                                                                        dummyInitiator);

    mocksControl.verify();

    assertSame(dummySimulationId, returnedSimulationId);
  }

  @Test(expected=RunInvocationException.class)
  public void testSimulationRunFailsOnRunInvocationProblem()
              throws RunInvocationException, AppManagerWSInvocationException,
                     NoConnectionException {
    final String dummyInitiator = "initiator";

    expect(mockAppManagerService.runSimulation(mockSimulationInputVO))
          .andThrow(new RunInvocationException(""));

    mocksControl.replay();

    final long returnedSimulationId = clientDirectService.runSimulation(mockSimulationInputVO,
                                                                        dummyInitiator);

    mocksControl.verify();

    assertSame(0, returnedSimulationId);
  }

  @Test
  public void testStoreFile() {
    FILE_DATA_TYPE dummyFileDataType = null;
    InputStream mockInputStream = null;
    String dummyName = null;
    String dummyUploader = null;

    FileStoreActionOutcomeVO outcome = null;
    try {
      outcome = clientDirectService.storeFile(dummyFileDataType,
                                              mockInputStream, dummyName,
                                              dummyUploader);
      fail("Should not permit a null file data type!");
    } catch (IllegalArgumentException e) {}

    dummyFileDataType = FILE_DATA_TYPE.CELLML;

    try {
      outcome = clientDirectService.storeFile(dummyFileDataType,
                                              mockInputStream, dummyName,
                                              dummyUploader);
      fail("Should not permit a null input stream!");
    } catch (IllegalArgumentException e) {}

    mockInputStream = mocksControl.createMock(InputStream.class);

    try {
      outcome = clientDirectService.storeFile(dummyFileDataType,
                                              mockInputStream, dummyName,
                                              dummyUploader);
      fail("Should not permit a blank uploader!");
    } catch (IllegalArgumentException e) {}

    dummyUploader = "dummyUploader";

    final FileStoreActionOutcomeVO mockOutcome = mocksControl.createMock(FileStoreActionOutcomeVO.class);

    expect(mockFileManager.storeFile(dummyFileDataType, mockInputStream,
                                     dummyName, dummyUploader))
          .andReturn(mockOutcome);

    mocksControl.replay();

    outcome = clientDirectService.storeFile(dummyFileDataType, mockInputStream,
                                            dummyName, dummyUploader);

    mocksControl.verify();

    assertSame(mockOutcome, outcome);
  }
}