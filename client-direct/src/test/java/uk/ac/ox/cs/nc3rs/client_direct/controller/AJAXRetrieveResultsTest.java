/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.Model;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.controller.AJAX.JSON;
import uk.ac.ox.cs.nc3rs.client_direct.controller.util.ControllerUtil;
import uk.ac.ox.cs.nc3rs.client_direct.exception.AppManagerWSInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.NoConnectionException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.ResultsNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.SimulationNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService;
import uk.ac.ox.cs.nc3rs.client_direct.value.ResultsVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationCurrentResultsVO;

/**
 * Unit test the AJAX controller results retrieval.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { AJAX.class, ControllerUtil.class, ObjectMapper.class } )
public class AJAXRetrieveResultsTest {

  private AJAX ajaxController;
  private ClientDirectService mockClientDirectService;
  private IMocksControl mocksControl;
  private Locale locale;
  private MessageSource mockMessageSource;
  private Model mockModel;
  private ObjectMapper mockObjectMapper;
  private ResultsVO mockResultsVO;
  private SimulationCurrentResultsVO mockAllSimulationData;
  private String dummySimulationId;

  @Before
  public void setUp() {
    ajaxController = new AJAX();

    mocksControl = createStrictControl();

    mockClientDirectService = mocksControl.createMock(ClientDirectService.class);
    ReflectionTestUtils.setField(ajaxController,
                                 ClientDirectIdentifiers.COMPONENT_CLIENT_DIRECT_SERVICE,
                                 mockClientDirectService);
    mockMessageSource = mocksControl.createMock(MessageSource.class);
    ajaxController.setMessageSource(mockMessageSource);

    mockAllSimulationData = mocksControl.createMock(SimulationCurrentResultsVO.class);
    mockModel = mocksControl.createMock(Model.class);
    mockObjectMapper = mocksControl.createMock(ObjectMapper.class);
    mockResultsVO = mocksControl.createMock(ResultsVO.class);

    locale = new Locale("en");
    dummySimulationId = "dummySimulationId";
  }

  @Test
  public void testRetrieveResults() throws Exception {
    /*
     * Invalid simulation identifier.
     */
    dummySimulationId = "2";
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
                    .andReturn(mockModel);
    mockStatic(ControllerUtil.class);
    expect(ControllerUtil.validSimulationId(dummySimulationId))
          .andReturn(false);

    Capture<String> captureKey = newCapture();
    Capture<JSON> captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    String retrieved = ajaxController.retrieveResults(dummySimulationId, locale,
                                                      mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                 captureKey.getValue());
    JSON capturedJSON = captureJSON.getValue();
    assertNull(capturedJSON.getJson());
    assertEquals("Invalid Simulation identifier", capturedJSON.getException());
    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, retrieved);

    resetAll();
    mocksControl.reset();

    /*
     * Null simulation data results.
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    expect(mockClientDirectService.retrieveSimulationResults(Long.valueOf(dummySimulationId)))
          .andReturn(mockAllSimulationData);
    // Return no results.
    expect(mockAllSimulationData.getResults()).andReturn(null);
    expectNew(ObjectMapper.class).andReturn(mockObjectMapper);
    final String dummyResultsJSON = "dummyResultsJSON";
    expect(mockObjectMapper.writeValueAsString(mockAllSimulationData))
          .andReturn(dummyResultsJSON);
    captureKey = newCapture();
    captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveResults(dummySimulationId, locale,
                                         mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                 captureKey.getValue());
    capturedJSON = captureJSON.getValue();
    assertEquals(dummyResultsJSON, capturedJSON.getJson());
    assertNull(capturedJSON.getException());

    resetAll();
    mocksControl.reset();

    /*
     * Empty simulation data results.
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    expect(mockClientDirectService.retrieveSimulationResults(Long.valueOf(dummySimulationId)))
          .andReturn(mockAllSimulationData);
    // Return results which are empty.
    expect(mockAllSimulationData.getResults()).andReturn(mockResultsVO);
    expect(mockResultsVO.hasResults()).andReturn(false);
    expectNew(ObjectMapper.class).andReturn(mockObjectMapper);
    expect(mockObjectMapper.writeValueAsString(mockAllSimulationData))
          .andReturn(dummyResultsJSON);
    captureKey = newCapture();
    captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveResults(dummySimulationId, locale,
                                         mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                 captureKey.getValue());
    capturedJSON = captureJSON.getValue();
    assertEquals(dummyResultsJSON, capturedJSON.getJson());
    assertNull(capturedJSON.getException());

    resetAll();
    mocksControl.reset();

    /*
     * Simulation data results processing (although no results for translation).
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    expect(mockClientDirectService.retrieveSimulationResults(Long.valueOf(dummySimulationId)))
          .andReturn(mockAllSimulationData);
    // Return non-empty results.
    expect(mockAllSimulationData.getResults()).andReturn(mockResultsVO);
    expect(mockResultsVO.hasResults()).andReturn(true);
    final Map<String, Object> dummyAvailableResults = new HashMap<String, Object>();
    expect(mockResultsVO.getResults()).andReturn(dummyAvailableResults);
    expectNew(ObjectMapper.class).andReturn(mockObjectMapper);
    expect(mockObjectMapper.writeValueAsString(mockAllSimulationData))
          .andReturn(dummyResultsJSON);
    captureKey = newCapture();
    captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveResults(dummySimulationId, locale,
                   mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                 captureKey.getValue());
    capturedJSON = captureJSON.getValue();
    assertEquals(dummyResultsJSON, capturedJSON.getJson());
    assertNull(capturedJSON.getException());

    resetAll();
    mocksControl.reset();

    /*
     * Simulation data results processing (empty set for translation).
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    expect(mockClientDirectService.retrieveSimulationResults(Long.valueOf(dummySimulationId)))
          .andReturn(mockAllSimulationData);
    // Return non-empty results.
    expect(mockAllSimulationData.getResults()).andReturn(mockResultsVO);
    expect(mockResultsVO.hasResults()).andReturn(true);
    final Set<BigDecimal> dummyRefsRequireTranslation = new HashSet<BigDecimal>();
    dummyAvailableResults.put(ClientDirectIdentifiers.KEY_RESULTS_FOR_TRANSLATION,
                              dummyRefsRequireTranslation);
    expect(mockResultsVO.getResults()).andReturn(dummyAvailableResults);
    expectNew(ObjectMapper.class).andReturn(mockObjectMapper);
    expect(mockObjectMapper.writeValueAsString(mockAllSimulationData))
          .andReturn(dummyResultsJSON);
    captureKey = newCapture();
    captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveResults(dummySimulationId, locale,
                   mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                 captureKey.getValue());
    capturedJSON = captureJSON.getValue();
    assertEquals(dummyResultsJSON, capturedJSON.getJson());
    assertNull(capturedJSON.getException());

    resetAll();
    mocksControl.reset();

    /*
     * Simulation data results processing.
     * Translation takes place.
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    expect(mockClientDirectService.retrieveSimulationResults(Long.valueOf(dummySimulationId)))
          .andReturn(mockAllSimulationData);
    // Return non-empty results.
    expect(mockAllSimulationData.getResults()).andReturn(mockResultsVO);
    expect(mockResultsVO.hasResults()).andReturn(true);
    final BigDecimal dummyRefRequiringTranslation = BigDecimal.ONE;
    dummyRefsRequireTranslation.add(dummyRefRequiringTranslation);
    dummyAvailableResults.put(ClientDirectIdentifiers.KEY_RESULTS_FOR_TRANSLATION,
                              dummyRefsRequireTranslation);
    final Map<BigDecimal, Map<String, Object>> dummyAllRefsResults = new HashMap<BigDecimal, Map<String, Object>>();
    final Map<String, Object> dummyAllRefsResult = new HashMap<String, Object>();
    final String dummyAPD90 = "dummyAPD90";
    dummyAllRefsResult.put(ClientDirectIdentifiers.KEY_RESULTS_PERREF_APD90,
                           dummyAPD90);
    final String dummyDeltaAPD90 = "dummyDeltaAPD90";
    dummyAllRefsResult.put(ClientDirectIdentifiers.KEY_RESULTS_PERREF_DELTA_APD90,
                           dummyDeltaAPD90);
    dummyAllRefsResults.put(dummyRefRequiringTranslation, dummyAllRefsResult);
    dummyAvailableResults.put(ClientDirectIdentifiers.KEY_RESULTS_REFERENCE_DATA,
                              dummyAllRefsResults);
    /* dummyAvailableResults :
       {
         referenceData = {
                           1 = {
                                 deltaAPD90 = dummyDeltaAPD90,
                                 apd90 = dummyAPD90
                               } 
                         },
         forTranslation=[1]
       }
    */
    expect(mockResultsVO.getResults()).andReturn(dummyAvailableResults);
    // >> translator() >> queryMessageSource()
    final String dummyTranslatedValue = "dummyTranslatedValue";
    expect(mockMessageSource.getMessage(isA(String.class), anyObject(Object[].class),
                                        isA(Locale.class)))
          .andReturn(dummyTranslatedValue).anyTimes();
    // << translator() << queryMessageSource()
    expectNew(ObjectMapper.class).andReturn(mockObjectMapper);
    expect(mockObjectMapper.writeValueAsString(mockAllSimulationData))
          .andReturn(dummyResultsJSON);
    captureKey = newCapture();
    captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveResults(dummySimulationId, locale,
                   mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                 captureKey.getValue());
    capturedJSON = captureJSON.getValue();
    assertEquals(dummyResultsJSON, capturedJSON.getJson());
    assertNull(capturedJSON.getException());
    // Lazy way to extract the value from Map structure!
    assertTrue(dummyAvailableResults.toString().contains("apd90=dummyTranslatedValue"));
    assertTrue(dummyAvailableResults.toString().contains("deltaAPD90=dummyTranslatedValue"));

    resetAll();
    mocksControl.reset();

    /*
     * Simulation data results processing (no translation).
     * IMPORTANT! The previous step translated dummy Map content!!
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    expect(mockClientDirectService.retrieveSimulationResults(Long.valueOf(dummySimulationId)))
          .andReturn(mockAllSimulationData);
    // Return non-empty results.
    expect(mockAllSimulationData.getResults()).andReturn(mockResultsVO);
    expect(mockResultsVO.hasResults()).andReturn(true);
    /* dummyAvailableResults (as translated from previous step!):
       {
         referenceData = {
                           1 = {
                                 deltaAPD90 = dummyTranslatedValue,
                                 apd90 = dummyTranslatedValue
                               } 
                         },
         forTranslation=[1]
       }
    */
    expect(mockResultsVO.getResults()).andReturn(dummyAvailableResults);
    // >> translator() >> queryMessageSource()
    // IMPORTANT! No translation takes place!
    expect(mockMessageSource.getMessage(isA(String.class), anyObject(Object[].class),
                                        isA(Locale.class)))
          .andReturn(dummyTranslatedValue).anyTimes();
    // << translator() << queryMessageSource()
    expectNew(ObjectMapper.class).andReturn(mockObjectMapper);
    expect(mockObjectMapper.writeValueAsString(mockAllSimulationData))
          .andReturn(dummyResultsJSON);
    captureKey = newCapture();
    captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveResults(dummySimulationId, locale,
                   mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                 captureKey.getValue());
    capturedJSON = captureJSON.getValue();
    assertEquals(dummyResultsJSON, capturedJSON.getJson());
    assertNull(capturedJSON.getException());

    resetAll();
    mocksControl.reset();

    /*
     * Test JSON mapping exception
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    expect(mockClientDirectService.retrieveSimulationResults(Long.valueOf(dummySimulationId)))
          .andReturn(mockAllSimulationData);
    // Return non-empty results.
    expect(mockAllSimulationData.getResults()).andReturn(mockResultsVO);
    expect(mockResultsVO.hasResults()).andReturn(true);
    expect(mockResultsVO.getResults()).andReturn(dummyAvailableResults);
    // >> translator() >> queryMessageSource()
    expect(mockMessageSource.getMessage(isA(String.class), anyObject(Object[].class),
                                        isA(Locale.class)))
          .andReturn(dummyTranslatedValue).anyTimes();
    // << translator() << queryMessageSource()
    expectNew(ObjectMapper.class).andReturn(mockObjectMapper);
    final String dummyJsonMappingException = "dummyJsonMappingException";
    expect(mockObjectMapper.writeValueAsString(mockAllSimulationData))
          .andThrow(new JsonMappingException(dummyJsonMappingException));
    captureKey = newCapture();
    captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveResults(dummySimulationId, locale,
                                               mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                 captureKey.getValue());
    capturedJSON = captureJSON.getValue();
    assertNull(capturedJSON.getJson());
    assertEquals("Application failure converting simulation results for display",
                 capturedJSON.getException());

    resetAll();
    mocksControl.reset();

    /*
     * Test other exception from ObjectMapper
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    expect(mockClientDirectService.retrieveSimulationResults(Long.valueOf(dummySimulationId)))
          .andReturn(mockAllSimulationData);
    // Return non-empty results.
    expect(mockAllSimulationData.getResults()).andReturn(mockResultsVO);
    expect(mockResultsVO.hasResults()).andReturn(true);
    expect(mockResultsVO.getResults()).andReturn(dummyAvailableResults);
    // >> translator() >> queryMessageSource()
    expect(mockMessageSource.getMessage(isA(String.class), anyObject(Object[].class),
                                        isA(Locale.class)))
          .andReturn(dummyTranslatedValue).anyTimes();
    // << translator() << queryMessageSource()
    expectNew(ObjectMapper.class).andReturn(mockObjectMapper);
    final String dummyJsonGenerationException = "dummyJsonGenerationException";
    expect(mockObjectMapper.writeValueAsString(mockAllSimulationData))
          .andThrow(new JsonGenerationException(dummyJsonGenerationException));
    captureKey = newCapture();
    captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveResults(dummySimulationId, locale,
                                               mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                 captureKey.getValue());
    capturedJSON = captureJSON.getValue();
    assertNull(capturedJSON.getJson());
    assertEquals(dummyJsonGenerationException, capturedJSON.getException());

    resetAll();
    mocksControl.reset();

    /*
     * App manager WS exception
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    final String dummyAppManagerWSInvocationException = "dummyAppManagerWSInvocationException";
    expect(mockClientDirectService.retrieveSimulationResults(Long.valueOf(dummySimulationId)))
          .andThrow(new AppManagerWSInvocationException(dummyAppManagerWSInvocationException));
    // >> queryMessageSource()
    expect(mockMessageSource.getMessage(isA(String.class), anyObject(Object[].class),
                                        isA(Locale.class)))
          .andThrow(new NoSuchMessageException("")).anyTimes();
    // << queryMessageSource()
    captureKey = newCapture();
    captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveResults(dummySimulationId, locale,
                                               mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                 captureKey.getValue());
    capturedJSON = captureJSON.getValue();
    assertNull(capturedJSON.getJson());
    assertEquals(ClientDirectIdentifiers.APP_MANAGER_SOMETHING_WRONG,
                 capturedJSON.getException());

    resetAll();
    mocksControl.reset();

    /*
     * No connection exception
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    final String dummyNoConnectionException = "dummyNoConnectionException";
    expect(mockClientDirectService.retrieveSimulationResults(Long.valueOf(dummySimulationId)))
          .andThrow(new NoConnectionException(dummyNoConnectionException));
    // >> queryMessageSource()
    expect(mockMessageSource.getMessage(isA(String.class), anyObject(Object[].class),
                                        isA(Locale.class)))
          .andThrow(new NoSuchMessageException("")).anyTimes();
    // << queryMessageSource()
    captureKey = newCapture();
    captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveResults(dummySimulationId, locale,
                                               mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                 captureKey.getValue());
    capturedJSON = captureJSON.getValue();
    assertNull(capturedJSON.getJson());
    assertEquals(ClientDirectIdentifiers.APP_MANAGER_OUT_OF_CONTACT,
                 capturedJSON.getException());

    resetAll();
    mocksControl.reset();

    /*
     * Results not found exception.
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    final String dummyResultsNotFoundException = "dummyResultsNotFoundException";
    expect(mockClientDirectService.retrieveSimulationResults(Long.valueOf(dummySimulationId)))
          .andThrow(new ResultsNotFoundException(dummyResultsNotFoundException));
    // >> queryMessageSource()
    expect(mockMessageSource.getMessage(isA(String.class), anyObject(Object[].class),
                                        isA(Locale.class)))
          .andThrow(new NoSuchMessageException("")).anyTimes();
    // << queryMessageSource()
    captureKey = newCapture();
    captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveResults(dummySimulationId, locale,
                                               mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                 captureKey.getValue());
    capturedJSON = captureJSON.getValue();
    assertNull(capturedJSON.getJson());
    assertEquals(dummyResultsNotFoundException, capturedJSON.getException());

    resetAll();
    mocksControl.reset();

    /*
     * Simulation not found exception.
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    final String dummySimulationNotFoundException = "dummySimulationNotFoundException";
    expect(mockClientDirectService.retrieveSimulationResults(Long.valueOf(dummySimulationId)))
          .andThrow(new SimulationNotFoundException(dummySimulationNotFoundException));
    // >> queryMessageSource()
    expect(mockMessageSource.getMessage(isA(String.class), anyObject(Object[].class),
                                        isA(Locale.class)))
          .andThrow(new NoSuchMessageException("")).anyTimes();
    // << queryMessageSource()
    captureKey = newCapture();
    captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveResults(dummySimulationId, locale,
                                               mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                 captureKey.getValue());
    capturedJSON = captureJSON.getValue();
    assertNull(capturedJSON.getJson());
    assertEquals(dummySimulationNotFoundException, capturedJSON.getException());

    resetAll();
    mocksControl.reset();

    /*
     * Simulation has empty PK data.
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    expect(mockClientDirectService.retrieveSimulationResults(Long.valueOf(dummySimulationId)))
          .andReturn(mockAllSimulationData);
    // Return non-empty results.
    expect(mockAllSimulationData.getResults()).andReturn(mockResultsVO);
    expect(mockResultsVO.hasResults()).andReturn(true);
    expect(mockResultsVO.getResults()).andReturn(dummyAvailableResults);

    final Map<BigDecimal, Map<String, Object>> dummyAvailablePKResultsData =
          new HashMap<BigDecimal, Map<String, Object>>();
    // Put some empty PK results data in
    dummyAvailableResults.put(ClientDirectIdentifiers.KEY_PKRESULTS_TIMEPOINT_DATA,
                              dummyAvailablePKResultsData);
    // Remove the results reference data.
    dummyAvailableResults.remove(ClientDirectIdentifiers.KEY_RESULTS_FOR_TRANSLATION);
    dummyAvailableResults.remove(ClientDirectIdentifiers.KEY_RESULTS_REFERENCE_DATA);
    /* dummyAvailableResults (as translated from previous step!):
    {
      timepointData = {}
    }
    */
    expectNew(ObjectMapper.class).andReturn(mockObjectMapper);
    expect(mockObjectMapper.writeValueAsString(mockAllSimulationData))
          .andReturn(dummyResultsJSON);
    expect(mockModel.addAttribute(isA(String.class), anyObject()))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveResults(dummySimulationId, locale,
                                               mockModel);

    verifyAll();
    mocksControl.verify();

    resetAll();
    mocksControl.reset();

    /*
     * Simulation has PK data - no translation required.
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    expect(mockClientDirectService.retrieveSimulationResults(Long.valueOf(dummySimulationId)))
          .andReturn(mockAllSimulationData);
    // Return non-empty results.
    expect(mockAllSimulationData.getResults()).andReturn(mockResultsVO);
    expect(mockResultsVO.hasResults()).andReturn(true);
    expect(mockResultsVO.getResults()).andReturn(dummyAvailableResults);
    final Map<String, Object> dummyPerTimepointPKData = new HashMap<String, Object>();
    dummyPerTimepointPKData.put(ClientDirectIdentifiers.KEY_PKRESULTS_PERTIMEPOINT_FORTRANS,
                                false);
    final String dummyAPD901 = BigDecimal.TEN.toString();
    dummyPerTimepointPKData.put(ClientDirectIdentifiers.KEY_PKRESULTS_PERTIMEPOINT_APD90S,
                                dummyAPD901);
    dummyAvailablePKResultsData.put(BigDecimal.ONE, dummyPerTimepointPKData);
    dummyAvailableResults.put(ClientDirectIdentifiers.KEY_PKRESULTS_TIMEPOINT_DATA,
                              dummyAvailablePKResultsData);
    /* dummyAvailableResults (as translated from previous step!):
    {
      timepointData = {
        1 = { forTranslation = false }
      }
    }
    */
    expectNew(ObjectMapper.class).andReturn(mockObjectMapper);
    expect(mockObjectMapper.writeValueAsString(mockAllSimulationData))
          .andReturn(dummyResultsJSON);
    expect(mockModel.addAttribute(isA(String.class), anyObject()))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveResults(dummySimulationId, locale,
                                               mockModel);

    verifyAll();
    mocksControl.verify();

    resetAll();
    mocksControl.reset();

    /*
     * Simulation has PK data - translation required.
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    expect(mockClientDirectService.retrieveSimulationResults(Long.valueOf(dummySimulationId)))
          .andReturn(mockAllSimulationData);
    // Return non-empty results.
    expect(mockAllSimulationData.getResults()).andReturn(mockResultsVO);
    expect(mockResultsVO.hasResults()).andReturn(true);
    expect(mockResultsVO.getResults()).andReturn(dummyAvailableResults);
    dummyPerTimepointPKData.put(ClientDirectIdentifiers.KEY_PKRESULTS_PERTIMEPOINT_FORTRANS,
                                true);
    final String dummyAPD902 = dummyTranslatedValue;
    final String dummyAPD903 = "dummyAPD903";
    dummyPerTimepointPKData.put(ClientDirectIdentifiers.KEY_PKRESULTS_PERTIMEPOINT_APD90S,
                                dummyAPD901.concat(",").concat(dummyAPD902)
                                .concat(",").concat(dummyAPD903));
    /* dummyAvailableResults (as translated from previous step!):
    {
      timepointData = {
        1 = { 
          forTranslation = false,
          apd90s = dummyAPD901,dummyAPD902
        }
      }
    }
    */
    // >> translator() >> queryMessageSource()
    expect(mockMessageSource.getMessage(isA(String.class), anyObject(Object[].class),
                                        isA(Locale.class)))
          .andReturn(dummyTranslatedValue).anyTimes();
    // << translator() << queryMessageSource()
    expectNew(ObjectMapper.class).andReturn(mockObjectMapper);
    expect(mockObjectMapper.writeValueAsString(mockAllSimulationData))
          .andReturn(dummyResultsJSON);
    captureKey = newCapture();
    captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveResults(dummySimulationId, locale,
                                               mockModel);

    verifyAll();
    mocksControl.verify();

    capturedJSON = captureJSON.getValue();
    assertTrue(dummyAvailableResults.toString().contains(",dummyTranslatedValue"));

    resetAll();
    mocksControl.reset();

    /*
     * Unexpected general exception.
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    final String dummyException = "dummyException";
    expect(mockClientDirectService.retrieveSimulationResults(Long.valueOf(dummySimulationId)))
          .andThrow(new UnsupportedOperationException(dummyException));

    captureKey = newCapture();
    captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveResults(dummySimulationId, locale,
                                               mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                 captureKey.getValue());
    capturedJSON = captureJSON.getValue();
    assertNull(capturedJSON.getJson());
    assertTrue(capturedJSON.getException().contains(dummyException));
  }
}