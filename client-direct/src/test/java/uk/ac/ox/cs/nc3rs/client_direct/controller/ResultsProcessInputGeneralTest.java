/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.anyString;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.isNull;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.security.Principal;

import org.easymock.Capture;
import org.junit.Test;
import org.springframework.ui.Model;

import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.compbio.client_shared.value.PortalFeature;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.controller.util.ControllerUtil;
import uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService;
import uk.ac.ox.cs.nc3rs.client_direct.value.InputOption;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;

/**
 * Unit test the results controller's input processing.
 * 
 * @author geoff
 */
public class ResultsProcessInputGeneralTest extends AbstractResultsProcessInputTest {

  @Test
  public void testActiveTesting() throws Exception {
    defaultPortalFeatureProcessing();

    SimulationInputVO mockSimulationInput = assignSimulationInputProperties(null,
                                                                            null,
                                                                            false);

    activeTesting(mockSimulationInput);

    replayAll();
    mocksControl.replay();

    final String returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_RESULTS, returnPage);
  }

  @Test
  public void testInputOptionProcessingAssignment() throws Exception {
    /*
     * Invalid InputOption enum.
     */
    dummyPkOrConcs = "dummyPkOrConcs";

    defaultPortalFeatureProcessing();
    final Capture<String> captureErrorMessage = newCapture();
    mockStatic(Main.class);
    Main.setModelPropertiesForMain(isA(ClientDirectService.class),
                                   isA(Principal.class), isA(Model.class),
                                   capture(captureErrorMessage));

    replayAll();
    mocksControl.replay();

    String returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_MAIN, returnPage);
    assertTrue(captureErrorMessage.getValue()
               .startsWith("Input options must be one of "));

    resetAll();
    mocksControl.reset();

    /*
     * Testing the concentration points.
     */
    dummyPlasmaConcPoints = new String[] { "dummyPlasmaConcPoint" };
    dummyPlasmaConcMax = "dummyPlasmaConcMax";
    dummyPlasmaConcMin = "dummyPlasmaConcMin";
    dummyPlasmaIntPtCount = "dummyPlasmaIntPtCount";
    dummyPlasmaIntPtLogScale = "dummyPlasmaInputPtLogScale";

    dummyPkOrConcs = InputOption.CONC_POINTS.toString();
    SimulationInputVO mockSimulationInput = mocksControl.createMock(SimulationInputVO.class);

    defaultPortalFeatureProcessing();
    // Purposely not calling assignSimulationInputProperties()!
    SimulationInputVO.Builder mockBuilder = mocksControl.createMock(SimulationInputVO.Builder.class);
    expectNew(SimulationInputVO.Builder.class).andReturn(mockBuilder);
    expect(mockBuilder.modelIdentifier(dummyModelIdentifier))
          .andReturn(mockBuilder);
    expect(mockBuilder.pacingFrequency(dummyPacingFrequency))
          .andReturn(mockBuilder);
    expect(mockBuilder.pacingMaxTime(dummyPacingMaxTime)).andReturn(mockBuilder);
    assignMeasurementObservations(mockBuilder, false);
    expect(mockBuilder.c50Type(dummyC50Type)).andReturn(mockBuilder);
    expect(mockBuilder.ic50Units(dummyIC50Units)).andReturn(mockBuilder);
    // Note the null values!
    expect(mockBuilder.plasmaConcMin(null)).andReturn(mockBuilder);
    expect(mockBuilder.plasmaConcMax(null)).andReturn(mockBuilder);
    expect(mockBuilder.plasmaIntPtCount(null)).andReturn(mockBuilder);
    expect(mockBuilder.plasmaIntPtLogScale(Boolean.FALSE.toString()))
          .andReturn(mockBuilder);
    expect(mockBuilder.plasmaConcPoints(dummyPlasmaConcPoints))
          .andReturn(mockBuilder);
    expect(mockBuilder.pkFile(null)).andReturn(mockBuilder);
    expect(mockBuilder.cellMLFile(null)).andReturn(mockBuilder);
    expect(mockBuilder.credibleIntervalPctiles(dummyCIPctiles))
          .andReturn(mockBuilder);
    expect(mockBuilder.notes(dummyNotes)).andReturn(mockBuilder);
    expect(mockBuilder.build()).andReturn(mockSimulationInput);

    latterTesting(mockSimulationInput);

    replayAll();
    mocksControl.replay();

    returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_RESULTS, returnPage);

    resetAll();
    mocksControl.reset();

    /*
     * Test concentration range and setting plasma int log scale definition.
     */
    dummyPkOrConcs = InputOption.CONC_RANGE.toString();
    dummyPlasmaIntPtLogScale = Results.BOX_CHECKED_VALUE;

    defaultPortalFeatureProcessing();
    // Purposely not calling assignSimulationInputProperties()!
    expectNew(SimulationInputVO.Builder.class).andReturn(mockBuilder);
    expect(mockBuilder.modelIdentifier(dummyModelIdentifier))
          .andReturn(mockBuilder);
    expect(mockBuilder.pacingFrequency(dummyPacingFrequency))
          .andReturn(mockBuilder);
    expect(mockBuilder.pacingMaxTime(dummyPacingMaxTime)).andReturn(mockBuilder);
    assignMeasurementObservations(mockBuilder, false);
    expect(mockBuilder.c50Type(dummyC50Type)).andReturn(mockBuilder);
    expect(mockBuilder.ic50Units(dummyIC50Units)).andReturn(mockBuilder);
    expect(mockBuilder.plasmaConcMin(dummyPlasmaConcMin)).andReturn(mockBuilder);
    expect(mockBuilder.plasmaConcMax(dummyPlasmaConcMax)).andReturn(mockBuilder);
    expect(mockBuilder.plasmaIntPtCount(dummyPlasmaIntPtCount))
          .andReturn(mockBuilder);
    expect(mockBuilder.plasmaIntPtLogScale(Boolean.TRUE.toString()))
          .andReturn(mockBuilder);
    expect(mockBuilder.plasmaConcPoints(null)).andReturn(mockBuilder);
    expect(mockBuilder.pkFile(null)).andReturn(mockBuilder);
    expect(mockBuilder.cellMLFile(null)).andReturn(mockBuilder);
    expect(mockBuilder.credibleIntervalPctiles(dummyCIPctiles))
          .andReturn(mockBuilder);
    expect(mockBuilder.notes(dummyNotes)).andReturn(mockBuilder);
    expect(mockBuilder.build()).andReturn(mockSimulationInput);

    latterTesting(mockSimulationInput);

    replayAll();
    mocksControl.replay();

    returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_RESULTS, returnPage);
  }

  @Test
  public void testCredibleIntervalPctiles() throws Exception {
    /*
     * 1. Credible interval percentiles specified but uncertainties disallowed. 
     */
    dummyCIPctiles = "dummyCIPctiles";

    defaultPortalFeatureProcessing();

    // Below is derived from assignSimulationInputProperties(null, null, false);
    final SimulationInputVO mockSimulationInput = mocksControl.createMock(SimulationInputVO.class);
    final SimulationInputVO.Builder mockBuilder = mocksControl.createMock(SimulationInputVO.Builder.class);
    expectNew(SimulationInputVO.Builder.class).andReturn(mockBuilder);
    expect(mockBuilder.modelIdentifier(anyString())).andReturn(mockBuilder);
    expect(mockBuilder.pacingFrequency(anyString())).andReturn(mockBuilder);
    expect(mockBuilder.pacingMaxTime(anyString())).andReturn(mockBuilder);
    assignMeasurementObservations(mockBuilder, false);
    expect(mockBuilder.c50Type(anyString())).andReturn(mockBuilder);
    expect(mockBuilder.ic50Units(anyString())).andReturn(mockBuilder);
    expect(mockBuilder.plasmaConcMin(anyString())).andReturn(mockBuilder);
    expect(mockBuilder.plasmaConcMax(anyString())).andReturn(mockBuilder);
    expect(mockBuilder.plasmaIntPtCount(anyString())).andReturn(mockBuilder);
    expect(mockBuilder.plasmaIntPtLogScale(anyString())).andReturn(mockBuilder);
    expect(mockBuilder.plasmaConcPoints(anyObject(String[].class)))
          .andReturn(mockBuilder);
    expect(mockBuilder.pkFile(anyObject(PortalFile.class)))
          .andReturn(mockBuilder);
    expect(mockBuilder.cellMLFile(anyObject(PortalFile.class)))
          .andReturn(mockBuilder);
    /* Even though credible interval pctiles are specified, the value is
       nullified as we're not allowing the uncertainties feature. */ 
    expect(mockBuilder.credibleIntervalPctiles(isNull(String.class)))
          .andReturn(mockBuilder);
    expect(mockBuilder.notes(anyString())).andReturn(mockBuilder);
    expect(mockBuilder.build()).andReturn(mockSimulationInput);

    activeTesting(mockSimulationInput);

    replayAll();
    mocksControl.replay();

    String returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_RESULTS, returnPage);

    resetAll();
    mocksControl.reset();

    /*
     * 2. Credible interval percentiles specified, spread box ticked, and 
     *    uncertainties feature allowed.
     */
    dummySpreadsEnabled = Results.BOX_CHECKED_VALUE;
    dummyAllowUncertainties = true;

    // Below is derived from defaultPortalFeatureProcessing();
    expect(ControllerUtil.allowFeature(PortalFeature.DYNAMIC_CELLMLS,
                                       mockClientDirectService))
          .andReturn(dummyAllowDynamicCellML);
    expect(ControllerUtil.allowFeature(PortalFeature.UNCERTAINTIES,
                                       mockClientDirectService))
          .andReturn(dummyAllowUncertainties);

    // Below is derived from assignSimulationInputProperties(null, null, false);
    expectNew(SimulationInputVO.Builder.class).andReturn(mockBuilder);
    expect(mockBuilder.modelIdentifier(anyString())).andReturn(mockBuilder);
    expect(mockBuilder.pacingFrequency(anyString())).andReturn(mockBuilder);
    expect(mockBuilder.pacingMaxTime(anyString())).andReturn(mockBuilder);
    assignMeasurementObservations(mockBuilder, false);
    expect(mockBuilder.c50Type(anyString())).andReturn(mockBuilder);
    expect(mockBuilder.ic50Units(anyString())).andReturn(mockBuilder);
    expect(mockBuilder.plasmaConcMin(anyString())).andReturn(mockBuilder);
    expect(mockBuilder.plasmaConcMax(anyString())).andReturn(mockBuilder);
    expect(mockBuilder.plasmaIntPtCount(anyString())).andReturn(mockBuilder);
    expect(mockBuilder.plasmaIntPtLogScale(anyString())).andReturn(mockBuilder);
    expect(mockBuilder.plasmaConcPoints(anyObject(String[].class)))
          .andReturn(mockBuilder);
    expect(mockBuilder.pkFile(anyObject(PortalFile.class)))
          .andReturn(mockBuilder);
    expect(mockBuilder.cellMLFile(anyObject(PortalFile.class)))
          .andReturn(mockBuilder);
    /* Credible interval pctiles value not nullified, but passed through. */ 
    expect(mockBuilder.credibleIntervalPctiles(dummyCIPctiles))
          .andReturn(mockBuilder);
    expect(mockBuilder.notes(anyString())).andReturn(mockBuilder);
    expect(mockBuilder.build()).andReturn(mockSimulationInput);

    activeTesting(mockSimulationInput);

    replayAll();
    mocksControl.replay();

    returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_RESULTS, returnPage);
  }
}