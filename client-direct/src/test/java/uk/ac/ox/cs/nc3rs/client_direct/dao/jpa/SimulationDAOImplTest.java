/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.dao.jpa;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.junit.Assert.assertSame;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.nc3rs.client_direct.dao.SimulationDAO;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Job;
import uk.ac.ox.cs.nc3rs.client_direct.entity.JobPKResult;
import uk.ac.ox.cs.nc3rs.client_direct.entity.JobResult;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation;

/**
 * Unit test the Simulation DAO implementation.
 *
 * @author geoff
 */
public class SimulationDAOImplTest {

  private boolean dummyHasPKResults;
  private boolean dummyHasResults;
  private EntityManager mockEntityManager;
  private IMocksControl mocksControl;
  private Job mockJob;
  private long dummySimulationId;
  private Query mockQuery;
  private Simulation mockSimulation;
  private SimulationDAO simulationDAO;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockEntityManager = mocksControl.createMock(EntityManager.class);
    simulationDAO = new SimulationDAOImpl();

    ReflectionTestUtils.setField(simulationDAO, "entityManager",
                                 mockEntityManager);

    dummySimulationId = 23l;
    mockJob = mocksControl.createMock(Job.class);
    mockQuery = mocksControl.createMock(Query.class);
    mockSimulation = mocksControl.createMock(Simulation.class);

    dummyHasResults = false;
    dummyHasPKResults = false;
  }

  private void avoidJobLazyInit() {
    // >> avoidJobLazyInit()
    expect(mockJob.hasResults()).andReturn(dummyHasResults);
    expect(mockJob.hasPKResults()).andReturn(dummyHasPKResults);
    // << avoidJobLazyInit()
  }

  private void findSimulationById() {
    // >> findBySimulationId()
    expect(mockEntityManager.find(Simulation.class, dummySimulationId))
          .andReturn(mockSimulation);
    // << findBySimulationId()    
  }

  @Test
  public void testAvoidJobLazyInit() {
    /*
     * Null job
     */
    final Job dummyJob = null;

    mocksControl.replay();

    simulationDAO.avoidJobLazyInit(dummyJob);

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Job and has all results.
     */
    dummyHasResults = true;
    dummyHasPKResults = true;
    expect(mockJob.hasResults()).andReturn(dummyHasResults);
    final Set<JobResult> dummyJobResults = new HashSet<JobResult>();
    expect(mockJob.getJobResults()).andReturn(dummyJobResults);
    expect(mockJob.hasPKResults()).andReturn(dummyHasPKResults);
    final Set<JobPKResult> dummyJobPKResults = new HashSet<JobPKResult>();
    expect(mockJob.getJobPKResults()).andReturn(dummyJobPKResults);

    mocksControl.replay();

    simulationDAO.avoidJobLazyInit(mockJob);

    mocksControl.verify();
  }

  @Test
  public void testDeleteSimulation() {
    // >> findBySimulationId()
    expect(mockEntityManager.find(Simulation.class, dummySimulationId))
          .andReturn(null);
    // << findBySimulationId()

    mocksControl.replay();

    simulationDAO.deleteSimulation(dummySimulationId);

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Simulation not yet completed and is not taking too long
     */
    findSimulationById();
    boolean dummyIsCompleted = false;
    expect(mockSimulation.isCompleted()).andReturn(dummyIsCompleted);
    boolean dummyIsTakingTooLong = false;
    expect(mockSimulation.isTakingTooLongToComplete()).andReturn(dummyIsTakingTooLong);

    mocksControl.replay();

    simulationDAO.deleteSimulation(dummySimulationId);

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Simulation not yet complete and is taking too long but no job or PK results
     */
    findSimulationById();
    expect(mockSimulation.isCompleted()).andReturn(dummyIsCompleted);
    dummyIsTakingTooLong = true;
    expect(mockSimulation.isTakingTooLongToComplete()).andReturn(dummyIsTakingTooLong);
    expect(mockSimulation.getJob()).andReturn(mockJob);
    avoidJobLazyInit();
    mockEntityManager.remove(mockSimulation);

    mocksControl.replay();

    simulationDAO.deleteSimulation(dummySimulationId);

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Simulation not yet complete and is taking too long with job results
     */
    findSimulationById();
    expect(mockSimulation.isCompleted()).andReturn(dummyIsCompleted);
    dummyIsTakingTooLong = true;
    expect(mockSimulation.isTakingTooLongToComplete()).andReturn(dummyIsTakingTooLong);
    expect(mockSimulation.getJob()).andReturn(mockJob);
    avoidJobLazyInit();
    mockEntityManager.remove(mockSimulation);

    mocksControl.replay();

    simulationDAO.deleteSimulation(dummySimulationId);

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Simulation completed
     */
    findSimulationById();
    dummyIsCompleted = true;
    expect(mockSimulation.isCompleted()).andReturn(dummyIsCompleted);
    expect(mockSimulation.getJob()).andReturn(mockJob);
    avoidJobLazyInit();
    mockEntityManager.remove(mockSimulation);

    mocksControl.replay();

    simulationDAO.deleteSimulation(dummySimulationId);

    mocksControl.verify();
  }

  @Test
  public void testFindBySimulationId() {
    findSimulationById();

    mocksControl.replay();

    final Simulation returnedSimulation = simulationDAO.findBySimulationId(dummySimulationId);

    mocksControl.verify();
    assertSame(mockSimulation, returnedSimulation);
  }

  @Test
  public void testFindByUser() {
    final Query mockQuery = mocksControl.createMock(Query.class);
    expect(mockEntityManager.createNamedQuery(Simulation.QUERY_SIMULATION_BY_CREATOR_ORDERED))
          .andReturn(mockQuery);
    final String dummyUser = "dummyUser";
    expect(mockQuery.setParameter(Simulation.PROPERTY_CREATOR, dummyUser))
          .andReturn(mockQuery);
    final List<Simulation> dummySimulations = new ArrayList<Simulation>();
    expect(mockQuery.getResultList()).andReturn(dummySimulations);

    mocksControl.replay();

    final List<Simulation> returnedSimulations = simulationDAO.findByUser(dummyUser);

    mocksControl.verify();
    assertSame(dummySimulations, returnedSimulations);
  }

  @Test
  public void testFindSimulations() {
    expect(mockEntityManager.createNamedQuery(Simulation.QUERY_SIMULATION_ALL)).andReturn(mockQuery);
    final List<Simulation> dummySimulations = new ArrayList<Simulation>();
    expect(mockQuery.getResultList()).andReturn(dummySimulations);

    mocksControl.replay();

    final List<Simulation> returnedSimulations = simulationDAO.findSimulations();

    mocksControl.verify();
    assertSame(dummySimulations, returnedSimulations);
  }

  @Test
  public void testStore() {
    // Emulate an entity manager persist operation
    expect(mockSimulation.getId()).andReturn(null);
    mockEntityManager.persist(mockSimulation);
    expectLastCall().once();

    mocksControl.replay();

    Simulation returnedSimulation = simulationDAO.store(mockSimulation);

    mocksControl.verify();
    assertSame(mockSimulation, returnedSimulation);

    mocksControl.reset();

    // Emulate an entity manager merge operation
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockEntityManager.merge(mockSimulation)).andReturn(mockSimulation);

    mocksControl.replay();

    returnedSimulation = simulationDAO.store(mockSimulation);

    mocksControl.verify();
    assertSame(mockSimulation, returnedSimulation);
  }

  @Test
  public void testStoreResults() {
    final long dummyAppManagerId = 4l; 
    expect(mockJob.getAppManagerId()).andReturn(dummyAppManagerId);
    expect(mockEntityManager.merge(mockJob)).andReturn(mockJob);
    avoidJobLazyInit();

    mocksControl.replay();

    final Job returnedJob = simulationDAO.storeResults(mockJob);

    mocksControl.verify();
    assertSame(mockJob, returnedJob);
  }
}