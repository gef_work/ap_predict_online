/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.Model;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation;
import uk.ac.ox.cs.nc3rs.client_direct.exception.DataRetrievalException;
import uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService;
import uk.ac.ox.cs.nc3rs.client_direct.value.ModelVO;

/**
 * Unit test the Main controller.
 * 
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { Main.class } )
public class MainTest {

  private ClientDirectService mockClientDirectService;
  private IMocksControl mocksControl;
  private Main mainController;
  private Model mockModel;
  private Principal mockPrincipal;
  private Simulation mockSimulation;

  @Before
  public void setUp() {
    mainController = new Main();

    mocksControl = createControl();
    mockClientDirectService = mocksControl.createMock(ClientDirectService.class);
    ReflectionTestUtils.setField(mainController,
                                 ClientDirectIdentifiers.COMPONENT_CLIENT_DIRECT_SERVICE,
                                 mockClientDirectService);

    mockModel = mocksControl.createMock(Model.class);
    mockPrincipal = mocksControl.createMock(Principal.class);
    mockSimulation = mocksControl.createMock(Simulation.class);
  }

  @Test
  public void testShowMain() throws NoSuchMethodException, SecurityException {
    mockStatic(Main.class, Main.class.getMethod("setModelPropertiesForMain",
                                                ClientDirectService.class,
                                                Principal.class,
                                                Model.class, String.class));
    Main.setModelPropertiesForMain(mockClientDirectService, mockPrincipal,
                                   mockModel, null);

    replayAll();
    mocksControl.replay();

    final String returned = mainController.showMain(mockPrincipal, mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(returned, ClientDirectIdentifiers.PAGE_MAIN);
  }

  @Test
  public void testSetModelPropertiesForMain() throws DataRetrievalException {
    final String dummyErrorMessage = "errorMessage";
    final String dummyUsername = "dummyUsername";
    // Empty simulations collection
    final List<Simulation> dummySimulations = new ArrayList<Simulation>();
    expect(mockPrincipal.getName()).andReturn(dummyUsername);
    expect(mockClientDirectService.retrieveUserSimulations(dummyUsername))
          .andReturn(dummySimulations);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                                  dummyErrorMessage)).andReturn(mockModel);
    mocksControl.replay();

    Main.setModelPropertiesForMain(mockClientDirectService, mockPrincipal,
                                   mockModel, dummyErrorMessage);

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Add a mock simulation to user's collection.
     */
    dummySimulations.add(mockSimulation);

    final List<ModelVO> dummyModelVOs = new ArrayList<ModelVO>();
    final Map<Short, String> dummyModelNames = new HashMap<Short, String>();

    expect(mockPrincipal.getName()).andReturn(dummyUsername);
    expect(mockClientDirectService.retrieveUserSimulations(dummyUsername))
          .andReturn(dummySimulations);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_SIMULATIONS,
                                  dummySimulations)).andReturn(mockModel);
    expect(mockClientDirectService.retrieveModels()).andReturn(dummyModelVOs);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_MODELS,
                                  dummyModelVOs)).
          andReturn(mockModel);
    expect(mockClientDirectService.retrieveModelNames()).andReturn(dummyModelNames);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MODEL_NAMES,
                                  dummyModelNames)).andReturn(mockModel);

    mocksControl.replay();

    Main.setModelPropertiesForMain(mockClientDirectService, mockPrincipal,
                                   mockModel, null);

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Emulate data access exception (so no simulations returned!).
     */
    dummySimulations.clear();

    expect(mockPrincipal.getName()).andReturn(dummyUsername);
    final String dummyDataRetrievalException = "dummyDataRetrievalException";
    expect(mockClientDirectService.retrieveUserSimulations(dummyUsername))
          .andThrow(new DataRetrievalException(dummyDataRetrievalException));
    final Capture<String> captureModelAttribute = newCapture();
    final Capture<String> captureErrorMessage = newCapture();
    expect(mockModel.addAttribute(capture(captureModelAttribute),
                                  capture(captureErrorMessage))).andReturn(mockModel);

    mocksControl.replay();

    Main.setModelPropertiesForMain(mockClientDirectService, mockPrincipal,
                                   mockModel, dummyErrorMessage);

    mocksControl.verify();

    final String capturedModelAttribute = captureModelAttribute.getValue();
    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                 capturedModelAttribute);
    final String capturedErrorMessage = captureErrorMessage.getValue();
    assertTrue(capturedErrorMessage.contains(dummyErrorMessage));
    assertTrue(capturedErrorMessage.contains(dummyDataRetrievalException));
  }
}