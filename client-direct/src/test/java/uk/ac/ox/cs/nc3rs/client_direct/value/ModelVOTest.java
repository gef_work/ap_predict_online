/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.value;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit test the Model value object.
 *
 * @author geoff
 */
public class ModelVOTest {

  private boolean dummyDefaultModel;
  private ModelVO model;
  private short dummyIdentifier;
  private String dummyCellMLURL;
  private String dummyDescription;
  private String dummyName;
  private String dummyPaperURL;

  @Before
  public void setUp() {
    dummyIdentifier = (short) 0;
    dummyName = null;
    dummyDescription = null;
    dummyCellMLURL = null;
    dummyPaperURL = null;
    dummyDefaultModel = false;
  }

  @Test
  public void testConstructor() {
    dummyIdentifier = (short) 1;
    dummyName = "dummyName";
    dummyDescription = "dummyDescription";
    dummyCellMLURL = "dummyCellMLURL";
    dummyPaperURL = "dummyPaperURL";
    dummyDefaultModel = false;

    model = new ModelVO(dummyIdentifier, dummyName, dummyDescription,
                        dummyCellMLURL, dummyPaperURL, dummyDefaultModel);
    assertSame(dummyIdentifier, model.getIdentifier());
    assertEquals(dummyName, model.getName());
    assertEquals(dummyDescription, model.getDescription());
    assertEquals(dummyCellMLURL, model.getCellmlURL());
    assertEquals(dummyPaperURL, model.getPaperURL());
    assertFalse(model.isDefaultModel());
    assertNotNull(model.toString());
  }

  @Test
  public void testConstructorFailsOnInvalidParameterValues() {
    dummyIdentifier = (short) 0;
    dummyName = "dummyName";

    try {
      new ModelVO(dummyIdentifier, dummyName, dummyDescription, dummyCellMLURL,
                  dummyPaperURL, dummyDefaultModel);
      fail("Should not allow an identifier value less than 1");
    } catch (IllegalArgumentException e) {}

    dummyIdentifier = (short) 1;
    dummyName = "";
    try {
      new ModelVO(dummyIdentifier, dummyName, dummyDescription, dummyCellMLURL,
                  dummyPaperURL, dummyDefaultModel);
      fail("Should not allow an empty name");
    } catch (IllegalArgumentException e) {}

    dummyName = "  ";
    try {
      new ModelVO(dummyIdentifier, dummyName, dummyDescription, dummyCellMLURL,
                  dummyPaperURL, dummyDefaultModel);
      fail("Should not allow the equivalent of an empty name");
    } catch (IllegalArgumentException e) {}
  }

  @Test
  public void testCompareTo() {
    dummyIdentifier = (short) 2;
    dummyName = "dummyName";
    dummyDescription = "dummyDescription";
    dummyDefaultModel = false;

    model = new ModelVO(dummyIdentifier, dummyName, dummyDescription,
                        dummyCellMLURL, dummyPaperURL, dummyDefaultModel);

    ModelVO otherModel = new ModelVO((short) 3, dummyName, dummyDescription,
                                     dummyCellMLURL, dummyPaperURL,
                                     dummyDefaultModel);

    assertTrue(model.compareTo(otherModel) < 0);
    assertNotEquals(model, otherModel);

    otherModel = new ModelVO((short) 2, dummyName, dummyDescription,
                             dummyCellMLURL, dummyPaperURL, dummyDefaultModel);

    assertTrue(model.compareTo(otherModel) == 0);
    assertEquals(model, otherModel);

    otherModel = new ModelVO((short) 1, dummyName, dummyDescription,
                             dummyCellMLURL, dummyPaperURL, dummyDefaultModel);

    assertTrue(model.compareTo(otherModel) > 0);
    assertNotEquals(model, otherModel);
  }
}