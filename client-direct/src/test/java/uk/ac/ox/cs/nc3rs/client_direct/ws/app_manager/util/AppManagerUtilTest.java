/*

  Original work:Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the names of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.util;

import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.expect;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.activation.DataHandler;
import javax.mail.util.ByteArrayDataSource;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.AssociatedItem;
import uk.ac.ox.cs.nc3rs.client_direct.value.MeasurementObservationsVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.InputOption;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;

/**
 * App Manager utility unit tests.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { AppManagerUtil.class, ByteArrayDataSource.class,
                   DataHandler.class } )
public class AppManagerUtilTest {

  private static final String cellMLDataSourceType = "text/xml; charset=UTF-8";
  private static final String pkDataSourceType = "text/tab-separated-values; charset=UTF-8";

  private BigDecimal dummyPacingFrequency;
  private BigDecimal dummyPacingMaxTime;
  private BigDecimal dummyPIC50ICaL, dummyHillICaL, dummySaturationICaL,
                     dummySpreadICaL;
  private BigDecimal dummyPIC50IK1, dummyHillIK1, dummySaturationIK1,
                     dummySpreadIK1;
  private BigDecimal dummyPIC50IKr, dummyHillIKr, dummySaturationIKr,
                     dummySpreadIKr;
  private BigDecimal dummyPIC50IKs, dummyHillIKs, dummySaturationIKs,
                     dummySpreadIKs;
  private BigDecimal dummyPIC50INa, dummyHillINa, dummySaturationINa,
                     dummySpreadINa;
  private BigDecimal dummyPIC50INaL, dummyHillINaL, dummySaturationINaL,
                     dummySpreadINaL;
  private BigDecimal dummyPIC50Ito, dummyHillIto, dummySaturationIto,
                     dummySpreadIto;
  private BigDecimal dummyPlasmaConcMax;
  private BigDecimal dummyPlasmaConcMin;
  private boolean dummyPlasmaIntPtLogScale;
  private MeasurementObservationsVO mockICaLObservations;
  private MeasurementObservationsVO mockIK1Observations;
  private MeasurementObservationsVO mockIKrObservations;
  private MeasurementObservationsVO mockIKsObservations;
  private MeasurementObservationsVO mockINaObservations;
  private MeasurementObservationsVO mockINaLObservations;
  private MeasurementObservationsVO mockItoObservations;
  private IMocksControl mocksControl;
  private InputOption dummyInputOption;
  private PortalFile mockCellMLFile;
  private PortalFile mockPKFile;
  private Short dummyModelIdentifier;
  private Short dummyPlasmaConcCount;
  private SimulationInputVO mockSimulationInputVO;
  private String dummyCIPctiles;
  private List<String> dummyPlasmaConcPoints;

  @Before
  public void setUp() {
    mocksControl = createControl();
    mockSimulationInputVO = mocksControl.createMock(SimulationInputVO.class);
    mockICaLObservations = mocksControl.createMock(MeasurementObservationsVO.class);
    mockIK1Observations = mocksControl.createMock(MeasurementObservationsVO.class);
    mockIKrObservations = mocksControl.createMock(MeasurementObservationsVO.class);
    mockIKsObservations = mocksControl.createMock(MeasurementObservationsVO.class);
    mockINaObservations = mocksControl.createMock(MeasurementObservationsVO.class);
    mockINaLObservations = mocksControl.createMock(MeasurementObservationsVO.class);
    mockItoObservations = mocksControl.createMock(MeasurementObservationsVO.class);
    mockPKFile = mocksControl.createMock(PortalFile.class);
    mockCellMLFile = mocksControl.createMock(PortalFile.class);

    dummyModelIdentifier = 10;
    dummyPacingFrequency = new BigDecimal("0.5");
    dummyPacingMaxTime = new BigDecimal("2.0");
    dummyPIC50ICaL = new BigDecimal("4.5");
    dummyHillICaL = new BigDecimal("0.45");
    dummySaturationICaL = new BigDecimal("45.0");
    dummySpreadICaL = new BigDecimal("0.12");
    dummyPIC50IK1 = new BigDecimal("4.6");
    dummyHillIK1 = new BigDecimal("0.46");
    dummySaturationIK1 = new BigDecimal("46.0");
    dummySpreadIK1 = new BigDecimal("0.23");
    dummyPIC50IKr = new BigDecimal("4.7");
    dummyHillIKr = new BigDecimal("0.47");
    dummySaturationIKr = new BigDecimal("47.0");
    dummySpreadIKr = new BigDecimal("0.34");
    dummyPIC50IKs = new BigDecimal("4.8");
    dummyHillIKs = new BigDecimal("0.48");
    dummySaturationIKs = new BigDecimal("48.0");
    dummySpreadIKs = new BigDecimal("0.45");
    dummyPIC50INa = new BigDecimal("4.9");
    dummyHillINa = new BigDecimal("0.49");
    dummySaturationINa = new BigDecimal("49.0");
    dummySpreadINa = new BigDecimal("0.56");
    dummyPIC50INaL = new BigDecimal("4.91");
    dummyHillINaL = new BigDecimal("0.491");
    dummySaturationINaL = new BigDecimal("49.1");
    dummySpreadINaL = new BigDecimal("0.561");
    dummyPIC50Ito = new BigDecimal("5.0");
    dummyHillIto = new BigDecimal("0.5");
    dummySaturationIto = new BigDecimal("50.0");
    dummySpreadIto = new BigDecimal("0.67");
    dummyCIPctiles = "dummyCIPctiles";
    dummyPlasmaConcPoints = new ArrayList<String>(Arrays.asList(new String[] { "a", "b" }));
    dummyPlasmaConcCount = Short.valueOf("6");
    dummyPlasmaConcMax = new BigDecimal("100");
    dummyPlasmaConcMin = new BigDecimal("10");
    dummyPlasmaIntPtLogScale = true;
    dummyInputOption = InputOption.CONC_RANGE;
  }

  private void localAssertRepetition(final ApPredictRunRequest request) {
    final AssociatedItem iCaLPIC50Data = request.getICaL().getPIC50Data().get(0);
    assertTrue(dummyPIC50ICaL.compareTo(iCaLPIC50Data.getC50()) == 0);
    assertTrue(dummyHillICaL.compareTo(iCaLPIC50Data.getHill()) == 0);
    assertTrue(dummySaturationICaL.compareTo(iCaLPIC50Data.getSaturation()) == 0);
    assertTrue(dummySpreadICaL.compareTo(request.getICaL().getC50Spread()) == 0);
    final AssociatedItem iK1PIC50Data = request.getIK1().getPIC50Data().get(0);
    assertTrue(dummyPIC50IK1.compareTo(iK1PIC50Data.getC50()) == 0);
    assertTrue(dummyHillIK1.compareTo(iK1PIC50Data.getHill()) == 0);
    assertTrue(dummySaturationIK1.compareTo(iK1PIC50Data.getSaturation()) == 0);
    assertTrue(dummySpreadIK1.compareTo(request.getIK1().getC50Spread()) == 0);
    final AssociatedItem iKrPIC50Data = request.getIKr().getPIC50Data().get(0);
    assertTrue(dummyPIC50IKr.compareTo(iKrPIC50Data.getC50()) == 0);
    assertTrue(dummyHillIKr.compareTo(iKrPIC50Data.getHill()) == 0);
    assertTrue(dummySaturationIKr.compareTo(iKrPIC50Data.getSaturation()) == 0);
    assertTrue(dummySpreadIKr.compareTo(request.getIKr().getC50Spread()) == 0);
    final AssociatedItem iKsPIC50Data = request.getIKs().getPIC50Data().get(0);
    assertTrue(dummyPIC50IKs.compareTo(iKsPIC50Data.getC50()) == 0);
    assertTrue(dummyHillIKs.compareTo(iKsPIC50Data.getHill()) == 0);
    assertTrue(dummySaturationIKs.compareTo(iKsPIC50Data.getSaturation()) == 0);
    assertTrue(dummySpreadIKs.compareTo(request.getIKs().getC50Spread()) == 0);
    final AssociatedItem iNaPIC50Data = request.getINa().getPIC50Data().get(0);
    assertTrue(dummyPIC50INa.compareTo(iNaPIC50Data.getC50()) == 0);
    assertTrue(dummyHillINa.compareTo(iNaPIC50Data.getHill()) == 0);
    assertTrue(dummySaturationINa.compareTo(iNaPIC50Data.getSaturation()) == 0);
    assertTrue(dummySpreadINa.compareTo(request.getINa().getC50Spread()) == 0);
    final AssociatedItem iNaLPIC50Data = request.getINaL().getPIC50Data().get(0);
    assertTrue(dummyPIC50INaL.compareTo(iNaLPIC50Data.getC50()) == 0);
    assertTrue(dummyHillINaL.compareTo(iNaLPIC50Data.getHill()) == 0);
    assertTrue(dummySaturationINaL.compareTo(iNaLPIC50Data.getSaturation()) == 0);
    assertTrue(dummySpreadINaL.compareTo(request.getINaL().getC50Spread()) == 0);
    final AssociatedItem itoPIC50Data = request.getIto().getPIC50Data().get(0);
    assertTrue(dummyPIC50Ito.compareTo(itoPIC50Data.getC50()) == 0);
    assertTrue(dummyHillIto.compareTo(itoPIC50Data.getHill()) == 0);
    assertTrue(dummySaturationIto.compareTo(itoPIC50Data.getSaturation()) == 0);
    assertTrue(dummySpreadIto.compareTo(request.getIto().getC50Spread()) == 0);
  }

  private void localExpectRepetition() {
    expect(mockSimulationInputVO.getCellMLFile()).andReturn(null);
    expect(mockSimulationInputVO.getModelIdentifier())
          .andReturn(dummyModelIdentifier);
    expect(mockSimulationInputVO.getPacingFrequency())
          .andReturn(dummyPacingFrequency);
    expect(mockSimulationInputVO.getPacingMaxTime())
          .andReturn(dummyPacingMaxTime);
    expect(mockSimulationInputVO.retrievePIC50ICaL()).andReturn(dummyPIC50ICaL);
    expect(mockSimulationInputVO.getICaLObservations())
          .andReturn(mockICaLObservations);
    expect(mockICaLObservations.getHill()).andReturn(dummyHillICaL);
    expect(mockICaLObservations.getSaturation()).andReturn(dummySaturationICaL);
    expect(mockICaLObservations.getSpread()).andReturn(dummySpreadICaL);
    expect(mockSimulationInputVO.retrievePIC50Ik1()).andReturn(dummyPIC50IK1);
    expect(mockSimulationInputVO.getIK1Observations())
          .andReturn(mockIK1Observations);
    expect(mockIK1Observations.getHill()).andReturn(dummyHillIK1);
    expect(mockIK1Observations.getSaturation()).andReturn(dummySaturationIK1);
    expect(mockIK1Observations.getSpread()).andReturn(dummySpreadIK1);
    expect(mockSimulationInputVO.retrievePIC50Ikr()).andReturn(dummyPIC50IKr);
    expect(mockSimulationInputVO.getIKrObservations())
          .andReturn(mockIKrObservations);
    expect(mockIKrObservations.getHill()).andReturn(dummyHillIKr);
    expect(mockIKrObservations.getSaturation()).andReturn(dummySaturationIKr);
    expect(mockIKrObservations.getSpread()).andReturn(dummySpreadIKr);
    expect(mockSimulationInputVO.retrievePIC50Iks()).andReturn(dummyPIC50IKs);
    expect(mockSimulationInputVO.getIKsObservations())
          .andReturn(mockIKsObservations);
    expect(mockIKsObservations.getHill()).andReturn(dummyHillIKs);
    expect(mockIKsObservations.getSaturation()).andReturn(dummySaturationIKs);
    expect(mockIKsObservations.getSpread()).andReturn(dummySpreadIKs);
    expect(mockSimulationInputVO.retrievePIC50INa()).andReturn(dummyPIC50INa);
    expect(mockSimulationInputVO.getINaObservations())
          .andReturn(mockINaObservations);
    expect(mockINaObservations.getHill()).andReturn(dummyHillINa);
    expect(mockINaObservations.getSaturation()).andReturn(dummySaturationINa);
    expect(mockINaObservations.getSpread()).andReturn(dummySpreadINa);
    expect(mockSimulationInputVO.retrievePIC50INaL()).andReturn(dummyPIC50INaL);
    expect(mockSimulationInputVO.getINaLObservations())
          .andReturn(mockINaLObservations);
    expect(mockINaLObservations.getHill()).andReturn(dummyHillINaL);
    expect(mockINaLObservations.getSaturation()).andReturn(dummySaturationINaL);
    expect(mockINaLObservations.getSpread()).andReturn(dummySpreadINaL);
    expect(mockSimulationInputVO.retrievePIC50Ito()).andReturn(dummyPIC50Ito);
    expect(mockSimulationInputVO.getItoObservations())
         .andReturn(mockItoObservations);
    expect(mockItoObservations.getHill()).andReturn(dummyHillIto);
    expect(mockItoObservations.getSaturation()).andReturn(dummySaturationIto);
    expect(mockItoObservations.getSpread()).andReturn(dummySpreadIto);
    expect(mockSimulationInputVO.getCredibleIntervalPctiles())
          .andReturn(dummyCIPctiles);
    expect(mockSimulationInputVO.getPlasmaConcPoints())
          .andReturn(dummyPlasmaConcPoints);
    expect(mockSimulationInputVO.getPlasmaConcMax())
          .andReturn(dummyPlasmaConcMax);
    expect(mockSimulationInputVO.getPkFile()).andReturn(mockPKFile);
    expect(mockSimulationInputVO.retrieveInputOption())
          .andReturn(dummyInputOption);
  }

  
  @Test
  public void testRunRequestCreationAllDataConcPoints() throws Exception {
    dummyInputOption = InputOption.CONC_POINTS;
    /*
     * Test points assignment
     */
    localExpectRepetition();

    mocksControl.replay();

    ApPredictRunRequest request = AppManagerUtil.createRunRequest(mockSimulationInputVO);

    mocksControl.verify();

    assertEquals(dummyModelIdentifier, request.getModelIdentifier());
    assertTrue(dummyPacingFrequency.compareTo(request.getPacingFreq()) == 0);
    assertTrue(dummyPacingMaxTime.compareTo(request.getPacingMaxTime()) == 0);
    localAssertRepetition(request);
    assertNotNull(request.getCredibleIntervals());
    assertTrue(request.getCredibleIntervals().isCalculateCredibleIntervals());
    assertEquals(dummyCIPctiles, request.getCredibleIntervals().getPercentiles());
    assertEquals("a,b", request.getPlasmaConcsAssigned());
    assertNull(request.getPlasmaConcCount());
    assertNull(request.getPlasmaConcsHiLo());
    assertFalse(request.isPlasmaConcLogScale());
    assertNull(request.getPKFile());
  }

  @Test
  public void testRunRequestCreationAllDataConcRange() throws Exception {
    localExpectRepetition();
    expect(mockSimulationInputVO.getPlasmaIntPtCount()).andReturn(dummyPlasmaConcCount);
    expect(mockSimulationInputVO.getPlasmaConcMin()).andReturn(dummyPlasmaConcMin);
    expect(mockSimulationInputVO.getPlasmaIntPtLogScale()).andReturn(dummyPlasmaIntPtLogScale);

    mocksControl.replay();

    ApPredictRunRequest request = AppManagerUtil.createRunRequest(mockSimulationInputVO);

    mocksControl.verify();

    assertEquals(dummyModelIdentifier, request.getModelIdentifier());
    assertTrue(dummyPacingFrequency.compareTo(request.getPacingFreq()) == 0);
    assertTrue(dummyPacingMaxTime.compareTo(request.getPacingMaxTime()) == 0);
    localAssertRepetition(request);
    assertNotNull(request.getCredibleIntervals());
    assertEquals(dummyPlasmaConcCount, request.getPlasmaConcCount());
    assertTrue(dummyPlasmaConcMax.compareTo(request.getPlasmaConcsHiLo().getHigh()) == 0);
    assertTrue(dummyPlasmaConcMin.compareTo(request.getPlasmaConcsHiLo().getLow()) == 0);
    assertTrue(request.isPlasmaConcLogScale());
    assertNull(request.getPlasmaConcsAssigned());
    assertNull(request.getPKFile());

    mocksControl.reset();

    /*
     * Repeat test but with branch test of plasma conc min.
     */
    localExpectRepetition();
    expect(mockSimulationInputVO.getPlasmaIntPtCount()).andReturn(dummyPlasmaConcCount);
    expect(mockSimulationInputVO.getPlasmaConcMin()).andReturn(null);
    expect(mockSimulationInputVO.getPlasmaIntPtLogScale()).andReturn(dummyPlasmaIntPtLogScale);

    mocksControl.replay();

    request = AppManagerUtil.createRunRequest(mockSimulationInputVO);

    mocksControl.verify();

    assertEquals(dummyModelIdentifier, request.getModelIdentifier());
    assertTrue(dummyPacingFrequency.compareTo(request.getPacingFreq()) == 0);
    assertTrue(dummyPacingMaxTime.compareTo(request.getPacingMaxTime()) == 0);
    localAssertRepetition(request);
    assertNotNull(request.getCredibleIntervals());
    assertEquals(dummyPlasmaConcCount, request.getPlasmaConcCount());
    assertTrue(dummyPlasmaConcMax.compareTo(request.getPlasmaConcsHiLo().getHigh()) == 0);
    assertNull(request.getPlasmaConcsHiLo().getLow());
    assertTrue(request.isPlasmaConcLogScale());
    assertNull(request.getPlasmaConcsAssigned());
    assertNull(request.getPKFile());
  }

  @Test
  public void testRunRequestCreationAllDataPK() throws Exception {
    dummyInputOption = InputOption.PK;
    /*
     * PK file empty content 
     */
    localExpectRepetition();
    String dummyPKFileContent = "";
    expect(mockPKFile.getContent()).andReturn(dummyPKFileContent);

    mocksControl.replay();

    ApPredictRunRequest request = AppManagerUtil.createRunRequest(mockSimulationInputVO);

    mocksControl.verify();

    assertNotNull(request.getCredibleIntervals());
    assertNull(request.getPlasmaConcCount());
    assertNull(request.getPlasmaConcsHiLo());
    assertFalse(request.isPlasmaConcLogScale());
    assertNull(request.getPlasmaConcsAssigned());
    assertNull(request.getPKFile());

    mocksControl.reset();

    /*
     * Repeat test but with PK file data reading exception. 
     */
    localExpectRepetition();
    dummyPKFileContent = "dummyPKFileContent";
    expect(mockPKFile.getContent()).andReturn(dummyPKFileContent);
    expectNew(ByteArrayDataSource.class, dummyPKFileContent, pkDataSourceType)
             .andThrow(new IOException("dummyIOException"));

    replayAll();
    mocksControl.replay();

    request = AppManagerUtil.createRunRequest(mockSimulationInputVO);

    verifyAll();
    mocksControl.verify();

    assertNotNull(request.getCredibleIntervals());
    assertNull(request.getPlasmaConcCount());
    assertNull(request.getPlasmaConcsHiLo());
    assertFalse(request.isPlasmaConcLogScale());
    assertNull(request.getPlasmaConcsAssigned());
    assertNull(request.getPKFile());

    resetAll();
    mocksControl.reset();

    /*
     * Repeat test but with PK file data. 
     */
    localExpectRepetition();
    dummyPKFileContent = "dummyPKFileContent";
    expect(mockPKFile.getContent()).andReturn(dummyPKFileContent);
    final ByteArrayDataSource mockByteArrayDataSource = mocksControl.createMock(ByteArrayDataSource.class);
    expectNew(ByteArrayDataSource.class, dummyPKFileContent, pkDataSourceType)
             .andStubReturn(mockByteArrayDataSource);
    final DataHandler mockDataHandler = mocksControl.createMock(DataHandler.class);
    expectNew(DataHandler.class, mockByteArrayDataSource)
             .andStubReturn(mockDataHandler);

    replayAll();
    mocksControl.replay();

    request = AppManagerUtil.createRunRequest(mockSimulationInputVO);

    verifyAll();
    mocksControl.verify();

    assertNotNull(request.getCredibleIntervals());
    assertNull(request.getPlasmaConcCount());
    assertNull(request.getPlasmaConcsHiLo());
    assertFalse(request.isPlasmaConcLogScale());
    assertNull(request.getPlasmaConcsAssigned());
    assertSame(mockDataHandler, request.getPKFile());
  }

  @Test
  public void testRunRequestCreateCellML() throws Exception {
    /*
     * CellML file, but without content.
     */
    expect(mockSimulationInputVO.getCellMLFile()).andReturn(mockCellMLFile);
    String dummyContent = null;
    expect(mockCellMLFile.getContent()).andReturn(dummyContent);
    expect(mockSimulationInputVO.getPacingFrequency()).andReturn(null);
    expect(mockSimulationInputVO.getPacingMaxTime()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50ICaL()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50Ikr()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50Iks()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50INa()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50INaL()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50Ik1()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50Ito()).andReturn(null);
    expect(mockSimulationInputVO.getCredibleIntervalPctiles()).andReturn(null);
    expect(mockSimulationInputVO.getPlasmaConcPoints()).andReturn(null);
    expect(mockSimulationInputVO.getPlasmaConcMax()).andReturn(null);
    expect(mockSimulationInputVO.getPkFile()).andReturn(null);
    expect(mockSimulationInputVO.retrieveInputOption()).andReturn(dummyInputOption);
    expect(mockSimulationInputVO.getPlasmaIntPtCount()).andReturn(null);
    expect(mockSimulationInputVO.getPlasmaConcMin()).andReturn(null);
    expect(mockSimulationInputVO.getPlasmaIntPtLogScale()).andReturn(false);

    mocksControl.replay();

    ApPredictRunRequest request = AppManagerUtil.createRunRequest(mockSimulationInputVO);

    mocksControl.verify();

    assertNull(request.getModelIdentifier());
    assertNull(request.getCellMLFile());

    mocksControl.reset();

    /*
     * CellML file, with content, exception thrown.
     */
    expect(mockSimulationInputVO.getCellMLFile()).andReturn(mockCellMLFile);
    dummyContent = "dummyContent";
    expect(mockCellMLFile.getContent()).andReturn(dummyContent);
    final String dummyIOException = "dummyIOException";
    expectNew(ByteArrayDataSource.class, dummyContent, cellMLDataSourceType)
             .andThrow(new IOException(dummyIOException));
    expect(mockSimulationInputVO.getPacingFrequency()).andReturn(null);
    expect(mockSimulationInputVO.getPacingMaxTime()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50ICaL()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50Ikr()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50Iks()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50INa()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50INaL()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50Ik1()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50Ito()).andReturn(null);
    expect(mockSimulationInputVO.getCredibleIntervalPctiles()).andReturn(null);
    expect(mockSimulationInputVO.getPlasmaConcPoints()).andReturn(null);
    expect(mockSimulationInputVO.getPlasmaConcMax()).andReturn(null);
    expect(mockSimulationInputVO.getPkFile()).andReturn(null);
    expect(mockSimulationInputVO.retrieveInputOption()).andReturn(dummyInputOption);
    expect(mockSimulationInputVO.getPlasmaIntPtCount()).andReturn(null);
    expect(mockSimulationInputVO.getPlasmaConcMin()).andReturn(null);
    expect(mockSimulationInputVO.getPlasmaIntPtLogScale()).andReturn(false);

    replayAll();
    mocksControl.replay();

    request = AppManagerUtil.createRunRequest(mockSimulationInputVO);

    verifyAll();
    mocksControl.verify();

    assertNull(request.getModelIdentifier());
    assertNull(request.getCellMLFile());

    resetAll();
    mocksControl.reset();

    /*
     * CellML file, with content, no exception.
     */
    expect(mockSimulationInputVO.getCellMLFile()).andReturn(mockCellMLFile);
    dummyContent = "dummyContent";
    expect(mockCellMLFile.getContent()).andReturn(dummyContent);
    final ByteArrayDataSource mockByteArrayDataSource = mocksControl.createMock(ByteArrayDataSource.class);
    expectNew(ByteArrayDataSource.class, dummyContent, cellMLDataSourceType)
             .andStubReturn(mockByteArrayDataSource);
    final DataHandler mockDataHandler = mocksControl.createMock(DataHandler.class);
    expectNew(DataHandler.class, mockByteArrayDataSource)
             .andStubReturn(mockDataHandler);
    expect(mockSimulationInputVO.getPacingFrequency()).andReturn(null);
    expect(mockSimulationInputVO.getPacingMaxTime()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50ICaL()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50Ikr()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50Iks()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50INa()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50INaL()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50Ik1()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50Ito()).andReturn(null);
    expect(mockSimulationInputVO.getCredibleIntervalPctiles()).andReturn(null);
    expect(mockSimulationInputVO.getPlasmaConcPoints()).andReturn(null);
    expect(mockSimulationInputVO.getPlasmaConcMax()).andReturn(null);
    expect(mockSimulationInputVO.getPkFile()).andReturn(null);
    expect(mockSimulationInputVO.retrieveInputOption()).andReturn(dummyInputOption);
    expect(mockSimulationInputVO.getPlasmaIntPtCount()).andReturn(null);
    expect(mockSimulationInputVO.getPlasmaConcMin()).andReturn(null);
    expect(mockSimulationInputVO.getPlasmaIntPtLogScale()).andReturn(false);

    replayAll();
    mocksControl.replay();

    request = AppManagerUtil.createRunRequest(mockSimulationInputVO);

    verifyAll();
    mocksControl.verify();

    assertNull(request.getModelIdentifier());
    assertSame(mockDataHandler, request.getCellMLFile());
  }
  

  @Test
  public void testRunRequestCreationNoData() {
    expect(mockSimulationInputVO.getCellMLFile()).andReturn(null);
    expect(mockSimulationInputVO.getModelIdentifier()).andReturn(dummyModelIdentifier);
    expect(mockSimulationInputVO.getPacingFrequency()).andReturn(null);
    expect(mockSimulationInputVO.getPacingMaxTime()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50ICaL()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50Ikr()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50Iks()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50INa()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50INaL()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50Ik1()).andReturn(null);
    expect(mockSimulationInputVO.retrievePIC50Ito()).andReturn(null);
    expect(mockSimulationInputVO.getCredibleIntervalPctiles()).andReturn(null);
    expect(mockSimulationInputVO.getPlasmaConcPoints()).andReturn(null);
    expect(mockSimulationInputVO.getPlasmaConcMax()).andReturn(null);
    expect(mockSimulationInputVO.getPkFile()).andReturn(null);
    expect(mockSimulationInputVO.retrieveInputOption()).andReturn(dummyInputOption);
    expect(mockSimulationInputVO.getPlasmaIntPtCount()).andReturn(null);
    expect(mockSimulationInputVO.getPlasmaConcMin()).andReturn(null);
    expect(mockSimulationInputVO.getPlasmaIntPtLogScale()).andReturn(false);

    mocksControl.replay();

    final ApPredictRunRequest request = AppManagerUtil.createRunRequest(mockSimulationInputVO);

    mocksControl.verify();

    assertEquals(dummyModelIdentifier, request.getModelIdentifier());
    assertNull(request.getPacingFreq());
    assertNull(request.getPacingMaxTime());
    assertNull(request.getICaL());
    assertNull(request.getIKr());
    assertNull(request.getIKs());
    assertNull(request.getINa());
    assertNull(request.getINaL());
    assertNull(request.getIK1());
    assertNull(request.getIto());
    assertNotNull(request.getCredibleIntervals());
    assertFalse(request.getCredibleIntervals().isCalculateCredibleIntervals());
    assertNull(request.getCredibleIntervals().getPercentiles());
    assertNull(request.getPlasmaConcsAssigned());
    assertNull(request.getPlasmaConcCount());
    assertNull(request.getPlasmaConcsHiLo().getHigh());
    assertNull(request.getPlasmaConcsHiLo().getLow());
    assertFalse(request.isPlasmaConcLogScale());
    assertNull(request.getPKFile());
  }
}