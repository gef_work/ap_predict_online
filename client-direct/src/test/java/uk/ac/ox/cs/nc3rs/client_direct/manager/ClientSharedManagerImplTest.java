/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.manager;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.compbio.client_shared.api.exception.DownstreamCommunicationException;
import uk.ac.ox.cs.compbio.client_shared.api.manager.ClientSharedManager;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.exception.AppManagerWSInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.NoConnectionException;
import uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.service.AppManagerService;

/**
 * Unit test shared client management implementation.
 *
 * @author geoff
 */
public class ClientSharedManagerImplTest {

  private AppManagerService mockAppManagerService;
  private ClientSharedManager clientSharedManager;
  private IMocksControl mocksControl;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    clientSharedManager = new ClientSharedManagerImpl();

    mockAppManagerService = mocksControl.createMock(AppManagerService.class);

    ReflectionTestUtils.setField(clientSharedManager,
                                 ClientDirectIdentifiers.COMPONENT_APP_MANAGER_SERVICE,
                                 mockAppManagerService);
  }

  @Test
  public void testRetrieveAppManagerWorkload() throws DownstreamCommunicationException,
                                                      AppManagerWSInvocationException,
                                                      NoConnectionException {
    final String dummyWorkload = "dummyWorkload";
    expect(mockAppManagerService.retrieveWorkload()).andReturn(dummyWorkload);

    mocksControl.replay();

    final String workload = clientSharedManager.retrieveAppManagerWorkload();

    mocksControl.verify();

    assertEquals(dummyWorkload, workload);
  }

  @Test
  public void testRetrieveAppManagerWorkloadHandlesExceptions() throws AppManagerWSInvocationException,
                                                                       NoConnectionException,
                                                                       DownstreamCommunicationException {
    final String dummyExceptionMessage = "dummyExceptionMessage";
    expect(mockAppManagerService.retrieveWorkload())
          .andThrow(new AppManagerWSInvocationException(dummyExceptionMessage));

    mocksControl.replay();

    String workload = null;
    String caughtExceptionMessage = null;
    try {
      workload = clientSharedManager.retrieveAppManagerWorkload();
      fail("Should catch exception thrown by app manager service.");
    } catch (Exception e) {
      caughtExceptionMessage = e.getMessage();
    }

    mocksControl.verify();

    assertNull(workload);
    assertEquals(dummyExceptionMessage, caughtExceptionMessage);

    mocksControl.reset();

    expect(mockAppManagerService.retrieveWorkload())
          .andThrow(new NoConnectionException(dummyExceptionMessage));

    mocksControl.replay();

    try {
      workload = clientSharedManager.retrieveAppManagerWorkload();
      fail("Should catch exception thrown by app manager service.");
    } catch (Exception e) {
      caughtExceptionMessage = e.getMessage();
    }

    mocksControl.verify();

    assertNull(workload);
    assertEquals(dummyExceptionMessage, caughtExceptionMessage);
  }
}