/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Test;

/**
 * Unit test the controller utility class.
 *
 * @author geoff
 */
public class ControllerUtilTest {

  private static final BigDecimal tooBig = new BigDecimal(Long.MAX_VALUE).add(BigDecimal.ONE);
  private static final BigDecimal tooLittle = new BigDecimal(Long.MIN_VALUE).subtract(BigDecimal.ONE);

  @Test
  public void testSimulationIdValidator() {
    String simulationId = null;
    assertFalse(ControllerUtil.validSimulationId(simulationId));

    simulationId = tooBig.toPlainString();
    assertFalse(ControllerUtil.validSimulationId(simulationId));
    simulationId = tooLittle.toPlainString();
    assertFalse(ControllerUtil.validSimulationId(simulationId));

    simulationId = "apples";
    assertFalse(ControllerUtil.validSimulationId(simulationId));

    simulationId = " 1 ";
    assertFalse(ControllerUtil.validSimulationId(simulationId));

    simulationId = "0";
    assertFalse(ControllerUtil.validSimulationId(simulationId));

    simulationId = "1";
    assertTrue(ControllerUtil.validSimulationId(simulationId));

    simulationId = Long.valueOf(Long.MAX_VALUE).toString();
    assertTrue(ControllerUtil.validSimulationId(simulationId));
  }
}