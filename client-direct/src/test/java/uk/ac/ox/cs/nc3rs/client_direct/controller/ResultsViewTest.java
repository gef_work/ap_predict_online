/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.Model;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.value.PortalFeature;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.controller.util.ControllerUtil;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation;
import uk.ac.ox.cs.nc3rs.client_direct.entity.SimulationInput;
import uk.ac.ox.cs.nc3rs.client_direct.exception.SimulationNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService;
import uk.ac.ox.cs.nc3rs.client_direct.value.ModelVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;

/**
 * Unit test the results view controller.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { Results.class, Main.class, Process.class,
                   ControllerUtil.class } )
public class ResultsViewTest {

  private ClientDirectService mockClientDirectService;
  private IMocksControl mocksControl;
  private Locale dummyLocale;
  private MessageSource mockMessageSource;
  private Model mockModel;
  private Principal mockPrincipal;
  private Results resultsController;
  private Simulation mockSimulation;
  private SimulationInput mockSimulationInput;
  private SimulationInputVO mockSimulationInputVO;
  private String dummySimulationId;

  @Before
  public void setUp() {
    resultsController = new Results();

    mocksControl = createControl();
    mockClientDirectService = mocksControl.createMock(ClientDirectService.class);
    mockMessageSource = mocksControl.createMock(MessageSource.class);
    ReflectionTestUtils.setField(resultsController, "messageSource", mockMessageSource);
    ReflectionTestUtils.setField(resultsController,
                                 ClientDirectIdentifiers.COMPONENT_CLIENT_DIRECT_SERVICE,
                                 mockClientDirectService);
    mockModel = mocksControl.createMock(Model.class);
    mockPrincipal = mocksControl.createMock(Principal.class);
    mockSimulation = mocksControl.createMock(Simulation.class);
    mockSimulationInput = mocksControl.createMock(SimulationInput.class);
    mockSimulationInputVO = mocksControl.createMock(SimulationInputVO.class);

    dummyLocale = Locale.getDefault();
    dummySimulationId = "1";
  }

  @Test
  public void testFailOnInvalidSimulationId() {
    mockStatic(ControllerUtil.class);
    expect(ControllerUtil.validSimulationId(dummySimulationId))
          .andReturn(false);
    mockStatic(Main.class);
    Main.setModelPropertiesForMain(mockClientDirectService, mockPrincipal,
                                   mockModel, null);

    replayAll();

    String returnPage = resultsController.resultsView(dummySimulationId,
                                                      dummyLocale, mockModel,
                                                      mockPrincipal);

    verifyAll();

    assertEquals(ClientDirectIdentifiers.PAGE_MAIN, returnPage);
  }

  @Test
  public void testSimulationRetrievalFailOnException() throws Exception {
    mockStatic(ControllerUtil.class);
    expect(ControllerUtil.validSimulationId(dummySimulationId))
          .andReturn(true);
    expect(mockClientDirectService.retrieveSimulation(Long.valueOf(dummySimulationId).longValue()))
          .andThrow(new SimulationNotFoundException());
    // anyTimes() used as no order of test execution and message queue system may have been populated.
    Capture<String> captureMessageKey = newCapture();
    Capture<Object[]> captureArgs = newCapture();
    Capture<Locale> captureLocale = newCapture();
    expect(mockMessageSource.getMessage(capture(captureMessageKey), capture(captureArgs), 
                                        capture(captureLocale)))
          .andThrow(new NoSuchMessageException(null)).anyTimes();
    mockStatic(Main.class);
    Main.setModelPropertiesForMain(mockClientDirectService, mockPrincipal,
                                   mockModel,
                                   ClientDirectIdentifiers.UNAVAILABLE_MSG_BUNDLE_ID);

    replayAll();
    mocksControl.replay();

    String returnPage = resultsController.resultsView(dummySimulationId,
                                                      dummyLocale, mockModel,
                                                      mockPrincipal);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_MAIN, returnPage);
  }

  @Test
  public void testSimulationRetrievalSucess() throws Exception {
    mockStatic(ControllerUtil.class);
    expect(ControllerUtil.validSimulationId(dummySimulationId))
          .andReturn(true);
    expect(mockClientDirectService.retrieveSimulation(Long.valueOf(dummySimulationId).longValue()))
          .andReturn(mockSimulation);
    expect(mockSimulation.getSimulationInput()).andReturn(mockSimulationInput);
    final boolean dummyAllowDynamicCellML = true;
    expect(ControllerUtil.allowFeature(PortalFeature.DYNAMIC_CELLMLS,
                                       mockClientDirectService))
          .andReturn(dummyAllowDynamicCellML);
    final boolean dummyAllowUncertainties = true;
    expect(ControllerUtil.allowFeature(PortalFeature.UNCERTAINTIES, 
                                       mockClientDirectService))
          .andReturn(dummyAllowUncertainties);
    expectNew(SimulationInputVO.class, mockSimulationInput)
             .andReturn(mockSimulationInputVO);
    // >> setModelProperties()
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_INPUT_VALUES,
                                  mockSimulationInputVO))
          .andReturn(mockModel);
    final List<ModelVO> dummyModels = new ArrayList<ModelVO>();
    expect(mockClientDirectService.retrieveModels()).andReturn(dummyModels);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_MODELS,
                                  dummyModels))
          .andReturn(mockModel);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SHOW_DYNAMIC_CELLML,
                                  dummyAllowDynamicCellML))
          .andReturn(mockModel);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SHOW_UNCERTAINTIES,
                                  dummyAllowUncertainties))
          .andReturn(mockModel);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  Long.valueOf(dummySimulationId)))
          .andReturn(mockModel);
    // << setModelProperties()

    replayAll();
    mocksControl.replay();

    String returnPage = resultsController.resultsView(dummySimulationId,
                                                      dummyLocale, mockModel,
                                                      mockPrincipal);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_RESULTS, returnPage);
  }
}