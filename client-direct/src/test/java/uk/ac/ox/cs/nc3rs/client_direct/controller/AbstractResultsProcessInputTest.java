/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the names of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.mockStatic;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.compbio.client_shared.value.PortalFeature;
import uk.ac.ox.cs.compbio.client_shared.value.file.FileStoreActionOutcomeVO;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.controller.util.ControllerUtil;
import uk.ac.ox.cs.nc3rs.client_direct.exception.AppManagerWSInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.NoConnectionException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.RunInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService;
import uk.ac.ox.cs.nc3rs.client_direct.value.InputOption;
import uk.ac.ox.cs.nc3rs.client_direct.value.ModelVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;

/**
 * Abstract Results controller testing.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { FileStoreActionOutcomeVO.class, Results.class, Main.class,
                   Process.class, ControllerUtil.class } )
public abstract class AbstractResultsProcessInputTest {

  protected boolean dummyAllowDynamicCellML;
  protected boolean dummyAllowUncertainties;
  protected ClientDirectService mockClientDirectService;
  protected HttpSession mockHttpSession;
  protected IMocksControl mocksControl;
  protected Locale dummyLocale;
  protected Long dummySimulationId;
  protected MessageSource mockMessageSource;
  protected Model mockModel;
  protected MultipartFile mockMultipartCellMLFile;
  protected MultipartFile mockMultipartPKFile;
  protected PortalFile mockCellMLFile;
  protected PortalFile mockPKFile;
  protected Principal mockPrincipal;
  protected Results resultsController;
  //protected Set<Role> dummyRestrictedRoles;
  protected String dummyModelAssignedToken,
                   dummyICaLC50, dummyICaLHill, dummyICaLSaturation,
                   dummyICaLSpread,
                   dummyIKrC50, dummyIKrHill, dummyIKrSaturation,
                   dummyIKrSpread,
                   dummyIK1C50, dummyIK1Hill, dummyIK1Saturation,
                   dummyIK1Spread,
                   dummyIKsC50, dummyIKsHill, dummyIKsSaturation,
                   dummyIKsSpread,
                   dummyItoC50, dummyItoHill, dummyItoSaturation,
                   dummyItoSpread,
                   dummyINaC50, dummyINaHill, dummyINaSaturation,
                   dummyINaSpread,
                   dummyINaLC50, dummyINaLHill, dummyINaLSaturation,
                   dummyINaLSpread,
                   dummyC50Type, dummyIC50Units, dummyModelIdentifier,
                   dummyPacingFrequency, dummyPacingMaxTime, dummyPkOrConcs,
                   dummyPlasmaConcMax, dummyPlasmaConcMin, dummyPlasmaIntPtCount,
                   dummyPlasmaIntPtLogScale, dummyCIPctiles, dummyNotes,
                   dummySpreadsEnabled, dummyPrincipalName;
  protected String[] dummyPlasmaConcPoints;

  @Before
  public void setUp() {
    resultsController = new Results();

    mocksControl = createStrictControl();
    mockClientDirectService = mocksControl.createMock(ClientDirectService.class);
    mockMessageSource = mocksControl.createMock(MessageSource.class);
    ReflectionTestUtils.setField(resultsController, "messageSource",
                                 mockMessageSource);
    ReflectionTestUtils.setField(resultsController,
                                 ClientDirectIdentifiers.COMPONENT_CLIENT_DIRECT_SERVICE,
                                 mockClientDirectService);
    mockStatic(ControllerUtil.class);

    mockHttpSession = mocksControl.createMock(HttpSession.class);
    mockModel = mocksControl.createMock(Model.class);
    mockPrincipal = mocksControl.createMock(Principal.class);

    dummyAllowDynamicCellML = false;
    dummyAllowUncertainties = false;
    dummyModelAssignedToken = "dummyModelAssignedToken";
    dummyModelIdentifier = "1";

    dummyPkOrConcs = InputOption.CONC_RANGE.toString();
    dummyPrincipalName = "dummyPrincipalName";
    //dummyRestrictedRoles = new HashSet<Role>();
    dummySimulationId = 5L;
  }

  protected void assignMeasurementObservations(final SimulationInputVO.Builder mockBuilder,
                                               final boolean assignSpreads) {
    expect(mockBuilder.iCaLC50(dummyICaLC50)).andReturn(mockBuilder);
    expect(mockBuilder.iCaLHill(dummyICaLHill)).andReturn(mockBuilder);
    expect(mockBuilder.iCaLSaturation(dummyICaLSaturation)).andReturn(mockBuilder);
    if (assignSpreads) {
      expect(mockBuilder.iCaLSpread(dummyICaLSpread)).andReturn(mockBuilder);
    } else {
      expect(mockBuilder.iCaLSpread(null)).andReturn(mockBuilder);
    }
    expect(mockBuilder.iK1C50(dummyIK1C50)).andReturn(mockBuilder);
    expect(mockBuilder.iK1Hill(dummyIK1Hill)).andReturn(mockBuilder);
    expect(mockBuilder.iK1Saturation(dummyIK1Saturation)).andReturn(mockBuilder);
    if (assignSpreads) {
      expect(mockBuilder.iK1Spread(dummyIK1Spread)).andReturn(mockBuilder);
    } else {
      expect(mockBuilder.iK1Spread(null)).andReturn(mockBuilder);
    }
    expect(mockBuilder.iKrC50(dummyIKrC50)).andReturn(mockBuilder);
    expect(mockBuilder.iKrHill(dummyIKrHill)).andReturn(mockBuilder);
    expect(mockBuilder.iKrSaturation(dummyIKrSaturation)).andReturn(mockBuilder);
    if (assignSpreads) {
      expect(mockBuilder.iKrSpread(dummyIKrSpread)).andReturn(mockBuilder);
    } else {
      expect(mockBuilder.iKrSpread(null)).andReturn(mockBuilder);
    }
    expect(mockBuilder.iKsC50(dummyIKsC50)).andReturn(mockBuilder);
    expect(mockBuilder.iKsHill(dummyIKsHill)).andReturn(mockBuilder);
    expect(mockBuilder.iKsSaturation(dummyIKsSaturation)).andReturn(mockBuilder);
    if (assignSpreads) {
      expect(mockBuilder.iKsSpread(dummyIKsSpread)).andReturn(mockBuilder);
    } else {
      expect(mockBuilder.iKsSpread(null)).andReturn(mockBuilder);
    }
    expect(mockBuilder.iNaC50(dummyINaC50)).andReturn(mockBuilder);
    expect(mockBuilder.iNaHill(dummyINaHill)).andReturn(mockBuilder);
    expect(mockBuilder.iNaSaturation(dummyINaSaturation)).andReturn(mockBuilder);
    if (assignSpreads) {
      expect(mockBuilder.iNaSpread(dummyINaSpread)).andReturn(mockBuilder);
    } else {
      expect(mockBuilder.iNaSpread(null)).andReturn(mockBuilder);
    }
    expect(mockBuilder.iNaLC50(dummyINaLC50)).andReturn(mockBuilder);
    expect(mockBuilder.iNaLHill(dummyINaLHill)).andReturn(mockBuilder);
    expect(mockBuilder.iNaLSaturation(dummyINaLSaturation)).andReturn(mockBuilder);
    if (assignSpreads) {
      expect(mockBuilder.iNaLSpread(dummyINaLSpread)).andReturn(mockBuilder);
    } else {
      expect(mockBuilder.iNaLSpread(null)).andReturn(mockBuilder);
    }
    expect(mockBuilder.itoC50(dummyItoC50)).andReturn(mockBuilder);
    expect(mockBuilder.itoHill(dummyItoHill)).andReturn(mockBuilder);
    expect(mockBuilder.itoSaturation(dummyItoSaturation)).andReturn(mockBuilder);
    if (assignSpreads) {
      expect(mockBuilder.itoSpread(dummyItoSpread)).andReturn(mockBuilder);
    } else {
      expect(mockBuilder.itoSpread(null)).andReturn(mockBuilder);
    }
  }

  protected SimulationInputVO assignSimulationInputProperties(final PortalFile mockPKFile,
                                                              final PortalFile mockCellMLFile,
                                                              final boolean assignSpreads)
                                                              throws Exception {
    final SimulationInputVO mockSimulationInput = mocksControl.createMock(SimulationInputVO.class);
    final SimulationInputVO.Builder mockBuilder = mocksControl.createMock(SimulationInputVO.Builder.class);
    // Testing the SimulationInputVO constructor and property assignment invocations.
    expectNew(SimulationInputVO.Builder.class)
             .andReturn(mockBuilder);
    expect(mockBuilder.modelIdentifier(dummyModelIdentifier))
          .andReturn(mockBuilder);
    expect(mockBuilder.pacingFrequency(dummyPacingFrequency))
          .andReturn(mockBuilder);
    expect(mockBuilder.pacingMaxTime(dummyPacingMaxTime)).andReturn(mockBuilder);
    assignMeasurementObservations(mockBuilder, assignSpreads);
    expect(mockBuilder.c50Type(dummyC50Type)).andReturn(mockBuilder);
    expect(mockBuilder.ic50Units(dummyIC50Units)).andReturn(mockBuilder);
    expect(mockBuilder.plasmaConcMin(dummyPlasmaConcMin)).andReturn(mockBuilder);
    expect(mockBuilder.plasmaConcMax(dummyPlasmaConcMax)).andReturn(mockBuilder);
    expect(mockBuilder.plasmaIntPtCount(dummyPlasmaIntPtCount))
          .andReturn(mockBuilder);
    expect(mockBuilder.plasmaIntPtLogScale(Boolean.FALSE.toString()))
          .andReturn(mockBuilder);
    expect(mockBuilder.plasmaConcPoints(dummyPlasmaConcPoints))
          .andReturn(mockBuilder);
    // The null multipart PK file generates a null PortalFile object.
    expect(mockBuilder.pkFile(mockPKFile)).andReturn(mockBuilder);
    expect(mockBuilder.cellMLFile(mockCellMLFile)).andReturn(mockBuilder);
    expect(mockBuilder.credibleIntervalPctiles(dummyCIPctiles))
          .andReturn(mockBuilder);
    expect(mockBuilder.notes(dummyNotes)).andReturn(mockBuilder);
    expect(mockBuilder.build()).andReturn(mockSimulationInput);

    return mockSimulationInput;
  }

  protected void activeTesting(final SimulationInputVO mockSimulationInput) 
                               throws RunInvocationException,
                                      AppManagerWSInvocationException,
                                      NoConnectionException {
    expect(mockHttpSession.getAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN))
          .andReturn(dummyModelAssignedToken).times(2);
    mockHttpSession.removeAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN);

    // No assignment problems
    expect(mockSimulationInput.hasAssignmentProblems()).andReturn(false);
    expect(mockPrincipal.getName()).andReturn(dummyPrincipalName);
    expect(mockClientDirectService.runSimulation(mockSimulationInput,
                                                 dummyPrincipalName))
          .andReturn(dummySimulationId);
    // >> setModelProperties()
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_INPUT_VALUES,
                                  mockSimulationInput)).andReturn(mockModel);
    final List<ModelVO> dummyModels = new ArrayList<ModelVO>();
    expect(mockClientDirectService.retrieveModels()).andReturn(dummyModels);
    expect(mockModel.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_MODELS,
                                  dummyModels))
          .andReturn(mockModel);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SHOW_DYNAMIC_CELLML,
                                  dummyAllowDynamicCellML))
          .andReturn(mockModel);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SHOW_UNCERTAINTIES,
                                  dummyAllowUncertainties))
          .andReturn(mockModel);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    // << setModelProperties()
  }

  protected void defaultPortalFeatureProcessing() {
    expect(ControllerUtil.allowFeature(PortalFeature.DYNAMIC_CELLMLS,
                                       mockClientDirectService))
          .andReturn(dummyAllowDynamicCellML);
    expect(ControllerUtil.allowFeature(PortalFeature.UNCERTAINTIES,
                                       mockClientDirectService))
          .andReturn(dummyAllowUncertainties);
  }

  protected void latterTesting(final SimulationInputVO mockSimulationInput) 
                               throws RunInvocationException,
                                      AppManagerWSInvocationException,
                                      NoConnectionException {
    // Lazy testing anything else (but emulating there not being any assignment problems).
    expect(mockHttpSession.getAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN))
           .andReturn(dummyModelAssignedToken).times(2);
    mockHttpSession.removeAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN);
    // No assignment problems.
    expect(mockSimulationInput.hasAssignmentProblems()).andReturn(false);
    expect(mockPrincipal.getName()).andReturn(dummyPrincipalName);
    expect(mockClientDirectService.runSimulation(isA(SimulationInputVO.class),
                                                 isA(String.class)))
           .andReturn(dummySimulationId);
    // >> setModelProperties()
    // Input values
    expect(mockModel.addAttribute(isA(String.class), anyObject()))
          .andReturn(mockModel);
    expect(mockClientDirectService.retrieveModels())
          .andReturn(new ArrayList<ModelVO>());
    // Any other, e.g. simulationId, exceptionMessage
    expect(mockModel.addAttribute(isA(String.class), anyObject()))
          .andReturn(mockModel).anyTimes();
    // << setModelProperties()
  }

  protected String runProcessInput() {
    return resultsController.processInput(dummyModelAssignedToken,
                                          dummyICaLC50, dummyICaLHill,
                                          dummyICaLSaturation, dummyICaLSpread,
                                          dummyIKrC50, dummyIKrHill,
                                          dummyIKrSaturation, dummyIKrSpread,
                                          dummyIK1C50, dummyIK1Hill,
                                          dummyIK1Saturation, dummyIK1Spread,
                                          dummyIKsC50, dummyIKsHill,
                                          dummyIKsSaturation, dummyIKsSpread,
                                          dummyItoC50, dummyItoHill,
                                          dummyItoSaturation, dummyItoSpread,
                                          dummyINaC50, dummyINaHill,
                                          dummyINaSaturation, dummyINaSpread,
                                          dummyINaLC50, dummyINaLHill,
                                          dummyINaLSaturation, dummyINaLSpread,
                                          dummyC50Type, dummyIC50Units,
                                          dummyModelIdentifier,
                                          dummyPacingFrequency,
                                          dummyPacingMaxTime, dummyPkOrConcs,
                                          dummyPlasmaConcPoints,
                                          mockMultipartPKFile,
                                          dummyPlasmaConcMax, dummyPlasmaConcMin,
                                          dummyPlasmaIntPtCount,
                                          dummyPlasmaIntPtLogScale, dummyNotes,
                                          dummySpreadsEnabled,
                                          mockMultipartCellMLFile,
                                          dummyCIPctiles,
                                          mockHttpSession, dummyLocale,
                                          mockModel, mockPrincipal);
  }
}