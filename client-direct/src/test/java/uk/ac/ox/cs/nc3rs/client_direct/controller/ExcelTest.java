/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.util.HashMap;
import java.util.Map;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.Model;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.controller.util.ControllerUtil;
import uk.ac.ox.cs.nc3rs.client_direct.exception.AppManagerWSInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.NoConnectionException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.ResultsNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.SimulationNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationCurrentResultsVO;

/**
 * Unit test the Excel controller.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { ControllerUtil.class } )
public class ExcelTest {

  private static final String errorInvalidSimulationId = "Invalid simulation identifier provided";

  private ClientDirectService mockClientDirectService;
  private Excel excelController;
  private IMocksControl mocksControl;
  private Model mockModel;
  private String dummySimulationId;

  @Before
  public void setUp() {
    excelController = new Excel();

    mocksControl = createStrictControl();
    mockClientDirectService = mocksControl.createMock(ClientDirectService.class);
    ReflectionTestUtils.setField(excelController, ClientDirectIdentifiers.COMPONENT_CLIENT_DIRECT_SERVICE,
                                 mockClientDirectService);

    mockModel = mocksControl.createMock(Model.class);
    dummySimulationId = "dummySimulationId";
  }

  @Test
  public void testGeneratesExcelView() throws AppManagerWSInvocationException,
                                              NoConnectionException,
                                              ResultsNotFoundException,
                                              SimulationNotFoundException {
    /*
     * Invalid simulation identifier.
     */
    mockStatic(ControllerUtil.class);
    expect(ControllerUtil.validSimulationId(dummySimulationId))
          .andReturn(false);

    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                                  errorInvalidSimulationId))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    String retrieved = excelController.generateExcelView(dummySimulationId, mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.VIEW_NAME_EXCEL, retrieved);

    resetAll();
    mocksControl.reset();

    /*
     * Valid simulation id + simulation results available.
     */
    dummySimulationId = "2";
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);

    final long dummySimulationIdLong = Long.valueOf(dummySimulationId);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationIdLong))
          .andReturn(mockModel);
    final Map<Short, String> dummyModelNames = new HashMap<Short, String>();
    expect(mockClientDirectService.retrieveModelNames())
          .andReturn(dummyModelNames);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MODEL_NAMES,
                                  dummyModelNames))
          .andReturn(mockModel);
    final SimulationCurrentResultsVO mockResultsVO = mocksControl.createMock(SimulationCurrentResultsVO.class);
    expect(mockClientDirectService.retrieveSimulationResults(dummySimulationIdLong))
          .andReturn(mockResultsVO);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                                  mockResultsVO))
          .andReturn(mockModel);


    replayAll();
    mocksControl.replay();

    retrieved = excelController.generateExcelView(dummySimulationId, mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.VIEW_NAME_EXCEL, retrieved);

    resetAll();
    mocksControl.reset();

    /*
     * Simulation not found exception.
     */
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);

    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationIdLong))
          .andReturn(mockModel);
    expect(mockClientDirectService.retrieveModelNames())
          .andReturn(dummyModelNames);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MODEL_NAMES,
                                  dummyModelNames))
          .andReturn(mockModel);
    final String dummySimulationNotFoundException = "dummySimulationNotFoundException";
    expect(mockClientDirectService.retrieveSimulationResults(dummySimulationIdLong))
          .andThrow(new SimulationNotFoundException(dummySimulationNotFoundException));
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                                  dummySimulationNotFoundException))
          .andReturn(mockModel);


    replayAll();
    mocksControl.replay();

    retrieved = excelController.generateExcelView(dummySimulationId, mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.VIEW_NAME_EXCEL, retrieved);

    resetAll();
    mocksControl.reset();

    /*
     * App Manager WS invocation exception.
     */
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);

    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationIdLong))
          .andReturn(mockModel);
    expect(mockClientDirectService.retrieveModelNames())
          .andReturn(dummyModelNames);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MODEL_NAMES,
                                  dummyModelNames))
          .andReturn(mockModel);
    final String dummyAppManagerWSInvocationException = "dummyAppManagerWSInvocationException";
    expect(mockClientDirectService.retrieveSimulationResults(dummySimulationIdLong))
          .andThrow(new AppManagerWSInvocationException(dummyAppManagerWSInvocationException));
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                                  dummyAppManagerWSInvocationException))
          .andReturn(mockModel);


    replayAll();
    mocksControl.replay();

    retrieved = excelController.generateExcelView(dummySimulationId, mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.VIEW_NAME_EXCEL, retrieved);

    resetAll();
    mocksControl.reset();

    /*
     * No connection exception.
     */
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);

    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationIdLong))
          .andReturn(mockModel);
    expect(mockClientDirectService.retrieveModelNames())
          .andReturn(dummyModelNames);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MODEL_NAMES,
                                  dummyModelNames))
          .andReturn(mockModel);
    final String dummyNoConnectionException = "dummyNoConnectionException";
    expect(mockClientDirectService.retrieveSimulationResults(dummySimulationIdLong))
          .andThrow(new NoConnectionException(dummyNoConnectionException));
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                                  dummyNoConnectionException))
          .andReturn(mockModel);


    replayAll();
    mocksControl.replay();

    retrieved = excelController.generateExcelView(dummySimulationId, mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.VIEW_NAME_EXCEL, retrieved);

    resetAll();
    mocksControl.reset();
    /*
     * Results not found exception.
     */
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);

    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationIdLong))
          .andReturn(mockModel);
    expect(mockClientDirectService.retrieveModelNames())
          .andReturn(dummyModelNames);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MODEL_NAMES,
                                  dummyModelNames))
          .andReturn(mockModel);
    final String dummyResultsNotFoundException = "dummyResultsNotFoundException";
    expect(mockClientDirectService.retrieveSimulationResults(dummySimulationIdLong))
          .andThrow(new ResultsNotFoundException(dummyResultsNotFoundException));
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                                  dummyResultsNotFoundException))
          .andReturn(mockModel);


    replayAll();
    mocksControl.replay();

    retrieved = excelController.generateExcelView(dummySimulationId, mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.VIEW_NAME_EXCEL, retrieved);
  }
}