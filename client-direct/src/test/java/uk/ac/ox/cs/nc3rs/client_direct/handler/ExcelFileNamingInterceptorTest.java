/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.handler;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;

/**
 * Unit test the Excel file naming.
 *
 * @author geoff
 */
public class ExcelFileNamingInterceptorTest {

  private ExcelFileNamingInterceptor interceptor;
  private HttpServletRequest mockRequest;
  private HttpServletResponse mockResponse;
  private IMocksControl mocksControl;
  private ModelAndView mockModelAndView;
  private Object mockHandler;

  @Before
  public void setUp() {
    interceptor = new ExcelFileNamingInterceptor();

    mocksControl = createStrictControl();
    mockHandler = mocksControl.createMock(Object.class);
    mockModelAndView = mocksControl.createMock(ModelAndView.class);
    mockRequest = mocksControl.createMock(HttpServletRequest.class);
    mockResponse = mocksControl.createMock(HttpServletResponse.class);
  }

  @Test
  public void testPostHandle() throws Exception {
    /*
     * Null request servlet path.
     */
    String dummyServletPath = null;
    expect(mockRequest.getServletPath()).andReturn(dummyServletPath);

    mocksControl.replay();

    interceptor.postHandle(mockRequest, mockResponse, mockHandler,
                           mockModelAndView);

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Servlet path doesn't contain Excel URL prefix.
     */
    dummyServletPath = "dummyServletPath";
    expect(mockRequest.getServletPath()).andReturn(dummyServletPath);

    mocksControl.replay();

    interceptor.postHandle(mockRequest, mockResponse, mockHandler,
                           mockModelAndView);

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Servlet path contains Excel URL prefix.
     */
    dummyServletPath = dummyServletPath.concat(ClientDirectIdentifiers.URL_PREFIX_EXCEL);
    expect(mockRequest.getServletPath()).andReturn(dummyServletPath);
    final Long dummySimulationId = 5L;
    final Map<String, Object> dummyModel = new HashMap<String, Object>();
    dummyModel.put(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                   dummySimulationId);
    expect(mockModelAndView.getModel()).andReturn(dummyModel);
    final Capture<String> captureName = newCapture();
    final Capture<String> captureValue = newCapture();
    mockResponse.setHeader(capture(captureName), capture(captureValue));

    mocksControl.replay();

    interceptor.postHandle(mockRequest, mockResponse, mockHandler,
                           mockModelAndView);

    mocksControl.verify();

    final String capturedName = captureName.getValue();
    final String capturedValue = captureValue.getValue();

    assertEquals("Content-Disposition", capturedName);
    assertEquals("attachment; filename=AP-Portal_" + dummySimulationId + ".xlsx",
                 capturedValue);
  }
}