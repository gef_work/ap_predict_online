/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.util.Locale;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.Model;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.controller.AJAX.JSON;
import uk.ac.ox.cs.nc3rs.client_direct.controller.util.ControllerUtil;
import uk.ac.ox.cs.nc3rs.client_direct.exception.AppManagerWSInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.NoConnectionException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.SimulationNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService;

/**
 * Unit test the AJAX Controller latest progress retrieval.
 * 
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { ControllerUtil.class } )
public class AJAXRetrieveLatestProgressTest {

  private AJAX ajaxController;
  private ClientDirectService mockClientDirectService;
  private IMocksControl mocksControl;
  private Locale locale;
  private MessageSource mockMessageSource;
  private Model mockModel;
  private String dummySimulationId;

  @Before
  public void setUp() {
    ajaxController = new AJAX();

    mocksControl = createStrictControl();

    mockClientDirectService = mocksControl.createMock(ClientDirectService.class);
    ReflectionTestUtils.setField(ajaxController,
                                 ClientDirectIdentifiers.COMPONENT_CLIENT_DIRECT_SERVICE,
                                 mockClientDirectService);
    mockModel = mocksControl.createMock(Model.class);
    mockMessageSource = mocksControl.createMock(MessageSource.class);
    ajaxController.setMessageSource(mockMessageSource);

    locale = new Locale("en");
    dummySimulationId = "dummySimulationId";
  }

  @Test
  public void testJSONClass() {
    String jsonStr = null;
    String exceptionStr = null;

    JSON json = ajaxController.new JSON(jsonStr, exceptionStr);
    assertNull(json.getJson());
    assertNull(json.getException());

    jsonStr = "jsonStr";
    exceptionStr = "exceptionStr";
    json = ajaxController.new JSON(jsonStr, exceptionStr);
    assertTrue(jsonStr.equals(json.getJson()));
    assertTrue(exceptionStr.equals(json.getException()));
  }

  @Test
  public void testRetrieveLatestProgress() throws AppManagerWSInvocationException,
                                                  NoConnectionException,
                                                  SimulationNotFoundException {
    /*
     * Invalid simulation identifier.
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
                    .andReturn(mockModel);
    mockStatic(ControllerUtil.class);
    expect(ControllerUtil.validSimulationId(dummySimulationId))
          .andReturn(false);

    Capture<String> captureKey = newCapture();
    Capture<JSON> captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    String retrieved = ajaxController.retrieveLatestProgress(dummySimulationId,
                                                             locale, mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.MODEL_ATTRIBUTE_LATEST_PROGRESS,
                 captureKey.getValue());
    JSON capturedJSON = captureJSON.getValue();
    assertNull(capturedJSON.getJson());
    assertEquals("Invalid Simulation identifier", capturedJSON.getException());
    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, retrieved);

    resetAll();
    mocksControl.reset();

    /*
     * Valid simulation identifier.
     */
    dummySimulationId = "2";
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    String dummyProgress = "dummyProgress";
    expect(mockClientDirectService.retrieveLatestProgress(Long.valueOf(dummySimulationId)))
          .andReturn(dummyProgress);
    captureKey = newCapture();
    captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveLatestProgress(dummySimulationId, locale,
                                                      mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.MODEL_ATTRIBUTE_LATEST_PROGRESS,
                 captureKey.getValue());
    capturedJSON = captureJSON.getValue();
    assertEquals(dummyProgress, capturedJSON.getJson());
    assertNull(capturedJSON.getException());
    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, retrieved);

    resetAll();
    mocksControl.reset();

    /*
     * Simulation not found exception.
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    String dummyExceptionMessage = "dummyExceptionMessage";
    expect(mockClientDirectService.retrieveLatestProgress(Long.valueOf(dummySimulationId)))
          .andThrow(new SimulationNotFoundException(dummyExceptionMessage));
    captureKey = newCapture();
    captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveLatestProgress(dummySimulationId, locale,
                                                      mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.MODEL_ATTRIBUTE_LATEST_PROGRESS,
    captureKey.getValue());
    capturedJSON = captureJSON.getValue();
    assertNull(capturedJSON.getJson());
    assertEquals(dummyExceptionMessage, capturedJSON.getException());
    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, retrieved);

    resetAll();
    mocksControl.reset();

    /*
     * App manager WS invocation exception
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    expect(mockClientDirectService.retrieveLatestProgress(Long.valueOf(dummySimulationId)))
          .andThrow(new AppManagerWSInvocationException(dummyExceptionMessage));
    // >> queryMessageSource()
    expect(mockMessageSource.getMessage(isA(String.class), anyObject(Object[].class),
                                        isA(Locale.class)))
          .andThrow(new NoSuchMessageException("")).anyTimes();
    // << queryMessageSource()
    captureKey = newCapture();
    captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveLatestProgress(dummySimulationId, locale,
                                                      mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.MODEL_ATTRIBUTE_LATEST_PROGRESS,
    captureKey.getValue());
    capturedJSON = captureJSON.getValue();
    assertNull(capturedJSON.getJson());
    assertEquals(ClientDirectIdentifiers.APP_MANAGER_SOMETHING_WRONG,
                 capturedJSON.getException());
    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, retrieved);

    resetAll();
    mocksControl.reset();

    /*
     * No connection exception.
     */
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                                  dummySimulationId))
          .andReturn(mockModel);
    expect(ControllerUtil.validSimulationId(dummySimulationId)).andReturn(true);
    expect(mockClientDirectService.retrieveLatestProgress(Long.valueOf(dummySimulationId)))
          .andThrow(new NoConnectionException(dummyExceptionMessage));
    // >> queryMessageSource()
    expect(mockMessageSource.getMessage(isA(String.class), anyObject(Object[].class),
                                        isA(Locale.class)))
          .andThrow(new NoSuchMessageException("")).anyTimes();
    // << queryMessageSource()
    captureKey = newCapture();
    captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureKey), capture(captureJSON)))
          .andReturn(mockModel);

    replayAll();
    mocksControl.replay();

    retrieved = ajaxController.retrieveLatestProgress(dummySimulationId, locale,
                                                      mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.MODEL_ATTRIBUTE_LATEST_PROGRESS,
    captureKey.getValue());
    capturedJSON = captureJSON.getValue();
    assertNull(capturedJSON.getJson());
    assertEquals(ClientDirectIdentifiers.APP_MANAGER_OUT_OF_CONTACT,
                 capturedJSON.getException());
    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, retrieved);
  }
}