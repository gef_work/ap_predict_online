/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.entity;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit test the job result entity.
 * 
 * @author geoff
 */
public class JobResultTest {

  private BigDecimal dummyReference;
  private boolean dummyForTranslation;
  private IMocksControl mocksControl;
  private String dummyAPD90;
  private String dummyDeltaAPD90;
  private String dummyTimes;
  private String dummyVoltages;
  private String dummyQNet;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
  }

  @Test
  public void testInitialisingConstructor() {
    try {
      new JobResult(dummyReference, dummyTimes, dummyVoltages, dummyAPD90,
                    dummyDeltaAPD90, dummyQNet, dummyForTranslation);
      fail("Should not permit null compound concentration/times/voltages/APD90/deltaAPD90.");
    } catch (IllegalArgumentException e) {}

    dummyReference = BigDecimal.TEN;
    try {
      new JobResult(dummyReference, dummyTimes, dummyVoltages, dummyAPD90,
                    dummyDeltaAPD90, dummyQNet, dummyForTranslation);
      fail("Should not permit null times/voltages/APD90/deltaAPD90.");
    } catch (IllegalArgumentException e) {}

    dummyTimes = "dummyTimes";
    try {
      new JobResult(dummyReference, dummyTimes, dummyVoltages, dummyAPD90,
                    dummyDeltaAPD90, dummyQNet, dummyForTranslation);
      fail("Should not permit null voltages/APD90/deltaAPD90.");
    } catch (IllegalArgumentException e) {}

    dummyVoltages = "dummyVoltages";
    try {
      new JobResult(dummyReference, dummyTimes, dummyVoltages, dummyAPD90,
                    dummyDeltaAPD90, dummyQNet, dummyForTranslation);
      fail("Should not permit null APD90/deltaAPD90.");
    } catch (IllegalArgumentException e) {}

    dummyAPD90 = "dummyAPD90";
    try {
      new JobResult(dummyReference, dummyTimes, dummyVoltages, dummyAPD90,
                    dummyDeltaAPD90, dummyQNet, dummyForTranslation);
      fail("Should not permit null deltaAPD90.");
    } catch (IllegalArgumentException e) {}

    dummyDeltaAPD90 = "dummyDeltaAPD90";
    final JobResult jobResult1 = new JobResult(dummyReference, dummyTimes,
                                               dummyVoltages, dummyAPD90,
                                               dummyDeltaAPD90, dummyQNet,
                                               dummyForTranslation);

    assertEquals(dummyReference, jobResult1.getReference());
    assertNotSame(dummyReference, jobResult1.getReference());
    assertSame(dummyAPD90, jobResult1.getAPD90());
    assertSame(dummyDeltaAPD90, jobResult1.getDeltaAPD90());
    assertFalse(jobResult1.getForTranslation());
    assertSame(dummyTimes, jobResult1.getTimes());
    assertSame(dummyVoltages, jobResult1.getVoltages());
    assertTrue(jobResult1.toString().contains(dummyVoltages));

    final JobResult jobResult2 = new JobResult(dummyReference, dummyTimes,
                                               dummyVoltages, dummyAPD90,
                                               dummyDeltaAPD90, dummyQNet,
                                               dummyForTranslation);
    assertEquals(jobResult1, jobResult2);
    assertNotSame(jobResult1, jobResult2);

    dummyForTranslation = true;
    final JobResult jobResult3 = new JobResult(dummyReference, dummyTimes,
                                               dummyVoltages, dummyAPD90,
                                               dummyDeltaAPD90, dummyQNet,
                                               dummyForTranslation);
    assertNotEquals(jobResult1, jobResult3);
    assertTrue(jobResult3.getForTranslation());

    dummyTimes = "a,b";
    dummyVoltages = "1";
    try {
      new JobResult(dummyReference, dummyTimes, dummyVoltages, dummyAPD90, 
                    dummyDeltaAPD90, dummyQNet, dummyForTranslation);
      fail("Should not permit time count not corresponding to voltage count.");
    } catch (IllegalArgumentException e) {}
  }

  @Test
  public void testSetJob() {
    dummyReference = BigDecimal.TEN;
    dummyTimes = "dummyTimes";
    dummyVoltages = "dummyVoltages";
    dummyAPD90 = "dummyAPD90";
    dummyDeltaAPD90 = "dummyDeltaAPD90";
    final JobResult jobResult1 = new JobResult(dummyReference, dummyTimes,
                                               dummyVoltages, dummyAPD90,
                                               dummyDeltaAPD90, dummyQNet,
                                               dummyForTranslation);

    final Job mockJob = mocksControl.createMock(Job.class);
    mockJob.addJobResult(isA(JobResult.class));

    mocksControl.replay();

    jobResult1.setJob(mockJob);

    mocksControl.verify();
  }
}