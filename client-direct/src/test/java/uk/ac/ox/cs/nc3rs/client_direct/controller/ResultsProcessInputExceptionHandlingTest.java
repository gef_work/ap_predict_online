/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.util.Locale;

import org.easymock.Capture;
import org.junit.Test;
import org.springframework.context.NoSuchMessageException;

import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.exception.AppManagerWSInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.NoConnectionException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.RunInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;

/**
 * Unit test the results controller's input processing run exception handling.
 * 
 * @author geoff
 */
public class ResultsProcessInputExceptionHandlingTest extends
             AbstractResultsProcessInputTest {

  @Test
  public void testSimulationRunInvocationExceptions() throws Exception {
    /*
     * App manager WS invocation exception.
     */
    defaultPortalFeatureProcessing();
    final String dummySessionToken = dummyModelAssignedToken;
    SimulationInputVO mockSimulationInput = assignSimulationInputProperties(null, null,
                                                                            false);

    expect(mockHttpSession.getAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN))
          .andReturn(dummySessionToken).times(2);
    mockHttpSession.removeAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN);

    expect(mockSimulationInput.hasAssignmentProblems()).andReturn(false);
    final String dummyPrincipalname = "dummyPrincipalName";
    expect(mockPrincipal.getName()).andReturn(dummyPrincipalname);
    String dummyErrorMessage = "dummyErrorMessage1";
    expect(mockClientDirectService.runSimulation(mockSimulationInput,
                                                 dummyPrincipalname))
          .andThrow(new AppManagerWSInvocationException(dummyErrorMessage));
    // anyTimes() used as no order of test execution and message queue system may have been populated.
    Capture<String> captureMessageKey = newCapture();
    Capture<Object[]> captureArgs = newCapture();
    Capture<Locale> captureLocale = newCapture();
    expect(mockMessageSource.getMessage(capture(captureMessageKey),
                                        capture(captureArgs), 
                                        capture(captureLocale)))
          .andThrow(new NoSuchMessageException(null)).anyTimes();

    mockStatic(Process.class);
    Process.setPropertiesForInput(mockClientDirectService, mockSimulationInput,
                                  mockHttpSession,
                                  ClientDirectIdentifiers.APP_MANAGER_SOMETHING_WRONG,
                                  mockModel);

    replayAll();
    mocksControl.replay();

    String returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_INPUT, returnPage);
    assertEquals(dummyErrorMessage, captureArgs.getValue()[0].toString());

    resetAll();
    mocksControl.reset();

    /*
     * No connection exception.
     */
    defaultPortalFeatureProcessing();
    mockSimulationInput = assignSimulationInputProperties(null, null, false);

    expect(mockHttpSession.getAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN))
          .andReturn(dummySessionToken).times(2);
    mockHttpSession.removeAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN);

    expect(mockSimulationInput.hasAssignmentProblems()).andReturn(false);
    expect(mockPrincipal.getName()).andReturn(dummyPrincipalname);
    dummyErrorMessage = "dummyErrorMessage2";
    expect(mockClientDirectService.runSimulation(mockSimulationInput,
                                                 dummyPrincipalname))
          .andThrow(new NoConnectionException(dummyErrorMessage));
    // anyTimes() used as no order of test execution and message queue system may have been populated.
    captureMessageKey = newCapture();
    captureArgs = newCapture();
    captureLocale = newCapture();
    expect(mockMessageSource.getMessage(capture(captureMessageKey),
                                        capture(captureArgs), 
                                        capture(captureLocale)))
          .andThrow(new NoSuchMessageException(null)).anyTimes();

    mockStatic(Process.class);
    Process.setPropertiesForInput(mockClientDirectService, mockSimulationInput,
                                  mockHttpSession,
                                  ClientDirectIdentifiers.APP_MANAGER_OUT_OF_CONTACT,
                                  mockModel);

    replayAll();
    mocksControl.replay();

    returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_INPUT, returnPage);
    assertNull(captureArgs.getValue());

    resetAll();
    mocksControl.reset();

    /*
     * Run invocation exception.
     */
    defaultPortalFeatureProcessing();
    mockSimulationInput = assignSimulationInputProperties(null, null, false);

    expect(mockHttpSession.getAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN))
          .andReturn(dummySessionToken).times(2);
    mockHttpSession.removeAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN);

    expect(mockSimulationInput.hasAssignmentProblems()).andReturn(false);
    expect(mockPrincipal.getName()).andReturn(dummyPrincipalname);
    dummyErrorMessage = "dummyErrorMessage3";
    expect(mockClientDirectService.runSimulation(mockSimulationInput,
                                                 dummyPrincipalname))
          .andThrow(new RunInvocationException(dummyErrorMessage));
    // anyTimes() used as no order of test execution and message queue system may have been populated.
    captureMessageKey = newCapture();
    captureArgs = newCapture();
    captureLocale = newCapture();
    expect(mockMessageSource.getMessage(capture(captureMessageKey),
                                        capture(captureArgs), 
                                        capture(captureLocale)))
          .andThrow(new NoSuchMessageException(null)).anyTimes();

    mockStatic(Process.class);
    Process.setPropertiesForInput(mockClientDirectService, mockSimulationInput,
                                  mockHttpSession, dummyErrorMessage, mockModel);

    replayAll();
    mocksControl.replay();

    returnPage = runProcessInput();

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientDirectIdentifiers.PAGE_INPUT, returnPage);
    assertNull(captureArgs.getValue());
  }
}