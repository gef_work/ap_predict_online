insert into users(username, password, enabled) values ('afruit', 'appbanora', true);
insert into users(username, password, enabled) values ('atea', 'darjeassam', true);
insert into authorities(username, authority) values ('afruit', 'ROLE_USER');
insert into authorities(username, authority) values ('atea', 'ROLE_USER');

commit;