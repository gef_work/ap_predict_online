<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:util="http://www.springframework.org/schema/util"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util.xsd">

  <description>
    <![CDATA[
    Client-direct general site-specific configurations.
    ]]>
  </description>

  <util:map id="c50Units"
            map-class="java.util.LinkedHashMap">
    <description>
      <![CDATA[
      LinkedHashMap so that first 'entry' below is considered the default.
      The key strings must not be changed as they referenced elsewhere.
      The value strings associated with the keys can be changed but they will be expected to 
        retain their meaning, e.g. "IC50 (nM)" could be changed to "C50 (nanoMolar)", but not
        "IC25 (nM)"!
      Entries can be removed but at least one entry must be defined.
      ]]>
    </description>
    <entry key="unit_c50_pIC50" value="pIC50 (-log(M))" />
    <entry key="unit_c50_IC50_M" value="IC50 (M)" />
    <entry key="unit_c50_IC50_uM" value="IC50 (µM)" />
    <entry key="unit_c50_IC50_nM" value="IC50 (nM)" />
  </util:map>

  <util:constant id="ION_CURRENT_ICAL"
                 static-field="uk.ac.ox.cs.nc3rs.client_direct.value.IonCurrent.ICaL" />
  <util:constant id="ION_CURRENT_IK1"
                 static-field="uk.ac.ox.cs.nc3rs.client_direct.value.IonCurrent.IK1" />
  <util:constant id="ION_CURRENT_IKR"
                 static-field="uk.ac.ox.cs.nc3rs.client_direct.value.IonCurrent.IKr" />
  <util:constant id="ION_CURRENT_IKS"
                 static-field="uk.ac.ox.cs.nc3rs.client_direct.value.IonCurrent.IKs" />
  <util:constant id="ION_CURRENT_INA"
                 static-field="uk.ac.ox.cs.nc3rs.client_direct.value.IonCurrent.INa" />
  <util:constant id="ION_CURRENT_ITO"
                 static-field="uk.ac.ox.cs.nc3rs.client_direct.value.IonCurrent.Ito" />
  <util:constant id="ION_CURRENT_INAL"
                 static-field="uk.ac.ox.cs.nc3rs.client_direct.value.IonCurrent.INaL" />

  <util:set id="pctiles"
            set-class="java.util.TreeSet"
            value-type="java.math.BigDecimal">
    <description>
      <![CDATA[
        '38 68 86 95' would represent half, 1, 1.5 and 2 standard deviations.
      ]]>
    </description>
    <value>38</value>
    <value>68</value>
    <value>86</value>
    <value>95</value>
  </util:set>

  <util:map id="spreads"
            map-class="java.util.HashMap">
    <description>
      <![CDATA[
      ]]>
    </description>
    <entry key-ref="ION_CURRENT_ICAL" value="0.15" />
    <entry key-ref="ION_CURRENT_IK1" value="0.18" />
    <entry key-ref="ION_CURRENT_IKR" value="0.18" />
    <entry key-ref="ION_CURRENT_IKS" value="0.17" />
    <entry key-ref="ION_CURRENT_INA" value="0.2" />
    <entry key-ref="ION_CURRENT_ITO" value="0.15" />
    <entry key-ref="ION_CURRENT_INAL" value="0.2" />
  </util:map>

  <bean id="configuration"
        class="uk.ac.ox.cs.nc3rs.client_direct.config.Configuration">
    <description>
      <![CDATA[
      Configuration object.
      Note : Not all configuration properties are constructor-provided. Some are @Autowire'd by
        type or via @Resource annotation.
      ]]>
    </description>
    <constructor-arg name="recommendedPlasmaConcMax" value="100">
      <description>
        <![CDATA[
        Default recommended value for maximum plasma concentration (µM) value.
        Users can enter values higher than the recommended maximum. 
        ]]>
      </description>
    </constructor-arg>
    <constructor-arg name="plasmaConcMin" value="0">
      <description>
        <![CDATA[
        Minimum plasma concentration (µM).
        Users cannot enter values lower than this.
        ]]>
      </description>
    </constructor-arg>
    <constructor-arg name="spreads" ref="spreads">
      <description>
        <![CDATA[
        Ion current spreads.
        ]]>
      </description>
    </constructor-arg>
    <constructor-arg name="credibleIntervalPctiles" ref="pctiles">
      <description>
        <![CDATA[
        Delta APD90 credible interval percentiles.
        ]]>
      </description>
    </constructor-arg>
  </bean>
</beans>