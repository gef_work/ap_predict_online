/**
 * Retrieve flag to indicate if the input option was Concentration Points.
 * 
 * @param input_option Simulation's input option.
 * @returns <code>true</code> if Concentration Points submitted, otherwise <code>false</code>.
 */
function is_conc_points_simulation(input_option) {
  return (input_option == INPUT_OPTION_CONC_POINTS);
}

/**
 * Retrieve flag to indicate if the input option was Concentration Range.
 * 
 * @param input_option Simulation's input option.
 * @returns <code>true</code> if Concentration Range submitted, otherwise <code>false</code>.
 */
function is_conc_range_simulation(input_option) {
  return (input_option == INPUT_OPTION_CONC_RANGE);
}

/**
 * Retrieve flag to indicate if the input option was PK file.
 * 
 * @param input_option Simulation's input option.
 * @returns <code>true</code> if PK file submitted, otherwise <code>false</code>.
 */
function is_pk_simulation(input_option) {
  return (input_option == INPUT_OPTION_PK);
}