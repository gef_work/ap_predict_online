var timer = jQuery('<img>').attr({ 'src': SPINNER_LOCATION, 'alt': 'Spinner image' });
var results_call_failure_count = 0;
var js_error_results;

jQuery(document).ready(
  function() {
    js_error_results = jQuery('#js_error_results');
  }
);

/* See uk.ac.ox.cs.nc3rs.app_manager.business.ProgressMonitor */
var APP_MANAGER_PROGRESS_FILE_NOT_FOUND = 'Progress file not found - counting down .. ';
var APP_MANAGERS_MAX_WARNINGS_COUNT = 12;

/**
 * Invoke the AJAX request for simulation results.
 * 
 *  @param simulation_id Simulation identifier.
 */
function ajax_for_results(simulation_id) {
  var results_url = CONTEXT_PATH + URL_PREFIX_RESULTS + simulation_id;
  js_error_results.hide();
  jQuery.ajax({
    url: results_url,
    async: true,
    type: 'GET',
    data: {},
    dataType: 'json',
    /* 2m allows for PKPD retrieve from app-manager, persist and prep for display! */
    timeout: 120000,
    contentType: 'application/json; charset=utf-8',
    success: function(response) {
               var json = cs_retrieve_json(response, MODEL_ATTRIBUTE_RESULTS, 'results',
                                           js_error_results);
               if (json != undefined) {
                 /* Reset the js error element and count (if it's visible) */
                 if (js_error_results.is(':visible')) {
                   cs_hide_js_error(js_error_results);
                   results_call_failure_count = 0;
                 }
                 var json_obj = jQuery.parseJSON(json);
                 /* SimulationCurrentResultsVO properties */
                 var simulation = json_obj['simulation'];
                 var progress = json_obj['progress'];
                 var results = json_obj['results'];
                 var app_manager_id = simulation['appManagerId'];
                 cs_assign_job_details(app_manager_id, HARDCODED_JOB_DETAIL);

                 DIV_LOADING.remove();

                 if (!jQuery.isEmptyObject(results)) {
                   /* Results have arrived */
                   results_arrival_actions();

                   process_results_response(simulation_id, results);
                   /* Normally a change event registered on the radios generated during
                      processing delta APD90 would trigger this call. */
                   show_ap_graph(retrieve_job_title(app_manager_id));
                   /* dialogs used when messages available */
                   set_dialogs();
                 } else {
                   /* No results arrived yet */
                   DIV_RESULTS.hide();
                   DIV_ADVICE.show();
                   var latest_progress = progress[0];
                   if (latest_progress != undefined) {

                     var progress_bar = jQuery('#progressbar');
                     var progress_label = jQuery('#progresslabel');

                     var latest_status = latest_progress.status;
                     if (latest_status.indexOf(TEXT_PROG) != -1) {
                       var use_status = latest_status.replace(TEXT_PROG, '');
                       if (use_status.indexOf(APP_MANAGER_PROGRESS_FILE_NOT_FOUND) != -1) {

                         var progress_bar = jQuery('#progressbar');
                         var progress_label = jQuery('#progresslabel');

                         var count = use_status.replace(APP_MANAGER_PROGRESS_FILE_NOT_FOUND, '') * 1;
                         var pct = Math.floor(((APP_MANAGERS_MAX_WARNINGS_COUNT - count) * 100) /
                                                APP_MANAGERS_MAX_WARNINGS_COUNT);

                         var label_text = TEXT_PREPARING + ' : ' + pct + '%';

                         if (progress_bar.is(':visible')) {
                           progress_bar.progressbar('value', pct);
                           progress_label.text(label_text);
                         } else {
                           var outer_div = jQuery('<div />').css({ 'width': '400px',
                                                                   'margin': '0 auto' });
                           progress_bar = jQuery('<div />').attr('id', 'progressbar')
                                                           .width('200px')
                                                           .text('')
                                                           .css({ 'width': '50%',
                                                                  'margin': '0 auto'});
                           progress_label = jQuery('<div />').attr('id', 'progresslabel')
                                                             .text(label_text) 
                                                             .css({ 'float': 'left',
                                                                    'margin-top': '4px' });
                           outer_div.append(progress_label);
                           outer_div.append(progress_bar);
                           DIV_PROGRESS.html(outer_div).show();

                           progress_bar.progressbar({
                             value: false
                           });
                           jQuery('.ui-progressbar').css({ 'height': '1.5em' });
                         }
                       } else {
                         DIV_PROGRESS.html(use_status).show();
                       }
                     }
                   } else {
                     if (!timer.is(':visible')) {
                       DIV_PROGRESS.append(jQuery('<span>').text(TEXT_PLEASE_WAIT));
                       DIV_PROGRESS.append(timer).show();
                     }
                   }
                   setTimeout(function() { ajax_for_results(simulation_id); }, 3000);
                 }
               }
             },
    error: function(xhr, status, error) {
             cs_handle_error(error, 'results', js_error_results);
             /* Try a few times to reconnect but give up eventually! */
             if (results_call_failure_count++ < 5) {
               setTimeout(function() { ajax_for_results(simulation_id); }, 6000);
             }
           }
  });
}

/**
 * Actions to perform when simulation results have arrived.
 */
function results_arrival_actions() {
  DIV_PROGRESS.empty().hide();
  clearInterval(SETINTERVAL_PROGRESS_ID);

  DIV_ADVICE.remove();
  DIV_RESULTS.show();
}

/**
 * Set the dialogs on buttons if there are simulation messages to show.
 */
function set_dialogs() {
  jQuery('button[name^=msgbutton_]').each(function() {
    var button_obj = jQuery(this);
    var msgbutton_name = button_obj.attr('name');
    var messages_id = 'messages_' + msgbutton_name.replace('msgbutton_', '');
    var messages_obj = jQuery('div[id=' + messages_id + ']');
    messages_obj.dialog({
      autoOpen: false
    });
  });

  jQuery('button[name^=msgbutton_]').click(function() {
    var msgbutton_name = jQuery(this).attr('name');
    var messages_id = 'messages_' + msgbutton_name.replace('msgbutton_', '');
    var el = jQuery('div[id=' + messages_id + ']');
    el.dialog({
      autoOpen: false,
      show: {
        effect: 'blind',
        duration: 100
      },
      hide: {},
      width: 500,
      position: {
        my: 'left top',
        at: 'left bottom',
        of: jQuery(this)
      }
    });
    el.dialog('open');
  });
}