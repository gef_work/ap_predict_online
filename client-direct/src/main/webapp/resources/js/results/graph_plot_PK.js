/**
 * Plot PKPD data.
 */
function show_plot_PK_graph() {

  if (PLOT_PK_DATASETS.length == 0) return;

  var plot_PK_colour_idx = 0;

  var timept_min;
  var timept_max;
  var apd90_min;
  var apd90_max;
  var dataset_count = 0;

  jQuery.each(PLOT_PK_DATASETS, function(key, val) {
    var existing_data = val.data;
    jQuery.each(existing_data, function() {
      dataset_count++;
      var timepoint = this[0] * 1.0;
      if (timept_min === undefined) timept_min = timepoint;
      if (timept_max === undefined) timept_max = timepoint;

      if (timepoint > timept_max) timept_max = timepoint;
      if (timepoint < timept_min) timept_min = timepoint;

      var apd90 = this[1] * 1.0;
      if (apd90_min === undefined) apd90_min = apd90;
      if (apd90_max === undefined) apd90_max = apd90;

      if (apd90 > apd90_max) apd90_max = apd90;
      if (apd90 < apd90_min) apd90_min = apd90;
    });
  });

  var overview_options = {
          legend: { 
                    show: true,
                    container: $('#plot_PK_overview_legend_trace')
                   },
          series: {
                    lines: {
                             show: true, lineWidth: 1
                            },
                    shadowSize: 0
                  },
          xaxis: { ticks: 4 },
          yaxis: { ticks: 4},
          grid: { color: '#999' },
          selection: { mode: 'xy' }
  };

  function getPKDefaultMainPanelOptions() {
    var options = { legend: { show: false },
                    yaxis: { axisLabel: GRAPH_LABEL_APD90,
                             axisLabelUseCanvas: true },
                    xaxis: { autoscaleMargin: 0.05,
                             axisLabel: GRAPH_LABEL_TIMEPOINT_H,
                             axisLabelPadding: 10,
                             axisLabelUseCanvas: true },
                    selection: { mode: 'xy' },
                    grid: { hoverable: true } };
  
    return $.extend(true, {}, options);
  };

  var main_options = getPKDefaultMainPanelOptions();

  /* Hard-code color indices to prevent them from shifting as things are turned on/off */
  $.each(PLOT_PK_DATASETS, function(key, val) {
    val.color = plot_PK_colour_idx;
    ++plot_PK_colour_idx;
  });
  
  /* Plot the original graph now and retain a holder for it. */
  var plot = $.plot($('#plot_PK_placeholder_trace'), PLOT_PK_DATASETS, main_options);
  /* Plot the overview graph and legend and retain a holder for it.*/
  var overview = $.plot($('#plot_PK_overview_trace'), PLOT_PK_DATASETS, overview_options);
  /* Make the plot overview retain the original axes, even when different data is plotted.*/
  overview_options = $.extend(true, {}, overview_options, {
                                                            xaxis: {
                                                                     min: overview.getAxes().xaxis.min,
                                                                     max: overview.getAxes().xaxis.max
                                                                   },
                                                            yaxis: { 
                                                                     min: overview.getAxes().yaxis.min,
                                                                     max: overview.getAxes().yaxis.max
                                                                   }
                                                          });
  /* Insert checkboxes */
  var choiceContainer = $('#plot_PK_choices_trace');
  choiceContainer.text('');
  $.each(PLOT_PK_DATASETS, function(key, val) {
    var plot_label = val.label;
    var plot_colour = val.color;
    var plot_messages = val.messages;

    /* Class legendColorBox defined in jquery.flot.js in function insertLegend() */
    var legendColorBox = $('#plot_PK_overview_legend_trace .legendColorBox:eq(' + plot_colour + ') div div');

    /* moz wasn't happy using border-color, which ie didn't mind! */
    var plot_colour = legendColorBox.css('border-left-color');

    var checkbox_id = 'id_pk_' + key;
    var checkbox = jQuery('<input />').attr( { id: checkbox_id,
                                               type: 'checkbox',
                                               name: key,
                                               checked: 'checked' } );
    var new_span = jQuery('<span />').css('background-color', plot_colour).html('&nbsp;&nbsp;');
    var checkbox_label = jQuery('<label />').attr( { for : checkbox_id } ).text(plot_label);

    choiceContainer.append(checkbox);
    choiceContainer.append(new_span);
    choiceContainer.append('&nbsp;&nbsp;');
    choiceContainer.append(checkbox_label);
    /* Show message view buttons if there's any to display for plot */
    if (plot_messages) {
      var message_button_id = 'pk_msgbutton_' + key;
      var message_div_id = 'pk_messages_' + key;

      var message_button = jQuery('<button />').attr( { id: message_button_id } ).text(TEXT_VIEW_MESSAGES);
      var message_div = jQuery('<div />').attr( { id: message_div_id, title: plot_label } );
      message_div.append(jQuery('<pre />').text(plot_messages));

      choiceContainer.append('&nbsp;&nbsp;');
      choiceContainer.append(message_button);
      choiceContainer.append(message_div);
    }
    choiceContainer.append('<br />');
  });

  /* Now connect the two - this method sets the "selection" ranges on both plots, and replots the main plot. */
  $('#plot_PK_placeholder_trace').bind('plotselected', function (event, ranges) {
    /* Clamp the zooming to prevent eternal zoom */
    if (ranges.xaxis.to - ranges.xaxis.from < 0.00001)
      ranges.xaxis.to = ranges.xaxis.from + 0.00001;
    if (ranges.yaxis.to - ranges.yaxis.from < 0.00001)
      ranges.yaxis.to = ranges.yaxis.from + 0.00001;
    main_options = $.extend(true, {}, main_options, {
                                                      xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to },
                                                      yaxis: { min: ranges.yaxis.from, max: ranges.yaxis.to }
                                                    });
    overview.setSelection(ranges, true);
    plotPKAccordingToChoicesAndZooming();
  });

  /* This method just calls the method on the main plot above. */
  $('#plot_PK_overview_trace').bind('plotselected', function (event, ranges) {
    plot.setSelection(ranges);
  });

  /* Gets called for all interactive data plotting. */
  function plotPKAccordingToChoicesAndZooming() {
    /* 'data' is the section of 'PLOT_PK_DATASETS' which will be plotted at the moment. */
    var data = [];
    choiceContainer.find('input:checked').each(function() {
      var key = $(this).attr('name');
      if (key && PLOT_PK_DATASETS[key]) {
        data.push(PLOT_PK_DATASETS[key]);
      }
    });
    if (data.length > 0) {
      plot = $.plot($('#plot_PK_placeholder_trace'), data, main_options);
      ranges = { xaxis: { from: plot.getAxes().xaxis.min, to: plot.getAxes().xaxis.max},
                 yaxis: { from: plot.getAxes().yaxis.min, to: plot.getAxes().yaxis.max} };
      overview = $.plot($('#plot_PK_overview_trace'), data, overview_options);
      overview.setSelection(ranges, true);
    }
  }

  /* If inputs are detected on the checkboxes fire off this method: */
  choiceContainer.find('input').click(plotPKAccordingToChoicesAndZooming);

  /* Reset button */
  $('#plot_PK_clear_selection_trace').click(resetPKEverything);

  /* Reset method. */
  function resetPKEverything() {
    /* Check all the boxes. */
    choiceContainer.find('input').each(function () {
                                         if ($(this).attr('checked') == false) {
                                           $(this).click();
                                         }
                                       });
    /* Let the main plot choose its own axes again */
    main_options = getPKDefaultMainPanelOptions();

    plotPKAccordingToChoicesAndZooming();
    overview.clearSelection();
  }

  var previousPoint = null;
  var precision = 8;
  var fixed_decimal_place = 3;
  $('#plot_PK_placeholder_trace').bind('plothover', function (event, pos, item) {
    if (typeof pos.y != 'undefined') {
      /* Ensure display of current mouse position axis values only while over plot area */
      var axes = plot.getAxes();
      var x_axis = axes.xaxis;
      var y_axis = axes.yaxis;

      /* pos.x is the non-log value of the cursor x pos */
      var point_val_x = point_val_y = '';
      if (pos.x >= x_axis.min && pos.x <= x_axis.max && pos.y >= y_axis.min && pos.y <= y_axis.max) {
        /* Display point values (e.g. timepoint and % change) only when within the plot
           axis range */
        point_val_x = pos.x.toFixed(fixed_decimal_place) + ' h'; 
        point_val_y = pos.y.toFixed(fixed_decimal_place) + ' ms';
      }
      $('#pk_point_val_x').html(point_val_x);
      $('#pk_point_val_y').text(point_val_y);

      if (item) {
        if (previousPoint != item.datapoint) {
          previousPoint = item.datapoint;

          $('#tooltip').remove();
          var x = item.datapoint[0],
              y = item.datapoint[1].toFixed(fixed_decimal_place);

          var contents = '[' + item.series.label + '] : ' + TEXT_AT_A_TIMEPOINT_OF + '. ' + 
                         x + 'h ' + TEXT_HAS_APD90 + ' ' + y + 'ms';
          show_tooltip(item.pageX, item.pageY, contents);
        }
      } else {
        $('#tooltip').remove();
        previousPoint = null;
      }
    }
  });

  /* leave the hiding until last (otherwise flot may complain */
  $('#plot_PK_overview_trace').hide();
  $('.graph_plot_PK_overview_legend_trace').hide();
}