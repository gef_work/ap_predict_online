var min_pct_change = -20.0;
var max_pct_change = 20.0;

var id_pct_change_prefix = 'id_pct_change_';
var pct_change_choices_trace_id = 'pct_change_choices_trace';
var pct_change_clear_selection_trace_id = 'pct_change_clear_selection_trace';
var pct_change_default_text_id = 'pct_change_default_text';
var pct_change_overview_trace_id = 'pct_change_overview_trace';
var pct_change_placeholder_trace_id = 'pct_change_placeholder_trace';

/**
 * Determine if the % change value is within configured min/max values (used to limit the 
 * % change axis extent).
 * 
 * @param pct_change % Change value.
 * @return true if pct change value is within the configured min/max values, otherwise false.
 */
function is_within_minmax(pct_change) {
  return pct_change >= min_pct_change && pct_change <= max_pct_change;
}

/* Shared between Delta APD90 and qNet (if available) */ 
var specific_x_ticks = [];
 
/**
 * Show the graph which, generally, is the upper graph showing Delta APD90 vs. Conc.
 * 
 * @param input_option Simulation input option.
 */
function show_pct_change_graph(input_option) {

  if (PCT_CHANGE_DATASETS.length == 0) return;

  var pct_change_colour_idx = 0;

  /* Used when displaying regular simulation pct change data. */
  var specific_y_ticks;

  /* Retain ascending order if modifying
     TODO : Should be responsive to max. concentration configuration value */
  var all_x_ticks = [ [-3.0, '0.001'], 
                      [-2.523,'0.003'],
                      [-2.0, '0.01'],
                      [-1.523, '0.03'],
                      [-1.0, '0.1'],
                      [-0.523, '0.3'],
                      [0.0 , '1'],
                      [0.477 , '3'],
                      [1.0 , '10'],
                      [1.477 , '30'],
                      [2.0 , '100'],
                      [2.477 , '300'],
                      [3.0 , '1000'],
                      [3.477 , '3000'],
                      [4.0 , '10000'],
                      [4.477 , '30000'],
                      [5.0 , '100000'] ];
  var fewer_x_ticks = [ [-3.0, '0.001'], 
                        [-2.0, '0.01'],
                        [-1.0, '0.1'],
                        [0.0 , '1'],
                        [1.0 , '10'],
                        [2.0 , '100'],
                        [3.0 , '1000'],
                        [4.0 , '10000'],
                        [5.0 , '100000'],
                        [6.0 , '1e6'],
                        [7.0 , '1e7'],
                        [8.0 , '1e8'],
                        [9.0 , '1e9'],
                        [10.0, '1e10'] ];
  var log_conc_min = cs_round_3dp(3.0); 
  var log_conc_max = cs_round_3dp(-3.0);
  /* If all % change values within +/-20% use an x-axis range of +/-20%.*/
  var set_minimum_y_axis = true;
  var min_pct_change = -20.0;
  var max_pct_change = 20.0;
  jQuery.each(PCT_CHANGE_DATASETS, function(key, val) {
    var existing_data = val.data;
    var confidence_data = val.confidenceData;
     jQuery.each(existing_data, function() {
      var concentration = this[0];
      var pct_change = this[1];

      concentration = cs_round_3dp(concentration * 1.0);
      if (concentration > log_conc_max) log_conc_max = concentration;
      if (concentration < log_conc_min) log_conc_min = concentration;

      /* Until assigned otherwise, assume that data may exceed the min/max % change range */
      if (set_minimum_y_axis) {
        /* If there's confidence data then take that into consideration */
        if (confidence_data) {
          var spreads = pct_change.split(',');
          /* These values may not be numeric, i.e. they may each be NoActionPotential_1! */
          jQuery.each(spreads, function() {
            var each_spread_value = this;
            if (jQuery.isNumeric(each_spread_value)) {
              if (!is_within_minmax(each_spread_value)) {
                set_minimum_y_axis = false;
                return false;
              }
            }
          });
        } else {
          if (jQuery.isNumeric(pct_change)) {
            set_minimum_y_axis = is_within_minmax(pct_change);
          }
        }
      }
    });
  });

  if (set_minimum_y_axis) {
    specific_y_ticks = [ [ min_pct_change, '-20'],
                         [ -15.0, '-15'],
                         [ -10.0, '-10'],
                         [ -5.0, '-5'],
                         [ 0.0, '0'],
                         [ 5.0, '5'],
                         [ 10.0, '10'],
                         [ 15.0, '15'],
                         [ max_pct_change, '20']
                       ];
  }

  /* Use nearest_ to record closest ticks lying outside the results concentrations range */
  var nearest_x_lower = [];
  var nearest_x_upper = [];
  /* If displaying over 10000 then we need to start displaying exponent value concentrations */
  var use_x_ticks = (log_conc_max > 4) ? fewer_x_ticks : all_x_ticks;
  jQuery.each(use_x_ticks, function() {
    var x_tick_log = cs_round_3dp(this[0] * 1.0);
    var x_tick_conc_txt = this[1];
    if (x_tick_log >= log_conc_min && x_tick_log <= log_conc_max) {
      /* If graph axis tick is within the recorded min and max result concs */
      specific_x_ticks.push([x_tick_log, x_tick_conc_txt]);
    }
    if (x_tick_log < log_conc_min) {
      nearest_x_lower = [x_tick_log, x_tick_conc_txt];
    }
    if (nearest_x_upper.length == 0 && x_tick_log > log_conc_max) {
      nearest_x_upper = [x_tick_log, x_tick_conc_txt];
    }
  });
  if (specific_x_ticks.length == 0 && nearest_x_lower.length == 2 && nearest_x_upper.length == 2) {
    /* Looks like the result concs (or selected concs) are within the default graph concs */
    specific_x_ticks.push(nearest_x_lower);
    specific_x_ticks.push(nearest_x_upper);
  }

  var overview_options = {
          legend: { 
                    show: true,
                    container: $('#pct_change_overview_legend_trace')
                   },
          series: {
                    lines: {
                             show: true, lineWidth: 1
                            },
                    shadowSize: 0
                  },
          xaxis: { ticks: 4 },
          yaxis: { ticks: 4},
          grid: { color: '#999' },
          selection: { mode: 'xy' }
  };

  function getDefaultMainPanelOptions() {
    var options = { legend: { show: false },
                    yaxis: { ticks: specific_y_ticks,
                             axisLabel: GRAPH_LABEL_DELTA_APD90,
                             axisLabelUseCanvas: true },
                    xaxis: { ticks: specific_x_ticks,
                             autoscaleMargin: 0.05,
                             axisLabel: GRAPH_LABEL_CONC_UM,
                             axisLabelPadding: 10,
                             axisLabelUseCanvas: true },
                    selection: { mode: 'xy' },
                    grid: { hoverable: true } };

    return $.extend(true, {}, options);
  };

  var main_options = getDefaultMainPanelOptions();

  /* Hard-code color indices to prevent them from shifting as things are turned on/off */
  $.each(PCT_CHANGE_DATASETS, function(key, val) {
    val.color = pct_change_colour_idx;
    ++pct_change_colour_idx;

    if (val.confidenceData) {
      cs_graph_percentiles.pre_process_pctiles(val, PCT_CHANGE_DATASETS);
    }
  });

  /* Plot the original graph now and retain a holder for it. */
  var plot = cs_fplot($('#' + pct_change_placeholder_trace_id), PCT_CHANGE_DATASETS, main_options);
  /* Plot the overview graph and legend and retain a holder for it.*/
  var overview = cs_fplot($('#pct_change_overview_trace'), PCT_CHANGE_DATASETS, overview_options);
  /* Make the plot overview retain the original axes, even when different data is plotted.*/
  overview_options = $.extend(true, {}, overview_options, {
                                                            xaxis: {
                                                                     min: overview.getAxes().xaxis.min,
                                                                     max: overview.getAxes().xaxis.max
                                                                   },
                                                            yaxis: { 
                                                                     min: overview.getAxes().yaxis.min,
                                                                     max: overview.getAxes().yaxis.max
                                                                   }
                                                          });
  /* Insert checkboxes */
  var choice_container = $('#' + pct_change_choices_trace_id);
  choice_container.text('');
  $.each(PCT_CHANGE_DATASETS, function(key, val) {
    var pct_change_div = jQuery('<div />');
    var plot_label = val.label;
    if (plot_label != undefined) {
    } else {
      /* Assume it's confidence data */
      return;
    }
    var plot_colour = val.color;
    var plot_messages = val.messages;

    /* Class legendColorBox defined in jquery.flot.js in function insertLegend() */
    var legendColorBox = $('#pct_change_overview_legend_trace .legendColorBox:eq(' + plot_colour + ') div div');

    /* moz wasn't happy using border-color, which ie didn't mind! */
    var plot_colour = legendColorBox.css('border-left-color');

    var new_span = jQuery('<span />').css('background-color', plot_colour).html('&nbsp;&nbsp;');
    var label = jQuery('<label />').text(plot_label);

    pct_change_div.append('&nbsp;');
    pct_change_div.append(new_span);
    pct_change_div.append('&nbsp;&nbsp;');
    pct_change_div.append(label);

    /* Show message view buttons if there's any to display for plot */
    if (plot_messages) {
      var message_button_name = 'msgbutton_' + key;
      var message_div_id = 'messages_' + key;

      var message_button = jQuery('<button />').attr( { name: message_button_name } )
                                               .text(TEXT_VIEW_MESSAGES);
      var message_div = jQuery('<div />').attr( { id: message_div_id,
                                                  title: plot_label } );
      message_div.append(jQuery('<pre />').text(plot_messages));

      pct_change_div.append('&nbsp;&nbsp;');
      pct_change_div.append(message_button);
      pct_change_div.append(message_div);
    }
    choice_container.append(pct_change_div);
  });

  /* There's no guarantee that these two values will ever be defined. */
  var confidence_checkbox;
  var pctile_span;

  /* Append a checkbox if there's confidence interval data available. */
  if (PCT_CHANGE_DATASETS.length > 1) {
    var pct_change_div = jQuery('<div />').attr( { id: 'pct_change_pctiles' });
    var elements = {};
    cs_graph_pct_change.append_pctile_checkboxes(elements);
    confidence_checkbox = elements.confidence_checkbox;
    pctile_span = elements.pctile_span;

    pct_change_div.append(TEXT_SHOW_CONFIDENCE + '&nbsp;');
    confidence_checkbox.prop('checked', true);
    pct_change_div.append(confidence_checkbox);
    pct_change_div.append('&nbsp;');
    pct_change_div.append(pctile_span);
    pctile_span.show();
    choice_container.append(pct_change_div);
  }

  /* Now connect the two - this method sets the "selection" ranges on both plots, and replots the main plot. */
  $('#' + pct_change_placeholder_trace_id).bind('plotselected', function (event, ranges) {
    /* Clamp the zooming to prevent eternal zoom */
    if (ranges.xaxis.to - ranges.xaxis.from < 0.00001)
      ranges.xaxis.to = ranges.xaxis.from + 0.00001;
    if (ranges.yaxis.to - ranges.yaxis.from < 0.00001)
      ranges.yaxis.to = ranges.yaxis.from + 0.00001;
    main_options = $.extend(true, {}, main_options, {
                                                      xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to },
                                                      yaxis: { min: ranges.yaxis.from, max: ranges.yaxis.to }
                                                    });
    overview.setSelection(ranges, true);
    plotAccordingToChoicesAndZooming();
  });

  /* This method just calls the method on the main plot above. */
  $('#pct_change_overview_trace').bind('plotselected', function (event, ranges) {
    plot.setSelection(ranges);
  });

  /* Gets called for all interactive data plotting. */
  function plotAccordingToChoicesAndZooming() {
    if (confidence_checkbox !== undefined) {
      confidence_checkbox.is(':checked') ? pctile_span.show() : pctile_span.hide();
    }

    cs_graph_pctiles_to_show = [];
    /* Update the %ile percents to show, based on those with the boxes currently checked */
    choice_container.find('input.' + cs_graph_pctile_checkbox_class + ':checked').each(function() {
      cs_graph_pctiles_to_show.push(jQuery(this).val());
    });

    /* 'data' is the section of 'pct_change_data' which will be plotted forthwith. */
    var data = [];
    var show_confidence = (pctile_span !== undefined && pctile_span.is(':visible'));
    jQuery.each(PCT_CHANGE_DATASETS, function(key, val) {
      var plot_label = val.label;
      if (plot_label != undefined) {
        data.push(val);
      } else {
        /* Assume it's confidence data */
        if (show_confidence) {
          if (jQuery.inArray(val.pctile, cs_graph_pctiles_to_show) != -1) {
            data.push(val);
          }
        }
      }
    });

    /* When looking at qNet this may be called on show/hide confidence data events */
    plot = cs_fplot($('#' + pct_change_placeholder_trace_id), data, main_options);

    ranges = { xaxis: { from: plot.getAxes().xaxis.min, to: plot.getAxes().xaxis.max},
               yaxis: { from: plot.getAxes().yaxis.min, to: plot.getAxes().yaxis.max} };
    overview = $.plot($('#pct_change_overview_trace'), data, overview_options);
    overview.setSelection(ranges, true);
  }

  /* Show/Hide confidence data? Checkboxes */
  choice_container.find('input:checkbox').click(function() {
    /* Unlike the 'Reset Graph' button, the show/hide of confidence
       data needs to be reflected in both Δ APD90 and qNet charts */
    plotAccordingToChoicesAndZooming();
    if (Object.size(QNET_DATASETS) != 0) {
      qnet_display_cd.qnet_plot_according_to_choices_and_zooming();
    }
  });

  /* Reset Graph button */
  $('#pct_change_clear_selection_trace').click(function() {
    switch (current_viewing) {
      case viewing.DELTAAPD90 :
        /* Let the main plot choose its own axes again */
        main_options = getDefaultMainPanelOptions();

        plotAccordingToChoicesAndZooming();
        overview.clearSelection();

        break;
      case viewing.QNET :
        qnet_display_cd.qnet_main_options = qnet_display_cd.qnet_get_default_main_panel_options();
        qnet_display_cd.qnet_plot_according_to_choices_and_zooming();

        break;
      default :
        console.log('ERROR : % Change reset button pressed when not viewing either Δ APD90 or qNet!')
        break;
    }
  });

  var previous_point = null;
  var fixed_decimal_place = 3;

  $('#' + pct_change_placeholder_trace_id).bind('plothover',
                                                function(event, pos, item) {
    previous_point = cs_process_plothover({ plot: plot,
                                            pos: pos,
                                            item: item,
                                            previous_point: previous_point,
                                            fixed_decimal_place: fixed_decimal_place,
                                            inversing: true,
                                            x: { element: $('#point_val_x'),
                                                 label: TEXT_CONC,
                                                 units: '&micro;M' },
                                            y: { element: $('#point_val_y'),
                                                 label: GRAPH_LABEL_DELTA_APD90,
                                                 units: '%' } });
  });

  /* leave the hiding until last (otherwise flot may complain */
  $('#pct_change_overview_trace').hide();
  $('.graph_pct_change_overview_legend_trace').hide();
}