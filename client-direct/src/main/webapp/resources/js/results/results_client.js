/**
 * ActionPotential per-concentration label.
 * 
 * @param concentration Compound concentration value.
 * @param job_title Job title.
 * @return Control label if concentration is 0 (zero), otherwise fancier label.
 */
function create_ap_label(concentration, job_title) {
  if (concentration == 0) {
    return CONTROL_LABEL;
  } else {
    return job_title + DELIMITER + concentration + MICRO_TEXT;
  }
}

/**
 * Process the AJAX response to a results request.
 * 
 * @param simulation_id Simulation identifier.
 * @param response AJAX response.
 */
function process_results_response(simulation_id, response) {
  var job_map = response[KEY_RESULTS_RESULTS];

  if (job_map != undefined) {
    reset_graph_datasets();

    var job_id = job_map[KEY_RESULTS_JOB_ID];
    var job_title = retrieve_job_title(job_id);

    var input_option = job_map[KEY_RESULTS_INPUT_OPTION];

    var delta_apd90_datasets = [];
    var pk_datasets = [];
    var qnet_datasets = [];

    if (job_title != undefined) {
      var messages = job_map[KEY_RESULTS_MESSAGES];
      var delta_apd90_pctile_names = job_map[KEY_RESULTS_DELTA_APD90_PERCENTILE_NAMES];

      var per_conc_data = job_map[KEY_RESULTS_REFERENCE_DATA];
      var pk_data = job_map[KEY_PKRESULTS_TIMEPOINT_DATA];

      var datasets = prepare_pct_change_and_qnet_datasets(job_title, job_id, messages, per_conc_data,
                                                          delta_apd90_pctile_names);
      delta_apd90_datasets = datasets['delta_apd90'];
      qnet_datasets = datasets['qnet'];

      if (pk_data != undefined) {
        prepare_pk_datasets(pk_data);
        jQuery.each(pk_datasets, function(index, pk_dataset) {
          PLOT_PK_DATASETS.push(pk_dataset);
        });
      }
    }

    delta_apd90_datasets = cs_sort_array_on_group_level_and_pacing_frequency(delta_apd90_datasets);
    qnet_datasets = cs_sort_array_on_group_level_and_pacing_frequency(qnet_datasets);

    jQuery.each(delta_apd90_datasets, function(index, delta_apd90_dataset) {
      PCT_CHANGE_DATASETS.push(delta_apd90_dataset);
    });
    jQuery.each(qnet_datasets, function(index, qnet_dataset) {
      QNET_DATASETS.push(qnet_dataset);
    });

    show_pct_change_graph(input_option);
    qnet_display_cd.show_qnet_graph('client-direct');

    if (typeof show_plot_PK_graph === 'function') {
      show_plot_PK_graph();
    }
  }
}

/**
 * Prepare the PKPD datasets for presentation.
 * 
 * @param pk_data PKPD data to present.
 */
function prepare_pk_datasets(pk_data) {
  var timepoints = [];
  for (var timepoint in pk_data) {
    timepoints.push( [ timepoint * 1, timepoint ] );
  }
  timepoints.sort(function(a, b) { return a[0] - b[0]; });

  PLOT_PK_DATASETS = [];

  jQuery.each(timepoints, function(i, timepoint) {
    var timepointNum = timepoint[0];
    var timepointStr = timepoint[1];

    var per_timepoint_data = pk_data[timepointStr];
    var apd90s = per_timepoint_data[KEY_PKRESULTS_PERTIMEPOINT_APD90S].split(',');
    if (PLOT_PK_DATASETS.length == 0) {
      var apd90_count = apd90s.length;
      PLOT_PK_DATASETS = [apd90_count];
      for (var idx=0; idx < apd90_count; idx++) {
        PLOT_PK_DATASETS[idx] = { 'label' : 'col ' + (idx+1),
                                  'data' : [] };
      }
    }
    var for_trans = per_timepoint_data[KEY_PKRESULTS_PERTIMEPOINT_FORTRANS];

    jQuery.each(apd90s, function(idx, apd90) {
      PLOT_PK_DATASETS[idx].data.push( [ timepointNum, +apd90 ] );
    });
  });
}

/**
 * Prepare the Δ APD90 and, if available, the qNet datasets.
 * 
 * @param job_title Job title.
 * @param job_id Job identifier.
 * @param messages Messages.
 * @param per_conc_data Per-concentration data.
 * @param delta_apd90_pctile_names Δ APD90 %ile names (or <code>null</code> if not defined).
 * @returns Prepared datasets for plotting.
 */
function prepare_pct_change_and_qnet_datasets(job_title, job_id, messages, per_conc_data,
                                              delta_apd90_pctile_names) {
  /* Assume data doesn't always arrive sorted by concentration */
  var concentrations = [];
  for (var concentration in per_conc_data) {
    /* Using an array to enable sorting on numerical (including scientific notation), whilst
       retaining the original concentration value for retrieving from the per_conc_data object
       (by string representation) */
    concentrations.push( [ concentration * 1, concentration ] );
  }
  /* Sort concentration data (e.g. compound concentration) into increasing values */
  concentrations.sort(function(a,b) { return a[0] - b[0]; });

  var delta_apd90_plotdata = [];
  var qnet_plotdata = [];
  var message_detail;

  var has_confidence_intervals = false;
  
  var MIN_CONC_TO_USE = 0.000000000000001;

  /* Traverse the per-concentration data. */
  jQuery.each(concentrations, function(i, concentration) {
    var concentration_num = concentration[0];
    var concentration_str = concentration[1];
    var each_conc_data = per_conc_data[concentration_str];
    var delta_apd90 = each_conc_data[KEY_RESULTS_PERREF_DELTA_APD90];
    var qnet = each_conc_data[KEY_RESULTS_PERREF_QNET];
    var split_times = each_conc_data[KEY_RESULTS_PERREF_TIMES].split(',');
    var split_voltages = each_conc_data[KEY_RESULTS_PERREF_VOLTAGES].split(',');

    /* Action Potential */
    var ap_plotdata = [];
    jQuery.each(split_times, function(index, time) {
      time = +time;
      ap_plotdata.push( [ time, split_voltages[index] ] );
    });
    AP_DATASETS.push({
      'label': create_ap_label(concentration_str, job_title),
      'data': ap_plotdata
    });

    /* (Delta)APD90 : Some apd90 values aren't numeric (in which case it's a message of some sort) */
    if (jQuery.isNumeric(delta_apd90)) {
      /* Max. no of decimals in javascript is 17! */
      if (concentration_num >= MIN_CONC_TO_USE) {
        /* Pushing a single (i.e. non-spread) value to the dataset */
        delta_apd90_plotdata.push( [ cs_log_10(concentration_num), delta_apd90 ] );
      }
    } else {
      /* Value may NoActionPotential_1 or spread data (perhaps including NoActionPotential_1!) */
      var messages_found = false;
      if (delta_apd90.indexOf(',')) {
        /* Looks like we could have confidence interval spreads. */
        var potential_spread_values = delta_apd90.split(',');
        jQuery.each(potential_spread_values, function(index, potential_spread_value) {
          if (!jQuery.isNumeric(potential_spread_value)) {
            messages_found = true;
            return;
          }
        });
        if (!messages_found) {
          has_confidence_intervals = true;
          if (concentration_num >= MIN_CONC_TO_USE) {
            /* Pushing the CSV of spread values to the dataset */
            delta_apd90_plotdata.push( [ cs_log_10(concentration_num), delta_apd90 ] );
          }
        }
      }

      if (messages_found) {
        /*
         * As of June 2020 this additional detail operation is now only relevant for simulation
         * results obtained prior to June 2020. Since June 2020 all messages now appear in the
         * messages.txt file and therefore no additional detail is required.
         * For databases holding simulation results prior to June 2020 however, there may still be
         * legacy values therein in JobResult#deltaAPD90 which may result in the following in
         * concentration_str :
         * 
         * (*) cs_appredict.no_action_potential_1            <-- legacy value
         * (*) NoActionPotential_1                           <-- new value
         * (*) NoActionPotential_1,NoActionPotential_1,NoActionPotential_1,NoActionPotential_1,NoActionPotential_1,NoActionPotential_1,NoActionPotential_1,NoActionPotential_1,NoActionPotential_1
         * 
         * The first of these values will be translated using the corresponding language bundle
         * property, the second and third would look a bit clumsy as there was no translation, so
         * they're no longer displayed.
         */
        if (delta_apd90.indexOf('NoActionPotential_') == -1) {
          var additional_detail = '\n + ' + TEXT_AT_A_CONCENTRATION_OF + ' ' + concentration_str + 'µM: ' + delta_apd90;

          if (message_detail != undefined) {
            message_detail += additional_detail;
          } else {
            message_detail = additional_detail;
          }
        }
      }
    }

    /* qNet (if present) */
    if (typeof qnet !== 'undefined' && qnet != null) {
      if (concentration_num >= MIN_CONC_TO_USE) {
        var use_qnet;
        if (qnet.indexOf(',') == -1) {

          use_qnet = retrieve_qnet_value(qnet, concentration_num)

        } else {
          var qnet_spreads = qnet.split(',');

          var use_qnet_spreads = [];
          jQuery.each(qnet_spreads, function() {
            var qnet_spread = this;
            use_qnet_spreads.push(retrieve_qnet_value(qnet_spread, concentration_num));
          });

          use_qnet = use_qnet_spreads.join(',');
        }

        qnet_plotdata.push( [ cs_log_10(concentration_num), use_qnet ] );
      }
    }
  });

  if (message_detail != undefined) {
    messages += '\n' + message_detail;
  }

  /* Group level and pacing frequency are used for ordering the datasets */
  var delta_apd90_datasets = [];
  delta_apd90_datasets.push({
    'label': job_title,
    'groupLevel': cs_retrieve_group_level(job_id),
    'pacingFrequency': cs_retrieve_pacing_frequency(job_id),
    'messages': messages,
    'data': delta_apd90_plotdata,
    'lines': { 'show': true },
    'points': { 'show': true },
    'dashes': { 'show': false },
    'shadowSize': 1,
    'confidenceData': has_confidence_intervals,
    'deltaAPD90PctileNames': delta_apd90_pctile_names
  });
  var qnet_datasets = [];
  if (qnet_plotdata.length > 0) {
    qnet_datasets.push({
      'label': job_title,
      'groupLevel': cs_retrieve_group_level(job_id),
      'pacingFrequency': cs_retrieve_pacing_frequency(job_id),
      'messages': messages,
      'data': qnet_plotdata,
      'lines': { 'show': true },
      'points': { 'show': true },
      'dashes': { 'show': false },
      'shadowSize': 1,
      'confidenceData': has_confidence_intervals,
      'deltaAPD90PctileNames': delta_apd90_pctile_names
    });
  }

  return {
    'delta_apd90': delta_apd90_datasets,
    'qnet': qnet_datasets
  };
}

/**
 * Retrieve the qNet value to use.
 * 
 * Gary's words : 11th June 2020 :
 * "-inf seems to pop out when interpolating with a lookup table that contains -1.79769e+308,
 *  I would treat it just the same as -1.79769e+308 !
 *  If you see any real sensible negative numbers that are large, they could theoretically
 *  happen when you are interpolating on the border region between sensible answers and
 *  -1.79769e+308 (that's probably how -1.64517e+308 arose), I would say anything < -1 you
 *  can probably plot as "off the bottom" same as -1.79769e+308 . These cases tend to arise
 *  in NoActionPotential sort of scenarios anyway.
 *  
 *  @param qnet qNet value.
 *  @param conc Compound concentration.
 *  @return qNet value to use.
 */
function retrieve_qnet_value(qnet, conc) {
  var use_qnet;

  if (jQuery.isNumeric(qnet)) {
    /* qNet value was numeric */
    qnet*=1;
    if (qnet < -1) {
      use_qnet = APPREDICT_NEGATIVE_DOUBLE_MAX;
    } else {
      use_qnet = qnet;
    }
  } else {
    /* qNet value wasn't numeric */
    if (qnet == '-inf' || qnet == 'nan' || qnet.contains('NoActionPotential')) {
    } else {
      /* qNet value wasn't anticipated */
      console.log('WARN : [' + conc + '] : Unanticipated value ' + qnet + ' encountered');
    }
    use_qnet = APPREDICT_NEGATIVE_DOUBLE_MAX;
  }

  return use_qnet;
}

/**
 * Show the x, y % change values as a tooltip.
 * 
 * @param x X-coordinate.
 * @param y Y-coordinate.
 * @param content Text to show in the tooltip.
 */
function show_tooltip(x, y, content) {
  jQuery('<div />').attr({ 'id' : 'tooltip' })
                   .css({ 'top': y + 5, 'left': x + 5 })
                   .addClass('graph_plot_tooltip')
                   .html(content)
                   .appendTo('body')
                   .fadeIn(200);
}

var assays;
var assay_groups;