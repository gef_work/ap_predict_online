/*
 * qNet display for client-direct.
 */
var qnet_display_cd = Object.create(qnet_display);
/* specific_x_ticks defined in graph_pct_change.js */
qnet_display_cd.qnet_x_ticks = specific_x_ticks;

qnet_display_cd.qnet_plot_according_to_choices_and_zooming = function() {
  var self = this;

  var data = [];
  var pct_change_confidence_checkbox = jQuery('#pct_change_confidence_checkbox');
  if (pct_change_confidence_checkbox.is(':visible')) {
    if (pct_change_confidence_checkbox.is(':checked')) {
      /* Display the selected %iles and median */
      $.each(self.qnet_data, function(key, val) {
        /* val.label available if it's a regular assay/freq simulation */
        var data_label = val.label;
        /* val.id id available if the data is a percentile */
        var data_id = val.id;
        if (typeof data_label !== 'undefined') {
          data.push(val);
        } else {
          /* See graph_pct_change.js#plotAccordingToChoicesAndZooming */
          if (jQuery.inArray(val.pctile, cs_graph_pctiles_to_show) != -1) {
            data.push(val);
          }
        }
      });
    } else {
      /* Display the median only */
      $.each(self.qnet_data, function(key, val) {
        /* val.label available if it's a regular assay/freq simulation */
        var data_label = val.label;
        if (typeof data_label !== 'undefined') {
          data.push(val);
        }
      });
    }
  } else {
    /* Not showing confidence data */ 
    data = self.qnet_data;
  }

  if (data.length > 0) {
    self.qnet_plot = cs_fplot($('#' + self.qnet_placeholder_trace_id), 
                              data, self.qnet_main_options);
  }
}