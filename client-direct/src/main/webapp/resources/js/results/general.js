/**
 * Turn a checkbox off
 * 
 * @param obj Checkbox object to switch off.
 */
function checked_off(obj) {
  obj.removeAttr('checked');
}

/**
 * Turn a checkbox on.
 * 
 * @param obj Checkbox object to switch on.
 */
function checked_on(obj) {
  obj.attr('checked', 'checked');
}

/**
 * Reset the following datasets used in graph display :
 * <ul>
 *   <li>AP_DATASETS</li>
 *   <li>PCT_CHANGE_DATASETS</li>
 *   <li>PLOT_PK_DATASETS</li>
 *   <li>QNET_DATASETS</li>
 * </ul>
 */
function reset_graph_datasets() {
  AP_DATASETS = [];
  PCT_CHANGE_DATASETS = [];
  PLOT_PK_DATASETS = [];
  QNET_DATASETS = [];
}

/**
 * Retrieve a job title for the job.
 * 
 * @param job_id Job identifier.
 * @return Job title, e.g. <code>QSAR@0.5 (Hz)</code>
 */
function retrieve_job_title(job_id) {
  var key = cs_id_as_key(job_id);
  var job_detail = JOB_DETAILS[key];
  if (job_detail != undefined) {
    var group_name = job_detail[KEY_SHARED_GROUP_NAME];
    var pacing_frequency = cs_retrieve_pacing_frequency(job_id);

    return group_name + DELIMITER + pacing_frequency + HZ_TEXT;
  }
}