      var progress_call_failure_count = 0;
      var js_error_progress;
      jQuery(document).ready(
        function() {
          js_error_progress = jQuery('#js_error_progress');
        }
      );

      /**
       * AJAX call to retrieve simulation progress.
       * 
       * @param simulation_id Simulation identifier.
       */
      function ajax_for_latest_progress(simulation_id) {
        var latest_progress_url = CONTEXT_PATH + URL_PREFIX_LATEST_PROGRESS + simulation_id;
        jQuery.ajax({
          url: latest_progress_url,
          async: true,
          type: 'GET',
          data: {},
          dataType: 'json',
          timeout: 6000,
          contentType: 'application/json; charset=utf-8',
          success: function(response) {
                     var json = cs_retrieve_json(response, MODEL_ATTRIBUTE_LATEST_PROGRESS, 'progress',
                                                 js_error_progress);
                     if (json != undefined) {
                       /* Reset the js error element and count (if it's visible) */
                       if (js_error_progress.is(':visible')) {
                         cs_hide_js_error(js_error_progress);
                         progress_call_failure_count = 0;
                       }
                       progress = json.replace(TEXT_PROG, '');
                       if (SIMULATION_FINISHED_STRING != progress) {
                         jQuery('#progress_' + simulation_id).html(progress);
                         setTimeout(function() { ajax_for_latest_progress(simulation_id); }, 5000);
                       } else {
                         var submit_button_id = '#submit_' + simulation_id;
                         if (!jQuery(submit_button_id).is(':visible')) {
                           jQuery(submit_button_id).show();
                         }
                         jQuery('#progress_' + simulation_id).html(TEXT_COMPLETE);
                       }
                     }
                   },
          error: function(xhr, status, error) {
                   cs_handle_error(error, 'progress', js_error_progress);
                   /* Try a few times to reconnect but give up eventually! */
                   if (progress_call_failure_count++ < 5) {
                     setTimeout(function() { ajax_for_latest_progress(simulation_id); }, 6000);
                   }
                 }
        });
      }

      jQuery(document).ready(
        function() {
          jQuery('.border_lr').css({ 'border-left': 'solid #bbf 2px',
                                     'border-right': 'solid #bbf 2px' });
          jQuery('.border_l').css({ 'border-left': 'solid #bbf 2px' });
          jQuery('.border_r').css({ 'border-right': 'solid #bbf 2px' });
          jQuery('.border_llg').css({ 'border-left': 'solid #ddd 1px' });

          /* When someone selects the 'view' radio submit the form */
          jQuery('input[name=view_simulation]').click(function(event) {
            var closest_form = jQuery(this).closest('form');
            closest_form.submit();
            event.preventDefault();
          });

          /* When someone selects the 'delete' radio submit the form */
          jQuery('input[name=delete_simulation]').click(function(event) {
            var closest_form = jQuery(this).closest('form');
            closest_form.submit();
            event.preventDefault();
          });

          /* When page is shown the progress spans of unfinished simulations are shown */
          jQuery('span[id^=progress_]').each(function() {
            var element_id = jQuery(this).attr('id');
            var simulation_id = element_id.replace('progress_', '');
            ajax_for_latest_progress(simulation_id);
          });
        }
      );