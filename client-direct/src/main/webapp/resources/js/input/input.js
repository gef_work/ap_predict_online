/**
 * Display the IC50 units according to whatever IC50 (<b>not pIC50</b>) is currently selected.
 * 
 * @param el_select_IC50_units C50 selection element.
 * @param els_C50_units C50 unit elements.
 */
function display_IC50_unit(el_select_IC50_units, els_C50_units) {
  els_C50_units.text(el_select_IC50_units.find(':selected').text());
}

/**
 * Indicate if the currently selected C50 type is IC50 or pIC50.
 * 
 * @param el_select_C50_type Element displaying C50 type.
 * @param c50_type_IC50 IC50 C50 type.
 * @returns <code>true</code> if IC50, otherwise {@code false}.
 */
function is_IC50_type_selected(el_select_C50_type, c50_type_IC50) {
  return (el_select_C50_type.find(':selected').text() == c50_type_IC50);
}

/**
 * Set the default configured (p)IC50 units.
 * 
 * @param c50_units Collection of configured options.
 * @param c50_type_pIC50 pIC50 type, i.e. pIC50.
 * @param c50_type_IC50 IC50 type, i.e. IC50.
 * @param unit_pIC50 pIC50 unit, i.e. -log(M).
 * @param unit_IC50_microM IC50 µM unit.
 * @param unit_IC50_M IC50 M unit.
 * @param unit_IC50_nanoM IC50 nM unit.
 * @param el_select_C50_type Element displaying C50 type.
 * @param el_select_IC50_units Element displaying IC50 unit.
 * @param els_C50_units Elements showing C50 (IC50 or pIC50) units.
 * @param el_IC50_span Span encompassing the unit select element.
 */
function set_c50_units(c50_units, c50_type_pIC50, c50_type_IC50, unit_pIC50, unit_IC50_microM,
                       unit_IC50_M, unit_IC50_nanoM, el_select_C50_type, el_select_IC50_units,
                       els_C50_units, el_IC50_span) {

  var c50_units_configured = false;
  /* Traverse the collection of c50units as defined (ordered) in appCtx.config.site.xml and as soon
     as a valid key is found, use it as the default */
  jQuery.each(c50_units, function(ic50_unit_key, ic50_unit_value) {
    var c50_type = '';
    var ic50_unit = '';
    /* Testing is against hardcoded string values in appCtx.config.site.xml */
    if (ic50_unit_key == 'unit_c50_pIC50') {
      c50_type = c50_type_pIC50;
      /* Note that here we're assigning µM as the default IC50 unit to display if moving away from
         the default pIC50 display, i.e. we're hard coding the default IC50 unit to display if the
         first choice is pIC50. TODO : Observe configured second choice! */
      ic50_unit = unit_IC50_microM;
      el_IC50_span.hide();
      els_C50_units.text(unit_pIC50);
    } else if (ic50_unit_key == 'unit_c50_IC50_uM') {
      c50_type = c50_type_IC50;
      ic50_unit = unit_IC50_microM;
      el_IC50_span.show();
      els_C50_units.text(ic50_unit);
    } else if (ic50_unit_key == 'unit_c50_IC50_nM') {
      c50_type = c50_type_IC50;
      ic50_unit = unit_IC50_nanoM;
      el_IC50_span.show();
      els_C50_units.text(ic50_unit);
    } else if (ic50_unit_key == 'unit_c50_IC50_M') {
      c50_type = c50_type_IC50;
      ic50_unit = unit_IC50_M;
      el_IC50_span.show();
      els_C50_units.text(ic50_unit);
    }

    if (c50_type != '' && ic50_unit != '') {
      /* As soon as a valid key found, assignment is complete! */
      el_select_C50_type.find('option[value=' + c50_type + ']').attr('selected', 'selected');
      el_select_IC50_units.find('option[value=' + ic50_unit + ']').attr('selected', 'selected');
      c50_units_configured = true;

      return false;
    }
  });

  if (!c50_units_configured) {
    cs_show_message('Default inhibitory concentration and units not configured. Using pIC50.');
    els_C50_units.text(unit_pIC50);
    el_select_C50_type.find('option[value=' + c50_type_pIC50 + ']').attr('selected', 'selected');
    el_select_IC50_units.find('option[value=' + unit_IC50_microM + ']').attr('selected', 'selected');
    el_IC50_span.hide();
  }
}

var input_pk_el;
var input_conc_points_el;
var input_conc_range_el;

/**
 * Disable the INaL-related fields if model not conducive to input.
 * 
 * @param model_id Model identifier.
 */
function show_inal(model_id) {
  jQuery('.inal').prop('disabled', jQuery.inArray(model_id + '', allow_inal) == -1);
}

/**
 * Display the PKPD file upload option.
 */
function show_pk() {
  input_conc_points_el.hide();
  input_conc_range_el.hide();
  input_pk_el.show();
}

/**
 * Display the input concentration points options.
 */
function show_conc_points() {
  input_pk_el.hide();
  input_conc_range_el.hide();
  input_conc_points_el.show();
}

/**
 * Display the input concentration range option.
 */
function show_conc_range() {
  input_conc_points_el.hide();
  input_pk_el.hide();
  input_conc_range_el.show();
}

jQuery(document).ready(
  function() {
    input_pk_el = jQuery('tr#input_pk');
    input_conc_points_el = jQuery('tr#input_conc_points');
    input_conc_range_el = jQuery('tr#input_conc_range');

    jQuery('input[type=radio][name=' + PARAM_NAME_PK_OR_CONCS + ']').on('change', function() {
      switch(jQuery(this).val()) {
        case OPTION_PK :
          show_pk();
          break;
        case OPTION_CONC_POINTS :
          show_conc_points();
          break;
        case OPTION_CONC_RANGE :
          show_conc_range();
          break;
      }
    });
  }
);