/**
 * 
 */
function show_hide_dynamic_upload(upload_option_selected) {
  var upload_element = jQuery('.upload_cellml');
  if (upload_option_selected) {
    upload_element.show();
  } else {
    upload_element.hide();
  }
}