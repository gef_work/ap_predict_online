<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers,
                uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers" %>
<sec:authorize access="hasAnyRole('ROLE_USER','ROLE_POWER_USER')">
<div class="shadowed" style="padding: 5px;">
  <form action="<%=ClientDirectIdentifiers.ACTION_SIMULATION_NEW%>"
        class="cs_inline"
        method="post">
    <input type="hidden"
           name="${_csrf.parameterName}"
           value="${_csrf.token}"/>
    <input type="submit"
           value="<spring:message code="general.new_simulation" />" />
  </form>

<c:if test="${not empty ma_simulation_id}">
  <form action="<%=ClientDirectIdentifiers.ACTION_SIMULATION_NEW%>"
        class="cs_inline"
        method="post">
    <input type="hidden"
           name="${_csrf.parameterName}"
           value="${_csrf.token}"/>
    <input type="hidden"
           name="<%=ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID%>"
           value="${ma_simulation_id}" />
    <input type="submit"
           value="<spring:message code="general.template_simulation" />" />
  </form>
</c:if>
</div>
</sec:authorize>
