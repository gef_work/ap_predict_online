<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers,
                uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers,
                uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO,
                uk.ac.ox.cs.nc3rs.client_direct.value.IC50_UNIT,
                uk.ac.ox.cs.nc3rs.client_direct.value.INPUT_UNIT" %>
<c:set var="hill_coefficient"><spring:message code="general.hill_coefficient" /></c:set>
<c:set var="saturation_level"><spring:message code="general.saturation_level" /></c:set>
<c:set var="spread"><spring:message code="general.spread" /></c:set>
  <table id="simulations"
         class="display">
    <thead>
      <tr>
        <th rowspan="2" class="border_l"><spring:message code="general.notes" /></th>
        <th rowspan="2"><spring:message code="general.date" /></th>
        <th rowspan="2" class="border_l"><spring:message code="general.model" /></th>
        <th colspan="2" class="ui-state-default border_lr">
          <spring:message code="general.pacing" />
        </th>
        <th colspan="8" class="ui-state-default border_lr">
          <spring:message code="general.ion_channel_concentrations" />
        </th>
        <th rowspan="2" class="border_r">&nbsp;</th>
        <th rowspan="2" class="ui-state-default border_lr">
          <spring:message code="general.compound_concentrations" />
        </th>
        <th rowspan="2" class="border_r"><spring:message code="general.progress" /></th>
        <th colspan="2" class="ui-state-default border_lr">
          <spring:message code="general.actions" />
        </th>
      </tr>
      <tr>
        <th class="border_l"><spring:message code="general.freq" /> (<%= INPUT_UNIT.Hz %>)</th>
        <th class="border_r"><spring:message code="general.max_time" /> (<%= INPUT_UNIT.mins %>)</th>
        <th class="border_l">IKr</th>
        <th>INa</th>
        <th>ICaL</th>
        <th>IKs</th>
        <th>IK1</th>
        <th>Ito</th>
        <th>INaL</th>
        <th class="border_r"><spring:message code="general.units" /></th>
        <th><spring:message code="general.view" /></th>
        <th class="border_r"><spring:message code="general.delete" /></th>
      </tr>
    </thead>
    <tbody>
<c:forEach items="${ma_simulations}"
           var="simulation">
  <c:set var="simulation_completed" value="${simulation.completed}" />
  <c:set var="simulation_taking_too_long" value="${simulation.takingTooLongToComplete}" />
  <c:set var="delete_submit_value"><spring:message code="general.delete" htmlEscape="false" /><c:if test="${simulation_taking_too_long}"> *</c:if></c:set>
  <c:set var="simulation_id" value="${simulation.id}" />
  <c:choose>
    <c:when test="${not empty simulation.simulationInput.cellMLFileName}">
      <c:set var="model_name" value="${simulation.simulationInput.cellMLFileName}" />
    </c:when>
    <c:otherwise>
      <c:set var="model_name" value="${ma_model_names[simulation.simulationInput.modelIdentifier]}" />
    </c:otherwise>
  </c:choose>
      <tr>
        <td class="border_l">
  <c:if test="${not empty simulation.simulationInput.notes}">
    <c:choose>
      <c:when test="${fn:length(simulation.simulationInput.notes) > 30}">
<pre class="inline_block"><c:out value="${fn:substring(simulation.simulationInput.notes, 0, 30)}" /></pre>
          <span class="cs_inline">
            ... (<a id="hover_for_notes_<c:out value="${simulation_id}" />"
                    title="" href="#" onclick="return false;"><spring:message code="general.more" /></a>)
          </span>
          <pre id="expanded_<c:out value="${simulation_id}" />" style="display: none;">
<c:out value="${simulation.simulationInput.notes}" /></pre>
      </c:when>
      <c:otherwise>
<pre><c:out value="${simulation.simulationInput.notes}" /></pre>
      </c:otherwise>
    </c:choose>
  </c:if>
        </td>
        <td><fmt:formatDate value="${simulation.persisted}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
        <td class="border_l"><c:out value="${model_name}" /></td>
        <td class="border_l"><c:out value="${simulation.simulationInput.pacingFrequency}" /></td>
        <td class="border_r"><c:out value="${simulation.simulationInput.pacingMaxTime}" /></td>
  <c:choose>
    <c:when test="${simulation.simulationInput.retrieveNonC50IKr()}">
        <td class="border_l effects_background"
            title="<spring:message code="main.non_ic_effects"
                                   arguments="${hill_coefficient},${simulation.simulationInput.hillIkr},
                                              ${saturation_level},${simulation.simulationInput.saturationIkr},
                                              ${spread},${simulation.simulationInput.spreadIkr}" />">
          <c:out value="${simulation.simulationInput.c50Ikr}" />
        </td>
    </c:when>
    <c:otherwise>
        <td class="border_l"><c:out value="${simulation.simulationInput.c50Ikr}" /></td>
    </c:otherwise>
  </c:choose>
  <c:choose>
    <c:when test="${simulation.simulationInput.retrieveNonC50INa()}">
        <td class="border_llg effects_background"
            title="<spring:message code="main.non_ic_effects"
                                   arguments="${hill_coefficient},${simulation.simulationInput.hillINa},
                                              ${saturation_level},${simulation.simulationInput.saturationINa},
                                              ${spread},${simulation.simulationInput.spreadINa}" />">
          <c:out value="${simulation.simulationInput.c50INa}" />
        </td>
    </c:when>
    <c:otherwise>
        <td class="border_llg"><c:out value="${simulation.simulationInput.c50INa}" /></td>
    </c:otherwise>
  </c:choose>
  <c:choose>
    <c:when test="${simulation.simulationInput.retrieveNonC50ICaL()}">
        <td class="border_llg effects_background"
            title="<spring:message code="main.non_ic_effects"
                                   arguments="${hill_coefficient},${simulation.simulationInput.hillICaL},
                                              ${saturation_level},${simulation.simulationInput.saturationICaL},
                                              ${spread},${simulation.simulationInput.spreadICaL}" />">
          <c:out value="${simulation.simulationInput.c50ICaL}" />
        </td>
    </c:when>
    <c:otherwise>
        <td class="border_llg"><c:out value="${simulation.simulationInput.c50ICaL}" /></td>
    </c:otherwise>
  </c:choose>
  <c:choose>
    <c:when test="${simulation.simulationInput.retrieveNonC50IKs()}">
        <td class="border_llg effects_background"
            title="<spring:message code="main.non_ic_effects"
                                   arguments="${hill_coefficient},${simulation.simulationInput.hillIks},
                                              ${saturation_level},${simulation.simulationInput.saturationIks},
                                              ${spread},${simulation.simulationInput.spreadIks}" />">
          <c:out value="${simulation.simulationInput.c50Iks}" />
        </td>
    </c:when>
    <c:otherwise>
        <td class="border_llg"><c:out value="${simulation.simulationInput.c50Iks}" /></td>
    </c:otherwise>
  </c:choose>
  <c:choose>
    <c:when test="${simulation.simulationInput.retrieveNonC50IK1()}">
        <td class="border_llg effects_background"
            title="<spring:message code="main.non_ic_effects"
                                   arguments="${hill_coefficient},${simulation.simulationInput.hillIk1},
                                              ${saturation_level},${simulation.simulationInput.saturationIk1},
                                              ${spread},${simulation.simulationInput.spreadIk1}" />">
          <c:out value="${simulation.simulationInput.c50Ik1}" />
        </td>
    </c:when>
    <c:otherwise>
        <td class="border_llg"><c:out value="${simulation.simulationInput.c50Ik1}" /></td>
    </c:otherwise>
  </c:choose>
  <c:choose>
    <c:when test="${simulation.simulationInput.retrieveNonC50Ito()}">
        <td class="border_llg effects_background"
            title="<spring:message code="main.non_ic_effects"
                                   arguments="${hill_coefficient},${simulation.simulationInput.hillIto},
                                              ${saturation_level},${simulation.simulationInput.saturationIto},
                                              ${spread},${simulation.simulationInput.spreadIto}" />">
          <c:out value="${simulation.simulationInput.c50Ito}" />
        </td>
    </c:when>
    <c:otherwise>
        <td class="border_llg"><c:out value="${simulation.simulationInput.c50Ito}" /></td>
    </c:otherwise>
  </c:choose>
  <c:choose>
    <c:when test="${simulation.simulationInput.retrieveNonC50INaL()}">
        <td class="border_llg effects_background"
            title="<spring:message code="main.non_ic_effects"
                                   arguments="${hill_coefficient},${simulation.simulationInput.hillINaL},
                                              ${saturation_level},${simulation.simulationInput.saturationINaL},
                                              ${spread},${simulation.simulationInput.spreadINaL}" />">
          <c:out value="${simulation.simulationInput.c50INaL}" />
        </td>
    </c:when>
    <c:otherwise>
        <td class="border_llg"><c:out value="${simulation.simulationInput.c50INaL}" /></td>
    </c:otherwise>
  </c:choose>
        <td class="border_r border_llg"><c:out value="${simulation.simulationInput.c50Type}" />
  <c:choose>
    <c:when test="${not empty simulation.simulationInput.ic50Units}">
        (<c:out value="${simulation.simulationInput.ic50Units}" />)
    </c:when>
    <c:otherwise>
        (<%= SimulationInputVO.UNIT_MINUS_LOG_M %>)
    </c:otherwise>
  </c:choose>
        </td>
  <c:choose>
    <c:when test="${not empty simulation.simulationInput.plasmaConcMax}">
        <td><spring:message code="general.range" /></td>
    </c:when>
    <c:when test="${not empty simulation.simulationInput.plasmaConcPoints}">
        <td><spring:message code="general.points" /></td>
    </c:when>
    <c:when test="${not empty simulation.simulationInput.pkFile}">
        <td><spring:message code="cs_input.pk" /></td>
    </c:when>
    <c:otherwise>
        <td>?</td>
    </c:otherwise>
  </c:choose>
  <c:choose>
    <c:when test="${not empty simulation.simulationInput.plasmaConcMax}">
        <td class="border_r">
          <c:out value="${simulation.simulationInput.plasmaConcMin}" />
          -
          <c:out value="${simulation.simulationInput.plasmaConcMax}" />
          &nbsp;(<%= IC50_UNIT.µM %>)
        </td>
    </c:when>
    <c:when test="${not empty simulation.simulationInput.plasmaConcPoints}">
      <c:choose>
        <c:when test="${fn:length(simulation.simulationInput.plasmaConcPoints) > 30}">
        <td class="border_r effects_background"
            title="${fn:replace(simulation.simulationInput.plasmaConcPoints, ' ', '<br />')}">
          <c:out value="${fn:substring(simulation.simulationInput.plasmaConcPoints, 0, 30)}" /> ...
          (<%= IC50_UNIT.µM %>)
        </td>
        </c:when>
        <c:otherwise>
        <td class="border_r">
          <c:out value="${fn:replace(simulation.simulationInput.plasmaConcPoints, ' ', ',')}" />
          (<%= IC50_UNIT.µM %>)
        </td>
        </c:otherwise>
      </c:choose>
    </c:when>
    <%-- Using pkFileName as LAZY loading pkFile objects! --%>
    <c:when test="${not empty simulation.simulationInput.pkFileName}">
        <td class="border_r">
      <c:choose>
        <c:when test="${fn:length(simulation.simulationInput.pkFileName) > 30}">
          <c:out value="${fn:substring(simulation.simulationInput.pkFileName, 0, 30)}" />  ...
        </c:when>
        <c:otherwise>
          <c:out value="${simulation.simulationInput.pkFileName}" />
        </c:otherwise>
      </c:choose>
        </td>
    </c:when>
    <c:otherwise>
        <td class="border_r">?</td>
    </c:otherwise>
  </c:choose>
        <td class="border_r">
  <c:choose>
    <c:when test="${simulation_completed}">
          <span><spring:message code="general.complete" /></span>
    </c:when>
    <c:otherwise>
          <span id="progress_<c:out value="${simulation_id}"/>"></span>
    </c:otherwise>
  </c:choose>
        </td>
        <td class="center">
          <form action="<%=ClientDirectIdentifiers.ACTION_RESULTS_VIEW%>"
                method="get">
            <input type="submit"
                   name="opt"
                   value="<spring:message code="general.view" />"
                   title="<spring:message code="general.click_to_view_simulation" />" />
            <input type="hidden"
                   name="<%= ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID %>"
                   value="<c:out value="${simulation_id}"/>" />
          </form>
        </td>
        <td class="center border_r">
          <form action="<%=ClientDirectIdentifiers.ACTION_RESULTS_DELETE%>"
                method="post">
            <input type="hidden"
                   name="${_csrf.parameterName}"
                   value="${_csrf.token}"/>
            <input type="hidden"
                   name="<%= ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID %>"
                   value="<c:out value="${simulation_id}"/>" />
            <input type="submit"
                   id="submit_<c:out value="${simulation_id}" />"
                   name="opt"
  <c:if test="${!simulation_completed && !simulation_taking_too_long}">
  <%-- Hide the delete button if simulation is supposedly running and it's not taking too long --%>
                   style="display:none;"
  </c:if>
                   value="<c:out value="${delete_submit_value}" />"
                   title="<spring:message code="general.click_to_delete_simulation" />" />
          </form>
        </td>
      </tr>
</c:forEach>
    </tbody>
  </table>

  <script type="text/javascript">
    /* <![CDATA[ */
      /* Expand a tooltip for notes longer than a certain number of characters */
      jQuery('a[id^=hover_for_notes_]').tooltip({
        open: function(event, ui) {
          ui.tooltip.css('max-width', '1200px');
        },
        show: {
          effect: 'slideDown',
          delay: 150
        },
        position: {
          my: 'left top',
          at: 'left bottom'
        },
        content: function() {
          var element_id = jQuery(this).attr('id');
          var simulation_id = element_id.replace('hover_for_notes_', '');
          return jQuery('#expanded_' + simulation_id).html();
        },
        tooltipClass: 'preformatted',
        track: true
      });
    /* ]]> */
  </script>
