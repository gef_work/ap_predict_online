<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${fn:length(ma_simulations) gt 0}">
<div class="shadowed">
  <%@include file="../common/dataTable.jsp" %>
</div>
</c:if>