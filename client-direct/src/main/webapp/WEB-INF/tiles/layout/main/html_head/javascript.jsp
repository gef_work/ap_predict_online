<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers,
                uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers,
                uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService" %>
<c:set var="locale" value="${fn:toLowerCase(pageContext.response.locale)}" />
<c:set var="available_langs"><%=ClientSharedIdentifiers.I18N_LANGS%></c:set>
    <script type="text/javascript" src="<c:url value="/resources/js/dataTables/jquery.dataTables-1.9.4.min.js" />"></script>
    <script type="text/javascript">
      /* <![CDATA[ */
      var models = {};
<c:forEach items="${ma_models}"
           var="cellml_model">
      models['${cellml_model.identifier}'] = { 'name' : '<c:out value="${cellml_model.name}" />',
                                               'description' : '<c:out value="${cellml_model.description}" />' };
</c:forEach>
      var URL_PREFIX_LATEST_PROGRESS = '<%=ClientDirectIdentifiers.URL_PREFIX_LATEST_PROGRESS%>';
      var MODEL_ATTRIBUTE_LATEST_PROGRESS = '<%=ClientDirectIdentifiers.MODEL_ATTRIBUTE_LATEST_PROGRESS%>';
      var SIMULATION_FINISHED_STRING = '<%=ClientDirectService.PROGRESS_INDICATING_SIMULATION_HAS_FINISHED%>';
      var TEXT_COMPLETE = '<spring:message code="general.complete" htmlEscape="false" />';
      /* ]]> */
    </script>
    <script type="text/javascript" src="<c:url value="/resources/js/dataTables/dataTables.js" />"></script>
<c:if test="${fn:contains(available_langs,locale)}">
    <!-- http://datatables.net/plug-ins/i18n -->
    <script type="text/javascript" src="<c:url value="/resources/js/dataTables/messages_${locale}.js" />"></script>
</c:if>
    <script type="text/javascript">
      /* <![CDATA[ */
      jQuery(document).ready(
        function() {
          /* 0-based column number for simulation date */
          var simulation_date_column_number = 1;
          jQuery('#simulations').dataTable({
            'bJQueryUI': true,
            'sPaginationType': 'full_numbers',
            'aaSorting': [[ simulation_date_column_number, 'desc' ]]
<c:if test="${fn:contains(available_langs,locale)}">
            ,'oLanguage' : dataTablesI18N
</c:if>
          });
        }
      );
      /* ]]> */
    </script>