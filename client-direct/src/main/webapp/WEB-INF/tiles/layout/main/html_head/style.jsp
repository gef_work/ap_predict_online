<%@taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
    <link rel="stylesheet" href="<c:url value="/resources/css/datatables/demo_table_jui.css" />" type="text/css" />
    <style type="text/css">
      .effects_background {
        background-image: url(<c:url value="/resources/img/additional.png" />);
        background-position: right;
        background-repeat: no-repeat;
        padding-right: 20px !important;
      }
    </style>