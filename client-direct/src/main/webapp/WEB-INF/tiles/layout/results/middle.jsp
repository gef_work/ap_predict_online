<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers,
                uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers,
                uk.ac.ox.cs.nc3rs.client_direct.value.C50_TYPE,
                uk.ac.ox.cs.nc3rs.client_direct.value.IC50_UNIT,
                uk.ac.ox.cs.nc3rs.client_direct.value.INPUT_UNIT,
                uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO" %>
<c:set var="unit_pIC50" value="<%= SimulationInputVO.UNIT_MINUS_LOG_M %>" />
<div class="shadowed">
  <div id="progress" class="input_highlighted shadow_1" style="border: 6px; text-align: center;"></div>
  <table>
    <tr>
      <td style="vertical-align: top;">
        <div id="input">
          <table>
            <tbody>
<c:if test="${not empty ma_input_values.notes}">
              <tr>
                <td colspan="3" class="input_division cs_rounded_5"> <spring:message code="general.notes" />
              </tr>
              <tr>
                <td colspan="3">
<pre><c:out value="${ma_input_values.notes}" /></pre>
                </td>
              </tr>
</c:if>
              <tr>
                <td colspan="3" class="input_division cs_rounded_5"> <spring:message code="general.cell_model" /> </td>
              </tr>
              <tr>
                <td><spring:message code="general.model" /></td>
                <td class="input_highlighted cs_rounded_3" id="model"></td>
                <td id="model_desc"></td>
              </tr>
              <tr>
                <td colspan="3" class="input_division cs_rounded_5"> <spring:message code="general.pacing_details" /> </td>
              </tr>
              <tr>
                <td><spring:message code="general.pacing_frequency" /></td>
                <td class="input_highlighted cs_rounded_3">
                  <fmt:formatNumber maxFractionDigits="8" value="${ma_input_values.pacingFrequency}" />
                </td>
                <td> (<%= INPUT_UNIT.Hz %>)</td>
              </tr>
              <tr>
                <td><spring:message code="cs_simulation.maximum_pacing_time" /></td>
                <td class="input_highlighted cs_rounded_3">
                  <fmt:formatNumber maxFractionDigits="8" value="${ma_input_values.pacingMaxTime}" />
                </td>
                <td> (<%= INPUT_UNIT.minutes %>)</td>
              </tr>
              <tr>
                <td colspan="3" class="input_division cs_rounded_5"> <spring:message code="general.ion_channel_concentrations" /> </td>
              </tr>
              <tr>
                <td colspan="3">
                  <spring:message code="general.concentrations_note1" /><br />
<c:if test="${!ma_input_values.nonC50Value}">
                  <spring:message code="general.concentrations_note2" />
</c:if>
                </td>
              </tr>
              <tr>
                <td colspan="3">
                  <table>
                    <thead>
                      <tr>
                        <th><spring:message code="general.ion_current" /></th>
                        <th class="input_highlighted cs_rounded_3">
                          <c:out value="${ma_input_values.c50Type}" />
                          (
<c:choose>
  <c:when test="${not empty ma_input_values.ic50Units}">
    <c:out value="${ma_input_values.ic50Units}" />
  </c:when>
  <c:otherwise>
    <c:out value="${unit_pIC50}" />
  </c:otherwise>
</c:choose>
                          )
                        </th>
<c:choose>
  <c:when test="${ma_input_values.nonC50Value}">
                        <th class="input_highlighted cs_rounded_3">
                          <spring:message code="general.hill_coefficient" />
                        </th>
                        <th class="input_highlighted cs_rounded_3">
                          <spring:message code="general.saturation_level" /> (%)
                        </th>
    <c:if test="${ma_show_uncertainties}">
                        <th class="input_highlighted cs_rounded_3">
                          <spring:message code="general.spread" />
                        </th>
    </c:if>
  </c:when>
  <c:otherwise>
                        <th><spring:message code="general.channel_protein" /></th>
                        <th><spring:message code="general.channel_gene" /></th>
  </c:otherwise>
</c:choose>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>IKr</td>
                        <td class="input_highlighted cs_rounded_3"
<c:if test="${not empty ma_input_values.retrievePIC50Ikr()}">
                            title="pIC50: <fmt:formatNumber maxFractionDigits="10" value="${ma_input_values.retrievePIC50Ikr()}" /> (<c:out value="${unit_pIC50}" />)"
</c:if>>
                          <c:out value="${ma_input_values.IKrObservations.c50}" />
                        </td>
<c:choose>
  <c:when test="${ma_input_values.nonC50Value && not empty ma_input_values.retrievePIC50Ikr()}">
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.IKrObservations.hill}" />
                        </td>
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.IKrObservations.saturation}" />
                        </td>
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.IKrObservations.spread}" />
                        </td>
  </c:when>
  <c:when test="${ma_input_values.nonC50Value}">
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
  </c:when>
  <c:otherwise>
                        <td>K<sub>v</sub>11.1</td>
                        <td><i>hERG</i> or <i>KCNH2</i></td>
  </c:otherwise>
</c:choose>
                      </tr>
                      <tr>
                        <td>INa</td>
                        <td class="input_highlighted cs_rounded_3"
<c:if test="${not empty ma_input_values.retrievePIC50INa()}">
                            title="pIC50: <fmt:formatNumber maxFractionDigits="10" value="${ma_input_values.retrievePIC50INa()}" /> (<c:out value="${unit_pIC50}" />)"
</c:if>>
                          <c:out value="${ma_input_values.INaObservations.c50}" />
                        </td>
<c:choose>
  <c:when test="${ma_input_values.nonC50Value && not empty ma_input_values.retrievePIC50INa()}">
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.INaObservations.hill}" />
                        </td>
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.INaObservations.saturation}" />
                        </td>
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.INaObservations.spread}" />
                        </td>
  </c:when>
  <c:when test="${ma_input_values.nonC50Value}">
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
  </c:when>
  <c:otherwise>
                        <td>Na<sub>v</sub>1.5</td>
                        <td><i>SCN5A</i></td>
  </c:otherwise>
</c:choose>
                      </tr>
                      <tr>
                        <td>ICaL</td>
                        <td class="input_highlighted cs_rounded_3"
<c:if test="${not empty ma_input_values.retrievePIC50ICaL()}">
                            title="pIC50: <fmt:formatNumber maxFractionDigits="10" value="${ma_input_values.retrievePIC50ICaL()}" /> (<c:out value="${unit_pIC50}" />)"
</c:if>>
                          <c:out value="${ma_input_values.ICaLObservations.c50}" />
                        </td>
<c:choose>
  <c:when test="${ma_input_values.nonC50Value && not empty ma_input_values.retrievePIC50ICaL()}">
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.ICaLObservations.hill}" />
                        </td>
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.ICaLObservations.saturation}" />
                        </td>
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.ICaLObservations.spread}" />
                        </td>
  </c:when>
  <c:when test="${ma_input_values.nonC50Value}">
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
  </c:when>
  <c:otherwise>
                        <td>Ca<sub>v</sub>1.2</td>
                        <td><i>CACNA1C</i></td>
  </c:otherwise>
</c:choose>
                      </tr>
                      <tr>
                        <td>IKs</td>
                        <td class="input_highlighted cs_rounded_3"
<c:if test="${not empty ma_input_values.retrievePIC50Iks()}">
                            title="pIC50: <fmt:formatNumber maxFractionDigits="10" value="${ma_input_values.retrievePIC50Iks()}" /> (<c:out value="${unit_pIC50}" />)"
</c:if>>
                          <c:out value="${ma_input_values.IKsObservations.c50}" />
                        </td>
<c:choose>
  <c:when test="${ma_input_values.nonC50Value && not empty ma_input_values.retrievePIC50Iks()}">
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.IKsObservations.hill}" />
                        </td>
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.IKsObservations.saturation}" />
                        </td>
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.IKsObservations.spread}" />
                        </td>
  </c:when>
  <c:when test="${ma_input_values.nonC50Value}">
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
  </c:when>
  <c:otherwise>
                        <td>K<sub>v</sub>7.1</td>
                        <td><i>KCNQ1</i>/<i>minK</i></td>
  </c:otherwise>
</c:choose>
                      </tr>
                      <tr>
                        <td>IK1</td>
                        <td class="input_highlighted cs_rounded_3"
<c:if test="${not empty ma_input_values.retrievePIC50Ik1()}">
                            title="pIC50: <fmt:formatNumber maxFractionDigits="10" value="${ma_input_values.retrievePIC50Ik1()}" /> (<c:out value="${unit_pIC50}" />)"
</c:if>>
                          <c:out value="${ma_input_values.IK1Observations.c50}" />
                        </td>
<c:choose>
  <c:when test="${ma_input_values.nonC50Value && not empty ma_input_values.retrievePIC50Ik1()}">
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.IK1Observations.hill}" />
                        </td>
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.IK1Observations.saturation}" />
                        </td>
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.IK1Observations.spread}" />
                        </td>
  </c:when>
  <c:when test="${ma_input_values.nonC50Value}">
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
  </c:when>
  <c:otherwise>
                        <td>K<sub>ir</sub>2.1</td>
                        <td><i>KCNJ2</i></td>
  </c:otherwise>
</c:choose>
                      </tr>
                      <tr>
                        <td>Ito</td>
                        <td class="input_highlighted cs_rounded_3"
<c:if test="${not empty ma_input_values.retrievePIC50Ito()}">
                            title="pIC50: <fmt:formatNumber maxFractionDigits="10" value="${ma_input_values.retrievePIC50Ito()}" /> (<c:out value="${unit_pIC50}" />)"
</c:if>>
                          <c:out value="${ma_input_values.itoObservations.c50}" />
                        </td>
<c:choose>
  <c:when test="${ma_input_values.nonC50Value && not empty ma_input_values.retrievePIC50Ito()}">
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.itoObservations.hill}" />
                        </td>
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.itoObservations.saturation}" />
                        </td>
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.itoObservations.spread}" />
                        </td>
  </c:when>
  <c:when test="${ma_input_values.nonC50Value}">
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
  </c:when>
  <c:otherwise>
                        <td>K<sub>v</sub>4.3</td>
                        <td><i>KCND3</i></td>
  </c:otherwise>
</c:choose>
                      </tr>
                      <tr>
                        <td>INaL</td>
                        <td class="input_highlighted cs_rounded_3"
<c:if test="${not empty ma_input_values.retrievePIC50INaL()}">
                            title="pIC50: <fmt:formatNumber maxFractionDigits="10" value="${ma_input_values.retrievePIC50INaL()}" /> (<c:out value="${unit_pIC50}" />)"
</c:if>>
                          <c:out value="${ma_input_values.INaLObservations.c50}" />
                        </td>
<c:choose>
  <c:when test="${ma_input_values.nonC50Value && not empty ma_input_values.retrievePIC50INaL()}">
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.INaLObservations.hill}" />
                        </td>
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.INaLObservations.saturation}" />
                        </td>
                        <td class="input_highlighted cs_rounded_3">
                          <fmt:formatNumber maxFractionDigits="4" value="${ma_input_values.INaLObservations.spread}" />
                        </td>
  </c:when>
  <c:when test="${ma_input_values.nonC50Value}">
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
                        <td class="input_highlighted cs_rounded_3">&nbsp;</td>
  </c:when>
  <c:otherwise>
                        <td>Na<sub>v</sub>1.5</td>
                        <td><i>SCN5A</i></td>
  </c:otherwise>
</c:choose>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
<c:choose>
  <c:when test="${not empty ma_input_values.plasmaConcMax}">
              <tr>
                <td colspan="3" class="input_division cs_rounded_5"> <spring:message code="general.conc_range" /> </td>
              </tr>
              <tr>
                <td colspan="3">
                  <div>
                    <div>
                      <table>
                        <tbody>
                          <tr>
                            <td><spring:message code="cs_general.min" /></td>
                            <td class="input_highlighted cs_rounded_3">
                              <fmt:formatNumber maxFractionDigits="8" value="${ma_input_values.plasmaConcMin}" />
                            </td>
                            <td>(<%= IC50_UNIT.µM %>)</td>
                          </tr>
                          <tr>
                            <td><spring:message code="cs_general.max" /></td>
                            <td class="input_highlighted cs_rounded_3">
                              <fmt:formatNumber maxFractionDigits="8" value="${ma_input_values.plasmaConcMax}" />
                            </td>
                            <td>(<%= IC50_UNIT.µM %>)</td>
                          </tr>
                          <tr>
                            <td><spring:message code="general.intermediate_point_count" /></td>
                            <td class="input_highlighted cs_rounded_3">
                              <c:out value="${ma_input_values.plasmaIntPtCount}" />
                            </td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><spring:message code="general.intermediate_point_logscale" /></td>
                            <td class="input_highlighted cs_rounded_3">
                              <c:out value="${ma_input_values.plasmaIntPtLogScale}" />
                            </td>
                            <td>&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </td>
              </tr>
  </c:when>
  <c:when test="${not empty ma_input_values.plasmaConcPoints}">
              <tr>
                <td colspan="3" class="input_division cs_rounded_5">
                  <spring:message code="general.conc_points" />
                </td>
              </tr>
              <tr>
                <td colspan="3">
                  <ol class="conc_points">
    <c:forEach items="${ma_input_values.plasmaConcPoints}" var="plasma_conc_point">
                    <li>
                      <span class="input_highlighted cs_rounded_3">
                        <c:out value="${plasma_conc_point}" />
                      </span> (<%= IC50_UNIT.µM %>)
                    </li>
    </c:forEach>
                  </ol>
                </td>
              </tr>
  </c:when>
  <%-- Using pkFile objects as overruling default LAZY loading! --%>
  <c:when test="${not empty ma_input_values.pkFile}">
              <tr>
                <td colspan="3" class="input_division cs_rounded_5"> <spring:message code="cs_input.pk" /> </td>
              </tr>
              <tr>
                <td colspan="3">
    <c:choose>
      <c:when test="${fn:length(ma_input_values.pkFile.name) > 90}">
        <c:out value="${fn:substring(ma_input_values.pkFile.name, 0, 90)}" />  ...
      </c:when>
      <c:otherwise>
        <c:out value="${ma_input_values.pkFile.name}" />
      </c:otherwise>
    </c:choose>
                </td>
              </tr>
  </c:when>
</c:choose>
            </tbody>
          </table>
        </div>
      </td>
      <td style="vertical-align: top;">
        <div id="loading">
          <spring:message code="results.loading" /> ... <img src="<c:url value="/resources/img/spinner.gif" />" />
        </div>
        <div id="advice" style="display: none;">
          <p><spring:message code="results.advice_1" />.</p>
          <p>
            <spring:message code="results.advice_2" />.
          </p>
          <p>
            <spring:message code="results.advice_3" />.
          </p>
        </div>
        <div id="results" style="display: none;">
          <table>
            <tr>
              <td>
                <div id="non_ap_charts">
                  <ul>
<c:if test="${not empty ma_input_values.pkFile}">
                    <li><a href="#tab3">PKPD - APD90 vs. Timepoint</a></li>
</c:if>
                    <li><a href="#tab2">Δ APD90 vs. Conc</a></li>
                    <li><a href="#tab1">qNet vs. Conc</a></li>
                  </ul>
<c:if test="${not empty ma_input_values.pkFile}">
                  <div id="tab3">
                    <table>
                      <tr>
                        <td>
                          <span style="font-size: x-small;"><spring:message code="cs_results.zoom_graph" /></span>
                          <div id="plot_PK_placeholder_trace"
                               class="graph_placeholder_trace"></div>
                        </td>
                      </tr>
                    </table>
                  </div>
</c:if>
                  <div id="tab2">
                    <table>
                      <tr>
                        <td>
                          <span style="font-size: x-small;"><spring:message code="cs_results.zoom_graph" /></span>
                          <div id="pct_change_placeholder_trace"
                               class="graph_placeholder_trace"></div>
                        </td>
                      </tr>
                    </table>
                  </div>
                  <div id="tab1">
                    <table>
                      <tr>
                        <td>
                          <span style="font-size: x-small;"><spring:message code="cs_results.zoom_graph" /></span>
                          <div id="qnet_placeholder_trace"
                               class="graph_placeholder_trace"></div>
                        </td>
                      </tr>
                    </table>
                  </div>
                </div>
              </td>
              <td>
                <%-- PKPD plot legend --%>
                <div id="choices_trace_pkpd">
                  <b><spring:message code="cs_results.source" /></b>
                  <div id="plot_PK_choices_trace"></div>
                  <input id="plot_PK_clear_selection_trace"
                         type="button" class="graph_input clear_selection_trace"
                         value="<spring:message code="cs_results.reset_graph" />" />
                  <table class="dynamic_display">
                    <tr>
                      <td><spring:message code="cs_results.timepoint" /></td>
                      <td id="pk_point_val_x">h</td>
                    </tr>
                    <tr>
                      <td><spring:message code="cs_results.apd90" /></td>
                      <td id="pk_point_val_y">ms</td>
                    </tr>
                  </table>
                  <!-- overview graph (which may be hidden!) -->
                  <div id="plot_PK_overview_trace"
                       class="graph_overview_trace graph_colours"></div>
                  <!-- colour coordinates (which may be hidden!) -->
                  <p id="plot_PK_overview_legend_trace"
                     class="graph_plot_PK_overview_legend_trace"></p>
                </div>

                <%-- Δ APD90 and qNet legend --%>
                <div id="choices_trace_pctchange_qnet">
                  <b><spring:message code="cs_results.source" /></b>
                  <div id="pct_change_choices_trace"
                       class="choices_trace"></div>
                  <input id="pct_change_clear_selection_trace"
                         type="button" class="graph_input clear_selection_trace"
                         value="<spring:message code="cs_results.reset_graph" />" />
                  <table class="dynamic_display">
                    <tr>
                      <td><spring:message code="cs_results.conc" />.</td>
                      <td id="point_val_x"></td>
                      <td><%= IC50_UNIT.µM %></td>
                    </tr>
                    <tr>
                      <td><spring:message code="cs_results.change" /></td>
                      <td id="point_val_y"></td>
                      <td>%</td>
                    </tr>
                  </table>
                  <!-- overview graph (which may be hidden!) -->
                  <div id="pct_change_overview_trace"
                       class="graph_overview_trace graph_colours"></div>
                  <!-- colour coordinates (which may be hidden!) -->
                  <p id="pct_change_overview_legend_trace"
                     class="graph_pct_change_overview_legend_trace"></p>
                </div>

                <%-- Export as .xsl (Excel) button --%>
                <div>
                  <form action="<%= ClientDirectIdentifiers.URL_PREFIX_EXCEL %>"
                        method="get"
                        target="_blank"
                        class="inline_block">
                    <input type="hidden"
                           name="<%= ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID %>"
                           value="<c:out value="${ma_simulation_id}" />" />
                    <input type="submit"
                           title="<spring:message code="cs_results.export_as_xls_title" />"
                           value=" <spring:message code="cs_results.export_as_xls" /> "/>
                  </form>
                </div>
              </td>
            </tr>
          </table>
          <br />
          <table >
            <tr>
              <td>
                <span style="font-size: x-small;"><spring:message code="cs_results.zoom_graph" /></span>
                <div id="ap_placeholder_trace"
                     class="graph_placeholder_trace"></div>
              </td>
              <td>
                <b><spring:message code="cs_results.simulation_and_concentration" /> (<%= IC50_UNIT.µM %>)</b>
                <div id="ap_choices_trace"></div>
                <p></p>
                <p></p>&nbsp;&nbsp;
                <input id="ap_clear_selection_trace"
                       type="button" class="graph_input"
                       value="<spring:message code="cs_results.reset_graph" />" />
                <!-- overview graph (which may be hidden) -->
                <p></p>
                <div id="ap_overview_trace"
                     class="graph_overview_trace graph_colours"></div>
                <!-- color coordinates (which may be hidden) -->
                <p id="ap_overview_legend_trace"
                   class="graph_ap_overview_legend_trace"></p>
              </td>
            </tr>
          </table>
        </div>
      </td>
    </tr>
  </table>
</div>