<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers,
                uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers,
                uk.ac.ox.cs.nc3rs.client_direct.value.IC50_UNIT,
                uk.ac.ox.cs.nc3rs.client_direct.value.INPUT_UNIT,
                uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO" %>
    <script type="text/javascript">
      /* <![CDATA[ */

      var DIV_ADVICE;
      var DIV_LOADING;
      var DIV_PROGRESS;
      var DIV_RESULTS;
<c:if test="${not empty ma_input_values.pkFile}">
      var DIV_NON_AP_CHARTS;
</c:if>

      var KEY_PKRESULTS_TIMEPOINT_DATA = '<%=ClientDirectIdentifiers.KEY_PKRESULTS_TIMEPOINT_DATA%>';
      var KEY_PKRESULTS_PERTIMEPOINT_APD90S = '<%=ClientDirectIdentifiers.KEY_PKRESULTS_PERTIMEPOINT_APD90S%>';
      var KEY_PKRESULTS_PERTIMEPOINT_FORTRANS = '<%=ClientDirectIdentifiers.KEY_PKRESULTS_PERTIMEPOINT_FORTRANS%>';

      /* shared between information data and JOB_DETAILS data */
      var KEY_SHARED_GROUP_NAME = 'groupName';
      var KEY_SHARED_GROUP_LEVEL = 'groupLevel'; 
      var KEY_SHARED_PACING_FREQUENCY = 'pacingFrequency';

      var KEY_RESULTS_JOB_ID = '<%=ClientDirectIdentifiers.KEY_RESULTS_JOB_ID%>';
      var KEY_RESULTS_MESSAGES = '<%=ClientDirectIdentifiers.KEY_RESULTS_MESSAGES%>';
      var KEY_RESULTS_DELTA_APD90_PERCENTILE_NAMES = '<%=ClientDirectIdentifiers.KEY_RESULTS_DELTA_APD90_PERCENTILE_NAMES%>';
      var KEY_RESULTS_PERREF_APD90 = '<%=ClientDirectIdentifiers.KEY_RESULTS_PERREF_APD90%>';
      var KEY_RESULTS_PERREF_DELTA_APD90 = '<%=ClientDirectIdentifiers.KEY_RESULTS_PERREF_DELTA_APD90%>';
      var KEY_RESULTS_PERREF_QNET = '<%=ClientDirectIdentifiers.KEY_RESULTS_PERREF_QNET%>';
      var KEY_RESULTS_PERREF_UPSTROKE_VELOCITY = '<%=ClientDirectIdentifiers.KEY_RESULTS_PERREF_UPSTROKE_VELOCITY%>';
      var KEY_RESULTS_PERREF_TIMES = '<%=ClientDirectIdentifiers.KEY_RESULTS_PERREF_TIMES%>';
      var KEY_RESULTS_PERREF_VOLTAGES = '<%=ClientDirectIdentifiers.KEY_RESULTS_PERREF_VOLTAGES%>';
      var KEY_RESULTS_INPUT_OPTION = '<%=ClientDirectIdentifiers.KEY_RESULTS_INPUT_OPTION%>';
      var KEY_RESULTS_REFERENCE_DATA = '<%=ClientDirectIdentifiers.KEY_RESULTS_REFERENCE_DATA%>';
      var KEY_RESULTS_RESULTS = '<%=ClientDirectIdentifiers.KEY_RESULTS_RESULTS%>';

      var MODEL_ATTRIBUTE_RESULTS = '<%=ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS%>';

      var SETINTERVAL_PROGRESS_ID;

      var SPINNER_LOCATION = '<c:url value="/resources/img/spinner.gif" />';

      var URL_PREFIX_RESULTS = '<%=ClientDirectIdentifiers.URL_PREFIX_RESULTS%>';

      /* job details */
      var JOB_DETAILS = {};

      var MICRO_TEXT = '<%= IC50_UNIT.µM %>';
      var HZ_TEXT = '<%= INPUT_UNIT.Hz %>'
      var CONTROL_LABEL = '0' + MICRO_TEXT;
      var DELIMITER = ' @ ';

      var AP_DATASETS = [];
      var PCT_CHANGE_DATASETS = [];
      var PLOT_PK_DATASETS = [];
      var QNET_DATASETS = [];

      <%-- GRAPH_LABEL_? require javascript escaping in case someone uses a ',",\, etc. char in the text
           (but no html escaping because it's going directly into a js string for output) --%>
      var GRAPH_LABEL_CONC_UM = '<spring:message code="cs_results.graphlabel_conc_um" htmlEscape="false" javaScriptEscape="true" />';
      var GRAPH_LABEL_APD90 = '<spring:message code="cs_results.graphlabel_apd90" htmlEscape="false" javaScriptEscape="true" />';
      var GRAPH_LABEL_DELTA_APD90 = '<spring:message code="cs_results.graphlabel_delta_apd90" htmlEscape="false" javaScriptEscape="true" />';
      var GRAPH_LABEL_QNET = '<spring:message code="cs_results.graphlabel_qnet" htmlEscape="false" javaScriptEscape="true" />';
      var GRAPH_LABEL_TIME = '<spring:message code="cs_results.graphlabel_time" htmlEscape="false" javaScriptEscape="true" />';
      var GRAPH_LABEL_TIMEPOINT_H = '<spring:message code="cs_results.graphlabel_timepoint_h" htmlEscape="false" javaScriptEscape="true" />';
      var GRAPH_LABEL_TIMEPOINT_S = '<spring:message code="cs_results.graphlabel_timepoint_s" htmlEscape="false" javaScriptEscape="true" />';
      var GRAPH_LABEL_MS = '<spring:message code="cs_results.graphlabel_ms" htmlEscape="false" javaScriptEscape="true" />';
      var GRAPH_LABEL_MEMBRANE_VOLTAGE = '<spring:message code="cs_results.graphlabel_membrane_voltage" htmlEscape="false" javaScriptEscape="true" />';
      var GRAPH_LABEL_MV = '<spring:message code="cs_results.graphlabel_mv" htmlEscape="false" javaScriptEscape="true" />';
      var GRAPH_LABEL_TIME_MS = GRAPH_LABEL_TIME + ' (' + GRAPH_LABEL_MS + ')';
      var GRAPH_LABEL_MEMBRANE_VOLTAGE_MV = GRAPH_LABEL_MEMBRANE_VOLTAGE + ' (' + GRAPH_LABEL_MV + ')';
      var TEXT_AT_A_CONCENTRATION_OF = '<spring:message code="cs_results.at_a_conc_of" htmlEscape="false" javaScriptEscape="true" />';
      var TEXT_AT_A_TIMEPOINT_OF = '<spring:message code="cs_results.at_a_timepoint_of" htmlEscape="false" javaScriptEscape="true" />';
      var TEXT_CONC = '<spring:message code="cs_results.conc" htmlEscape="false" javaScriptEscape="true" />';
      var TEXT_HAS_APD90 = '<spring:message code="cs_results.has_apd90" htmlEscape="false" javaScriptEscape="true" />';
      var TEXT_HAS_CHANGED = '<spring:message code="cs_results.has_changed" htmlEscape="false" javaScriptEscape="true" />';
      var TEXT_PLEASE_WAIT = '<spring:message code="results.please_wait" htmlEscape="false" javaScriptEscape="true" /> ... ';
      var TEXT_PREPARING = '<spring:message code="results.preparing" htmlEscape="false" javaScriptEscape="true" />';
      var TEXT_QNET = '<spring:message code="cs_results.qnet" htmlEscape="false" javaScriptEscape="true" />';
      var TEXT_QNET_AVAILABILITY = '<spring:message code="cs_results.qnet_availability" htmlEscape="false" javaScriptEscape="true" />';
      var TEXT_SHOW_CONFIDENCE = '<spring:message code="cs_results.show_confidence" htmlEscape="false" javaScriptEscape="true" />';
      var TEXT_SIMULATION_NAME = '<spring:message code="results.simulation" htmlEscape="false" javaScriptEscape="true" />';
      var TEXT_TIMEPOINT = '<spring:message code="cs_results.timepoint" htmlEscape="false" javaScriptEscape="true" />';
      var TEXT_VIEW_MESSAGES = '<spring:message code="cs_results.view_messages" htmlEscape="false" javaScriptEscape="true" />';

      /* Usually derived from provenance data. For client (direct) it's hardcoded. */
      var HARDCODED_JOB_DETAIL = {};
      HARDCODED_JOB_DETAIL[KEY_SHARED_GROUP_NAME] = TEXT_SIMULATION_NAME; /* e.g. "simulation" */
      HARDCODED_JOB_DETAIL[KEY_SHARED_GROUP_LEVEL] = '1';
      HARDCODED_JOB_DETAIL[KEY_SHARED_PACING_FREQUENCY] = '<c:out value="${ma_input_values.pacingFrequency}" />';

      Object.size = function(obj) {
        var size = 0;
        var key;
        for (key in obj) {
          if (obj.hasOwnProperty(key)) size++;
        }
        return size;
      };

      /* ]]> */
    </script>
    <!--[if lte IE 8]>
    <script type="text/javascript" src="<c:url value="/resources/js/query/flot/0.8.1/excanvas.min.js" />"></script>
    <![endif]-->
    <script type="text/javascript" src="<c:url value="/resources/js/results/general.js" />"></script>

    <script type="text/javascript" src="<c:url value="/resources/js/jquery/flot/0.8.1/jquery.flot.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/flot/0.8.1/jquery.flot.canvas.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/flot/0.8.1/jquery.flot.errorbars.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/flot/0.8.1/jquery.flot.fillbetween.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/flot/0.8.1/jquery.flot.selection.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/flot/jquery.flot.axislabels.js" />"></script>

    <script type="text/javascript" src="<c:url value="/resources/js/various/base64.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/various/canvas2image.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/flot/jquery.flot.saveAsImage.js" />"></script>

    <script type="text/javascript" src="<c:url value="/resources/js/cs-flotting.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/cs-qnet.js" />"></script>

    <script type="text/javascript" src="<c:url value="/resources/js/results/graph_action_potential.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/results/graph_pct_change.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/results/graph_qnet.js" />"></script>
<c:if test="${not empty ma_input_values.pkFile}">
    <script type="text/javascript" src="<c:url value="/resources/js/results/graph_plot_PK.js" />"></script>
</c:if>
    <script type="text/javascript" src="<c:url value="/resources/js/results/results_client.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/results/results.js" />"></script>

    <script type="text/javascript">
      /* <![CDATA[ */
      var models = [];
<c:forEach items="${ma_models}"
           var="cellml_model">
      models.push({ 'id' : <c:out value="${cellml_model.identifier}" />,
                    'name' : '<c:out value="${cellml_model.name}" />',
                    'description' : '<c:out value="${cellml_model.description}" />' });
</c:forEach>

      const viewing = {
        PKPD: 'PKPD',
        DELTAAPD90: 'Δ APD90',
        QNET: 'qNet'
      }
      var current_viewing;

      jQuery(document).ready(
        function() {

          DIV_ADVICE = jQuery('#advice');
          DIV_LOADING = jQuery('#loading');
          DIV_PROGRESS = jQuery('#progress');
          DIV_RESULTS = jQuery('#results');

          var pkpd = jQuery('#choices_trace_pkpd');
          var pctchange_qnet = jQuery('#choices_trace_pctchange_qnet');

          DIV_NON_AP_CHARTS = jQuery('#non_ap_charts');
          var tab_cnt = DIV_NON_AP_CHARTS.find('>ul >li').length;
          var has_pkpd = (tab_cnt == 3);
          /* Set default display */
          pkpd.hide();
          pctchange_qnet.hide();
          if (has_pkpd) {
            pkpd.show();
            current_viewing = viewing.PKPD;
          } else {
            pctchange_qnet.show();
            current_viewing = viewing.DELTAAPD90;
          }

          /* Adjust choices trace if someone clicks on the flot results display tabs */
          DIV_NON_AP_CHARTS.tabs({
            activate: function(event, ui) {
              var tab_idx = ui.newTab.index();
              pkpd.hide();
              pctchange_qnet.hide();

              var has_qnet = Object.size(QNET_DATASETS) > 0;
              switch (tab_idx) {
                /*
                   If PKPD    : 0 == PKPD; 1 == Δ APD90; 2 == qNet
                   If no PKPD : 0 == Δ APD90; 1 == qNet
                 */
                case 0 :
                  if (has_pkpd) {
                    /* 3-tab */
                    pkpd.show();
                    current_viewing = viewing.PKPD;
                  } else {
                    /* For Δ APD90 (when 2-tab) */
                    pctchange_qnet.show();
                    current_viewing = viewing.DELTAAPD90;
                  }
                  break;
                case 1 :
                  /* For Δ APD90 (when 3-tab) or qNet (when 2-tab)*/
                  if (has_pkpd) {
                    current_viewing = viewing.DELTAAPD90;
                    /* Show pctchange_qnet when PKPD */
                    pctchange_qnet.show();
                  } else {
                    current_viewing = viewing.QNET;
                    if (has_qnet) {
                      /* Show pctchange_qnet when qNet data */
                      pctchange_qnet.show();
                    }
                  }
                  break;
                case 2 :
                  if (has_pkpd) {
                    /* For qNet (when 3-tab) */
                    current_viewing = viewing.QNET;
                    if (has_qnet) {
                      /* Show when qNet data */
                      pctchange_qnet.show();
                    }
                  } else {
                    console.log('ERROR : Tab index 2 selection expected only when PKPD data available');
                  }
                  break;
                default :
                  console.log('ERROR : ' + tab_cnt + ' tabs encountered when max. 2 expected.');
              }
            }
          });

          SETINTERVAL_PROGRESS_ID = setInterval(function() {
            DIV_PROGRESS.toggleClass('shadow_2');
          }, 1000);

          /* ajax_for_results defined in results.js */
          ajax_for_results('<c:out value="${ma_simulation_id}" />');
<c:if test="${not empty ma_input_values.modelIdentifier}">
          jQuery.each(models, function(index, value) {
            var model = value;
            if (model.id == '<c:out value="${ma_input_values.modelIdentifier}" />') {
              jQuery('#model').html(model.name);
              jQuery('#model_desc').html(model.description);
              return;
            }
          });
</c:if>
<c:if test="${ma_show_dynamic_cellml && not empty ma_input_values.cellMLFileName}">
          jQuery('#model').html('<c:out value="${ma_input_values.cellMLFileName}" />');
          jQuery('#model_desc').html('&nbsp;');
</c:if>
          /* Excel report form submission */
          jQuery('#export_form').submit(function(event) {
            jQuery('input:hidden[name=<%= ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID %>]').val(CURRENT_SIMULATION_ID);
          });
        }
      );
      /* ]]> */
    </script>
