<%@taglib prefix="c"
          uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="spring"
          uri="http://www.springframework.org/tags" %>
<%@page import="uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers,
                uk.ac.ox.cs.nc3rs.client_direct.value.InputOption,
                uk.ac.ox.cs.nc3rs.client_direct.value.StatusVO" %>
<c:set value="${pageContext.request.contextPath}/"
       var="context_path" />
<%-- Correspond to properties defined in AJAX controller inner class --%>
<c:set value="<%=ClientDirectIdentifiers.KEY_EXCEPTION%>"
       var="key_exception" />
<c:set value="<%=ClientDirectIdentifiers.KEY_JSON%>"
       var="key_json" />
<c:set value="<%=StatusVO.TEXT_PROGRESS%>"
       var="text_progress" />
    <script type="text/javascript">
     /* <![CDATA[ */

     jQuery(document).tooltip();

     var CONTEXT_PATH = '${pageContext.request.contextPath}/';
     var INPUT_OPTION_PK = '<%=InputOption.PK%>';
     var INPUT_OPTION_CONC_POINTS = '<%=InputOption.CONC_POINTS%>';
     var INPUT_OPTION_CONC_RANGE = '<%=InputOption.CONC_RANGE%>'
     var KEY_EXCEPTION = '<c:out value="${key_exception}" />';
     var KEY_JSON = '<c:out value="${key_json}" />';
     <%-- Note appended space below.. --%>
     var TEXT_PROG = '<c:out value="${text_progress}" /> ';
     /* ]]> */
    </script>
    <script src="<c:url value="/resources/js/general.js" />"
            type="text/javascript" ></script>
    <script src="<c:url value="/resources/js/site/site.js" />"
            type="text/javascript" ></script>
