<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers,
                uk.ac.ox.cs.compbio.client_shared.business.file.FileStoragePK,
                uk.ac.ox.cs.compbio.client_shared.entity.PortalFile,
                uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers,
                uk.ac.ox.cs.nc3rs.client_direct.config.Configuration,
                uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO,
                uk.ac.ox.cs.nc3rs.client_direct.value.C50_TYPE,
                uk.ac.ox.cs.nc3rs.client_direct.value.IC50_UNIT,
                uk.ac.ox.cs.nc3rs.client_direct.value.InputOption,
                uk.ac.ox.cs.nc3rs.client_direct.value.INPUT_UNIT,
                uk.ac.ox.cs.nc3rs.client_direct.value.IonCurrent" %>
<c:set var="max_pacing_frequency"><%=SimulationInputVO.MAX_PACING_FREQUENCY%></c:set>
<c:set var="max_pacing_maxtime"><%=SimulationInputVO.MAX_PACING_MAX_TIME%></c:set>
<c:set var="recommended_max_plasma_conc"><%=Configuration.RECOMMENDED_PLASMA_CONC_MAX%></c:set>
<c:set var="min_pacing_frequency"><%=SimulationInputVO.MIN_PACING_FREQUENCY%></c:set>
<c:set var="min_plasma_conc"><%=Configuration.PLASMA_CONC_MIN%></c:set>
<c:set var="view_contact"><%=ClientSharedIdentifiers.VIEW_CONTACT%></c:set>
<c:set var="contact_name"><spring:message code="cs_general.contact" /></c:set>
<c:set var="notes_length"><%=SimulationInputVO.MAX_NOTES_LENGTH%></c:set>
<c:set var="info_location"><c:url value="/resources/img/info.png" /></c:set>
<c:set var="hill_coefficient"><spring:message code="general.hill_coefficient" /></c:set>
<c:set var="saturation_level"><spring:message code="general.saturation_level" /></c:set>
<c:set var="max_conc_point_count"><%=SimulationInputVO.MAX_CONC_POINT_COUNT %></c:set>
<c:set var="min_hill"><%=SimulationInputVO.MIN_HILL_COEFFICIENT %></c:set>
<c:set var="max_hill"><%=SimulationInputVO.MAX_HILL_COEFFICIENT %></c:set>
<c:set var="min_spread_gt"><%=SimulationInputVO.MIN_SPREAD_GT %></c:set>
<c:set var="max_spread"><%=SimulationInputVO.MAX_SPREAD %></c:set>
<c:set var="option_conc_points"><%=InputOption.CONC_POINTS %></c:set>
<c:set var="option_conc_range"><%=InputOption.CONC_RANGE %></c:set>
<c:set var="option_pk"><%=InputOption.PK %></c:set>
<c:set var="file_max_size"><%=PortalFile.MAX_SIZE%></c:set>
<c:set var="pk_max_all_columns"><%=FileStoragePK.MAX_ALL_COLUMNS%></c:set>
<c:set var="channel_ical"><%=IonCurrent.ICaL.toString()%></c:set>
<c:set var="channel_ik1"><%=IonCurrent.IK1.toString()%></c:set>
<c:set var="channel_ikr"><%=IonCurrent.IKr.toString()%></c:set>
<c:set var="channel_iks"><%=IonCurrent.IKs.toString()%></c:set>
<c:set var="channel_ina"><%=IonCurrent.INa.toString()%></c:set>
<c:set var="channel_ito"><%=IonCurrent.Ito.toString()%></c:set>
<c:set var="channel_inal"><%=IonCurrent.INaL.toString()%></c:set>
<div class="shadowed">
  <form action="<%=ClientDirectIdentifiers.ACTION_RESULTS_VIEW%>"
        accept-charset="UTF-8"
        enctype="multipart/form-data"
        id="simulation_run_form"
        method="post">
    <input type="hidden"
           name="${_csrf.parameterName}"
           value="${_csrf.token}" />
    <input type="hidden"
           name="<%= ClientDirectIdentifiers.PARAM_NAME_TOKEN %>"
           value="${ma_token}" />
<c:if test="${ma_show_uncertainties}">
    <%-- Currently only "hardcoding" the values. --%>
    <input type="hidden"
           name="<%= ClientDirectIdentifiers.PARAM_NAME_PCTILES%>"
           value="${ma_pctiles}" />
</c:if>
    <table>
      <tbody>
<c:if test="${not empty ma_input_values && not empty ma_input_values.assignmentProblems}">
        <tr>
          <td colspan="4" class="input_division cs_rounded_5"> <spring:message code="input.problems" /> </td>
        </tr>
        <tr>
          <td colspan="4" class="cs_block_span">
  <c:forEach items="${ma_input_values.assignmentProblems}"
             var="assignment_problem">
          <span class="error_text"><c:out value="${assignment_problem}" /></span>
  </c:forEach>
          </td>
        </tr>
</c:if>
        <tr>
          <td colspan="4" class="input_division cs_rounded_5">
            <spring:message code="general.notes" />
            <img src="${info_location}" class="info"
                 title="<spring:message code="input.notes_max_chars"
                                        arguments="${notes_length}" /> "/>
          </td>
        </tr>
        <tr>
          <td colspan="4">
            <textarea name="<%=ClientDirectIdentifiers.PARAM_NAME_NOTES%>"
                      rows="3" cols="120" wrap="hard" 
                      maxlength="<%=SimulationInputVO.MAX_NOTES_LENGTH%>"><c:if test="${not empty ma_input_values}"><c:out value="${ma_input_values.notes}" /></c:if></textarea>
            <p style="font-size: small;"><spring:message code="input.notes_warning" /></p>
          </td>
        </tr>
        <tr>
          <td colspan="4" class="input_division cs_rounded_5">
            <spring:message code="general.cell_model" />
            <span style="font-size: small; margin-left: 20px;">
              <%-- Not escaping because the message contains an html <a /> element to render --%>
              ( <spring:message code="input.contact_more_models"
                                arguments="${pageContext.request.contextPath},${view_contact},${contact_name}"
                                htmlEscape="false" /> )
            </span>
          </td>
        </tr>
        <tr>
          <td><spring:message code="general.model" /></td>
          <td id="models"></td>
          <td style="width: 450px;">
            <span id="model_desc" ></span>
<c:if test="${ma_show_dynamic_cellml}">
            <img src="${info_location}" class="info upload_cellml"
                 title="<spring:message code="cs_input.upload_cellml_file_desc"
                                        arguments="${file_max_size}" />" />
            <input class="upload_cellml" type="file" name="<%=ClientSharedIdentifiers.PARAM_NAME_FILE_CELLML%>" />
</c:if>
          </td>
          <td>
            <button id="model_dialog_button"
                    type="button"> <spring:message code="input.all_model_details" /> </button>
            <div id="model_dialog_div">
              <table>
<c:forEach items="${ma_models}"
           var="model">
                <tr>
                  <td>
  <c:out value="${model.description}" />
                  </td>
                  <td>
  <c:if test="${not empty model.cellmlURL}">
                    - <a href="<c:out value="${model.cellmlURL}" />" target="blank">CellML</a>
  </c:if>
                  </td>
                  <td>
  <c:if test="${not empty model.paperURL}">
                    - <a href="<c:out value="${model.paperURL}" />" target="blank">Paper</a>
  </c:if>
                  </td>
                </tr>
</c:forEach>
              </table>
            </div>
          </td>
        </tr>
        <tr>
          <td colspan="4" class="input_division cs_rounded_5"> <spring:message code="general.pacing_details" /> </td>
        </tr>
        <tr>
          <td><spring:message code="general.pacing_frequency" /></td>
          <td>
            <input name="<%=ClientDirectIdentifiers.PARAM_NAME_PACING_FREQUENCY%>"
                   size="10" maxlength="10"
                   type="text" /> (<%= INPUT_UNIT.Hz %>)
            <img src="${info_location}" class="info"
                 title="<spring:message code="input.min_max"
                                        arguments="${min_pacing_frequency},${max_pacing_frequency}" /> "/>
          </td>
          <td><spring:message code="input.desc_pacing_frequency" />
          </td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><spring:message code="cs_simulation.maximum_pacing_time" /></td>
          <td>
            <input name="<%=ClientDirectIdentifiers.PARAM_NAME_PACING_MAX_TIME%>"
                   size="10" maxlength="10"
                   type="text" /> (<%= INPUT_UNIT.mins %>)
            <img src="${info_location}" class="info"
                 title="<spring:message code="input.min_max"
                                        arguments=">0,${max_pacing_maxtime}" />" />
          </td>
          <td><spring:message code="cs_simulation.maximum_pacing_time" /></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="4" class="input_division cs_rounded_5"> <spring:message code="general.ion_channel_concentrations" /> </td>
        </tr>
        <tr>
          <td colspan="4">
            <spring:message code="general.concentrations_note1" /><br />
            <spring:message code="general.concentrations_note2"
                            arguments="${hill_coefficient}, ${saturation_level}"
                            htmlEscape="false" /><br />
          </td>
        </tr>
        <tr>
          <td colspan="4">
            <table>
              <thead>
                <tr>
                  <th><spring:message code="general.ion_current" /></th>
                  <th colspan="2">
                    <select id="c50_type"
                            name="<%=ClientDirectIdentifiers.PARAM_NAME_C50_TYPE%>">
                      <option value="<%=C50_TYPE.pIC50%>"><%=C50_TYPE.pIC50%></option>
                      <option value="<%=C50_TYPE.IC50%>"><%=C50_TYPE.IC50%></option>
                    </select>
                    <span id="ic50_span"
                          style="display: none">
                      <select id="ic50_units"
                              name="<%=ClientDirectIdentifiers.PARAM_NAME_IC50_UNITS%>">
                        <option value="<%=IC50_UNIT.M%>"><%=IC50_UNIT.M%></option>
                        <option value="<%=IC50_UNIT.µM%>"><%= IC50_UNIT.µM %></option>
                        <option value="<%=IC50_UNIT.nM%>"><%=IC50_UNIT.nM%></option>
                      </select>
                      <img src="${info_location}" class="info"
                           title="<spring:message code="input.min" arguments="IC50 > 0" />" />
                    </span>
                  </th>
                  <th>
                    <span class="non_additional_info">
                      <spring:message code="input.more_options" /><br />
                      <button type="button" class="details_button"
                              title="<spring:message code="input.more_options_title" />"> + </button>
                    </span>
                    <span class="additional_info">
                      <spring:message code="input.fewer_options" /><br />
                      <button type="button" class="details_button"
                              title="<spring:message code="input.fewer_options_title" />"> — </button>
                    </span>
                  </th>
                  <th class="additional_info">
                    <spring:message code="general.hill_coefficient" /> 
                    <img src="${info_location}" class="info"
                         title="<spring:message code="input.min_max"
                                                arguments="${min_hill},${max_hill}" />" />
                  </th>
                  <th class="additional_info">
                    <spring:message code="general.saturation_level" /> (%)
                    <img src="${info_location}" class="info"
                         title="<spring:message code="input.saturation_info" />" />
                  </th>
<c:if test="${ma_show_uncertainties}">
                  <th class="additional_info" colspan="2">
                    <spring:message code="input.spread" />
                    <img src="${info_location}" class="info"
                         title="<spring:message code="input.spread_info"
                                                arguments="${min_spread_gt},${max_spread}" />" />
                    &nbsp;-
                    <a href="https://doi.org/10.1016/j.vascn.2013.04.007"
                       target="blank"
                       title="Journal of Pharmacological and Toxicological Methods - 
                              Variability in high-throughput ion-channel screening data and consequences for cardiac safety assessment">Paper</a>
                  </th>
</c:if>
                  <th class="non_additional_info"><spring:message code="general.channel_protein" /></th>
                  <th class="non_additional_info"><spring:message code="general.channel_gene" /></th>
                  <th class="non_additional_info"><spring:message code="cs_general.description" /></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>IKr</td>
                  <td>
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_IKR_C50%>"
                           size="12" maxlength="12"
                           type="text" />
                  </td>
                  <td class="c50_units">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="additional_info">
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_IKR_HILL%>"
                           size="8" maxlength="8"
                           type="text" />
                  </td>
                  <td class="additional_info">
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_IKR_SATURATION%>"
                           size="8" maxlength="8"
                           type="text" />
                  </td>
<c:if test="${ma_show_uncertainties}">
                  <td id="spreads_toggle" class="additional_info" rowspan="7">
                    <spring:message code="input.spreads_toggle" htmlEscape="false" /><br />
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_SPREADS_ENABLED%>"
                           type="checkbox" />
                  </td>
                  <td class="additional_info">
                    <input class="spread_entry"
                           name="<%=ClientDirectIdentifiers.PARAM_NAME_IKR_SPREAD%>"
                           size="8" maxlength="8"
                           type="text" />
                    <span class="default_spread" style="color: grey;">&nbsp;&nbsp;(<span><c:out value="${ma_spreads[channel_ikr]}" /></span>)</span>
                  </td>
</c:if>
                  <td class="non_additional_info">K<sub>v</sub>11.1</td>
                  <td class="non_additional_info"><i>hERG</i> or <i>KCNH2</i></td>
                  <td class="non_additional_info"><spring:message code="input.desc_ion_current_IKr" /></td>
                </tr>
                <tr>
                  <td>INa</td>
                  <td>
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_INA_C50%>"
                           size="12" maxlength="12"
                           type="text" />
                  </td>
                  <td class="c50_units">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="additional_info">
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_INA_HILL%>"
                           size="8" maxlength="8"
                           type="text" />
                  </td>
                  <td class="additional_info">
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_INA_SATURATION%>"
                           size="8" maxlength="8"
                           type="text" />
                  </td>
<c:if test="${ma_show_uncertainties}">
                  <td class="additional_info">
                    <input class="spread_entry"
                           name="<%=ClientDirectIdentifiers.PARAM_NAME_INA_SPREAD%>"
                           size="8" maxlength="8"
                           type="text" />
                    <span class="default_spread" style="color: grey;">&nbsp;&nbsp;(<span><c:out value="${ma_spreads[channel_ina]}" /></span>)</span>
                  </td>
</c:if>
                  <td class="non_additional_info">Na<sub>v</sub>1.5</td>
                  <td class="non_additional_info"><i>SCN5A</i></td>
                  <td class="non_additional_info"><spring:message code="input.desc_ion_current_INa" /></td>
                </tr>
                <tr>
                  <td>ICaL</td>
                  <td>
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_ICAL_C50%>"
                           size="12" maxlength="12"
                           type="text" />
                  </td>
                  <td class="c50_units">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="additional_info">
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_ICAL_HILL%>"
                           size="8" maxlength="8"
                           type="text" />
                  </td>
                  <td class="additional_info">
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_ICAL_SATURATION%>"
                           size="8" maxlength="8"
                           type="text" />
                  </td>
<c:if test="${ma_show_uncertainties}">
                  <td class="additional_info">
                    <input class="spread_entry"
                           name="<%=ClientDirectIdentifiers.PARAM_NAME_ICAL_SPREAD%>"
                           size="8" maxlength="8"
                           type="text" />
                    <span class="default_spread" style="color: grey;">&nbsp;&nbsp;(<span><c:out value="${ma_spreads[channel_ical]}" /></span>)</span>
                  </td>
</c:if>
                  <td class="non_additional_info">Ca<sub>v</sub>1.2</td>
                  <td class="non_additional_info"><i>CACNA1C</i></td>
                  <td class="non_additional_info"><spring:message code="input.desc_ion_current_ICaL" /></td>
                </tr>
                <tr>
                  <td>IKs</td>
                  <td>
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_IKS_C50%>"
                           size="12" maxlength="12"
                           type="text" />
                  </td>
                  <td class="c50_units">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="additional_info">
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_IKS_HILL%>"
                           size="8" maxlength="8"
                           type="text" />
                  </td>
                  <td class="additional_info">
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_IKS_SATURATION%>"
                           size="8" maxlength="8"
                           type="text" />
                  </td>
<c:if test="${ma_show_uncertainties}">
                  <td class="additional_info">
                    <input class="spread_entry"
                           name="<%=ClientDirectIdentifiers.PARAM_NAME_IKS_SPREAD%>"
                           size="8" maxlength="8"
                           type="text" />
                    <span class="default_spread" style="color: grey;">&nbsp;&nbsp;(<span><c:out value="${ma_spreads[channel_iks]}" /></span>)</span>
                  </td>
</c:if>
                  <td class="non_additional_info">K<sub>v</sub>7.1</td>
                  <td class="non_additional_info"><i>KCNQ1</i>/<i>minK</i></td>
                  <td class="non_additional_info"><spring:message code="input.desc_ion_current_IKs" /></td>
                </tr>
                <tr>
                  <td>
                    IK1
                  </td>
                  <td>
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_IK1_C50%>"
                           size="12" maxlength="12"
                           type="text" />
                  </td>
                  <td class="c50_units">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="additional_info">
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_IK1_HILL%>"
                           size="8" maxlength="8"
                           type="text" />
                  </td>
                  <td class="additional_info">
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_IK1_SATURATION%>"
                           size="8" maxlength="8"
                           type="text" />
                  </td>
<c:if test="${ma_show_uncertainties}">
                  <td class="additional_info">
                    <input class="spread_entry"
                           name="<%=ClientDirectIdentifiers.PARAM_NAME_IK1_SPREAD%>"
                           size="8" maxlength="8"
                           type="text" />
                    <span class="default_spread" style="color: grey;">&nbsp;&nbsp;(<span><c:out value="${ma_spreads[channel_ik1]}" /></span>)</span>
                  </td>
</c:if>
                  <td class="non_additional_info">K<sub>ir</sub>2.1</td>
                  <td class="non_additional_info"><i>KCNJ2</i></td>
                  <td class="non_additional_info"><spring:message code="input.desc_ion_current_IK1" /></td>
                </tr>
                <tr>
                  <td>
                    Ito
                  </td>
                  <td>
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_ITO_C50%>"
                           size="12" maxlength="12"
                           type="text" />
                  </td>
                  <td class="c50_units">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="additional_info">
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_ITO_HILL%>"
                           size="8" maxlength="8"
                           type="text" />
                  </td>
                  <td class="additional_info">
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_ITO_SATURATION%>"
                           size="8" maxlength="8"
                           type="text" />
                  </td>
<c:if test="${ma_show_uncertainties}">
                  <td class="additional_info">
                    <input class="spread_entry"
                           name="<%=ClientDirectIdentifiers.PARAM_NAME_ITO_SPREAD%>"
                           size="8" maxlength="8"
                           type="text" />
                    <span class="default_spread" style="color: grey;">&nbsp;&nbsp;(<span><c:out value="${ma_spreads[channel_ito]}" /></span>)</span>
                  </td>
</c:if>
                  <td class="non_additional_info">K<sub>v</sub>4.3</td>
                  <td class="non_additional_info"><i>KCND3</i></td>
                  <td class="non_additional_info"><spring:message code="input.desc_ion_current_Ito" /></td>
                </tr>
                <tr>
                  <td>
                    INaL
                  </td>
                  <td>
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_INAL_C50%>"
                           size="12" maxlength="12"
                           type="text"
                           class="inal" />
                  </td>
                  <td class="c50_units">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td class="additional_info">
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_INAL_HILL%>"
                           size="8" maxlength="8"
                           type="text"
                           class="inal" />
                  </td>
                  <td class="additional_info">
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_INAL_SATURATION%>"
                           size="8" maxlength="8"
                           type="text"
                           class="inal" />
                  </td>
<c:if test="${ma_show_uncertainties}">
                  <td class="additional_info">
                    <input class="spread_entry inal"
                           name="<%=ClientDirectIdentifiers.PARAM_NAME_INAL_SPREAD%>"
                           size="8" maxlength="8"
                           type="text" />
                    <span class="default_spread" style="color: grey;">&nbsp;&nbsp;(<span><c:out value="${ma_spreads[channel_inal]}" /></span>)</span>
                  </td>
</c:if>
                  <td class="non_additional_info">Na<sub>v</sub>1.5</td>
                  <td class="non_additional_info"><i>SCN5A</i></td>
                  <td class="non_additional_info"><spring:message code="input.desc_ion_current_INaL" /></td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="4" class="input_division cs_rounded_5">
            <spring:message code="general.conc_range" /> 
            <input type="radio"
                   name="<%=ClientDirectIdentifiers.PARAM_NAME_PK_OR_CONCS%>"
                   value="<c:out value="${option_conc_range}" />"
                   checked="checked" />
            &nbsp;&nbsp;
            <span class="cs_bold cs_underline"><spring:message code="cs_general.or" /></span>
            &nbsp;&nbsp;
            <spring:message code="general.conc_points" /> 
            <input type="radio"
                   name="<%=ClientDirectIdentifiers.PARAM_NAME_PK_OR_CONCS%>"
                   value="<c:out value="${option_conc_points}" />" />
            &nbsp;&nbsp;
            <span class="cs_bold cs_underline"><spring:message code="cs_general.or" /></span>
            &nbsp;&nbsp;
            <spring:message code="cs_input.pk" />
            <input type="radio"
                   name="<%=ClientDirectIdentifiers.PARAM_NAME_PK_OR_CONCS%>"
                   value="<c:out value="${option_pk}" />" />
          </td>
        </tr>
        <tr id="input_pk" style="display: none;">
          <td colspan="4">
            <table>
              <tbody>
                <tr>
                  <td><spring:message code="cs_input.upload_pk" /></td>
                  <td>
                    <input type="file" name="<%=ClientSharedIdentifiers.PARAM_NAME_FILE_PK%>" />
                    <img src="${info_location}" class="info"
                         title="<spring:message code="cs_input.upload_pk_file_desc"
                                                arguments="${file_max_size},${pk_max_all_columns}" />" />
                  </td>
                  <td><spring:message code="cs_input.upload_pk_file" /></td>
                </tr>
            </table>
          </td>
        </tr>
        <tr id="input_conc_points" style="display: none;">
          <td colspan="4">
            <table>
              <tbody>
                <tr>
                  <td>
<c:forEach var="pointIdx" begin="1" end="${max_conc_point_count}">
                    <c:out value="${pointIdx}" />.&nbsp;
                    <input type="text"
                           name="<%=ClientDirectIdentifiers.PARAM_NAME_PLASMA_CONC_POINTS%>"
                           size="10" /> (<%= IC50_UNIT.µM %>)<br />
</c:forEach>
                  </td>
                </tr>
            </table>
          </td>
        </tr>
        <tr id="input_conc_range">
          <td colspan="4">
            <table>
              <tbody>
                <tr>
                  <td><spring:message code="cs_general.min" /></td>
                  <td>
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_PLASMA_CONC_MIN%>"
                           size="10" maxlength="10"
                           type="text"
                           value="<c:out value="${min_plasma_conc}" />" /> (<%= IC50_UNIT.µM %>)
                    <img src="${info_location}" class="info"
                         title="<spring:message code="input.min"
                                                arguments="${min_plasma_conc}" />" />
                  </td>
                  <td><spring:message code="general.conc_min" /></td>
                </tr>
                <tr>
                  <td><spring:message code="cs_general.max" /></td>
                  <td>
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_PLASMA_CONC_MAX%>"
                           size="10" maxlength="10"
                           type="text"
                           value="<c:out value="${recommended_max_plasma_conc}" />" /> (<%= IC50_UNIT.µM %>)
                    <img src="${info_location}" class="info"
                         title="<spring:message code="input.min"
                                                arguments="${min_plasma_conc}" /> "/>
                  </td>
                  <td><spring:message code="general.conc_max" /></td>
                </tr>
                <tr>
                  <td><spring:message code="general.intermediate_point_count" /></td>
                  <td>
                    <select name="<%=ClientDirectIdentifiers.PARAM_NAME_PLASMA_INTERMEDIATE_POINT_COUNT%>">
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4" selected="selected">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                    </select>
                  </td>
                  <td><spring:message code="input.desc_intermediate_point_count" /></td>
                </tr>
                <tr>
                  <td><spring:message code="general.intermediate_point_logscale" /></td>
                  <td>
                    <input name="<%=ClientDirectIdentifiers.PARAM_NAME_PLASMA_INTERMEDIATE_POINT_LOG%>"
                           type="checkbox"
                           checked="checked" />
                  </td>
                  <td><spring:message code="input.desc_intermediate_point_logscale" /></td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="4" class="input_division cs_rounded_5">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="4">
            <input name=""
                   type="submit"
                   value=" <spring:message code="input.run_simulation" /> " />
            <input type="reset"
                   value=" <spring:message code="cs_general.reset" /> " />
          </td>
        </tr>
      </tbody>
    </table>
  </form>
</div>
