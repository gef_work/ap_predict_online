<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers,
                uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers,
                uk.ac.ox.cs.nc3rs.client_direct.config.Configuration,
                uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO,
                uk.ac.ox.cs.nc3rs.client_direct.value.C50_TYPE,
                uk.ac.ox.cs.nc3rs.client_direct.value.IC50_UNIT,
                uk.ac.ox.cs.nc3rs.client_direct.value.InputOption,
                uk.ac.ox.cs.nc3rs.client_direct.value.INPUT_UNIT" %>
<c:set var="max_pacing_frequency"><%=SimulationInputVO.MAX_PACING_FREQUENCY%></c:set>
<c:set var="max_pacing_maxtime"><%=SimulationInputVO.MAX_PACING_MAX_TIME%></c:set>
<c:set var="min_pacing_frequency"><%=SimulationInputVO.MIN_PACING_FREQUENCY%></c:set>
<c:set var="min_plasma_conc"><%=Configuration.PLASMA_CONC_MIN%></c:set>
<%-- name_? vars are not escaped because the values are arguments to a subsequent spring:message --%>
<c:set var="name_pacing_frequency"><spring:message code="general.pacing_frequency" htmlEscape="false" /></c:set>
<c:set var="name_pacing_maxtime"><spring:message code="cs_simulation.maximum_pacing_time" htmlEscape="false" /></c:set>
<c:set var="name_plasma_conc_max"><spring:message code="general.conc_max" htmlEscape="false" /></c:set>
<c:set var="name_plasma_conc_min"><spring:message code="general.conc_min" htmlEscape="false" /></c:set>
<c:set var="unit_IC50_microM" value="<%=IC50_UNIT.µM%>" />
<c:set var="unit_IC50_M" value="<%=IC50_UNIT.M%>" />
<c:set var="unit_IC50_nanoM" value="<%=IC50_UNIT.nM%>" />
<c:set var="unit_pIC50" value="<%= SimulationInputVO.UNIT_MINUS_LOG_M %>" />
<c:set var="c50_type_IC50" value="<%=C50_TYPE.IC50%>" />
<c:set var="c50_type_pIC50" value="<%=C50_TYPE.pIC50%>" />
<c:set var="locale" value="${fn:toLowerCase(pageContext.response.locale)}" />
<c:set var="available_langs"><%=ClientSharedIdentifiers.I18N_LANGS%></c:set>
<c:set var="inhibitory_concentration"><spring:message code="general.inhibitory_concentration" htmlEscape="false" /></c:set>
<c:set var="hill_coefficient"><spring:message code="general.hill_coefficient" htmlEscape="false" /></c:set>
<c:set var="saturation_level"><spring:message code="general.saturation_level" htmlEscape="false" /></c:set>
<c:set var="spread"><spring:message code="general.spread" htmlEscape="false" /></c:set>
<c:set var="default_hill"><%=SimulationInputVO.DEFAULT_HILL_COEFFICIENT %></c:set>
<c:set var="default_saturation"><%=SimulationInputVO.DEFAULT_SATURATION %></c:set>
<c:set var="min_hill"><%=SimulationInputVO.MIN_HILL_COEFFICIENT %></c:set>
<c:set var="max_hill"><%=SimulationInputVO.MAX_HILL_COEFFICIENT %></c:set>
<c:set var="min_saturation"><%=SimulationInputVO.MIN_SATURATION %></c:set>
<c:set var="min_spread_gt"><%=SimulationInputVO.MIN_SPREAD_GT %></c:set>
<c:set var="max_spread"><%=SimulationInputVO.MAX_SPREAD %></c:set>
<c:set var="name_pk_or_concs"><%=ClientDirectIdentifiers.PARAM_NAME_PK_OR_CONCS %></c:set>
<c:set var="option_conc_points"><%=InputOption.CONC_POINTS %></c:set>
<c:set var="option_conc_range"><%=InputOption.CONC_RANGE %></c:set>
<c:set var="option_pk"><%=InputOption.PK %></c:set>
<c:set var="identifier_dynamic_cellml"><%=ClientSharedIdentifiers.IDENTIFIER_DYNAMIC_CELLML%></c:set>
<c:set var="annotated_cellml_upload"><spring:message code="cs_input.annotated_cellml_upload" /></c:set>
<%-- The annotated CellML upload description features html anchor tags! --%>
<c:set var="annotated_cellml_upload_desc"><spring:message code="cs_input.annotated_cellml_upload_desc" htmlEscape="false" /></c:set>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/validate/jquery.validate-1.14.0.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/validate/additional-methods.1.14.0.min.js" />"></script>
<c:if test="${fn:contains(available_langs,locale)}">
    <%-- https://github.com/jzaefferer/jquery-validation/tree/master/src/localization --%>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/validate/messages_${locale}.js" />"></script>
</c:if>
    <script type="text/javascript" src="<c:url value="/resources/js/input/input.js" />"></script>
<c:if test="${ma_show_dynamic_cellml}">
    <script type="text/javascript" src="<c:url value="/resources/js/input/input_optional.js" />"></script>
</c:if>
    <script type="text/javascript">
      /* <![CDATA[ */
      var models = [];
<c:forEach items="${ma_models}"
           var="cellml_model">
      models.push({ 'id' : <c:out value="${cellml_model.identifier}" />,
                    'name' : '<c:out value="${cellml_model.name}" />',
                    'description' : '<c:out value="${cellml_model.description}" />',
                    'cellml' : '<c:out value="${cellml_model.cellmlURL}" />',
                    'paper' : '<c:out value="${cellml_model.paperURL}" />',
                    'defaultModel' : <c:out value="${cellml_model.defaultModel}" /> });
      </c:forEach>
<c:if test="${ma_show_dynamic_cellml}">
      <%-- The annotated CellML upload description features html anchor tags! --%>
      models.push({ 'id' : <c:out value="${identifier_dynamic_cellml}" />,
                    'name' : '<c:out value="${annotated_cellml_upload}" />',
                    'description' : '${annotated_cellml_upload_desc}',
                    'cellml' : 'n/a',
                    'paper' : 'n/a',
                    'defaultModel' : false });
</c:if>
      var PARAM_NAME_PK_OR_CONCS = '<c:out value="${name_pk_or_concs}" />';
      var OPTION_CONC_POINTS = '<c:out value="${option_conc_points}" />';
      var OPTION_CONC_RANGE = '<c:out value="${option_conc_range}" />';
      var OPTION_PK = '<c:out value="${option_pk}" />';
      var c50_type_IC50 = '<c:out value="${c50_type_IC50}" />';
      var c50_type_pIC50 = '<c:out value="${c50_type_pIC50}" />';
      var unit_IC50_microM = '<c:out value="${unit_IC50_microM}" />';
      var unit_IC50_M = '<c:out value="${unit_IC50_M}" />';
      var unit_IC50_nanoM = '<c:out value="${unit_IC50_nanoM}" />';
      var unit_pIC50 = '<c:out value="${unit_pIC50}" />';

      var allow_inal = ['6','8'];
      <c:if test="${ma_show_dynamic_cellml}">
        allow_inal.push('<c:out value="${identifier_dynamic_cellml}" />');
      </c:if>

      jQuery(document).ready(
        function() {
          /* Models and their display */
          var column_models = jQuery('#models');
          var column_model_desc = jQuery('#model_desc');
          var el_model_selector = jQuery('<select />').attr({ 'id': 'model',
                                                              'name': '<%=ClientDirectIdentifiers.PARAM_NAME_MODEL_IDENTIFIER%>'});
          var model_descriptions = {};
<c:if test="${ma_show_dynamic_cellml}">
  <c:choose>
    <c:when test="${empty ma_input_values}">
          <%-- No input values - hide the CellML upload display --%>
          show_hide_dynamic_upload(false);
    </c:when>
    <c:otherwise>
      <c:choose>
        <c:when test="${empty ma_input_values.modelIdentifier}">
          <%-- Input values - no model identifier indicating previous regular model use --%>
          show_hide_dynamic_upload(true);
        </c:when>
        <c:when test="${ma_input_values.modelIdentifier == identifier_dynamic_cellml}">
          <%-- Input values - model identifier is dynamic cellml value --%>
          show_hide_dynamic_upload(true);
        </c:when>
        <c:otherwise>
          <%-- Input values - model identifier is regular model --%>
          show_hide_dynamic_upload(false);
        </c:otherwise>
      </c:choose>
    </c:otherwise>
  </c:choose>
</c:if>
          jQuery.each(models, function(index, value) {
            var model = value;
            var model_id = model.id;
            model_descriptions[model_id + ''] = model.description;
            var option = jQuery('<option />').attr({ 'value' : model_id })
                                             .html(model.name);
<c:choose>
  <c:when test="${empty ma_input_values}">
            <%-- No input values, show the default model --%>
            if (model.defaultModel) {
              option.attr('selected', 'selected');
              column_model_desc.html(model.description);
              show_inal(model_id);
            }
  </c:when>
  <c:otherwise>
            <%-- Input values containing model identifier, so highlight model --%>
            if (model_id == '<c:out value="${ma_input_values.modelIdentifier}" />') {
              option.attr('selected', 'selected');
              column_model_desc.html(model.description);
              show_inal(model_id);
            }
    <c:if test="${ma_show_dynamic_cellml && empty ma_input_values.modelIdentifier}">
            <%-- Input values not containing model identifier, so highlight dynamic cellml --%>
            if (model_id == <c:out value="${identifier_dynamic_cellml}" />) {
              option.attr('selected', 'selected');
              column_model_desc.html(model.description);
              show_inal(model_id);
            }
    </c:if>
  </c:otherwise>
</c:choose>
            el_model_selector.append(option);
          });
          column_models.append(el_model_selector);

          var details_button = jQuery('.details_button');
          details_button.css( { 'font-weight' : 'bold' } );
          details_button.click(function(event) {
            jQuery('.additional_info').toggle();
            jQuery('.non_additional_info').toggle();
            event.preventDefault();
          });

          var el_select_IC50_units = jQuery('#ic50_units');
          var el_select_C50_type = jQuery('#c50_type');
          var el_IC50_span = jQuery('#ic50_span');
          var els_C50_units = jQuery('.c50_units');

<c:choose>
  <c:when test="${empty ma_input_values}">
          /* Default values */
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_PACING_FREQUENCY%>]').val('1.0');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_PACING_MAX_TIME%>]').val('5');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_ICAL_HILL%>]').val('${default_hill}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_ICAL_SATURATION%>]').val('${default_saturation}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_IK1_HILL%>]').val('${default_hill}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_IK1_SATURATION%>]').val('${default_saturation}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_IKR_HILL%>]').val('${default_hill}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_IKR_SATURATION%>]').val('${default_saturation}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_IKS_HILL%>]').val('${default_hill}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_IKS_SATURATION%>]').val('${default_saturation}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_ITO_HILL%>]').val('${default_hill}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_ITO_SATURATION%>]').val('${default_saturation}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_INA_HILL%>]').val('${default_hill}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_INA_SATURATION%>]').val('${default_saturation}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_INAL_HILL%>]').val('${default_hill}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_INAL_SATURATION%>]').val('${default_saturation}');

          <%-- Display the configured default (p)IC50 units - ordered using LinkedHashMap in 
               site context configuration file, e.g. appCtx.config.site.xml.
               The first of the ordered items is the default. --%>
          var c50_units = {};
    <c:forEach items="${ma_c50units}" var="c50_unit" >
          c50_units['<c:out value="${c50_unit.key}" />'] = '<c:out value="${c50_unit.value}" />';
    </c:forEach>
          <%-- TODO : If pIC50 is the configured default, set_c50_units hardcodes the default IC50
                      units as µM, which may not be the preferred IC50 units! --%>
          set_c50_units(c50_units, c50_type_pIC50, c50_type_IC50, unit_pIC50, unit_IC50_microM,
                        unit_IC50_M, unit_IC50_nanoM, el_select_C50_type, el_select_IC50_units,
                        els_C50_units, el_IC50_span);

          <%-- Default is to hide the additional (Hill/Saturation) details --%>
          jQuery('.additional_info').hide();
  </c:when>
  <c:otherwise>
          /* Selected values */
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_PACING_FREQUENCY%>]').val('${ma_input_values.pacingFrequency}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_PACING_MAX_TIME%>]').val('${ma_input_values.pacingMaxTime}');

          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_IKR_C50%>]').val('${ma_input_values.IKrObservations.c50}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_IKR_HILL%>]').val('${ma_input_values.IKrObservations.hill}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_IKR_SATURATION%>]').val('${ma_input_values.IKrObservations.saturation}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_ICAL_C50%>]').val('${ma_input_values.ICaLObservations.c50}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_ICAL_HILL%>]').val('${ma_input_values.ICaLObservations.hill}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_ICAL_SATURATION%>]').val('${ma_input_values.ICaLObservations.saturation}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_IK1_C50%>]').val('${ma_input_values.IK1Observations.c50}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_IK1_HILL%>]').val('${ma_input_values.IK1Observations.hill}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_IK1_SATURATION%>]').val('${ma_input_values.IK1Observations.saturation}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_IKS_C50%>]').val('${ma_input_values.IKsObservations.c50}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_IKS_HILL%>]').val('${ma_input_values.IKsObservations.hill}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_IKS_SATURATION%>]').val('${ma_input_values.IKsObservations.saturation}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_ITO_C50%>]').val('${ma_input_values.itoObservations.c50}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_ITO_HILL%>]').val('${ma_input_values.itoObservations.hill}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_ITO_SATURATION%>]').val('${ma_input_values.itoObservations.saturation}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_INA_C50%>]').val('${ma_input_values.INaObservations.c50}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_INA_HILL%>]').val('${ma_input_values.INaObservations.hill}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_INA_SATURATION%>]').val('${ma_input_values.INaObservations.saturation}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_INAL_C50%>]').val('${ma_input_values.INaLObservations.c50}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_INAL_HILL%>]').val('${ma_input_values.INaLObservations.hill}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_INAL_SATURATION%>]').val('${ma_input_values.INaLObservations.saturation}');
    <c:if test="${ma_show_uncertainties}">
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_IKR_SPREAD%>]').val('${ma_input_values.IKrObservations.spread}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_INA_SPREAD%>]').val('${ma_input_values.INaObservations.spread}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_ICAL_SPREAD%>]').val('${ma_input_values.ICaLObservations.spread}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_IKS_SPREAD%>]').val('${ma_input_values.IKsObservations.spread}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_IK1_SPREAD%>]').val('${ma_input_values.IK1Observations.spread}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_ITO_SPREAD%>]').val('${ma_input_values.itoObservations.spread}');
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_INAL_SPREAD%>]').val('${ma_input_values.INaLObservations.spread}');
    </c:if>

    <c:if test="${not empty ma_input_values.plasmaConcMax}">
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_PLASMA_CONC_MAX%>]').val('${ma_input_values.plasmaConcMax}');
    </c:if>
    <c:if test="${not empty ma_input_values.plasmaConcMin}">
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_PLASMA_CONC_MIN%>]').val('${ma_input_values.plasmaConcMin}');
    </c:if>
          <%-- Show the C50 type and units according to input values --%>
          el_select_C50_type.find('option[value=<c:out value="${ma_input_values.c50Type}" />]').attr('selected', 'selected');
          if (is_IC50_type_selected(el_select_C50_type, c50_type_IC50)) {
            el_select_IC50_units.find('option[value=<c:out value="${ma_input_values.ic50Units}" />]').attr('selected', 'selected');
            el_IC50_span.show();
            display_IC50_unit(el_select_IC50_units, els_C50_units);
          } else {
            els_C50_units.text(unit_pIC50);
          }
    <c:if test="${not empty ma_input_values.plasmaIntPtCount}">
          jQuery('select[name=<%=ClientDirectIdentifiers.PARAM_NAME_PLASMA_INTERMEDIATE_POINT_COUNT%>]').
                         find('option[value=<c:out value="${ma_input_values.plasmaIntPtCount}" />]').
                         attr('selected', 'selected');
    </c:if>
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_PLASMA_INTERMEDIATE_POINT_LOG%>]').removeAttr('checked');
    <c:if test="${ma_input_values.plasmaIntPtLogScale}">
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_PLASMA_INTERMEDIATE_POINT_LOG%>]').attr('checked', 'checked');
    </c:if>
    <c:forEach items="${ma_input_values.plasmaConcPoints}" var="plasma_conc_point"
               varStatus="loop">
          jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_PLASMA_CONC_POINTS%>]:eq(<c:out value="${loop.index}" />)').val('<c:out value="${plasma_conc_point}" />');
    </c:forEach>
    <%-- Show the relevant pk/points/range data --%>
    <c:choose>
      <c:when test="${not empty ma_input_values.pkFile}">
          show_pk();
          jQuery('input[type=radio][name=<c:out value="${name_pk_or_concs}" />][value=<c:out value="${option_pk}" />]').attr('checked', 'checked');
      </c:when>
      <c:when test="${not empty ma_input_values.plasmaConcMax}">
          show_conc_range();
          jQuery('input[type=radio][name=<c:out value="${name_pk_or_concs}" />][value=<c:out value="${option_conc_range}" />]').attr('checked', 'checked');
      </c:when>
      <c:when test="${not empty ma_input_values.plasmaConcPoints}">
          show_conc_points();
          jQuery('input[type=radio][name=<c:out value="${name_pk_or_concs}" />][value=<c:out value="${option_conc_points}" />]').attr('checked', 'checked');
      </c:when>
    </c:choose>

    <c:choose>
      <c:when test="${ma_input_values.nonC50Value}">
            <%-- If there are Hill and/or Saturation values then show the additional details --%>
            jQuery('.additional_info').show();
            jQuery('.non_additional_info').hide();
      </c:when>
      <c:otherwise>
            <%-- No Hill and/or Saturation values, so hide the additional details --%>
            jQuery('.additional_info').hide();
      </c:otherwise>
    </c:choose>
  </c:otherwise>
</c:choose>
          /* Listen out for the C50 type changing */
          el_select_C50_type.change(function() {
            if (is_IC50_type_selected(el_select_C50_type, c50_type_IC50)) {
              el_IC50_span.show();
              display_IC50_unit(el_select_IC50_units, els_C50_units);
            } else {
              el_IC50_span.hide();
              els_C50_units.text(unit_pIC50);
            }
          });

          /* Listen for the Model changing. */
          el_model_selector.change(function() {
            var model_id = jQuery(this).find(':selected').attr('value');
            column_model_desc.html(model_descriptions[model_id + '']);
            /* The O'Hara variants */
<c:if test="${ma_show_dynamic_cellml}">
            var selected_upload_option = (model_id == <c:out value="${identifier_dynamic_cellml}" />);
            show_hide_dynamic_upload(selected_upload_option);
</c:if>
            show_inal(model_id);

<c:if test="${ma_show_uncertainties}">
            /* Depending on chosen model, show_inal may have enabled or disabled all in the INaL row */
            var spread_checkbox = jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_SPREADS_ENABLED%>]');  
            if (!spread_checkbox.is(':checked')) {
              /* If show_inal blanket-enabled the INaL row inputs, disable spread if box not ticked */
              spread_entries.attr('disabled', 'disabled');
            }
</c:if>
          });

          el_select_IC50_units.change(function() {
            display_IC50_unit(jQuery(this), els_C50_units);
          });

          /* Plasma concentrations */
          jQuery('#tabs').tabs();

          jQuery('#model_dialog_div').dialog({
            autoOpen: false,
            show: {
              effect: 'blind',
              duration: 100
            },
            title: '<spring:message code="input.all_model_details" />',
            hide: {},
            width: 700,
            position: {
              my: 'left top',
              at: 'left bottom',
              of: jQuery('#model_dialog_button')
            }
          });

          /* Model descriptions dialog */
          jQuery('#model_dialog_button').click(function(event) {
            jQuery('#model_dialog_div').dialog('open');
            event.preventDefault();
          });

<c:if test="${ma_show_uncertainties}">
          /* Adjust spread entries according to checkbox. */
          var spread_checkbox = jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_SPREADS_ENABLED%>]');
          var spread_entries = jQuery('.spread_entry');

          /* Set everything to default/empty settings. */
          spread_checkbox.removeAttr('checked');
          spread_entries.attr('disabled', 'disabled');
  <c:if test="${empty ma_input_values}">
          spread_entries.val('');
  </c:if>
          /* Toggle enable/disable spread calculations. */
          spread_checkbox.change(function() {
            if (this.checked) {
              /* Make spreads enabled */
              spread_entries.removeAttr('disabled');
              var inal_disabled = jQuery('input[name=<%=ClientDirectIdentifiers.PARAM_NAME_INAL_C50%>]').is(':disabled');
              if (inal_disabled) {
                /* INaL is special case - if it's disabled then keep it that way */
                jQuery('.inal').attr('disabled', 'disabled');
              }
            } else {
              /* Make spreads disabled */
              spread_entries.attr('disabled', 'disabled');
            }
          });

          jQuery('.default_spread').click(function(event) {
            var span_clicked = jQuery(this);

            /* Find the text within the child span (i.e. the default value) */
            var default_spread = span_clicked.children('span').text();
 
            if (default_spread != 'n/a') {
              var input_span = span_clicked.siblings('input');
              input_span.val(default_spread);
            }
            event.preventDefault();
          });
</c:if>

          jQuery.validator.addMethod('saturation_100', function(value, element) {
            return this.optional(element) || value != 100;
          }, '<spring:message code="input.saturation_100_error" />');

          /* Add a method which ensures that a 50% inhibitory concentration is provided
             if a Hill, saturation or spread value is specified. */
          jQuery.validator.addMethod('ensure_ic', function(value, element) {
            /* Input element name e.g. ik1Saturation, itoHill, etc.. */
            var el_name = jQuery(element).attr('name');
            var replace_this = '';
            var default_value;
            if (el_name.indexOf('Hill') > -1) {
              replace_this = 'Hill';
              default_value = 1;
            } else if (el_name.indexOf('Saturation') > -1) {
              replace_this = 'Saturation';
              default_value = 0;
            } else if (el_name.indexOf('Spread') > -1) {
              replace_this = 'Spread';
            }
            if (replace_this == '') {
              return true;
            }
            /* Isolate the ion channel name, e.g. ik1, ical */
            var channel = el_name.replace(replace_this, '');
            /* Indicate if an non-null, non-default value has been assigned */
            var this_has_value = (value != '' && value != default_value);
            /* Verify if the corresponding ion channel has an inhibitory concentration supplied. */ 
            var ic_has_value = jQuery('input[name=' + channel + 'C50]').val() != '';

            if (this_has_value && !ic_has_value) {
              return this.optional(element) || false;
            } else {
              return this.optional(element) || true;
            }
          }, '<spring:message code="input.ensure_ic" />');

          jQuery.validator.addMethod('gt_than', function(value, element, params) {
            return this.optional(element) || parseFloat(value) > params;
          }, jQuery.validator.format('<spring:message code="input.value_gt" />'));

          var simulation_run_form = jQuery('#simulation_run_form');
          simulation_run_form.validate({
            rules: {
              '<%=ClientDirectIdentifiers.PARAM_NAME_PACING_FREQUENCY%>' : {
                required: true,
                number: true,
                max: ${max_pacing_frequency},
                min: ${min_pacing_frequency}
              },
              '<%=ClientDirectIdentifiers.PARAM_NAME_PACING_MAX_TIME%>' : {
                required: true,
                number: true,
                max: ${max_pacing_maxtime}
              },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IKR_C50%>' : { number: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_ICAL_C50%>' : { number: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IK1_C50%>' : { number: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IKS_C50%>' : { number: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_ITO_C50%>' : { number: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_INA_C50%>' : { number: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_INAL_C50%>' : { number: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IKR_HILL%>' : { number: true, max: ${max_hill},
                                                                     min: ${min_hill}, ensure_ic: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_ICAL_HILL%>' : { number: true, max: ${max_hill},
                                                                      min: ${min_hill}, ensure_ic: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IK1_HILL%>' : { number: true, max: ${max_hill},
                                                                     min: ${min_hill}, ensure_ic: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IKS_HILL%>' : { number: true, max: ${max_hill},
                                                                     min: ${min_hill}, ensure_ic: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_ITO_HILL%>' : { number: true, max: ${max_hill},
                                                                     min: ${min_hill}, ensure_ic: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_INA_HILL%>' : { number: true, max: ${max_hill},
                                                                     min: ${min_hill}, ensure_ic: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_INAL_HILL%>' : { number: true, max: ${max_hill},
                                                                     min: ${min_hill}, ensure_ic: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IKR_SATURATION%>' : { number: true, min: ${min_saturation},
                                                                           saturation_100: true, ensure_ic: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_ICAL_SATURATION%>' : { number: true, min: ${min_saturation},
                                                                            saturation_100: true, ensure_ic: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IK1_SATURATION%>' : { number: true, min: ${min_saturation},
                                                                           saturation_100: true, ensure_ic: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IKS_SATURATION%>' : { number: true, min: ${min_saturation},
                                                                           saturation_100: true, ensure_ic: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_ITO_SATURATION%>' : { number: true, min: ${min_saturation},
                                                                           saturation_100: true, ensure_ic: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_INA_SATURATION%>' : { number: true, min: ${min_saturation},
                                                                           saturation_100: true, ensure_ic: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_INAL_SATURATION%>' : { number: true, min: ${min_saturation},
                                                                           saturation_100: true, ensure_ic: true },
<c:if test="${ma_show_uncertainties}">
              '<%=ClientDirectIdentifiers.PARAM_NAME_IKR_SPREAD%>' : { number: true, gt_than: ${min_spread_gt},
                                                                       max: ${max_spread}, ensure_ic: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_ICAL_SPREAD%>' : { number: true, gt_than: ${min_spread_gt},
                                                                       max: ${max_spread}, ensure_ic: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IKS_SPREAD%>' : { number: true, gt_than: ${min_spread_gt},
                                                                       max: ${max_spread}, ensure_ic: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_INA_SPREAD%>' : { number: true, gt_than: ${min_spread_gt},
                                                                       max: ${max_spread}, ensure_ic: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_ITO_SPREAD%>' : { number: true, gt_than: ${min_spread_gt},
                                                                       max: ${max_spread}, ensure_ic: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IK1_SPREAD%>' : { number: true, gt_than: ${min_spread_gt},
                                                                       max: ${max_spread}, ensure_ic: true },
              '<%=ClientDirectIdentifiers.PARAM_NAME_INAL_SPREAD%>' : { number: true, gt_than: ${min_spread_gt},
                                                                        max: ${max_spread}, ensure_ic: true },
</c:if>
              '<%=ClientDirectIdentifiers.PARAM_NAME_PLASMA_CONC_MIN%>' : {
                number: true,
                min: ${min_plasma_conc}
              },
              '<%=ClientDirectIdentifiers.PARAM_NAME_PLASMA_CONC_MAX%>' : {
                number: true,
                min: ${min_plasma_conc}
              }
            },
            messages: {
              '<%=ClientDirectIdentifiers.PARAM_NAME_PACING_FREQUENCY%>' : {
                required: '<spring:message code="cs_input.form_value_required" arguments="${name_pacing_frequency}" />',
                number: '<spring:message code="input.form_numeric_only" arguments="${name_pacing_frequency}," />',
                max: '<spring:message code="input.form_max_value"
                                      arguments="${name_pacing_frequency}, ${max_pacing_frequency}" />',
                min: '<spring:message code="input.form_min_value"
                                      arguments="${name_pacing_frequency}, ${min_pacing_frequency}" />'
              },
              '<%=ClientDirectIdentifiers.PARAM_NAME_PACING_MAX_TIME%>' : {
                required: '<spring:message code="cs_input.form_value_required" arguments="${name_pacing_maxtime}" />',
                number: '<spring:message code="input.form_numeric_only" arguments="${name_pacing_maxtime}," />',
                max: '<spring:message code="input.form_max_value"
                                        arguments="${name_pacing_maxtime}, ${max_pacing_maxtime}" />'
              },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IKR_C50%>' : { 
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="IKr,${inhibitory_concentration}" />'},
              '<%=ClientDirectIdentifiers.PARAM_NAME_ICAL_C50%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="ICaL,${inhibitory_concentration}" />'},
              '<%=ClientDirectIdentifiers.PARAM_NAME_IK1_C50%>' : { 
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="IK1,${inhibitory_concentration}" />'},
              '<%=ClientDirectIdentifiers.PARAM_NAME_IKS_C50%>' : { 
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="IKs,${inhibitory_concentration}" />'},
              '<%=ClientDirectIdentifiers.PARAM_NAME_ITO_C50%>' : { 
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="Ito,${inhibitory_concentration}" />'},
              '<%=ClientDirectIdentifiers.PARAM_NAME_INA_C50%>' : { 
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="INa,${inhibitory_concentration}" />'},
              '<%=ClientDirectIdentifiers.PARAM_NAME_INAL_C50%>' : { 
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="INaL,${inhibitory_concentration}" />'},
              '<%=ClientDirectIdentifiers.PARAM_NAME_IKR_HILL%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="IKr,${hill_coefficient}" />',
                max: '<spring:message code="input.form_max_value"
                                      arguments="IKr ${hill_coefficient}, ${max_hill}" />',
                min: '<spring:message code="input.form_min_value"
                                      arguments="IKr ${hill_coefficient}, ${min_hill}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_ICAL_HILL%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="ICaL,${hill_coefficient}" />',
                max: '<spring:message code="input.form_max_value"
                                      arguments="ICaL ${hill_coefficient}, ${max_hill}" />',
                min: '<spring:message code="input.form_min_value"
                                      arguments="ICaL ${hill_coefficient}, ${min_hill}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IK1_HILL%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="IK1,${hill_coefficient}" />',
                max: '<spring:message code="input.form_max_value"
                                      arguments="IK1 ${hill_coefficient}, ${max_hill}" />',
                min: '<spring:message code="input.form_min_value"
                                      arguments="IK1 ${hill_coefficient}, ${min_hill}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IKS_HILL%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="IKs,${hill_coefficient}" />',
                max: '<spring:message code="input.form_max_value"
                                      arguments="IKs ${hill_coefficient}, ${max_hill}" />',
                min: '<spring:message code="input.form_min_value"
                                      arguments="IKs ${hill_coefficient}, ${min_hill}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_ITO_HILL%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="Ito,${hill_coefficient}" />',
                max: '<spring:message code="input.form_max_value"
                                      arguments="Ito ${hill_coefficient}, ${max_hill}" />',
                min: '<spring:message code="input.form_min_value"
                                      arguments="Ito ${hill_coefficient}, ${min_hill}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_INA_HILL%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="INa,${hill_coefficient}" />',
                max: '<spring:message code="input.form_max_value"
                                      arguments="INa ${hill_coefficient}, ${max_hill}" />',
                min: '<spring:message code="input.form_min_value"
                                      arguments="INa ${hill_coefficient}, ${min_hill}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_INAL_HILL%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="INaL,${hill_coefficient}" />',
                max: '<spring:message code="input.form_max_value"
                                      arguments="INaL ${hill_coefficient}, ${max_hill}" />',
                min: '<spring:message code="input.form_min_value"
                                      arguments="INaL ${hill_coefficient}, ${min_hill}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IKR_SATURATION%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="IKr,${saturation_level}" />',
                min: '<spring:message code="input.form_min_value"
                                      arguments="IKr ${saturation_level}, ${min_saturation}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_ICAL_SATURATION%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="ICaL,${saturation_level}" />',
                min: '<spring:message code="input.form_min_value" 
                                      arguments="ICaL ${saturation_level}, ${min_saturation}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IK1_SATURATION%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="IK1,${saturation_level}" />',
                min: '<spring:message code="input.form_min_value"
                                      arguments="IK1 ${saturation_level}, ${min_saturation}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IKS_SATURATION%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="IKs,${saturation_level}" />',
                min: '<spring:message code="input.form_min_value"
                                      arguments="IKs ${saturation_level}, ${min_saturation}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_ITO_SATURATION%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="Ito,${saturation_level}" />',
                min: '<spring:message code="input.form_min_value"
                                      arguments="Ito ${saturation_level}, ${min_saturation}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_INA_SATURATION%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="INa,${saturation_level}" />',
                min: '<spring:message code="input.form_min_value"
                                      arguments="INa ${saturation_level}, ${min_saturation}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_INAL_SATURATION%>' : {
                  number: '<spring:message code="input.form_numeric_only"
                                           arguments="INaL,${saturation_level}" />',
                  min: '<spring:message code="input.form_min_value"
                                        arguments="INaL ${saturation_level}, ${min_saturation}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IKR_SPREAD%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="IKr,${spread}" />',
                max: '<spring:message code="input.form_max_value"
                                      arguments="IKr ${spread}, ${max_spread}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_ICAL_SPREAD%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="ICaL,${spread}" />',
                max: '<spring:message code="input.form_max_value" 
                                      arguments="ICaL ${spread}, ${max_spread}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IKS_SPREAD%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="IKs,${spread}" />',
                max: '<spring:message code="input.form_max_value"
                                      arguments="IKs ${spread}, ${max_spread}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_INA_SPREAD%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="INa,${spread}" />',
                max: '<spring:message code="input.form_max_value"
                                      arguments="INa ${spread}, ${max_spread}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_ITO_SPREAD%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="Ito,${spread}" />',
                min: '<spring:message code="input.form_max_value"
                                      arguments="Ito ${spread}, ${max_spread}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_IK1_SPREAD%>' : {
                 number: '<spring:message code="input.form_numeric_only"
                                          arguments="IK1,${spread}" />',
                 min: '<spring:message code="input.form_max_value"
                                       arguments="IK1 ${spread}, ${max_spread}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_INAL_SPREAD%>' : {
                 number: '<spring:message code="input.form_numeric_only"
                                          arguments="INaL,${spread}" />',
                 min: '<spring:message code="input.form_max_value"
                                       arguments="INaL ${spread}, ${max_spread}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_PLASMA_CONC_MIN%>' : {
                  number: '<spring:message code="input.form_numeric_only"
                                           arguments="${name_plasma_conc_min}," />',
                  min: '<spring:message code="input.form_min_value"
                                        arguments="${name_plasma_conc_min}, ${min_plasma_conc}" />' },
              '<%=ClientDirectIdentifiers.PARAM_NAME_PLASMA_CONC_MAX%>' : {
                number: '<spring:message code="input.form_numeric_only"
                                         arguments="${name_plasma_conc_max}," />',
                min: '<spring:message code="input.form_min_value"
                                      arguments="${name_plasma_conc_max}, ${min_plasma_conc}" />'
              }
            }
          });
        }
      );
      /* ]]> */
    </script>