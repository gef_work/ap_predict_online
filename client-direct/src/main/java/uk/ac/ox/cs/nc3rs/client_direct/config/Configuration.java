/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.config;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.value.IonCurrent;
import uk.ac.ox.cs.nc3rs.client_direct.value.ModelVO;

/**
 * Client configuration options.
 *
 * @author geoff
 */
// Values declared in config/appCtx.config.xml and config/appCtx.config.{site,cellModels.site}.xml
@Component(ClientDirectIdentifiers.COMPONENT_CONFIGURATION)
public class Configuration {

  private static final BigDecimal illegalMaxPctileVal = new BigDecimal("100");

  /** System-defined default max. plasma concentration value. */
  public static BigDecimal RECOMMENDED_PLASMA_CONC_MAX = new BigDecimal("100");
  /** System-defined default min. plasma concentration value. */
  public static BigDecimal PLASMA_CONC_MIN = BigDecimal.ZERO;

  private final List<ModelVO> models = new ArrayList<ModelVO>();
  private final Map<Short, String> modelNames = new HashMap<Short, String>();
  private final Map<IonCurrent, BigDecimal> spreads = new HashMap<IonCurrent, BigDecimal>();
  private final Set<BigDecimal> credibleIntervalPctiles = new TreeSet<BigDecimal>();

  // Note: It's a linked HashMap to retain order
  private final Map<String, String> c50Units = new LinkedHashMap<String, String>();

  private static final Log log = LogFactory.getLog(Configuration.class);

  /**
   * Initialising constructor.
   * 
   * @param recommendedPlasmaConcMax Recommended maximum plasma concentration.
   * @param plasmaConcMin Minimum plasma concentration.
   * @param spreads Ion current spreads.
   * @param credibleIntervalPctiles Default credible interval percentiles.
   * @throws IllegalArgumentException If an invalid argument is passed.
   */
  public Configuration(final String recommendedPlasmaConcMax,
                       final String plasmaConcMin,
                       final Map<IonCurrent, String> spreads,
                       final Set<BigDecimal> credibleIntervalPctiles)
                       throws IllegalArgumentException {
    log.debug("~Configuration() : Invoked.");

    if (recommendedPlasmaConcMax == null || plasmaConcMin == null) {
      throw new IllegalArgumentException("A recommended maximum and minimum plasma concentration must be configured");
    }
    BigDecimal provisionalRecommendedPlasmaConcMax;
    BigDecimal provisionalPlasmaConcMin;
    try {
      provisionalRecommendedPlasmaConcMax = new BigDecimal(recommendedPlasmaConcMax);
      provisionalPlasmaConcMin = new BigDecimal(plasmaConcMin);
    } catch (NumberFormatException e) {
      throw new IllegalArgumentException("A numeric value is required for recommended maximum and minimum plasma concentration");
    }
    if (provisionalRecommendedPlasmaConcMax.compareTo(BigDecimal.ZERO) <= 0) {
      throw new IllegalArgumentException("A positive recommended maximum plasma concentration must be configured");
    }
    if (provisionalPlasmaConcMin.compareTo(provisionalRecommendedPlasmaConcMax) >= 0) {
      throw new IllegalArgumentException("A minimum plasma concentation must be less than the maximum");
    }

    RECOMMENDED_PLASMA_CONC_MAX = provisionalRecommendedPlasmaConcMax;
    PLASMA_CONC_MIN = provisionalPlasmaConcMin;

    if (spreads != null && !spreads.isEmpty()) {
      if (credibleIntervalPctiles == null || credibleIntervalPctiles.isEmpty()) {
        throw new IllegalArgumentException("Credible interval percentiles must be specified if spread data is provided");
      }

      for (final Map.Entry<IonCurrent, String> spreadEntry : spreads.entrySet()) {
        final IonCurrent ionCurrent = spreadEntry.getKey();
        final String spreadValue = spreadEntry.getValue();
        if (ionCurrent != null) {
          BigDecimal spread = null;
          if (!StringUtils.isBlank(spreadValue)) {
            try {
              spread = new BigDecimal(spreadValue);
            } catch (NumberFormatException e) {}
            log.info("~Configuration() : Channel '" + ionCurrent + "' - Spread '" + spread + "'.");
          }
          this.spreads.put(ionCurrent, spread);
        }
      }

      for (final BigDecimal credibleIntervalPctile : credibleIntervalPctiles) {
        if (credibleIntervalPctile == null ||
            credibleIntervalPctile.compareTo(BigDecimal.ZERO) <= 0 ||
            credibleIntervalPctile.compareTo(illegalMaxPctileVal) >= 0) {
          throw new IllegalArgumentException("A credible interval percentile value of '" + credibleIntervalPctile + "' is not valid!");
        } else {
          this.credibleIntervalPctiles.add(credibleIntervalPctile);
        }
      }
    }
  }

  @PostConstruct
  private void postConstruct() {

    for (final Map.Entry<String, String> c50UnitEntry : c50Units.entrySet()) {
      log.info("~postConstruct() : C50 key '" + c50UnitEntry.getKey() + "', unit '" + c50UnitEntry.getValue() + "'.");
    }

    Collections.sort(models);

    for (final ModelVO model : this.models) {
      final String modelName = model.getName();
      final short modelIdentifier = model.getIdentifier();

      log.info("~postConstruct() : Model name '" + modelName + "', identifier '" + modelIdentifier + "'.");
      modelNames.put(new Short(modelIdentifier), modelName);
    }

    log.info("~postConstruct() : Recommended maximum plasma concentration '" + RECOMMENDED_PLASMA_CONC_MAX + "'.");
    log.info("~postConstruct() : Minimum plasma concentration '" + PLASMA_CONC_MIN + "'.");
  }

  /**
   * Assign the C50 units.
   * 
   * @param c50Units Context-defined C50 units.
   * @throws IllegalArgumentException If reassigning or configured C50 units is {@code null} or
   *                                  empty.
   */
  // see Tips section @ http://docs.spring.io/spring/docs/3.0.x/spring-framework-reference/html/beans.html#beans-autowired-annotation-qualifiers
  @Resource(name="c50Units")
  public void setC50Units(final Map<String, String> c50Units) throws IllegalArgumentException {
    if (!this.c50Units.isEmpty() || c50Units == null || c50Units.isEmpty()) {
      throw new IllegalArgumentException("At least one C50 unit must be configured");
    }

    this.c50Units.putAll(c50Units);
  }

  /**
   * Retrieve the configured C50 unit options.
   * 
   * @return (Unmodifiable) configured C50 unit options.
   */
  public Map<String, String> getC50Units() {
    return Collections.unmodifiableMap(c50Units);
  }

  /**
   * Retrieve configured collections of model identifiers and their names.
   * 
   * @return Unmodifiable collection of model identifiers and their names.
   */
  public Map<Short, String> getModelNames() {
    log.debug("~retrieveModelNames() : Invoked.");

    return Collections.unmodifiableMap(modelNames);
  }

  /**
   * Retrieved configured models.
   * 
   * @return Unmodifiable collection of models.
   */
  public List<ModelVO> getModels() {
    log.debug("~getModels() : Invoked.");

    return Collections.unmodifiableList(models);
  }

  /**
   * Retrieve the spreads associated with ion channels.
   * 
   * @return Ion channel spread values.
   */
  public Map<IonCurrent, BigDecimal> getSpreads() {
    return Collections.unmodifiableMap(spreads);
  }

  /**
   * Retrieve the credible interval percentiles.
   * 
   * @return Unmodifiable collection of credible interval percentiles, or empty
   *         collection if no spread data configured.
   */
  public Set<BigDecimal> getCredibleIntervalPctiles() {
    return Collections.unmodifiableSet(credibleIntervalPctiles);
  }

  /**
   * Assign the models to use.
   * 
   * @param configuredModels Models to use.
   * @throws IllegalArgumentException If reassigning or configured model 
   *                                  collection is {@code null} or empty.
   */
  @Autowired(required=true)
  protected void setModels(final Set<ModelVO> configuredModels)
                           throws IllegalArgumentException {
    if (!models.isEmpty() || configuredModels == null ||
        configuredModels.isEmpty()) {
      throw new IllegalArgumentException("At least one Model must be configured in the application");
    }

    models.addAll(configuredModels);
  }
}