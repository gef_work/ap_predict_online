/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the names of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller;

import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.controller.AbstractMessageSourceAwareController;
import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.compbio.client_shared.value.PortalFeature;
import uk.ac.ox.cs.compbio.client_shared.value.file.FILE_DATA_TYPE;
import uk.ac.ox.cs.compbio.client_shared.value.file.FileStoreActionOutcomeVO;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.controller.util.ControllerUtil;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation;
import uk.ac.ox.cs.nc3rs.client_direct.entity.SimulationInput;
import uk.ac.ox.cs.nc3rs.client_direct.exception.AppManagerWSInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.NoConnectionException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.RunInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService;
import uk.ac.ox.cs.nc3rs.client_direct.value.InputOption;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;

/**
 * MVC Controller which processes form input.
 * 
 * @author geoff
 */
@Controller
public class Results extends AbstractMessageSourceAwareController {

  // Checkbox checked status
  protected static final String BOX_CHECKED_VALUE = "on";

  @Autowired @Qualifier(ClientDirectIdentifiers.COMPONENT_CLIENT_DIRECT_SERVICE)
  private ClientDirectService clientDirectService;

  private static final Log log = LogFactory.getLog(Results.class);

  /**
   * Process form input and view results (or return to input if problems).
   * 
   * @param modelAssignedToken Model assigned token.
   * @param iCaLC50 Ca C50.
   * @param iCaLHill Ca Hill. 
   * @param iCaLSaturation Ca saturation.
   * @param iCaLSpread Ca spread.
   * @param iKrC50 hERG C50.
   * @param iKrHill hERG Hill.
   * @param iKrSaturation hERG saturation.
   * @param iKrSpread hERG spread.
   * @param iK1C50 IK1 C50.
   * @param iK1Hill IK1 Hill.
   * @param iK1Saturation IK1 saturation.
   * @param iK1Spread IK1 spread.
   * @param iKsC50 IKs C50.
   * @param iKsHill IKs Hill.
   * @param iKsSaturation IKs saturation.
   * @param iKsSpread IKs spread.
   * @param itoC50 Ito C50.
   * @param itoHill Ito Hill.
   * @param itoSaturation Ito saturation.
   * @param itoSpread Ito spread.
   * @param iNaC50 Fast sodium C50.
   * @param iNaHill Fast sodium Hill.
   * @param iNaSaturation Fast sodium saturation.
   * @param iNaSpread Fast sodium spread.
   * @param iNaLC50 Late/Persistent sodium C50.
   * @param iNaLHill Late/Persistent sodium Hill.
   * @param iNaLSaturation Late/Persistent sodium saturation.
   * @param iNaLSpread Late/Persistent sodium spread.
   * @param c50Type C50 type.
   * @param ic50Units IC50 units.
   * @param modelIdentifier Model identifier.
   * @param pacingFrequency Pacing frequency.
   * @param pacingMaxTime Pacing max. time.
   * @param pkOrConcs Indicator if PK file or concentrations used for input.
   * @param plasmaConcPoints Plasma concentration points.
   * @param multipartFilePK PK file.
   * @param plasmaConcMax Maximum plasma concentration.
   * @param plasmaConcMin Minimum plasma concentration.
   * @param plasmaIntPtCount Intermediate point count.
   * @param plasmaIntPtLogScale Intermediate point log scale.
   * @param notes Notes.
   * @param spreadsEnabled Spreads enabled indicator.
   * @param multipartFileCellML (Optional) Multipart CellML file.
   * @param credibleIntervalPctiles (Optional) Credible intervale percentiles.
   * @param session Active session.
   * @param locale Interface locale.
   * @param model UI model.
   * @param principal User prinicipal.
   * @return Page name to view.
   */
  @RequestMapping(method=RequestMethod.POST,
                  value=ClientDirectIdentifiers.ACTION_RESULTS_VIEW)
  public String processInput(final @RequestParam(required=true,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_TOKEN)
                                   String modelAssignedToken,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_ICAL_C50)
                                   String iCaLC50,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_ICAL_HILL)
                                   String iCaLHill,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_ICAL_SATURATION)
                                   String iCaLSaturation,
                             @RequestParam(required=false,
                                           value=ClientDirectIdentifiers.PARAM_NAME_ICAL_SPREAD)
                                   String iCaLSpread,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_IKR_C50)
                                   String iKrC50,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_IKR_HILL)
                                   String iKrHill,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_IKR_SATURATION)
                                   String iKrSaturation,
                             @RequestParam(required=false,
                                          value=ClientDirectIdentifiers.PARAM_NAME_IKR_SPREAD)
                                   String iKrSpread,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_IK1_C50)
                                   String iK1C50,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_IK1_HILL)
                                   String iK1Hill,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_IK1_SATURATION)
                                   String iK1Saturation,
                             @RequestParam(required=false,
                                           value=ClientDirectIdentifiers.PARAM_NAME_IK1_SPREAD)
                                   String iK1Spread,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_IKS_C50)
                                   String iKsC50,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_IKS_HILL)
                                   String iKsHill,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_IKS_SATURATION)
                                   String iKsSaturation,
                             @RequestParam(required=false,
                                           value=ClientDirectIdentifiers.PARAM_NAME_IKS_SPREAD)
                                   String iKsSpread,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_ITO_C50)
                                   String itoC50,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_ITO_HILL)
                                   String itoHill,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_ITO_SATURATION)
                                   String itoSaturation,
                             @RequestParam(required=false,
                                           value=ClientDirectIdentifiers.PARAM_NAME_ITO_SPREAD)
                                   String itoSpread,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_INA_C50)
                                   String iNaC50,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_INA_HILL)
                                   String iNaHill,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_INA_SATURATION)
                                   String iNaSaturation,
                             @RequestParam(required=false,
                                           value=ClientDirectIdentifiers.PARAM_NAME_INA_SPREAD)
                                   String iNaSpread,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_INAL_C50)
                                   String iNaLC50,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_INAL_HILL)
                                   String iNaLHill,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_INAL_SATURATION)
                                   String iNaLSaturation,
                             @RequestParam(required=false,
                                           value=ClientDirectIdentifiers.PARAM_NAME_INAL_SPREAD)
                                   String iNaLSpread,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_C50_TYPE)
                                   String c50Type,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_IC50_UNITS)
                                   String ic50Units,
                             @RequestParam(required=false,
                                           value=ClientDirectIdentifiers.PARAM_NAME_MODEL_IDENTIFIER)
                                   String modelIdentifier,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_PACING_FREQUENCY)
                                   String pacingFrequency,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_PACING_MAX_TIME)
                                   String pacingMaxTime,
                             final @RequestParam(required=true,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_PK_OR_CONCS)
                                   String pkOrConcs,
                             @RequestParam(required=false,
                                           value=ClientDirectIdentifiers.PARAM_NAME_PLASMA_CONC_POINTS)
                                   String[] plasmaConcPoints,
                             final @RequestParam(required=false,
                                                 value=ClientSharedIdentifiers.PARAM_NAME_FILE_PK)
                                   MultipartFile multipartFilePK,
                             @RequestParam(required=false,
                                           value=ClientDirectIdentifiers.PARAM_NAME_PLASMA_CONC_MAX)
                                   String plasmaConcMax,
                             @RequestParam(required=false,
                                           value=ClientDirectIdentifiers.PARAM_NAME_PLASMA_CONC_MIN)
                                   String plasmaConcMin,
                             @RequestParam(required=false,
                                           value=ClientDirectIdentifiers.PARAM_NAME_PLASMA_INTERMEDIATE_POINT_COUNT)
                                   String plasmaIntPtCount,
                             @RequestParam(required=false,
                                           value=ClientDirectIdentifiers.PARAM_NAME_PLASMA_INTERMEDIATE_POINT_LOG)
                                   String plasmaIntPtLogScale,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_NOTES)
                                   String notes,
                             final @RequestParam(required=false,
                                                 value=ClientDirectIdentifiers.PARAM_NAME_SPREADS_ENABLED)
                                   String spreadsEnabled,
                             @RequestParam(required=false,
                                           value=ClientSharedIdentifiers.PARAM_NAME_FILE_CELLML)
                                   MultipartFile multipartFileCellML,
                             @RequestParam(required=false,
                                           value=ClientDirectIdentifiers.PARAM_NAME_PCTILES)
                                   String credibleIntervalPctiles,
                             final HttpSession session,
                             final Locale locale,
                             final Model model,
                             final Principal principal) {
    final String logPrefix = "~processInput() : ";
    log.debug(logPrefix.concat("Invoked."));

    /*
     * Process uncertainties/spread input.
     */
    boolean useSpreadsEnabled = false;
    boolean nullifySpreadValues = false;
    if (BOX_CHECKED_VALUE.equalsIgnoreCase(spreadsEnabled)) {
      log.debug(logPrefix.concat("Going to use spreads!"));
      useSpreadsEnabled = true;
    } else {
      // User hasn't selected to enable spreads, so nullify any values that arrive.
      nullifySpreadValues = true;
    }

    /*
     * Query to determine access to restricted portal features.
     */
    final boolean allowUncertainties = ControllerUtil.allowFeature(PortalFeature.UNCERTAINTIES,
                                                                   clientDirectService);

    if (!allowUncertainties) {
      if (useSpreadsEnabled) {
        log.warn(logPrefix.concat("User chose to use spreads despite lacking access rights!")); 
      }
      // User wasn't allow to specify spreads, so nullify any values supplied.
      nullifySpreadValues = true;
    }

    if (nullifySpreadValues) {
      iCaLSpread = iKrSpread = iKsSpread = iK1Spread = itoSpread = iNaSpread =
      iNaLSpread = null;
      credibleIntervalPctiles = null;
    }

    final boolean allowDynamicCellML = ControllerUtil.allowFeature(PortalFeature.DYNAMIC_CELLMLS,
                                                                   clientDirectService);
    if (!allowDynamicCellML) {
      if (multipartFileCellML != null) {
        log.warn(logPrefix.concat("User has supplied dynamic CellML when not authorised!"));
      }

      multipartFileCellML = null;
    }

    /*
     * Plasma int pt log scale.
     */
    String usePlasmaIntPtLogScale = null;
    if (BOX_CHECKED_VALUE.equalsIgnoreCase(plasmaIntPtLogScale)) {
      usePlasmaIntPtLogScale = Boolean.TRUE.toString();
    } else {
      usePlasmaIntPtLogScale = Boolean.FALSE.toString();
    }

    /*
     * Process concentration provision input options
     */
    InputOption inputOption = null;
    try {
      inputOption = InputOption.valueOf(pkOrConcs);
    } catch (IllegalArgumentException e) {
      // Application error!
      final String errorMessage = "Input options must be one of '" + Arrays.asList(InputOption.values()) + "'.";
      log.error(logPrefix.concat(errorMessage));

      Main.setModelPropertiesForMain(clientDirectService, principal, model,
                                     errorMessage);
      return ClientDirectIdentifiers.PAGE_MAIN;
    }

    PortalFile pkFile = null;
    String failedPKFileProcessing = null; 

    switch (inputOption) {
      case CONC_POINTS :
        plasmaConcMax = plasmaConcMin = plasmaIntPtCount = plasmaIntPtLogScale = null;
        break;
      case CONC_RANGE :
        plasmaConcPoints = null;
        break;
      case PK :
        plasmaConcPoints = null;
        plasmaConcMax = plasmaConcMin = plasmaIntPtCount = plasmaIntPtLogScale = null;

        FileStoreActionOutcomeVO outcomePK = processMultipart(multipartFilePK,
                                                              FILE_DATA_TYPE.PK,
                                                              principal);
        if (outcomePK == null) {
          // No (or empty) multipart file supplied.
          outcomePK = new FileStoreActionOutcomeVO(null, false,
                                                   "Error : PK processing specified but no PK file uploaded!");
        }

        if (outcomePK.isSuccess()) {
          pkFile = outcomePK.getPortalFile();
        } else {
          // Hold the problem in a variable for later (i.e. after input VO build) return to client. 
          failedPKFileProcessing = outcomePK.getInformation();
        }

        break;
    }

    PortalFile cellMLFile = null;
    String failedCellMLFileProcessing = null;

    final FileStoreActionOutcomeVO outcomeCellML = processMultipart(multipartFileCellML,
                                                                    FILE_DATA_TYPE.CELLML,
                                                                    principal);

    if (outcomeCellML != null) {
      // Reset the model identifier as we're using CellML!
      modelIdentifier = null;

      if (outcomeCellML.isSuccess()) {
        cellMLFile = outcomeCellML.getPortalFile();
        log.debug(logPrefix.concat("CellML file persisted '" + cellMLFile + "'."));
      } else {
        // Hold the problem in a variable for later (i.e. after input VO build) return to client. 
        failedCellMLFileProcessing = outcomeCellML.getInformation();
      }
    }

    log.debug("~processInput() : modelIdentifier : " + modelIdentifier);
    log.debug("~processInput() : cellMLFile : " + cellMLFile);

    // Build process for simulation input generates assignment problems if data isn't valid
    final SimulationInputVO simulationInputVO = new SimulationInputVO.Builder()
                                                                     .modelIdentifier(modelIdentifier)
                                                                     .pacingFrequency(pacingFrequency)
                                                                     .pacingMaxTime(pacingMaxTime)
                                                                     .iCaLC50(iCaLC50)
                                                                     .iCaLHill(iCaLHill)
                                                                     .iCaLSaturation(iCaLSaturation)
                                                                     .iCaLSpread(iCaLSpread)
                                                                     .iK1C50(iK1C50)
                                                                     .iK1Hill(iK1Hill)
                                                                     .iK1Saturation(iK1Saturation)
                                                                     .iK1Spread(iK1Spread)
                                                                     .iKrC50(iKrC50)
                                                                     .iKrHill(iKrHill)
                                                                     .iKrSaturation(iKrSaturation)
                                                                     .iKrSpread(iKrSpread)
                                                                     .iKsC50(iKsC50)
                                                                     .iKsHill(iKsHill)
                                                                     .iKsSaturation(iKsSaturation)
                                                                     .iKsSpread(iKsSpread)
                                                                     .iNaC50(iNaC50)
                                                                     .iNaHill(iNaHill)
                                                                     .iNaSaturation(iNaSaturation)
                                                                     .iNaSpread(iNaSpread)
                                                                     .iNaLC50(iNaLC50)
                                                                     .iNaLHill(iNaLHill)
                                                                     .iNaLSaturation(iNaLSaturation)
                                                                     .iNaLSpread(iNaLSpread)
                                                                     .itoC50(itoC50)
                                                                     .itoHill(itoHill)
                                                                     .itoSaturation(itoSaturation)
                                                                     .itoSpread(itoSpread)
                                                                     .c50Type(c50Type)
                                                                      // there may be a value for IC50 Units defined even if the C50 type is pIC50!
                                                                     .ic50Units(ic50Units)
                                                                     .plasmaConcMin(plasmaConcMin)
                                                                     .plasmaConcMax(plasmaConcMax)
                                                                     .plasmaIntPtCount(plasmaIntPtCount)
                                                                     .plasmaIntPtLogScale(usePlasmaIntPtLogScale)
                                                                     .plasmaConcPoints(plasmaConcPoints)
                                                                     .pkFile(pkFile)
                                                                     .cellMLFile(cellMLFile)
                                                                     .credibleIntervalPctiles(credibleIntervalPctiles)
                                                                     .notes(notes)
                                                                     .build();
    /* Strategy is to ensure that there's no simulation resubmission by enabling a session-bound
       token to be set when a new simulation is requested, and then the token is removed when it
       arrives here for physical creation. The token's removal from session prevents accidental
       resubmission. */
    if (session == null || session.getAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN) == null) {
      log.info(logPrefix.concat("No session token - resubmission attempt?"));
      final String useText = queryMessageSource(ClientDirectIdentifiers.RESUBMISSION_MSG_BUNDLE_ID,
                                                new Object[] {}, locale);
      Main.setModelPropertiesForMain(clientDirectService, principal, model, useText);
      return ClientDirectIdentifiers.PAGE_MAIN;
    }
    // Remove the session token as soon as possible.
    final String sessionToken = (String) session.getAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN);
    session.removeAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN);

    if (!sessionToken.equals(modelAssignedToken)) {
      log.info(logPrefix.concat("Session '" + sessionToken + "', does not equal Model '" + modelAssignedToken + "'."));
      final String useText = queryMessageSource(ClientDirectIdentifiers.RESUBMISSION_MSG_BUNDLE_ID,
                                                new Object[] {}, locale);
      Main.setModelPropertiesForMain(clientDirectService, principal, model, useText);
      return ClientDirectIdentifiers.PAGE_MAIN;
    }

    String returnPage = ClientDirectIdentifiers.PAGE_MAIN;
    if (simulationInputVO.hasAssignmentProblems() || failedPKFileProcessing != null
                                                  || failedCellMLFileProcessing != null) {
      log.debug(logPrefix.concat("Simulation input problems. Returning to input."));

      final List<String> exceptionMessages = new ArrayList<String>();
      if (failedPKFileProcessing != null) {
        exceptionMessages.add(failedPKFileProcessing);
      }
      if (failedCellMLFileProcessing != null) {
        exceptionMessages.add(failedCellMLFileProcessing);
      }
      final String exceptionMessage = exceptionMessages.isEmpty() ? null : 
                                       StringUtils.join(exceptionMessages, ", ");
      Process.setPropertiesForInput(clientDirectService, simulationInputVO,
                                    session, exceptionMessage, model);

      returnPage = ClientDirectIdentifiers.PAGE_INPUT;
    } else {
      log.debug(logPrefix.concat("Simulation input validated... about to process."));
      try {
        final long simulationId = clientDirectService.runSimulation(simulationInputVO,
                                                                    principal.getName());
        setModelProperties(model, simulationInputVO, allowDynamicCellML,
                           allowUncertainties, Long.valueOf(simulationId));
        returnPage = ClientDirectIdentifiers.PAGE_RESULTS;
      } catch (AppManagerWSInvocationException e) {
        final String useText = queryMessageSource(ClientDirectIdentifiers.APP_MANAGER_SOMETHING_WRONG,
                                                  new Object[] { e.getMessage() },
                                                  locale);
        Process.setPropertiesForInput(clientDirectService, simulationInputVO,
                                      session, useText, model);
        returnPage = ClientDirectIdentifiers.PAGE_INPUT;
      } catch (NoConnectionException e) {
        final String useText = queryMessageSource(ClientDirectIdentifiers.APP_MANAGER_OUT_OF_CONTACT,
                                                  new Object[] {}, locale);
        Process.setPropertiesForInput(clientDirectService, simulationInputVO,
                                      session, useText, model);
        returnPage = ClientDirectIdentifiers.PAGE_INPUT;
      } catch (RunInvocationException e) {
        final String useText = queryMessageSource(e.getMessage(),
                                                  new Object[] {}, locale);
        Process.setPropertiesForInput(clientDirectService, simulationInputVO,
                                      session, useText, model);
        returnPage = ClientDirectIdentifiers.PAGE_INPUT;
      }
    }

    return returnPage;
  }

  private FileStoreActionOutcomeVO processMultipart(final MultipartFile multipartFile,
                                                    final FILE_DATA_TYPE dataType,
                                                    final Principal principal) {
    FileStoreActionOutcomeVO outcome = null;

    final boolean hasFile = multipartFile != null && !multipartFile.isEmpty();

    if (hasFile) {
      InputStream inputStream = null;
      try {
        inputStream = multipartFile.getInputStream();
      } catch (IOException e) {
        final String errorMessage = "Application Error : Error handling the input stream from "
                                    .concat(dataType.toString()).concat(" file!");
        log.error("~processMultipart() : ".concat(errorMessage));

        return new FileStoreActionOutcomeVO(null, false, errorMessage);
      }

      if (inputStream == null) {
        final String errorMessage = "Application Error : No ".concat(dataType.toString())
                                    .concat(" file input stream available!");
        log.error("~processMultipart() : ".concat(errorMessage));

        return new FileStoreActionOutcomeVO(null, false, errorMessage);
      }

      outcome = clientDirectService.storeFile(dataType, inputStream,
                                              multipartFile.getOriginalFilename(),
                                              principal.getName());

      try {
        inputStream.close();
      } catch (IOException e) {
        log.warn("~processMultipart() : IO Error closing input stream '" + e.getMessage() + "'.");
      }
    }

    return outcome;
  }

  /**
   * Delete simulation results.
   * 
   * @param simulationId Simulation identifier.
   * @param principal User principal.
   * @param model UI model.
   * @return View name.
   */
  @RequestMapping(method=RequestMethod.POST,
                  value=ClientDirectIdentifiers.ACTION_RESULTS_DELETE)
  public String resultsDelete(final @RequestParam(required=true,
                                                  value=ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID)
                                    String simulationId,
                              final Principal principal,
                              final Model model) {
    log.debug("~resultsDelete() : Invoked for simulation id '" + simulationId + "'.");

    String returnPage = ClientDirectIdentifiers.PAGE_MAIN;

    if (!ControllerUtil.validSimulationId(simulationId)) {
      log.error("~resultsDelete() : Invalid simulation id provided.");
      Main.setModelPropertiesForMain(clientDirectService, principal, model, null);
    } else {
      final long useSimulationId = Long.valueOf(simulationId);
      clientDirectService.deleteSimulation(useSimulationId);

      Main.setModelPropertiesForMain(clientDirectService, principal, model, null);
    }

    return returnPage;
  }

  /**
   * View simulation results.
   * 
   * @param simulationId Simulation identifier.
   * @param locale Locale.
   * @param principal User principal.
   * @param model UI Model.
   * @return View name.
   */
  @RequestMapping(method=RequestMethod.GET,
                  value=ClientDirectIdentifiers.ACTION_RESULTS_VIEW)
  public String resultsView(final @RequestParam(required=false,
                                                value=ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID)
                                  String simulationId,
                            final Locale locale,
                            final Model model,
                            final Principal principal) {
    log.debug("~resultsView() : Invoked for simulation id '" + simulationId + "'.");

    String returnPage = ClientDirectIdentifiers.PAGE_MAIN;

    if (!ControllerUtil.validSimulationId(simulationId)) {
      log.error("~resultsView() : Invalid simulation id provided.");
      Main.setModelPropertiesForMain(clientDirectService, principal, model, null);
    } else {
      final long useSimulationId = Long.valueOf(simulationId);
      try {
        final Simulation simulation = clientDirectService.retrieveSimulation(useSimulationId);
        final SimulationInput simulationInput = simulation.getSimulationInput();

        // Query to determine access to restricted portal feature.
        final boolean allowDynamicCellML = ControllerUtil.allowFeature(PortalFeature.DYNAMIC_CELLMLS,
                                                                       clientDirectService);
        final boolean allowUncertainties = ControllerUtil.allowFeature(PortalFeature.UNCERTAINTIES,
                                                                       clientDirectService);

        setModelProperties(model, new SimulationInputVO(simulationInput),
                           allowDynamicCellML, allowUncertainties,
                           useSimulationId);
        returnPage = ClientDirectIdentifiers.PAGE_RESULTS;
      } catch (Exception e) {
        log.warn("~resultsView() : Query for simulation '" + simulationId + "' generated exception '" + e.getMessage() + "'.");
        /* Note: If the simulation doesn't exist there won't be any spring
                 security data so it'll generate an AccessDeniedException! */
        Main.setModelPropertiesForMain(clientDirectService, principal, model,
                                       queryMessageSource(ClientDirectIdentifiers.UNAVAILABLE_MSG_BUNDLE_ID,
                                                          new Object[] {}, locale));
      }
    }

    return returnPage;
  }

  private void setModelProperties(final Model model,
                                  final SimulationInputVO simulationInputVO,
                                  final boolean allowDynamicCellML,
                                  final boolean allowUncertainties,
                                  final Long simulationId) {
    model.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_INPUT_VALUES,
                       simulationInputVO);
    model.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_MODELS,
                       clientDirectService.retrieveModels());
    model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SHOW_DYNAMIC_CELLML,
                       allowDynamicCellML);
    model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SHOW_UNCERTAINTIES,
                       allowUncertainties);
    model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                       simulationId);
  }
}