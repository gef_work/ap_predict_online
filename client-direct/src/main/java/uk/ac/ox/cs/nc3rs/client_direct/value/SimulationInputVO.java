/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the names of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.value;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.nc3rs.client_direct.config.Configuration;
import uk.ac.ox.cs.nc3rs.client_direct.entity.SimulationInput;
import uk.ac.ox.cs.nc3rs.client_direct.util.ConverterUtil;
import uk.ac.ox.cs.nc3rs.client_direct.util.ValidatorUtil;
import uk.ac.ox.cs.nc3rs.client_direct.util.ValidatorUtil.TYPE;

/**
 * Value object representing the input data for a simulation run.
 * <p>
 * If you are creating a SimulationInputVO using the {@linkplain Builder#build()}
 * method you must check for value assignment problems via the {@link }
 * 
 * @author geoff
 */
public class SimulationInputVO {

  /*
   * These default/max/min values appear in the JSP data input pages as well
   * as being used internally for verifying input values. 
   * TODO: Should really be configurable!
   */
  public static final String DEFAULT_HILL_COEFFICIENT = "1";
  public static final String DEFAULT_SATURATION = "0";
  public static final BigDecimal DEFAULT_HILL_COEFFICIENT_BD = new BigDecimal(DEFAULT_HILL_COEFFICIENT);
  public static final BigDecimal DEFAULT_SATURATION_BD = new BigDecimal(DEFAULT_SATURATION);

  public static final int MAX_CONC_POINT_COUNT = 20;
  public static final int MAX_NOTES_LENGTH = 1000;

  public static final String MAX_HILL_COEFFICIENT = "5";
  public static final String MAX_PACING_FREQUENCY = "5";
  public static final String MAX_PACING_MAX_TIME = "120";
  public static final String MAX_PLASMA_INT_PT_CNT = "11";
  public static final String MAX_SPREAD = "2";

  public static final String MIN_HILL_COEFFICIENT = "0.1";
  public static final String MIN_PACING_FREQUENCY = "0.05";
  public static final String MIN_PACING_MAX_TIME = "0";
  public static final String MIN_SATURATION = "0";
  /** Value must be greater than 0! */
  public static final String MIN_SPREAD_GT = "0";

  public static final String UNIT_MINUS_LOG_M = "-log(M)";

  /*
   * IVN_ ... Input Value Name ..
   * Note: In addition to appearing in messages when invalid data encountered
   *       these IVN_ values appear textually in the Excel export.
   */
  public static final String IVN_MODEL_IDENTIFIER = "Model";
  public static final String IVN_PACING_FREQUENCY = "Pacing frequency";
  public static final String IVN_MAXIMUM_PACING_TIME = "Maximum pacing time";
  public static final String IVN_C50_TYPE = "C50 Type";
  public static final String IVN_IC50_UNITS = "IC50 Units";
  // e.g. "C50 IKr"
  public static final String IVN_IKr_C50 = ivnConstruct(MeasurementObservation.C50,
                                                        IonCurrent.IKr);
  // e.g. "Hill IKr"
  public static final String IVN_IKr_Hill = ivnConstruct(MeasurementObservation.Hill,
                                                         IonCurrent.IKr);
  public static final String IVN_IKr_Saturation = ivnConstruct(MeasurementObservation.Saturation,
                                                               IonCurrent.IKr);
  public static final String IVN_IKr_Spread = ivnConstruct(MeasurementObservation.Spread,
                                                           IonCurrent.IKr);
  public static final String IVN_INa_C50 = ivnConstruct(MeasurementObservation.C50,
                                                        IonCurrent.INa);
  public static final String IVN_INa_Hill = ivnConstruct(MeasurementObservation.Hill,
                                                         IonCurrent.INa);
  public static final String IVN_INa_Saturation = ivnConstruct(MeasurementObservation.Saturation,
                                                               IonCurrent.INa);
  public static final String IVN_INa_Spread = ivnConstruct(MeasurementObservation.Spread,
                                                           IonCurrent.INa);
  public static final String IVN_INaL_C50 = ivnConstruct(MeasurementObservation.C50,
                                                         IonCurrent.INaL);
  public static final String IVN_INaL_Hill = ivnConstruct(MeasurementObservation.Hill,
                                                          IonCurrent.INaL);
  public static final String IVN_INaL_Saturation = ivnConstruct(MeasurementObservation.Saturation,
                                                                IonCurrent.INaL);
  public static final String IVN_INaL_Spread = ivnConstruct(MeasurementObservation.Spread,
                                                            IonCurrent.INaL);
  public static final String IVN_ICaL_C50 = ivnConstruct(MeasurementObservation.C50,
                                                         IonCurrent.ICaL);
  public static final String IVN_ICaL_Hill = ivnConstruct(MeasurementObservation.Hill,
                                                          IonCurrent.ICaL);
  public static final String IVN_ICaL_Saturation = ivnConstruct(MeasurementObservation.Saturation,
                                                                IonCurrent.ICaL);
  public static final String IVN_ICaL_Spread = ivnConstruct(MeasurementObservation.Spread,
                                                            IonCurrent.ICaL);
  public static final String IVN_IKs_C50 = ivnConstruct(MeasurementObservation.C50,
                                                        IonCurrent.IKs);
  public static final String IVN_IKs_Hill = ivnConstruct(MeasurementObservation.Hill,
                                                         IonCurrent.IKs);
  public static final String IVN_IKs_Saturation = ivnConstruct(MeasurementObservation.Saturation,
                                                               IonCurrent.IKs);
  public static final String IVN_IKs_Spread = ivnConstruct(MeasurementObservation.Spread,
                                                           IonCurrent.IKs);
  public static final String IVN_IK1_C50 = ivnConstruct(MeasurementObservation.C50,
                                                        IonCurrent.IK1);
  public static final String IVN_IK1_Hill = ivnConstruct(MeasurementObservation.Hill,
                                                         IonCurrent.IK1);
  public static final String IVN_IK1_Saturation = ivnConstruct(MeasurementObservation.Saturation,
                                                               IonCurrent.IK1);
  public static final String IVN_IK1_Spread = ivnConstruct(MeasurementObservation.Spread,
                                                           IonCurrent.IK1);
  public static final String IVN_Ito_C50 = ivnConstruct(MeasurementObservation.C50,
                                                        IonCurrent.Ito);
  public static final String IVN_Ito_Hill = ivnConstruct(MeasurementObservation.Hill,
                                                         IonCurrent.Ito);
  public static final String IVN_Ito_Saturation = ivnConstruct(MeasurementObservation.Saturation,
                                                               IonCurrent.Ito);
  public static final String IVN_Ito_Spread = ivnConstruct(MeasurementObservation.Spread,
                                                           IonCurrent.Ito);
  public static final String IVN_CONC_POINTS = "Compound concentration points";
  public static final String IVN_CONC_MIN = "Compound concentration min.";
  public static final String IVN_CONC_MAX = "Compound concentration max.";
  public static final String IVN_CONC_INT_PT_CNT = "Compound concentration intermediate point count";
  public static final String IVN_CONC_INT_PT_LOGSCALE = "Compound concentration intermediate point log scale";
  public static final String IVN_NOTES = "Notes";
  public static final String IVN_PK_FILENAME = "PK file name";

  /*
   * Messages used when invalid data encountered.
   */
  private static final String invalid = "Invalid ";
  public static final String INVALID_C50_TYPE = invalid.concat(IVN_C50_TYPE).concat(" (Note: Valid options are " + StringUtils.join(C50_TYPE.values(), ", ") + ")");
  public static final String INVALID_IC50_UNITS = invalid.concat(IVN_IC50_UNITS).concat(" (Note: Valid options are " + StringUtils.join(IC50_UNIT.values(), ", ") + ")");
  public static final String INVALID_MODEL_IDENTIFIER = invalid.concat(IVN_MODEL_IDENTIFIER);
  public static final String INVALID_PACING_FREQUENCY = invalid.concat(IVN_PACING_FREQUENCY).concat(" (Note: Min. is " + MIN_PACING_FREQUENCY + ", max. is " + MAX_PACING_FREQUENCY + ")");
  public static final String INVALID_PACING_MAX_TIME = invalid.concat(IVN_MAXIMUM_PACING_TIME).concat(" (Note: Min. is >0, max. is " + MAX_PACING_MAX_TIME + ")");
  public static final String INVALID_PLASMA_CONC_MAX = invalid.concat(IVN_CONC_MAX);
  public static final String INVALID_PLASMA_CONC_MIN = invalid.concat(IVN_CONC_MIN).concat(" (Note: Min. is " + Configuration.PLASMA_CONC_MIN + ")");
  public static final String INVALID_PLASMA_INT_PT_COUNT = invalid.concat(IVN_CONC_INT_PT_CNT);
  public static final String INVALID_PLASMA_INT_PT_LOG_SCALE = invalid.concat(IVN_CONC_INT_PT_LOGSCALE);

  public static final String INVALID_CELLML = "Expecting either a model identifier or a CellML file";
  public static final String INVALID_EXCESS_PLASMA_INT_PT_CNT = "Cannot have a value of Intermediate Point Count greater than ".concat(MAX_PLASMA_INT_PT_CNT);
  public static final String INVALID_IC50_VALUE_OF_0 = "Cannot use an IC50 value of 0";
  public static final String INVALID_NEGATIVE_IC50 = "Cannot use a negative IC50 value";
  public static final String INVALID_NEGATIVE_PLASMA_INT_PT_CNT = "Cannot have a negative value for the Intermediate Point Count";
  public static final String INVALID_SPREAD = "Spread values must be within the range ".concat(MIN_SPREAD_GT).concat(" < spread <= ").concat(MAX_SPREAD);
  public static final String INVALID_SPREAD_PCTILES = "Mismatch between spread value and credible interval percentiles provision";
  public static final String INVALID_PK_OR_CONCS = "Expecting either a Concentration Range, Concentration Points, or a PK file";
  public static final String INVALID_PLASMA_CONC_MIN_MAX = "Cannot have a Minimum value greater than or equal to the Maximum for Plasma Concentration";
  public static final String INVALID_PLASMA_CONC_POINT = "Invalid (e.g. non-numeric or -ve) value encountered in Plasma Concentration points";
  public static final String INVALID_TYPE_NONASSIGNMENT = "IC50/pIC50 type must be provided";
  public static final String INVALID_UNIT_NONASSIGNMENT_IF_IC50 = "IC50 Units are required if an IC50 value is provided";
  public static final String UNDEFINED_PLASMA_CONC_MAX = "A Maximum Plasma Concentration must be provided";

  private static final String invalidObservations = "%s Hill and/or Saturation Level and/or Spread defined when no Inhibitory Concentration value defined";

  /** Message if a C50 Type is being assigned, it must be specified, i.e. cannot be null */
  public static final String SPECIFY_C50_TYPE = "C50 Type must be specified when assigning";
  /** Message if a IC50 Units is being assigned, it must be specified, i.e. cannot be null */
  public static final String SPECIFY_IC50_UNITS = "IC50 Units must be specified when assigning";

  /*
   * Used internally for verifying input value validity.
   */
  private static final int maxPlasmaIntPtCnt = Integer.valueOf(MAX_PLASMA_INT_PT_CNT);
  private static final BigDecimal maxHillCoefficient = new BigDecimal(MAX_HILL_COEFFICIENT);
  private static final BigDecimal maxPacingFrequency = new BigDecimal(MAX_PACING_FREQUENCY);
  private static final BigDecimal maxPacingMaxTime = new BigDecimal(MAX_PACING_MAX_TIME);
  private static final BigDecimal maxSpread = new BigDecimal(MAX_SPREAD);
  private static final BigDecimal minHillCoefficient = new BigDecimal(MIN_HILL_COEFFICIENT);
  private static final BigDecimal minPacingFrequency = new BigDecimal(MIN_PACING_FREQUENCY);
  private static final BigDecimal minPacingMaxTime = new BigDecimal(MIN_PACING_MAX_TIME);
  private static final BigDecimal minSaturation = new BigDecimal(MIN_SATURATION);
  private static final BigDecimal minSpreadGT = new BigDecimal(MIN_SPREAD_GT);
  private static final Short dynamicCellMLIdentifier = Short.valueOf(ClientSharedIdentifiers.IDENTIFIER_DYNAMIC_CELLML);

  private final MeasurementObservationsVO iCaLObservations;
  private final MeasurementObservationsVO iKrObservations;
  private final MeasurementObservationsVO iK1Observations;
  private final MeasurementObservationsVO iKsObservations;
  private final MeasurementObservationsVO itoObservations;
  private final MeasurementObservationsVO iNaObservations;
  private final MeasurementObservationsVO iNaLObservations;

  private final C50_TYPE c50Type;
  private final IC50_UNIT ic50Units;
  private final Short modelIdentifier;
  private final BigDecimal pacingMaxTime;
  private final BigDecimal pacingFrequency;
  private final BigDecimal plasmaConcMax;
  private final BigDecimal plasmaConcMin;
  private final Short plasmaIntPtCount;
  private final Boolean plasmaIntPtLogScale;
  private final List<String> plasmaConcPoints = new ArrayList<String>();
  private final PortalFile pkFile;
  private final PortalFile cellMLFile;
  private final String cellMLFileName;
  private final String credibleIntervalPctiles;
  private final String notes;
  private final Collection<String> assignmentProblems = new ArrayList<String>();
  /* Indicator of any of the MeasurementObservationVO objects having a
     non-default/null Hill, saturation or spread value. */
  private final boolean hasNonC50Value;

  private enum MeasurementObservation {
    C50,
    Hill,
    Saturation,
    Spread
  }

  public static class Builder {
    private Short modelIdentifier;
    private BigDecimal pacingMaxTime;
    private BigDecimal pacingFrequency;
    private BigDecimal iCaLC50;
    private BigDecimal iCaLHill;
    private BigDecimal iCaLSaturation;
    private BigDecimal iCaLSpread;
    private BigDecimal iKrC50;
    private BigDecimal iKrHill;
    private BigDecimal iKrSaturation;
    private BigDecimal iKrSpread;
    private BigDecimal iK1C50;
    private BigDecimal iK1Hill;
    private BigDecimal iK1Saturation;
    private BigDecimal iK1Spread;
    private BigDecimal iKsC50;
    private BigDecimal iKsHill;
    private BigDecimal iKsSaturation;
    private BigDecimal iKsSpread;
    private BigDecimal itoC50;
    private BigDecimal itoHill;
    private BigDecimal itoSaturation;
    private BigDecimal itoSpread;
    private BigDecimal iNaC50;
    private BigDecimal iNaHill;
    private BigDecimal iNaSaturation;
    private BigDecimal iNaSpread;
    private BigDecimal iNaLC50;
    private BigDecimal iNaLHill;
    private BigDecimal iNaLSaturation;
    private BigDecimal iNaLSpread;
    private C50_TYPE c50Type;
    private IC50_UNIT ic50Units;
    private BigDecimal plasmaConcMax;
    private BigDecimal plasmaConcMin;
    private Short plasmaIntPtCount;
    private Boolean plasmaIntPtLogScale;
    private List<String> plasmaConcPoints = new ArrayList<String>();
    private PortalFile pkFile;
    private PortalFile cellMLFile;
    private String credibleIntervalPctiles;
    private String notes;
    private final Collection<String> assignmentProblems = new ArrayList<String>();

    /**
     * Constructor.
     */
    public Builder() {}

    /**
     * Build the object.
     * <p>
     * <b>Important!</b> : This method may create an object which is not in a
     * valid state. Invalid state (if any) is indicated by the 
     * {@linkplain SimulationInputVO#hasAssignmentProblems()} method.
     * 
     * @return New object.
     */
    public SimulationInputVO build() {
      if (isPIC50()) {
        // If it's a pIC50 then the IC50 units are nullified.
        ic50Units = null;
      }
      assignDefaultValues();
      meetsMinimumDataRequirements();
      preBuildValidation();
      return new SimulationInputVO(this);
    }

    /**
     * Model identifier.
     * 
     * @param val Model identifier.
     * @return Builder.
     */
    public Builder modelIdentifier(final String val) {
      if (val == null) {
        this.modelIdentifier = null;
      } else {
        if (ValidatorUtil.isValid(val, TYPE.SHORT)) {
          this.modelIdentifier = Short.valueOf(val);
        } else {
          assignmentProblems.add(INVALID_MODEL_IDENTIFIER);
        }
      }
      return this;
    }

    /**
     * Maximum pacing time.
     * 
     * @param val Maximum pacing time (in minutes).
     * @return Builder.
     */
    public Builder pacingMaxTime(final String val) {
      if (ValidatorUtil.isValid(val, TYPE.BIGDECIMAL)) {
        pacingMaxTime = new BigDecimal(val);
        if (pacingMaxTime.compareTo(maxPacingMaxTime) == 1 ||
            pacingMaxTime.compareTo(minPacingMaxTime) < 1) {
          assignmentProblems.add(INVALID_PACING_MAX_TIME);
        }
      } else {
        assignmentProblems.add(INVALID_PACING_MAX_TIME);
      }
      return this;
    }

    /**
     * Pacing Frequency.
     * 
     * @param val Pacing frequency (in Hz).
     * @return Builder.
     */
    public Builder pacingFrequency(final String val) {
      if (ValidatorUtil.isValid(val, TYPE.BIGDECIMAL)) {
        pacingFrequency = new BigDecimal(val);
        if (pacingFrequency.compareTo(maxPacingFrequency) == 1 ||
            pacingFrequency.compareTo(minPacingFrequency) == -1) {
          assignmentProblems.add(INVALID_PACING_FREQUENCY);
        }
      } else {
        assignmentProblems.add(INVALID_PACING_FREQUENCY);
      }
      return this;
    }

    private void assign(final String value, final IonCurrent channelCurrent,
                        final MeasurementObservation measurementObservation) {
      BigDecimal assign = null;
      boolean problem = true;
      switch (measurementObservation) {
        case C50:
          if (!StringUtils.isBlank(value)) {
            if (ValidatorUtil.isValid(value, TYPE.BIGDECIMAL)) {
              assign = new BigDecimal(value);
              problem = false;
            }
          } else {
            problem = false;
          }
          break;
        case Saturation:
          if (!StringUtils.isBlank(value)) {
            if (ValidatorUtil.isValid(value, TYPE.BIGDECIMAL)) {
              assign = new BigDecimal(value);
              if (assign.compareTo(minSaturation) >= 0) {
                problem = false;
              }
            }
          } else {
            problem = false;
          }
          break;
        case Hill:
          if (!StringUtils.isBlank(value)) {
            if (ValidatorUtil.isValid(value, TYPE.BIGDECIMAL)) {
              assign = new BigDecimal(value);
              if (assign.compareTo(maxHillCoefficient) <= 0 &&
                  assign.compareTo(minHillCoefficient) >= 0) {
                problem = false;
              }
            }
          } else {
            problem = false;
          }
          break;
        case Spread:
          if (!StringUtils.isBlank(value)) {
            if (ValidatorUtil.isValid(value, TYPE.BIGDECIMAL)) {
              assign = new BigDecimal(value);
              if (assign.compareTo(minSpreadGT) == 1 &&
                  assign.compareTo(maxSpread) <= 0) {
                // Value must be *greater than* the min and less than or equal to max.
                problem = false;
              }
            }
          } else {
            problem = false;
          }
          break;
        default:
          throw new UnsupportedOperationException("Unrecognised measurement observation of '" + measurementObservation + "'.");
      }

      if (problem) {
        assignmentProblems.add(invalidConstruct(ivnConstruct(measurementObservation,
                                                             channelCurrent)));
      }
      switch (channelCurrent) {
        case ICaL:
          switch (measurementObservation) {
            case C50: iCaLC50 = assign; break;
            case Hill: iCaLHill = assign; break;
            case Saturation: iCaLSaturation = assign; break;
            case Spread: iCaLSpread = assign; break;
          }
          break;
        case IK1:
          switch (measurementObservation) {
            case C50: iK1C50 = assign; break;
            case Hill: iK1Hill = assign; break;
            case Saturation: iK1Saturation = assign; break;
            case Spread: iK1Spread = assign; break;
          }
          break;
        case IKr:
          switch (measurementObservation) {
            case C50: iKrC50 = assign; break;
            case Hill: iKrHill = assign; break;
            case Saturation: iKrSaturation = assign; break;
            case Spread: iKrSpread = assign; break;
          }
          break;
        case IKs:
          switch (measurementObservation) {
            case C50: iKsC50 = assign; break;
            case Hill: iKsHill = assign; break;
            case Saturation: iKsSaturation = assign; break;
            case Spread: iKsSpread = assign; break;
          }
          break;
        case INa:
          switch (measurementObservation) {
            case C50: iNaC50 = assign; break;
            case Hill: iNaHill = assign; break;
            case Saturation: iNaSaturation = assign; break;
            case Spread: iNaSpread = assign; break;
          }
          break;
        case INaL:
          switch (measurementObservation) {
            case C50: iNaLC50 = assign; break;
            case Hill: iNaLHill = assign; break;
            case Saturation: iNaLSaturation = assign; break;
            case Spread: iNaLSpread = assign; break;
          }
          break;
        case Ito:
          switch (measurementObservation) {
            case C50: itoC50 = assign; break;
            case Hill: itoHill = assign; break;
            case Saturation: itoSaturation = assign; break;
            case Spread: itoSpread = assign; break;
          }
          break;
        default:
          throw new UnsupportedOperationException("Unrecognised current of '" + channelCurrent + "'.");
      }
    }

    private void assignDefaultValues() {
      if (iCaLC50 != null) {
        if (iCaLHill == null) iCaLHill = DEFAULT_HILL_COEFFICIENT_BD;
        if (iCaLSaturation == null) iCaLSaturation = DEFAULT_SATURATION_BD;
      }
      if (iK1C50 != null) {
        if (iK1Hill == null) iK1Hill = DEFAULT_HILL_COEFFICIENT_BD;
        if (iK1Saturation == null) iK1Saturation = DEFAULT_SATURATION_BD;
      }
      if (iKrC50 != null) {
        if (iKrHill == null) iKrHill = DEFAULT_HILL_COEFFICIENT_BD;
        if (iKrSaturation == null) iKrSaturation = DEFAULT_SATURATION_BD;
      }
      if (iKsC50 != null) {
        if (iKsHill == null) iKsHill = DEFAULT_HILL_COEFFICIENT_BD;
        if (iKsSaturation == null) iKsSaturation = DEFAULT_SATURATION_BD;
      }
      if (iNaC50 != null) {
        if (iNaHill == null) iNaHill = DEFAULT_HILL_COEFFICIENT_BD;
        if (iNaSaturation == null) iNaSaturation = DEFAULT_SATURATION_BD;
      }
      if (iNaLC50 != null) {
        if (iNaLHill == null) iNaLHill = DEFAULT_HILL_COEFFICIENT_BD;
        if (iNaLSaturation == null) iNaLSaturation = DEFAULT_SATURATION_BD;
      }
      if (itoC50 != null) {
        if (itoHill == null) itoHill = DEFAULT_HILL_COEFFICIENT_BD;
        if (itoSaturation == null) itoSaturation = DEFAULT_SATURATION_BD;
      }
    }

    /**
     * L-type calcium / CaV1.2 channel C50.
     * 
     * @param val CaV1.2 C50.
     * @return Builder.
     */
    public Builder iCaLC50(final String val) {
      assign(val, IonCurrent.ICaL, MeasurementObservation.C50);
      return this;
    }

    /**
     * L-type calcium / CaV1.2 channel Hill Coefficient.
     * 
     * @param val CaV1.2 Hill Coefficient.
     * @return Builder.
     */
    public Builder iCaLHill(final String val) {
      assign(val, IonCurrent.ICaL, MeasurementObservation.Hill);
      return this;
    }

    /**
     * L-type calcium / CaV1.2 channel Saturation Level.
     * 
     * @param val CaV1.2 Saturation Level.
     * @return Builder.
     */
    public Builder iCaLSaturation(final String val) {
      assign(val, IonCurrent.ICaL, MeasurementObservation.Saturation);
      return this;
    }

    /**
     * L-type calcium / CaV1.2 channel Spread.
     * 
     * @param val CaV1.2 Spread.
     * @return Builder.
     */
    public Builder iCaLSpread(final String val) {
      assign(val, IonCurrent.ICaL, MeasurementObservation.Spread);
      return this;
    }

    /**
     * IK1 / KCNN4 a.k.a KCa3.1 channel C50.
     * 
     * @param val C50.
     * @return Builder.
     */
    public Builder iK1C50(final String val) {
      assign(val, IonCurrent.IK1, MeasurementObservation.C50);
      return this;
    }

    /**
     * IK1 / KCNN4 a.k.a KCa3.1 channel Hill Coefficient.
     * 
     * @param val KCa3.1 Hill Coefficient.
     * @return Builder.
     */
    public Builder iK1Hill(final String val) {
      assign(val, IonCurrent.IK1, MeasurementObservation.Hill);
      return this;
    }

    /**
     * IK1 / KCNN4 a.k.a KCa3.1 channel Saturation Level.
     * 
     * @param val KCa3.1 Saturation Level.
     * @return Builder.
     */
    public Builder iK1Saturation(final String val) {
      assign(val, IonCurrent.IK1, MeasurementObservation.Saturation);
      return this;
    }

    /**
     * IK1 / KCNN4 a.k.a KCa3.1 channel Spread.
     * 
     * @param val KCa3.1 Spread.
     * @return Builder.
     */
    public Builder iK1Spread(final String val) {
      assign(val, IonCurrent.IK1, MeasurementObservation.Spread);
      return this;
    }

    /**
     * IKr / hERG channel C50.
     * 
     * @param val hERG C50.
     * @return Builder.
     */
    public Builder iKrC50(final String val) {
      assign(val, IonCurrent.IKr, MeasurementObservation.C50);
      return this;
    }

    /**
     * IKr / hERG channel Hill Coefficient.
     * 
     * @param val hERG Hill Coefficient.
     * @return Builder.
     */
    public Builder iKrHill(final String val) {
      assign(val, IonCurrent.IKr, MeasurementObservation.Hill);
      return this;
    }

    /**
     * IKr / hERG channel channel Saturation Level.
     * 
     * @param val hERG Saturation Level.
     * @return Builder.
     */
    public Builder iKrSaturation(final String val) {
      assign(val, IonCurrent.IKr, MeasurementObservation.Saturation);
      return this;
    }

    /**
     * IKr / hERG channel channel Spread.
     * 
     * @param val hERG Spread.
     * @return Builder.
     */
    public Builder iKrSpread(final String val) {
      assign(val, IonCurrent.IKr, MeasurementObservation.Spread);
      return this;
    }

    /**
     * IKs / KCNQ1 + MinK channel C50.
     * 
     * @param val KCNQ1 C50.
     * @return Builder.
     */
    public Builder iKsC50(final String val) {
      assign(val, IonCurrent.IKs, MeasurementObservation.C50);
      return this;
    }

    /**
     * IKs / KCNQ1 + MinK channel Hill Coefficient.
     * 
     * @param val KCNQ1 Hill Coefficient.
     * @return Builder.
     */
    public Builder iKsHill(final String val) {
      assign(val, IonCurrent.IKs, MeasurementObservation.Hill);
      return this;
    }

    /**
     * IKs / KCNQ1 + MinK channel Saturation Level.
     * 
     * @param val KCNQ1 Saturation Level.
     * @return Builder.
     */
    public Builder iKsSaturation(final String val) {
      assign(val, IonCurrent.IKs, MeasurementObservation.Saturation);
      return this;
    }

    /**
     * IKs / KCNQ1 + MinK channel Spread.
     * 
     * @param val KCNQ1 Spread.
     * @return Builder.
     */
    public Builder iKsSpread(final String val) {
      assign(val, IonCurrent.IKs, MeasurementObservation.Spread);
      return this;
    }

    /**
     * Ito / Kv4.3 + KChIP2.2 channel C50.
     * 
     * @param val Kv4.3 C50.
     * @return Builder.
     */
    public Builder itoC50(final String val) {
      assign(val, IonCurrent.Ito, MeasurementObservation.C50);
      return this;
    }

    /**
     * Ito / Kv4.3 + KChIP2.2 channel Hill Coefficient.
     * 
     * @param val Kv4.3 Hill Coefficient.
     * @return Builder.
     */
    public Builder itoHill(final String val) {
      assign(val, IonCurrent.Ito, MeasurementObservation.Hill);
      return this;
    }

    /**
     * Ito / Kv4.3 + KChIP2.2 channel Saturation Level.
     * 
     * @param val Kv4.3 Saturation Level.
     * @return Builder.
     */
    public Builder itoSaturation(final String val) {
      assign(val, IonCurrent.Ito, MeasurementObservation.Saturation);
      return this;
    }

    /**
     * Ito / Kv4.3 + KChIP2.2 channel Spread.
     * 
     * @param val Kv4.3 Spread.
     * @return Builder.
     */
    public Builder itoSpread(final String val) {
      assign(val, IonCurrent.Ito, MeasurementObservation.Spread);
      return this;
    }

    /**
     * Na / NaV1.5 channel C50.
     * 
     * @param val NaV1.5 C50.
     * @return Builder.
     */
    public Builder iNaC50(final String val) {
      assign(val, IonCurrent.INa, MeasurementObservation.C50);
      return this;
    }

    /**
     * Na / NaV1.5 channel Hill Coefficient.
     * 
     * @param val NaV1.5 Hill Coefficient.
     * @return Builder.
     */
    public Builder iNaHill(final String val) {
      assign(val, IonCurrent.INa, MeasurementObservation.Hill);
      return this;
    }

    /**
     * Na / NaV1.5 channel Saturation Level.
     * 
     * @param val NaV1.5 Saturation Level.
     * @return Builder.
     */
    public Builder iNaSaturation(final String val) {
      assign(val, IonCurrent.INa, MeasurementObservation.Saturation);
      return this;
    }

    /**
     * Na / NaV1.5 channel Spread.
     * 
     * @param val NaV1.5 Spread.
     * @return Builder.
     */
    public Builder iNaSpread(final String val) {
      assign(val, IonCurrent.INa, MeasurementObservation.Spread);
      return this;
    }

    /**
     * NaL / NaV1.5 channel C50.
     * 
     * @param val NaV1.5 C50.
     * @return Builder.
     */
    public Builder iNaLC50(final String val) {
      assign(val, IonCurrent.INaL, MeasurementObservation.C50);
      return this;
    }

    /**
     * NaL / NaV1.5 channel Hill Coefficient.
     * 
     * @param val NaV1.5 Hill Coefficient.
     * @return Builder.
     */
    public Builder iNaLHill(final String val) {
      assign(val, IonCurrent.INaL, MeasurementObservation.Hill);
      return this;
    }

    /**
     * NaL / NaV1.5 channel Saturation Level.
     * 
     * @param val NaV1.5 Saturation Level.
     * @return Builder.
     */
    public Builder iNaLSaturation(final String val) {
      assign(val, IonCurrent.INaL, MeasurementObservation.Saturation);
      return this;
    }

    /**
     * NaL / NaV1.5 channel Spread.
     * 
     * @param val NaV1.5 Spread.
     * @return Builder.
     */
    public Builder iNaLSpread(final String val) {
      assign(val, IonCurrent.INaL, MeasurementObservation.Spread);
      return this;
    }

    /**
     * C50 Type, i.e. IC50 or pIC50.
     * 
     * @param val IC50 or pIC50.
     * @return Builder.
     */
    public Builder c50Type(final String val) {
      if (!StringUtils.isBlank(val)) {
        try {
          c50Type = C50_TYPE.valueOf(val);
        } catch (IllegalArgumentException e) {
          assignmentProblems.add(INVALID_C50_TYPE);
        }
      } else {
        assignmentProblems.add(SPECIFY_C50_TYPE);
      }
      return this;
    }

    /**
     * C50 Units, i.e. M, µM, nM.
     * 
     * @param val C50 units.
     * @return Builder.
     */
    public Builder ic50Units(final String val) {
      if (!StringUtils.isBlank(val)) {
        try {
          ic50Units = IC50_UNIT.valueOf(val);
        } catch (IllegalArgumentException e) {
          assignmentProblems.add(INVALID_IC50_UNITS);
        }
      } else {
        assignmentProblems.add(SPECIFY_IC50_UNITS);
      }
      return this;
    }

    /**
     * Maximum plasma concentration.
     * 
     * @param val Maximum plasma concentration (in µM).
     * @return Builder.
     */
    public Builder plasmaConcMax(final String val) {
      if (!StringUtils.isBlank(val)) {
        if (ValidatorUtil.isValid(val, TYPE.BIGDECIMAL)) {
          plasmaConcMax = new BigDecimal(val);
        } else {
          assignmentProblems.add(INVALID_PLASMA_CONC_MAX);
        }
      }
      return this;
    }

    /**
     * Minimum plasma concentration.
     * 
     * @param val Minimum plasma concentration (in µM).
     * @return Builder.
     */
    public Builder plasmaConcMin(final String val) {
      if (!StringUtils.isBlank(val)) {
        if (ValidatorUtil.isValid(val, TYPE.BIGDECIMAL)) {
          plasmaConcMin = new BigDecimal(val);
          if (plasmaConcMin.compareTo(Configuration.PLASMA_CONC_MIN) == -1) {
            assignmentProblems.add(INVALID_PLASMA_CONC_MIN);
          }
        } else {
          assignmentProblems.add(INVALID_PLASMA_CONC_MIN);
        }
      }
      return this;
    }

    /**
     * Plasma concentrations - Intermediate point count.
     * 
     * @param val Plasma concentrations intermediate point count.
     * @return Builder.
     */
    public Builder plasmaIntPtCount(final String val) {
      if (!StringUtils.isBlank(val)) {
        if (ValidatorUtil.isValid(val, TYPE.SHORT)) {
          plasmaIntPtCount = new Short(val);
          if (plasmaIntPtCount < 0) {
            assignmentProblems.add(INVALID_NEGATIVE_PLASMA_INT_PT_CNT);
          }
          if (plasmaIntPtCount > maxPlasmaIntPtCnt) {
            assignmentProblems.add(INVALID_EXCESS_PLASMA_INT_PT_CNT);
          }
        } else {
          assignmentProblems.add(INVALID_PLASMA_INT_PT_COUNT);
        }
      }
      return this;
    }

    /**
     * Plasma concentrations - Log scale.
     * <ul>
     *   <li>{@code true} if ..
     *     <ul>
     *       <li>val =~ /true/i</li>
     *       <li>val is 1</li>
     *     </ul>
     *   </li>
     *   <li>{@code false} if ..
     *     <ul>
     *       <li>val =~ /false/i</li>
     *       <li>val is 0</li>
     *     </ul>
     *   </li>
     *   <li>Otherwise assignment problem</li>
     * </ul>
     * If this value is not specified a default value of {@code true} will be used. 
     * 
     * @param val Plasma concentrations log scale.
     * @return Builder.
     */
    public Builder plasmaIntPtLogScale(final String val) {
      if (!StringUtils.isBlank(val)) {
        if (val.equalsIgnoreCase("true") || val.equals("1")) {
          plasmaIntPtLogScale = true;
        } else if (val.equalsIgnoreCase("false") || val.equals("0")) {
          plasmaIntPtLogScale = false;
        } else {
          assignmentProblems.add(INVALID_PLASMA_INT_PT_LOG_SCALE);
        }
      } else {
        assignmentProblems.add(INVALID_PLASMA_INT_PT_LOG_SCALE);
      }
      return this;
    }

    /**
     * Assign the plasma concentration points.
     * 
     * @param val Plasma concentration point collection.
     * @return Builder.
     */
    public Builder plasmaConcPoints(final String[] val) {
      // Purge null/empty values
      final List<String> valuePoints = new ArrayList<String>();
      if (val != null && val.length > 0) {
        for (final String point : val) {
          if (!StringUtils.isBlank(point)) {
            valuePoints.add(point.trim());
          }
        }
      }

      // Use HashSet to remove duplicates.
      final Set<BigDecimal> validPoints = new HashSet<BigDecimal>();
      for (final String point : valuePoints) {
        BigDecimal stripped = null;
        if (ValidatorUtil.isValid(point, TYPE.BIGDECIMAL)) {
          stripped = new BigDecimal(point).stripTrailingZeros();
          if (stripped.compareTo(BigDecimal.ZERO) >= 0) {
            validPoints.add(stripped);
            if (validPoints.size() == SimulationInputVO.MAX_CONC_POINT_COUNT) {
              break;
            }
          } else {
            assignmentProblems.add(INVALID_PLASMA_CONC_POINT);
          }
        } else {
          assignmentProblems.add(INVALID_PLASMA_CONC_POINT);
        }
      }

      // Transfer to list and sort.
      final List<BigDecimal> sortedConcPoints = new ArrayList<BigDecimal>(validPoints);
      Collections.sort(sortedConcPoints);
      // Transfer from BigDecimal to String
      for (final BigDecimal point : sortedConcPoints) {
        plasmaConcPoints.add(point.toPlainString());
      }

      return this;
    }

    /**
     * Assign the CellML file.
     * 
     * @param val CellML file to assign.
     * @return Builder.
     */
    public Builder cellMLFile(final PortalFile val) {
      cellMLFile = val;

      return this;
    }

    /**
     * Assign the PK file.
     * 
     * @param val PK file to assign.
     * @return Builder.
     */
    public Builder pkFile(final PortalFile val) {
      pkFile = val;

      return this;
    }

    /**
     * Assign the Delta APD90 credible interval percentiles.
     * 
     * @param val Delta APD90 credible interval percentiles.
     * @return Builder.
     */
    public Builder credibleIntervalPctiles(final String val) {
      credibleIntervalPctiles = val;

      return this;
    }

    /**
     * Assign simulation notes.
     * 
     * @param val Simulation notes.
     * @return Builder.
     */
    public Builder notes(final String val) {
      String useNotes = val;
      if (!StringUtils.isBlank(val)) {
        useNotes = useNotes.trim();
        if (useNotes.length() > MAX_NOTES_LENGTH) {
          useNotes = useNotes.substring(0, MAX_NOTES_LENGTH);
        }
      }
      notes = useNotes;

      return this;
    }

    private boolean c50Assigned() {
      return (iCaLC50 != null || iK1C50 != null || iKrC50 != null || iKsC50 != null || 
              itoC50 != null || iNaC50 != null || iNaLC50 != null);
    }

    private boolean anyNegativeValues() {
      if (hasNegativeValue(iCaLC50) || hasNegativeValue(iK1C50) || hasNegativeValue(iKrC50) ||
          hasNegativeValue(iKsC50) || hasNegativeValue(itoC50) || hasNegativeValue(iNaC50) ||
          hasNegativeValue(iNaLC50)) {
        return true;
      }
      return false;
    }

    private boolean hasPlasmaConcPoints() {
      return !plasmaConcPoints.isEmpty();
    }

    private boolean hasPlasmaConcRange() {
      return (plasmaConcMax != null && plasmaConcMin != null &&
              plasmaIntPtCount != null && plasmaIntPtLogScale != null);
    }

    private boolean hasPKFile() {
      return (pkFile != null);
    }

    private boolean hasSpreadValues() {
      if (iCaLSpread != null || iK1Spread != null || iKrSpread != null ||
          iKsSpread != null || itoSpread != null || iNaSpread != null ||
          iNaLSpread != null) {
        return true;
      }
      return false;
    }

    private boolean anyZeroValues() {
      if (hasZeroValue(iCaLC50) || hasZeroValue(iK1C50) || hasZeroValue(iKrC50) ||
          hasZeroValue(iKsC50) || hasZeroValue(itoC50) || hasZeroValue(iNaC50) ||
          hasZeroValue(iNaLC50)) {
        return true;
      }
      return false;
    }

    private boolean hasNegativeValue(final BigDecimal value) {
      return (value != null && BigDecimal.ZERO.compareTo(value) == 1) ? true : false;
    }

    private boolean hasZeroValue(final BigDecimal value) {
      return (value != null && BigDecimal.ZERO.compareTo(value) == 0) ? true : false;
    }

    private boolean isPIC50() {
      return (c50Type != null && C50_TYPE.pIC50.compareTo(c50Type) == 0) ? true : false;
    }

    private boolean isIC50() {
      return (C50_TYPE.IC50.compareTo(c50Type) == 0) ? true : false;
    }

    // Make sure the minimum data is assigned.
    private void meetsMinimumDataRequirements() {
      if ((modelIdentifier == null ||
           modelIdentifier.equals(dynamicCellMLIdentifier))
          && cellMLFile == null) {
        assignmentProblems.add(INVALID_CELLML);
      }

      if (!hasPlasmaConcPoints() && !hasPlasmaConcRange() &&
          !hasPKFile()) {
        assignmentProblems.add(INVALID_PK_OR_CONCS);
      }
    }

    // Check that builder pattern assignment hasn't led to invalid state before
    // VO gets created.
    private void preBuildValidation() {
      if (c50Assigned()) {
        // If there's a (p)IC50 assigned, make sure that type, units and values 
        // are ok.
        if (c50Type == null) {
          assignmentProblems.add(INVALID_TYPE_NONASSIGNMENT);
        } else {
          if (isIC50()) {
            if (ic50Units == null) {
              assignmentProblems.add(INVALID_UNIT_NONASSIGNMENT_IF_IC50);
            }
            if (anyZeroValues()) {
              assignmentProblems.add(INVALID_IC50_VALUE_OF_0);
            }
            if (anyNegativeValues()) {
              assignmentProblems.add(INVALID_NEGATIVE_IC50);
            }
          }
        }
      }

      // Verify the channel assignments
      if (!MeasurementObservationsVO.valid(iCaLC50, iCaLHill, iCaLSaturation,
                                           iCaLSpread)) {
        assignmentProblems.add(invalidObservationConstruct(IonCurrent.ICaL));
      }
      if (!MeasurementObservationsVO.valid(iK1C50, iK1Hill, iK1Saturation,
                                           iK1Spread)) {
        assignmentProblems.add(invalidObservationConstruct(IonCurrent.IK1));
      }
      if (!MeasurementObservationsVO.valid(iKrC50, iKrHill, iKrSaturation,
                                           iKrSpread)) {
        assignmentProblems.add(invalidObservationConstruct(IonCurrent.IKr));
      }
      if (!MeasurementObservationsVO.valid(iKsC50, iKsHill, iKsSaturation,
                                           iKsSpread)) {
        assignmentProblems.add(invalidObservationConstruct(IonCurrent.IKs));
      }
      if (!MeasurementObservationsVO.valid(itoC50, itoHill, itoSaturation,
                                           itoSpread)) {
        assignmentProblems.add(invalidObservationConstruct(IonCurrent.Ito));
      }
      if (!MeasurementObservationsVO.valid(iNaC50, iNaHill, iNaSaturation,
                                           iNaSpread)) {
        assignmentProblems.add(invalidObservationConstruct(IonCurrent.INa));
      }
      if (!MeasurementObservationsVO.valid(iNaLC50, iNaLHill, iNaLSaturation,
                                           iNaLSpread)) {
        assignmentProblems.add(invalidObservationConstruct(IonCurrent.INaL));
      }

      if (plasmaConcMax != null && plasmaConcMin != null &&
          plasmaConcMin.compareTo(plasmaConcMax) >= 0) {
        assignmentProblems.add(INVALID_PLASMA_CONC_MIN_MAX);
      }

      if ((hasPlasmaConcPoints() && (hasPlasmaConcRange() || hasPKFile())) ||
          (hasPlasmaConcRange() && (hasPlasmaConcPoints() || hasPKFile())) ||
          (hasPKFile() && (hasPlasmaConcPoints() || hasPlasmaConcRange()))) {
        assignmentProblems.add(INVALID_PK_OR_CONCS);
      }

      if (modelIdentifier != null && cellMLFile != null) {
        assignmentProblems.add(INVALID_CELLML);
      }

      if ((hasSpreadValues() && StringUtils.isBlank(credibleIntervalPctiles)) ||
          (!hasSpreadValues() && !StringUtils.isBlank(credibleIntervalPctiles))) {
        assignmentProblems.add(INVALID_SPREAD_PCTILES);
      }
    }
  }

  /*
   * Create a value object, but it may be in an invalid state - as determined
   * by values appearing in {@linkplain #assignmentProblems}.
   */
  private SimulationInputVO(final Builder builder) {
    modelIdentifier = builder.modelIdentifier;
    pacingMaxTime = builder.pacingMaxTime;
    pacingFrequency = builder.pacingFrequency;
    iCaLObservations = new MeasurementObservationsVO(builder.iCaLC50,
                                                     builder.iCaLHill,
                                                     builder.iCaLSaturation,
                                                     builder.iCaLSpread);
    iKrObservations = new MeasurementObservationsVO(builder.iKrC50,
                                                    builder.iKrHill,
                                                    builder.iKrSaturation,
                                                    builder.iKrSpread);
    iK1Observations = new MeasurementObservationsVO(builder.iK1C50,
                                                    builder.iK1Hill,
                                                    builder.iK1Saturation,
                                                    builder.iK1Spread);
    iKsObservations = new MeasurementObservationsVO(builder.iKsC50,
                                                    builder.iKsHill,
                                                    builder.iKsSaturation,
                                                    builder.iKsSpread);
    itoObservations = new MeasurementObservationsVO(builder.itoC50,
                                                    builder.itoHill,
                                                    builder.itoSaturation,
                                                    builder.itoSpread);
    iNaObservations = new MeasurementObservationsVO(builder.iNaC50,
                                                    builder.iNaHill,
                                                    builder.iNaSaturation,
                                                    builder.iNaSpread);
    iNaLObservations = new MeasurementObservationsVO(builder.iNaLC50,
                                                     builder.iNaLHill,
                                                     builder.iNaLSaturation,
                                                     builder.iNaLSpread);
    hasNonC50Value = retrieveNonC50ValueIndicator(Arrays.asList(iCaLObservations,
                                                                iKrObservations,
                                                                iK1Observations,
                                                                iKsObservations,
                                                                itoObservations,
                                                                iNaObservations,
                                                                iNaLObservations));
    c50Type = builder.c50Type;
    ic50Units = builder.ic50Units;
    plasmaConcMax = builder.plasmaConcMax;
    plasmaConcMin = builder.plasmaConcMin;
    plasmaIntPtCount = builder.plasmaIntPtCount;
    // Post-141106 ApPredict now defaults to true for using log scale
    plasmaIntPtLogScale = builder.plasmaIntPtLogScale == null ? true : builder.plasmaIntPtLogScale;
    plasmaConcPoints.addAll(builder.plasmaConcPoints);
    pkFile = builder.pkFile;
    cellMLFile = builder.cellMLFile;
    cellMLFileName = (cellMLFile == null) ? null : cellMLFile.getName();
    credibleIntervalPctiles = builder.credibleIntervalPctiles;
    notes = builder.notes;

    assignmentProblems.addAll(builder.assignmentProblems);
  }

  /**
   * Create simulation input value object from a <b>persisted</b> simulation
   * input entity.
   * 
   * @param simulationInput Persisted (i.e. not transient) simulation input
   *                        entity.
   * @throws IllegalArgumentException If a transient {@link SimulationInput} is
   *                                  provided.
   */
  // It as assumed that a persisted object has a valid state.
  public SimulationInputVO(final SimulationInput simulationInput)
                           throws IllegalArgumentException {
    if (simulationInput.getId() == null) {
      throw new IllegalArgumentException("A transient entity cannot be used for simulation input value construction.");
    }

    modelIdentifier = simulationInput.getModelIdentifier();
    pacingFrequency = simulationInput.getPacingFrequency();
    pacingMaxTime = simulationInput.getPacingMaxTime();
    iCaLObservations = new MeasurementObservationsVO(simulationInput.getC50ICaL(),
                                                     simulationInput.getHillICaL(),
                                                     simulationInput.getSaturationICaL(),
                                                     simulationInput.getSpreadICaL());
    iK1Observations = new MeasurementObservationsVO(simulationInput.getC50Ik1(),
                                                    simulationInput.getHillIk1(),
                                                    simulationInput.getSaturationIk1(),
                                                    simulationInput.getSpreadIk1());
    iKsObservations = new MeasurementObservationsVO(simulationInput.getC50Iks(),
                                                    simulationInput.getHillIks(),
                                                    simulationInput.getSaturationIks(),
                                                    simulationInput.getSpreadIks());
    iKrObservations = new MeasurementObservationsVO(simulationInput.getC50Ikr(),
                                                    simulationInput.getHillIkr(),
                                                    simulationInput.getSaturationIkr(),
                                                    simulationInput.getSpreadIkr());
    itoObservations = new MeasurementObservationsVO(simulationInput.getC50Ito(),
                                                    simulationInput.getHillIto(),
                                                    simulationInput.getSaturationIto(),
                                                    simulationInput.getSpreadIto());
    iNaObservations = new MeasurementObservationsVO(simulationInput.getC50INa(),
                                                    simulationInput.getHillINa(),
                                                    simulationInput.getSaturationINa(),
                                                    simulationInput.getSpreadINa());
    iNaLObservations = new MeasurementObservationsVO(simulationInput.getC50INaL(),
                                                     simulationInput.getHillINaL(),
                                                     simulationInput.getSaturationINaL(),
                                                     simulationInput.getSpreadINaL());
    hasNonC50Value = retrieveNonC50ValueIndicator(Arrays.asList(iCaLObservations,
                                                                iKrObservations,
                                                                iK1Observations,
                                                                iKsObservations,
                                                                itoObservations,
                                                                iNaObservations,
                                                                iNaLObservations));
    c50Type = simulationInput.getC50Type();
    ic50Units = simulationInput.getIc50Units();
    plasmaConcMin = simulationInput.getPlasmaConcMin();
    plasmaConcMax = simulationInput.getPlasmaConcMax();
    plasmaIntPtCount = simulationInput.getPlasmaIntPtCount();
    plasmaIntPtLogScale = simulationInput.isPlasmaIntPtLogScale();
    final String persistedPlasmaConcPoints = simulationInput.getPlasmaConcPoints();
    if (!StringUtils.isBlank(persistedPlasmaConcPoints)) {
      plasmaConcPoints.addAll(Arrays.asList(persistedPlasmaConcPoints.split(" ")));
    }
    pkFile = simulationInput.getPkFile();
    cellMLFile = null;
    cellMLFileName = simulationInput.getCellMLFileName();
    credibleIntervalPctiles = simulationInput.getCredibleIntervalPctiles();
    notes = simulationInput.getNotes();
  }

  /**
   * Indicate if there are non-C50, e.g. Hill, saturation or spread measurement
   * observations defined for <b>any</b> ion channel.
   * 
   * @return {@code true} if Hill, saturation or spread value present for
   *         <b>any</b> ion channel, otherwise {@code false}.
   */
  // This is a UI value to indicate whether to display some options/data.
  public boolean isNonC50Value() {
    return hasNonC50Value;
  }

  /**
   * Retrieve the type of simulation based on the provided input options, e.g.
   * PK file, Conc range, Conc points.
   * 
   * @return Simulation type as input option.
   * @throws IllegalStateException If no concentration data supplied.
   */
  public InputOption retrieveInputOption() throws IllegalStateException {
    InputOption inputOption = null;
    if (getPkFile() != null) {
      inputOption = InputOption.PK;
    } else if (!getPlasmaConcPoints().isEmpty()) {
      inputOption = InputOption.CONC_POINTS;
    } else if (getPlasmaConcMax() != null) {
      inputOption = InputOption.CONC_RANGE;
    } else {
      final String errorMessage = "Indeterminable simulation type";
      throw new IllegalStateException(errorMessage);
    }

    return inputOption;
  }

  private boolean retrieveNonC50ValueIndicator(final List<MeasurementObservationsVO> allObservations) {
    boolean hasNonC50Value = false;
    for (final MeasurementObservationsVO compoundObservations : allObservations) {
      if (compoundObservations.hasNonC50Value()) {
        hasNonC50Value = true;
        break;
      }
    }
    return hasNonC50Value;
  }

  /**
   * Indicate if there have been assignment problems in creating the object.
   * <p>
   * This should never be {@code true} if using the
   * {@linkplain SimulationInputVO#SimulationInputVO(SimulationInput)}
   * constructor but may be {@code true} if using the {@linkplain SimulationInputVO.Builder#build()}
   * method.
   *  
   * @return {@code true} if assignment problems, otherwise {@code false}.
   */
  public boolean hasAssignmentProblems() {
    return (!getAssignmentProblems().isEmpty());
  }

  protected static String invalidConstruct(final String ivn) {
    return invalid.concat(ivn);
  }

  /**
   * Use this to format the "invalid Hill/Saturation/Spread" message with the
   * specified ion channel, e.g. "ICaL has invalid ...".
   * 
   * @param channelCurrent Ion channel current.
   * @return String containing invalid data message.
   */
  protected static String invalidObservationConstruct(final IonCurrent channelCurrent) {
    return String.format(invalidObservations, channelCurrent.toString());
  }

  protected static String ivnConstruct(final MeasurementObservation measurementObservation,
                                       final IonCurrent channelCurrent) {
    return measurementObservation.toString().concat(" ").concat(channelCurrent.toString());
  }

  // Retrieve a pIC50 value from whatever the user supplied.
  private BigDecimal retrievePIC50(final BigDecimal c50) {
    if (c50 == null) {
      return c50;
    }
    if (C50_TYPE.pIC50.compareTo(c50Type) == 0) {
      // The original was a pIC50
      return c50;
    }

    return ConverterUtil.ic50ToPIC50(ic50Units, c50);
  }

  /**
   * Retrieve the ICaL pIC50 value.
   * 
   * @return ICaL pIC50 value (or {@code null} if not assigned).
   */
  // Used currently for app-manager ApPredict creation and pIC50 element title display!
  public BigDecimal retrievePIC50ICaL() {
    return retrievePIC50(getICaLObservations().getC50());
  }

  /**
   * Retrieve the IK1 pIC50 value.
   * 
   * @return IK1 pIC50 value (or {@code null} if not assigned).
   */
  // Used currently for app-manager ApPredict creation and pIC50 element title display
  public BigDecimal retrievePIC50Ik1() {
    return retrievePIC50(getIK1Observations().getC50());
  }

  /**
   * Retrieve the IKr pIC50 value.
   * 
   * @return IKr pIC50 value (or {@code null} if not assigned).
   */
  // Used currently for app-manager ApPredict creation and pIC50 element title display
  public BigDecimal retrievePIC50Ikr() {
    return retrievePIC50(getIKrObservations().getC50());
  }

  /**
   * Retrieve the IKs pIC50 value.
   * 
   * @return IKs pIC50 value (or {@code null} if not assigned).
   */
  // Used currently for app-manager ApPredict creation and pIC50 element title display
  public BigDecimal retrievePIC50Iks() {
    return retrievePIC50(getIKsObservations().getC50());
  }

  /**
   * Retrieve the Ito pIC50 value.
   * 
   * @return Ito pIC50 value (or {@code null} if not assigned).
   */
  // Used currently for app-manager ApPredict creation and pIC50 element title display
  public BigDecimal retrievePIC50Ito() {
    return retrievePIC50(getItoObservations().getC50());
  }

  /**
   * Retrieve the INa pIC50 value.
   * 
   * @return INa pIC50 value (or {@code null} if not assigned).
   */
  // Used currently for app-manager ApPredict creation and pIC50 element title display
  public BigDecimal retrievePIC50INa() {
    return retrievePIC50(getINaObservations().getC50());
  }

  /**
   * Retrieve the INaL pIC50 value.
   * 
   * @return INaL pIC50 value (or {@code null} if not assigned).
   */
  // Used currently for app-manager ApPredict creation and pIC50 element title display
  public BigDecimal retrievePIC50INaL() {
    return retrievePIC50(getINaLObservations().getC50());
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "SimulationInputVO [iCaLObservations=" + iCaLObservations
        + ", iKrObservations=" + iKrObservations + ", iK1Observations="
        + iK1Observations + ", iKsObservations=" + iKsObservations
        + ", itoObservations=" + itoObservations + ", iNaObservations="
        + iNaObservations + ", iNaLObservations="
        + iNaLObservations + ", c50Type=" + c50Type + ", ic50Units=" + ic50Units
        + ", modelIdentifier=" + modelIdentifier + ", pacingMaxTime="
        + pacingMaxTime + ", pacingFrequency=" + pacingFrequency
        + ", plasmaConcMax=" + plasmaConcMax + ", plasmaConcMin="
        + plasmaConcMin + ", plasmaIntPtCount=" + plasmaIntPtCount
        + ", plasmaIntPtLogScale=" + plasmaIntPtLogScale + ", plasmaConcPoints="
        + plasmaConcPoints + ", pkFile=" + (pkFile != null) + ", cellMLFile="
        + (cellMLFile != null)
        + ", notes=" + notes + ", assignmentProblems=" + assignmentProblems
        + ", hasNonC50Value=" + hasNonC50Value + "]";
  }

  /**
   * Retrieve the ICaL measurement observations.
   * 
   * @return ICaL measurement observations (obj. containing nothing if none
   *         provided).
   */
  public MeasurementObservationsVO getICaLObservations() {
    return iCaLObservations;
  }

  /**
   * Retrieve the IKr measurement observations.
   * 
   * @return IKr measurement observations (obj. containing nothing if none
   *         provided).
   */
  public MeasurementObservationsVO getIKrObservations() {
    return iKrObservations;
  }

  /**
   * Retrieve the IK1 measurement observations.
   * 
   * @return IK1 measurement observations (obj. containing nothing if none
   *         provided).
   */
  public MeasurementObservationsVO getIK1Observations() {
    return iK1Observations;
  }

  /**
   * Retrieve the IKs measurement observations.
   * 
   * @return IKs measurement observations (obj. containing nothing if none
   *         provided).
   */
  public MeasurementObservationsVO getIKsObservations() {
    return iKsObservations;
  }

  /**
   * Retrieve the Ito measurement observations.
   * 
   * @return Ito measurement observations (obj. containing nothing if none
   *         provided).
   */
  public MeasurementObservationsVO getItoObservations() {
    return itoObservations;
  }

  /**
   * Retrieve the INa measurement observations.
   * 
   * @return INa measurement observations (obj. containing nothing if none
   *         provided).
   */
  public MeasurementObservationsVO getINaObservations() {
    return iNaObservations;
  }

  /**
   * Retrieve the INaL measurement observations.
   * 
   * @return INaL measurement observations (obj. containing nothing if none
   *         provided).
   */
  public MeasurementObservationsVO getINaLObservations() {
    return iNaLObservations;
  }
  /**
   * Retrieve the 50% inhibitory concentration type, e.g. pIC50, IC50.
   * 
   * @return 50% inhibitory concentration type.
   */
  public C50_TYPE getC50Type() {
    return c50Type;
  }

  /**
   * 50% inhibitory concentration units, e.g. µM.
   * 
   * @return 50% inhibitory concentration units.
   */
  public IC50_UNIT getIc50Units() {
    return ic50Units;
  }

  /**
   * Retrieve the model identifier.
   * 
   * @return The model identifier, or {@code null} if none provided (i.e. using
   *         CellML instead).
   */
  public Short getModelIdentifier() {
    return modelIdentifier;
  }

  /**
   * Retrieve the pacing frequency.
   * 
   * @return The pacing frequency.
   */
  public BigDecimal getPacingFrequency() {
    return pacingFrequency;
  }

  /**
   * Retrieve the max. pacing time.
   * 
   * @return The max. pacing time.
   */
  public BigDecimal getPacingMaxTime() {
    return pacingMaxTime;
  }

  /**
   * Retrieve the maximum plasma concentration.
   * <p>
   * Applicable only for simulation input of type {@link InputOption#CONC_RANGE}.
   * 
   * @return The maximum plasma concentration.
   */
  public BigDecimal getPlasmaConcMax() {
    return plasmaConcMax;
  }

  /**
   * Retrieve the minimum plasma concentration.
   * <p>
   * Applicable only for simulation input of type {@link InputOption#CONC_RANGE}.
   * 
   * @return The minimum plasma concentration.
   */
  public BigDecimal getPlasmaConcMin() {
    return plasmaConcMin;
  }

  /**
   * Retrieve the (plasma concentrations) intermediate point count.
   * <p>
   * Applicable only for simulation input of type {@link InputOption#CONC_RANGE}.
   * 
   * @return The intermediate point count (or {@code null} if none supplied).
   */
  public Short getPlasmaIntPtCount() {
    return plasmaIntPtCount;
  }

  /**
   * Indicator of whether user wants plasma increments based on log scale or not.
   * 
   * @return {@code true} if log scale requested (or by default if not specified),
   *         otherwise {@code false}.
   */
  public boolean getPlasmaIntPtLogScale() {
    return plasmaIntPtLogScale;
  }

  /**
   * Retrieve the individually assigned plasma concentration points.
   * <p>
   * Applicable only for simulation input of type {@link InputOption#CONC_POINTS}.
   * 
   * @return Unmodifiable plasma concentration point collection, or empty
   *         collection if none assigned.
   */
  public List<String> getPlasmaConcPoints() {
    return Collections.unmodifiableList(plasmaConcPoints);
  }

  /**
   * Retrieve the CellML file name.
   * 
   * @return The CellML file name (or {@code null} if not assigned).
   */
  public String getCellMLFileName() {
    return cellMLFileName;
  }

  /**
   * Retrieve the CellML file.
   * 
   * @return The CellML file (or {@code null} if not assigned).
   */
  public PortalFile getCellMLFile() {
    return cellMLFile;
  }

  /**
   * Retrieve the PK file.
   * <p>
   * Applicable only for simulation input of type {@link InputOption#PK}.
   * 
   * @return The PK file (or {@code null} if not assigned).
   */
  public PortalFile getPkFile() {
    return pkFile;
  }

  /**
   * Retrieve Delta APD90 credible interval percentiles.
   * 
   * @return Delta APD90 credible interval percentiles (or {@code null} if not
   *         assigned).
   */
  public String getCredibleIntervalPctiles() {
    return credibleIntervalPctiles;
  }

  /**
   * Retrieve the simulation notes.
   * 
   * @return The simulation notes (or {@code null} if none provided).
   */
  public String getNotes() {
    return notes;
  }

  /**
   * Retrieve value assignment problems (if any).
   * 
   * @return Collection of problems, or empty collection if none encountered.
   */
  public Collection<String> getAssignmentProblems() {
    return Collections.unmodifiableCollection(assignmentProblems);
  }
}