/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.value;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;

/**
 * Simulation results value object.
 *
 * @author geoff
 */
public class ResultsVO {

  /**
   * Hold defined structure representation of the simulation results.
   * 
   * @author geoff
   */
  public class PerReferenceDataVO {
    private final BigDecimal reference;
    private final Map<String, Object> referenceData = new HashMap<String, Object>();

    private PerReferenceDataVO(final BigDecimal reference,
                               final Map<String, Object> referenceData) { 
      this.reference = reference;
      if (referenceData != null && !referenceData.isEmpty()) {
        this.referenceData.putAll(referenceData);
      }
    }

    /**
     * Retrieve the reference, e.g. compound concentration.
     * 
     * @return Reference, e.g. compound concentration (in μM).
     */
    public BigDecimal getReference() {
      return reference;
    }

    /**
     * Retrieve the APD90 value.
     * 
     * @return APD90 value, or {@code null} if not provided.
     */
    public String retrieveAPD90() {
      return (String) referenceData.get(ClientDirectIdentifiers.KEY_RESULTS_PERREF_APD90);
    }

    /**
     * Retrieve the Delta APD90 value.
     * 
     * @return Delta APD90 value, or {@code null} if not provided.
     */
    public String retrieveDeltaAPD90() {
      return (String) referenceData.get(ClientDirectIdentifiers.KEY_RESULTS_PERREF_DELTA_APD90);
    }

    /**
     * Retrieve the qNet value.
     * 
     * @return qNet value, or {@code null} if not provided.
     */
    public String retrieveQNet() {
      return (String) referenceData.get(ClientDirectIdentifiers.KEY_RESULTS_PERREF_QNET);
    }

    /**
     * Retrieve the voltage trace times.
     * 
     * @return Voltage trace times (as CSV string), or {@code null} if not provided.
     */
    public String retrieveVoltageTraceTimes() {
      return (String) referenceData.get(ClientDirectIdentifiers.KEY_RESULTS_PERREF_TIMES);
    }

    /**
     * Retrieve the voltage trace voltages.
     * 
     * @return Voltage trace voltages (as CSV string), or {@code null} if not provided.
     */
    public String retrieveVoltageTraceVoltages() {
      return (String) referenceData.get(ClientDirectIdentifiers.KEY_RESULTS_PERREF_VOLTAGES);
    }
  }

  // Ordered according to ordering of the incoming data.
  private final List<PerReferenceDataVO> perReferenceData = new ArrayList<PerReferenceDataVO>();
  private final Map<String, Object> results = new HashMap<String, Object>();

  private static final Log log = LogFactory.getLog(ResultsVO.class);

  /**
   * Initialising constructor.
   * 
   * @param results Simulation results.
   */
  public ResultsVO(final Map<String, Object> results) {
    if (results != null) {
      this.results.putAll(results);
      if (results.containsKey(ClientDirectIdentifiers.KEY_RESULTS_REFERENCE_DATA)) {
        @SuppressWarnings("unchecked")
        final Map<BigDecimal, Map<String, Object>> resultsReferenceData = (Map<BigDecimal, Map<String, Object>>)
                                                                           results.get(ClientDirectIdentifiers.KEY_RESULTS_REFERENCE_DATA);
        if (resultsReferenceData != null) {
          for (final Map.Entry<BigDecimal, Map<String, Object>> eachResultsReferenceData :
                                                                resultsReferenceData.entrySet()) {
            if (eachResultsReferenceData.getKey() != null &&
                eachResultsReferenceData.getValue() != null) {
              // <reference, <key, value>>, where key could be ClientDirectIdentifiers.KEY_RESULTS_PERREF_VOLTAGES, etc
              perReferenceData.add(new PerReferenceDataVO(eachResultsReferenceData.getKey(),
                                                          eachResultsReferenceData.getValue()));
            }
          }
        } else {
          log.warn("~ResultsVO() : Map key '" + ClientDirectIdentifiers.KEY_RESULTS_REFERENCE_DATA + "' found but null value encountered!");
        }
      }
    }
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "ResultsVO [results=" + results + "]";
  }

  /**
   * Retrieve the simulation results.
   * <p>
   * Data retrieved here is as-per assigned and the map represents data objects
   * keyed on, forexample,
   * {@link ClientDirectIdentifiers#KEY_RESULTS_REFERENCE_DATA}.
   * 
   * @return Unmodifiable Map of the results, or empty if none available.
   */
  public Map<String, Object> getResults() {
    return Collections.unmodifiableMap(results);
  }

  /**
   * Indicate presence of per-reference data.
   * 
   * @return {@code true} if reference data available, otherwise {@code false}.
   */
  public boolean hasPerReferenceData() {
    return !perReferenceData.isEmpty();
  }

  /**
   * Indicate presence of results data.
   * 
   * @return {@code true} if results data available, otherwise {@code false}.
   */
  public boolean hasResults() {
    return !results.isEmpty();
  }

  /**
   * Retrieve the per-reference data, i.e. a collection of data values 
   * associated with a particular compound concentration, e.g. the DeltaAPD90 at
   * concentration 3.0µM.
   * 
   * @param sortedByReference {@code true} if reference data sorted by reference.
   * @return Per-reference data (sorted by reference if requested), or empty
   *         collection if none available.
   * @see #hasPerReferenceData()
   */
  public List<PerReferenceDataVO> retrievePerReferenceData(final boolean sortedByReference) {
    List<PerReferenceDataVO> perReferenceData = null;
    if (sortedByReference) {
      if (hasPerReferenceData()) {
        @SuppressWarnings("unchecked")
        final Map<BigDecimal, Map<String, Object>> resultsReferenceData = (Map<BigDecimal, Map<String, Object>>)
                                                                           results.get(ClientDirectIdentifiers.KEY_RESULTS_REFERENCE_DATA);
        // Transfer to TreeMap. Ignore BigDecimal natural sorting / equality issues.
        final Map<BigDecimal, Map<String, Object>> sortedResultsReferenceData =
                                                   new TreeMap<BigDecimal, Map<String, Object>>(resultsReferenceData);
        final List<PerReferenceDataVO> sortedPerReferenceData = new ArrayList<PerReferenceDataVO>();
        for (final Map.Entry<BigDecimal, Map<String, Object>> eachSorted : sortedResultsReferenceData.entrySet()) {
          sortedPerReferenceData.add(new PerReferenceDataVO(eachSorted.getKey(),
                                                            eachSorted.getValue()));
        }
        perReferenceData = sortedPerReferenceData;
      } else {
        perReferenceData = new ArrayList<PerReferenceDataVO>();
      }
    } else {
      perReferenceData = this.perReferenceData;
    }
    return perReferenceData;
  }
}