/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the names of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct;

/**
 * Client-direct identifiers.
 * 
 * @author geoff
 */
public final class ClientDirectIdentifiers {

  /** Form actions */
  public static final String ACTION_RESULTS_DELETE = "delete";
  public static final String ACTION_RESULTS_VIEW = "results";
  public static final String ACTION_SIMULATION_NEW = "new";

  public static final String COMPONENT_APP_MANAGER_MANAGER = "appManagerManager";
  public static final String COMPONENT_APP_MANAGER_SERVICE = "appManagerService";
  /** Component name for the App Manager WS WSS4J security interceptor
      <p>
      See also (sample.)appCtx.ws.security-outgoing.xml */
  public static final String COMPONENT_APP_MANAGER_SERVICES_INTERCEPTOR = "wsAppManagerServicesInterceptor";
  public static final String COMPONENT_CLIENT_DIRECT_SERVICE = "clientDirectService";
  public static final String COMPONENT_CONFIGURATION = "configuration";
  public static final String COMPONENT_CONFIGURATION_MANAGER = "configurationManager";
  public static final String COMPONENT_PERMISSION_MANAGER = "permissionManager";
  public static final String COMPONENT_SIMULATION_DAO = "simulationDAO";
  public static final String COMPONENT_SIMULATION_MANAGER = "simulationManager";

  /** These two key values correspond to the data structure returned by AJAX controller */
  public static final String KEY_JSON = "json";
  public static final String KEY_EXCEPTION = "exception";

  public static final String KEY_PKRESULTS_TIMEPOINT_DATA = "timepointData";
  public static final String KEY_PKRESULTS_PERTIMEPOINT_APD90S = "apd90s";
  public static final String KEY_PKRESULTS_PERTIMEPOINT_FORTRANS = "forTranslation";

  public static final String KEY_RESULTS_DELTA_APD90_PERCENTILE_NAMES = "deltaAPD90PercentileNames";
  public static final String KEY_RESULTS_FOR_TRANSLATION = "forTranslation";
  public static final String KEY_RESULTS_JOB_ID = "jobId";
  public static final String KEY_RESULTS_MESSAGES = "messages";
  public static final String KEY_RESULTS_INPUT_OPTION = "inputOption";
  public static final String KEY_RESULTS_REFERENCE_DATA = "referenceData";
  /** This value corresponds to the ResultsVO property "results" */
  public static final String KEY_RESULTS_RESULTS = "results";

  public static final String KEY_RESULTS_PERREF_APD90 = "apd90";
  public static final String KEY_RESULTS_PERREF_DELTA_APD90 = "deltaAPD90";
  public static final String KEY_RESULTS_PERREF_QNET = "qNet";
  public static final String KEY_RESULTS_PERREF_TIMES = "times";
  public static final String KEY_RESULTS_PERREF_UPSTROKE_VELOCITY = "upstrokeVelocity";
  public static final String KEY_RESULTS_PERREF_VOLTAGES = "voltages";

  public static final String MODEL_ATTRIBUTE_C50_UNITS = "ma_c50units";
  public static final String MODEL_ATTRIBUTE_INPUT_VALUES = "ma_input_values";
  public static final String MODEL_ATTRIBUTE_LATEST_PROGRESS = "ma_latest_progress";
  public static final String MODEL_ATTRIBUTE_MODELS = "ma_models";
  public static final String MODEL_ATTRIBUTE_PCTILES = "ma_pctiles";
  public static final String MODEL_ATTRIBUTE_SIMULATIONS = "ma_simulations";
  public static final String MODEL_ATTRIBUTE_SPREADS = "ma_spreads";
  public static final String MODEL_ATTRIBUTE_TOKEN = "ma_token";

  /* Update tiles.xml if any of these properties change */
  public static final String PAGE_DEFAULT_ERROR = "page_default_error";
  public static final String PAGE_INPUT = "page_input";
  public static final String PAGE_MAIN = "page_main";
  public static final String PAGE_RESULTS = "page_results";

  /* If some of the channel param names are changed then check 
     input/html_head/javascript.jsp files for references! */
  public static final String PARAM_NAME_ICAL_C50 = "icalC50";
  public static final String PARAM_NAME_ICAL_HILL = "icalHill";
  public static final String PARAM_NAME_ICAL_SATURATION = "icalSaturation";
  public static final String PARAM_NAME_ICAL_SPREAD = "icalSpread";
  public static final String PARAM_NAME_IKR_C50 = "ikrC50";
  public static final String PARAM_NAME_IKR_HILL = "ikrHill";
  public static final String PARAM_NAME_IKR_SATURATION = "ikrSaturation";
  public static final String PARAM_NAME_IKR_SPREAD = "ikrSpread";
  public static final String PARAM_NAME_IK1_C50 = "ik1C50";
  public static final String PARAM_NAME_IK1_HILL = "ik1Hill";
  public static final String PARAM_NAME_IK1_SATURATION = "ik1Saturation";
  public static final String PARAM_NAME_IK1_SPREAD = "ik1Spread";
  public static final String PARAM_NAME_IKS_C50 = "iksC50";
  public static final String PARAM_NAME_IKS_HILL = "iksHill";
  public static final String PARAM_NAME_IKS_SATURATION = "iksSaturation";
  public static final String PARAM_NAME_IKS_SPREAD = "iksSpread";
  public static final String PARAM_NAME_ITO_C50 = "itoC50";
  public static final String PARAM_NAME_ITO_HILL = "itoHill";
  public static final String PARAM_NAME_ITO_SATURATION = "itoSaturation";
  public static final String PARAM_NAME_ITO_SPREAD = "itoSpread";
  public static final String PARAM_NAME_INA_C50 = "inaC50";
  public static final String PARAM_NAME_INA_HILL = "inaHill";
  public static final String PARAM_NAME_INA_SATURATION = "inaSaturation";
  public static final String PARAM_NAME_INA_SPREAD = "inaSpread";
  public static final String PARAM_NAME_INAL_C50 = "inalC50";
  public static final String PARAM_NAME_INAL_HILL = "inalHill";
  public static final String PARAM_NAME_INAL_SATURATION = "inalSaturation";
  public static final String PARAM_NAME_INAL_SPREAD = "inalSpread";
  /* Other parameter names */
  public static final String PARAM_NAME_C50_TYPE = "c50_type";
  public static final String PARAM_NAME_IC50_UNITS = "ic50_units";
  public static final String PARAM_NAME_MODEL_IDENTIFIER = "model_identifier";
  public static final String PARAM_NAME_PACING_FREQUENCY = "pacing_frequency";
  public static final String PARAM_NAME_PACING_MAX_TIME = "pacing_max_time";
  public static final String PARAM_NAME_PCTILES = "credible_interval_pctiles";
  public static final String PARAM_NAME_PK_OR_CONCS = "pk_or_concs";
  public static final String PARAM_NAME_PLASMA_CONC_MAX = "plasma_conc_max";
  public static final String PARAM_NAME_PLASMA_CONC_MIN = "plasma_conc_min";
  public static final String PARAM_NAME_PLASMA_CONC_POINTS = "plasma_conc_points";
  public static final String PARAM_NAME_PLASMA_INTERMEDIATE_POINT_COUNT = "plasma_int_pt_cnt";
  public static final String PARAM_NAME_PLASMA_INTERMEDIATE_POINT_LOG = "plasma_int_pt_log";
  public static final String PARAM_NAME_NOTES = "notes";
  public static final String PARAM_NAME_SPREADS_ENABLED = "spreads_enabled";
  public static final String PARAM_NAME_TOKEN = "token";

  public static final String SESSION_PARAM_TOKEN = "session_token";

  /** Spring security ACL service - as named in application context */ 
  public static final String SPRING_SECURITY_ACL_SERVICE = "clientSharedACLService";

  public static final String URL_PREFIX_EXCEL = "excel/";
  public static final String URL_PREFIX_LATEST_PROGRESS = "ajax/latest_progress/";
  public static final String URL_PREFIX_RESULTS = "ajax/results/";

  public static final String VIEW_NAME_EXCEL = "viewExcel";

  public static final String VIEW_MAIN = "main";
  public static final String VIEW_RESULTS = "results";

  // TODO: Make this configurable!
  public static final String APP_MANAGER_OUT_OF_CONTACT = "appmanager.out_of_contact";
  // TODO: Make this configurable!
  public static final String APP_MANAGER_SOMETHING_WRONG = "appmanager.something_wrong";

  public static final String RESUBMISSION_MSG_BUNDLE_ID = "error.resubmission";
  public static final String UNAVAILABLE_MSG_BUNDLE_ID = "error.simulation_unavailable";

  private ClientDirectIdentifiers() {}
}