/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.manager;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunResponse.Response;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApplicationStatusRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApplicationStatusResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.BusinessDataUploadCompletedRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ObjectFactory;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.PKResults;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.Results;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllResultsRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllResultsResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllStatusesRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllStatusesResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.StatusRecord;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Job;
import uk.ac.ox.cs.nc3rs.client_direct.entity.JobPKResult;
import uk.ac.ox.cs.nc3rs.client_direct.entity.JobResult;
import uk.ac.ox.cs.nc3rs.client_direct.exception.RunInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.value.StatusVO;
import uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.proxy.AppManagerProxy;
import uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.service.AppManagerService;

/**
 * Implementation of App Manager management tasks.
 * 
 * @author geoff
 */
@Component(ClientDirectIdentifiers.COMPONENT_APP_MANAGER_MANAGER)
public class AppManagerManagerImpl implements AppManagerManager {

  @Autowired
  private AppManagerProxy appManagerProxy;

  // defined in appCtx.appManager.ws.xml
  private Map<String, String> appManagerCodes = new HashMap<String, String>();

  private static final ObjectFactory objectFactory = new ObjectFactory();

  private static final Log log = LogFactory.getLog(AppManagerManagerImpl.class);


  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.AppManagerManager#retrieveProgress(long)
   */
  @Override
  public List<StatusVO> retrieveProgress(final long appManagerId) {
    log.debug("~retrieveProgress() : [?][" + appManagerId + "] : Invoked.");

    final RetrieveAllStatusesRequest retrieveAllStatusesRequest = objectFactory.createRetrieveAllStatusesRequest();
    retrieveAllStatusesRequest.setAppManagerId(appManagerId);

    // Make the web service call
    final RetrieveAllStatusesResponse retrieveAllStatusesResponse = appManagerProxy.retrieveAllProgress(retrieveAllStatusesRequest);

    final List<StatusVO> progressStatuses = new LinkedList<StatusVO>();
    for (final StatusRecord statusRecord : retrieveAllStatusesResponse.getStatusRecord()) {
      final StatusVO status = new StatusVO(statusRecord.getEntered(),
                                           statusRecord.getStatus(),
                                           statusRecord.getLevel());
      if (status.isProgressStatus()) {
        progressStatuses.add(status);
      }
    }

    return Collections.unmodifiableList(progressStatuses);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.AppManagerManager#retrieveResults(long)
   */
  @Override
  public Job retrieveResults(final long appManagerId) {
    final String logPrefix = "~retrieveResults() : [" + appManagerId + "] : ";
    log.debug(logPrefix.concat("Invoked."));

    final RetrieveAllResultsRequest retrieveAllResultsRequest = objectFactory.createRetrieveAllResultsRequest();
    retrieveAllResultsRequest.setAppManagerId(appManagerId);

    // Make the web service call
    final RetrieveAllResultsResponse retrieveAllResultsResponse = appManagerProxy.retrieveAllResults(retrieveAllResultsRequest);
    final long responseAppManagerId = retrieveAllResultsResponse.getAppManagerId();
    final String messages = retrieveAllResultsResponse.getMessages();
    final String deltaAPD90PercentileNames = retrieveAllResultsResponse.getDeltaAPD90PercentileNames();
    final List<Results> results = retrieveAllResultsResponse.getResults();
    final List<PKResults> pkResults = retrieveAllResultsResponse.getPKResults();

    final Job job = new Job(responseAppManagerId);
    job.setMessages(messages);
    job.setDeltaAPD90PercentileNames(deltaAPD90PercentileNames);

    if (results != null) {
      for (final Results result : results) {
        /*
         * Prior to June 2020 the process here was to detect the presence of
         * NoActionPotential_X values in APD90 or Δ APD90 values (derived from
         * ApPredict's voltage_results.dat output), and provide a translatable
         * and more meaningful output than that appearing in messages.txt.
         * It's since been decided that this is no longer necessary as the
         * messages.txt file is now providing all the necessary detail.
         */
        final boolean forTranslation = false;

        job.addJobResult(new JobResult(result.getReference(), result.getTimes(),
                                       result.getVoltages(), result.getAPD90(),
                                       result.getDeltaAPD90(), result.getQNet(),
                                       forTranslation));
      }
    }

    if (pkResults != null) {
      for (final PKResults pkResult : pkResults) {
        final BigDecimal timepoint = pkResult.getTimepoint();
        final String apd90s = pkResult.getAPD90S();
        if (timepoint != null && !StringUtils.isBlank(apd90s)) {
          /*
           * Prior to June 2020 the process here was to detect the presence of
           * NoActionPotential_X values in APD90 values (derived from
           * ApPredict's voltage_results.dat output), and provide a translatable
           * and more meaningful output than that appearing in messages.txt.
           * It's since been decided that this is no longer necessary as the
           * messages.txt file is now providing all the necessary detail.
           */
          final boolean forTranslation = false;

          job.addJobPKResult(new JobPKResult(timepoint, apd90s, forTranslation));
        }
      }
    }

    return job;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.AppManagerManager#retrieveWorkload()
   */
  @Override
  public String retrieveWorkload() {
    log.debug("~retrieveWorkload() : [?][?] : Invoked.");

    final ApplicationStatusRequest applicationStatusRequest = objectFactory.createApplicationStatusRequest();
    applicationStatusRequest.setData(APP_MANAGER_WORKLOAD_IDENTIFIER);

    final ApplicationStatusResponse applicationStatusResponse = appManagerProxy.applicationStatusRequest(applicationStatusRequest);

    return applicationStatusResponse.getStatus();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.AppManagerManager#runApPredict(uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunRequest)
   */
  @Override
  public long runApPredict(final ApPredictRunRequest runRequest)
                           throws RunInvocationException {
    final String logPrefix = "~runApPredict() : [?][?] : ";
    log.debug(logPrefix.concat("Invoked."));

    final ApPredictRunResponse runResponse = appManagerProxy.invokeRunRequest(runRequest);
    final Response response = runResponse.getResponse();

    final String responseCode = response.getResponseCode();
    if (responseCode != null) {
      // Default error message
      String errorMessage = "Unrecognised App Manager response code of '" + responseCode + "' encountered";

      final String messageKey = appManagerCodes.get(responseCode);
      if (messageKey != null && AppManagerService.APP_MANAGER_CAPACITY_BUNDLE_IDENTIFIER.equals(messageKey)) {
        log.info(logPrefix.concat("App Manager indicates that operating at capacity."));
        errorMessage = messageKey;
      }

      throw new RunInvocationException(errorMessage);
    }

    final Long appManagerId = response.getAppManagerId();
    if (appManagerId != null) {
      log.debug("~runApPredict() : [?][" + appManagerId + "] : app-manager id returned.");
      return appManagerId;
    } else {
      throw new RunInvocationException("Problem! The simulation invocation has failed to generate an identifier.");
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.AppManagerManager#signalResultsUploaded(long)
   */
  @Override
  public void signalResultsUploaded(long appManagerId) {
    final BusinessDataUploadCompletedRequest completedNotificationRequest = objectFactory.createBusinessDataUploadCompletedRequest();
    completedNotificationRequest.setAppManagerId(appManagerId);

    appManagerProxy.signalResultsUploaded(completedNotificationRequest);
  }

  /**
   * (Spring-injected) App Manager codes.
   * 
   * @param appManagerCodes App Manager codes.
   */
  // see appCtx.appManager.ws.xml
  @Value("#{appManagerCodes}")
  public void setAppManagerCodes(final Map<String, String> appManagerCodes) {
    this.appManagerCodes.clear();
    if (appManagerCodes != null) {
      this.appManagerCodes.putAll(appManagerCodes);
    }
  }
}