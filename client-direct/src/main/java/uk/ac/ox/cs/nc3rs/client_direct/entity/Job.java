/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.entity;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Simulation job (representing an App Manager invocation of ApPredict).
 *
 * @author geoff
 */
@Entity
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE,
                                 region = "JobCache")
public class Job implements Serializable {

  private static final long serialVersionUID = 7700021759274065428L;

  /* Consistent property name */
  public static final String PROPERTY_APP_MANAGER_ID = "appManagerId";

  // Persistence identity (Surrogate primary key)
  @TableGenerator(name="job_id_gen", table="sequence_pks_client_direct",
                  pkColumnName="pk_seq_name", pkColumnValue="job_id",
                  valueColumnName="pk_seq_value", allocationSize=1)
  @Id @GeneratedValue(strategy=GenerationType.TABLE, generator="job_id_gen")
  private Long id;

  @JsonIgnore
  @Column(insertable=true, nullable=false, updatable=false)
  private Long appManagerId;

  /*
   * If the job generated messages (e.g. "At conc of X cell did not repolarise").
   * Since PK simulations started this can grow into approx 100 lines of 100
   *   characters each!
   */
  @Lob
  @Column(insertable=false, length=65536, nullable=true, updatable=true)
  private String messages;

  /* CSV-format string (potentially a single value) of DeltaAPD90 percentile
     names.
     Percentile names will potentially differ between a Simulation's Jobs as
     some will have been run with uncertainties calculated, others not. */
  @Column(insertable=false, length=5120, nullable=true, updatable=true)
  private String deltaAPD90PercentileNames;

  @JsonIgnore
  // bidirectional Job [1] <-> [0..*] JobPKResult
  @OneToMany(cascade={ CascadeType.MERGE, CascadeType.REMOVE },
             fetch=FetchType.LAZY,
             mappedBy="job",
             orphanRemoval=true)
  @org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE,
                                   region = "JobPKResultCache")
  private Set<JobPKResult> jobPKResults = new HashSet<JobPKResult>();

  @JsonIgnore
  // bidirectional Job [1] <-> [0..*] JobResult
  @OneToMany(cascade={ CascadeType.MERGE, CascadeType.REMOVE },
             fetch=FetchType.LAZY,
             mappedBy="job",
             orphanRemoval=true)
  @org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE,
                                   region = "JobResultCache")
  private Set<JobResult> jobResults = new HashSet<JobResult>();

  // Optimistic locking concurrency control
  @Version
  private Long lockVersion;

  /** <b>Do not invoke directly.</b> */
  protected Job() {}

  /**
   * Initialising constructor.
   * 
   * @param appManagerId App Manager identifier.
   */
  public Job(final long appManagerId) {
    this.appManagerId = appManagerId;
  }

  /**
   * Add a job PK result to the collection.
   * 
   * @param jobPKResult Job PK result to add.
   */
  public void addJobPKResult(final JobPKResult jobPKResult) {
    jobPKResults.add(jobPKResult);
  }

  /**
   * Add a job result to the collection.
   * 
   * @param jobResult Job result to add.
   */
  public void addJobResult(final JobResult jobResult) {
    jobResults.add(jobResult);
  }

  /**
   * Indicates presence of job PK results.
   * 
   * @return True if job has PK results attached.
   */
  public boolean hasPKResults() {
    return (!jobPKResults.isEmpty());
  }

  /**
   * Indicates presence of job results.
   * 
   * @return True if job has results attached.
   */
  public boolean hasResults() {
    return (!jobResults.isEmpty());
  }

  /**
   * Retrieve copies of the JobResults attached to this Job, but without the persistence
   * relationships.
   * 
   * @return Collection of job results.
   */
  @SuppressWarnings("deprecation")
  public Set<JobResult> retrieveJobResultCopies() {
    final Set<JobResult> copiedJobResults = new HashSet<JobResult>(getJobResults().size());
    for (final JobResult jobResult : getJobResults()) {
      copiedJobResults.add(new JobResult(jobResult.getReference(),
                                         jobResult.getTimes(),
                                         jobResult.getVoltages(),
                                         jobResult.getAPD90(),
                                         jobResult.getDeltaAPD90(),
                                         jobResult.getQNet(),
                                         jobResult.getForTranslation()));
    }
    return copiedJobResults;
  }

  /**
   * Retrieve copies of the JobPKResults attached to this Job, but without the 
   * persistence relationships.
   * 
   * @return Collection of job PK results.
   */
  public Set<JobPKResult> retrieveJobPKResultCopies() {
    final Set<JobPKResult> copiedJobPKResults = new HashSet<JobPKResult>(getJobPKResults().size());
    for (final JobPKResult jobPKResult : getJobPKResults()) {
      copiedJobPKResults.add(new JobPKResult(jobPKResult.getTimepoint(),
                                             jobPKResult.getApd90s(),
                                             jobPKResult.isForTranslation()));
    }
    return copiedJobPKResults;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Job [id=" + id + ", appManagerId=" + appManagerId + ", messages="
        + messages + ", deltaAPD90PercentileNames=" + deltaAPD90PercentileNames
        + ", lockVersion=" + lockVersion + "]";
  }

  /**
   * Non-unique app manager identifier (not known until job received by app manager).
   * In general this value would be expected to be unique but in certain circumstances (such as 
   *   the App Manager being restarted and the id's being reset) it may not be. This should not be
   *   an issue for finished simulations as this value is only used when a simulation is being
   *   actively run - once finished the App Manager id is redundant.
   * 
   * @return The App Manager identifier.
   */
  public long getAppManagerId() {
    return appManagerId;
  }

  /**
   * Retrieve the Delta APD90 percentile names (in the form of a CSV string).
   * 
   * @return Delta APD90 percentile names.
   */
  public String getDeltaAPD90PercentileNames() {
    return deltaAPD90PercentileNames;
  }

  /**
   * Assign the Delta APD90 percentile names.
   * 
   * @param deltaAPD90PercentileNames Delta APD90 percentile names to set.
   */
  public void setDeltaAPD90PercentileNames(final String deltaAPD90PercentileNames) {
    this.deltaAPD90PercentileNames = deltaAPD90PercentileNames;
  }

  /**
   * Retrieve the jobs PK results.
   * 
   * @return Unmodifiable collection of job PK results (empty collection if none available).
   */
  public Set<JobPKResult> getJobPKResults() {
    return Collections.unmodifiableSet(jobPKResults);
  }

  /**
   * Retrieve the jobs results.
   * 
   * @return Unmodifiable collection of job results (empty collection if none available).
   */
  public Set<JobResult> getJobResults() {
    return Collections.unmodifiableSet(jobResults);
  }

  /**
   * Retrieve the job's messages.
   * 
   * @return Job messages, optionally {@code null}.
   */
  public String getMessages() {
    return messages;
  }

  /**
   * Assign the job's messages.
   * 
   * @param messages Job messages, optionally {@code null}.
   */
  public void setMessages(final String messages) {
    this.messages = messages;
  }
}