/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.dao;

import java.util.List;

import uk.ac.ox.cs.nc3rs.client_direct.entity.Job;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation;

/**
 * Interface for the Simulation Data Access Object.
 * 
 * @author Geoff Williams
 */
public interface SimulationDAO {

  /**
   * Load up Job's dependent objects.
   * 
   * @param job Parent Job.
   */
  void avoidJobLazyInit(Job job);

  /**
   * Remove the simulation (if it's completed, otherwise the request is silently ignored).
   * 
   * @param simulationId Simulation identifier.
   */
  void deleteSimulation(long simulationId);

  /**
   * Retrieve a simulation with the specified id.
   * 
   * @param simulationId Persistence simulation id.
   * @return Simulation with requested simulation id, or null if not found.
   */
  Simulation findBySimulationId(long simulationId);

  /**
   * Retrieve a collection of the user's Simulations in the database.
   *
   * @param user Simulation creator.
   * @return Simulations which the specified user created (or empty list if none found).
   */
  List<Simulation> findByUser(String user);

  /**
   * Retrieve all simulations.
   * 
   * @return Persisted simulations (or empty list if none found).
   */
  List<Simulation> findSimulations();

  /**
   * Persist a simulation.
   * 
   * @param simulation Simulation to persist.
   * @return The managed (stored) simulation.
   */
  Simulation store(Simulation simulation);

  /**
   * Store the results of a simulation job.
   * 
   * @param job Job to store.
   * @return Persisted Job.
   */
  Job storeResults(Job job);

}