/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.service;

import java.util.Collections;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunRequest;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Job;
import uk.ac.ox.cs.nc3rs.client_direct.exception.AppManagerWSInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.NoConnectionException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.RunInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.StatusVO;
import uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.manager.AppManagerManager;
import uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.util.AppManagerUtil;

/**
 * Implementation of the App Manager service.
 *
 * @author geoff
 */
@Component(ClientDirectIdentifiers.COMPONENT_APP_MANAGER_SERVICE)
public class AppManagerServiceImpl implements AppManagerService {

  @Autowired @Qualifier(ClientDirectIdentifiers.COMPONENT_APP_MANAGER_MANAGER)
  private AppManagerManager appManagerManager;

  private static final Log log = LogFactory.getLog(AppManagerServiceImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.AppManagerService#retrieveLatestCompletedProgress(long)
   */
  @Override
  public String retrieveLatestCompletedProgress(final long appManagerId)
                                                throws AppManagerWSInvocationException,
                                                       NoConnectionException {
    log.debug("~retrieveLatestCompletedProgress() : [" + appManagerId + "] : Invoked.");

    final List<StatusVO> allProgress = retrieveProgress(appManagerId);
    String latestProgress = null;
    for (final StatusVO eachStatus : allProgress) {
      if (eachStatus.isProgressStatus() && !eachStatus.isProgressFileNotFoundStatus()) {
        // don't show "progress file not found" progress!
        latestProgress = eachStatus.getStatus();
        break;
      }
    }

    return latestProgress;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.AppManagerService#retrieveProgress(long)
   */
  @Override
  public List<StatusVO> retrieveProgress(final long appManagerId)
                                         throws AppManagerWSInvocationException,
                                                NoConnectionException {
    log.debug("~retrieveProgress() : [" + appManagerId + "] : Invoked.");

    return Collections.unmodifiableList(appManagerManager.retrieveProgress(appManagerId));
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.AppManagerService#retrieveResults(long)
   */
  @Override
  public Job retrieveResults(final long appManagerId)
                             throws AppManagerWSInvocationException,
                                    NoConnectionException {  
    log.debug("~retrieveResults() : [" + appManagerId + "] : Invoked.");

    return appManagerManager.retrieveResults(appManagerId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.AppManagerService#retrieveWorkload()
   */
  @Override
  public String retrieveWorkload() throws AppManagerWSInvocationException,
                                          NoConnectionException {
    log.debug("~retrieveWorkload() : Invoked.");

    return appManagerManager.retrieveWorkload();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.AppManagerService#runSimulation(uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO)
   */
  @Override
  public long runSimulation(final SimulationInputVO simulationInput)
                            throws AppManagerWSInvocationException,
                                   NoConnectionException,
                                   RunInvocationException {
    log.debug("~runSimulation() : Invoked.");
    final ApPredictRunRequest runRequest = AppManagerUtil.createRunRequest(simulationInput);

    return appManagerManager.runApPredict(runRequest);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.AppManagerService#signalResultsUploaded(long)
   */
  @Override
  public void signalResultsUploaded(long appManagerId) throws AppManagerWSInvocationException,
                                                              NoConnectionException {
    log.debug("~signalResultsUploaded() : [" + appManagerId + "] : Invoked.");

    appManagerManager.signalResultsUploaded(appManagerId);
  }
}