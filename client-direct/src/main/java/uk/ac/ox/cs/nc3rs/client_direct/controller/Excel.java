/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.controller.util.ControllerUtil;
import uk.ac.ox.cs.nc3rs.client_direct.exception.AppManagerWSInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.NoConnectionException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.ResultsNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.SimulationNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationCurrentResultsVO;

/**
 * Controller returning Excel views.
 *
 * @author geoff
 */
@Controller
public class Excel {

  private static final String errorInvalidSimulationId = "Invalid simulation identifier provided";

  @Autowired @Qualifier(ClientDirectIdentifiers.COMPONENT_CLIENT_DIRECT_SERVICE)
  private ClientDirectService clientDirectService;

  private static final Log log = LogFactory.getLog(Excel.class);

  /**
   * Generate an excel view.
   * 
   * @param simulationId simulation identifier.
   * @param model UI model.
   * @return View name.
   */
  @RequestMapping(value=ClientDirectIdentifiers.URL_PREFIX_EXCEL,
                  method=RequestMethod.GET)
  public String generateExcelView(final @RequestParam(value=ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID,
                                                      required=true)
                                        String simulationId,
                                  final Model model) {
    log.debug("~generateExcelView() : Invoked for simulation id '" + simulationId + "'.");

    if (!ControllerUtil.validSimulationId(simulationId)) {
      log.debug("~generateExcelView() : Invalid simulation id of '" + simulationId + "' provided!");
      model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                         errorInvalidSimulationId);
    } else {
      final long useSimulationId = Long.valueOf(simulationId);
      model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                         useSimulationId);
      model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MODEL_NAMES,
                         clientDirectService.retrieveModelNames());

      try {
        final SimulationCurrentResultsVO simulationCurrentResults = 
              clientDirectService.retrieveSimulationResults(useSimulationId);
        model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                           simulationCurrentResults);
        log.debug("~generateExcelView() : Simulation and current results retrieved without generating exception.");
      } catch (SimulationNotFoundException e) {
        log.warn("~generateExcelView() : Simulation '" + simulationId + "' not found '" + e.getMessage() + "'.");
        model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                           e.getMessage());
      } catch (AppManagerWSInvocationException e) {
        log.warn("~generateExcelView() : Simulation '" + simulationId + "'. App Manager WS invocation exception '" + e.getMessage() + "'.");
        model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                           e.getMessage());
      } catch (NoConnectionException e) {
        log.warn("~generateExcelView() : Simulation '" + simulationId + "'. No connection exception '" + e.getMessage() + "'.");
        model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                           e.getMessage());
      } catch (ResultsNotFoundException e) {
        log.warn("~generateExcelView() : Simulation '" + simulationId + "'. Results not found exception '" + e.getMessage() + "'.");
        model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                           e.getMessage());
      }
    }

    return ClientDirectIdentifiers.VIEW_NAME_EXCEL;
  }
}