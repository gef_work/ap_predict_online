/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Per-reference, e.g. compound concentration, results from a simulation.
 *
 * @author geoff
 */
@Entity
@Table(uniqueConstraints = {
  @UniqueConstraint(columnNames = { "jc_jobid", "reference" },
                    name="unique_jobid_reference")
})
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy.READ_ONLY,
                                 region = "JobResultCache")
public class JobResult implements Serializable {

  private static final long serialVersionUID = 2684542726112868403L;

  /*
     Not using a table generator because usually at least 4-10 per-reference
       (i.e. per-concentration) values, and PK sims have 100. The default
       allocationSize is 50 which is almost a useful quantity.
     Using GenerationType.AUTO which defers strategy to persistence provider.
       HSQLDB defaults to GenerationType.IDENTITY, MySQL doesn't support
       GenerationType.SEQUENCE, uff!
   */
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO, generator="native")
  @org.hibernate.annotations.GenericGenerator(name="native", strategy="native")
  private Long id;

  @Column(insertable=true, nullable=false, updatable=false)
  private String reference;

  /* String of non-quoted csv times (MySQL seems to create a "longtext" (4Gb)
     field, HSQL a varchar(xxxx)!).
     At most there's up to 700 times (and corresponding voltages), each
     representing a point in the voltage trace charts */
  @Column(insertable=true, nullable=false, updatable=false)
  @org.hibernate.annotations.Type(type="text")
  private String times;

  /* String of non-quoted csv voltages (MySQL seems to create a "longtext"
    (4Gb) field, HSQL a varchar(xxxx)!).
     At most there's up to 700 voltages (and corresponding times), each
     representing a point in the voltage trace charts */
  @Column(insertable=true, nullable=false, updatable=false)
  @org.hibernate.annotations.Type(type="text")
  private String voltages;

  /* Sometimes an APD90 value may not be possible to calculate, in which case
     a string such as "cs_appredict.no_action_potential_2" may be written! */
  @Column(insertable=true, length=100, nullable=false, updatable=false)
  private String apd90;

  /* Sometimes a delta APD90 value may not be possible to calculate, in which
     case a string such as "NoActionPotential_2,NoActionPotential_2,NoActionPotential_2"
     may be written to this column (if spread are used)! */
  @Column(insertable=true, length=2000, nullable=false, updatable=false)
  private String deltaAPD90;

  @Column(insertable=true, length=2000, nullable=true, updatable=false)
  private String qNet;

  @JsonIgnore
  // True if (delta)APD90 properties holds value for client interface i18n translation.
  @Column(insertable=true, nullable=false, updatable=false)
  private Boolean forTranslation;

  // Bidirectional Job [1..*] <-> [1] JobResult
  @ManyToOne
  @JoinColumn(name="jc_jobid", insertable=true, nullable=false, updatable=false,
              foreignKey = @ForeignKey(name="fk_jobresult_jobid"))
  private Job job;

  /** <b>Do not invoke directly.</b> */
  protected JobResult() {};

  /**
   * Initialising constructor.
   * <p>
   * All values (except {@code qNet}) are mandatory.
   * 
   * @param reference Compound/Plasma concentration.
   * @param times CSV simulation times.
   * @param voltages CSV simulation voltages at corresponding time.
   * @param apd90 APD90 value. (Could be a CSV, single numeric value, or a
   *              String!)
   * @param deltaAPD90 Delta APD90 value. (Could be a CSV, single numeric value,
   *                   or a String!)
   * @param qNet (Optional) qNet.
   * @param forTranslation Indicator that APD90 and/or Δ APD90 value is a
   *                       string (e.g. a bundle identifier) for translation.
   * @throws IllegalArgumentException If {@code null} values passed as an
   *                                  argument, or if the voltage trace vars
   *                                  (times, voltages) have data arrays of
   *                                  different lengths.
   * @see #getForTranslation()
   */
  public JobResult(final BigDecimal reference, final String times,
                   final String voltages, final String apd90,
                   final String deltaAPD90, final String qNet,
                   final boolean forTranslation) {
    if (reference == null || times == null || voltages == null || apd90 == null
        || deltaAPD90 == null) {
      throw new IllegalArgumentException("Cannot create a job result with empty values!");
    }
    if (times.split(",").length != voltages.split(",").length) {
      throw new IllegalArgumentException("Cannot create a job results unless voltage trace times and voltages are of equal length!");
    }

    this.reference = reference.toPlainString();
    this.times = times;
    this.voltages = voltages;
    this.apd90 = apd90;
    this.deltaAPD90 = deltaAPD90;
    this.qNet = qNet;
    this.forTranslation = forTranslation;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "JobResult [id=" + id + ", reference=" + reference + ", times="
        + times + ", voltages=" + voltages + ", apd90=" + apd90
        + ", deltaAPD90=" + deltaAPD90 + ", qNet=" + qNet + ", forTranslation="
        + forTranslation + ", job=" + job + "]";
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((apd90 == null) ? 0 : apd90.hashCode());
    result = prime * result
        + ((deltaAPD90 == null) ? 0 : deltaAPD90.hashCode());
    result = prime * result
        + ((forTranslation == null) ? 0 : forTranslation.hashCode());
    result = prime * result + ((qNet == null) ? 0 : qNet.hashCode());
    result = prime * result + ((reference == null) ? 0 : reference.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    JobResult other = (JobResult) obj;
    if (apd90 == null) {
      if (other.apd90 != null)
        return false;
    } else if (!apd90.equals(other.apd90))
      return false;
    if (deltaAPD90 == null) {
      if (other.deltaAPD90 != null)
        return false;
    } else if (!deltaAPD90.equals(other.deltaAPD90))
      return false;
    if (forTranslation == null) {
      if (other.forTranslation != null)
        return false;
    } else if (!forTranslation.equals(other.forTranslation))
      return false;
    if (qNet == null) {
      if (other.qNet != null)
        return false;
    } else if (!qNet.equals(other.qNet))
      return false;
    if (reference == null) {
      if (other.reference != null)
        return false;
    } else if (!reference.equals(other.reference))
      return false;
    return true;
  }

  /**
   * Retrieve the reference, e.g. compound concentration.
   * 
   * @return Reference value.
   */
  public BigDecimal getReference() {
    return new BigDecimal(reference);
  }

  /**
   * Associate this result with the parent job, i.e. establish bi-directional
   * relationship.
   * 
   * @param job Parent job.
   */
  public void setJob(final Job job) {
    job.addJobResult(this);
    this.job = job;
  }

  /**
   * Retrieve the AP times. Units are ms (milliseconds).
   * 
   * @return CSV format series of times corresponding to CSV format series of
   *         membrane voltages.
   */
  public String getTimes() {
    return times;
  }

  /**
   * Retrieve the Membrane Voltages. Units are mV.
   *  
   * @return CSV format series of membrane voltages corresponding to CSV format
   *         series of times.
   */
  public String getVoltages() {
    return voltages;
  }

  /**
   * Retrieve the APD90 value or a coded reason for a value not be determined,
   * e.g. when a cell did not depolarise.
   * 
   * @return APD90 value, or codified reason for lack of a value.
   */
  public String getAPD90() {
    return apd90;
  }

  /**
   * Retrieve the delta APD90 value or a coded reason for a value not be
   * determined, e.g. when a cell did not depolarise.
   * 
   * @return Delta APD90 value, or codified reason for lack of a value.
   */
  public String getDeltaAPD90() {
    return deltaAPD90;
  }

  /**
   * Retrieve the qNet data.
   * 
   * @return qNet data, or {@code null} if not calculated.
   */
  public String getQNet() {
    return qNet;
  }

  /**
   * <ul>
   *   <li>
   *     Since June 2020, any simulation results will always return {@code false}
   *     from this method.
   *   </li>
   *   <li>
   *     Prior to June 2020 the value was used in indicate if the APD90 and 
   *     Δ APD90 values are codes for translation (as opposed to a numerical
   *     value).
   *     <p>
   *     Usually occurs where no APD90 and Δ APD90 value could be determined,
   *     e.g. when a cell did not repolarise, in which case a bundle identifier
   *     may have been substituted for those values.
   *   </li>
   * </ul>
   * 
   * @return {@code true} if the APD90 or Δ APD90 field contains a code for
   *         translation.
   */
  @Deprecated
  public boolean getForTranslation() {
    return forTranslation;
  }
}