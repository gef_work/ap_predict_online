/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.value.PortalFeature;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.controller.util.ControllerUtil;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation;
import uk.ac.ox.cs.nc3rs.client_direct.exception.SimulationNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService;
import uk.ac.ox.cs.nc3rs.client_direct.value.IonCurrent;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;

/**
 * MVC Controller for handling whether to display the input page or show all
 * existing simulations.
 * 
 * @author geoff
 */
@Controller
public class Process {

  @Autowired @Qualifier(ClientDirectIdentifiers.COMPONENT_CLIENT_DIRECT_SERVICE)
  private ClientDirectService clientDirectService;

  private static final Log log = LogFactory.getLog(Process.class);

  /**
   * Ensure that the correct model and session attributes are assigned for when
   * the input page is to be shown.
   * 
   * @param clientDirectService Client direct service.
   * @param simulationInput Simulation input (or {@code null} if no input yet
   *                        defined).
   * @param session HTTP session (for population).
   * @param exceptionMessage Exception message (optional).
   * @param model UI model (for population).
   */
  public static void setPropertiesForInput(final ClientDirectService clientDirectService,
                                           final SimulationInputVO simulationInput,
                                           final HttpSession session,
                                           final String exceptionMessage,
                                           final Model model) {
    model.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_MODELS,
                       clientDirectService.retrieveModels());

    if (simulationInput != null) {
      model.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_INPUT_VALUES,
                         simulationInput);
    } else {
      model.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_C50_UNITS,
                         clientDirectService.retrieveC50Units());
    }

    // Query to determine access to restricted portal features.
    final boolean allowUncertainties = ControllerUtil.allowFeature(PortalFeature.UNCERTAINTIES,
                                                                   clientDirectService);
    model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SHOW_UNCERTAINTIES,
                       allowUncertainties);

    if (allowUncertainties) {
      // Can't access maps by Enum key in standard EL, so switch to strings.
      final Map<String, String> uiSpreads = new HashMap<String, String>();
      for (final Map.Entry<IonCurrent, BigDecimal> spreadEntry : 
           clientDirectService.retrieveSpreads().entrySet()) {
        final BigDecimal spread = spreadEntry.getValue();
        final String useSpread = spread == null ? null : spread.toPlainString(); 
        uiSpreads.put(spreadEntry.getKey().toString(), useSpread);
      }
      model.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_SPREADS,
                         uiSpreads);

      final StringBuffer pctiles = new StringBuffer();
      for (final BigDecimal pctile : clientDirectService.retrieveCredibleIntervalPctiles()) {
        pctiles.append(pctile.toPlainString());
        pctiles.append(" ");
      }
      model.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_PCTILES,
                         pctiles.toString().trim());
    }

    final boolean allowDynamicCellML = ControllerUtil.allowFeature(PortalFeature.DYNAMIC_CELLMLS,
                                                                   clientDirectService);
    model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SHOW_DYNAMIC_CELLML,
                       allowDynamicCellML);

    // Place manually created token into the model and session for checking on input form submission
    final String token = UUID.randomUUID().toString();
    model.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_TOKEN, token);
    session.setAttribute(ClientDirectIdentifiers.SESSION_PARAM_TOKEN, token);

    if (exceptionMessage != null) {
      model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                         exceptionMessage);
    }
  }

  /**
   * Request to create a new simulation.
   * 
   * @param simulationId Simulation identifier (if using an existing simulation
   *                     as a template).
   * @param model UI model.
   * @param session HTTP session.
   * @return View name.
   */
  @RequestMapping(value=ClientDirectIdentifiers.ACTION_SIMULATION_NEW)
  public String simulationNew(final @RequestParam(required=false,
                                                  value=ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID)
                                    String simulationId,
                              final Model model,
                              final HttpSession session) {
    log.debug("~simulationNew() : Invoked.");

    SimulationInputVO simulationInput = null;
    if (simulationId != null) {
      if (!ControllerUtil.validSimulationId(simulationId)) {
        log.warn("~simulationNew() : Invalid simulation id provided.");
      } else {
        log.debug("~simulationNew() : Creating a new simulation based on simulation '" + simulationId + "'."); 
        try {
          final Simulation simulation = clientDirectService.retrieveSimulation(Long.valueOf(simulationId));
          simulationInput = new SimulationInputVO(simulation.getSimulationInput());
        } catch (SimulationNotFoundException e) {
          log.warn("~simulationNew() : Simulation for identifier '" + simulationId + "' not found.");
        }
      }
    }

    setPropertiesForInput(clientDirectService, simulationInput, session, null,
                          model);

    return ClientDirectIdentifiers.PAGE_INPUT;
  }
}
