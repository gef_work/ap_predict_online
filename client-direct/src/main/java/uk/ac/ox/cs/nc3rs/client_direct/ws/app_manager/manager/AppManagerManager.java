/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.manager;

import java.util.List;

import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunRequest;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Job;
import uk.ac.ox.cs.nc3rs.client_direct.exception.AppManagerWSInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.NoConnectionException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.RunInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.value.StatusVO;

/**
 * App Manager management interface.
 * 
 * @author geoff
 */
public interface AppManagerManager {

  // App manager's response code indicating capacity problems (srvact.prerun.ApPredictRunRequestReceiver).
  // See appCtx.appManager.ws.xml appManagerCodes definition.
  public static final String APP_MANAGER_CODE_AT_CAPACITY = "capacity.1";
  // See the app manager xsd
  public static final String APP_MANAGER_WORKLOAD_IDENTIFIER = "Workload";

  /**
   * Retrieve the progress statuses of a simulation.
   * 
   * @param appManagerId App Manager identifier.
   * @return Simulation unmodifiable progress list (or empty list if non available).
   * @throws AppManagerWSInvocationException If App Manager WS interaction generated an exception.
   * @throws NoConnectionException If can't connect to the App Manager Web services.
   */
  List<StatusVO> retrieveProgress(long appManagerId) throws AppManagerWSInvocationException,
                                                            NoConnectionException;

  /**
   * Retrieve the results of a simulation.
   * 
   * @param appManagerId App Manager identifier.
   * @return Results of simulation (in the form of a (potentially void of results!) Job entity).
   * @throws AppManagerWSInvocationException If App Manager WS interaction generated an exception.
   * @throws NoConnectionException If can't connect to the App Manager Web services.
   */
  Job retrieveResults(long appManagerId) throws AppManagerWSInvocationException,
                                                NoConnectionException;

  /**
   * Retrieve data of the workload.
   * 
   * @return Current workload.
   * @throws AppManagerWSInvocationException If App Manager WS interaction generated an exception.
   * @throws NoConnectionException If can't connect to the App Manager Web services.
   */
  String retrieveWorkload() throws AppManagerWSInvocationException, NoConnectionException;

  /**
   * Invoke the ApPredict run.
   * 
   * @param runRequest Run request.
   * @return App Manager identifier.
   * @throws AppManagerWSInvocationException If App Manager WS interaction generated an exception.
   * @throws NoConnectionException If can't connect to the App Manager Web services.
   * @throws RunInvocationException If there's a problem invoking ApPredict.
   */
  long runApPredict(ApPredictRunRequest runRequest) throws AppManagerWSInvocationException,
                                                           NoConnectionException,
                                                           RunInvocationException;

  /**
   * Signal to App Manager that the results have been uploaded and the data can be removed from
   * App Manager.
   *  
   * @param appManagerId App Manager identifier.
   * @throws AppManagerWSInvocationException If App Manager WS interaction generated an exception.
   * @throws NoConnectionException If can't connect to the App Manager Web services.
   */
  void signalResultsUploaded(long appManagerId) throws AppManagerWSInvocationException,
                                                       NoConnectionException;
}