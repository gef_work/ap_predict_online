/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the names of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.manager;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.dao.SimulationDAO;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Job;
import uk.ac.ox.cs.nc3rs.client_direct.entity.JobPKResult;
import uk.ac.ox.cs.nc3rs.client_direct.entity.JobResult;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation;
import uk.ac.ox.cs.nc3rs.client_direct.entity.SimulationInput;
import uk.ac.ox.cs.nc3rs.client_direct.exception.DataRetrievalException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.SimulationNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.value.MeasurementObservationsVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;

/**
 * Implementation of simulation operations.
 * 
 * @author geoff
 */
@Component(ClientDirectIdentifiers.COMPONENT_SIMULATION_MANAGER)
@Transactional(readOnly=true)
public class SimulationManagerImpl implements SimulationManager {

  @Autowired @Qualifier(ClientDirectIdentifiers.COMPONENT_SIMULATION_DAO)
  private SimulationDAO simulationDAO;

  /** High enough value for maximum number of simulations to retrieve for a user */
  public static final int RANDOM_HIGH_ENOUGH_MAX_RETRIEVAL_COUNT = Integer.valueOf(10000);

  private static final Log log = LogFactory.getLog(SimulationManagerImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.manager.SimulationManager#createSimulation(long, java.lang.String, uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO)
   */
  @Override
  @Transactional(readOnly=false)
  public long createSimulation(final long appManagerId, final String creator,
                               final SimulationInputVO simulationInputVO) {
    log.debug("~createSimulation() : [?][" + appManagerId + "] : Creating simulation with specified app-manager id.");

    final MeasurementObservationsVO compoundObservationsICaL = simulationInputVO.getICaLObservations();
    final MeasurementObservationsVO compoundObservationsIK1 = simulationInputVO.getIK1Observations();
    final MeasurementObservationsVO compoundObservationsIKr = simulationInputVO.getIKrObservations();
    final MeasurementObservationsVO compoundObservationsIKs = simulationInputVO.getIKsObservations();
    final MeasurementObservationsVO compoundObservationsINa = simulationInputVO.getINaObservations();
    final MeasurementObservationsVO compoundObservationsINaL = simulationInputVO.getINaLObservations();
    final MeasurementObservationsVO compoundObservationsIto = simulationInputVO.getItoObservations();
    final SimulationInput simulationInput = new SimulationInput(compoundObservationsICaL.getC50(),
                                                                compoundObservationsICaL.getHill(),
                                                                compoundObservationsICaL.getSaturation(),
                                                                compoundObservationsICaL.getSpread(),
                                                                compoundObservationsIK1.getC50(),
                                                                compoundObservationsIK1.getHill(),
                                                                compoundObservationsIK1.getSaturation(),
                                                                compoundObservationsIK1.getSpread(),
                                                                compoundObservationsIKr.getC50(),
                                                                compoundObservationsIKr.getHill(),
                                                                compoundObservationsIKr.getSaturation(),
                                                                compoundObservationsIKr.getSpread(),
                                                                compoundObservationsIKs.getC50(),
                                                                compoundObservationsIKs.getHill(),
                                                                compoundObservationsIKs.getSaturation(),
                                                                compoundObservationsIKs.getSpread(),
                                                                compoundObservationsINa.getC50(),
                                                                compoundObservationsINa.getHill(),
                                                                compoundObservationsINa.getSaturation(),
                                                                compoundObservationsINa.getSpread(),
                                                                compoundObservationsINaL.getC50(),
                                                                compoundObservationsINaL.getHill(),
                                                                compoundObservationsINaL.getSaturation(),
                                                                compoundObservationsINaL.getSpread(),
                                                                compoundObservationsIto.getC50(),
                                                                compoundObservationsIto.getHill(),
                                                                compoundObservationsIto.getSaturation(),
                                                                compoundObservationsIto.getSpread(),
                                                                simulationInputVO.getC50Type(),
                                                                simulationInputVO.getIc50Units(),
                                                                simulationInputVO.getModelIdentifier(),
                                                                simulationInputVO.getPacingMaxTime(),
                                                                simulationInputVO.getPacingFrequency(),
                                                                simulationInputVO.getPlasmaConcMax(),
                                                                simulationInputVO.getPlasmaConcMin(),
                                                                simulationInputVO.getPlasmaIntPtCount(),
                                                                simulationInputVO.getPlasmaIntPtLogScale(),
                                                                StringUtils.join(simulationInputVO.getPlasmaConcPoints(), " "),
                                                                simulationInputVO.getPkFile(),
                                                                simulationInputVO.getCellMLFileName(),
                                                                simulationInputVO.getCredibleIntervalPctiles(),
                                                                simulationInputVO.getNotes());
    // Create a new job (corresponding to the App Manager invocation) and link to the simulaton. 
    final Job job = new Job(appManagerId);
    final Simulation simulation = simulationDAO.store(new Simulation(job, creator,
                                                                     simulationInput));

    return simulation.getId();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.manager.SimulationManager#deleteSimulation(long)
   */
  @Override
  @Transactional(readOnly=false)
  public void deleteSimulation(final long simulationId) {
    log.debug("~deleteSimulation() : [" + simulationId + "] : Invoked.");

    simulationDAO.deleteSimulation(simulationId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.manager.SimulationManager#recordResults(long, uk.ac.ox.cs.nc3rs.client_direct.entity.Job)
   */
  @Override
  @Transactional(readOnly=false)
  public Job recordResults(final long simulationId, final Job appManagerJob) {
    final String logPrefix = "~recordResults() : [" + simulationId + "][" + appManagerJob.getAppManagerId() + "] : ";
    log.debug(logPrefix.concat("Invoked."));

    final Simulation simulation = simulationDAO.findBySimulationId(simulationId);
    final Job job = simulation.getJob();
    job.setMessages(appManagerJob.getMessages());
    job.setDeltaAPD90PercentileNames(appManagerJob.getDeltaAPD90PercentileNames());

    for (final JobResult result : appManagerJob.retrieveJobResultCopies()) {
      result.setJob(job);
    }
    for (final JobPKResult pkResult : appManagerJob.retrieveJobPKResultCopies()) {
      pkResult.setJob(job);
    }

    return simulationDAO.storeResults(job);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.manager.SimulationManager#retrieveAllSimulations()
   */
  @Override
  public List<Simulation> retrieveAllSimulations() {
    log.debug("~retrieveAllSimulations() : Invoked.");

    return simulationDAO.findSimulations();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.manager.SimulationManager#retrieveResults(long)
   */
  @Override
  public Job retrieveResults(final long simulationId) {
    log.debug("~retrieveResults() : [" + simulationId + "] : Invoked.");

    final Simulation simulation = simulationDAO.findBySimulationId(simulationId);
    final Job job = simulation.getJob();

    // Note that not loading PK file data!
    simulationDAO.avoidJobLazyInit(job);

    return job;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.manager.SimulationManager#retrieveSimulation(long)
   */
  @Override
  public Simulation retrieveSimulation(final long simulationId)
                                       throws SimulationNotFoundException {
    final String logPrefix = "~retrieveSimulation() : [" + simulationId + "] : ";
    log.debug(logPrefix.concat("Invoked."));

    final Simulation simulation = simulationDAO.findBySimulationId(simulationId);
    if (simulation == null) {
      log.info(logPrefix.concat("Simulation not found!!"));
      throw new SimulationNotFoundException("Simulation with identifier '" + simulationId + "' not found");
    }

    final PortalFile pkFile = simulation.getSimulationInput().getPkFile();
    if (pkFile != null) {
      /* Overcome the SimulationInput <--> PortalFile default lazy load... but
         may result in db read delaying response by 0.5s if v. large file */
      pkFile.getContent();
    }

    return simulation;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.manager.SimulationManager#retrieveUserSimulations(java.lang.String)
   */
  @Override
  public List<Simulation> retrieveUserSimulations(final String user)
                                                  throws DataRetrievalException {
    log.debug("~retrieveUserSimulations() : [?] : Invoked.");

    final List<Simulation> userSimulations = new ArrayList<Simulation>();
    try {
      userSimulations.addAll(simulationDAO.findByUser(user));
    } catch (EntityNotFoundException e) {
      // If, for example, a referenced portal file has been manually deleted from db!?!
      final String errorMessage = "Error '" + e.getMessage() + "' when retrieving simulation details for '" + user + "'.";
      log.error("~retrieveUserSimulations() : ".concat(errorMessage));
      throw new DataRetrievalException(errorMessage);
    }

    return userSimulations;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.manager.SimulationManager#save(uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation)
   */
  @Override
  @Transactional(readOnly=false)
  public Simulation save(final Simulation simulation) {
    log.debug("~save() : [?] : Invoked.");

    return simulationDAO.store(simulation);
  }
}