/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.proxy;

import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApplicationStatusRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApplicationStatusResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.BusinessDataUploadCompletedRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllResultsRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllResultsResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllStatusesRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllStatusesResponse;

/**
 * Service interface to the WS Proxy services which App Manager provides.
 *
 * @author geoff
 */
public interface AppManagerProxy {

  /**
   * Query the App Manager status.
   * 
   * @param statusRequest Query request.
   * @return Response.
   */
  ApplicationStatusResponse applicationStatusRequest(ApplicationStatusRequest statusRequest);

  /**
   * Invoke the run request on the App Manager.
   * 
   * @param runRequest Request to invoke.
   * @return Response to request.
   */
  ApPredictRunResponse invokeRunRequest(ApPredictRunRequest runRequest);

  /**
   * Retrieve the progress for a simulation.
   * 
   * @param statusesRequest Progress/Statuses wanted.
   * @return Response to request.
   */
  RetrieveAllStatusesResponse retrieveAllProgress(RetrieveAllStatusesRequest statusesRequest);

  /**
   * Retrieve the results for a simulation.
   * 
   * @param resultsRequest Results wanted.
   * @return Response to request.
   */
  RetrieveAllResultsResponse retrieveAllResults(RetrieveAllResultsRequest resultsRequest);

  /**
   * Signal to App Manager that the results have been uploaded and the data can be removed from
   * App Manager.
   *  
   * @param completedRequest Request.
   */
  void signalResultsUploaded(BusinessDataUploadCompletedRequest completedRequest);
}