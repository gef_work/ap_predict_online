/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.annotate.JsonIgnore;

import uk.ac.ox.cs.compbio.client_shared.api.entity.SecuredEntity;
import uk.ac.ox.cs.nc3rs.client_direct.value.InputOption;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;

/**
 * Domain object representing a simulation.
 *
 * @author geoff
 */
@Entity
@NamedQueries({
  @NamedQuery(name=Simulation.QUERY_SIMULATION_BY_CREATOR_ORDERED,
              query="SELECT simulation FROM Simulation AS simulation " +
                    "  WHERE " + Simulation.PROPERTY_CREATOR + " = :" + Simulation.PROPERTY_CREATOR +
                    "  ORDER BY " + Simulation.PROPERTY_PERSISTED + " DESC"),
  @NamedQuery(name=Simulation.QUERY_SIMULATION_ALL,
              query="SELECT simulation FROM Simulation AS simulation")
})
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE,
                                 region = "SimulationCache")
public class Simulation implements SecuredEntity, Serializable {

  private static final long serialVersionUID = -1890718528853060825L;

  // Default grace period.
  private static final long DEFAULT_GRACE_PERIOD = 20000l;
  // A minute represented in milliseconds.
  private static final long MINUTE_MULTIPLE = 60000l;
  // TODO : Configuration. Approx maximum time a simulation to takes to run (in minutes).
  private static final long MAX_SIMULATION_TIME_MINUTES = 60l;
  private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

  /* Consistent property name */
  public static final String PROPERTY_CREATOR = "creator";
  public static final String PROPERTY_PERSISTED = "persisted";

  /** Consistent query naming - Query all simulations */
  public static final String QUERY_SIMULATION_ALL = "simulation.queryAll";
  /** Consistent query naming - Query simulation by its creator */
  public static final String QUERY_SIMULATION_BY_CREATOR_ORDERED = "simulation.queryByCreatorOrdered";

  // Persistence identity (Surrogate primary key)
  @TableGenerator(name="simulation_id_gen", table="sequence_pks_client_direct",
                  pkColumnName="pk_seq_name", pkColumnValue="simulation_id",
                  valueColumnName="pk_seq_value", allocationSize=1)
  @Id @GeneratedValue(strategy=GenerationType.TABLE, generator="simulation_id_gen")
  private Long id;

  // Unidirectional Simulation [1] --> [1] SimulationInput
  @OneToOne(cascade= { CascadeType.PERSIST, CascadeType.REMOVE },
            fetch=FetchType.EAGER )
  @JoinColumn(name="simulation_input_id",
              nullable=false,
              updatable=false,
              unique=true,
              foreignKey = @ForeignKey(name="fk_id_simulation_input"))
  private SimulationInput simulationInput;

  @JsonIgnore
  // Unidirectional Simulation [1] --> [1] Job
  @OneToOne(cascade= { CascadeType.PERSIST, CascadeType.REMOVE },
            fetch=FetchType.EAGER )
  @JoinColumn(name="job_id",
              nullable=true,
              updatable=true,
              unique=true,
              foreignKey = @ForeignKey(name="fk_id_job"))
  private Job job;

  // Optimistic locking concurrency control
  @Version
  private Long lockVersion;

  @JsonIgnore
  @Column(insertable=true, nullable=false, updatable=false)
  private String creator;

  // TODO : Don't rely on this. It is updated based on simulation progress derived from UI interaction.
  @JsonIgnore
  @Temporal(value=TemporalType.TIMESTAMP)
  private Date completed;

  @Temporal(value=TemporalType.TIMESTAMP)
  private Date persisted;

  /** <b>Do not invoke directly.</b> */
  protected Simulation() {}

  @Transient
  private static final Log log = LogFactory.getLog(Simulation.class);

  /**
   * Initialising constructor.
   * 
   * @param job Simulation job (mandatory).
   * @param creator Simulation creator (mandatory).
   * @param simulationInput Simulation input entity (mandatory).
   * @throws IllegalArgumentException If any arg is {@code null}.
   */
  public Simulation(final Job job, final String creator,
                    final SimulationInput simulationInput) {
    if (job == null || creator == null || simulationInput == null) {
      throw new IllegalArgumentException("Simulation creation requires job, creator and simulation assignment.");
    }

    this.job = job;
    this.creator = creator;
    this.simulationInput = simulationInput;
  }

  // internal callback method
  @PrePersist
  protected void onCreate() {
    persisted = new Date();
  }

  /**
   * Assign the Simulation as being completed.
   * 
   * Note that the Simulation must first be persisted before being assigned as
   * completed.
   */
  public void assignCompleted() {
    assert (getPersisted() != null) : "Invalid object state: Simulation object can not be completed before being persisted!";

    completed = new Date();
    log.debug("~assignCompleted() : [" + id + "] : " + sdf.format(completed));
  }

  /**
   * Indicator of whether the simulation has finished or not.
   * 
   * @return {@code true} if simulation has completed, otherwise {@code false}.
   */
  public boolean isCompleted() {
    return (completed != null);
  }

  /**
   * Indicate if the simulation had concentration range input.
   * 
   * @param simulation Simulation to test.
   * @return {@code true} if simulation provided had concentration range
   *         input, otherwise {@code false}.
   */
  public static boolean hasConcRangeInput(final Simulation simulation) {
    boolean isConcRangeInput = false;
    if (simulation != null &&
        (InputOption.CONC_RANGE == simulation.retrieveInputOption())) {
      isConcRangeInput = true;
    }
    return isConcRangeInput;
  }

  /**
   * Indicate if the simulation had concentration points input.
   * 
   * @param simulation Simulation to test.
   * @return {@code true} if simulation provided had concentration points
   *         input, otherwise {@code false}.
   */
  public static boolean hasConcsPointInput(final Simulation simulation) {
    boolean isConcsPointInput = false;
    if (simulation != null &&
        (InputOption.CONC_POINTS == simulation.retrieveInputOption())) {
      isConcsPointInput = true;
    }
    return isConcsPointInput;
  }

  /**
   * Indicate if the simulation had PK file input.
   * 
   * @param simulation Simulation to test.
   * @return {@code true} if simulation provided had PK file input, otherwise
   *         {@code false}.
   */
  public static boolean hasPKInput(final Simulation simulation) {
    boolean isPKInput = false;
    if (simulation != null &&
        (InputOption.PK == simulation.retrieveInputOption())) {
      isPKInput = true;
    }
    return isPKInput;
  }

  /**
   * Indicator of whether a simulation is taking too long to complete.
   * 
   * @return {@code true} if it's taking too long.
   */
  public boolean isTakingTooLongToComplete() {
    if (isCompleted()) {
      return false;
    }
    return (new Date().getTime() - persisted.getTime() >=
            (MAX_SIMULATION_TIME_MINUTES * MINUTE_MULTIPLE)) ? true : false;
  }

  /**
   * Retrieve the simulation input option.
   * 
   * @return Simulation input option, or {@code null} if 
   *        {@linkplain #simulationInput} is {@code null} or transient.
   */
  public InputOption retrieveInputOption() {
    InputOption inputOption = null;
    if (simulationInput != null && getId() != null) {
      inputOption = new SimulationInputVO(simulationInput).retrieveInputOption();
    }
    return inputOption;
  }

  /**
   * Indicate if we're within the post-completion grace period (basic on the
   * simulation's input options, e.g. PK, conc range.
   * 
   * This is to allow time for post-completion processing to complete, and can
   * only be called if the Simulation's {@linkplain #simulationInput} has been
   * persisted.
   * 
   * @return {@code true} if of {@link InputOption} type and within the
   *         post-completion grace period, otherwise {@code false}.
   * @throws IllegalStateException If {@linkplain #simulationInput} has not been
   *         persisted.
   */
  public boolean withinPostCompletionGracePeriod()
                 throws IllegalStateException {

    if (getSimulationInput() == null || getSimulationInput().getId() == null) {
      final String errorMessage = "Post-completion grace period calculation requires presence of simulation input";
      throw new IllegalStateException(errorMessage);
    }

    if (!isCompleted()) {
      // If we haven't even completed yet, we're not within the grace period.
      return false;
    }

    final long currentTimeMillis = System.currentTimeMillis();
    Long timeAgo = null;
    switch (retrieveInputOption()) {
      case PK :
        // Allow PK processing results return delays of 1 min. 
        timeAgo = currentTimeMillis - MINUTE_MULTIPLE;
        break;
      default :
        // Allow non-PK processing results return delays of, for example, 20s!
        timeAgo = currentTimeMillis - DEFAULT_GRACE_PERIOD;
        break;
    }

    // Return true if simulation completed during grace period e.g, since 1 minute ago
    log.debug("~withinPostCompletionGracePeriod() : [" + id + "] : Completed "
              + sdf.format(completed) + " Ago " + sdf.format(new Date(timeAgo)));
    return (completed.after(new Date(timeAgo)));
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Simulation [id=" + id + ", simulationInput=" + simulationInput
        + ", job=" + job + ", lockVersion=" + lockVersion + ", creator="
        + creator + ", completed=" + completed + ", persisted=" + persisted
        + "]";
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((completed == null) ? 0 : completed.hashCode());
    result = prime * result + ((creator == null) ? 0 : creator.hashCode());
    result = prime * result + ((persisted == null) ? 0 : persisted.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Simulation other = (Simulation) obj;
    if (completed == null) {
      if (other.completed != null)
        return false;
    } else if (!completed.equals(other.completed))
      return false;
    if (creator == null) {
      if (other.creator != null)
        return false;
    } else if (!creator.equals(other.creator))
      return false;
    if (persisted == null) {
      if (other.persisted != null)
        return false;
    } else if (!persisted.equals(other.persisted))
      return false;
    return true;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.compbio.client_shared.api.entity.SecuredEntity#getId()
   */
  @Override
  public Long getId() {
    return id;
  }

  /**
   * Retrieve the App Manager identifier (indirectly from the Simulation's Job!).
   * 
   * @return App Manager identifier.
   * @see Job#getAppManagerId()
   */
  public long getAppManagerId() {
    return job.getAppManagerId();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.compbio.client_shared.api.entity.SecuredEntity#getCreator()
   */
  @Override
  public String getCreator() {
    return creator;
  }

  /**
   * Retrieve the simulation job.
   * 
   * @return Simulation job.
   */
  public Job getJob() {
    return job;
  }

  /**
   * @return the persisted
   */
  public Date getPersisted() {
    return persisted;
  }

  /**
   * Retrieve the simulation input.
   * 
   * @return The simulation input (or {@code null} if none available).
   */
  public SimulationInput getSimulationInput() {
    return simulationInput;
  }
}