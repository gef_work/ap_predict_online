/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.manager;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.manager.CSConfigurationManager;
import uk.ac.ox.cs.compbio.client_shared.value.PortalFeature;
import uk.ac.ox.cs.compbio.client_shared.value.security.Role;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.config.Configuration;
import uk.ac.ox.cs.nc3rs.client_direct.value.IonCurrent;
import uk.ac.ox.cs.nc3rs.client_direct.value.ModelVO;

/**
 * Implementation of the configuration management.
 * 
 * @author geoff
 */
@Component(ClientDirectIdentifiers.COMPONENT_CONFIGURATION_MANAGER)
public class ConfigurationManagerImpl implements ConfigurationManager {

  @Autowired @Qualifier(ClientDirectIdentifiers.COMPONENT_CONFIGURATION)
  private Configuration configuration;

  @Autowired @Qualifier(ClientSharedIdentifiers.COMPONENT_CLIENT_SHARED_CONFIGURATION_MANAGER)
  private CSConfigurationManager csConfigurationManager;

  private static final Log log = LogFactory.getLog(ConfigurationManagerImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.manager.ConfigurationManager#retrieveC50Units()
   */
  @Override
  public Map<String, String> retrieveC50Units() {
    log.debug("~retrieveC50Units() : Invoked.");

    return configuration.getC50Units();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.manager.ConfigurationManager#retrieveCredibleIntervalPctiles()
   */
  @Override
  public Set<BigDecimal> retrieveCredibleIntervalPctiles() {
    log.debug("~retrieveCredibleIntervalPctiles() : Invoked.");

    return configuration.getCredibleIntervalPctiles();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.manager.ConfigurationManager#retrieveModelNames()
   */
  @Override
  public Map<Short, String> retrieveModelNames() {
    log.debug("~retrieveModelNames() : Invoked.");

    return configuration.getModelNames();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.manager.ConfigurationManager#retrieveModels()
   */
  @Override
  public List<ModelVO> retrieveModels() {
    log.debug("~retrieveModels() : Invoked.");

    return configuration.getModels();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.compbio.client_shared.manager.CSConfigurationManager#retrieveRequiredRoles(uk.ac.ox.cs.compbio.client_shared.value.PortalFeature)
   */
  @Override
  public Set<Role> retrieveRequiredRoles(final PortalFeature portalFeature) {
    log.debug("~retrieveRequiredRoles() : Invoked.");

    return csConfigurationManager.retrieveRequiredRoles(portalFeature);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.manager.ConfigurationManager#retrieveSpreads()
   */
  @Override
  public Map<IonCurrent, BigDecimal> retrieveSpreads() {
    log.debug("~retrieveSpreads() : Invoked.");

    return configuration.getSpreads();
  }
}