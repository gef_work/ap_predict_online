/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * Job PK results.
 *
 * @author geoff
 */
@Entity
@Table(uniqueConstraints = {
  @UniqueConstraint(columnNames = { "jc_jobid", "timepoint" },
                    name="unique_jobid_timepoint")
})
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy.READ_ONLY,
                                 region = "JobPKResultCache")
public class JobPKResult implements Serializable {

  private static final long serialVersionUID = 2684542726112868403L;

  /*
   * Not using a table generator because could be up to 50,000 records. The 
   *   default allocationSize is 50 which isn't a useful quantity.
   * Using GenerationType.AUTO which defers strategy to persistence provider.
   *   HSQLDB defaults to GenerationType.IDENTITY, MySQL doesn't support
   *   GenerationType.SEQUENCE, uff!
   */
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO, generator="native")
  @org.hibernate.annotations.GenericGenerator(name="native", strategy="native")
  private Long id;

  @Column(insertable=true, nullable=false, precision=19, scale=10,
          updatable=false)
  private BigDecimal timepoint;

  /* String of non-quoted csv times, each comma-separated value corresponds to
     a PK file column (so usually no more than a couple of 100 chars!) */
  // MySQL seems to create a "longtext" (4Gb) field, HSQL a varchar(xxxx)!
  @Column(insertable=true, length=40000, nullable=false, updatable=false)
  private String apd90s;

  @JsonIgnore
  // True if the APD90s property holds a value for client interface i18n translation.
  @Column(insertable=true, nullable=false, updatable=false)
  private Boolean forTranslation;

  // Bidirectional Job [1..*] <-> [1] JobPKResult
  @ManyToOne
  @JoinColumn(name="jc_jobid", insertable=true, nullable=false, updatable=false,
              foreignKey = @ForeignKey(name="fk_jobpkresult_jobid"))
  private Job job;

  /** <b>Do not invoke directly.</b> */
  protected JobPKResult() {};

  /**
   * Initialising constructor.
   * 
   * @param timepoint Time point.
   * @param apd90s CSV collection of APD90 values.
   * @param forTranslation Indicator that APD90s value contains a string
   *                        (e.g. a bundle identifier) for translation.
   * @throws IllegalArgumentException If {@code null} values provided.
   */
  public JobPKResult(final BigDecimal timepoint, final String apd90s,
                     final boolean forTranslation)
                     throws IllegalArgumentException {
    if (timepoint == null || apd90s == null) {
      throw new IllegalArgumentException("Cannot create a job PK result with empty values!");      
    }
    this.timepoint = timepoint;
    this.apd90s = apd90s;
    this.forTranslation = forTranslation;
  }

  /**
   * Associate this result with the parent job, i.e. establish bi-directional
   * relationship.
   * 
   * @param job Parent job.
   */
  public void setJob(final Job job) {
    job.addJobPKResult(this);
    this.job = job;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "JobPKResult [id=" + id + ", timepoint=" + timepoint + ", apd90s="
        + apd90s + ", forTranslation=" + forTranslation + "]";
  }

  /**
   * Retrieve flag to indicate if the APD90's contains a value requiring
   * translation.
   * 
   * @return {@code true} if the APD90 values contain a value for translation.
   */
  public boolean isForTranslation() {
    return forTranslation;
  }

  /**
   * Retrieve the PK timepoint.
   * 
   * @return The PK timepoint.
   */
  public BigDecimal getTimepoint() {
    return timepoint;
  }

  /**
   * Retrieve the APD90 values for the timepoint.
   * <p>
   * The value is CSV format.
   * 
   * @return The APD90s for the timepoint.
   */
  public String getApd90s() {
    return apd90s;
  }
}