/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.service;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;

import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.compbio.client_shared.value.PortalFeature;
import uk.ac.ox.cs.compbio.client_shared.value.file.FILE_DATA_TYPE;
import uk.ac.ox.cs.compbio.client_shared.value.file.FileStoreActionOutcomeVO;
import uk.ac.ox.cs.compbio.client_shared.value.security.Role;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation;
import uk.ac.ox.cs.nc3rs.client_direct.exception.AppManagerWSInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.DataRetrievalException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.NoConnectionException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.ResultsNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.RunInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.SimulationNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.value.IonCurrent;
import uk.ac.ox.cs.nc3rs.client_direct.value.ModelVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationCurrentResultsVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;

/**
 * Interface to client (direct) services.
 * 
 * @author geoff
 */
public interface ClientDirectService {

  /** Text returned when progress requested but no progress in statuses! Not started, or a problem retrieving data. */
  public static final String PROGRESS_INDICATING_SOME_KIND_OF_PROBLEM = "";
  /** Text returned when progress requested but the simulation is in the finished state. */
  public static final String PROGRESS_INDICATING_SIMULATION_HAS_FINISHED = "!";

  /**
   * Remove the simulation (if it's completed, otherwise the request is 
   * silently ignored).
   * 
   * @param simulationId Simulation identifier.
   */
  @PreAuthorize("hasPermission(#simulationId, 'uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation', 'administration')")
  void deleteSimulation(long simulationId);

  /**
   * Retrieve all portal files a user has creator or can see.
   * 
   * @return Portal files which the specified user can view.
   */
  @PostFilter("hasPermission(filterObject, 'administration') or " +
              "hasPermission(filterObject, 'read')")
  List<PortalFile> retrieveAllFiles();

  /**
   * Retrieve all simulations a user has creator or can see.
   * 
   * @return Simulations which the specified user can view.
   */
  @PostFilter("hasPermission(filterObject, 'administration') or " +
              "hasPermission(filterObject, 'read')")
  List<Simulation> retrieveAllSimulations();

  /**
   * Retrieve configured C50 units.
   * 
   * @return Configured C50 units.
   */
  Map<String, String> retrieveC50Units();

  /**
   * Retrieve the default credible interval percentiles.
   * 
   * @return Default credible interval percentiles.
   */
  Set<BigDecimal> retrieveCredibleIntervalPctiles();

  /**
   * Retrieve the required roles for feature access.
   * 
   * @param portalFeature Feature intended to be accessed.
   * @return Roles required to use the feature, or empty collection if
   *         unrestricted access.
   */
  Set<Role> retrieveRequiredRoles(PortalFeature portalFeature);

  /**
   * Retrieve a simulation's latest progress.
   * 
   * @param simulationId Simulation identifier.
   * @return Simulation's latest progress if found, otherwise various indicators.
   * @throws AppManagerWSInvocationException If App Manager WS interaction 
   *                                         generated an exception.
   * @throws NoConnectionException If can't connect to the App Manager Web 
   *                               services.
   * @throws SimulationNotFoundException If simulation not found.
   */
  @PreAuthorize("hasPermission(#simulationId, 'uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation', 'read') or " +
                "hasPermission(#simulationId, 'uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation', 'administration')")
  String retrieveLatestProgress(long simulationId)
                                throws AppManagerWSInvocationException,
                                       NoConnectionException,
                                       SimulationNotFoundException;

  /**
   * Retrieve a map of model names keyed by their identifier.
   * 
   * @return Map of model names (keyed by identifier).
   */
  Map<Short, String> retrieveModelNames();

  /**
   * Retrieve the config-defined models.
   * 
   * @return Available models (ordered by model identifier).
   */
  List<ModelVO> retrieveModels();

  /**
   * Retrieve the simulation and all dependent objects, e.g. simulation input,
   * portal file, etc..
   *  
   * @param simulationId Simulation identifier.
   * @return Simulation if found;
   * @throws SimulationNotFoundException If simulation not found.
   */
  @PreAuthorize("hasPermission(#simulationId, 'uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation', 'read') or " +
                "hasPermission(#simulationId, 'uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation', 'administration')")
  Simulation retrieveSimulation(long simulationId)
                                throws SimulationNotFoundException;

  /**
   * Retrieve the current results of a simulation request.
   * <p>
   * This may not necessarily be the simulation results, but the progress status
   * if the simulation is currently running.
   * 
   * @param simulationId Simulation identifier.
   * @return Simulation's current results.
   * @throws AppManagerWSInvocationException If App Manager WS interaction 
   *                                         generated an exception.
   * @throws NoConnectionException If can't connect to the App Manager Web 
   *                               services.
   * @throws ResultsNotFoundException If simulation results can't be found.
   * @throws SimulationNotFoundException If simulation can't be found.
   */
  @PreAuthorize("hasPermission(#simulationId, 'uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation', 'read') or " +
                "hasPermission(#simulationId, 'uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation', 'administration')")
  SimulationCurrentResultsVO retrieveSimulationResults(long simulationId)
                                                       throws AppManagerWSInvocationException,
                                                              NoConnectionException,
                                                              ResultsNotFoundException,
                                                              SimulationNotFoundException;

  /**
   * Retrieve the ion channel spreads.
   * 
   * @return Ion channel spread values.
   */
  Map<IonCurrent, BigDecimal> retrieveSpreads();

  /**
   * Retrieve a collection of the user's Simulations in the database along with
   * dependent objects except for portal file objects.
   * 
   * @param user Interface user.
   * @return Simulations which the specified user created (or empty list if none
   *         found).
   * @throws DataRetrievalException If problem retrieving persisted data.
   */
  @PostFilter("hasPermission(filterObject, 'administration')")
  List<Simulation> retrieveUserSimulations(String user)
                                           throws DataRetrievalException;

  /**
   * Run a simulation.
   * 
   * @param simulationInput Simulation input.
   * @param initiator Name of user initiating the simulation.
   * @return Simulation identifier.
   * @throws AppManagerWSInvocationException If App Manager WS interaction
                                             generated an exception.
   * @throws NoConnectionException If can't connect to the App Manager Web
   *                               services.
   * @throws RunInvocationException If there's a problem invoking ApPredict.
   */
  long runSimulation(SimulationInputVO simulationInput, String initiator)
                     throws AppManagerWSInvocationException,
                            NoConnectionException, RunInvocationException;

  /**
   * Store the file.
   * 
   * @param fileDataType Nature of file data.
   * @param inputStream File input stream object (caller is responsible for 
   *                    closing the stream).
   * @param name Optional file name.
   * @param uploader Name of user uploading the file.
   * @return File storage action outcome.
   * @throws IllegalArgumentException If any parameter is {@code null} or empty.
   */
  FileStoreActionOutcomeVO storeFile(FILE_DATA_TYPE fileDataType,
                                     InputStream inputStream,
                                     String name, String uploader)
                                     throws IllegalArgumentException;
}