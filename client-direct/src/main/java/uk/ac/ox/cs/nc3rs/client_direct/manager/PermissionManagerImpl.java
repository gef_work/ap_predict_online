/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.manager;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.acls.domain.ObjectIdentityImpl;
import org.springframework.security.acls.model.MutableAcl;
import org.springframework.security.acls.model.MutableAclService;
import org.springframework.security.acls.model.NotFoundException;
import org.springframework.security.acls.model.ObjectIdentity;
import org.springframework.security.acls.model.Permission;
import org.springframework.security.acls.model.Sid;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.business.security.util.PermissionUtil;

/**
 * Permission management for Access Control Lists (ACL) lists.
 *
 * @author geoff
 */
@Component(ClientDirectIdentifiers.COMPONENT_PERMISSION_MANAGER)
public class PermissionManagerImpl implements PermissionManager {

  // Access control list service
  @Autowired @Qualifier(ClientDirectIdentifiers.SPRING_SECURITY_ACL_SERVICE)
  private MutableAclService mutableAclService;

  private static final Log log = LogFactory.getLog(PermissionManagerImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.manager.PermissionManager#addPermission(java.lang.Class, java.lang.Long, org.springframework.security.acls.model.Sid, org.springframework.security.acls.model.Permission)
   */
  @Override
  public void addPermission(final Class<?> secureObjectClass, final Long secureObjectId,
                            final Sid recipient, final Permission permission) {
    log.debug("~addPermission() : Request to add permission [" + permission + "] to [" + secureObjectClass +
                                 "]#[" + secureObjectId + "] for [" + recipient + "]");

    // the object being secured
    final ObjectIdentity objectIdentity = new ObjectIdentityImpl(secureObjectClass, secureObjectId);
    // update access control list
    updateAcls(objectIdentity, recipient, permission);
  }

  /*
   * 
   */
  private void updateAcls(final ObjectIdentity secureObjectIdentity, final Sid recipient,
                          final Permission permission) {
    MutableAcl acl;
    try {
      acl = (MutableAcl) mutableAclService.readAclById(secureObjectIdentity);
      log.debug("~updateAcls(..) : ACL retrieved for object");
    } catch (NotFoundException nfe) {
      log.debug("~updateAcls(..) : ACL not found for object");
      acl = mutableAclService.createAcl(secureObjectIdentity);
    }

    final List<Sid> sids = new ArrayList<Sid>(1);
    sids.add(recipient);
    final boolean administrativeMode = true;
    boolean assignPermission = false;
    try {
      if (acl.isGranted(PermissionUtil.retrievePermissionAsList(permission), sids, administrativeMode)) {
        log.debug("~updateAcls(..) : Permission already granted");
      } else {
        log.debug("~updateAcls(..) : Permission had been previously denied");
        assignPermission = true;
      }
    } catch (NotFoundException nfe) {
      log.debug("~updateAcls(..) : No previous permission (grant/deny) found");
      assignPermission = true;
    }

    if (assignPermission) {
      log.debug("~updateAcls(..) : Assigning permission to object");

      acl.insertAce(acl.getEntries().size(), permission, recipient, true);
      mutableAclService.updateAcl(acl);
    } else {
      log.debug("~updateAcls(..) : ACL unchanged for object");
    }
  }
}