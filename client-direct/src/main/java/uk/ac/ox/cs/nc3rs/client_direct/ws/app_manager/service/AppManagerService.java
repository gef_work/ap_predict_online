/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.service;

import java.util.List;

import uk.ac.ox.cs.nc3rs.client_direct.entity.Job;
import uk.ac.ox.cs.nc3rs.client_direct.exception.AppManagerWSInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.NoConnectionException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.RunInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.StatusVO;

/**
 * Interface to the App manager web services.
 *
 * @author geoff
 */
public interface AppManagerService {

  // TODO: Make this configurable!
  public static final String APP_MANAGER_CAPACITY_BUNDLE_IDENTIFIER = "appmanager.at_capacity";

  /**
   * Retrieve data of the workload.
   * 
   * @return Current workload.
   * @throws AppManagerWSInvocationException If App Manager WS interaction generated an exception.
   * @throws NoConnectionException If can't connect to the App Manager Web services.
   */
  String retrieveWorkload() throws AppManagerWSInvocationException, NoConnectionException;

  /**
   * Retrieve the latest "completed" progress status of the simulation;
   * 
   * @param appManagerId App Manager identifier.
   * @return Latest simulation completed progress (or null if no progress in statuses).
   * @throws AppManagerWSInvocationException If App Manager WS interaction generated an exception.
   * @throws NoConnectionException If can't connect to the App Manager Web services.
   */
  String retrieveLatestCompletedProgress(long appManagerId) throws AppManagerWSInvocationException,
                                                                   NoConnectionException;

  /**
   * Retrieve the progress statuses of a simulation.
   * 
   * @param appManagerId App Manager identifier.
   * @return Simulation unmodifiable progress list (or empty list if non available).
   * @throws AppManagerWSInvocationException If App Manager WS interaction generated an exception.
   * @throws NoConnectionException If can't connect to the App Manager Web services.
   */
  List<StatusVO> retrieveProgress(long appManagerId) throws AppManagerWSInvocationException,
                                                            NoConnectionException;

  /**
   * Retrieve the results of a simulation.
   * 
   * @param appManagerId App Manager identifier.
   * @return Results of simulation (in the form of a (potentially void of results!) Job entity).
   * @throws AppManagerWSInvocationException If App Manager WS interaction generated an exception.
   * @throws NoConnectionException If can't connect to the App Manager Web services.
   */
  Job retrieveResults(long appManagerId) throws AppManagerWSInvocationException,
                                                NoConnectionException;

  /**
   * Run a simulation based on the input provided.
   * 
   * @param simulationInput User input.
   * @return App Manager identifier.
   * @throws AppManagerWSInvocationException If App Manager WS interaction generated an exception.
   * @throws NoConnectionException If can't connect to the App Manager Web services.
   * @throws RunInvocationException If there's a problem invoking ApPredict.
   */
  long runSimulation(SimulationInputVO simulationInput) throws AppManagerWSInvocationException,
                                                               NoConnectionException,
                                                               RunInvocationException;

  /**
   * Signal to App Manager that the results have been uploaded and the data can be removed from
   * App Manager.
   *  
   * @param appManagerId App Manager identifier.
   * @throws AppManagerWSInvocationException If App Manager WS interaction generated an exception.
   * @throws NoConnectionException If can't connect to the App Manager Web services.
   */
  void signalResultsUploaded(long appManagerId) throws AppManagerWSInvocationException,
                                                       NoConnectionException;
}