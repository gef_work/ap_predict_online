/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller.util;

import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.compbio.client_shared.controller.util.SecurityUtil;
import uk.ac.ox.cs.compbio.client_shared.value.PortalFeature;
import uk.ac.ox.cs.compbio.client_shared.value.security.Role;
import uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService;

/**
 * Utility class for controllers.
 * 
 * @author Geoff Williams
 */
public class ControllerUtil {

  private static final Log log = LogFactory.getLog(ControllerUtil.class);

  // Hidden constructor
  private ControllerUtil() {}

  /**
   * Indicate if the current user (as determined by the thread security context)
   * has access to the specified feature.
   * 
   * @param feature Portal feature.
   * @param clientDirectService {@code client-direct} service.
   * @return {@code true} if allowed to access the feature, otherwise
   *         {@code false}.
   */
  public static boolean allowFeature(final PortalFeature feature,
                                     final ClientDirectService clientDirectService) {
    log.debug("~allowFeature() : Invoked.");
    final Set<Role> restrictedRoles = clientDirectService.retrieveRequiredRoles(feature);
    boolean allowFeature = false;

    if (restrictedRoles.isEmpty()) {
      // Unrestricted access
      allowFeature = true;
    } else {
      allowFeature = SecurityUtil.hasRoles(restrictedRoles);
    }

    return allowFeature;
  }

  /**
   * Validate a simulation identifier is a positive integer greater than 0 (but
   * less than or equal to a maximum value a {@code Long} can hold. The input
   * value is taken "as-is", i.e. no trimming or whatever.
   * 
   * @param simulationId Simulation identifier.
   * @return Indicator of validity.
   */
  public static boolean validSimulationId(final String simulationId) {
    log.debug("~validSimulationId() : Invoked with '" + simulationId + "'.");

    if (simulationId == null) {
      return false;
    }

    Long converted = null;
    try {
      converted = new Long(simulationId);
    } catch (NumberFormatException e) {
      return false;
    }

    if (converted < 1) {
      return false;
    }

    return true;
  }
}