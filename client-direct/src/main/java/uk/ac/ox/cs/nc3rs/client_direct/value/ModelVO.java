/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.value;

import org.apache.commons.lang3.StringUtils;

/**
 * Value object representing cell models.
 * 
 * @author geoff
 */
public class ModelVO implements Comparable<ModelVO> {

  private final short identifier;
  private final String name;
  private final String description;
  private final String cellmlURL;
  private final String paperURL;
  private final boolean defaultModel;

  /**
   * Initialising constructor.
   * 
   * @param identifier Model identifier, e.g. 1, 2, 3 (as passed to ApPredict)
   * @param name (Mandatory) Model name, e.g. Shannon.
   * @param description Model description, e.g. Rabbit model.
   * @param cellmlURL CellML URL.
   * @param paperURL Paper URL.
   * @param defaultModel Default model indicator.
   * @throws IllegalArgumentException If supplied parameters have an
   *                                  inappropriate value.
   */
  public ModelVO(final short identifier, final String name,
                 final String description, final String cellmlURL,
                 final String paperURL, final boolean defaultModel) {
    if (identifier < 1 || StringUtils.isBlank(name)) {
      throw new IllegalArgumentException("Model identifier must be a +ve number, and Model name must be provided.");
    }
    this.identifier = identifier;
    this.name = name;
    this.description = description;
    this.cellmlURL = cellmlURL;
    this.paperURL = paperURL;
    this.defaultModel = defaultModel;
  }

  /* (non-Javadoc)
   * @see java.lang.Comparable#compareTo(java.lang.Object)
   */
  @Override
  public int compareTo(final ModelVO otherModel) {
    return this.identifier - otherModel.getIdentifier();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + identifier;
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ModelVO other = (ModelVO) obj;
    if (identifier != other.identifier)
      return false;
    return true;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "ModelVO [identifier=" + identifier + ", name=" + name
        + ", description=" + description + ", cellmlURL=" + cellmlURL
        + ", paperURL=" + paperURL + ", defaultModel=" + defaultModel + "]";
  }

  /**
   * Retrieve the model identifier, i.e. the value submitted to
   * {@code ApPredict} in the {@code --model} arg.
   * 
   * @return The model identifier.
   */
  public short getIdentifier() {
    return identifier;
  }

  /**
   * Retrieve the model name.
   * 
   * @return Model name.
   */
  public String getName() {
    return name;
  }

  /**
   * Retrieve the model description.
   * 
   * @return Model description (or {@code null} if not assigned).
   */
  public String getDescription() {
    return description;
  }

  /**
   * Retrieve the model's CellML URL.
   * 
   * @return CellML URL (or {@code null} if not assigned).
   */
  public String getCellmlURL() {
    return cellmlURL;
  }

  /**
   * Retrieve the model's research paper URL.
   * 
   * @return Research paper URL for model (or {@code null} if not assigned).
   */
  public String getPaperURL() {
    return paperURL;
  }

  /**
   * Flag to indicate if this is the portal's default model or not.
   * 
   * @return {@code true} if the default model, otherwise {@code false}.
   */
  public boolean isDefaultModel() {
    return defaultModel;
  }
}