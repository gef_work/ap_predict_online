/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.value;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Simulation status data value object.
 * 
 * @author Geoff Williams.
 */
public class StatusVO {

  // The status AppManager generates in its output to indicate processing completion.
  private static final String appManagerCompletedText = "PROG: ..done!";
  // The status AppManager generates in its output to indicate % completed!
  private static final String appManagerPctCompletedText = "% completed";
  /*
   * The status AppManager generates in its output to indicate progress file not
   * found.
   * Note that this may be because either the the simulation hasn't started yet
   * (e.g. if it's slow loading an uncertainty/variability file) or because 
   * is finished and been deleted.
   */
  // see AppManager ProgressMonitor.java
  private static final String appManagerProgressFileNotFoundText = " Progress file not found - ";

  /** Trace level */
  public static final String TRACE_PREFIX = "T";
  /** Debug level */
  public static final String DEBUG_PREFIX = "D";
  /** Info level */
  public static final String INFO_PREFIX = "I";
  /** Warn level */
  public static final String WARN_PREFIX = "W";
  /** Error level */
  public static final String ERROR_PREFIX = "E";
  /** Fatal level */
  public static final String FATAL_PREFIX = "F";

  public static final String TEXT_PROGRESS = "PROG:";
  public static final String TEXT_PROCESS = "PROC:";

  private final String timestamp;
  private final String status;
  private final String level;
  private final type statusType;

  private static enum type {
    PROGRESS, PROCESS, OTHER
  }

  private static final Log log = LogFactory.getLog(StatusVO.class);

  /**
   * Initialising constructor.
   * 
   * @param timestamp Date/Time - null if assigning current date/time.
   * @param status Status message.
   * @param level Level, e.g. {@link #WARN_PREFIX}.
   */
  public StatusVO(final String timestamp, final String status, final String level) {
    this.timestamp = (timestamp == null) ? new Long(new Date().getTime()).toString() : timestamp;
    this.status = status;
    this.level = level;
    if (status != null) {
      if (status.contains(TEXT_PROGRESS)) {
        statusType = type.PROGRESS;
      } else if (status.contains(TEXT_PROCESS)) {
        statusType = type.PROCESS;
      } else {
        statusType = type.OTHER;
      }
    } else {
      log.info("~StatusVO() : Received a null status value when building a StatusVO!");
      statusType = type.OTHER;
    }
  }

  /**
   * Indicator of whether status data represents process data.
   * 
   * @return Indicator.
   */
  public boolean isProcessStatus() {
    return statusType.equals(type.PROCESS);
  }

  /**
   * Indicator of whether the status data is of the "Progress file not found" variety.
   * 
   * @return True if it's a progress file not found status, otherwise false.
   */
  public boolean isProgressFileNotFoundStatus() {
    return (isProgressStatus() && status.contains(appManagerProgressFileNotFoundText));
  }

  /**
   * Indicator of whether status data represents progress data.
   * 
   * @return Indicator.
   */
  public boolean isProgressStatus() {
    return statusType.equals(type.PROGRESS);
  }

  /**
   * Determine whether any of the status objects indicate that the simulation is completed.
   * 
   * @param statuses Collection of statuses to traverse.
   * @return True if simulation indicates completed, otherwise false.
   */
  public static boolean statusesIndicatesSimulationCompleted(final Collection<StatusVO> statuses) {
    if (statuses == null) {
      return false;
    }

    // pre-PKPD processing the presence of "PROG: ..done!" in the statuses was sufficient.
    for (final StatusVO status : statuses) {
      if (status != null && statusRepresentsCompleted(status.getStatus())) {
        return true;
      }
    }

    /*
     * post-PKPD some processing didn't record the "PROG: ..done!" in the 
     *   statuses as the progress file was removed before it was read in!
     * Instead traverse the statuses and look for evidence of progress file 
     *   being removed after "X% completed" being recorded. 
     */
    boolean seemsFinished = false;
    boolean containsProgFileNotFound = false;
    for (final StatusVO statusVO : statuses) {
      if (statusVO != null) {
        final String status = statusVO.getStatus();
        log.debug("~statusesIndicatesSimulationCompleted() : Status '" + status + "'.");

        if (status != null) {
          if (status.contains(appManagerProgressFileNotFoundText)) {
            log.debug("~statusesIndicatesSimulationCompleted() : Contains Progress file not found!.");
            containsProgFileNotFound = true;
          } else if (containsProgFileNotFound && status.contains(appManagerPctCompletedText)) {
            log.debug("~statusesIndicatesSimulationCompleted() : Contains % completed!.");
            seemsFinished = true;
            break;
          }
        }
      }
    }

    return seemsFinished;
  }

  /**
   * Test to indicate whether there's the simulation completion text in the status message.
   * 
   * @param statusMessage Status message.
   * @return True if message indicates simulation completed, otherwise false.
   */
  public static boolean statusRepresentsCompleted(final String statusMessage) {
    return (statusMessage != null &&
            appManagerCompletedText.equals(statusMessage.trim())) ? true : false;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "StatusVO [timestamp=" + timestamp + ", status=" + status + ", level=" + level + "]";
  }

  /**
   * @return the timestamp
   */
  public String getTimestamp() {
    return timestamp;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @return the level
   */
  public String getLevel() {
    return level;
  }
}