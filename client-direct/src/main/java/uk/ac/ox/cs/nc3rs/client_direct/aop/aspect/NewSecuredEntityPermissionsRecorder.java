/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.aop.aspect;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.compbio.client_shared.api.entity.SecuredEntity;
import uk.ac.ox.cs.nc3rs.client_direct.business.security.model.AbstractNewSecureEntityPermissionsRecorder;

/**
 * Retrieves secured entity's properties for security purposes.
 *
 * @author geoff
 */
@Component
@Aspect
@Order(1)
public class NewSecuredEntityPermissionsRecorder extends AbstractNewSecureEntityPermissionsRecorder {

  private static final Log log = LogFactory.getLog(NewSecuredEntityPermissionsRecorder.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.business.security.model.AbstractNewSecureEntityPermissionsRecorder#retrieveCreator(java.lang.Object)
   */
  @Override
  protected String retrieveCreator(final Object storedObject) {
    log.debug("~retrieveCreator() : Object '" + storedObject + "' received.");
    final SecuredEntity securedEntity = (SecuredEntity) storedObject;

    final String creator = securedEntity.getCreator();
    log.debug("~retrieveCreator() : SecuredEntity '" + securedEntity.toString() + "'.");

    return creator;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.business.security.model.AbstractNewSecureEntityPermissionsRecorder#retrievePersistenceId(java.lang.Object)
   */
  @Override
  protected Long retrievePersistenceId(final Object interceptedObject) {
    log.debug("~retrievePersistenceId() : Object '" + interceptedObject + "' received.");
    final SecuredEntity securedEntity = (SecuredEntity) interceptedObject;

    final Long securedEntityId = securedEntity.getId();
    log.debug("~retrievePersistenceId() : SecuredEntity '" + securedEntity.toString() + "'.");

    return securedEntityId;
  }
}