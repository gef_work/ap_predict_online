/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.value;

import java.math.BigDecimal;

/**
 * Measured observations of the effects of a compound on an Ion Channel.
 * <p>
 * Note that the values used within this class are as they are entered by the
 * user, so the {@link #c50} property may be representing either a pIC50 or
 * IC50.
 *
 * @author geoff
 */
public class MeasurementObservationsVO {
  private final BigDecimal c50;
  private final BigDecimal hill;
  private final BigDecimal saturation;
  private final BigDecimal spread;

  /**
   * Initialising constructor.
   * 
   * @param c50 50% inhibitory concentration.
   * @param hill Hill Coefficient.
   * @param saturation Saturation Level.
   * @param spread Spread.
   */
  public MeasurementObservationsVO(final BigDecimal c50, final BigDecimal hill,
                                   final BigDecimal saturation,
                                   final BigDecimal spread) {
    this.c50 = c50;
    this.hill = hill;
    this.saturation = saturation;
    this.spread = spread;
  }

  /**
   * Indicate if there are non-C50, e.g. Hill, saturation or spread 
   * observation values.
   * 
   * @return {@code true} if Hill, saturation or spread value present,
   *         otherwise {@code false}.
   */
  public boolean hasNonC50Value() {
    return (hasHillValue(hill) || hasSaturationValue(saturation) ||
            spread != null); 
  }

  /**
   * Indicate if there is any non-null and non-default values defined amongst
   * the args.
   * 
   * @param checkHill Hill Coefficient value (potentially {@code null}) to check.
   * @param checkSaturation Saturation Level value (potentially {@code null})
   *                        to check.
   * @param checkSpread Spread value (potentially {@code null}) to check.
   * @return {@code true} if there's a non-{@code null}, non-default value
   *         assigned, otherwise {@code false}.
   */
  public static boolean hasNonC50Value(final String checkHill,
                                       final String checkSaturation,
                                       final String checkSpread) {
    return (hasHillValue(checkHill) || hasSaturationValue(checkSaturation) ||
            checkSpread != null);
  }

  private static boolean hasHillValue(final String checkHill) {
    return (checkHill != null &&
            !checkHill.equals(SimulationInputVO.DEFAULT_HILL_COEFFICIENT));
  }

  private static boolean hasHillValue(final BigDecimal checkHill) {
    return (checkHill != null &&
            checkHill.compareTo(SimulationInputVO.DEFAULT_HILL_COEFFICIENT_BD) != 0);
  }

  private static boolean hasSaturationValue(final String checkSaturation) {
    return (checkSaturation != null &&
            !checkSaturation.equals(SimulationInputVO.DEFAULT_SATURATION));
  }

  private static boolean hasSaturationValue(final BigDecimal checkSaturation) {
    return (checkSaturation != null &&
            checkSaturation.compareTo(SimulationInputVO.DEFAULT_SATURATION_BD) != 0);
  }

  /**
   * Indicate if the values to create a compound measurements object are valid
   * - it does not test the values themselves, merely their presence to verify
   * if the necessary data is being provided.
   * 
   * @param c50 50% inhibitory concentration (value may be either an IC50 or
   *            pIC50 value).
   * @param hill Hill coefficient.
   * @param saturation Saturation Level.
   * @param spread Spread.
   * @return {@code true} if valid values, otherwise {@code false}.
   */
  public static boolean valid(final BigDecimal c50, final BigDecimal hill,
                              final BigDecimal saturation,
                              final BigDecimal spread) {
    boolean valid = true;

    // If no inhib conc value then shouldn't have anything else either!
    if (c50 == null && (hasHillValue(hill) || hasSaturationValue(saturation) ||
                        spread != null)) {
      valid = false;
    }

    return valid;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "MeasurementObservationsVO [c50=" + c50 + ", hill=" + hill + ", saturation="
        + saturation + ", spread=" + spread + "]";
  }

  /**
   * 50% Inhibitory Concentration.
   * <p>
   * This value may be either a pIC50 or an IC50 value.
   * 
   * @return 50% inhibitory concentration, or {@code null} if not defined.
   */
  public BigDecimal getC50() {
    return c50;
  }

  /**
   * Hill Coefficient.
   * 
   * @return Hill Coefficient, or {@code null} if not defined.
   */
  public BigDecimal getHill() {
    return hill;
  }

  /**
   * Saturation Level.
   * 
   * @return Saturation Level, or {@code null} if not defined.
   */
  public BigDecimal getSaturation() {
    return saturation;
  }

  /**
   * Retrieve the spread.
   * 
   * @return Spread value, or {@code null} if not defined.
   */
  public BigDecimal getSpread() {
    return spread;
  }
}