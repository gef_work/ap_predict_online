/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.controller.AbstractMessageSourceAwareController;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.controller.util.ControllerUtil;
import uk.ac.ox.cs.nc3rs.client_direct.exception.AppManagerWSInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.NoConnectionException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.ResultsNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.SimulationNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService;
import uk.ac.ox.cs.nc3rs.client_direct.value.ResultsVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationCurrentResultsVO;

/**
 * MVC Controller responding to AJAX requests from the client.
 * <p>
 * This controller returns a bean name which will be detected by the appropriate view resolver. In
 * this case the MVC Model (probably containing a Java object) will be converted into JSON format
 * for the client to render using javascript.
 *
 * @author geoff
 */
@Controller
public class AJAX extends AbstractMessageSourceAwareController {

  @Autowired @Qualifier(ClientDirectIdentifiers.COMPONENT_CLIENT_DIRECT_SERVICE)
  private ClientDirectService clientDirectService;

  private static final Log log = LogFactory.getLog(AJAX.class);

  /**
   * Bean to place into the return MVC model.
   * 
   * @author geoff
   */
  public class JSON implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String json;
    private final String exception;

    public JSON(final String json, final String exception) {
      this.json = json;
      this.exception = exception;
    }

    /** @return the json */
    public String getJson() {
      return json;
    }

    /** @return the exception */
    public String getException() {
      return exception;
    }
  }

  /**
   * Retrieve the latest progress for a simulation.
   * 
   * @param simulationId Simulation identifier.
   * @param locale Locale.
   * @param model MVC Model.
   * @return Latest progress.
   */
  @RequestMapping(value=ClientDirectIdentifiers.URL_PREFIX_LATEST_PROGRESS + "{" + ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID + "}",
                  method=RequestMethod.GET)
  public String retrieveLatestProgress(final @PathVariable(ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID)
                                             String simulationId,
                                       final Locale locale,
                                       final Model model) {
    log.debug("~retrieveLatestProgress() : [" + simulationId + "] : Invoked.");

    model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                       simulationId);
    if (!ControllerUtil.validSimulationId(simulationId)) {
      model.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_LATEST_PROGRESS,
                         new JSON(null, "Invalid Simulation identifier"));
    } else {
      JSON json = null;
      try {
        json = new JSON(clientDirectService.retrieveLatestProgress(Long.valueOf(simulationId)),
                        null);
      } catch (SimulationNotFoundException e) {
        json = new JSON(null, e.getMessage());
      } catch (AppManagerWSInvocationException e) {
        json = new JSON(null, queryMessageSource(ClientDirectIdentifiers.APP_MANAGER_SOMETHING_WRONG,
                                                 new Object[] { e.getMessage() },
                                                 locale));
      } catch (NoConnectionException e) {
        json = new JSON(null, queryMessageSource(ClientDirectIdentifiers.APP_MANAGER_OUT_OF_CONTACT,
                                                 new Object[] {}, locale));
      }
      model.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_LATEST_PROGRESS, json);
    }

    return ClientSharedIdentifiers.VIEW_NAME_JSON;
  }

  private void translator(final Set<BigDecimal> refsRequireTranslation,
                          final Map<BigDecimal, Map<String, Object>> allRefsResults,
                          final Locale locale, final String keyResultsPerRefField) {
    final Map<BigDecimal, Map<String, Object>> adjustedEntries = new HashMap<BigDecimal, Map<String, Object>>();

    // Traverse concentrations.
    for (final BigDecimal refRequiresTranslation : refsRequireTranslation) {
      log.debug("~translator() : [?] : Checking reference '" + refRequiresTranslation + "' for translation.");
      // Extract the delta APD90-containing map of the sim results at the reference
      final Map<String, Object> resultsAtRef = allRefsResults.get(refRequiresTranslation);

      final String perRefField = (String) resultsAtRef.get(keyResultsPerRefField);
      final String translated = queryMessageSource(perRefField, new Object[] {},
                                                   locale);
      if (!perRefField.equals(translated)) {
        // Translation's been made.
        log.debug("~translator() : [?] : '" + keyResultsPerRefField + "' : Translated '" + perRefField + "' to '" + translated + "'.");
        // Use the original as the source of the to-be-updated collection.
        final Map<String, Object> newResultsAtRef = new HashMap<String, Object>(resultsAtRef);
        newResultsAtRef.put(keyResultsPerRefField, translated);
        adjustedEntries.put(refRequiresTranslation, newResultsAtRef);
      }
    }

    if (!adjustedEntries.isEmpty()) {
      for (final Map.Entry<BigDecimal, Map<String, Object>> adjustedEntry : adjustedEntries.entrySet()) {
        final BigDecimal referenceOfAdjusted = adjustedEntry.getKey();
        log.debug("~translator() : [?] : Adjusting entry for '" + referenceOfAdjusted + "'.");
        final Map<String, Object> adjustedReferenceResults = adjustedEntry.getValue();

        // Replace the bundle identifier with a queried locale translation
        allRefsResults.put(referenceOfAdjusted, adjustedReferenceResults);
      }
    }
  }

  /**
   * Retrieve results of simulation.
   * 
   * @param simulationId Simulation identifier.
   * @param locale Locale.
   * @param model MVC Model.
   * @return Results JSON.
   */
  @SuppressWarnings("unchecked")
  @RequestMapping(value=ClientDirectIdentifiers.URL_PREFIX_RESULTS + "{" + ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID + "}",
                  method=RequestMethod.GET)
  public String retrieveResults(final @PathVariable(ClientSharedIdentifiers.PARAM_NAME_SIMULATION_ID)
                                      String simulationId,
                                final Locale locale,
                                final Model model) {
    final String logPrefix = "~retrieveResults() : [" + simulationId + "] : ";
    log.debug(logPrefix.concat("Invoked."));

    model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID,
                       simulationId);
    if (!ControllerUtil.validSimulationId(simulationId)) {
      model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                         new JSON(null, "Invalid Simulation identifier"));
    } else {
      try {
        final SimulationCurrentResultsVO allSimulationData = clientDirectService.retrieveSimulationResults(Long.valueOf(simulationId));
        final ResultsVO results = allSimulationData.getResults();
        if (results != null && results.hasResults()) {
          final Map<String, Object> availableResultsData = results.getResults();
          /* Need to look into the reference data for the results to see if
             there's been an inability to generate an APD90 or DeltaAPD90 value
             due to cell depolarisation. KEY_RESULTS_FOR_TRANSLATION indicates
             presence of such via a collection of references requiring
             translation. */
          final Set<BigDecimal> refsRequireTranslation =
               (Set<BigDecimal>) availableResultsData.get(ClientDirectIdentifiers.KEY_RESULTS_FOR_TRANSLATION);
          if (refsRequireTranslation != null && !refsRequireTranslation.isEmpty()) {
            final Map<BigDecimal, Map<String, Object>> allRefsResults =
                 (Map<BigDecimal, Map<String, Object>>) availableResultsData.get(ClientDirectIdentifiers.KEY_RESULTS_REFERENCE_DATA);

            translator(refsRequireTranslation, allRefsResults, locale,
                       ClientDirectIdentifiers.KEY_RESULTS_PERREF_APD90);
            translator(refsRequireTranslation, allRefsResults, locale,
                       ClientDirectIdentifiers.KEY_RESULTS_PERREF_DELTA_APD90);
          }

          /* Similar to above, but for PK data (if any) translation */
          final Map<BigDecimal, Map<String, Object>> availablePKResultsData =
               (Map<BigDecimal, Map<String, Object>>) availableResultsData.get(ClientDirectIdentifiers.KEY_PKRESULTS_TIMEPOINT_DATA);
          if (availablePKResultsData != null && !availablePKResultsData.isEmpty()) {
            for (final Map.Entry<BigDecimal, Map<String, Object>> pkResult : availablePKResultsData.entrySet()) {
              final Map<String, Object> perTimepointPKData = pkResult.getValue();

              final boolean requiresTranslation = (Boolean) perTimepointPKData.get(ClientDirectIdentifiers.KEY_PKRESULTS_PERTIMEPOINT_FORTRANS);
              if (requiresTranslation) {
                final String apd90s = (String) perTimepointPKData.get(ClientDirectIdentifiers.KEY_PKRESULTS_PERTIMEPOINT_APD90S);
                final List<String> translated = new ArrayList<String>();
                boolean apd90Translated = false;
                for (final String apd90 : apd90s.split(",")) {
                  try {
                    new BigDecimal(apd90);
                    translated.add(apd90);
                  } catch (Exception e) {
                    // Assume non-numeric so requiring translation attempt.
                    final String translatedAPD90 = queryMessageSource(apd90,
                                                                      new Object[] {},
                                                                      locale);
                    // The APD90 collection is CSV, so remove commas from translated text!
                    translatedAPD90.replaceAll(",", " - ");
                    if (!apd90.equals(translatedAPD90)) {
                      log.debug(logPrefix.concat("PK APD90 value of '" + apd90 + "' translated to '" + translatedAPD90 + "'."));
                      apd90Translated = true;
                      translated.add(translatedAPD90);
                    } else {
                      translated.add(apd90);
                    }
                  }
                }
                if (apd90Translated) {
                  // Place APD90s collection with substituted translations into results
                  final String translatedAPD90s = StringUtils.join(translated, ",");
                  log.debug(logPrefix.concat("Replacing '" + apd90s + "' translated to '" + translatedAPD90s + "'."));
                  perTimepointPKData.put(ClientDirectIdentifiers.KEY_PKRESULTS_PERTIMEPOINT_APD90S,
                                         translatedAPD90s);
                }
              }
            }
          }
        }

        String resultsJSON = null;
        try {
          // See also @JsonIgnore on SimulationInput#pkFile property!
          resultsJSON = new ObjectMapper().writeValueAsString(allSimulationData);
          log.trace(logPrefix.concat("JSON '" + resultsJSON + "'."));
          model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                             new JSON(resultsJSON, null));
        } catch (JsonMappingException e) {
          /* This can happen if hibernate lazy-loading has returned an incomplete object graph
             which jackson tries to serialize but it fails because the hibernate session no 
             longer exists as the transaction's finished. */
          log.error(logPrefix.concat("JsonMappingException '" + e.getMessage() + "'."));
          model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                             new JSON(null, "Application failure converting simulation results for display"));
        } catch (Exception e) {
          log.error(logPrefix.concat("Exception '" + e.getMessage() + "' when handling JSON."));
          model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                             new JSON(null, e.getMessage()));
        }
      } catch (AppManagerWSInvocationException e) {
        model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                           new JSON(null, queryMessageSource(ClientDirectIdentifiers.APP_MANAGER_SOMETHING_WRONG,
                                                             new Object[] { e.getMessage() },
                                                             locale)));
      } catch (NoConnectionException e) {
        model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                           new JSON(null, queryMessageSource(ClientDirectIdentifiers.APP_MANAGER_OUT_OF_CONTACT,
                                                             new Object[] {},
                                                             locale)));
      } catch (ResultsNotFoundException e) {
        model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                           new JSON(null, e.getMessage()));
      } catch (SimulationNotFoundException e) {
        model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                           new JSON(null, e.getMessage()));
      } catch (Exception e) {
        final String errorMessage = "Unexpected error encountered '" + e.getMessage() + "'";
        log.error(logPrefix.concat(errorMessage));
        model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS,
                            new JSON(null, errorMessage));
      }
    }

    return ClientSharedIdentifiers.VIEW_NAME_JSON;
  }
}