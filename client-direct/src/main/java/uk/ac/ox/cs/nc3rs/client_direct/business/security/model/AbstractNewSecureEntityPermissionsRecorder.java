/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.business.security.model;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import uk.ac.ox.cs.compbio.client_shared.api.entity.SecuredEntity;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.aop.aspect.MethodInterceptingDomainObjectUtil;
import uk.ac.ox.cs.nc3rs.client_direct.business.security.util.GrantedAuthorityAndSidUtil;
import uk.ac.ox.cs.nc3rs.client_direct.business.security.util.PermissionUtil;
import uk.ac.ox.cs.nc3rs.client_direct.manager.PermissionManager;

/**
 * Abstract secure object permissions recorder.
 * <p>
 * Generally used by aspects around the DAO store* function when creating a new entity.
 * </p>
 *
 * @author geoff
 */
public abstract class AbstractNewSecureEntityPermissionsRecorder {

  @Autowired @Qualifier(ClientDirectIdentifiers.COMPONENT_PERMISSION_MANAGER)
  private PermissionManager permissionManager;

  private static final Log log = LogFactory.getLog(AbstractNewSecureEntityPermissionsRecorder.class);

  /**
   * Retrieve the name of whatever created the object.
   * 
   * @param storedObject Object being persisted.
   * @return Name of object creator.
   */
  protected abstract String retrieveCreator(Object storedObject);

  /**
   * Retrieve the persistence identifier of the intercepted object.
   * 
   * @param interceptedObject Intercepted object.
   * @return Persistence identifier.
   */
  protected abstract Long retrievePersistenceId(Object interceptedObject);

  /**
   * Around advice on the DAO store() method which monitors a change in the identity (not strictly
   * the persistence identity!) object to determine if a new object has been created.
   * If so, then automatically give the user creating the object the administration authority for
   * the newly created object.
   * 
   * @param proceedingJoinPoint The JoinPoint being advised.
   * @return Object (or {@code null}) intended to be return from the store() routine.
   * @throws Throwable Anything thrown by the joinpoint.
   */
  @Around("execution(public * uk.ac.ox.cs.nc3rs.client_direct.dao.*DAO.store(..)) ||"
      +   "execution(public * uk.ac.ox.cs.compbio.client_shared.dao.*DAO.store(..))")
  public Object creationAdvice(final ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
    log.debug("~creationAdvice() : Invoked.");

    // Find the secured entity in the joinpoint args
    final Object securedEntity = MethodInterceptingDomainObjectUtil.getDomainObjectInstance(proceedingJoinPoint,
                                                                                            SecuredEntity.class);
    final Long preStoreId = retrievePersistenceId(securedEntity);

    // Invoke the underlying method
    final Object stored = proceedingJoinPoint.proceed();

    final Long postStoreId = retrievePersistenceId(stored);

    // Update various things if we created a new object?
    if ((postStoreId != null) && (!postStoreId.equals(preStoreId))) {
      log.debug("~creationAdvice(ProceedingJoinPoint) : Object id [" + preStoreId + "]-[" + postStoreId + "] change.");

      assignAdministrationPermissionToCreator(stored.getClass(), postStoreId,
                                              retrieveCreator(stored));
    }

    return stored;
  }

  /**
   * Assign administration permission to the newly created secured entity for the identified VREUser.
   * 
   * @param secureObjectClass Secured object class.
   * @param secureObjectId Persistence identity to apply permission settings for.
   * @param creator Identity of the object creator.
   */
  protected void assignAdministrationPermissionToCreator(final Class<?> secureObjectClass,
                                                         final Long secureObjectId,
                                                         final String creator) {
    log.debug("~assignAdministrationPermissionToCreator() : Assigning admin permission to creator [" + secureObjectClass + "] - [" + secureObjectId + "] - [" + creator + "]");

    permissionManager.addPermission(secureObjectClass, secureObjectId,
                                    GrantedAuthorityAndSidUtil.retrieveCreatorAsPrincipalSid(creator),
                                    PermissionUtil.retrieveAdministrationPermission());
  }
}