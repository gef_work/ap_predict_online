/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation;
import uk.ac.ox.cs.nc3rs.client_direct.exception.DataRetrievalException;
import uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService;

/**
 * MVC Controller for front page display.
 * 
 * @author geoff
 */
@Controller
public class Main {

  @Autowired @Qualifier(ClientDirectIdentifiers.COMPONENT_CLIENT_DIRECT_SERVICE)
  private ClientDirectService clientDirectService;

  private static final Log log = LogFactory.getLog(Main.class);

  /**
   * Ensure that the correct model attributes are assigned for when the main page is to be shown.
   * 
   * @param clientDirectService Client (direct) service.
   * @param principal User principal.
   * @param model UI model (for population).
   * @param errorMessage Error message.
   */
  public static void setModelPropertiesForMain(final ClientDirectService clientDirectService,
                                               final Principal principal,
                                               final Model model,
                                               final String errorMessage) {
    log.debug("~setModelPropertiesForMain() : Invoked.");
    final List<String> errorMessages = new ArrayList<String>();
    if (!StringUtils.isBlank(errorMessage)) {
      errorMessages.add(errorMessage);
    }

    final List<Simulation> simulations  = new ArrayList<Simulation>();
    try {
      simulations.addAll(clientDirectService.retrieveUserSimulations(principal.getName()));
    } catch (DataRetrievalException e) {
      errorMessages.add(e.getMessage());
    }

    log.debug("~setModelPropertiesForMain() : Retrieved '" + simulations.size() + "' simulations.");
    if (!simulations.isEmpty()) {
      model.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_SIMULATIONS,
                         simulations);
      model.addAttribute(ClientDirectIdentifiers.MODEL_ATTRIBUTE_MODELS,
                         clientDirectService.retrieveModels());
      model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MODEL_NAMES,
                         clientDirectService.retrieveModelNames());
    }
    if (!errorMessages.isEmpty()) {
      model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR,
                         StringUtils.join(errorMessages, "; "));
    }
  }

  /**
   * Show the main page.
   * 
   * @param principal User principal.
   * @param model MVC model.
   * @return View name.
   */
  @RequestMapping(method=RequestMethod.GET,
                  value=ClientDirectIdentifiers.VIEW_MAIN)
  public String showMain(final Principal principal,
                         final Model model) {
    log.debug("~showMain() : Invoked.");

    final String showPage = ClientDirectIdentifiers.PAGE_MAIN;

    setModelPropertiesForMain(clientDirectService, principal, model, null);

    return showPage;
  }
}