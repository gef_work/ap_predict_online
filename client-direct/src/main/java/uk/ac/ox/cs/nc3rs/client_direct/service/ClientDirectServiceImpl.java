/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.service;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.compbio.client_shared.manager.FileManager;
import uk.ac.ox.cs.compbio.client_shared.value.PortalFeature;
import uk.ac.ox.cs.compbio.client_shared.value.file.FILE_DATA_TYPE;
import uk.ac.ox.cs.compbio.client_shared.value.file.FileStoreActionOutcomeVO;
import uk.ac.ox.cs.compbio.client_shared.value.security.Role;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Job;
import uk.ac.ox.cs.nc3rs.client_direct.entity.JobPKResult;
import uk.ac.ox.cs.nc3rs.client_direct.entity.JobResult;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation;
import uk.ac.ox.cs.nc3rs.client_direct.exception.AppManagerWSInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.DataRetrievalException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.NoConnectionException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.ResultsNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.RunInvocationException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.SimulationNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.manager.ConfigurationManager;
import uk.ac.ox.cs.nc3rs.client_direct.manager.SimulationManager;
import uk.ac.ox.cs.nc3rs.client_direct.value.IonCurrent;
import uk.ac.ox.cs.nc3rs.client_direct.value.ModelVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.ResultsVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationCurrentResultsVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.StatusVO;
import uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.service.AppManagerService;

/**
 * Implementation of the client (direct) service interface.
 * 
 * @author geoff
 */
@Component(ClientDirectIdentifiers.COMPONENT_CLIENT_DIRECT_SERVICE)
public class ClientDirectServiceImpl implements ClientDirectService {

  @Autowired @Qualifier(ClientDirectIdentifiers.COMPONENT_APP_MANAGER_SERVICE)
  private AppManagerService appManagerService;

  @Autowired @Qualifier(ClientDirectIdentifiers.COMPONENT_CONFIGURATION_MANAGER)
  private ConfigurationManager configurationManager;

  @Autowired @Qualifier(ClientSharedIdentifiers.COMPONENT_FILE_MANAGER)
  private FileManager fileManager;

  @Autowired @Qualifier(ClientDirectIdentifiers.COMPONENT_SIMULATION_MANAGER)
  private SimulationManager simulationManager;

  private static final Log log = LogFactory.getLog(ClientDirectServiceImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService#deleteSimulation(long)
   */
  @Override
  public void deleteSimulation(final long simulationId) {
    log.debug("~deleteSimulation() : Invoked for simulation id '" + simulationId + "'.");

    simulationManager.deleteSimulation(simulationId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService#retrieveAllFiles()
   */
  @Override
  public List<PortalFile> retrieveAllFiles() {
    log.debug("~retrieveAllFiles() : Invoked.");

    return fileManager.retrieveAllFiles();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService#retrieveAllSimulations()
   */
  @Override
  public List<Simulation> retrieveAllSimulations() {
    log.debug("~retrieveAllSimulations() : Invoked.");

    return simulationManager.retrieveAllSimulations();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService#retrieveC50Units()
   */
  @Override
  public Map<String, String> retrieveC50Units() {
    log.debug("~retrieveC50Units() : Invoked.");

    return configurationManager.retrieveC50Units();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService#retrieveCredibleIntervalPctiles()
   */
  @Override
  public Set<BigDecimal> retrieveCredibleIntervalPctiles() {
    log.debug("~retrieveCredibleIntervalPctiles() : Invoked.");

    return configurationManager.retrieveCredibleIntervalPctiles();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService#retrieveLatestProgress(long)
   */
  @Override
  public String retrieveLatestProgress(final long simulationId)
                                       throws AppManagerWSInvocationException,
                                              NoConnectionException,
                                              SimulationNotFoundException {
    final String logPrefix = "~retrieveLatestProgress() : [" + simulationId + "] : ";
    log.debug(logPrefix.concat("Invoked."));

    final Simulation simulation = retrieveSimulation(simulationId);

    if (simulation.isCompleted()) {
      return PROGRESS_INDICATING_SIMULATION_HAS_FINISHED;
    } else {
      // Rare possibility that two simulation's share an app manager identifier - See doco!
      final long appManagerId = simulation.getAppManagerId();

      final String latestProgress = appManagerService.retrieveLatestCompletedProgress(appManagerId);
      if (latestProgress == null) {
        return PROGRESS_INDICATING_SOME_KIND_OF_PROBLEM;
      } else {
        if (StatusVO.statusRepresentsCompleted(latestProgress)) {
          // This *usually* gets invoked when viewing the simulations table.
          log.debug(logPrefix.concat("Assigned completed via latest progress!"));
          updateSimulationStatus(simulation);
        }
        return latestProgress;
      }
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService#retrieveModelNames()
   */
  @Override
  public Map<Short, String> retrieveModelNames() {
    log.debug("~retrieveModelNames() : Invoked.");

    return configurationManager.retrieveModelNames();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService#retrieveModels()
   */
  @Override
  public List<ModelVO> retrieveModels() {
    log.debug("~retrieveModels() : Invoked.");

    return configurationManager.retrieveModels();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService#retrieveRequiredRoles(uk.ac.ox.cs.compbio.client_shared.value.PortalFeature)
   */
  @Override
  public Set<Role> retrieveRequiredRoles(final PortalFeature portalFeature) {
    log.debug("~retrieveRequiredRoles() : Invoked for feature '" + portalFeature + "'.");

    return configurationManager.retrieveRequiredRoles(portalFeature);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService#retrieveSimulation(long)
   */
  @Override
  public Simulation retrieveSimulation(final long simulationId)
                                       throws SimulationNotFoundException {
    log.debug("~retrieveSimulation() : [" + simulationId + "] : Invoked.");

    final Simulation simulation = simulationManager.retrieveSimulation(simulationId);

    return simulation;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService#retrieveSimulationResults(java.lang.Long)
   */
  @SuppressWarnings("deprecation")
  @Override
  public SimulationCurrentResultsVO retrieveSimulationResults(final long simulationId)
                                                              throws AppManagerWSInvocationException,
                                                                     NoConnectionException,
                                                                     ResultsNotFoundException,
                                                                     SimulationNotFoundException {
    final String logPrefix = "~retrieveSimulationResults() : [" + simulationId + "] : "; 
    log.debug(logPrefix.concat("Invoked."));

    Simulation simulation = retrieveSimulation(simulationId);

    final long appManagerId = simulation.getAppManagerId();

    final List<StatusVO> progress = new ArrayList<StatusVO>();
    ResultsVO results = null;

    if (simulation.isCompleted()) {
      log.debug(logPrefix.concat("Simulation indicates completed."));
      /* First try to find results which have been persisted locally (i.e. by
         ClientDirect). */
      Job job = simulationManager.retrieveResults(simulationId);
      boolean hasJobResults = job.hasResults();
      if (!hasJobResults) {
        log.debug(logPrefix.concat("client-direct doesn't have results."));
        /* Failing ClientDirect having persisted results, see if AppManager has
           any. */
        final Job appManagerJob = appManagerService.retrieveResults(appManagerId);
        hasJobResults = appManagerJob.hasResults();
        if (hasJobResults) {
          // AppManager had job results, persist them locally.
          log.debug(logPrefix.concat("app-manager has results."));
          job = simulationManager.recordResults(simulationId, appManagerJob);
          appManagerService.signalResultsUploaded(appManagerId);
        }
      }

      if (!hasJobResults) {
        if (!simulation.withinPostCompletionGracePeriod()) {
          // Uh-oh! Simulation completed, no job results and not within a grace period!
          log.info(logPrefix.concat("Status 'completed' but no results found!"));
          throw new ResultsNotFoundException("Simulation '" + simulationId + "' has status 'completed' but no results found!");
        }
      } else {
        final Map<String, Object> jobMap = new HashMap<String, Object>();

        jobMap.put(ClientDirectIdentifiers.KEY_RESULTS_JOB_ID,
                   job.getAppManagerId());
        jobMap.put(ClientDirectIdentifiers.KEY_RESULTS_MESSAGES,
                   job.getMessages());
        jobMap.put(ClientDirectIdentifiers.KEY_RESULTS_DELTA_APD90_PERCENTILE_NAMES,
                   job.getDeltaAPD90PercentileNames());
        jobMap.put(ClientDirectIdentifiers.KEY_RESULTS_INPUT_OPTION,
                   simulation.retrieveInputOption());

        /* Retain a list of references where there was an ApPredict code (rather
           than a numerical value) for the APD90 and DeltaAPD90 value. */
        final Set<BigDecimal> forTranslation = new HashSet<BigDecimal>();
        // Note: BigDecimal has a irregular natural sorting, so can't use TreeMap.
        final Map<BigDecimal, Map<String, Object>> keyedOnReference = new HashMap<BigDecimal, Map<String, Object>>();
        for (final JobResult jobResult : job.getJobResults()) {
          final Map<String, Object> referenceData = new HashMap<String, Object>();

          final BigDecimal reference = jobResult.getReference();

          referenceData.put(ClientDirectIdentifiers.KEY_RESULTS_PERREF_APD90,
                            jobResult.getAPD90());
          referenceData.put(ClientDirectIdentifiers.KEY_RESULTS_PERREF_DELTA_APD90,
                            jobResult.getDeltaAPD90());
          referenceData.put(ClientDirectIdentifiers.KEY_RESULTS_PERREF_TIMES,
                            jobResult.getTimes());
          referenceData.put(ClientDirectIdentifiers.KEY_RESULTS_PERREF_VOLTAGES,
                            jobResult.getVoltages());
          referenceData.put(ClientDirectIdentifiers.KEY_RESULTS_PERREF_QNET,
                            jobResult.getQNet());
          keyedOnReference.put(reference, referenceData);

          /*
           * Any simulation results generated since June 2020 will be returned
           * a value of {@code false} from JobResult#getForTranslation().
           * Prior to June 2020 it could be either {@code true} or {@code false},
           * depending on whether NoActionPotential_X values had been
           * encountered in APD90 or Δ APD90 values. 
           */
          if (jobResult.getForTranslation()) {
            forTranslation.add(reference);
          }

        }
        jobMap.put(ClientDirectIdentifiers.KEY_RESULTS_REFERENCE_DATA,
                   keyedOnReference);
        jobMap.put(ClientDirectIdentifiers.KEY_RESULTS_FOR_TRANSLATION,
                   forTranslation);

        if (job.hasPKResults()) {
          // Traverse any PKPD results.
          final Map<BigDecimal, Map<String, Object>> keyedOnTimepoint = new HashMap<BigDecimal, Map<String, Object>>();
          for (final JobPKResult jobPKResult : job.getJobPKResults()) {
            final Map<String, Object> timepointData = new HashMap<String, Object>();

            final BigDecimal timepoint = jobPKResult.getTimepoint();

            timepointData.put(ClientDirectIdentifiers.KEY_PKRESULTS_PERTIMEPOINT_APD90S,
                              jobPKResult.getApd90s());
            timepointData.put(ClientDirectIdentifiers.KEY_PKRESULTS_PERTIMEPOINT_FORTRANS,
                              jobPKResult.isForTranslation());

            // NOTE : Keyed on a BigDecimal timepoint, so not sorted numerically!
            keyedOnTimepoint.put(timepoint, timepointData);
          }
          jobMap.put(ClientDirectIdentifiers.KEY_PKRESULTS_TIMEPOINT_DATA,
                     keyedOnTimepoint);
        }

        results = new ResultsVO(jobMap);

        log.debug(logPrefix.concat("Results retrieved."));
      }
    } else {
      // Simulation incomplete (hopefully running!), retrieve progress.
      progress.addAll(appManagerService.retrieveProgress(appManagerId));
      log.debug(logPrefix.concat("Retrieved '" + progress.size() + "' progresses."));
      if (progress.isEmpty()) {
        // This is legitimate situation when, for example, during large PK initial processing. 
        log.debug(logPrefix.concat("Simulation indicates incomplete, but no progress available."));
      } else {
        if (StatusVO.statusesIndicatesSimulationCompleted(progress)) {
          // This *usually* gets invoked when actively viewing the simulation.
          log.debug(logPrefix.concat("Assigned completed via results!"));
          simulation = updateSimulationStatus(simulation);
        }
      }
    }

    return new SimulationCurrentResultsVO(simulation, progress, results);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService#retrieveSpreads()
   */
  @Override
  public Map<IonCurrent, BigDecimal> retrieveSpreads() {
    log.debug("~retrieveSpreads() : Invoked.");

    return configurationManager.retrieveSpreads();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService#retrieveUserSimulations(java.lang.String)
   */
  @Override
  public List<Simulation> retrieveUserSimulations(final String user)
                                                  throws DataRetrievalException {
    log.debug("~retrieveUserSimulations() : Invoked for user '" + user + "'.");

    return simulationManager.retrieveUserSimulations(user);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService#runSimulation(uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO, java.lang.String)
   */
  @Override
  public long runSimulation(final SimulationInputVO simulationInput,
                            final String initiator)
                            throws AppManagerWSInvocationException, 
                                   NoConnectionException,
                                   RunInvocationException {
    log.debug("~runSimulation() : Invoked with simulation input '" + simulationInput.toString() + "'.");

    /* TODO : This will run the simulation even if there's a failure in
              createSimulation (although any potential problems should have been
              addressed in SimulationInputVO creation!) */ 
    final long appManagerId = appManagerService.runSimulation(simulationInput);

    return simulationManager.createSimulation(appManagerId, initiator,
                                              simulationInput);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.service.ClientDirectService#storeFile(uk.ac.ox.cs.compbio.client_shared.value.file.FILE_DATA_TYPE, java.io.InputStream, java.lang.String, java.lang.String)
   */
  @Override
  public FileStoreActionOutcomeVO storeFile(final FILE_DATA_TYPE fileDataType,
                                            final InputStream inputStream,
                                            final String name,
                                            final String uploader)
                                            throws IllegalArgumentException {
    if (fileDataType == null || inputStream == null || StringUtils.isEmpty(uploader)) {
      throw new IllegalArgumentException("File data type, file, and uploader's name required to store file.");
    }
    log.debug("~storeFile() : Invoked for file data type '" + fileDataType + "'.");

    return fileManager.storeFile(fileDataType, inputStream, name, uploader);
  }

  private Simulation updateSimulationStatus(final Simulation simulation) {
    simulation.assignCompleted();
    return simulationManager.save(simulation);
  }
}