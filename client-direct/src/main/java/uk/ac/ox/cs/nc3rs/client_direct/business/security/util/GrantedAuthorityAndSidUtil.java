/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.business.security.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.acls.domain.GrantedAuthoritySid;
import org.springframework.security.acls.domain.PrincipalSid;
import org.springframework.security.acls.model.Sid;

/**
* Utility class for working around with :
 * <ul>
 *   <li>
 *     {@link Sid} objects (both {@link PrincipalSid} and {@link GrantedAuthoritySid}) for use in
 *     Spring Security access control mechanisms.<br>
 *     These usually involve data derived from the Spring Security table <code>acl_sid, acl_class,
 *     acl_object_identity, acl_entry</code>.
 *   </li>
 * </ul>
  *
 * @author geoff
 */
public class GrantedAuthorityAndSidUtil {

  private static final Log log = LogFactory.getLog(GrantedAuthorityAndSidUtil.class);

  /**
   * Retrieve a <code>PrincipalSid</code> (i.e. a String object wrapped in a fancy object name)
   * derived from the secured object creator.
   * 
   * @param creator Secured object creator.
   * @return PrincipalSid representing the creator.
   */
  public static Sid retrieveCreatorAsPrincipalSid(final String creator) {
    assert (creator != null) : "Inappropriate attempt to retrieve user as principal sid using null creator name.";
    log.debug("~retrieveCreatorAsPrincipalSid() : Retrieving Creator '" + creator + "' as a PrincipalSid.");

    final PrincipalSid sid = new PrincipalSid(creator);

    return sid;
  }
}