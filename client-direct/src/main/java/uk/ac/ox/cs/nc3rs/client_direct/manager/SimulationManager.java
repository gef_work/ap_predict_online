/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.manager;

import java.util.List;

import uk.ac.ox.cs.nc3rs.client_direct.entity.Job;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation;
import uk.ac.ox.cs.nc3rs.client_direct.exception.DataRetrievalException;
import uk.ac.ox.cs.nc3rs.client_direct.exception.SimulationNotFoundException;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;

/**
 * Manager for simulation operations.
 * 
 * @author geoff
 */
public interface SimulationManager {

  /**
   * Create a simulation (and an associated Job) based on the user input.
   * 
   * @param appManagerId App Manager identifier.
   * @param creator Name of user initiating the simulation.
   * @param simulationInput User input.
   * @return Simulation identifier.
   */
  long createSimulation(long appManagerId, String creator,
                        SimulationInputVO simulationInput);

  /**
   * Remove the simulation (if it's completed, otherwise the request is silently
   * ignored).
   * 
   * @param simulationId Simulation identifier.
   */
  void deleteSimulation(long simulationId);

  /**
   * Record the simulation's results.
   * 
   * @param simulationId Simulation identifier.
   * @param appManagerJob Job, as derived from data retrieved from app manager.
   * @return Persisted Job.
   */
  Job recordResults(long simulationId, Job appManagerJob);

  /**
   * Retrieve all simulations.
   * 
   * @return Simulations all simulations.
   */
  List<Simulation> retrieveAllSimulations();

  /**
   * Retrieve results for a simulation.
   * 
   * @param simulationId Simulation identifier.
   * @return Simulation job (with attached results if there are any).
   */
  Job retrieveResults(long simulationId);

  /**
   * Retrieve the simulation and all dependent objects, e.g. simulation input,
   * portal file, etc..
   * 
   * @param simulationId Simulation identifier.
   * @return Simulation.
   * @throws SimulationNotFoundException If simulation can't be found.
   */
  Simulation retrieveSimulation(long simulationId)
                                throws SimulationNotFoundException;

  /**
   * Retrieve a collection of the user's Simulations in the database along with
   * dependent objects except for portal file objects.
   *
   * @param user Simulation creator.
   * @return Simulations which the specified user created (or empty list if none
   *         found).
   * @throws DataRetrievalException If problem retrieving data.
   */
  List<Simulation> retrieveUserSimulations(String user)
                                           throws DataRetrievalException;
  ;

  /**
   * Save a simulation.
   * 
   * @param simulation Simulation to save.
   * @return Saved simulation.
   */
  Simulation save(Simulation simulation);
}