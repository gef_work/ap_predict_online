/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the names of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.ws.app_manager.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.AssociatedItem;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ChannelData;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ObjectFactory;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.TypeCredibleIntervals;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.TypePlasmaConcsHiLo;
import uk.ac.ox.cs.nc3rs.client_direct.value.MeasurementObservationsVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.IonCurrent;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;

/**
 * Utility class for App Manager operations.
 *
 * @author geoff
 */
public final class AppManagerUtil {

  private static final String cellMLDataSourceType = "text/xml; charset=UTF-8";
  private static final String pkDataSourceType = "text/tab-separated-values; charset=UTF-8";
  private static final String invokingComponentName = "client-direct";

  private static final ObjectFactory objectFactory = new ObjectFactory();

  private static final Log log = LogFactory.getLog(AppManagerUtil.class);

  // Assign Ion Channel pIC50 to the ApPredict run request.
  private static void assignToRequest(final ApPredictRunRequest request,
                                      final IonCurrent ionCurrent,
                                      final BigDecimal pIC50,
                                      final BigDecimal hill,
                                      final BigDecimal saturation,
                                      final BigDecimal spread) {
    final AssociatedItem associatedItem = objectFactory.createAssociatedItem();
    associatedItem.setC50(pIC50);
    associatedItem.setHill(hill);
    associatedItem.setSaturation(saturation);

    final ChannelData channelData = objectFactory.createChannelData();
    channelData.getPIC50Data().add(associatedItem);
    channelData.setC50Spread(spread);
 
    switch (ionCurrent) {
      case ICaL :
        request.setICaL(channelData);
        break;
      case IKr :
        request.setIKr(channelData);
        break;
      case IK1 :
        request.setIK1(channelData);
        break;
      case IKs :
        request.setIKs(channelData);
        break;
      case Ito :
        request.setIto(channelData);
        break;
      case INa :
        request.setINa(channelData);
        break;
      case INaL :
        request.setINaL(channelData);
        break;
    }
  }

  /**
   * Create the ApPredict WS SOAP object.
   * 
   * @param simulationInputVO Simulation input.
   * @return ApPredict WS SOAP object.
   */
  public static ApPredictRunRequest createRunRequest(final SimulationInputVO simulationInputVO) {
    log.debug("~createRunRequest() : Invoked.");

    final ApPredictRunRequest runRequest = objectFactory.createApPredictRunRequest();

    final PortalFile cellMLFile = simulationInputVO.getCellMLFile();
    if (cellMLFile != null) {
      final String cellMLFileContent = cellMLFile.getContent();
      if (!StringUtils.isBlank(cellMLFileContent)) {
        DataSource cellMLDataSource = null;
        try {
          cellMLDataSource = new ByteArrayDataSource(cellMLFileContent,
                                                     cellMLDataSourceType);
        } catch (IOException e) {
          log.error("~createRunRequest() : CellML content to ByteArrayDataSource : IOException '" + e.getMessage() + "'.");
        }
        if (cellMLDataSource != null) {
          runRequest.setCellMLFile(new DataHandler(cellMLDataSource));
        }
      }
    } else {
      runRequest.setModelIdentifier(simulationInputVO.getModelIdentifier());
    }

    final BigDecimal pacingFrequency = simulationInputVO.getPacingFrequency();
    if (pacingFrequency != null) {
      runRequest.setPacingFreq(pacingFrequency);
    }
    final BigDecimal pacingMaxTime = simulationInputVO.getPacingMaxTime();
    if (pacingMaxTime != null) {
      runRequest.setPacingMaxTime(pacingMaxTime);
    }
    final BigDecimal pIC50ICaL = simulationInputVO.retrievePIC50ICaL();
    if (pIC50ICaL != null) {
      final MeasurementObservationsVO iCaLObservations = simulationInputVO.getICaLObservations();
      assignToRequest(runRequest, IonCurrent.ICaL, pIC50ICaL,
                      iCaLObservations.getHill(),
                      iCaLObservations.getSaturation(),
                      iCaLObservations.getSpread());
    }
    final BigDecimal pIC50IK1 = simulationInputVO.retrievePIC50Ik1();
    if (pIC50IK1 != null) {
      final MeasurementObservationsVO iK1Observations = simulationInputVO.getIK1Observations();
      assignToRequest(runRequest, IonCurrent.IK1, pIC50IK1,
                      iK1Observations.getHill(),
                      iK1Observations.getSaturation(),
                      iK1Observations.getSpread());
    }
    final BigDecimal pIC50IKr = simulationInputVO.retrievePIC50Ikr();
    if (pIC50IKr != null) {
      final MeasurementObservationsVO iKrObservations = simulationInputVO.getIKrObservations();
      assignToRequest(runRequest, IonCurrent.IKr, pIC50IKr,
                      iKrObservations.getHill(),
                      iKrObservations.getSaturation(),
                      iKrObservations.getSpread());
    }
    final BigDecimal pIC50IKs = simulationInputVO.retrievePIC50Iks();
    if (pIC50IKs != null) {
      final MeasurementObservationsVO iKsObservations = simulationInputVO.getIKsObservations();
      assignToRequest(runRequest, IonCurrent.IKs, pIC50IKs,
                      iKsObservations.getHill(),
                      iKsObservations.getSaturation(),
                      iKsObservations.getSpread());
    }
    final BigDecimal pIC50INa = simulationInputVO.retrievePIC50INa();
    if (pIC50INa != null) {
      final MeasurementObservationsVO iNaObservations = simulationInputVO.getINaObservations();
      assignToRequest(runRequest, IonCurrent.INa, pIC50INa,
                      iNaObservations.getHill(),
                      iNaObservations.getSaturation(),
                      iNaObservations.getSpread());
    }
    final BigDecimal pIC50INaL = simulationInputVO.retrievePIC50INaL();
    if (pIC50INaL != null) {
      final MeasurementObservationsVO iNaLObservations = simulationInputVO.getINaLObservations();
      assignToRequest(runRequest, IonCurrent.INaL, pIC50INaL,
                      iNaLObservations.getHill(),
                      iNaLObservations.getSaturation(),
                      iNaLObservations.getSpread());
    }
    final BigDecimal pIC50Ito = simulationInputVO.retrievePIC50Ito();
    if (pIC50Ito != null) {
      final MeasurementObservationsVO itoObservations = simulationInputVO.getItoObservations();
      assignToRequest(runRequest, IonCurrent.Ito, pIC50Ito,
                      itoObservations.getHill(),
                      itoObservations.getSaturation(),
                      itoObservations.getSpread());
    }

    final String credibleIntervalPctiles = simulationInputVO.getCredibleIntervalPctiles();
    final TypeCredibleIntervals typeCredibleIntervals = objectFactory.createTypeCredibleIntervals();
    if (!StringUtils.isBlank(credibleIntervalPctiles)) {
      typeCredibleIntervals.setCalculateCredibleIntervals(true);
      typeCredibleIntervals.setPercentiles(credibleIntervalPctiles);
    } else {
      typeCredibleIntervals.setCalculateCredibleIntervals(false);
      typeCredibleIntervals.setPercentiles(null);
    }
    runRequest.setCredibleIntervals(typeCredibleIntervals);

    // Default is mandatory (as defined by WSDL) switch off of plasma conc log scaling
    runRequest.setPlasmaConcLogScale(false);

    final List<String> plasmaConcPoints = simulationInputVO.getPlasmaConcPoints();
    final BigDecimal plasmaConcMax = simulationInputVO.getPlasmaConcMax();
    final PortalFile pkFile = simulationInputVO.getPkFile();

    switch (simulationInputVO.retrieveInputOption()) {
      case CONC_POINTS :
        // --plasma-concs
        runRequest.setPlasmaConcsAssigned(StringUtils.join(plasmaConcPoints, ","));

        break;
      case CONC_RANGE :
        // --plasma-conc-count
        final Short plasmaIntPtCount = simulationInputVO.getPlasmaIntPtCount();
        if (plasmaIntPtCount != null) {
          runRequest.setPlasmaConcCount(plasmaIntPtCount);
        }

        // --plasma-conc-high
        final TypePlasmaConcsHiLo highLowConcs = objectFactory.createTypePlasmaConcsHiLo();
        highLowConcs.setHigh(plasmaConcMax);
        // --plasma-conc-low
        final BigDecimal plasmaConcMin = simulationInputVO.getPlasmaConcMin();
        if (plasmaConcMin != null) {
          highLowConcs.setLow(plasmaConcMin);
        }
        runRequest.setPlasmaConcsHiLo(highLowConcs);
        runRequest.setPlasmaConcLogScale(simulationInputVO.getPlasmaIntPtLogScale());

        break;
      case PK :
        // --pkFile
        final String pkFileContent = pkFile.getContent();
        if (!StringUtils.isBlank(pkFileContent)) {
          DataSource pkDataSource = null;
          try {
            pkDataSource = new ByteArrayDataSource(pkFileContent,
                                                   pkDataSourceType);
          } catch (IOException e) {
            log.error("~createRunRequest() : PKFile content to ByteArrayDataSource : IOException '" + e.getMessage() + "'.");
          }

          if (pkDataSource != null) {
            // Had used a second DataHandler arg of MediaType.APPLICATION_OCTET_STREAM_VALUE rejected!
            runRequest.setPKFile(new DataHandler(pkDataSource));
          }
        }

        break;
    }

    // Value here corresponds to the enumerated value for app-manager WSDL's Invocation_Source element. 
    runRequest.setInvocationSource(invokingComponentName);

    log.debug("~createRunRequest() : Created '" + runRequest.toString() + "'.");

    return runRequest;
  }
}