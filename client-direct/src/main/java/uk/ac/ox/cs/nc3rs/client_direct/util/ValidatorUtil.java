/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.util;

import java.math.BigDecimal;

/**
 * General value validating utility class.
 * 
 * @author geoff
 */
public final class ValidatorUtil {

  /**
   * The type of object to convert to.
   */
  public static enum TYPE {
    BIGDECIMAL,
    SHORT
  }

  // Hidden constructor
  private ValidatorUtil() {}

  /**
   * Check value can be constructed of the specified type.
   * <p>
   *   Note that even if failOnNull is set to false, if a null is passed to a object
   *   type's constructor, this may itself throw an exception, which will generate a false return
   *   value. I.e. Even though a BigDecimal property value can be assigned as null, the object can't 
   *   be constructed with a null value. 
   * </p>
   * 
   * @param val Value to check.
   * @param type Type expected.
   * @param failOnNull Consider a null value to be an invalid value.
   * @return True if val is of type expected.
   */
  public static boolean isValid(final String val, final TYPE type, final boolean failOnNull) {
    if (failOnNull && val == null) {
      return false;
    }
    try {
      switch (type) {
        case BIGDECIMAL :
          new BigDecimal(val);
          break;
        case SHORT :
          new Short(val);
          break;
        default :
          break;
      }
    } catch (Exception e) {
      // If failOnNull is false, this primarily catches vals of null when the type constructor won't accept it. 
      return false;
    }
    return true;
  }

  /**
   * Check value is of specified type. A passed null value will be considered to be an invalid
   * value.
   * 
   * @param val Value to check.
   * @param type Type expected.
   * @return True if value is of type expected.
   */
  public static boolean isValid(final String val, final TYPE type) {
    return isValid(val, type, true);
  }
}