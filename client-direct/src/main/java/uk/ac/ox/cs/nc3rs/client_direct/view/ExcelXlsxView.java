/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the names of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.compbio.client_shared.view.document.AbstractXlsxView;
import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation;
import uk.ac.ox.cs.nc3rs.client_direct.entity.SimulationInput;
import uk.ac.ox.cs.nc3rs.client_direct.value.C50_TYPE;
import uk.ac.ox.cs.nc3rs.client_direct.value.IC50_UNIT;
import uk.ac.ox.cs.nc3rs.client_direct.value.INPUT_UNIT;
import uk.ac.ox.cs.nc3rs.client_direct.value.ResultsVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationCurrentResultsVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.ResultsVO.PerReferenceDataVO;

/**
 * Excel view handling which uses XSSF rather than HSSF and thus allows more
 * than 256 columns per page.
 * 
 * https://superuser.com/questions/366468/what-is-the-maximum-allowed-rows-in-a-microsoft-excel-xls-or-xlsx
 *
 * @author geoff
 */
public class ExcelXlsxView extends AbstractXlsxView {

  private static final String headerReferenceConc = "Concentration (" + IC50_UNIT.µM + ")";
  private static final String headerReferenceConc2 = "Conc. %s ".concat(" ").concat(IC50_UNIT.µM.toString());
  private static final String headerDeltaAPD = "Δ APD90 (%)";
  private static final String headerMembraneVoltage = "Membrane Voltage (mV)";
  private static final String headerTime = "Time (ms)";
  private static final String headerPKTimepoint = "Timepoint (h)";
  private static final String headerPKSubject = "Subject %s";
  private static final String headerQNet = "qNet (C/F)";
  private static final String unitPercent = "%";

  private static final String sheetTitleInputValuesSheet = "Input Values";
  private static final String sheetTitlePctChangeQNet = "% Change and qNet";
  private static final String sheetTitleVoltageTracesCombined = "Voltage Traces (Plot format data)";
  private static final String sheetTitleVoltageTracesPerConc = "Voltage Traces (Per-concentration)";
  private static final String sheetTitlePKFile = "PK File data";
  private static final String sheetTitlePKResults = "PK Results (Per-timepoint APD90 (ms)";
  private static final String emptyCell = "";

  private static final String sheetNameError = "ExcelGenerationFailure";
  private static final String sheetNameNoData = "NoDataAvailable";
  private static final String errorProcessingMessage = "A data processing problem has occurred. Please advise the portal developer";
  private static final String noDataMessage = "No input values or results available";

  // Place all the column titles in this row
  private static final int headerRowNumber = 0;
  // Row number to start data value entries.
  private static final int dataRowNumberStartAt = headerRowNumber + 1;

  private static final Log log = LogFactory.getLog(ExcelXlsxView.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.compbio.client_shared.view.document.AbstractXlsView#buildExcelDocument(java.util.Map, org.apache.poi.ss.usermodel.Workbook, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
   */
  @Override
  protected void buildExcelDocument(final Map<String, Object> model,
                                    final Workbook workbook,
                                    final HttpServletRequest request,
                                    final HttpServletResponse response)
                                    throws Exception {

    log.debug("~buildExcelDocument() : Invoked.");

    final String errorMessage = 
          (String) model.get(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MESSAGE_ERROR);
    final SimulationCurrentResultsVO simulationContainer = 
          (SimulationCurrentResultsVO) model.get(ClientSharedIdentifiers.MODEL_ATTRIBUTE_RESULTS);
    @SuppressWarnings("unchecked")
    final Map<Short, String> modelNames =
          (Map<Short, String>) model.get(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MODEL_NAMES);
    final Long simulationId = 
          (Long) model.get(ClientSharedIdentifiers.MODEL_ATTRIBUTE_SIMULATION_ID);

    if (errorMessage != null) {
      log.debug("~buildExcelDocument() : Error '" + errorMessage + "' encountered for simulation '" + simulationId + "'.");
      createEmptyDocument(workbook, sheetNameError, errorMessage);
    } else {
      try {
        final Simulation simulation = simulationContainer.getSimulation();
        final boolean hasPKInput = Simulation.hasPKInput(simulation);
        final ResultsVO allResultsData = simulationContainer.getResults();

        if (simulation == null && allResultsData == null) {
          log.debug("~buildExcelDocument() : " + noDataMessage + ".");
          createEmptyDocument(workbook, sheetNameNoData, noDataMessage);
        } else {
          if (simulation != null) {
            loadInputValueSheet(workbook, simulation, modelNames);
          }

          if (allResultsData.hasPerReferenceData()) {
            final List<PerReferenceDataVO> sortedPerConcData = 
                                           allResultsData.retrievePerReferenceData(true);
            loadConcChangeSheet(workbook, sortedPerConcData,
                               (String) allResultsData.getResults()
                                                      .get(ClientDirectIdentifiers.KEY_RESULTS_DELTA_APD90_PERCENTILE_NAMES));
            loadVoltageTracePerConc(workbook, sortedPerConcData);
            loadVoltageTracePlot(workbook, sortedPerConcData);
            if (hasPKInput) {
              loadPKInput(workbook, simulation.getSimulationInput().getPkFile());
              @SuppressWarnings("unchecked")
              final Map<BigDecimal, Map<String, Object>> keyedOnTimepoint = 
                   (Map<BigDecimal, Map<String, Object>>) allResultsData.getResults().get(ClientDirectIdentifiers.KEY_PKRESULTS_TIMEPOINT_DATA);
              loadPKResults(workbook, keyedOnTimepoint);
            }
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
        log.error("~buildExcelDocument() : " + errorProcessingMessage + ". '" + e.getMessage() + "'");
        final int sheetCount = workbook.getNumberOfSheets();
        for (int sheetIdx = 0; sheetIdx < sheetCount; sheetIdx++) {
          try {
            workbook.removeSheetAt(sheetIdx);
          } catch (IndexOutOfBoundsException ioobe) {
            // Seems to get in a sheet count pickle if exception was generated whilst building workbook!
          }
        }
        createEmptyDocument(workbook, sheetNameError, errorProcessingMessage);
      }
    }
  }

  // Add a row providing channel values
  private void addInputValueRow(final Sheet sheet, final int rowNum, final String name,
                                final BigDecimal value, final String notes, final int colName,
                                final int colValue, final int colNotes) {
    final Row row = sheet.createRow(rowNum);
    row.createCell(colName).setCellValue(new XSSFRichTextString(name));
    final Cell cellValue = row.createCell(colValue);
    if (value != null) {
      cellValue.setCellValue(value.doubleValue());
    }
    final Cell cellNotes = row.createCell(colNotes);
    if (notes != null) {
      cellNotes.setCellValue(new XSSFRichTextString(notes));
    } else {
      cellNotes.setCellValue(new XSSFRichTextString(emptyCell));
    }
  } 

  // Create an empty document if there's no data or a problem.
  private void createEmptyDocument(final Workbook workbook, final String sheetName,
                                   final String message) {
    final Sheet sheet = workbook.createSheet(sheetName);
    final Row row = sheet.createRow(headerRowNumber);
    row.createCell(0).setCellValue(new XSSFRichTextString(message));
  }

  private void loadPKInput(final Workbook workbook, final PortalFile pkFile) {
    log.debug("~loadPKFile() : Invoked.");

    final Sheet pkFileSheet = workbook.createSheet(sheetTitlePKFile);

    // Create the header row
    int rowNum = headerRowNumber;
    String pkContent = pkFile.getContent();
    if (pkFile == null || StringUtils.isBlank(pkContent)) {
      final String warnMessage = "A null or empty PK file encountered!";
      log.warn("~loadPKFile() : ".concat(warnMessage));

      Row row = pkFileSheet.createRow(rowNum);
      row.createCell(0).setCellValue(new XSSFRichTextString(warnMessage));

      return;
    }

    final int colTimepoint = 0;

    // No header row here!

    if (pkContent.contains("\r\n")) {
      pkContent = pkContent.replaceAll("\r\n", "\n");
    }
    if (pkContent.contains("\r")) {
      pkContent = pkContent.replaceAll("\r", "\n");
    }
    final List<String> linesLF = Arrays.asList(pkContent.split("\n"));

    for (final String line : linesLF) {
      final List<String> cols = Arrays.asList(line.split("\t"));
      final int colsSize = cols.size();
      switch (colsSize) {
        case 0:
        case 1:
          continue;
        default:
          final Row row = pkFileSheet.createRow(rowNum++);
          final Double timepoint = Double.valueOf(cols.get(0));
          row.createCell(colTimepoint).setCellValue(timepoint);
          final int colsLimit = colsSize - 1;
          for (int concIdx = 1; concIdx <= colsLimit; concIdx++) {
            final Double concentration = Double.valueOf(cols.get(concIdx));
            row.createCell(concIdx).setCellValue(concentration);
          }
      }
    }
  }

  // Load PK Results data.
  private void loadPKResults(final Workbook workbook,
                             final Map<BigDecimal, Map<String, Object>> keyedOnTimepoint) {
    log.debug("~loadPKResults() : Invoked.");

    if (keyedOnTimepoint == null) {
      final String errorMessage = "PK Results expected for Excel PK output!";
      log.error("~loadPKResults() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    final Sheet pkResultsSheet = workbook.createSheet(sheetTitlePKResults);

    int rowNum = headerRowNumber;
    final int colTimepoint = 0;

    Row headerRow = pkResultsSheet.createRow(rowNum++);
    headerRow.createCell(colTimepoint, CellType.STRING)
             .setCellValue(headerPKTimepoint);

    boolean writeSubjectColHeaders = true;

    // Data is arriving in no particular timepoint order, so sort it.
    final List<BigDecimal> timepoints = new ArrayList<BigDecimal>(keyedOnTimepoint.keySet());
    Collections.sort(timepoints);
    for (final BigDecimal timepoint : timepoints) {
      final Map<String, Object> pkResults = keyedOnTimepoint.get(timepoint);

      final String apd90s = (String) pkResults.get(ClientDirectIdentifiers.KEY_PKRESULTS_PERTIMEPOINT_APD90S);

      if (timepoint != null && !StringUtils.isBlank(apd90s)) {
        final List<String> apd90Values = Arrays.asList(apd90s.split(","));
        final int subjectCount = apd90Values.size();

        // Write the header row subject column titles
        if (writeSubjectColHeaders) {
          for (int subjectIdx = 1; subjectIdx <= subjectCount; subjectIdx++) {
            headerRow.createCell(subjectIdx, CellType.STRING)
                     .setCellValue(String.format(headerPKSubject, subjectIdx));
          }
          writeSubjectColHeaders = false;
        }

        // Write normal, per-timepoint rows
        final Row row = pkResultsSheet.createRow(rowNum++);
        row.createCell(colTimepoint, CellType.NUMERIC)
           .setCellValue(timepoint.doubleValue());
        int subjectIdx = 1;
        for (final String apd90Value : apd90Values) {
          Double cellValue = null;
          try {
            cellValue = Double.valueOf(apd90Value);
          } catch (Exception e) {
            log.warn("~loadPKResults() : Non-double value '" + apd90Value + "' omitted from Excel.");
          }
          final Cell cell = row.createCell(subjectIdx++, CellType.NUMERIC);
          if (cellValue != null) {
            cell.setCellValue(cellValue);
          }
        }
      }
    }
  }

  // Load Input Value data into workbook sheet
  private void loadInputValueSheet(final Workbook workbook,
                                   final Simulation simulation,
                                   final Map<Short, String> modelNames) {
    log.debug("~loadInputValueSheet() : Invoked.");

    final SimulationInput simulationInput = simulation.getSimulationInput();

    final Short modelIndentifier = simulationInput.getModelIdentifier();
    final BigDecimal pacingFrequency =  simulationInput.getPacingFrequency();
    final BigDecimal pacingMaxTime = simulationInput.getPacingMaxTime();
    final C50_TYPE c50Type = simulationInput.getC50Type();
    final IC50_UNIT ic50Units = simulationInput.getIc50Units();
    final BigDecimal c50ICaL = simulationInput.getC50ICaL();
    final BigDecimal hillICaL = simulationInput.getHillICaL();
    final BigDecimal saturationICaL = simulationInput.getSaturationICaL();
    final BigDecimal c50IK1 = simulationInput.getC50Ik1();
    final BigDecimal hillIK1 = simulationInput.getHillIk1();
    final BigDecimal saturationIK1 = simulationInput.getSaturationIk1();
    final BigDecimal c50IKr = simulationInput.getC50Ikr();
    final BigDecimal hillIKr = simulationInput.getHillIkr();
    final BigDecimal saturationIKr = simulationInput.getSaturationIkr();
    final BigDecimal c50IKs = simulationInput.getC50Iks();
    final BigDecimal hillIKs = simulationInput.getHillIks();
    final BigDecimal saturationIKs = simulationInput.getSaturationIks();
    final BigDecimal c50INa = simulationInput.getC50INa();
    final BigDecimal hillINa = simulationInput.getHillINa();
    final BigDecimal saturationINa = simulationInput.getSaturationINa();
    final BigDecimal c50INaL = simulationInput.getC50INaL();
    final BigDecimal hillINaL = simulationInput.getHillINaL();
    final BigDecimal saturationINaL = simulationInput.getSaturationINaL();
    final BigDecimal c50Ito = simulationInput.getC50Ito();
    final BigDecimal hillIto = simulationInput.getHillIto();
    final BigDecimal saturationIto = simulationInput.getSaturationIto();
    final BigDecimal plasmaConcMin = simulationInput.getPlasmaConcMin();
    final BigDecimal plasmaConcMax = simulationInput.getPlasmaConcMax();
    final Short plasmaIntPtCount = simulationInput.getPlasmaIntPtCount();
    final boolean plasmaIntPtLogScale = simulationInput.isPlasmaIntPtLogScale();
    //final PortalFile portalFile = simulationInput.getPkFile();
    final String plasmaConcPoints = simulationInput.getPlasmaConcPoints();
    final String cellMLFileName = simulationInput.getCellMLFileName();
    final String notes = simulationInput.getNotes();

    final Sheet assayInputValuesSheet = workbook.createSheet(sheetTitleInputValuesSheet);

    // Create the header row
    int rowNum = headerRowNumber;
    final int colName = 0;
    final int colValue = 1;
    final int colNotes = 2;

    Row row = assayInputValuesSheet.createRow(rowNum++);
    row.createCell(colName)
       .setCellValue(new XSSFRichTextString(SimulationInputVO.IVN_MODEL_IDENTIFIER));
    final Cell modelCell = row.createCell(colValue);
    if (cellMLFileName != null) {
      modelCell.setCellValue(new XSSFRichTextString(cellMLFileName));
    } else {
      modelCell.setCellValue(new Double(modelIndentifier));
      row.createCell(colNotes)
         .setCellValue(new XSSFRichTextString(modelNames.get(Short.valueOf(modelIndentifier))));
    }

    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_PACING_FREQUENCY, pacingFrequency,
                     INPUT_UNIT.Hz.toString(), colName, colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_MAXIMUM_PACING_TIME, pacingMaxTime,
                     INPUT_UNIT.minutes.toString(), colName, colValue, colNotes);

    row = assayInputValuesSheet.createRow(rowNum++);
    row.createCell(colName)
       .setCellValue(new XSSFRichTextString(SimulationInputVO.IVN_C50_TYPE));
    if (c50Type != null) {
      row.createCell(colValue)
         .setCellValue(new XSSFRichTextString(c50Type.toString()));
      if (c50Type.compareTo(C50_TYPE.pIC50) == 0) {
        row.createCell(colNotes)
           .setCellValue(new XSSFRichTextString(SimulationInputVO.UNIT_MINUS_LOG_M));
      } else {
        if (ic50Units != null) {
          row.createCell(colNotes)
             .setCellValue(new XSSFRichTextString(ic50Units.toString()));
        } else {
          row.createCell(colNotes)
             .setCellValue(new XSSFRichTextString(emptyCell));
        }
      }
    } else {
      row.createCell(colValue);
      row.createCell(colNotes).setCellValue(new XSSFRichTextString(emptyCell));
    }

    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_IKr_C50, c50IKr, null, colName,
                     colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_IKr_Hill, hillIKr, null, colName,
                     colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_IKr_Saturation, saturationIKr,
                     unitPercent, colName, colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_INa_C50, c50INa, null, colName,
                     colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_INa_Hill, hillINa, null, colName,
                     colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_INa_Saturation, saturationINa,
                     unitPercent, colName, colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_ICaL_C50, c50ICaL, null, colName,
                     colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_ICaL_Hill, hillICaL, null, colName,
                     colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_ICaL_Saturation, saturationICaL,
                     unitPercent, colName, colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_IKs_C50, c50IKs, null, colName,
                     colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_IKs_Hill, hillIKs, null, colName,
                     colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_IKs_Saturation, saturationIKs,
                     unitPercent, colName, colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_IK1_C50, c50IK1, null,
                     colName, colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_IK1_Hill, hillIK1, null, colName,
                     colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_IK1_Saturation, saturationIK1,
                     unitPercent, colName, colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_Ito_C50, c50Ito, null, colName,
                     colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_Ito_Hill, hillIto, null, colName,
                     colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_Ito_Saturation, saturationIto,
                     unitPercent, colName, colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_INaL_C50, c50INaL, null, colName,
                     colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_INaL_Hill, hillINaL, null, colName,
                     colValue, colNotes);
    addInputValueRow(assayInputValuesSheet, rowNum++,
                     SimulationInputVO.IVN_INaL_Saturation, saturationINaL,
                     unitPercent, colName, colValue, colNotes);

    switch (new SimulationInputVO(simulationInput).retrieveInputOption()) {
      case CONC_POINTS :
        row = assayInputValuesSheet.createRow(rowNum++);
        row.createCell(colName)
           .setCellValue(new XSSFRichTextString(SimulationInputVO.IVN_CONC_POINTS));
        row.createCell(colValue)
           .setCellValue(new XSSFRichTextString(StringUtils.replace(plasmaConcPoints, " ", ", ")));
        row.createCell(colNotes)
           .setCellValue(new XSSFRichTextString(IC50_UNIT.µM.toString()));

        break;
      case CONC_RANGE :
        addInputValueRow(assayInputValuesSheet, rowNum++,
                         SimulationInputVO.IVN_CONC_MIN, plasmaConcMin,
                         IC50_UNIT.µM.toString(), colName, colValue, colNotes);
        addInputValueRow(assayInputValuesSheet, rowNum++,
                         SimulationInputVO.IVN_CONC_MAX, plasmaConcMax,
                         IC50_UNIT.µM.toString(), colName, colValue, colNotes);

        row = assayInputValuesSheet.createRow(rowNum++);
        row.createCell(colName)
           .setCellValue(new XSSFRichTextString(SimulationInputVO.IVN_CONC_INT_PT_CNT));
        if (plasmaIntPtCount != null) {
          row.createCell(colValue).setCellValue(new Double(plasmaIntPtCount));
        } else {
          row.createCell(colValue);
        }
        row.createCell(colNotes).setCellValue(new XSSFRichTextString(emptyCell));

        // Compound concentration intermediate points log scale
        row = assayInputValuesSheet.createRow(rowNum++);
        row.createCell(colName)
           .setCellValue(new XSSFRichTextString(SimulationInputVO.IVN_CONC_INT_PT_LOGSCALE));
        row.createCell(colValue)
           .setCellValue(new XSSFRichTextString(plasmaIntPtLogScale ? "true" : "false"));
        row.createCell(colNotes)
           .setCellValue(new XSSFRichTextString(emptyCell));

        break;
      case PK :
        row = assayInputValuesSheet.createRow(rowNum++);
        row.createCell(colName)
           .setCellValue(new XSSFRichTextString(SimulationInputVO.IVN_PK_FILENAME));
        row.createCell(colValue)
           .setCellValue(new XSSFRichTextString(simulationInput.getPkFile()
                                                               .getName()));
        row.createCell(colNotes)
           .setCellValue(new XSSFRichTextString(""));

        break;
    }

    // Notes
    row = assayInputValuesSheet.createRow(rowNum++);
    row.createCell(colName)
       .setCellValue(new XSSFRichTextString(SimulationInputVO.IVN_NOTES));
    if (notes != null) {
      row.createCell(colValue).setCellValue(new XSSFRichTextString(notes));
    } else {
      row.createCell(colValue);
    }
    row.createCell(colNotes, CellType.STRING)
       .setCellValue(new XSSFRichTextString(emptyCell));
  }

  // Load Result's Pct Change or PK data into workbook sheet.
  private void loadConcChangeSheet(final Workbook workbook,
                                   final List<PerReferenceDataVO> sortedPerConcData,
                                   final String deltaAPD90PctileNames) {
    log.debug("~loadConcChangeSheet() : Invoked.");

    final int concColNum = 0;
    // e.g. Delta APD 90 or APD 90
    final int variantColNum = 1;
    final int qNetColNum = 2;

    final Sheet concChangeSheet = workbook.createSheet(sheetTitlePctChangeQNet);
    final Row concChangeHeaderRow = concChangeSheet.createRow(headerRowNumber);
    concChangeHeaderRow.createCell(concColNum)
                       .setCellValue(new XSSFRichTextString(headerReferenceConc));
    concChangeHeaderRow.createCell(variantColNum)
                       .setCellValue(new XSSFRichTextString(headerDeltaAPD));
    concChangeHeaderRow.createCell(qNetColNum)
                       .setCellValue(new XSSFRichTextString(headerQNet));

    /* Place any Delta APD90 %ile name(s) between the column title and any %ile
       values. */
    int concChangeRowNum = headerRowNumber + 1;
    if (!StringUtils.isBlank(deltaAPD90PctileNames)) {
      concChangeSheet.createRow(concChangeRowNum++)
                     .createCell(variantColNum)
                     .setCellValue(deltaAPD90PctileNames);
    }

    for (final PerReferenceDataVO perConcData : sortedPerConcData) {
      final BigDecimal concentration = perConcData.getReference();
      final Row perConcentrationRow = concChangeSheet.createRow(concChangeRowNum++);
      perConcentrationRow.createCell(concColNum).setCellValue(concentration.doubleValue());

      final String deltaAPD90 = perConcData.retrieveDeltaAPD90();
      final String qNet = perConcData.retrieveQNet();

      setCellBasedOnContent(perConcentrationRow.createCell(variantColNum),
                            deltaAPD90);
      setCellBasedOnContent(perConcentrationRow.createCell(qNetColNum), qNet);
    }
  }

  /*
   * Set cell type based on whether it's a number or string.
   * {@code null} is set to a 'n/a' string.
   */
  private void setCellBasedOnContent(final Cell cell, final String content) {
    if (content == null) {
      cell.setCellValue(new XSSFRichTextString("n/a"));
    } else {
      try {
        final Double numericVariant = Double.valueOf(content);
        cell.setCellValue(numericVariant);
      } catch (NumberFormatException e) {
        cell.setCellValue(new XSSFRichTextString(content));
      }
    }
  }

  // Load plottable voltage trace data into workbook sheet.
  private void loadVoltageTracePlot(final Workbook workbook,
                                    final List<PerReferenceDataVO> sortedPerConcData) {
    log.debug("~loadVoltageTracePlot() : Invoked.");
    final Sheet voltageTraceSheetCombined = workbook.createSheet(sheetTitleVoltageTracesCombined);
    final Row voltageTraceCombinedHeaderRow = voltageTraceSheetCombined.createRow(headerRowNumber);

    // First sheet column is for times
    int combinedColNum = 0;
    voltageTraceCombinedHeaderRow.createCell(combinedColNum++)
                                 .setCellValue(new XSSFRichTextString(headerTime));
    for (final PerReferenceDataVO perConcData : sortedPerConcData) {
      final BigDecimal concentration = perConcData.getReference();
      // Creating a concentration-specific column in the sheet
      final String concentrationTitle = String.format(headerReferenceConc2,
                                                  concentration.toPlainString());
      voltageTraceCombinedHeaderRow.createCell(combinedColNum++)
                                   .setCellValue(new XSSFRichTextString(concentrationTitle));
    }

    // Load up the combined data in a big (Tree)Map (sorted on time).
    //   Structure: <time, <concentration, voltage>>
    final Map<Double, Map<BigDecimal, BigDecimal>> combined = new TreeMap<Double, Map<BigDecimal, BigDecimal>>();

    for (final PerReferenceDataVO perConcData : sortedPerConcData) {
      final BigDecimal concentration = perConcData.getReference();
      final String csvTimes = perConcData.retrieveVoltageTraceTimes();
      final String csvVoltages = perConcData.retrieveVoltageTraceVoltages();

      if (csvTimes != null && csvVoltages != null) {
        final String[] times = csvTimes.split(",");
        final String[] voltages = csvVoltages.split(",");

        for (int idx = 0; idx < times.length; idx++) {
          final Double time = Double.valueOf(times[idx]);
          final BigDecimal voltage = new BigDecimal(voltages[idx]);

          if (combined.containsKey(time)) {
            final Map<BigDecimal, BigDecimal> existingTimeOutput = combined.get(time);
            existingTimeOutput.put(concentration, voltage);
          } else {
            // HashMap because we're "get"ting using sorted unique concentrations.
            final Map<BigDecimal, BigDecimal> newTimeCombined = new HashMap<BigDecimal, BigDecimal>();
            newTimeCombined.put(concentration, voltage);
            combined.put(time, newTimeCombined);
          }
        }
      }
    }

    // Populate the corresponding Excel sheet. 
    if (!combined.isEmpty()) {
      int combinedRowNum = dataRowNumberStartAt;
      for (final Map.Entry<Double, Map<BigDecimal, BigDecimal>> eachTimeData : combined.entrySet()) {
        final Double time = eachTimeData.getKey();
        final Map<BigDecimal, BigDecimal> eachTimeValues = eachTimeData.getValue();

        combinedColNum = 0;
        final Row voltageTraceRow = voltageTraceSheetCombined.createRow(combinedRowNum++);
        voltageTraceRow.createCell(combinedColNum).setCellValue(time);

        for (final PerReferenceDataVO perConcData : sortedPerConcData) {
          final BigDecimal concentration = perConcData.getReference();

          combinedColNum++;
          if (eachTimeValues.containsKey(concentration)) {
            voltageTraceRow.createCell(combinedColNum)
                           .setCellValue(eachTimeValues.get(concentration).doubleValue());
          }
        }
      }
    }
  }

  // Load per-concentration voltage trace data into workbook sheet.
  private void loadVoltageTracePerConc(final Workbook workbook,
                                            final List<PerReferenceDataVO> sortedPerConcData) {
    log.debug("~loadVoltageTracePerConc() : Invoked.");
    final Sheet voltageTraceSheetPerRef = workbook.createSheet(sheetTitleVoltageTracesPerConc);
    final Row voltageTracePerConcHeaderRow1 = voltageTraceSheetPerRef.createRow(headerRowNumber);
    final Row voltageTracePerConcHeaderRow2 = voltageTraceSheetPerRef.createRow(headerRowNumber + 1);
    final int headerRowCount = 2;

    final CellStyle mergedCellStyle = workbook.createCellStyle();
    //mergedCellStyle.setAlignment(CellStyle.ALIGN_CENTER);

    // Time column number incremented by amount below for each per-concentration data.
    // Currently time vs. voltage then a separating empty column.
    final int nextTimeColNumIncrement = 3;
    int colNumTime = 0;
    for (final PerReferenceDataVO perConcData : sortedPerConcData) {
      int colNumVoltage = colNumTime + 1;

      final BigDecimal concentration = perConcData.getReference();

      // Creating a concentration-specific pair of columns in the sheet
      final String concentrationTitle = String.format(headerReferenceConc2,
                                                      concentration.toPlainString());

      // On the first header row the concentration in merged columns
      final Cell mergedRefCell = voltageTracePerConcHeaderRow1.createCell(colNumTime);
      mergedRefCell.setCellValue(new XSSFRichTextString(concentrationTitle));
      mergedRefCell.setCellStyle(mergedCellStyle);

      voltageTraceSheetPerRef.addMergedRegion(new CellRangeAddress(0, 0,
                                                                   colNumTime,
                                                                   colNumVoltage));
      // On the second header row the time and membrane voltage
      voltageTracePerConcHeaderRow2.createCell(colNumTime, CellType.STRING)
                                   .setCellValue(new XSSFRichTextString(headerTime));
      voltageTracePerConcHeaderRow2.createCell(colNumVoltage, CellType.STRING)
                                   .setCellValue(new XSSFRichTextString(headerMembraneVoltage));

      final String csvTimes = perConcData.retrieveVoltageTraceTimes();
      final String csvVoltages = perConcData.retrieveVoltageTraceVoltages();

      if (csvTimes != null && csvVoltages != null) {
        final String[] times = csvTimes.split(",");
        final String[] voltages = csvVoltages.split(",");

        // Initialise to be first row beneath the headers.
        int perRefRowNum = headerRowNumber + headerRowCount;
        for (int idx = 0; idx < times.length; idx++) {
          final Double time = Double.valueOf(times[idx]);
          final Double voltage = Double.valueOf(voltages[idx]);

          Row traceRow = voltageTraceSheetPerRef.getRow(perRefRowNum);
          if (traceRow == null) {
            traceRow = voltageTraceSheetPerRef.createRow(perRefRowNum);
          }

          traceRow.createCell(colNumTime, CellType.NUMERIC)
                  .setCellValue(time);
          traceRow.createCell(colNumVoltage, CellType.NUMERIC)
                  .setCellValue(voltage);

          perRefRowNum++;
        }
      }

      colNumTime += nextTimeColNumIncrement;
    }
  }
}