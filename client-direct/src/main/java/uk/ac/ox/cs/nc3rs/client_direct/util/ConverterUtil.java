/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.util;

import java.math.BigDecimal;

import uk.ac.ox.cs.nc3rs.client_direct.value.IC50_UNIT;

/**
 * General utilities for converting things.
 *
 * @author geoff
 */
public final class ConverterUtil {
  /** One million */
  private static final BigDecimal million = new BigDecimal("1000000");
  private static final BigDecimal minus1 = new BigDecimal("-1");
  private static final BigDecimal billion = new BigDecimal("1000000000");

  // Hidden constructor
  private ConverterUtil() {}

  /**
   * Convert an IC50 to a pIC50 value.
   * 
   * @param ic50Unit IC50 units.
   * @param ic50 IC50 value.
   * @return IllegalArgumentException If an IC50 of 0 is passed.
   */
  public static BigDecimal ic50ToPIC50(final IC50_UNIT ic50Unit, final BigDecimal ic50) {
    if (ic50 == null) {
      return ic50;
    }
    if (BigDecimal.ZERO.compareTo(ic50) == 0) {
      throw new IllegalArgumentException("Cannot convert a 0 IC50 value");
    }

    BigDecimal c50AsMolar = null;
    switch (ic50Unit) {
      case M :
        c50AsMolar = ic50;
        break;
      case µM :
        c50AsMolar = ic50.divide(million);
        break;
      case nM :
        c50AsMolar = ic50.divide(billion);
        break;
    }

    final Double multiple = Math.log10(c50AsMolar.doubleValue());
    final BigDecimal pIC50 = minus1.multiply(new BigDecimal(multiple));
    return pIC50;
  }
}