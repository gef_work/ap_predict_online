/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import uk.ac.ox.cs.nc3rs.client_direct.ClientDirectIdentifiers;
import uk.ac.ox.cs.nc3rs.client_direct.dao.SimulationDAO;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Job;
import uk.ac.ox.cs.nc3rs.client_direct.entity.Simulation;

/**
 * Implementation of the Simulation Data Access Object interface.
 * <p>
 * Queries should use named parameters ensuring that dangerous characters should be automatically
 * escaped JDBC driver.
 * </p> 
 * 
 * @author Geoff Williams
 */
@Repository(ClientDirectIdentifiers.COMPONENT_SIMULATION_DAO)
public class SimulationDAOImpl implements SimulationDAO {

  @PersistenceContext
  private EntityManager entityManager;

  private static final Log log = LogFactory.getLog(SimulationDAOImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.dao.SimulationDAO#avoidJobLazyInit(uk.ac.ox.cs.nc3rs.client_direct.entity.Job)
   */
  @Override
  public void avoidJobLazyInit(final Job job) {
    log.debug("~avoidJobLazyInit() : Invoked.");
    if (job != null) {
      if (job.hasResults()) {
        job.getJobResults().size();
      }
      if (job.hasPKResults()) {
        job.getJobPKResults().size();
      }
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.dao.SimulationDAO#deleteSimulation(long)
   */
  @Override
  public void deleteSimulation(final long simulationId) {
    log.debug("~deleteSimulation() : [" + simulationId + "] : Invoked.");

    final Simulation simulation = findBySimulationId(simulationId);
    if (simulation != null && (simulation.isCompleted() || 
                               simulation.isTakingTooLongToComplete())) {
      final Job job = simulation.getJob();
      avoidJobLazyInit(job);
      entityManager.remove(simulation);
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.dao.SimulationDAO#findBySimulationId(long)
   */
  @Override
  public Simulation findBySimulationId(final long simulationId) {
    log.debug("~findBySimulationId() : [" + simulationId + "] : Invoked.");

    return entityManager.find(Simulation.class, simulationId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.dao.SimulationDAO#findByUser(java.lang.String)
   */
  @Override
  public List<Simulation> findByUser(final String user) {
    log.debug("~findByUser() : [?] : Invoked for user '" + user + "'.");

    final Query query = entityManager.createNamedQuery(Simulation.QUERY_SIMULATION_BY_CREATOR_ORDERED);
    query.setParameter(Simulation.PROPERTY_CREATOR, user);

    @SuppressWarnings("unchecked")
    final List<Simulation> simulations = query.getResultList();

    log.debug("~findByUser() : '" + simulations.size() + "' simulations found for '" + user + "'.");

    return simulations;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.dao.SimulationDAO#findSimulations()
   */
  @Override
  public List<Simulation> findSimulations() {
    log.debug("~findSimulations() : [?] : Invoked.");

    final Query query = entityManager.createNamedQuery(Simulation.QUERY_SIMULATION_ALL);
    @SuppressWarnings("unchecked")
    final List<Simulation> simulations = query.getResultList();

    log.debug("~findSimulations() : '" + simulations.size() + "' retrieved.");

    return simulations;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.dao.SimulationDAO#store(uk.ac.ox.cs.nc3rs.business_manager.entity.Simulation)
   */
  @Override
  public Simulation store(final Simulation simulation) {
    final Long simulationId = simulation.getId();
    log.debug("~store() : [" + simulationId + "] : Invoked.");

    if (simulationId != null) {
      return entityManager.merge(simulation);
    } else {
      entityManager.persist(simulation);
      return simulation;
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.client_direct.dao.SimulationDAO#storeResults(uk.ac.ox.cs.nc3rs.client_direct.entity.Job)
   */
  @Override
  public Job storeResults(Job job) {
    log.debug("~storeResults() : [?] : Invoked for job with app-manager id '" + job.getAppManagerId() + "'.");

    job = entityManager.merge(job);
    avoidJobLazyInit(job);

    return job;
  }
}