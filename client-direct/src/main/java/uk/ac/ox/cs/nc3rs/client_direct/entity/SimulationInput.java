/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2019, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the names of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.nc3rs.client_direct.entity;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.TableGenerator;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.annotate.JsonIgnore;

import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.nc3rs.client_direct.value.C50_TYPE;
import uk.ac.ox.cs.nc3rs.client_direct.value.MeasurementObservationsVO;
import uk.ac.ox.cs.nc3rs.client_direct.value.IC50_UNIT;
import uk.ac.ox.cs.nc3rs.client_direct.value.SimulationInputVO;

/**
 * Simulation input persistence object.
 *
 * @author geoff
 */
@Entity
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy.READ_ONLY,
                                 region = "SimulationInputCache")
public class SimulationInput {

  // Persistence identity (Surrogate primary key)
  @JsonIgnore
  @TableGenerator(name="simulation_input_id_gen", table="sequence_pks_client_direct",
                  pkColumnName="pk_seq_name", pkColumnValue="simulation_input_id",
                  valueColumnName="pk_seq_value", allocationSize=1)
  @Id @GeneratedValue(strategy=GenerationType.TABLE, generator="simulation_input_id_gen")
  private Long id;

  @Column(insertable=true, nullable=true, updatable=false)
  private String c50ICaL;

  @Column(insertable=true, nullable=true, updatable=false)
  private String hillICaL;

  @Column(insertable=true, nullable=true, updatable=false)
  private String saturationICaL;

  @Column(insertable=true, nullable=true, updatable=false)
  private String spreadICaL;

  @Column(insertable=true, nullable=true, updatable=false)
  private String c50Ikr;

  @Column(insertable=true, nullable=true, updatable=false)
  private String hillIkr;

  @Column(insertable=true, nullable=true, updatable=false)
  private String saturationIkr;

  @Column(insertable=true, nullable=true, updatable=false)
  private String spreadIkr;

  @Column(insertable=true, nullable=true, updatable=false)
  private String c50Ik1;

  @Column(insertable=true, nullable=true, updatable=false)
  private String hillIk1;

  @Column(insertable=true, nullable=true, updatable=false)
  private String saturationIk1;

  @Column(insertable=true, nullable=true, updatable=false)
  private String spreadIk1;

  @Column(insertable=true, nullable=true, updatable=false)
  private String c50Iks;

  @Column(insertable=true, nullable=true, updatable=false)
  private String hillIks;

  @Column(insertable=true, nullable=true, updatable=false)
  private String saturationIks;

  @Column(insertable=true, nullable=true, updatable=false)
  private String spreadIks;

  @Column(insertable=true, nullable=true, updatable=false)
  private String c50Ito;

  @Column(insertable=true, nullable=true, updatable=false)
  private String hillIto;

  @Column(insertable=true, nullable=true, updatable=false)
  private String saturationIto;

  @Column(insertable=true, nullable=true, updatable=false)
  private String spreadIto;

  @Column(insertable=true, nullable=true, updatable=false)
  private String c50INa;

  @Column(insertable=true, nullable=true, updatable=false)
  private String hillINa;

  @Column(insertable=true, nullable=true, updatable=false)
  private String saturationINa;

  @Column(insertable=true, nullable=true, updatable=false)
  private String spreadINa;

  @Column(insertable=true, nullable=true, updatable=false)
  private String c50INaL;

  @Column(insertable=true, nullable=true, updatable=false)
  private String hillINaL;

  @Column(insertable=true, nullable=true, updatable=false)
  private String saturationINaL;

  @Column(insertable=true, nullable=true, updatable=false)
  private String spreadINaL;

  @Column(insertable=true, nullable=true, updatable=false)
  private C50_TYPE c50Type;

  @Column(insertable=true, nullable=true, updatable=false)
  private IC50_UNIT ic50Units;

  @Column(insertable=true, nullable=true, updatable=false)
  private Short modelIdentifier;

  @Column(insertable=true, nullable=false, updatable=false)
  private String pacingMaxTime;

  @Column(insertable=true, nullable=true, updatable=false)
  private String pacingFrequency;

  @Column(insertable=true, nullable=true, updatable=false)
  private String plasmaConcMax;

  @Column(insertable=true, nullable=true, updatable=false)
  private String plasmaConcMin;

  @Column(insertable=true, nullable=true, updatable=false)
  private Short plasmaIntPtCount;

  @Column(insertable=true, nullable=false, updatable=false)
  private boolean plasmaIntPtLogScale;

  // Individual concentration value points are separated by spaces.
  @Column(insertable=true, nullable=true, updatable=false)
  private String plasmaConcPoints;

  // CellML file name
  @Column(insertable=true, nullable=true, updatable=false)
  private String cellMLFileName;

  // Use this to retain lazy loading of PK files (e.g. avoid eager loading for main display)
  @Column(insertable=true, nullable=true, updatable=false)
  private String pkFileName;

  /* Unidirectional SimulationInput [1] --> [1] PortalFile (PK file, which is 
     persisted independently) */
  @OneToOne(cascade= { CascadeType.REMOVE },
            fetch=FetchType.LAZY )
  // EAGER fetching causes delays to presentation of dataTable
  @JoinColumn(name="portal_file_id",
              nullable=true,
              updatable=false,
              unique=true,
              foreignKey = @ForeignKey(name="fk_id_portal_file"))
  /* Transferred JsonIgnore here to avoid JsonMappingException now that LAZY 
     loading used which supplies HibernateProxy objects to UI. */
  @JsonIgnore
  private PortalFile pkFile;

  // Delta APD90 credible interval percentiles.
  @Column(insertable=true, nullable=true, updatable=false)
  private String credibleIntervalPctiles;

  @Column(insertable=true, nullable=true, updatable=false,
          length=SimulationInputVO.MAX_NOTES_LENGTH)
  private String notes;

  /** <b>Do not invoke directly.</b> */
  protected SimulationInput() {};

  /**
   * Initialising constructor.
   * <p>
   * If at least one C50 value has been assigned then the C50 Type must be specified.<br>
   * In situations where the C50 value is not assigned for a channel the corresponding Hill and
   * Saturation values will be automatically nullified - even if the arguments are passed as a
   * value.
   * </p>
   * 
   * @param iCaLC50 Calcium (p)I/XC50 value.
   * @param iCaLHill Calcium Hill Coefficient value.
   * @param iCaLSaturation Calcium Saturation Level value.
   * @param iCaLSpread Calcium spread.
   * @param iKrC50 IKr (p)I/XC50 value.
   * @param iKrHill IKr Hill Coefficient value.
   * @param iKrSaturation IKr Saturation Level value.
   * @param iKrSpread IKr spread.
   * @param iK1C50 Ik1 (p)I/XC50 value.
   * @param iK1Hill Ik1 Hill Coefficient value.
   * @param iK1Saturation Ik1 Saturation Level value.
   * @param iK1Spread Ik1 spread.
   * @param iKsC50 Iks (p)I/XC50 value.
   * @param iKsHill Iks Hill Coefficient value.
   * @param iKsSaturation Iks Saturation Level value.
   * @param iKsSpread Iks spread.
   * @param itoC50 Ito (p)I/XC50 value.
   * @param itoHill Ito Hill Coefficient value.
   * @param itoSaturation Ito Saturation Level value.
   * @param itoSpread Ito spread.
   * @param iNaC50 Sodium (p)I/XC50 value.
   * @param iNaHill Sodium Hill Coefficient value.
   * @param iNaSaturation Sodium Saturation Level value.
   * @param iNaSpread Sodium spread.
   * @param iNaLC50 Late/Persistent sodium (p)I/XC50 value.
   * @param iNaLHill Late/Persistent sodium Hill Coefficient value.
   * @param iNaLSaturation Late/Persistent sodium Saturation Level value.
   * @param iNaLSpread Late/Persistent sodium spread.
   * @param c50Type IC50 or pIC50 type indicator.
   * @param ic50Units IC50 units (if IC50 type). 
   * @param modelIdentifier Model identifier.
   * @param pacingMaxTime Maximum pacing time (in minutes).
   * @param pacingFrequency Pacing frequency (in Hz).
   * @param plasmaConcMax Maximum compound concentration (in uM).
   * @param plasmaConcMin Minimum compound concentration (in uM).
   * @param plasmaIntPtCount Concentration intermediate point count.
   * @param plasmaIntPtLogScale Concentration intermediate point log scale use.
   * @param plasmaConcPoints Individual plasma concentration points (values 
   *                         separated by spaces).
   * @param pkFile PK file.
   * @param cellMLFileName CellML file name.
   * @param credibleIntervalPctiles Delta APD90 credible interval percentiles.
   * @param notes Simulation notes.
   */
  public SimulationInput(final BigDecimal iCaLC50, final BigDecimal iCaLHill,
                         final BigDecimal iCaLSaturation,
                         final BigDecimal iCaLSpread,
                         final BigDecimal iK1C50, final BigDecimal iK1Hill,
                         final BigDecimal iK1Saturation,
                         final BigDecimal iK1Spread,
                         final BigDecimal iKrC50, final BigDecimal iKrHill,
                         final BigDecimal iKrSaturation,
                         final BigDecimal iKrSpread,
                         final BigDecimal iKsC50, final BigDecimal iKsHill,
                         final BigDecimal iKsSaturation,
                         final BigDecimal iKsSpread,
                         final BigDecimal iNaC50, final BigDecimal iNaHill,
                         final BigDecimal iNaSaturation,
                         final BigDecimal iNaSpread,
                         final BigDecimal iNaLC50, final BigDecimal iNaLHill,
                         final BigDecimal iNaLSaturation,
                         final BigDecimal iNaLSpread,
                         final BigDecimal itoC50, final BigDecimal itoHill,
                         final BigDecimal itoSaturation,
                         final BigDecimal itoSpread,
                         final C50_TYPE c50Type, final IC50_UNIT ic50Units,
                         final Short modelIdentifier,
                         final BigDecimal pacingMaxTime,
                         final BigDecimal pacingFrequency,
                         final BigDecimal plasmaConcMax,
                         final BigDecimal plasmaConcMin,
                         final Short plasmaIntPtCount,
                         final boolean plasmaIntPtLogScale,
                         final String plasmaConcPoints, final PortalFile pkFile,
                         final String cellMLFileName,
                         final String credibleIntervalPctiles,
                         final String notes) {
    this.c50ICaL = asString(iCaLC50);
    if (this.c50ICaL != null) {
      this.hillICaL = asString(iCaLHill);
      this.saturationICaL = asString(iCaLSaturation);
      this.spreadICaL = asString(iCaLSpread);
    }
    this.c50Ikr = asString(iKrC50);
    if (this.c50Ikr != null) {
      this.hillIkr = asString(iKrHill);
      this.saturationIkr = asString(iKrSaturation);
      this.spreadIkr = asString(iKrSpread);
    }
    this.c50Ik1 = asString(iK1C50);
    if (this.c50Ik1 != null) {
      this.hillIk1 = asString(iK1Hill);
      this.saturationIk1 = asString(iK1Saturation);
      this.spreadIk1 = asString(iK1Spread);
    }
    this.c50Iks = asString(iKsC50);
    if (this.c50Iks != null) {
      this.hillIks = asString(iKsHill);
      this.saturationIks = asString(iKsSaturation);
      this.spreadIks = asString(iKsSpread);
    }
    this.c50Ito = asString(itoC50);
    if (this.c50Ito != null) {
      this.hillIto = asString(itoHill);
      this.saturationIto = asString(itoSaturation);
      this.spreadIto = asString(itoSpread);
    }
    this.c50INa = asString(iNaC50);
    if (this.c50INa != null) {
      this.hillINa = asString(iNaHill);
      this.saturationINa = asString(iNaSaturation);
      this.spreadINa = asString(iNaSpread);
    }
    this.c50INaL = asString(iNaLC50);
    if (this.c50INaL != null) {
      this.hillINaL = asString(iNaLHill);
      this.saturationINaL = asString(iNaLSaturation);
      this.spreadINaL = asString(iNaLSpread);
    }

    this.c50Type = c50Type;
    this.ic50Units = ic50Units;

    this.modelIdentifier = modelIdentifier;
    this.pacingMaxTime = asString(pacingMaxTime);
    this.pacingFrequency = asString(pacingFrequency);
    this.plasmaConcMax = asString(plasmaConcMax);
    this.plasmaConcMin = asString(plasmaConcMin);
    this.plasmaIntPtCount = plasmaIntPtCount;
    this.plasmaIntPtLogScale = plasmaIntPtLogScale;

    this.plasmaConcPoints = StringUtils.isBlank(plasmaConcPoints) ? null : plasmaConcPoints;

    this.pkFile = pkFile;
    this.pkFileName = pkFile == null ? null : pkFile.getName();
    this.cellMLFileName = cellMLFileName;
    this.credibleIntervalPctiles = credibleIntervalPctiles;
    this.notes = notes;
  }

  private BigDecimal asBigDecimal(final String value) {
    return value == null ? null : new BigDecimal(value);
  }
  private String asString(final BigDecimal value) {
    return value == null ? null : value.toPlainString();
  }

  /**
   * Determine if there are any non-C50 values for ICaL.
   * 
   * @return {@code true} if non-C50 values (e.g. Hill, saturation, spread)
   *         specified and non-default, otherwise {@code false}.
   */
  // Used currently data table display!
  public boolean retrieveNonC50ICaL() {
    return (MeasurementObservationsVO.hasNonC50Value(hillICaL, saturationICaL,
                                                     spreadICaL));
  }

  /**
   * Determine if there are any non-C50 values for IK1.
   * 
   * @return {@code true} if non-C50 values (e.g. Hill, saturation, spread)
   *         specified and non-default, otherwise {@code false}.
   */
  // Used currently data table display!
  public boolean retrieveNonC50IK1() {
    return (MeasurementObservationsVO.hasNonC50Value(hillIk1, saturationIk1,
                                                     spreadIk1));
  }

  /**
   * Determine if there are any non-C50 values for IKr.
   * 
   * @return {@code true} if non-C50 values (e.g. Hill, saturation, spread)
   *         specified and non-default, otherwise {@code false}.
   */
  // Used currently data table display!
  public boolean retrieveNonC50IKr() {
    return (MeasurementObservationsVO.hasNonC50Value(hillIkr, saturationIkr,
                                                     spreadIkr));
  }

  /**
   * Determine if there are any non-C50 values for IKs.
   * 
   * @return {@code true} if non-C50 values (e.g. Hill, saturation, spread)
   *         specified and non-default, otherwise {@code false}.
   */
  // Used currently data table display!
  public boolean retrieveNonC50IKs() {
    return (MeasurementObservationsVO.hasNonC50Value(hillIks, saturationIks,
                                                     spreadIks));
  }

  /**
   * Determine if there are any non-C50 values for INa.
   * 
   * @return {@code true} if non-C50 values (e.g. Hill, saturation, spread)
   *         specified and non-default, otherwise {@code false}.
   */
  // Used currently data table display!
  public boolean retrieveNonC50INa() {
    return (MeasurementObservationsVO.hasNonC50Value(hillINa, saturationINa,
                                                     spreadINa));
  }

  /**
   * Determine if there are any non-C50 values for INaL.
   * 
   * @return {@code true} if non-C50 values (e.g. Hill, saturation, spread)
   *         specified and non-default, otherwise {@code false}.
   */
  // Used currently data table display!
  public boolean retrieveNonC50INaL() {
    return (MeasurementObservationsVO.hasNonC50Value(hillINaL, saturationINaL,
                                                     spreadINaL));
  }

  /**
   * Determine if there are any non-C50 values for Ito.
   * 
   * @return {@code true} if non-C50 values (e.g. Hill, saturation, spread)
   *         specified and non-default, otherwise {@code false}.
   */
  // Used currently data table display!
  public boolean retrieveNonC50Ito() {
    return (MeasurementObservationsVO.hasNonC50Value(hillIto, saturationIto,
                                                     spreadIto));
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "SimulationInput [id=" + id + ", c50ICaL=" + c50ICaL + ", hillICaL="
        + hillICaL + ", saturationICaL=" + saturationICaL + ", spreadICaL="
        + spreadICaL + ", c50Ikr=" + c50Ikr + ", hillIkr=" + hillIkr
        + ", saturationIkr=" + saturationIkr + ", spreadIkr=" + spreadIkr
        + ", c50Ik1=" + c50Ik1 + ", hillIk1=" + hillIk1 + ", saturationIk1="
        + saturationIk1 + ", spreadIk1=" + spreadIk1 + ", c50Iks=" + c50Iks
        + ", hillIks=" + hillIks + ", saturationIks=" + saturationIks
        + ", spreadIks=" + spreadIks + ", c50Ito=" + c50Ito + ", hillIto="
        + hillIto + ", saturationIto=" + saturationIto + ", spreadIto="
        + spreadIto + ", c50INa=" + c50INa + ", hillINa=" + hillINa
        + ", saturationINa=" + saturationINa + ", spreadINa=" + spreadINa
        + ", c50INaL=" + c50INaL + ", hillINaL=" + hillINaL 
        + ", saturationINaL=" + saturationINaL + ", spreadINaL=" + spreadINaL 
        + ", c50Type=" + c50Type + ", ic50Units=" + ic50Units
        + ", modelIdentifier=" + modelIdentifier + ", pacingMaxTime="
        + pacingMaxTime + ", pacingFrequency=" + pacingFrequency
        + ", plasmaConcMax=" + plasmaConcMax + ", plasmaConcMin="
        + plasmaConcMin + ", plasmaIntPtCount=" + plasmaIntPtCount
        + ", plasmaIntPtLogScale=" + plasmaIntPtLogScale + ", plasmaConcPoints="
        + plasmaConcPoints + ", cellMLFileName=" + cellMLFileName
        /* Don't include the pkFile itself otherwise it'll grumble, e.g. could
           not initialize proxy - no Session.*/
        + ", pkFileName=" + pkFileName + ", credibleIntervalPctiles="
        + credibleIntervalPctiles + ", notes=" + notes + "]";
  }

  /**
   * Retrieve the entity identifier.
   * 
   * @return The entity identifier.
   */
  public Long getId() {
    return id;
  }

  /**
   * Retrieve the C50 value for ICaL.
   * 
   * @return ICaL C50 value (or {@code null} if not specified).
   */
  public BigDecimal getC50ICaL() {
    return asBigDecimal(c50ICaL);
  }

  /**
   * Retrieve the Hill Coefficient value for ICaL.
   * 
   * @return ICaL Hill Coefficient value (or {@code null} if not specified).
   */
  public BigDecimal getHillICaL() {
    return asBigDecimal(hillICaL);
  }

  /**
   * Retrieve the Saturation Level value for ICaL.
   * 
   * @return ICaL Saturation Level value (or {@code null} if not specified).
   */
  public BigDecimal getSaturationICaL() {
    return asBigDecimal(saturationICaL);
  }

  /**
   * Retrieve the spread value for ICaL.
   * 
   * @return ICaL spread value (or {@code null} if not specified).
   */
  public BigDecimal getSpreadICaL() {
    return asBigDecimal(spreadICaL);
  }

  /**
   * Retrieve the C50 value for IKr.
   * 
   * @return IKr C50 value (or {@code null} if not specified).
   */
  public BigDecimal getC50Ikr() {
    return asBigDecimal(c50Ikr);
  }

  /**
   * Retrieve the Hill Coefficient value for IKr.
   * 
   * @return IKr Hill Coefficient value (or {@code null} if not specified).
   */
  public BigDecimal getHillIkr() {
    return asBigDecimal(hillIkr);
  }

  /**
   * Retrieve the Saturation Level value for IKr.
   * 
   * @return IKr Saturation Level value (or {@code null} if not specified).
   */
  public BigDecimal getSaturationIkr() {
    return asBigDecimal(saturationIkr);
  }

  /**
   * Retrieve the spread value for IKr.
   * 
   * @return IKr spread value (or {@code null} if not specified).
   */
  public BigDecimal getSpreadIkr() {
    return asBigDecimal(spreadIkr);
  }

  /**
   * Retrieve the C50 value for IK1.
   * 
   * @return IK1 C50 value (or {@code null} if not specified).
   */
  public BigDecimal getC50Ik1() {
    return asBigDecimal(c50Ik1);
  }

  /**
   * Retrieve the Hill Coefficient value for IK1.
   * 
   * @return IK1 Hill Coefficient value (or {@code null} if not specified).
   */
  public BigDecimal getHillIk1() {
    return asBigDecimal(hillIk1);
  }

  /**
   * Retrieve the Saturation Level value for IK1.
   * 
   * @return IK1 Saturation Level value (or {@code null} if not specified).
   */
  public BigDecimal getSaturationIk1() {
    return asBigDecimal(saturationIk1);
  }

  /**
   * Retrieve the spread value for IK1.
   * 
   * @return IK1 spread value (or {@code null} if not specified).
   */
  public BigDecimal getSpreadIk1() {
    return asBigDecimal(spreadIk1);
  }

  /**
   * Retrieve the C50 value for IKs.
   * 
   * @return IKs C50 value (or {@code null} if not specified).
   */
  public BigDecimal getC50Iks() {
    return asBigDecimal(c50Iks);
  }

  /**
   * Retrieve the Hill Coefficient value for IKs.
   * 
   * @return IKs Hill Coefficient value (or {@code null} if not specified).
   */
  public BigDecimal getHillIks() {
    return asBigDecimal(hillIks);
  }

  /**
   * Retrieve the Saturation Level value for IKs.
   * 
   * @return IKs Saturation Level value (or {@code null} if not specified).
   */
  public BigDecimal getSaturationIks() {
    return asBigDecimal(saturationIks);
  }

  /**
   * Retrieve the spread value for IKs.
   * 
   * @return IKs spread value (or {@code null} if not specified).
   */
  public BigDecimal getSpreadIks() {
    return asBigDecimal(spreadIks);
  }

  /**
   * Retrieve the C50 value for Ito.
   * 
   * @return Ito C50 value (or {@code null} if not specified).
   */
  public BigDecimal getC50Ito() {
    return asBigDecimal(c50Ito);
  }

  /**
   * Retrieve the Hill Coefficient value for Ito.
   * 
   * @return Ito Hill Coefficient value (or {@code null} if not specified).
   */
  public BigDecimal getHillIto() {
    return asBigDecimal(hillIto);
  }

  /**
   * Retrieve the Saturation Level value for Ito.
   * 
   * @return Ito Saturation Level value (or {@code null} if not specified).
   */
  public BigDecimal getSaturationIto() {
    return asBigDecimal(saturationIto);
  }

  /**
   * Retrieve the spread value for Ito.
   * 
   * @return Ito spread value (or {@code null} if not specified).
   */
  public BigDecimal getSpreadIto() {
    return asBigDecimal(spreadIto);
  }

  /**
   * Retrieve the C50 value for INa.
   * 
   * @return INa C50 value (or {@code null} if not specified).
   */
  public BigDecimal getC50INa() {
    return asBigDecimal(c50INa);
  }

  /**
   * Retrieve the Hill Coefficient value for INa.
   * 
   * @return INa Hill Coefficient value (or {@code null} if not specified).
   */
  public BigDecimal getHillINa() {
    return asBigDecimal(hillINa);
  }

  /**
   * Retrieve the Saturation Level value for INa.
   * 
   * @return INa Saturation Level value (or {@code null} if not specified).
   */
  public BigDecimal getSaturationINa() {
    return asBigDecimal(saturationINa);
  }

  /**
   * Retrieve the spread value for INa.
   * 
   * @return INa spread value (or {@code null} if not specified).
   */
  public BigDecimal getSpreadINa() {
    return asBigDecimal(spreadINa);
  }

  /**
   * Retrieve the C50 value for INaL.
   * 
   * @return INaL C50 value (or {@code null} if not specified).
   */
  public BigDecimal getC50INaL() {
    return asBigDecimal(c50INaL);
  }

  /**
   * Retrieve the Hill Coefficient value for INaL.
   * 
   * @return INaL Hill Coefficient value (or {@code null} if not specified).
   */
  public BigDecimal getHillINaL() {
    return asBigDecimal(hillINaL);
  }

  /**
   * Retrieve the Saturation Level value for INaL.
   * 
   * @return INaL Saturation Level value (or {@code null} if not specified).
   */
  public BigDecimal getSaturationINaL() {
    return asBigDecimal(saturationINaL);
  }

  /**
   * Retrieve the spread value for INaL.
   * 
   * @return INaL spread value (or {@code null} if not specified).
   */
  public BigDecimal getSpreadINaL() {
    return asBigDecimal(spreadINaL);
  }

  /**
   * Retrieve the C50 type (if specified).
   * 
   * @return C50 type, e.g. pIC50 or IC50 (or {@code null} if not specified).
   */
  public C50_TYPE getC50Type() {
    return c50Type;
  }

  /**
   * Retrieve the IC50 units (if specified).
   * 
   * @return IC50 units (or {@code null} if not specified).
   */
  public IC50_UNIT getIc50Units() {
    return ic50Units;
  }

  /**
   * Retrieve the model identifier.
   * 
   * @return Model identifier, or {@code null} if using a CellML file.
   */
  public Short getModelIdentifier() {
    return modelIdentifier;
  }

  /**
   * Retrieve the pacing max time.
   * 
   * @return Pacing Max Time (or {@code null} if not specified).
   */
  public BigDecimal getPacingMaxTime() {
    return asBigDecimal(pacingMaxTime);
  }

  /**
   * Retrieve the pacing frequency.
   * 
   * @return Pacing frequency (or {@code null} if not specified).
   */
  public BigDecimal getPacingFrequency() {
    return asBigDecimal(pacingFrequency);
  }

  /**
   * Retrieve the maximum plasma concentration value.
   * 
   * @return Maximum plasma concentration (or {@code null} if not specified).
   */
  public BigDecimal getPlasmaConcMax() {
    return asBigDecimal(plasmaConcMax);
  }

  /**
   * Retrieve the minimum plasma concentration value.
   * 
   * @return Minimum plasma concentration (or {@code null} if not specified).
   */
  public BigDecimal getPlasmaConcMin() {
    return asBigDecimal(plasmaConcMin);
  }

  /**
   * Retrieve the plasma intermediate point count.
   * 
   * @return Plasma intermediate point count (or {@code null} if not specified).
   */
  public Short getPlasmaIntPtCount() {
    return plasmaIntPtCount;
  }

  /**
   * Retrieve the plasma intermediate point log scale preference.
   * 
   * @return True if intermediate point plasma concentrations based on log scale, otherwise false.
   */
  public boolean isPlasmaIntPtLogScale() {
    return plasmaIntPtLogScale;
  }

  /**
   * Retrieve individual plasma concentration points.
   * <p>
   * Note that concentration points, if defined, are returned as a
   * space-separated string of values. 
   *  
   * @return Plasma concentration points, or {@code null} if not specified.
   */
  public String getPlasmaConcPoints() {
    return plasmaConcPoints;
  }

  /**
   * Retrieve the PK file.
   * 
   * @return PK file (or {@code null} if none assigned).
   */
  public PortalFile getPkFile() {
    return pkFile;
  }

  /**
   * Retrieve the PK file name.
   * 
   * @return PK file name (or {@code null} if no PK file);
   */
  public String getPkFileName() {
    return pkFileName;
  }

  /**
   * Retrieve the CellML file name.
   * 
   * @return CellML file name (or {@code null} if no CellML file used);
   */
  public String getCellMLFileName() {
    return cellMLFileName;
  }

  /**
   * Retrieve the Delta APD90 credible interval percentiles.
   * 
   * @return Credible interval percentiles, or {@code null} if not specified.
   */
  public String getCredibleIntervalPctiles() {
    return credibleIntervalPctiles;
  }

  /**
   * Retrieve the simulation notes.
   * 
   * @return Simulation notes (or {@code null} if none specified).
   */
  public String getNotes() {
    return notes;
  }
}