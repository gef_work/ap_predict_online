# Action Potential prediction -- Web Interface Client Direct (*)

This is the code for running the AP-Portal client-direct web interface, i.e. the part that deals
with browser presentation.

In essence `client-direct` performs the following tasks :

  1. Presents the user with the opportunity to enter a compound data, e.g. IC50s, Hills, in the
     `client-direct` interface and click "Submit".
  1. `client-direct` calls `app-manager` to run the `ApPredict` simulation.
  1. Polls `app-manager` until simulation results are ready.
  1. Displays available simulation results for a compound.

## Installation

Please see either of the following :

  1. http://apportal.readthedocs.io/en/latest/installation/components/client-direct/index.html
  1. ap_predict_online/docs/AP-Portal/RtD/_build/html/installation/components/client-direct/index.html