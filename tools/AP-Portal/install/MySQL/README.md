## Terminology

_install_ is the process of using an installed MySQL binary archive to create a central database 
          for each of the AP-Portal components to use.

## MySQL version.

So far these scripts have been tested **only** using a MySQL 5.6.27 binary install.

## NOTES.

The person installing is assumed to be a non-*root* user as there's no need for the AP-Portal MySQL
to be installed *root* (and anyone with *root* privileges will have numerous alternative installation
options). If the AP-Portal MySQL is to be started automatically at system boot time then there are
two options :
 1. As a *root* user: A basic `sysV` init script is provided, or
 1. Alternatively, on some systems, the `cron` `@reboot` option may work.

Adjust `conf/my.cnf` if you don't want to listen on `127.0.0.1` port `3307`.  
Adjust `sql/install.sql` if you'd prefer to assign your own AP-Portal application database user
name and password.

## Helper tools.

 * `(/usr/sbin/)lsof -i -n -P`
 * `(/usr/bin/)strace -e trace=open ~/apps/mysql/current/bin/mysqladmin ...`