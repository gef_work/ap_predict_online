create database app_manager;
create database client character set utf8 collate utf8_general_ci;
create database client_direct character set utf8 collate utf8_general_ci;
create database business_manager;

create user 'apportal_user'@'localhost' identified by 'password';

grant all on app_manager.* to 'apportal_user'@'localhost' identified by 'password';
grant all on client.* to 'apportal_user'@'localhost' identified by 'password';
grant all on client_direct.* to 'apportal_user'@'localhost' identified by 'password';
grant all on business_manager.* to 'apportal_user'@'localhost' identified by 'password';

flush privileges;
