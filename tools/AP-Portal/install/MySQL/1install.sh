#!/bin/bash -e

#
# Binaries can be obtained from wget https://dev.mysql.com/get/Downloads/MySQL-5.6/mysql-5.6.27-linux-glibc2.5-x86_64.tar.gz
# for example.
#

if [ $# -ne 4 ]; then
  echo ""
  echo "  Use is: 1install.sh <mysql binary location> <database dir> <system_user> <system_group>"
  echo "    e.g.: 1install.sh /apps/mysql/5.6.27 /data/apportal user_me group_me"
  echo ""

  exit 1
fi

mysql_binary=$1
data_dir=$2
system_user=$3
system_group=$4

cecho() {
  message=${1:='No message!'}
  echo ""; echo -en "  "'\E[1;33;44m'" $message "
  tput sgr0
  echo ""; echo ""
  sleep 3
  return
}

#
# If the data directory already exists, don't just overwrite it!
#
if [ -d "${data_dir}" ]; then
  echo ""
  echo "  Database directory ${data_dir} already exists. Please either remove it"
  echo "  or select a different location. Exiting!"
  echo ""

  exit 1
fi

cecho "Creating ${data_dir} directory"

#
# As we're installing unprivileged, emulate a privileged deployment structure in the data
#   directory.
#
mkdir -p ${data_dir}/etc/init.d
mkdir -p ${data_dir}/innodb
mkdir -p ${data_dir}/var/{log,lib}/mysql
mkdir -p ${data_dir}/var/run/mysqld

cecho "Copying installation scripts"

#
# Create a temporary copy of the installation files in a subdirectory of the data directory. This
# temporary directory will subsequently be removed at the end of this processing.
#
scripts_dir=${data_dir}/scripts
buildscripts_dir=${scripts_dir}/buildscript

mkdir -p ${scripts_dir}
# Copy everything to temporary
cp -rv * ${scripts_dir}

pushd ${data_dir}

#
# Within the various config/shell files just copied to the temporary directory, update the content
#   by replacing placeholders with whatever args the user has just run this script.
#
for file in `grep -rl "<\(mysql_binary\|data_dir\|system_user\|system_group\)>" ${scripts_dir}`; do
  sed -i "s|<mysql_binary>|${mysql_binary}|g" ${file}
  sed -i "s|<data_dir>|${data_dir}|g" ${file}
  sed -i "s|<system_user>|${system_user}|g" ${file}
  sed -i "s|<system_group>|${system_group}|g" ${file}
done

cecho "Creating the MySQL configuration file"
${buildscripts_dir}/1create_conf.sh ${data_dir} ${scripts_dir}

cecho "Creating the default MySQL database setup"
${buildscripts_dir}/2create_database.sh ${mysql_binary} ${data_dir} ${system_user}

cecho "Portalising the database setup"
${buildscripts_dir}/3portalise.sh ${mysql_binary} ${data_dir} ${scripts_dir} ${system_user}

#
# Copy the sysV init script.
#
cp -v ${scripts_dir}/scripts/init/mysql_apportal ${data_dir}/etc/init.d/

#
# Remove the temporary scripts directory.
#
rm -rf ${scripts_dir}

popd