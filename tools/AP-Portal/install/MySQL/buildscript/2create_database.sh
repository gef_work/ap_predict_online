#!/bin/bash -e

#
# Use MySQL's mysql_install_db script to install the AP-Portal database in the data directory.
#

if [ $# -ne 3 ]; then
  echo ""
  echo "  Use is: 2create_database.sh <mysql binary location> <database dir> <system_user>"
  echo "    e.g.: 2create_database.sh /apps/mysql/5.6.27 /data/apportal user_me"
  echo ""

  exit 1
fi

mysql_binary=$1
data_dir=$2
system_user=$3

if [ ! -d "${data_dir}" ]; then
  echo ""
  echo "  Database directory ${data_dir} does not exist. Exiting!"
  echo ""

  exit 1
fi

pushd $data_dir

#
# Should really be using ${mysql_binary}/bin/mysql_secure_installation!
#
${mysql_binary}/scripts/mysql_install_db --user=${system_user} \
                                         --basedir=${mysql_binary} \
                                         --keep-my-cnf \
                                         --defaults-file=${data_dir}/my.cnf \
                                         --datadir=`pwd`

popd
