#!/bin/bash -e

#
# Start the newly-created database (in the previous step!) and run the install.sql script to 
# assign the AP-Portal application database user, password and privileges. At the end shut down
# the database.
#

if [ $# -ne 4 ]; then
  echo ""
  echo "  Use is: 3portalise.sh <mysql binary location> <database dir> <scripts dir> <system user>"
  echo "    e.g.: 3portalise.sh /apps/mysql/5.6.27 /data/apportal /data/apportal/scripts user_me"
  echo ""

  exit 1
fi

mysql_binary=$1
data_dir=$2
scripts_dir=$3
system_user=$4

if [ ! -d "${data_dir}" ]; then
  echo ""
  echo "  Database directory ${data_dir} does not exist. Exiting!"
  echo ""

  exit 1
fi

pushd $data_dir

err_file=${data_dir}/apportal_import.err
rm -f ${err_file}

${mysql_binary}/bin/mysqld_safe --defaults-file=${data_dir}/my.cnf \
                                --basedir=${mysql_binary} \
                                --datadir=${data_dir} \
                                --ledir=${mysql_binary}/bin &

# Just in case mysql's a bit slow!
sleep 2s

#
# mysql (and mysqladmin below) seem to have problem reading the socket details
# if --defaults-file=??? is specified in the command line. Haven't got time to
# investigate why! I had tried using `strace -e trace=open ${mysql_binary}/bin/mysql ...`
# and it seemed to read the --defaults-file file but just ignored the entire
# content.
#
${mysql_binary}/bin/mysql --socket=${data_dir}/var/lib/mysql/mysql.sock --user=root <${scripts_dir}/sql/install.sql >${err_file}

if [ -s ${err_file} ]; then
  # File exists and not empty
  echo ""
  echo "  Something went wrong! Please check the content of ${err_file}"
  echo ""
else 
  if [ -f ${err_file} ]; then
    rm -f ${err_file}
  fi

  echo ""
  echo "  To start the MySQL server on localhost:3307 (as defined in the my.cnf). For example ..."
  echo "    ${mysql_binary}/bin/mysqld_safe --defaults-file=${data_dir}/my.cnf --basedir=${mysql_binary} --datadir=${data_dir} --ledir=${mysql_binary}/bin &"
  echo "  Please check the installation (password is in the sql init script!). For example ..."
  echo "    ${mysql_binary}/bin/mysql --socket=${data_dir}/var/lib/mysql/mysql.sock -u apportal_user -p"
  echo "  To stop the MySQL server. For example ..."
  echo "    ${mysql_binary}/bin/mysqladmin --socket=${data_dir}/var/lib/mysql/mysql.sock -u root -h 127.0.0.1 -P3307 shutdown"

  echo ""
fi

${mysql_binary}/bin/mysqladmin --socket=${data_dir}/var/lib/mysql/mysql.sock -u root shutdown

popd
