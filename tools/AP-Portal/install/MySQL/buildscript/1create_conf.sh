#!/bin/bash -e

#
# Simply copies the my.cnf from the temporary scripts directory to the base of the data directory.
#

if [ $# -ne 2 ]; then
  echo ""
  echo "  Use is: 1create_conf.sh <database dir> <scripts dir>"
  echo "    e.g.: 1create_conf.sh /data/apportal /data/apportal/scripts"
  echo ""

  exit 1
fi

data_dir=$1
scripts_dir=$2

if [ ! -d "${data_dir}" ]; then
  echo ""
  echo "  Database directory ${data_dir} does not exist. Exiting!"
  echo ""

  exit 1
fi

cp -v ${scripts_dir}/conf/my.cnf ${data_dir}/my.cnf