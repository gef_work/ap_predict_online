#!/bin/bash -e

CATALINA_HOME=${1:-`pwd`}

if [ ! -d ${CATALINA_HOME} ]; then
  echo ""
  echo "  Directory ${CATALINA_HOME} does not exist. Exiting!"
  echo ""

  exit 1
fi

pushd $CATALINA_HOME

for component in {client,client-direct,app-manager,site-business,dose-response-manager}; do
  ./reset.sh ${component}
done

popd
