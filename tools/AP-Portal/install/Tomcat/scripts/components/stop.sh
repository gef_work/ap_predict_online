#!/bin/bash -e

if [ $# -lt 1 ] || [ $# -gt 2 ]; then
  echo ""
  echo "  Usage: stop.sh <component> (Optional <CATALINA_HOME>, defaults to `pwd`)"
  echo "    e.g. stop.sh app-manager ${HOME}/apps/tomcat/CATALINA_HOME"
  echo ""

  exit 1
fi

component=$1
CATALINA_HOME=${2:-`pwd`}

if [ ! -d ${CATALINA_HOME}/${component} ]; then
  echo ""
  echo "  Unknown AP-Portal component ${CATALINA_HOME}/${component}. Exiting!"
  echo ""

  exit 1
fi

. ${CATALINA_HOME}/${component}.env

${CATALINA_HOME}/bin/shutdown.sh