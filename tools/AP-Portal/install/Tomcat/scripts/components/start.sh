#!/bin/bash -e

if [ $# -lt 1 ] || [ $# -gt 2 ]; then
  echo ""
  echo "  Usage: start.sh <component> (Optional <CATALINA_HOME>, defaults to `pwd`)"
  echo "    e.g. start.sh app-manager ${HOME}/apps/tomcat/CATALINA_HOME"
  echo ""

  exit 1
fi

component=$1
CATALINA_HOME=${2:-`pwd`}

CATALINA_BASE=${CATALINA_HOME}/${component}

if [ ! -d "${CATALINA_BASE}" ]; then
  echo ""
  echo "  Unknown AP-Portal component ${CATALINA_BASE}. Exiting!"
  echo ""

  exit 1
fi

rm -rf ${CATALINA_BASE}/{logs,temp,work}/*
rm -f ${CATALINA_HOME}/logs/${component}.log*

. ${CATALINA_HOME}/secure.env
. ${CATALINA_HOME}/${component}.env

${CATALINA_HOME}/bin/catalina.sh start