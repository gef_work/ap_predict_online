#!/bin/bash -e

if [ $# -lt 1 ] || [ $# -gt 2 ]; then
  echo ""
  echo "  Usage: reset.sh <component> <ap_predict_online>"
  echo "    e.g. reset.sh app-manager /apps/src/ap_predict_online"
  echo ""

  exit 1
fi

component=$1
# Default to /apps/src/ap_predict_online if not defined.
ap_predict_online=${2:-/apps/src/ap_predict_online}

if [ -d ${component} ] ; then
  rm -rf ${component}/{webapps,logs,temp,work}/*
  rm -f logs/${component}.log*
fi

underscored=${component//-/_}

if [ "dose-response-manager" == "${component}" ]; then
  cp -v ${ap_predict_online}/${component}/target/fdr_manager-0.0.1-SNAPSHOT.war ${component}/webapps/
  cp -v ${ap_predict_online}/${component}/lib/libfittingdoseresponse.so ${component}/lib/
else
  cp -v ${ap_predict_online}/${component}/target/${underscored}-0.0.1-SNAPSHOT.war ${component}/webapps/
fi
