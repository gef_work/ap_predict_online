#!/bin/bash -e

if [ $# -gt 1 ]; then
  echo ""
  echo "  Use is: start_all.sh (Optional <CATALINA_HOME>, defaults to `pwd`)"
  echo "    e.g.: start_all.sh"
  echo "    e.g.: start_all.sh /apps/tomcat/CATALINA_HOME"
  echo ""

  exit 1
fi

CATALINA_HOME=${1:-`pwd`}

if [ ! -d ${CATALINA_HOME} ]; then
  echo ""
  echo "  Directory ${CATALINA_HOME} does not exist. Exiting!"
  echo ""

  exit 1
fi

pushd $CATALINA_HOME

for component in {client,client-direct,app-manager,site-business,dose-response-manager}; do
  ./start.sh ${component}
done

popd