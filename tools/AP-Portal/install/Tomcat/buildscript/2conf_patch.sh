#!/bin/bash -e

if [ $# -ne 3 ]; then
  echo ""
  echo "  Use is: 2conf_patch.sh <install_dir> <scripts dir> <tomcat_ver>"
  echo "    e.g.: 2conf_patch.sh /apps/tomcat /apps/tomcat/scripts 7.0.56"
  echo ""

  exit 1
fi

install_dir=$1
scripts_dir=$2
tomcat_ver=$3

pushd ${install_dir}/vhosts

for vhost in {app-manager,client,client-direct,dose-response-manager,site-business}; do
  pushd ${vhost}

  patch conf/server.xml ${scripts_dir}/${tomcat_ver}/patch/${vhost}.patch

  popd
done

popd