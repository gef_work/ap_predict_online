#!/bin/bash -e

if [ $# -ne 2 ]; then
  echo ""
  echo "  Use is: copy_base.sh <source dir> <install_dir>"
  echo "    e.g.: copy_base.sh /apps/src/apache-tomcat-7.0.56 /apps/tomcat"
  echo ""

  exit 1
fi

tomcat_src=$1
install_dir=$2

#
# 1. Copy bin,lib,logs into CATALINA_HOME
#
pushd ${install_dir}/CATALINA_HOME

cp -rv ${tomcat_src}/{bin,lib,logs} .
rm bin/tomcat-juli.jar

popd

#
# 2. Copy others into vhosts
#

pushd ${install_dir}/vhosts

for vhost in {app-manager,client,client-direct,dose-response-manager,site-business}; do
  # Create directory
  mkdir -p ${vhost}/{bin,lib,logs,temp,webapps,work}

  # Copy across configuration files
  cp -rv ${tomcat_src}/conf ${vhost}

  cp -v ${tomcat_src}/bin/tomcat-juli.jar ${vhost}/bin/
done

popd