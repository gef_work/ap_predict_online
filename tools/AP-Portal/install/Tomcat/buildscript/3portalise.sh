#!/bin/bash -e

if [ $# -ne 1 ]; then
  echo ""
  echo "  Use is: 3portalise.sh <install dir>"
  echo "    e.g.: 3portalise.sh /apps/tomcat"
  echo ""

  exit 1
fi

install_dir=$1

pushd ${install_dir}/vhosts

for vhost in {app-manager,client,client-direct,dose-response-manager,site-business}; do
  pushd ${vhost}

  ln -s ../../CATALINA_HOME/{host.pkcs12,trustedkeystore.jks} .

  popd
done

popd

pushd ${install_dir}/CATALINA_HOME

# procdir is the directory for holding simulation process information files.
mkdir procdir

ln -s ../vhosts/* .

cp -rv ${install_dir}/scripts/scripts/components/* .
cp -rv ${install_dir}/scripts/envs/components/* .
cp -v ${install_dir}/scripts/envs/secure.env .
cp -rv ${install_dir}/scripts/READMEs/* .

popd
