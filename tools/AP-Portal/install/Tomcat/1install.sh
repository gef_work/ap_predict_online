#!/bin/bash -e

#
# Apache Tomcat binary installs available from https://tomcat.apache.org/download-70.cgi or
# http://archive.apache.org/dist/tomcat/
#

if [ $# -ne 3 ]; then
  echo ""
  echo "  Use is: 1install.sh <tomcat source code> <tomcat destination> <tomcat version>"
  echo "    e.g.: 1install.sh /apps/src/apache-tomcat-7.0.56 /apps/tomcat 7.0.56"
  echo ""

  exit 1
fi

tomcat_src=$1
install_dir=$2
tomcat_ver=$3

cecho() {
  message=${1:='No message!'}
  echo ""; echo -en "  "'\E[1;33;44m'" $message "
  tput sgr0
  echo ""; echo ""
  sleep 3
  return
}

if [ ! -d "${tomcat_src}" ]; then
  echo ""
  echo "  Tomcat source code directory ${tomcat_src} does not exist! Exiting."
  echo ""

  exit 1
fi

#
# If the directory already exists, don't just overwrite the content.
#
if [ -d "${install_dir}/CATALINA_HOME" ] || [ -d "${install_dir}/vhosts" ]; then
  echo ""
  echo "  Some/all of the Tomcat destination directory(ies) ${install_dir}/{CATALINA_HOME,vhosts} already exist(s)!"
  echo "  Please remove them prior to install."
  echo ""

  exit 1
fi

cecho "Creating ${install_dir} directories"

mkdir -p ${install_dir}/{CATALINA_HOME,vhosts}

cecho "Copying installation scripts"

#
# Create a temporary copy of the installation files in a subdirectory of the data directory. This
# temporary directory will subsequently be removed at the end of this processing.
#
scripts_dir=${install_dir}/scripts
buildscripts_dir=${scripts_dir}/buildscript

mkdir -p ${scripts_dir}
# Copy everything to temporary
cp -rv * ${scripts_dir}

pushd ${install_dir}

cecho "Copying vanilla Tomcat files to component structures"
${buildscripts_dir}/1copy_base.sh ${tomcat_src} ${install_dir}

cecho "Applying component-specific patches to vanilla configuration file"
${buildscripts_dir}/2conf_patch.sh ${install_dir} ${scripts_dir} ${tomcat_ver}

cecho "Portalise the tomcat vhosts"
${buildscripts_dir}/3portalise.sh ${install_dir}

#
# Remove the temporary scripts directory.
#
rm -rf ${scripts_dir}

popd