## Terminology.

_install_ is the process of taking vanilla Tomcat files and deploying them into a consistent
          deployment file structure for each of the AP-Portal components.

## NOTES.

 1. The patch files are Tomcat-version specific. There was a small change to Tomcat `server.xml`
    files between v7 releases which resulted in the patches being unsuitable.
 1. The Tomcats (a virtual host for each AP-Portal component) are expected to  
    a) Use the https protocol (using self-signed certificates), and;  
    b) Have default ports assigned in the patch files, and;  
    c) Have only the `client` and `client-direct` components configured by default to listen on non-`localhost/127.0.0.1` addresses.

## Helper tools.

 * `(/usr/sbin/)lsof -i -n -P`