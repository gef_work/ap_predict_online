#!/usr/bin/perl

# 
# Centos: yum install perl-Archive-Tar
# Centos: yum install perl-libwww-perl
# Centos: yum install perl-LWP-Protocol-https 
#

use strict;
use warnings;
use Archive::Tar;
use LWP::Simple;

my $gary_files = "https://www.cs.ox.ac.uk/people/gary.mirams/files";
my $url_manifest = "$gary_files/appredict_lookup_table_manifest.txt";

#
# Read in all files in this directory.
#
my @local_files=`ls`;
chomp @local_files;

#
# Read in the lookup file names listed in the remote manifest
#
my $manifest_content = get $url_manifest;
my @exists_remotely = split "\n", $manifest_content;

# These files are missing
my @missing;

#
# Perl Cookbook : 4.7. Finding Elements in One Array but Not Another
#
my %exists_locally;
@exists_locally{@local_files} = ();

foreach my $remote_name (@exists_remotely) {
  push(@missing, $remote_name) unless exists $exists_locally{$remote_name}
}

#
# Traverse the files we're lacking and download each in turn.
#
my @downloaded = ();
foreach my $file_to_download (@missing) {
  # Get the file
  my $url_lookup = "$gary_files/$file_to_download";
  print "$file_to_download - Retrieving.";
  my $file_content = get $url_lookup;

  # Write to disk
  print "\n$file_to_download - Writing to disk.";
  my $file = $file_to_download;
  open (my $file_handle, '>', $file) or die "Could not open file '$file' $!";
  print $file_handle $file_content;
  close $file_handle;
  # Unpack if it's a tgz
  if ($file_to_download =~ /\.tgz$/i) {
    print "\n$file_to_download - Unpacking.\n";
    my $tar = Archive::Tar->new;
    $tar->read( $file_to_download );
    $tar->extract();

    push(@downloaded, $file_to_download);
  }
}
