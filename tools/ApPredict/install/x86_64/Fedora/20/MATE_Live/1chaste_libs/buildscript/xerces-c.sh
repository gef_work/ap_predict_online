#!/bin/bash -e

if [ $# -ne 4 ]; then
  echo ""
  echo "  Use is build_xerces-c.sh <build dir> <download dir> <chaste-libs dir> <processor count>"
  echo "    e.g. build_xerces-c.sh ~/chaste-libs-151002/build ~/chaste-libs-151002/download ~/chaste-libs-151002 8"
  echo ""

  exit 1
fi

build_dir=$1
download_dir=$2
chaste_libs_dir=$3
processor_count=$4

xerces_c=xerces-c-3.1.2

pushd ${download_dir}

# Some alternatives if necessary ...
#   wget http://archive.apache.org/dist/xml/xerces-c/Xerces-C_<version>
#
if [ -f "${xerces_c}.tar.gz" ]; then
  echo "  ${xerces_c}.tar.gz already downloaded"
else
  wget http://archive.apache.org/dist/xerces/c/3/sources/${xerces_c}.tar.gz
fi

popd

pushd ${build_dir}

cp -v ${download_dir}/${xerces_c}.tar.gz .

tar -zxf ${xerces_c}.tar.gz

pushd ${xerces_c}

export XERCESCROOT=`pwd`

./configure --prefix=${chaste_libs_dir}

make -j${processor_count}
make install

popd

rm -rf ${xerces_c}*
rm -vf ${download_dir}/${xerces_c}.tar.gz

popd
