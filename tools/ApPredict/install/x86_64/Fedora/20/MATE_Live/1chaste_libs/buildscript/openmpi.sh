#!/bin/bash

if [ $# -ne 4 ]; then
  echo ""
  echo "  Use is build_openmpi.sh <build dir> <download dir> <chaste-libs dir> <processor count>"
  echo "    e.g. build_openmpi.sh ~/chaste-libs-151002/build ~/chaste-libs-151002/download ~/chaste-libs-151002 8"
  echo ""

  exit 1
fi

build_dir=$1
download_dir=$2
chaste_libs_dir=$3
processor_count=$4

openmpi=openmpi-1.6.4

pushd ${download_dir}

#
# Some alternatives if necessary ...
#  wget http://www.open-mpi.org/software/ompi/v1.6/downloads/openmpi-1.6.4.tar.gz
#

if [ -f "${openmpi}.tar.gz" ]; then
  echo "  ${openmpi}.tar.gz already downloaded"
else
  wget http://ftp.mcs.anl.gov/pub/petsc/externalpackages/${openmpi}.tar.gz
fi

popd

pushd ${build_dir}

cp -v ${download_dir}/${openmpi}.tar.gz .

tar -zxf ${openmpi}.tar.gz

pushd ${openmpi}

./configure --prefix=${chaste_libs_dir}

make -j${processor_count} all install

popd

rm -rf ${openmpi}*
rm -vf ${download_dir}/${openmpi}.tar.gz

popd
