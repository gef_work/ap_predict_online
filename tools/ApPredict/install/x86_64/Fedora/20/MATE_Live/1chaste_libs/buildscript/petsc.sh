#!/bin/bash -e

if [ $# -ne 3 ]; then
  echo ""
  echo "  Use is build_petsc.sh <build dir> <download dir> <chaste-libs dir>"
  echo "    e.g. build_petsc.sh ~/chaste-libs-151002/build ~/chaste-libs-151002/download ~/chaste-libs-151002"
  echo ""

  exit 1
fi

build_dir=$1
download_dir=$2
chaste_libs_dir=$3

petsc_tar_gz=petsc-3.4.4.tar.gz
parmetis=parmetis-4.0.2-p5
metis=metis-5.0.2-p3
hdf5=hdf5-1.8.10-patch1
f2cblaslapack=f2cblaslapack-3.4.1.q
sundials=sundials-2.5.0
hypre=hypre-2.9.1a

pushd ${download_dir}

if [ -f "${petsc_tar_gz}" ]; then
  echo "  ${petsc_tar_gz} already downloaded"
else
  wget http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/${petsc_tar_gz}
fi

#
# Some alternatives if necessary ...
#  wget https://www.hdfgroup.org/ftp/HDF5/releases/hdf5-1.8.10-patch1/src/hdf5-1.8.10-patch1.tar.gz
#

for dependency in {${parmetis},${metis},${hdf5},${f2cblaslapack},${sundials},${hypre}}; do
  full_name="${dependency}.tar.gz"
  if [ -f ${full_name} ]; then
    echo "  ${dependency} already downloaded"
  else
    wget http://ftp.mcs.anl.gov/pub/petsc/externalpackages/${full_name}
  fi
done

popd

pushd ${build_dir}

cp -v ${download_dir}/${petsc_tar_gz} .

tar -zxf ${petsc_tar_gz}

pushd petsc-3.4.4

export PETSC_DIR=`pwd`
export PETSC_ARCH=linux-gnu
#
# `--download-openmpi` doesn't work with the `--prefix` option
#
./config/configure.py --prefix=${chaste_libs_dir} \
                      --with-gnu-compilers=true \
                      --with-x=false \
                      --with-clanguage=cxx \
                      --with-shared-libraries=true \
                      --with-debugging=0 \
                      --with-mpi-dir=${chaste_libs_dir} \
                      --download-hdf5=${download_dir}/${hdf5}.tar.gz \
                      --download-f2cblaslapack=${download_dir}/${f2cblaslapack}.tar.gz \
                      --download-metis=${download_dir}/${metis}.tar.gz \
                      --download-parmetis=${download_dir}/${parmetis}.tar.gz \
                      --download-sundials=${download_dir}/${sundials}.tar.gz \
                      --download-hypre=${download_dir}/${hypre}.tar.gz
make
make install

popd

rm -rf petsc-3.4.4*
rm -vf ${download_dir}/${petsc_tar_gz}
rm -vf ${download_dir}/${hdf5}.tar.gz
rm -vf ${download_dir}/${f2cblaslapack}.tar.gz
rm -vf ${download_dir}/${metis}.tar.gz
rm -vf ${download_dir}/${parmetis}.tar.gz
rm -vf ${download_dir}/${sundials}.tar.gz
rm -vf ${download_dir}/${hypre}.tar.gz

popd
