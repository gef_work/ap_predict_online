## Terminology.

_install_ is the process of both compiling and transferring a subset of the compiled-from-source
          files into a consistent file structure.