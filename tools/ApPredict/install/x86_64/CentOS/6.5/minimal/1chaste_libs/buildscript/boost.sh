#!/bin/bash -e

if [ $# -ne 4 ]; then
  echo ""
  echo "  Use is boost.sh <build dir> <download dir> <chaste-libs dir> <processor count>"
  echo "    e.g. boost.sh ~/chaste-libs-151002/build ~/chaste-libs-151002/download ~/chaste-libs-151002 8"
  echo ""

  exit 1
fi

build_dir=$1
download_dir=$2
chaste_libs_dir=$3
processor_count=$4

boost_ver=1.54.0
boost=boost_1_54_0

pushd ${download_dir}

if [ -f "${boost}.tar.gz" ]; then
  echo "  ${boost}.tar.gz already downloaded"
else
  wget https://netix.dl.sourceforge.net/project/boost/boost/${boost_ver}/${boost}.tar.gz
fi

popd

pushd ${build_dir}

cp -v ${download_dir}/${boost}.tar.gz .

tar -zxf ${boost}.tar.gz

pushd ${boost}

./bootstrap.sh --with-libraries=system,filesystem,serialization
./b2 -j${processor_count} --prefix=${chaste_libs_dir} install

popd

rm -rf ${boost}*
rm -vf ${download_dir}/${boost}.tar.gz

popd
