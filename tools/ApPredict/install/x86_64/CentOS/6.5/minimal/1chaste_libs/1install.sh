#!/bin/bash -e

if [ $# -ne 2 ]; then
  echo ""
  echo "  Use is 1install.sh <install dir> <processor count>"
  echo "    e.g. 1install.sh ${HOME} 8"
  echo ""

  exit 1
fi

install_dir=$1
processor_count=$2

cecho() {
  message=${1:='No message!'}
  echo ""; echo -en "  "'\E[1;33;44m'" $message "
  tput sgr0
  echo ""; echo ""
  sleep 3
  return
}

if [ ! -d "${install_dir}" ]; then
  cecho "Create installation directory ${install_dir}"
  mkdir -p ${install_dir}
fi

date=`date +%y%m%d`
chaste_libs_dir=${install_dir}/chaste-libs/${date}

if [ ! -d "${chaste_libs_dir}" ]; then
  cecho "Creating directory ${chaste_libs_dir}"
  mkdir -p ${chaste_libs_dir}
fi

# build scripts
scripts_dir=${chaste_libs_dir}/scripts
buildscripts_dir=${scripts_dir}/buildscript

# build/install directories
build_dir=${chaste_libs_dir}/build
download_dir=${chaste_libs_dir}/download
bin_dir=${chaste_libs_dir}/bin
lib_dir=${chaste_libs_dir}/lib

for directory in {${scripts_dir},${build_dir},${download_dir}}; do
  if [ ! -d "${directory}" ]; then
    mkdir ${directory}
  fi
done

cecho "Copying installation scripts"
# Copy the installation scripts into a subdirectory of the chaste-libs directory.
cp -rv * ${scripts_dir}

pushd ${chaste_libs_dir}

cecho "OpenMPI installation"
if [ -f ${lib_dir}/libmpi.so ]; then
  echo "  Presence of ${lib_dir}/libmpi.so taken as successful existing build of OpenMPI"
else
  ${buildscripts_dir}/openmpi.sh ${build_dir} ${download_dir} ${chaste_libs_dir} ${processor_count}
fi

cecho "PETSc, HDF5, f2cblaslapack, METIS, ParMETIS, Sundials, Hypre installation"
if [ -f ${lib_dir}/libpetsc.so ]; then
  echo "  Presence of ${lib_dir}/libpetsc.so taken as successful existing build of PETSc, HDF5, f2cblaslapack, METIS, ParMETIS, Sundials and Hypre"
else
  ${buildscripts_dir}/petsc.sh ${build_dir} ${download_dir} ${chaste_libs_dir}
fi

cecho "XSD installation"
if [ -f ${bin_dir}/xsd ]; then
  echo "  Presence of ${bin_dir}/xsd taken as successful existing build of XSD"
else
  ${buildscripts_dir}/xsd.sh ${build_dir} ${download_dir} ${chaste_libs_dir}
fi

cecho "Xerces-C installation"
if [ -f ${lib_dir}/libxerces-c.so ]; then
  echo "  Presence of ${lib_dir}/libxerces-c.so taken as successful existing build of Xerces-C"
else
  ${buildscripts_dir}/xerces-c.sh ${build_dir} ${download_dir} ${chaste_libs_dir} ${processor_count}
fi

cecho "Boost installation"
if [ -f ${lib_dir}/libboost_filesystem.so ]; then
  echo "  Presence of ${lib_dir}/libboost_filesystem.so taken as successful existing build of Boost"
else
  ${buildscripts_dir}/boost.sh ${build_dir} ${download_dir} ${chaste_libs_dir} ${processor_count}
fi

cecho "4Suite_XML,Amara,html5lib,isodate,pyparsing,python_dateutil,rdflib,six,SPARQLWrapper installation"
if [ -f ${lib_dir}/python2.6/site-packages/Amara-1.2.0.2-py2.6.egg ]; then
  echo "  Presence of ${lib_dir}/python2.6/site-packages/Amara-1.2.0.2-py2.6.egg taken as successful existing build of Python libraries"
else
  ${buildscripts_dir}/python.sh ${build_dir} ${download_dir} ${chaste_libs_dir}
fi

cecho "Removing unnecessary directories"

rmdir ${build_dir} ${download_dir}
rm -rf ${chaste_libs_dir}/scripts ${chaste_libs_dir}/conf

popd
