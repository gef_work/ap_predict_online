#!/bin/bash -e

if [ $# -ne 3 ]; then
  echo ""
  echo "  Use is xsd.sh <build dir> <download dir> <chaste-libs dir>"
  echo "    e.g. xsd.sh ~/chaste-libs-151002/build ~/chaste-libs-151002/download ~/chaste-libs-151002"
  echo ""

  exit 1
fi

build_dir=$1
download_dir=$2
chaste_libs_dir=$3

xsd=xsd-3.3.0-x86_64-linux-gnu

pushd ${download_dir}

if [ -f "${xsd}.tar.bz2" ]; then
  echo "  ${xsd}.tar.bz2 already downloaded"
else
  wget http://www.codesynthesis.com/download/xsd/3.3/linux-gnu/x86_64/${xsd}.tar.bz2
fi

popd

pushd ${build_dir}

cp -v ${download_dir}/${xsd}.tar.bz2 .

tar -xjf ${xsd}.tar.bz2

pushd ${xsd}

cp -v bin/xsd ${chaste_libs_dir}/bin
cp -vr libxsd/xsd ${chaste_libs_dir}/include

popd

rm -rf ${xsd}*
rm -vf ${download_dir}/${xsd}.tar.bz2

popd
