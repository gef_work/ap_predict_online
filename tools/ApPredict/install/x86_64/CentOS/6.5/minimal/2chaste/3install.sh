#!/bin/bash -e

if [ $# -ne 2 ]; then
  echo ""
  echo "  Use is: 3install.sh <base_install dir> <chaste-libs>/lib"
  echo "    e.g.: 3install.sh ${HOME}/apps ${HOME}/chaste-libs/151004/lib"
  echo ""

  exit 1
fi

base_install_dir=$1
chaste_libs_dir=$2

if [ ! -d ${base_install_dir} ]; then
  echo ""
  echo "  Creating directory ${base_install_dir}"
  echo ""

  mkdir -p ${base_install_dir}
fi

#
# 1. Download the credible-interval files.
#

appredict_base_dir=${base_install_dir}/appredict

if [ ! -d ${appredict_base_dir} ]; then
  echo ""
  echo "  Creating directory ${appredict_base_dir}"
  echo ""

  mkdir -p ${appredict_base_dir}
fi

pushd ${appredict_base_dir}

if [ ! -f "shannon_wang_puglisi_weber_bers_2004_model_updated_4d_hERG_IKs_INa_ICaL_0.5Hz_generator.arch" ]; then
  wget http://www.cs.ox.ac.uk/people/gary.mirams/files/shannon_wang_puglisi_weber_bers_2004_model_updated_4d_hERG_IKs_INa_ICaL_0.5Hz_generator.arch.tgz
  tar -zxf shannon_wang_puglisi_weber_bers_2004_model_updated_4d_hERG_IKs_INa_ICaL_0.5Hz_generator.arch.tgz
  rm -vf shannon_wang_puglisi_weber_bers_2004_model_updated_4d_hERG_IKs_INa_ICaL_0.5Hz_generator.arch.tgz
fi
if [ ! -f "shannon_wang_puglisi_weber_bers_2004_model_updated_4d_hERG_IKs_INa_ICaL_1Hz_generator.arch" ]; then
  wget http://www.cs.ox.ac.uk/people/gary.mirams/files/shannon_wang_puglisi_weber_bers_2004_model_updated_4d_hERG_IKs_INa_ICaL_1Hz_generator.arch.tgz
  tar -zxf shannon_wang_puglisi_weber_bers_2004_model_updated_4d_hERG_IKs_INa_ICaL_1Hz_generator.arch.tgz
  rm -vf shannon_wang_puglisi_weber_bers_2004_model_updated_4d_hERG_IKs_INa_ICaL_1Hz_generator.arch.tgz
fi

popd

#
# 2. Set up the ApPredict 
#

date=`date +%y%m%d`
full_install_path=${base_install_dir}/appredict/${date}
libs_install_path=${full_install_path}/libs

if [ ! -d ${libs_install_path} ]; then
  echo ""
  echo "  Creating directory ${libs_install_path}"
  echo ""

  mkdir -p ${libs_install_path}
fi

cp -v ApPredict.sh ${full_install_path}
sed -i "s~<chaste-libs>~${chaste_libs_dir}~g" ${full_install_path}/ApPredict.sh

cp -v ApPredict/apps/src/ApPredict ${full_install_path}

cp -v Chaste/lib/*.so ${libs_install_path}
