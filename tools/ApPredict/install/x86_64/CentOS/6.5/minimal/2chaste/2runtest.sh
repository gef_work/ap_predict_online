#!/bin/bash -e

if [ $# -ne 1 ]; then
  echo ""
  echo "  Use is: 2runtest.sh <chaste-libs>/lib"
  echo "    e.g.: 2runtest.sh ${HOME}/chaste-libs/151004/lib"
  echo ""

  exit 1
fi

chaste_libs=$1

export LD_LIBRARY_PATH=Chaste/lib:${chaste_libs}

echo ""
echo "  Using LD_LIBRARY_PATH value of ${LD_LIBRARY_PATH}"
echo ""nano 

./ApPredict/apps/src/ApPredict --model 1 \
                               --pacing-freq 1.0 \
                               --pic50-cal 4.5 \
                               --pic50-herg 5.6 \
                               --pic50-ik1 3.4 \
                               --pic50-iks 4.4 \
                               --pic50-ito 2.9 \
                               --pic50-na 5.2 \
                               --plasma-conc-high 100 \
                               --plasma-conc-low 0 \
                               --plasma-conc-count 5 \
                               --plasma-conc-logscale true

if [ ! -d "testoutput" ]; then
  echo ""
  echo "  Had expected a 'testoutput' directory to remove!"
  echo ""
else
  echo ""
  echo "  Test appears to have completed successfully!"
  echo ""

  rm -rf testoutput
fi
