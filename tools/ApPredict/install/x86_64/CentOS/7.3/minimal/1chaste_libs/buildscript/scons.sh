#!/bin/bash

if [ $# -ne 3 ]; then
  echo ""
  echo "  Use is scons.sh <build dir> <download dir> <chaste-libs dir>"
  echo "    e.g. scons.sh ~/chaste-libs-151002/build ~/chaste-libs-151002/download ~/chaste-libs-151002"
  echo ""

  exit 1
fi

build_dir=$1
download_dir=$2
chaste_libs_dir=$3

scons=scons-2.5.1

pushd ${download_dir}

#
# Some alternatives if necessary ...
#  wget http://www.open-mpi.org/software/ompi/v1.6/downloads/openmpi-1.6.4.tar.gz
#

if [ -f "${scons}.tar.gz" ]; then
  echo "  ${scons}.tar.gz already downloaded"
else
  wget http://sourceforge.mirrorservice.org/s/sc/scons/scons/2.5.1/${scons}.tar.gz
fi

popd

pushd ${build_dir}

cp -v ${download_dir}/${scons}.tar.gz .

tar -zxf ${scons}.tar.gz

pushd ${scons}

python setup.py install --prefix=${chaste_libs_dir}

popd

rm -rf ${scons}*
rm -vf ${download_dir}/${scons}.tar.gz

popd
