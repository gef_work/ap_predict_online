#!/bin/bash -e

if [ $# -ne 2 ]; then
  echo ""
  echo "  Use is 1build.sh <chaste libs dir> <processor count>"
  echo "   e.g. 1build.sh /home/me/chaste-libs/170106/ 8"
  echo ""

  exit  1
fi

chaste_libs_dir=$1
processor_count=$2

today=`date +%y%m%d`

chaste_today=chaste_${today}

git clone -b develop https://github.com/Chaste/Chaste.git ${chaste_today} 
# An additional `-c http.sslVerify=false` argument may be required!
pushd ${chaste_today}
git checkout fa6617cd23373091eb9d215714fe6ec411cdbaab
chaste_hash=`git rev-parse --short HEAD`
popd
chaste_full=${chaste_today}_${chaste_hash}
mv ${chaste_today} ${chaste_full}

appredict_today=appredict_${today}

git clone --recursive https://github.com/Chaste/ApPredict.git ${appredict_today}
pushd ${appredict_today}
appredict_hash=`git rev-parse --short HEAD`
popd
appredict_full=${appredict_today}_${appredict_hash}
mv ${appredict_today} ${appredict_full}

new_patch_file=default.py.patch.${today}
cp -v default.py.patch ${new_patch_file}
sed -i -- "s|<chaste-dir>|${chaste_full}|g" ${new_patch_file}
sed -i -- "s|<chaste-libs-dir>|${chaste_libs_dir}|g" ${new_patch_file}
patch -p0 < ${new_patch_file}

pushd ${chaste_full}
patch -p0 < ../SConsTools.py.patch
popd

ln -s ../../${appredict_full} ${chaste_full}/projects

export CHASTE_LIBS=${chaste_libs_dir}
export PATH=${CHASTE_LIBS}/bin:${PATH}
export PYTHONPATH=${CHASTE_LIBS}/lib/python2.7/site-packages:${CHASTE_LIBS}/lib64/python2.7/site-packages

echo ""
echo "  CHASTE_LIBS=${CHASTE_LIBS}"
echo "  PATH=${PATH}"
echo "  PYTHONPATH=${PYTHONPATH}"
echo ""
sleep 1

pushd ${chaste_full}

scons -j${processor_count} cl=1 b=GccOptNative exe=1 projects/${appredict_full}/apps/src

popd

ln -s ${appredict_full} ApPredict
ln -s ${chaste_full} Chaste
