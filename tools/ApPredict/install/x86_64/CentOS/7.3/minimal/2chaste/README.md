## Terminology.

_build_ here refers to compiling Chaste from source code.  

_runtest_ is to test to see if the compile was successful.  

_install_ is the process of transferring a subset of the compiled-from-source files into a
          consistent file structure.