#!/bin/bash -e

if [ $# -ne 3 ]; then
  echo ""
  echo "  Use is build_python.sh <build dir> <download dir> <chaste-libs dir>"
  echo "    e.g. build_python.sh ~/chaste-libs-151002/build ~/chaste-libs-151002/download ~/chaste-libs-151002"
  echo ""

  exit 1
fi

build_dir=$1
download_dir=$2
chaste_libs_dir=$3

_4Suite_XML=4Suite-XML-1.0.2
amara=Amara-1.2.0.2
html5lib=html5lib-0.999
lxml=lxml-3.4.4
rdflib=rdflib-4.2.1

pushd ${download_dir}

if [ -f "${amara}.tar.gz" ]; then
  echo "  ${amara}.tar.gz already downloaded"
else 
  wget https://pypi.python.org/packages/source/4/4Suite-XML/${_4Suite_XML}.tar.gz
  wget https://pypi.python.org/packages/source/A/Amara/${amara}.tar.gz
  wget https://pypi.python.org/packages/source/h/html5lib/${html5lib}.tar.gz
  wget https://pypi.python.org/packages/source/l/lxml/${lxml}.tar.gz
  wget https://pypi.python.org/packages/source/r/rdflib/${rdflib}.tar.gz
fi

popd

if [ ! -d "${chaste_libs_dir}/lib/python2.6/site-packages" ]; then
  mkdir -p ${chaste_libs_dir}/lib/python2.6/site-packages
fi
if [ ! -d "${chaste_libs_dir}/lib64/python2.6/site-packages" ]; then
  mkdir -p ${chaste_libs_dir}/lib64/python2.6/site-packages
fi
export PYTHONPATH=${chaste_libs_dir}/lib/python2.6/site-packages:${chaste_libs_dir}/lib64/python2.6/site-packages

pushd ${build_dir}

for python_package in {${_4Suite_XML},${amara},${lxml},${html5lib},${rdflib}}; do
  cp -v ${download_dir}/${python_package}.tar.gz .

  tar -zxf ${python_package}.tar.gz

  if [ "${_4Suite_XML}" == "${python_package}" ]; then
    # Overcome presence of version 'GIT_VERSION' in installed python-rtslib
    patch -p0 < ../scripts/Version.py.patch
  fi

  pushd ${python_package}

  python setup.py install --prefix=${chaste_libs_dir}

  popd

  rm -rf ${python_package}*
  rm -vf ${download_dir}/${python_package}.tar.gz
done

popd