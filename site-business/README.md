# Action Potential prediction -- Site Business

Site-specific business logic placing compound data into `business-manager-api` defined objects
which `business_manager` can subsequently process.

In this case `site-business` represents an example scenario of site business processing for
demonstration purposes.

## Installation :

  1. http://apportal.readthedocs.io/en/latest/installation/components/site-business/index.html
  1. ap_predict_online/docs/AP-Portal/RtD/_build/html/installation/components/site-business/index.html