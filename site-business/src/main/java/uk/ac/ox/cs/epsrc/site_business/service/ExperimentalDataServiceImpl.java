/*

  Original work: Copyright (c) 2015, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.site_business.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.site_business.SiteIdentifiers;
import uk.ac.ox.cs.nc3rs.business_manager.api.APIIdentifiers;
import uk.ac.ox.cs.nc3rs.business_manager.api.dao.SiteDAO;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.SiteDatabaseInteractionException;
import uk.ac.ox.cs.nc3rs.business_manager.api.service.ExperimentalDataService;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ExperimentalDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.experimental.DefaultExperimentalDataVO;

/**
 * Site-specific implementation of the experimental data retrieval processes.
 * 
 * @author geoff
 */
@Component(APIIdentifiers.COMPONENT_EXPERIMENTAL_DATA_SERVICE)
public class ExperimentalDataServiceImpl implements ExperimentalDataService {

  @Autowired @Qualifier(SiteIdentifiers.COMPONENT_SITE_DAO) 
  private SiteDAO siteDAO;

  private static final Log log = LogFactory.getLog(ExperimentalDataServiceImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.service.ExperimentalDataService#retrieveExperimentalData(java.lang.String, java.lang.String)
   */
  @Override
  public ExperimentalDataHolder retrieveExperimentalData(final String compoundIdentifier,
                                                         final String userId)
                                                         throws IllegalArgumentException,
                                                                InvalidValueException {
    log.debug("~retrieveExperimentalData() : [" + compoundIdentifier + "][" + userId + "]");

    ExperimentalDataHolder experimentalData = null;
    try {
      experimentalData = siteDAO.retrieveExperimentalData(compoundIdentifier,
                                                          userId);
    } catch (SiteDatabaseInteractionException e) {
      e.printStackTrace();
      final String errorMessage = "Error interacting with site db '" + e.getMessage() + "'";
      log.error("~retrieveExperimentalData() : " + errorMessage + "'.");
      final List<String> problems = new ArrayList<String>();
      problems.add(errorMessage);
      experimentalData = new DefaultExperimentalDataVO(null, problems);
    }

    return experimentalData;
  }
}