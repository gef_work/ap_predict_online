/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.site_business.value.object.datarecord.summary;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICRawDataSource;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.helper.ICDataHelper;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICRawDataSourceVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.QualityControlFailureVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.datarecord.summary.AbstractSummaryDataRecordVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICType;

/**
 * OUCS summary data record value object.
 * 
 * @author geoff
 */
public class CSSummaryDataRecordVO extends AbstractSummaryDataRecordVO {

  public static final String COLUMN_NAME_ASSAY = "ASSAY";
  public static final String COLUMN_NAME_COMPOUND_IDENTIFIER = "COMPOUND_IDENTIFIER";
  public static final String COLUMN_NAME_ION_CHANNEL = "ION_CHANNEL";
  public static final String TABLE_NAME_SUMMARY = "SUMMARY";

  private static final String columnNameAvgHillSlope = "SLOPE_AVG";
  private static final String columnNamePIC50Avg = "PIC50_AVG";
  private static final String columnNamePIC50Mod = "PIC50_MOD";
  private static final String columnNameCount = "N";

  private static final Log log = LogFactory.getLog(CSSummaryDataRecordVO.class);

  /**
   * Initialising constructor.
   * <p>
   * Note that as summary data doesn't have a surrogate primary key (or other unique identifier)
   * we are relying on super's unique id generation.
   * 
   * @param rawData Raw data record.
   * @throws InvalidValueException If an invalid value is encountered.
   * @throws IllegalArgumentException If required arguments are not supplied or if the argument
   *                                  validation fails.
   */
  public CSSummaryDataRecordVO(final Map<String, Object> rawData) throws IllegalArgumentException,
                                                                         InvalidValueException {
    // Summary data doesn't have a surrogate primary key.
    super(null, TABLE_NAME_SUMMARY, rawData);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.HillCoefficientDataHolder#eligibleHillCoefficient()
   */
  public BigDecimal eligibleHillCoefficient() {
    log.debug("~eligibleHillCoefficient() : Invoked.");
    return (BigDecimal) getRawDataValueByColumnName(columnNameAvgHillSlope);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ICDataHolder#eligibleICData()
   */
  @Override
  public List<ICData> eligibleICData() throws InvalidValueException {
    log.debug("~eligibleICData() : Invoked.");
    final List<ICRawDataSource> icRawDataSources = new ArrayList<ICRawDataSource>(1);
    icRawDataSources.add(new ICRawDataSourceVO(columnNamePIC50Avg, null, columnNamePIC50Mod,
                                               columnNameCount, null, null, ICType.PIC50));
    return ICDataHelper.eligibleICData(icRawDataSources, this);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.MarkedDataHolder#isMarkedInvalid()
   */
  @Override
  public QualityControlFailureVO isMarkedInvalid() throws InvalidValueException {
    return null;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.MarkedDataHolder#isMarkedInactive()
   */
  @Override
  public QualityControlFailureVO isMarkedInactive() throws InvalidValueException {
    return null;
  }
}