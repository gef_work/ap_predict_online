/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.site_business.value.object.datarecord.individual;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICRawDataSource;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.individual.MaxRespConcData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.QualityControlTest;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.helper.ICDataHelper;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.helper.MaxRespDataHelper;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.helper.QualityControlDataHelper;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICRawDataSourceVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.QualityControlFailureVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.datarecord.individual.AbstractIndividualDataRecordVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.maxrespconc.MaxRespConcRawDataSourceVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICType;

/**
 * OUCS individual data record value object.
 *
 * @author geoff
 */
public class CSIndividualDataRecordVO extends AbstractIndividualDataRecordVO {

  public static final String COLUMN_NAME_ASSAY = "ASSAY";
  public static final String COLUMN_NAME_COMPOUND_IDENTIFIER = "COMPOUND_IDENTIFIER";
  public static final String COLUMN_NAME_INDIVIDUAL_ID = "INDIVIDUAL_ID";
  public static final String COLUMN_NAME_ION_CHANNEL = "ION_CHANNEL";
  public static final String TABLE_NAME_INDIVIDUAL = "INDIVIDUAL";

  private static final String columnNameHighestConc = "HIGHEST_CONC";
  private static final String columnNameHillSlope = "HILL_SLOPE";
  public static final String COLUMN_NAME_IC20 = "IC20";
  public static final String COLUMN_NAME_IC20_UNIT = "IC20_UNIT";
  public static final String COLUMN_NAME_IC20_MOD = "IC20_MOD";
  public static final String COLUMN_NAME_IC20_HILL = "IC20_HILL";
  private static final String columnName_pIC50 = "PIC50";
  private static final String columnName_pIC50Mod = "PIC50_MOD";
  private static final String columnNameQCInactive = "INACTIVE";
  private static final String columnNameQCInvalid = "INVALID";
  private static final String columnNameResponse = "RESPONSE";

  private static final Log log = LogFactory.getLog(CSIndividualDataRecordVO.class);

  /**
   * Initialising constructor.
   * <p>
   * Note that as individual data doesn't have a surrogate primary key (or other unique identifier,
   * see list below) we are relying on super's unique id generation. This probably doesn't reflect
   * a real-world situation!
   * <ul>
   *   <li>The individual id is non-unique in individual table.</li>
   *   <li>
   *     It may be shared by many summary data records, i.e. xref'd individual data records are
   *     the ones which have provided input to the linking summary, so a summary id combo isn't
   *     feasible.
   *   </li>
   *   <li>Individual data records may exist without a parent summary record.</li>
   *   <li>
   *     Child screening/dose-response data may not exist for an individual data record, so a
   *     screening id combo isn't feasible.</li>
   * </ul>
   * 
   * @param rawData Raw data record.
   * @throws InvalidValueException If an invalid value is encountered.
   * @throws IllegalArgumentException If required arguments are not supplied or if the argument
   *                                  validation fails.
   */
  public CSIndividualDataRecordVO(final Map<String, Object> rawData)
                                  throws IllegalArgumentException, InvalidValueException {
    super(null, TABLE_NAME_INDIVIDUAL, rawData);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.HillCoefficientDataHolder#eligibleHillCoefficient()
   */
  public BigDecimal eligibleHillCoefficient() throws InvalidValueException {
    log.debug("~eligibleHillCoefficient() : Invoked.");
    BigDecimal eligibleHillSlope = null;
    try {
      Object provisionalEligibleHillSlope = null;
      try {
        provisionalEligibleHillSlope = getRawDataValueByColumnName(columnNameHillSlope);
        eligibleHillSlope = (BigDecimal) provisionalEligibleHillSlope; 
      } catch (ClassCastException e) {
        final String warnMessage = "Expecting a decimal number, but found '" + provisionalEligibleHillSlope + "' instead!";
        log.warn("~eligibleHillCoefficient() : " + warnMessage);
        throw new InvalidValueException(new Object[] { warnMessage });
      }
    } catch (IllegalArgumentException e) {
      // Programming error which should be resolved at installation time!
    }

    return eligibleHillSlope;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ICDataHolder#eligibleICData()
   */
  @Override
  public List<ICData> eligibleICData() throws InvalidValueException {
    log.debug("~eligibleICData() : Invoked.");
    final List<ICRawDataSource> icRawDataSources = new ArrayList<ICRawDataSource>();
    icRawDataSources.add(new ICRawDataSourceVO(columnName_pIC50, null, columnName_pIC50Mod, null,
                                               null, null, ICType.PIC50));
    icRawDataSources.add(new ICRawDataSourceVO(COLUMN_NAME_IC20, COLUMN_NAME_IC20_UNIT,
                                               COLUMN_NAME_IC20_MOD, null, null, null, ICType.IC20));
    return ICDataHelper.eligibleICData(icRawDataSources, this);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.MaxRespDataHolder#eligibleMaxRespData()
   */
  public List<MaxRespConcData> eligibleMaxRespData() throws InvalidValueException {
    log.debug("~eligibleMaxRespData() : Invoked.");
    final List<MaxRespConcRawDataSourceVO> dataSources = new ArrayList<MaxRespConcRawDataSourceVO>(1);
    dataSources.add(new MaxRespConcRawDataSourceVO(columnNameResponse, columnNameHighestConc,
                                                   Unit.uM));
    try {
      return MaxRespDataHelper.eligibleMaxRespData(dataSources, this);
    } catch (IllegalArgumentException e) {
      // Programming error which should be resolved at installation time!
    }
    return null;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.MarkedDataHolder#isMarkedInactive()
   */
  @Override
  public QualityControlFailureVO isMarkedInactive() throws InvalidValueException {
    log.debug("~isMarkedInactive() : Invoked.");

    // Create a collection of tests which will test the raw data for inactivity.
    final List<QualityControlTest> compoundIsInactiveTests = new ArrayList<QualityControlTest>(1);
    compoundIsInactiveTests.add(new QualityControlTest() {
      public boolean passed(final DataRecord dataRecord) throws InvalidValueException {
        Boolean inactive = null;
        try {
          Object provisionalInactive = null;
          try {
            provisionalInactive = dataRecord.getRawDataValueByColumnName(columnNameQCInactive);
            inactive = (Boolean) provisionalInactive;
          } catch (ClassCastException e) {
            final String warnMessage = "Expecting a boolean representation, but found '" + provisionalInactive + "' instead!";
            log.warn("~isMarkedInactive() : " + warnMessage);
            throw new InvalidValueException(new Object[] { warnMessage });
          }
        } catch (IllegalArgumentException e) {
          // Programming error which should be resolved at installation time!
        }

        // If inactivity is undefined or false then return true, i.e. test passes.
        // Otherwise consider it inactive and return false.
        return (inactive == null || !inactive) ? true : false;
      }
      public String nature() {
        return "Testing data property '" + columnNameQCInactive + "'";
      }
    });

    return QualityControlDataHelper.qcCheck(compoundIsInactiveTests, this);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.MarkedDataHolder#isMarkedInvalid()
   */
  @Override
  public QualityControlFailureVO isMarkedInvalid() throws InvalidValueException {
    log.debug("~isMarkedInvalid() : Invoked.");

    final List<QualityControlTest> invalidDataTests = new ArrayList<QualityControlTest>(1);
    invalidDataTests.add(new QualityControlTest() {
      public boolean passed(final DataRecord dataRecord) {
        final Boolean invalid = (Boolean) dataRecord.getRawDataValueByColumnName(columnNameQCInvalid);
        return (invalid == null || !invalid) ? true : false; 
      }
      public String nature() {
        return "Testing data property '" + columnNameQCInvalid + "'";
      }
    });
    return QualityControlDataHelper.qcCheck(invalidDataTests, this);
  }
}