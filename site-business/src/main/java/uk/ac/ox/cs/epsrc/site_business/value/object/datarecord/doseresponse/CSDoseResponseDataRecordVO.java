/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.site_business.value.object.datarecord.doseresponse;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.datarecord.doseresponse.AbstractDoseResponseDataRecordVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.doseresponse.DoseResponsePairVO;

/**
 * OUCS dose-response data record value object.
 *
 * @author geoff
 */
public class CSDoseResponseDataRecordVO extends AbstractDoseResponseDataRecordVO {

  public static final String COLUMN_NAME_SCREENING_ID = "SCREENING_ID";
  public static final String TABLE_NAME_SCREENING = "SCREENING";

  private static final String COLUMN_NAME_CONCENTRATION = "CONCENTRATION";
  private static final String COLUMN_NAME_RESPONSE = "RESPONSE";

  private static final Log log = LogFactory.getLog(CSDoseResponseDataRecordVO.class);

  /**
   * Initialising constructor.
   * 
   * @param rawData Raw data record.
   * @throws IllegalArgumentException If required arguments are not supplied or if the argument
   *                                  validation fails.
   * @throws InvalidValueException If an invalid value is encountered.
   */
  public CSDoseResponseDataRecordVO(final Map<String, Object> rawData)
                                    throws IllegalArgumentException, InvalidValueException {
    // Dose-response data doesn't have a surrogate primary key.
    super(null, TABLE_NAME_SCREENING, rawData);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.DoseResponseDataHolder#eligibleDoseResponseData()
   */
  public DoseResponsePairVO eligibleDoseResponseData() throws InvalidValueException {
    log.debug("~eligibleDoseResponseData() : Invoked.");
    DoseResponsePairVO doseResponsePair = null;

    Object provisionalConcentration = null;
    Object provisionalResponse = null;
    try {
      provisionalConcentration = getRawDataValueByColumnName(COLUMN_NAME_CONCENTRATION);
      provisionalResponse = getRawDataValueByColumnName(COLUMN_NAME_RESPONSE);
      try {
        final BigDecimal concentration = (BigDecimal) provisionalConcentration; 
        final BigDecimal response = (BigDecimal) provisionalResponse;
        if (concentration == null || response == null) {
          log.warn("~eligibleDoseResponseData() : Rejected as null encountered in concentration '" + concentration +
                   "', response '" + response + " in raw data '" + getRawData() + "'.");
        } else {
          doseResponsePair = new DoseResponsePairVO(concentration, response);
          log.debug("~eligibleDoseResponseData() : Adding '" + doseResponsePair.toString() + "'.");
        }
      } catch (ClassCastException e) {
        final String warnMessage = "Expecting a decimal representation, but found '" + 
                                    provisionalConcentration + "' and '" + provisionalResponse + "' instead!";
        log.warn("~eligibleDoseResponseData() : " + warnMessage);
        throw new InvalidValueException(new Object[] { warnMessage });
      }
    } catch (IllegalArgumentException e) {
      // Programming error which should be resolved at installation time!
    }

    return doseResponsePair;
  }
}