/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.site_business.value.object.datarecord.individual;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICRawDataSource;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.individual.MaxRespConcData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.helper.ICDataHelper;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICRawDataSourceVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.QualityControlFailureVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.datarecord.individual.AbstractIndividualDataRecordVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICType;

/**
 * OUCS Manual Patch data record (holding NaV1.5 data) value object.
 *
 * @author geoff
 */
public class CSManualPatchNaV15DataRecordVO extends AbstractIndividualDataRecordVO {

  private static final String columnNameNaV15 = "NAV1_5";

  private static final Log log = LogFactory.getLog(CSManualPatchNaV15DataRecordVO.class);

  /**
   * Initialising constructor.
   * 
   * @param tableName Name of data table.
   * @param rawData Raw data record.
   * @throws InvalidValueException If an invalid value is encountered.
   * @throws IllegalArgumentException If required arguments are not supplied or if the argument
   *                                  validation fails.
   */
  public CSManualPatchNaV15DataRecordVO(final String tableName, final Map<String, Object> rawData)
                                        throws IllegalArgumentException, InvalidValueException {
    super(null, tableName, rawData);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.HillCoefficientDataHolder#eligibleHillCoefficient()
   */
  public BigDecimal eligibleHillCoefficient() throws InvalidValueException {
    return null;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ICDataHolder#eligibleICData()
   */
  @Override
  public List<ICData> eligibleICData() throws InvalidValueException {
    log.debug("~eligibleICData() : Invoked.");
    final List<ICRawDataSource> icRawDataSources = new ArrayList<ICRawDataSource>(1);
    icRawDataSources.add(new ICRawDataSourceVO(columnNameNaV15, null, null, null, null, null,
                                               ICType.PIC50));
    return ICDataHelper.eligibleICData(icRawDataSources, this);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.MaxRespDataHolder#eligibleMaxRespData()
   */
  public List<MaxRespConcData> eligibleMaxRespData() throws InvalidValueException {
    return new ArrayList<MaxRespConcData>();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.MarkedDataHolder#isMarkedInvalid()
   */
  @Override
  public QualityControlFailureVO isMarkedInvalid() throws InvalidValueException {
    return null;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.MarkedDataHolder#isMarkedInactive()
   */
  @Override
  public QualityControlFailureVO isMarkedInactive() throws InvalidValueException {
    return null;
  }
}