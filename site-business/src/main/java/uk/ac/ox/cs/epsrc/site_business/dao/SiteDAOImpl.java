/*

  Original work: Copyright (c) 2015, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.site_business.dao;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.MapIterator;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.collections.map.MultiKeyMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import uk.ac.ox.cs.epsrc.site_business.SiteIdentifiers;
import uk.ac.ox.cs.epsrc.site_business.value.object.datarecord.doseresponse.CSDoseResponseDataRecordVO;
import uk.ac.ox.cs.epsrc.site_business.value.object.datarecord.individual.CSIndividualDataRecordVO;
import uk.ac.ox.cs.epsrc.site_business.value.object.datarecord.individual.CSManualPatchCaV12DataRecordVO;
import uk.ac.ox.cs.epsrc.site_business.value.object.datarecord.individual.CSManualPatchHERGDataRecordVO;
import uk.ac.ox.cs.epsrc.site_business.value.object.datarecord.individual.CSManualPatchNaV15DataRecordVO;
import uk.ac.ox.cs.epsrc.site_business.value.object.datarecord.summary.CSSummaryDataRecordVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.APIIdentifiers;
import uk.ac.ox.cs.nc3rs.business_manager.api.dao.SiteDAO;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.IndeterminableAssayException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.IndeterminableIonChannelException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.SiteDatabaseInteractionException;
import uk.ac.ox.cs.nc3rs.business_manager.api.util.AssayUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.util.LogUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ExperimentalDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.IrregularSiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.QSARDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.QSARDataHolder.DataPacket;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.transfer.experimental.ExperimentalResult;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ProblemVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.experimental.ConcQTPctChangeValuesVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.experimental.DefaultExperimentalDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.experimental.DefaultExperimentalResultVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.qsar.DefaultQSARDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening.AggregatedSiteDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening.IrregularSiteDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening.SiteDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProblemType;

/**
 * Implementation of the site data access object interface.
 * 
 * @author geoff
 */
@Repository(SiteIdentifiers.COMPONENT_SITE_DAO)
@Transactional(readOnly=true)
public class SiteDAOImpl implements SiteDAO {

  // experimental table and column names
  private static final String tableNameExperimental = "EXPERIMENTAL";
  private static final String tableNameExperimentalValues = "EXPERIMENTAL_VALUES";
  private static final String columnNameExperimentalId = "EXPERIMENTAL_ID";
  private static final String columnNamePacingFrequency = "PACING_FREQUENCY";
  private static final String columnNameCompoundConcentration = "CONCENTRATION";
  private static final String columnNameQTPctChangeMean = "QT_PCT_CHANGE_MEAN";
  private static final String columnNameQTPctChangeSEM = "QT_PCT_CHANGE_SEM";

  // QSAR table and column names
  private static final String tableNameQSAR = "QSAR";
  private static final String columnNameCAV1_2 = "CAV1_2";
  private static final String columnNameNAV1_5 = "NAV1_5";
  private static final String columnNameHERG = "HERG";
  // Manual Patch table name (shares same column names as QSAR!)
  private static final String tableNameManualPatch = "MANUAL_PATCH";

  private static final String equals = " = ?";
  private static final String select = "select * from ";
  private static final String where = " where ";

  /** SQL for querying summary data tables by compound identifier. */
  public static final String QUERY_SUMMARY_BY_COMPOUND_IDENTIFIER = select.
    concat(CSSummaryDataRecordVO.TABLE_NAME_SUMMARY).concat(where).
    concat(CSSummaryDataRecordVO.COLUMN_NAME_COMPOUND_IDENTIFIER).concat(equals);
  /** SQL for querying individual data tables by compound identifier (i.e. individual only). */
  public static final String QUERY_INDIVIDUAL_BY_COMPOUND_IDENTIFIER = select.
    concat(CSIndividualDataRecordVO.TABLE_NAME_INDIVIDUAL).concat(where).
    concat(CSIndividualDataRecordVO.COLUMN_NAME_COMPOUND_IDENTIFIER).concat(equals);
  /** SQL for querying individual data tables by individual identifier (i.e. xref from summary). */
  public static final String QUERY_INDIVIDUAL_BY_INDIVIDUAL_ID = select.
    concat(CSIndividualDataRecordVO.TABLE_NAME_INDIVIDUAL).concat(where).
    concat(CSIndividualDataRecordVO.COLUMN_NAME_INDIVIDUAL_ID).concat(equals);
  /** SQL for querying screening data tables. */
  public static final String QUERY_SCREENING_BY_SCREENING_ID = select.
    concat(CSDoseResponseDataRecordVO.TABLE_NAME_SCREENING).concat(where).
    concat(CSDoseResponseDataRecordVO.COLUMN_NAME_SCREENING_ID).concat(equals);

  private static final String queryExperimentalByCompoundIdentifier = select.
    concat(tableNameExperimental).concat(where).
    concat(CSSummaryDataRecordVO.COLUMN_NAME_COMPOUND_IDENTIFIER).concat(equals);
  private static final String queryExperimentalValuesByExperimentalId = select.
    concat(tableNameExperimentalValues).concat(where).
    concat(columnNameExperimentalId).concat(equals);

  private static final String queryQSARByCompoundIdentifier = select.
    concat(tableNameQSAR).concat(where).
    concat(CSSummaryDataRecordVO.COLUMN_NAME_COMPOUND_IDENTIFIER).concat(equals);

  protected static final String QUERY_MANUAL_PATCH_BY_COMPOUND_IDENTIFIER = select.
    concat(tableNameManualPatch).concat(where).
    concat(CSSummaryDataRecordVO.COLUMN_NAME_COMPOUND_IDENTIFIER).concat(equals);

  @Autowired @Qualifier(APIIdentifiers.COMPONENT_ASSAY_QSAR)
  private AssayVO qsarAssay;
  @Autowired @Qualifier(APIIdentifiers.COMPONENT_ASSAY_QUATTRO_FLIPR)
  private AssayVO quattroFLIPRAssay;
  @Autowired @Qualifier(APIIdentifiers.COMPONENT_ASSAY_BARRACUDA)
  private AssayVO barracudaAssay;
  @Autowired @Qualifier(APIIdentifiers.COMPONENT_ASSAY_PATCHXPRESS)
  private AssayVO patchXpressAssay;
  @Autowired @Qualifier(APIIdentifiers.COMPONENT_ASSAY_QPATCH)
  private AssayVO qpatchAssay;
  @Autowired @Qualifier(APIIdentifiers.COMPONENT_ASSAY_MANUAL_PATCH)
  private AssayVO manualPatchAssay;

  @Autowired @Qualifier(SiteIdentifiers.COMPONENT_TEMPLATE_SITE)
  private JdbcTemplate siteJdbcTemplate;

  private static final Log log = LogFactory.getLog(SiteDAOImpl.class);

  // determine the assay value object using the assay name.
  private AssayVO determineAssay(final String assayName) throws IndeterminableAssayException {

    AssayVO assay = null;
    if (AssayUtil.ASSAY_NAME_BARRACUDA.equalsIgnoreCase(assayName)) {
      assay = barracudaAssay;
    } else if (AssayUtil.ASSAY_NAME_QUATTRO_FLIPR.equalsIgnoreCase(assayName)) {
      assay = quattroFLIPRAssay;
    } else if (AssayUtil.ASSAY_NAME_PATCHXPRESS.equalsIgnoreCase(assayName)) {
      assay = patchXpressAssay;
    } else if (AssayUtil.ASSAY_NAME_QPATCH.equalsIgnoreCase(assayName)) {
      assay = qpatchAssay;
    } else {
      throw new IndeterminableAssayException(assayName);
    }
    return assay;
  }

  // determine the ion channel using the name provided.
  private IonChannel determineIonChannel(final String ionChannelName)
                                         throws IndeterminableIonChannelException {
    IonChannel ionChannel = null;
    try {
      ionChannel = IonChannel.valueOf(ionChannelName);
    } catch (final IllegalArgumentException e) {
      throw new IndeterminableIonChannelException(ionChannelName);
    }
    return ionChannel;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.dao.SiteDAO#retrieveAggregatedSiteData(java.lang.String, java.lang.String)
   */
  // TODO : Refactoring candidate? A lot of processing going on while holding a transaction open!
  @Override
  public AggregatedSiteDataVO retrieveAggregatedSiteData(final String compoundIdentifier,
                                                         final String userId)
                                                         throws InvalidValueException,
                                                                SiteDatabaseInteractionException {
    final String namedLogPrefix = "~retrieveAggregatedSiteData() : ";
    final String identifiedLogPrefix1 = namedLogPrefix.concat("[" + compoundIdentifier + "] : ");

    log.debug(identifiedLogPrefix1.concat("Invoked."));

    final List<SiteDataHolder> allSiteData = new ArrayList<SiteDataHolder>();
    final List<IrregularSiteDataHolder> allIrregularSiteData = new ArrayList<IrregularSiteDataHolder>(0);

    Map<String, Object> rawManualPatchRecord = new HashMap<String, Object>();
    try {
      rawManualPatchRecord.putAll(siteJdbcTemplate.queryForMap(QUERY_MANUAL_PATCH_BY_COMPOUND_IDENTIFIER,
                                                               new Object[] { compoundIdentifier }));
    } catch (EmptyResultDataAccessException e) {
      // no problem!
      log.debug(identifiedLogPrefix1.concat(" No Manual Patch records."));
    } catch (Exception e) {
      throw new SiteDatabaseInteractionException("Could not query Manual Patch successfully due to '" + e.getMessage() + "'");
    }

    if (!rawManualPatchRecord.isEmpty()) {
      final BigDecimal rawCAV1_2 = (BigDecimal) rawManualPatchRecord.get(columnNameCAV1_2);
      final BigDecimal rawNAV1_5 = (BigDecimal) rawManualPatchRecord.get(columnNameNAV1_5);
      final BigDecimal rawHERG = (BigDecimal) rawManualPatchRecord.get(columnNameHERG);

      if (rawCAV1_2 != null) {
        final Map<IndividualDataRecord, List<DoseResponseDataRecord>> cav12MPNonSummaryData = 
              new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();
        final IndividualDataRecord manualPatchCaV12IndividualData = 
              new CSManualPatchCaV12DataRecordVO(tableNameManualPatch, rawManualPatchRecord);
        cav12MPNonSummaryData.put(manualPatchCaV12IndividualData, null);
        allSiteData.add(new SiteDataVO(manualPatchAssay, IonChannel.CaV1_2, null,
                                       cav12MPNonSummaryData));
      }
      if (rawNAV1_5 != null) {
        final Map<IndividualDataRecord, List<DoseResponseDataRecord>> nav15MPNonSummaryData = 
              new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();
        final IndividualDataRecord manualPatchNaV15IndividualData = 
              new CSManualPatchNaV15DataRecordVO(tableNameManualPatch, rawManualPatchRecord);
        nav15MPNonSummaryData.put(manualPatchNaV15IndividualData, null);
        allSiteData.add(new SiteDataVO(manualPatchAssay, IonChannel.NaV1_5, null,
                                       nav15MPNonSummaryData));
      }
      if (rawHERG != null) {
        final Map<IndividualDataRecord, List<DoseResponseDataRecord>> hergMPNonSummaryData = 
              new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();
        final IndividualDataRecord manualPatchHERGIndividualData = 
              new CSManualPatchHERGDataRecordVO(tableNameManualPatch, rawManualPatchRecord);
        hergMPNonSummaryData.put(manualPatchHERGIndividualData, null);
        allSiteData.add(new SiteDataVO(manualPatchAssay, IonChannel.hERG, null,
                                       hergMPNonSummaryData));
      }
    }

    final List<Map<String, Object>> rawSummaryRecords = new ArrayList<Map<String, Object>>();
    try {
      rawSummaryRecords.addAll(siteJdbcTemplate.queryForList(QUERY_SUMMARY_BY_COMPOUND_IDENTIFIER,
                                                             new Object[] { compoundIdentifier }));
    } catch (final DataAccessException e) {
      log.error(identifiedLogPrefix1.concat(e.getMessage()));
      throw new SiteDatabaseInteractionException(e.getMessage());
    }

    final int rawSummaryRecordsCount = rawSummaryRecords.size();
    log.debug(identifiedLogPrefix1.concat(" Has '" + rawSummaryRecordsCount + "' summary records."));

    if (rawSummaryRecordsCount == 0) {
      // Look for individual data records only, i.e. no summary data.
      final List<Map<String, Object>> unsortedRawIndividualRecords = new ArrayList<Map<String, Object>>();
      try {
        unsortedRawIndividualRecords.addAll(siteJdbcTemplate.queryForList(QUERY_INDIVIDUAL_BY_COMPOUND_IDENTIFIER,
                                                                new Object[] { compoundIdentifier }));
      } catch (final DataAccessException e) {
        log.error(identifiedLogPrefix1.concat(e.getMessage()));
        throw new SiteDatabaseInteractionException(e.getMessage());
      }

      final int unsortedRawIndividualRecordsCount = unsortedRawIndividualRecords.size();
      log.debug(identifiedLogPrefix1.concat(" Has '" + unsortedRawIndividualRecordsCount + "' individual records by compound identifier."));

      if (unsortedRawIndividualRecordsCount > 0) {
        final MultiKeyMap relatedIndividualRecords = new MultiKeyMap();
        for (final Map<String, Object> unsortedRawIndividualRecord : unsortedRawIndividualRecords) {
          final String assayName = (String) unsortedRawIndividualRecord.get(CSIndividualDataRecordVO.COLUMN_NAME_ASSAY);
          final Integer individualId = (Integer) unsortedRawIndividualRecord.get(CSIndividualDataRecordVO.COLUMN_NAME_INDIVIDUAL_ID);
          final String ionChannelName = (String) unsortedRawIndividualRecord.get(CSIndividualDataRecordVO.COLUMN_NAME_ION_CHANNEL);

          final List<Map<String, Object>> relatedRawIndividualRecords;
          if (relatedIndividualRecords.containsKey(assayName, ionChannelName, individualId)) {
            relatedRawIndividualRecords = (List<Map<String, Object>>) relatedIndividualRecords.get(assayName, ionChannelName, individualId);
          } else {
            relatedRawIndividualRecords = new ArrayList<Map<String, Object>>();
          }
          relatedRawIndividualRecords.add(unsortedRawIndividualRecord);
          relatedIndividualRecords.put(assayName, ionChannelName, individualId, relatedRawIndividualRecords);
        }

        final MapIterator iterator = relatedIndividualRecords.mapIterator();
        while (iterator.hasNext()) {
          iterator.next();
          final MultiKey key = (MultiKey) iterator.getKey();
          final List<Map<String, Object>> rawIndividualRecords = (List<Map<String, Object>>) iterator.getValue();

          final String assayName = (String) key.getKey(0);
          final String ionChannelName = (String) key.getKey(1);
          final Integer individualId = (Integer) key.getKey(2);

          // determine the ion channel
          IonChannel ionChannel = null;
          try {
            ionChannel = determineIonChannel(ionChannelName);
          } catch (final IndeterminableIonChannelException e) {
            log.warn(identifiedLogPrefix1.concat("Exception '" + e.getMessage() + "'."));
            final List<ProblemVO> problems = new ArrayList<ProblemVO>();
            problems.add(new ProblemVO(ProblemType.INDETERMINABLE_ION_CHANNEL, e.getParams()));
            allIrregularSiteData.add(new IrregularSiteDataVO(null, ionChannel, null, null,
                                                             problems));
            continue;
          }

          // determine the assay
          AssayVO assay = null;
          try {
            assay = determineAssay(assayName);
          } catch (final IndeterminableAssayException e) {
            log.warn(identifiedLogPrefix1.concat("Exception '" + e.getMessage() + "'."));
            final List<ProblemVO> problems = new ArrayList<ProblemVO>();
            problems.add(new ProblemVO(ProblemType.INDETERMINABLE_ASSAY, e.getParams()));
            allIrregularSiteData.add(new IrregularSiteDataVO(assay, ionChannel, null, null,
                                                             problems));
            continue;
          }

          final String identifier = retrieveLogIdentifier(compoundIdentifier, assay, ionChannel,
                                                          individualId);
          final String identifiedLogPrefix = namedLogPrefix.concat(identifier);

          final int rawIndividualRecordsCount = rawIndividualRecords.size();
          log.debug(identifiedLogPrefix.concat(" Has '" + rawIndividualRecordsCount + "' individual records derived from compound identifier in individual."));

          processIndividual(allSiteData, compoundIdentifier, rawIndividualRecords, assay,
                            ionChannel, null, individualId, identifier, null);
        }
      }
    } else {
      // Summary data records found, look for individual records.
      for (final Map<String, Object> rawSummaryRecord : rawSummaryRecords) {
        final String assayName = (String) rawSummaryRecord.get(CSSummaryDataRecordVO.COLUMN_NAME_ASSAY);
        final String ionChannelName = (String) rawSummaryRecord.get(CSSummaryDataRecordVO.COLUMN_NAME_ION_CHANNEL);
        final Integer individualId = (Integer) rawSummaryRecord.get(CSIndividualDataRecordVO.COLUMN_NAME_INDIVIDUAL_ID);

        final SummaryDataRecord summaryData = new CSSummaryDataRecordVO(rawSummaryRecord);
        final String summaryId = summaryData.getId();

        log.debug(identifiedLogPrefix1.concat("Summary data '" + summaryData + "'."));

        // determine the ion channel
        IonChannel ionChannel = null;
        try {
          ionChannel = determineIonChannel(ionChannelName);
        } catch (final IndeterminableIonChannelException e) {
          log.warn(identifiedLogPrefix1.concat("Exception '" + e.getMessage() + "'."));
          final List<ProblemVO> problems = new ArrayList<ProblemVO>();
          problems.add(new ProblemVO(ProblemType.INDETERMINABLE_ION_CHANNEL, e.getParams()));
          allIrregularSiteData.add(new IrregularSiteDataVO(null, ionChannel, summaryData, null,
                                                           problems));
          continue;
        }

        // determine the assay
        AssayVO assay = null;
        try {
          assay = determineAssay(assayName);
        } catch (final IndeterminableAssayException e) {
          log.warn(identifiedLogPrefix1.concat("Exception '" + e.getMessage() + "'."));
          final List<ProblemVO> problems = new ArrayList<ProblemVO>();
          problems.add(new ProblemVO(ProblemType.INDETERMINABLE_ASSAY, e.getParams()));
          allIrregularSiteData.add(new IrregularSiteDataVO(assay, ionChannel, summaryData, null,
                                                           problems));
          continue;
        }

        final String identifier = retrieveLogIdentifier(compoundIdentifier, assay, ionChannel,
                                                        individualId);
        final String identifiedLogPrefix = namedLogPrefix.concat(identifier);

        final Map<IndividualDataRecord, List<DoseResponseDataRecord>> nonSummaryData =
              new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();
        final List<Map<String, Object>> rawIndividualRecords = new ArrayList<Map<String, Object>>();
        try {
          rawIndividualRecords.addAll(siteJdbcTemplate.queryForList(QUERY_INDIVIDUAL_BY_INDIVIDUAL_ID,
                                                                    new Object[] { individualId }));
        } catch (final DataAccessException e) {
          log.error(identifiedLogPrefix.concat(e.getMessage()));
          throw new SiteDatabaseInteractionException(e.getMessage());
        }

        final int rawIndividualRecordsCount = rawIndividualRecords.size();
        log.debug(identifiedLogPrefix.concat(" Has '" + rawIndividualRecordsCount + "' individual records derived compound identifier in summary."));

        if (rawIndividualRecordsCount > 0) {
          processIndividual(allSiteData, compoundIdentifier, rawIndividualRecords, assay,
                            ionChannel, summaryId, individualId, identifier, summaryData);
        } else {
          allSiteData.add(new SiteDataVO(assay, ionChannel, summaryData, nonSummaryData));
        }
      }
    }

    return new AggregatedSiteDataVO(allSiteData, allIrregularSiteData, null);
  }

  private String retrieveLogIdentifier(final String compoundIdentifier, final AssayVO assay,
                                       final IonChannel ionChannel, final Integer individualId) {
    final StringBuffer logIdentifier = new StringBuffer();
    logIdentifier.append(LogUtil.retrieveBasePrefix(compoundIdentifier, assay.getName(),
                                                    ionChannel.toString()));
    logIdentifier.append("[" + individualId + "]");
    return logIdentifier.toString();
  }

  private void processIndividual(final List<SiteDataHolder> allSiteData,
                                 final String compoundIdentifier,
                                 final List<Map<String, Object>> rawIndividualRecords,
                                 final AssayVO assay,
                                 final IonChannel ionChannel,
                                 final String summaryId,
                                 final Integer individualId,
                                 final String identifier,
                                 final SummaryDataRecord summaryData)
                                 throws IllegalArgumentException, InvalidValueException,
                                        SiteDatabaseInteractionException {
    final String identifiedLogPrefix = "~processIndividual() : ".concat(identifier).concat(" : ");

    // Data derived from summary data which dictates assay and ion channel (and summary data).
    final Map<IndividualDataRecord, List<DoseResponseDataRecord>> nonSummaryData =
          new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();

    for (final Map<String, Object> rawIndividualRecord : rawIndividualRecords) {
      final IndividualDataRecord individualDataRecord = new CSIndividualDataRecordVO(rawIndividualRecord);
      final Integer screeningId = (Integer) rawIndividualRecord.get(CSDoseResponseDataRecordVO.COLUMN_NAME_SCREENING_ID);

      if (screeningId != null) {
        List<Map<String, Object>> rawScreeningRecords = new ArrayList<Map<String, Object>>();
        try {
          rawScreeningRecords.addAll(siteJdbcTemplate.queryForList(QUERY_SCREENING_BY_SCREENING_ID,
                                                                   new Object[] { screeningId }));
        } catch (final DataAccessException e) {
          log.error(identifiedLogPrefix.concat(e.getMessage()));
          throw new SiteDatabaseInteractionException(e.getMessage());
        }
        final int rawScreeningRecordsCount = rawScreeningRecords.size();
        log.debug(identifiedLogPrefix + " Screening Id '" + screeningId + "' has '" + rawScreeningRecordsCount + "' records.");
        final List<DoseResponseDataRecord> doseResponseData = 
              new ArrayList<DoseResponseDataRecord>(rawScreeningRecordsCount);
        if (rawScreeningRecordsCount > 0) {
          for (final Map<String, Object> rawScreeningRecord : rawScreeningRecords) {
            doseResponseData.add(new CSDoseResponseDataRecordVO(rawScreeningRecord));
          }
        }
        nonSummaryData.put(individualDataRecord, doseResponseData);
      } else {
        log.debug(identifiedLogPrefix + " No screening records.");
        nonSummaryData.put(individualDataRecord, null);
      }
    }

    allSiteData.add(new SiteDataVO(assay, ionChannel, summaryData, nonSummaryData));
  }

  // row mapper for experimental concentration values
  private class ConcentrationValuesRowInfoMapper implements RowMapper<ConcQTPctChangeValuesVO> {
    public ConcQTPctChangeValuesVO mapRow(final ResultSet resultSet, final int rowNumber)
                                        throws SQLException {
      final ConcQTPctChangeValuesVO concentrationValues =
        new ConcQTPctChangeValuesVO(resultSet.getBigDecimal(columnNameCompoundConcentration),
                                    resultSet.getBigDecimal(columnNameQTPctChangeMean),
                                    resultSet.getBigDecimal(columnNameQTPctChangeSEM));
      return concentrationValues;
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.dao.SiteDAO#retrieveExperimentalData(java.lang.String, java.lang.String)
   */
  @Override
  public ExperimentalDataHolder retrieveExperimentalData(final String compoundIdentifier,
                                                         final String userId)
                                                         throws InvalidValueException,
                                                                SiteDatabaseInteractionException {
    final String namedLogPrefix = "~retrieveExperimentalData() : ";
    log.debug(namedLogPrefix.concat("Invoked for compound '" + compoundIdentifier + "'."));
    final String identifiedLogPrefix = namedLogPrefix + "Compound : '" + compoundIdentifier + "' : ";
    final List<Map<String, Object>> rawData = new ArrayList<Map<String, Object>>();
    try {
      rawData.addAll(siteJdbcTemplate.queryForList(queryExperimentalByCompoundIdentifier,
                                                   new Object[] { compoundIdentifier }));
    } catch (DataAccessException e) {
      log.error(identifiedLogPrefix.concat(e.getMessage()));
      e.printStackTrace();
      throw new SiteDatabaseInteractionException(e.getMessage());
    }

    ExperimentalDataHolder processedExperimentalData = null;
    if (rawData.isEmpty()) {
      log.debug(identifiedLogPrefix.concat("No experimental data available for compound '" + 
                compoundIdentifier + "'."));
      processedExperimentalData = new DefaultExperimentalDataVO(null, null);
    } else {
      // k: pacing frequency; v: experimental id
      final Map<BigDecimal, Integer> allFrequencyData = new HashMap<BigDecimal, Integer>();
      for (final Map<String, Object> eachRawData : rawData) {
        final Integer experimentalId = (Integer) eachRawData.get(columnNameExperimentalId);
        final BigDecimal pacingFrequency = (BigDecimal) eachRawData.get(columnNamePacingFrequency);

        allFrequencyData.put(pacingFrequency, experimentalId);
      }

      // now extract the per-pacing frequency experimental QT % change measurements.
      final Set<ExperimentalResult> experimentalData = new HashSet<ExperimentalResult>();
      for (final Map.Entry<BigDecimal, Integer> eachFrequencyData : allFrequencyData.entrySet()) {
        final BigDecimal pacingFrequency = eachFrequencyData.getKey();
        final Integer experimentalId = eachFrequencyData.getValue();

        final List<ConcQTPctChangeValuesVO> concentrationValues = new ArrayList<ConcQTPctChangeValuesVO>(); 
        try {
          concentrationValues.addAll(siteJdbcTemplate.query(queryExperimentalValuesByExperimentalId, 
                                                            new Object[] { experimentalId },
                                                            new ConcentrationValuesRowInfoMapper()));
        } catch (DataAccessException e) {
          log.error(identifiedLogPrefix.concat(e.getMessage()));
          e.printStackTrace();
          throw new SiteDatabaseInteractionException(e.getMessage());
        }
        experimentalData.add(new DefaultExperimentalResultVO(pacingFrequency,
                                                             concentrationValues));
      }
      processedExperimentalData = new DefaultExperimentalDataVO(experimentalData, null);
    }

    return processedExperimentalData;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.dao.SiteDAO#retrieveQSARData(java.lang.String, java.lang.String)
   */
  @Override
  public QSARDataHolder retrieveQSARData(final String compoundIdentifier,
                                         final String userId)
                                         throws InvalidValueException,
                                                SiteDatabaseInteractionException {
    final String namedLogPrefix = "~retrieveQSARData() : ";
    log.debug(namedLogPrefix.concat("Invoked for compound '" + compoundIdentifier + "'."));
    final String identifiedLogPrefix = namedLogPrefix.concat("Compound : '" + compoundIdentifier + "' : ");

    QSARDataHolder qsarData = null;

    Map<String, Object> rawQSARRecord = new HashMap<String, Object>();
    try {
      rawQSARRecord.putAll(siteJdbcTemplate.queryForMap(queryQSARByCompoundIdentifier,
                                                        new Object[] { compoundIdentifier }));
    } catch (EmptyResultDataAccessException e) {
      // no problem!
      log.debug(identifiedLogPrefix.concat(" No QSAR records."));
    } catch (Exception e) {
      throw new SiteDatabaseInteractionException("Could not query QSAR successfully due to '" + e.getMessage() + "'");
    }

    if (!rawQSARRecord.isEmpty()) {
      //final String rawCompoundIdentifier = (String) rawQSARRecord.get(CSSummaryDataRecordVO.COLUMN_NAME_COMPOUND_IDENTIFIER);
      final BigDecimal rawCAV1_2 = (BigDecimal) rawQSARRecord.get(columnNameCAV1_2);
      final BigDecimal rawNAV1_5 = (BigDecimal) rawQSARRecord.get(columnNameNAV1_5);
      final BigDecimal rawHERG = (BigDecimal) rawQSARRecord.get(columnNameHERG);
  
      final Set<DataPacket> dataPackets = new HashSet<DataPacket>();
      if (rawCAV1_2 != null) {
        dataPackets.add(new DataPacket(IonChannel.CaV1_2, rawCAV1_2));
      }
      if (rawNAV1_5 != null) {
        dataPackets.add(new DataPacket(IonChannel.NaV1_5, rawNAV1_5));
      }
      if (rawHERG != null) {
        dataPackets.add(new DataPacket(IonChannel.hERG, rawHERG));
      }
      qsarData = new DefaultQSARDataVO(qsarAssay, dataPackets, null);
      log.debug(identifiedLogPrefix.concat(" Retrieved : '" + qsarData.toString() + "'."));
    } else {
      qsarData = new DefaultQSARDataVO(qsarAssay, null, null);
    }
    
    return qsarData;
  }
}