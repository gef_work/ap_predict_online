/*

  Original work: Copyright (c) 2015, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.site_business.business;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.annotation.Header;

import uk.ac.ox.cs.epsrc.site_business.SiteIdentifiers;
import uk.ac.ox.cs.nc3rs.business_manager.api.APIIdentifiers;
import uk.ac.ox.cs.nc3rs.business_manager.api.dao.SiteDAO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ExperimentalDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.QSARDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.experimental.DefaultExperimentalDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.qsar.DefaultQSARDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening.AggregatedSiteDataVO;

/**
 * Component which loads the site's experimental and screening data into a generic format for onward
 * processing.
 * 
 * @author geoff
 */
// bean referenced in multiple application contexts
public class SiteDataLoader {

  @Autowired @Qualifier(APIIdentifiers.COMPONENT_ASSAY_QSAR)
  private AssayVO qsarAssay;

  @Autowired @Qualifier(SiteIdentifiers.COMPONENT_SITE_DAO)
  private SiteDAO siteDAO;

  private static final Log log = LogFactory.getLog(SiteDataLoader.class);

  /**
   * Load the experimental data for the compound from the site's databases.
   * 
   * @param compoundIdentifier Compound identifier.
   * @return Experimental data holder.
   */
  // method name specified in appCtx.proc.site.experimental.xml
  public ExperimentalDataHolder loadExperimentalData(final @Header(required=true,
                                                                   value=APIIdentifiers.SI_HDR_COMPOUND_IDENTIFIER)
                                                           String compoundIdentifier,
                                                     final @Header(required=false,
                                                                   value=APIIdentifiers.SI_HDR_USER_ID)
                                                           String userId) {
    log.debug("~loadExperimentalData() : [" + compoundIdentifier + "][" + userId + "]");

    ExperimentalDataHolder experimentalData;
    try {
      experimentalData = siteDAO.retrieveExperimentalData(compoundIdentifier,
                                                          userId);
    } catch (Exception e) {
      final List<String> problems = new ArrayList<String>();
      problems.add(e.getMessage());
      experimentalData = new DefaultExperimentalDataVO(null, problems);
    }

    return experimentalData;
  }

  // method name specified in appCtx.proc.site.QSAR.xml
  public QSARDataHolder loadQSARData(final @Header(required=true,
                                                   value=APIIdentifiers.SI_HDR_COMPOUND_IDENTIFIER)
                                           String compoundIdentifier,
                                     final @Header(required=false,
                                                   value=APIIdentifiers.SI_HDR_USER_ID)
                                           String userId) {
    log.debug("~loadQSARData() : [" + compoundIdentifier + "][" + userId + "]");

    QSARDataHolder qsarData;
    try {
      qsarData = siteDAO.retrieveQSARData(compoundIdentifier, userId);
    } catch (Exception e) {
      final List<String> problems = new ArrayList<String>();
      problems.add(e.getMessage());
      qsarData = new DefaultQSARDataVO(qsarAssay, null, problems);
    }

    return qsarData;
  }

  /**
   * Read in the site's data for onward processing.
   * 
   * @param compoundIdentifier Compound name.
   * @return Aggregated site data for the compound.
   */
  // method name specified in appCtx.proc.site.screening.xml
  public AggregatedSiteDataVO loadScreeningData(final @Header(required=true,
                                                              value=APIIdentifiers.SI_HDR_COMPOUND_IDENTIFIER)
                                                      String compoundIdentifier,
                                                final @Header(required=false,
                                                              value=APIIdentifiers.SI_HDR_USER_ID)
                                                      String userId) {
    log.debug("~loadScreeningData() : [" + compoundIdentifier + "][" + userId + "]");

    AggregatedSiteDataVO aggregatedSiteData = null;
    try {
      aggregatedSiteData = siteDAO.retrieveAggregatedSiteData(compoundIdentifier,
                                                              userId);
      log.debug("~loadScreeningData() : Retrieved '" + aggregatedSiteData.getSiteDataHolders().size() + "' site data objects for '" + compoundIdentifier + "'.");
      log.debug("~loadScreeningData() : Retrieved '" + aggregatedSiteData.getIrregularSiteDataHolders().size() + "' irregular site data objects for '" + compoundIdentifier + "'.");
    } catch (Exception e) {
      final List<String> problems = new ArrayList<String>();
      problems.add(e.getMessage());
      aggregatedSiteData = new AggregatedSiteDataVO(problems);
    }

    return aggregatedSiteData;
  }
}