/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.site_business.dao;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.site_business.dao.SiteDAOImpl;
import uk.ac.ox.cs.nc3rs.business_manager.api.dao.SiteDAO;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.SiteDatabaseInteractionException;

/**
 * Unit test SiteDAO implementation.
 * 
 * @author geoff
 */
public class SiteDAOTest {

  private IMocksControl mocksControl;
  private JdbcTemplate mockJdbcTemplate;
  private SiteDAO siteDAO;

  private static String dummyUserId = null;

  @Before
  public void setUp() {
    siteDAO = new SiteDAOImpl();

    mocksControl = createStrictControl();

    mockJdbcTemplate = mocksControl.createMock(JdbcTemplate.class);
    ReflectionTestUtils.setField(siteDAO, "siteJdbcTemplate", mockJdbcTemplate);
  }

  @SuppressWarnings("serial")
  @Test(expected=SiteDatabaseInteractionException.class)
  public void testCompound1() throws SiteDatabaseInteractionException,
                                     InvalidValueException {
    final String compound1 = "cmpd1";
    final Object[] params = new Object[] { compound1 };
    expect(mockJdbcTemplate.queryForMap(SiteDAOImpl.QUERY_MANUAL_PATCH_BY_COMPOUND_IDENTIFIER,
                                        params))
          .andThrow(new DataAccessException("FAIL!!") {});

    mocksControl.replay();

    siteDAO.retrieveAggregatedSiteData(compound1, dummyUserId);
  }
}