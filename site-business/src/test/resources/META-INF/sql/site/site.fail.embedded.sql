---
--- Compound data - Summary records.
---

drop table SUMMARY if exists;
create table SUMMARY (
  COMPOUND_IDENTIFIER varchar(5) not null,
  ASSAY               varchar(20) not null,
  ION_CHANNEL         varchar(10) not null,
  INDIVIDUAL_ID       integer,
  N                   integer not null,
  PIC50_AVG           numeric(6,3),
  PIC50_MOD           char(1),
  SLOPE_AVG           numeric(6,3)
);

insert into SUMMARY (COMPOUND_IDENTIFIER, ASSAY, ION_CHANNEL, INDIVIDUAL_ID, N, PIC50_AVG, PIC50_MOD)
values ('cmpd1', 'Quattro_FLIPRd', 'CaV1_2', 1, 2, 4.5, '=');
insert into SUMMARY (COMPOUND_IDENTIFIER, ASSAY, ION_CHANNEL, INDIVIDUAL_ID, N, PIC50_AVG, PIC50_MOD)
values ('cmpd1', 'Quattro_FLIPR', 'NaV1_5a', 2, 2, 4.3, '<');

---
--- Compound data - Individual records.
---


drop table INDIVIDUAL if exists;
create table INDIVIDUAL (
  INDIVIDUAL_ID   integer not null,
  /* Screening id points to dose-response data if there is some */
  SCREENING_ID    integer,
  IC20            numeric(9,3),
  IC20_UNIT       char(2),
  IC20_MOD        char(1),
  IC20_HILL       numeric(6,3),
  PIC50           numeric(6,3),
  PIC50_MOD       char(1),
  HIGHEST_CONC    numeric(6,3),
  RESPONSE        numeric(6,3),
  HILL_SLOPE      numeric(6,3),
  INACTIVE        boolean default false,
  INVALID         boolean default false,
  /* Individual-only compounds */
  COMPOUND_IDENTIFIER varchar(7),
  ASSAY               varchar(20),
  ION_CHANNEL         varchar(10)
);

insert into INDIVIDUAL (INDIVIDUAL_ID, SCREENING_ID, PIC50, PIC50_MOD, HIGHEST_CONC, RESPONSE, INACTIVE, INVALID)
values (1, 100, 4.4, '=', 30.0, 3.3, 0, 0);
insert into INDIVIDUAL (INDIVIDUAL_ID, SCREENING_ID, PIC50, PIC50_MOD, HIGHEST_CONC, RESPONSE, INACTIVE, INVALID)
values (1, 101, 4.6, '', 30.0, 3.4, 0, 0);
insert into INDIVIDUAL (INDIVIDUAL_ID, SCREENING_ID, PIC50, PIC50_MOD, HIGHEST_CONC, RESPONSE, INACTIVE, INVALID)
values (2, 102, 4.1, '>', 30.0, 3.3, 0, 0);
insert into INDIVIDUAL (INDIVIDUAL_ID, SCREENING_ID, PIC50, PIC50_MOD, HIGHEST_CONC, RESPONSE, INACTIVE, INVALID)
values (2, 103, 4.3, '', 30.0, 3.4, 0, 0);

---
--- Screening data - this is dose-response point data.
---

drop table SCREENING if exists;
create table SCREENING (
  SCREENING_ID    integer not null,
  FREQUENCY       numeric(6,3),
  CONCENTRATION   numeric(6,3),
  RESPONSE        numeric(6,3),
  PCT_CHANGE_MEAN numeric(6,3),
  PCT_CHANGE_SEM  numeric(6,3)
);

insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (100, 0.5, 0.1, 0.005, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (100, 0.5, 0.3, 0.05, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (100, 0.5, 1.0, 0.5, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (100, 0.5, 3.0, 1.5, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (100, 0.5, 10.0, 4.5, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (100, 0.5, 30.0, 6.7, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (101, 0.5, 0.1, 0.005, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (101, 0.5, 0.3, 0.05, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (101, 0.5, 1.0, 0.5, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (101, 0.5, 3.0, 1.5, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (101, 0.5, 10.0, 4.5, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (101, 0.5, 30.0, 6.7, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (102, 0.5, 0.1, 0.005, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (102, 0.5, 0.3, 0.05, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (102, 0.5, 1.0, 0.5, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (102, 0.5, 3.0, 1.5, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (102, 0.5, 10.0, 4.5, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (102, 0.5, 30.0, 6.7, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (103, 0.5, 0.1, 0.005, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (103, 0.5, 0.3, 0.05, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (103, 0.5, 1.0, 0.5, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (103, 0.5, 3.0, 1.5, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (103, 0.5, 10.0, 4.5, 3.4, 1.2);
insert into SCREENING (SCREENING_ID, FREQUENCY, CONCENTRATION, RESPONSE, PCT_CHANGE_MEAN, PCT_CHANGE_SEM)
values (103, 0.5, 30.0, 6.7, 3.4, 1.2);

---
--- Experimental data
---

drop table EXPERIMENTAL if exists;
create table EXPERIMENTAL (
  COMPOUND_IDENTIFIER varchar(5) not null,
  EXPERIMENTAL_ID     integer not null,
  PACING_FREQUENCY    numeric(6,3)
);

insert into EXPERIMENTAL (COMPOUND_IDENTIFIER, EXPERIMENTAL_ID, PACING_FREQUENCY)
values ('cmpd1', 1, 0.5);

drop table EXPERIMENTAL_VALUES if exists;
create table EXPERIMENTAL_VALUES (
  EXPERIMENTAL_ID    integer not null,
  CONCENTRATION      numeric(6,3),
  QT_PCT_CHANGE_MEAN numeric(6,3),
  QT_PCT_CHANGE_SEM  numeric(6,3)
);

insert into EXPERIMENTAL_VALUES (EXPERIMENTAL_ID, CONCENTRATION, QT_PCT_CHANGE_MEAN, QT_PCT_CHANGE_SEM)
values (1, 0.0, null, null);
insert into EXPERIMENTAL_VALUES (EXPERIMENTAL_ID, CONCENTRATION, QT_PCT_CHANGE_MEAN, QT_PCT_CHANGE_SEM)
values (1, 0.3, 2.2, 1.4);
insert into EXPERIMENTAL_VALUES (EXPERIMENTAL_ID, CONCENTRATION, QT_PCT_CHANGE_MEAN, QT_PCT_CHANGE_SEM)
values (1, 1.0, 4.2, 1.2);
insert into EXPERIMENTAL_VALUES (EXPERIMENTAL_ID, CONCENTRATION, QT_PCT_CHANGE_MEAN, QT_PCT_CHANGE_SEM)
values (1, 3.0, 4.8, 1.6);
insert into EXPERIMENTAL_VALUES (EXPERIMENTAL_ID, CONCENTRATION, QT_PCT_CHANGE_MEAN, QT_PCT_CHANGE_SEM)
values (1, 10.0, 6.7, 0.9);

---
--- QSAR data
---

drop table QSAR if exists;
create table QSAR (
  COMPOUND_IDENTIFIER varchar(5) not null,
  CAV1_2              numeric(6,3),
  NAV1_5              numeric(6,3),
  HERG                numeric(6,3)
);

insert into QSAR (COMPOUND_IDENTIFIER, CAV1_2, NAV1_5, HERG)
values ('cmpd1', 4.1, 4.2, 4.6);
insert into QSAR (COMPOUND_IDENTIFIER, CAV1_2, NAV1_5, HERG)
values ('cmpd2', 3.0, 4.6, 5.2);

---
--- Manual Patch data
---

drop table MANUAL_PATCH if exists;
create table MANUAL_PATCH (
  COMPOUND_IDENTIFIER varchar(5) not null,
  CAV1_2              numeric(6,3),
  NAV1_5              numeric(6,3),
  HERG                numeric(6,3)
);

insert into MANUAL_PATCH (COMPOUND_IDENTIFIER, CAV1_2, NAV1_5, HERG)
values ('cmpd1', 4.1, 4.2, 4.6);
insert into MANUAL_PATCH (COMPOUND_IDENTIFIER, CAV1_2, NAV1_5, HERG)
values ('cmpd2', 3.0, 4.6, 5.2);
insert into MANUAL_PATCH (COMPOUND_IDENTIFIER, CAV1_2, NAV1_5, HERG)
values ('cmpd3', 4.1, 4.2, 4.6);