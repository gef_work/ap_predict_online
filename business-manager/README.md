# Action Potential prediction -- Business Manager

Generic business processing of `business-manager-api` derived objects containing data from 
`site-business`.

## Installation

Please see either of the following :

  1. http://apportal.readthedocs.io/en/latest/installation/components/business-manager/index.html
  1. ap_predict_online/docs/AP-Portal/RtD/_build/html/installation/components/business-manager/index.html