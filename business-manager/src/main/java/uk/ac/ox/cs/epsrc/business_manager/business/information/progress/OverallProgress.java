/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.information.progress;

import org.apache.commons.lang3.StringUtils;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Overall progress detail.
 *
 * @author geoff
 */
public class OverallProgress extends AbstractProgress {

  private static final long serialVersionUID = -5529193571647987087L;

  private final boolean terminating;
  private final String compoundIdentifier;

  public static class Builder extends AbstractProgress.Builder {
    // Optional parameters
    private String compoundIdentifier = null;
    // Default is non-terminating progress.
    private boolean terminating = false;

    /**
     * Initialising constructor with minimum data. Default is non-terminating progress.
     * 
     * @param level Progress level.
     * @param text Progress text.
     * @param simulationId Simulation identifier.
     * @throws IllegalArgumentException See {@code super} constructor.
     */
    public Builder(final InformationLevel level, final String text, final Long simulationId)
                   throws IllegalArgumentException {
      super(level, text, simulationId);
    }

    /**
     * Initialising constructor with minimum data and optional terminating flag.
     * 
     * @param level Progress level.
     * @param text Progress text.
     * @param terminating {@code true} if this is the last progress of its kind (e.g. last Overall).
     * @param simulationId Simulation identifier.
     * @throws IllegalArgumentException See {@code super} constructor.
     */
    public Builder(final InformationLevel level, final String text, final boolean terminating,
                   final Long simulationId) throws IllegalArgumentException {
      super(level, text, simulationId);
      this.terminating = terminating;
    }

    /**
     * Initialising constructor with optional args.
     * 
     * @param level Progress level.
     * @param text Progress text.
     * @param args Progress args.
     * @param simulationId Simulation identifier.
     * @throws IllegalArgumentException See {@code super} constructor.
     */
    public Builder(final InformationLevel level, final String text, final String[] args,
                   final Long simulationId) {
      super(level, text, args, simulationId);
    }

    /**
     * Initialising constructor with optional args and terminating flag.
     * 
     * @param level Progress level.
     * @param text Progress text.
     * @param args Progress args.
     * @param terminating {@code true} if this is the last progress of its kind (e.g. last Overall).
     * @param simulationId Simulation identifier.
     * @throws IllegalArgumentException See {@code super} constructor.
     */
    public Builder(final InformationLevel level, final String text, final String[] args,
                   final boolean terminating, final Long simulationId) {
      super(level, text, args, simulationId);
      this.terminating = terminating;
    }


    /**
     * Used when creating a {@code OverallProgress} for the first time for a simulation.
     * 
     * @param compoundIdentifier Compound identifier.
     * @return Builder.
     * @throws IllegalArgumentException If {@link #compoundIdentifier} is blank.
     */
    public Builder onCreation(final String compoundIdentifier)
                              throws IllegalArgumentException {
      if (StringUtils.isBlank(compoundIdentifier)) {
        throw new IllegalArgumentException("A compound identifier must be provided on overall progress creation.");
      }
      this.compoundIdentifier = compoundIdentifier;
      super.setOnCreation(true);
      return this;
    }

    /**
     * Instruct the builder to build!
     * 
     * @return Newly built progress data.
     */
    public OverallProgress build() {
      return new OverallProgress(this);
    }
  }

  /**
   * @param builder
   */
  private OverallProgress(final Builder builder) {
    super(builder);
    compoundIdentifier = builder.compoundIdentifier;
    terminating = builder.terminating;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "OverallProgress [terminating=" + terminating
        + ", compoundIdentifier=" + compoundIdentifier + ", toString()="
        + super.toString() + "]";
  }

  /**
   * Retrieve the compound identifier.
   * 
   * @return The compound identifier, or {@code null} if not assigned.
   */
  public String getCompoundIdentifier() {
    return compoundIdentifier;
  }

  /**
   * Retrieve an indicator of whether the progress is an indicator that it is the last progress.
   * 
   * @return {@code true} if this is the last progress, otherwise {@code false}.
   */
  public boolean isTerminating() {
    return terminating;
  }
}