/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.dose_response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.DoseResponseData;
import uk.ac.ox.cs.epsrc.business_manager.value.object.doseresponse.DoseResponseFittingResultVO;
import uk.ac.ox.cs.epsrc.fdr_manager.ws._1.FDRRunRequest;
import uk.ac.ox.cs.epsrc.fdr_manager.ws._1.FDRRunResponse;
import uk.ac.ox.cs.epsrc.fdr_manager.ws._1.ObjectFactory;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingResult;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.doseresponse.DoseResponsePairVO;

/**
 * Dose-Response fitting run utility class.
 *
 * @author geoff
 */
public class FDRRunUtil {

  private static final ObjectFactory fdrObjectFactory = new ObjectFactory();

  private static final Log log = LogFactory.getLog(FDRRunUtil.class);

  /**
   * Create a JAXB FDRRunRequest with only the concentrations and responses assigned, i.e.
   * no fitting type (e.g. IC50_ONLY) or rounding or Hill min. or max.
   * 
   * @param doseResponseData Dose-response data to place into FDR request.
   * @return JAXB FDR request.
   */
  public static FDRRunRequest createFDRRunRequest(final DoseResponseData doseResponseData) {
    log.debug("~createFDRRunRequest() : Invoked.");

    final FDRRunRequest fdrRunRequest = fdrObjectFactory.createFDRRunRequest();
    final List<String> concentrations = new ArrayList<String>();
    final List<String> responses = new ArrayList<String>();
    for (final DoseResponsePairVO doseResponsePair : doseResponseData.doseResponsePoints()) {
      concentrations.add(doseResponsePair.getDose().toPlainString());
      responses.add(doseResponsePair.getResponse().toPlainString());
    }
    fdrRunRequest.setConcentrations(StringUtils.join(concentrations, ","));
    fdrRunRequest.setInhibitions(StringUtils.join(responses, ","));

    return fdrRunRequest;
  }

  /**
   * Extract the data from WS JAXB dose response fitting class and place into a value object.
   * 
   * @param fdrRunResponse WS JAXB dose response fitting response.
   * @return Fitted result value object (or null if invalid data encountered).
   */
  public static DoseResponseFittingResult dataExtractFDRRunResponse(final FDRRunResponse fdrRunResponse) {
    log.debug("~dataExtractFDRRunResponse() : Invoked.");
    final String responseIC50 = fdrRunResponse.getIC50();
    final String responseHillCoefficient = fdrRunResponse.getHillCoefficient();
    final String responseError = fdrRunResponse.getError();

    BigDecimal ic50 = null;
    if (responseIC50 != null) {
      ic50 = new BigDecimal(responseIC50);
    }
    BigDecimal hillCoefficient = null;
    if (responseHillCoefficient != null) {
      hillCoefficient = new BigDecimal(responseHillCoefficient);
    }
    final String error = responseError;

    DoseResponseFittingResult doseResponseFittingResult = null;
    try {
      doseResponseFittingResult = new DoseResponseFittingResultVO(ic50, hillCoefficient, error);
      log.debug("~dataExtractFDRRunResponse() : Returned '" + doseResponseFittingResult.toString() + "'.");
    } catch (InvalidValueException e) {
      log.warn("~dataExtractFDRRunResponse() : Dose-response fitting response contained invalid value(s) in : IC50 '" + responseIC50 + "', Hill '" + responseHillCoefficient + "', Error '" + responseError + "'.");
    }

    return doseResponseFittingResult; 
  }
}
