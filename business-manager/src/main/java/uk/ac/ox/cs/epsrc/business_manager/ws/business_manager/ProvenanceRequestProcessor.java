/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.business_manager;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager;
import uk.ac.ox.cs.epsrc.business_manager.service.SimulationService;
import uk.ac.ox.cs.epsrc.business_manager.value.object.InformationVO;
import uk.ac.ox.cs.epsrc.business_manager.ws.business_manager.request_validator.ProvenanceValidatorChain;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ObjectFactory;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProvenanceRecordStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProvenanceRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProvenanceResponse;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidCompoundIdentifierException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidSimulationIdException;

/**
 * Provenance request processing.
 *
 * @author geoff
 */
public class ProvenanceRequestProcessor {

  public enum REQUEST_BY {
    SIMULATION_ID
  }

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_INFORMATION_MANAGER)
  private InformationManager informationManager;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE)
  private SimulationService simulationService;

  private ProvenanceValidatorChain validatorChain;

  // Makes web service JAXB objects available.
  private static final ObjectFactory objectFactory = new ObjectFactory();

  private static final Log log = LogFactory.getLog(ProvenanceRequestProcessor.class);

  /** Default constructor */
  protected ProvenanceRequestProcessor() {}

  /**
   * Initialising constructor.
   * 
   * @param provenanceValidatorChain Validator chain to handle validation of incoming request.
   */
  public ProvenanceRequestProcessor(final ProvenanceValidatorChain provenanceValidatorChain) {
    validatorChain = provenanceValidatorChain; 
  }

  /**
   * Process a provenance request.
   * 
   * @param provenanceRequest Incoming provenance request.
   * @return Corresponding response.
   * @throws InvalidSimulationIdException If an invalid simulation id is encountered.
   * @throws InvalidCompoundIdentifierException If an invalid compound name is encountered.
   */
  public ProvenanceResponse processProvenanceRequest(final ProvenanceRequest provenanceRequest)
                                                     throws InvalidCompoundIdentifierException,
                                                            InvalidSimulationIdException {
    final String logPrefix = "~processProvenanceRequest() : ";
    log.debug(logPrefix.concat("Invoked."));

    final List<Long> simulationIds = provenanceRequest.getSimulationIds();
    final boolean hasSimulationIds = (simulationIds != null && !simulationIds.isEmpty());

    REQUEST_BY requestBy = null;
    final List<Object> identifiers = new ArrayList<Object>();
    if (hasSimulationIds) {
      log.debug(logPrefix.concat("Requested provenance by simulation id."));
      requestBy = REQUEST_BY.SIMULATION_ID;
      identifiers.addAll(simulationIds);
    } else {
      final String warn = "No simulation ids used in provenance request.";
      log.warn(logPrefix.concat(warn));
    }

    validatorChain.provenanceValidation(provenanceRequest, requestBy);

    final ProvenanceResponse provenanceResponse = objectFactory.createProvenanceResponse();

    if (requestBy != null) {
      switch (requestBy) {
        case SIMULATION_ID :
          for (final Object identifier : identifiers) {
            long simulationId;
            try {
              simulationId = Long.valueOf(identifier.toString());
            } catch (NumberFormatException e) {
              log.error(logPrefix.concat("Inappropriate simulation id value of '" + identifier + "' received!"));
              continue;
            }
            final String identifiedLogPrefix = logPrefix.concat("[") + simulationId + "] : ";
            log.debug(identifiedLogPrefix.concat("Retrieving provenance from information service."));
            final Long simulationIdLong = Long.valueOf(simulationId);
            final InformationVO provenanceData = informationManager.retrieveProvenance(simulationIdLong);
            final ProvenanceRecordStructure provenanceRecordStructure = objectFactory.createProvenanceRecordStructure();

            provenanceRecordStructure.setSimulationId(simulationIdLong);

            final Simulation simulation = simulationService.findBySimulationId(simulationId);

            if (provenanceData != null && simulation != null) {
              log.debug(identifiedLogPrefix.concat("Has provenance data."));
              provenanceRecordStructure.setProvenance(provenanceData.getInformation());
            }

            provenanceResponse.getProvenanceRecordStructure().add(provenanceRecordStructure);
          }
          break;
        default :
          final String errorMessage = "Unrecognised provenance request selector '" + requestBy + "'.";
          log.error("~processProvenanceRequest() : ".concat(errorMessage));
          throw new UnsupportedOperationException(errorMessage);
      }
    }

    return provenanceResponse;
  }
}
