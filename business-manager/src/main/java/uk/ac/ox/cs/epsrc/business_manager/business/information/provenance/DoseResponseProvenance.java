/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.information.provenance;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Provenance for dose-response information.
 *
 * @author geoff
 */
public class DoseResponseProvenance extends AbstractProvenance {

  private static final long serialVersionUID = -5949349321558094654L;

  private final String individualDataId;
  private final String doseResponseDataId;
  private final Map<String, Object> rawData = new HashMap<String, Object>();
  private final String dose_in_uM;
  private final String response;

  public static class Builder extends AbstractProvenance.Builder {
    // Optional parameters
    private String individualDataId = null;
    private String doseResponseDataId = null;
    private Map<String, Object> rawData = new HashMap<String, Object>();
    private String dose_in_uM = null;
    private String response = null;


    /**
     * Initialising constructor with minimum data.
     * 
     * @param level Provenance level.
     * @param text Provenance text.
     * @param simulationId Simulation Identifier.
     * @throws IllegalArgumentException See {@code super} constructor.
     */
    public Builder(final InformationLevel level, final String text, final Long simulationId)
                   throws IllegalArgumentException {
      super(level, text, simulationId);
    }

    /**
     * Individual data identifier.
     * 
     * @param val Individual data identifier.
     * @return Builder.
     */
    public Builder individualDataId(final String val) {
      individualDataId = val;
      return this;
    }

    /**
     * Dose-response data identifier.
     * 
     * @param val Dose-response data identifier.
     * @return Builder.
     */
    public Builder doseResponseDataId(final String val) {
      doseResponseDataId = val;
      return this;
    }

    /**
     * Raw data.
     * 
     * @param val Raw data.
     * @return Builder.
     */
    public Builder rawData(final Map<String, Object> val) {
      rawData.putAll(val);
      return this;
    }

    /**
     * Dose in uM.
     * 
     * @param val Dose in uM.
     * @return Builder.
     */
    public Builder dose_in_uM(final String val) {
      dose_in_uM = val;
      return this;
    }

    /**
     * Dose response.
     * 
     * @param val Response.
     * @return Builder.
     */
    public Builder response(final String val) {
      response = val;
      return this;
    }

    /**
     * Instruct the builder to build!
     * 
     * @return Newly built provenance data.
     */
    public DoseResponseProvenance build() {
      return new DoseResponseProvenance(this);
    }
  }

  private DoseResponseProvenance(final Builder builder) {
    super(builder);
    individualDataId = builder.individualDataId;
    doseResponseDataId = builder.doseResponseDataId;
    rawData.putAll(builder.rawData);
    dose_in_uM = builder.dose_in_uM;
    response = builder.response;
  }

  /**
   * Retrieve the individual data identifier.
   * 
   * @return Individual data id, or {@code null} if not assigned.
   */
  public String getIndividualDataId() {
    return individualDataId;
  }

  /**
   * Retrieve the dose-response data identifier.
   * 
   * @return Dose-Response data identifier, or {@code null} if not assigned.
   */
  public String getDoseResponseDataId() {
    return doseResponseDataId;
  }

  /**
   * Retrieve the raw data record.
   * 
   * @return Unmodifiable raw data record, or empty collection if not assigned.
   */
  public Map<String, Object> getRawData() {
    return Collections.unmodifiableMap(rawData);
  }

  /**
   * Retrieve the dose-response value in uM.
   * 
   * @return Dose-Response value in uM, or {@code null} it not assigned.
   */
  public String getDose_in_uM() {
    return dose_in_uM;
  }

  /**
   * Retrieve the response.
   * 
   * @return The response, or {@code null} if not assigned.
   */
  public String getResponse() {
    return response;
  }
}