/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Holder of an <i>Assay</i>'s pIC50 (and perhaps Hill Coefficient) data for its <i>Ion Channel</i>s
 * (e.g. hERG, CaV1.2, etc).
 *
 * @author geoff
 */
@Entity
@NamedQueries({
  @NamedQuery(name=SimulationAssay.QUERY_FOR_ASSAYS_BY_SIMULATION_ID,
              query="SELECT simulationassay FROM SimulationAssay AS simulationassay" +
                    "  WHERE simulationId = :simulationId")
})
@org.hibernate.annotations.ForeignKey(name="fk_simass_absicvpid")
public class SimulationAssay extends AbstractIonChannelValuesParent implements Cloneable {

  private static final long serialVersionUID = -5681663707681332124L;

  /** Consistent query naming */
  public static final String QUERY_FOR_ASSAYS_BY_SIMULATION_ID = "simulationAssay.queryForAssaysBySimulationId";

  @Transient
  private static final Log log = LogFactory.getLog(SimulationAssay.class);

  /** <b>Do not invoke directly.</b> */
  protected SimulationAssay() {}

  /**
   * Initialising constructor.
   * 
   * @param simulationId Optional simulation identifier.
   * @param assayName Assay name.
   * @param ionChannelValues Ion channel values.
   */
  public SimulationAssay(final Long simulationId, final String assayName,
                         final Set<IonChannelValues> ionChannelValues) {
    super(assayName, simulationId);
    log.debug("~SimulationAssay() : Invoked.");
    if (ionChannelValues != null && !ionChannelValues.isEmpty()) {
      for (final IonChannelValues eachIonChannelValues : ionChannelValues) {
        addIonChannelValues(eachIonChannelValues);
      }
    }
  }

  /**
   * Add a new {@linkplain IonChannelValues} to parent.
   * <p>
   * Only use when object is not for eventual persisting. I.e. Used after cloning a <code>SimulationAssay</code>
   * to temporarily add a new <code>IonChannelValues</code> due to inheritance / grouping.
   * 
   * @param newIonChannelValues New ion channel values to append to existing collection.
   * @throws IllegalArgumentException If null argument passed.
   */
  public void appendIonChannelValues(final IonChannelValues newIonChannelValues)
                                     throws IllegalArgumentException {
    if (newIonChannelValues == null) {
      throw new IllegalArgumentException("Invalid null new IonChannelValues object passed to append routine!");
    }
    addIonChannelValues(newIonChannelValues);
  }

  /**
   * This cloning process does not copy the surrogate primary keys of objects, just the "business"
   * data. Bidirectional relationships are retained though.
   * 
   * @return Cloned object.
   */
  @Override
  public SimulationAssay clone() throws CloneNotSupportedException {
    log.debug("~clone() : Invoked.");
    final Set<IonChannelValues> ionChannelValues = getIonChannelValues();
    final Set<IonChannelValues> clonedIonChannelValues = new HashSet<IonChannelValues>(ionChannelValues.size());
    for (final IonChannelValues eachIonChannelValues : ionChannelValues) {
      clonedIonChannelValues.add(eachIonChannelValues.clone());
    }
    return new SimulationAssay(getSimulationId(), getAssayName(), clonedIonChannelValues);
  }

  /**
   * Retrieve the assay name.
   * 
   * @return The assay name.
   */
  public String getAssayName() {
    return getParentName();
  }
}