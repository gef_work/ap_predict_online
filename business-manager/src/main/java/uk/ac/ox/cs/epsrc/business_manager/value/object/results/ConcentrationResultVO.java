/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object.results;

/**
 * Per-compound concentration simulation values.
 *
 * @author geoff
 */
public class ConcentrationResultVO {

  private final float compoundConcentration;
  private final String times;
  private final String voltages;
  private final String deltaAPD90;
  private final String upstrokeVelocity;
  private final String qNet;

  public ConcentrationResultVO(final float compoundConcentration,
                               final String times, final String voltages,
                               final String deltaAPD90,
                               final String upstrokeVelocity, final String qNet) {
    this.compoundConcentration = compoundConcentration;
    this.times = times;
    this.voltages = voltages;
    this.deltaAPD90 = deltaAPD90;
    this.upstrokeVelocity = upstrokeVelocity;
    this.qNet = qNet;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "ConcentrationResultVO [compoundConcentration="
        + compoundConcentration + ", times=" + times + ", voltages=" + voltages
        + ", deltaAPD90=" + deltaAPD90 + ", upstrokeVelocity="
        + upstrokeVelocity + ", qNet=" + qNet + "]";
  }

  /**
   * @return the compoundConcentration
   */
  public float getCompoundConcentration() {
    return compoundConcentration;
  }

  /**
   * @return the times
   */
  public String getTimes() {
    return times;
  }

  /**
   * @return the voltages
   */
  public String getVoltages() {
    return voltages;
  }

  /**
   * @return the deltaAPD90
   */
  public String getDeltaAPD90() {
    return deltaAPD90;
  }

  /**
   * @return the upstrokeVelocity
   */
  public String getUpstrokeVelocity() {
    return upstrokeVelocity;
  }

  /**
   * Retrieve the qNet results.
   * 
   * @return qNet results (or {@code null} if non calculated).
   */
  public String getQNet() {
    return qNet;
  }
}