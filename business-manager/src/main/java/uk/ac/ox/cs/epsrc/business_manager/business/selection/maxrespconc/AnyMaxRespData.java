/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.selection.maxrespconc;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.business.selection.maxrespconc.MaxRespDataSelectionStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.individual.MaxRespConcData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;

/**
 * Select the first max response data encountered which is derived from data records.
 *
 * @author geoff
 */
public class AnyMaxRespData implements MaxRespDataSelectionStrategy {

  private static final Log log = LogFactory.getLog(AnyMaxRespData.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.business.selection.maxrespconc.MaxRespDataSelectionStrategy#select(java.util.List)
   */
  @Override
  public MaxRespConcData select(final List<MaxRespConcData> options) throws IllegalArgumentException {
    log.debug("~select() : Invoked.");
    if (options == null) {
      final String errorMessage = "Illegal attempt to determine max response and concentration using a null options collection.";
      log.error("~select() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    MaxRespConcData selected = null;
    for (final MaxRespConcData option : options) {
      if (option.getMaxResp() != null && option.getMaxRespConc(Unit.M) != null) {
        log.debug("~select() : Selected '" + option.toString() + "'.");
        selected = option;
        break;
      }
    }

    return selected;
  }
}