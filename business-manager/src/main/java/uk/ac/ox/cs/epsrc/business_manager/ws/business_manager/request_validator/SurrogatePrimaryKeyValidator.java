/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.business_manager.request_validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.Ordered;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidSimulationIdException;
import uk.ac.ox.cs.nc3rs.business_manager.api.request.validator.SimulationIdsValidator;

/**
 * Validate the surrogate primary key nature of the simulation ids.
 *
 * @author geoff
 */
public class SurrogatePrimaryKeyValidator implements Ordered, SimulationIdsValidator {

  private static final Log log = LogFactory.getLog(SurrogatePrimaryKeyValidator.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.request.validator.SimulationIdsValidator#validate(java.lang.Object)
   */
  @Override
  public void validate(final Object containingObject) throws InvalidSimulationIdException {
    log.debug("~validate() : Invoked.");

    final Set<String> simulationIds = new HashSet<String>();

    throw new UnsupportedOperationException("Unknown instance of '" + containingObject.getClass() + "' when validating simulation ids.");

    /*
    final List<String> failed = new ArrayList<String>();
    for (final String simulationId : simulationIds) {
      try {
        final Long id = new Long(simulationId);
        if (id < 1L) {
          failed.add(simulationId);
        }
      } catch (NumberFormatException e) {
        failed.add(simulationId);
      }
    }

    if (failed.size() > 0) {
      final String message = "The following simulation ids were considered invalid '" + Arrays.toString(failed.toArray()) + "'.";

      log.info("~validate() : " + message);
      throw new InvalidSimulationIdException(message);
    }
    */
  }

  /* (non-Javadoc)
   * @see org.springframework.core.Ordered#getOrder()
   */
  @Override
  public int getOrder() {
    return 0;
  }
}