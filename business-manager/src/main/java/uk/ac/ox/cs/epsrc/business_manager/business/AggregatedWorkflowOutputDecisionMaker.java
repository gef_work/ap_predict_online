/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.MapIterator;
import org.apache.commons.collections.map.MultiKeyMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.annotation.Header;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.IndividualIVS;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.InputValueSource;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.SummaryIVS;
import uk.ac.ox.cs.epsrc.business_manager.value.object.workflow.OutcomeVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.APIIdentifiers;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;

/**
 * Decide what pIC50 value (or values) to use in the case of there being conflict between two 
 * or more values available for a specific assay and ion channel combination.
 * <p>
 * For example, in some situations there may be more than one summary data record for a compound 
 * in Iwks/FLIPR data and only one could be used.
 *
 * @author geoff
 */
// TODO: Determine if the AggregatedWorkflowOutputDecisionMaker is used or not - i.e. are there
//       really situations where there are more than one value available for an assay/ion channel.
public class AggregatedWorkflowOutputDecisionMaker {

  // JMS Template for sending provenance details.
  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JMS_TEMPLATE)
  private BusinessManagerJMSTemplate jmsTemplate;

  private static final Log log = LogFactory.getLog(AggregatedWorkflowOutputDecisionMaker.class);

  /**
   * Receive the aggregated workflow outcomes and find if there's any <i>duplicated</i> data within
   * the collection. Take the appropriate actions to handle this <i>duplicated</i> data.
   * <p>
   * <i>Duplicated</i> data can occur in situations where perhaps there are two summary records for
   * the same assay / ion channel combination. Equally, if a site perhaps has two sources of 
   * individual data for a particular assay / ion channel combination, e.g. data from two sites held
   * in two separate files, then these may have been read into two separate (supposedly summary-
   * specific) site data objects.
   * <p>
   * Beyond this point there is only a single per-assay, per-ion channel dataset, potentially
   * containing :
   * <ul>
   *   <li>
   *     Summary data (perhaps with that summary's individual and dose-response data).<br>
   *     TODO : Determine how to handle if two summary data records. 
   *   </li>
   *   <li>Individual data ( </li>
   * </ul>
   * 
   * @param compoundIdentifier Compound identifier.
   * @param processingType Processing type.
   * @param simulationId Simulation identifier.
   * @param outcomes Screening (i.e. not including QSAR in this case) workflow input value data
   *                 holders.
   * @return Selected input value source(s) following any decision-making process if more than one
   *         input value source for an assay / ion channel combination).
   */
  public List<OutcomeVO> makeDecisions(final @Header(value=APIIdentifiers.SI_HDR_COMPOUND_IDENTIFIER,
                                                     required=true)
                                             String compoundIdentifier,
                                       final @Header(value=BusinessIdentifiers.SI_HDR_PROCESSING_TYPE,
                                                     required=true)
                                             ProcessingType processingType,
                                       final @Header(value=BusinessIdentifiers.SI_HDR_SIMULATION_ID,
                                                     required=true)
                                             Long simulationId,
                                       final List<OutcomeVO> outcomes) {
    final String logPrefix = "~makeDecisions() : [".concat(compoundIdentifier).concat("] : ");
    log.debug(logPrefix.concat("Invoked."));

    final MultiKeyMap assayIonChannels = new MultiKeyMap();

    for (final OutcomeVO eachOutcome : outcomes) {
      log.debug(logPrefix.concat("Outcome '" + eachOutcome.toString() + "'."));

      final AssayVO assay = eachOutcome.getAssay();
      final String assayName = assay.getName();                   // key 1
      final IonChannel ionChannel = eachOutcome.getIonChannel();  // key 2

      if (assayIonChannels.containsKey(assayName, ionChannel)) {
        final String identifiedPrefix = "[".concat(compoundIdentifier).concat("]").
                                        concat("[").concat(assayName).concat("]").
                                        concat("[").concat(ionChannel.toString()).concat("] : ");
        final OutcomeVO existingOutcome = (OutcomeVO) assayIonChannels.get(assayName, ionChannel);

        if (summaryIVSInside(existingOutcome) || summaryIVSInside(eachOutcome)) {
          // Check if current or prior outcome was a summary!
          final String warnMessage = "TODO : SummaryIVS encountered during decision-making!";
          log.warn(identifiedPrefix.concat(warnMessage));
          throw new UnsupportedOperationException(warnMessage);
        }

        /* Provisionally combine multiple (per-summary supposedly!) site data objects with only 
           individual data - assume that they represent the PatchXpress data from two individual
           tables. */

        final List<InputValueSource> existingIVSs = existingOutcome.getInputValueSources();
        final List<InputValueSource> newIVSs = eachOutcome.getInputValueSources();
        final List<InputValueSource> combinedIVSs = new ArrayList<InputValueSource>();
        combinedIVSs.addAll(existingIVSs);
        combinedIVSs.addAll(newIVSs);
        final OutcomeVO combinedOutcome = new OutcomeVO(assay, ionChannel, combinedIVSs);
        // Replace the original with the combined.
        assayIonChannels.put(assayName, ionChannel, combinedOutcome);
      } else {
        assayIonChannels.put(assayName, ionChannel, eachOutcome);
      }
    }

    final List<OutcomeVO> resolved = new ArrayList<OutcomeVO>();
    final MapIterator iterator = assayIonChannels.mapIterator();
    while (iterator.hasNext()) {
      iterator.next();
      final OutcomeVO outcome = (OutcomeVO) iterator.getValue();
      log.debug(logPrefix.concat("Decided '" + outcome + "'."));
      resolved.add(outcome);
    }

    return resolved;
  }

  private boolean summaryIVSInside(final OutcomeVO outcome) {
    boolean summaryIVSFound = false;

    for (final InputValueSource inputValueSource : outcome.getInputValueSources()) {
      if (inputValueSource instanceof SummaryIVS) {
        summaryIVSFound = true;
        break;
      }
    }
    return summaryIVSFound;
  }

  /* TODO : Decide what to do when there's more than one outcome (i.e. per-summary object) for an assay / 
            ion channel combo. See also WorkflowProcessor.addInputValueSource. */
  private OutcomeVO decide(final String logPrefix,
                           final OutcomeVO option1, final OutcomeVO option2) {
    final String identifiedPrefix = "~decide() : ".concat(logPrefix);

    // Input value sources are a summary one, or one or more individual sources.
    for (final InputValueSource inputValueSource : option1.getInputValueSources()) {
      if (inputValueSource instanceof IndividualIVS) {
        final IndividualIVS individualIVS = (IndividualIVS) inputValueSource;
        final BigDecimal pIC50 = individualIVS.getpIC50();
        final BigDecimal hillCoefficient = individualIVS.getHillCoefficient();
        //final PC50EvaluationStrategy strategy = individualIVS.getStrategy();
        //final IndividualDataRecord individualData = individualIVS.getIndividualDataRecord();
        //final List<DoseResponseDataRecord> doseResponseData = individualIVS.getDoseResponseDataRecords();
      } else if (inputValueSource instanceof SummaryIVS) {
        final String warnMessage = "TODO : Option1 - SummaryIVS encountered during decision-making.";
        log.warn(identifiedPrefix.concat(warnMessage));
        throw new UnsupportedOperationException(warnMessage);

        // Decision-making on ... 1. Highest ranking pIC50 evaluation strategy?
        //                        2. Last updated/modified record data?
        //                        3. Strongest block?

        /*
        final SummaryIVS summaryIVS = (SummaryIVS) inputValueSource;
        final List<BigDecimal> pIC50s = summaryIVS.getpIC50s();
        final BigDecimal avgHillCoefficient = summaryIVS.getAvgHillCoefficient();
        final PC50EvaluationStrategy strategy = summaryIVS.getStrategy();
        final SummaryDataRecord summaryData = summaryIVS.getSummaryDataRecord();
        */
      } else {

      }
    }

    for (final InputValueSource inputValueSource : option2.getInputValueSources()) {
      if (inputValueSource instanceof IndividualIVS) {
        final IndividualIVS individualIVS = (IndividualIVS) inputValueSource;
        final BigDecimal pIC50 = individualIVS.getpIC50();
        final BigDecimal hillCoefficient = individualIVS.getHillCoefficient();
        //final PC50EvaluationStrategy strategy = individualIVS.getStrategy();
        //final IndividualDataRecord individualData = individualIVS.getIndividualDataRecord();
        //final List<DoseResponseDataRecord> doseResponseData = individualIVS.getDoseResponseDataRecords();
      } else if (inputValueSource instanceof SummaryIVS) {
        final String warnMessage = "TODO : Option2 - SummaryIVS encountered during decision-making.";
        log.warn(identifiedPrefix.concat(warnMessage));
        throw new UnsupportedOperationException(warnMessage);
        /*
        final SummaryIVS summaryIVS = (SummaryIVS) inputValueSource;
        final List<BigDecimal> pIC50s = summaryIVS.getpIC50s();
        final BigDecimal avgHillCoefficient = summaryIVS.getAvgHillCoefficient();
        final PC50EvaluationStrategy strategy = summaryIVS.getStrategy();
        final SummaryDataRecord summaryData = summaryIVS.getSummaryDataRecord();
        */
      } else {

      }
    }

    return option1;
  }
}
