/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual.doseresponse;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.IndividualProvenance;
import uk.ac.ox.cs.epsrc.business_manager.dao.information.ProvenanceDAO;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.DoseResponseData;
import uk.ac.ox.cs.epsrc.business_manager.ws.FDRServicesProxy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.AbstractPC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.doseresponse.FittingStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.NoSuchDataException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingParams;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingResult;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.IndividualProcessing;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;

/**
 * Abstracted dose-response fitting strategies for PC50 evaluation.
 *
 * @author geoff
 */
@Component
public abstract class AbstractDoseResponseFittingStrategy extends AbstractPC50EvaluationStrategy
                                                          implements FittingStrategy, Ordered {

  private static final long serialVersionUID = -110633051293268377L;

  // System strategy identifier.
  private final String sysProvenanceStrategyIdentifier;
  /**If this value changes it will need to be reflected in the client provenance.js javascript. */
  protected static final String sysProvenanceStrategyPrefix = ProvenanceDAO.DOSE_RESPONSE_PREFIX.concat("fs_");

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_FDR_SERVICES_PROXY)
  private transient FDRServicesProxy fdrServicesProxy;

  // JMS Template for sending provenance details.
  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JMS_TEMPLATE)
  private transient BusinessManagerJMSTemplate jmsTemplate;

  private static final Log log = LogFactory.getLog(AbstractDoseResponseFittingStrategy.class);

  /**
   * Initialising constructor.
   * 
   * @param description Description of the fitting strategy.
   * @param defaultActive Indicator of whether the strategy should be active by default.
   * @param defaultInvocationOrder Default strategy's invocation order.
   * @throws IllegalArgumentException If an inappropriate argument value exists.
   */
  protected AbstractDoseResponseFittingStrategy(final String description,
                                                final boolean defaultActive,
                                                final int defaultInvocationOrder)
                                                throws IllegalArgumentException {
    super(description, defaultActive, defaultInvocationOrder);
    this.sysProvenanceStrategyIdentifier = sysProvenanceStrategyPrefix
                                           .concat(retrieveSysProvenanceSuffix());
  }

  /**
   * Call the dose-response fitting web service and post-process the response.
   * 
   * @param simulationId Simulation identifier.
   * @param provenanceable {@code true} if sufficient data available to write provenance information.
   * @param individualDataId Identifier of the individual data record.
   * @param recordIdentifier (Optional if not provenanceable) Textual logging representation of
   *                         summary (if available) and individual data record identifiers.
   * @param individualC50Data Individual record C50 data.
   * @param doseResponseData Dose-response data point collection.
   * @param doseResponseFittingParams Parameters for passing to WS invocation, e.g. IC50_AND_HILL, etc.
   * @param pctIC The inhibitory concentration (e.g. 20%, 50%) to compare the fitting value against 
   * @return A valid dose-response fit, or {@code null} if not available.
   * @throws IllegalArgumentException If any required arguments have {@code null} values.
   */
  protected DoseResponseFittingResult callWS(final long simulationId,
                                             final boolean provenanceable,
                                             final String individualDataId,
                                             final String recordIdentifier,
                                             final ICData individualC50Data,
                                             final DoseResponseData doseResponseData,
                                             final DoseResponseFittingParams doseResponseFittingParams,
                                             final ICPercent pctIC)
                                             throws IllegalArgumentException {
    final String logPrefix = "~callWS() : [" + recordIdentifier + "] : ";

    final String fittingNature = retrieveFittingNature();
    assert (fittingNature != null) : "Invalid null value retrieve for fitting nature";

    if (provenanceable) {
      jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.TRACE,
                                                                  fittingNature.concat(" data available for fitting!"),
                                                                  simulationId)
                                                         .individualDataId(individualDataId)
                                                         .doseResponseData(doseResponseData)
                                                         .drFittingStrategyIdentifier(getSysProvenanceStrategyIdentifier())
                                                         .build());
    }

    final DoseResponseFittingResult response = getFdrServicesProxy().callFDRWebService(doseResponseData,
                                                                                       doseResponseFittingParams);

    if (response == null) {
      // e.g. if read timed out or data problem!
      final String warnMessage = fittingNature.concat("problem! Either communication or data processing problem when attempting dose-response fitting.");
      if (provenanceable) {
        jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.WARN,
                                                                    warnMessage, simulationId)
                                                           .individualDataId(individualDataId)
                                                           .build());
      }
      log.warn(logPrefix.concat(warnMessage));
      return null;
    }

    final String fittingNaturePIC50 = fittingNature.concat("pIC50 of '" + response.getPC50ValueShortened() + "' was ");
    // Received a good response from the dose-response fitting web service.
    final BigDecimal fittedIC50 = response.getIC50();
    // Test fitted data value against the individual data's specified record percent inhibitory concentration data.
    boolean isPotentialDose = false;
    try {
      isPotentialDose = individualC50Data.isPotentialDose(pctIC, fittedIC50);
    } catch (NoSuchDataException e) {}

    if (isPotentialDose) {
      final String infoMessage = fittingNaturePIC50.concat("appropriate!");
      if (provenanceable) {
        jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.TRACE,
                                                                    infoMessage, simulationId)
                                                           .individualDataId(individualDataId)
                                                           .build());
      }
      log.debug(logPrefix + infoMessage);
      return response;
    } else {
      final String debugMessage = fittingNaturePIC50.concat("inappropriate!");
      if (provenanceable) {
        jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.TRACE,
                                                                    debugMessage, simulationId)
                                                           .individualDataId(individualDataId)
                                                           .build());
      }
      log.debug(logPrefix + debugMessage);
      return null;
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.doseresponse.FittingStrategy#fitDoseResponseData(long, boolean, uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.IndividualProcessing, uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData, java.lang.String, uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingParams, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent)
   */
  @Override
  public DoseResponseFittingResult fitDoseResponseData(final long simulationId,
                                                       final boolean inputDataGathering,
                                                       final IndividualProcessing individualProcessing,
                                                       final ICData individualICData,
                                                       final String recordIdentifier,
                                                       final DoseResponseFittingParams doseResponseFittingParams,
                                                       final ICPercent pctIC)
                                                       throws IllegalArgumentException,
                                                              InvalidValueException {
    final String identifyingLogPrefix = "~fitDoseResponseData() : [" + recordIdentifier + "] : ";
    // Verify the input args.
    if (individualProcessing == null || individualICData == null ||
        doseResponseFittingParams == null) {
      final String errorMessage = "Fitting dose-response data requires: 1. Individual processing, 2. Individual c50Data and, 3. Dose-Response fitting parameters.";
      log.error(identifyingLogPrefix.concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }
    if (!individualICData.hasICPctData(pctIC)) {
      log.debug(identifyingLogPrefix.concat(" No '" + pctIC + "' per cent inhibitory concentration data to test a fitted result against!"));
      return null;
    }

    final String individualDataId = individualProcessing.getIndividualData().getId();
    final boolean provenanceable = !inputDataGathering;

    /* Note: Dose-Response data may appear only in the individual data record, e.g. max resp and
             conc, therefore no returning null at this point if no dose-response data records. */
    final DoseResponseData doseResponseData = retrieveDoseResponseData(simulationId,
                                                                       individualProcessing,
                                                                       individualICData,
                                                                       recordIdentifier,
                                                                       doseResponseFittingParams,
                                                                       individualDataId,
                                                                       provenanceable);

    if (doseResponseData == null) {
      log.debug(identifyingLogPrefix.concat("No dose-response data to fit."));
      return null;
    }

    return callWS(simulationId, provenanceable, individualDataId, recordIdentifier, individualICData,
                  doseResponseData, doseResponseFittingParams, pctIC);
  }

  /**
   * Retrieve the dose-response data from the individual data which corresponds to the
   * dose-response strategy.
   * <p>
   * It is assumed that all arguments have been assigned appropriately, i.e. <b>Only</b> invocable
   * from {@link #fitDoseResponseData(long, boolean, IndividualProcessing, ICData, String,
   * DoseResponseFittingParams, ICPercent)}
   * 
   * @param simulationId Simulation identifier. 
   * @param individualProcessing Individual processing object.
   * @param individualC50Data Individual record C50 data.
   * @param recordIdentifier (Optional) Textual logging representation of summary (if 
   *                                available) and individual data record identifiers.
   * @param doseResponseFittingParams Parameters for passing to WS invocation, e.g. IC50_AND_HILL, etc.
   * @param individualDataId Identifier of the individual data record.
   * @param provenanceable True if sufficient data available to write provenance information.
   * @return The dose-response data corresponding to the dose-response fitting strategy, or
   *         {@code null} if none available.
   * @throws InvalidValueException If an invalid value encountered.
   */
  protected abstract DoseResponseData retrieveDoseResponseData(final long simulationId,
                                                               final IndividualProcessing individualProcessing,
                                                               final ICData individualC50Data,
                                                               final String recordIdentifier,
                                                               final DoseResponseFittingParams doseResponseFittingParams,
                                                               final String individualDataId,
                                                               final boolean provenanceable)
                                                               throws InvalidValueException;

  /**
   * Retrieve textual representation of the nature of the fitting.
   * 
   * @return Textual representation of the nature of the fitting.
   */
  protected abstract String retrieveFittingNature();

  /**
   * Retrieve a system hardcoded provenance strategy suffix.
   * 
   * @return System hardcoded provenance strategy suffix.
   */
  protected abstract String retrieveSysProvenanceSuffix();

  /**
   * Retrieve the dose-response fitting services proxy.
   * 
   * @return Dose-Response fitting services proxy.
   */
  protected FDRServicesProxy getFdrServicesProxy() {
    return fdrServicesProxy;
  }

  /**
   * Retrieve the JMS template.
   * 
   * @return JMS template.
   */
  protected BusinessManagerJMSTemplate getJmsTemplate() {
    return jmsTemplate;
  }

  /**
   * Retrieve the system provenance fitting strategy identifier.
   * 
   * @return Dose-response fitting system provenance strategy identifier.
   */
  public String getSysProvenanceStrategyIdentifier() {
    return sysProvenanceStrategyIdentifier;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.business.dataprocessing.evaluation.pc50.PC50EvaluationStrategy#usesEqualityModifier()
   */
  @Override
  public boolean usesEqualityModifier() {
    return false;
  }
}