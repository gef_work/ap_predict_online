/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object.doseresponse;

import java.math.BigDecimal;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.util.ConverterUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingResult;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICType;

/**
 * Result of the Dose-Response fitting processing.
 *
 * @author geoff
 */
public class DoseResponseFittingResultVO implements DoseResponseFittingResult {

  private static final long serialVersionUID = 6281455445675158706L;

  private final BigDecimal ic50;
  private final BigDecimal hillCoefficient;
  private final String error;

  /**
   * Initialising constructor.
   * 
   * @param ic50 Fitted IC50 (uM).
   * @param hillCoefficient Fitted Hill Coefficient.
   * @param error Processing error.
   * @throws InvalidValueException If fitted IC50 is {@code null} or 0 (zero).
   */
  public DoseResponseFittingResultVO(final BigDecimal ic50, final BigDecimal hillCoefficient,
                                     final String error) throws InvalidValueException {
    if (ic50 == null) {
      final String errorMessage = "Cannot have an IC50 value null";
      final Object[] nature = new Object[] { errorMessage };
      throw new InvalidValueException(nature);
    }
    if (BigDecimal.ZERO.compareTo(ic50) == 0) {
      final String errorMessage = "Cannot have an IC50 value of 0";
      final Object[] nature = new Object[] { errorMessage };
      throw new InvalidValueException(nature);
    }
    this.ic50 = ic50;
    this.hillCoefficient = hillCoefficient;
    this.error = error;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "DoseResponseFittingResultVO [ic50=" + ic50 + ", hillCoefficient="
        + hillCoefficient + ", error=" + error + "]";
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingResult#getError()
   */
  @Override
  public String getError() {
    return error;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingResult#getHillCoefficient()
   */
  @Override
  public BigDecimal getHillCoefficient() {
    return hillCoefficient;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingResult#getIC50()
   */
  @Override
  public BigDecimal getIC50() {
    return ic50;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingResult#getPC50Value()
   */
  @Override
  public BigDecimal getPC50Value() {
    BigDecimal pIC50 = null;
    try {
      // constructor prevents assignment of invalid IC50 value.
      pIC50 = ConverterUtil.convert(ICType.IC50, Unit.uM, ICType.PIC50, Unit.minusLogM, ic50);
    } catch (InvalidValueException e) {}
    return pIC50;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingResult#getPC50ValueShortened()
   */
  @Override
  public String getPC50ValueShortened() {
    return ConverterUtil.bdShortened(getPC50Value());
  }
}