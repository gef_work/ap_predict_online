/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.IndividualC50DataStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.ModelAsInactiveStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.doseresponse.FittingStrategy;

/**
 * This {@link InputValueSource} object, containing an individual data potential input value (i.e.
 * a pIC50 and Hill (if available)), is created during workflow processing when the
 * {@link PC50EvaluationStrategy} is generally of type {@link FittingStrategy},
 * {@link IndividualC50DataStrategy} or {@link ModelAsInactiveStrategy}.
 *
 * @author geoff
 * @see SummaryIVS
 * @see QSARIVS
 */
public class IndividualIVS implements InputValueSource {

  private static final long serialVersionUID = -6047058412643365617L;

  // An individual record will generate a single pIC50
  private final BigDecimal pIC50;
  private final BigDecimal hillCoefficient;
  // The strategy from which the pIC50 value was derived.
  private final PC50EvaluationStrategy strategy;

  private static final Log log = LogFactory.getLog(IndividualIVS.class);

  /**
   * Initialising constructor containing the data that will be used later by ApPredict.
   * 
   * @param pIC50 pIC50, as determined by workflow processing.
   * @param hillCoefficient Hill Coefficient, as determined by workflow processing, or {@code null}
   *                        if not available.
   * @param strategy Strategy used to determine pIC50 and Hill Coefficient.
   * @throws IllegalArgumentException If the pIC50 or strategy value is {@code null}.
   */
  public IndividualIVS(final BigDecimal pIC50, final BigDecimal hillCoefficient,
                       final PC50EvaluationStrategy strategy)
                       throws IllegalArgumentException {
    if (pIC50 == null || strategy == null) {
      final String errorMessage = "Both a pIC50 and PC50 Evaluation strategy must be passed to individual input value source constructor";
      log.error("~IndividualIVS() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }
    this.pIC50 = pIC50;
    this.hillCoefficient = hillCoefficient;
    this.strategy = strategy;
  }

  /**
   * Indicator as to whether the strategy used to obtain the input value source was based on
   * presence of the equality modifier.
   * 
   * @return True if strategy based on equality modifier, otherwise false.
   */
  public boolean isOriginalData() {
    return strategy.usesEqualityModifier();
  }

  /**
   * Retrieve the default invocation order of the pIC50 evaluation strategy which generated this
   * pIC50.
   * 
   * @return Default invocation order of pIC50-generating strategy.
   */
  public int retrieveStrategyOrder() {
    return strategy.getOrder();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "IndividualIVS [pIC50=" + pIC50 + ", hillCoefficient="
        + hillCoefficient + ", strategy=" + strategy + "]";
  }

  /**
   * Retrieve the input value pIC50.
   * 
   * @return Non-{@code null} pIC50 value.
   */
  public BigDecimal getpIC50() {
    return pIC50;
  }

  /**
   * Retrieve the input value Hill coefficient.
   * 
   * @return Hill coefficient, or <code>null</code> if not available.
   */
  public BigDecimal getHillCoefficient() {
    return hillCoefficient;
  }
}