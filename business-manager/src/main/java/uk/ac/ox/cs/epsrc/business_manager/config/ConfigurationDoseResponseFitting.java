/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.config;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;

/**
 * DoseResponse fitting configuration options.
 *
 * @author geoff
 */
// Values declared in config/appCtx.config.doseResponseFitting.site.xml
@Component(BusinessIdentifiers.COMPONENT_CONFIGURATION_DOSERESPONSE_FITTING)
public class ConfigurationDoseResponseFitting {

  private final Map<String, Boolean> drfStrategyHillFitting = new HashMap<String, Boolean>();
  private final String defaultDoseResponseFittingStrategy;
  private final boolean defaultDoseResponseFittingRounded;
  private final float defaultDoseResponseFittingHillMax;
  private final float defaultDoseResponseFittingHillMin;

  private static final Log log = LogFactory.getLog(ConfigurationDoseResponseFitting.class);

  /**
   * Initialising constructor.
   * 
   * @param defaultDoseResponseFittingStrategy Default IC50_ONLY or IC50_AND_HILL for DoseResponse fitting.
   * @param defaultDoseResponseFittingRounded Default rounding.
   * @param defaultDoseResponseFittingHillMin Default minimum Hill Coefficient.
   * @param defaultDoseResponseFittingHillMax Default maximum Hill Coefficient.
   */
  public ConfigurationDoseResponseFitting(final String defaultDoseResponseFittingStrategy,
                                          final boolean defaultDoseResponseFittingRounded,
                                          final float defaultDoseResponseFittingHillMin,
                                          final float defaultDoseResponseFittingHillMax) {
    log.debug("~ConfigurationDoseResponseFitting() : Invoked.");

    this.defaultDoseResponseFittingStrategy = defaultDoseResponseFittingStrategy;
    this.defaultDoseResponseFittingRounded = defaultDoseResponseFittingRounded;
    this.defaultDoseResponseFittingHillMax = defaultDoseResponseFittingHillMax;
    this.defaultDoseResponseFittingHillMin = defaultDoseResponseFittingHillMin;

    log.info("~ConfigurationDoseResponseFitting() : Created initial configuration of '" + toString() + "'.");
  }

  /**
   * Indicate whether the fitting strategy, e.g. IC50_ONLY/IC50_AND_HILL is one which involves 
   * Hill coefficient manipulation.
   * 
   * @param fittingStrategy Dose-Response fitting strategy.
   * @return True if fitting strategy involves Hill min and max values, otherwise false.
   * @throws IllegalArgumentException If a <code>null</code> value is passed as the arg or no such
   *                                  fitting strategy has been configured.
   */
  public boolean isHillFittingStrategy(final String fittingStrategy)
                                       throws IllegalArgumentException {
    log.debug("~isHillFittingStrategy() : Invoked with '" + fittingStrategy + "'.");
    if (fittingStrategy == null) {
      final String errorMessage = "Inappropriate attempt to determine dose-response fitting strategy status using null strategy!";
      log.error("~isHillFittingStrategy() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    if (drfStrategyHillFitting.containsKey(fittingStrategy)) {
      return drfStrategyHillFitting.get(fittingStrategy);
    } else {
      final String errorMessage = "Unrecognized dose-response fitting strategy of '" + fittingStrategy + "'.";
      log.error("~isHillFittingStrategy() : " + errorMessage);
      throw new IllegalArgumentException(errorMessage);
    }
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "ConfigurationDoseResponseFitting [drfStrategyHillFitting="
        + drfStrategyHillFitting + ", defaultDoseResponseFittingStrategy="
        + defaultDoseResponseFittingStrategy + ", defaultDoseResponseFittingRounded="
        + defaultDoseResponseFittingRounded
        + ", defaultDoseResponseFittingHillMax="
        + defaultDoseResponseFittingHillMax
        + ", defaultDoseResponseFittingHillMin="
        + defaultDoseResponseFittingHillMin + "]";
  }

  /**
   * Retrieve the configured relationships between dose-response strategies and whether they're
   * Hill-fitting.
   * 
   * @return Unmodifiable collection of configured dose-response strategy and their Hill-fitting
   *         relationship.
   */
  public Map<String, Boolean> getDRFStrategyHillFitting() {
    return Collections.unmodifiableMap(drfStrategyHillFitting);
  }

  /**
   * Assign the dose-response strategies for Hill Coefficient fitting.
   * 
   * @param drfStrategyHillFitting (Possibly null value for) Dose-Response strategies for Hill 
   *                               Coefficient fitting.
   */
  // see Tips section @ http://docs.spring.io/spring/docs/3.0.x/spring-framework-reference/html/beans.html#beans-autowired-annotation-qualifiers
  @Resource(name="drfStrategyHillFitting")
  public void setDRFStrategyHillFitting(final Map<String, Boolean> drfStrategyHillFitting) {
    if (!getDRFStrategyHillFitting().isEmpty()) {
      final String errorMessage = "Inappropriate attempt to reassign dose-response strategy Hill-fitting relationships.";
      log.error("~setDRFStrategyHillFitting() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    if (drfStrategyHillFitting != null) {
      this.drfStrategyHillFitting.putAll(drfStrategyHillFitting);
    }
  }

  /**
   * Retrieve the default dose-response fitting strategy, e.g. IC50_ONLY.
   * 
   * @return The default dose-response fitting strategy.
   */
  public String getDefaultDoseResponseFittingStrategy() {
    return defaultDoseResponseFittingStrategy.toString();
  }

  /**
   * Retrieve the indicator of whether dose-response fitting is rounded or not.
   * 
   * @return True if rounding, otherwise false;
   */
  public boolean isDefaultDoseResponseFittingRounded() {
    return defaultDoseResponseFittingRounded;
  }

  /**
   * Retrieve the dose-response fitting Hill maximum value.
   * 
   * @return Dose-response fitting Hill maximum value.
   */
  public float getDefaultDoseResponseFittingHillMax() {
    return defaultDoseResponseFittingHillMax;
  }

  /**
   * Retrieve the dose-response fitting Hill minimum value.
   * 
   * @return Dose-response fitting Hill minimum value.
   */
  public float getDefaultDoseResponseFittingHillMin() {
    return defaultDoseResponseFittingHillMin;
  }
}