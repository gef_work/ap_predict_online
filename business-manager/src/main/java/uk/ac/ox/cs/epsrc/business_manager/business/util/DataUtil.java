/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.NoSuchDataException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Modifier;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;

/**
 * Utility class for data processing.
 *
 * @author geoff
 */
@Component
public class DataUtil {

  private static final Log log = LogFactory.getLog(DataUtil.class);

  /**
   * Retrieve the PC50 value from the 50% inhibition data in {@code icData} if the data has an
   * equality modifier.
   * 
   * @param icData Inhibitory concentration data.
   * @param recordIdentifier Logging record identifier (optionally {@code null}).
   * @return PC50 value if 50% inhibition data had an equality modifier, otherwise {@code null}.
   * @throws IllegalArgumentException If {@code null} value passed for {@code icData}.
   */
  public static BigDecimal pc50ByC50EqualityModifier(final ICData icData,
                                                     final String recordIdentifier)
                                                     throws IllegalArgumentException {
    final String logPrefix = "~pc50ByC50EqualityModifier() : [" + recordIdentifier + "] : ";
    log.debug(logPrefix.concat("Invoked."));
    if (icData == null) {
      final String errorMessage = "Illegal null value passed as ICData argument when finding PC50 with equality modifier.";
      log.error(logPrefix.concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    BigDecimal extractedPC50 = null;
    try {
      // Note : 50% only being considered!
      if (icData.isModifierEquals(ICPercent.PCT50)) {
        extractedPC50 = icData.retrieveEquivalentPC50Value(ICPercent.PCT50);
        log.debug(logPrefix.concat("Equality modifier - PC50 was '" + extractedPC50 + "'."));
      }
    } catch (NoSuchDataException e) {}
    return extractedPC50;
  }

  /**
   * Retrieve the nearest integer value to the original PC50 value (respecting the modifier) from
   * the 50% inhibition data in {@code icData} if the data did not have an equality modifier.
   * 
   * @param icData Inhibitory concentration data.
   * @param recordIdentifier (Optional) Logging record identifier.
   * @return Nearest integer PC50 value, otherwise {@code null}.
   * @throws IllegalArgumentException If {@code null} value passed for {@code icData}.
   */
  public static BigDecimal pc50ByC50NearestInteger(final ICData icData,
                                                   final String recordIdentifier)
                                                   throws IllegalArgumentException {
    if (icData == null) {
      final String errorMessage = "Illegal null value passed as ICData argument when finding PC50 by nearest integer.";
      log.error("~pc50ByNearestInteger() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    final String logPrefix = "~pc50ByC50NearestInteger() : [" + recordIdentifier + "] : ";
    log.debug(logPrefix.concat("Invoked."));
    BigDecimal extractedPC50 = null;
    try {
      // Note : 50% only being considered!
      if (!icData.isModifierEquals(ICPercent.PCT50)) {
        final boolean isEffectiveGreater = icData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50);
        extractedPC50 = new BigDecimal(roundToNearestInteger(isEffectiveGreater,
                                                             icData.retrieveEquivalentPC50Value(ICPercent.PCT50)));
        log.debug(logPrefix.concat("Non-equality modifier found - nearest integer was '" + extractedPC50 + "'."));
      }
    } catch (NoSuchDataException e) {}

    return extractedPC50;
  }

  /**
   * Retrieve the nearest integer pC50 value to the original pC50 value, respecting the modifier,
   * under the following conditions :
   * <ul>
   *   <li>C50 data, i.e. not C20 data.</li>
   *   <li>
   *     IC50 data with {@link Modifier#GREATER} modifier, or pI/XC50 data with a 
   *     {@link Modifier#LESS} modifier.<br>
   *     <b>NOTE : </b> In situations where the modifiers are reversed it is assumed that the data
   *     is highly active at the lowest tested concentration, and such data will be handled
   *     elsewhere.
   *   </li>
   * </ul>
   * Examples :
   * <ul>
   *   <li>pC50 = 4, modifier = {@link Modifier#LESS} : Resulting pIC50 of 3</li>
   *   <li>pC50 = 4.3, modifier = {@link Modifier#LESS} : Resulting pIC50 of 4</li>
   *   <li>IC50 = 1000μM, modifier = {@link Modifier#GREATER} : Resulting pIC50 of 2</li>
   * </ul>
   * 
   * @param icData Inhibitory concentration data.
   * @param recordIdentifier (Optional) Logging record identifier.
   * @return Nearest integer pIC50, respecting modifier if criteria satisfied, otherwise {@code null}.
   * @throws IllegalArgumentException If {@code null} value passed for {@code icData}.
   */
  @Deprecated
  public static BigDecimal pc50ByNearestIntegerIfNotHighlyActive(final ICData icData,
                                                                 final String recordIdentifier)
                                                                 throws IllegalArgumentException {
    final String logPrefix = "~pc50ByNearestIntegerIfNotHighlyActive() : [" + recordIdentifier + "] : ";
    log.debug(logPrefix.concat("Invoked."));
    if (icData == null) {
      final String errorMessage = "Illegal null value passed as ICData argument to strategy for finding PC50 by nearest integer if not highly active.";
      log.error(logPrefix.concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    BigDecimal extractedPC50 = null;
    try {
      // TODO : Only C50 data considered when evaluating pIC50 by nearest integer when not highly active.
      if (!icData.isModifierEquals(ICPercent.PCT50)) {
        final boolean isEffectiveGreater = icData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50);
        if (isEffectiveGreater) {
          log.debug(logPrefix.concat("Assuming highly active at lowest concentration '" + icData.toString() + "'."));
        } else {
          // Only get nearest integer if 1) IC50 and '>', or 2) pIC50 and '<'
          extractedPC50 = new BigDecimal(roundToNearestInteger(isEffectiveGreater,
                                                               icData.retrieveEquivalentPC50Value(ICPercent.PCT50)));
          log.debug(logPrefix.concat("Criteria matched - nearest integer was '" + extractedPC50 + "'."));
        }
      }
    } catch (NoSuchDataException e) {}

    return extractedPC50;
  }

  /**
   * Round the value to the nearest integer based on the direction the modifier instructs.
   * <p>
   * Note that we're rounding to {@link RoundingMode#FLOOR} and {@link RoundingMode#CEILING} for
   * decimal numbers (i.e. we don't round towards zero, but +ve and -ve infinity).  
   * 
   * @param roundUp Round the value up?
   * @param value Value to round to the nearest integer.
   * @return Nearest integer value.
   */
  public static int roundToNearestInteger(final boolean roundUp, final BigDecimal value) {
    if (value == null) {
      throw new NullPointerException("Invalid attempt to round a null value to nearest int.");
    }
    log.debug("~roundToNearestInteger() : Invoked.");

    final boolean isWholeIntegerValue = (value.setScale(0, RoundingMode.DOWN).compareTo(value) == 0);

    int nearestInteger;
    if (isWholeIntegerValue) {
      final BigDecimal nextIntegerValue = roundUp ? value.add(BigDecimal.ONE) :
                                                    value.subtract(BigDecimal.ONE);
      nearestInteger = nextIntegerValue.intValue();
    } else {
      final BigDecimal roundedValue = roundUp ? value.setScale(0, RoundingMode.CEILING) : 
                                                value.setScale(0, RoundingMode.FLOOR);
      nearestInteger = roundedValue.intValue();
    }

    if (log.isDebugEnabled()) {
      final String modifierMessage = roundUp ? "greater" : "less than";
      log.debug("~roundToNearestInteger() : Original value '" + value.toPlainString() + "' with modifier '" + modifierMessage + "' produces '" + nearestInteger + "'.");
    }
    return nearestInteger;
  }
}