/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.IndividualProcessing;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;

/**
 * Value object holding a single individual record as well as its summary data (optional) and its
 * dose-response data (optional). This avoids passing the <code>SiteDataVO</code> around in messages,
 * instead an individual-specific object is passed.
 *
 * @author geoff
 */
public class IndividualProcessingVO implements IndividualProcessing {

  private final SummaryDataRecord summaryData;
  private final IndividualDataRecord individualData;

  private final List<DoseResponseDataRecord> doseResponseData = new ArrayList<DoseResponseDataRecord>();

  /**
   * Initialising constructor.
   * 
   * @param summaryData Summary data (optional).
   * @param individualData Individual data (required).
   * @param doseResponseData Dose-response data for the individual data (optional).
   */
  public IndividualProcessingVO(final SummaryDataRecord summaryData, 
                                final IndividualDataRecord individualData,
                                final List<DoseResponseDataRecord> doseResponseData) {
    if (individualData == null) {
      throw new NullPointerException("Individual processing requires an individual data record!");
    }
    this.summaryData = summaryData;
    this.individualData = individualData;
    if (doseResponseData != null) {
      this.doseResponseData.addAll(doseResponseData);
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.value.processing.IndividualProcessing#hasDoseResponseData()
   */
  @Override
  public boolean hasDoseResponseData() {
    return !doseResponseData.isEmpty();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "IndividualProcessingVO [summaryData=" + summaryData
        + ", individualData=" + individualData + ", doseResponseData="
        + doseResponseData + "]";
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.value.processing.IndividualProcessing#getDoseResponseData()
   */
  @Override
  public List<DoseResponseDataRecord> getDoseResponseData() {
    return Collections.unmodifiableList(doseResponseData);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.value.processing.IndividualProcessing#getIndividualData()
   */
  @Override
  public IndividualDataRecord getIndividualData() {
    return individualData;
  }
}
