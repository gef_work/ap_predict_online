/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.annotation.Header;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.entity.SimulationAssay;
import uk.ac.ox.cs.epsrc.business_manager.manager.SimulationAssayManager;
import uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager;
import uk.ac.ox.cs.epsrc.business_manager.value.object.PreInvocationProcessingVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata.ProcessedSiteDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ExperimentalDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;

/**
 * Persisting all relevant (e.g. pIC50s, Hill Coeffs, etc.) data derived from the experimental,
 * QSAR and screening.
 *
 * @author geoff
 */
public class PostProcessingDataRecorder {

  // JMS Template for sending provenance details.
  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JMS_TEMPLATE)
  private BusinessManagerJMSTemplate jmsTemplate;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_SIMULATIONASSAY_MANAGER)
  private SimulationAssayManager simulationAssayManager;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_SIMULATION_MANAGER)
  private SimulationManager simulationManager;

  private static final Log log = LogFactory.getLog(PostProcessingDataRecorder.class);

  /**
   * Record the data.
   * 
   * @param processingType Simulation processing type.
   * @param simulationId Simulation identifier.
   * @param processedSiteData Processed site data.
   * @return Data for pre-invocation processing.
   * @throws IllegalArgumentException If a {@code null} value is received for the
   *                                  {@code processedSiteData}.
   */
  // See appCtx.sim.processSimulation.xml
  public PreInvocationProcessingVO postProcessingDataRecording(final @Header(value=BusinessIdentifiers.SI_HDR_PROCESSING_TYPE,
                                                                             required=true)
                                                                     ProcessingType processingType,
                                                               final @Header(value=BusinessIdentifiers.SI_HDR_SIMULATION_ID,
                                                                             required=true)
                                                                     Long simulationId,
                                                               final ProcessedSiteDataVO processedSiteData)
                                                               throws IllegalArgumentException {
    final String identifiedLogPrefix = "~postProcessedDataRecording() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));
    if (processedSiteData == null) {
      final String errorMessage = "Null processed site data passed for processed site data!";
      log.error(identifiedLogPrefix.concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    final boolean inputDataGathering = ProcessingType.isInputDataGathering(processingType);

    final List<SimulationAssay> simulationAssays = new ArrayList<SimulationAssay>();
    final List<SimulationAssay> transients = simulationAssayManager.createTransient(inputDataGathering,
                                                                                    simulationId,
                                                                                    processedSiteData.getQSAR(),
                                                                                    processedSiteData.getScreening());
    if (inputDataGathering) {
      simulationAssays.addAll(transients);
    } else {
      simulationAssays.addAll(simulationAssayManager.persistSimulationAssays(transients));
      jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.TRACE,
                                                           MessageKey.ASSAY_VALUES_PERSISTED.getBundleIdentifier(),
                                                           new String[] { }, simulationId).build());
    }

    PreInvocationProcessingVO preInvocationProcessingVO = null;
    final ExperimentalDataHolder experimentalDataHolder = processedSiteData.getExperimental();
    if (experimentalDataHolder != null) {
      preInvocationProcessingVO = new PreInvocationProcessingVO(experimentalDataHolder.retrievePerFreqCompoundConcs(),
                                                                simulationAssays);
    } else {
      log.debug(identifiedLogPrefix.concat("ProcessedSiteData contained null experimental data object."));
      preInvocationProcessingVO = new PreInvocationProcessingVO(null, simulationAssays);
    }
    return preInvocationProcessingVO;
  }

  /**
   * Terminate a simulation in situations such as there being no site data to process, e.g. if
   * only alerting/estimated inhibitory concentration values encountered.
   * 
   * @param processingType Input data gathering or otherwise.
   * @param simulationId Simulation identifier.
   * @param processedSiteData Processed site data (must not contain site data!).
   * @return Processed site data.
   * @throws IllegalArgumentException If processed site data is {@code null} or contains site data.
   */
  public ProcessedSiteDataVO postProcessingSimulationTermination(final @Header(value=BusinessIdentifiers.SI_HDR_PROCESSING_TYPE,
                                                                               required=true)
                                                                       ProcessingType processingType,
                                                                 final @Header(value=BusinessIdentifiers.SI_HDR_SIMULATION_ID,
                                                                               required=true)
                                                                       Long simulationId,
                                                                 final ProcessedSiteDataVO processedSiteData)
                                                                 throws IllegalArgumentException {
    final String identifiedLogPrefix = "~postProcessingSimulationTermination() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    if (processedSiteData == null) {
      final String errorMessage = "Null processed site data passed for processed site data!";
      log.error(identifiedLogPrefix.concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }
    if (processedSiteData.isContainingSiteData()) {
      final String errorMessage = "Invalid attempt to terminate simulation when site data available!";
      log.error(identifiedLogPrefix.concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    if (ProcessingType.isInputDataGathering(processingType)) {
      log.debug(identifiedLogPrefix.concat("Input data gathering - ignoring terminate simulation instruction."));
    } else {
      log.debug(identifiedLogPrefix.concat("Not input data gathering - terminating simulation."));
      simulationManager.simulationTerminating(simulationId);
    }

    return processedSiteData;
  }
}