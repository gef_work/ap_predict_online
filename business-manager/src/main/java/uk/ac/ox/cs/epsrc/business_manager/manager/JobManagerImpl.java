/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.manager;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.JobProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.config.ConfigurationActionPotential;
import uk.ac.ox.cs.epsrc.business_manager.dao.JobDAO;
import uk.ac.ox.cs.epsrc.business_manager.entity.GroupedInvocationInput;
import uk.ac.ox.cs.epsrc.business_manager.entity.Job;
import uk.ac.ox.cs.epsrc.business_manager.entity.JobResult;
import uk.ac.ox.cs.epsrc.business_manager.value.object.PerJobConcentrationResultsVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedDataVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.JobDiagnosticsVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Job management implementation.
 *
 * @author geoff
 */
@Component(BusinessIdentifiers.COMPONENT_JOB_MANAGER)
@Transactional(readOnly=true)
public class JobManagerImpl implements JobManager {

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_CONFIGURATION_ACTIONPOTENTIAL)
  private ConfigurationActionPotential configurationActionPotential;

  // JMS Template for sending provenance details.
  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JMS_TEMPLATE)
  private BusinessManagerJMSTemplate jmsTemplate;

  // information data access object
  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JOB_DAO)
  private JobDAO jobDAO;

  private static final Log log = LogFactory.getLog(JobManagerImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.JobManager#assignAppManagerIdToJob(long, java.lang.String)
   */
  @Override
  @Transactional(readOnly=false)
  public void assignAppManagerIdToJob(final long jobId, final String appManagerId) {
    final String identifiedLogPrefix = "~assignAppManagerIdToJob() : Job#[" + jobId + "]AppMgr#[" + appManagerId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    jobDAO.assignAppManagerIdToJob(jobId, appManagerId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.JobManager#createAndStore(long, uk.ac.ox.cs.epsrc.business_manager.value.ProcessedDataVO)
   */
  @Override
  @Transactional(readOnly=false)
  public Set<Job> createAndStore(final long simulationId, final ProcessedDataVO processedData) {
    final String identifiedLogPrefix = "~createAndStore() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    final Map<BigDecimal, Set<BigDecimal>> perFrequencyConcentrations = new HashMap<BigDecimal, Set<BigDecimal>>();
    perFrequencyConcentrations.putAll(processedData.getPerFrequencyConcentrations());

    boolean usingDefaultCompoundConcentrations = true;
    if (perFrequencyConcentrations.isEmpty()) {
      log.debug(identifiedLogPrefix.concat("Using configuration-supplied per-frequency concentrations."));
      perFrequencyConcentrations.putAll(configurationActionPotential.getDefaultPerFrequencyConcentrations());
    } else {
      log.debug(identifiedLogPrefix.concat("Using experimentally defined per-frequency concentrations."));
      usingDefaultCompoundConcentrations = false;
    }

    final Set<GroupedInvocationInput> groupedAndInherited = processedData.getProcessedData();

    final Set<Job> jobs = new HashSet<Job>();

    for (final Map.Entry<BigDecimal, Set<BigDecimal>> frequencyConcentrations : perFrequencyConcentrations.entrySet()) {
      // Switch from BigDecimal to Float values for concentrations.
      final BigDecimal pacingFrequency = frequencyConcentrations.getKey();
      final Set<BigDecimal> compoundConcentrations = frequencyConcentrations.getValue();
      final Set<Float> floatConcentrations = new TreeSet<Float>();
      for (final BigDecimal compoundConcentration : compoundConcentrations) {
        floatConcentrations.add(compoundConcentration.floatValue());
      }

      // Create the Job objects from the grouped invocation input values.
      for (final GroupedInvocationInput groupedInvocationInput : groupedAndInherited) {
        final Job job = new Job(simulationId, pacingFrequency.floatValue(),
                                floatConcentrations, usingDefaultCompoundConcentrations,
                                groupedInvocationInput);
        log.debug(identifiedLogPrefix.concat("Created '" + job.toString() + "'."));
        jobs.add(job);
      }
    }

    jobDAO.storeJobs(jobs);

    return jobs;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.JobManager#determineJobsCompletedForSimulation(long)
   */
  @Override
  public boolean determineJobsCompletedForSimulation(final long simulationId) {
    log.debug("~determineJobsCompletedForSimulation() : Invoked for simualtion id '" + simulationId + "'.");
    final long incompleteJobsCount = jobDAO.incompleteJobsForSimulationCount(simulationId);

    return (incompleteJobsCount == 0);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.JobManager#findJobsBySimulationId(long)
   */
  @Override
  public List<Job> findJobsBySimulationId(final long simulationId) {
    log.debug("~findJobsBySimulationId() : Invoked for simulation id '" + simulationId + "'.");

    return jobDAO.findJobsBySimulationId(simulationId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.JobManager#persistDiagnostics(java.lang.String, java.lang.String, java.lang.String)
   */
  @Override
  // Note: (Default) transaction propogation is REQUIRED, i.e. Support a current transaction, create a new one if none exists.
  @Transactional(readOnly=false)
  public void persistDiagnostics(final String appManagerId, final String info,
                                 final String output) throws IllegalArgumentException {
    final String identifiedLogPrefix = "~persistDiagnostics() : AppMgr#[" + appManagerId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    if (StringUtils.isBlank(appManagerId)) {
      final String errorMessage = "Invalid app manager id value of '" + appManagerId + "' received!";
      log.error(identifiedLogPrefix.concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    final Job job = jobDAO.findJobByAppManagerId(appManagerId);
    if (job != null) {
      job.setInfo(info);
      job.setOutput(output);
      jobDAO.save(job);
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.JobManager#persistResults(java.lang.String, java.lang.String, java.lang.String, java.util.Set)
   */
  @Override
  @Transactional(readOnly=false)
  public Job persistResults(final String appManagerId, final String messages,
                            final String deltaAPD90PercentileNames,
                            final Set<PerJobConcentrationResultsVO> perJobConcentrationResultsVO)
                            throws IllegalArgumentException {
    final String identifiedLogPrefix = "~persistResults() : AppMgr#[" + appManagerId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    if (StringUtils.isBlank(appManagerId)) {
      final String errorMessage = "Invalid app manager id value of '" + appManagerId + "' received!";
      log.error(identifiedLogPrefix.concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    final Job job = jobDAO.findJobByAppManagerId(appManagerId);
    if (job != null) {
      final Long simulationId = job.getSimulationId();
      final Long jobId = job.getId();
      if (StringUtils.isNotBlank(messages)) {
        jmsTemplate.sendProgress(new JobProgress.Builder(InformationLevel.WARN,
                                                         MessageKey.JOB_GENERATED_MESSAGES.getBundleIdentifier(),
                                                         new String[] { },
                                                         simulationId, jobId)
                                                .build());
        job.setMessages(messages);
      }
      job.setDeltaAPD90PercentileNames(deltaAPD90PercentileNames);

      boolean jobResultsRetrieved = false;
      if (!perJobConcentrationResultsVO.isEmpty()) {
        for (final PerJobConcentrationResultsVO eachPerJobConcentrationResults : perJobConcentrationResultsVO) {
          final String compoundConcentrationIn = eachPerJobConcentrationResults.getCompoundConcentration();
          final String deltaAPD90 = eachPerJobConcentrationResults.getDeltaAPD90();
          final String upstrokeVelocity = null;
          final String apTimes = eachPerJobConcentrationResults.getTimes();
          final String apVoltages = eachPerJobConcentrationResults.getVoltages();
          final String qNet = eachPerJobConcentrationResults.getQNet();

          Float compoundConcentration = null;
          try {
            compoundConcentration = new Float(compoundConcentrationIn);
          } catch (NumberFormatException e) {
            log.error("~persistResults() : An invalid non-numeric compound concentration of '" + compoundConcentrationIn + "' encountered!");
          }

          // The constructor establishes bidirectional relationship
          new JobResult(job, compoundConcentration, deltaAPD90,
                        upstrokeVelocity, apTimes, apVoltages, qNet);
          jobResultsRetrieved = true;
        }
      } else {
        log.warn(identifiedLogPrefix.concat("Empty results object returned."));
        // Persist empty results for a job!? Assume job failed to run!
        jmsTemplate.sendProgress(new JobProgress.Builder(InformationLevel.ERROR,
                                                         MessageKey.JOB_RUN_FAILURE.getBundleIdentifier(),
                                                         new String[] { },
                                                         simulationId, jobId)
                                                .build());
        job.considerFailed();
        jobDAO.storeJobResults(job, true);
      }

      if (jobResultsRetrieved) {
        // Merge job object and assign as completed.
        jobDAO.storeJobResults(job, true);

        jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.DEBUG,
                                                             MessageKey.JOB_RESULTS_RECEIVED.getBundleIdentifier(),
                                                             new String[] { jobId.toString() },
                                                             simulationId)
                                                    .build());
      }
    }

    return job;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.JobManager#purgeSimulationJobData(long)
   */
  @Override
  @Transactional(readOnly=false)
  public Set<String> purgeSimulationJobData(final long simulationId) {
    final String identifiedLogPrefix = "~purgeSimulationJobData() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    // Find unfinished jobs, i.e. haven't failed but no results yet.
    final Set<String> unfinishedJobsAppManagerIds = new HashSet<String>();
    final List<Job> simulationJobs = jobDAO.findJobsBySimulationId(simulationId);
    for (final Job job : simulationJobs) {
      if (job.isUnfinished()) {
        unfinishedJobsAppManagerIds.add(job.getAppManagerId());
      }
    }

    // Remove all the jobs.
    jobDAO.removeJobs(simulationJobs);

    return unfinishedJobsAppManagerIds;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.JobManager#retrieveDiagnostics(long)
   */
  @Override
  public JobDiagnosticsVO retrieveDiagnostics(final long jobId) {
    final String identifiedLogPrefix = "~retrieveDiagnostics() : Job#[" + jobId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    final Job job = jobDAO.findByJobId(jobId);

    String info = null;
    String output = null;

    if (job != null) {
      info = job.getInfo();
      output = job.getOutput();
    }

    return new JobDiagnosticsVO(info, output);
  }
}