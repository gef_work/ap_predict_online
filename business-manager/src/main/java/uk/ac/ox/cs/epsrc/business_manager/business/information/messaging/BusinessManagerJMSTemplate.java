/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.information.messaging;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import uk.ac.ox.cs.epsrc.business_manager.business.SiteDataRecorderServiceActivator;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.JobProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.FundamentalProvenance;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.Provenance;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.SummaryProvenance;
import uk.ac.ox.cs.epsrc.business_manager.service.SimulationService;
import uk.ac.ox.cs.epsrc.business_manager.business.workflow.WorkflowProcessor;

/**
 * Business manager's JMS template.
 *
 * @author geoff
 */
public class BusinessManagerJMSTemplate extends JmsTemplate {

  private static final Log log = LogFactory.getLog(BusinessManagerJMSTemplate.class);

  /**
   * Send progress information.
   * <p>
   * Generally the Progress will be one of {@link JobProgress} or {@link OverallProgress}. 
   * 
   * @param progress Progress data.
   */
  public void sendProgress(final Progress progress) {
    log.trace("~sendProgress() : About to send progress '" + progress.toString() + "'.");
    this.send(new MessageCreator() {
      public Message createMessage(final Session session) throws JMSException {
        final ObjectMessage message = session.createObjectMessage();
        message.setObject(progress);
        return message;
      }
    });
  }

  /**
   * Send provenance information.
   * <p>
   * Under normal circumstances {@linkplain FundamentalProvenance} records would be created as part
   * of simulation request processing (e.g. {@linkplain SimulationService#processSimulationsRequest(java.util.List)})
   * whereas other provenance (e.g. {@linkplain SummaryProvenance}, etc) would be created in
   * perhaps {@link SiteDataRecorderServiceActivator#recordScreeningSiteData(ProcessingType, Long, AggregatedSiteDataVO)}
   * and then all provenance updated in subsequent processing, e.g. {@linkplain WorkflowProcessor}.
   * 
   * @param provenance Provenance data.
   */
  public void sendProvenance(final Provenance provenance) {
    log.trace("~sendProvenance() : About to send provenance '" + provenance.toString() + "'.");
    this.send(new MessageCreator() {
      public Message createMessage(final Session session) throws JMSException {
        final ObjectMessage message = session.createObjectMessage();
        message.setObject(provenance);
        return message;
      }
    });
  }
}