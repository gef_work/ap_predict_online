/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Grouped invocation input values.
 * <p>
 * These values are those that appear in input values table groups. There is only one value set 
 * per-grouping, per simulation, e.g. Iwks/FLIPR/Barracuda. Unlike {@linkplain SimulationAssay} 
 * these value sets may have inherited pIC50 data from other assays (according to the grouping and
 * inheriting strategies).
 *
 * @author geoff
 */
@org.hibernate.annotations.ForeignKey(name="fk_id_grpinvinp_absicvp")

@Entity
@NamedQueries({
  @NamedQuery(name=GroupedInvocationInput.QUERY_BY_SIMULATION_ID,
              query="SELECT groupedinvocationinput FROM GroupedInvocationInput AS groupedinvocationinput " +
                    "  WHERE " + Simulation.PROPERTY_SIMULATION_ID + " = :" + Simulation.PROPERTY_SIMULATION_ID)
})
public class GroupedInvocationInput extends AbstractIonChannelValuesParent {

  private static final long serialVersionUID = -2334290777324038589L;

  /** Consistent query naming - Query job by app manager identifier. */
  public static final String QUERY_BY_SIMULATION_ID = "groupedinvocationinput.queryBySimulationId";

  // Allow ordered grouping. Note that the grouping levels may be assay levels if no grouping used.
  @Column(nullable=false, updatable=false)
  private Short groupLevel;

  /* Holds relationship of jobs for grouped invocation input. Potentially more than one Job can
     refer to the input, e.g. if multiple pacing frequencies used. */
  // bidirectional GroupedInvocationInput [1] <-> [1..*] Job
  @OneToMany(mappedBy="groupedInvocationInput")
  @org.hibernate.annotations.ForeignKey(name="fk_jt_groupedinvocationinput",
                                        inverseName="fk_jt_job")
  private Set<Job> jobs = new HashSet<Job>();

  @Transient
  private static final Log log = LogFactory.getLog(GroupedInvocationInput.class);

  /** <b>Do not invoke directly.</b> */
  protected GroupedInvocationInput() {}

  /**
  * Initialising constructor.
  * 
  * @param simulationId Simulation identifier.
  * @param groupLevel Group level.
  * @param groupName Group name.
  * @param ionChannelValues Ion channel values.
  */
  public GroupedInvocationInput(final Long simulationId, final Short groupLevel,
                                final String groupName,
                                final Set<IonChannelValues> ionChannelValues) {
    super(groupName, simulationId);
    log.debug("~GroupedInvocationInput() : Invoked.");
    this.groupLevel = groupLevel;
    if (ionChannelValues != null && !ionChannelValues.isEmpty()) {
      for (final IonChannelValues eachIonChannelValues : ionChannelValues) {
        addIonChannelValues(eachIonChannelValues);
      }
    }
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "GroupedInvocationInput [groupLevel=" + groupLevel
        + ", toString()=" + super.toString() + "]";
  }

  /**
   * Retrieve the assay group name.
   * 
   * @return The assay group name.
   */
  public String getAssayGroupName() {
    return getParentName();
  }

  /**
   * Retrieve the assay group level.
   * 
   * @return The assay group level.
   */
  public Short getAssayGroupLevel() {
    return groupLevel;
  }
}