/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.Ordered;

import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.AbstractPC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.ModelAsInactiveStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.QualityControlFailureVO;

/**
 * Strategy to highlight when data is marked as inactive.
 *
 * @author geoff
 */
public class DataMarkedAsInactive extends AbstractPC50EvaluationStrategy
                                  implements ModelAsInactiveStrategy, Ordered {

  private static final long serialVersionUID = -1625814694777049995L;

  private static final Log log = LogFactory.getLog(DataMarkedAsInactive.class);

  /**
   * {@inheritDoc}
   */
  protected DataMarkedAsInactive(final String description, final boolean defaultActive,
                                 final int defaultInvocationOrder) throws IllegalArgumentException {
    super(description, defaultActive, defaultInvocationOrder);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.ModelAsInactiveStrategy#modelAsInactive(uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO, uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord, uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData, java.lang.String)
   */
  @Override
  public String modelAsInactive(final AssayVO assayVO, final IndividualDataRecord individualData,
                                final ICData individualC50Data, final String identifyingPrefix)
                                throws InvalidValueException {
    final String logPrefix = "~modelAsInactive() : " + identifyingPrefix + " : ";
    log.debug(logPrefix.concat(" Invoked."));

    final QualityControlFailureVO qcFailure = individualData.isMarkedInactive();

    final String reason = (qcFailure != null) ? qcFailure.getReason() : null;

    return reason;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy#usesEqualityModifier()
   */
  @Override
  public boolean usesEqualityModifier() {
    return false;
  }
}