/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.selection.maxrespconc;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.individual.MaxRespConcData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.IndividualProcessing;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;

/**
 * Process the individual data records max resp and max resp concentration values (if available!).
 *
 * @author geoff
 */
@Component
public class IndividualMaxRespAndConcDataProcessor {

  private static final Log log = LogFactory.getLog(IndividualMaxRespAndConcDataProcessor.class);

  /**
   * Retrieve the max resp / max resp conc from the individual data record being processed.
   * 
   * @param individualProcessing Individual processing object.
   * @return The chosen max resp / max resp conc data from the individual data record (or null if
   *         no suitable data).
   * @throws IllegalArgumentException If null individual processing arg provided.
   * @throws InvalidValueException If invalid value encountered during processing.
   */
  public MaxRespConcData individualDataMaxRespAndConc(final IndividualProcessing individualProcessing)
                                                      throws IllegalArgumentException,
                                                             InvalidValueException {
    if (individualProcessing == null) {
      throw new IllegalArgumentException("Cannot retrieve a max resp and conc from a null Individual processing object!");
    }
    log.debug("~individualDataMaxRespAndConc() : Invoked.");

    final IndividualDataRecord individualDataRecord = individualProcessing.getIndividualData();
    final MaxRespConcData maxRespConcData = MaxRespDataSelector.chooseMaxRespData(individualDataRecord.eligibleMaxRespData());
    if (maxRespConcData != null) {
      log.debug("~individualDataMaxRespAndConc() : Selected '" + maxRespConcData.toString() + "'.");
    }

    return maxRespConcData;
  }
}