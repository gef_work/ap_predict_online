/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.business_manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.service.InformationService;
import uk.ac.ox.cs.epsrc.business_manager.service.SimulationService;
import uk.ac.ox.cs.epsrc.business_manager.value.object.InformationVO;
import uk.ac.ox.cs.epsrc.business_manager.ws.business_manager.request_validator.ProgressValidatorChain;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ObjectFactory;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProgressRecordStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProgressRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProgressResponse;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidCompoundIdentifierException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidSimulationIdException;

/**
 * Progress request processing.
 *
 * @author geoff
 */
public class ProgressRequestProcessor {

  /**
   * Identification means by which progress is being requested.
   */
  public enum REQUEST_BY {
    SIMULATION_ID
  }

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_INFORMATION_SERVICE)
  private InformationService informationService;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE)
  private SimulationService simulationService;

  private ProgressValidatorChain validatorChain;

  // Makes web service JAXB objects available.
  private static final ObjectFactory objectFactory = new ObjectFactory();

  private static final Log log = LogFactory.getLog(ProgressRequestProcessor.class);

  /** Default constructor */
  protected ProgressRequestProcessor() {}

  /**
   * Initialising constructor.
   * 
   * @param progressValidatorChain Validator chain to handle validation of incoming request.
   */
  public ProgressRequestProcessor(final ProgressValidatorChain progressValidatorChain) {
    validatorChain = progressValidatorChain; 
  }

  /**
   * Process a progress request.
   * 
   * @param progressRequest Incoming progress request.
   * @return Corresponding response.
   * @throws InvalidSimulationIdException If an invalid simulation id is encountered.
   * @throws InvalidCompoundIdentifierException If an invalid compound name is encountered.
   */
  public ProgressResponse processProgressRequest(final ProgressRequest progressRequest)
                                                 throws InvalidCompoundIdentifierException,
                                                        InvalidSimulationIdException {
    final String logPrefix = "~processProgressRequest() : ";
    log.debug(logPrefix.concat("Invoked."));

    final List<Long> simulationIds = progressRequest.getSimulationIds();
    final boolean hasSimulationIds = (simulationIds != null && !simulationIds.isEmpty());

    REQUEST_BY requestBy = null;
    final List<Object> identifiers = new ArrayList<Object>();
    if (hasSimulationIds) {
      log.debug(logPrefix.concat("Requested progress by simulation id."));
      requestBy = REQUEST_BY.SIMULATION_ID;
      identifiers.addAll(simulationIds);
    } else {
      final String warn = "No simulation ids used in progress request.";
      log.warn(logPrefix.concat(warn));
    }

    validatorChain.progressValidation(progressRequest, requestBy);

    final ProgressResponse progressResponse = objectFactory.createProgressResponse();

    if (requestBy != null) {
      switch (requestBy) {
        case SIMULATION_ID :
          for (final Object identifier : identifiers) {
            long simulationId;
            try {
              simulationId = Long.valueOf(identifier.toString());
            } catch (NumberFormatException e) {
              log.error(logPrefix.concat("Inappropriate simulation id value of '" + identifier + "' received!"));
              continue;
            }
            log.debug(logPrefix.concat("[") + simulationId + "] : ".concat("Retrieving progress from information service."));
            final InformationVO information = informationService.retrieveProgress(simulationId);
            final ProgressRecordStructure progressRecordStructure = loadStructure(information);

            progressResponse.getProgressRecordStructure().add(progressRecordStructure);
          }
          break;
        default :
          final String errorMessage = "Unrecognised provenance request selector '" + requestBy + "'.";
          log.error(logPrefix.concat(errorMessage));
          throw new UnsupportedOperationException(errorMessage);
      }
    }

    return progressResponse;
  }

  // load the JAXB WS object with information data.
  private ProgressRecordStructure loadStructure(final InformationVO information) {
    log.debug("~loadStructure() : Invoked.");
    final ProgressRecordStructure progressRecordStructure = objectFactory.createProgressRecordStructure();

    if (information != null) {
      progressRecordStructure.setSimulationId(information.getSimulationId());
      progressRecordStructure.setBusinessManagerProgress(information.getInformation());
      final Set<String> appManagerJobProgress = information.getAppManagerJobProgress();
      for (final String eachAppManagerJobProgress : appManagerJobProgress) {
        progressRecordStructure.getAppManagerJobProgress().add(eachAppManagerJobProgress);
      }
    }

    return progressRecordStructure;
  }
}
