/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.checker;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.SimulationProcessingGateway;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.config.ConfigurationActionPotential;
import uk.ac.ox.cs.epsrc.business_manager.entity.PIC50Data;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Determine if the experimental data for a simulation has changed.
 *
 * @author geoff
 */
public class ExperimentalDataChange extends AbstractChangeChecker {

  private static final int defaultScale = PIC50Data.DEFAULT_SCALE;
  private static final RoundingMode defaultRoundingMode = PIC50Data.DEFAULT_ROUNDING_MODE;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JMS_TEMPLATE)
  private BusinessManagerJMSTemplate jmsTemplate;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_CONFIGURATION_ACTIONPOTENTIAL)
  private ConfigurationActionPotential configurationActionPotential;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_SIMULATION_MANAGER)
  private SimulationManager simulationManager;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_SIMULATION_PROCESSING_GATEWAY)
  private SimulationProcessingGateway simulationProcessingGateway;

  private static final Log log = LogFactory.getLog(ExperimentalDataChange.class);

  /**
   * Initialising constructor.
   * 
   * @param order Order of invocation.
   */
  public ExperimentalDataChange(final int order) {
    super(order);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.business.checker.ChangeChecker#getNature()
   */
  @Override
  public String getNature() {
    return MessageKey.SIMULATION_CHANGE_EXPERIMENTAL.getBundleIdentifier();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.business.checker.ChangeChecker#requiresReRun(uk.ac.ox.cs.epsrc.business_manager.entity.Simulation)
   */
  @Override
  public boolean requiresReRun(final Simulation simulation) {
    final long simulationId = simulation.getId();
    final String identifiedLogPrefix = "~requiresReRun() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    /*
     * TODO: Avoid calling simulation processing gateway multiple times in different change checkers.
     * TODO: Relying on just per-frequency concentration data to determine changes in "experimental"
     *       data. Should really be recording all experimental data values at time of simulation!?
     * 
     * We are however relying on the simulation processing gateway (rather than an
     *  ExperimentalDataService) because there is no guarantee that there is any experimental data
     *  as its presence is site-specific.
     */

    // Retrieve the per-frequency concentrations data which would be used if the simulation were to run now.
    final ProcessedDataVO processedData = simulationProcessingGateway.inputDataGathering(simulation);
    if (processedData == null) {
      jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.DEBUG,
                                                           "Input data gathering timed out.",
                                                           simulationId).build());
      log.warn(identifiedLogPrefix.concat("SimulationProcessingGateway input data gathering returned null! Filtered business_manager.input_data_gathering.timeout set too low?"));
      return false;
    }
    // Only experimental per-frequency concentrations are returned by input data gathering.
    Map<BigDecimal, Set<BigDecimal>> newPerFrequencyConcentrations = processedData.getPerFrequencyConcentrations();
    boolean usingConfiguredPerFrequencyConcentrations = false;
    if (newPerFrequencyConcentrations.isEmpty()) {
      // No experimental per-frequency concentrations, so use current configuration-defined per-frequency concs.
      newPerFrequencyConcentrations = configurationActionPotential.getDefaultPerFrequencyConcentrations();
      usingConfiguredPerFrequencyConcentrations = true;
    }

    // Retrieve the per-frequency concentrations data used on the simulation's previous invocation.
    final Map<BigDecimal, Set<BigDecimal>> previousPerFrequencyConcentrations = simulationManager.retrievePerFrequencyConcentrations(simulationId);

    final boolean experimentalDataDiffers = experimentalDataDiffers(newPerFrequencyConcentrations,
                                                                    previousPerFrequencyConcentrations,
                                                                    usingConfiguredPerFrequencyConcentrations,
                                                                    simulationId);
    String message = "Experimental data re-run checked : ";
    if (experimentalDataDiffers) {
      message += "Experimental data differs.";
      // No need to send progress as progress is going to get purged!
    } else {
      message += "No change in experimental data.";
      jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.DEBUG, message,
                                                           simulationId).build());
    }
    log.debug(identifiedLogPrefix.concat(message));

    return experimentalDataDiffers;
  }

  /**
   * Determine if experimental data values differ between current and previous runs.
   * 
   * @param newExperimentalData Current experimental data.
   * @param previousExperimentalData Previous experimental data.
   * @param usingConfiguredPerFrequencyConcentrations {@code true} if using 
   *                                                  {@linkplain ConfigurationActionPotential}-assigned
   *                                                  per-frequency concentrations.
   * @param simulationId Simulation identifier. 
   * @return {@code true} if experimental data differs, otherwise {@code false}.
   */
  protected static boolean experimentalDataDiffers(final Map<BigDecimal, Set<BigDecimal>> newExperimentalData,
                                                   final Map<BigDecimal, Set<BigDecimal>> previousExperimentalData,
                                                   final boolean usingConfiguredPerFrequencyConcentrations,
                                                   final long simulationId) {
    final String identifiedLogPrefix = "~experimentalDataDiffers() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked. New '" + newExperimentalData + "', previous '" + previousExperimentalData + "'."));

    if (usingConfiguredPerFrequencyConcentrations && previousExperimentalData.isEmpty()) {
      log.debug(identifiedLogPrefix.concat("No previous experimental data, and current data contains only default per-frequency concentrations."));
      return false;
    }

    final int newPerFrequencyCount = newExperimentalData.size();
    final int previousPerFrequencyCount = previousExperimentalData.size();

    if (newPerFrequencyCount != previousPerFrequencyCount) {
      log.debug(identifiedLogPrefix.concat("Per frequency count now '" + newPerFrequencyCount + "', previously '" + previousPerFrequencyCount + "'."));
      return true;
    }

    for (final BigDecimal eachNewPerFrequency : newExperimentalData.keySet()) {
      BigDecimal matchedPreviousPerFrequency = null;
      log.debug(identifiedLogPrefix.concat("Looking for new frequency '" + eachNewPerFrequency + "'."));
      for (final BigDecimal eachPreviousPerFrequency : previousExperimentalData.keySet()) {
        log.debug(identifiedLogPrefix.concat("Check against previous frequency '" + eachPreviousPerFrequency + "'."));
        if (eachNewPerFrequency.setScale(defaultScale, defaultRoundingMode).compareTo
           (eachPreviousPerFrequency.setScale(defaultScale, defaultRoundingMode)) == 0) {
          log.debug(identifiedLogPrefix.concat("Matched '" + eachPreviousPerFrequency + "'."));
          matchedPreviousPerFrequency = eachPreviousPerFrequency;
          break;
        }
      }
      if (matchedPreviousPerFrequency != null) {
        final Set<BigDecimal> newConcentrations = newExperimentalData.get(eachNewPerFrequency);
        final int newConcentrationsCount = newConcentrations.size();
        final Set<BigDecimal> previousConcentrations = previousExperimentalData.get(matchedPreviousPerFrequency);
        final int previousConcentrationsCount = previousConcentrations.size();

        if (newConcentrationsCount != previousConcentrationsCount) {
          log.debug(identifiedLogPrefix.concat("Concentrations imbalance between new '" + newConcentrations + "', and previous '" + previousConcentrations + "'."));
          return true;
        }

        for (final BigDecimal eachNewConcentration : newConcentrations) {
          log.debug(identifiedLogPrefix.concat("Looking for new concentration '" + eachNewConcentration + "'."));
          boolean found = false;
          for (final BigDecimal eachPreviousConcentration : previousConcentrations) {
            log.debug(identifiedLogPrefix.concat("Check against previous concentration '" + eachPreviousConcentration + "'."));
            if (eachNewConcentration.setScale(defaultScale, defaultRoundingMode).compareTo(
                eachPreviousConcentration.setScale(defaultScale, defaultRoundingMode)) == 0) {
              found = true;
              break;
            }
          }
          if (!found) {
            log.debug(identifiedLogPrefix.concat("Concentrations irregularity between new '" + newConcentrations + "', and previous '" + previousConcentrations + "'."));
            return true;
          }
        }
      } else {
        log.debug(identifiedLogPrefix.concat("Couldn't find frequency '" + eachNewPerFrequency + "', in '" + previousExperimentalData.keySet() + "'."));
        return true;
      }
    }

    return false;
  }
}