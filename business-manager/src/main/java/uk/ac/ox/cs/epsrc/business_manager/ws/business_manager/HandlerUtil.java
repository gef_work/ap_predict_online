/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.business_manager;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProcessSimulationsRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.SimulationDetail;

/**
 * Utility class for request handlers.
 * 
 * @author geoff
 */
public class HandlerUtil {

  private static final Log log = LogFactory.getLog(HandlerUtil.class);

  /**
   * Generic tidying of incoming request array objects.
   * 
   * @param incoming Incoming array request object.
   * @return Tidied {@code LinkedHashSet} representation. 
   */
  public static Set<String> removeDuplicatesNullAndTrim(final List<String> incoming) {
    log.debug("~removeDuplicatesNullAndTrim() : Invoked.");
    assert (incoming != null) : "Inappropriate attempt to tidy a null List object.";

    final Set<String> tidiedIncoming = new LinkedHashSet<String>(incoming.size());
    for (final String value : incoming) {
      if (value == null) {
        continue;
      }
      tidiedIncoming.add(value.trim());
    }

    return tidiedIncoming;
  }

  /**
   * Retrieve compound names from request objects exactly as they appear in the order in the
   * request object.
   * 
   * @param requestObject Request object.
   * @return Compound names.
   */
  public static List<String> retrieveOrderedRawCompoundIdentifiers(final Object requestObject) {
    log.debug("~retrieveOrderedRawCompoundIdentifiers() : Invoked.");
    assert (requestObject != null) : "Inappropriate attempt to retrieve raw compound names from a null parameter object.";

    final List<String> compoundIdentifiers = new ArrayList<String>();
    if (requestObject instanceof ProcessSimulationsRequest) {
      final ProcessSimulationsRequest processSimulationsRequest = (ProcessSimulationsRequest) requestObject;
      for (final SimulationDetail simulationDetail : processSimulationsRequest.getSimulationDetail()) {
        compoundIdentifiers.add(simulationDetail.getCompoundIdentifier());
      }
    } else {
      final String errorMessage = "Unknown instance of '" + requestObject.getClass() + "' when retrieving compound names.";
      log.error("~retrieveOrderedRawCompoundIdentifiers() : ".concat(errorMessage));
      throw new UnsupportedOperationException(errorMessage);
    }

    return compoundIdentifiers;
  }

  /**
   * Extract specific content from incoming request objects and put it through the 
   * {@link #removeDuplicatesNullAndTrim(List)} process.
   * 
   * @param requestObject Request object.
   * @return Tidied content, e.g. simulation ids, compound names.
   */
  public static Set<String> tidyIncoming(final Object requestObject) {
    log.debug("~tidyIncoming() : Invoked.");
    assert (requestObject != null) : "Inappropriate attempt to tidy an incoming null-valued request object";
    final String objectName = requestObject.getClass().getName();

    final List<String> toTidy = new ArrayList<String>();
    if (requestObject instanceof ProcessSimulationsRequest) {
      toTidy.addAll(retrieveOrderedRawCompoundIdentifiers(requestObject));
    } else {
      throw new UnsupportedOperationException("Unrecognised request object of type '" + objectName + "' for tidying.");
    }

    final Set<String> uniqueToTidy = removeDuplicatesNullAndTrim(toTidy);
    if (toTidy.size() != uniqueToTidy.size()) {
      log.info("~tidyIncoming() : Duplicated '" + objectName + "' content detected and removed prior to onward processing.");
    }

    return uniqueToTidy;
  }
}