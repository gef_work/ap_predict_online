/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.checker;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.SimulationProcessingGateway;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.entity.GroupedInvocationInput;
import uk.ac.ox.cs.epsrc.business_manager.entity.IonChannelValues;
import uk.ac.ox.cs.epsrc.business_manager.entity.PIC50Data;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.manager.ResultsManager;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Determine if the input values data for a simulation has changed.
 *
 * @author geoff
 */
public class InputValueChange extends AbstractChangeChecker {

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JMS_TEMPLATE)
  private BusinessManagerJMSTemplate jmsTemplate;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_RESULTS_MANAGER)
  private ResultsManager resultsManager;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_SIMULATION_PROCESSING_GATEWAY)
  private SimulationProcessingGateway simulationProcessingGateway;

  private static final Log log = LogFactory.getLog(InputValueChange.class);

  /**
   * Initialising constructor.
   * 
   * @param order Order of invocation.
   */
  public InputValueChange(final int order) {
    super(order);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.business.checker.ChangeChecker#getNature()
   */
  @Override
  public String getNature() {
    return MessageKey.SIMULATION_CHANGE_INPUT_VALUES.getBundleIdentifier();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.business.checker.ChangeChecker#requiresReRun(uk.ac.ox.cs.epsrc.business_manager.entity.Simulation)
   */
  @Override
  public boolean requiresReRun(final Simulation simulation) {
    final long simulationId = simulation.getId();
    final String identifiedLogPrefix = "~requiresReRun() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    // Retrieve the input data values which would be used if the simulation were to run now.
    final ProcessedDataVO processedData = simulationProcessingGateway.inputDataGathering(simulation);
    if (processedData == null) {
      jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.DEBUG,
                                                           "Input data gathering timed out.",
                                                           simulationId).build());
      log.warn(identifiedLogPrefix.concat("SimulationProcessingGateway input data gathering returned null! Filtered business_manager.input_data_gathering.timeout set too low?"));
      return false;
    }
    final Set<GroupedInvocationInput> newSimulationInputs = processedData.getProcessedData();
    final int newCount = newSimulationInputs.size();

    // Retrieve the input data values used on the simulation's previous invocation.
    final List<GroupedInvocationInput> previousSimulationInputs = resultsManager.retrieveInputValuesEntities(simulationId);
    final int previousCount = previousSimulationInputs.size();

    if (newCount != previousCount) {
      final String message = "Input value data count previously '" + previousCount + "', now '" + newCount + "'.";
      jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.INFO, message,
                                                           simulationId).build());
      log.debug(identifiedLogPrefix.concat(message));
      return true;
    }

    final boolean inputValuesDiffer = inputValuesDiffer(newSimulationInputs,
                                                        previousSimulationInputs);
    String message = "Input value data re-run checked : ";
    if (inputValuesDiffer) {
      message += "Input values differ.";
      // No need to send progress as progress is going to get purged!
    } else {
      message += "No change in input values.";
      jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.DEBUG, message,
                                                           simulationId).build());
    }
    log.debug(identifiedLogPrefix.concat(message));

    return inputValuesDiffer;
  }

  /*
   * Determine if simulation input values differ between current and previous runs.
   * <p>
   * The two collections must be of equal size.
   * 
   * @param newSimulationInputs Current simulation input values.
   * @param previousSimulationInputs Previous simulation input values.
   * @return {@code true} if simulation input values differ, otherwise {@code false}.
   */
  private static boolean inputValuesDiffer(final Set<GroupedInvocationInput> newSimulationInputs,
                                           final List<GroupedInvocationInput> previousSimulationInputs) {
    log.debug("~inputValuesDiffer() : Invoked.");

    for (final GroupedInvocationInput eachNewSimulationInput : newSimulationInputs) {
      final Short newAssayGroupLevel = eachNewSimulationInput.getAssayGroupLevel();
      final String newAssayGroupName = eachNewSimulationInput.getAssayGroupName();
      final Set<IonChannelValues> newIonChannelValues = eachNewSimulationInput.getIonChannelValues();
      final int newIonChannelValuesCount = newIonChannelValues.size();

      boolean matched = false;
      for (final GroupedInvocationInput eachPreviousSimulationInput : previousSimulationInputs) {
        final Short previousAssayGroupLevel = eachPreviousSimulationInput.getAssayGroupLevel();
        final String previousAssayGroupName = eachPreviousSimulationInput.getAssayGroupName();
        final Set<IonChannelValues> previousIonChannelValues = eachPreviousSimulationInput.getIonChannelValues();
        final int previousIonChannelValuesCount = previousIonChannelValues.size();

        if (previousAssayGroupLevel == newAssayGroupLevel &&
            previousAssayGroupName.equals(newAssayGroupName) &&
            previousIonChannelValuesCount == newIonChannelValuesCount) {
          log.debug("~inputValuesDiffer() : Match on group name/level/ion channel cnt for '" + newAssayGroupName + "', '" + newAssayGroupLevel + "'.");
          if (ionChannelValuesSame(newIonChannelValues, previousIonChannelValues)) {
            matched = true;
          }

          break;
        }
      }

      if (!matched) {
        log.debug("~inputValuesDiffer() : No identical match found for '" + newAssayGroupName + "', '" + newAssayGroupLevel + "'.");
        return true;
      }
    }

    return false;
  }

  /*
   * Determine if ion channel values collections are the same between current and previous run.
   * 
   * @param newIonChannelValues Current ion channel values data.
   * @param previousIonChannelValues Previous ion channel values data.
   * @return {@code true} if ion channel values data same, otherwise {@code false}.
   */
  private static boolean ionChannelValuesSame(final Set<IonChannelValues> newIonChannelValues,
                                              final Set<IonChannelValues> previousIonChannelValues) {
    log.debug("~ionChannelValuesSame() : Invoked.");

    if (newIonChannelValues.size() != previousIonChannelValues.size()) {
      return false;
    }

    for (final IonChannelValues eachNewIonChannelValues : newIonChannelValues) {
      final String newIonChannelName = eachNewIonChannelValues.getIonChannelName();
      final List<PIC50Data> newPIC50Data = eachNewIonChannelValues.getpIC50Data();
      final String newSourceAssayName = eachNewIonChannelValues.getSourceAssayName();
      final int newPIC50DataCount = newPIC50Data.size();

      boolean matched = false;
      // Traverse the previous ion channel values to find the latest in the previous collection.
      for (final IonChannelValues eachPreviousIonChannelValues : previousIonChannelValues) {
        final String previousIonChannelName = eachPreviousIonChannelValues.getIonChannelName();
        final List<PIC50Data> previousPIC50Data = eachPreviousIonChannelValues.getpIC50Data();
        final String previousSourceAssayName = eachPreviousIonChannelValues.getSourceAssayName();
        final int previousPIC50DataCount = previousPIC50Data.size();

        if (previousIonChannelName.equals(newIonChannelName) &&
            previousSourceAssayName.equals(newSourceAssayName) &&
            previousPIC50DataCount == newPIC50DataCount) {
          log.debug("~ionChannelValuesSame() : Match on ion channel name/source assay/pIC50 data cnt for '" + newIonChannelName + "', '" + newSourceAssayName + "'.");
          if (pIC50DataSame(newPIC50Data, previousPIC50Data)) {
            matched = true;
            break;
          }
        }
      }

      if (!matched) {
        log.debug("~ionChannelValuesSame() : No identical match found for '" + newIonChannelName + "', '" + newSourceAssayName + "'.");
        return false;
      }
    }

    log.debug("~ionChannelValuesSame() : No change in IonChannelValues objects.");
    return true;
  }

  /**
   * Determine if pIC50 data collections are the same between current and previous run.
   * <p>
   * This doesn't do a precise check against previous and current as the precise origin of pIC50
   * data is not retained, so we check against obvious differences, e.g. a different number of 
   * unequal {@link PIC50Data} objects between then and now, or a new or missing PIC50Data object.
   * 
   * @param newPIC50Data Current pIC50 data.
   * @param previousPIC50Data Previous pIC50 data.
   * @return {@code true} if pIC50 data same, otherwise {@code false}.
   */
  // TODO: Improve accuracy of comparing previous against current PIC50 data. 
  private static boolean pIC50DataSame(final List<PIC50Data> newPIC50Data,
                                       final List<PIC50Data> previousPIC50Data) {
    log.debug("~pIC50DataSame() : Invoked.");

    if (newPIC50Data.size() != previousPIC50Data.size()) {
      return false;
    }

    // Using HashSet equality, see if count of unique PIC50Data objects has changed.
    final Set<PIC50Data> newPIC50DataSet = new HashSet<PIC50Data>(newPIC50Data);
    final Set<PIC50Data> previousPIC50DataSet = new HashSet<PIC50Data>(previousPIC50Data);

    if (newPIC50DataSet.size() != previousPIC50DataSet.size()) {
      log.debug("~pIC50DataSame() : Indications (based on PIC50Data equality) are that the data has changed.");
      return false;
    }

    // Same number of unique PIC50Data objects previously and currently, so a new one has appeared.
    for (final PIC50Data eachNewPIC50Data : newPIC50DataSet) {
      boolean matched = false;
      log.debug("~pIC50DataSame() : Seeking PIC50Data '" + eachNewPIC50Data + "'.");
      for (final PIC50Data eachPreviousPIC50Data : previousPIC50DataSet) {
        if (eachPreviousPIC50Data.equals(eachNewPIC50Data)) {
          log.debug("~pIC50DataSame() : Matched against previous PIC50Data '" + eachPreviousPIC50Data + "'.");
          matched = true;
          break;
        }
      }
      if (!matched) {
        log.debug("~pIC50DataSame() : Couldn't find a match for a new PIC50Data object '" + eachNewPIC50Data + "' in previous PIC50Data objects.");
        return false;
      }
    }

    log.debug("~pIC50DataSame() : No change in PIC50Data objects.");
    return true;
  }
}