/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.manager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.AbstractProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.JobProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.AbstractProvenance;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.DoseResponseProvenance;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.FundamentalProvenance;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.IndividualProvenance;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.Provenance;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.SummaryProvenance;
import uk.ac.ox.cs.epsrc.business_manager.dao.information.InformationDAO;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.DoseResponseData;
import uk.ac.ox.cs.epsrc.business_manager.value.object.InformationVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Implementation of the information, e.g. provenance, progress, management.
 *
 * @author geoff
 */
@Component(BusinessIdentifiers.COMPONENT_INFORMATION_MANAGER)
public class InformationManagerImpl implements InformationManager {

  // information data access object
  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_ORIENT_DAO)
  private InformationDAO informationDAO;

  private static final Log log = LogFactory.getLog(InformationManagerImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager#purge(long)
   */
  @Override
  public void purge(final long simulationId) {
    log.debug("~purge() : Invoked for simulation id '" + simulationId + "'.");
    informationDAO.purge(simulationId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager#recordProgress(uk.ac.ox.cs.epsrc.business_manager.business.progress.Progress)
   */
  @Override
  public void recordProgress(final Progress progress) {
    final String identifiedLogPrefix = "~recordProgress() : [" + ((AbstractProgress) progress).getSimulationId() + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked : '" + progress.toString() + "'."));

    final AbstractProgress abstractProgress = (AbstractProgress) progress;
    final Date timestamp = abstractProgress.getTimestamp();
    final InformationLevel level = abstractProgress.getLevel();
    final String text = abstractProgress.getText();

    if (timestamp == null || level == null || text == null) {
      final String warn = "Invalid null timestamp, level or text in progress '" + progress.toString() + "'.";
      log.warn(identifiedLogPrefix.concat(warn));
    }
    final List<String> args = abstractProgress.getArgs();

    if (progress instanceof OverallProgress) {
      final OverallProgress overallProgress = (OverallProgress) progress;
      final Long simulationId = overallProgress.getSimulationId();

      if (overallProgress.hasJustBeenCreated()) {
        informationDAO.createProgressAll(overallProgress.getCompoundIdentifier(), simulationId,
                                         timestamp, level, text, args);
      } else {
        informationDAO.updateProgressAll(simulationId, timestamp, level, text, args);
      }
      if (overallProgress.isTerminating()) {
        // Append a terminating marker.
        informationDAO.updateProgressAll(simulationId, timestamp, InformationLevel.TRACE,
                                         MessageKey.SYSTEM_TERMINATION_KEY.getBundleIdentifier(),
                                         new ArrayList<String>());
      }
    } else if (progress instanceof JobProgress) {
      final JobProgress jobProgress = (JobProgress) progress;
      final long jobId = jobProgress.getJobId();

      if (jobProgress.hasJustBeenCreated()) {
        log.debug(identifiedLogPrefix.concat("1. First job progress for this jobId/simulationId combination."));
        informationDAO.createProgressJob(jobProgress.getSimulationId(), jobId,
                                         jobProgress.getGroupName(), jobProgress.getGroupLevel(),
                                         jobProgress.getPacingFrequency(), timestamp, level, text,
                                         args);
      } else {
        log.debug(identifiedLogPrefix.concat("2. Appending job progress to existing jobId/simulationId combination."));
        try {
          informationDAO.updateProgressJob(jobId, timestamp, level, text, args);
        } catch (IllegalStateException e) {
          final String errorMessage = "Job [" + jobId + "] : Illegal db state encountered'" + e.getMessage() + "'.";
          log.error(identifiedLogPrefix.concat(errorMessage));
        }
      }
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager#recordProvenance(uk.ac.ox.cs.epsrc.business_manager.business.provenance.Provenance)
   */
  @Override
  public void recordProvenance(final Provenance provenance) {
    final String logPrefix = "~recordProvenance() : ";
    log.trace(logPrefix.concat("Invoked."));

    final AbstractProvenance abstractProvenance = (AbstractProvenance) provenance;
    final InformationLevel level = abstractProvenance.getLevel();
    final String text = abstractProvenance.getText();
    final List<String> args = abstractProvenance.getArgs();

    if (level == null || text == null) {
      final String warn = "Invalid null level or text in provenance '" + provenance.toString() + "'.";
      log.warn(logPrefix.concat(warn));
    }

    if (provenance instanceof FundamentalProvenance) {
      final FundamentalProvenance fundamental = (FundamentalProvenance) provenance;
      final String compoundIdentifier = fundamental.getCompoundIdentifier();
      final Long simulationId = fundamental.getSimulationId();

      // TODO : Provisionally used to differentiate logging messages
      final boolean hasCompoundName = compoundIdentifier != null;

      if (simulationId != null) {
        if (hasCompoundName) {
          /* This should be called at least once for a simulation to associate the compound with the
               simulation id. */
          log.trace(logPrefix.concat("1. Compound name + Simulation Id."));
          informationDAO.writeFundamental(compoundIdentifier, simulationId, level, text, args);
        } else {
          // Generally only a simulation identifier needs to be used.
          log.trace(logPrefix.concat("2. Simulation id."));
          informationDAO.writeFundamental(null, simulationId, level, text, args);
        }
      } else {
        String errorMessage;
        if (hasCompoundName) {
          errorMessage = "Fundamental write/update requires more than just a compound name.";
        } else {
          errorMessage = "Unexpected fundamental provenance recording instruction of '" + provenance.toString() + "'.";
        }
        log.error(logPrefix.concat(errorMessage));
        throw new UnsupportedOperationException(errorMessage);
      }
    } else if (provenance instanceof SummaryProvenance) {
      final SummaryProvenance summary = (SummaryProvenance) provenance;
      final Long simulationId = summary.getSimulationId();
      final String assayName = summary.getAssayName();
      final String ionChannelName = summary.getIonChannelName();
      final String summaryDataId = summary.getSummaryDataId();
      final List<BigDecimal> pIC50s = summary.getpIC50s();

      if (assayName != null && ionChannelName != null) {
        // Create summary linked to simulation
        log.trace(logPrefix.concat("4. Summary Id + Simulation Id + Assay/IonChannel"));
        informationDAO.writeSummary(simulationId, summaryDataId, assayName,
                                    ionChannelName, summary.getRawData(), level, text, args);
      } else if (!pIC50s.isEmpty()) {
        log.trace(logPrefix.concat("5. Summay Id + pIC50s"));
        informationDAO.updateSummary(simulationId, summaryDataId, level, text, args, pIC50s,
                                     summary.getHillCoefficient());
      } else {
        log.trace(logPrefix.concat("6. Summary Id"));
        informationDAO.updateSummary(simulationId, summaryDataId, level, text, args);
      }
    } else if (provenance instanceof IndividualProvenance) {
      final IndividualProvenance individual = (IndividualProvenance) provenance;

      final String assayName = individual.getAssayName();
      final String individualDataId = individual.getIndividualDataId();
      final String ionChannelName = individual.getIonChannelName();
      final Map<String, Object> rawData = individual.getRawData();
      final BigDecimal rawHill = individual.getRawHill();
      final List<BigDecimal> pIC50s = individual.getpIC50s();
      final String summaryDataId = individual.getSummaryDataId();
      final DoseResponseData doseResponseData = individual.getDoseResponseData();

      final Long simulationId = individual.getSimulationId();

      if (summaryDataId != null) {
        // create individual linked to summary)
        log.trace(logPrefix.concat("7. Summary Id + Individual Id."));
        informationDAO.writeIndividual(simulationId, summaryDataId, individualDataId, rawHill,
                                       rawData, level, text, args);
      } else if (assayName != null && ionChannelName != null) {
        // create individual linked to simulation
        log.trace(logPrefix.concat("8. Individual Id + Simulation Id."));
        informationDAO.writeIndividual(simulationId, individualDataId, assayName, ionChannelName,
                                       rawHill, rawData, level, text, args);
      } else if (!pIC50s.isEmpty()) {
        log.trace(logPrefix.concat("9. Individual Id + pIC50."));
        informationDAO.updateIndividual(simulationId, individualDataId, level, text, args,
                                        pIC50s.get(0), individual.getWorkflowHill());
      } else if (doseResponseData != null) {
        if (individual.isRecordingDoseResponses()) {
          log.trace(logPrefix.concat("10 : Individual Id + Dose response data + Recording all values."));
          informationDAO.updateIndividual(simulationId, individualDataId, level, text, args,
                                          doseResponseData);
        } else {
          log.trace(logPrefix.concat("11 : Individual Id + Dose response data + Fitting strategy identifier."));
          informationDAO.updateIndividual(simulationId, individualDataId, level, text, args,
                                          doseResponseData,
                                          individual.getDrFittingStrategyIdentifier());
        }
      } else {
        log.trace(logPrefix.concat("12. Individual Id."));
        informationDAO.updateIndividual(simulationId, individualDataId, level, text, args);
      }
    } else if (provenance instanceof DoseResponseProvenance) {
      final DoseResponseProvenance doseResponse = (DoseResponseProvenance) provenance;
      final Long simulationId = doseResponse.getSimulationId();
      final String dose_in_uM = doseResponse.getDose_in_uM();
      final String doseResponseDataId = doseResponse.getDoseResponseDataId();
      final String individualDataId = doseResponse.getIndividualDataId();
      final String response = doseResponse.getResponse();

      if (individualDataId != null) {
        // Create dose-response linked to individual
        log.trace(logPrefix.concat("12. Individual Id + DoseResponse Id."));
        informationDAO.writeDoseResponse(simulationId, individualDataId, doseResponseDataId,
                                         doseResponse.getRawData(), level, text, args);
      } else if (dose_in_uM != null && response != null) {
        log.trace(logPrefix.concat("13 : Dose Response Id + dose-response values."));
        informationDAO.updateDoseResponse(simulationId, doseResponseDataId, level, text, args,
                                          dose_in_uM, response);
      } else {
        log.trace(logPrefix.concat("14 : Dose Response Id."));
        informationDAO.updateDoseResponse(simulationId, doseResponseDataId, level, text, args);
      }
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager#retrieveProgress(long)
   */
  @Override
  public InformationVO retrieveProgress(final long simulationId) {
    log.debug("~retrieveProgress() : [" + simulationId + "] : Invoked.");
    return informationDAO.retrieveProgress(simulationId, INFORMATION_FORMAT.JSON);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager#retrieveProvenance(long)
   */
  @Override
  public InformationVO retrieveProvenance(final long simulationId) {
    log.debug("~retrieveProvenance() : [" + simulationId + "] : Invoked.");
    return informationDAO.retrieveProvenance(simulationId, INFORMATION_FORMAT.JSON);
  }
}