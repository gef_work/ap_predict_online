/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.selection.c50;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.business.selection.ic.ICDataSelectionStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;

/**
 * Select the IC50 / pIC50 / pXC50 data read in from the raw data records according to the 
 * defined {@link ICDataSelectionStrategy} collection, e.g. {@link PC50DataAndEqualityModifier}.
 *
 * @author geoff
 */
public class C50DataSelector {

  private static final List<ICDataSelectionStrategy> icDataSelectionStrategies = new ArrayList<ICDataSelectionStrategy>();

  private static final Log log = LogFactory.getLog(C50DataSelector.class);

  static {
    icDataSelectionStrategies.add(new PC50DataAndEqualityModifier());
    icDataSelectionStrategies.add(new PC50DataAndNonEqualityModifier());
    icDataSelectionStrategies.add(new AnyIC50Data());
    icDataSelectionStrategies.add(new AnyC20Data());
  }

  /**
   * Retrieve the {@link ICData} from the eligible values according to the assigned selection
   * strategies.
   * <p>
   * It is expected but not mandated that the eligible IC data will arrive in order of priority,
   * e.g. that perhaps the pIC50/pXC50 values will precede IC50 values. This collection is then
   * passed to an ordered collection of selection strategies in sequence, e.g.
   * {@link PC50DataAndEqualityModifier}, {@link PC50DataAndNonEqualityModifier}, etc.
   * 
   * @param eligibleICData Eligible {@link ICData} data derived from the raw data record, or empty
   *                       collection if none available.
   * @return The first of the eligible {@link ICData} encountered which meets the criteria defined
   *         by the (prioritised) strategies, or <tt>null</tt> if none found.
   * @throws IllegalArgumentException If the <tt>eligibleICData</tt> arg is <code>null</code>
   */
  public static ICData selectFrom(final List<ICData> eligibleICData) {
    log.debug("~selectFrom() : Invoked.");
    if (eligibleICData == null) {
      final String message = "Null eligible IC data options passed so no selection made for assignment.";
      log.error("~selectFrom() : " + message);
      throw new IllegalArgumentException(message);
    }

    ICData ic50Data = null;
    ICData ic20Data = null;

    for (final ICDataSelectionStrategy icDataSelectionStrategy : icDataSelectionStrategies) {
      final ICPercent strategyPercent = icDataSelectionStrategy.retrievePctIC();
      if ((ic50Data != null && strategyPercent.compareTo(ICPercent.PCT50) == 0) ||
          (ic20Data != null && strategyPercent.compareTo(ICPercent.PCT20) == 0)) {
        // We've already got this pct ic data, try next one.
        continue;
      }

      // TODO : Sucky code! For each strategy it checks all eligible, even already selected ones!
      final ICData selected = icDataSelectionStrategy.select(eligibleICData);
      if (selected != null) {
        switch (strategyPercent) {
          case PCT20 :
            ic20Data = selected;
            break;
          case PCT50 :
            ic50Data = selected;
            break;
          default :
            throw new UnsupportedOperationException("Unrecognized IC percentage value of '" + strategyPercent + "' encountered!");
        }
        log.debug("~selectFrom() : For '" + strategyPercent + "' , selected '" + selected + "'");
      }

      if (ic20Data != null && ic50Data != null) {
        break;
      }
    }

    ICData icData = null;
    if (ic20Data == null && ic50Data == null) {
      // This is a valid outcome.
      log.info("~selectFrom() : No eligible 20 or 50 percent inhibitory concentration data found in raw data.");
    } else if (ic20Data != null && ic50Data != null) {
      log.debug("~selectFrom() : An ICData object with both 50 and 20 percent inhibitory concentration created.");
      icData = new ICDataVO(ic50Data, ic20Data);
    } else {
      icData = ic20Data == null ? ic50Data : ic20Data;
    }

    return icData;
  }

  /**
   * Overwrite the default C50 data selection strategies.
   * 
   * @param icDataSelectionStrategies New C50 data selection strategies.
   */
  public static void setC50DataSelectionStrategies(final List<ICDataSelectionStrategy> icDataSelectionStrategies) {
    if (icDataSelectionStrategies == null || icDataSelectionStrategies.isEmpty()) {
      final String message = "Invalid attempt to assign a null or empty collection of C50 data selection strategies.";
      log.error("~setC50DataSelectionStrategies() : " + message);
      throw new IllegalArgumentException(message);
    }
    log.warn("~setC50DataSelectionStratagies() : Overwriting the default C50 data selection strategies.");
    icDataSelectionStrategies.clear();
    icDataSelectionStrategies.addAll(icDataSelectionStrategies);
  }
}