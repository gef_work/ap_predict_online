/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.information.progress;

import java.io.Serializable;

/**
 * Marker interface for progress data.
 *
 * @author geoff
 */
public interface Progress extends Serializable {

  /**
   * Progress information identifiers.<br>
   * The message keys have corresponding client bundle identifier keys, e.g. "progress.job_created"
   * (which correspond to the messages to show in the client interface).
   */
  public enum MessageKey {
    AGGR_SITEDATA_ANALYSIS("progress.aggr_sitedata_analysis"),
    AGGR_SITEDATA_ANALYSIS_IRREGULAR("progress.aggr_sitedata_analysis_irregular"),
    AGGR_SITEDATA_COUNT("progress.aggr_sitedata_count"),
    AP_PREDICT_PREPROCESSING("progress.ap_predict_preprocessing"),
    APP_MANAGER_RUN_REQUEST_RESPONSE("appmanager.run_request_response"),
    APP_MANAGER_UNRECOGNISED_RESPONSE("appmanager.unrecognised_response"),
    ASSAY_VALUES_PERSISTED("progress.assay_values_persisted"),
    ASSAY_VALUES_PROCESSED_PREINVOCATION("progress.assay_values_processedpreinvocation"),
    BUSINESS_MANAGER_UNRECOGNISED_PROBLEM("businessmanager.unrecognised_problem"),
    DISTRIBUTING("progress.distributing"),
    JOB_CREATED("progress.job_created"),
    JOB_DATA_CREATION("progress.job_data_creation"),
    JOB_GENERATED_MESSAGES("progress.job_generated_messages"),
    JOB_RUN_FAILURE("cs_error.job_run_failure"),
    JOB_RESULTS_RECEIVED("progress.job_results_received"),
    MARK_CROSS("cs_general.mark_cross"),
    MARK_TICK("cs_general.mark_tick"),
    REQUEST_RECEIVED("progress.request_received"),
    RERUN_CHECK("progress.rerun_check"),
    RERUN_NOT_REQUIRED("progress.rerun_not_required"),
    RERUN_PROCESSING("progress.rerun_processing"),
    RERUN_REQUIRED("progress.rerun_required"),
    RESET_PROCESSING("progress.reset_processing"),
    SIMULATION_ALREADY_EXISTS("progress.simulation_already_exists"),
    SIMULATION_CHANGE_ADMINISTRATOR("progress.simulation_change_administrator"),
    SIMULATION_CHANGE_APPLICATION("progress.simulation_change_application"),
    SIMULATION_CHANGE_EXPERIMENTAL("progress.simulation_change_experimental"),
    SIMULATION_CHANGE_INPUT_VALUES("progress.simulation_change_input_values"),
    SIMULATION_CHANGED("progress.simulation_changed"),
    SIMULATION_DATA_COLLATED("progress.simulation_data_collated"),
    SIMULATION_INITIATION("progress.simulation_initiation"),
    SIMULATION_IDENTIFIER_CREATED("progress.simulation_id_created"),
    SIMULATION_STATE_PREPROCESSING("progress.simulation_state_preprocessing"),
    /* Indicator that the simulation is being stopped for some reason, i.e. db comms failure */
    SIMULATION_TERMINATING("progress.simulation_terminating"),
    SIMULATION_UNCHANGED("progress.simulation_unchanged"),
    SITEDATA_BREAKDOWN("progress.sitedata_breakdown"),
    SITEDATA_SCREENING_DATA_RETRIEVE_PROBLEM("progress.screening_data_retrieve_problem"),
    /* Indicator that the simulation processing is finishing */
    SYSTEM_TERMINATION_KEY("progress.system_termination_key");

    private final String bundleIdentifier;

    MessageKey(final String bundleIdentifier) {
      this.bundleIdentifier = bundleIdentifier;
    }

    /**
     * @return the bundleIdentifier
     */
    public String getBundleIdentifier() {
      return bundleIdentifier;
    }
  }
}
