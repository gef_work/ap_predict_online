/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.manager;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;

import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedSimulationRequestVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.SimulationRequestVO;

/**
 * Interface to simulation manager implementation.
 *
 * @author geoff
 */
public interface SimulationManager {


  /**
   * Create a new Simulation referring to the specified compound identifier.
   * <p>
   * This function does not persist the simulation, it merely creates the object
   * for persistence.
   * 
   * @param inputDataGathering {@code true} if the simulation is for input data
   *                           gathering purposes, otherwise {@code false}.
   * @param compoundIdentifier Compound identifier.
   * @param assayGrouping Whether simulation enacts assay grouping.
   * @param valueInheriting Whether simulation enacts value inheriting.
   * @param betweenGroups Whether simulation enacts value inheriting between
   *                      groups.
   * @param withinGroups Whether simulation enacts value inheriting within
   *                     groups.
   * @param pc50EvaluationStrategies PC50 evaluation strategies.
   * @param doseResponseFittingStrategy Dose-response fitting strategy, e.g.
   *                                    {@code IC50_ONLY}.
   * @param doseResponseFittingRounding Whether rounding of value to pIC50 0
   *                                    (zero) should occur.
   * @param doseResponseFittingHillMinMax {@code true} if Hill min/max fitting,
   *                                      otherwise {@code false}. 
   * @param doseResponseFittingHillMax Max. Hill Coefficient value (or
   *                                   {@code null} if not applicable).
   * @param doseResponseFittingHillMin Min. Hill Coefficient value (or
   *                                   {@code null} if not applicable).
   * @param cellModelIdentifier CellML Model identifier.
   * @param pacingMaxTime Maximum pacing time.
   * @param userId (Optional) User identifier.
   * @return Created simulation.
   */
  Simulation createNewSimulation(boolean inputDataGathering,
                                 String compoundIdentifier,
                                 boolean assayGrouping,
                                 boolean valueInheriting, boolean betweenGroups,
                                 boolean withinGroups,
                                 String pc50EvaluationStrategies,
                                 String doseResponseFittingStrategy,
                                 boolean doseResponseFittingRounding,
                                 boolean doseResponseFittingHillMinMax,
                                 Float doseResponseFittingHillMax,
                                 Float doseResponseFittingHillMin,
                                 short cellModelIdentifier,
                                 BigDecimal pacingMaxTime, String userId);

  /**
   * Retrieve a simulation with the specified id.
   * 
   * @param simulationId Persistence simulation id.
   * @return Simulation corresponding to simulation id, or {@code null} if not
   *         found.
   */
  Simulation find(long simulationId);

  /**
   * Retrieve simulation according to request parameters.
   * 
   * @param simulationRequest Simulation request.
   * @return Simulation corresponding to request data, otherwise {@code null} if
   *         not found.
   */
  Simulation find(SimulationRequestVO simulationRequest);

  /**
   * Retrieve a simulation according to all the simulation properties which
   * makes it unique.
   * 
   * @param compoundIdentifier Compound identifier.
   * @param assayGrouping Whether simulation enacts assay grouping.
   * @param valueInheriting Whether simulation enacts value inheriting.
   * @param betweenGroups Whether simulation enacts value inheriting between
   *                      groups.
   * @param withinGroups Whether simulation enacts value inheriting within
   *                     groups.
   * @param pc50EvaluationStrategies PC50 evaluation strategies.
   * @param doseResponseFittingStrategy Dose-response fitting strategy, e.g.
   *                                    {@code IC50_ONLY}.
   * @param doseResponseFittingRounding Whether rounding of value to pIC50 0
   *                                    (zero) should occur.
   * @param doseResponseFittingHillMinMax {@code true} if Hill min/max fitting,
   *                                      otherwise false. 
   * @param doseResponseFittingHillMax Max. Hill Coefficient value (or
   *                                   {@code null} if not applicable).
   * @param doseResponseFittingHillMin Min. Hill Coefficient value (or
   *                                   {@code null} if not applicable).
   * @param cellModelIdentifier CellML Model identifier.
   * @param pacingMaxTime Maximum pacing time (in minutes).
   * @return Simulation corresponding to specified inputs, otherwise {@code null}.
   */
  Simulation find(String compoundIdentifier, boolean assayGrouping,
                  boolean valueInheriting, boolean betweenGroups,
                  boolean withinGroups, String pc50EvaluationStrategies,
                  String doseResponseFittingStrategy,
                  boolean doseResponseFittingRounding,
                  boolean doseResponseFittingHillMinMax,
                  Float doseResponseFittingHillMax,
                  Float doseResponseFittingHillMin, short cellModelIdentifier,
                  BigDecimal pacingMaxTime);

  /**
   * Determine if the simulation is completed (following the completion of one
   * of its jobs).
   * 
   * @param simulationId Simulation identifier.
   * @return {@code true} if simulation is completed, otherwise {@code false}.
   * @see SimulationManager#simulationTerminating(long)
   */
  boolean jobCompletedProcessing(long simulationId);

  /**
   * Force a simulation to re-run.
   * 
   * @param simulation Simulation to run again.
   * @return Updated re-run simulation.
   */
  Simulation processingForceReRun(Simulation simulation);

  /**
   * Force a simulation to be reset (aka stopped).
   * 
   * @param simulation Simulation to reset.
   * @return Updated, reset simulation.
   */
  Simulation processingForceReset(Simulation simulation);

  /**
   * Undertake regular (i.e. not reset or re-run) simulation processing.
   *  
   * @param simulation Simulation to be processed.
   * @return Processed simulation.
   */
  Simulation processingRegular(Simulation simulation);

  /**
   * Perform existing simulation request processing.
   * 
   * @param simulation Existing simulation.
   * @param simulationRequest Simulation request.
   * @return Processed simulation request value object.
   */
  ProcessedSimulationRequestVO requestProcessingExistingSimulation(Simulation simulation,
                                                                   SimulationRequestVO simulationRequest);

  /**
   * Perform input data only request processing.
   * 
   * @param simulationRequest Simulation request.
   * @return Processed simulation request value object.
   */
  ProcessedSimulationRequestVO requestProcessingInputDataOnly(SimulationRequestVO simulationRequest);

  /**
   * Perform new simulation request processing.
   * 
   * @param simulationRequest Simulation request.
   * @return Processed simulation request value object.
   */
  ProcessedSimulationRequestVO requestProcessingNewSimulation(SimulationRequestVO simulationRequest);

  /**
   * Retrieve per-frequency compound concentrations for the simulation based on
   * its jobs.
   * <p>
   * For whatever it's worth the collections returned of the <i>Tree</i>
   * variety, i.e. {@code TreeMap} and {@code TreeSet} to enable natural
   * ordering.
   * <p>
   * Note also that there may be loss of accuracy between the source data and
   * what is returned here as the source data may not have originally been held
   * in BigDecimal format.
   * 
   * @param simulationId Simulation identifier.
   * @return Unmodifiable collection of per-frequency compound concentrations.
   */
  public Map<BigDecimal, Set<BigDecimal>> retrievePerFrequencyConcentrations(long simulationId);

  /**
   * Save a simulation.
   * 
   * @param simulation Simulation to save.
   * @return Saved simulation.
   */
  Simulation save(Simulation simulation);

  /**
   * The simulation is terminating without invoking an ApPredict.
   * <p>
   * Note that this is occurs after input value data has been aggregated from
   * experimental and assay sources and it is determined that there is no
   * simulation to run.
   * 
   * @param simulationId Simulation identifier.
   * @see #jobCompletedProcessing(long) For post-simulation run terminating.
   */
  void simulationTerminating(long simulationId);

  /**
   * Validate user input and/or assign default values then return a validated
   * object if no problems, otherwise throw the appropriate exception.
   * 
   * @param compoundIdentifier Compound identifier.
   * @param forceReRun Force re-run process.
   * @param reset Reset process.
   * @param assayGrouping Whether simulation enacts assay grouping.
   * @param valueInheriting Whether simulation enacts value inheriting.
   * @param betweenGroups Whether simulation enacts value inheriting between
   *                      groups.
   * @param withinGroups Whether simulation enacts value inheriting within
   *                     groups.
   * @param pc50EvaluationStrategies PC50 evaluation strategies.
   * @param doseResponseFittingStrategy Dose-response fitting option, e.g. 
   *                                    {@code IC50_ONLY}.
   * @param doseResponseFittingRounding Whether rounding of value to pIC50 0
   *                                    (zero) should occur.
   * @param doseResponseFittingHillMinMax Whether Hill min/max fitting should
   *                                      occur (or {@code null} if not
   *                                      applicable).
   * @param doseResponseFittingHillMax Max. Hill Coefficient value (or
   *                                   {@code null} if not applicable).
   * @param doseResponseFittingHillMin Min. Hill Coefficient value (or
   *                                   {@code null} if not applicable).
   * @param cellModelIdentifier CellML Model identifier.
   * @param pacingMaxTime Maximum pacing time (in minutes).
   * @param inputDataOnly Whether the simulation request is for input data only.
   * @param userId User identifier.
   * @return Simulation request value object.
   * @throws IllegalArgumentException If illegal properties are received, e.g. a
   *                                  -ve max. pacing time or -ve cell model
   *                                  identifier.
   */
  SimulationRequestVO userInputValidation(final String compoundIdentifier,
                                          final Boolean forceReRun,
                                          final Boolean reset,
                                          final Boolean assayGrouping,
                                          final Boolean valueInheriting,
                                          final Boolean betweenGroups,
                                          final Boolean withinGroups,
                                          final String pc50EvaluationStrategies,
                                          final String doseResponseFittingStrategy,
                                          final Boolean doseResponseFittingRounding,
                                          final Boolean doseResponseFittingHillMinMax,
                                          final Float doseResponseFittingHillMax,
                                          final Float doseResponseFittingHillMin,
                                          final Short cellModelIdentifier,
                                          final BigDecimal pacingMaxTime,
                                          final Boolean inputDataOnly,
                                          final String userId)
                                          throws IllegalArgumentException;
}