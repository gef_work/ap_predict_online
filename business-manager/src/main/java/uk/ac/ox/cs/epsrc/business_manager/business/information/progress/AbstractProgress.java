/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.information.progress;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Abstract progress object.
 *
 * @author geoff
 */
public abstract class AbstractProgress implements Progress {

  private static final long serialVersionUID = -8894579832740993055L;

  private final boolean onCreation;
  private final Date timestamp;
  private final InformationLevel level;
  private final String text;
  private final List<String> args = new ArrayList<String>();
  private final Long simulationId;

  public static class Builder {
    // Optional parameters
    private final List<String> args = new ArrayList<String>();
    // Required parameters
    private final InformationLevel level;
    private final String text;
    private final Long simulationId;
    // System 
    private boolean onCreation = false;                                        // default non-creating.
    private final Date timestamp;

    /**
     * Initialising constructor with minimum data. Default is non-terminating progress.
     * 
     * @param level Information level.
     * @param text Progress text.
     * @param simulationId Simulation identifier.
     * @throws IllegalArgumentException If {@code null} object received for {@link #level} or
     *                                  {@link #simulationId}, or if {@link #text} is blank.
     */
    public Builder(final InformationLevel level, final String text, final Long simulationId)
                   throws IllegalArgumentException {
      if (level == null || StringUtils.isBlank(text) || simulationId == null) {
        final String errorMessage = "Cannot build a progress object with a null level or simulationId, nor with blank text.";
        throw new IllegalArgumentException(errorMessage);
      }
      this.timestamp = new Date();
      this.level = level;
      this.text = text;
      this.simulationId = simulationId;
    }

    /**
     * Initialising constructor with optional argument data.
     * 
     * @param level Information level.
     * @param text Progress text.
     * @param args Progress args.
     * @param simulationId Simulation identifier.
     * @throws IllegalArgumentException See {@code super} constructor.
     */
    public Builder(final InformationLevel level, final String text, final String[] args,
                   final Long simulationId) {
      this(level, text, simulationId);
      if (args != null && args.length > 0) {
        this.args.addAll(Arrays.asList(args));
      }
    }

    /**
     * Assign the flag to indicate that this progress object indicates the first record for the
     * type-specific identifiers.
     * 
     * @param onCreation {@code true} if creating, otherwise {@code false} if appending.
     */
    public void setOnCreation(boolean onCreation) {
      this.onCreation = onCreation;
    }
  }

  protected AbstractProgress(final Builder builder) {
    this.timestamp = builder.timestamp;
    this.level = builder.level;
    this.text = builder.text;
    this.args.addAll(builder.args);
    this.simulationId = builder.simulationId;
    this.onCreation = builder.onCreation;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "AbstractProgress [onCreation=" + onCreation + ", timestamp="
        + timestamp + ", level=" + level + ", text=" + text + ", args=" + args
        + ", simulationId=" + simulationId + "]";
  }

  /**
   * Retrieve the simulation identifier.
   * 
   * @return Simulation identifier, or {@code null} if not assigned.
   */
  public Long getSimulationId() {
    return simulationId;
  }

  /**
   * Retrieve the date/time of progress creation.
   * 
   * @return The date/time of progress creation.
   */
  public Date getTimestamp() {
    return timestamp;
  }

  /**
   * Retrieve the information detail level.
   * 
   * @return The {@link InformationLevel} detail level.
   */
  public InformationLevel getLevel() {
    return level;
  }

  /**
   * Retrieve the progress text.
   * 
   * @return Progress text.
   */
  public String getText() {
    return text;
  }

  /**
   * Retrieve progress text arguments.
   * 
   * @return Unmodifiable collection of arguments to progress text if assigned, otherwise empty
   *         collection.
   */
  public List<String> getArgs() {
    return Collections.unmodifiableList(args);
  }

  /**
   * Indicate that this is the first occurrence of the progress is to be persisted.
   *  
   * @return {@code true} if first occurrence, otherwise {@code false}.
   */
  public boolean hasJustBeenCreated() {
    return onCreation;
  }
}