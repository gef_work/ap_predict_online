/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.epsrc.business_manager.config.ConfigurationActionPotential;
import uk.ac.ox.cs.epsrc.business_manager.config.ConfigurationDoseResponseFitting;
import uk.ac.ox.cs.epsrc.business_manager.value.object.CellModelVO;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.DefConAssayGroupStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.DefConAssayStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.DefConCellModelStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.DefConDRFStrategyHillFitting;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.DefConDoseResponseFittingStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.DefConIonChannelStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.DefConPC50EvaluationStrategyStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.DefConRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.DefConResponse;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ObjectFactory;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayGroupVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 * Implementation of the configuration service.
 *
 * @author geoff
 */
@Component(BusinessIdentifiers.COMPONENT_CONFIG_SERVICE)
public class ConfigServiceImpl implements ConfigService {

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_CONFIGURATION)
  private Configuration configuration;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_CONFIGURATION_ACTIONPOTENTIAL)
  private ConfigurationActionPotential configurationActionPotential;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_CONFIGURATION_DOSERESPONSE_FITTING)
  private ConfigurationDoseResponseFitting configurationDoseResponseFitting;

  private static final ObjectFactory objectFactory = new ObjectFactory();

  private static final Log log = LogFactory.getLog(ConfigServiceImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.service.ConfigService#retrieveDefaultConfigurations(uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.DefConRequest)
   */
  @Override
  public DefConResponse retrieveDefaultConfigurations(final DefConRequest doesNothing) {
    log.debug("~retrieveDefCon() : Invoked.");

    final DefConResponse response = objectFactory.createDefConResponse();

    final List<PC50EvaluationStrategy> defaultStrategies = configuration.getDefaultPC50EvaluationStrategies();
    for (final PC50EvaluationStrategy defaultStrategy : defaultStrategies) {
      // TODO : Verify that this is what's supposed to be happening! I.e. order -> id.
      final Long id = Long.valueOf(defaultStrategy.getOrder());
      final String description = defaultStrategy.getDescription();
      final int defaultInvocationOrder = defaultStrategy.getOrder();
      final boolean defaultActive = defaultStrategy.getDefaultActive();

      final DefConPC50EvaluationStrategyStructure structure = new DefConPC50EvaluationStrategyStructure();
      structure.setId(id);
      structure.setDescription(description);
      structure.setDefaultInvocationOrder(defaultInvocationOrder);
      structure.setDefaultActive(defaultActive);

      response.getDefConPC50EvaluationStrategyStructure().add(structure);
    }

    response.setDefaultForceReRun(configuration.isDefaultForceReRun());
    response.setDefaultReset(Configuration.DEFAULT_FORCE_RESET);

    response.setDefaultAssayGrouping(configuration.isDefaultAssayGrouping());
    response.setDefaultValueInheriting(configuration.isDefaultValueInheriting());
    response.setDefaultBetweenGroups(configuration.isDefaultBetweenGroups());
    response.setDefaultWithinGroups(configuration.isDefaultWithinGroups());

    response.setDefaultMinInformationLevel(configuration.getDefaultMinInformationLevel());
    response.setDefaultMaxInformationLevel(configuration.getDefaultMaxInformationLevel());

    final DefConDoseResponseFittingStructure doseResponseFittingStructure = objectFactory.createDefConDoseResponseFittingStructure();

    final Map<String, Boolean> drfStrategyHillFitting = configurationDoseResponseFitting.getDRFStrategyHillFitting();
    for (final Map.Entry<String, Boolean> eachDRFStrategyHillFitting : drfStrategyHillFitting.entrySet()) {
      final DefConDRFStrategyHillFitting drfStrategyHillFittingObj = objectFactory.createDefConDRFStrategyHillFitting();
      drfStrategyHillFittingObj.setStrategy(eachDRFStrategyHillFitting.getKey());
      drfStrategyHillFittingObj.setHillFitting(eachDRFStrategyHillFitting.getValue());
      doseResponseFittingStructure.getDefConDRFStrategyHillFitting().add(drfStrategyHillFittingObj);
    }
    doseResponseFittingStructure.setDefaultDoseResponseFittingStrategy(configurationDoseResponseFitting.getDefaultDoseResponseFittingStrategy());
    doseResponseFittingStructure.setDefaultDoseResponseFittingRounded(configurationDoseResponseFitting.isDefaultDoseResponseFittingRounded());
    doseResponseFittingStructure.setDefaultDoseResponseFittingHillMax(configurationDoseResponseFitting.getDefaultDoseResponseFittingHillMax());
    doseResponseFittingStructure.setDefaultDoseResponseFittingHillMin(configurationDoseResponseFitting.getDefaultDoseResponseFittingHillMin());
    response.setDefConDoseResponseFittingStructure(doseResponseFittingStructure);

    final Set<AssayVO> assays = configuration.getAssays();
    for (final AssayVO assay : assays) {
      final DefConAssayStructure assayStructure = objectFactory.createDefConAssayStructure();
      assayStructure.setLevel(assay.getLevel());
      assayStructure.setName(assay.getName());
      response.getDefConAssayStructure().add(assayStructure);
    }

    final Set<AssayGroupVO> assayGroups = configuration.getAssayGroups();
    for (final AssayGroupVO assayGroup : assayGroups) {
      final DefConAssayGroupStructure assayGroupStructure = objectFactory.createDefConAssayGroupStructure();
      assayGroupStructure.setLevel(assayGroup.getLevel());
      assayGroupStructure.setName(assayGroup.getName());
      response.getDefConAssayGroupStructure().add(assayGroupStructure);
    }

    final Set<IonChannel> ionChannels = configuration.getIonChannels();
    for (final IonChannel ionChannel : ionChannels) {
      final DefConIonChannelStructure ionChannelStructure = objectFactory.createDefConIonChannelStructure();
      ionChannelStructure.setDescription(ionChannel.getDescription());
      ionChannelStructure.setName(ionChannel.name());
      ionChannelStructure.setDisplayOrder(ionChannel.getDisplayOrder());
      response.getDefConIonChannelStructure().add(ionChannelStructure);
    }

    final List<CellModelVO> cellModels = configurationActionPotential.getCellModels();
    final BigDecimal defaultPacingMaxTime = configurationActionPotential.getDefaultPacingMaxTime();

    for (final CellModelVO cellModel : cellModels) {
      final DefConCellModelStructure cellModelStructure = objectFactory.createDefConCellModelStructure();
      cellModelStructure.setCellMLURL(cellModel.getCellmlURL());
      cellModelStructure.setDefault(cellModel.isDefaultModel());
      cellModelStructure.setDescription(cellModel.getDescription());
      final short cellModelIdentifier = cellModel.getIdentifier();
      cellModelStructure.setIdentifier(cellModelIdentifier);
      cellModelStructure.setName(cellModel.getName());
      cellModelStructure.setPaperURL(cellModel.getPaperURL());

      BigDecimal usePacingMaxTime = defaultPacingMaxTime;
      if (cellModel.isUnlimitedPacingTime()) {
        // CellML assignment overrides whatever the default may have been, to unlimited.
        usePacingMaxTime = null;
      } else {
        final BigDecimal cellMLPacingMaxTime = cellModel.getPacingMaxTime();
        if (cellMLPacingMaxTime != null) {
          // CellML assignment overrides whatever the default may have been, to a specified value.
          usePacingMaxTime = cellMLPacingMaxTime;
        } else {
          // Use the default max. pacing time, whatever it was.
        }
      }
      cellModelStructure.setPacingMaxTime(usePacingMaxTime);

      response.getDefConCellModelStructure().add(cellModelStructure);
    }

    response.setDefaultSimulationRequestProcessingPolling(configuration.getDefaultRequestProcessingPolling());

    return response;
  }
}
