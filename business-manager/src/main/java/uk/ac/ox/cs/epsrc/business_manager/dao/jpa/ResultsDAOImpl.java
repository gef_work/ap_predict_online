/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.dao.jpa;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.dao.ResultsDAO;
import uk.ac.ox.cs.epsrc.business_manager.entity.AbstractIonChannelValuesParent;
import uk.ac.ox.cs.epsrc.business_manager.entity.GroupedInvocationInput;
import uk.ac.ox.cs.epsrc.business_manager.entity.IonChannelValues;
import uk.ac.ox.cs.epsrc.business_manager.entity.Job;
import uk.ac.ox.cs.epsrc.business_manager.entity.JobResult;
import uk.ac.ox.cs.epsrc.business_manager.entity.PIC50Data;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;

/**
 * Results DAO implementation.
 *
 * @author geoff
 */
@Repository(BusinessIdentifiers.COMPONENT_RESULTS_DAO)
public class ResultsDAOImpl implements ResultsDAO {

  @PersistenceContext
  private EntityManager entityManager;

  private static final Log log = LogFactory.getLog(ResultsDAOImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.ResultsDAO#retrieveInputValues(long)
   */
  @SuppressWarnings("unchecked")
  @Override
  public List<GroupedInvocationInput> retrieveInputValues(final long simulationId) {
    final String identifiedLogPrefix = "~retrieveInputValues() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    final Query query = entityManager.createNamedQuery(GroupedInvocationInput.QUERY_BY_SIMULATION_ID);
    query.setParameter(Simulation.PROPERTY_SIMULATION_ID, simulationId);

    final List<GroupedInvocationInput> inputValues = new ArrayList<GroupedInvocationInput>();
    try {
      inputValues.addAll((List<GroupedInvocationInput>) query.getResultList());
      // Traverse the object graph to load values into objects.
      for (final GroupedInvocationInput groupedInvocationInput : inputValues) {
        final AbstractIonChannelValuesParent parent = (AbstractIonChannelValuesParent) groupedInvocationInput;
        for (final IonChannelValues eachIonChannelValues : parent.getIonChannelValues()) {
          for (@SuppressWarnings("unused")
               final PIC50Data pIC50Data : eachIonChannelValues.getpIC50Data()) {}
        }
      }
    } catch (NoResultException e) {
      log.debug(identifiedLogPrefix.concat("No input values exist."));
    }

    return inputValues;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.ResultsDAO#retrieveResults(long)
   */
  @SuppressWarnings("unchecked")
  @Override
  public List<Job> retrieveResults(final long simulationId) {
    final String identifiedLogPrefix = "~retrieveResults() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));
    final Query query = entityManager.createNamedQuery(Job.QUERY_JOB_DATA_BY_SIMULATION_ID);
    query.setParameter(Simulation.PROPERTY_SIMULATION_ID, simulationId);

    final List<Job> jobsWithResults = new ArrayList<Job>();
    try {
      final List<Job> jobs = (List<Job>) query.getResultList();
      for (final Job job : jobs) {
        final Set<JobResult> jobResults = job.getJobResults();
        if (!jobResults.isEmpty()) {
          jobsWithResults.add(job);
        }
      }
    } catch (NoResultException e) {
      log.debug(identifiedLogPrefix.concat("No results exist."));
    }

    return jobsWithResults;
  }
}