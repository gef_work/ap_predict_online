/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.dao.jpa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.dao.SimulationDAO;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;

/**
 * Implementation of the Simulation Data Access Object interface.
 * <p>
 * Queries should use named parameters ensuring that dangerous characters should be automatically
 * escaped JDBC driver.
 * </p> 
 * 
 * @author Geoff Williams
 */
@Repository(BusinessIdentifiers.COMPONENT_SIMULATION_DAO)
public class SimulationDAOImpl implements SimulationDAO {

  @PersistenceContext
  private EntityManager entityManager;

  private static final Log log = LogFactory.getLog(SimulationDAOImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.SimulationDAO#findByAllProperties(java.lang.String, boolean, boolean, boolean, boolean, java.lang.String, java.lang.String, boolean, boolean, java.lang.Float, java.lang.Float, short, java.math.BigDecimal)
   */
  @Override
  public Simulation findByAllProperties(final String compoundIdentifier, final boolean assayGrouping,
                                        final boolean valueInheriting, final boolean betweenGroups,
                                        final boolean withinGroups,
                                        final String pc50EvaluationStrategies,
                                        final String doseResponseFittingStrategy,
                                        final boolean doseResponseFittingRounding,
                                        final boolean doseResponseFittingHillMinMax,
                                        final Float doseResponseFittingHillMax,
                                        final Float doseResponseFittingHillMin,
                                        final short cellModelIdentifier,
                                        final BigDecimal pacingMaxTime) {
    final String identifiedLogPrefix = "~findByAllProperties() : [" + compoundIdentifier + "] : ";
    log.debug(identifiedLogPrefix.concat(Simulation.PROPERTY_COMPOUND_IDENTIFIER + "=" + compoundIdentifier));
    log.debug(identifiedLogPrefix.concat(Simulation.PROPERTY_ASSAY_GROUPING + "=" + assayGrouping));
    log.debug(identifiedLogPrefix.concat(Simulation.PROPERTY_VALUE_INHERITING + "=" + valueInheriting));
    log.debug(identifiedLogPrefix.concat(Simulation.PROPERTY_BETWEEN_GROUPS + "=" + betweenGroups));
    log.debug(identifiedLogPrefix.concat(Simulation.PROPERTY_WITHIN_GROUPS + "=" + withinGroups));
    log.debug(identifiedLogPrefix.concat(Simulation.PROPERTY_PC50_EVALUATION_STRATEGIES + "=" + pc50EvaluationStrategies));
    log.debug(identifiedLogPrefix.concat(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_STRATEGY + "=" + doseResponseFittingStrategy));
    log.debug(identifiedLogPrefix.concat(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_ROUNDING + "=" + doseResponseFittingRounding));
    log.debug(identifiedLogPrefix.concat(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MINMAX + "=" + doseResponseFittingHillMinMax));
    log.debug(identifiedLogPrefix.concat(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MIN + "=" + doseResponseFittingHillMin));
    log.debug(identifiedLogPrefix.concat(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MAX + "=" + doseResponseFittingHillMax));
    log.debug(identifiedLogPrefix.concat(Simulation.PROPERTY_CELLML_MODEL_IDENTIFIER + "=" + cellModelIdentifier));
    log.debug(identifiedLogPrefix.concat(Simulation.PROPERTY_PACING_MAX_TIME + "=" + pacingMaxTime));

    final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
    final CriteriaQuery<Simulation> criteriaQuery = criteriaBuilder.createQuery(Simulation.class);
    final Root<Simulation> root = criteriaQuery.from(Simulation.class);
    final List<Predicate> predicates = new ArrayList<Predicate>();
    predicates.add(criteriaBuilder.equal(root.get(Simulation.PROPERTY_COMPOUND_IDENTIFIER),
                                         compoundIdentifier));
    predicates.add(criteriaBuilder.equal(root.get(Simulation.PROPERTY_ASSAY_GROUPING),
                                         assayGrouping));
    predicates.add(criteriaBuilder.equal(root.get(Simulation.PROPERTY_VALUE_INHERITING),
                                         valueInheriting));
    predicates.add(criteriaBuilder.equal(root.get(Simulation.PROPERTY_BETWEEN_GROUPS),
                                         betweenGroups));
    predicates.add(criteriaBuilder.equal(root.get(Simulation.PROPERTY_WITHIN_GROUPS),
                                         withinGroups));
    predicates.add(criteriaBuilder.equal(root.get(Simulation.PROPERTY_PC50_EVALUATION_STRATEGIES),
                                         pc50EvaluationStrategies));
    predicates.add(criteriaBuilder.equal(root.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_STRATEGY),
                                         doseResponseFittingStrategy));
    predicates.add(criteriaBuilder.equal(root.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_ROUNDING),
                                         doseResponseFittingRounding));
    predicates.add(criteriaBuilder.equal(root.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MINMAX),
                                         doseResponseFittingHillMinMax));
    if (doseResponseFittingHillMin != null) {
      log.debug(identifiedLogPrefix.concat("Hill min is not null."));
      predicates.add(criteriaBuilder.equal(root.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MIN),
                                           doseResponseFittingHillMin));
    } else {
      log.debug(identifiedLogPrefix.concat("Hill min is null."));
      predicates.add(criteriaBuilder.isNull(root.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MIN)));
    }
    if (doseResponseFittingHillMax != null) {
      log.debug(identifiedLogPrefix.concat("Hill max is not null."));
      predicates.add(criteriaBuilder.equal(root.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MAX),
                                           doseResponseFittingHillMax));
    } else {
      log.debug(identifiedLogPrefix.concat("Hill max is null."));
      predicates.add(criteriaBuilder.isNull(root.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MAX)));
    }
    predicates.add(criteriaBuilder.equal(root.get(Simulation.PROPERTY_CELLML_MODEL_IDENTIFIER),
                                         cellModelIdentifier));
    if (pacingMaxTime != null) {
      log.debug(identifiedLogPrefix.concat("Pacing max time is not null."));
      predicates.add(criteriaBuilder.equal(root.get(Simulation.PROPERTY_PACING_MAX_TIME),
                                           pacingMaxTime));
    } else {
      log.debug(identifiedLogPrefix.concat("Pacing max time is null."));
      predicates.add(criteriaBuilder.isNull(root.get(Simulation.PROPERTY_PACING_MAX_TIME)));
    }

    criteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
    final TypedQuery<Simulation> typedQuery = entityManager.createQuery(criteriaQuery);

    Simulation simulation = null;
    try {
      simulation = typedQuery.getSingleResult();
      log.debug(identifiedLogPrefix.concat("Simulation exists '" + simulation.toString() + "'."));
    } catch (NoResultException e) {
      log.debug(identifiedLogPrefix.concat("No simulation exists."));
    }

    return simulation;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.SimulationDAO#findBySimulationId(long)
   */
  @Override
  public Simulation findBySimulationId(final long simulationId) {
    log.debug("~findBySimulationId() : [" + simulationId + "] : Invoked.");

    return entityManager.find(Simulation.class, simulationId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.SimulationDAO#store(uk.ac.ox.cs.epsrc.business_manager.entity.Simulation)
   */
  @Override
  public Simulation store(final Simulation simulation) {
    final Long simulationId = simulation.getId();
    final String identifiedLogPrefix = "~store() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked for '" + simulation.toString() + "'."));

    if (simulationId != null) {
      return entityManager.merge(simulation);
    } else {
      entityManager.persist(simulation);
      return simulation;
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.SimulationDAO#updateOnJobsCompletion(long)
   */
  @Override
  public void updateOnJobsCompletion(final long simulationId) {
    final String identifiedLogPrefix = "~updateOnJobsCompletion() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    final Simulation simulation = findBySimulationId(simulationId);
    if (simulation != null) {
      simulation.assignCompleted();
      store(simulation);
    } else {
      final String warnMessage = "Simulation with identifier '" + simulationId + "' cannot be found!";
      log.debug(identifiedLogPrefix.concat(warnMessage));
    }
  }
}