/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.information.provenance;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.DoseResponseData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Individual data record provenance information.
 *
 * @author geoff
 */
public class IndividualProvenance extends AbstractProvenance {

  private static final long serialVersionUID = 4436821826435477809L;

  private final String summaryDataId;
  private final String individualDataId;
  private final String assayName;
  private final String ionChannelName;
  private final List<BigDecimal> pIC50s = new ArrayList<BigDecimal>();
  private final BigDecimal workflowHill;
  private final BigDecimal rawHill;
  private final DoseResponseData doseResponseData;
  private final String drFittingStrategyIdentifier;
  private final boolean recordingDoseResponses;
  private final Map<String, Object> rawData = new HashMap<String, Object>();

  public static class Builder extends AbstractProvenance.Builder {
    // Optional parameters
    private String summaryDataId = null;
    private String individualDataId = null;
    private String assayName = null;
    private String ionChannelName = null;
    private List<BigDecimal> pIC50s = new ArrayList<BigDecimal>();
    private Map<String, Object> rawData = new HashMap<String, Object>();
    private DoseResponseData doseResponseData = null;
    private BigDecimal rawHill = null;
    private BigDecimal workflowHill = null;
    private boolean recordingDoseResponses = false;
    private String drFittingStrategyIdentifier = null;

    /**
     * Initialising constructor with minimum data.
     * 
     * @param level Provenance level.
     * @param text Provenance text.
     * @param simulationId Simulation Identifier.
     * @throws IllegalArgumentException See {@code super} constructor.
     */
    public Builder(final InformationLevel level, final String text, final Long simulationId)
                   throws IllegalArgumentException {
      super(level, text, simulationId);
    }

    /**
     * Summary data identifier.
     * 
     * @param val Summary data identifier.
     * @return Builder.
     */
    public Builder summaryDataId(final String val) {
      summaryDataId= val;
      return this;
    }

    /**
     * Individual data identifier.
     * 
     * @param val Individual data identifier.
     * @return Builder.
     */
    public Builder individualDataId(final String val) {
      individualDataId = val;
      return this;
    }

    /**
     * Assay name.
     * 
     * @param val Assay name.
     * @return Builder.
     */
    public Builder assayName(final String val) {
      assayName = val;
      return this;
    }

    /**
     * Ion channel name.
     * 
     * @param val Ion channel name.
     * @return Builder.
     */
    public Builder ionChannelName(final String val) {
      ionChannelName = val;
      return this;
    }

    /**
     * pIC50 values.
     * 
     * @param val pIC50 values.
     * @return Builder.
     */
    public Builder pIC50s(final List<BigDecimal> val) {
      pIC50s.addAll(Collections.unmodifiableList(val));
      return this;
    }

    /**
     * Raw data.
     * 
     * @param val Raw data.
     * @return Builder.
     */
    public Builder rawData(final Map<String, Object> val) {
      rawData.putAll(val);
      return this;
    }

    /**
     * An individual record's dose-response values (derived from it's dose-response data).
     * 
     * @param val Dose-response data.
     * @return Builder.
     */
    public Builder doseResponseData(final DoseResponseData val) {
      doseResponseData = val;
      return this;
    }

    /**
     * Hill Coefficient as determined by workflow processing.
     * 
     * @param val Workflow-derived Hill Coefficient.
     * @return Builder.
     */
    public Builder workflowHill(final BigDecimal val) {
      workflowHill = val;
      return this;
    }

    /**
     * Raw Hill Coefficient.
     * 
     * @param val Raw Hill Coefficient.
     * @return Builder.
     */
    public Builder rawHill(final BigDecimal val) {
      rawHill = val;
      return this;
    }

    /**
     * Dose-response data's fitting strategy identifier.
     * 
     * @param val Fitting strategy identifier.
     * @return Builder.
     */
    public Builder drFittingStrategyIdentifier(final String val) {
      drFittingStrategyIdentifier = val;
      return this;
    }

    /**
     * Indicate that we're recording dose-response values from site data.
     * 
     * @return Builder.
     */
    public Builder recordingDoseResponses() {
      recordingDoseResponses = true;
      return this;
    }

    /**
     * Instruct the builder to build!
     * 
     * @return Newly built provenance data.
     */
    public IndividualProvenance build() {
      return new IndividualProvenance(this);
    }
  }

  private IndividualProvenance(final Builder builder) {
    super(builder);
    summaryDataId = builder.summaryDataId;
    individualDataId = builder.individualDataId;
    assayName = builder.assayName;
    ionChannelName = builder.ionChannelName;
    pIC50s.addAll(builder.pIC50s);
    rawData.putAll(builder.rawData);
    workflowHill = builder.workflowHill;
    rawHill = builder.rawHill;
    doseResponseData = builder.doseResponseData;
    drFittingStrategyIdentifier = builder.drFittingStrategyIdentifier;
    recordingDoseResponses = builder.recordingDoseResponses;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "IndividualProvenance [summaryDataId=" + summaryDataId
        + ", individualDataId=" + individualDataId + ", assayName=" + assayName
        + ", ionChannelName=" + ionChannelName + ", pIC50s=" + pIC50s
        + ", workflowHill=" + workflowHill + ", rawHill=" + rawHill
        + ", doseResponseData=" + doseResponseData
        + ", drFittingStrategyIdentifier=" + drFittingStrategyIdentifier
        + ", recordingDoseResponses=" + recordingDoseResponses + ", rawData="
        + rawData + "]";
  }

  /**
   * Retrieve the summary data identifier.
   * 
   * @return The summary data identifier, or {@code null} if not assigned.
   */
  public String getSummaryDataId() {
    return summaryDataId;
  }

  /**
   * Retrieve the individual data identifier.
   * 
   * @return The individual data identifier, or {@code null} if not assigned.
   */
  public String getIndividualDataId() {
    return individualDataId;
  }

  /**
   * Retrieve the assay name.
   * 
   * @return The assay name, or {@code null} if not assigned.
   */
  public String getAssayName() {
    return assayName;
  }

  /**
   * Retrieve the ion channel name.
   * 
   * @return The ion channel name, or {@code null} if not assigned.
   */
  public String getIonChannelName() {
    return ionChannelName;
  }

  /**
   * Retrieve the pIC50 values.
   * 
   * @return Unmodifiable pIC50 value collection, or empty collection if none assigned.
   */
  public List<BigDecimal> getpIC50s() {
    return Collections.unmodifiableList(pIC50s);
  }

  /**
   * Retrieve the raw data record.
   * 
   * @return Unmodified raw data record, or empty collection if none assigned. 
   */
  public Map<String, Object> getRawData() {
    return Collections.unmodifiableMap(rawData);
  }

  /**
   * Retrieve the dose-response data.
   * 
   * @return The dose-response data, or {@code null} if not assigned.
   */
  public DoseResponseData getDoseResponseData() {
    return doseResponseData;
  }

  /**
   * The Hill Coefficient value assigned during the workflow processing. Could be the raw hill value
   * or a fitted value or default value.
   * 
   * @return The workflow Hill Coefficient value, or {@code null} if not assigned.
   */
  public BigDecimal getWorkflowHill() {
    return workflowHill;
  }

  /**
   * Retrieve the raw Hill Coefficient value.
   * 
   * @return Raw Hill Coefficient value, or {@code null} if not assigned.
   */
  public BigDecimal getRawHill() {
    return rawHill;
  }

  /**
   * Indicate if we're recording the dose-response data from site data.
   * 
   * @return True if recording all dose-response data from site data, otherwise false.
   */
  public boolean isRecordingDoseResponses() {
    return recordingDoseResponses;
  }

  /**
   * Retrieve the dose-response fitting strategy identifier.
   * 
   * @return Fitting strategy identifier, or {@code null} if not assigned.
   */
  public String getDrFittingStrategyIdentifier() {
    return drFittingStrategyIdentifier;
  }
}