/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Simulation entity.
 *
 * @author geoff
 */
@Entity
@NamedQueries({
  @NamedQuery(name=Simulation.QUERY_SIMULATION_BY_ALL_PROPERTIES,
              query="SELECT simulation FROM Simulation AS simulation " +
                    "  WHERE " + Simulation.PROPERTY_COMPOUND_IDENTIFIER + " = :" + Simulation.PROPERTY_COMPOUND_IDENTIFIER + " AND " +
                                 Simulation.PROPERTY_ASSAY_GROUPING + " = :" + Simulation.PROPERTY_ASSAY_GROUPING + " AND " +
                                 Simulation.PROPERTY_VALUE_INHERITING + " = :" + Simulation.PROPERTY_VALUE_INHERITING + " AND " +
                                 Simulation.PROPERTY_BETWEEN_GROUPS + " = :" + Simulation.PROPERTY_BETWEEN_GROUPS + " AND " +
                                 Simulation.PROPERTY_WITHIN_GROUPS + " = :" + Simulation.PROPERTY_WITHIN_GROUPS + " AND " +
                                 Simulation.PROPERTY_PC50_EVALUATION_STRATEGIES + " = :" + Simulation.PROPERTY_PC50_EVALUATION_STRATEGIES + " AND " +
                                 Simulation.PROPERTY_DOSE_RESPONSE_FITTING_STRATEGY + " = :" + Simulation.PROPERTY_DOSE_RESPONSE_FITTING_STRATEGY + " AND " +
                                 Simulation.PROPERTY_DOSE_RESPONSE_FITTING_ROUNDING + " = :" + Simulation.PROPERTY_DOSE_RESPONSE_FITTING_ROUNDING + " AND " +
                                 Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MINMAX + " = :" + Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MINMAX + " AND " +
                                 Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MIN + " = :" + Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MIN + " AND " +
                                 Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MAX + " = :" + Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MAX + " AND " +
                                 Simulation.PROPERTY_CELLML_MODEL_IDENTIFIER + " = :" + Simulation.PROPERTY_CELLML_MODEL_IDENTIFIER + " AND " +
                                 Simulation.PROPERTY_PACING_MAX_TIME + " = :" + Simulation.PROPERTY_PACING_MAX_TIME),
  @NamedQuery(name=Simulation.QUERY_SIMULATION_BY_COMPOUND_IDENTIFIER,
              query="SELECT simulation FROM Simulation AS simulation " +
                    "  WHERE " + Simulation.PROPERTY_COMPOUND_IDENTIFIER + " = :" + Simulation.PROPERTY_COMPOUND_IDENTIFIER),
  @NamedQuery(name=Simulation.QUERY_SIMULATION_BY_REQUEST_PROCESSED,
              query="SELECT simulation FROM Simulation AS simulation " +
                    "  WHERE " + Simulation.PROPERTY_REQUEST_PROCESSED + " IS FALSE"),
  @NamedQuery(name=Simulation.UPDATE_SIMULATION_AS_REQUEST_PROCESSED,
              query="UPDATE Simulation SET " + Simulation.PROPERTY_REQUEST_PROCESSED + "=true, "
                                             + Simulation.PROPERTY_CURRENT_STATUS + "=0 "          // See CurrentStatus
                                    + "WHERE id = :" + Simulation.PROPERTY_SIMULATION_ID)
})
/*
 * TODO: https://bitbucket.org/gef_work/ap_predict_online/issues/38/business-manager-duplicate-simulations
 */
@Table(uniqueConstraints = {
  @UniqueConstraint(columnNames = { Simulation.PROPERTY_COMPOUND_IDENTIFIER,
                                    Simulation.PROPERTY_ASSAY_GROUPING,
                                    Simulation.PROPERTY_VALUE_INHERITING,
                                    Simulation.PROPERTY_BETWEEN_GROUPS,
                                    Simulation.PROPERTY_WITHIN_GROUPS,
                                    Simulation.PROPERTY_PC50_EVALUATION_STRATEGIES,
                                    Simulation.PROPERTY_DOSE_RESPONSE_FITTING_STRATEGY,
                                    Simulation.PROPERTY_DOSE_RESPONSE_FITTING_ROUNDING,
                                    Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MINMAX,
                                    Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MIN,
                                    Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MAX,
                                    Simulation.PROPERTY_CELLML_MODEL_IDENTIFIER,
                                    Simulation.PROPERTY_PACING_MAX_TIME },
                    name="unique_elements")
})
public class Simulation implements Serializable {

  private static final long serialVersionUID = 1375653976613046603L;

  /* Consistent property name */
  public static final String PROPERTY_COMPOUND_IDENTIFIER = "compoundIdentifier";
  public static final String PROPERTY_ASSAY_GROUPING = "assayGrouping";
  public static final String PROPERTY_VALUE_INHERITING = "valueInheriting";
  public static final String PROPERTY_BETWEEN_GROUPS = "betweenGroups";
  public static final String PROPERTY_WITHIN_GROUPS = "withinGroups";
  public static final String PROPERTY_PC50_EVALUATION_STRATEGIES = "pc50EvaluationStrategies";
  public static final String PROPERTY_DOSE_RESPONSE_FITTING_STRATEGY = "doseResponseFittingStrategy";
  public static final String PROPERTY_DOSE_RESPONSE_FITTING_ROUNDING = "doseResponseFittingRounding";
  public static final String PROPERTY_DOSE_RESPONSE_FITTING_HILL_MINMAX = "doseResponseFittingHillMinMax";
  public static final String PROPERTY_DOSE_RESPONSE_FITTING_HILL_MIN = "doseResponseFittingHillMin";
  public static final String PROPERTY_DOSE_RESPONSE_FITTING_HILL_MAX = "doseResponseFittingHillMax";
  public static final String PROPERTY_SIMULATION_ID = "simulationId";
  public static final String PROPERTY_CELLML_MODEL_IDENTIFIER = "cellModelIdentifier";
  public static final String PROPERTY_PACING_MAX_TIME = "pacingMaxTime";
  private static final String propertyUserId = "userId";

  public static final String PROPERTY_CURRENT_STATUS = "currentStatus";
  public static final String PROPERTY_PROCESS_TASK = "processTask";
  public static final String PROPERTY_REQUEST_PROCESSED = "requestProcessed";

  protected static final short busy = CurrentStatus.BUSY.getValue();

  /** Consistent query naming - Query simulation by all properties */
  public static final String QUERY_SIMULATION_BY_ALL_PROPERTIES = "simulation.queryByAllProperties";
  /** Consistent query naming - Query simulation by compound identifier */
  public static final String QUERY_SIMULATION_BY_COMPOUND_IDENTIFIER = "simulation.queryByCompoundIdentifier";
  /** Consistent query naming - Query simulation by request processed (see appCtx.sim.processRequest.xml) */
  public static final String QUERY_SIMULATION_BY_REQUEST_PROCESSED = "simulation.queryByRequestProcessed";
  /** Consistent query naming - Update simulation as request processed (see appCtx.sim.processRequest.xml) */
  public static final String UPDATE_SIMULATION_AS_REQUEST_PROCESSED = "simulation.updateAsRequestProcessed";

  // Persistence identity (Surrogate primary key)
  @TableGenerator(name="simulation_id_gen", table="sequence_pks_business_manager",
                  pkColumnName="pk_seq_name", pkColumnValue="simulation_id",
                  valueColumnName="pk_seq_value", allocationSize=1)
  @Id @GeneratedValue(strategy=GenerationType.TABLE, generator="simulation_id_gen")
  private Long id;

  // Optimistic locking concurrency control
  @Version
  private Long lockVersion;

  @Column(length=50, nullable=false, updatable=false, name=PROPERTY_COMPOUND_IDENTIFIER)
  private String compoundIdentifier;

  /*
   * System flag - see appCtx.sim.processRequest.xml int-jpa:inbound-channel-adapter
   * 
   * When persisted as {@code false} (i.e. as a new simulation, or one that's being reset) it's an
   * indication that the Simulation is to be polled and passed to simulation workflow processing.
   * 
   * This is immediately set to {@code true} in SI post- SQL-selection prior to workflow processing.
   */
  @Column(nullable=false, name=PROPERTY_REQUEST_PROCESSED)
  private boolean requestProcessed;

  @Column(nullable=false, name=PROPERTY_CURRENT_STATUS)
  private Short currentStatus;

  @Column(insertable=true, nullable=false, updatable=true, name=PROPERTY_ASSAY_GROUPING)
  private boolean assayGrouping;

  @Column(insertable=true, nullable=false, updatable=true, name=PROPERTY_VALUE_INHERITING)
  private boolean valueInheriting;

  @Column(insertable=true, nullable=false, updatable=true, name=PROPERTY_BETWEEN_GROUPS)
  private boolean betweenGroups;

  @Column(insertable=true, nullable=false, updatable=true, name=PROPERTY_WITHIN_GROUPS)
  private boolean withinGroups;

  @Column(insertable=true, nullable=false, updatable=true, name=PROPERTY_PC50_EVALUATION_STRATEGIES)
  private String pc50EvaluationStrategies;

  @Column(insertable=true, nullable=false, updatable=true, name=PROPERTY_DOSE_RESPONSE_FITTING_STRATEGY)
  private String doseResponseFittingStrategy;

  @Column(insertable=true, nullable=false, updatable=true, name=PROPERTY_DOSE_RESPONSE_FITTING_ROUNDING)
  private boolean doseResponseFittingRounding;

  @Column(insertable=true, nullable=false, updatable=true, name=PROPERTY_DOSE_RESPONSE_FITTING_HILL_MINMAX)
  private boolean doseResponseFittingHillMinMax;

  @Column(insertable=true, nullable=true, updatable=true, name=PROPERTY_DOSE_RESPONSE_FITTING_HILL_MAX)
  private Float doseResponseFittingHillMax;

  @Column(insertable=true, nullable=true, updatable=true, name=PROPERTY_DOSE_RESPONSE_FITTING_HILL_MIN)
  private Float doseResponseFittingHillMin;

  @Column(insertable=true, nullable=false, updatable=false, name=PROPERTY_CELLML_MODEL_IDENTIFIER)
  private short cellModelIdentifier;

  @Column(insertable=true, nullable=true, updatable=false, name=PROPERTY_PACING_MAX_TIME)
  private BigDecimal pacingMaxTime;

  @Column(insertable=false, nullable=true, updatable=true, name=PROPERTY_PROCESS_TASK)
  private Short processTask;

  // User id is assigned on simulation creation and nullified once completed.
  @Column(insertable=true, nullable=true, updatable=true, name=propertyUserId)
  private String userId;

  @Temporal(value=TemporalType.TIMESTAMP)
  private Date completed;

  @Temporal(value=TemporalType.TIMESTAMP)
  private Date latestCheck;

  @Temporal(value=TemporalType.TIMESTAMP)
  private Date persisted;

  /**
   * Internal representation of the current status.
   * <p>
   * Generally it's one of two states :
   * <ul>
   *   <li>BUSY - If simulation is currently (or about to be) being processed</li>
   *   <li>FREE - If simulation has completed processing</li>
   * </ul>
   */
  private enum CurrentStatus {
    // If BUSY value changes then must change Simulation.UPDATE_SIMULATION_AS_REQUEST_PROCESSED
    BUSY((short)0),
    FREE((short)1);

    private final short value;
    private CurrentStatus(final short value) {
      this.value = value;
    }
    public short getValue() {
      return value;
    }
  }

  // Internal representation of the simulation process options.
  private enum ProcessTask {
    FORCE_RERUN(Short.valueOf((short)0)),
    RESET(Short.valueOf((short)1));

    private final Short value;
    private ProcessTask(final Short value) {
      this.value = value;
    }
    public Short getValue() {
      return value;
    }
  }

  @Transient
  private static transient final Log log = LogFactory.getLog(Simulation.class);

  /** <b>Do not invoke directly.</b> */
  protected Simulation() {}

  /**
   * Initialising constructor.
   * 
   * @param compoundIdentifier Compound identifier.
   * @param assayGrouping Whether simulation enacts assay grouping.
   * @param valueInheriting Whether simulation enacts value inheriting.
   * @param betweenGroups Whether simulation enacts value inheriting between
   *                      groups.
   * @param withinGroups Whether simulation enacts value inheriting within
   *                     groups.
   * @param pc50EvaluationStrategies PC50 evaluation strategies.
   * @param doseResponseFittingStrategy Dose-response fitting strategy, e.g.
   *                                    IC50_ONLY.
   * @param doseResponseFittingRounding Whether rounding of value to pIC50 0
   *                                    (zero) should occur.
   * @param doseResponseFittingHillMinMax {@code true} if to use Hill min/max
   *                                      values for fitting.
   * @param doseResponseFittingHillMax Max. Hill Coefficient value (or
   *                                   {@code null} if not applicable).
   * @param doseResponseFittingHillMin Min. Hill Coefficient value (or
   *                                   {@code null} if not applicable).
   * @param cellModelIdentifier CellML Model identifier.
   * @param pacingMaxTime Maximum pacing time (in minutes).
   * @param userId (Optional) User identifier.
   * @throws IllegalArgumentException If compound identifier has no value.
   */
  public Simulation(final String compoundIdentifier, final boolean assayGrouping,
                    final boolean valueInheriting, final boolean betweenGroups,
                    final boolean withinGroups,
                    final String pc50EvaluationStrategies,
                    final String doseResponseFittingStrategy,
                    final boolean doseResponseFittingRounding,
                    final boolean doseResponseFittingHillMinMax,
                    final Float doseResponseFittingHillMax,
                    final Float doseResponseFittingHillMin,
                    final short cellModelIdentifier,
                    final BigDecimal pacingMaxTime, final String userId) {
    if (StringUtils.isBlank(compoundIdentifier)) {
      throw new IllegalArgumentException("Invalid attempt to assign a unvalued compound identifier");
    }
    this.compoundIdentifier = compoundIdentifier;

    // By default a new simulation is considered to be busy (as it's about to be processed)!
    this.currentStatus = CurrentStatus.BUSY.getValue();
    this.requestProcessed = false;

    this.assayGrouping = assayGrouping;
    this.valueInheriting = valueInheriting;
    this.betweenGroups = betweenGroups;
    this.withinGroups = withinGroups;

    this.pc50EvaluationStrategies = pc50EvaluationStrategies;

    this.doseResponseFittingStrategy = doseResponseFittingStrategy;
    this.doseResponseFittingRounding = doseResponseFittingRounding;
    this.doseResponseFittingHillMinMax = doseResponseFittingHillMinMax;
    this.doseResponseFittingHillMax = doseResponseFittingHillMax;
    this.doseResponseFittingHillMin = doseResponseFittingHillMin;

    this.cellModelIdentifier = cellModelIdentifier;

    this.pacingMaxTime = pacingMaxTime;
    this.userId = userId;
  }

  /**
   * Initialising constructor of a never-persisted input data-retrieving simulation
   * 
   * @param fakeSimulationId Fake simulation identifier.
   * @param compoundIdentifier Compound identifier.
   * @param assayGrouping Whether simulation enacts assay grouping.
   * @param valueInheriting Whether simulation enacts value inheriting.
   * @param betweenGroups Whether simulation enacts value inheriting between
   *                      groups.
   * @param withinGroups Whether simulation enacts value inheriting within
   *                     groups.
   * @param pc50EvaluationStrategies PC50 evaluation strategies.
   * @param doseResponseFittingStrategy Dose-response fitting strategy, e.g.
   *                                    {@code IC50_ONLY}.
   * @param doseResponseFittingRounding Whether rounding of value to pIC50 0
   *                                    (zero) should occur.
   * @param doseResponseFittingHillMinMax {@code true} if to use Hill min/max
   *                                      values for fitting.
   * @param doseResponseFittingHillMax Max. Hill Coefficient value (or
   *                                   {@code null} if not applicable).
   * @param doseResponseFittingHillMin Min. Hill Coefficient value (or
   *                                   {@code null} if not applicable).
   * @param cellModelIdentifier CellML Model identifier.
   * @param pacingMaxTime Maximum pacing time (in minutes).
   * @param userId (Optional) User identifier.
   */
  public Simulation(final long fakeSimulationId, final String compoundIdentifier,
                    final boolean assayGrouping, final boolean valueInheriting,
                    final boolean betweenGroups, final boolean withinGroups,
                    final String pc50EvaluationStrategies,
                    final String doseResponseFittingStrategy,
                    final boolean doseResponseFittingRounding,
                    final boolean doseResponseFittingHillMinMax,
                    final Float doseResponseFittingHillMax,
                    final Float doseResponseFittingHillMin,
                    final short cellModelIdentifier,
                    final BigDecimal pacingMaxTime, final String userId) {
    this(compoundIdentifier, assayGrouping, valueInheriting, betweenGroups,
         withinGroups, pc50EvaluationStrategies, doseResponseFittingStrategy,
         doseResponseFittingRounding, doseResponseFittingHillMinMax,
         doseResponseFittingHillMax, doseResponseFittingHillMin,
         cellModelIdentifier, pacingMaxTime, userId);
    this.id = fakeSimulationId;
  }

  // internal callback method
  @PrePersist
  protected void onCreate() {
    persisted = new Date();
  }

  /**
   * Remove the <i>busy</i> state of the simulation (by assigning as <i>free</i>).
   */
  public void assignStateNotBusy() {
    currentStatus = CurrentStatus.FREE.getValue();
  }

  // Change the state of the simulation to <i>busy</i>, i.e. not <i>free</i>.
  private void assignStateBusy() {
    currentStatus = CurrentStatus.BUSY.getValue();
  }

  /**
   * Simulation's finished, so ...
   * <ul>
   *   <li>Assign simulation completion date/time as being now</li>
   *   <li>Assign simulation as no longer being 'busy'</li>
   *   <li>Clear the user id value</li>
   * </ul> 
   * 
   * @throws IllegalStateException If the simulation is in a 'free' state.
   */
  public void assignCompleted() throws IllegalStateException {
    if (!isBusy()) {
      throw new IllegalStateException("Cannot assign a non-busy simulation as 'completed'.");
    }
    assignStateNotBusy();
    clearUserId();
    this.completed = new Date();
  }

  /**
   * Assign the latest re-run check date/time as being now.
   */
  public void assignLatestCheck() {
    this.latestCheck = new Date();
  }

  /**
   * Assign any non-regular processing task to undertake.
   * 
   * @param forceReRun Force a re-run process (if {@code true}).
   * @param reset Reset process (if {@code true}).
   * @return {@code true} if process tasks have changed, otherwise {@code false}.
   * @throws IllegalArgumentException If both {@code forceReRun} and {@code reset} are {@code true}. 
   */
  public boolean assignProcessTask(final boolean forceReRun, final boolean reset)
                                   throws IllegalArgumentException {
    log.debug("~assignProcessTask() : Invoked.");
    if (forceReRun && reset) {
      final String errorMessage = "Forcing re-run should have switched reset off!";
      log.error("~assignProcessTask() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    boolean changed = false;
    final boolean currentlyNull = null == processTask;

    if (forceReRun) {
      if (!forcingReRun()) {
        // Instruct force re-run if not already assigned as such.
        processTask = ProcessTask.FORCE_RERUN.getValue();
        changed = true;
      }
    } else if (reset) {
      if (!forcingReRun() || currentlyNull) {
        // Instruct reset if not already forcing re-run or if unassigned. 
        processTask = ProcessTask.RESET.getValue();
        changed = true;
      }
    } else {
      // Do nothing.
    }

    return changed;
  }

  /**
   * Has the simulation instruction been to force a simulation re-run, i.e. override the default
   * re-running processing.
   * 
   * @return {@code true} if the simulation requires re-running.
   */
  public boolean forcingReRun() {
    return (ProcessTask.FORCE_RERUN.getValue().equals(processTask));
  }

  /**
   * Does the simulation require reseting?
   * 
   * @return {@code true} if simulation requires reseting.
   */
  public boolean forcingReset() {
    return (ProcessTask.RESET.getValue().equals(processTask));
  }

  /**
   * Retrieve indicator which flags if the simulation has been re-run checked.
   * 
   * @return {@code true} if simulation has been re-run checked.
   */
  public boolean hasBeenReRunChecked() {
    return (latestCheck != null);
  }

  /**
   * Determine if the simulation's status indicates that it's currently <i>busy</i>.
   * 
   * @return {@code true} if simulation is <i>busy</i>, otherwise {@code false}.
   * @see CurrentStatus
   */
  public boolean isBusy() {
    return (CurrentStatus.BUSY.getValue() == currentStatus);
  }

  /**
   * Reset the simulation state, assigning the simulation state as "busy".
   * <p>
   * This will {@code null}ify :
   * <ul>
   *   <li>@Temporal properties {@link #completed} and {@link #latestCheck}.</li>
   *   <li>{@link #processTask}</li>
   * </ul>
   */
  public void resetSystemState() {
    resetSystemState(false);
  }
  
  /**
   * Reset the simulation state.
   * <p>
   * This will assign the specified 'busy' state, and {@code null}ify :
   * <ul>
   *   <li>@Temporal properties {@link #completed} and {@link #latestCheck}.</li>
   *   <li>{@link #processTask}</li>
   * </ul>
   * 
   * @param assignNotBusy If {@code true} assign simulation state to be non-"busy", otherwise
   *                      assign as "busy".
   */
  public void resetSystemState(final boolean assignNotBusy) {
    if (assignNotBusy) {
      assignStateNotBusy();
    } else {
      assignStateBusy();
    }

    completed = null;
    latestCheck = null;
    processTask = null;
  }

  /**
   * Retrieve a textual description of the current status.
   * 
   * @return Textual description of the current status.
   */
  public String retrieveCurrentStatusDescription() {
    String description = null;
    assert this.currentStatus != null : "retrieveCurrestStatusDescription() called when current state of simuation is unassigned!";
    for (final CurrentStatus currentStatus : CurrentStatus.values()) {
      if (this.currentStatus == currentStatus.value) {
        description = currentStatus.name();
      }
    }
    return description;
  }

  /**
   * Assign the system flag to indicate that the Simulation should avail itself to SQL-selection for
   * subsequent passing through the simulation processing (i.e. not request processing) workflow.
   * 
   * @param userId (Optional) User identifier.
   */
  public void makeAvailableForProcessing(final String userId) {
    this.requestProcessed = false;
    setUserId(userId);
    assignStateBusy();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Simulation [id=" + id + ", lockVersion=" + lockVersion
        + ", compoundIdentifier=" + compoundIdentifier + ", requestProcessed="
        + requestProcessed + ", currentStatus=" + currentStatus
        + ", assayGrouping=" + assayGrouping + ", valueInheriting="
        + valueInheriting + ", betweenGroups=" + betweenGroups
        + ", withinGroups=" + withinGroups + ", pc50EvaluationStrategies="
        + pc50EvaluationStrategies + ", doseResponseFittingStrategy="
        + doseResponseFittingStrategy + ", doseResponseFittingRounding="
        + doseResponseFittingRounding + ", doseResponseFittingHillMinMax="
        + doseResponseFittingHillMinMax + ", doseResponseFittingHillMax="
        + doseResponseFittingHillMax + ", doseResponseFittingHillMin="
        + doseResponseFittingHillMin + ", cellModelIdentifier="
        + cellModelIdentifier + ", pacingMaxTime=" + pacingMaxTime
        + ", processTask=" + processTask + ", completed=" + completed
        + ", latestCheck=" + latestCheck + ", persisted=" + persisted + "]";
  }

  /**
   * Retrieve the CellML Model identifier.
   * 
   * @return CellML Model identifier.
   */
  public short getCellModelIdentifier() {
    return cellModelIdentifier;
  }

  /**
   * Retrieve the compound identifier.
   * 
   * @return Compound identifier.
   */
  public String getCompoundIdentifier() {
    return compoundIdentifier;
  }

  /**
   * Retrieve the (surrogate primary key) simulation identifier.
   * 
   * @return Simulation identifier )or {@code null} if entity not yet persisted.
   */
  public Long getId() {
    return id;
  }

  /**
   * Retrieve the latest re-run check date/time.
   * 
   * @return The date/time of latest re-run check, or {@code null} if not assigned.
   */
  public Date getLatestCheck() {
    return latestCheck;
  }

  /**
   * Retrieve the simulation's completed date/time.
   * 
   * @return Simulation completion date/time, or {@code null} if not yet completed.
   * @see #assignCompleted()
   */
  public Date getCompleted() {
    return completed;
  }

  /**
   * Indicate whether assays are being grouped or are isolated.
   * 
   * @return {@code true} if assays are being grouped, otherwise {@code false}.
   */
  public boolean isAssayGrouping() {
    return assayGrouping;
  }

  /**
   * Indicate whether (p)IC50 values being inherited up the assay (or assay group) hierarchy.
   * 
   * @return {@code true} if inheriting, otherwise {@code false}.
   */
  public boolean isValueInheriting() {
    return valueInheriting;
  }

  /**
   * @return the betweenGroups
   */
  public boolean isBetweenGroups() {
    return betweenGroups;
  }

  /**
   * @return the withinGroups
   */
  public boolean isWithinGroups() {
    return withinGroups;
  }

  /**
   * Retrieve the maximum pacing time.
   * 
   * @return The maximum pacing time (in minutes), or {@code null} if not specified.
   */
  public BigDecimal getPacingMaxTime() {
    return pacingMaxTime;
  }

  /**
   * The PC50 evaluation strategies which have been chosen by the user, either by simply accepting
   * the default configured strategies, or by modifying the default configuration.
   * 
   * @return PC50 evaluation strategies to use (as CSV).
   */
  public String getPc50EvaluationStrategies() {
    return pc50EvaluationStrategies;
  }

  /**
   * Retrieve the dose-response fitting strategy, e.g. IC50_ONLY.
   * 
   * @return Dose-Response fitting strategy.
   */
  public String getDoseResponseFittingStrategy() {
    return doseResponseFittingStrategy;
  }

  /**
   * @return the doseResponseFittingRounding
   */
  public boolean isDoseResponseFittingRounding() {
    return doseResponseFittingRounding;
  }

  /**
   * Indicator if fitting Hill min/max with dose-response calculations.
   * 
   * @return {@code true} if fitting Hill min/max, otherwise {@code false}.
   */
  public boolean isDoseResponseFittingHillMinMax() {
    return doseResponseFittingHillMinMax;
  }

  /**
   * @return the doseResponseFittingHillMax
   */
  public Float getDoseResponseFittingHillMax() {
    return doseResponseFittingHillMax;
  }

  /**
   * @return the doseResponseFittingHillMin
   */
  public Float getDoseResponseFittingHillMin() {
    return doseResponseFittingHillMin;
  }

  /**
   * Retrieve the user identifier.
   * 
   * @return User identifier (or {@code null} if not assigned).
   */
  public String getUserId() {
    return userId;
  }

  /**
   * Assign the user id.
   * <p>
   * Prefer {@link #clearUserId()} if clearing the user id.
   * 
   * @param userId User identifier to assign (or {@code null} if not required).
   * @see #clearUserId()
   */
  public void setUserId(final String userId) {
    this.userId = userId;
  }

  /**
   * Remove the user id from the simulation.
   * <p>
   * Call this to ensure that user ids are not persisted indefinitely.
   */
  public void clearUserId() {
    this.userId = null;
  }
}