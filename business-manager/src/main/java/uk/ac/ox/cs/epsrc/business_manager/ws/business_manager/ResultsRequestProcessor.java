/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.business_manager;

import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.service.ResultsService;
import uk.ac.ox.cs.epsrc.business_manager.service.SimulationService;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.ConcentrationResultVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.GroupedValuesVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.IonChannelValuesVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.JobDiagnosticsVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.JobResultsVO;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.InputValues;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.InputValuesRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.InputValuesResponse;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.InputValuesStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ObjectFactory;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ResultsConcentrationStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ResultsJobDataStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ResultsRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ResultsResponse;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.RetrieveDiagnosticsRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.RetrieveDiagnosticsResponse;

/**
 * JAXB results request processor.
 *
 * @author geoff
 */
public class ResultsRequestProcessor {

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_RESULTS_SERVICE)
  private ResultsService resultsService;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE)
  private SimulationService simulationService;

  private static final ObjectFactory objectFactory = new ObjectFactory();

  private static final Log log = LogFactory.getLog(ResultsRequestProcessor.class);

  /**
   * Retrieve diagnostics information.
   * 
   * @param request Diagnostics information request.
   * @return Response.
   */
  public RetrieveDiagnosticsResponse retrieveDiagnostics(final RetrieveDiagnosticsRequest request) {
    log.debug("~retrieveDiagnostics() : Invoked.");

    final long jobId = request.getJobId();

    final JobDiagnosticsVO jobDiagnostics = resultsService.retrieveDiagnostics(jobId);

    final RetrieveDiagnosticsResponse response = objectFactory.createRetrieveDiagnosticsResponse();
    response.setJobId(jobId);
    response.setInfo(jobDiagnostics.getInfo());
    response.setOutput(jobDiagnostics.getOutput());

    return response;
  }

  /**
   * Retrieve the input values for the simulation.
   * 
   * @param request Input values request.
   * @return Input values response.
   */
  public InputValuesResponse retrieveInputValues(final InputValuesRequest request) {
    final long simulationId = request.getSimulationId();
    final String identifiedLogPrefix = "~retrieveInputValues() : [" + simulationId + "] : ";
    log.debug(identifiedLogPrefix.concat("Invoked."));

    final InputValuesResponse response = objectFactory.createInputValuesResponse();

    // If request processing has completed there *may* be input values.
    final Set<GroupedValuesVO> groupedInputValues = resultsService.retrieveInputValues(simulationId);
    for (final GroupedValuesVO groupedValues : groupedInputValues) {
      final short groupLevel = groupedValues.getLevel();
      final String groupName = groupedValues.getName();

      final InputValues outInputValues = objectFactory.createInputValues();

      outInputValues.setGroupLevel(groupLevel);
      outInputValues.setGroupName(groupName);
      log.debug(identifiedLogPrefix.concat("Group name '" + groupName + "', level '" + groupLevel + "'."));

      for (final IonChannelValuesVO ionChannelValues : groupedValues.getIonChannelValues()) {
        final String ionChannelName = ionChannelValues.getName();
        final String sourceAssayName = ionChannelValues.getSourceAssayName();
        final String pIC50s = ionChannelValues.getpIC50s();
        final String hills = ionChannelValues.getHills();
        final String originals = ionChannelValues.getOriginals();
        final String strategyOrders = ionChannelValues.getStrategyOrders();

        final InputValuesStructure inputValuesStructure = objectFactory.createInputValuesStructure();
        inputValuesStructure.setIonChannelName(ionChannelName);
        inputValuesStructure.setSourceAssayName(sourceAssayName);
        inputValuesStructure.setPIC50S(pIC50s);
        inputValuesStructure.setHills(hills);
        inputValuesStructure.setOriginals(originals);
        inputValuesStructure.setStrategyOrders(strategyOrders);

        outInputValues.getInputValuesStructure().add(inputValuesStructure);
      }
      response.getInputValues().add(outInputValues);
    }

    return response;
  }

  /**
   * Retrieve the results for a simulation.
   * 
   * @param request Results request.
   * @return Results response.
   */
  public ResultsResponse retrieveResults(final ResultsRequest request) {
    log.debug("~retrieveResults() : Invoked.");
    final long simulationId = request.getSimulationId();

    final ResultsResponse response = objectFactory.createResultsResponse();

    final List<JobResultsVO> jobResults = resultsService.retrieveResults(simulationId);
    for (final JobResultsVO jobResult : jobResults) {
      final long jobId = jobResult.getJobId();
      final String messages = jobResult.getMessages();
      final String deltaAPD90PercentileNames = jobResult.getDeltaAPD90PercentileNames();
      final List<ConcentrationResultVO> concentrationResults = jobResult.getConcentrationResults();

      final ResultsJobDataStructure jobDataStructure = objectFactory.createResultsJobDataStructure();
      jobDataStructure.setJobId(jobId);
      jobDataStructure.setJobPacingFrequency(jobResult.getPacingFrequency());
      jobDataStructure.setJobAssayGroupName(jobResult.getAssayGroupName());
      jobDataStructure.setJobAssayGroupLevel(jobResult.getAssayGroupLevel());
      jobDataStructure.setMessages(messages);
      jobDataStructure.setDeltaAPD90PercentileNames(deltaAPD90PercentileNames);

      for (final ConcentrationResultVO concentrationResult : concentrationResults) {
        final float compoundConcentration = concentrationResult.getCompoundConcentration();
        log.debug("~retrieveResults() : Job id '" + jobId + "', compound concentration '" + compoundConcentration + "'.");
        final String deltaAPD90 = concentrationResult.getDeltaAPD90();
        final String upstrokeVelocity = concentrationResult.getUpstrokeVelocity();
        final String times = concentrationResult.getTimes();
        final String voltages = concentrationResult.getVoltages();
        final String qNet = concentrationResult.getQNet();

        final ResultsConcentrationStructure resultsConcentrationStructure = objectFactory.createResultsConcentrationStructure();
        resultsConcentrationStructure.setCompoundConcentration(compoundConcentration);
        resultsConcentrationStructure.setDeltaAPD90(deltaAPD90);
        resultsConcentrationStructure.setUpstrokeVelocity(upstrokeVelocity);
        resultsConcentrationStructure.setTimes(times);
        resultsConcentrationStructure.setVoltages(voltages);
        resultsConcentrationStructure.setQNet(qNet);

        jobDataStructure.getResultsConcentrationStructure().add(resultsConcentrationStructure);
      }

      response.getResultsJobDataStructure().add(jobDataStructure);
    }

    return response;
  }
}