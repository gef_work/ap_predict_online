/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.checker.ChangeChecker;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayGroupVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 * Configuration object.
 *
 * @author geoff
 */
// Values declared in config/appCtx.config.site.xml
@Component(BusinessIdentifiers.COMPONENT_CONFIGURATION)
public class Configuration {

  private static final Log log = LogFactory.getLog(Configuration.class);

  private final boolean defaultForceReRun;
  /**
   * Flag whether to force a reset on every simulation.
   */
  // Previously considered a truly configurable option, now deemed to be a hardcode candidate.
  public static final boolean DEFAULT_FORCE_RESET = false;
  // Arbitrary default simulation request processing polling period.
  private static final int arbitraryDefaultSimulationRequestProcessingPollingPeriod = 1000;

  private final boolean defaultAssayGrouping;
  private final boolean defaultValueInheriting;
  private final boolean defaultBetweenGroups;
  private final boolean defaultWithinGroups;

  private final short defaultMinInformationLevel;
  private final short defaultMaxInformationLevel;

  private final short defaultQuietPeriod;

  private int defaultRequestProcessingPolling = arbitraryDefaultSimulationRequestProcessingPollingPeriod;

  private static final String comma = ",";

  // Inject any context-defined PC50EvaluationStrategy implementations (appCtx.config.pc50Evaluators.xml)
  @Autowired
  private List<PC50EvaluationStrategy> defaultPC50EvaluationStrategies = new ArrayList<PC50EvaluationStrategy>();

  // Inject any context-defined ChangeChecker implementations (appCtx.config.changeCheckers.site.xml)
  @Autowired
  private List<ChangeChecker> changeCheckers = new ArrayList<ChangeChecker>();

  // Spring-injected via constructor (appCtx.config.assays.site.xml)
  private final Set<AssayVO> assays = new HashSet<AssayVO>();
  // Spring-injected via constructor (appCtx.config.assays.site.xml)
  private final Set<AssayGroupVO> assayGroups = new HashSet<AssayGroupVO>();
  // Spring-injected via constructor (appCtx.config.ionChannels.site.xml)
  private Set<IonChannel> ionChannels = new HashSet<IonChannel>();

  /**
   * Initialising constructor.
   * 
   * (*Note: This period may be overridden by other factors).
   * 
   * @param defaultForceReRun Default simulation force re-run value.
   * @param defaultAssayGrouping Default assay grouping option.
   * @param defaultValueInheriting Default value inheriting option.
   * @param defaultBetweenGroups Default value inheriting between groups option.
   * @param defaultWithinGroups Default value inheriting within groups option.
   * @param defaultMinInformationLevel Default minimum information level.
   * @param defaultMaxInformationLevel Default maximum information level.
   * @param defaultQuietPeriod Period after a simulation run in which a
   *                           simulation request will not invoke a re-run*.
   * @param assays Assays to assign (ctx-specified beans auto-injected).
   * @param assayGroups Assay groups to assign (ctx-specified beans auto-injected).
   * @param ionChannels Ion Channels to assign (ctx-specified beans auto-injected).
   */
  @Autowired(required=true)
  public Configuration(final boolean defaultForceReRun,
                       final boolean defaultAssayGrouping,
                       final boolean defaultValueInheriting,
                       final boolean defaultBetweenGroups,
                       final boolean defaultWithinGroups,
                       final short defaultMinInformationLevel,
                       final short defaultMaxInformationLevel,
                       final short defaultQuietPeriod,
                       final Set<AssayVO> assays,
                       final Set<AssayGroupVO> assayGroups,
                       final Set<IonChannel> ionChannels) {
    log.debug("~Configuration() : Invoked.");

    this.defaultForceReRun = defaultForceReRun;
    this.defaultAssayGrouping = defaultAssayGrouping;
    this.defaultValueInheriting = defaultValueInheriting;
    this.defaultBetweenGroups = defaultBetweenGroups;
    this.defaultWithinGroups = defaultWithinGroups;

    this.defaultMinInformationLevel = defaultMinInformationLevel;
    this.defaultMaxInformationLevel = defaultMaxInformationLevel;
    this.defaultQuietPeriod = defaultQuietPeriod;

    /* Initialised here to ensure constructed *and initialised*.
       As AppManagerManagerImpl retrieves the assays, groups and ion 
       channels in a @PostConstruct, using an @Autowire'd setter at a method
       level can result in Configuration being calling after construction but
       prior to data population (i.e. when not fully initialised) */
    assignAssays(assays);
    assignAssayGroups(assayGroups);
    assignIonChannels(ionChannels);

    log.info("~Configuration() : Created initial configuration of '" + toString() + "'.");
  }

  @PostConstruct
  private void initialiseChainSort() {
    Collections.sort(changeCheckers, AnnotationAwareOrderComparator.INSTANCE);
    Collections.sort(defaultPC50EvaluationStrategies,
                     AnnotationAwareOrderComparator.INSTANCE);

    for (final ChangeChecker changeChecker : changeCheckers) {
      log.info("~initialiseChainSort() : ChangeChecker '" + changeChecker.toString() + "'.");
    }
    for (final PC50EvaluationStrategy pc50EvaluationStrategy : defaultPC50EvaluationStrategies) {
      log.info("~initialiseChainSort() : PC50EvaluationStrategy '" + pc50EvaluationStrategy.toString() + "'.");
    }
  }

  // Assign the assays.
  private void assignAssays(final Set<AssayVO> assays) {
    if (assays == null || assays.isEmpty()) {
      final String warnMessage = "No Assays have been configured!";
      log.warn("~assignAssays() : ".concat(warnMessage));
    } else {
      for (final AssayVO assay : assays) {
        final String assayName = assay.getName();
        log.info("~assignAssays() : Assigning assay '" + assayName + "'.");
        if (assay.hasConfidenceIntervalSpreads()) {
          log.info("~assignAssays() :   Confidence spread intervals '" + assay.getAssaySpreads().toString() + "'.");
        }
        this.assays.add(assay);
      }
    }
  }

  // Assign the assay groups.
  private void assignAssayGroups(final Set<AssayGroupVO> assayGroups) {
    if (assayGroups == null || assayGroups.isEmpty()) {
      final String warnMessage = "No Assay Groups have been configured!";
      log.warn("~assignAssayGroups() : ".concat(warnMessage));
    } else {
      for (final AssayGroupVO assayGroup : assayGroups) {
        final String assayGroupName = assayGroup.getName();
        log.info("~assignAssayGroups() : Assigning assay group '" + assayGroupName + "'.");
        this.assayGroups.add(assayGroup);
      }
    }
  }

  // Assign the ion channels to be made available in the application.
  private void assignIonChannels(final Set<IonChannel> ionChannels) {
    if (ionChannels == null || ionChannels.isEmpty()) {
      final String warnMessage = "No Ion Channels have been configured!";
      log.warn("~assignIonChannels() : ".concat(warnMessage));
    } else {
      for (final IonChannel ionChannel : ionChannels) {
        log.info("~assignIonChannels() : Assigning ion channel name '" + ionChannel + "'.");
        this.ionChannels.add(ionChannel);
      }
    }
  }

  /**
   * Retrieve a map of the assay group level by assay name.
   * 
   * @return Map k: assay name; v: assay group level.
   */
  public Map<String, Short> retrieveAssayGroupLevelByAssayName() {
    log.debug("~retrieveAssayGroupLevelByAssayName() : Invoked.");
    final Map<String, Short> assayGroupLevelByAssayName = new HashMap<String, Short>();
    for (final AssayVO assay : getAssays()) {
      assayGroupLevelByAssayName.put(assay.getName(), assay.getGroup().getLevel());
    }
    return Collections.unmodifiableMap(assayGroupLevelByAssayName);
  }

  /**
   * Retrieve a map of the assay level by assay name.
   * 
   * @return Map k: assay name; v: assay level.
   */
  public Map<String, Short> retrieveAssayLevelByAssayName() {
    log.debug("~retrieveAssayLevelByAssayName() : Invoked.");
    final Map<String, Short> assayLevelByAssayNameMap = new HashMap<String, Short>();
    for (final AssayVO assay : getAssays()) {
      assayLevelByAssayNameMap.put(assay.getName(), assay.getLevel());
    }
    return Collections.unmodifiableMap(assayLevelByAssayNameMap);
  }

  /**
   * Retrieve a map of the assay name by assay level.
   * 
   * @return Map k: assay name; v: assay level.
   */
  public Map<Short, String> retrieveAssayNameByAssayLevel() {
    log.debug("~retrieveAssayNameByAssayLevel() : Invoked.");
    final Map<Short, String> assayNameByAssayLevelMap = new HashMap<Short, String>();
    for (final AssayVO assay : getAssays()) {
      assayNameByAssayLevelMap.put(assay.getLevel(), assay.getName());
    }
    return Collections.unmodifiableMap(assayNameByAssayLevelMap);
  }

  /**
   * Retrieve a map of the assay group name by assay group level.
   * 
   * @return Map k: assay group level; v: assay group name.
   */
  public Map<Short, String> retrieveAssayGroupNameByGroupLevelMap() {
    log.debug("~retrieveGroupNameByGroupLevelMap() : Invoked.");
    final Map<Short, String> groupNameByGroupLevel = new HashMap<Short, String>();
    for (final AssayGroupVO assayGroup : getAssayGroups()) {
      final Short level = assayGroup.getLevel();
      final String name = assayGroup.getName();
      groupNameByGroupLevel.put(level, name);
    }
    return Collections.unmodifiableMap(groupNameByGroupLevel);
  }

  /**
   * Retrieve the ordered CSV of active strategies in their default invocation order value.
   * 
   * @return CSV of active strategies, ordered by default invocation order.
   */
  public String retrieveOrderedStrategiesAsCSV() {
    log.debug("~retrieveOrderedStrategiesAsCSV() : Invoked.");
    final List<String> activeStrategies = new ArrayList<String>();
    for (final PC50EvaluationStrategy strategy : getDefaultPC50EvaluationStrategies()) {
      if (strategy.getDefaultActive()) {
        activeStrategies.add(new Integer(strategy.getOrder()).toString());
      }
    }

    return StringUtils.join(activeStrategies, comma);
  }

  /**
   * Retrieve PC50 evaluation strategies as ordered by their default invocation
   * order.
   * 
   * @param defaultInvocationOrdersCSV Default invocation orders.
   * @return Strategies corresponding to default invocation orders.
   * @throws IllegalArgumentException If {@code null} argument supplied.
   */
  public List<PC50EvaluationStrategy> retrieveStrategiesByCSV(final String defaultInvocationOrdersCSV)
                                                              throws IllegalArgumentException {
    log.debug("~retrieveStrategiesByCSV() : Invoked with '" + defaultInvocationOrdersCSV + "'.");
    if (defaultInvocationOrdersCSV == null) {
      final String errorMessage = "Illegal attempt to retrieve PC50 evaluation strategies using a null default invocation order string.";
      log.error("~retrieveStrategiesByCSV() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }
    final List<String> defaultInvocationOrders = Arrays.asList(StringUtils.split(defaultInvocationOrdersCSV, comma));

    final List<PC50EvaluationStrategy> strategies = new ArrayList<PC50EvaluationStrategy>();
    final Set<Integer> accounted = new HashSet<Integer>();
    for (final String defaultInvocationOrder : defaultInvocationOrders) {
      final int searchForDefaultInvocationOrder = Integer.valueOf(defaultInvocationOrder);
      for (final PC50EvaluationStrategy eachDefaultStrategy : getDefaultPC50EvaluationStrategies()) {
        final int eachDefaultInvocationOrder = eachDefaultStrategy.getOrder();
        if (accounted.contains(eachDefaultInvocationOrder)) {
          continue;
        } else {
          if (searchForDefaultInvocationOrder == eachDefaultInvocationOrder) {
            log.debug("~retrieveStrategiesByCSV() : Appending '" + eachDefaultInvocationOrder + "'.");
            strategies.add(eachDefaultStrategy);
            accounted.add(eachDefaultInvocationOrder);
            break;
          }
        }
      }
    }

    return strategies;
  }

  /**
   * Retrieve the assay configurations.
   * 
   * @return Assays (or empty collection if none!)
   */
  public Set<AssayVO> getAssays() {
    return Collections.unmodifiableSet(assays);
  }

  /**
   * Retrieve a collection of configured assay groups.
   * 
   * @return Unmodifiable collection of configured assay groups.
   */
  public Set<AssayGroupVO> getAssayGroups() {
    return Collections.unmodifiableSet(assayGroups);
  }

  /**
   * Assign the default simulation request processing polling period (in
   * milliseconds)
   * 
   * @param defaultRequestProcessingPolling Default simulation request processing
   *                                        polling period (in ms).
   */
  @Value("${business_manager.request_processing.polling}")
  public void setDefaultRequestProcessingPolling(final int defaultRequestProcessingPolling) {
    if (defaultRequestProcessingPolling > 0) {
      this.defaultRequestProcessingPolling = defaultRequestProcessingPolling;
    }
    log.info("~setDefaultRequestProcessingPolling() : Default request processing polling period '" + defaultRequestProcessingPolling + "'.");
  }

  /**
   * Retrieve the collection of configured ion channels.
   * 
   * @return Unmodifiable collection of configured ion channels.
   */
  public Set<IonChannel> getIonChannels() {
    return Collections.unmodifiableSet(ionChannels);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Configuration [defaultForceReRun=" + defaultForceReRun
        + ", defaultAssayGrouping=" + defaultAssayGrouping
        + ", defaultValueInheriting=" + defaultValueInheriting
        + ", defaultBetweenGroups=" + defaultBetweenGroups
        + ", defaultWithinGroups=" + defaultWithinGroups
        + ", defaultMinInformationLevel=" + defaultMinInformationLevel
        + ", defaultMaxInformationLevel=" + defaultMaxInformationLevel
        + ", defaultQuietPeriod=" + defaultQuietPeriod
        + ", defaultRequestProcessingPolling=" + defaultRequestProcessingPolling
        + ", defaultPC50EvaluationStrategies=" + defaultPC50EvaluationStrategies
        + ", changeCheckers=" + changeCheckers + ", assays=" + assays
        + ", assayGroups=" + assayGroups + ", ionChannels=" + ionChannels + "]";
  }

  /**
   * Retrieve the default PC50 evaluation strategies.
   * 
   * @return Unmodifiable collection of default PC50 evaluation strategies,
   *         empty if none available.
   */
  public List<PC50EvaluationStrategy> getDefaultPC50EvaluationStrategies() {
    return Collections.unmodifiableList(defaultPC50EvaluationStrategies);
  }

  /**
   * Retrieve the flag indicating if to "force re-run" every simulation which is
   * requested.
   * 
   * @return {@code true} if forcing re-run, otherwise {@code false}.
   */
  public boolean isDefaultForceReRun() {
    return defaultForceReRun;
  }

  /**
   * Retrieve the flag indicating if assay grouping is to be on by default.
   * 
   * @return {@code true} of assay grouping is to take place, otherwise
   *         {@code false}.
   */
  public boolean isDefaultAssayGrouping() {
    return defaultAssayGrouping;
  }

  /**
   * Retrieve the flag indicating if values are to be inherited.
   * 
   * @return {@code true} if inheriting between assays (or assay groups), otherwise
   *         {@code false}.
   */
  public boolean isDefaultValueInheriting() {
    return defaultValueInheriting;
  }

  /**
   * Retrieve the flag indicating if values are to be inherited between assays
   * groupings.
   * 
   * @return {@code true} if inheriting between assay groups, otherwise
   *         {@code false}.
   */
  public boolean isDefaultBetweenGroups() {
    return defaultBetweenGroups;
  }

  /**
   * Retrieve the flag indicating if values are to be inherited between assays
   * with their groupings.
   *  
   * @return {@code true} if inheriting values within assay groupings, otherwise
   *         {@code false}.
   */
  public boolean isDefaultWithinGroups() {
    return defaultWithinGroups;
  }

  /**
   * Retrieve the default minimum information level to display. Usually used for
   * showing/hiding progress information in the client.
   * 
   * @return The default minimum information level to display.
   */
  public short getDefaultMinInformationLevel() {
    return defaultMinInformationLevel;
  }

  /**
   * Retrieve the default maximum information level to display. Usually used for
   * showing/hiding progress information in the client.
   * 
   * @return The default maximum information level to provide.
   */
  public short getDefaultMaxInformationLevel() {
    return defaultMaxInformationLevel;
  }

  /**
   * Retrieve the collection of checkers to see if something has changed prior
   * to a simulation run, e.g. if the amount of time between a current and
   * previous simulation run for a compound has been reached.
   * 
   * @return Unmodifiable collection of change checkers.
   */
  public List<ChangeChecker> getChangeCheckers() {
    return Collections.unmodifiableList(changeCheckers);
  }

  /**
   * Retrieve the default quiet period, i.e. the time between a previous and new
   * simulation which must elapse before being considered for a re-run. This is
   * potentially overrun by other settings.
   * 
   * @return The default quiet period (in minutes).
   */
  public short getDefaultQuietPeriod() {
    return defaultQuietPeriod;
  }

  /**
   * Retrieve the default simulation request processing polling period (in
   * milliseconds).
   * 
   * @return Default simulation request processing polling period (in
   *         milliseconds).
   */
  public int getDefaultRequestProcessingPolling() {
    return defaultRequestProcessingPolling;
  }
}