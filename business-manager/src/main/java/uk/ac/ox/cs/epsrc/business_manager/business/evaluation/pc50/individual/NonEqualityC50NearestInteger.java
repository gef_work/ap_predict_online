/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;

import uk.ac.ox.cs.epsrc.business_manager.business.util.DataUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.AbstractPC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.IndividualC50DataStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;

/**
 *  PC50 evaluation strategy which looks for the nearest integer according to the data held in the
 *  record's 50% inhibitory concentration data. 
 *
 * @author geoff
 */
public class NonEqualityC50NearestInteger extends AbstractPC50EvaluationStrategy
                                          implements IndividualC50DataStrategy, Ordered {
  private static final long serialVersionUID = -6616655828735308862L;

  /**
   * {@inheritDoc}
   */
  @Autowired(required=true)
  public NonEqualityC50NearestInteger(final String description, final boolean defaultActive,
                                      final int defaultInvocationOrder) {
    super(description, defaultActive, defaultInvocationOrder);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.IndividualC50DataStrategy#evaluatePC50(uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData, java.lang.String)
   */
  @Override
  public BigDecimal evaluatePC50(final ICData icData, final String recordIdentifier)
                                 throws IllegalArgumentException {
    // Note: Only considering 50% inhib conc data.
    return DataUtil.pc50ByC50NearestInteger(icData, recordIdentifier);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy#usesEqualityModifier()
   */
  @Override
  public boolean usesEqualityModifier() {
    return false;
  }
}