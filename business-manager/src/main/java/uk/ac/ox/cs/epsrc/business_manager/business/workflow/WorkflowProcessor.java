/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.workflow;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.annotation.Header;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual.NonEqualityC50NearestInteger;
import uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.summary.SummaryDataOnlyC50NonEqualityNearestInteger;
import uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.summary.SummaryDataOnlyNonEqualityNonInclusionAlerting;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.IndividualProvenance;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.SummaryProvenance;
import uk.ac.ox.cs.epsrc.business_manager.business.selection.c50.C50DataSelector;
import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.service.SimulationService;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.IndividualIVS;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.InputValueSource;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.SummaryIVS;
import uk.ac.ox.cs.epsrc.business_manager.value.object.IndividualProcessingVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.doseresponse.DoseResponseFittingParamsVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.workflow.OutcomeVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.APIIdentifiers;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.AlertingNonInclusionStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.DerivedFromC20Strategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.IndividualC50DataStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.ModelAsInactiveStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.ModelAsInvalidStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.doseresponse.FittingStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.summary.SummaryOnlyC50DataStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.selection.ic.ICDataSelectionStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.NoSuchDataException;
import uk.ac.ox.cs.nc3rs.business_manager.api.util.ConverterUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingParams;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingResult;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.IndividualProcessing;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICHillVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProblemType;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;

/**
 * Workflow processing.
 *
 * @author geoff
 */
public class WorkflowProcessor {

  private static final BigDecimal defaultInactivePIC50Value = BigDecimal.ZERO;
  private static final BigDecimal defaultHillCoefficientValue = BigDecimal.ONE;
  // Value used in the IndividualPC50s store to indicate that a strategy which does not produce a
  // pIC50 value (e.g. alerting or invalid data) was successful.
  private static final BigDecimal placeholderIndividualPC50sValue = new BigDecimal("-999.999");

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_CONFIGURATION)
  private Configuration configuration;

  // JMS Template for sending provenance details.
  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JMS_TEMPLATE)
  private BusinessManagerJMSTemplate jmsTemplate;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE)
  private SimulationService simulationService;

  private static final Log log = LogFactory.getLog(WorkflowProcessor.class);

  /**
   * Internal class to hold the pIC50 / pXC50 values for physical (or virtual, if the only source
   * of individual data record count is denoted by a summary data record property) individual data
   * records for a particular assay/ion channel combination.
   * <p>
   * <b>NOTE :</b> On occassions (e.g. when alerting or invalid data encountered) the pIC50/pXC50
   * value assigned may be the placeholder value, i.e. this collection is not the definitive set of
   * pIC50s for onward processing, onward processing values are stored in {@linkplain InputValueSource}
   * objects.
   * <p>
   * A collection of separate pC50 values which is gradually filled with pC50 values as the 
   *   PC50 evaluation strategies are traversed.
   * In the case of summary data without individual the "count" of number of individual records
   *   information is sometimes provided in the summary record. Otherwise we try to physically
   *   count the number of individual records.
   */
  private class IndividualPC50s {

    // Use this number of individual data records if actual number can't be determined.
    private static final int numberToCreateOnInsufficientData = 1;

    // Data holding structure. k: individual record identifier (if known, otherwise a number). v: pIC50
    private Map<String, BigDecimal> individualPC50s = new HashMap<String, BigDecimal>();

    /**
     * Initialise a collection of individual record-identified pC50s based on the site's data.
     * This constructor looks at the physical (i.e. what's been read in) and quoted (what's written
     * in the raw data records) counts to determine how many pC50 values will need to be provided
     * for this assay/ion channel.
     * <p>
     * If, for whatever reason, there are physically fewer individual records than the number 
     * quoted in the summary data, then create fake ("anonymous") individual record identifiers with
     * which to hold the PC50 values.
     * <p>
     * The reason for this code is because in some systems there are inconsistencies between what
     * has been manually recorded (regarding individual data) by users in summary data, and what is
     * actually retrievable from the database.
     * 
     * @param identifyingPrefix Logging identifying prefix.
     * @param summaryData Summary data.
     * @param nonSummaryData Non-Summary data.
     * @throws InvalidValueException If an invalid value is encountered.
     */
    private IndividualPC50s(final String identifyingPrefix, final SummaryDataRecord summaryData,
                            final Map<IndividualDataRecord, List<DoseResponseDataRecord>> nonSummaryData)
                            throws InvalidValueException {
      final String logPrefix = "~IndividualPC50s() : ".concat(identifyingPrefix);
      log.debug(logPrefix.concat("Invoked."));

      // Number of individual records used to determine the summary PC50 value.
      Integer minimumNumberOfIndividualPC50sToCreate = null;

      /* Step 1.
           If there's summary record data try to dig out the relevant values. */
      if (summaryData != null) {
        final ICData summaryC50Data = C50DataSelector.selectFrom(summaryData.eligibleICData());
        /* TODO: Hardcoding that we're only interested in using 50% inhib. conc. data to determine
                 count of the records used to determine the IC value and count of available records. */
        if (summaryC50Data != null && summaryC50Data.hasICPctData(ICPercent.PCT50)) {
          Integer sumC50ValueIndRecCount = null;
          Integer allIndRecCount = null;
          try {
            sumC50ValueIndRecCount = summaryC50Data.retrieveCountOfRecordsUsedToDetermineICValue(ICPercent.PCT50);
            allIndRecCount = summaryC50Data.retrieveCountOfAvailableRecords(ICPercent.PCT50);
          } catch (NoSuchDataException e) {}

          if (hasPositiveValue(allIndRecCount)) {
            // The count of all individual records (presumably valid ones!) takes precedence.
            minimumNumberOfIndividualPC50sToCreate = allIndRecCount;
          } else if (hasPositiveValue(sumC50ValueIndRecCount)) {
            // As a backup, if there's a value of the count which contributed to the summary C50 value, use it.
            minimumNumberOfIndividualPC50sToCreate = sumC50ValueIndRecCount;
          } else {
            log.debug(logPrefix.concat("Impossible to determine number of IndividualPC50s to create from summary data!"));
          }
          log.debug(logPrefix.concat("Summary ICData data indicates '" + allIndRecCount +
                                     "' individual records available, of which '" + sumC50ValueIndRecCount +
                                     "' records contributed to the calculation of the C50 value."));
        } else {
          log.warn(logPrefix.concat("No summary ICData data available."));
        }
      } else {
        // Maybe only individual data?
        log.debug(logPrefix.concat("No summary data available."));
      }

      /* Step 2.
           Compare any value derived from the summary C50 data with what has been physically 
           retrieved. Alternative, if no summary indicators, use the individual data alone to
           determine what the minimum number of IndividualPC50s should be. */
      if (!nonSummaryData.isEmpty()) {
        // Count up the number of individual records which appear to have been designated as valid.
        int validIndividualRecordCount = nonSummaryData.entrySet().size();

        if (minimumNumberOfIndividualPC50sToCreate != null) {
          // We have a value derived from summary C50. Compare it with our individual data.
          if (validIndividualRecordCount != minimumNumberOfIndividualPC50sToCreate) {
            log.warn(logPrefix.concat("Inconsistency: Summary record indicates '" + minimumNumberOfIndividualPC50sToCreate +
                                      "' individual records to be used, but counted '" + validIndividualRecordCount +
                                      "' valid individual records"));
          }
        } else {
          // No summary indicators of individual record count, so use the individual count.
          minimumNumberOfIndividualPC50sToCreate = validIndividualRecordCount;
        }
      } else {
        log.debug(logPrefix.concat("No non-summary data available to contribute to determination of IndividualPC50 count!"));
      }

      if (minimumNumberOfIndividualPC50sToCreate == null) {
        log.warn(logPrefix.concat("Could not accurately determine the number of individual records. Assigning a count of '" + numberToCreateOnInsufficientData + "'!"));
        minimumNumberOfIndividualPC50sToCreate = numberToCreateOnInsufficientData;
      }

      // Populate the individual PC50 data holding structures with valid.
      for (final Map.Entry<IndividualDataRecord, List<DoseResponseDataRecord>> nonSummaryDataEntry : nonSummaryData.entrySet()) {
        final IndividualDataRecord individualData = nonSummaryDataEntry.getKey();
        final String individualDataId = individualData.getId();
        log.debug(logPrefix.concat("Created individual PC50 entry with id '" + individualDataId + "'."));
        individualPC50s.put(individualDataId, null);
      }
      final int createdIndividualPC50Count = individualPC50s.size();
      if (createdIndividualPC50Count < minimumNumberOfIndividualPC50sToCreate) {
        // We need to create more holding values, e.g. if there was only a summary record and it indicated the num. of individuals.
        final int anonymousCount = minimumNumberOfIndividualPC50sToCreate - createdIndividualPC50Count;
        for (int i = 0; i < anonymousCount; i++) {
          log.warn(logPrefix.concat("Created anonymous individual PC50 entry with id '" + i + "'."));
          individualPC50s.put(Integer.valueOf(i).toString(), null);
        }
      }
    }

    /**
     * Assign a PC50 value to an identifier (if not already set).
     * 
     * @param individualDataId Individual data identifier.
     * @param pc50 PC50 value to assign.
     * @return True if all individual PC50's have been assigned, otherwise false.
     */
    private boolean assignValue(final String individualDataId, final BigDecimal pc50) {
      return assignValue(individualDataId, pc50, true);
    }

    /*
     * Assign a PC50 value to an identifier (if not already set), indicating if this is an
     * individual assignment.
     * Return indicator of all values in data holding structure being assigned.
     */
    private boolean assignValue(final String individualDataId, final BigDecimal pc50,
                                final boolean individualAssignment) {
      if (!hasValue(individualDataId)) {
        log.debug("~assignValue() : Assigning '" + pc50 + "' to '" + individualDataId + "'.");
        individualPC50s.put(individualDataId, pc50);
      } else {
        log.debug("~assignValue() : Individual id '" + individualDataId + "' already assigned - '" + pc50 + "' ignored.");
      }
      return individualAssignment ? isFull() : false;
    }

    /**
     * Assign a PC50 value to each individual record (if not already set).
     * Returns "true" to indicate all values in data holding structure have been assigned.
     */
    private boolean assignAll(final BigDecimal pc50) {
      log.debug("~assignAll() : Assigning all individual data records a PC50 of '" + pc50 + "'.");
      for (final String individualDataId : individualPC50s.keySet()) {
        assignValue(individualDataId, pc50, false);
      }
      return true;
    }

    // Test for a positive integer value.
    private boolean hasPositiveValue(final Integer testValue) {
      return (testValue != null && testValue > 0);
    }

    // Indicate if individual data record has a PC50 value assigned already
    private boolean hasValue(final String individualDataId) {
      assert(individualPC50s.containsKey(individualDataId)) : "Unrecognised individual PC50 identifier of '" + individualDataId + "'.";
      return (individualPC50s.get(individualDataId) != null);
    }

    // Indicate if data holding structure is full.
    private boolean isFull() {
      boolean isFull = individualPC50s.containsValue(null) == false;
      log.debug("~isFull() : Indicating '" + isFull + "'.");
      return isFull;
    }

    // Retrieve the PC50 contents of the data holding structure.
    private Collection<BigDecimal> getPC50s() {
      return individualPC50s.values();
    }

    // Log any unassigned PC50s to try to locate them
    private void logUnassigned(final String identifyingPrefix) {
      for (final String individualDataId : individualPC50s.keySet()) {
        if (!hasValue(individualDataId)) {
          log.warn("~logUnassigned() : " + identifyingPrefix + " : No PC50 value for individual record with identifier '" + individualDataId + "'.");
        }
      }
    }
  }

  /**
   * Internal class holding the ICData to use for the individual record.
   * <p>
   * The reason this private class exists is because in this parent class we traverse the collection
   * of C50 evaluation strategies and rather than repeatedly query the individual data record for
   * the C50 data to use (note that there may be a number of fields which contain C50 data so we 
   * try each in turn until we find a valid value), we do it once and hold it in the cache.
   */
  private class IndividualICDataCache {
    // Data holding structure.
    private final Map<String, ICData> c50DataCache = new HashMap<String, ICData>();

    /**
     * Retrieve either a cached or a newly selected (via an {@link ICDataSelectionStrategy}) ICData
     * derived from the passed individual data record. This function performs the role of 
     * selecting the ICData value to use from potentially a collection of eligible ICData objects,
     * as were defined in the site-specific individual data record.
     * <p>
     * On first call for an individual data record the value retrieved will be placed in the cache
     * and on any subsequent calls (if necessary) the cached value retrieved.
     * 
     * @param individualData Individual data containing the sites eligible IC data.
     * @return Selected inhibitory concentration data from the individual data (or the cache),
     *         potentially {@code null} if no data available.
     * @throws InvalidValueException If an invalid value is encountered.
     */
    private ICData loadOrRetrieveIDRICData(final IndividualDataRecord individualData)
                                           throws InvalidValueException {
      final String individualId = individualData.getId();
      ICData individualICData = null;
      if (c50DataCache.containsKey(individualId)) {
        log.debug("~getCachedC50Data() : Retrieving individual ICData from cache.");
        individualICData = c50DataCache.get(individualId);
      } else {
        individualICData = C50DataSelector.selectFrom(individualData.eligibleICData());
        c50DataCache.put(individualId, individualICData);
      }
      return individualICData;
    }
  }

  /**
   * Default constructor. For system use.
   */
  protected WorkflowProcessor() {}

  /* Perform a verification check that the input value source data is as expected.
     Current convention is that you can't have mixed summary and individual input value sources,
       nor multiple summary input value sources.
     TODO: See also AggregatedWorkflowOutputDecisionMaker.decide regarding which input value sources
             get passed onwards for processing. */
  private void addInputValueSource(final List<InputValueSource> existingIVSs,
                                   final InputValueSource newIVS) {
    final boolean isSummaryIVSNew = newIVS instanceof SummaryIVS ? true : false;

    for (final InputValueSource existingIVS : existingIVSs) {
      final boolean isSummaryIVSExisting = existingIVS instanceof SummaryIVS ? true : false;
      if (isSummaryIVSExisting && isSummaryIVSNew) {
        throw new UnsupportedOperationException("Can not have multiple summary input value sources generated by workflow processing.");
      }
      if (isSummaryIVSNew != isSummaryIVSExisting) {
        throw new UnsupportedOperationException("Can not have a mix of summary and individual input value sources generated by workflow processing.");
      }
    }

    existingIVSs.add(newIVS);
  }

  /**
   * Process the workflow.
   * <p>
   * Will attempt to take the (per-assay, per-ion channel) site data and loop through the summary
   * and individual data records, trying to extract the pC50 data on per-individual data record
   * basis. For example :
   * <ul>
   *   <li>
   *     If there's only a summary record which indicates a summary pC50 value was derived from
   *     four individual records, then there'll be four replicated pC50 values for the 
   *     assay/ion channel.
   *   </li>
   *   <li>
   *     If there's a summary record with a pC50 modifier, and six individual records then six
   *     individual pC50 values will be expected, derived either from PC50 evaluators which derive
   *     data from individual data records, or from replication of nearest integer pC50 value
   *     based on the summary pC50 and the modifier.
   *   </li>
   * </ul>
   * Note that there may be more than one (per-assay, per-ion channel) site data as on occasions
   * more than one summary record may exist for a compound.
   * 
   * @param compoundIdentifier Compound identifier.
   * @param processingType Processing type.
   * @param simulationId Simulation identifier.
   * @param inputDataGatheringSimulation (Optional) Input data gathering simulation object. 
   * @param siteDataHolder Site data.
   * @return Holding object containing the pIC50(s) (and Hill(s) if appropriate) derived from the
   *         site data.
   * @throws IllegalArgumentException If a {@code null} value arrives for the site data holder.
   * @throws InvalidValueException If an invalid value is encountered.
   */
  public OutcomeVO processWorkflow(final @Header(value=APIIdentifiers.SI_HDR_COMPOUND_IDENTIFIER,
                                                 required=true)
                                         String compoundIdentifier,
                                   final @Header(value=BusinessIdentifiers.SI_HDR_PROCESSING_TYPE,
                                                 required=true)
                                         ProcessingType processingType,
                                   final @Header(value=BusinessIdentifiers.SI_HDR_SIMULATION_ID,
                                                 required=true)
                                         Long simulationId,
                                   final @Header(value=BusinessIdentifiers.SI_HDR_INPUTDATAGATHERING_SIMULATION,
                                                 required=false)
                                         Simulation inputDataGatheringSimulation,
                                   final SiteDataHolder siteDataHolder)
                                   throws IllegalArgumentException, InvalidValueException {
    log.debug("~processWorkflow() : Invoked with compound identifier '" + compoundIdentifier + "', simulation id '" + simulationId + "', processing type '" + processingType + "'.");

    if (siteDataHolder == null) {
      final String errorMessage = "Invalid attempt to process workflow using a null-valued site data holder.";
      log.error("~processWorkflow() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    final boolean inputDataGathering = ProcessingType.isInputDataGathering(processingType);

    final AssayVO assayVO = siteDataHolder.getAssay();
    final IonChannel ionChannel = siteDataHolder.getIonChannel();
    final String assayName = assayVO.getName();
    final String ionChannelName = ionChannel.name();

    final String alertingPrefix = "[" + assayName + "][" + ionChannelName + "] : ";
    final String identifyingPrefix = "[" + simulationId + "][" + compoundIdentifier + "]".concat(alertingPrefix);
    String methodPrefix = "~processWorkflow() : ".concat(identifyingPrefix);

    final SummaryDataRecord summaryData = siteDataHolder.getSummaryDataRecord(false);
    String summaryIdentifier = "";
    String summaryDataId = null;
    if (summaryData != null) {
      summaryDataId = summaryData.getId();
      summaryIdentifier = "S#".concat(summaryDataId).concat(" : ");
      methodPrefix += summaryIdentifier;
    }
    final boolean hasSummaryData = (summaryDataId != null);

    Simulation simulation = null;
    if (inputDataGathering) {
      simulation = inputDataGatheringSimulation;
    } else {
      simulation = simulationService.findBySimulationId(simulationId);
    }

    // TODO : https://bitbucket.org/gef_work/apo_private/issue/36/business-manager-pc50evaluationstrategies
    final String invocationOrdersCSV = simulation.getPc50EvaluationStrategies();
    final List<PC50EvaluationStrategy> pc50EvaluationStrategies = configuration.retrieveStrategiesByCSV(invocationOrdersCSV);

    // Read in the nature of dose-response fitting which the simulation expects, e.g. IC50_ONLY, no rounding, etc..
    final DoseResponseFittingParams doseResponseFittingParams =
          new DoseResponseFittingParamsVO(simulation.getDoseResponseFittingStrategy(),
                                          simulation.isDoseResponseFittingRounding(),
                                          simulation.getDoseResponseFittingHillMax(),
                                          simulation.getDoseResponseFittingHillMin());

    final Map<IndividualDataRecord, List<DoseResponseDataRecord>> nonSummaryData = siteDataHolder.getNonSummaryDataRecords();
    /* This variable determines an expected number of individual PC50s (based on site data content)
       and uses unique identifiers found in individual records to create an association between
       computed PC50s and individual records. It's used during processing to flag up when all
       PC50 values have been determined for the assay / ion channel. */
    final IndividualPC50s individualPC50s = new IndividualPC50s(identifyingPrefix, summaryData,
                                                                nonSummaryData);

    /* This variable holds the full pC50 and Hill Coefficients for individual data records for
       onward processing. */
    final List<InputValueSource> inputValueSources = new ArrayList<InputValueSource>();

    final IndividualICDataCache individualRecordsICDataCache = new IndividualICDataCache();

    boolean isFull = false;
    boolean writeInitialIndividualProvenance = true;
    // Traverse the pIC50-determining strategies.
    for (final PC50EvaluationStrategy pc50EvaluationStrategy : pc50EvaluationStrategies) {
      final Integer defaultInvocationOrder = pc50EvaluationStrategy.getOrder();
      final String strategyDescription = pc50EvaluationStrategy.getDescription();

      log.debug(methodPrefix + "Strategy order '" + defaultInvocationOrder + "', type '" + strategyDescription + "'.");
      if (pc50EvaluationStrategy instanceof SummaryOnlyC50DataStrategy) {
        // Derive pIC50(s) from summary PC50 value if possible.
        if (hasSummaryData) {
          if (!inputDataGathering) {
            /*
            jmsTemplate.sendProvenance(new SummaryProvenance.Builder(InformationLevel.TRACE,
                "Strategy : " + strategyDescription).
                summaryDataId(summaryDataId).build());
            */
          }
          final BigDecimal evaluatedPC50 = ((SummaryOnlyC50DataStrategy) pc50EvaluationStrategy).evaluatePC50(siteDataHolder,
                                                                                                              summaryIdentifier);
          if (evaluatedPC50 != null) {
            log.debug(methodPrefix + "SummaryOnlyC50DataStrategy returned a PC50 of '" + evaluatedPC50 + "'.");
            // Fill collection to indicate we have found every individual PC50 value from a single summary value.
            isFull = individualPC50s.assignAll(evaluatedPC50);
            final List<BigDecimal> pIC50s = new ArrayList<BigDecimal>(individualPC50s.getPC50s());
            BigDecimal hillCoefficient = defaultHillCoefficientValue;
            boolean isDefaultHill = true;
            final BigDecimal eligibleSummaryHillCoefficient = summaryData.eligibleHillCoefficient();
            if (pc50EvaluationStrategy instanceof SummaryDataOnlyC50NonEqualityNearestInteger) {
              log.info(methodPrefix.concat("Nearest Integer (Summary) strategy - Ignore '" + eligibleSummaryHillCoefficient + "', use '" + hillCoefficient + "' instead!"));
            } else {
              if (eligibleSummaryHillCoefficient != null) {
                hillCoefficient = eligibleSummaryHillCoefficient;
                isDefaultHill = false;
              }
            }

            if (!inputDataGathering) {
              jmsTemplate.sendProvenance(new SummaryProvenance.Builder(InformationLevel.INFO,
                                                                       retrieveProvenancePrefix(strategyDescription),
                                                                       simulationId)
                                                              .summaryDataId(summaryDataId)
                                                              .pIC50s(pIC50s)
                                                              .hillCoefficient(hillCoefficient)
                                                              .build());
              jmsTemplate.sendProvenance(new SummaryProvenance.Builder(InformationLevel.INFO,
                                                                       retrieveProvenanceData(evaluatedPC50, hillCoefficient, isDefaultHill),
                                                                       simulationId)
                                                              .summaryDataId(summaryDataId)
                                                              .build());
            }

            addInputValueSource(inputValueSources, new SummaryIVS(pIC50s, hillCoefficient,
                                                                  pc50EvaluationStrategy,
                                                                  summaryData));

            // Summary data has spoken! All PC50s have been assigned so break.
            break;
          } else {
            if (!inputDataGathering) {
              jmsTemplate.sendProvenance(new SummaryProvenance.Builder(InformationLevel.TRACE,
                                                                       retrieveNoInputValuePrefix(strategyDescription),
                                                                       simulationId)
                                                              .summaryDataId(summaryDataId).build());
            }
            log.debug(methodPrefix + "SummaryOnlyC50DataStrategy returned null.");
          }
        } else {
          // No summary data, so won't be needing summary pC50 evaluation
        }
      } else if (pc50EvaluationStrategy instanceof SummaryDataOnlyNonEqualityNonInclusionAlerting) {
        if (hasSummaryData) {
          final SummaryDataRecord summaryDataOnlyRecord = siteDataHolder.getSummaryDataRecord(true);
          // Only interested if summary data and no individual data
          if (summaryDataOnlyRecord != null) {
            log.debug(methodPrefix.concat("Summary record and no individual data."));
            final ICData summaryOnlyICData = C50DataSelector.selectFrom(summaryDataOnlyRecord.eligibleICData());
            final String alert = ((SummaryDataOnlyNonEqualityNonInclusionAlerting) pc50EvaluationStrategy)
                                 .soundAlert(summaryOnlyICData);
            if (alert != null) {
              final String identifiedAlert = alertingPrefix + " Identifier [" + summaryDataOnlyRecord.getId() + "] : " + alert;
              if (!inputDataGathering) {
                // Sending to overall progress because potentially no simulation will run.
                jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.WARN,
                                                                     identifiedAlert,
                                                                     new String[] { }, simulationId)
                                                            .build());

                // If this was the only data as a source of a Simulation Job then it may not be visible!
                jmsTemplate.sendProvenance(new SummaryProvenance.Builder(InformationLevel.INFO,
                                                                         retrieveProvenancePrefix(strategyDescription),
                                                                         simulationId)
                                                                .summaryDataId(summaryDataId)
                                                                .build());
                jmsTemplate.sendProvenance(new SummaryProvenance.Builder(InformationLevel.INFO,
                                                                         alert, simulationId)
                                                                .summaryDataId(summaryDataId)
                                                                .build());
              }
              isFull = individualPC50s.assignAll(placeholderIndividualPC50sValue);
              if (isFull) {
                break;
              } else {
                continue;
              }
            } else {
              log.debug(methodPrefix + "Alert was null!");
            }
          }
        }
      } else if (pc50EvaluationStrategy instanceof DerivedFromC20Strategy ||
                 pc50EvaluationStrategy instanceof FittingStrategy || 
                 pc50EvaluationStrategy instanceof IndividualC50DataStrategy ||
                 pc50EvaluationStrategy instanceof ModelAsInactiveStrategy ||
                 pc50EvaluationStrategy instanceof ModelAsInvalidStrategy ||
                 pc50EvaluationStrategy instanceof AlertingNonInclusionStrategy) {
        // Derive pIC50 from individual dose-response or pC50 data.
        for (final Map.Entry<IndividualDataRecord, List<DoseResponseDataRecord>> nonSummaryEntry :
                   nonSummaryData.entrySet()) {
          final IndividualDataRecord individualData = nonSummaryEntry.getKey();
          final List<DoseResponseDataRecord> doseResponseData = nonSummaryEntry.getValue();

          final String individualId = individualData.getId();
          // Check to see if a pIC50 value has already been assigned - if so process next.
          if (individualPC50s.hasValue(individualId)) {
            continue;
          }

          /* Retrieve the definitive C50 data which can be derived from the potential collection
             determinable from the individual record. */
          final ICData individualICData = individualRecordsICDataCache.loadOrRetrieveIDRICData(individualData);
          if (individualICData == null) {
            if (writeInitialIndividualProvenance) {
              // Only write provenance indicating inability to determine a 50% inhib conc. value
              // the first time the non-summary is processed (during per-strategy processing).
              if (!inputDataGathering) {
                jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.WARN,
                                                                     ProblemType.INDETERMINABLE_50_PER_CENT_INHIB_CONC.getMessageKey(),
                                                                     new String[] { assayName, ionChannelName,
                                                                                    individualData.getSourceName() },
                                                                     simulationId).build());

                final String warning = alertingPrefix.concat("[" + individualData.getSourceName() + "] : Important! 50% inhibition data indeterminable");
                jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.WARN,
                                                                            warning, simulationId)
                                                                   .individualDataId(individualId)
                                                                   .build());
              }
            }
            log.info(methodPrefix + "Could not determine individual ICData!");
            continue;
          }

          if (writeInitialIndividualProvenance) {
            // Only write provenance the first time the non-summary is processed (during per-
            // strategy processing).
            if (!inputDataGathering) {
              /*
              jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.INFO,
                  "Individual data has pIC50 '" + individualICData.getPC50Value() + "', modifier '" + individualICData.getPC50Modifier() + "'.").
                  individualDataId(individualId).build());
              */
            }
          }
          if (!inputDataGathering) {
            /*
            jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.TRACE,
                "Strategy : " + strategyDescription).
                individualDataId(individualId).build());
            */
          }

          final String recordIdentifier = identifyingPrefix.concat(summaryIdentifier).
                                                            concat("I#".concat(individualId).
                                                            concat(" : "));

          if (pc50EvaluationStrategy instanceof FittingStrategy) {
            // We're going to try to determine a PC50 via dose-response fitting.
            final IndividualProcessing individualProcessing = new IndividualProcessingVO(summaryData,
                                                                                         individualData,
                                                                                         doseResponseData);
            /* TODO: Hardcoding that we're only testing dose-response fitting against a 50% 
                     inhibitory concentration value in individual data. */
            final DoseResponseFittingResult fittingResult =
                  ((FittingStrategy) pc50EvaluationStrategy).fitDoseResponseData(simulationId,
                                                                                 inputDataGathering,
                                                                                 individualProcessing,
                                                                                 individualICData,
                                                                                 recordIdentifier,
                                                                                 doseResponseFittingParams,
                                                                                 ICPercent.PCT50);
            if (fittingResult != null) {
              final String fittingError = fittingResult.getError();
              if (fittingError != null) {
                log.warn(methodPrefix.concat("No implementation of dose-response fitting error handling."));
              }
              final BigDecimal fitted_pIC50 = fittingResult.getPC50Value();
              if (fitted_pIC50 != null) {
                final BigDecimal fittedHillCoefficient = fittingResult.getHillCoefficient();
                final List<BigDecimal> provenanceValue = new ArrayList<BigDecimal>(1);
                provenanceValue.add(fitted_pIC50);
                if (!inputDataGathering) {
                  jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.INFO,
                                                                              retrieveProvenancePrefix(strategyDescription),
                                                                              simulationId)
                                                                     .individualDataId(individualId)
                                                                     .pIC50s(provenanceValue)
                                                                     .workflowHill(fittedHillCoefficient)
                                                                     .build());
                  jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.INFO,
                                                                              retrieveProvenanceData(fitted_pIC50,
                                                                                                     fittedHillCoefficient,
                                                                                                     false),
                                                                              simulationId)
                                                                     .individualDataId(individualId)
                                                                     .build());
                }
                isFull = individualPC50s.assignValue(individualId, fitted_pIC50);

                addInputValueSource(inputValueSources, new IndividualIVS(fitted_pIC50,
                                                                         fittedHillCoefficient,
                                                                         pc50EvaluationStrategy));
                if (isFull) {
                  break;
                } else {
                  continue;
                }
              }
            } else {
              if (!inputDataGathering) {
                jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.TRACE,
                                                                            retrieveNoInputValuePrefix(strategyDescription),
                                                                            simulationId)
                                                                   .individualDataId(individualId)
                                                                   .build());
              }
            }
          } else if (pc50EvaluationStrategy instanceof IndividualC50DataStrategy) {
            // We're going to try to determine a PC50 from the individual data record.
            final BigDecimal c50Data_pIC50 = ((IndividualC50DataStrategy) pc50EvaluationStrategy).evaluatePC50(individualICData,
                                                                                                               recordIdentifier);
            if (c50Data_pIC50 != null) {
              BigDecimal hillCoefficient = defaultHillCoefficientValue;
              boolean isDefaultHill = true;
              final BigDecimal eligibleIndividualHillCoefficient = individualData.eligibleHillCoefficient();
              if (pc50EvaluationStrategy instanceof NonEqualityC50NearestInteger) {
                log.info(methodPrefix.concat("Nearest Integer (Individual) strategy - Ignore '" + eligibleIndividualHillCoefficient + "', use '" + hillCoefficient + "' instead!"));
              } else {
                if (eligibleIndividualHillCoefficient != null) {
                  hillCoefficient = eligibleIndividualHillCoefficient;
                  isDefaultHill = false;
                }
              }

              final List<BigDecimal> provenanceValue = new ArrayList<BigDecimal>(1);
              provenanceValue.add(c50Data_pIC50);
              if (!inputDataGathering) {
                jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.INFO,
                                                                            retrieveProvenancePrefix(strategyDescription),
                                                                            simulationId)
                                                                   .individualDataId(individualId)
                                                                   .pIC50s(provenanceValue)
                                                                   .workflowHill(hillCoefficient)
                                                                   .build());
                jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.INFO,
                                                                            retrieveProvenanceData(c50Data_pIC50,
                                                                                                   hillCoefficient,
                                                                                                   isDefaultHill),
                                                                            simulationId)
                                                                   .individualDataId(individualId)
                                                                   .build());
              }
              isFull = individualPC50s.assignValue(individualId, c50Data_pIC50);

              addInputValueSource(inputValueSources, new IndividualIVS(c50Data_pIC50,
                                                                       hillCoefficient,
                                                                       pc50EvaluationStrategy));
              if (isFull) {
                break;
              } else {
                continue;
              }
            } else {
              if (!inputDataGathering) {
                jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.TRACE,
                                                                            retrieveNoInputValuePrefix(strategyDescription),
                                                                            simulationId)
                                                                   .individualDataId(individualId)
                                                                   .build());
              }
            }
          } else if (pc50EvaluationStrategy instanceof ModelAsInactiveStrategy) {
            final String reason = ((ModelAsInactiveStrategy) pc50EvaluationStrategy).modelAsInactive(assayVO,
                                                                                                     individualData,
                                                                                                     individualICData,
                                                                                                     identifyingPrefix);
            if (reason != null) {
              if (!inputDataGathering) {
                final List<BigDecimal> provenanceValue = new ArrayList<BigDecimal>(1);
                provenanceValue.add(defaultInactivePIC50Value);
                jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.INFO,
                                                                            retrieveProvenancePrefix(strategyDescription),
                                                                            simulationId)
                                                                   .individualDataId(individualId)
                                                                   .pIC50s(provenanceValue)
                                                                   .workflowHill(defaultHillCoefficientValue)
                                                                   .build());
                jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.INFO,
                                                                            reason, simulationId)
                                                                   .individualDataId(individualId)
                                                                   .build());
              }

              isFull = individualPC50s.assignValue(individualId, defaultInactivePIC50Value);
              addInputValueSource(inputValueSources, new IndividualIVS(defaultInactivePIC50Value,
                                                                       defaultHillCoefficientValue,
                                                                       pc50EvaluationStrategy));
              if (isFull) {
                break;
              } else {
                continue;
              }
            } else {
              if (!inputDataGathering) {
                jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.TRACE,
                                                                            retrieveNoInputValuePrefix(strategyDescription),
                                                                            simulationId)
                                                                   .individualDataId(individualId)
                                                                   .build());
              }
            }
          } else if (pc50EvaluationStrategy instanceof DerivedFromC20Strategy) {
            final ICHillVO derivedFromIC20 = ((DerivedFromC20Strategy) pc50EvaluationStrategy).deriveFromC20(individualData,
                                                                                                             individualICData,
                                                                                                             identifyingPrefix);
            if (derivedFromIC20 != null) {
              final BigDecimal pIC50Value = derivedFromIC20.getIcValue();
              final BigDecimal hillCoefficient = derivedFromIC20.getHillValue();

              if (!inputDataGathering) {
                jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.INFO,
                                                                            retrieveProvenancePrefix(strategyDescription),
                                                                            simulationId)
                                                                   .individualDataId(individualId)
                                                                   .pIC50s(Arrays.asList( new BigDecimal[] { pIC50Value } ))
                                                                   .workflowHill(hillCoefficient)
                                                                   .build());
                jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.INFO,
                                                                            retrieveProvenanceData(pIC50Value,
                                                                                                   hillCoefficient,
                                                                                                   false),
                                                                            simulationId)
                                                                   .individualDataId(individualId)
                                                                   .build());
              }

              isFull = individualPC50s.assignValue(individualId, pIC50Value);

              addInputValueSource(inputValueSources, new IndividualIVS(pIC50Value, hillCoefficient,
                                                                       pc50EvaluationStrategy));
              if (isFull) {
                break;
              } else {
                continue;
              }
            } else {
              if (!inputDataGathering) {
                jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.TRACE,
                                                                            retrieveNoInputValuePrefix(strategyDescription),
                                                                            simulationId)
                                                                   .individualDataId(individualId)
                                                                   .build());
              }
            }
          } else if (pc50EvaluationStrategy instanceof ModelAsInvalidStrategy) {
            final String modelAsInvalid = ((ModelAsInvalidStrategy) pc50EvaluationStrategy).modelAsInvalid(assayVO,
                                                                                                           individualData,
                                                                                                           individualICData,
                                                                                                           identifyingPrefix);
            if (modelAsInvalid != null) {
              log.debug(methodPrefix.concat("Model as invalid!"));
              final String identifiedInvalid = alertingPrefix + " Identifier [" + individualId + "] : " + modelAsInvalid;
              if (!inputDataGathering) {
                jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.WARN,
                                                                     identifiedInvalid,
                                                                     new String[] {}, simulationId)
                                                            .build());

                // If this was the only data as a source of a Simulation Job then it won't be visible!
                jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.WARN,
                                                                            "Data considered invalid!",
                                                                            simulationId)
                                                                   .individualDataId(individualId)
                                                                   .build());
              }

              isFull = setPlaceholderIndividualPC50sValue(individualId, individualPC50s,
                                                          identifyingPrefix);
              if (isFull) {
                break;
              } else {
                continue;
              }
            } else {
              if (!inputDataGathering) {
                jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.TRACE,
                                                                            retrieveNoInputValuePrefix(strategyDescription),
                                                                            simulationId)
                                                                   .individualDataId(individualId)
                                                                   .build());
              }
            }
          } else if (pc50EvaluationStrategy instanceof AlertingNonInclusionStrategy) {
            log.debug(methodPrefix + "Alerting Strategy");
            final AlertingNonInclusionStrategy strategy = (AlertingNonInclusionStrategy) pc50EvaluationStrategy;
            final String alert = strategy.soundAlert(individualICData);
            if (alert != null) {
              final String identifiedAlert = alertingPrefix + " Identifier [" + individualId + "] : " + alert;
              if (!inputDataGathering) {
                // Sending to overall progress because potentially no simulation will run.
                jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.WARN,
                                                                     identifiedAlert,
                                                                     new String[] { }, simulationId)
                                                            .build());

                // If this was the only data as a source of a Simulation Job then it may not be visible!
                jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.INFO,
                                                                            retrieveProvenancePrefix(strategyDescription),
                                                                            simulationId)
                                                                   .individualDataId(individualId)
                                                                   .build());
                jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.INFO,
                                                                            alert, simulationId)
                                                                   .individualDataId(individualId)
                                                                   .build());
              }
              isFull = setPlaceholderIndividualPC50sValue(individualId, individualPC50s,
                                                          identifyingPrefix);
              if (isFull) {
                break;
              } else {
                continue;
              }
            } else {
              log.debug(methodPrefix + "Alert was null!");
            }
          }
        } // end 'for (nonSummaryEntry) {' 
        if (writeInitialIndividualProvenance) {
          writeInitialIndividualProvenance = false;
        }
      } else {
        final String errorMessage = "Unrecognised PC50EvaluationStrategy of '" + pc50EvaluationStrategy + "' encountered!";
        log.error(errorMessage);
        throw new UnsupportedOperationException(methodPrefix.concat(errorMessage));
      }
      if (isFull) {
        // Exit the PC50 evaluation strategy traversal.
        break;
      }
    }

    if (isFull) {
      log.debug(methodPrefix + "All PC50s '" + StringUtils.join(individualPC50s.getPC50s(), ",") + "' found for '" + siteDataHolder.toString() +"' in workflow processing.");
    } else {
      individualPC50s.logUnassigned(identifyingPrefix);
    }

    return new OutcomeVO(assayVO, ionChannel, inputValueSources);
  }

  // Retrieve a prefix for provenance data when no pIC50 evaluated by strategy
  private String retrieveNoInputValuePrefix(final String strategyDescription) {
    return "No input value for : ".concat(strategyDescription);
  }

  // Retrieve a prefix for provenance which is indicative of the strategy used.
  private String retrieveProvenancePrefix(final String strategyDescription) {
    // Use of the pipe symbol corresponds to client provenance/progress highlighting!
    return "|".concat(strategyDescription).concat("| : ");
  }

  // Retrieve a summation of the pIC50/Hill data evaluated.
  private String retrieveProvenanceData(final BigDecimal pIC50, final BigDecimal hill,
                                        final boolean isDefaultHill) {
    final StringBuffer provenanceData = new StringBuffer();
    provenanceData.append("pIC50 '");
    provenanceData.append(ConverterUtil.bdShortened(pIC50));
    provenanceData.append("', Hill '");
    provenanceData.append(ConverterUtil.bdShortened(hill));
    provenanceData.append("'");
    if (isDefaultHill) {
      provenanceData.append(" (default)");
    }
    return provenanceData.toString();
  }

  // Assigning a dummy value for the purpose of acknowledging decision made!
  private boolean setPlaceholderIndividualPC50sValue(final String individualId,
                                                     final IndividualPC50s individualPC50s,
                                                     final String identifyingPrefix) {
    log.debug("~setPlaceholderIndividualPC50sValue() : " + identifyingPrefix + "Setting placeholder value of '" + placeholderIndividualPC50sValue + "'.");
    return individualPC50s.assignValue(individualId, placeholderIndividualPC50sValue);
  }
}
