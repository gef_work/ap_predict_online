/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.DoseResponseData;
import uk.ac.ox.cs.epsrc.business_manager.ws.dose_response.FDRRunUtil;
import uk.ac.ox.cs.epsrc.fdr_manager.ws._1.FDRRunRequest;
import uk.ac.ox.cs.epsrc.fdr_manager.ws._1.FDRRunResponse;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingParams;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingResult;

/**
 * Web service gateway proxy for talking to the dose-response fitting service.
 *
 * @author geoff
 */
public class FDRServicesProxy extends WebServiceGatewaySupport {

  private static final Log log = LogFactory.getLog(FDRServicesProxy.class);

  /**
   * Call the dose-response fitting service.
   * 
   * @param doseResponseData Dose-Response data to convert to the service request.
   * @param doseResponseFittingParams Dose-Response fitting parameters.
   * @return Value object of the dose-response service results, or null if WS failure or invalid data encountered.
   */
  public DoseResponseFittingResult callFDRWebService(final DoseResponseData doseResponseData,
                                                     final DoseResponseFittingParams doseResponseFittingParams) {
    log.debug("~callFDRWebService() : Invoked.");

    final FDRRunRequest fdrRunRequest = FDRRunUtil.createFDRRunRequest(doseResponseData);
    fdrRunRequest.setParameterfitting(doseResponseFittingParams.getFittingOption());
    fdrRunRequest.setRounded(doseResponseFittingParams.isRounding());
    fdrRunRequest.setHillmax(doseResponseFittingParams.getHillMax());
    fdrRunRequest.setHillmin(doseResponseFittingParams.getHillMin());

    FDRRunResponse fdrRunResponse = null;
    try {
      fdrRunResponse = (FDRRunResponse) getWebServiceTemplate().marshalSendAndReceive(fdrRunRequest);
    } catch (WebServiceIOException e) {
      log.error("~callFDRWebService() : Error '" + e.getMessage() + "' for dose-response fitting request '" + doseResponseData.toString() + "'.");
      e.printStackTrace();
    }

    DoseResponseFittingResult doseResponseFittingResult = null;
    if (fdrRunResponse != null) {
      doseResponseFittingResult = FDRRunUtil.dataExtractFDRRunResponse(fdrRunResponse);
      if (doseResponseFittingResult == null) {
        log.warn("~callFDRWebService() : Could not extract a dose-response fitting result for '" + doseResponseData.toString() + "'.");
      }
    }
    return doseResponseFittingResult;
  }
}
