/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.dao.information;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager.INFORMATION_FORMAT;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.DoseResponseData;
import uk.ac.ox.cs.epsrc.business_manager.value.object.InformationVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.util.LogUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.doseresponse.DoseResponsePairVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

import com.orientechnologies.orient.core.config.OGlobalConfiguration;
import com.orientechnologies.orient.core.db.ODatabaseRecordThreadLocal;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentPool;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.db.record.ODatabaseRecordTx;
import com.orientechnologies.orient.core.exception.OStorageException;
import com.orientechnologies.orient.core.id.ORID;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OSchema;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.query.OSQLSynchQuery;

/**
 * Provenance data access object implementation.
 * <p>
 * NOTE : This class should be wrapped by AOP classes, e.g. {@link uk.ac.ox.cs.epsrc.business_manager.aspect.information.OrientArgChecker}
 * which perform transaction management and argument validation.
 *
 * @author geoff
 */
@Component(BusinessIdentifiers.COMPONENT_ORIENT_DAO)
public class OrientDAOImpl implements InformationDAO {
  /*
   * Provenance persistence
   */
  private static final String classFundamental = "Fundamental";
  private static final String classSummary = "Summary";
  private static final String classIndividual = "Individual";
  private static final String classDoseResponse = "DoseResponse";

  private static final String fieldNameCompoundIdentifier = "compoundIdentifier";
  private static final String fieldNameSimulationId = "simulationId";
  // The field actually stores the orient class id values of the summary classes.
  private static final String fieldNameSummaryData = "summaryData";
  // The field actually stores the orient class id values of the individual classes. 
  private static final String fieldNameIndividualData = "individualData";

  private static final String fieldNameRawData = "rawData";
  // Used when a summary data value is replicated between the count of contributing individual records
  private static final String fieldNameSummary_pIC50s = "summary_pIC50s";
  private static final String fieldNameSummaryHillCoeff = "summary_hillCoeff";
  // Used when an individual data value is derived from an individual (or contributing dose-response) records.
  private static final String fieldNameIndividual_pIC50 = "individual_pIC50";
  private static final String fieldNameIndividualRawHill = "individual_rawHill";
  private static final String fieldNameIndividualWorkflowHill = "individual_workflowHill";

  private static final String fieldNameSummaryDataId = "summaryDataId";
  private static final String fieldNameAssayName = "assayName";
  private static final String fieldNameIonChannelName = "ionChannelName";

  private static final String fieldNameIndividualDataId = "individualDataId";
  // The field actually stores the orient class id values of the dose-response classes.
  private static final String fieldNameDoseResponseData = "doseResponseData";

  private static final String fieldNameDoseResponseDataId = "doseResponseDataId";
  private static final String fieldNameDose_in_uM = "dose_in_uM";
  private static final String fieldNameDoseResponse = "doseResponse";

  private static final String fieldNameProvenance = "provenance";
  private static final String fieldNameLevel = "level";
  private static final String fieldNameText = "text";
  private static final String fieldNameArgs = "args";

  /*
   * Progress persistence
   */
  private static final String classProgressAll = "All";
  private static final String classProgressJob = "Job";

  private static final String fieldNameProgress = "progress";
  private static final String fieldNameJob = "job";

  private static final String fieldNameTimestamp = "timestamp";

  private static final String fieldNameJobId = "jobId";
  private static final String fieldNameGroupName = "groupName";
  private static final String fieldNameGroupLevel = "groupLevel";
  private static final String fieldNamePacingFrequency = "pacingFrequency";

  public static final String ORIENT_INAME = "memory:live";
  public static final String ORIENT_IUSERNAME = "admin";
  public static final String ORIENT_IUSERPASSWORD = "admin";

  private static final String regex = "\\\"#\\d+:\\d+\\\"";
  private static final Pattern p = Pattern.compile(regex);

  private static final Log log = LogFactory.getLog(OrientDAOImpl.class);

  @PostConstruct
  protected void initialiseOrient() {
    final String methodName = prefConcat("initialiseOrient()");
    log.trace(methodName.concat(" : Invoked."));
    OGlobalConfiguration.LOG_CONSOLE_LEVEL.setValue("info");
    OGlobalConfiguration.LOG_FILE_LEVEL.setValue("info");

    // Default database pool maximum size
    OGlobalConfiguration.DB_POOL_MAX.setValue(200);
    // Time in ms of delay to check pending closed connections (checks for dirty conns and frees them)
    OGlobalConfiguration.SERVER_CHANNEL_CLEAN_DELAY.setValue(1000);
    // Dumps the configuration at application startup
    OGlobalConfiguration.ENVIRONMENT_DUMP_CFG_AT_STARTUP.setValue(true);

    ODatabaseDocumentTx database = null;
    try {
      try {
        log.trace(methodName.concat(" : About to acquire global [1]."));
        database = ODatabaseDocumentPool.global().acquire(ORIENT_INAME, ORIENT_IUSERNAME,
                                                          ORIENT_IUSERPASSWORD);
      } catch (OStorageException e) {
        log.trace(methodName.concat(" : Exception '" + e.getMessage() + "'."));
        log.trace(methodName.concat(" : Creating new database '" + ORIENT_INAME + "'."));
        new ODatabaseDocumentTx(ORIENT_INAME).create();
        log.trace(methodName.concat(" : About to acquire global [2]."));
        database = ODatabaseDocumentPool.global().acquire(ORIENT_INAME, ORIENT_IUSERNAME,
                                                          ORIENT_IUSERPASSWORD);
        final OSchema schema = database.getMetadata().getSchema();

        final OClass fundamentalClass = schema.createClass(classFundamental);
        final OClass summaryClass = schema.createClass(classSummary);
        final OClass individualClass = schema.createClass(classIndividual);
        final OClass doseResponseClass = schema.createClass(classDoseResponse);

        fundamentalClass.createProperty(fieldNameCompoundIdentifier, OType.STRING).
                         setMandatory(true).setMin("1");
        fundamentalClass.createProperty(fieldNameSimulationId, OType.LONG);
        fundamentalClass.createProperty(fieldNameSummaryData, OType.LINKSET, summaryClass);
        fundamentalClass.createProperty(fieldNameIndividualData, OType.LINKSET, individualClass);
        fundamentalClass.createProperty(fieldNameProvenance, OType.EMBEDDEDLIST);

        summaryClass.createProperty(fieldNameSummaryDataId, OType.STRING).
                                    setMandatory(true).setMin("1");
        summaryClass.createProperty(fieldNameIndividualData, OType.LINKSET, individualClass);
        summaryClass.createProperty(fieldNameSimulationId, OType.LONG);
        summaryClass.createProperty(fieldNameAssayName, OType.STRING);
        summaryClass.createProperty(fieldNameIonChannelName, OType.STRING);
        summaryClass.createProperty(fieldNameSummary_pIC50s, OType.EMBEDDEDLIST);
        summaryClass.createProperty(fieldNameRawData, OType.STRING);
        summaryClass.createProperty(fieldNameProvenance, OType.EMBEDDEDLIST);

        individualClass.createProperty(fieldNameIndividualDataId, OType.STRING).
                                       setMandatory(true).setMin("1");
        individualClass.createProperty(fieldNameDoseResponseData, OType.LINKSET, doseResponseClass);
        individualClass.createProperty(fieldNameSimulationId, OType.LONG);
        individualClass.createProperty(fieldNameAssayName, OType.STRING);
        individualClass.createProperty(fieldNameIonChannelName, OType.STRING);
        individualClass.createProperty(fieldNameIndividual_pIC50, OType.STRING);
        individualClass.createProperty(fieldNameIndividualRawHill,  OType.STRING);
        individualClass.createProperty(fieldNameIndividualWorkflowHill, OType.STRING);
        /* Note: There are dose-response properties assigned dynamically in individualClass objects. */
        individualClass.createProperty(fieldNameRawData, OType.STRING);
        individualClass.createProperty(fieldNameProvenance, OType.EMBEDDEDLIST);

        doseResponseClass.createProperty(fieldNameDoseResponseDataId, OType.STRING).
                                         setMandatory(true).setMin("1");
        doseResponseClass.createProperty(fieldNameSimulationId, OType.LONG);
        doseResponseClass.createProperty(fieldNameRawData, OType.STRING);
        doseResponseClass.createProperty(fieldNameDose_in_uM, OType.STRING);
        doseResponseClass.createProperty(fieldNameDoseResponse, OType.STRING);
        doseResponseClass.createProperty(fieldNameProvenance, OType.EMBEDDEDLIST);

        final OClass allProgressClass = schema.createClass(classProgressAll);
        final OClass jobProgressClass = schema.createClass(classProgressJob);

        // Represents all progress.
        allProgressClass.createProperty(fieldNameCompoundIdentifier, OType.STRING);
        allProgressClass.createProperty(fieldNameSimulationId, OType.LONG)
                        .setMandatory(true).setMin("1");
        allProgressClass.createProperty(fieldNameProgress, OType.EMBEDDEDLIST);
        allProgressClass.createProperty(fieldNameJob, OType.LINKSET, jobProgressClass);

        // Represents simulation jobs (usu. an assay grouping and freq object) progress
        jobProgressClass.createProperty(fieldNameJobId, OType.LONG);
        jobProgressClass.createProperty(fieldNameGroupName, OType.STRING);
        jobProgressClass.createProperty(fieldNameGroupLevel, OType.SHORT);
        jobProgressClass.createProperty(fieldNamePacingFrequency, OType.FLOAT);
        jobProgressClass.createProperty(fieldNameProgress, OType.EMBEDDEDLIST);

        schema.save();
      }
    } finally {
      if (database != null) {
        log.trace(methodName.concat(" : Closing database."));
        database.close();
      }
    }
  }

  // Various query strings.
  private static final String doseResponseBySimulationAndDataIdQuery;
  private static final String fundamentalBySimulationIdQuery;
  private static final String individualBySimulationAndDataIdQuery;
  private static final String individualBySummaryAndIndividualDataIdQuery;
  private static final String progressAllBySimulationIdQuery;
  private static final String progressJobByJobIdQuery;
  private static final String summaryBySimulationAndDataIdQuery;

  static {
    final String and = " and ";
    final String eq = " = :";
    final String selectEverythingFrom = "select * from ";
    final String where = " where ";
    doseResponseBySimulationAndDataIdQuery = selectEverythingFrom.concat(classDoseResponse).concat(where)
                                             .concat(fieldNameSimulationId).concat(eq).concat(fieldNameSimulationId)
                                             .concat(and)
                                             .concat(fieldNameDoseResponseDataId).concat(eq).concat(fieldNameDoseResponseDataId);
    fundamentalBySimulationIdQuery = selectEverythingFrom.concat(classFundamental)
                                     .concat(where)
                                     .concat(fieldNameSimulationId).concat(eq).concat(fieldNameSimulationId);
    individualBySimulationAndDataIdQuery = selectEverythingFrom
                                           .concat(classIndividual)
                                           .concat(where)
                                           .concat(fieldNameSimulationId).concat(eq).concat(fieldNameSimulationId)
                                           .concat(and)
                                           .concat(fieldNameIndividualDataId).concat(eq).concat(fieldNameIndividualDataId);
    individualBySummaryAndIndividualDataIdQuery = selectEverythingFrom.concat(classIndividual).concat(where)
                                                 .concat(fieldNameSummaryDataId).concat(eq).concat(fieldNameSummaryDataId)
                                                 .concat(and)
                                                 .concat(fieldNameIndividualDataId).concat(eq).concat(fieldNameIndividualDataId);
    progressJobByJobIdQuery = selectEverythingFrom.concat(classProgressJob).concat(where)
                              .concat(fieldNameJobId).concat(eq).concat(fieldNameJobId);
    progressAllBySimulationIdQuery = selectEverythingFrom.concat(classProgressAll)
                                     .concat(where)
                                     .concat(fieldNameSimulationId).concat(eq).concat(fieldNameSimulationId);
    summaryBySimulationAndDataIdQuery = selectEverythingFrom.concat(classSummary)
                                        .concat(where)
                                        .concat(fieldNameSimulationId).concat(eq).concat(fieldNameSimulationId)
                                        .concat(and)
                                        .concat(fieldNameSummaryDataId).concat(eq).concat(fieldNameSummaryDataId);
  }

  // Create the progress text.
  private ODocument createProgress(final String timestamp, final InformationLevel level,
                                   final String text, final List<String> args) {
    return new ODocument().field(fieldNameTimestamp, timestamp).field(fieldNameLevel, level)
                          .field(fieldNameText, text).field(fieldNameArgs, args, OType.EMBEDDEDLIST);
  }

  // Create the provenance text with args.
  private ODocument createProvenance(final InformationLevel level, final String text,
                                     final List<String> args) {
    final List<String> useArgs = new ArrayList<String>();
    if (args != null) {
      useArgs.addAll(args);
    }
    return new ODocument().field(fieldNameLevel, level).field(fieldNameText, text)
                          .field(fieldNameArgs, useArgs, OType.EMBEDDEDLIST);
  }

  // Create progress collection.
  private List<ODocument> createProgressCollection(final String timestamp,
                                                   final InformationLevel level, final String text,
                                                   final List<String> args) {
    final List<ODocument> newProgress = new ArrayList<ODocument>();
    newProgress.add(createProgress(timestamp, level, text, args));
    return newProgress;
  }

  // Create provenance collection with args.
  private List<ODocument> createProvenanceCollection(final InformationLevel level,
                                                     final String text,
                                                     final List<String> args) {
    final List<ODocument> newProvenances = new ArrayList<ODocument>();
    newProvenances.add(createProvenance(level, text, args));
    return newProvenances;
  }

  // Indicate if there's something representing a surrogate primary key
  private boolean hasValidId(final long value) {
    return (value > 0);
  }

  // Retrieve dose-response provenance by dose-response *data* id.
  private List<ODocument> queryDoseResponseBySimulationAndDataId(final long simulationId,
                                                                 final String doseResponseDataId) {
    final String methodName = "queryDoseResponseByDataId()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    identifyingSB.append("[" + doseResponseDataId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    final Map<String, Object> params = new HashMap<String, Object>();
    params.put(fieldNameDoseResponseDataId, doseResponseDataId);
    params.put(fieldNameSimulationId, simulationId);
    final List<ODocument> found = new OSQLSynchQuery<ODocument>(doseResponseBySimulationAndDataIdQuery)
                                      .execute(params);
    log.trace(identifyingLogPrefix.concat("Found '" + found.size() + "' records."));
    return found;
  }

  // Retrieve fundamental progress by system simulation id.
  private List<ODocument> queryFundamentalBySimulationId(final long simulationId) {
    final String methodName = "queryFundamentalBySimulationId()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    final boolean hasSimulationId = hasValidId(simulationId);
    if (!hasSimulationId) {
      final String error = "Simulation id must be provided to retrieve fundamental class objects from OrientDb.";
      log.error(identifyingLogPrefix.concat(error));
      System.out.println(error);
      return new ArrayList<ODocument>();
    }

    final Map<String, Object> params = new HashMap<String, Object>();
    params.put(fieldNameSimulationId, simulationId);
    final List<ODocument> found = new OSQLSynchQuery<ODocument>(fundamentalBySimulationIdQuery)
                                      .execute(params);
    log.trace(identifyingLogPrefix.concat("Found '" + found.size() + "' records."));
    return found;
  }

  // Retrieve individual by individual *data* identifier.
  private List<ODocument> queryIndividualBySimulationAndDataId(final long simulationId,
                                                               final String individualDataId) {
    final String methodName = "queryIndividualBySimulationAndDataId()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    identifyingSB.append("[" + individualDataId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    queryFundamentalBySimulationId(simulationId);

    final Map<String, Object> params = new HashMap<String, Object>();
    params.put(fieldNameSimulationId, simulationId);
    params.put(fieldNameIndividualDataId, individualDataId);
    final List<ODocument> found = new OSQLSynchQuery<ODocument>(individualBySimulationAndDataIdQuery)
                                     .execute(params);
    log.trace(identifyingLogPrefix.concat("Found '" + found.size() + "' records."));
    return found;
  }

  // Retrieve individual by summary and individual *data* identifiers.
  private List<ODocument> queryIndividualBySummaryAndIndividualDataId(final String summaryDataId,
                                                                      final String individualDataId) {
    final String methodName = "queryIndividualBySummaryAndIndividualDataId()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + summaryDataId + "]");
    identifyingSB.append("[" + individualDataId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    final Map<String, Object> params = new HashMap<String, Object>();
    params.put(fieldNameSummaryDataId, summaryDataId);
    params.put(fieldNameIndividualDataId, individualDataId);
    final List<ODocument> found = new OSQLSynchQuery<ODocument>(individualBySummaryAndIndividualDataIdQuery)
                                      .execute(params);
    log.trace(identifyingLogPrefix.concat("Found '" + found.size() + "' records."));
    return found;
  }

  // Retrieve the simulation/all progress by system simulation id.
  private List<ODocument> queryProgressAllBySimulationId(final long simulationId) {
    final String methodName = "queryProgressAllBySimulationId()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    final Map<String, Object> params = new HashMap<String, Object>();
    params.put(fieldNameSimulationId,  simulationId);
    final List<ODocument> found = new OSQLSynchQuery<ODocument>(progressAllBySimulationIdQuery)
                                      .execute(params);
    log.trace(identifyingLogPrefix.concat("Found '" + found.size() + "' records."));
    return found;
  }

  // Retrieve summary progress by the summary *data* identifier.
  private List<ODocument> querySummaryBySimulationAndDataId(final long simulationId,
                                                            final String summaryDataId) {
    final String methodName = "querySummaryBySimulationAndDataId()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    identifyingSB.append("[" + summaryDataId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    if (StringUtils.isBlank(summaryDataId)) {
      final String error = "No summary data id was provided to retrieve summary class objects from OrientDb.";
      log.error(identifyingLogPrefix.concat(error));
      System.out.println(error);
      return new ArrayList<ODocument>();
    }

    final Map<String, Object> params = new HashMap<String, Object>();
    params.put(fieldNameSimulationId, simulationId);
    params.put(fieldNameSummaryDataId, summaryDataId);
    final List<ODocument> found = new OSQLSynchQuery<ODocument>(summaryBySimulationAndDataIdQuery).execute(params);
    log.trace(identifyingLogPrefix.concat("Found '" + found.size() + "' records."));
    return found;
  }

  // Put quotes around an ORID
  private String quoteORID(final ORID orid) {
    return "\"".concat(orid.toString().concat("\""));
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProgressDAO#updateProgressJob(long, java.util.Date, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List)
   */
  @Override
  public void updateProgressJob(final long jobId, final Date timestamp, final InformationLevel level,
                                final String text, final List<String> args)
                                throws IllegalArgumentException, IllegalStateException {
    final String methodName = "updateProgressJob()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + jobId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat("Invoked."));
    if (!hasValidId(jobId) || timestamp == null || level == null || StringUtils.isBlank(text)) {
      final List<String> messages = new ArrayList<String>();
      if (!hasValidId(jobId)) messages.add("jobId '" + jobId + "'");
      if (timestamp == null) messages.add("timestamp '" + timestamp + "'");
      if (level == null) messages.add("level '" + level + "'");
      if (StringUtils.isBlank(text)) messages.add("text '" + text + "'");
      throwArgError(methodName, identifying,
                    "Invalid value in: ".concat(StringUtils.join(messages, ": ")));
    }

    final Map<String, Object> params = new HashMap<String, Object>();
    params.put(fieldNameJobId, jobId);
    final List<ODocument> found = new OSQLSynchQuery<ODocument>(progressJobByJobIdQuery).execute(params);
    final int foundCount = found.size();
    log.trace(identifyingLogPrefix.concat("Found '" + foundCount + "' records."));

    if (foundCount == 0) {
      throwStateError(methodName, identifying,
                      "No job progress data structure exists for job id '" + jobId + "'.");
    }
    if (foundCount > 1) {
      throwStateError(methodName, identifying,
                      "More than one job progress data structure exists for job id '" + jobId + "'.");
    }

    final ODocument existingJobProgress = found.iterator().next();
    final String timestampStr = Long.valueOf(timestamp.getTime()).toString();
    // Append the latest progress text to existing/nested texts. 
    appendProgress(existingJobProgress, timestampStr, level, text, args);
    log.trace(identifyingLogPrefix.concat("Job progress JSON '" + existingJobProgress.toJSON() + "'."));
  }

  // Add a progress to existing.
  private void appendProgress(final ODocument existingDocument, final String timestamp,
                              final InformationLevel level, final String text,
                              final List<String> args) {
    final List<ODocument> progress = existingDocument.field(fieldNameProgress);
    progress.add(createProgress(timestamp, level, text, args));
    existingDocument.save();
  }

  // Add a provenance to existing.
  private void appendProvenance(final ODocument existingDocument, final InformationLevel level,
                                final String text, final List<String> args) {
    final List<ODocument> provenances = existingDocument.field(fieldNameProvenance);
    provenances.add(createProvenance(level, text, args));
    existingDocument.save();
  }

  private String prefConcat(final String methodName) {
    return "~odb-".concat(methodName);
  }

  
  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.InformationDAO#purge(long)
   */
  @Override
  public void purge(final long simulationId) {
    final String methodName = "purge()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat("Invoked."));

    purge("progress", simulationId, queryProgressAllBySimulationId(simulationId));
    purge("provenance", simulationId, queryFundamentalBySimulationId(simulationId));
  }

  // Common code between purging different types of information.
  private void purge(final String informationType, final long simulationId,
                     final List<ODocument> information) {
    final String methodName = "purge(x3)";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    if (!information.isEmpty()) {
      final int informationCount = information.size();
      if (informationCount == 1) {
        final ODocument progressAll = information.get(0);
        for (final ODocument document : traversingQuery(progressAll.getIdentity())) {
          log.trace(identifyingLogPrefix.concat("Removing " + informationType + " '" + document.toJSON() + "'."));
          document.delete();
        }
      } else {
        throwStateError(methodName, identifying,
                        "There were '" + informationCount + "' " + informationType + " data documents for simulation id '" + simulationId + "'.");
      }
    }
  }

  // Build up the JSON representation of the traversed document data.
  private String recursiveJSONBuilder(String currentJSON, final Map<String, String> nestedJSON) {
    final Matcher matcher = p.matcher(currentJSON);
    // Are there any ORID identifier-like sequences, e.g. "#23:1" indicating linked documents?
    boolean ignoringParentORID = true;
    while (matcher.find()) {
      // Yes there was...
      // But we're not interested in the first one because it'll be the current @rid identifier!
      if (ignoringParentORID) {
        ignoringParentORID = false;
        continue;
      }

      // Fish out the matched "#23:1" (note the quotes)
      final String matchedORID = matcher.group();
      if (nestedJSON.containsKey(matchedORID)) {
        // Find the json string of the orient document to replace the "#23:1"
        final String jsonToBeInjected = nestedJSON.get(matchedORID);
        // First need to check the JSON toBeInjected itself for identifier-like sequences!
        currentJSON = currentJSON.replace(matchedORID, recursiveJSONBuilder(jsonToBeInjected, nestedJSON));
      }
    }

    return currentJSON;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProgressDAO#retrieveProgress(long, uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager.INFORMATION_FORMAT)
   */
  @Override
  public InformationVO retrieveProgress(final long simulationId, final INFORMATION_FORMAT format)
                                        throws IllegalArgumentException, IllegalStateException {
    final String methodName = "retrieveProgress()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat("Invoked."));

    final List<ODocument> progressAlls = queryProgressAllBySimulationId(simulationId);
    final int progressAllsCount = progressAlls.size();

    if (progressAllsCount == 0) {
      log.trace(identifyingLogPrefix.concat("No all progress structures found."));
      return new InformationVO(simulationId, InformationVO.EMPTY_ASSOC_ARRAY);
    }
    if (progressAllsCount > 1) {
      throwStateError(methodName, identifying, "More than one progress structure exists.");
    }

    final ODocument existingAll = progressAlls.iterator().next();
    final ORID existingAllORID = existingAll.getIdentity();

    // Store a collection of document JSONs keyed on a quote ORID value.
    final Map<String, String> connectedDocuments = new HashMap<String, String>();
    String rootJSON = null;
    // Traverse the retrieved documents
    for (final ODocument currentDocument : traversingQuery(existingAllORID)) {
      final ORID currentDocumentORID = currentDocument.getIdentity();
      if (currentDocumentORID != null) {
        final String currentJSON = currentDocument.toJSON();
        if (currentDocumentORID.equals(existingAllORID)) {
          // Set the current document of the simulation/all progress to be the root JSON.
          rootJSON = currentJSON;
        } else {
          // Document ORID different to the simulation/all progress, i.e. job progress 
          final String currentORIDQuoted = quoteORID(currentDocumentORID);
          connectedDocuments.put(currentORIDQuoted, currentJSON);
        }
      }
    }

    final StringBuffer output = new StringBuffer();
    if (rootJSON != null) {
      if (connectedDocuments.isEmpty()) {
        // If there were no job progress documents.
        output.append(rootJSON);
      } else {
        // If job progress documents encountered.
        final String allJSON = recursiveJSONBuilder(rootJSON, connectedDocuments);
        output.append(allJSON);
      }
    }

    return new InformationVO(simulationId, output.toString());
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProvenanceDAO#retrieveProvenance(long, uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager.INFORMATION_FORMAT)
   */
  @Override
  public InformationVO retrieveProvenance(final long simulationId, final INFORMATION_FORMAT format)
                                          throws IllegalArgumentException, IllegalStateException {
    final String methodName = "retrieveProgress()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat("Invoked."));

    // Retrieve the fundamental record which this individual belongs to.
    final List<ODocument> existingFundamentals = queryFundamentalBySimulationId(simulationId);
    assert (existingFundamentals.size() == 1) : "Should only be able to retrieve one fundamental for simulation id '" + simulationId + "'. Retrieved '" + existingFundamentals.size() + "'!";
    final ODocument existingFundamental = existingFundamentals.get(0);
    final ORID existingFundamentalORID = existingFundamental.getIdentity();

    // Store a collection of document JSONs keyed on a quote ORID value.
    final Map<String, String> connectedDocuments = new HashMap<String, String>();
    String rootJSON = null; 
    // Traverse the retrieved documents.
    for (final ODocument document : traversingQuery(existingFundamentalORID)) {
      final ORID orid = document.getIdentity();
      if (orid != null) {
        log.trace(identifyingLogPrefix.concat("Retrieving '" + orid.toString() + "' json of '" + document.toJSON() + "'."));
        final String json = document.toJSON();
        if (orid.equals(existingFundamentalORID)) {
          // Set the current document of the fundamental provenance to be the root JSON.
          rootJSON = json;
        } else {
          // Document ORID different to the fundamental, i.e. child provenance.
          final String currentORIDQuoted = quoteORID(orid);
          connectedDocuments.put(currentORIDQuoted, json);
        }
      }
    }

    final StringBuffer output = new StringBuffer();
    if (rootJSON != null) {
      if (connectedDocuments.isEmpty()) {
        // If there were no provenance documents.
        output.append(rootJSON);
      } else {
        // If provenance documents encountered.
        final String allJSON = recursiveJSONBuilder(rootJSON, connectedDocuments);
        output.append(allJSON);
      }
    }

    return new InformationVO(simulationId, output.toString());
  }

  // Frequently used arg error-throwing.
  private void throwArgError(final String method, final String identifying,
                             final String errorMessage) throws IllegalArgumentException {
    log.error(prefConcat(method).concat(" : ").concat(identifying).concat(" : ").concat(errorMessage));
    throw new IllegalArgumentException(errorMessage);
  }

  // Frequently used state error-throwing.
  private void throwStateError(final String method, final String identifying,
                               final String errorMessage) throws IllegalStateException {
    log.error(prefConcat(method).concat(" : ").concat(identifying).concat(" : ").concat(errorMessage));
    throw new IllegalStateException(errorMessage);
  }

  /*
   * Perform a document traversal based on the ORID value.
   * 
   * Search connected documents to the simulation/all progress document.
   * For example, for a simulation with jobs there would be a parent simulation/all progress
   *   document, as well as a job progress document. The simulation/all progress document would
   *   contain text such as ... "@rid":"#13:0" ... "job":["#14:0"] ... indicating a placeholder
   *   to the ORID identifier array of job documents, one of which would contain  ... "@rid":"#14:0".
   * This returns the parent as well as linked documents.
   * http://www.orientechnologies.com/docs/last/SQL-Traverse.html
   */
  private List<ODocument> traversingQuery(final ORID orid) {
    final String command = "traverse * from " + orid;
    final ODatabaseRecordTx database = (ODatabaseRecordTx) ODatabaseRecordThreadLocal.INSTANCE.get();
    return database.command(new OSQLSynchQuery<ODocument>(command)).execute();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProvenanceDAO#updateDoseResponse(long, java.lang.String, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List)
   */
  @Override
  public void updateDoseResponse(final long simulationId, final String doseResponseDataId,
                                 final InformationLevel level, final String text,
                                 final List<String> args) throws IllegalArgumentException,
                                                                 IllegalStateException {
    final String methodName = "updateDoseResponse(x5)";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    identifyingSB.append("[" + doseResponseDataId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat("Invoked."));
    final List<ODocument> results = queryDoseResponseBySimulationAndDataId(simulationId,
                                                                           doseResponseDataId);
    final int resultCount = results.size();
    if (resultCount == 1) {
      final ODocument existingDoseResponse = results.get(0);
      appendProvenance(existingDoseResponse, level, text, args);
      existingDoseResponse.save();
    } else {
      throwStateError(methodName, identifying,
                      "DoseResponseData with id#" + doseResponseDataId + " has '" + resultCount + "' records for class '" + classDoseResponse + "'!");
    }
  }


  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProvenanceDAO#updateDoseResponse(long, java.lang.String, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List, java.lang.String, java.lang.String)
   */
  @Override
  public void updateDoseResponse(final long simulationId, final String doseResponseDataId,
                                 final InformationLevel level, final String text,
                                 final List<String> args, final String dose_in_uM,
                                 final String response) throws IllegalArgumentException,
                                                               IllegalStateException {
    final String methodName = "updateDoseResponse(x7)";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    identifyingSB.append("[" + doseResponseDataId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat("Invoked."));
    if (StringUtils.isBlank(dose_in_uM) || StringUtils.isBlank(response)) {
      final List<String> messages = new ArrayList<String>();
      if (StringUtils.isBlank(dose_in_uM)) messages.add("Dose in uM '" + dose_in_uM + "'");
      if (StringUtils.isBlank(response)) messages.add("Response '" + response + "'");
      throwArgError(methodName, identifying,
                    "Invalid value in: ".concat(StringUtils.join(messages, ": ")));
    }

    final List<ODocument> results = queryDoseResponseBySimulationAndDataId(simulationId,
                                                                           doseResponseDataId);
    final int resultCount = results.size();
    if (resultCount == 1) {
      final ODocument existingDoseResponse = results.get(0);
      existingDoseResponse.field(fieldNameDose_in_uM, dose_in_uM);
      existingDoseResponse.field(fieldNameDoseResponse, response);
      appendProvenance(existingDoseResponse, level, text, args);
      existingDoseResponse.save();
    } else {
      throwStateError(methodName, identifying,
                      "DoseResponseData with id '" + doseResponseDataId + "' has '" + resultCount + "' records for class '" + classDoseResponse + "'!");
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProvenanceDAO#updateIndividual(long, java.lang.String, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List)
   */
  @Override
  public void updateIndividual(final long simulationId, final String individualDataId,
                               final InformationLevel level, final String text,
                               final List<String> args) {
    final String methodName = "updateIndividual(x5)";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    identifyingSB.append("[" + individualDataId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat("Invoked."));
    final List<ODocument> results = queryIndividualBySimulationAndDataId(simulationId,
                                                                         individualDataId);
    final int resultCount = results.size();
    if (resultCount == 1) {
      final ODocument existingIndividual = results.get(0);
      appendProvenance(existingIndividual, level, text, args);
      existingIndividual.save();
    } else {
      throwStateError(methodName, identifying,
                      "IndividualData with id#" + individualDataId + " has '" + resultCount + "' records for class + '" + classIndividual + "'!");
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProvenanceDAO#updateIndividual(long, java.lang.String, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List, java.math.BigDecimal, java.math.BigDecimal)
   */
  @Override
  public void updateIndividual(final long simulationId, final String individualDataId,
                               final InformationLevel level, final String text,
                               final List<String> args, final BigDecimal pIC50,
                               final BigDecimal workflowHill) {
    final String methodName = "updateIndividual(x7[A])";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    identifyingSB.append("[" + individualDataId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat(" : Invoked."));
    if (pIC50 == null || workflowHill == null) {
      final List<String> messages = new ArrayList<String>();
      if (pIC50 == null) messages.add("pIC50 value of '" + pIC50 + "'.");
      if (workflowHill == null) messages.add("Workflow Hill '" + workflowHill + "'.");
      throwArgError(methodName, identifying,
                    "Invalid value in: ".concat(StringUtils.join(messages, ": ")));
    }
    final List<ODocument> results = queryIndividualBySimulationAndDataId(simulationId,
                                                                         individualDataId);
    final int resultCount = results.size();
    if (resultCount == 1) {
      final ODocument existingIndividual = results.get(0);
      existingIndividual.field(fieldNameIndividual_pIC50, pIC50.toPlainString());
      existingIndividual.field(fieldNameIndividualWorkflowHill, workflowHill);
      appendProvenance(existingIndividual, level, text, args);
      existingIndividual.save();
    } else {
      throwStateError(methodName, identifying,
                      "IndividualData with id#" + individualDataId + " has '" + resultCount + "' records for class + '" + classIndividual + "'!");
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProvenanceDAO#updateIndividual(long, java.lang.String, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List, uk.ac.ox.cs.epsrc.business_manager.value.datainterface.DoseResponseData)
   */
  @Override
  public void updateIndividual(final long simulationId, final String individualDataId,
                               final InformationLevel level, final String text,
                               final List<String> args, final DoseResponseData doseResponseData) {
    final String methodName = "updateIndividual(x6)";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    identifyingSB.append("[" + individualDataId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(prefConcat(identifyingLogPrefix).concat(" : Invoked."));
    if (doseResponseData == null) {
      throwArgError(methodName, identifying,
                    "Invalid value in doseResponseData '" + doseResponseData + "'.");
    }
    final List<ODocument> results = queryIndividualBySimulationAndDataId(simulationId,
                                                                         individualDataId);
    final int resultCount = results.size();
    if (resultCount == 1) {
      final ODocument existingIndividual = results.get(0);
      final Map<String, String> doseResponses = new HashMap<String, String>();
      for (final DoseResponsePairVO doseResponsePair : doseResponseData.doseResponsePoints()) {
        final BigDecimal dose_in_uM = doseResponsePair.getDose();
        final BigDecimal doseResponse = doseResponsePair.getResponse();
        doseResponses.put(dose_in_uM.toPlainString(), doseResponse.toPlainString());
      }
      appendProvenance(existingIndividual, level, text, args);
      // TODO : Transfer "every" to var.
      existingIndividual.field(DOSE_RESPONSE_PREFIX.concat("every"), doseResponses);
      existingIndividual.save();
    } else {
      throwStateError(methodName, identifying,
                      "IndividualData with id#" + individualDataId + " has '" + resultCount + "' records for class + '" + classIndividual + "'!");
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProvenanceDAO#updateIndividual(long, java.lang.String, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List, uk.ac.ox.cs.epsrc.business_manager.value.datainterface.DoseResponseData, java.lang.String)
   */
  @Override
  public void updateIndividual(final long simulationId, final String individualDataId,
                               final InformationLevel level, final String text,
                               final List<String> args, final DoseResponseData doseResponseData,
                               final String doseResponseFittingStrategyIdentifier) {
    final String methodName = "updateIndividual(x7[B])";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    identifyingSB.append("[" + individualDataId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(prefConcat(identifyingLogPrefix).concat(" : Invoked."));
    if (doseResponseData == null || StringUtils.isBlank(doseResponseFittingStrategyIdentifier)) {
      final List<String> messages = new ArrayList<String>();
      if (doseResponseData == null) messages.add("Dose-Response data value of '" + doseResponseData + "'.");
      if (StringUtils.isBlank(doseResponseFittingStrategyIdentifier)) messages.add("Fitting Strategy of '" + doseResponseFittingStrategyIdentifier + "'.");
      throwArgError(methodName, identifying,
                    "Invalid value in: ".concat(StringUtils.join(messages, ": ")));
    }

    final List<ODocument> results = queryIndividualBySimulationAndDataId(simulationId,
                                                                         individualDataId);
    final int resultCount = results.size();
    if (resultCount == 1) {
      final ODocument existingIndividual = results.get(0);
      final Map<String, String> doseResponses = new HashMap<String, String>();
      for (final DoseResponsePairVO doseResponsePair : doseResponseData.doseResponsePoints()) {
        final BigDecimal dose_in_uM = doseResponsePair.getDose();
        final BigDecimal doseResponse = doseResponsePair.getResponse();
        doseResponses.put(dose_in_uM.toPlainString(), doseResponse.toPlainString());
      }
      final String fieldName = doseResponseFittingStrategyIdentifier;
      appendProvenance(existingIndividual, level, text, args);
      existingIndividual.field(fieldName, doseResponses);
      existingIndividual.save();
    } else {
      throwStateError(methodName, identifying,
                      "IndividualData with id#" + individualDataId + " has '" + resultCount + "' records for class + '" + classIndividual + "'!");
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProvenanceDAO#updateSummary(long, java.lang.String, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List)
   */
  @Override
  public void updateSummary(final long simulationId, final String summaryDataId,
                            final InformationLevel level, final String text,
                            final List<String> args) {
    final String methodName = "updateSummary(x5)";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    identifyingSB.append("[" + summaryDataId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat(" : Invoked."));

    final List<ODocument> results = querySummaryBySimulationAndDataId(simulationId, summaryDataId);
    final int resultCount = results.size();
    if (resultCount == 1) {
      final ODocument existingSummary = results.get(0);
      appendProvenance(existingSummary, level, text, args);
      existingSummary.save();
    } else {
      throwStateError(methodName, identifying,
                      "Unexpected count of '" + resultCount  + "' records for class '" + classSummary + "'.");
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProvenanceDAO#updateSummary(long, java.lang.String, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List, java.util.List, java.math.BigDecimal)
   */
  @Override
  public void updateSummary(final long simulationId, final String summaryDataId,
                            final InformationLevel level, final String text,
                            final List<String> args, final List<BigDecimal> pIC50s,
                            final BigDecimal hillCoefficient) {
    final String methodName = "updateSummary(x7)";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    identifyingSB.append("[" + summaryDataId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat(" : Invoked."));
    if (pIC50s == null || hillCoefficient == null) {
      final List<String> messages = new ArrayList<String>();
      if (pIC50s == null) messages.add("pIC50s value of '" + pIC50s + "'.");
      if (hillCoefficient == null) messages.add("Hill Coefficient '" + hillCoefficient + "'.");
      throwArgError(methodName, identifying,
                    "Invalid value in: ".concat(StringUtils.join(messages, ": ")));
    }

    final List<ODocument> results = querySummaryBySimulationAndDataId(simulationId, summaryDataId);
    final int resultCount = results.size();
    if (resultCount == 1) {
      final ODocument existingSummary = results.get(0);
      existingSummary.field(fieldNameSummary_pIC50s, pIC50s);
      existingSummary.field(fieldNameSummaryHillCoeff, hillCoefficient);
      appendProvenance(existingSummary, level, text, args);
      existingSummary.save();
    } else {
      throwStateError(methodName, identifying,
                      "Unexpected count of '" + resultCount + "' records for class + '" + classSummary + "'!");
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProvenanceDAO#writeDoseResponse(long, java.lang.String, java.lang.String, java.util.Map, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List)
   */
  @Override
  public void writeDoseResponse(final long simulationId, final String individualDataId,
                                final String doseResponseDataId, final Map<String, Object> rawData,
                                final InformationLevel level, final String text,
                                final List<String> args) {
    final String methodName = "writeDoseResponse()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    identifyingSB.append("[" + individualDataId + "]");
    identifyingSB.append("[" + doseResponseDataId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat(" : Invoked."));
    if (StringUtils.isBlank(individualDataId) || StringUtils.isBlank(doseResponseDataId) ||
        rawData == null || rawData.isEmpty() || level == null || text == null) {
      final List<String> messages = new ArrayList<String>();
      if (StringUtils.isBlank(individualDataId)) messages.add("Individual data id '" + individualDataId + "'.");
      if (StringUtils.isBlank(doseResponseDataId)) messages.add("Dose-Response data id '" + doseResponseDataId + "'.");
      if (rawData == null || rawData.isEmpty()) messages.add("Raw data '" + rawData + "'.");
      if (level == null) messages.add("Information level is '" + level + "'.");
      if (text == null) messages.add("Text is '" + text + "'.");
      throwArgError(methodName, identifying,
                    "Invalid value in: ".concat(StringUtils.join(messages, ": ")));
    }

    // Retrieve the individual record which this dose-response belongs to.
    final List<ODocument> existingIndividuals = queryIndividualBySimulationAndDataId(simulationId,
                                                                                     individualDataId);
    final int individualCount = existingIndividuals.size();
    if (individualCount != 1) {
      throwStateError(methodName, identifying,
                      "Individual data with id '" + individualDataId + "' has '" + individualCount + "' records for class '" + classIndividual + "'!");
    }

    final ODocument existingIndividual = existingIndividuals.get(0);

    // Read and append (if necessary) to dose-response.
    final List<ODocument> results = queryDoseResponseBySimulationAndDataId(simulationId,
                                                                           doseResponseDataId);
    final int doseResponseCount = results.size();
    switch (doseResponseCount) {
      case 0 :
        log.trace(identifyingLogPrefix.concat(" : Empty results."));
        final ODocument newDoseResponse = new ODocument(classDoseResponse);
        newDoseResponse.field(fieldNameSimulationId, simulationId);
        newDoseResponse.field(fieldNameDoseResponseDataId, doseResponseDataId);
        newDoseResponse.field(fieldNameRawData, new JSONObject(rawData));
        newDoseResponse.field(fieldNameProvenance, createProvenanceCollection(level, text, args));
        newDoseResponse.save();

        log.trace(identifyingLogPrefix.concat(" : Updating individual with link to new dose-response record."));
        final Set<ORID> existingDoseResponseClassIds = existingIndividual.field(fieldNameDoseResponseData);
        if (existingDoseResponseClassIds == null) {
          log.trace(identifyingLogPrefix.concat("Creating first individual record link."));
          final Set<ORID> newDoseResponseClassIds = new HashSet<ORID>();
          newDoseResponseClassIds.add(newDoseResponse.getIdentity());
          existingIndividual.field(fieldNameDoseResponseData, newDoseResponseClassIds);
        } else {
          log.trace(identifyingLogPrefix.concat("Appending individual to existing links."));
          existingDoseResponseClassIds.add(newDoseResponse.getIdentity());
        }
        existingIndividual.save();

        log.trace(identifyingLogPrefix.concat("Individual JSON '" + existingIndividual.toJSON() + "'."));
        break;
      default :
        final String error = "Unexpected count of '" + doseResponseCount  + "' records for class '" + classDoseResponse + "' (IndividualId [" + individualDataId + "], DoseResponseId [" + doseResponseDataId + "]).";
        log.error(identifyingLogPrefix.concat(error));
        System.out.println(error);
        break;
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProvenanceDAO#writeFundamental(java.lang.String, long, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List)
   */
  @Override
  public void writeFundamental(final String compoundIdentifier, final long simulationId,
                               final InformationLevel level, final String text,
                               final List<String> args) throws IllegalArgumentException,
                                                               IllegalStateException {
    final String methodName = "writeFundamental()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + compoundIdentifier + "]");
    identifyingSB.append("[" + simulationId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat(" : Invoked."));
    if (!hasValidId(simulationId) || level == null || text == null) {
      final List<String> messages = new ArrayList<String>();
      // No compound identifier is valid if we're going to append data
      if (!hasValidId(simulationId)) messages.add("Simulation id '" + simulationId + "'.");
      if (level == null) messages.add("Information level is '" + level + "'.");
      if (text == null) messages.add("Text is '" + text + "'.");
      throwArgError(methodName, identifying,
                    "Invalid value in: ".concat(StringUtils.join(messages, ": ")));
    }

    final List<ODocument> results = queryFundamentalBySimulationId(simulationId);
    final int resultCount = results.size();
    switch (resultCount) {
      case 0 :
        log.trace(identifyingLogPrefix.concat(" : Empty results."));
        if (StringUtils.isBlank(compoundIdentifier)) {
          throwArgError(methodName, identifying,
                        "Attempt to create a new '" + classFundamental + "' document denied as we need both compound identifier and simulation id.");
        } else {
          // We need both properties to create a new record.
          final ODocument newFundamental = new ODocument(classFundamental);
          newFundamental.field(fieldNameCompoundIdentifier, compoundIdentifier);
          newFundamental.field(fieldNameSimulationId, simulationId);
          newFundamental.field(fieldNameProvenance, createProvenanceCollection(level, text,
                                                                                       args));
          newFundamental.save();
        }
        break;
      case 1 :
        appendProvenance(results.get(0), level, text, args);
        break;
      default :
        throwStateError(methodName, identifying,
                        "Unexpected count of '" + resultCount  + "' records for class '" + classFundamental + "'.");
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProvenanceDAO#writeIndividual(long, java.lang.String, java.lang.String, java.lang.String, java.math.BigDecimal, java.util.Map, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List)
   */
  @Override
  public void writeIndividual(final long simulationId, final String individualDataId,
                              final String assayName, final String ionChannelName,
                              final BigDecimal rawHill, final Map<String, Object> rawData,
                              final InformationLevel level, final String text,
                              final List<String> args) throws IllegalArgumentException,
                                                              IllegalStateException {
    final String methodName = "writeIndividual(x9)";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append(LogUtil.retrieveBasePrefix("", assayName, ionChannelName));
    identifyingSB.append("[" + simulationId + "]");
    identifyingSB.append("[" + individualDataId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat(" : Invoked."));
    if (!hasValidId(simulationId) || StringUtils.isBlank(individualDataId) ||
        StringUtils.isBlank(assayName) || StringUtils.isBlank(ionChannelName) ||
        rawData == null || rawData.isEmpty() || level == null || text == null) {
      final List<String> messages = new ArrayList<String>();
      if (!hasValidId(simulationId)) messages.add("Simulation id '" + simulationId + "'.");
      if (StringUtils.isBlank(individualDataId)) messages.add("Individual data id '" + individualDataId + "'.");
      if (StringUtils.isBlank(assayName)) messages.add("Assay name '" + assayName + "'.");
      if (StringUtils.isBlank(ionChannelName)) messages.add("Ion Channel name '" + ionChannelName + "'.");
      if (rawData == null || rawData.isEmpty()) messages.add("Raw data '" + rawData + "'.");
      if (level == null) messages.add("Information level is '" + level + "'.");
      if (text == null) messages.add("Text is '" + text + "'.");
      throwArgError(methodName, identifying,
                    "Invalid value in: ".concat(StringUtils.join(messages, ": ")));
    }

    // Retrieve the fundamental record which this individual will belong to.
    final List<ODocument> existingFundamentals = queryFundamentalBySimulationId(simulationId);
    final int fundamentalsCount = existingFundamentals.size();
    if (fundamentalsCount != 1) {
      throwStateError(methodName, identifying,
                      "Expecting a single fundamental provenance structure for simulation id '" + simulationId + "'.");
    }

    final ODocument existingFundamental = existingFundamentals.get(0);
    // Read and append (if necessary) to individual.
    final List<ODocument> results = queryIndividualBySimulationAndDataId(simulationId,
                                                                         individualDataId);
    final int individualCount = results.size();
    if (individualCount == 0) {
      final ODocument newIndividual = new ODocument(classIndividual);
      newIndividual.field(fieldNameIndividualDataId, individualDataId);
      newIndividual.field(fieldNameSimulationId, simulationId);
      newIndividual.field(fieldNameAssayName, assayName);
      newIndividual.field(fieldNameIonChannelName, ionChannelName);
      newIndividual.field(fieldNameIndividualRawHill, rawHill);
      newIndividual.field(fieldNameRawData, new JSONObject(rawData));
      newIndividual.field(fieldNameProvenance, createProvenanceCollection(level, text, args));
      newIndividual.save();
      final ORID newIndividualId = newIndividual.getIdentity();

      log.trace(identifyingLogPrefix.concat(" : Updating fundamental with link to new individual record."));
      final Set<ORID> existingIndividualClassIds = existingFundamental.field(fieldNameIndividualData);
      if (existingIndividualClassIds == null) {
        log.trace(identifyingLogPrefix.concat(" : Creating first individual record link."));
        final Set<ORID> newIndividualClassIds = new HashSet<ORID>();
        newIndividualClassIds.add(newIndividualId);
        existingFundamental.field(fieldNameIndividualData, newIndividualClassIds);
      } else {
        existingIndividualClassIds.add(newIndividualId);
      }
      existingFundamental.save();
    } else {
      throwStateError(methodName, identifying,
                      "Unexpected count of '" + individualCount  + "' records for class '" + classIndividual + "'.");
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProvenanceDAO#writeIndividual(long, java.lang.String, java.lang.String, java.math.BigDecimal, java.util.Map, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List)
   */
  @Override
  public void writeIndividual(final long simulationId, final String summaryDataId,
                              final String individualDataId, final BigDecimal rawHill,
                              final Map<String, Object> rawData, final InformationLevel level,
                              final String text, final List<String> args)
                              throws IllegalArgumentException, IllegalStateException {
    final String methodName = "writeIndividual(x8)";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    identifyingSB.append("[" + summaryDataId + "]");
    identifyingSB.append("[" + individualDataId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat(" : Invoked."));
    if (StringUtils.isBlank(summaryDataId) || StringUtils.isBlank(individualDataId) || rawData == null ||
        rawData.isEmpty() || level == null || text == null) {
      final List<String> messages = new ArrayList<String>();
      if (StringUtils.isBlank(summaryDataId)) messages.add("Summary data id '" + summaryDataId + "'.");
      if (StringUtils.isBlank(individualDataId)) messages.add("Individual data id '" + individualDataId + "'.");
      if (rawData == null || rawData.isEmpty()) messages.add("Raw data '" + rawData + "'.");
      if (level == null) messages.add("Information level is '" + level + "'.");
      if (text == null) messages.add("Text is '" + text + "'.");
      throwArgError(methodName, identifying,
                    "Invalid value in: ".concat(StringUtils.join(messages, ": ")));
    }

    // Retrieve the summary record which this individual belongs to.
    final List<ODocument> existingSummaries = querySummaryBySimulationAndDataId(simulationId,
                                                                                summaryDataId);
    final int summariesCount = existingSummaries.size();
    if (summariesCount != 1) {
      throwStateError(methodName, identifying,
                      "Expecting a single summary provenance structure for summary data id '" + summaryDataId + "'.");
    }

    final ODocument existingSummary = existingSummaries.get(0);
    // Read and append (if necessary) to summary.
    final List<ODocument> results = queryIndividualBySummaryAndIndividualDataId(summaryDataId,
                                                                                individualDataId);
    final int resultCount = results.size();
    switch (resultCount) {
      case 0 :
        log.trace(identifyingLogPrefix.concat("Empty results."));
        final ODocument newIndividual = new ODocument(classIndividual);
        newIndividual.field(fieldNameSimulationId, simulationId);
        newIndividual.field(fieldNameIndividualDataId, individualDataId);
        newIndividual.field(fieldNameIndividualRawHill, rawHill);
        newIndividual.field(fieldNameRawData, new JSONObject(rawData));
        newIndividual.field(fieldNameProvenance, createProvenanceCollection(level, text, args));
        newIndividual.save();
        final ORID newIndividualId = newIndividual.getIdentity();

        log.trace(identifyingLogPrefix.concat("Updating summary with link to new individual record."));
        final Set<ORID> existingIndividualClassIds = existingSummary.field(fieldNameIndividualData);
        if (existingIndividualClassIds == null) {
          log.trace(identifyingLogPrefix.concat("Creating first individual record link."));
          final Set<ORID> newIndividualClassIds = new HashSet<ORID>();
          newIndividualClassIds.add(newIndividualId);
          existingSummary.field(fieldNameIndividualData, newIndividualClassIds);
        } else {
          log.trace(identifyingLogPrefix.concat("Appending individual to existing links."));
          existingIndividualClassIds.add(newIndividualId);
        }
        existingSummary.save();

        log.trace(identifyingLogPrefix.concat("Summary JSON '" + existingSummary.toJSON() + "'."));
        break;
      case 1 :
        log.trace(identifyingLogPrefix.concat("One result found."));
        appendProvenance(results.get(0), level, text, null);
        log.trace(identifyingLogPrefix.concat("Individual(1) JSON '" + results.get(0).toJSON() + "'."));
        break;
      default :
        final String error = "Unexpected count of '" + resultCount  + "' records for class '" + classIndividual + "'.";
        log.error(identifyingLogPrefix.concat(error));
        System.out.println(error);
        break;
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProgressDAO#updateProgressAll(long, java.util.Date, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List)
   */
  @Override
  public void updateProgressAll(final long simulationId, final Date timestamp,
                                final InformationLevel level, final String text,
                                final List<String> args) throws IllegalArgumentException,
                                                                IllegalStateException,
                                                                UnsupportedOperationException {
    final String methodName = "updateProgressAll()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat("Invoked."));
    if (!hasValidId(simulationId) || timestamp == null || level == null ||
        StringUtils.isBlank(text)) {
      final List<String> messages = new ArrayList<String>();
      if (!hasValidId(simulationId)) messages.add("simulationId '" + simulationId + "'");
      if (timestamp == null) messages.add("timestamp '" + timestamp + "'");
      if (level == null) messages.add("level '" + level + "'");
      if (StringUtils.isBlank(text)) messages.add("text '" + text + "'");
      throwArgError(methodName, identifying,
                    "Invalid value in: ".concat(StringUtils.join(messages, ": ")));
    }

    final List<ODocument> results = queryProgressAllBySimulationId(simulationId);
    final int resultCount = results.size();
    final String timestampStr = Long.valueOf(timestamp.getTime()).toString();

    if (resultCount == 0) {
      throwStateError(methodName, identifying, "No simulation/all progress structure found.");
    }
    if (resultCount > 1) {
      throwStateError(methodName, identifying,
                      "More than one simulation/all progress structure found.");
    }
    appendProgress(results.iterator().next(), timestampStr, level, text, args);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProgressDAO#createProgressAll(java.lang.String, long, java.util.Date, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List)
   */
  @Override
  public void createProgressAll(final String compoundIdentifier, final long simulationId,
                                final Date timestamp, final InformationLevel level, final String text,
                                final List<String> args) throws IllegalArgumentException,
                                                                IllegalStateException {
    final String methodName = "createProgressAll()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + compoundIdentifier + "]");
    identifyingSB.append("[" + simulationId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat("Invoked."));
    if (StringUtils.isBlank(compoundIdentifier) || !hasValidId(simulationId) || timestamp == null ||
        level == null || StringUtils.isBlank(text)) {
      final List<String> messages = new ArrayList<String>();
      if (!hasValidId(simulationId)) messages.add("simulationId '" + simulationId + "'");
      if (timestamp == null) messages.add("timestamp '" + timestamp + "'");
      if (level == null) messages.add("level '" + level + "'");
      if (StringUtils.isBlank(text)) messages.add("text '" + text + "'");
      throwArgError(methodName, identifying,
                    "Invalid value in: ".concat(StringUtils.join(messages, ": ")));
    }

    final List<ODocument> results = queryProgressAllBySimulationId(simulationId);

    final int resultCount = results.size();
    final String timestampStr = Long.valueOf(timestamp.getTime()).toString();

    if (resultCount == 1) {
      throwStateError(methodName, identifying, "A simulation/all progress structure has been found.");
    }
    if (resultCount > 1) {
      throwStateError(methodName, identifying,
                      "More than one simulation/all progress structure found.");
    }
    log.trace(identifyingLogPrefix.concat("Empty results."));

    final ODocument newAll = new ODocument(classProgressAll);
    newAll.field(fieldNameCompoundIdentifier, compoundIdentifier);
    newAll.field(fieldNameSimulationId, simulationId);
    newAll.field(fieldNameProgress, createProgressCollection(timestampStr, level, text, args));
    newAll.save();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.ProgressDAO#createProgressJob(long, long, java.lang.String, short, float, java.util.Date, uk.ac.ox.cs.nc3rs.business_manager.api.value.InformationLevel, java.lang.String, java.util.List)
   */
  @Override
  public void createProgressJob(final long simulationId, final long jobId,
                                final String groupName, final short groupLevel,
                                final float pacingFrequency, final Date timestamp,
                                final InformationLevel level, final String text,
                                final List<String> args)
                                throws IllegalArgumentException, IllegalStateException {
    final String methodName = "createProgressJob()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    identifyingSB.append("[" + jobId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat("Invoked."));
    if (!hasValidId(simulationId) || !hasValidId(jobId) || StringUtils.isBlank(groupName) ||
        groupLevel < 0 || pacingFrequency < 0.0 || timestamp == null || level == null ||
            StringUtils.isBlank(text)) {
      final List<String> messages = new ArrayList<String>();
      if (!hasValidId(simulationId)) messages.add("simulationId '" + simulationId + "'");
      if (!hasValidId(jobId)) messages.add("jobId '" + jobId + "'");
      if (StringUtils.isBlank(groupName)) messages.add("groupName '" + groupName + "'");
      if (groupLevel < 0) messages.add("groupLevel '" + groupLevel + "'");
      if (pacingFrequency < 0.0) messages.add("pacingFrequency '" + pacingFrequency + "'");
      if (timestamp == null) messages.add("timestamp '" + timestamp + "'");
      if (level == null) messages.add("level '" + level + "'");
      if (StringUtils.isBlank(text)) messages.add("text '" + text + "'");
      throwArgError(methodName, identifying,
                    "Invalid value in: ".concat(StringUtils.join(messages, ": ")));
    }

    // Find simulation progress structure (should only be one!) to append new job progress to.
    final List<ODocument> results = queryProgressAllBySimulationId(simulationId);
    final int resultCount = results.size();

    if (resultCount == 0) {
      throwStateError(methodName, identifying,
                      "No simulation/all progress structure found.");
    }
    if (resultCount > 1) {
      throwStateError(methodName, identifying,
                      "More than one simulation/all progress structure found.");
    }

    // Create new job progress doc
    final ODocument newJob = new ODocument(classProgressJob);
    newJob.field(fieldNameJobId, jobId);
    newJob.field(fieldNameGroupName, groupName);
    newJob.field(fieldNameGroupLevel, groupLevel);
    newJob.field(fieldNamePacingFrequency, pacingFrequency);
    newJob.field(fieldNameProgress, createProgressCollection(Long.valueOf(timestamp.getTime()).toString(),
                                                             level, text, args));
    newJob.save();

    final ODocument existingAllProgress = results.get(0);
    final Set<ORID> existingJobClassIds = existingAllProgress.field(fieldNameJob);
    // create the link from the progress for the simulation to the new per-job progress.
    if (existingJobClassIds == null) {
      log.trace(identifyingLogPrefix.concat("Creating first job progress record link."));
      final Set<ORID> newJobClassIds = new HashSet<ORID>();
      newJobClassIds.add(newJob.getIdentity());
      existingAllProgress.field(fieldNameJob, newJobClassIds);
    } else {
      log.trace(identifyingLogPrefix.concat("Appending job to existing links."));
      existingJobClassIds.add(newJob.getIdentity());
    }
    existingAllProgress.save();

    log.trace(identifyingLogPrefix.concat("Job id#" + jobId + " JSON '" + newJob.toJSON() + "'."));
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.information.ProvenanceDAO#writeSummary(long, java.lang.String, java.lang.String, java.lang.String, java.util.Map, uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel, java.lang.String, java.util.List)
   */
  @Override
  public void writeSummary(final long simulationId, final String summaryDataId,
                           final String assayName, final String ionChannelName,
                           final Map<String, Object> rawData, final InformationLevel level,
                           final String text, final List<String> args) {
    final String methodName = "writeProgressJob()";
    final StringBuffer identifyingSB = new StringBuffer();
    identifyingSB.append("[" + simulationId + "]");
    identifyingSB.append("[" + summaryDataId + "]");
    final String identifying = identifyingSB.toString();
    final String identifyingLogPrefix = prefConcat(methodName).concat(" : ").concat(identifying).concat(" : ");

    log.trace(identifyingLogPrefix.concat(" : Invoked."));
    if (!hasValidId(simulationId) || StringUtils.isBlank(summaryDataId) ||
        StringUtils.isBlank(assayName) || StringUtils.isBlank(ionChannelName) ||
        rawData == null || rawData.isEmpty() || level == null || text == null) {
      final List<String> messages = new ArrayList<String>();
      if (!hasValidId(simulationId)) messages.add("Simulation id '" + simulationId + "'.");
      if (StringUtils.isBlank(summaryDataId)) messages.add("Summary data id '" + summaryDataId + "'.");
      if (StringUtils.isBlank(assayName)) messages.add("Assay name '" + assayName + "'.");
      if (StringUtils.isBlank(ionChannelName)) messages.add("Ion Channel name '" + ionChannelName + "'.");
      if (rawData == null || rawData.isEmpty()) messages.add("Raw data '" + rawData + "'.");
      if (level == null) messages.add("Information level is '" + level + "'.");
      if (text == null) messages.add("Text is '" + text + "'.");
      throwArgError(methodName, identifying,
                    "Invalid value in: ".concat(StringUtils.join(messages, ": ")));
    }

    // Retrieve the fundamental record which this summary belongs to.
    final List<ODocument> existingFundamentals = queryFundamentalBySimulationId(simulationId);
    final int fundamentalsCount = existingFundamentals.size();
    if (fundamentalsCount != 1) {
      throwStateError(methodName, identifying,
                      "Expecting 1 but retrieved '" + fundamentalsCount + "' provenance structure(s) for simulation id '" + simulationId + "'.");
    }

    final ODocument existingFundamental = existingFundamentals.get(0);
    // Read and append (if necessary) to summary.
    final List<ODocument> results = querySummaryBySimulationAndDataId(simulationId, summaryDataId);
    final int resultCount = results.size();
    if (resultCount == 0) {
      final ODocument newSummary = new ODocument(classSummary);
      newSummary.field(fieldNameSummaryDataId, summaryDataId);
      newSummary.field(fieldNameSimulationId, simulationId);
      newSummary.field(fieldNameAssayName, assayName);
      newSummary.field(fieldNameIonChannelName, ionChannelName);
      newSummary.field(fieldNameRawData, new JSONObject(rawData));
      newSummary.field(fieldNameProvenance, createProvenanceCollection(level, text, args));
      newSummary.save();

      final Set<ORID> existingSummaryClassIds = existingFundamental.field(fieldNameSummaryData);
      if (existingSummaryClassIds == null) {
        log.trace(identifyingLogPrefix.concat("Creating first summary record link."));
        final Set<ORID> newSummaryClassIds = new HashSet<ORID>();
        newSummaryClassIds.add(newSummary.getIdentity());
        existingFundamental.field(fieldNameSummaryData, newSummaryClassIds);
      } else {
        log.trace(identifyingLogPrefix.concat("Appending summary to existing links."));
        existingSummaryClassIds.add(newSummary.getIdentity());
      }
      existingFundamental.save();
    } else {
      throwStateError(methodName, identifying,
                      "Unexpected count of '" + resultCount  + "' records for class '" + classSummary + "'.");
    }
  }

  @PreDestroy
  protected void closeOrient() {
    log.trace("~odb-closeOrient() : Closing pool.");
    ODatabaseDocumentPool.global().close();
  }

  protected void dropOrient() {
    log.trace("~odb-dropOrient() : Dropping Orient.");
    final ODatabaseDocumentTx database = ODatabaseDocumentPool.global().acquire(ORIENT_INAME,
                                                                                ORIENT_IUSERNAME,
                                                                                ORIENT_IUSERPASSWORD);
    database.drop();
  }
}
