/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.dao;

import java.math.BigDecimal;

import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;

/**
 * Interface for the Simulation Data Access Object.
 * 
 * @author Geoff Williams
 */
public interface SimulationDAO {

  /**
   * Retrieve a simulation according to .
   * 
   * @param compoundIdentifier Compound identifier.
   * @param assayGrouping Whether simulation enacts assay grouping.
   * @param valueInheriting Whether simulation enacts value inheriting.
   * @param betweenGroups Whether simulation enacts value inheriting between groups.
   * @param withinGroups Whether simulation enacts value inheriting withing groups.
   * @param pc50EvaluationStrategies PC50 evaluation strategies.
   * @param doseResponseFittingStrategy Dose-response fitting strategy, e.g. IC50_ONLY.
   * @param doseResponseFittingRounding Whether rounding of value to pIC50 0 (zero) should occur.
   * @param doseResponseFittingHillMinMax True if Hill min/max fitting, otherwise false. 
   * @param doseResponseFittingHillMax Max. Hill Coefficient value (or {@code null} if not applicable).
   * @param doseResponseFittingHillMin Min. Hill Coefficient value (or {@code null} if not applicable).
   * @param cellModelIdentifier CellML Model identifier.
   * @param pacingMaxTime Maximum pacing time (in minutes), (or {@code null} if not specified).
   * @return Simulation corresponding to specified inputs, otherwise {@code null}.
   */
  Simulation findByAllProperties(String compoundIdentifier, boolean assayGrouping,
                                 boolean valueInheriting, boolean betweenGroups,
                                 boolean withinGroups, String pc50EvaluationStrategies,
                                 String doseResponseFittingStrategy, boolean doseResponseFittingRounding,
                                 boolean doseResponseFittingHillMinMax,
                                 Float doseResponseFittingHillMax, Float doseResponseFittingHillMin,
                                 short cellModelIdentifier, BigDecimal pacingMaxTime);

  /**
   * Retrieve the simulation with the specified id.
   * 
   * @param simulationId Persistence simulation id.
   * @return Simulation with requested simulation id, or {@code null} if not found.
   */
  Simulation findBySimulationId(long simulationId);

  /**
   * Persist a simulation.
   * 
   * @param simulation Simulation to persist.
   * @return The managed (stored) simulation.
   */
  Simulation store(Simulation simulation);

  /**
   * Update the simulation to indicate that all jobs, if there were any, have
   * been completed.
   * <p>
   * Usually you'd expect some simulations to run, so perhaps it may take a few
   * minutes before this function is called. If however there was no
   * data for the compound, or perhaps there were only estimated/modifier IC50s
   * and the user only wanted simulations to run using actual IC50 values, then
   * the this is called on simulation termination immediately after returning
   * from retrieving the site data.
   * 
   * @param simulationId Simulation identifier.
   */
  void updateOnJobsCompletion(long simulationId);

}