/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 * 
 */
package uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.Ordered;

import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.AbstractPC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.DerivedFromC20Strategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.NoSuchDataException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICHillVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;

/**
 * Situation where the 50% inhibitory concentration value has a non-equality modifier whereas the
 * 20% inhibitory concentration value has an equality modifier - in which case calculate the pIC50
 * value from the 20% data value, taking into account Hill Coefficient value if available.
 *
 * @author geoff
 */
public class C50NonEqualityC20Equality extends AbstractPC50EvaluationStrategy
                                       implements DerivedFromC20Strategy, Ordered {

  protected static final BigDecimal defaultHillCoefficient = BigDecimal.ONE;

  private static final long serialVersionUID = -789259499988353450L;

  private static final Log log = LogFactory.getLog(C50NonEqualityC20Equality.class);

  /**
   * {@inheritDoc}
   */
  protected C50NonEqualityC20Equality(final String description, final boolean defaultActive,
                                      final int defaultInvocationOrder)
                                      throws IllegalArgumentException {
    super(description, defaultActive, defaultInvocationOrder);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.DerivedFromC20Strategy#deriveFromC20(uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord, uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData, java.lang.String)
   */
  @Override
  public ICHillVO deriveFromC20(final IndividualDataRecord individualData, 
                                final ICData icData, final String identifyingPrefix)
                                throws InvalidValueException {
    final String logPrefix = "~deriveFromC20() : " + identifyingPrefix + " : ";
    log.debug(logPrefix.concat(" Invoked."));

    ICHillVO icHillVO = null;
    try {
      /* Conditions :
       *   1. 50% and 20% inhibitory concentration data.
       *   2. IC50 inhibitory concentration has a non-equality modifier.
       *   3. IC20 inhibitory concentration has an equality modifier.
       */
      if ((icData.hasICPctData(ICPercent.PCT50) && icData.hasICPctData(ICPercent.PCT20)) &&
          (!icData.isModifierEquals(ICPercent.PCT50) && icData.isModifierEquals(ICPercent.PCT20))) {
        final BigDecimal recordedHill = individualData.eligibleHillCoefficient();
        BigDecimal useHill = defaultHillCoefficient;
        String source = "default";
        if (recordedHill != null) {
          source = "recorded";
          useHill = recordedHill;
        }
        log.debug(logPrefix.concat("Using the " + source + " Hill value of '" + useHill + "'."));
        final BigDecimal pIC50 = icData.retrievePC50BasedOnIC20(useHill);
        if (pIC50 != null) {
          icHillVO = new ICHillVO(pIC50, useHill);
        }
      }
    } catch (NoSuchDataException e) { }

    return icHillVO;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy#usesEqualityModifier()
   */
  @Override
  public boolean usesEqualityModifier() {
    return true;
  }
}