/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object.doseresponse;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingParams;

/**
 * Value object holding Dose-Response fitting parameters.
 *
 * @author geoff
 */
public class DoseResponseFittingParamsVO implements DoseResponseFittingParams {

  private final String fittingOption;
  private final boolean rounding;
  private final Float hillMax;
  private final Float hillMin;

  /**
   * Intialising constructor.
   * 
   * @param fittingOption Selected fitting option, e.g. IC50_ONLY.
   * @param rounding Whether result rounding (to pIC50 0) should occur.
   * @param hillMax Maximum Hill Coefficient (or null if not relevant).
   * @param hillMin Minimum Hill Coefficient (or null if not relevant).
   */
  public DoseResponseFittingParamsVO(final String fittingOption, final boolean rounding,
                                     final Float hillMax, final Float hillMin) {
    this.fittingOption = fittingOption;
    this.rounding = rounding;
    this.hillMin = hillMin;
    this.hillMax = hillMax;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "DoseResponseFittingParamsVO [fittingOption=" + fittingOption
        + ", rounding=" + rounding + ", hillMax=" + hillMax + ", hillMin="
        + hillMin + "]";
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingParams#getFittingOption()
   */
  @Override
  public String getFittingOption() {
    return fittingOption;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingParams#getHillMax()
   */
  @Override
  public Float getHillMax() {
    return hillMax;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingParams#getHillMin()
   */
  @Override
  public Float getHillMin() {
    return hillMin;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingParams#isRounding()
   */
  @Override
  public boolean isRounding() {
    return rounding;
  }
}