/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.annotation.Header;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.InputValueSource;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.QSARIVS;
import uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata.ProcessedSiteDataVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.workflow.OutcomeVO;
//import uk.ac.ox.cs.nc3rs.business_manager.api.business.StaticDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.APIIdentifiers;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ExperimentalDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.QSARDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.QSARDataHolder.DataPacket;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.AbstractProblemTransferVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening.AggregatedSiteDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;

/**
 * Check to see what data has returned from the various processing entities.
 *
 * @author geoff
 */
public class PostProcessingDataAggregator {

  // JMS Template for sending provenance details.
  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JMS_TEMPLATE)
  private BusinessManagerJMSTemplate jmsTemplate;

  private static final String cross = MessageKey.MARK_CROSS.getBundleIdentifier();
  private static final String tick = MessageKey.MARK_TICK.getBundleIdentifier();

  private static final Log log = LogFactory.getLog(PostProcessingDataAggregator.class);

  /**
   * Collate the data from the various sources :
   * <ul>
   *   <li>Experimental data querying</li>
   *   <li>QSAR processing</li>
   *   <li>Screening processing</li>
   * </ul>
   * 
   * @param compoundIdentifier Compound identifier.
   * @param processingType Processing type.
   * @param simulationId Simulation identifier.
   * @param processedDataCollection Collection of processed data objects.
   * @return Processed site data value object.
   * @throws IllegalArgumentException If a {@code null} value is received for the
   *                                  {@code processedDataCollection} arg.
   */
  @SuppressWarnings("unchecked")
  // See appCtx.sim.processSimulation.xml
  public ProcessedSiteDataVO postProcessingDataCollation(final @Header(value=APIIdentifiers.SI_HDR_COMPOUND_IDENTIFIER,
                                                                       required=true)
                                                               String compoundIdentifier,
                                                         final @Header(value=BusinessIdentifiers.SI_HDR_PROCESSING_TYPE,
                                                                       required=true)
                                                               ProcessingType processingType,
                                                         final @Header(value=BusinessIdentifiers.SI_HDR_SIMULATION_ID,
                                                                       required=true)
                                                               Long simulationId,
                                                         final List<Object> processedDataCollection)
                                                         throws IllegalArgumentException {
    final String logPrefix = "~postProcessingDataCollation() : [" + simulationId + "][" + compoundIdentifier + "] : ";

    if (processedDataCollection == null) {
      throw new IllegalArgumentException("Null processed data collection passed for post-processing collation!");
    }

    final boolean inputDataGathering = ProcessingType.isInputDataGathering(processingType);
    log.debug(logPrefix.concat("Invoked."));

    Boolean receivedExperimental = false;
    Boolean receivedQSAR = false;
    Boolean receivedScreening = false;

    ExperimentalDataHolder experimental = null;
    final List<OutcomeVO> screening = new ArrayList<OutcomeVO>();
    final List<OutcomeVO> qsar = new ArrayList<OutcomeVO>();

    for (final Object eachProcessed : processedDataCollection) {
      log.debug(logPrefix.concat("Object '" + eachProcessed.getClass() + "'."));
      if (eachProcessed instanceof ArrayList<?>) {
        // Screening data: Collection of OutcomeVO's generated by screening's WorkflowProcessor
        final List<OutcomeVO> processedScreeningData = (List<OutcomeVO>) eachProcessed;
        log.debug(logPrefix.concat("'" + processedScreeningData.size() + "' items of screening data received."));
        for (final OutcomeVO eachOutcome : processedScreeningData) {
          if (!eachOutcome.getInputValueSources().isEmpty()) {
            screening.add(eachOutcome);
          } else {
            /* Valid case if invalid or no *measured* (i.e. only estimated) inhibitory concentration
               data encountered. */
            log.debug(logPrefix.concat("Received an empty outcome for '" + eachOutcome.toString() + "'!"));
          }
        }
        receivedScreening = true;
      } else if (eachProcessed instanceof QSARDataHolder) {
        log.debug(logPrefix.concat("QSARDataHolder received."));
        final QSARDataHolder qsarDataHolder = (QSARDataHolder) eachProcessed;
        /* For screening the workflow processing generates the OutcomeVO collection, but if QSAR 
           processing did the same there'd be two lots of ArrayList<?> arriving in processedDataCollection! */
        if (qsarDataHolder.containsData()) {
          final AssayVO assay = qsarDataHolder.getAssay();
          receivedQSAR = true;
          for (final DataPacket dataPacket : qsarDataHolder.getDataPackets()) {
            final IonChannel ionChannel = dataPacket.getIonChannel();
            final BigDecimal pIC50 = dataPacket.getpIC50();

            // Only expecting one pIC50 outcome per ion channel for QSAR
            final List<InputValueSource> inputValueSources = new ArrayList<InputValueSource>(1);
            inputValueSources.add(new QSARIVS(pIC50));

            qsar.add(new OutcomeVO(assay, ionChannel, inputValueSources));
          }
        }
        if (!inputDataGathering) {
          final List<String> problems = ((AbstractProblemTransferVO) qsarDataHolder).getProblems();
          if (!problems.isEmpty()) {
            jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.WARN,
                                                                 StringUtils.join(problems, "\n"),
                                                                 new String[] { },
                                                                 simulationId).build());
          }
        }
      } else if (eachProcessed instanceof ExperimentalDataHolder) {
        log.debug(logPrefix.concat("ExperimentalDataHolder received."));
        experimental = (ExperimentalDataHolder) eachProcessed;

        if (experimental.containsData()) {
          receivedExperimental = true;
        }
      } else if (eachProcessed instanceof AggregatedSiteDataVO) {
        /*
         * AggregatedSiteDataVO appears here when no screening data (regular or irregular).
         * See appCtx.proc.screening.xml -- screening_screeningDataRecipientListRouter
         * If problems were encountered they should have been copied to the JMS progress system
         *   earlier in the pipeline, e.g. SiteDataRecorderServiceActivator#recordScreeningSiteData()
         */
        log.debug(logPrefix.concat("AggregatedSiteDataVO received by post processing data collation. Indicates no screening data found."));
        final AggregatedSiteDataVO aggregatedSiteData = (AggregatedSiteDataVO) eachProcessed;
        if (aggregatedSiteData.hasSiteData() || aggregatedSiteData.hasIrregularSiteData()) {
          final String errorMessage = "Screening data (regular or irregular) found when none expected!";
          log.error(logPrefix.concat(errorMessage));
          throw new IllegalStateException(errorMessage);
        }

        if (inputDataGathering && aggregatedSiteData.hasProblems()) {
          log.warn(logPrefix.concat("Removing from StaticDataHolder."));
          log.warn(logPrefix.concat("Problems are : '" + aggregatedSiteData.getProblems() + "'."));

          //StaticDataHolder.remove(compoundIdentifier);
        }
      } else {
        if (eachProcessed instanceof Simulation) {
          log.info(logPrefix.concat("Simulation object received. Bridge endpoint used in Spring Integration, e.g. perhaps no QSAR processing?"));
        } else {
          log.warn(logPrefix.concat("Object of unrecognised type '" + eachProcessed.getClass() + "' received."));
        }
      }
    }

    final ProcessedSiteDataVO processedSiteData = new ProcessedSiteDataVO(experimental, qsar,
                                                                          screening);

    if (!inputDataGathering) {
      jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.INFO,
                                                           MessageKey.SIMULATION_DATA_COLLATED.getBundleIdentifier(),
                                                           new String[] { tickOrCross(receivedExperimental),
                                                                          tickOrCross(receivedQSAR),
                                                                          tickOrCross(receivedScreening) },
                                                           simulationId).build());
    }

    return processedSiteData;
  }

  // return a tick or cross according to boolean input.
  private String tickOrCross(final boolean received) {
    return received ? tick : cross;
  }
}
