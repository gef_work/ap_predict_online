/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.summary.SummaryOnlyC50DataStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;

/**
 * This {@link InputValueSource} object, containing summary data input data value(s) (i.e. pIC50(s)
 * and a Hill Coefficient), is created during <i>Screening</i> workflow processing when the 
 * {@link PC50EvaluationStrategy} is of type {@link SummaryOnlyC50DataStrategy}.
 *
 * @author geoff
 * @see IndividualIVS
 * @see QSARIVS
 */
public class SummaryIVS implements InputValueSource {

  private static final long serialVersionUID = 7927984366709762778L;

  // A summary record may generate a number of pIC50's, e.g. according to the count of individuals.
  private final List<BigDecimal> pIC50s = new ArrayList<BigDecimal>();
  // In summary records it is anticipated that the Hill Coefficient value is an average.
  private final BigDecimal hillCoefficient;
  // The strategy from which the pIC50 value was derived.
  private final PC50EvaluationStrategy strategy;
  private final SummaryDataRecord summaryDataRecord;

  private static final Log log = LogFactory.getLog(SummaryIVS.class);

  /**
   * Intialising constructor for when the pIC50(s) arrived from summary data. It is currently
   * expected that a number of pIC50s will be created and an average Hill Coefficient used.
   * 
   * @param pIC50s pIC50(s) which the strategy generated.
   * @param hillCoefficient Hill Coefficient (most likely to be an average value?).
   * @param strategy Strategy which generated the pIC50 value(s).
   * @param summaryDataRecord Summary data record.
   * @throws IllegalArgumentException If no or a {@code null} pIC50(s) encountered.
   */
  public SummaryIVS(final List<BigDecimal> pIC50s, final BigDecimal hillCoefficient,
                    final PC50EvaluationStrategy strategy,
                    final SummaryDataRecord summaryDataRecord) {
    log.debug("~SummaryIVS() : Invoked.");
    if (pIC50s == null || pIC50s.isEmpty() || pIC50s.contains(null)) {
      final String errorMessage = "None or a null pIC50 encountered in summary input value source constructor";
      log.error("~SummaryIVS() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }
    this.pIC50s.addAll(pIC50s);
    this.hillCoefficient = hillCoefficient;
    this.strategy = strategy;
    this.summaryDataRecord = summaryDataRecord;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "SummaryIVS [pIC50s=" + pIC50s + ", hillCoefficient="
        + hillCoefficient + ", strategy=" + strategy + ", summaryDataRecord="
        + summaryDataRecord + "]";
  }

  /**
   * Indicator as to whether the C50 data was as per the original raw data.
   * 
   * @return {@code true} if true copy of raw data, otherwise {@code false}.
   */
  public boolean isOriginalData() {
    return strategy.usesEqualityModifier();
  }

  /**
   * Retrieve the default invocation order of the pIC50 evaluation strategy which generated this 
   * pIC50.
   * 
   * @return Default invocation order of pIC50-generating strategy.
   */
  public int retrieveStrategyOrder() {
    return strategy.getOrder();
  }

  /**
   * Retrieve the pIC50s for this summary data record.
   * <p>
   * The number of values will depend on the count of individual data records which contributed to
   * the summary pIC50 value.
   * 
   * @return Unmodifiable collection of non-{@code null} individual pIC50s for the summary data.
   */
  public List<BigDecimal> getpIC50s() {
    return Collections.unmodifiableList(pIC50s);
  }

  /**
   * Retrieve the Hill Coefficient.
   * <p>
   * This is most likely to be an average value.
   * 
   * @return Hill Coefficient (or {@code null} if none provided).
   */
  public BigDecimal getHillCoefficient() {
    return hillCoefficient;
  }

  /**
   * Retrieve the summary data record which has generated the pIC50 values.
   * 
   * @return The summary data record.
   */
  public SummaryDataRecord getSummaryDataRecord() {
    return summaryDataRecord;
  }
}