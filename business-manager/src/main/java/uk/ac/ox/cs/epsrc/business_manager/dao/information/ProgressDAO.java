/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.dao.information;

import java.util.Date;
import java.util.List;

import uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager.INFORMATION_FORMAT;
import uk.ac.ox.cs.epsrc.business_manager.value.object.InformationVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Interface to progress data access object handling persisted (not dynamically created if a 
 * simulation is running) progress data.
 *
 * @author geoff
 */
public interface ProgressDAO {

  /**
   * Create simulation/all progress information.
   *
   * @param compoundIdentifier Compound identifier.
   * @param simulationId Simulation identifier.
   * @param timestamp Progress timestamp.
   * @param level Progress level.
   * @param text Progress text.
   * @param args Information arguments.
   * @throws IllegalArgumentException On {@code null} or invalid identifiers.
   * @throws IllegalStateException If the query of simulation/all progress returns one or more
   *                               records.
   */
  void createProgressAll(String compoundIdentifier, long simulationId, Date timestamp,
                         InformationLevel level, String text, List<String> args)
                         throws IllegalArgumentException, IllegalStateException;

  /**
   * Create job progress information.
   * <p>
   * This operation requires a prior call to {@link #createProgressAll(String, long, Date, InformationLevel, String, List)}
   * to have been made (as this job's progress will become associated with it).
   * 
   * @param simulationId Simulation identifier.
   * @param jobId Job identifier.
   * @param groupName Group name.
   * @param groupLevel Group level.
   * @param pacingFrequency Pacing frequency.
   * @param timestamp Progress timestamp.
   * @param level Progress level.
   * @param text Progress text.
   * @param args Information arguments.
   * @throws IllegalArgumentException On {@code null} or invalid identifiers.
   * @throws IllegalStateException If there is anything other than one simulation/all progress data
   *                               structure found for the supplied {@code simulationId}. 
   */
  void createProgressJob(long simulationId, long jobId, String groupName, short groupLevel,
                         float pacingFrequency, Date timestamp, InformationLevel level, String text,
                         List<String> args) throws IllegalArgumentException, IllegalStateException;

  /**
   * Retrieve the persisted (not dynamic) simulation progress (not job progress) for the compound.
   * 
   * @param simulationId Simulation identifier.
   * @param format Format of the returned progress.
   * @return Progress value object (potentially empty if none found) for compound.
   * @throws IllegalArgumentException If an invalid simulation identifier or a {@code null}
   *                                  format value.
   * @throws IllegalStateException If there is more than one progress structure found for the
   *                               supplied {@code simulationId}.
   */
  InformationVO retrieveProgress(long simulationId, INFORMATION_FORMAT format)
                                 throws IllegalArgumentException, IllegalStateException;

  /**
   * Update simulation/all progress information.
   * <p>
   * Before simulation/all progress can be added the {@link #createProgressAll(String, long, Date, InformationLevel, String, List)}
   * method must have been invoked for the specified simulation.
   *
   * @param simulationId Simulation identifier.
   * @param timestamp Progress timestamp.
   * @param level Progress level.
   * @param text Progress text.
   * @param args Information arguments.
   * @throws IllegalArgumentException On {@code null} or invalid identifiers.
   * @throws IllegalStateException If there is anything other than one simulation/all progress data
   *                               structure found for the supplied {@code simulationId}.
   */
  void updateProgressAll(long simulationId, Date timestamp, InformationLevel level, String text,
                         List<String> args) throws IllegalArgumentException, IllegalStateException;

  /**
   * Update job progress information.
   * <p>
   * Before job progress can be added the {@link #createProgressJob(long, long, String, short, float, Date, InformationLevel, String, List)}
   * method must have been invoked for the specified job.
   * <p>
   * A job progress data structure will contain at least one, but possibly many, job progress texts. 
   * 
   * @param jobId Job identifier.
   * @param timestamp Progress timestamp.
   * @param level Progress level.
   * @param text Progress text.
   * @param args Information arguments.
   * @throws IllegalArgumentException On {@code null} or invalid identifiers.
   * @throws IllegalStateException If there is anything other than one job progress data structure
   *                               found for the supplied {@code jobId}.
   */
  void updateProgressJob(long jobId, Date timestamp, InformationLevel level, String text,
                         List<String> args) throws IllegalArgumentException, IllegalStateException;
}