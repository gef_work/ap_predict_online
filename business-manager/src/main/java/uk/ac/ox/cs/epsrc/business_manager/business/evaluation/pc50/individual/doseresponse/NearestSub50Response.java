/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual.doseresponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import uk.ac.ox.cs.epsrc.business_manager.business.DoseResponseTransformer;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.IndividualProvenance;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.DoseResponseData;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingParams;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.IndividualProcessing;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Dose-response fitting strategy which involves taking the nearest sub-50 inhibition data.
 *
 * @author geoff
 */
public class NearestSub50Response extends AbstractDoseResponseFittingStrategy {

  private static final long serialVersionUID = -1277698763486438911L;

  // Brief, informative description of fitting nature appearing in various output.
  private static final String fittingNature = "Nearest sub-50 ";
  // Used in other parts of the application.
  private static final String sysProvenanceStrategySuffix = "sub50";

  private static final Log log = LogFactory.getLog(NearestSub50Response.class);

  /**
   * Initialising constructor.
   * 
   * @param description Description of the fitting strategy.
   * @param defaultActive Indicator of whether the strategy should be active by default.
   * @param defaultInvocationOrder Strategy's invocation order.
   */
  @Autowired(required=true)
  public NearestSub50Response(final String description, final boolean defaultActive,
                              final int defaultInvocationOrder) {
    super(description, defaultActive, defaultInvocationOrder);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual.doseresponse.AbstractDoseResponseFittingStrategy#retrieveDoseResponseData(long, uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.IndividualProcessing, uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData, java.lang.String, uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingParams, java.lang.String, boolean)
   */
  @Override
  protected DoseResponseData retrieveDoseResponseData(final long simulationId,
                                                      final IndividualProcessing individualProcessing,
                                                      final ICData individualC50Data,
                                                      final String recordIdentifier,
                                                      final DoseResponseFittingParams doseResponseFittingParams,
                                                      final String individualDataId,
                                                      final boolean provenanceable)
                                                      throws InvalidValueException {
    final String logPrefix = "~retrieveDoseResponseData() : " + recordIdentifier + " : ";
    log.debug(logPrefix.concat("Invoked [").concat(retrieveFittingNature()).concat("]"));

    // If there's no data available then send provenance (if appropriate) and exit.
    if (!individualProcessing.hasDoseResponseData()) {
      final String debugMessage = "No collection of dose-response records available!";
      if (provenanceable) {
        getJmsTemplate().sendProvenance(new IndividualProvenance.Builder(InformationLevel.TRACE,
                                                                         debugMessage, simulationId)
                                                                .individualDataId(individualDataId)
                                                                .build());
      }
      log.debug(logPrefix + debugMessage);
      return null;
    }

    // Retrieve the dose-response data corresponding to the point below 50% response.
    final DoseResponseData doseResponseData = DoseResponseTransformer.nearestSub50ResponseTransform(individualProcessing);

    // If there's no sub-50 data send provenance then exit.
    if (doseResponseData.doseResponsePoints().isEmpty()) {
      log.debug("~derivePC50FromFitting() : Dose-response data available, but no sub-50% (dose-)response data points available!");

      final String debugMessage = "No (dose-)response data point found in records!";
      if (provenanceable) {
        getJmsTemplate().sendProvenance(new IndividualProvenance.Builder(InformationLevel.TRACE,
                                                                         debugMessage, simulationId)
                                                                .individualDataId(individualDataId)
                                                                .build());
      }
      log.debug(logPrefix + debugMessage);
      return null;
    }

    return doseResponseData;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual.doseresponse.AbstractDoseResponseFittingStrategy#retrieveFittingNature()
   */
  @Override
  protected String retrieveFittingNature() {
    return fittingNature;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual.doseresponse.AbstractDoseResponseFittingStrategy#retrieveSysProvenanceSuffix()
   */
  @Override
  protected String retrieveSysProvenanceSuffix() {
    return sysProvenanceStrategySuffix;
  }
}