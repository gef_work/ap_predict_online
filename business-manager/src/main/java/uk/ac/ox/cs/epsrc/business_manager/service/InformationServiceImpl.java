/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.entity.Job;
import uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager;
import uk.ac.ox.cs.epsrc.business_manager.manager.JobManager;
import uk.ac.ox.cs.epsrc.business_manager.value.object.AppManagerJobProgressVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.InformationVO;
import uk.ac.ox.cs.epsrc.business_manager.ws.app_manager.manager.AppManagerManager;

/**
 * Implementation of the information service.
 *
 * @author geoff
 */
@Component(BusinessIdentifiers.COMPONENT_INFORMATION_SERVICE)
public class InformationServiceImpl implements InformationService {

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_APP_MANAGER_MANAGER)
  private AppManagerManager appManagerManager;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_INFORMATION_MANAGER)
  private InformationManager informationManager;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JOB_MANAGER)
  private JobManager jobManager;

  private static final Log log = LogFactory.getLog(InformationServiceImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.service.InformationService#retrieveProgress(long)
   */
  @Override
  public InformationVO retrieveProgress(final long simulationId) {
    log.debug("~retrieveProgress(Long) : Invoked.");

    // retrieve Orientdb progress data.
    final InformationVO informationVO = informationManager.retrieveProgress(simulationId);

    // retrieve additional job progress data from the app manager.
    final List<Job> jobs = jobManager.findJobsBySimulationId(simulationId);
    for (final Job job : jobs) {
      final String appManagerId = job.getAppManagerId();
      final Long jobId = job.getId();
      if (appManagerId != null) {
        // Need to call out to the app manager for the statuses of any jobs running.
        final AppManagerJobProgressVO jobProgress = new AppManagerJobProgressVO(jobId);
        // TODO: App Manager id.... either it's a string or it's a long!! 
        appManagerManager.retrieveJobProgress(jobProgress, Long.valueOf(appManagerId));
        final JSONObject jsonObject = new JSONObject(jobProgress);
        final String jobProgressJSON = jsonObject.toString();
        informationVO.assignAppManagerJobProgress(jobProgressJSON);
      }
    }

    return informationVO;
  }
}
