/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.annotation.Header;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.DoseResponseProvenance;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.FundamentalProvenance;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.IndividualProvenance;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.SummaryProvenance;
import uk.ac.ox.cs.epsrc.business_manager.value.object.doseresponse.DoseResponseDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ExperimentalDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.IrregularSiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.QSARDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ProblemVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.doseresponse.DoseResponsePairVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening.AggregatedSiteDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProblemType;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;

/**
 * Spring integration service activator which records the important parts of a site's data.
 *
 * @author geoff
 */
public class SiteDataRecorderServiceActivator {

  // JMS Template for sending provenance details.
  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JMS_TEMPLATE)
  private BusinessManagerJMSTemplate jmsTemplate;

  private static final Log log = LogFactory.getLog(SiteDataRecorderServiceActivator.class);

  /**
   * Record the site-specific experimental data loaded in from their data source.
   * 
   * @param processingType Processing type.
   * @param simulationId Simulation identifier.
   * @param experimentalData Non-null experimental data.
   * @return Experimental data.
   * @throws IllegalArgumentException If <code>experimentalData</code> argument is null.
   */
  public ExperimentalDataHolder recordExperimentalSiteData(final @Header(value=BusinessIdentifiers.SI_HDR_PROCESSING_TYPE,
                                                                         required=true)
                                                                 ProcessingType processingType,
                                                           final @Header(value=BusinessIdentifiers.SI_HDR_SIMULATION_ID,
                                                                         required=true)
                                                                 Long simulationId,
                                                           final ExperimentalDataHolder experimentalData)
                                                           throws IllegalArgumentException {
    if (experimentalData == null) {
      final String errorMessage = "Illegal attempt to record experimental site data using a null experimental data holder object.";
      log.error("~recordExperimentalSiteData() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    if (ProcessingType.isInputDataGathering(processingType)) {
      return experimentalData;
    }
    log.debug("~recordExperimentalSiteData() : Invoked.");

    if (experimentalData.containsData()) {
      // TODO : Implement recording of experimental data.
      System.out.println("~recordExperimentalSiteData() : TODO: Implement recording of experimental data.");
    }

    return experimentalData;
  }

  /**
   * Record the site-specific QSAR data.
   * 
   * @param processingType Processing type.
   * @param simulationId Simulation identifier.
   * @param qsarData QSAR data.
   * @return QSAR data.
   * @throws IllegalArgumentException If <code>qsarData</code> argument is null.
   */
  public QSARDataHolder recordQSARSiteData(final @Header(value=BusinessIdentifiers.SI_HDR_PROCESSING_TYPE,
                                                        required=true)
                                                 ProcessingType processingType,
                                           final @Header(value=BusinessIdentifiers.SI_HDR_SIMULATION_ID,
                                                         required=true)
                                                 Long simulationId,
                                           final QSARDataHolder qsarData)
                                           throws IllegalArgumentException {
    if (qsarData == null) {
      final String errorMessage = "Illegal attempt to record QSAR site data using a null QSAR data holder object.";
      log.error("~recordQSARSiteData() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    if (ProcessingType.isInputDataGathering(processingType)) {
      return qsarData;
    }
    log.debug("~recordQSARSiteData() : Invoked.");

    if (qsarData.containsData()) {
      // TODO : Implement recording of QSAR data.
      log.warn("~recordQSARSiteData() : TODO: Implement recording of QSAR data.");
    }

    return qsarData;
  }

  /**
   * Record the site-specific screening data loaded in from their data source(s).
   * 
   * @param processingType Processing type.
   * @param simulationId Simulation identifier.
   * @param aggregatedSiteDataVO Site-specific aggregated site data.
   * @return Site-specific aggregated site data.
   * @throws IllegalArgumentException If no {@code aggregatedSiteDataVO} is supplied.
   * @throws InvalidValueException If invalid value encountered.
   */
  public AggregatedSiteDataVO recordScreeningSiteData(final @Header(value=BusinessIdentifiers.SI_HDR_PROCESSING_TYPE,
                                                                    required=true)
                                                            ProcessingType processingType,
                                                      final @Header(value=BusinessIdentifiers.SI_HDR_SIMULATION_ID,
                                                                    required=true)
                                                            Long simulationId,
                                                      final AggregatedSiteDataVO aggregatedSiteDataVO)
                                                      throws IllegalArgumentException,
                                                             InvalidValueException {
    if (aggregatedSiteDataVO == null) {
      final String errorMessage = "Invalid attempt to record screening site data using null aggregated site data";
      log.error("~recordScreeningSiteData() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    if (ProcessingType.isInputDataGathering(processingType)) {
      return aggregatedSiteDataVO;
    }
    log.debug("~recordScreeningSiteData() : Invoked.");

    // First check to see if there were problems reading in site screening data.
    for (final String retrievalProblem : aggregatedSiteDataVO.getProblems()) {
      jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.ERROR,
                                                           MessageKey.SITEDATA_SCREENING_DATA_RETRIEVE_PROBLEM.getBundleIdentifier(),
                                                           new String[] { retrievalProblem },
                                                           simulationId).build());
    }

    final List<SiteDataHolder> siteDataHolder = aggregatedSiteDataVO.getSiteDataHolders();
    final int siteDataCount = siteDataHolder.size();

    final List<IrregularSiteDataHolder> irregularSiteDataHolder = aggregatedSiteDataVO.getIrregularSiteDataHolders();
    final int irregularSiteDataCount = irregularSiteDataHolder.size();

    jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.DEBUG,
                                                         MessageKey.AGGR_SITEDATA_COUNT.getBundleIdentifier(),
                                                         new String[] { Integer.valueOf(siteDataCount).toString(), 
                                                                        Integer.valueOf(irregularSiteDataCount).toString() },
                                                         simulationId).build());

    /*
     * Loop through all the regular and irregular site data retrieved from the site and create
     * placeholders in the provenance system.
     */
    if (siteDataCount > 0) {
      jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.DEBUG,
                                                           MessageKey.AGGR_SITEDATA_ANALYSIS.getBundleIdentifier(),
                                                           simulationId).build());

      for (final SiteDataHolder eachSiteData : siteDataHolder) {
        final String assayName = eachSiteData.getAssay().getName();
        final String ionChannelName = eachSiteData.getIonChannel().name();
        final String logPrefix = "~recordScreeningSiteData() : [" + assayName + "][" + ionChannelName + "] : ";

        final SummaryDataRecord summaryData = eachSiteData.getSummaryDataRecord(false);
        final boolean hasSummaryDataRecord = (summaryData != null);

        String summaryDataId = null;
        log.debug(logPrefix);
        if (hasSummaryDataRecord) {
          summaryDataId = summaryData.getId();
          log.debug(logPrefix + "SummaryData Id '" + summaryDataId + "'.");
          final String summaryDataSourceName = summaryData.getSourceName();

          // create new summary with assay / ion channel.
          jmsTemplate.sendProvenance(new SummaryProvenance.Builder(InformationLevel.TRACE,
                                                                   "Summary record from source '" + summaryDataSourceName + "'.",
                                                                   simulationId)
                                                          .summaryDataId(summaryDataId)
                                                          .assayName(assayName)
                                                          .ionChannelName(ionChannelName)
                                                          .rawData(summaryData.getRawData())
                                                          .build());
        }

        final Map<IndividualDataRecord, List<DoseResponseDataRecord>> nonSummaryData = eachSiteData.getNonSummaryDataRecords();
        final int nonSummaryDataCount = nonSummaryData.size();
        final boolean hasNonSummaryData = (nonSummaryDataCount > 0);

        if (hasNonSummaryData) {
          log.debug(logPrefix + "NonSummary data available - append to parent (Summ/Sim) provenance.");
          if (hasSummaryDataRecord) {
            // update a summary record.
            jmsTemplate.sendProvenance(new SummaryProvenance.Builder(InformationLevel.TRACE,
                                                                     "Individual record(s) linked to Summary.",
                                                                     simulationId)
                                                            .summaryDataId(summaryDataId).build());
          } else {
            // update a simulation record.
            jmsTemplate.sendProvenance(new FundamentalProvenance.Builder(InformationLevel.TRACE,
                                                                         "Individual record(s) linked to Simulation.",
                                                                         simulationId).build());
          }

          for (final Map.Entry<IndividualDataRecord, List<DoseResponseDataRecord>> eachNonSummaryData :
                     nonSummaryData.entrySet()) {
            final IndividualDataRecord individualData = eachNonSummaryData.getKey();
            final String individualDataId = individualData.getId();
            final String individualDataSourceName = individualData.getSourceName();
            final Map<String, Object> individualDataRawData = individualData.getRawData();
            final BigDecimal individualRawHill = individualData.eligibleHillCoefficient();
            if (hasSummaryDataRecord) {
              // Create a new individual, link it to summary
              log.debug(logPrefix + "IndividualData Id '" + individualDataId + "' with SummaryData.");
              jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.TRACE,
                                                                          "Individual records from source '" + individualDataSourceName + "'.",
                                                                          simulationId)
                                                                 .summaryDataId(summaryDataId)
                                                                 .individualDataId(individualDataId)
                                                                 .rawHill(individualRawHill)
                                                                 .rawData(individualDataRawData)
                                                                 .build());
            } else {
              log.debug(logPrefix + "IndividualData Id '" + individualDataId + "' without SummaryData.");
              // Create a new individual, link it to simulation
              jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.TRACE,
                                                                          "Individual records from source '" + individualDataSourceName + "'.",
                                                                          simulationId)
                                                                 .individualDataId(individualDataId)
                                                                 .assayName(assayName)
                                                                 .ionChannelName(ionChannelName)
                                                                 .rawHill(individualRawHill)
                                                                 .rawData(individualDataRawData)
                                                                 .build());
            }

            final List<DoseResponseDataRecord> doseResponseData = eachNonSummaryData.getValue();

            if (doseResponseData == null) { 
              continue;
            }

            final List<DoseResponsePairVO> doseResponsePairs = new ArrayList<DoseResponsePairVO>();
            if (!doseResponseData.isEmpty()) {
              for (final DoseResponseDataRecord eachDoseResponseData : doseResponseData) {
                final String doseResponseDataId = eachDoseResponseData.getId();
                final String doseResponseDataSourceName = eachDoseResponseData.getSourceName();
                final Map<String, Object> doseResponseDataRawData = eachDoseResponseData.getRawData();

                log.debug(logPrefix + "IndividualData Id '" + individualDataId + "' DoseResponseData Id '" + doseResponseDataId + "'.");
                // create a new dose_response, link it to individual.
                jmsTemplate.sendProvenance(new DoseResponseProvenance.Builder(InformationLevel.TRACE,
                                                                              "Dose-response records from source '" + doseResponseDataSourceName + "'.",
                                                                              simulationId)
                                                                     .individualDataId(individualDataId)
                                                                     .doseResponseDataId(doseResponseDataId)
                                                                     .rawData(doseResponseDataRawData)
                                                                     .build());

                final DoseResponsePairVO doseResponsePair = eachDoseResponseData.eligibleDoseResponseData();
                if (doseResponsePair != null) {
                  doseResponsePairs.add(doseResponsePair);
                  jmsTemplate.sendProvenance(new DoseResponseProvenance.Builder(InformationLevel.TRACE,
                                                                                "Dose-response values found in raw data records.",
                                                                                simulationId)
                                                                       .doseResponseDataId(doseResponseDataId)
                                                                       .dose_in_uM(doseResponsePair.getDose().toPlainString())
                                                                       .response(doseResponsePair.getResponse().toPlainString())
                                                                       .build());
                } else {
                  jmsTemplate.sendProvenance(new DoseResponseProvenance.Builder(InformationLevel.INFO,
                                                                                "No dose-Response values found in raw data record.",
                                                                                simulationId)
                                                                       .doseResponseDataId(doseResponseDataId)
                                                                       .build());
                  
                }
              }
            }
            if (!doseResponsePairs.isEmpty()) {
              jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.TRACE,
                                                                          "Dose-response record(s) linked to Individual.",
                                                                          simulationId)
                                                                 .individualDataId(individualDataId)
                                                                 .doseResponseData(new DoseResponseDataVO(doseResponsePairs))
                                                                 .recordingDoseResponses().build());
            }
          }
        } else {
          log.debug(logPrefix + " Summary only.");
          jmsTemplate.sendProvenance(new SummaryProvenance.Builder(InformationLevel.TRACE,
                                                                   "Summary data only! No individual record(s) found.",
                                                                   simulationId)
                                                          .summaryDataId(summaryDataId).build());
        }
      }
    }

    if (irregularSiteDataCount > 0) {
      for (final IrregularSiteDataHolder eachIrregularSiteData : irregularSiteDataHolder) {
        final String assayName = eachIrregularSiteData.getAssay().getName();
        final String ionChannelName = eachIrregularSiteData.getIonChannel().name();
        final String logPrefix = "~recordScreeningSiteData() : [" + assayName + "][" + ionChannelName + "] : ";

        for (final ProblemVO problemVO : eachIrregularSiteData.getProblems()) {
          final ProblemType problem = problemVO.getProblem();
          switch (problem) {
            case NO_DOSE_RESPONSES :
            case NO_DOSE_RESPONSES_MATCHED :
            case UNATTRIBUTABLE_DOSE_RESPONSES :
              final String problemDescription = problem.getMessageKey();
              final String[] problemParams = problemVO.retrieveParamsAsStringArray();
              jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.WARN,
                                                                   problemDescription,
                                                                   problemParams, simulationId)
                                                          .build());
              jmsTemplate.sendProvenance(new FundamentalProvenance.Builder(InformationLevel.WARN,
                                                                           problemDescription,
                                                                           problemParams,
                                                                           simulationId).build());
              break;
            default :
              final String warnText = "Problem '" + problem + "' reported by site. Implement appropriate handling in business manager code!";
              System.out.println(warnText);
              log.warn(logPrefix.concat(warnText));
              break;
          }
        }
      }
    }

    return aggregatedSiteDataVO;
  }
}
