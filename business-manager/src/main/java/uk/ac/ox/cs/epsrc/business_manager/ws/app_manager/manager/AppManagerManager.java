/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.app_manager.manager;

import java.util.Set;

import uk.ac.ox.cs.epsrc.business_manager.entity.Job;
import uk.ac.ox.cs.epsrc.business_manager.value.object.AppManagerJobProgressVO;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ResultsReadyNotificationRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.BusinessDataUploadCompletedRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllResultsRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveAllResultsResponse;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveDiagnosticsRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.RetrieveDiagnosticsResponse;

/**
 * Interface to the App manager operations.
 *
 * @author geoff
 */
public interface AppManagerManager {

  // See the app manager xsd
  public static final String APP_MANAGER_WORKLOAD_IDENTIFIER = "Workload";

  /**
   * Create a diagnostics retrieval request based on the notification from App manager that results
   * are ready. 
   * 
   * @param notificationRequest Results are ready notification request.
   * @return Diagnostics retrieval request.
   */
  public RetrieveDiagnosticsRequest createDiagnosticsRequest(ResultsReadyNotificationRequest notificationRequest);

  /**
   * Create the ApPredict WS SOAP object.
   * 
   * @param simulationId Simulation identifier.
   * @param job Simulation job.
   * @return ApPredict WS SOAP object.
   */
  public ApPredictRunRequest createRunRequest(Long simulationId, Job job);

  /**
   * Delete the simulations from the app manager;
   * 
   * @param appManagerIds Identities of simulations to delete.
   * @return Identities of simulations deleted.
   * @throws IllegalArgumentException If {@code null} value is passed as parameter.
   */
  public Set<String> deleteJobs(Set<String> appManagerIds) throws IllegalArgumentException;

  /** 
   * Handle the response received from the ApPredict run request.
   * 
   * @param jobId Job identifier.
   * @param simulationId Simulation identifier.
   * @param runRequest Original request.
   * @param runResponse Response to original request.
   */
  public void handleRunResponse(Long jobId, Long simulationId, ApPredictRunRequest runRequest,
                                ApPredictRunResponse runResponse);

  /**
   * Persist the diagnostics data.
   * 
   * @param diagnosticsResponse Diagnostics data.
   * @return Request to retrieve all the job results.
   */
  public RetrieveAllResultsRequest persistDiagnostics(RetrieveDiagnosticsResponse diagnosticsResponse);

  /**
   * Persist the simulation results data.
   * 
   * @param resultsResponse App manager response with simulation results.
   * @return Completion notification for app manager.
   */
  public BusinessDataUploadCompletedRequest persistResults(RetrieveAllResultsResponse resultsResponse);

  /**
   * Retrieve the job progress from the app manager, i.e. it is being retrieved dynamically with
   * each invocation and is not being persisted by the BusinessManager.
   *  
   * @param jobProgress Job progress data-holding object (for data transfer out from this routine).
   * @param appManagerId App manager id.
   */
  public void retrieveJobProgress(AppManagerJobProgressVO jobProgress, Long appManagerId);

  /**
   * Retrieve data of the workload.
   * 
   * @return Current workload.
   */
  String retrieveWorkload();
}