/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Information (e.g. progress, provenance) value object.
 *
 * @author geoff
 */
public class InformationVO {

  private final Long simulationId;
  private final String information;
  private final Set<String> appManagerJobProgress = new HashSet<String>();

  public static final String EMPTY_ASSOC_ARRAY = "{}";

  private static final Log log = LogFactory.getLog(InformationVO.class);

  /**
   * Initialising constructor.
   * 
   * @param simulationId Simulation identifier.
   * @param information The information to return (generally in JSON format). If <code>null</code>
   *                    is passed this will be converted into an empty associative array representation.
   */
  public InformationVO(final Long simulationId, final String information) {
    this.simulationId = simulationId;
    if (information == null || information.trim().length() == 0) {
      this.information = EMPTY_ASSOC_ARRAY;
    } else {
      this.information = information;
    }
    log.debug("~InformationVO() : Created '" + this.toString() + "'.");
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "InformationVO [simulationId=" + simulationId + ", information="
        + information + ", appManagerJobProgress=" + appManagerJobProgress
        + "]";
  }

  /**
   * Concatenate or assign app manager job progress data.
   * 
   * @param appManagerJobProgress JSON-format app Manager job progress data.
   */
  public void assignAppManagerJobProgress(final String appManagerJobProgress) {
    log.debug("~appendAppManagerJobProgress() : Invoked.");

    if (appManagerJobProgress != null) {
      this.appManagerJobProgress.add(appManagerJobProgress);
    }
  }

  /**
   * Indicate if the object contains information.
   * 
   * @return True if information available, otherwise false.
   */
  public boolean hasInformation() {
    return EMPTY_ASSOC_ARRAY.equals(information) ? false : true;
  }

  /**
   * Retrieve the simulation identifier.
   * 
   * @return Simulation identifier (or null if not supplied).
   */
  public Long getSimulationId() {
    return simulationId;
  }

  /**
   * Retrieve the information.
   * 
   * @return Information, or empty associative array if none available.
   */
  public String getInformation() {
    return information;
  }

  /**
   * Retrieve the app manager job progress.
   * 
   * @return Unmodifiable app manager job progress (or empty collection if none available).
   */
  public Set<String> getAppManagerJobProgress() {
    return Collections.unmodifiableSet(appManagerJobProgress);
  }
}