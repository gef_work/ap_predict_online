/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.Ordered;

import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.AbstractPC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.AlertingNonInclusionStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.NoSuchDataException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;

/**
 * Determine if an alert is to be flagged and the data not included in the simulation if there are
 * no equality modifiers in either of inhibitory concentration data. If there is an equality
 * modifier then no alert will be flagged and the data will be available for onward procesing.
 *
 * @author geoff
 */
public abstract class AbstractNonEqualityModifierNonInclusionAlerting extends AbstractPC50EvaluationStrategy
                                                                      implements AlertingNonInclusionStrategy,
                                                                                 Ordered {

  private static final String activeAlert = "No IC50 measured. Compound highly active at lowest concentration.";
  private static final String inactiveAlert = "No IC50 measured. Compound inactive at highest concentration";

  private static final long serialVersionUID = 4351123874263699155L;

  private static final Log log = LogFactory.getLog(AbstractNonEqualityModifierNonInclusionAlerting.class);

  /**
   * {@inheritDoc}
   */
  protected AbstractNonEqualityModifierNonInclusionAlerting(final String description,
                                                            final boolean defaultActive,
                                                            final int defaultInvocationOrder)
                                                            throws IllegalArgumentException {
    super(description, defaultActive, defaultInvocationOrder);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.AlertingNonInclusionStrategy#soundAlert(uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData)
   */
  @Override
  public String soundAlert(final ICData icData) {
    log.debug("~soundAlert() : Invoked.");
    String detailedAlert = null;

    final boolean has20PctData = icData.hasICPctData(ICPercent.PCT20);
    final boolean has50PctData = icData.hasICPctData(ICPercent.PCT50);
    // isActive indicating either unassigned, active or inactive
    Boolean isActive = null;

    try {
      Boolean pct20IndicatesActive = null;
      Boolean pct50IndicatesActive = null;
      if (has20PctData) {
        if (icData.isModifierEquals(ICPercent.PCT20)) {
          return null;
        }
        pct20IndicatesActive = icData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT20);
      }
      if (has50PctData) {
        if (icData.isModifierEquals(ICPercent.PCT50)) {
          return null;
        }
        pct50IndicatesActive = icData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50);
      }

      if (pct20IndicatesActive != null && pct50IndicatesActive != null) {
        // Presence of both non-equality C20 and C50 inhib conc modifiers.
        if (pct20IndicatesActive && pct50IndicatesActive) {
          isActive = true;
        } else if (!pct20IndicatesActive && !pct50IndicatesActive) {
          isActive = false;
        }
        if (isActive != null) {
          detailedAlert = icData.retrieveOriginalInfo(ICPercent.PCT20).concat("], [")
                          .concat(icData.retrieveOriginalInfo(ICPercent.PCT50));
        } else {
          // It's looking like dodgy data if it hits here as conflicting modifiers!!!
          log.warn("~soundAlert() : Conflicting modifiers encountered in '" + icData.toString() + "'!");
        }
      } else if (pct20IndicatesActive != null) {
        // 20% inhib. conc. data indicates no exact C20 value, but data indicates either active or inactive.
        isActive = pct20IndicatesActive; 
        detailedAlert = icData.retrieveOriginalInfo(ICPercent.PCT20);
      } else if (pct50IndicatesActive != null) {
        // 50% inhib. conc. data indicates no exact C50 value, but data indicates either active or inactive.
        isActive = pct50IndicatesActive;
        detailedAlert = icData.retrieveOriginalInfo(ICPercent.PCT50);
      }

      if (isActive != null) {
        detailedAlert = "[".concat(detailedAlert).concat("]");
        if (isActive) {
          detailedAlert = activeAlert.concat(" : ").concat(detailedAlert);
        } else {
          detailedAlert = inactiveAlert.concat(" : ").concat(detailedAlert);
        }
      }
    } catch (NoSuchDataException e) {}

    return detailedAlert;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy#usesEqualityModifier()
   */
  @Override
  public boolean usesEqualityModifier() {
    return false;
  }
}