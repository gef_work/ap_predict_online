/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object;

import java.math.BigDecimal;

/**
 * Value object holding <b>quality-controlled</b> (i.e. verified and standardised) data 
 * representing a user's simulation request, e.g. :
 * <ul>
 *   <li>Compound identifier</li>
 *   <li>Force a simulation re-run</li>
 *   <li>PC50 evaluation strategies</li>
 *   <li>Inheritance strategies</li>
 *   <li>etc.</li>
 * </ul>
 *
 * @author geoff
 */
public class SimulationRequestVO {

  private final String compoundIdentifier;
  private final boolean forceReRun;
  private final boolean reset;
  private final boolean assayGrouping;
  private final boolean valueInheriting;
  private final boolean withinGroups;
  private final boolean betweenGroups;
  private final String pc50EvaluationStrategies;
  private final String doseResponseFittingStrategy;
  private final boolean doseResponseFittingRounding;
  private final boolean doseResponseFittingHillMinMax;
  private final Float doseResponseFittingHillMax;
  private final Float doseResponseFittingHillMin;
  private final short cellModelIdentifier;
  private final boolean inputDataOnly;
  private final BigDecimal pacingMaxTime; 
  private final String userId;

  /**
   * Initialising constructor being passed verified and standardised parameters.
   * 
   * @param compoundIdentifier Compound identifier.
   * @param forceReRun {@code true} if simulation is to be forcefully re-run.
   * @param reset {@code true} if simulation is to be reset.
   * @param assayGrouping {@code true} if input value assay grouping is to occur.
   * @param valueInheriting {@code true} if input value value inheriting is to
   *                        occur.
   * @param withinGroups {@code true} if input value inheriting within assay
   *                     groups (if assay grouping!) is to occur.
   * @param betweenGroups {@code true} if input value inheriting between assay
   *                      groups (if assay grouping!) is to occur. 
   * @param pc50EvaluationStrategies Ordered CSV representation of PC50
   *                                 evaluation strategies to undertake.
   * @param doseResponseFittingStrategy Dose-response fitting strategy, e.g.
   *                                    IC50_ONLY.
   * @param doseResponseFittingRounding Whether rounding of value to pIC50 0
   *                                    (zero) should occur.
   * @param doseResponseFittingHillMinMax Where fitting a min and/or max Hill
   *                                      value.
   * @param doseResponseFittingHillMax Max. Hill Coefficient value (or
   *                                   {@code null} if not applicable).
   * @param doseResponseFittingHillMin Min. Hill Coefficient value (or
   *                                   {@code null} if not applicable).
   * @param cellModelIdentifier CellML Model identifier.
   * @param pacingMaxTime Maximum pacing time (in minutes).
   * @param inputDataOnly Whether the simulation request is for input data only.
   * @param userId User identifier.
   */
  public SimulationRequestVO(final String compoundIdentifier,
                             final boolean forceReRun,
                             final boolean reset, final boolean assayGrouping,
                             final boolean valueInheriting,
                             final boolean withinGroups,
                             final boolean betweenGroups,
                             final String pc50EvaluationStrategies,
                             final String doseResponseFittingStrategy,
                             final boolean doseResponseFittingRounding,
                             final boolean doseResponseFittingHillMinMax,
                             final Float doseResponseFittingHillMax,
                             final Float doseResponseFittingHillMin,
                             final short cellModelIdentifier,
                             final BigDecimal pacingMaxTime,
                             final boolean inputDataOnly,
                             final String userId) {
    this.compoundIdentifier = compoundIdentifier;
    this.forceReRun = forceReRun;
    this.reset = reset;
    this.assayGrouping = assayGrouping;
    this.valueInheriting = valueInheriting;
    this.withinGroups = withinGroups;
    this.betweenGroups = betweenGroups;
    this.pc50EvaluationStrategies = pc50EvaluationStrategies;
    this.doseResponseFittingStrategy = doseResponseFittingStrategy;
    this.doseResponseFittingRounding = doseResponseFittingRounding;
    this.doseResponseFittingHillMinMax = doseResponseFittingHillMinMax;
    this.doseResponseFittingHillMax = doseResponseFittingHillMax;
    this.doseResponseFittingHillMin = doseResponseFittingHillMin;
    this.cellModelIdentifier = cellModelIdentifier;
    this.pacingMaxTime = pacingMaxTime;
    this.inputDataOnly = inputDataOnly;
    this.userId = userId;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "SimulationRequestVO [compoundIdentifier=" + compoundIdentifier
        + ", forceReRun=" + forceReRun + ", reset=" + reset + ", assayGrouping="
        + assayGrouping + ", valueInheriting=" + valueInheriting
        + ", withinGroups=" + withinGroups + ", betweenGroups=" + betweenGroups
        + ", pc50EvaluationStrategies=" + pc50EvaluationStrategies
        + ", doseResponseFittingStrategy=" + doseResponseFittingStrategy
        + ", doseResponseFittingRounding=" + doseResponseFittingRounding
        + ", doseResponseFittingHillMinMax=" + doseResponseFittingHillMinMax
        + ", doseResponseFittingHillMax=" + doseResponseFittingHillMax
        + ", doseResponseFittingHillMin=" + doseResponseFittingHillMin
        + ", cellModelIdentifier=" + cellModelIdentifier + ", inputDataOnly="
        + inputDataOnly + ", pacingMaxTime=" + pacingMaxTime + ", userId="
        + userId + "]";
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + (assayGrouping ? 1231 : 1237);
    result = prime * result + (betweenGroups ? 1231 : 1237);
    result = prime * result + cellModelIdentifier;
    result = prime * result
        + ((compoundIdentifier == null) ? 0 : compoundIdentifier.hashCode());
    result = prime * result + ((doseResponseFittingHillMax == null) ? 0
        : doseResponseFittingHillMax.hashCode());
    result = prime * result + ((doseResponseFittingHillMin == null) ? 0
        : doseResponseFittingHillMin.hashCode());
    result = prime * result + (doseResponseFittingHillMinMax ? 1231 : 1237);
    result = prime * result + (doseResponseFittingRounding ? 1231 : 1237);
    result = prime * result + ((doseResponseFittingStrategy == null) ? 0
        : doseResponseFittingStrategy.hashCode());
    result = prime * result + (forceReRun ? 1231 : 1237);
    result = prime * result + (inputDataOnly ? 1231 : 1237);
    result = prime * result
        + ((pacingMaxTime == null) ? 0 : pacingMaxTime.hashCode());
    result = prime * result + ((pc50EvaluationStrategies == null) ? 0
        : pc50EvaluationStrategies.hashCode());
    result = prime * result + (reset ? 1231 : 1237);
    result = prime * result + ((userId == null) ? 0 : userId.hashCode());
    result = prime * result + (valueInheriting ? 1231 : 1237);
    result = prime * result + (withinGroups ? 1231 : 1237);
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    SimulationRequestVO other = (SimulationRequestVO) obj;
    if (assayGrouping != other.assayGrouping)
      return false;
    if (betweenGroups != other.betweenGroups)
      return false;
    if (cellModelIdentifier != other.cellModelIdentifier)
      return false;
    if (compoundIdentifier == null) {
      if (other.compoundIdentifier != null)
        return false;
    } else if (!compoundIdentifier.equals(other.compoundIdentifier))
      return false;
    if (doseResponseFittingHillMax == null) {
      if (other.doseResponseFittingHillMax != null)
        return false;
    } else if (!doseResponseFittingHillMax
        .equals(other.doseResponseFittingHillMax))
      return false;
    if (doseResponseFittingHillMin == null) {
      if (other.doseResponseFittingHillMin != null)
        return false;
    } else if (!doseResponseFittingHillMin
        .equals(other.doseResponseFittingHillMin))
      return false;
    if (doseResponseFittingHillMinMax != other.doseResponseFittingHillMinMax)
      return false;
    if (doseResponseFittingRounding != other.doseResponseFittingRounding)
      return false;
    if (doseResponseFittingStrategy == null) {
      if (other.doseResponseFittingStrategy != null)
        return false;
    } else if (!doseResponseFittingStrategy
        .equals(other.doseResponseFittingStrategy))
      return false;
    if (forceReRun != other.forceReRun)
      return false;
    if (inputDataOnly != other.inputDataOnly)
      return false;
    if (pacingMaxTime == null) {
      if (other.pacingMaxTime != null)
        return false;
    } else if (!pacingMaxTime.equals(other.pacingMaxTime))
      return false;
    if (pc50EvaluationStrategies == null) {
      if (other.pc50EvaluationStrategies != null)
        return false;
    } else if (!pc50EvaluationStrategies.equals(other.pc50EvaluationStrategies))
      return false;
    if (reset != other.reset)
      return false;
    if (userId == null) {
      if (other.userId != null)
        return false;
    } else if (!userId.equals(other.userId))
      return false;
    if (valueInheriting != other.valueInheriting)
      return false;
    if (withinGroups != other.withinGroups)
      return false;
    return true;
  }

  /**
   * Retrieve the CellML Model identifier.
   * 
   * @return CellML Model identifier.
   */
  public short getCellModelIdentifier() {
    return cellModelIdentifier;
  }

  /**
   * @return the compoundIdentifier
   */
  public String getCompoundIdentifier() {
    return compoundIdentifier;
  }

  /**
   * Retrieve the force re-run flag.
   * 
   * @return {@code true} if forcing re-run, otherwise {@code false}.
   */
  public boolean isForceReRun() {
    return forceReRun;
  }

  /**
   * Retrieve the reset flag.
   * 
   * @return {@code true} if resetting, otherwise {@code false}.
   */
  public boolean isReset() {
    return reset;
  }

  /**
   * @return the assayGrouping
   */
  public boolean isAssayGrouping() {
    return assayGrouping;
  }

  /**
   * @return the valueInheriting
   */
  public boolean isValueInheriting() {
    return valueInheriting;
  }

  /**
   * @return the withinGroups
   */
  public boolean isWithinGroups() {
    return withinGroups;
  }

  /**
   * @return the betweenGroups
   */
  public boolean isBetweenGroups() {
    return betweenGroups;
  }

  /**
   * @return the pc50EvaluationStrategies
   */
  public String getPc50EvaluationStrategies() {
    return pc50EvaluationStrategies;
  }

  /**
   * Retrieve the dose-response fitting strategy.
   * 
   * @return The dose-response fitting strategy.
   */
  public String getDoseResponseFittingStrategy() {
    return doseResponseFittingStrategy;
  }

  /**
   * @return the doseResponseFittingRounding
   */
  public boolean isDoseResponseFittingRounding() {
    return doseResponseFittingRounding;
  }

  /**
   * Retrieve indicator of whether to use minimum and maximum Hill coefficient
   * values in dose-response fitting calculations.
   * 
   * @return {@code true} if to use minimum and maximum Hill coefficient values
   *         in dose-response fitting, otherwise {@code false}.
   */
  public boolean isDoseResponseFittingHillMinMax() {
    return doseResponseFittingHillMinMax;
  }

  /**
   * Retrieve the maximum value allowable for Hill Coefficient.
   * 
   * @return Dose-Response fitting maximum Hill Coefficient (or {@code null} if
   *         not specified/relevant).
   */
  public Float getDoseResponseFittingHillMax() {
    return doseResponseFittingHillMax;
  }

  /**
   * Retrieve the minimum value allowable for Hill Coefficient.
   * 
   * @return Dose-Response fitting minimum Hill Coefficient (or {@code null} if
   *         not specified/relevant).
   */
  public Float getDoseResponseFittingHillMin() {
    return doseResponseFittingHillMin;
  }

  /**
   * Retrieve the maximum pacing time (in minutes).
   * 
   * @return Maximum pacing time (in minutes), or {@code null} if not specified.
   */
  public BigDecimal getPacingMaxTime() {
    return pacingMaxTime;
  }

  /**
   * Retrieve flag to indicate whether the user has request to run input data
   * only retrieval.
   * 
   * @return {@code true} if user seeking input data only, otherwise
   *         {@code false} (i.e. a normal simulation run.
   */
  public boolean isInputDataOnly() {
    return inputDataOnly;
  }

  /**
   * Retrieve the user identifier.
   * 
   * @return User identifier.
   */
  public String getUserId() {
    return userId;
  }
}