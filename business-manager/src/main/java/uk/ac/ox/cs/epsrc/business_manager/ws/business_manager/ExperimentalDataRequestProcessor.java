/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.business_manager;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.service.SimulationService;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ExperimentalResultsPerConcentration;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ExperimentalResultsPerFrequency;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ExperimentalResultsRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ExperimentalResultsResponse;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ObjectFactory;
import uk.ac.ox.cs.nc3rs.business_manager.api.APIIdentifiers;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.service.ExperimentalDataService;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ExperimentalDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.transfer.experimental.ExperimentalResult;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.AbstractProblemTransferVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.experimental.ConcQTPctChangeValuesVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Process requests for experimental data.
 *
 * @author geoff
 */
public class ExperimentalDataRequestProcessor {

  @Autowired @Qualifier(APIIdentifiers.COMPONENT_EXPERIMENTAL_DATA_SERVICE)
  private ExperimentalDataService experimentalDataService;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JMS_TEMPLATE)
  private BusinessManagerJMSTemplate jmsTemplate;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE)
  private SimulationService simulationService;

  private static final ObjectFactory objectFactory = new ObjectFactory();

  private static final Log log = LogFactory.getLog(ExperimentalDataRequestProcessor.class);

  /**
   * Retrieve the experimental data.
   * 
   * @param request Request for experimental data.
   * @return Response to request.
   * @throws IllegalArgumentException If an illegal argument used in method call.
   * @throws InvalidValueException If an invalid value encountered.
   */
  // method referenced from appCtx.experimental.processRequest.xml
  public ExperimentalResultsResponse retrieveExperimentalData(final ExperimentalResultsRequest request)
                                                              throws IllegalArgumentException,
                                                                     InvalidValueException {
    log.debug("~retrieveExperimentalData() : Invoked.");
    final long simulationId = request.getSimulationId();
    final String userId = request.getUserId();

    final ExperimentalResultsResponse response = objectFactory.createExperimentalResultsResponse();

    final Simulation simulation = simulationService.findBySimulationId(simulationId);
    if (simulation != null) {
      final String compoundIdentifier = simulation.getCompoundIdentifier();
      final ExperimentalDataHolder experimentalDataHolder = experimentalDataService.retrieveExperimentalData(compoundIdentifier,
                                                                                                             userId);
      response.setSimulationId(simulationId);
      if (experimentalDataHolder != null && experimentalDataHolder.containsData()) {
        transferBetweenStructures(response, experimentalDataHolder);
        final List<String> problems = ((AbstractProblemTransferVO) experimentalDataHolder).getProblems();
        if (problems.size() > 0) {
          jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.WARN,
                                                               problems.get(0), new String[] { },
                                                               simulationId).build());
        }
      }
    }

    return response;
  }

  // transfer the experimental data from the holder to a JAXB object
  private void transferBetweenStructures(final ExperimentalResultsResponse response,
                                         final ExperimentalDataHolder holder) {
    final String identifiedPrefix = "~transferBetweenStructures() : ";
    final Set<ExperimentalResult> experimentalResults = holder.getExperimentalResults();
    for (final ExperimentalResult experimentalResult : experimentalResults) {
      final BigDecimal pacingFrequency = experimentalResult.getPacingFrequency();

      final ExperimentalResultsPerFrequency perFrequency = objectFactory.createExperimentalResultsPerFrequency();
      perFrequency.setFrequencyHz(pacingFrequency.toPlainString());

      for (final ConcQTPctChangeValuesVO concentrationQTPctChangeValues : experimentalResult.getConcentrationQTPctChangeValues()) {
        final BigDecimal compoundConcentration = concentrationQTPctChangeValues.getCompoundConcentration();
        final BigDecimal qtPctChangeMean = concentrationQTPctChangeValues.getQtPctChangeMean();
        final BigDecimal qtPctChangeSEM = concentrationQTPctChangeValues.getQtPctChangeSEM();

        final ExperimentalResultsPerConcentration perConcentration = objectFactory.createExperimentalResultsPerConcentration();
        if (compoundConcentration != null) {
          final String compoundConcentrationStr = compoundConcentration.toPlainString();
          perConcentration.setPlasmaConc(compoundConcentrationStr);
          final String infoPrefix = identifiedPrefix.concat("Experimental data at Freq/Conc '" + pacingFrequency + "'/'" + compoundConcentrationStr + "' ");
          if (qtPctChangeMean != null) {
            perConcentration.setQtPctChangeMean(qtPctChangeMean.toPlainString());
            if (qtPctChangeSEM != null) {
              perConcentration.setQtPctChangeSEM(qtPctChangeSEM.toPlainString());
            } else {
              log.warn(infoPrefix.concat("contained null % change SEM value = ignoring SEM data."));
            }
          } else {
            log.warn(infoPrefix.concat("contained null % change mean value = ignoring mean and SEM data."));
          }
          perFrequency.getExperimentalResultsPerConcentration().add(perConcentration);
        } else {
          log.warn(identifiedPrefix.concat("Experimental data at Freq '" + pacingFrequency + "' contained null compound concentration value(s) - ignoring data."));
        }
      }

      response.getExperimentalResultsPerFrequency().add(perFrequency);
    }
  }
}
