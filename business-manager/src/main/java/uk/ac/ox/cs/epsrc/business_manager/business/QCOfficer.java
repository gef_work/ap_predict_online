/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.annotation.Header;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.IndividualProvenance;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.nc3rs.business_manager.api.APIIdentifiers;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.QualityControlFailureVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening.AbstractGenericSiteDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening.SiteDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;

/**
 * Remove items if they've failed quality control.
 *
 * @author geoff
 */
public class QCOfficer {

  // JMS Template for sending provenance details.
  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JMS_TEMPLATE)
  private BusinessManagerJMSTemplate jmsTemplate;

  private static final Log log = LogFactory.getLog(QCOfficer.class);

  private class VerifiedSiteDataVO extends SiteDataVO {
    /**
     * Initialising constructor.
     * 
     * @param assay Assay.
     * @param ionChannel Ion Channel.
     * @param summaryDataRecord Summary data record (Optionally null if only individual data available).
     * @param nonSummaryDataRecords Non-summary (i.e. individual and (if appropriate) dose-response) data records.
     * @throws IllegalArgumentException See {@link AbstractGenericSiteDataVO#AbstractGenericSiteDataVO(AssayVO, IonChannel, SummaryDataRecord, Map, boolean)}
     */
    public VerifiedSiteDataVO(final AssayVO assay, final IonChannel ionChannel,
                              final SummaryDataRecord summaryDataRecord,
                              final Map<IndividualDataRecord, List<DoseResponseDataRecord>> nonSummaryDataRecords)
                              throws IllegalArgumentException {
      super(assay, ionChannel, summaryDataRecord, nonSummaryDataRecords);
    }
  }

  /**
   * Remove data which has failed the site's quality controls.
   * 
   * @param compoundIdentifier Compound identifier.
   * @param processingType Processing type.
   * @param simulationId Simulation identifier.
   * @param inputDataGatheringSimulation (Optional) Input data gathering simulation object. 
   * @param initialSiteData Site data.
   * @return Quality-controlled site data.
   * @throws IllegalArgumentException If the <code>initialSiteData</code> parameter is null.
   * @throws InvalidValueException In invalid value encountered.
   */
  public SiteDataHolder removeQCFailedData(final @Header(value=APIIdentifiers.SI_HDR_COMPOUND_IDENTIFIER,
                                                         required=true)
                                                 String compoundIdentifier,
                                           final @Header(value=BusinessIdentifiers.SI_HDR_PROCESSING_TYPE,
                                                         required=true)
                                                 ProcessingType processingType,
                                           final @Header(value=BusinessIdentifiers.SI_HDR_SIMULATION_ID,
                                                         required=true)
                                                 Long simulationId,
                                           final @Header(value=BusinessIdentifiers.SI_HDR_INPUTDATAGATHERING_SIMULATION,
                                                         required=false)
                                                 Simulation inputDataGatheringSimulation,
                                           final SiteDataHolder initialSiteData)
                                           throws IllegalArgumentException, InvalidValueException {
    if (initialSiteData == null) {
      final String errorMessage = "Invalid use of null site data holder when removing QC-failed data.";
      log.error("~removeQCFailedData() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }
    final boolean inputDataGathering = ProcessingType.isInputDataGathering(processingType);

    final AssayVO assayVO = initialSiteData.getAssay();
    final String assayName = assayVO.getName();
    final IonChannel ionChannel = initialSiteData.getIonChannel();
    final String ionChannelName = ionChannel.name();

    final String identifyingPrefix = "[" + simulationId + "][" + compoundIdentifier + "][" + assayName + "][" + ionChannelName + "] : ";
    String methodPrefix = "~removeQCFailedData() : ".concat(identifyingPrefix);

    final SummaryDataRecord initialSummaryData = initialSiteData.getSummaryDataRecord(false);

    SummaryDataRecord transitionalSummaryData = null;
    if (initialSummaryData != null) {
      // Test for summary data quality control data invalidity
      final QualityControlFailureVO summaryQCInvalidityFailure = initialSummaryData.isMarkedInvalid();
      if (summaryQCInvalidityFailure == null) {
        log.debug(methodPrefix.concat("Summary data passed data invalidity QC."));
        transitionalSummaryData = initialSummaryData;
      } else {
        final String reason = summaryQCInvalidityFailure.getReason();
        final String message = "Summary data for [" + assayName + "][" + ionChannelName + "] to be ignored. Failing QC invalidity test '" + reason + "'.";
        log.info(methodPrefix.concat(message));
        if (!inputDataGathering) {
          jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.DEBUG,
                                                               message, simulationId).build());
        }
        transitionalSummaryData = null;
      }
    }

    final Map<IndividualDataRecord, List<DoseResponseDataRecord>> transitionalNonSummaryData = 
          new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();
    
    final Map<IndividualDataRecord, List<DoseResponseDataRecord>> initialNonSummaryData = initialSiteData.getNonSummaryDataRecords();
    if (!initialNonSummaryData.isEmpty()) {
      for (final Map.Entry<IndividualDataRecord, List<DoseResponseDataRecord>> eachInitialNonSummaryData :
                 initialNonSummaryData.entrySet()) {
        final IndividualDataRecord eachInitialIndividualData = eachInitialNonSummaryData.getKey();

        final QualityControlFailureVO individualQCInvalidityFailure = eachInitialIndividualData.isMarkedInvalid();
        if (individualQCInvalidityFailure == null) {
          log.debug(methodPrefix.concat("Individual data passed invalidity QC"));
          transitionalNonSummaryData.put(eachInitialIndividualData,
                                         eachInitialNonSummaryData.getValue());
        } else {
          final String reason = individualQCInvalidityFailure.getReason();
          final String message = "Individual data record for [" + assayName + "][" + ionChannelName + "] to be ignored. Failing QC invalidity test '" + reason + "'.";
          log.info(methodPrefix.concat(message));
          if (!inputDataGathering) {
            jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.DEBUG,
                                                                 message, simulationId).build());
            /* Pipe symbol for client-side emphasising when displaying progress/provenance */
            jmsTemplate.sendProvenance(new IndividualProvenance.Builder(InformationLevel.INFO,
                                                                        "|Quality control marked record as invalid|",
                                                                        simulationId)
                                                               .individualDataId(eachInitialIndividualData.getId())
                                                               .build());
          }
        }
      }
    }

    return new VerifiedSiteDataVO(assayVO, ionChannel, transitionalSummaryData,
                                  transitionalNonSummaryData);
  }
}