/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object.workflow;

/**
 * Value object holding dose-response fitting configuration values.
 *
 * @author geoff
 */
public class DoseResponseFittingConfigVO {

  private final String parameterFitting;
  private final boolean rounded;
  private final float hillMax;
  private final float hillMin;

  /**
   * Initialising constructor.
   * 
   * @param parameterFitting Parameter fitting technique, e.g. IC50 only.
   * @param rounded True if to limit IC50s &gt; 1e6 to 1e6 (to pIC50 0), otherwise limitless. 
   * @param hillMax Maximum Hill Coefficient (for use if fitting is IC50_AND_HILL).
   * @param hillMin Minimum Hill Coefficient (for use if fitting is IC50_AND_HILL).
   */
  public DoseResponseFittingConfigVO(final String parameterFitting, final boolean rounded,
                                     final float hillMax, final float hillMin) {
    this.parameterFitting = parameterFitting;
    this.rounded = rounded;
    this.hillMax = hillMax;
    this.hillMin = hillMin;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "DoseResponseFittingConfigVO [parameterFitting=" + parameterFitting
        + ", rounded=" + rounded + ", hillMax=" + hillMax + ", hillMin="
        + hillMin + "]";
  }

  /**
   * @return the parameterFitting
   */
  public String getParameterFitting() {
    return parameterFitting;
  }

  /**
   * @return the rounded
   */
  public boolean isRounded() {
    return rounded;
  }

  /**
   * @return the hillMax
   */
  public float getHillMax() {
    return hillMax;
  }

  /**
   * @return the hillMin
   */
  public float getHillMin() {
    return hillMin;
  }

  
}