/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.DoseResponseData;
import uk.ac.ox.cs.epsrc.business_manager.value.object.doseresponse.DoseResponseDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.IndividualProcessing;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.doseresponse.DoseResponsePairVO;

/**
 * Component for converting data structures into dose-response collections.
 *
 * @author geoff
 */
public class DoseResponseTransformer {

  private static final Log log = LogFactory.getLog(DoseResponseTransformer.class);

  /**
   * Transfer the individual processing dose-response records into a dose response data object.
   * <p>
   * It is assumed that there is dose-response data available in the individual processing argument.
   * 
   * @param individualProcessing Individual processing object.
   * @return Non-null dose-response data object derived from dose-response records.
   * @throws IllegalArgumentException If argument is null or without dose-response data.
   * @throws InvalidValueException If invalid value is encountered.
   */
  public static DoseResponseData allDoseResponseTransform(final IndividualProcessing individualProcessing)
                                                          throws IllegalArgumentException,
                                                                 InvalidValueException {
    log.debug("~allDoseResponseTransform() : Invoked.");
    if (individualProcessing == null) {
      throw new IllegalArgumentException("Individual processing is null!");
    }
    final List<DoseResponseDataRecord> doseResponseDataRecords = individualProcessing.getDoseResponseData();
    if (doseResponseDataRecords == null || doseResponseDataRecords.isEmpty()) {
      // This should not happen as the system should have passed data which is known to have dose-responses
      throw new IllegalArgumentException("Individual processing contained a null dose-response data collection!");
    }

    final List<DoseResponsePairVO> individualDoseResponseDataPoints = 
          new ArrayList<DoseResponsePairVO>(doseResponseDataRecords.size());
    for (final DoseResponseDataRecord doseResponseDataRecord : doseResponseDataRecords) {
      individualDoseResponseDataPoints.add(doseResponseDataRecord.eligibleDoseResponseData());
    }

    return new DoseResponseDataVO(individualDoseResponseDataPoints);
  }

  /**
   * Transform the individual processing dose-response data into the nearest sub-50 response point.
   * 
   * @param individualProcessing Individual processing object.
   * @return Dose-response data representing a singular dose-response point below 50% (or object containing
   *         empty value object if no data available).
   * @throws IllegalArgumentException If argument is null.
   * @throws InvalidValueException If invalid value is encountered.
   */
  public static DoseResponseData nearestSub50ResponseTransform(final IndividualProcessing individualProcessing)
                                                               throws IllegalArgumentException,
                                                                      InvalidValueException {
    log.debug("~nearestSub50ResponseTransform() : Invoked.");
    if (individualProcessing == null) {
      throw new IllegalArgumentException("Individual processing is null!");
    }
    final DoseResponsePairVO nearestSub50DoseResponsePair = allDoseResponseTransform(individualProcessing).
                                                            nearestSub50ResponsePoint();

    final List<DoseResponsePairVO> sub50DoseResponsePairs = new ArrayList<DoseResponsePairVO>();
    if (nearestSub50DoseResponsePair != null) {
      sub50DoseResponsePairs.add(nearestSub50DoseResponsePair);
    }
    final DoseResponseData sub50DoseResponseData = new DoseResponseDataVO(sub50DoseResponsePairs);

    return sub50DoseResponseData;
  }
}
