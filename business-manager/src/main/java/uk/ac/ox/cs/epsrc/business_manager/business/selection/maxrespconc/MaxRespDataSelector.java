/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.selection.maxrespconc;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.business.selection.maxrespconc.MaxRespDataSelectionStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.individual.MaxRespConcData;

/**
 * Process the max response / max response concentration data read in from the raw data records.
 *
 * @author geoff
 */
public class MaxRespDataSelector {

  private static final List<MaxRespDataSelectionStrategy> defaultMaxRespDataSelectionStrategies = new ArrayList<MaxRespDataSelectionStrategy>(1);
  private static final List<MaxRespDataSelectionStrategy> maxRespDataSelectionStrategies = defaultMaxRespDataSelectionStrategies;

  private static final Log log = LogFactory.getLog(MaxRespDataSelector.class);

  static {
    defaultMaxRespDataSelectionStrategies.add(new AnyMaxRespData());
  }

  /**
   * Retrieve the {@link MaxRespConcData} from the eligible raw data options which conforms to the 
   * selection strategies assigned.
   * 
   * @param eligibleMaxRespData Eligible {@link MaxRespConcData} derived from the raw data. Optionally
   *                            this value may be an empty collection if data doesn't allow, but not null.
   * @return The {@link MaxRespConcData} which meets the criteria defined by the (prioritised) strategies,
   *         or <code>null</code> if no suitable data.
   * @throws IllegalArgumentException If a <code>null</code> object is passed for eligible max
   *                                  response data.
   */
  public static MaxRespConcData chooseMaxRespData(final List<MaxRespConcData> eligibleMaxRespData) {
    log.debug("~chooseMaxRespData() : Invoked.");
    if (eligibleMaxRespData == null) {
      final String errorMessage = "Cannot pass a null eligibleMaxRespData collection to MaxRespDataSelector.";
      log.error("~chooseMaxRespData() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }
    if (eligibleMaxRespData.isEmpty()) {
      // valid outcome in circumstances where, for example, the table doesn't have a PCT_MAX_INHIB/MAX_RESP
      log.debug("~chooseMaxRespData() : No eligibleMaxRespData passed so no selection made.");
      return null;
    }

    MaxRespConcData selected = null;
    for (final MaxRespDataSelectionStrategy maxRespDataSelectionStrategy : maxRespDataSelectionStrategies) {
      final MaxRespConcData maxRespConcData = maxRespDataSelectionStrategy.select(eligibleMaxRespData);
      if (maxRespConcData != null) {
        log.debug("~chooseMaxRespData() : Selected '" + maxRespConcData.toString() + "'.");
        selected = maxRespConcData;
        break;
      }
    }

    return selected;
  }

  /**
   * Overwrite the default max response data selection strategies
   * 
   * @param newMaxRespDataSelectionStrategies New strategies to assign.
   * @throws IllegalArgumentException If null or no strategies assigned.
   */
  public static void setMaxRespDataSelectionStrategies(final List<MaxRespDataSelectionStrategy> newMaxRespDataSelectionStrategies) {
    if (newMaxRespDataSelectionStrategies == null || newMaxRespDataSelectionStrategies.isEmpty()) {
      final String message = "Invalid attempt to assign a null or empty collection of max resp data selection strategies.";
      log.error("~setMaxRespDataSelectionStrategies() : ".concat(message));
      throw new IllegalArgumentException(message);
    }
    log.warn("~setMaxRespDataSelectionStrategies() : Overwriting the default max response data selection strategies.");
    maxRespDataSelectionStrategies.clear();
    maxRespDataSelectionStrategies.addAll(newMaxRespDataSelectionStrategies);
  }
}