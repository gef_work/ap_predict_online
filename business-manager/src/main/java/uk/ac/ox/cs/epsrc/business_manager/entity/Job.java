/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.entity;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Simulation job value holder, to be submitted to the app manager.
 *
 * @author geoff
 */
@Entity
@NamedQueries({
  @NamedQuery(name=Job.QUERY_JOB_BY_APP_MANAGER_ID,
              query="SELECT job FROM Job AS job " +
                    "  WHERE " + Job.PROPERTY_APP_MANAGER_ID + " = :" + Job.PROPERTY_APP_MANAGER_ID),
  @NamedQuery(name=Job.QUERY_JOB_DATA_BY_SIMULATION_ID,
              query="SELECT job FROM Job AS job " +
                    "  WHERE " + Job.PROPERTY_SIMULATION_ID + " = :" + Job.PROPERTY_SIMULATION_ID),
  @NamedQuery(name=Job.QUERY_JOB_BY_COMPLETED_FOR_SIMULATION,
              query="SELECT COUNT(*) FROM Job " +
                    "  WHERE (" + Job.PROPERTY_SIMULATION_ID + " = :" + Job.PROPERTY_SIMULATION_ID + ") " +
                    "    AND (" + Job.PROPERTY_COMPLETED + " IS FALSE)")
})
@Table(uniqueConstraints = {
  @UniqueConstraint(columnNames = { Job.PROPERTY_APP_MANAGER_ID },
                    name="unique_appmanagerid")
})
public class Job implements Serializable {

  private static final long serialVersionUID = 8679132523752603653L;

  /* Consistent property name */
  public static final String PROPERTY_APP_MANAGER_ID = "appManagerId";
  public static final String PROPERTY_COMPLETED = "completed";
  public static final String PROPERTY_SIMULATION_ID = "simulationId";

  /** Consistent query naming - Query job by app manager identifier. */
  public static final String QUERY_JOB_BY_APP_MANAGER_ID = "job.queryByAppManagerId";
  /** Consistent query naming - Querying job to see if all jobs for a simulation are completed. */
  public static final String QUERY_JOB_BY_COMPLETED_FOR_SIMULATION = "job.queryByCompletedForSimulation";
  /** Consistent query naming - Query job data by simulation identifier. */
  public static final String QUERY_JOB_DATA_BY_SIMULATION_ID = "job.queryJobDataBySimulationId";

  // Persistence identity (Surrogate primary key)
  @TableGenerator(name="job_id_gen", table="sequence_pks_business_manager",
                  pkColumnName="pk_seq_name", pkColumnValue="job_id",
                  valueColumnName="pk_seq_value", allocationSize=1)
  @Id @GeneratedValue(strategy=GenerationType.TABLE, generator="job_id_gen")
  private Long id;

  // Simulation identifier.
  @org.hibernate.annotations.Index(name="idx_job_simulationid")
  @Column(nullable=false, updatable=false)
  private Long simulationId;

  // App manager identifier (not known until job received by app manager)
  @Column(insertable=false, length=100, nullable=true, updatable=true)
  private String appManagerId;

  // Experimental data pacing frequency (or default-defined values if no experimental data)
  @Column(nullable=false, updatable=false)
  private Float pacingFrequency;

  // Compound concentrations (or configuration-defined values if no experimental data)
  //   comma-separated collection of float values.
  @ElementCollection(fetch=FetchType.LAZY)
  @org.hibernate.annotations.ForeignKey(name="fk_job_cmpdconcsid")
  private Set<Float> compoundConcentrations = new TreeSet<Float>();

  /* More than one Job may refer to a single GroupedInvocationInput as Job is
       per-pacingFrequency and perhaps more than one frequency, e.g. 0.5Hz and
       1.0Hz, share the same GroupedInvocationInput.
     Note: inverseJoinColumns (The foreign key columns of the join table which
                               reference the primary table of the entity that
                               *does not own* the association */
  // bidirectional Job [1..*] <-> [1] GroupedInvocationInput
  @ManyToOne(cascade={ CascadeType.PERSIST, CascadeType.REMOVE },
             fetch=FetchType.LAZY)
  @JoinTable(name="Job_GroupedInvocationInput",
             uniqueConstraints={ @UniqueConstraint(columnNames={ "jc_jobid" },
                                                   name="unique_jobid") },
             joinColumns={ @JoinColumn(name="jc_jobid",
                                       nullable=false) },
             inverseJoinColumns={ @JoinColumn(name="jc_groupedinvocationinputid") })
  private GroupedInvocationInput groupedInvocationInput;

  // HashSet means that order isn't preserved.
  // bidirectional Job [1] <-> [0..*] JobResult
  @OneToMany(cascade={ CascadeType.MERGE, CascadeType.REMOVE },
             fetch=FetchType.LAZY,
             mappedBy="job",
             orphanRemoval=true)
  private Set<JobResult> jobResults = new HashSet<JobResult>();

  @Lob
  @Column(insertable=false, length=65536, nullable=true, updatable=true)
  private String messages;

  /* CSV-format string (potentially a single value) of DeltaAPD90 percentile
     names.
     Percentile names will potentially differ between a Simulation's Jobs as
     some will have been run with uncertainties calculated, others not. */
  @Column(insertable=false, length=5120, nullable=true, updatable=true)
  private String deltaAPD90PercentileNames;

  // info for 64 cores ~= 100k! 
  @Lob
  @Column(insertable=false, length=512000, nullable=true, updatable=true)
  private String info;

  /* Generally seems to be about the 7k mark, but have noticed some control
     compounds generating up to 20k (although in such circumstances other 
     issues (related to inefficiencies in the provenance processing code) are
     likely to result in performance degradation problems!).
     Lookup table downloading progress appears in this field so we end up with a
     few hundred Kb of data so expanding to 500Kb! */
  @Lob
  @Column(insertable=false, length=512000, nullable=true, updatable=true)
  private String output;

  // Whether the job has completed or not.
  // Note : Property queried by SQL, not by getter!
  @Column(nullable=false, updatable=true, name=Job.PROPERTY_COMPLETED)
  private Boolean completed;

  // Whether the job failed or not.
  @Column(nullable=false, updatable=true)
  private Boolean failed;

  /* Indicator to show whether the job was created using configuration-defined
     compound concentrations or whether they've been derived from experimental
     results */
  @Transient
  private boolean usingConfigurationCompoundConcentrations = true;

  @Transient
  private static final Log log = LogFactory.getLog(Job.class);

  /** <b>Do not invoke directly.</b> */
  protected Job() {}

  /**
   * Initialising constructor.
   * 
   * @param simulationId Simulation identifier.
   * @param pacingFrequency Experimental pacing frequency.
   * @param compoundConcentrations Experimental compound concentrations.
   * @param usingConfigurationCompoundConcentrations {@code true} if
   *        configuration-defined compound concentrations being used, otherwise
   *        {@code false}, e.g. from experimental data.
   * @param groupedInvocationInput Grouped invocation input.
   */
  public Job(final Long simulationId, final Float pacingFrequency,
             final Set<Float> compoundConcentrations,
             final boolean usingConfigurationCompoundConcentrations,
             final GroupedInvocationInput groupedInvocationInput) {
    log.debug("~Job() : Invoked.");
    this.simulationId = simulationId;
    this.pacingFrequency = pacingFrequency;
    if (compoundConcentrations != null && !compoundConcentrations.isEmpty()) {
      this.compoundConcentrations.addAll(compoundConcentrations);
    }
    this.usingConfigurationCompoundConcentrations = usingConfigurationCompoundConcentrations;
    this.groupedInvocationInput = groupedInvocationInput;
    this.completed = false;
    this.failed = false;
  }

  /**
   * Add a job to the collection.
   * 
   * @param jobResult Job result to add.
   */
  public void addJobResult(final JobResult jobResult) {
    jobResults.add(jobResult);
  }

  /**
   * Assign the Job as being completed.
   * 
   * Use in preference to {@linkplain #setCompleted(Boolean)}.
   */
  public void assignCompleted() {
    log.debug("~assignCompleted() : Invoked.");
    // Assigning a null value as serves no purpose once job is complete.
    setAppManagerId(null);
    setCompleted(true);
  }

  /**
   * Consider the job to have failed.
   * <p>
   * Note: Once a job is assigned as having failed, it cannot then be reversed.
   * @throws IllegalStateException If job results present.
   */
  public void considerFailed() throws IllegalStateException {
    if (!getJobResults().isEmpty()) {
      // TODO : Accommodate situation where a job failed half-way through.
      throw new IllegalStateException("Cannot consider a job with results to have failed!");
    }

    this.failed = true;
  }

  private boolean hasFailed() {
    return this.failed != null && this.failed;
  }

  /**
   * Indicator if Job is believed to be running, e.g. no results yet and it
   * hasn't failed.
   * 
   * @return {@code true} if job is still running, else {@code false}.
   */
  public boolean isUnfinished() {
    return getJobResults().isEmpty() && !hasFailed();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime
        * result
        + ((compoundConcentrations == null) ? 0 : compoundConcentrations
            .hashCode());
    result = prime
        * result
        + ((groupedInvocationInput == null) ? 0 : groupedInvocationInput
            .hashCode());
    result = prime * result
        + ((pacingFrequency == null) ? 0 : pacingFrequency.hashCode());
    result = prime * result
        + ((simulationId == null) ? 0 : simulationId.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Job other = (Job) obj;
    if (compoundConcentrations == null) {
      if (other.compoundConcentrations != null)
        return false;
    } else if (!compoundConcentrations.equals(other.compoundConcentrations))
      return false;
    if (groupedInvocationInput == null) {
      if (other.groupedInvocationInput != null)
        return false;
    } else if (!groupedInvocationInput.equals(other.groupedInvocationInput))
      return false;
    if (pacingFrequency == null) {
      if (other.pacingFrequency != null)
        return false;
    } else if (!pacingFrequency.equals(other.pacingFrequency))
      return false;
    if (simulationId == null) {
      if (other.simulationId != null)
        return false;
    } else if (!simulationId.equals(other.simulationId))
      return false;
    return true;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Job [id=" + id + ", simulationId=" + simulationId
        + ", appManagerId=" + appManagerId + ", pacingFrequency="
        + pacingFrequency + ", compoundConcentrations=" + compoundConcentrations
        + ", groupedInvocationInput=" + groupedInvocationInput + ", jobResults="
        + jobResults + ", deltaAPD90PercentileNames="
        + deltaAPD90PercentileNames + ", completed=" + completed
        + ", failed=" + failed
        + ", messages=" + (messages != null) + ", info=" + (info != null)
        + ", output=" + (output != null) 
        + ", usingConfigurationCompoundConcentrations="
        + usingConfigurationCompoundConcentrations + "]";
  }

  /**
   * Retrieve the (surrogate primary) key identifier.
   * 
   * @return Job identifier.
   */
  public Long getId() {
    return id;
  }

  /**
   * Retrieve the jobs results.
   * 
   * @return Unmodifiable collection of job results (empty if none available).
   */
  public Set<JobResult> getJobResults() {
    return Collections.unmodifiableSet(jobResults);
  }

  /**
   * @return the appManagerId
   */
  public String getAppManagerId() {
    return appManagerId;
  }

  /**
   * @return the pacingFrequency
   */
  public Float getPacingFrequency() {
    return pacingFrequency;
  }

  /**
   * @return the simulationId
   */
  public Long getSimulationId() {
    return simulationId;
  }

  /**
   * Retrieve the compound concentrations.
   * 
   * @return Unmodifiable collection of compound concentrations (empty if none
   *         available).
   */
  public Set<Float> getCompoundConcentrations() {
    return Collections.unmodifiableSet(compoundConcentrations);
  }

  /**
   * @return the groupedInvocationInput
   */
  public GroupedInvocationInput getGroupedInvocationInput() {
    return groupedInvocationInput;
  }

  /**
   * Assign the app-manager identifier.
   * 
   * @param appManagerId app-manager identifier.
   */
  public void setAppManagerId(final String appManagerId) {
    this.appManagerId = appManagerId;
  }

  /**
   * @return the info
   */
  public String getInfo() {
    return info;
  }

  /**
   * @param info the info to set
   */
  public void setInfo(final String info) {
    this.info = info;
  }

  /**
   * @return the output
   */
  public String getOutput() {
    return output;
  }

  /**
   * Set the job as completed.
   *  
   * @param completed {@code true} if Job is completed, otherwise {@code false}.
   * @see #assignCompleted()
   */
  protected void setCompleted(final Boolean completed) {
    this.completed = completed;
  }

  /**
   * @param output the output to set
   */
  public void setOutput(final String output) {
    this.output = output;
  }

  /**
   * @return the messages
   */
  public String getMessages() {
    return messages;
  }

  /**
   * @param messages the messages to set
   */
  public void setMessages(final String messages) {
    this.messages = messages;
  }

  /**
   * Retrieve the Delta APD90 percentile names (in the form of a CSV string).
   * 
   * @return Delta APD90 percentile names.
   */
  public String getDeltaAPD90PercentileNames() {
    return deltaAPD90PercentileNames;
  }

  /**
   * Assign the Delta APD90 percentile names.
   * 
   * @param deltaAPD90PercentileNames Delta APD90 percentile names to set.
   */
  public void setDeltaAPD90PercentileNames(final String deltaAPD90PercentileNames) {
    this.deltaAPD90PercentileNames = deltaAPD90PercentileNames;
  }

  /**
   * Flag to indicate if using configuration-defined compound concentrations or
   * not.
   * 
   * @return {@code true} if using configuration-defined compound concentrations,
   *         otherwise {@code false}.
   */
  public boolean isUsingConfigurationCompoundConcentrations() {
    return usingConfigurationCompoundConcentrations;
  }
}