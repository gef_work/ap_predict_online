/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object.workflow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.InputValueSource;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 * Holding object for <i>Screening</i> and <i>QSAR</i> data processing {@link InputValueSource}(s)
 * for a particular <i>Assay</i> and {@link IonChannel}. Input value sources generally are at least
 * the pIC50 value (or values) and potentially also a Hill Coefficient, but the implementations have
 * other properties. 
 * <p>
 * The populated object may contain just one <tt>InputValueSource</tt> for an <i>Assay</i> and <i>Ion Channel</i>
 * combination if there was, for example, an equality modifier in a summary data record. However it
 * may alternatively contain more than one <tt>InputValueSource</tt>, each derived from a number of individual
 * data records for a compound's <i>Assay</i>/<tt>IonChannel</tt>.
 *
 * @author geoff
 */
public class OutcomeVO implements Serializable {

  private static final long serialVersionUID = -7670797506053161922L;

  private final AssayVO assay;
  private final IonChannel ionChannel;
  private final List<InputValueSource> inputValueSources = new ArrayList<InputValueSource>();

  private static final Log log = LogFactory.getLog(OutcomeVO.class);

  /**
   * Initialising constructor.
   *  
   * @param assay Non-null assay.
   * @param ionChannel Non-null ion channel.
   * @param inputValueSources Workflow inputValueSources.
   * @throws IllegalArgumentException If either the assay or ion channel are not provided.
   */
  public OutcomeVO(final AssayVO assay, final IonChannel ionChannel, final List<InputValueSource> inputValueSources) {
    log.debug("~OutComeVO() : Invoked.");
    if (assay == null) {
      throw new IllegalArgumentException("An assay value must be provided when creating an Outcome value object.");
    }
    if (ionChannel == null) {
      throw new IllegalArgumentException("An ion channel value must be provided when creating an Outcome value object.");
    }
    this.assay = assay;
    this.ionChannel = ionChannel;
    if (inputValueSources != null && !inputValueSources.isEmpty()) {
      this.inputValueSources.addAll(inputValueSources);
    }
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "OutcomeVO [assay=" + assay + ", ionChannel=" + ionChannel
        + ", inputValueSources=" + inputValueSources + "]";
  }

  /**
   * Retrieve the simulation input value sources.
   * <p>
   * These will be a collection of objects containing pIC50 (and perhaps Hill Coefficient and other
   * properties) values for the <i>Assay</i> and <i>Ion Channel</i>.
   * 
   * @return Unmodifiable collection of simulation input value sources (or empty collection if none
   *         available).
   */
  public List<InputValueSource> getInputValueSources() {
    return Collections.unmodifiableList(inputValueSources);
  }

  /**
   * Retrieve the simulation assay.
   * 
   * @return Simulation assay.
   */
  public AssayVO getAssay() {
    return assay;
  }

  /**
   * Retrieve the simulation ion channel.
   * 
   * @return Simulation ion channel.
   */
  public IonChannel getIonChannel() {
    return ionChannel;
  }
}