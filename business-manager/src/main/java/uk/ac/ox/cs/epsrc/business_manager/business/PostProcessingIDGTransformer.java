/*

  Copyright (c) 2016, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.integration.annotation.Header;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.entity.GroupedInvocationInput;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedDataVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata.ProcessedSiteDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;

/**
 * Transformer as part of the input data gathering pipeline.
 *
 * @author geoff
 */
// See integration/simulation/appCtx.sim.processSimulation.xml
public class PostProcessingIDGTransformer {

  private static final Log log = LogFactory.getLog(PostProcessingIDGTransformer.class);

  /**
   * As part of input data gathering, if there was no input data (for example if there were only
   * estimated inhibitory concentration values and alerting requested instead), then create an
   * empty {@linkplain ProcessedDataVO} to return to the client.
   * 
   * @param processingType Input data gathering or not.
   * @param simulationId Simulation identifier.
   * @param processedSiteData Processed site data (should be void of site data!).
   * @return Empty processed site data value object.
   * @throws IllegalArgumentException If not input data gathering or {@code null} site data or there's site data available.
   */
  public ProcessedDataVO transformToEmptyProcessed(final @Header(value=BusinessIdentifiers.SI_HDR_PROCESSING_TYPE,
                                                                 required=true)
                                                         ProcessingType processingType,
                                                   final @Header(value=BusinessIdentifiers.SI_HDR_SIMULATION_ID,
                                                                 required=true)
                                                         Long simulationId,
                                                   final ProcessedSiteDataVO processedSiteData)
                                                   throws IllegalArgumentException {
    log.debug("~transformToEmptyProcessed() : Invoked.");

    if (processedSiteData == null || processedSiteData.isContainingSiteData()) {
      final String errorMessage = "Shouldn't be here if there's no site data object or it contains site data!";
      log.error("~transformToEmptyProcessed() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }
    if (!ProcessingType.isInputDataGathering(processingType)) {
      final String errorMessage = "Not input data gathering, but ended up in an input data gathering pipeline!";
      log.error("~transformToEmptyProcessed() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    log.debug("~transformToEmptyProcessed() : Creating empty object for input data gathering.");
    final Map<BigDecimal, Set<BigDecimal>> perFrequencyConcentrations = new HashMap<BigDecimal, Set<BigDecimal>>();
    final Set<GroupedInvocationInput> processedData = new HashSet<GroupedInvocationInput>();

    return new ProcessedDataVO(perFrequencyConcentrations, processedData);
  }
}