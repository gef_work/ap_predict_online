/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.selection.c50;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ICDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.IndividualProcessing;

/**
 * Process C50 data by passing the {@link ICData} collections retrieved from individual or summary data
 * records to the {@link C50DataSelector}.
 *
 * @author geoff
 */
public class C50DataProcessor {

  private static final Log log = LogFactory.getLog(C50DataProcessor.class);

  /**
   * The C50 data will be chosen based on the strategies defined by the {@link C50DataSelector}.
   * 
   * @param individualProcessing Individual processing data.
   * @return Chosen C50 data.
   * @throws InvalidValueException If an invalid value encountered.
   * @throws IllegalArgumentException If null individual processing arg provided.
   * @throws InvalidValueException If invalid value encountered during processing.
   */
  public ICData individualDataPC50(final IndividualProcessing individualProcessing)
                                    throws IllegalArgumentException, InvalidValueException {
    if (individualProcessing == null) {
      throw new IllegalArgumentException("Individual processing data must be provided to extract individual data C50 data.");
    }
    log.debug("~individualDataPC50() : Invoked.");
    final ICDataHolder icDataHolder = (ICDataHolder) individualProcessing.getIndividualData();
    return processC50Data(icDataHolder);
  }

  /*
   * Process C50 Data which all ICDataHolder implementing classes may hold.
   */
  protected ICData processC50Data(final ICDataHolder icDataHolder) throws InvalidValueException {
    log.debug("~processC50Data() : Invoked.");
    ICData icData = null;
    if (icDataHolder != null) {
      icData = C50DataSelector.selectFrom(icDataHolder.eligibleICData());
    }
    return icData;
  }

  /**
   * The C50 data will be chosen based on the strategies defined by the {@link C50DataSelector}.
   * 
   * @param siteDataHolder Site data.
   * @return Chosen C50 data.
   * @throws IllegalArgumentException If null site data holder arg provided.
   * @throws InvalidValueException If invalid value encountered during processing.
   */
  public ICData summaryDataPC50(final SiteDataHolder siteDataHolder) 
                                 throws IllegalArgumentException, InvalidValueException {
    if (siteDataHolder == null) {
      throw new IllegalArgumentException("Site data must be provided to extract summary data C50 data.");
    }
    log.debug("~summaryDataPC50() : Invoked.");
    final ICDataHolder icDataHolder = (ICDataHolder) siteDataHolder.getSummaryDataRecord(false);
    return processC50Data(icDataHolder);
  }
}