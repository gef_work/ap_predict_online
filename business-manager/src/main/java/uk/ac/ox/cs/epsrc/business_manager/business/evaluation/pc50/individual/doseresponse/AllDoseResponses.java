/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual.doseresponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import uk.ac.ox.cs.epsrc.business_manager.business.DoseResponseTransformer;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.IndividualProvenance;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.DoseResponseData;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingParams;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.IndividualProcessing;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Dose-response fitting strategy which involves using all dose-response data.
 *
 * @author geoff
 */
public class AllDoseResponses extends AbstractDoseResponseFittingStrategy {

  private static final long serialVersionUID = -5924635478756548934L;

  // Brief, informative description of fitting nature appearing in various output.
  private static final String fittingNature = "All point fitting ";
  // Used in other parts of the application.
  private static final String sysProvenanceStrategySuffix = "all";

  private static final Log log = LogFactory.getLog(AllDoseResponses.class);

  /**
   * Initialising constructor.
   * 
   * @param description Description of the fitting strategy.
   * @param defaultActive Indicator of whether the strategy should be active by default.
   * @param defaultInvocationOrder Default strategy's invocation order.
   */
  @Autowired(required=true)
  public AllDoseResponses(final String description, final boolean defaultActive,
                          final int defaultInvocationOrder) {
    super(description, defaultActive, defaultInvocationOrder);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual.doseresponse.AbstractDoseResponseFittingStrategy#retrieveDoseResponseData(long, uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.IndividualProcessing, uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData, java.lang.String, uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingParams, java.lang.String, boolean)
   */
  @Override
  protected DoseResponseData retrieveDoseResponseData(final long simulationId,
                                                      final IndividualProcessing individualProcessing,
                                                      final ICData individualC50Data,
                                                      final String recordIdentifier,
                                                      final DoseResponseFittingParams doseResponseFittingParams,
                                                      final String individualDataId,
                                                      final boolean provenanceable)
                                                      throws InvalidValueException {
    final String logPrefix = "~retrieveDoseResponseData() : " + recordIdentifier + " : ";
    log.debug(logPrefix.concat("Invoked [").concat(retrieveFittingNature()).concat("]"));

    // If there's no data available then send provenance (if appropriate) and exit.
    if (!individualProcessing.hasDoseResponseData()) {
      final String debugMessage = "No collection of dose-response records available!";
      if (provenanceable) {
        getJmsTemplate().sendProvenance(new IndividualProvenance.Builder(InformationLevel.TRACE,
                                                                         debugMessage, simulationId)
                                                                .individualDataId(individualDataId)
                                                                .build());
      }
      log.debug(logPrefix + debugMessage);
      return null;
    }

    // Retrieve all dose-response data from the individual/dose-response data.
    return DoseResponseTransformer.allDoseResponseTransform(individualProcessing);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual.doseresponse.AbstractDoseResponseFittingStrategy#retrieveFittingNature()
   */
  @Override
  protected String retrieveFittingNature() {
    return fittingNature;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual.doseresponse.AbstractDoseResponseFittingStrategy#retrieveSysProvenanceSuffix()
   */
  @Override
  protected String retrieveSysProvenanceSuffix() {
    return sysProvenanceStrategySuffix;
  }
}