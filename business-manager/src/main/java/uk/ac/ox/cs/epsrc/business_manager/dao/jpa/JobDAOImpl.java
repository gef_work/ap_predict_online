/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.dao.jpa;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.dao.JobDAO;
import uk.ac.ox.cs.epsrc.business_manager.entity.Job;

/**
 * Implementation of the Job DAO.
 *
 * @author geoff
 */
@Repository(BusinessIdentifiers.COMPONENT_JOB_DAO)
public class JobDAOImpl implements JobDAO {

  @PersistenceContext
  private EntityManager entityManager;

  private static final Log log = LogFactory.getLog(JobDAOImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.JobDAO#assignAppManagerIdToJob(long, java.lang.String)
   */
  @Override
  public void assignAppManagerIdToJob(final long jobId, final String appManagerId) {
    log.debug("~assignAppManagerIdToJob() : Invoked for job id '" + jobId + "', appManagerId '" + appManagerId + "'.");
    final Job job = entityManager.find(Job.class, jobId);
    if (job != null) {
      job.setAppManagerId(appManagerId);
      entityManager.merge(job);
    } else {
      final String errorMessage = "Tried to update Job with id '" + jobId + "' with app manager id of '" + appManagerId + "', but Job does not exist!";
      log.error("~assignAppManagerIdToJob() : " + errorMessage);
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.JobDAO#findByJobId(long)
   */
  @Override
  public Job findByJobId(final long jobId) {
    log.debug("~findByJobId() : Invoked for job with id '" + jobId + "'.");

    return entityManager.find(Job.class, jobId);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.JobDAO#findJobByAppManagerId(java.lang.String)
   */
  @Override
  public Job findJobByAppManagerId(final String appManagerId) {
    log.debug("~findJobByAppManagerId() : Invoked.");
    final Query query = entityManager.createNamedQuery(Job.QUERY_JOB_BY_APP_MANAGER_ID);
    query.setParameter(Job.PROPERTY_APP_MANAGER_ID, appManagerId);

    Job job = null;
    try {
      job = (Job) query.getSingleResult();
    } catch (NoResultException e) {
      log.debug("~findJobByAppManagerId() : No Job exists for app manager id '" + appManagerId + "'.");
    }

    return job;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.JobDAO#findJobsBySimulationId(long)
   */
  @SuppressWarnings("unchecked")
  @Override
  public List<Job> findJobsBySimulationId(final long simulationId) {
    log.debug("~findJobsBySimulationId() : Invoked for simulation id '" + simulationId + "'.");

    final Query query = entityManager.createNamedQuery(Job.QUERY_JOB_DATA_BY_SIMULATION_ID);
    query.setParameter(Job.PROPERTY_SIMULATION_ID, simulationId);

    final List<Job> results = (List<Job>) query.getResultList();
    // https://stackoverflow.com/questions/1115480/can-javax-persistence-query-getresultlist-return-null
    return results == null ? new ArrayList<Job>() : results;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.JobDAO#incompleteJobsForSimulationCount(long)
   */
  @Override
  public long incompleteJobsForSimulationCount(final long simulationId) {
    log.debug("~incompleteJobsForSimulationCount() : Invoked for simulation id '" + simulationId + "'.");

    final Query query = entityManager.createNamedQuery(Job.QUERY_JOB_BY_COMPLETED_FOR_SIMULATION);
    query.setParameter(Job.PROPERTY_SIMULATION_ID, simulationId);

    final Long incompleteCount = (Long) query.getSingleResult();
    log.debug("~incompleteJobsForSimulationCount() : Incomplete count of '" + incompleteCount + "' for simulation '" + simulationId + "'.");

    return incompleteCount.longValue();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.JobDAO#removeJobs(java.util.List)
   */
  @Override
  public boolean removeJobs(final List<Job> jobs) {
    log.debug("~removeJobs() : Invoked.");

    if (jobs != null) {
      for (final Job job : jobs) {
        entityManager.remove(job);
      }
    }
    final int removedCount = jobs == null ? 0 : jobs.size();

    log.debug("~removeJobs() : '" + removedCount + "' jobs were removed.");

    return (removedCount > 0);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.JobDAO#save(uk.ac.ox.cs.epsrc.business_manager.entity.Job)
   */
  @Override
  public void save(final Job job) {
    if (job.getId() != null) {
      log.debug("~save() : Going to merge.");
      entityManager.merge(job);
    } else {
      log.debug("~save() : Going to persist.");
      entityManager.persist(job);
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.JobDAO#storeJobResults(uk.ac.ox.cs.epsrc.business_manager.entity.Job, boolean)
   */
  @Override
  public void storeJobResults(final Job job, final boolean assignCompleted) {
    log.debug("~storeJobResults() : Invoked.");

    if (assignCompleted) {
      job.assignCompleted();
    }

    entityManager.merge(job);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.epsrc.business_manager.dao.JobDAO#storeJobs(java.util.Set)
   */
  @Override
  public void storeJobs(final Set<Job> jobs) {
    log.debug("~storeJobs() : Invoked.");

    for (final Job job : jobs) {
      entityManager.persist(job);
    }
  }
}