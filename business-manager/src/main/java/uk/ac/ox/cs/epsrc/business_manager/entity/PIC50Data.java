/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * pIC50 and pIC50-related, e.g. Hill Coefficient, ion channel data.
 *
 * @author geoff
 */
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy.READ_ONLY,
                                 region = "uk.ac.ox.cs.epsrc.business_manager.entity.PIC50Data")

@Entity
public class PIC50Data implements Cloneable, Serializable {

  public static final int DEFAULT_SCALE = 10;
  public static final RoundingMode DEFAULT_ROUNDING_MODE = RoundingMode.HALF_EVEN;

  private static final long serialVersionUID = 594888267720645031L;

  // Persistence identity (Surrogate primary key)
  @TableGenerator(name="pic50data_id_gen", table="sequence_pks_business_manager",
                  pkColumnName="pk_seq_name", pkColumnValue="pic50data_id",
                  valueColumnName="pk_seq_value", allocationSize=20)
  @Id @GeneratedValue(strategy=GenerationType.TABLE, generator="pic50data_id_gen")
  private Long id;

  // pIC50 value
  @Column(nullable=false, updatable=false, precision=19, scale=DEFAULT_SCALE)
  private BigDecimal value;

  // hill coefficient
  @Column(nullable=true, updatable=false, precision=19, scale=DEFAULT_SCALE)
  private BigDecimal hill;

  // indicator as to whether the pIC50 and hill are the original, raw values
  @Column(nullable=true, updatable=false)
  private Boolean original;

  // Default invocation order of the pIC50 evaluation strategy which generated this pIC50 (if 
  // applicable, e.g. not for QSAR data, but usually for screening data).
  @Column(nullable=true, updatable=false)
  private Integer strategyOrder;

  // bidirectional PIC50Data [0..*] <-> [1] IonChannelValues
  @ManyToOne
  @JoinColumn(name="jc_ionchannelvaluesid", updatable=false)
  @org.hibernate.annotations.ForeignKey(name="fk_pic50data_ionchvalsid")
  private IonChannelValues ionChannelValues;

  @Transient
  private static final Log log = LogFactory.getLog(PIC50Data.class);

  /** <b>Do not invoke directly.</b> */
  protected PIC50Data() {}

  /**
   * Initialising constructor.
   * 
   * @param value pIC50 value.
   * @param hill Hill coefficient.
   * @param original {@code true} if the pIC50 and Hill values are the original raw data values.
   * @param strategyOrder pIC50 evaluation strategy default invocation order (or {@code null} if
   *                      not applicable, e.g. QSAR data).
   * @throws IllegalArgumentException If a {@code null} pIC50 parameter value.
   */
  public PIC50Data(final BigDecimal value, final BigDecimal hill,
                   final boolean original, final Integer strategyOrder)
                   throws IllegalArgumentException {
    if (value == null) {
      final String errorMessage = "Illegal attempt to assign a null pIC50 value to PIC50Data";
      log.error("PIC50Data() : ".concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    this.value = value;
    this.hill = hill;
    this.original = original;
    this.strategyOrder = strategyOrder;
  }

  /**
   * This cloning process does not copy the persistence identifiers of objects, just the "business"
   * data.
   * 
   * @return Cloned object.
   */
  @Override
  protected PIC50Data clone() throws CloneNotSupportedException {
    return new PIC50Data(value, hill, original, strategyOrder);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((hill == null) ? 0 : hill.hashCode());
    result = prime * result + ((original == null) ? 0 : original.hashCode());
    result = prime * result
        + ((strategyOrder == null) ? 0 : strategyOrder.hashCode());
    result = prime * result + ((value == null) ? 0 : value.hashCode());
    return result;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    PIC50Data other = (PIC50Data) obj;
    if (hill == null) {
      if (other.hill != null)
        return false;
    } else if (hill.setScale(DEFAULT_SCALE, DEFAULT_ROUNDING_MODE).compareTo
              (other.hill.setScale(DEFAULT_SCALE, DEFAULT_ROUNDING_MODE)) != 0)           // Note: compareTo(), not equals()!
      return false;
    if (original == null) {
      if (other.original != null)
        return false;
    } else if (!original.equals(other.original))
      return false;
    if (strategyOrder == null) {
      if (other.strategyOrder != null)
        return false;
    } else if (!strategyOrder.equals(other.strategyOrder))
      return false;
    if (value == null) {
      if (other.value != null)
        return false;
    } else if (value.setScale(DEFAULT_SCALE, DEFAULT_ROUNDING_MODE).compareTo
              (other.value.setScale(DEFAULT_SCALE, DEFAULT_ROUNDING_MODE)) != 0)         // Note: compareTo(), not equals()!
      return false;
    return true;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "PIC50Data [id=" + id + ", value=" + value + ", hill=" + hill
        + ", original=" + original + ", strategyOrder=" + strategyOrder + "]";
  }

  /**
   * Retrieve the pIC50 value.
   * 
   * @return Non-{@code null} pIC50 value.
   */
  public BigDecimal getValue() {
    return value;
  }

  /**
   * Retrieve the Hill coefficient value.
   * 
   * @return Hill coefficient value (possibly {@code null}).
   */
  public BigDecimal getHill() {
    return hill;
  }

  /**
   * Retrieve the default invocation order of the pIC50 evaluation strategy which generated this 
   * pIC50.
   * 
   * @return pIC50-generating strategy if screening data, otherwise {@code null}.
   */
  public Integer getStrategyOrder() {
    return strategyOrder;
  }

  /**
   * Indicator as to whether the pIC50 and Hill are original (rather than estimated).
   * 
   * @return {@code true} if using the original, raw values, otherwise {@code false}.
   */
  public boolean getOriginal() {
    return original != null && original == true ? true : false;
  }

  /**
   * @param ionChannelValues the ionChannelValues to set
   */
  public void setIonChannelValues(final IonChannelValues ionChannelValues) {
    this.ionChannelValues = ionChannelValues;
  }
}