/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.annotation.Header;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.JobProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.entity.GroupedInvocationInput;
import uk.ac.ox.cs.epsrc.business_manager.entity.Job;
import uk.ac.ox.cs.epsrc.business_manager.manager.JobManager;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;

/**
 * Service activator performing the job creation task.
 *
 * @author geoff
 */
public class JobCreatingServiceActivator {

  // JMS Template for sending provenance details.
  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JMS_TEMPLATE)
  private BusinessManagerJMSTemplate jmsTemplate;

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_JOB_MANAGER)
  private JobManager jobManager;

  private static final Log log = LogFactory.getLog(JobCreatingServiceActivator.class);

  /**
   * Create the jobs to invoke from the processed data.
   * 
   * @param processingType Processing type.
   * @param simulationId Simulation identifier.
   * @param processedData Processed (grouped, inherited) data.
   * @return Collection of jobs.
   */
  public Set<Job> createJobs(final @Header(value=BusinessIdentifiers.SI_HDR_PROCESSING_TYPE,
                                           required=true)
                                   ProcessingType processingType,
                             final @Header(value=BusinessIdentifiers.SI_HDR_SIMULATION_ID,
                                           required=true)
                                   Long simulationId,
                             final ProcessedDataVO processedData) {
    log.debug("~createJobs() : Invoked.");

    final Set<Job> jobs = jobManager.createAndStore(simulationId, processedData);

    for (final Job job : jobs) {
      final Long jobId = job.getId();
      final GroupedInvocationInput groupedInvocationInput = job.getGroupedInvocationInput();
      final String assayGroupName = groupedInvocationInput.getAssayGroupName();
      final Short assayGroupLevel = groupedInvocationInput.getAssayGroupLevel();
      final Float pacingFrequency = job.getPacingFrequency();

      jmsTemplate.sendProgress(new OverallProgress.Builder(InformationLevel.TRACE,
                                                           MessageKey.JOB_DATA_CREATION.getBundleIdentifier(),
                                                           new String[] { assayGroupName,
                                                                          pacingFrequency.toString(),
                                                                          jobId.toString() },
                                                           simulationId).build());

      jmsTemplate.sendProgress(new JobProgress.Builder(InformationLevel.TRACE,
                                                       MessageKey.JOB_CREATED.getBundleIdentifier(),
                                                       new String[] { assayGroupName,
                                                                      pacingFrequency.toString() },
                                                       simulationId, jobId)
                                              .onCreation(assayGroupName, assayGroupLevel,
                                                          pacingFrequency).build());
    }

    return jobs;
  }
}