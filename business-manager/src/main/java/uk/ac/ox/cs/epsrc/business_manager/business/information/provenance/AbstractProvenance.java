/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.information.provenance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Abstracted provenance object.
 *
 * @author geoff
 */
public abstract class AbstractProvenance implements Provenance {

  private static final long serialVersionUID = 5207914333017752777L;

  private final InformationLevel level;
  private final String text;
  private final List<String> args = new ArrayList<String>();
  private final Long simulationId;

  public static class Builder {
    // Required parameters
    private final InformationLevel level;
    private final String text;
    private final List<String> args = new ArrayList<String>();
    private final Long simulationId;

    /**
     * Initialising constructor with minimum data.
     * 
     * @param level Information level.
     * @param text Provenance text.
     * @param simulationId Simulation identifier.
     * @throws IllegalArgumentException If {@code null} object received for {@link #level} or
     *                                  {@link #simulationId}, or if {@link #text} is blank.
     */
    public Builder(final InformationLevel level, final String text, final Long simulationId)
                   throws IllegalArgumentException {
      if (level == null || StringUtils.isBlank(text) || simulationId == null) {
        final String errorMessage = "Cannot build a provenance object with a null level or simulationId, nor with blank text.";
        throw new IllegalArgumentException(errorMessage);
      }
      this.level = level;
      this.text = text;
      this.simulationId = simulationId;
    }

    /**
     * Initialising constructor with minimum data.
     * 
     * @param level Information level.
     * @param text Provenance text.
     * @param args Provenance args.
     * @param simulationId Simulation identifier.
     * @throws IllegalArgumentException See {@code #Builder(InformationLevel, String, Long)}.
     */
    public Builder(final InformationLevel level, final String text, final String[] args,
                   final Long simulationId) throws IllegalArgumentException {
      this(level, text, simulationId);
      if (args != null && args.length > 0) {
        this.args.addAll(Arrays.asList(args));
      }
    }
  }

  protected AbstractProvenance(final Builder builder) {
    this.level = builder.level;
    this.text = builder.text;
    this.args.addAll(builder.args);
    this.simulationId = builder.simulationId;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "AbstractProvenance [level=" + level + ", text=" + text + ", args="
        + args + ", toString()=" + super.toString() + "]";
  }

  /**
   * Retrieve the information detail level.
   * 
   * @return The {@link InformationLevel} detail level.
   */
  public InformationLevel getLevel() {
    return level;
  }

  /**
   * Retrieve the provenance text.
   * 
   * @return Provenance text.
   */
  public String getText() {
    return text;
  }

  /**
   * Retrieve provenance text arguments.
   * 
   * @return Unmodifiable collection of arguments to provenance text if assigned, otherwise empty
   *         collection.
   */
  public List<String> getArgs() {
    return Collections.unmodifiableList(args);
  }

  /**
   * Retrieve the simulation identifier.
   * 
   * @return The simulation identifier.
   */
  public Long getSimulationId() {
    return simulationId;
  }
}