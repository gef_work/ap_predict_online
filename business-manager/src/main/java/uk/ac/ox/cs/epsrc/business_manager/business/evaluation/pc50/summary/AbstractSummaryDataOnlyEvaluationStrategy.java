/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.summary;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.Ordered;

import uk.ac.ox.cs.epsrc.business_manager.business.selection.c50.C50DataSelector;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.AbstractPC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.summary.SummaryOnlyC50DataStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;

/**
 * Abstracted summary data only PC50 evaluation strategy.
 *
 * @author geoff
 */
public abstract class AbstractSummaryDataOnlyEvaluationStrategy extends AbstractPC50EvaluationStrategy
                                                                implements Ordered,
                                                                           SummaryOnlyC50DataStrategy {

  private static final long serialVersionUID = 2346379133259620732L;

  private static final Log log = LogFactory.getLog(AbstractSummaryDataOnlyEvaluationStrategy.class);

  /**
   * Initialising constructor.
   * 
   * @param description Description of the PC50 evaluation strategy.
   * @param defaultActive Indicator of whether the strategy should be active by default.
   * @param defaultInvocationOrder Default strategy invocation order.
   */
  protected AbstractSummaryDataOnlyEvaluationStrategy(final String description,
                                                      final boolean defaultActive,
                                                      final int defaultInvocationOrder) {
    super(description, defaultActive, defaultInvocationOrder);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.summary.SummaryOnlyC50DataStrategy#evaluatePC50(uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder, java.lang.String)
   */
  @Override
  public BigDecimal evaluatePC50(final SiteDataHolder siteDataHolder,
                                 final String recordIdentifier) throws IllegalArgumentException,
                                                                       InvalidValueException {
    final String identifyingLogPrefix = "~evaluatePC50() : [" + recordIdentifier + "] : ";
    // Verify the input args
    if (siteDataHolder == null) {
      final String errorMessage = "Evaluating summary PC50 when no individual data requires a site data holder.";
      log.error(identifyingLogPrefix.concat(errorMessage));
      throw new IllegalArgumentException(errorMessage);
    }

    BigDecimal evaluatedPC50 = null;
    final SummaryDataRecord summaryDataOnlyRecord = siteDataHolder.getSummaryDataRecord(true);
    // Only interested if summary data and no individual data
    if (summaryDataOnlyRecord != null) {
      log.debug(identifyingLogPrefix.concat("Summary record and no individual data."));
      final ICData summaryOnlyICData = C50DataSelector.selectFrom(summaryDataOnlyRecord.eligibleICData());
      // Need a summary data ICData
      if (summaryOnlyICData != null) {
        log.debug(identifyingLogPrefix.concat("Summary record has C50 data."));
        evaluatedPC50 = retrievePC50ByStrategy(summaryOnlyICData, recordIdentifier);
      } else {
        log.debug(identifyingLogPrefix.concat("Summary record has no C50 cata."));
      }
    } else {
      log.debug(identifyingLogPrefix.concat("Either 1) No summary data record, or 2) Summary data record with individual data."));
    }

    return evaluatedPC50;
  }

  /**
   * Retrieve the PC50 value from the {@link ICData} based on the evaluation strategy.
   * <p>
   * It is assumed that all arguments have been assigned appropriately, i.e. <b>Only</b> invoke
   * from {@link #evaluatePC50(SiteDataHolder, String)}.
   * 
   * @param summaryICData Summary record C50 data.
   * @param recordIdentifier Record identifier.
   * @return Evaluated PC50 value, or null of not determined.
   */
  protected abstract BigDecimal retrievePC50ByStrategy(ICData summaryICData, String recordIdentifier);
}