/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Object class entity.
 *
 * @author geoff
 */
//second-level ehcache : see business_manager-ehcache.xml + business_manager-persistence.xml
/*
 * NONSTRICT_READ_WRITE :
 * Caches data that is sometimes updated without ever locking the cache.
 * If concurrent access to an item is possible, this concurrency strategy makes no guarantee that
 * the item returned from the cache is the latest version available in the database. Configure your
 * cache timeout accordingly!
 */
@org.hibernate.annotations.Cache(usage = org.hibernate.annotations.CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)

@NamedQueries({
  @NamedQuery(name = ObjectClass.QUERY_OBJECTCLASS_BY_CLASSNAME,
              query = "SELECT objectClass FROM ObjectClass AS objectClass" +
                      "  WHERE objectClass.className = :className",
              hints = {@QueryHint(name = "org.hibernate.cacheable", value="true"),
                       @QueryHint(name = "org.hibernate.cacheRegion",
                                  value="uk.ac.ox.cs.epsrc.business_manager.entity.ObjectClass")})
})
@Entity
public class ObjectClass implements Serializable {

  private static final long serialVersionUID = 3192561299303485132L;

  /** Consistent query naming - Select ObjectClass by class name. */
  public static final String QUERY_OBJECTCLASS_BY_CLASSNAME = "objectClass.queryByClassName";

  @TableGenerator(name="object_class_id_gen", table="sequence_pks_business_manager",
                  pkColumnName="pk_seq_name", pkColumnValue="object_class_id",
                  valueColumnName="pk_seq_value", allocationSize=1)
  @Id @GeneratedValue(strategy=GenerationType.TABLE, generator="object_class_id_gen")
  private Long objectClassId;

  @Column(length=200, nullable=false, unique=true, updatable=false)
  private String className;

  @Transient
  private static final Log log = LogFactory.getLog(ObjectClass.class);

  /** Default constructor - do not use. */
  protected ObjectClass() {}

  /**
   * Initialising constructor.
   * 
   * @param className Class name.
   */
  public ObjectClass(final String className) {
    if (className == null) {
      throw new NullPointerException("Invalid attempt to construct an ObjectClass object with a null className parameter.");
    }
    this.className = className;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "ObjectClass [objectClassId=" + objectClassId + ", className="
        + className + "]";
  }

  /**
   * @return the className
   */
  public String getClassName() {
    return className;
  }

  /**
   * @return the objectClassId
   */
  public Long getObjectClassId() {
    return objectClassId;
  }
}