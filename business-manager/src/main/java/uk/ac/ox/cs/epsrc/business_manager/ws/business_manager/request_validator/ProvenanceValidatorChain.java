/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.business_manager.request_validator;

import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.epsrc.business_manager.ws.business_manager.ProvenanceRequestProcessor.REQUEST_BY;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProvenanceRequest;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidSimulationIdException;
import uk.ac.ox.cs.nc3rs.business_manager.api.request.validator.CompoundIdentifiersValidator;
import uk.ac.ox.cs.nc3rs.business_manager.api.request.validator.SimulationIdsValidator;

/**
 *
 *
 * @author geoff
 */
@Component
public class ProvenanceValidatorChain {

  // Find all Spring-configured CompoundIdentifiersValidator objects and inject them.
  @Autowired
  private List<CompoundIdentifiersValidator> compoundIdentifiersValidators;

  // Find all Spring-configured SimulationIdsValidator objects and inject them.
  @Autowired
  private List<SimulationIdsValidator> simulationIdsValidators;

  private static final Log log = LogFactory.getLog(ProvenanceValidatorChain.class);

  @PostConstruct
  private void initialiseChainSort() {
    Collections.sort(compoundIdentifiersValidators, AnnotationAwareOrderComparator.INSTANCE);
    Collections.sort(simulationIdsValidators, AnnotationAwareOrderComparator.INSTANCE);
  }

  /**
   * Ordered compound name validation of the provenance request.
   * 
   * @param provenanceRequest Incoming provenance request.
   * @param requestBy Identification means by which progress is being requested.
   * @throws InvalidSimulationIdException If the request was invalid by virtue of invalid simulation
   *                                      id(s).
   */
  public void provenanceValidation(final ProvenanceRequest provenanceRequest,
                                   final REQUEST_BY requestBy)
                                   throws InvalidSimulationIdException {
    log.debug("~provenanceValidation() : Invoked.");
    assert(provenanceRequest != null) : "Invalid attempt to invoke process provenance request validation using a null request object.";

    switch (requestBy) {
      case SIMULATION_ID :
        if (simulationIdsValidators != null) {
          for (final SimulationIdsValidator simulationIdsValidator : simulationIdsValidators) {
            simulationIdsValidator.validate(provenanceRequest);
          }
        }
        break;
      default :
        final String error = "Unrecognised provenance request by '" + requestBy + "'.";
        log.error("~provenanceValidation() : " + error);
        break;
    }
  }
}