<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           xmlns:tns="http://www.cs.ox.ac.uk/epsrc/business_manager/ws/1"
           targetNamespace="http://www.cs.ox.ac.uk/epsrc/business_manager/ws/1"
           elementFormDefault="qualified">
  <xs:annotation>
    <xs:documentation xml:lang="en">
      <![CDATA[
      Web Services which a business manager understands.
      Note : Any changes here result in name changes to :
              1. Internal JAXB classes,
              2. The web service WSDL (and hence calling client interfaces!!) - see spring context
                 <int-ws:inbound-gateway> and associated <oxm:jaxb2-marshaller> elements.
             The JAXB classes are generated at build time (maven-jaxb2-plugin) and are 
              referenced in some of the spring web service application contexts, which will likely
              need to be updated.
      ]]>
    </xs:documentation>
  </xs:annotation>

  <xs:element name="DefConAssayStructure">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Assay structure
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="Name" type="xs:string" />
        <xs:element name="Level" type="xs:short" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="DefConAssayGroupStructure">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Default assay grouping structure
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="Name" type="xs:string" />
        <xs:element name="Level" type="xs:short" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="DefConCellModelStructure">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Default CellML model structure.
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="Identifier" type="xs:short" />
        <xs:element name="Name" type="xs:string" />
        <xs:element name="Description" type="xs:string" />
        <xs:element name="CellMLURL" type="xs:string" />
        <xs:element name="PaperURL" type="xs:string" />
        <xs:element name="Default" type="xs:boolean" />
        <xs:element name="PacingMaxTime" type="xs:decimal" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="DefConIonChannelStructure">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Default ion channel structure
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="Name" type="xs:string" />
        <xs:element name="Description" type="xs:string" />
        <xs:element name="DisplayOrder" type="xs:short" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="DefConPC50EvaluationStrategyStructure">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Structure for responses to request for strategies for PC50 evaluation.
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="Id" type="xs:long" />
        <xs:element name="Description" type="xs:string" />
        <xs:element name="DefaultInvocationOrder" type="xs:int" />
        <xs:element name="DefaultActive" type="xs:boolean" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="DefConDRFStrategyHillFitting">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Structure for collection of possible dose-response fitting strategies, e.g. IC50_ONLY and
        whether they involve Hill fitting.
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="Strategy" type="xs:string" />
        <xs:element name="HillFitting" type="xs:boolean" /> 
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="DefConDoseResponseFittingStructure">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Structure for responses to request for dose-response fitting configurations.
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element ref="tns:DefConDRFStrategyHillFitting" maxOccurs="unbounded" />
        <xs:element name="DefaultDoseResponseFittingStrategy" type="xs:string" />
        <xs:element name="DefaultDoseResponseFittingRounded" type="xs:boolean" />
        <xs:element name="DefaultDoseResponseFittingHillMax" type="xs:float" />
        <xs:element name="DefaultDoseResponseFittingHillMin" type="xs:float" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="DefConRequest">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Request the default configuration values.
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType />
  </xs:element>

  <xs:element name="DefConResponse">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Response to request for default configuration values.
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="DefaultForceReRun" type="xs:boolean" />
        <xs:element name="DefaultReset" type="xs:boolean" />
        <xs:element name="DefaultAssayGrouping" type="xs:boolean" />
        <xs:element name="DefaultValueInheriting" type="xs:boolean" />
        <xs:element name="DefaultBetweenGroups" type="xs:boolean" />
        <xs:element name="DefaultWithinGroups" type="xs:boolean" />
        <xs:element name="DefaultMinInformationLevel" type="xs:short" />
        <xs:element name="DefaultMaxInformationLevel" type="xs:short" />
        <xs:element ref="tns:DefConDoseResponseFittingStructure" />
        <xs:element ref="tns:DefConPC50EvaluationStrategyStructure" maxOccurs="unbounded" />
        <xs:element ref="tns:DefConAssayStructure" maxOccurs="unbounded" />
        <xs:element ref="tns:DefConAssayGroupStructure" maxOccurs="unbounded" />
        <xs:element ref="tns:DefConIonChannelStructure" maxOccurs="unbounded" />
        <xs:element ref="tns:DefConCellModelStructure" maxOccurs="unbounded" />
        <xs:element name="DefaultSimulationRequestProcessingPolling" type="xs:int" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="AppManagerWorkloadRequest">
    <xs:annotation xml:lang="en">
      <xs:documentation>
        <![CDATA[
        Request for App Manager component workload.
        ]]>
     </xs:documentation> 
    </xs:annotation>
    <xs:complexType />
  </xs:element>

  <xs:element name="AppManagerWorkloadResponse">
    <xs:annotation xml:lang="en">
      <xs:documentation>
        <![CDATA[
        Response to request for input values for a simulation.
        ]]>
     </xs:documentation> 
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="Workload" type="xs:string" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="InputValuesRequest">
    <xs:annotation xml:lang="en">
      <xs:documentation>
        <![CDATA[
        Request for input values for a simulation.
        ]]>
     </xs:documentation> 
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="SimulationId" type="xs:long" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="InputValuesResponse">
    <xs:annotation xml:lang="en">
      <xs:documentation>
        <![CDATA[
        Response to request for input values for a simulation.
        ]]>
     </xs:documentation> 
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element ref="tns:InputValues" maxOccurs="unbounded" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="InputValues">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Data structure for holding the simulation input values.
        Input values could be either per-assay or per-assay-group, depending on the user's choice.
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="GroupLevel" type="xs:short" />
        <xs:element name="GroupName" type="xs:string" />
        <xs:element ref="tns:InputValuesStructure" maxOccurs="unbounded" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="InputValuesStructure">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Data structure for holding simulation input values
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="IonChannelName" type="xs:string"/>
        <xs:element name="SourceAssayName" type="xs:string" />
        <xs:element name="PIC50s" type="xs:string" />
        <xs:element name="Hills" type="xs:string" />
        <xs:element name="Originals" type="xs:string" />
        <xs:element name="StrategyOrders" type="xs:string" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="RetrieveDiagnosticsRequest">
    <xs:annotation xml:lang="en">
      <xs:documentation>
        <![CDATA[
        Request for job diagnostics information.
        ]]>
     </xs:documentation> 
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="JobId" type="xs:long" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="RetrieveDiagnosticsResponse">
    <xs:annotation xml:lang="en">
      <xs:documentation>
        <![CDATA[
        Response to request for job diagnostics information.
        ]]>
     </xs:documentation> 
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="JobId" type="xs:long" />
        <xs:element name="Info" type="xs:string" />
        <xs:element name="Output" type="xs:string" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="ProcessSimulationsRequest">
    <xs:annotation xml:lang="en">
      <xs:documentation>
        <![CDATA[
        Client request for simulation(s) for compound(s).
        ]]>
     </xs:documentation> 
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element ref="tns:SimulationDetail" maxOccurs="unbounded" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="ProcessSimulationsResponse">
    <xs:annotation xml:lang="en">
      <xs:documentation>
        <![CDATA[
        Response to client request for simulation(s) for compound(s).
        ]]>
      </xs:documentation> 
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element ref="tns:ProcessSimulationsResponseStructure" maxOccurs="unbounded" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="ProcessSimulationsResponseStructure">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Structure of the simulation response.
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="CompoundIdentifier" type="xs:string" />
        <xs:element name="SimulationId" type="xs:long" />
        <xs:element name="InputData" type="xs:string" minOccurs="0"/>
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="ProgressRecordStructure">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Structure of a progress record.
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="SimulationId" type="xs:long" />
        <xs:element name="BusinessManagerProgress" type="xs:string" />
        <xs:element name="AppManagerJobProgress" type="xs:string" maxOccurs="unbounded" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="ProgressRequest">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Request for progress details.
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:choice>
          <xs:element name="SimulationIds" type="xs:long" maxOccurs="unbounded" />
        </xs:choice>
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="ProgressResponse">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Response for request for progress details.
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element ref="tns:ProgressRecordStructure" maxOccurs="unbounded" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="ProvenanceRecordStructure">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Structure of a provenance record.
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="SimulationId" type="xs:long" />
        <xs:element name="Provenance" type="xs:string" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="ProvenanceRequest">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Request for provenance details.
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:choice>
          <xs:element name="SimulationIds" type="xs:long" maxOccurs="unbounded" />
        </xs:choice>
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="ProvenanceResponse">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Response for request for provenance details.
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element ref="tns:ProvenanceRecordStructure" maxOccurs="unbounded" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="ResultsRequest">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Request for simulation results.
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:choice>
          <xs:element name="SimulationId" type="xs:long" />
        </xs:choice>
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="ResultsResponse">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Response for request for simulation results.
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="SimulationId" type="xs:long" />
        <xs:element ref="tns:ResultsJobDataStructure" maxOccurs="unbounded" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="ResultsJobDataStructure">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Data structure for holding per-job simulation results.
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="JobId" type="xs:long" />
        <xs:element name="JobPacingFrequency" type="xs:float" />
        <xs:element name="JobAssayGroupName" type="xs:string" />
        <xs:element name="JobAssayGroupLevel" type="xs:short" />
        <xs:element name="Messages" type="xs:string" />
        <xs:element name="DeltaAPD90PercentileNames" type="xs:string">
          <xs:annotation>
            <xs:documentation>
              <![CDATA[
              Takes the form of a CSV string, although it may be null/empty if retrieving from a
              database containing legacy (i.e. pre- 0.0.2) simulation data.
              ]]>
            </xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:element ref="tns:ResultsConcentrationStructure" maxOccurs="unbounded" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="ResultsConcentrationStructure">
    <xs:annotation>
      <xs:documentation>
        <![CDATA[
        Data structure for holding per-job per-concentration results.
        ]]>
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="CompoundConcentration" type="xs:float" />
        <xs:element name="Times" type="xs:string" />
        <xs:element name="Voltages" type="xs:string" />
        <xs:element name="DeltaAPD90" type="xs:string" />
        <xs:element name="UpstrokeVelocity" type="xs:string" />
        <xs:element name="QNet" type="xs:string" minOccurs="0"/>
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="ExperimentalResultsRequest">
    <xs:annotation xml:lang="en">
      <xs:documentation>
        Client request for experimental results
     </xs:documentation> 
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="SimulationId" type="xs:long" />
        <xs:element name="UserId" type="xs:string" minOccurs="0"/>
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="ExperimentalResultsResponse">
    <xs:annotation xml:lang="en">
      <xs:documentation>
        Response to client's request for experimental results 
     </xs:documentation> 
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="SimulationId" type="xs:long" />
        <xs:element maxOccurs="unbounded" ref="tns:ExperimentalResultsPerFrequency" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="ExperimentalResultsPerFrequency">
    <xs:annotation xml:lang="en">
      <xs:documentation>
        Per-frequency experimental results
     </xs:documentation> 
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="FrequencyHz" type="xs:string" />
        <xs:element maxOccurs="unbounded" ref="tns:ExperimentalResultsPerConcentration" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="ExperimentalResultsPerConcentration">
    <xs:annotation xml:lang="en">
      <xs:documentation>
        Per-concentration experimental results
     </xs:documentation> 
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="PlasmaConc" type="xs:string" />
        <xs:element name="QtPctChangeMean" type="xs:string" />
        <xs:element name="QtPctChangeSEM" type="xs:string" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:element name="SimulationDetail">
    <xs:complexType>
      <xs:sequence>
        <xs:element name="CompoundIdentifier" type="xs:string" />
      </xs:sequence>
      <xs:attribute name="ForceReRun" type="xs:boolean">
        <xs:annotation>
          <xs:documentation>
            <![CDATA[
              Force re-run's primary function is to act as an override of both the following timeouts:
              1. Time since simulation completed.
              2. Time since previous re-run checks (e.g. has new data arrived, has the application
              been updated, etc.,) were completed to see if a new simulation is required.

              Normally, if the timeouts have not expired, a process simulation request will be
              ignored and the latest results returned as a representation of the most recent data
              (thus allowing multiple users to view the latest simulations concurrently without
              continually starting a new simulation).

              There may however be situations where an application administrator has set a long
              timeout duration but there's a desire to force a simulation re-run before the timeout
              expires, e.g., new data is known to have arrived, which is what this attribute is for.

              If the attribute is set to true it has the following impact :
                1) If no simulation exists for the compound it will be ignored, the simulation will run
                     as a normal simulation.
                2) If the simulation is currently running the simulation will be reset (i.e. purged -
                     stopped and data removed), and then re-run.
                3) If a simulation has been completed then it will be purged and re-run.

              If both ForceReRun and Reset are 'true' then Reset is ignored.
            ]]>
          </xs:documentation> 
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="Reset" type="xs:boolean">
        <xs:annotation>
          <xs:documentation>
            <![CDATA[
              This will attempt to reset (i.e. stop and purge) a *running* simulation for a 
              compound. If the simulation is not considered to be running then it will have no
              effect.

              This may be required in situations where the simulation has failed at some point and
              the portal has insufficient failsafe operations to cleanly handle the situation,
              leaving a simulation supposedly permanently running.

              If both ForceReRun and Reset are 'true' then Reset is ignored.
            ]]>
          </xs:documentation> 
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="AssayGrouping" type="xs:boolean">
        <xs:annotation>
          <xs:documentation>
            <![CDATA[
            ]]>
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="ValueInheriting" type="xs:boolean">
        <xs:annotation>
          <xs:documentation>
            <![CDATA[
            ]]>
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="WithinGroups" type="xs:boolean">
        <xs:annotation>
          <xs:documentation>
            <![CDATA[
            ]]>
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="BetweenGroups" type="xs:boolean">
        <xs:annotation>
          <xs:documentation>
            <![CDATA[
            ]]>
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="PC50EvaluationStrategies" type="xs:string">
        <xs:annotation>
          <xs:documentation>
            <![CDATA[
              Which strategies to determine pIC50 values are to used and their priority.
            ]]>
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="DoseResponseFittingStrategy" type="xs:string">
        <xs:annotation>
          <xs:documentation>
            <![CDATA[
              Dose-Response fitting strategy, e.g. IC50_ONLY, IC50_AND_HILL.
            ]]>
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="DoseResponseFittingRounding" type="xs:boolean">
        <xs:annotation>
          <xs:documentation>
            <![CDATA[
              Whether to round negative IC50 values to 0
            ]]>
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="DoseResponseFittingHillMinMax" type="xs:boolean">
        <xs:annotation>
          <xs:documentation>
            <![CDATA[
              Whether to observe Hill min. and max. values or not (assuming fitting strategy allows it).
            ]]>
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="DoseResponseFittingHillMax" type="xs:float">
        <xs:annotation>
          <xs:documentation>
            <![CDATA[
              Maximum Hill value to use (if applicable based on other parameters).
            ]]>
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="DoseResponseFittingHillMin" type="xs:float">
        <xs:annotation>
          <xs:documentation>
            <![CDATA[
              Minimum Hill value to use (if applicable based on other parameters).
            ]]>
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="InputDataOnly" type="xs:boolean">
        <xs:annotation>
          <xs:documentation>
            <![CDATA[
            Whether the simulation request is for the retrieval of input data only.
            ]]>
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="CellMLModelIdentifier" type="xs:short">
        <xs:annotation>
          <xs:documentation>
            <![CDATA[
            CellML Model identifier.
            ]]>
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="PacingMaxTime" type="xs:decimal">
        <xs:annotation>
          <xs:documentation>
            <![CDATA[
            CellML Model maximum pacing time (in minutes).
            ]]>
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
      <xs:attribute name="UserId" type="xs:string">
        <xs:annotation>
          <xs:documentation>
            <![CDATA[
            (Optional) User identifier.
            ]]>
          </xs:documentation>
        </xs:annotation>
      </xs:attribute>
    </xs:complexType>
    <xs:unique name="UniqueCompoundIdentifier">
      <xs:selector xpath="CompoundIdentifier" />
      <xs:field xpath="@value" />
    </xs:unique>
  </xs:element>
  <xs:element name="ResultsReadyNotificationRequest">
    <xs:complexType>
      <xs:sequence>
        <xs:element name="AppManagerId" type="xs:long" />
        <xs:element name="Host" type="xs:string" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>