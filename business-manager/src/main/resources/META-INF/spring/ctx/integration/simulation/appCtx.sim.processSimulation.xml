<?xml version="1.0" encoding="UTF-8"?>
<beans:beans xmlns="http://www.springframework.org/schema/integration"
             xmlns:beans="http://www.springframework.org/schema/beans"
             xmlns:task="http://www.springframework.org/schema/task"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.2.xsd
                                 http://www.springframework.org/schema/integration http://www.springframework.org/schema/integration/spring-integration-3.0.xsd
                                 http://www.springframework.org/schema/task http://www.springframework.org/schema/task/spring-task-3.2.xsd">

  <beans:description>
    <![CDATA[
    Business manager main SI context for simulation processing.
    ]]>
  </beans:description>

  <!-- Initial phase of handling an individual simulation process ++++++++++++++++++++++++++++++ -->

  <channel id="processSimulation_channel_toInputDataGathered"
           datatype="uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedDataVO" />
  <channel id="processSimulation_channel_toSimulationHeaderEnricher"
           datatype="uk.ac.ox.cs.epsrc.business_manager.entity.Simulation" />

  <!-- The gateway to Simulation (rather than request) processing using Spring Integration -->
  <gateway id="simulationProcessingGateway"
           service-interface="uk.ac.ox.cs.epsrc.business_manager.business.SimulationProcessingGateway">
    <!-- Under regular simulation runs there is expected to be no problem replying immediately so
         timeout is low. -->
    <method name="regularSimulation"
            reply-timeout="2000"
            request-channel="processSimulation_channel_toSimulationHeaderEnricher">
      <!-- Add the following header to the message. -->
      <header name="#{T(uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers).SI_HDR_PROCESSING_TYPE}"
              value="#{T(uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType).REGULAR_SIMULATION.toString()}" />
    </method>
    <!-- Under input data gathering situations the process is expected to wait for the response,
           which may take a number of seconds if site web services are being queried.
         Input data gathering uses transient (non-persisted) Simulation objects. -->
    <method name="inputDataGathering"
            reply-timeout="@business_manager.input_data_gathering.timeout@"
            reply-channel="processSimulation_channel_toInputDataGathered"
            request-channel="processSimulation_channel_toSimulationHeaderEnricher">
      <!-- Add the following header to the message. -->
      <header name="#{T(uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers).SI_HDR_PROCESSING_TYPE}"
              value="#{T(uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType).INPUT_DATA_GATHERING.toString()}" />
    </method>
  </gateway>

  <channel id="processSimulation_channel_toProgressHeaderEnricherPreFilter"
           datatype="uk.ac.ox.cs.epsrc.business_manager.entity.Simulation" />

  <task:executor id="processSimulation_toProcessingExecutor"
                 pool-size="10-20"
                 queue-capacity="10"
                 keep-alive="120" />
  <!-- This publish-subscribe channel distributes the simulation to the experimental, QSAR and
         screening processing workflows (as defined in imported resources below).
       Note the presence of "apply-sequence" to enable a subsequent aggregator 
         (processSimulation_postProcessingAggregator) to aggregate pub-sub messages. -->
  <publish-subscribe-channel apply-sequence="true"
                             id="publishsubscribeChannel_toProcessing"
                             datatype="uk.ac.ox.cs.epsrc.business_manager.entity.Simulation"
                             task-executor="processSimulation_toProcessingExecutor">
    <interceptors>
      <!-- wiretap the channel to update progress information. -->
      <wire-tap channel="processSimulation_channel_toProgressHeaderEnricherPreFilter" />
    </interceptors>
  </publish-subscribe-channel>

  <!-- Update the message header to include the simulation identifier. -->
  <header-enricher id="processSimulation_simulationIdHeaderEnricher"
                   input-channel="processSimulation_channel_toSimulationHeaderEnricher"
                   output-channel="publishsubscribeChannel_toProcessing">
    <header name="#{T(uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers).SI_HDR_SIMULATION_ID}"
            expression="payload.id" />
    <header name="#{T(uk.ac.ox.cs.nc3rs.business_manager.api.APIIdentifiers).SI_HDR_COMPOUND_IDENTIFIER}"
            expression="payload.compoundIdentifier" />
    <header name="#{T(uk.ac.ox.cs.nc3rs.business_manager.api.APIIdentifiers).SI_HDR_USER_ID}"
            expression="payload.userId" />
    <!-- TODO: Would prefer this to go into the header at simulationProcessingGateway when input 
               data gathering rather than for all traffic! -->
    <header name="#{T(uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers).SI_HDR_INPUTDATAGATHERING_SIMULATION}"
            expression="getPayload()" />
  </header-enricher>

  <channel id="channel_toPostProcessingAggregator" />

  <beans:import resource="processing/experimental/appCtx.proc.experimental.xml" />
  <beans:import resource="processing/qsar/appCtx.proc.qsar.xml" />
  <beans:import resource="processing/screening/appCtx.proc.screening.xml" />

  <channel id="processSimulation_channel_toPostProcessingRouter"
           datatype="uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata.ProcessedSiteDataVO" />

  <aggregator id="processSimulation_postProcessingAggregator"
              input-channel="channel_toPostProcessingAggregator"
              method="postProcessingDataCollation"
              output-channel="processSimulation_channel_toPostProcessingRouter">
    <beans:bean class="uk.ac.ox.cs.epsrc.business_manager.business.PostProcessingDataAggregator">
      <beans:description>
        <![CDATA[
        This object receives whatever is returned from the paths followed by the subscribers to the 
          publish-subscribe channel, i.e. experimental, QSAR and screening data (if there is any!).
        ]]> 
      </beans:description>
    </beans:bean>
  </aggregator>

  <channel id="processSimulation_channel_toPostProcessingRecorder"
           datatype="uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata.ProcessedSiteDataVO" />
  <channel id="processSimulation_channel_toTerminatingNoSiteDataToProcess"
           datatype="uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata.ProcessedSiteDataVO" />

  <!-- Route the post-processed data object to specified channels depending on presence or 
       otherwise of data to process! -->
  <recipient-list-router id="processSimulation_processedDataRecipientListRouter"
                         input-channel="processSimulation_channel_toPostProcessingRouter">
    <!-- Processed data exists -->
    <recipient channel="processSimulation_channel_toPostProcessingRecorder"
               selector-expression="payload.containingSiteData" />
    <!-- No processed data exists. -->
    <recipient channel="processSimulation_channel_toTerminatingNoSiteDataToProcess"
               selector-expression="!payload.containingSiteData" />
  </recipient-list-router>

  <!-- On simulation non-run send progress information to the messaging system via a transformation
       and also ensure a reply is sent to the client if input data gathering. -->
  <publish-subscribe-channel id="processSimulation_pschannel_toTerminatingSimulationOnNoData"
                             datatype="uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata.ProcessedSiteDataVO" />

  <service-activator id="processSimulation_postProcessingSimulationTermination"
                     input-channel="processSimulation_channel_toTerminatingNoSiteDataToProcess"
                     method="postProcessingSimulationTermination"
                     output-channel="processSimulation_pschannel_toTerminatingSimulationOnNoData">
    <beans:bean class="uk.ac.ox.cs.epsrc.business_manager.business.PostProcessingDataRecorder">
      <beans:description>
        <![CDATA[
        This object receives the empty processed site data and persists a simulation entity 
        indicating simulation pre-invocation processing completion.
        ]]> 
      </beans:description>
    </beans:bean>
  </service-activator>

  <channel id="processSimulation_channel_toTerminationIDGFilter1"
           datatype="uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata.ProcessedSiteDataVO" />
  <channel id="processSimulation_channel_toTerminationIDGFilter2"
           datatype="uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata.ProcessedSiteDataVO" />

  <!-- Bridge to a new channel which can have message header filtering. -->
  <bridge input-channel="processSimulation_pschannel_toTerminatingSimulationOnNoData"
          output-channel="processSimulation_channel_toTerminationIDGFilter1" />
  <bridge input-channel="processSimulation_pschannel_toTerminatingSimulationOnNoData"
          output-channel="processSimulation_channel_toTerminationIDGFilter2" />

  <channel id="processSimulation_channel_toTerminationIDGTransformer"
           datatype="uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata.ProcessedSiteDataVO" />

  <!-- If the process is input data gathering, prepare to send reply to client, otherwise drop
       the message (and leave things to the simulation terminating message key). -->
  <filter expression="headers['#{T(uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers).SI_HDR_PROCESSING_TYPE}'] eq
                              '#{T(uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType).INPUT_DATA_GATHERING.toString()}'"
          input-channel="processSimulation_channel_toTerminationIDGFilter1"
          output-channel="processSimulation_channel_toTerminationIDGTransformer" />

  <transformer id="processSimulation_postProcessingSimulationTerminationInputDataGathering"
               input-channel="processSimulation_channel_toTerminationIDGTransformer"
               method="transformToEmptyProcessed"
               output-channel="processSimulation_channel_toInputDataGathered">
    <beans:bean class="uk.ac.ox.cs.epsrc.business_manager.business.PostProcessingIDGTransformer">
      <beans:description>
        <![CDATA[
        This object receives the empty processed site data and will convert it into an empty object
        to return to the client.
        ]]> 
      </beans:description>
    </beans:bean>
  </transformer>

  <channel id="processSimulation_channel_toTerminationJMSHeaderEnricher"
           datatype="uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata.ProcessedSiteDataVO" />

  <!-- If the process is input data gathering, drop the JMS progress call. -->
  <filter expression="headers['#{T(uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers).SI_HDR_PROCESSING_TYPE}'] ne
                              '#{T(uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType).INPUT_DATA_GATHERING.toString()}'"
          input-channel="processSimulation_channel_toTerminationIDGFilter2"
          output-channel="processSimulation_channel_toTerminationJMSHeaderEnricher" />

  <header-enricher input-channel="processSimulation_channel_toTerminationJMSHeaderEnricher"
                   output-channel="jmsActiveMQ_executorChannel_toInformationTransformer">
    <header name="#{T(uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers).SI_HDR_PROGRESS_MESSAGEKEY}"
            ref="msgkeySimulationTerminating" />
  </header-enricher>

  <!-- See jms/appCtx.jms.ActiveMQ.xml -->
  <transformer id="processSimulation_transformerProcessedSiteDataToProgress"
               input-channel="jmsActiveMQ_executorChannel_toInformationTransformer"
               method="processedSiteDataToProgress"
               output-channel="channel_toOutboundJMS"
               ref="informationTransformer" />

  <channel id="processSimulation_channel_toPreInvocationProcessing"
           datatype="uk.ac.ox.cs.epsrc.business_manager.value.object.PreInvocationProcessingVO" />

  <service-activator id="processSimulation_postProcessingRecorder"
                     input-channel="processSimulation_channel_toPostProcessingRecorder"
                     method="postProcessingDataRecording"
                     output-channel="processSimulation_channel_toPreInvocationProcessing">
    <beans:bean class="uk.ac.ox.cs.epsrc.business_manager.business.PostProcessingDataRecorder">
      <beans:description>
        <![CDATA[
        This object receives the processed site data and will persist it.
        ]]> 
      </beans:description>
    </beans:bean>
  </service-activator>

  <channel id="processSimulation_channel_toPreInvocationRouting"
           datatype="uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedDataVO" />

  <service-activator id="processSimulation_preInvocationProcessing"
                     input-channel="processSimulation_channel_toPreInvocationProcessing"
                     method="preInvocationProcessing"
                     output-channel="processSimulation_channel_toPreInvocationRouting">
    <beans:bean class="uk.ac.ox.cs.epsrc.business_manager.business.PreInvocationAssayValueProcessing">
      <beans:description>
        <![CDATA[
        This object receives the per-assay ion channel pIC50s & Hill, etc and generates objects to
        split on and subsequently invoke.
        ]]> 
      </beans:description>
    </beans:bean>
  </service-activator>

  <channel id="processSimulation_channel_toJobCreator"
           datatype="uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedDataVO" />

  <!-- Route the pre-invocation (but post-processed) object to specified channels depending on
       whether it's been an input data gathering workflow --> 
  <recipient-list-router id="processSimulation_preInvocationRecipientListRouter"
                         input-channel="processSimulation_channel_toPreInvocationRouting">
    <recipient channel="processSimulation_channel_toInputDataGathered"
               selector-expression="headers['#{T(uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers).SI_HDR_PROCESSING_TYPE}'] eq 
                                            '#{T(uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType).INPUT_DATA_GATHERING.toString()}'" />
    <recipient channel="processSimulation_channel_toJobCreator"
               selector-expression="headers['#{T(uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers).SI_HDR_PROCESSING_TYPE}'] ne 
                                            '#{T(uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType).INPUT_DATA_GATHERING.toString()}'" />
  </recipient-list-router>

  <channel id="processSimulation_channel_toJobSplitter"
           datatype="java.util.Set" />

  <service-activator id="processSimulation_jobCreator"
                     input-channel="processSimulation_channel_toJobCreator"
                     method="createJobs"
                     output-channel="processSimulation_channel_toJobSplitter">
    <beans:bean class="uk.ac.ox.cs.epsrc.business_manager.business.JobCreatingServiceActivator">
      <beans:description>
        <![CDATA[
        This object creates the jobs to be split on and sent to the application invocation.
        ]]>
      </beans:description>
    </beans:bean>
  </service-activator>

  <channel id="processSimulation_channel_toJobIdHeaderEnricher"
           datatype="uk.ac.ox.cs.epsrc.business_manager.entity.Job" />

  <splitter input-channel="processSimulation_channel_toJobSplitter"
            output-channel="processSimulation_channel_toJobIdHeaderEnricher" />

  <!-- Place the newly-assigned job identifier in the message headers -->
  <header-enricher input-channel="processSimulation_channel_toJobIdHeaderEnricher"
                   output-channel="appManager_channel_toAppManagerSOAPCreator">
    <header expression="payload.id"
            name="#{T(uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers).SI_HDR_JOB_ID}" />
  </header-enricher>

  <!-- At this point the job queue handler has sent job messages from the queue to app manager -->
  <!-- web service outbound gateway and processing is transferred to the app manager contexts. -->

  <!-- Send progress information to the messaging system via a transformation ++++++++++++++++++ -->
  <channel id="processSimulation_channel_toProgressHeaderEnricher"
           datatype="uk.ac.ox.cs.epsrc.business_manager.entity.Simulation" />

  <!-- If the process is input data gathering, then disregard away the progress update request -->
  <filter expression="headers['#{T(uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers).SI_HDR_PROCESSING_TYPE}'] ne 
                              '#{T(uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType).INPUT_DATA_GATHERING.toString()}'"
          input-channel="processSimulation_channel_toProgressHeaderEnricherPreFilter"
          output-channel="processSimulation_channel_toProgressHeaderEnricher" />

  <header-enricher input-channel="processSimulation_channel_toProgressHeaderEnricher"
                   output-channel="jmsActiveMQ_executorChannel_toInformationTransformer">
    <header name="#{T(uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers).SI_HDR_PROGRESS_MESSAGEKEY}"
            ref="msgkeyDistributing" />
  </header-enricher>

  <!-- See jms/appCtx.jms.ActiveMQ.xml -->
  <transformer id="processSimulation_transformerSimulationToProgress"
               input-channel="jmsActiveMQ_executorChannel_toInformationTransformer"
               method="simulationToProgress"
               output-channel="channel_toOutboundJMS"
               ref="informationTransformer" />

</beans:beans>
