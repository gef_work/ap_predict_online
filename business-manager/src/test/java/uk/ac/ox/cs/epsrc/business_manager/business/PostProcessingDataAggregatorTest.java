/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.PostProcessingDataAggregator;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.InputValueSource;
import uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata.ProcessedSiteDataVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.workflow.OutcomeVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ExperimentalDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.QSARDataHolder.DataPacket;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.qsar.DefaultQSARDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening.AggregatedSiteDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;

/**
 * Unit test the post-processing data aggregator.
 *
 * @author geoff
 */
public class PostProcessingDataAggregatorTest {

  private BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate;
  private IMocksControl mocksControl;
  private final Long dummySimulationId = 1L;
  private PostProcessingDataAggregator aggregator;
  private final String dummyCompoundIdentifier = "dummyCompoundIdentifier";

  private static final String cross = MessageKey.MARK_CROSS.getBundleIdentifier();
  private static final String tick = MessageKey.MARK_TICK.getBundleIdentifier();

  @Before
  public void setUp() {
    aggregator = new PostProcessingDataAggregator();
    mocksControl = createStrictControl();
    mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    ReflectionTestUtils.setField(aggregator, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
  }

  @Test(expected=IllegalArgumentException.class)
  public void testPostProcessingDataCollationFailsOnNullData() {
    ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    aggregator.postProcessingDataCollation(dummyCompoundIdentifier, dummyProcessingType,
                                           dummySimulationId, null);
  }

  @Test
  public void testNoDataToCollate() {
    ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    final List<Object> processedDataCollection = new ArrayList<Object>();

    Capture<Progress> dummyOverallProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress));

    mocksControl.replay();
    ProcessedSiteDataVO processedSiteData = aggregator.postProcessingDataCollation(dummyCompoundIdentifier,
                                                                                   dummyProcessingType,
                                                                                   dummySimulationId,
                                                                                   processedDataCollection);
    mocksControl.verify();

    assertFalse(processedSiteData.isContainingSiteData());
    assertNull(processedSiteData.getExperimental());
    assertTrue(processedSiteData.getQSAR().isEmpty());
    assertTrue(processedSiteData.getScreening().isEmpty());

    OverallProgress capturedOverallProgress = (OverallProgress) dummyOverallProgress.getValue();
    assertEquals(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertEquals(MessageKey.SIMULATION_DATA_COLLATED.getBundleIdentifier(),
                 capturedOverallProgress.getText());
    List<String> overallProgressArgs = capturedOverallProgress.getArgs();
    assertEquals(cross, overallProgressArgs.get(0));
    assertEquals(cross, overallProgressArgs.get(1));
    assertEquals(cross, overallProgressArgs.get(2));
    assertSame(InformationLevel.INFO, capturedOverallProgress.getLevel());

    // Repeat above but for input data gathering.
    mocksControl.reset();

    dummyProcessingType = ProcessingType.INPUT_DATA_GATHERING;

    mocksControl.replay();
    processedSiteData = aggregator.postProcessingDataCollation(dummyCompoundIdentifier,
                                                               dummyProcessingType,
                                                               dummySimulationId,
                                                               processedDataCollection);
    mocksControl.verify();

    assertFalse(processedSiteData.isContainingSiteData());
    assertNull(processedSiteData.getExperimental());
    assertTrue(processedSiteData.getQSAR().isEmpty());
    assertTrue(processedSiteData.getScreening().isEmpty());
  }

  @Test
  public void testPostProcessingDataCollationSuccessWhenScreeningData() {
    /* Test 1 : Emulate receipt of empty collection of input value sources, e.g. due to removal of
                invalid data */
    ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    final List<Object> processedDataCollection = new ArrayList<Object>();

    // Create a fake outcome of screening data processing.
    final List<OutcomeVO> dummyScreeningData = new ArrayList<OutcomeVO>();
    final OutcomeVO mockScreeningData = mocksControl.createMock(OutcomeVO.class);
    dummyScreeningData.add(mockScreeningData);
    processedDataCollection.add(dummyScreeningData);

    // Empty collection of input value sources
    final List<InputValueSource> dummyInputValueSources = new ArrayList<InputValueSource>();
    expect(mockScreeningData.getInputValueSources()).andReturn(dummyInputValueSources);

    Capture<Progress> dummyOverallProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress));

    mocksControl.replay();
    ProcessedSiteDataVO processedSiteData = aggregator.postProcessingDataCollation(dummyCompoundIdentifier,
                                                                                   dummyProcessingType,
                                                                                   dummySimulationId,
                                                                                   processedDataCollection);
    mocksControl.verify();

    assertFalse(processedSiteData.isContainingSiteData());
    assertNull(processedSiteData.getExperimental());
    assertTrue(processedSiteData.getQSAR().isEmpty());
    assertTrue(processedSiteData.getScreening().isEmpty());

    OverallProgress capturedOverallProgress = (OverallProgress) dummyOverallProgress.getValue();
    assertEquals(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertEquals(MessageKey.SIMULATION_DATA_COLLATED.getBundleIdentifier(),
                 capturedOverallProgress.getText());
    List<String> overallProgressArgs = capturedOverallProgress.getArgs();
    assertEquals(cross, overallProgressArgs.get(0));
    assertEquals(cross, overallProgressArgs.get(1));
    assertEquals(tick, overallProgressArgs.get(2));
    assertSame(InformationLevel.INFO, capturedOverallProgress.getLevel());

    mocksControl.reset();

    // Test 2 : Emulate there being a viable input value source available.
    final InputValueSource dummyInputValueSource = mocksControl.createMock(InputValueSource.class);
    dummyInputValueSources.add(dummyInputValueSource);

    expect(mockScreeningData.getInputValueSources()).andReturn(dummyInputValueSources);

    dummyOverallProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress));

    mocksControl.replay();
    processedSiteData = aggregator.postProcessingDataCollation(dummyCompoundIdentifier,
                                                               dummyProcessingType,
                                                               dummySimulationId,
                                                               processedDataCollection);
    mocksControl.verify();

    assertTrue(processedSiteData.isContainingSiteData());
    assertNull(processedSiteData.getExperimental());
    assertTrue(processedSiteData.getQSAR().isEmpty());
    assertFalse(processedSiteData.getScreening().isEmpty());

    capturedOverallProgress = (OverallProgress) dummyOverallProgress.getValue();
    assertEquals(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertEquals(MessageKey.SIMULATION_DATA_COLLATED.getBundleIdentifier(),
                 capturedOverallProgress.getText());
    overallProgressArgs = capturedOverallProgress.getArgs();
    assertEquals(cross, overallProgressArgs.get(0));
    assertEquals(cross, overallProgressArgs.get(1));
    assertEquals(tick, overallProgressArgs.get(2));
    assertSame(InformationLevel.INFO, capturedOverallProgress.getLevel());
  }

  @Test
  public void testPostProcessingDataCollationSuccessWhenQSARData() {
    /*
     * Emulate QSAR processing completed but no data or problems in content.
     */
    ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    final List<Object> processedDataCollection = new ArrayList<Object>();

    // Create a fake outcome of QSAR data processing.
    final DefaultQSARDataVO mockQSARDataVO = mocksControl.createMock(DefaultQSARDataVO.class);
    processedDataCollection.add(mockQSARDataVO);

    // Emulate no QSAR data therein.
    expect(mockQSARDataVO.containsData()).andReturn(false);
    // Emulate no problems during QSAR processing.
    final List<String> dummyProblems = new ArrayList<String>();
    expect(mockQSARDataVO.getProblems()).andReturn(dummyProblems);

    Capture<Progress> dummyOverallProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress));

    mocksControl.replay();
    ProcessedSiteDataVO processedSiteData = aggregator.postProcessingDataCollation(dummyCompoundIdentifier,
                                                                                   dummyProcessingType,
                                                                                   dummySimulationId,
                                                                                   processedDataCollection);
    mocksControl.verify();

    assertFalse(processedSiteData.isContainingSiteData());
    assertNull(processedSiteData.getExperimental());
    assertTrue(processedSiteData.getQSAR().isEmpty());
    assertTrue(processedSiteData.getScreening().isEmpty());

    OverallProgress capturedOverallProgress = (OverallProgress) dummyOverallProgress.getValue();
    assertEquals(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertEquals(MessageKey.SIMULATION_DATA_COLLATED.getBundleIdentifier(),
                 capturedOverallProgress.getText());
    List<String> overallProgressArgs = capturedOverallProgress.getArgs();
    assertEquals(cross, overallProgressArgs.get(0));
    assertEquals(cross, overallProgressArgs.get(1));
    assertEquals(cross, overallProgressArgs.get(2));
    assertSame(InformationLevel.INFO, capturedOverallProgress.getLevel());

    mocksControl.reset();

    /*
     * Emulate QSAR data processing but only problems, no data.
     */
    expect(mockQSARDataVO.containsData()).andReturn(false);
    // Here are the problems.
    final String dummyProblem = "dummyProblem";
    dummyProblems.add(dummyProblem);
    expect(mockQSARDataVO.getProblems()).andReturn(dummyProblems);

    Capture<Progress> dummyOverallProgressProblems = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgressProblems));

    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress));

    mocksControl.replay();
    aggregator.postProcessingDataCollation(dummyCompoundIdentifier, dummyProcessingType,
                                           dummySimulationId, processedDataCollection);
    mocksControl.verify();

    // Test the content of the problems being sent to progress.
    OverallProgress capturedOverallProgressProblems = (OverallProgress) dummyOverallProgressProblems.getValue();
    assertEquals(dummySimulationId, capturedOverallProgressProblems.getSimulationId());
    assertEquals(dummyProblem, capturedOverallProgressProblems.getText());

    // No need to retest overall progress!

    mocksControl.reset();

    final AssayVO mockAssay = mocksControl.createMock(AssayVO.class);
    final DataPacket mockDataPacket = mocksControl.createMock(DataPacket.class);
    final IonChannel dummyIonChannel = IonChannel.CaV1_2;

    final Set<DataPacket> dummyDataPackets = new HashSet<DataPacket>(); 
    dummyDataPackets.add(mockDataPacket);
    final BigDecimal dummyPIC50 = BigDecimal.ONE;
    dummyProblems.clear();

    // Emulate QSAR data processing with (a single assay/ion channel) data and no problems.
    expect(mockQSARDataVO.containsData()).andReturn(true);
    expect(mockQSARDataVO.getAssay()).andReturn(mockAssay);
    expect(mockQSARDataVO.getDataPackets()).andReturn(dummyDataPackets);
    expect(mockDataPacket.getIonChannel()).andReturn(dummyIonChannel);
    expect(mockDataPacket.getpIC50()).andReturn(dummyPIC50);
    expect(mockQSARDataVO.getProblems()).andReturn(dummyProblems);
    dummyOverallProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress));

    mocksControl.replay();
    processedSiteData = aggregator.postProcessingDataCollation(dummyCompoundIdentifier,
                                                               dummyProcessingType,
                                                               dummySimulationId,
                                                               processedDataCollection);
    mocksControl.verify();

    assertTrue(processedSiteData.isContainingSiteData());
    assertNull(processedSiteData.getExperimental());
    assertFalse(processedSiteData.getQSAR().isEmpty());
    assertTrue(processedSiteData.getScreening().isEmpty());

    capturedOverallProgress = (OverallProgress) dummyOverallProgress.getValue();
    assertEquals(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertEquals(MessageKey.SIMULATION_DATA_COLLATED.getBundleIdentifier(),
                 capturedOverallProgress.getText());
    overallProgressArgs = capturedOverallProgress.getArgs();
    assertEquals(cross, overallProgressArgs.get(0));
    assertEquals(tick, overallProgressArgs.get(1));
    assertEquals(cross, overallProgressArgs.get(2));
    assertSame(InformationLevel.INFO, capturedOverallProgress.getLevel());
  }

  @Test
  public void testPostProcessingDataCollationSuccessWhenExperimentalData() {
    ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    final List<Object> processedDataCollection = new ArrayList<Object>();

    // Create a mock outcome of experimental data processing which says it doesn't contain data.
    final ExperimentalDataHolder mockExperimentalDataHolder = mocksControl.createMock(ExperimentalDataHolder.class);
    expect(mockExperimentalDataHolder.containsData()).andReturn(false);
    processedDataCollection.add(mockExperimentalDataHolder);

    Capture<Progress> dummyOverallProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress));

    mocksControl.replay();
    ProcessedSiteDataVO processedSiteData = aggregator.postProcessingDataCollation(dummyCompoundIdentifier,
                                                                                   dummyProcessingType,
                                                                                   dummySimulationId,
                                                                                   processedDataCollection);

    mocksControl.verify();

    assertNotNull(processedSiteData.getExperimental());
    assertTrue(processedSiteData.getQSAR().isEmpty());
    assertTrue(processedSiteData.getScreening().isEmpty());

    OverallProgress capturedOverallProgress = (OverallProgress) dummyOverallProgress.getValue();
    assertEquals(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertEquals(MessageKey.SIMULATION_DATA_COLLATED.getBundleIdentifier(),
                 capturedOverallProgress.getText());
    List<String> overallProgressArgs = capturedOverallProgress.getArgs();
    assertEquals(cross, overallProgressArgs.get(0));
    assertEquals(cross, overallProgressArgs.get(1));
    assertEquals(cross, overallProgressArgs.get(2));
    assertSame(InformationLevel.INFO, capturedOverallProgress.getLevel());

    mocksControl.reset();

    // Create a mock outcome of experimental data processing which says it contains data.
    processedDataCollection.clear();
    expect(mockExperimentalDataHolder.containsData()).andReturn(true);
    processedDataCollection.add(mockExperimentalDataHolder);

    dummyOverallProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress));

    mocksControl.replay();
    processedSiteData = aggregator.postProcessingDataCollation(dummyCompoundIdentifier,
                                                               dummyProcessingType,
                                                               dummySimulationId,
                                                               processedDataCollection);

    mocksControl.verify();

    assertNotNull(processedSiteData.getExperimental());
    assertTrue(processedSiteData.getQSAR().isEmpty());
    assertTrue(processedSiteData.getScreening().isEmpty());

    capturedOverallProgress = (OverallProgress) dummyOverallProgress.getValue();
    assertEquals(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertEquals(MessageKey.SIMULATION_DATA_COLLATED.getBundleIdentifier(),
                 capturedOverallProgress.getText());
    overallProgressArgs = capturedOverallProgress.getArgs();
    assertEquals(tick, overallProgressArgs.get(0));
    assertEquals(cross, overallProgressArgs.get(1));
    assertEquals(cross, overallProgressArgs.get(2));
    assertSame(InformationLevel.INFO, capturedOverallProgress.getLevel());
  }

  @Test
  public void testPostProcessingDataCollationFailWhenAggregatedSiteDataHasData() {
    ProcessingType dummyProcessingType = ProcessingType.INPUT_DATA_GATHERING;
    final List<Object> processedDataCollection = new ArrayList<Object>();

    // Create a mock outcome of aggregated site data processing.
    final AggregatedSiteDataVO mockAggregatedSiteData = mocksControl.createMock(AggregatedSiteDataVO.class);

    expect(mockAggregatedSiteData.hasSiteData()).andReturn(false);
    expect(mockAggregatedSiteData.hasIrregularSiteData()).andReturn(true);
    processedDataCollection.add(mockAggregatedSiteData);

    mocksControl.replay();

    ProcessedSiteDataVO processedSiteData = null;
    try {
      processedSiteData = aggregator.postProcessingDataCollation(dummyCompoundIdentifier,
                                                                 dummyProcessingType,
                                                                 dummySimulationId,
                                                                 processedDataCollection);
      fail("Should not permit content in aggregated site data.");
    } catch (IllegalStateException e) {}

    mocksControl.verify();

    assertNull(processedSiteData);

    mocksControl.reset();
    processedDataCollection.clear();

    expect(mockAggregatedSiteData.hasSiteData()).andReturn(true);
    processedDataCollection.add(mockAggregatedSiteData);

    mocksControl.replay();

    try {
      processedSiteData = aggregator.postProcessingDataCollation(dummyCompoundIdentifier,
                                                                 dummyProcessingType,
                                                                 dummySimulationId,
                                                                 processedDataCollection);
      fail("Should not permit content in aggregated site data.");
    } catch (IllegalStateException e) {}

    mocksControl.verify();

    assertNull(processedSiteData);
  }

  @Test
  public void testPostProcessingDataCollationSuccessWhenAggregatedSiteData() {
    ProcessingType dummyProcessingType = ProcessingType.INPUT_DATA_GATHERING;
    final List<Object> processedDataCollection = new ArrayList<Object>();

    /*
     * Test (1) input data gathering and (2) no regular or irregular site data and no problems.
     */
    final AggregatedSiteDataVO mockAggregatedSiteData = mocksControl.createMock(AggregatedSiteDataVO.class);
    expect(mockAggregatedSiteData.hasSiteData()).andReturn(false);
    expect(mockAggregatedSiteData.hasIrregularSiteData()).andReturn(false);
    expect(mockAggregatedSiteData.hasProblems()).andReturn(false);
    processedDataCollection.add(mockAggregatedSiteData);

    mocksControl.replay();

    ProcessedSiteDataVO processedSiteData = aggregator.postProcessingDataCollation(dummyCompoundIdentifier,
                                                                                   dummyProcessingType,
                                                                                   dummySimulationId,
                                                                                   processedDataCollection);

    mocksControl.verify();

    assertNull(processedSiteData.getExperimental());
    assertTrue(processedSiteData.getQSAR().isEmpty());
    assertTrue(processedSiteData.getScreening().isEmpty());

    mocksControl.reset();
    processedDataCollection.clear();

    /*
     * Test (1) input data gathering and (2) no regular or irregular site data but problems.
     */
    expect(mockAggregatedSiteData.hasSiteData()).andReturn(false);
    expect(mockAggregatedSiteData.hasIrregularSiteData()).andReturn(false);
    expect(mockAggregatedSiteData.hasProblems()).andReturn(true);
    expect(mockAggregatedSiteData.getProblems()).andReturn(new ArrayList<String>());

    processedDataCollection.add(mockAggregatedSiteData);

    mocksControl.replay();

    processedSiteData = aggregator.postProcessingDataCollation(dummyCompoundIdentifier,
                                                               dummyProcessingType,
                                                               dummySimulationId,
                                                               processedDataCollection);

    mocksControl.verify();

    mocksControl.reset();
    processedDataCollection.clear();

    dummyProcessingType = ProcessingType.REGULAR_SIMULATION;

    /*
     * Test (1) regular simulation and (2) no regular or irregular site data and no problems.
     */
    expect(mockAggregatedSiteData.hasSiteData()).andReturn(false);
    expect(mockAggregatedSiteData.hasIrregularSiteData()).andReturn(false);
    processedDataCollection.add(mockAggregatedSiteData);

    Capture<Progress> dummyOverallProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress));

    mocksControl.replay();
    processedSiteData = aggregator.postProcessingDataCollation(dummyCompoundIdentifier,
                                                               dummyProcessingType,
                                                               dummySimulationId,
                                                               processedDataCollection);
    mocksControl.verify();

    assertNull(processedSiteData.getExperimental());
    assertTrue(processedSiteData.getQSAR().isEmpty());
    assertTrue(processedSiteData.getScreening().isEmpty());

    OverallProgress capturedOverallProgress = (OverallProgress) dummyOverallProgress.getValue();
    assertEquals(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertEquals(MessageKey.SIMULATION_DATA_COLLATED.getBundleIdentifier(),
                 capturedOverallProgress.getText());
    List<String> overallProgressArgs = capturedOverallProgress.getArgs();
    assertEquals(cross, overallProgressArgs.get(0));
    assertEquals(cross, overallProgressArgs.get(1));
    assertEquals(cross, overallProgressArgs.get(2));
    assertSame(InformationLevel.INFO, capturedOverallProgress.getLevel());
  }

  @Test
  public void testPostProcessingDataCollationWhenSimulationOrOtherData() {
    // Test input data gathering.
    ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    final List<Object> processedDataCollection = new ArrayList<Object>();

    // Create a mock simulation object.
    final Simulation mockSimulation = mocksControl.createMock(Simulation.class);
    processedDataCollection.add(mockSimulation);

    Capture<Progress> dummyOverallProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress));

    mocksControl.replay();
    ProcessedSiteDataVO processedSiteData = aggregator.postProcessingDataCollation(dummyCompoundIdentifier,
                                                                                   dummyProcessingType,
                                                                                   dummySimulationId,
                                                                                   processedDataCollection);

    mocksControl.verify();

    assertNull(processedSiteData.getExperimental());
    assertTrue(processedSiteData.getQSAR().isEmpty());
    assertTrue(processedSiteData.getScreening().isEmpty());

    mocksControl.reset();

    processedDataCollection.clear();
    // Create a dummy random object.
    final Object dummyObject = new Object();
    processedDataCollection.add(dummyObject);

    dummyOverallProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress));

    mocksControl.replay();
    processedSiteData = aggregator.postProcessingDataCollation(dummyCompoundIdentifier,
                                                               dummyProcessingType,
                                                               dummySimulationId,
                                                               processedDataCollection);

    mocksControl.verify();

    assertNull(processedSiteData.getExperimental());
    assertTrue(processedSiteData.getQSAR().isEmpty());
    assertTrue(processedSiteData.getScreening().isEmpty());
  }
}