/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.AggregatedWorkflowOutputDecisionMaker;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.InputValueSource;
import uk.ac.ox.cs.epsrc.business_manager.value.object.workflow.OutcomeVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;

/**
 * Unit test the workflow output decision maker.
 *
 * @author geoff
 */
public class AggregatedWorkflowOutputDecisionMakerTest {

  private AggregatedWorkflowOutputDecisionMaker decisionMaker;
  private BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate;
  private IMocksControl mockControl;
  private final Long dummySimulationId = 1L;
  private final ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
  private final String dummyCompoundIdentifier = "fakeCompoundIdentifier";
  

  @Before
  public void setUp() {
    decisionMaker = new AggregatedWorkflowOutputDecisionMaker();
    mockControl = createStrictControl();
    mockBusinessManagerJmsTemplate = mockControl.createMock(BusinessManagerJMSTemplate.class);
    ReflectionTestUtils.setField(decisionMaker, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
  }

  @Test
  public void testEmptyOutcomes() {
    final List<OutcomeVO> dummyOutcomes = new ArrayList<OutcomeVO>();
    List<OutcomeVO> decisions = decisionMaker.makeDecisions(dummyCompoundIdentifier,
                                                            dummyProcessingType, dummySimulationId,
                                                            dummyOutcomes);
    assertTrue(decisions.isEmpty());
  }

  @Test
  public void testSingleOutcome() {
    final List<OutcomeVO> mockOutcomes = new ArrayList<OutcomeVO>();
    OutcomeVO mockOutcome1 = mockControl.createMock(OutcomeVO.class);
    mockOutcomes.add(mockOutcome1);
    AssayVO mockAssay1 = mockControl.createMock(AssayVO.class);
    IonChannel dummyIonChannel1 = IonChannel.CaV1_2;

    expect(mockOutcome1.getAssay()).andReturn(mockAssay1);
    expect(mockAssay1.getName()).andReturn("dummyMockAssay1Name");
    expect(mockOutcome1.getIonChannel()).andReturn(dummyIonChannel1);

    mockControl.replay();
    List<OutcomeVO> decisions = decisionMaker.makeDecisions(dummyCompoundIdentifier,
                                                            dummyProcessingType, dummySimulationId,
                                                            mockOutcomes);
    mockControl.verify();

    assertTrue(1 == decisions.size());
  }

  @Test
  public void testEmulatingTwoIndividual() {
    final List<OutcomeVO> mockOutcomes = new ArrayList<OutcomeVO>();
    OutcomeVO mockOutcome1 = mockControl.createMock(OutcomeVO.class);
    OutcomeVO mockOutcome2 = mockControl.createMock(OutcomeVO.class);
    mockOutcomes.add(mockOutcome1);
    mockOutcomes.add(mockOutcome2);

    AssayVO mockAssay1 = mockControl.createMock(AssayVO.class);
    AssayVO mockAssay2 = mockControl.createMock(AssayVO.class);

    IonChannel dummyIonChannel1 = IonChannel.CaV1_2;
    IonChannel dummyIonChannel2 = IonChannel.CaV1_2;

    expect(mockOutcome1.getAssay()).andReturn(mockAssay1);
    expect(mockAssay1.getName()).andReturn("dummyMockAssay1Name");
    expect(mockOutcome1.getIonChannel()).andReturn(dummyIonChannel1);

    expect(mockOutcome2.getAssay()).andReturn(mockAssay2);
    expect(mockAssay2.getName()).andReturn("dummyMockAssay1Name");
    expect(mockOutcome2.getIonChannel()).andReturn(dummyIonChannel2);

    // Existing, i.e. first, is checked first for summary data, then latest in array.
    expect(mockOutcome1.getInputValueSources()).andReturn(new ArrayList<InputValueSource>());
    expect(mockOutcome2.getInputValueSources()).andReturn(new ArrayList<InputValueSource>());

    // Now combine results.
    expect(mockOutcome1.getInputValueSources()).andReturn(new ArrayList<InputValueSource>());
    expect(mockOutcome2.getInputValueSources()).andReturn(new ArrayList<InputValueSource>());

    mockControl.replay();

    List<OutcomeVO> decisions = decisionMaker.makeDecisions(dummyCompoundIdentifier,
                                                            dummyProcessingType, dummySimulationId,
                                                            mockOutcomes);
    mockControl.verify();

    assertTrue(1 == decisions.size());
  }


  @Test
  public void testEmulatingThreeIndividual() {
    final List<OutcomeVO> mockOutcomes = new ArrayList<OutcomeVO>();
    OutcomeVO mockOutcome1 = mockControl.createMock(OutcomeVO.class);
    OutcomeVO mockOutcome2 = mockControl.createMock(OutcomeVO.class);
    OutcomeVO mockOutcome3 = mockControl.createMock(OutcomeVO.class);
    mockOutcomes.add(mockOutcome1);
    mockOutcomes.add(mockOutcome2);
    mockOutcomes.add(mockOutcome3);

    AssayVO mockAssay1 = mockControl.createMock(AssayVO.class);
    AssayVO mockAssay2 = mockControl.createMock(AssayVO.class);
    AssayVO mockAssay3 = mockControl.createMock(AssayVO.class);

    IonChannel dummyIonChannel1 = IonChannel.CaV1_2;
    IonChannel dummyIonChannel2 = IonChannel.CaV1_2;
    IonChannel dummyIonChannel3 = IonChannel.CaV1_2;

    expect(mockOutcome1.getAssay()).andReturn(mockAssay1);
    expect(mockAssay1.getName()).andReturn("dummyMockAssay1Name");
    expect(mockOutcome1.getIonChannel()).andReturn(dummyIonChannel1);

    expect(mockOutcome2.getAssay()).andReturn(mockAssay2);
    expect(mockAssay2.getName()).andReturn("dummyMockAssay1Name");
    expect(mockOutcome2.getIonChannel()).andReturn(dummyIonChannel2);

    // Existing, i.e. first, is checked first for summary data, then new one.
    expect(mockOutcome1.getInputValueSources()).andReturn(new ArrayList<InputValueSource>());
    expect(mockOutcome2.getInputValueSources()).andReturn(new ArrayList<InputValueSource>());

    // Now combine results.
    expect(mockOutcome1.getInputValueSources()).andReturn(new ArrayList<InputValueSource>());
    expect(mockOutcome2.getInputValueSources()).andReturn(new ArrayList<InputValueSource>());

    // Now process third one
    expect(mockOutcome3.getAssay()).andReturn(mockAssay3);
    expect(mockAssay3.getName()).andReturn("dummyMockAssay1Name");
    expect(mockOutcome3.getIonChannel()).andReturn(dummyIonChannel3);

    // The first to be checked is the object created in prior iteration (combination of first and
    // second), then new one.
    expect(mockOutcome3.getInputValueSources()).andReturn(new ArrayList<InputValueSource>()).times(2);

    mockControl.replay();

    List<OutcomeVO> decisions = decisionMaker.makeDecisions(dummyCompoundIdentifier,
                                                            dummyProcessingType, dummySimulationId,
                                                            mockOutcomes);
    mockControl.verify();

    assertTrue(1 == decisions.size());
  }
}