/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.information.progress;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.JobProgress;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Unit test the job progress.
 *
 * @author geoff
 */
public class JobProgressTest {

  private InformationLevel dummyInformationLevel;
  private String dummyText;
  private Long dummyJobId;
  private Long dummySimulationId;

  @Before
  public void setUp() {
    dummyInformationLevel = null;
    dummyText = null;
    dummyJobId = null;
    dummySimulationId = null;
  }

  @Test
  public void testBuilderFourArgConstructorArgChecking() {
    dummyText = "dummyText";
    dummySimulationId = 1L;
    dummyJobId = 2L;

    JobProgress jobProgress = null;
    try {
      jobProgress = new JobProgress.Builder(dummyInformationLevel, dummyText, dummySimulationId,
                                            dummyJobId).build();
      fail("Should not allow null information level!");
    } catch (Exception e) {}

    dummyInformationLevel = InformationLevel.TRACE;
    dummyText = null;

    try {
      jobProgress = new JobProgress.Builder(dummyInformationLevel, dummyText, dummySimulationId,
                                            dummyJobId).build();
      fail("Should not allow null text!");
    } catch (Exception e) {}

    dummyText = "dummyText";
    dummySimulationId = null;

    try {
      jobProgress = new JobProgress.Builder(dummyInformationLevel, dummyText, dummySimulationId,
                                            dummyJobId).build();
      fail("Should not allow null simulation id!");
    } catch (Exception e) {}

    dummySimulationId = 1L;
    dummyJobId = null;

    try {
      jobProgress = new JobProgress.Builder(dummyInformationLevel, dummyText, dummySimulationId,
                                            dummyJobId).build();
      fail("Should not allow null job id!");
    } catch (Exception e) {}

    assertNull(jobProgress);
  }

  @Test
  public void testBuilderFourArgConstructorCreation() {
    dummyInformationLevel = InformationLevel.TRACE;
    dummyText = "dummyText";
    final Short dummyGroupLevel = Short.valueOf("2");
    final String dummyGroupName = "dummyGroupName";
    dummyJobId = 3L;
    final Float dummyPacingFrequency = 3.2F;
    dummySimulationId = 4L;
    JobProgress jobProgress = new JobProgress.Builder(dummyInformationLevel, dummyText,
                                                      dummySimulationId, dummyJobId)
                                             .onCreation(dummyGroupName, dummyGroupLevel,
                                                         dummyPacingFrequency) 
                                             .build();
    assertEquals(dummyInformationLevel, jobProgress.getLevel());
    assertEquals(dummyText, jobProgress.getText());
    assertEquals(dummyGroupLevel, jobProgress.getGroupLevel());
    assertEquals(dummyGroupName, jobProgress.getGroupName());
    assertEquals(dummyJobId, jobProgress.getJobId());
    assertEquals(dummyPacingFrequency, jobProgress.getPacingFrequency());
    assertEquals(dummySimulationId, jobProgress.getSimulationId());
    assertNotNull(jobProgress.toString());
  }

  @Test
  public void testBuilderFourArgConstructorNotCreating() {
    dummyInformationLevel = InformationLevel.TRACE;
    dummyText = "dummyText";
    dummySimulationId = 1L;
    dummyJobId = 2L;
    final JobProgress jobProgress = new JobProgress.Builder(dummyInformationLevel, dummyText,
                                                            dummySimulationId, dummyJobId).build();
    assertEquals(dummyInformationLevel, jobProgress.getLevel());
    assertEquals(dummyText, jobProgress.getText());
    assertEquals(dummySimulationId, jobProgress.getSimulationId());
    assertEquals(dummyJobId, jobProgress.getJobId());
    assertNull(jobProgress.getGroupLevel());
    assertNull(jobProgress.getGroupName());
    assertNull(jobProgress.getPacingFrequency());
    assertNotNull(jobProgress.toString());
  }
}