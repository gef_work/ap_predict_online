/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.service;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertSame;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.manager.JobManager;
import uk.ac.ox.cs.epsrc.business_manager.manager.ResultsManager;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.GroupedValuesVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.JobDiagnosticsVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.JobResultsVO;

/**
 * Unit test the results service implementation.
 *
 * @author geoff
 */
public class ResultsServiceImplTest {

  private IMocksControl mocksControl;
  private JobDiagnosticsVO mockJobDiagnostics;
  private JobManager mockJobManager;
  private long dummyJobId = 2l;
  private long dummySimulationId = 32l;
  private ResultsManager mockResultsManager;
  private ResultsService resultsService;

  @Before
  public void setUp() {
    resultsService = new ResultsServiceImpl();

    mocksControl = createStrictControl();
    mockJobManager = mocksControl.createMock(JobManager.class);
    mockResultsManager = mocksControl.createMock(ResultsManager.class);

    ReflectionTestUtils.setField(resultsService, BusinessIdentifiers.COMPONENT_JOB_MANAGER,
                                 mockJobManager);
    ReflectionTestUtils.setField(resultsService, BusinessIdentifiers.COMPONENT_RESULTS_MANAGER,
                                 mockResultsManager);

    mockJobDiagnostics = mocksControl.createMock(JobDiagnosticsVO.class);
  }

  @Test
  public void testRetrieveDiagnostics() {
    expect(mockJobManager.retrieveDiagnostics(dummyJobId)).andReturn(mockJobDiagnostics);

    mocksControl.replay();

    final JobDiagnosticsVO returnedJobDiagnostics = resultsService.retrieveDiagnostics(dummyJobId);

    mocksControl.verify();

    assertSame(mockJobDiagnostics, returnedJobDiagnostics);
  }

  @Test
  public void testRetrieveInputValues() {
    final Set<GroupedValuesVO> dummyGroupedValuesVO = new HashSet<GroupedValuesVO>();
    expect(mockResultsManager.retrieveInputValues(dummySimulationId)).andReturn(dummyGroupedValuesVO);

    mocksControl.replay();

    Set<GroupedValuesVO> returnedGroupedValuesVO = resultsService.retrieveInputValues(dummySimulationId);

    mocksControl.verify();

    assertSame(dummyGroupedValuesVO, returnedGroupedValuesVO);
  }

  @Test
  public void testRetrieveResults() {
    final List<JobResultsVO> dummyJobResults = new ArrayList<JobResultsVO>();
    expect(mockResultsManager.retrieveResults(dummySimulationId)).andReturn(dummyJobResults);

    mocksControl.replay();

    List<JobResultsVO> returnedJobResults = resultsService.retrieveResults(dummySimulationId);

    mocksControl.verify();

    assertSame(dummyJobResults, returnedJobResults);
  }
}