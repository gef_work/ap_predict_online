/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.util;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.easymock.IMocksControl;
import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.business.util.DataUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.NoSuchDataException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;

/**
 * Unit test the data utility class.
 *
 * @author geoff
 */
public class DataUtilTest {

  @Test
  public void testConstructor() {
    new DataUtil();
  }

  @Test(expected=IllegalArgumentException.class)
  public void testPC50ByC50EqualityModifierFailsOnNullArg() {
    ICData dummyC50Data = null;
    String dummyRecordIdentifier = null;
    DataUtil.pc50ByC50EqualityModifier(dummyC50Data, dummyRecordIdentifier);
  }

  @Test
  public void testPC50ByC50EqualityModifier() throws NoSuchDataException {
    final IMocksControl mocksControl = createStrictControl(); 
    final ICData mockC50Data = mocksControl.createMock(ICData.class);
    final String dummyRecordIdentifier = "dummyRecordIdentifier";

    expect(mockC50Data.isModifierEquals(ICPercent.PCT50)).andReturn(false);

    mocksControl.replay();
    BigDecimal pc50 = DataUtil.pc50ByC50EqualityModifier(mockC50Data, dummyRecordIdentifier);

    mocksControl.verify();

    assertNull(pc50); 

    mocksControl.reset();

    expect(mockC50Data.isModifierEquals(ICPercent.PCT50)).andReturn(true);
    expect(mockC50Data.retrieveEquivalentPC50Value(ICPercent.PCT50)).andReturn(BigDecimal.TEN);

    mocksControl.replay();

    pc50 = DataUtil.pc50ByC50EqualityModifier(mockC50Data, dummyRecordIdentifier);

    mocksControl.verify();

    assertTrue(BigDecimal.TEN.compareTo(pc50) == 0); 
  }

  @Test(expected=IllegalArgumentException.class)
  public void testPC50ByNearestIntegerIfNotHighlyActiveFailsOnNullArg() {
    ICData dummyC50Data = null;
    String dummyRecordIdentifier = null;
    DataUtil.pc50ByNearestIntegerIfNotHighlyActive(dummyC50Data, dummyRecordIdentifier);
  }

  @Test
  public void testPC50ByNearestIntegerIfNotHighlyActive() throws NoSuchDataException {
    final IMocksControl mocksControl = createStrictControl();
    final ICData mockC50Data = mocksControl.createMock(ICData.class);
    final String dummyRecordIdentifier = "dummyRecordIdentifier";

    /*
     * If an equality modifier is present then nearest integer returns null.
     */
    boolean dummyIsModifierEquals = true;

    expect(mockC50Data.isModifierEquals(ICPercent.PCT50)).andReturn(dummyIsModifierEquals);

    mocksControl.replay();

    BigDecimal pc50 = DataUtil.pc50ByNearestIntegerIfNotHighlyActive(mockC50Data,
                                                                     dummyRecordIdentifier);

    mocksControl.verify();

    assertNull(pc50); 

    mocksControl.reset();

    /*
     * If IC value and modifier '<' or pC value and modifier '>', returns null. 
     */
    dummyIsModifierEquals = false;
    expect(mockC50Data.isModifierEquals(ICPercent.PCT50)).andReturn(dummyIsModifierEquals);
    boolean dummyPCComparisonEffectiveGreaterModifier = true;
    expect(mockC50Data.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50))
          .andReturn(dummyPCComparisonEffectiveGreaterModifier);

    mocksControl.replay();

    pc50 = DataUtil.pc50ByNearestIntegerIfNotHighlyActive(mockC50Data, dummyRecordIdentifier);

    mocksControl.verify();

    assertNull(pc50);

    mocksControl.reset();

    /*
     * If IC value and modifier '>' or pC value and modifier '<', get nearest int.
     */
    expect(mockC50Data.isModifierEquals(ICPercent.PCT50)).andReturn(dummyIsModifierEquals);
    dummyPCComparisonEffectiveGreaterModifier = false;
    expect(mockC50Data.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50))
          .andReturn(dummyPCComparisonEffectiveGreaterModifier);
    BigDecimal dummyPC50Value = BigDecimal.ONE.add(new BigDecimal("0.5"));
    expect(mockC50Data.retrieveEquivalentPC50Value(ICPercent.PCT50)).andReturn(dummyPC50Value);

    mocksControl.replay();

    pc50 = DataUtil.pc50ByNearestIntegerIfNotHighlyActive(mockC50Data, dummyRecordIdentifier);
    mocksControl.verify();

    assertTrue(pc50.compareTo(BigDecimal.ONE) == 0);

    mocksControl.reset();

    expect(mockC50Data.isModifierEquals(ICPercent.PCT50)).andReturn(dummyIsModifierEquals);
    dummyPCComparisonEffectiveGreaterModifier = false;
    expect(mockC50Data.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50))
          .andReturn(dummyPCComparisonEffectiveGreaterModifier);
    dummyPC50Value = new BigDecimal("2");
    expect(mockC50Data.retrieveEquivalentPC50Value(ICPercent.PCT50)).andReturn(dummyPC50Value);

    mocksControl.replay();

    pc50 = DataUtil.pc50ByNearestIntegerIfNotHighlyActive(mockC50Data, dummyRecordIdentifier);
    mocksControl.verify();

    assertTrue(pc50.compareTo(BigDecimal.ONE) == 0);
  }

  @Test(expected=IllegalArgumentException.class)
  public void testPC50ByC50NearestIntegerFailsOnNullArg() {
    ICData dummyC50Data = null;
    String dummyRecordIdentifier = null;
    DataUtil.pc50ByC50NearestInteger(dummyC50Data, dummyRecordIdentifier);
  }

  @Test
  public void testPC50ByNearestInteger() throws NoSuchDataException {
    final IMocksControl mocksControl = createStrictControl();
    final ICData mockC50Data = mocksControl.createMock(ICData.class);
    final String dummyRecordIdentifier = "dummyRecordIdentifier";

    /*
     * If an equality modifier is present then nearest integer returns null.
     */
    boolean dummyIsModifierEquals = true;

    expect(mockC50Data.isModifierEquals(ICPercent.PCT50)).andReturn(dummyIsModifierEquals);

    mocksControl.replay();

    BigDecimal pc50 = DataUtil.pc50ByC50NearestInteger(mockC50Data, dummyRecordIdentifier);

    mocksControl.verify();

    assertNull(pc50); 

    mocksControl.reset();

    /*
     * If IC value and modifier '<' or pC value and modifier '>', get nearest int.
     */
    dummyIsModifierEquals = false;
    expect(mockC50Data.isModifierEquals(ICPercent.PCT50)).andReturn(dummyIsModifierEquals);
    boolean dummyPCComparisonEffectiveGreaterModifier = false;
    expect(mockC50Data.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50))
          .andReturn(dummyPCComparisonEffectiveGreaterModifier);
    final BigDecimal dummyPC50Value = BigDecimal.ONE;
    expect(mockC50Data.retrieveEquivalentPC50Value(ICPercent.PCT50)).andReturn(dummyPC50Value);

    mocksControl.replay();

    pc50 = DataUtil.pc50ByC50NearestInteger(mockC50Data, dummyRecordIdentifier);
    mocksControl.verify();

    assertTrue(dummyPC50Value.compareTo(pc50.add(BigDecimal.ONE)) == 0); 

    mocksControl.reset();

    /*
     * If IC value and modifier '>' or pC value and modifier '<', get nearest int.
     */
    expect(mockC50Data.isModifierEquals(ICPercent.PCT50)).andReturn(dummyIsModifierEquals);
    dummyPCComparisonEffectiveGreaterModifier = false;
    expect(mockC50Data.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50))
          .andReturn(dummyPCComparisonEffectiveGreaterModifier);
    expect(mockC50Data.retrieveEquivalentPC50Value(ICPercent.PCT50)).andReturn(dummyPC50Value);

    mocksControl.replay();

    pc50 = DataUtil.pc50ByC50NearestInteger(mockC50Data, dummyRecordIdentifier);
    mocksControl.verify();

    assertTrue(dummyPC50Value.compareTo(pc50.add(BigDecimal.ONE)) == 0); 
  }

  /**
   * Test the rounding to nearest integer process fails on null value.
   */
  @Test(expected=NullPointerException.class)
  public void testRoundingToNearestIntegerFailsOnNullValue() {
    DataUtil.roundToNearestInteger(true, null);
  }

  /**
   * Test rounding to nearest integer on whole number calculations.
   */
  @Test
  public void testRoundingToNearestIntegerOnWholeNumbers() {
    boolean modifierGreater = true;
    BigDecimal value = BigDecimal.ZERO;

    int result = DataUtil.roundToNearestInteger(modifierGreater, value);
    assertSame(1, result);
    modifierGreater = false;
    result = DataUtil.roundToNearestInteger(modifierGreater, value);
    assertSame(-1, result);

    value = BigDecimal.ONE;
    result = DataUtil.roundToNearestInteger(modifierGreater, value);
    assertSame(0, result);
    modifierGreater = true;
    result = DataUtil.roundToNearestInteger(modifierGreater, value);
    assertSame(2, result);

    value = BigDecimal.ZERO.subtract(BigDecimal.ONE);
    result = DataUtil.roundToNearestInteger(modifierGreater, value);
    assertSame(0, result);
    modifierGreater = false;
    result = DataUtil.roundToNearestInteger(modifierGreater, value);
    assertSame(-2, result);

  }

  /**
   * Test rounding to nearest integer on decimal number calculations.
   */
  @Test
  public void testRoundingToNearestIntegerOnDecimalNumbers() {
    boolean modifierGreater = true;
    BigDecimal value = new BigDecimal("0.5");

    int result = DataUtil.roundToNearestInteger(modifierGreater, value);
    assertSame(1, result);
    modifierGreater = false;
    result = DataUtil.roundToNearestInteger(modifierGreater, value);
    assertSame(0, result);

    value = new BigDecimal("1.2");
    result = DataUtil.roundToNearestInteger(modifierGreater, value);
    assertSame(1, result);
    modifierGreater = true;
    result = DataUtil.roundToNearestInteger(modifierGreater, value);
    assertSame(2, result);

    value = new BigDecimal("1.7");
    result = DataUtil.roundToNearestInteger(modifierGreater, value);
    assertSame(2, result);
    modifierGreater = false;
    result = DataUtil.roundToNearestInteger(modifierGreater, value);
    assertSame(1, result);

    value = new BigDecimal("-2.1");
    result = DataUtil.roundToNearestInteger(modifierGreater, value);
    assertSame(-3, result);
    modifierGreater = true;
    result = DataUtil.roundToNearestInteger(modifierGreater, value);
    assertSame(-2, result);
  }
}