/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.manager;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.dao.SimulationAssayDAO;
import uk.ac.ox.cs.epsrc.business_manager.entity.IonChannelValues;
import uk.ac.ox.cs.epsrc.business_manager.entity.PIC50Data;
import uk.ac.ox.cs.epsrc.business_manager.entity.SimulationAssay;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.IndividualIVS;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.InputValueSource;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.QSARIVS;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.SummaryIVS;
import uk.ac.ox.cs.epsrc.business_manager.value.object.workflow.OutcomeVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.util.AssayUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 * Unit test the SimulationAssayManagerImpl object.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { SimulationAssayManagerImpl.class, PIC50Data.class,
                   IonChannelValues.class, SimulationAssay.class } )
public class SimulationAssayManagerImplTest {

  private static final String dummyAssayNameBarracuda = AssayUtil.ASSAY_NAME_BARRACUDA;
  private static final String dummyAssayNameManualPatch = AssayUtil.ASSAY_NAME_MANUAL_PATCH;
  private static final String dummyAssayNameQSAR = AssayUtil.ASSAY_NAME_QSAR;
  private static final IonChannel dummyIonChannelCaV12 = IonChannel.CaV1_2;
  private static final IonChannel dummyIonChannelhERG = IonChannel.hERG;
  private static final IonChannel dummyIonChannelNaV15 = IonChannel.NaV1_5;
  private static final Long dummySimulationId = 4l;
  private static final String dummyIonChannelNameCaV12 = dummyIonChannelCaV12.toString();

  private IMocksControl mocksControl;
  
  private SimulationAssayDAO mockSimulationAssayDAO;
  private SimulationAssayManager simulationAssayManager;

  @Before
  public void setUp() {
    simulationAssayManager = new SimulationAssayManagerImpl();

    mocksControl = createStrictControl();

    mockSimulationAssayDAO = mocksControl.createMock(SimulationAssayDAO.class);

    ReflectionTestUtils.setField(simulationAssayManager,
                                 BusinessIdentifiers.COMPONENT_SIMULATIONASSAY_DAO,
                                 mockSimulationAssayDAO);
  }

  @Test
  public void testCreateTransient() throws Exception {
    // 1. No incoming data.
    boolean dummyInputDataGathering = false;
    final List<OutcomeVO> dummyQSAROutcomes = new ArrayList<OutcomeVO>();
    final List<OutcomeVO> dummyScreeningOutcomes = new ArrayList<OutcomeVO>();

    mocksControl.replay();

    List<SimulationAssay> created = simulationAssayManager.createTransient(dummyInputDataGathering,
                                                                           dummySimulationId,
                                                                           dummyQSAROutcomes,
                                                                           dummyScreeningOutcomes);

    mocksControl.verify();
    assertEquals(0, created.size());

    mocksControl.reset();

    /*
     * 2. An outcome present - emulating QSAR CaV1.2 data only.
     */
    final BigDecimal dummyDefaultHillCoefficient = null;
    final OutcomeVO mockQSARCaV12Outcome = mocksControl.createMock(OutcomeVO.class);
    dummyQSAROutcomes.add(mockQSARCaV12Outcome);
    final List<InputValueSource> dummyQSARCaV12IVSs = new ArrayList<InputValueSource>();
    final QSARIVS mockQSARCaV12IVS = mocksControl.createMock(QSARIVS.class);
    dummyQSARCaV12IVSs.add(mockQSARCaV12IVS);
    expect(mockQSARCaV12Outcome.getInputValueSources())
          .andReturn(dummyQSARCaV12IVSs);

    final BigDecimal dummyQSARCaV12PIC50 = BigDecimal.ONE;
    expect(mockQSARCaV12IVS.getpIC50()).andReturn(dummyQSARCaV12PIC50);
    final PIC50Data mockQSARCaV12PIC50Data = mocksControl.createMock(PIC50Data.class);
    expectNew(PIC50Data.class, dummyQSARCaV12PIC50, dummyDefaultHillCoefficient,
                               true, null)
             .andReturn(mockQSARCaV12PIC50Data);

    final AssayVO mockQSARCaV12Assay = mocksControl.createMock(AssayVO.class);
    expect(mockQSARCaV12Outcome.getAssay()).andReturn(mockQSARCaV12Assay);
    expect(mockQSARCaV12Outcome.getIonChannel()).andReturn(dummyIonChannelCaV12);
    expect(mockQSARCaV12Assay.getName()).andReturn(dummyAssayNameQSAR);

    // Traverse the structures, building up the simulation assays collection
    final IonChannelValues mockIonChannelValuesQSARCaV12 = mocksControl.createMock(IonChannelValues.class);
    Capture<String> captureIonChannelNameCaV12 = newCapture();
    Capture<String> captureAssayNameQSAR_1 = newCapture();
    Capture<List<PIC50Data>> capturePIC50Data = newCapture();
    expectNew(IonChannelValues.class, capture(captureIonChannelNameCaV12),
                                      capture(captureAssayNameQSAR_1),
                                      capture(capturePIC50Data))
             .andReturn(mockIonChannelValuesQSARCaV12);
    final SimulationAssay mockSimulationAssayQSARCaV12 = mocksControl.createMock(SimulationAssay.class);
    Capture<Long> captureSimulationId = newCapture();
    Capture<String> captureAssayNameQSAR_2 = newCapture();
    Capture<Set<IonChannelValues>> captureIonChannelValues = newCapture();
    expectNew(SimulationAssay.class, capture(captureSimulationId),
                                     capture(captureAssayNameQSAR_2),
                                     capture(captureIonChannelValues))
             .andReturn(mockSimulationAssayQSARCaV12);

    replayAll();
    mocksControl.replay();

    created = simulationAssayManager.createTransient(dummyInputDataGathering,
                                                     dummySimulationId,
                                                     dummyQSAROutcomes,
                                                     dummyScreeningOutcomes);

    verifyAll();
    mocksControl.verify();

    assertSame(1, created.size());

    final String capturedIonChannelNameCaV12 = captureIonChannelNameCaV12.getValue();
    final String capturedAssayNameQSAR_1 = captureAssayNameQSAR_1.getValue();
    final List<PIC50Data> capturedPIC50Data = capturePIC50Data.getValue();
    assertSame(dummyIonChannelNameCaV12, capturedIonChannelNameCaV12);
    assertSame(dummyAssayNameQSAR, capturedAssayNameQSAR_1);
    assertSame(1, capturedPIC50Data.size());
    assertEquals(mockQSARCaV12PIC50Data, capturedPIC50Data.get(0));

    final Long capturedSimulationId = captureSimulationId.getValue();
    final String capturedAssayNameQSAR_2 = captureAssayNameQSAR_2.getValue();
    final Set<IonChannelValues> capturedIonChannelValues = captureIonChannelValues.getValue();
    assertSame(dummySimulationId, capturedSimulationId);
    assertSame(dummyAssayNameQSAR, capturedAssayNameQSAR_2);
    assertSame(1, capturedIonChannelValues.size());
    assertSame(mockIonChannelValuesQSARCaV12,
               capturedIonChannelValues.iterator().next());

    resetAll();
    mocksControl.reset();

    /*
     * 3. Two outcomes present - emulating 1) QSAR CaV1.2 data, 2) QSAR NaV1.5 data.
     */
    dummyQSAROutcomes.clear();
    dummyScreeningOutcomes.clear();
    dummyQSARCaV12IVSs.clear();

    // Repeat the CaV1.2 QSAR as above.
    dummyQSAROutcomes.add(mockQSARCaV12Outcome);
    expect(mockQSARCaV12Outcome.getInputValueSources())
          .andReturn(dummyQSARCaV12IVSs);
    dummyQSARCaV12IVSs.add(mockQSARCaV12IVS);
    expect(mockQSARCaV12IVS.getpIC50()).andReturn(dummyQSARCaV12PIC50);
    expectNew(PIC50Data.class, dummyQSARCaV12PIC50, dummyDefaultHillCoefficient,
                               true, null)
             .andReturn(mockQSARCaV12PIC50Data);
    expect(mockQSARCaV12Outcome.getAssay()).andReturn(mockQSARCaV12Assay);
    expect(mockQSARCaV12Outcome.getIonChannel()).andReturn(dummyIonChannelCaV12);
    expect(mockQSARCaV12Assay.getName()).andReturn(dummyAssayNameQSAR);

    // Now the NaV1.5 QSAR.
    final OutcomeVO mockQSARNaV15Outcome = mocksControl.createMock(OutcomeVO.class);
    dummyQSAROutcomes.add(mockQSARNaV15Outcome);
    final List<InputValueSource> dummyQSARNav15IVSs = new ArrayList<InputValueSource>();
    final QSARIVS mockQSARNaV15IVS = mocksControl.createMock(QSARIVS.class);
    dummyQSARNav15IVSs.add(mockQSARNaV15IVS);
    expect(mockQSARNaV15Outcome.getInputValueSources())
          .andReturn(dummyQSARNav15IVSs);
    final BigDecimal dummyQSARNav15PIC50 = BigDecimal.ZERO;
    expect(mockQSARNaV15IVS.getpIC50()).andReturn(dummyQSARNav15PIC50);
    final PIC50Data mockQSARNaV15PIC50Data = mocksControl.createMock(PIC50Data.class);
    expectNew(PIC50Data.class, dummyQSARNav15PIC50, dummyDefaultHillCoefficient,
                               true, null)
             .andReturn(mockQSARNaV15PIC50Data);

    final AssayVO mockQSARNaV15Assay = mocksControl.createMock(AssayVO.class);
    expect(mockQSARNaV15Outcome.getAssay()).andReturn(mockQSARNaV15Assay);
    expect(mockQSARNaV15Outcome.getIonChannel()).andReturn(dummyIonChannelNaV15);
    expect(mockQSARNaV15Assay.getName()).andReturn(dummyAssayNameQSAR);

    // Traverse the structures - but we don't know the order of
    // structure.entrySet() traversal, i.e. CaV12 or NaV15 first!!!
    IonChannelValues mockIonChannelValues_1 = mocksControl.createMock(IonChannelValues.class);
    Capture<String> captureIonChannelName_1 = newCapture();
    Capture<String> captureAssayName_1_1 = newCapture();
    Capture<List<PIC50Data>> capturePIC50Data_1 = newCapture();
    expectNew(IonChannelValues.class, capture(captureIonChannelName_1),
                                      capture(captureAssayName_1_1),
                                      capture(capturePIC50Data_1))
             .andReturn(mockIonChannelValues_1);
    IonChannelValues mockIonChannelValues_2 = mocksControl.createMock(IonChannelValues.class);
    Capture<String> captureIonChannelName_2 = newCapture();
    Capture<String> captureAssayName_2_1 = newCapture();
    Capture<List<PIC50Data>> capturePIC50Data_2 = newCapture();
    expectNew(IonChannelValues.class, capture(captureIonChannelName_2),
                                      capture(captureAssayName_2_1),
                                      capture(capturePIC50Data_2))
             .andReturn(mockIonChannelValues_2);

    Capture<Long> captureSimulationId_1 = newCapture();
    Capture<String> captureAssayName_1_2 = newCapture();
    Capture<Set<IonChannelValues>> captureIonChannelValues_1 = newCapture();
    final SimulationAssay mockSimulationAssay = mocksControl.createMock(SimulationAssay.class);

    expectNew(SimulationAssay.class, capture(captureSimulationId_1),
                                     capture(captureAssayName_1_2),
                                     capture(captureIonChannelValues_1))
             .andReturn(mockSimulationAssay);

    replayAll();
    mocksControl.replay();

    created = simulationAssayManager.createTransient(dummyInputDataGathering,
                                                     dummySimulationId,
                                                     dummyQSAROutcomes,
                                                     dummyScreeningOutcomes);

    verifyAll();
    mocksControl.verify();

    assertSame(1, created.size());

    final String capturedIonChannelName_1 = captureIonChannelName_1.getValue();
    final String capturedIonChannelName_2 = captureIonChannelName_2.getValue();
    assertNotEquals(capturedIonChannelName_1, capturedIonChannelName_2);
    final String capturedAssayName_1_1 = captureAssayName_1_1.getValue();
    final String capturedAssayName_1_2 = captureAssayName_1_2.getValue();
    assertEquals(capturedAssayName_1_1, capturedAssayName_1_2);
    final List<PIC50Data> capturedPIC50Data_1 = capturePIC50Data_1.getValue();
    final List<PIC50Data> capturedPIC50Data_2 = capturePIC50Data_2.getValue();
    assertSame(1, capturedPIC50Data_1.size());
    assertSame(1, capturedPIC50Data_2.size());
    assertNotEquals(capturedPIC50Data_1.get(0), capturedPIC50Data_2.get(0));

    final Set<IonChannelValues> capturedIonChannelValues_1 = captureIonChannelValues_1.getValue();
    assertSame(2, capturedIonChannelValues_1.size());

    resetAll();
    mocksControl.reset();

    /* 4. Four outcomes present - emulating 1) MANUAL_PATCH summary hERG data,
                                            2) QSAR CaV1.2 data,
                                            3) QSAR NaV1.5 data,
                                            4) 3 Barracuda data. 2 individual hERG, 1 summary CaV1.2.
          InputDataGathering too! */
    dummyInputDataGathering = true;
    dummyQSAROutcomes.clear();
    dummyScreeningOutcomes.clear();
    dummyQSARCaV12IVSs.clear();
    dummyQSARNav15IVSs.clear();

    // QSAR data appears at the head of the outcomes list!
    // Repeat the CaV1.2 QSAR.
    dummyQSAROutcomes.add(mockQSARCaV12Outcome);
    dummyQSARCaV12IVSs.add(mockQSARCaV12IVS);
    expect(mockQSARCaV12Outcome.getInputValueSources())
          .andReturn(dummyQSARCaV12IVSs);
    expect(mockQSARCaV12IVS.getpIC50()).andReturn(dummyQSARCaV12PIC50);
    expectNew(PIC50Data.class, dummyQSARCaV12PIC50, dummyDefaultHillCoefficient,
                               true, null)
             .andReturn(mockQSARCaV12PIC50Data);
    expect(mockQSARCaV12Outcome.getAssay()).andReturn(mockQSARCaV12Assay);
    expect(mockQSARCaV12Outcome.getIonChannel()).andReturn(dummyIonChannelCaV12);
    expect(mockQSARCaV12Assay.getName()).andReturn(dummyAssayNameQSAR);

    // Repeat the NaV1.5 QSAR.
    dummyQSAROutcomes.add(mockQSARNaV15Outcome);
    dummyQSARNav15IVSs.add(mockQSARNaV15IVS);
    expect(mockQSARNaV15Outcome.getInputValueSources())
          .andReturn(dummyQSARNav15IVSs);
    expect(mockQSARNaV15IVS.getpIC50()).andReturn(dummyQSARNav15PIC50);
    expectNew(PIC50Data.class, dummyQSARNav15PIC50, dummyDefaultHillCoefficient,
                               true, null)
             .andReturn(mockQSARNaV15PIC50Data);
    expect(mockQSARNaV15Outcome.getAssay()).andReturn(mockQSARNaV15Assay);
    expect(mockQSARNaV15Outcome.getIonChannel()).andReturn(dummyIonChannelNaV15);
    expect(mockQSARNaV15Assay.getName()).andReturn(dummyAssayNameQSAR);

    // 4.1 New hERG Manual Patch Summary.
    final OutcomeVO mockMPhERGOutcome = mocksControl.createMock(OutcomeVO.class);
    dummyScreeningOutcomes.add(mockMPhERGOutcome);
    final List<InputValueSource> dummyMPhERGIVSs = new ArrayList<InputValueSource>();
    final SummaryIVS mockMPhERGIVS = mocksControl.createMock(SummaryIVS.class);
    dummyMPhERGIVSs.add(mockMPhERGIVS);
    expect(mockMPhERGOutcome.getInputValueSources())
          .andReturn(dummyMPhERGIVSs);

    final BigDecimal dummyMPhERGPIC50_1 = new BigDecimal("1.1");
    final BigDecimal dummyMPhERGPIC50_2 = new BigDecimal("2.2");
    final BigDecimal dummyMPhERGHill = new BigDecimal("3.3");
    final boolean dummyMPhERGOriginal = true;
    final int dummyMPhERGStrategyOrder = 5;

    final List<BigDecimal> dummyMPhERGPIC50s = new ArrayList<BigDecimal>();
    dummyMPhERGPIC50s.add(dummyMPhERGPIC50_1);
    dummyMPhERGPIC50s.add(dummyMPhERGPIC50_2);
    expect(mockMPhERGIVS.getpIC50s()).andReturn(dummyMPhERGPIC50s);
    expect(mockMPhERGIVS.getHillCoefficient()).andReturn(dummyMPhERGHill);
    expect(mockMPhERGIVS.isOriginalData()).andReturn(dummyMPhERGOriginal);
    expect(mockMPhERGIVS.retrieveStrategyOrder())
          .andReturn(dummyMPhERGStrategyOrder);

    final PIC50Data mockMPhERGPIC50_1 = mocksControl.createMock(PIC50Data.class);
    expectNew(PIC50Data.class, dummyMPhERGPIC50_1, dummyMPhERGHill,
                               dummyMPhERGOriginal, dummyMPhERGStrategyOrder)
             .andReturn(mockMPhERGPIC50_1);
    final PIC50Data mockMPhERGPIC50_2 = mocksControl.createMock(PIC50Data.class);
    expectNew(PIC50Data.class, dummyMPhERGPIC50_2, dummyMPhERGHill,
                               dummyMPhERGOriginal, dummyMPhERGStrategyOrder)
             .andReturn(mockMPhERGPIC50_2);

    final AssayVO mockMPhERGAssay = mocksControl.createMock(AssayVO.class);
    expect(mockMPhERGOutcome.getAssay()).andReturn(mockMPhERGAssay);
    expect(mockMPhERGOutcome.getIonChannel()).andReturn(dummyIonChannelhERG);
    expect(mockMPhERGAssay.getName()).andReturn(dummyAssayNameManualPatch);

    // 4.4a New Barracuda individual
    final OutcomeVO mockBcdahERGOutcome = mocksControl.createMock(OutcomeVO.class);
    dummyScreeningOutcomes.add(mockBcdahERGOutcome);
    final IndividualIVS mockBcdahERGIVS_1 = mocksControl.createMock(IndividualIVS.class);
    final IndividualIVS mockBcdahERGIVS_2 = mocksControl.createMock(IndividualIVS.class);
    final List<InputValueSource> dummyBcdahERGIVSs = new ArrayList<InputValueSource>();
    dummyBcdahERGIVSs.add(mockBcdahERGIVS_1);
    dummyBcdahERGIVSs.add(mockBcdahERGIVS_2);
    expect(mockBcdahERGOutcome.getInputValueSources())
          .andReturn(dummyBcdahERGIVSs);
    // Barracuda hERG Individual PIC50 1
    final BigDecimal dummyBcdahERGPIC50_1 = new BigDecimal("4.4");
    final BigDecimal dummyBcdahERGHill_1 = new BigDecimal("5.5");
    final boolean dummyBcdahERGOriginal_1 = false;
    final int dummyBcdahERGStrategyOrder_1 = 3;
    expect(mockBcdahERGIVS_1.getpIC50()).andReturn(dummyBcdahERGPIC50_1);
    expect(mockBcdahERGIVS_1.getHillCoefficient()).andReturn(dummyBcdahERGHill_1);
    expect(mockBcdahERGIVS_1.isOriginalData()).andReturn(dummyBcdahERGOriginal_1);
    expect(mockBcdahERGIVS_1.retrieveStrategyOrder())
          .andReturn(dummyBcdahERGStrategyOrder_1);
    final PIC50Data mockBcdahERGPIC50Data_1 = mocksControl.createMock(PIC50Data.class);
    expectNew(PIC50Data.class, dummyBcdahERGPIC50_1, dummyBcdahERGHill_1,
                               dummyBcdahERGOriginal_1,
                               dummyBcdahERGStrategyOrder_1)
             .andReturn(mockBcdahERGPIC50Data_1);
    // Barracuda hERG Individual PIC50 2
    final BigDecimal dummyBcdahERGPIC50_2 = new BigDecimal("6.6");
    final BigDecimal dummyBcdahERGHill_2= new BigDecimal("7.7");
    final boolean dummyBcdahERGOriginal_2 = true;
    final int dummyBcdahERGStrategyOrder_2 = 2;
    expect(mockBcdahERGIVS_2.getpIC50()).andReturn(dummyBcdahERGPIC50_2);
    expect(mockBcdahERGIVS_2.getHillCoefficient()).andReturn(dummyBcdahERGHill_2);
    expect(mockBcdahERGIVS_2.isOriginalData()).andReturn(dummyBcdahERGOriginal_2);
    expect(mockBcdahERGIVS_2.retrieveStrategyOrder())
          .andReturn(dummyBcdahERGStrategyOrder_2);
    final PIC50Data mockBcdahERGPIC50Data_2 = mocksControl.createMock(PIC50Data.class);
    expectNew(PIC50Data.class, dummyBcdahERGPIC50_2, dummyBcdahERGHill_2,
                               dummyBcdahERGOriginal_2,
                               dummyBcdahERGStrategyOrder_2)
             .andReturn(mockBcdahERGPIC50Data_2);

    final AssayVO mockBcdahERGAssay = mocksControl.createMock(AssayVO.class);
    expect(mockBcdahERGOutcome.getAssay()).andReturn(mockBcdahERGAssay);
    expect(mockBcdahERGOutcome.getIonChannel()).andReturn(dummyIonChannelhERG);
    expect(mockBcdahERGAssay.getName()).andReturn(dummyAssayNameBarracuda);

    // 4.4b Barracuda Summary CaV
    final OutcomeVO mockBcdaCaV12Outcome = mocksControl.createMock(OutcomeVO.class);
    dummyScreeningOutcomes.add(mockBcdaCaV12Outcome);
    final SummaryIVS mockBcdaCaV12IVS = mocksControl.createMock(SummaryIVS.class);
    final List<InputValueSource> dummyBcdaCaV12IVSs = new ArrayList<InputValueSource>();
    dummyBcdaCaV12IVSs.add(mockBcdaCaV12IVS);
    expect(mockBcdaCaV12Outcome.getInputValueSources())
          .andReturn(dummyBcdaCaV12IVSs);
    // Barracuda CaV Summary PIC50
    final BigDecimal dummyBcdaCaV12PIC50_1 = new BigDecimal("8.8");
    final BigDecimal dummyBcdaCaV12PIC50_2 = new BigDecimal("9.9");
    final List<BigDecimal> dummyBcdaCaV12PIC50s = new ArrayList<BigDecimal>();
    dummyBcdaCaV12PIC50s.add(dummyBcdaCaV12PIC50_1);
    dummyBcdaCaV12PIC50s.add(dummyBcdaCaV12PIC50_2);
    final BigDecimal dummyBcdaCaV12Hill = new BigDecimal("0.1");
    final boolean dummyBcdaCaV12Original = true;
    final int dummyBcdaCaV12StrategyOrder = 9;
    expect(mockBcdaCaV12IVS.getpIC50s()).andReturn(dummyBcdaCaV12PIC50s);
    expect(mockBcdaCaV12IVS.getHillCoefficient()).andReturn(dummyBcdaCaV12Hill);
    expect(mockBcdaCaV12IVS.isOriginalData()).andReturn(dummyBcdaCaV12Original);
    expect(mockBcdaCaV12IVS.retrieveStrategyOrder())
          .andReturn(dummyBcdaCaV12StrategyOrder);
    final PIC50Data mockBcdaCaV12PIC50Data_1 = mocksControl.createMock(PIC50Data.class);
    expectNew(PIC50Data.class, dummyBcdaCaV12PIC50_1, dummyBcdaCaV12Hill,
                               dummyBcdaCaV12Original,
                               dummyBcdaCaV12StrategyOrder)
             .andReturn(mockBcdaCaV12PIC50Data_1);
    final PIC50Data mockBcdaCaV12PIC50Data_2 = mocksControl.createMock(PIC50Data.class);
    expectNew(PIC50Data.class, dummyBcdaCaV12PIC50_2, dummyBcdaCaV12Hill,
                               dummyBcdaCaV12Original,
                               dummyBcdaCaV12StrategyOrder)
             .andReturn(mockBcdaCaV12PIC50Data_2);

    final AssayVO mockBcdaCaV12Assay = mocksControl.createMock(AssayVO.class);
    expect(mockBcdaCaV12Outcome.getAssay()).andReturn(mockBcdaCaV12Assay);
    expect(mockBcdaCaV12Outcome.getIonChannel()).andReturn(dummyIonChannelCaV12);
    expect(mockBcdaCaV12Assay.getName()).andReturn(dummyAssayNameBarracuda);

    IonChannelValues mockIonChannelValues = mocksControl.createMock(IonChannelValues.class);
    expectNew(IonChannelValues.class, isA(String.class),     // ion channel name
                                      isA(String.class),     // assay name
                                      isA(List.class))       // pIC50 data.
             .andReturn(mockIonChannelValues).times(5);      // pIC50 collection for each ion channel / assay combination.

    expectNew(SimulationAssay.class, isA(Long.class),        // simulation id
                                     isA(String.class),      // assay name
                                     anyObject(List.class))  // ion channel values
             .andReturn(mockSimulationAssay).times(3);       // Ion Channel Values collection for each assay.

    replayAll();
    mocksControl.replay();

    created = simulationAssayManager.createTransient(dummyInputDataGathering,
                                                     dummySimulationId,
                                                     dummyQSAROutcomes,
                                                     dummyScreeningOutcomes);

    verifyAll();
    mocksControl.verify();

    resetAll();
    mocksControl.reset();
  }

  @Test
  public void testPersistSimulationAssays() {
    final List<SimulationAssay> dummyTransientSimulationAssays = new ArrayList<SimulationAssay>();
    final SimulationAssay mockSimulationAssay = mocksControl.createMock(SimulationAssay.class);
    dummyTransientSimulationAssays.add(mockSimulationAssay);
    final SimulationAssay mockStoredSimulationAssay = mocksControl.createMock(SimulationAssay.class);

    expect(mockSimulationAssayDAO.store(mockSimulationAssay))
          .andReturn(mockStoredSimulationAssay);

    mocksControl.replay();

    final List<SimulationAssay> persisted = simulationAssayManager.persistSimulationAssays(dummyTransientSimulationAssays);

    mocksControl.verify();
    assertTrue(persisted.contains(mockStoredSimulationAssay));
  }

  @Test
  public void testSetDefaultHillCoefficient() throws Exception {
    final boolean dummyInputDataGathering = false;
    final BigDecimal dummyDefaultHillCoefficient = BigDecimal.ONE;
    ((SimulationAssayManagerImpl) simulationAssayManager).setDefaultHillCoefficient(dummyDefaultHillCoefficient);

    final List<OutcomeVO> dummyQSAROutcomes = new ArrayList<OutcomeVO>();
    final List<OutcomeVO> dummyScreeningOutcomes = new ArrayList<OutcomeVO>();

    final OutcomeVO mockQSARCaV12Outcome = mocksControl.createMock(OutcomeVO.class);
    dummyQSAROutcomes.add(mockQSARCaV12Outcome);
    final List<InputValueSource> dummyQSARCaV12IVSs = new ArrayList<InputValueSource>();
    final QSARIVS mockQSARCaV12IVS = mocksControl.createMock(QSARIVS.class);
    dummyQSARCaV12IVSs.add(mockQSARCaV12IVS);
    expect(mockQSARCaV12Outcome.getInputValueSources())
          .andReturn(dummyQSARCaV12IVSs);

    final BigDecimal dummyQSARCaV12PIC50 = BigDecimal.ONE;
    expect(mockQSARCaV12IVS.getpIC50()).andReturn(dummyQSARCaV12PIC50);
    final PIC50Data mockQSARCaV12PIC50Data = mocksControl.createMock(PIC50Data.class);
    expectNew(PIC50Data.class, dummyQSARCaV12PIC50, dummyDefaultHillCoefficient,
                               true, null)
             .andReturn(mockQSARCaV12PIC50Data);

    final AssayVO mockQSARCaV12Assay = mocksControl.createMock(AssayVO.class);
    expect(mockQSARCaV12Outcome.getAssay()).andReturn(mockQSARCaV12Assay);
    expect(mockQSARCaV12Outcome.getIonChannel()).andReturn(dummyIonChannelCaV12);
    expect(mockQSARCaV12Assay.getName()).andReturn(dummyAssayNameQSAR);

    // Traverse the structures, building up the simulation assays collection
    final IonChannelValues mockIonChannelValuesQSARCaV12 = mocksControl.createMock(IonChannelValues.class);
    expectNew(IonChannelValues.class, isA(String.class), isA(String.class),
                                      isA(List.class))
             .andReturn(mockIonChannelValuesQSARCaV12);
    final SimulationAssay mockSimulationAssayQSARCaV12 = mocksControl.createMock(SimulationAssay.class);
    expectNew(SimulationAssay.class, isA(Long.class), isA(String.class),
                                     anyObject(List.class))
             .andReturn(mockSimulationAssayQSARCaV12);

    replayAll();
    mocksControl.replay();

    simulationAssayManager.createTransient(dummyInputDataGathering,
                                           dummySimulationId, dummyQSAROutcomes,
                                           dummyScreeningOutcomes);

    verifyAll();
    mocksControl.verify();

    resetAll();
    mocksControl.reset();
  }
}