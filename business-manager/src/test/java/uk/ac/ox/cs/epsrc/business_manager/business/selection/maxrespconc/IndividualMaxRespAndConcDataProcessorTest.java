/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.selection.maxrespconc;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import uk.ac.ox.cs.epsrc.business_manager.business.selection.maxrespconc.IndividualMaxRespAndConcDataProcessor;
import uk.ac.ox.cs.epsrc.business_manager.business.selection.maxrespconc.MaxRespDataSelector;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.individual.MaxRespConcData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.IndividualProcessing;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;

/**
 * Unit test the max resp / max resp conc data processing.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(MaxRespDataSelector.class)
public class IndividualMaxRespAndConcDataProcessorTest {

  private IndividualMaxRespAndConcDataProcessor individualMaxRespAndConcDataProcessor;

  @Before
  public void setUp() {
    individualMaxRespAndConcDataProcessor = new IndividualMaxRespAndConcDataProcessor();
  }

  /**
   * Test passing null fails
   * @throws InvalidValueException 
   */
  @Test(expected=IllegalArgumentException.class)
  public void testIndividualMaxRespAndConcFailOnNull() throws InvalidValueException {
    IndividualProcessing dummyIndividualProcessing = null;
    individualMaxRespAndConcDataProcessor.individualDataMaxRespAndConc(dummyIndividualProcessing);
  }

  /**
   * Test passing valid individual data success.
   * @throws InvalidValueException 
   */
  @Test
  public void testIndividualMaxRespAndConcSuccess() throws InvalidValueException {
    IMocksControl mocksControl = createStrictControl();
    IndividualProcessing mockIndividualProcessing = mocksControl.createMock(IndividualProcessing.class);
    IndividualDataRecord mockIndividualDataRecord = mocksControl.createMock(IndividualDataRecord.class);
    List<MaxRespConcData> dummyEligibleMaxRespData = new ArrayList<MaxRespConcData>();
    expect(mockIndividualProcessing.getIndividualData()).andReturn(mockIndividualDataRecord);
    expect(mockIndividualDataRecord.eligibleMaxRespData()).andReturn(dummyEligibleMaxRespData);
    mockStatic(MaxRespDataSelector.class);
    expect(MaxRespDataSelector.chooseMaxRespData(dummyEligibleMaxRespData)).andReturn(null);

    replayAll();
    mocksControl.replay();

    MaxRespConcData selected = individualMaxRespAndConcDataProcessor.individualDataMaxRespAndConc(mockIndividualProcessing);

    verifyAll();
    mocksControl.verify();

    assertNull(selected);

    resetAll();
    mocksControl.reset();

    expect(mockIndividualProcessing.getIndividualData()).andReturn(mockIndividualDataRecord);
    expect(mockIndividualDataRecord.eligibleMaxRespData()).andReturn(dummyEligibleMaxRespData);
    mockStatic(MaxRespDataSelector.class);
    final MaxRespConcData mockMaxRespConcData = mocksControl.createMock(MaxRespConcData.class);
    expect(MaxRespDataSelector.chooseMaxRespData(dummyEligibleMaxRespData)).andReturn(mockMaxRespConcData);

    replayAll();
    mocksControl.replay();

    selected = individualMaxRespAndConcDataProcessor.individualDataMaxRespAndConc(mockIndividualProcessing);

    verifyAll();
    mocksControl.verify();

    assertNotNull(selected);
  }
}