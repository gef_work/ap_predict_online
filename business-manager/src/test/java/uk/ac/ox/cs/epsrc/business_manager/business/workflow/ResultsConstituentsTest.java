/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.workflow;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.selection.c50.C50DataSelector;
import uk.ac.ox.cs.epsrc.business_manager.business.workflow.WorkflowProcessor;
import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.service.SimulationService;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.IndividualC50DataStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.summary.SummaryOnlyC50DataStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.NoSuchDataException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;

/**
 * Unit test that workflow processing results conform to convention.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { C50DataSelector.class } )
public class ResultsConstituentsTest {
  private BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate;
  private Configuration mockConfiguration;
  private IMocksControl mocksControl;
  private SimulationService mockSimulationService;
  private WorkflowProcessor workflowProcessor;
  private final Long dummySimulationId = 4L;
  private ProcessingType dummyProcessingType = ProcessingType.INPUT_DATA_GATHERING;
  private final String dummyCompoundIdentifier = "dummyCompoundIdentifier";

  @Before
  public void setUp() {
    workflowProcessor = new WorkflowProcessor();
    mocksControl = createStrictControl();
    mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    mockConfiguration = mocksControl.createMock(Configuration.class);
    mockSimulationService = mocksControl.createMock(SimulationService.class);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_CONFIGURATION,
                                 mockConfiguration);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE,
                                 mockSimulationService);
  }

  @Test
  public void testDoubleSummaryResultFail() {
    // Impossible to test as SummaryOnlyC50DataStrategy code breaks from strategy iteration!
  }

  @Test
  public void testMixedResultFail() throws InvalidValueException, NoSuchDataException {
    final ICPercent dummyPctIC = ICPercent.PCT50;

    final SiteDataHolder mockSiteDataHolder = mocksControl.createMock(SiteDataHolder.class);
    final AssayVO mockAssayVO = mocksControl.createMock(AssayVO.class);
    expect(mockSiteDataHolder.getAssay()).andReturn(mockAssayVO);
    final IonChannel dummyIonChannel = IonChannel.hERG;
    expect(mockSiteDataHolder.getIonChannel()).andReturn(dummyIonChannel);
    final String dummyAssayName = "dummyAssayName";
    expect(mockAssayVO.getName()).andReturn(dummyAssayName);

    final SummaryDataRecord mockSummaryDataRecord = mocksControl.createMock(SummaryDataRecord.class);
    expect(mockSiteDataHolder.getSummaryDataRecord(false)).andReturn(mockSummaryDataRecord);
    final String dummySummaryDataRecordId = "dummySummaryDataRecordId";
    expect(mockSummaryDataRecord.getId()).andReturn(dummySummaryDataRecordId);

    // Create the mock PC50 evaluation strategies to iterate through.
    final Simulation mockInputDataGatheringSimulation = mocksControl.createMock(Simulation.class);
    final String dummyInvocationOrdersCSV = "";
    expect(mockInputDataGatheringSimulation.getPc50EvaluationStrategies()).andReturn(dummyInvocationOrdersCSV);
    final List<PC50EvaluationStrategy> dummyPC50EvaluationStrategies = new ArrayList<PC50EvaluationStrategy>();
    final IndividualC50DataStrategy mockIndividualC50DataStrategy = mocksControl.createMock(TestIndividualC50DataStrategy.class);
    final SummaryOnlyC50DataStrategy mockSummaryOnlyC50DataStrategy = mocksControl.createMock(TestSummaryOnlyC50DataStrategy.class);
    dummyPC50EvaluationStrategies.add((PC50EvaluationStrategy) mockIndividualC50DataStrategy);
    dummyPC50EvaluationStrategies.add((PC50EvaluationStrategy) mockSummaryOnlyC50DataStrategy);

    expect(mockConfiguration.retrieveStrategiesByCSV(dummyInvocationOrdersCSV)).andReturn(dummyPC50EvaluationStrategies);

    final String dummyDoseResponseFittingStrategy = "dummyDoseResponseFittingStrategy";
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingStrategy()).andReturn(dummyDoseResponseFittingStrategy);
    final boolean isDoseResponseFittingRounding = true;
    expect(mockInputDataGatheringSimulation.isDoseResponseFittingRounding()).andReturn(isDoseResponseFittingRounding);
    final Float dummyDoseResponseFittingHillMax = 0.5F;
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingHillMax()).andReturn(dummyDoseResponseFittingHillMax);
    final Float dummyDoseResponseFittingHillMin = 2F;
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingHillMin()).andReturn(dummyDoseResponseFittingHillMin);

    final Map<IndividualDataRecord, List<DoseResponseDataRecord>> mockNonSummaryData = 
          new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();
    final IndividualDataRecord mockIndividualDataRecord = mocksControl.createMock(IndividualDataRecord.class);
    mockNonSummaryData.put(mockIndividualDataRecord, null);
    expect(mockSiteDataHolder.getNonSummaryDataRecords()).andReturn(mockNonSummaryData);

    // Enter IndividualPC50s
    final List<ICData> dummySummaryEligibleC50Data = new ArrayList<ICData>();
    expect(mockSummaryDataRecord.eligibleICData()).andReturn(dummySummaryEligibleC50Data);
    final ICData mockSummaryC50Data = mocksControl.createMock(ICData.class);
    mockStatic(C50DataSelector.class);
    expect(C50DataSelector.selectFrom(dummySummaryEligibleC50Data)).andReturn(mockSummaryC50Data);
    expect(mockSummaryC50Data.hasICPctData(dummyPctIC)).andReturn(true);
    // By specifying an additional individual record in count an anonymous individual PC50 is
    //   created which the summary strategy will try, erroneously, to populate.
    final int dummyNonSummaryRecordCount = mockNonSummaryData.size() + 1;
    final Integer dummyCountOfRecordsUsedToDetermineC50Value = dummyNonSummaryRecordCount;
    expect(mockSummaryC50Data.retrieveCountOfRecordsUsedToDetermineICValue(ICPercent.PCT50)).andReturn(dummyCountOfRecordsUsedToDetermineC50Value);
    final Integer dummyCountOfAvailableRecords = dummyNonSummaryRecordCount;
    expect(mockSummaryC50Data.retrieveCountOfAvailableRecords(ICPercent.PCT50)).andReturn(dummyCountOfAvailableRecords);
    final String dummyIndividualDataRecordId = "dummyIndividualDataRecord";
    expect(mockIndividualDataRecord.getId()).andReturn(dummyIndividualDataRecordId);
    // Exit IndividualPC50s

    // Enter pIC50-determining strategy iteration.
    final Integer dummyDefaultInvocationOrder1 = 1;
    final Integer dummyDefaultInvocationOrder2 = 2;
    final String dummyDescription1 = "dummyDescription1";
    final String dummyDescription2 = "dummyDescription2";

    /*
     * Strategy 1 .... a successful individual data C50 retrieval
     */
    expect(((PC50EvaluationStrategy) mockIndividualC50DataStrategy).getOrder()).andReturn(dummyDefaultInvocationOrder1);
    expect(((PC50EvaluationStrategy) mockIndividualC50DataStrategy).getDescription()).andReturn(dummyDescription1);

    expect(mockIndividualDataRecord.getId()).andReturn(dummyIndividualDataRecordId);
    // Enter IndividualC50CacheManager.getICData
    expect(mockIndividualDataRecord.getId()).andReturn(dummyIndividualDataRecordId);
    final List<ICData> dummyIndividualDataRecordEligibleC50Data = new ArrayList<ICData>();
    expect(mockIndividualDataRecord.eligibleICData()).andReturn(dummyIndividualDataRecordEligibleC50Data);
    final ICData mockSelectedIndividualDataRecordC50Data = mocksControl.createMock(ICData.class);
    expect(C50DataSelector.selectFrom(dummyIndividualDataRecordEligibleC50Data))
          .andReturn(mockSelectedIndividualDataRecordC50Data);
    // Exit IndividualC50CacheManager.getICData

    final BigDecimal dummyDataRecordC50DataPIC50 = BigDecimal.ONE;
    expect(mockIndividualC50DataStrategy.evaluatePC50(isA(ICData.class), isA(String.class)))
          .andReturn(dummyDataRecordC50DataPIC50);
    final BigDecimal dummyDataRecordEligibleHill = new BigDecimal("0.777");
    expect(mockIndividualDataRecord.eligibleHillCoefficient()).andReturn(dummyDataRecordEligibleHill);

    /*
     * Strategy 2 .... a successful summary data C50 retrieval
     */
    expect(((PC50EvaluationStrategy) mockSummaryOnlyC50DataStrategy).getOrder()).andReturn(dummyDefaultInvocationOrder2);
    expect(((PC50EvaluationStrategy) mockSummaryOnlyC50DataStrategy).getDescription()).andReturn(dummyDescription2);

    final BigDecimal dummyStrategyEvaluatedPC50 = BigDecimal.ONE;
    expect(mockSummaryOnlyC50DataStrategy.evaluatePC50(isA(SiteDataHolder.class), isA(String.class)))
           .andReturn(dummyStrategyEvaluatedPC50);
    final BigDecimal dummySummaryDataRecordIndividualHill = new BigDecimal("0.89");
    expect(mockSummaryDataRecord.eligibleHillCoefficient()).andReturn(dummySummaryDataRecordIndividualHill);

    replayAll();
    mocksControl.replay();

    try { 
      workflowProcessor.processWorkflow(dummyCompoundIdentifier, dummyProcessingType,
                                        dummySimulationId, mockInputDataGatheringSimulation,
                                        mockSiteDataHolder);
      fail("Should not permit a summary result to coexist with individual results.");
    } catch (UnsupportedOperationException e) {}
  }
}