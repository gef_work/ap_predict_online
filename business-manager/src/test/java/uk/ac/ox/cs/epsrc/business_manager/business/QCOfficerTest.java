/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.QCOfficer;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.Provenance;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.QualityControlFailureVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;

/**
 * Unit test the Quality Control Officer.
 *
 * @author geoff
 */
public class QCOfficerTest {

  private BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate;
  private IMocksControl mocksControl;
  private final Long dummySimulationId = 1L;
  private QCOfficer qcOfficer;
  private final String dummyCompoundIdentifier = "dummyCompoundIdentifier";

  @Before
  public void setUp() {
    qcOfficer = new QCOfficer();
    mocksControl = createStrictControl();
    mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    ReflectionTestUtils.setField(qcOfficer, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
  }

  @Test(expected=IllegalArgumentException.class)
  public void testFailsOnNullArgument() throws IllegalArgumentException, InvalidValueException {
    ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    Simulation mockSimulation = mocksControl.createMock(Simulation.class);
    qcOfficer.removeQCFailedData(dummyCompoundIdentifier, dummyProcessingType, dummySimulationId,
                                 mockSimulation, null);
  }

  @Test
  public void testRemoveQCFailedDataWhenNoSummaryOrIndividualData() throws IllegalArgumentException,
                                                                           InvalidValueException {
    ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    AssayVO mockAssayVO = mocksControl.createMock(AssayVO.class);
    Simulation mockSimulation = mocksControl.createMock(Simulation.class);
    SiteDataHolder mockSiteDataHolder = mocksControl.createMock(SiteDataHolder.class);

    expect(mockSiteDataHolder.getAssay()).andReturn(mockAssayVO);
    final String mockAssayName = "mockAssayName";
    expect(mockAssayVO.getName()).andReturn(mockAssayName);
    final IonChannel dummyIonChannel = IonChannel.CaV1_2;
    expect(mockSiteDataHolder.getIonChannel()).andReturn(dummyIonChannel);
    // Emulate no summary or individual data record available.
    expect(mockSiteDataHolder.getSummaryDataRecord(false)).andReturn(null);
    expect(mockSiteDataHolder.getNonSummaryDataRecords()).andReturn(new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>());

    mocksControl.replay();

    try {
      qcOfficer.removeQCFailedData(dummyCompoundIdentifier, dummyProcessingType, dummySimulationId,
                                   mockSimulation, mockSiteDataHolder);
      fail("Should not have allowed a no-summary and no-individual situation.");
    } catch (IllegalArgumentException e) {
      // success.
    }

    mocksControl.verify();
  }

  @Test
  public void testRemoveQCFailedDataWhenSummaryOnlyValidData() throws IllegalArgumentException,
                                                                      InvalidValueException {
    ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    AssayVO mockAssayVO = mocksControl.createMock(AssayVO.class);
    Simulation mockSimulation = mocksControl.createMock(Simulation.class);
    SiteDataHolder mockSiteDataHolder = mocksControl.createMock(SiteDataHolder.class);

    expect(mockSiteDataHolder.getAssay()).andReturn(mockAssayVO);
    final String mockAssayName = "mockAssayName";
    expect(mockAssayVO.getName()).andReturn(mockAssayName);
    final IonChannel dummyIonChannel = IonChannel.CaV1_2;
    expect(mockSiteDataHolder.getIonChannel()).andReturn(dummyIonChannel);

    final SummaryDataRecord mockSummaryDataRecord = mocksControl.createMock(SummaryDataRecord.class);
    expect(mockSiteDataHolder.getSummaryDataRecord(false)).andReturn(mockSummaryDataRecord);
    // Emulate no individual data record.
    // Emulated data appearing valid (by returning null).
    expect(mockSummaryDataRecord.isMarkedInvalid()).andReturn(null);

    expect(mockSiteDataHolder.getNonSummaryDataRecords()).andReturn(new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>());

    mocksControl.replay();

    SiteDataHolder siteDataHolder = qcOfficer.removeQCFailedData(dummyCompoundIdentifier,
                                                                 dummyProcessingType,
                                                                 dummySimulationId,
                                                                 mockSimulation, mockSiteDataHolder);

    mocksControl.verify();
  }

  @Test
  public void testRemoveQCFailedDataWhenSummaryOnlyInvalidData() throws IllegalArgumentException,
                                                                        InvalidValueException {
    ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    AssayVO mockAssayVO = mocksControl.createMock(AssayVO.class);
    Simulation mockSimulation = mocksControl.createMock(Simulation.class);
    SiteDataHolder mockSiteDataHolder = mocksControl.createMock(SiteDataHolder.class);

    expect(mockSiteDataHolder.getAssay()).andReturn(mockAssayVO);
    final String mockAssayName = "mockAssayName";
    expect(mockAssayVO.getName()).andReturn(mockAssayName);
    final IonChannel dummyIonChannel = IonChannel.CaV1_2;
    expect(mockSiteDataHolder.getIonChannel()).andReturn(dummyIonChannel);

    final SummaryDataRecord mockSummaryDataRecord = mocksControl.createMock(SummaryDataRecord.class);
    expect(mockSiteDataHolder.getSummaryDataRecord(false)).andReturn(mockSummaryDataRecord);
    // Emulate no individual data record.
    final QualityControlFailureVO mockQualityControlFailureVO = mocksControl.createMock(QualityControlFailureVO.class);
    // Emulated data appearing invalid (by returning an object).
    expect(mockSummaryDataRecord.isMarkedInvalid()).andReturn(mockQualityControlFailureVO);
    final String dummyQCFailureReason = "dummyQCFailureReason";
    expect(mockQualityControlFailureVO.getReason()).andReturn(dummyQCFailureReason);

    final Capture<Progress> dummyOverallProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress));

    expect(mockSiteDataHolder.getNonSummaryDataRecords()).andReturn(new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>());

    mocksControl.replay();

    try {
      qcOfficer.removeQCFailedData(dummyCompoundIdentifier, dummyProcessingType, dummySimulationId,
                                   mockSimulation, mockSiteDataHolder);
      fail("Should not have allowed a no-summary and no-individual situation.");
    } catch (IllegalArgumentException e) {
      // success.
    }

    mocksControl.verify();

    OverallProgress capturedOverallProgress = (OverallProgress) dummyOverallProgress.getValue();
    assertEquals(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertNotNull(capturedOverallProgress.getText());
    assertSame(InformationLevel.DEBUG, capturedOverallProgress.getLevel());
  }

  @Test
  public void testRemoveQCFailedDataWhenIndividualOnlyValidData() throws IllegalArgumentException,
                                                                      InvalidValueException {
    ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    AssayVO mockAssayVO = mocksControl.createMock(AssayVO.class);
    Simulation mockSimulation = mocksControl.createMock(Simulation.class);
    SiteDataHolder mockSiteDataHolder = mocksControl.createMock(SiteDataHolder.class);

    expect(mockSiteDataHolder.getAssay()).andReturn(mockAssayVO);
    final String mockAssayName = "mockAssayName";
    expect(mockAssayVO.getName()).andReturn(mockAssayName);
    final IonChannel dummyIonChannel = IonChannel.CaV1_2;
    expect(mockSiteDataHolder.getIonChannel()).andReturn(dummyIonChannel);

    // Emulate no summary data record.
    expect(mockSiteDataHolder.getSummaryDataRecord(false)).andReturn(null);
    // Emulate individual data record.
    final Map<IndividualDataRecord, List<DoseResponseDataRecord>> dummyNonSummaryDataRecords =
          new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();
    final IndividualDataRecord mockIndividualDataRecord = mocksControl.createMock(IndividualDataRecord.class);
    dummyNonSummaryDataRecords.put(mockIndividualDataRecord, new ArrayList<DoseResponseDataRecord>());
    expect(mockSiteDataHolder.getNonSummaryDataRecords()).andReturn(dummyNonSummaryDataRecords);
    // Emulate no quality control failure.
    expect(mockIndividualDataRecord.isMarkedInvalid()).andReturn(null);
    

    mocksControl.replay();

    SiteDataHolder siteDataHolder = qcOfficer.removeQCFailedData(dummyCompoundIdentifier,
                                                                 dummyProcessingType,
                                                                 dummySimulationId,
                                                                 mockSimulation, mockSiteDataHolder);

    mocksControl.verify();
  }

  @Test
  public void testRemoveQCFailedDataWhenIndividualOnlyInvalidData() throws IllegalArgumentException,
                                                                           InvalidValueException {
    ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    AssayVO mockAssayVO = mocksControl.createMock(AssayVO.class);
    Simulation mockSimulation = mocksControl.createMock(Simulation.class);
    SiteDataHolder mockSiteDataHolder = mocksControl.createMock(SiteDataHolder.class);

    expect(mockSiteDataHolder.getAssay()).andReturn(mockAssayVO);
    final String mockAssayName = "mockAssayName";
    expect(mockAssayVO.getName()).andReturn(mockAssayName);
    final IonChannel dummyIonChannel = IonChannel.CaV1_2;
    expect(mockSiteDataHolder.getIonChannel()).andReturn(dummyIonChannel);

    // Emulate no summary data record.
    expect(mockSiteDataHolder.getSummaryDataRecord(false)).andReturn(null);
    // Emulate individual data record.
    final Map<IndividualDataRecord, List<DoseResponseDataRecord>> dummyNonSummaryDataRecords =
          new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();
    final IndividualDataRecord mockIndividualDataRecord = mocksControl.createMock(IndividualDataRecord.class);
    dummyNonSummaryDataRecords.put(mockIndividualDataRecord, new ArrayList<DoseResponseDataRecord>());
    expect(mockSiteDataHolder.getNonSummaryDataRecords()).andReturn(dummyNonSummaryDataRecords);
    final QualityControlFailureVO mockQualityControlFailureVO = mocksControl.createMock(QualityControlFailureVO.class);
    // Emulated data appearing invalid (by returning an object).
    expect(mockIndividualDataRecord.isMarkedInvalid()).andReturn(mockQualityControlFailureVO);
    final String dummyQCFailureReason = "dummyQCFailureReason";
    expect(mockQualityControlFailureVO.getReason()).andReturn(dummyQCFailureReason);

    final Capture<Progress> dummyOverallProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress));

    final String dummyIndividualDataRecordId = "dummyIndividualDataRecordId";
    expect(mockIndividualDataRecord.getId()).andReturn(dummyIndividualDataRecordId);

    final Capture<Provenance> dummyProvenance = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(dummyProvenance));

    mocksControl.replay();

    try {
      SiteDataHolder siteDataHolder = qcOfficer.removeQCFailedData(dummyCompoundIdentifier,
                                                                   dummyProcessingType,
                                                                   dummySimulationId,
                                                                   mockSimulation, mockSiteDataHolder);
      fail("Should not allow no summary data and failed (hence removed) individual data record.");
    } catch (IllegalArgumentException e) {
      // Success.
    }

    mocksControl.verify();
  }
}