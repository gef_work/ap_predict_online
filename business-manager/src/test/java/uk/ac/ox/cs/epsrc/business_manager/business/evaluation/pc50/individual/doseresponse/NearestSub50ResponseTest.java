/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual.doseresponse;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.util.ArrayList;
import java.util.List;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.DoseResponseTransformer;
import uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual.doseresponse.NearestSub50Response;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.IndividualProvenance;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.Provenance;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.DoseResponseData;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingParams;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.IndividualProcessing;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.doseresponse.DoseResponsePairVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Unit test the nearest sub-50 dose-response PC50 fitting strategy.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(DoseResponseTransformer.class)
public class NearestSub50ResponseTest {

  private NearestSub50Response nearestSub50Response;
  private BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate;
  private IMocksControl mocksControl;
  private final String dummyDescription = "dummyDescription";
  private final boolean dummyDefaultActive = true;
  private final int dummyDefaultInvocationOrder = 1;
  private final long dummySimulationId = 1l;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);

    nearestSub50Response = new NearestSub50Response(dummyDescription, dummyDefaultActive,
                                                    dummyDefaultInvocationOrder);
    ReflectionTestUtils.setField(nearestSub50Response, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
  }

  @Test
  public void testConstructor() {
    assertNotNull(nearestSub50Response.getSysProvenanceStrategyIdentifier());
  }

  @Test
  public void testRetrieveDoseResponseDataWhenNoAvailableData() throws InvalidValueException {
    IndividualProcessing mockIndividualProcessing = mocksControl.createMock(IndividualProcessing.class); 
    ICData mockIndividualC50Data = mocksControl.createMock(ICData.class);
    String dummyRecordIdentifier = "dummyRecordIdentifier";
    DoseResponseFittingParams mockDoseResponseFittingParams = mocksControl.createMock(DoseResponseFittingParams.class);
    final String dummyIndividualDataId = "dummyIndividualDataId";
    boolean dummyProvenanceable = false;

    expect(mockIndividualProcessing.hasDoseResponseData()).andReturn(false);

    mocksControl.replay();

    DoseResponseData data = nearestSub50Response.retrieveDoseResponseData(dummySimulationId,
                                                                          mockIndividualProcessing,
                                                                          mockIndividualC50Data,
                                                                          dummyRecordIdentifier,
                                                                          mockDoseResponseFittingParams,
                                                                          dummyIndividualDataId,
                                                                          dummyProvenanceable);

    mocksControl.verify();

    assertNull(data);

    mocksControl.reset();

    // Provenanceable
    dummyProvenanceable = true;
    expect(mockIndividualProcessing.hasDoseResponseData()).andReturn(false);
    final Capture<Provenance> dummyProvenance = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(dummyProvenance));

    mocksControl.replay();

    data = nearestSub50Response.retrieveDoseResponseData(dummySimulationId, mockIndividualProcessing,
                                                         mockIndividualC50Data,
                                                         dummyRecordIdentifier,
                                                         mockDoseResponseFittingParams,
                                                         dummyIndividualDataId,
                                                         dummyProvenanceable);

    mocksControl.verify();

    assertNull(data);
    IndividualProvenance capturedProvenance = (IndividualProvenance) dummyProvenance.getValue();
    assertTrue(InformationLevel.TRACE.compareTo(capturedProvenance.getLevel()) == 0);
  }

  @Test
  public void testRetrieveDoseResponseDataWhenAvailableData() throws InvalidValueException {
    IndividualProcessing mockIndividualProcessing = mocksControl.createMock(IndividualProcessing.class); 
    ICData mockIndividualC50Data = mocksControl.createMock(ICData.class);
    String dummyRecordIdentifier = "dummyRecordIdentifier";
    DoseResponseFittingParams mockDoseResponseFittingParams = mocksControl.createMock(DoseResponseFittingParams.class);
    final String dummyIndividualDataId = "dummyIndividualDataId";
    boolean dummyProvenanceable = true;

    expect(mockIndividualProcessing.hasDoseResponseData()).andReturn(true);
    DoseResponseData mockDoseResponseData = mocksControl.createMock(DoseResponseData.class);

    mockStatic(DoseResponseTransformer.class);
    expect(DoseResponseTransformer.nearestSub50ResponseTransform(mockIndividualProcessing))
          .andReturn(mockDoseResponseData);

    // Dose-response data, but an empty sub-50 dose-response points list is returned
    final List<DoseResponsePairVO> dummyDoseResponsePoints = new ArrayList<DoseResponsePairVO>();
    expect(mockDoseResponseData.doseResponsePoints()).andReturn(dummyDoseResponsePoints);
    final Capture<Provenance> dummyProvenance = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(dummyProvenance));

    replayAll();
    mocksControl.replay();

    DoseResponseData data = nearestSub50Response.retrieveDoseResponseData(dummySimulationId,
                                                                          mockIndividualProcessing,
                                                                          mockIndividualC50Data,
                                                                          dummyRecordIdentifier,
                                                                          mockDoseResponseFittingParams,
                                                                          dummyIndividualDataId,
                                                                          dummyProvenanceable);

    verifyAll();
    mocksControl.verify();

    assertNull(data);

    mocksControl.reset();

    dummyProvenanceable = false;

    expect(mockIndividualProcessing.hasDoseResponseData()).andReturn(true);
    mockStatic(DoseResponseTransformer.class);
    expect(DoseResponseTransformer.nearestSub50ResponseTransform(mockIndividualProcessing))
          .andReturn(mockDoseResponseData);

    // Dose-response data, but an empty sub-50 dose-response points list is returned
    expect(mockDoseResponseData.doseResponsePoints()).andReturn(dummyDoseResponsePoints);

    replayAll();
    mocksControl.replay();

    data = nearestSub50Response.retrieveDoseResponseData(dummySimulationId, mockIndividualProcessing,
                                                         mockIndividualC50Data,
                                                         dummyRecordIdentifier,
                                                         mockDoseResponseFittingParams,
                                                         dummyIndividualDataId,
                                                         dummyProvenanceable);

    verifyAll();
    mocksControl.verify();

    assertNull(data);

    mocksControl.reset();

    // Now with a sub-50 dose-response point.
    expect(mockIndividualProcessing.hasDoseResponseData()).andReturn(true);
    mockStatic(DoseResponseTransformer.class);
    expect(DoseResponseTransformer.nearestSub50ResponseTransform(mockIndividualProcessing))
          .andReturn(mockDoseResponseData);

    // Dose-response data, but a populated sub-50 dose-response points list is returned
    dummyDoseResponsePoints.clear();
    DoseResponsePairVO mockDoseResponsePairVO = mocksControl.createMock(DoseResponsePairVO.class);
    dummyDoseResponsePoints.add(mockDoseResponsePairVO);
    expect(mockDoseResponseData.doseResponsePoints()).andReturn(dummyDoseResponsePoints);
  
    replayAll();
    mocksControl.replay();

    data = nearestSub50Response.retrieveDoseResponseData(dummySimulationId,
                                                         mockIndividualProcessing,
                                                         mockIndividualC50Data,
                                                         dummyRecordIdentifier,
                                                         mockDoseResponseFittingParams,
                                                         dummyIndividualDataId,
                                                         dummyProvenanceable);

    verifyAll();
    mocksControl.verify();

    assertNotNull(data);
  }
}