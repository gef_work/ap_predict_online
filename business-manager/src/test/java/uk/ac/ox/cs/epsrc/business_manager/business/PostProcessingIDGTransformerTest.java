/*

  Copyright (c) 2016, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedDataVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata.ProcessedSiteDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;

/**
 *
 *
 * @author geoff
 */
public class PostProcessingIDGTransformerTest {

  private IMocksControl mocksControl;
  private Long dummySimulationId = 3L;
  private PostProcessingIDGTransformer transformer;
  private ProcessedSiteDataVO mockProcessedSiteData;
  private ProcessingType dummyProcessingType;

  @Before
  public void setUp() {
    transformer = new PostProcessingIDGTransformer();
    mocksControl = createStrictControl();
  }

  @Test
  public void testTransformToEmptyProcessed() {
    /*
     * Test 1 : Null processed site data.
     */
    mockProcessedSiteData = null;

    try {
      transformer.transformToEmptyProcessed(dummyProcessingType, dummySimulationId,
                                            mockProcessedSiteData);
      fail("Should not permit a null processed site data!");
    } catch (IllegalArgumentException e) {}

    /*
     * Test 2 : Processed site data contains data.
     */
    mockProcessedSiteData = mocksControl.createMock(ProcessedSiteDataVO.class);

    expect(mockProcessedSiteData.isContainingSiteData()).andReturn(true);

    mocksControl.replay();

    try {
      transformer.transformToEmptyProcessed(dummyProcessingType, dummySimulationId,
                                            mockProcessedSiteData);
      fail("Should not permit transformation if site data available!");
    } catch (IllegalArgumentException e) {}

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Test 3 : Fail if not input data gathering.
     */
    dummyProcessingType = ProcessingType.REGULAR_SIMULATION;

    expect(mockProcessedSiteData.isContainingSiteData()).andReturn(false);

    mocksControl.replay();

    try {
      transformer.transformToEmptyProcessed(dummyProcessingType, dummySimulationId,
                                            mockProcessedSiteData);
      fail("Should not permit transformation if not input data gathering!");
    } catch (IllegalArgumentException e) {}

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Test 4 : Non-fail processing
     */
    dummyProcessingType = ProcessingType.INPUT_DATA_GATHERING;

    expect(mockProcessedSiteData.isContainingSiteData()).andReturn(false);

    mocksControl.replay();

    final ProcessedDataVO returned = transformer.transformToEmptyProcessed(dummyProcessingType,
                                                                           dummySimulationId,
                                                                           mockProcessedSiteData);

    mocksControl.verify();

    assertTrue(returned.getPerFrequencyConcentrations().isEmpty());
    assertTrue(returned.getProcessedData().isEmpty());
  }
}