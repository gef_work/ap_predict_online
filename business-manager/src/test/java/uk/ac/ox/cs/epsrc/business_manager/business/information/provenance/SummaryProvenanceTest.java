/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.information.provenance;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.SummaryProvenance;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Unit test the summary provenance information. 
 *
 * @author geoff
 */
public class SummaryProvenanceTest {

  private InformationLevel dummyInformationLevel;
  private String dummyText;
  private Long dummySimulationId;

  @Before
  public void setUp() {
    dummyInformationLevel = InformationLevel.TRACE;
    dummyText = "dummyText";
    dummySimulationId = 4L;
  }

  @Test
  public void testBuilderThreeArgConstructorNoAssignments() {
    final SummaryProvenance provenance = new SummaryProvenance.Builder(dummyInformationLevel,
                                                                       dummyText, dummySimulationId)
                                                                    .build();
    assertEquals(dummyInformationLevel, provenance.getLevel());
    assertEquals(dummyText, provenance.getText());
    assertEquals(dummySimulationId, provenance.getSimulationId());
    assertNull(provenance.getAssayName());
    assertNull(provenance.getHillCoefficient());
    assertNull(provenance.getIonChannelName());
    assertTrue(provenance.getpIC50s().isEmpty());
    assertTrue(provenance.getRawData().isEmpty());
    assertNull(provenance.getSummaryDataId());
    assertNotNull(provenance.toString());
  }

  @Test
  public void testBuilderThreeArgConstructorAssignments() {
    final String dummyAssayName = "dummyAssayName";
    final BigDecimal dummyHillCoefficient = BigDecimal.ONE;
    final String dummyIonChannelName = "dummyIonChannelName";
    final List<BigDecimal> dummyPIC50s = new ArrayList<BigDecimal>();
    final BigDecimal dummyPIC50 = BigDecimal.TEN;
    dummyPIC50s.add(dummyPIC50);
    final Map<String, Object> dummyRawData = new HashMap<String, Object>();
    final String dummyKey1 = "dummyKey1";
    final String dummyVal1 = "dummyVal1";
    dummyRawData.put(dummyKey1, dummyVal1);
    final String dummySummaryDataId = "dummySummaryDataId";

    final SummaryProvenance provenance = new SummaryProvenance.Builder(dummyInformationLevel,
                                                                       dummyText, dummySimulationId)
                                                                    .assayName(dummyAssayName)
                                                                    .hillCoefficient(dummyHillCoefficient)
                                                                    .ionChannelName(dummyIonChannelName)
                                                                    .pIC50s(dummyPIC50s)
                                                                    .rawData(dummyRawData)
                                                                    .summaryDataId(dummySummaryDataId)
                                                                    .build();
    assertEquals(dummyAssayName, provenance.getAssayName());
    assertTrue(dummyHillCoefficient.compareTo(provenance.getHillCoefficient()) == 0);
    assertEquals(dummyIonChannelName, provenance.getIonChannelName());
    assertFalse(provenance.getpIC50s().isEmpty());
    assertEquals(1, provenance.getpIC50s().size());
    assertFalse(provenance.getRawData().isEmpty());
    assertEquals(1, provenance.getRawData().size());
    assertEquals(dummySummaryDataId, provenance.getSummaryDataId());
    assertNotNull(provenance.toString());
  }
}