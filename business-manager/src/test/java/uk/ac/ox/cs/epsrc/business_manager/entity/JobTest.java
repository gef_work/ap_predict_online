/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 * 
 */
package uk.ac.ox.cs.epsrc.business_manager.entity;

import static org.easymock.EasyMock.createControl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.Set;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit test Job object.
 *
 * @author geoff
 */
public class JobTest {

  private IMocksControl mocksControl;
  private Job job;

  @Before
  public void setUp() {
    mocksControl = createControl();
  }

  @Test
  public void testAssignCompleted() {
    // Note: Can't test {@code completed} property as not gettable! 
    job = new Job();
    final String dummyAppManagerId = "dummyAppManagerId";
    job.setAppManagerId(dummyAppManagerId);

    assertSame(dummyAppManagerId, job.getAppManagerId());

    job.assignCompleted();

    assertNull(job.getAppManagerId());
  }

  @Test
  public void testDefaultConstructor() {
    job = new Job();
    assertNull(job.getAppManagerId());
    assertTrue(job.getCompoundConcentrations().isEmpty());
    assertNull(job.getGroupedInvocationInput());
    assertNull(job.getId());
    assertTrue(job.getJobResults().isEmpty());
    assertNull(job.getMessages());
    assertNull(job.getOutput());
    assertNull(job.getPacingFrequency());
    assertNull(job.getSimulationId());
    assertTrue(job.isUsingConfigurationCompoundConcentrations());
    assertTrue(job.hashCode() > 0);
    assertNull(job.getDeltaAPD90PercentileNames());

    assertNotNull(job.toString());
  }

  @Test
  public void testUnfinished() {
    job = new Job();
    // Empty job results and not failed.
    assertTrue(job.isUnfinished());
    // Empty job results and failed.
    job.considerFailed();
    assertFalse(job.isUnfinished());

    job = new Job();
    final JobResult mockJobResult = mocksControl.createMock(JobResult.class);
    job.addJobResult(mockJobResult);
    // Job results.
    assertFalse(job.isUnfinished());

    try {
      job.considerFailed();
      fail("Cannot consider a job with results to have failed!");
    } catch (IllegalStateException e) {}
  }

  @Test
  public void testInitialisingConstructor() {
    Long dummySimulationId = null;
    Float dummyPacingFrequency = null;
    Set<Float> dummyCompoundConcentrations = null;
    boolean dummyUsingDefaultCompoundConcentrations = true;
    GroupedInvocationInput dummyGroupedInvocationInput = null;
    job = new Job(dummySimulationId, dummyPacingFrequency, dummyCompoundConcentrations,
                  dummyUsingDefaultCompoundConcentrations, dummyGroupedInvocationInput);

    assertNull(job.getAppManagerId());
    assertTrue(job.getCompoundConcentrations().isEmpty());
    assertNull(job.getGroupedInvocationInput());
    assertNull(job.getId());
    assertTrue(job.getJobResults().isEmpty());
    assertNull(job.getMessages());
    assertNull(job.getOutput());
    assertNull(job.getPacingFrequency());
    assertNull(job.getSimulationId());
    assertTrue(job.isUsingConfigurationCompoundConcentrations());

    dummySimulationId = 1L;
    dummyPacingFrequency = 0.5F;
    dummyCompoundConcentrations = new HashSet<Float>();
    final Float dummyCompoundConcentration1 = 0.0F;
    final Float dummyCompoundConcentration2 = 1.0F;
    dummyCompoundConcentrations.add(dummyCompoundConcentration1);
    dummyCompoundConcentrations.add(dummyCompoundConcentration2);
    dummyUsingDefaultCompoundConcentrations = false;
    dummyGroupedInvocationInput = mocksControl.createMock(GroupedInvocationInput.class);

    job = new Job(dummySimulationId, dummyPacingFrequency, dummyCompoundConcentrations,
                  dummyUsingDefaultCompoundConcentrations, dummyGroupedInvocationInput);

    assertNull(job.getAppManagerId());
    assertEquals(2, job.getCompoundConcentrations().size());
    assertTrue(job.getCompoundConcentrations().contains(dummyCompoundConcentration1));
    assertTrue(job.getCompoundConcentrations().contains(dummyCompoundConcentration2));
    assertSame(dummyGroupedInvocationInput, job.getGroupedInvocationInput());
    assertNull(job.getId());
    assertTrue(job.getJobResults().isEmpty());
    assertNull(job.getMessages());
    assertNull(job.getOutput());
    assertEquals(dummyPacingFrequency, job.getPacingFrequency());
    assertEquals(dummySimulationId, job.getSimulationId());
    assertFalse(job.isUsingConfigurationCompoundConcentrations());
  }

  @Test
  public void testSetters() {
    final Long dummySimulationId = 1L;
    final Float dummyPacingFrequency = 0.5F;
    final Set<Float> dummyCompoundConcentrations = new HashSet<Float>();
    final Float dummyCompoundConcentration1 = 0.0F;
    final Float dummyCompoundConcentration2 = 1.0F;
    dummyCompoundConcentrations.add(dummyCompoundConcentration1);
    dummyCompoundConcentrations.add(dummyCompoundConcentration2);
    final boolean dummyUsingDefaultCompoundConcentrations = false;
    final GroupedInvocationInput dummyGroupedInvocationInput = mocksControl.createMock(GroupedInvocationInput.class);

    job = new Job(dummySimulationId, dummyPacingFrequency, dummyCompoundConcentrations,
                  dummyUsingDefaultCompoundConcentrations, dummyGroupedInvocationInput);

    final JobResult mockJobResult = mocksControl.createMock(JobResult.class);
    assertTrue(job.getJobResults().isEmpty());
    job.addJobResult(mockJobResult);
    assertEquals(1, job.getJobResults().size());

    final String dummyMessages = "dummyMessages";
    assertNull(job.getMessages());
    job.setMessages(dummyMessages);
    assertEquals(dummyMessages, job.getMessages());

    final String dummyInfo = "dummyInfo";
    assertNull(job.getInfo());
    job.setInfo(dummyInfo);
    assertEquals(dummyInfo, job.getInfo());

    final String dummyOutput = "dummyOutput";
    assertNull(job.getOutput());
    job.setOutput(dummyOutput);
    assertEquals(dummyOutput, job.getOutput());

    final Boolean dummyCompleted = Boolean.TRUE;
    job.setCompleted(dummyCompleted);

    final String dummyAppManagerId = "dummyAppManagerId";
    job.setAppManagerId(dummyAppManagerId);

    String dummyDeltaAPD90PctileNames = null;
    job.setDeltaAPD90PercentileNames(dummyDeltaAPD90PctileNames);
    assertNull(job.getDeltaAPD90PercentileNames());
    dummyDeltaAPD90PctileNames = "dummyDeltaAPD90PctileNames";
    job.setDeltaAPD90PercentileNames(dummyDeltaAPD90PctileNames);
    assertEquals(dummyDeltaAPD90PctileNames, job.getDeltaAPD90PercentileNames());

    assertNotNull(job.toString());
  }
}