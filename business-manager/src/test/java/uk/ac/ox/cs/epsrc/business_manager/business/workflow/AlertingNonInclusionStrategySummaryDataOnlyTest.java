/*

  Copyright (c) 2016, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.workflow;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.summary.SummaryDataOnlyNonEqualityNonInclusionAlerting;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.Provenance;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.SummaryProvenance;
import uk.ac.ox.cs.epsrc.business_manager.business.selection.c50.C50DataSelector;
import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.service.SimulationService;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.AlertingNonInclusionStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.NoSuchDataException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;

/**
 *
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { C50DataSelector.class } )
public class AlertingNonInclusionStrategySummaryDataOnlyTest {
  private BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate;
  private Configuration mockConfiguration;
  private IMocksControl mocksControl;
  private SimulationService mockSimulationService;
  private SiteDataHolder mockSiteDataHolder;
  private SummaryDataRecord mockSummaryDataRecord;
  private WorkflowProcessor workflowProcessor;
  private final Long dummySimulationId = 4L;
  private ProcessingType dummyProcessingType = ProcessingType.INPUT_DATA_GATHERING;
  private final String dummyCompoundIdentifier = "dummyCompoundIdentifier";

  @Before
  public void setUp() {
    workflowProcessor = new WorkflowProcessor();
    mocksControl = createStrictControl();
    mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    mockConfiguration = mocksControl.createMock(Configuration.class);
    mockSimulationService = mocksControl.createMock(SimulationService.class);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_CONFIGURATION,
                                 mockConfiguration);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE,
                                 mockSimulationService);

    mockSiteDataHolder = mocksControl.createMock(SiteDataHolder.class);
    final AssayVO mockAssayVO = mocksControl.createMock(AssayVO.class);
    expect(mockSiteDataHolder.getAssay()).andReturn(mockAssayVO);
    final IonChannel dummyIonChannel = IonChannel.hERG;
    expect(mockSiteDataHolder.getIonChannel()).andReturn(dummyIonChannel);
    final String dummyAssayName = "dummyAssayName";
    expect(mockAssayVO.getName()).andReturn(dummyAssayName);
  }

  private void setCommonOptions(final Simulation mockInputDataGatheringSimulation) throws NoSuchDataException, InvalidValueException {
    final ICPercent dummyPctIC = ICPercent.PCT50;

    final String dummyDoseResponseFittingStrategy = "dummyDoseResponseFittingStrategy";
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingStrategy()).andReturn(dummyDoseResponseFittingStrategy);
    final boolean isDoseResponseFittingRounding = true;
    expect(mockInputDataGatheringSimulation.isDoseResponseFittingRounding()).andReturn(isDoseResponseFittingRounding);
    final Float dummyDoseResponseFittingHillMax = 0.5F;
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingHillMax()).andReturn(dummyDoseResponseFittingHillMax);
    final Float dummyDoseResponseFittingHillMin = 2F;
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingHillMin()).andReturn(dummyDoseResponseFittingHillMin);

    final Map<IndividualDataRecord, List<DoseResponseDataRecord>> mockNonSummaryData = 
          new LinkedHashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();
    expect(mockSiteDataHolder.getNonSummaryDataRecords()).andReturn(mockNonSummaryData);

    final List<ICData> dummySummaryEligibleC50Data = new ArrayList<ICData>();
    expect(mockSummaryDataRecord.eligibleICData()).andReturn(dummySummaryEligibleC50Data);
    final ICData mockSummaryC50Data = mocksControl.createMock(ICData.class);
    mockStatic(C50DataSelector.class);
    expect(C50DataSelector.selectFrom(dummySummaryEligibleC50Data)).andReturn(mockSummaryC50Data);
    expect(mockSummaryC50Data.hasICPctData(dummyPctIC)).andReturn(true);
    final Integer dummyCountOfRecordsUsedToDetermineC50Value = null;
    expect(mockSummaryC50Data.retrieveCountOfRecordsUsedToDetermineICValue(ICPercent.PCT50)).andReturn(dummyCountOfRecordsUsedToDetermineC50Value);
    // Count indicates an anonymous individual will need to be created
    final Integer dummyCountOfAvailableRecords = mockNonSummaryData.size() + 1;
    expect(mockSummaryC50Data.retrieveCountOfAvailableRecords(ICPercent.PCT50)).andReturn(dummyCountOfAvailableRecords);
  }

  @Test
  public void testSummaryDataOnlyAlertingNonInclusionStrategyNoSummaryData() throws IllegalArgumentException,
                                                                                    InvalidValueException,
                                                                                    NoSuchDataException {
    expect(mockSiteDataHolder.getSummaryDataRecord(false)).andReturn(null);
    // Create the mock PC50 evaluation strategies to iterate through.
    final Simulation mockInputDataGatheringSimulation = mocksControl.createMock(Simulation.class);
    final String dummyInvocationOrdersCSV = "";
    expect(mockInputDataGatheringSimulation.getPc50EvaluationStrategies()).andReturn(dummyInvocationOrdersCSV);
    final List<PC50EvaluationStrategy> dummyPC50EvaluationStrategies = new ArrayList<PC50EvaluationStrategy>();
    final PC50EvaluationStrategy mockStrategy = mocksControl.createMock(SummaryDataOnlyNonEqualityNonInclusionAlerting.class);
    dummyPC50EvaluationStrategies.add((PC50EvaluationStrategy) mockStrategy);

    expect(mockConfiguration.retrieveStrategiesByCSV(dummyInvocationOrdersCSV)).andReturn(dummyPC50EvaluationStrategies);

    final String dummyDoseResponseFittingStrategy = "dummyDoseResponseFittingStrategy";
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingStrategy()).andReturn(dummyDoseResponseFittingStrategy);
    final boolean isDoseResponseFittingRounding = true;
    expect(mockInputDataGatheringSimulation.isDoseResponseFittingRounding()).andReturn(isDoseResponseFittingRounding);
    final Float dummyDoseResponseFittingHillMax = 0.5F;
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingHillMax()).andReturn(dummyDoseResponseFittingHillMax);
    final Float dummyDoseResponseFittingHillMin = 2F;
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingHillMin()).andReturn(dummyDoseResponseFittingHillMin);

    final Map<IndividualDataRecord, List<DoseResponseDataRecord>> mockNonSummaryData = 
          new LinkedHashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();
    expect(mockSiteDataHolder.getNonSummaryDataRecords()).andReturn(mockNonSummaryData);

    // Enter pIC50-determining strategy iteration.
    final Integer dummyDefaultInvocationOrder1 = 1;
    final String dummyDescription1 = "dummyDescription1";
    expect(((PC50EvaluationStrategy) mockStrategy).getOrder()).andReturn(dummyDefaultInvocationOrder1);
    expect(((PC50EvaluationStrategy) mockStrategy).getDescription()).andReturn(dummyDescription1);

    replayAll();
    mocksControl.replay();

    workflowProcessor.processWorkflow(dummyCompoundIdentifier, dummyProcessingType,
                                      dummySimulationId, mockInputDataGatheringSimulation,
                                      mockSiteDataHolder);

    verifyAll();
    mocksControl.verify();
  }

  @Test
  public void testSummaryDataOnlyAlertingNonInclusionStrategyNoSummaryOnlyData() throws IllegalArgumentException,
                                                                                        InvalidValueException,
                                                                                        NoSuchDataException {
    mockSummaryDataRecord = mocksControl.createMock(SummaryDataRecord.class);
    expect(mockSiteDataHolder.getSummaryDataRecord(false)).andReturn(mockSummaryDataRecord);
    final String dummySummaryDataRecordId = "dummySummaryDataRecordId";
    expect(mockSummaryDataRecord.getId()).andReturn(dummySummaryDataRecordId);
    // Create the mock PC50 evaluation strategies to iterate through.
    final Simulation mockInputDataGatheringSimulation = mocksControl.createMock(Simulation.class);
    final String dummyInvocationOrdersCSV = "";
    expect(mockInputDataGatheringSimulation.getPc50EvaluationStrategies()).andReturn(dummyInvocationOrdersCSV);
    final List<PC50EvaluationStrategy> dummyPC50EvaluationStrategies = new ArrayList<PC50EvaluationStrategy>();
    final PC50EvaluationStrategy mockStrategy = mocksControl.createMock(SummaryDataOnlyNonEqualityNonInclusionAlerting.class);
    dummyPC50EvaluationStrategies.add((PC50EvaluationStrategy) mockStrategy);

    expect(mockConfiguration.retrieveStrategiesByCSV(dummyInvocationOrdersCSV)).andReturn(dummyPC50EvaluationStrategies);

    setCommonOptions(mockInputDataGatheringSimulation);

    // Enter pIC50-determining strategy iteration.
    final Integer dummyDefaultInvocationOrder1 = 1;
    final String dummyDescription1 = "dummyDescription1";
    expect(((PC50EvaluationStrategy) mockStrategy).getOrder()).andReturn(dummyDefaultInvocationOrder1);
    expect(((PC50EvaluationStrategy) mockStrategy).getDescription()).andReturn(dummyDescription1);

    expect(mockSiteDataHolder.getSummaryDataRecord(true)).andReturn(null);

    replayAll();
    mocksControl.replay();

    workflowProcessor.processWorkflow(dummyCompoundIdentifier, dummyProcessingType,
                                      dummySimulationId, mockInputDataGatheringSimulation,
                                      mockSiteDataHolder);

    verifyAll();
    mocksControl.verify();
  }

  @Test
  public void testSummaryDataOnlyAlertingNonInclusionStrategyNoAlert() throws IllegalArgumentException,
                                                                              InvalidValueException,
                                                                              NoSuchDataException {
    mockSummaryDataRecord = mocksControl.createMock(SummaryDataRecord.class);
    expect(mockSiteDataHolder.getSummaryDataRecord(false)).andReturn(mockSummaryDataRecord);
    final String dummySummaryDataRecordId = "dummySummaryDataRecordId";
    expect(mockSummaryDataRecord.getId()).andReturn(dummySummaryDataRecordId);
    // Create the mock PC50 evaluation strategies to iterate through.
    final Simulation mockInputDataGatheringSimulation = mocksControl.createMock(Simulation.class);
    final String dummyInvocationOrdersCSV = "";
    expect(mockInputDataGatheringSimulation.getPc50EvaluationStrategies()).andReturn(dummyInvocationOrdersCSV);
    final List<PC50EvaluationStrategy> dummyPC50EvaluationStrategies = new ArrayList<PC50EvaluationStrategy>();
    final PC50EvaluationStrategy mockStrategy = mocksControl.createMock(SummaryDataOnlyNonEqualityNonInclusionAlerting.class);
    dummyPC50EvaluationStrategies.add((PC50EvaluationStrategy) mockStrategy);

    expect(mockConfiguration.retrieveStrategiesByCSV(dummyInvocationOrdersCSV)).andReturn(dummyPC50EvaluationStrategies);

    setCommonOptions(mockInputDataGatheringSimulation);

    // Enter pIC50-determining strategy iteration.
    final Integer dummyDefaultInvocationOrder1 = 1;
    final String dummyDescription1 = "dummyDescription1";
    expect(((PC50EvaluationStrategy) mockStrategy).getOrder()).andReturn(dummyDefaultInvocationOrder1);
    expect(((PC50EvaluationStrategy) mockStrategy).getDescription()).andReturn(dummyDescription1);

    expect(mockSiteDataHolder.getSummaryDataRecord(true)).andReturn(mockSummaryDataRecord);
    final List<ICData> dummyEligibleC50Data = new ArrayList<ICData>();
    final ICData mockICData = mocksControl.createMock(ICData.class);
    dummyEligibleC50Data.add(mockICData);
    expect(mockSummaryDataRecord.eligibleICData()).andReturn(dummyEligibleC50Data);
    expect(C50DataSelector.selectFrom(dummyEligibleC50Data)).andReturn(mockICData);

    final String dummyAlert = null;
    expect(((AlertingNonInclusionStrategy) mockStrategy).soundAlert(mockICData)).andReturn(dummyAlert);

    replayAll();
    mocksControl.replay();

    workflowProcessor.processWorkflow(dummyCompoundIdentifier, dummyProcessingType,
                                      dummySimulationId, mockInputDataGatheringSimulation,
                                      mockSiteDataHolder);

    verifyAll();
    mocksControl.verify();
  }

  @Test
  public void testSummaryDataOnlyAlertingNonInclusionStrategyAlertsIDG() throws IllegalArgumentException,
                                                                                InvalidValueException,
                                                                                NoSuchDataException {
    mockSummaryDataRecord = mocksControl.createMock(SummaryDataRecord.class);
    expect(mockSiteDataHolder.getSummaryDataRecord(false)).andReturn(mockSummaryDataRecord);
    final String dummySummaryDataRecordId = "dummySummaryDataRecordId";
    expect(mockSummaryDataRecord.getId()).andReturn(dummySummaryDataRecordId);
    // Create the mock PC50 evaluation strategies to iterate through.
    final Simulation mockInputDataGatheringSimulation = mocksControl.createMock(Simulation.class);
    final String dummyInvocationOrdersCSV = "";
    expect(mockInputDataGatheringSimulation.getPc50EvaluationStrategies()).andReturn(dummyInvocationOrdersCSV);
    final List<PC50EvaluationStrategy> dummyPC50EvaluationStrategies = new ArrayList<PC50EvaluationStrategy>();
    final PC50EvaluationStrategy mockStrategy = mocksControl.createMock(SummaryDataOnlyNonEqualityNonInclusionAlerting.class);
    dummyPC50EvaluationStrategies.add((PC50EvaluationStrategy) mockStrategy);

    expect(mockConfiguration.retrieveStrategiesByCSV(dummyInvocationOrdersCSV)).andReturn(dummyPC50EvaluationStrategies);

    setCommonOptions(mockInputDataGatheringSimulation);

    // Enter pIC50-determining strategy iteration.
    final Integer dummyDefaultInvocationOrder1 = 1;
    final String dummyDescription1 = "dummyDescription1";
    expect(((PC50EvaluationStrategy) mockStrategy).getOrder()).andReturn(dummyDefaultInvocationOrder1);
    expect(((PC50EvaluationStrategy) mockStrategy).getDescription()).andReturn(dummyDescription1);

    expect(mockSiteDataHolder.getSummaryDataRecord(true)).andReturn(mockSummaryDataRecord);
    final List<ICData> dummyEligibleC50Data = new ArrayList<ICData>();
    final ICData mockICData = mocksControl.createMock(ICData.class);
    dummyEligibleC50Data.add(mockICData);
    expect(mockSummaryDataRecord.eligibleICData()).andReturn(dummyEligibleC50Data);
    expect(C50DataSelector.selectFrom(dummyEligibleC50Data)).andReturn(mockICData);

    final String dummyAlert = "dummyAlert";
    expect(((AlertingNonInclusionStrategy) mockStrategy).soundAlert(mockICData)).andReturn(dummyAlert);

    final String dummyId = "dummyId";
    expect(mockSummaryDataRecord.getId()).andReturn(dummyId);

    replayAll();
    mocksControl.replay();

    workflowProcessor.processWorkflow(dummyCompoundIdentifier, dummyProcessingType,
                                      dummySimulationId, mockInputDataGatheringSimulation,
                                      mockSiteDataHolder);

    verifyAll();
    mocksControl.verify();
  }

  @Test
  public void testSummaryDataOnlyAlertingNonInclusionStrategyAlertsRegSim() throws IllegalArgumentException,
                                                                                   InvalidValueException,
                                                                                   NoSuchDataException {
    dummyProcessingType = ProcessingType.REGULAR_SIMULATION;

    mockSummaryDataRecord = mocksControl.createMock(SummaryDataRecord.class);
    expect(mockSiteDataHolder.getSummaryDataRecord(false)).andReturn(mockSummaryDataRecord);
    final String dummySummaryDataRecordId = "dummySummaryDataRecordId";
    expect(mockSummaryDataRecord.getId()).andReturn(dummySummaryDataRecordId);
    final Simulation mockSimulation = mocksControl.createMock(Simulation.class);
    if (!ProcessingType.isInputDataGathering(dummyProcessingType)) {
      expect(mockSimulationService.findBySimulationId(dummySimulationId))
            .andReturn(mockSimulation);
    }
    final String dummyInvocationOrdersCSV = "";
    expect(mockSimulation.getPc50EvaluationStrategies()).andReturn(dummyInvocationOrdersCSV);
    final List<PC50EvaluationStrategy> dummyPC50EvaluationStrategies = new ArrayList<PC50EvaluationStrategy>();
    final PC50EvaluationStrategy mockStrategy = mocksControl.createMock(SummaryDataOnlyNonEqualityNonInclusionAlerting.class);
    dummyPC50EvaluationStrategies.add((PC50EvaluationStrategy) mockStrategy);

    expect(mockConfiguration.retrieveStrategiesByCSV(dummyInvocationOrdersCSV)).andReturn(dummyPC50EvaluationStrategies);

    setCommonOptions(mockSimulation);

    // Enter pIC50-determining strategy iteration.
    final Integer dummyDefaultInvocationOrder1 = 1;
    final String dummyDescription1 = "dummyDescription1";
    expect(((PC50EvaluationStrategy) mockStrategy).getOrder()).andReturn(dummyDefaultInvocationOrder1);
    expect(((PC50EvaluationStrategy) mockStrategy).getDescription()).andReturn(dummyDescription1);

    expect(mockSiteDataHolder.getSummaryDataRecord(true)).andReturn(mockSummaryDataRecord);
    final List<ICData> dummyEligibleC50Data = new ArrayList<ICData>();
    final ICData mockICData = mocksControl.createMock(ICData.class);
    dummyEligibleC50Data.add(mockICData);
    expect(mockSummaryDataRecord.eligibleICData()).andReturn(dummyEligibleC50Data);
    expect(C50DataSelector.selectFrom(dummyEligibleC50Data)).andReturn(mockICData);

    final String dummyAlert = "dummyAlert";
    expect(((AlertingNonInclusionStrategy) mockStrategy).soundAlert(mockICData)).andReturn(dummyAlert);

    final String dummyId = "dummyId";
    expect(mockSummaryDataRecord.getId()).andReturn(dummyId);

    Capture<Progress> captureOverallProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(captureOverallProgress));

    Capture<Provenance> captureSummaryProvenance1 = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(captureSummaryProvenance1));
    Capture<Provenance> captureSummaryProvenance2 = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(captureSummaryProvenance2));

    replayAll();
    mocksControl.replay();

    workflowProcessor.processWorkflow(dummyCompoundIdentifier, dummyProcessingType,
                                      dummySimulationId, mockSimulation,
                                      mockSiteDataHolder);

    verifyAll();
    mocksControl.verify();

    final OverallProgress capturedOverallProgress = (OverallProgress) captureOverallProgress.getValue();
    assertEquals(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertSame(InformationLevel.WARN, capturedOverallProgress.getLevel());
    assertTrue(capturedOverallProgress.getText().contains(dummyAlert));
    assertTrue(capturedOverallProgress.getArgs().isEmpty());

    final SummaryProvenance capturedSummaryProvenance1 = (SummaryProvenance) captureSummaryProvenance1.getValue();
    assertEquals(dummySummaryDataRecordId, capturedSummaryProvenance1.getSummaryDataId());
    assertSame(InformationLevel.INFO, capturedSummaryProvenance1.getLevel());
    assertFalse(capturedSummaryProvenance1.getText().contains(dummyAlert));

    final SummaryProvenance capturedSummaryProvenance2 = (SummaryProvenance) captureSummaryProvenance2.getValue();
    assertEquals(dummySummaryDataRecordId, capturedSummaryProvenance2.getSummaryDataId());
    assertSame(InformationLevel.INFO, capturedSummaryProvenance2.getLevel());
    assertTrue(capturedSummaryProvenance2.getText().contains(dummyAlert));
  }
}