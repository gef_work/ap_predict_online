/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.dose_response;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.DoseResponseData;
import uk.ac.ox.cs.epsrc.business_manager.ws.dose_response.FDRRunUtil;
import uk.ac.ox.cs.epsrc.fdr_manager.ws._1.FDRRunResponse;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingResult;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.doseresponse.DoseResponsePairVO;

/**
 * Unit test the FDR run request SOAP creation class.
 *
 * @author geoff
 */
public class FDRRunUtilTest {

  /**
   * Test the creation of the SOAP request.
   */
  @Test
  public void testSOAPCreationRequest() {
    final BigDecimal dose = BigDecimal.ONE;
    final BigDecimal response = BigDecimal.TEN;

    final IMocksControl mocksControl = createStrictControl();
    final DoseResponseData mockDoseResponseData = mocksControl.createMock(DoseResponseData.class);
    final List<DoseResponsePairVO> individualDoseResponses = new ArrayList<DoseResponsePairVO>();
    final DoseResponsePairVO mockIndividualDoseResponse = mocksControl.createMock(DoseResponsePairVO.class);
    individualDoseResponses.add(mockIndividualDoseResponse);

    expect(mockDoseResponseData.doseResponsePoints()).andReturn(individualDoseResponses);
    expect(mockIndividualDoseResponse.getDose()).andReturn(dose).atLeastOnce();
    expect(mockIndividualDoseResponse.getResponse()).andReturn(response).atLeastOnce();
    mocksControl.replay();
    FDRRunUtil.createFDRRunRequest(mockDoseResponseData);
    mocksControl.verify();
  }

  /**
   * Test the extraction works as expected.
   */
  @Test
  public void testSOAPDataExtraction() {
    final IMocksControl mocksControl = createStrictControl();
    final FDRRunResponse mockFDRRunResponse = mocksControl.createMock(FDRRunResponse.class);
    String ic50 = null;
    String hillCoefficient = null;
    String error = null;
    expect(mockFDRRunResponse.getIC50()).andReturn(ic50).atLeastOnce();
    expect(mockFDRRunResponse.getHillCoefficient()).andReturn(hillCoefficient).atLeastOnce();
    expect(mockFDRRunResponse.getError()).andReturn(error).atLeastOnce();
    mocksControl.replay();
    DoseResponseFittingResult doseResponseFittingResult = FDRRunUtil.dataExtractFDRRunResponse(mockFDRRunResponse);
    mocksControl.verify();
    assertNull(doseResponseFittingResult);

    mocksControl.reset();

    
    ic50 = "2000.0";
    hillCoefficient = "3.2";
    error = "apples";

    expect(mockFDRRunResponse.getIC50()).andReturn(ic50).atLeastOnce();
    expect(mockFDRRunResponse.getHillCoefficient()).andReturn(hillCoefficient).atLeastOnce();
    expect(mockFDRRunResponse.getError()).andReturn(error).atLeastOnce();
    mocksControl.replay();
    doseResponseFittingResult = FDRRunUtil.dataExtractFDRRunResponse(mockFDRRunResponse);
    mocksControl.verify();
    assertNotNull(doseResponseFittingResult);
  }
}