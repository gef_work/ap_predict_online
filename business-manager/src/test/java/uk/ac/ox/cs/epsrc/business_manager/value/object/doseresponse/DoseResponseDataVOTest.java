/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object.doseresponse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.value.object.doseresponse.DoseResponseDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.doseresponse.DoseResponsePairVO;

/**
 * Unit test the dose-response data value object.
 *
 * @author geoff
 */
public class DoseResponseDataVOTest {

  private DoseResponseDataVO doseResponseDataVO;

  /**
   * Test the finding of nearest sub-limit dose-response values.
   */
  @Test
  public void testFindingNearestDoseResponsePairFailsOnNull() {
    final List<DoseResponsePairVO> doseResponsePoints = null;
    try {
      doseResponseDataVO = new DoseResponseDataVO(doseResponsePoints);
      fail("Should not accept null parameter when finding nearest dose-response pair.");
    } catch (NullPointerException e) {}
  }

  @Test
  public void testFindingNearestDoseResponsePair() {
    final List<DoseResponsePairVO> doseResponsePoints = new ArrayList<DoseResponsePairVO>();
    doseResponseDataVO = new DoseResponseDataVO(doseResponsePoints);
    DoseResponsePairVO nearestSub50 = doseResponseDataVO.findNearestDoseResponsePair();
    assertNull(nearestSub50);

    final DoseResponsePairVO doseResponsePair1 = new DoseResponsePairVO(BigDecimal.ONE, BigDecimal.TEN);
    doseResponsePoints.add(doseResponsePair1);

    doseResponseDataVO = new DoseResponseDataVO(doseResponsePoints);
    nearestSub50 = doseResponseDataVO.findNearestDoseResponsePair();
    assertNotNull(nearestSub50);
    assertEquals(BigDecimal.TEN, nearestSub50.getResponse());

    final BigDecimal pair2Dose = new BigDecimal("0.9");
    final DoseResponsePairVO doseResponsePair2 = new DoseResponsePairVO(pair2Dose, BigDecimal.TEN);
    doseResponsePoints.add(doseResponsePair2);

    doseResponseDataVO = new DoseResponseDataVO(doseResponsePoints);
    nearestSub50 = doseResponseDataVO.findNearestDoseResponsePair();
    assertEquals(pair2Dose, nearestSub50.getDose());

    final BigDecimal pair3Dose = new BigDecimal("1.1");
    final DoseResponsePairVO doseResponsePair3 = new DoseResponsePairVO(pair3Dose, BigDecimal.TEN);
    doseResponsePoints.add(doseResponsePair3);

    doseResponseDataVO = new DoseResponseDataVO(doseResponsePoints);
    nearestSub50 = doseResponseDataVO.findNearestDoseResponsePair();
    assertEquals(pair2Dose, nearestSub50.getDose());

    final BigDecimal pair4Dose = new BigDecimal("0.8");
    final DoseResponsePairVO doseResponsePair4 = new DoseResponsePairVO(pair4Dose, new BigDecimal("50.1"));
    doseResponsePoints.add(doseResponsePair4);
    // Shouldn't complain if there's a value above 50... it can happen!

    doseResponseDataVO = new DoseResponseDataVO(doseResponsePoints);
    nearestSub50 = doseResponseDataVO.findNearestDoseResponsePair();
    assertEquals(BigDecimal.TEN, nearestSub50.getResponse());
  }
}