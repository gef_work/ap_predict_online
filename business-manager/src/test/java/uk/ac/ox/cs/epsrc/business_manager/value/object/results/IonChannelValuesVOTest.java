/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object.results;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

/**
 * Unit test the ion channel values value object.
 *
 * @author geoff
 */
public class IonChannelValuesVOTest {

  @Test
  public void testConstructor() {
    String dummyName = null;
    String dummyPIC50s = null;
    String dummyHills = null;
    String dummyOriginals = null;
    String dummySourceAssayName = null;
    String dummyStrategyOrders = null;

    IonChannelValuesVO ionChannelValuesVO = new IonChannelValuesVO(dummyName,
                                                                   dummySourceAssayName,
                                                                   dummyPIC50s,
                                                                   dummyHills,
                                                                   dummyOriginals,
                                                                   dummyStrategyOrders);

    assertNotNull(ionChannelValuesVO.toString());
    assertNull(ionChannelValuesVO.getName());
    assertNull(ionChannelValuesVO.getSourceAssayName());
    assertNull(ionChannelValuesVO.getpIC50s());
    assertNull(ionChannelValuesVO.getHills());
    assertNull(ionChannelValuesVO.getOriginals());
    assertNull(ionChannelValuesVO.getStrategyOrders());

    dummyName = "dummyName";
    dummyPIC50s = "dummyPIC50s";
    dummyHills = "dummyHills";
    dummyOriginals = "dummyOriginals";
    dummySourceAssayName = "dummySourceAssayName";
    dummyStrategyOrders = "dummyStrategyOrders";

    ionChannelValuesVO = new IonChannelValuesVO(dummyName, dummySourceAssayName,
                                                dummyPIC50s, dummyHills,
                                                dummyOriginals,
                                                dummyStrategyOrders);

    assertEquals(dummyName, ionChannelValuesVO.getName());
    assertEquals(dummySourceAssayName, ionChannelValuesVO.getSourceAssayName());
    assertEquals(dummyPIC50s, ionChannelValuesVO.getpIC50s());
    assertEquals(dummyHills, ionChannelValuesVO.getHills());
    assertEquals(dummyOriginals, ionChannelValuesVO.getOriginals());
    assertEquals(dummyStrategyOrders, ionChannelValuesVO.getStrategyOrders());
}
}