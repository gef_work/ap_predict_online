/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.information.provenance;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.DoseResponseProvenance;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Unit test the dose-response provenance information.
 *
 * @author geoff
 */
public class DoseResponseProvenanceTest {

  private InformationLevel dummyInformationLevel;
  private String dummyText;
  private Long dummySimulationId;

  @Before
  public void setUp() {
    dummyInformationLevel = InformationLevel.TRACE;
    dummyText = "dummyText";
    dummySimulationId = 4L;
  }

  @Test
  public void testBuilderThreeArgConstructorNoAssignment() {
    final DoseResponseProvenance provenance = new DoseResponseProvenance.Builder(dummyInformationLevel,
                                                                                 dummyText,
                                                                                 dummySimulationId)
                                                                        .build();
    assertEquals(dummyInformationLevel, provenance.getLevel());
    assertEquals(dummyText, provenance.getText());
    assertEquals(dummySimulationId, provenance.getSimulationId());
    assertNull(provenance.getDose_in_uM());
    assertNull(provenance.getDoseResponseDataId());
    assertNull(provenance.getIndividualDataId());
    assertTrue(provenance.getRawData().isEmpty());
    assertNull(provenance.getResponse());
  }

  @Test
  public void testBuilderThreeArgConstructorAssignments() {
    final String dummyDoseInUM = "dummyDoseInUM";
    final String dummyDoseResponseDataId = "dummyDoseResponseDataId";
    final String dummyIndividualDataId = "dummyIndividualDataId";
    final Map<String, Object> dummyRawData = new HashMap<String, Object>();
    final String dummyKey = "dummyKey";
    final String dummyValue = "dummyValue";
    dummyRawData.put(dummyKey, dummyValue);
    dummyRawData.put(dummyKey, dummyValue);
    final String dummyResponse = "dummyResponse";
    DoseResponseProvenance provenance = new DoseResponseProvenance.Builder(dummyInformationLevel,
                                                                           dummyText,
                                                                           dummySimulationId)
                                                                  .dose_in_uM(dummyDoseInUM)
                                                                  .doseResponseDataId(dummyDoseResponseDataId)
                                                                  .individualDataId(dummyIndividualDataId)
                                                                  .rawData(dummyRawData)
                                                                  .response(dummyResponse)
                                                                  .build();
    assertEquals(dummyDoseInUM, provenance.getDose_in_uM());
    assertEquals(dummyDoseResponseDataId, provenance.getDoseResponseDataId());
    assertEquals(dummyIndividualDataId, provenance.getIndividualDataId());
    assertEquals(1, provenance.getRawData().size());
    assertTrue(dummyResponse == provenance.getResponse());
    assertNotNull(provenance.toString());
  }
}