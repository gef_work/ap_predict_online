/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.checker;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.SimulationProcessingGateway;
import uk.ac.ox.cs.epsrc.business_manager.business.checker.InputValueChange;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.entity.GroupedInvocationInput;
import uk.ac.ox.cs.epsrc.business_manager.entity.IonChannelValues;
import uk.ac.ox.cs.epsrc.business_manager.entity.PIC50Data;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.manager.ResultsManager;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Unit test the input value change checker.
 *
 * @author geoff
 */
public class InputValueChangeTest {

  private BusinessManagerJMSTemplate mockJmsTemplate;
  private IMocksControl mocksControl;
  private InputValueChange inputValueChange;
  private final int dummyOrder = 1;
  private final Long dummySimulationId = 1L;
  private ResultsManager mockResultsManager;
  private Simulation mockSimulation;
  private SimulationProcessingGateway mockSimulationProcessingGateway;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    mockResultsManager = mocksControl.createMock(ResultsManager.class);
    mockSimulation = mocksControl.createMock(Simulation.class);
    mockSimulationProcessingGateway = mocksControl.createMock(SimulationProcessingGateway.class);
    inputValueChange = new InputValueChange(dummyOrder);
    ReflectionTestUtils.setField(inputValueChange, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockJmsTemplate);
    ReflectionTestUtils.setField(inputValueChange, BusinessIdentifiers.COMPONENT_RESULTS_MANAGER,
                                 mockResultsManager);
    ReflectionTestUtils.setField(inputValueChange, BusinessIdentifiers.COMPONENT_SIMULATION_PROCESSING_GATEWAY,
                                 mockSimulationProcessingGateway);
  }

  @Test
  public void testConstructor() {
    assertEquals(dummyOrder, inputValueChange.getOrder());
  }

  @Test
  public void testGetNature() {
    assertEquals(MessageKey.SIMULATION_CHANGE_INPUT_VALUES.getBundleIdentifier(),
                 inputValueChange.getNature());
  }

  @Test
  public void testRequiresReRunReturnsTrueIfSimulationProcessingGatewayTimesOut() {
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockSimulationProcessingGateway.inputDataGathering(mockSimulation))
          .andReturn(null);
    final Capture<OverallProgress> captureOverallProgress = newCapture();
    mockJmsTemplate.sendProgress(capture(captureOverallProgress));

    mocksControl.replay();

    final boolean requiresReRun = inputValueChange.requiresReRun(mockSimulation);

    mocksControl.verify();

    assertFalse(requiresReRun);
    final OverallProgress overallProgress = captureOverallProgress.getValue();
    assertSame(InformationLevel.DEBUG, overallProgress.getLevel());
    assertEquals("Input data gathering timed out.", overallProgress.getText());
    assertSame(dummySimulationId, overallProgress.getSimulationId());
  }

  @Test
  public void testRequiresReRunReturnsFalseIfNoSimulationInputs() {
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    final ProcessedDataVO mockProcessedDataVO = mocksControl.createMock(ProcessedDataVO.class);
    expect(mockSimulationProcessingGateway.inputDataGathering(mockSimulation))
          .andReturn(mockProcessedDataVO);
    final Set<GroupedInvocationInput> dummyNewSimulationInputs = new HashSet<GroupedInvocationInput>();
    expect(mockProcessedDataVO.getProcessedData()).andReturn(dummyNewSimulationInputs);
    final List<GroupedInvocationInput> dummyPreviousSimulationInputs = new ArrayList<GroupedInvocationInput>();
    expect(mockResultsManager.retrieveInputValuesEntities(dummySimulationId))
          .andReturn(dummyPreviousSimulationInputs);
    final Capture<Progress> dummyOverallProgress = newCapture();
    mockJmsTemplate.sendProgress(capture(dummyOverallProgress));

    mocksControl.replay();

    final boolean requiresReRun = inputValueChange.requiresReRun(mockSimulation);

    mocksControl.verify();

    assertFalse(requiresReRun);
    final OverallProgress capturedOverallProgress = (OverallProgress) dummyOverallProgress.getValue();
    assertEquals(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertSame(InformationLevel.DEBUG, capturedOverallProgress.getLevel());
  }

  @Test
  public void testRequiresReRunReturnsTrueIfDifferentGroupedInvocationInputsCounts() {
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    final ProcessedDataVO mockNewProcessedDataVO = mocksControl.createMock(ProcessedDataVO.class);
    expect(mockSimulationProcessingGateway.inputDataGathering(mockSimulation))
          .andReturn(mockNewProcessedDataVO);

    final Set<GroupedInvocationInput> dummyNewSimulationInputs = new HashSet<GroupedInvocationInput>();
    final GroupedInvocationInput mockNewGroupedInvocationInput = mocksControl.createMock(GroupedInvocationInput.class);
    dummyNewSimulationInputs.add(mockNewGroupedInvocationInput);
    expect(mockNewProcessedDataVO.getProcessedData()).andReturn(dummyNewSimulationInputs);

    final List<GroupedInvocationInput> dummyPreviousSimulationInputs = new ArrayList<GroupedInvocationInput>();
    final GroupedInvocationInput mockPreviousSimulationInputs1 = mocksControl.createMock(GroupedInvocationInput.class);
    final GroupedInvocationInput mockPreviousSimulationInputs2 = mocksControl.createMock(GroupedInvocationInput.class);
    dummyPreviousSimulationInputs.add(mockPreviousSimulationInputs1);
    dummyPreviousSimulationInputs.add(mockPreviousSimulationInputs2);
    expect(mockResultsManager.retrieveInputValuesEntities(dummySimulationId))
          .andReturn(dummyPreviousSimulationInputs);

    final Capture<Progress> dummyOverallProgress = newCapture();
    mockJmsTemplate.sendProgress(capture(dummyOverallProgress));

    mocksControl.replay();

    final boolean requiresReRun = inputValueChange.requiresReRun(mockSimulation);

    mocksControl.verify();

    assertTrue(requiresReRun);
    final OverallProgress capturedOverallProgress = (OverallProgress) dummyOverallProgress.getValue();
    assertEquals(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertSame(InformationLevel.INFO, capturedOverallProgress.getLevel());
  }

  @Test
  public void testRequiresReRunReturnsTrueIfDifferentIonChannelValues() {
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    final ProcessedDataVO mockNewProcessedDataVO = mocksControl.createMock(ProcessedDataVO.class);
    expect(mockSimulationProcessingGateway.inputDataGathering(mockSimulation))
          .andReturn(mockNewProcessedDataVO);

    final Set<GroupedInvocationInput> dummyNewSimulationInputs = new HashSet<GroupedInvocationInput>();
    final GroupedInvocationInput mockNewGroupedInvocationInput = mocksControl.createMock(GroupedInvocationInput.class);
    dummyNewSimulationInputs.add(mockNewGroupedInvocationInput);
    expect(mockNewProcessedDataVO.getProcessedData()).andReturn(dummyNewSimulationInputs);

    final List<GroupedInvocationInput> dummyPreviousSimulationInputs = new ArrayList<GroupedInvocationInput>();
    final GroupedInvocationInput mockPreviousSimulationInputs = mocksControl.createMock(GroupedInvocationInput.class);
    dummyPreviousSimulationInputs.add(mockPreviousSimulationInputs);
    expect(mockResultsManager.retrieveInputValuesEntities(dummySimulationId))
          .andReturn(dummyPreviousSimulationInputs);

    // >> inputValuesDiffer()
    final Short dummyAssayGroupLevel = Short.valueOf("2");
    final String dummyAssayGroupName = "dummyAssayGroupName";
    final Set<IonChannelValues> dummyNewIonChannelValues = new HashSet<IonChannelValues>();
    final IonChannelValues mockNewIonChannelValues = mocksControl.createMock(IonChannelValues.class);
    dummyNewIonChannelValues.add(mockNewIonChannelValues);
    expect(mockNewGroupedInvocationInput.getAssayGroupLevel()).andReturn(dummyAssayGroupLevel);
    expect(mockNewGroupedInvocationInput.getAssayGroupName()).andReturn(dummyAssayGroupName);
    expect(mockNewGroupedInvocationInput.getIonChannelValues()).andReturn(dummyNewIonChannelValues);

    expect(mockPreviousSimulationInputs.getAssayGroupLevel()).andReturn(dummyAssayGroupLevel);
    expect(mockPreviousSimulationInputs.getAssayGroupName()).andReturn(dummyAssayGroupName);
    final Set<IonChannelValues> dummyPreviousIonChannelValues = new HashSet<IonChannelValues>();
    final IonChannelValues mockPreviousIonChannelValues = mocksControl.createMock(IonChannelValues.class);
    dummyPreviousIonChannelValues.add(mockPreviousIonChannelValues);
    expect(mockPreviousSimulationInputs.getIonChannelValues()).andReturn(dummyPreviousIonChannelValues);
    // >> ionChannelValuesSame()
    final String dummyIonChannelName = "dummyIonChannelName";
    final List<PIC50Data> dummyNewPIC50Data = new ArrayList<PIC50Data>();
    final String dummyNewSourceAssayName = "dummyNewSourceAssayName";
    expect(mockNewIonChannelValues.getIonChannelName()).andReturn(dummyIonChannelName);
    expect(mockNewIonChannelValues.getpIC50Data()).andReturn(dummyNewPIC50Data);
    expect(mockNewIonChannelValues.getSourceAssayName()).andReturn(dummyNewSourceAssayName);
    expect(mockPreviousIonChannelValues.getIonChannelName()).andReturn(dummyIonChannelName);
    expect(mockPreviousIonChannelValues.getpIC50Data()).andReturn(dummyNewPIC50Data);
    final String dummyPreviousSourceAssayName = "dummyPreviousSourceAssayName";
    expect(mockPreviousIonChannelValues.getSourceAssayName()).andReturn(dummyPreviousSourceAssayName);
    // << ionChannelValuesSame()
    // << inputValuesDiffer()

    mocksControl.replay();

    final boolean requiresReRun = inputValueChange.requiresReRun(mockSimulation);

    mocksControl.verify();

    assertTrue(requiresReRun);
  }

  @Test
  public void testRequiresReRunReturnsTrueIfDifferentPIC50Data() {
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    final ProcessedDataVO mockNewProcessedDataVO = mocksControl.createMock(ProcessedDataVO.class);
    expect(mockSimulationProcessingGateway.inputDataGathering(mockSimulation))
          .andReturn(mockNewProcessedDataVO);

    final Set<GroupedInvocationInput> dummyNewSimulationInputs = new HashSet<GroupedInvocationInput>();
    final GroupedInvocationInput mockNewGroupedInvocationInput = mocksControl.createMock(GroupedInvocationInput.class);
    dummyNewSimulationInputs.add(mockNewGroupedInvocationInput);
    expect(mockNewProcessedDataVO.getProcessedData()).andReturn(dummyNewSimulationInputs);

    final List<GroupedInvocationInput> dummyPreviousSimulationInputs = new ArrayList<GroupedInvocationInput>();
    final GroupedInvocationInput mockPreviousSimulationInputs = mocksControl.createMock(GroupedInvocationInput.class);
    dummyPreviousSimulationInputs.add(mockPreviousSimulationInputs);
    expect(mockResultsManager.retrieveInputValuesEntities(dummySimulationId))
          .andReturn(dummyPreviousSimulationInputs);

    // >> inputValuesDiffer()
    final Short dummyAssayGroupLevel = Short.valueOf("2");
    final String dummyAssayGroupName = "dummyAssayGroupName";
    final Set<IonChannelValues> dummyNewIonChannelValues = new HashSet<IonChannelValues>();
    final IonChannelValues mockNewIonChannelValues = mocksControl.createMock(IonChannelValues.class);
    dummyNewIonChannelValues.add(mockNewIonChannelValues);
    expect(mockNewGroupedInvocationInput.getAssayGroupLevel()).andReturn(dummyAssayGroupLevel);
    expect(mockNewGroupedInvocationInput.getAssayGroupName()).andReturn(dummyAssayGroupName);
    expect(mockNewGroupedInvocationInput.getIonChannelValues()).andReturn(dummyNewIonChannelValues);

    expect(mockPreviousSimulationInputs.getAssayGroupLevel()).andReturn(dummyAssayGroupLevel);
    expect(mockPreviousSimulationInputs.getAssayGroupName()).andReturn(dummyAssayGroupName);
    final Set<IonChannelValues> dummyPreviousIonChannelValues = new HashSet<IonChannelValues>();
    final IonChannelValues mockPreviousIonChannelValues = mocksControl.createMock(IonChannelValues.class);
    dummyPreviousIonChannelValues.add(mockPreviousIonChannelValues);
    expect(mockPreviousSimulationInputs.getIonChannelValues()).andReturn(dummyPreviousIonChannelValues);
    // >> ionChannelValuesSame()
    final String dummyIonChannelName = "dummyIonChannelName";
    final List<PIC50Data> dummyNewPIC50Data = new ArrayList<PIC50Data>();
    final PIC50Data mockNewPIC50Data = mocksControl.createMock(PIC50Data.class);
    dummyNewPIC50Data.add(mockNewPIC50Data);
    final String dummySourceAssayName = "dummySourceAssayName";
    expect(mockNewIonChannelValues.getIonChannelName()).andReturn(dummyIonChannelName);
    expect(mockNewIonChannelValues.getpIC50Data()).andReturn(dummyNewPIC50Data);
    expect(mockNewIonChannelValues.getSourceAssayName()).andReturn(dummySourceAssayName);

    final List<PIC50Data> dummyPreviousPIC50Data = new ArrayList<PIC50Data>();
    final PIC50Data mockPreviousPIC50Data = mocksControl.createMock(PIC50Data.class);
    dummyPreviousPIC50Data.add(mockPreviousPIC50Data);
    expect(mockPreviousIonChannelValues.getIonChannelName()).andReturn(dummyIonChannelName);
    expect(mockPreviousIonChannelValues.getpIC50Data()).andReturn(dummyPreviousPIC50Data);
    expect(mockPreviousIonChannelValues.getSourceAssayName()).andReturn(dummySourceAssayName);
    // >> pIC50DataSame()
    // << pIC50DataSame()
    // << ionChannelValuesSame()
    // << inputValuesDiffer()

    mocksControl.replay();

    final boolean requiresReRun = inputValueChange.requiresReRun(mockSimulation);

    mocksControl.verify();

    assertTrue(requiresReRun);
  }
}