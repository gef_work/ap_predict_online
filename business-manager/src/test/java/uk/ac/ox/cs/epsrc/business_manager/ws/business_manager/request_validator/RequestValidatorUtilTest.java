/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.business_manager.request_validator;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.ws.business_manager.request_validator.RequestValidatorUtil;

/**
 * Unit test the request validator utility class.
 *
 * @author geoff
 */
public class RequestValidatorUtilTest {

  /**
   * Test the generic string validation routine.
   */
  @Test
  public void testGenericStringValidation() {
    try {
      RequestValidatorUtil.genericStringCollectionValidator(null, -1, 0, null);
      fail("Maximum length must be greater than or equal to minimum length");
    } catch (AssertionError e) {
      
    }
    // null collection
    String problem = RequestValidatorUtil.genericStringCollectionValidator(null, 0, 0, null);
    assertNotNull(problem);
    // empty collection
    Set<String> values = new HashSet<String>();
    problem = RequestValidatorUtil.genericStringCollectionValidator(values, 0, 0, null);
    assertNotNull(problem);
    // null in collection
    values.add(null);
    problem = RequestValidatorUtil.genericStringCollectionValidator(values, 0, 0, null);
    assertNotNull(problem);
    // empty in collection
    values.clear();
    values.add("");
    problem = RequestValidatorUtil.genericStringCollectionValidator(values, 0, 0, null);
    assertNotNull(problem);
    // min length exceeded
    values.clear();
    values.add("a");
    problem = RequestValidatorUtil.genericStringCollectionValidator(values, 3, 2, null);
    assertNotNull(problem);
    // max length exceeded
    values.clear();
    values.add("a");
    problem = RequestValidatorUtil.genericStringCollectionValidator(values, 0, 0, null);
    assertNotNull(problem);
    // everything ok
    values.clear();
    values.add("a");
    problem = RequestValidatorUtil.genericStringCollectionValidator(values, 1, 0, null);
    assertNull(problem);
  }
}