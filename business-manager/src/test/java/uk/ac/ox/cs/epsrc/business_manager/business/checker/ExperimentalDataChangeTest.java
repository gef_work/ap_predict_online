/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.checker;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.SimulationProcessingGateway;
import uk.ac.ox.cs.epsrc.business_manager.business.checker.ExperimentalDataChange;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.config.ConfigurationActionPotential;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Unit test the experimental data change checker.
 *
 * @author geoff
 */
public class ExperimentalDataChangeTest {

  private BusinessManagerJMSTemplate mockJMSTemplate;
  private ConfigurationActionPotential mockConfigurationAP;
  private ExperimentalDataChange experimentalDataChange;
  private IMocksControl mocksControl;
  private static final int dummyOrder = 2; 
  private static final Long dummySimulationId = 1L;
  private Map<BigDecimal, Set<BigDecimal>> dummyNewPerFreqConcs;
  private Map<BigDecimal, Set<BigDecimal>> dummyPreviousPerFreqConcs;
  private ProcessedDataVO mockProcessedData;
  private Simulation mockSimulation;
  private SimulationManager mockSimulationManager;
  private SimulationProcessingGateway mockSimulationProcessingGateway;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockConfigurationAP = mocksControl.createMock(ConfigurationActionPotential.class);
    mockJMSTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    mockProcessedData = mocksControl.createMock(ProcessedDataVO.class);
    mockSimulation = mocksControl.createMock(Simulation.class);
    mockSimulationManager = mocksControl.createMock(SimulationManager.class);
    mockSimulationProcessingGateway = mocksControl.createMock(SimulationProcessingGateway.class);

    experimentalDataChange = new ExperimentalDataChange(dummyOrder);
    ReflectionTestUtils.setField(experimentalDataChange, BusinessIdentifiers.COMPONENT_CONFIGURATION_ACTIONPOTENTIAL,
                                 mockConfigurationAP);
    ReflectionTestUtils.setField(experimentalDataChange, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockJMSTemplate);
    ReflectionTestUtils.setField(experimentalDataChange, BusinessIdentifiers.COMPONENT_SIMULATION_MANAGER,
                                 mockSimulationManager);
    ReflectionTestUtils.setField(experimentalDataChange, BusinessIdentifiers.COMPONENT_SIMULATION_PROCESSING_GATEWAY,
                                 mockSimulationProcessingGateway);
  }

  @Test
  public void testConstructor() {
    assertEquals(dummyOrder, experimentalDataChange.getOrder());
    assertEquals(MessageKey.SIMULATION_CHANGE_EXPERIMENTAL.getBundleIdentifier(),
                 experimentalDataChange.getNature());
  }

  @Test
  public void testRequiresReRunReturnsFalseIfNoPerFrequencyConcentrationsData() {
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockSimulationProcessingGateway.inputDataGathering(mockSimulation))
          .andReturn(mockProcessedData);
    dummyNewPerFreqConcs = new HashMap<BigDecimal, Set<BigDecimal>>();
    expect(mockProcessedData.getPerFrequencyConcentrations())
          .andReturn(dummyNewPerFreqConcs);
    expect(mockConfigurationAP.getDefaultPerFrequencyConcentrations())
          .andReturn(dummyNewPerFreqConcs);
    dummyPreviousPerFreqConcs = new HashMap<BigDecimal, Set<BigDecimal>>();
    expect(mockSimulationManager.retrievePerFrequencyConcentrations(dummySimulationId))
          .andReturn(dummyPreviousPerFreqConcs);
    // >> experimentalDataDiffers()
    // << experimentalDataDiffers()
    final Capture<OverallProgress> captureOverallProgress = newCapture();
    mockJMSTemplate.sendProgress(capture(captureOverallProgress));

    mocksControl.replay();

    final boolean requiresReRun = experimentalDataChange.requiresReRun(mockSimulation);

    mocksControl.verify();

    assertFalse(requiresReRun);
    final OverallProgress overallProgress = captureOverallProgress.getValue();
    assertSame(InformationLevel.DEBUG, overallProgress.getLevel());
    assertEquals("Experimental data re-run checked : No change in experimental data.",
                 overallProgress.getText());
    assertSame(dummySimulationId, overallProgress.getSimulationId());
  }

  @Test
  public void testRequiresReRunReturnsFalseIfSamePerFrequencyConcentrationsData() {
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockSimulationProcessingGateway.inputDataGathering(mockSimulation))
          .andReturn(mockProcessedData);
    dummyNewPerFreqConcs = new HashMap<BigDecimal, Set<BigDecimal>>();
    dummyNewPerFreqConcs.put(BigDecimal.ZERO,
                             new HashSet<BigDecimal>(Arrays.asList(BigDecimal.TEN, BigDecimal.ZERO, BigDecimal.ONE)));
    dummyNewPerFreqConcs.put(BigDecimal.TEN,
                             new HashSet<BigDecimal>(Arrays.asList(BigDecimal.TEN, BigDecimal.ONE, BigDecimal.ZERO)));
    expect(mockProcessedData.getPerFrequencyConcentrations())
          .andReturn(dummyNewPerFreqConcs);
    dummyPreviousPerFreqConcs = new HashMap<BigDecimal, Set<BigDecimal>>();
    dummyPreviousPerFreqConcs.put(BigDecimal.TEN,
                                  new HashSet<BigDecimal>(Arrays.asList(BigDecimal.ZERO, BigDecimal.TEN, BigDecimal.ONE)));
    dummyPreviousPerFreqConcs.put(BigDecimal.ZERO,
                                  new HashSet<BigDecimal>(Arrays.asList(BigDecimal.ONE, BigDecimal.ZERO, BigDecimal.TEN)));
    expect(mockSimulationManager.retrievePerFrequencyConcentrations(dummySimulationId))
          .andReturn(dummyPreviousPerFreqConcs);
    // >> experimentalDataDiffers()
    // << experimentalDataDiffers()
    final Capture<OverallProgress> captureOverallProgress = newCapture();
    mockJMSTemplate.sendProgress(capture(captureOverallProgress));

    mocksControl.replay();

    final boolean requiresReRun = experimentalDataChange.requiresReRun(mockSimulation);

    mocksControl.verify();

    assertFalse(requiresReRun);
    final OverallProgress overallProgress = captureOverallProgress.getValue();
    assertSame(InformationLevel.DEBUG, overallProgress.getLevel());
    assertEquals("Experimental data re-run checked : No change in experimental data.",
                 overallProgress.getText());
    assertSame(dummySimulationId, overallProgress.getSimulationId());
  }

  @Test
  public void testRequiresReRunReturnsTrueIfDifferentPerFrequencyConcentrationsData1() {
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockSimulationProcessingGateway.inputDataGathering(mockSimulation))
          .andReturn(mockProcessedData);
    dummyNewPerFreqConcs = new HashMap<BigDecimal, Set<BigDecimal>>();
    dummyNewPerFreqConcs.put(BigDecimal.ZERO,
                             new HashSet<BigDecimal>(Arrays.asList(BigDecimal.TEN, BigDecimal.ZERO, BigDecimal.ONE)));
    dummyNewPerFreqConcs.put(BigDecimal.TEN,
                             new HashSet<BigDecimal>(Arrays.asList(BigDecimal.TEN, BigDecimal.ONE, BigDecimal.ZERO)));
    expect(mockProcessedData.getPerFrequencyConcentrations())
          .andReturn(dummyNewPerFreqConcs);
    dummyPreviousPerFreqConcs = new HashMap<BigDecimal, Set<BigDecimal>>();
    dummyPreviousPerFreqConcs.put(BigDecimal.TEN,
                                  new HashSet<BigDecimal>(Arrays.asList(BigDecimal.ZERO, BigDecimal.TEN, BigDecimal.ONE)));
    dummyPreviousPerFreqConcs.put(BigDecimal.ZERO,
                                  new HashSet<BigDecimal>(Arrays.asList(BigDecimal.ONE, BigDecimal.TEN)));
    expect(mockSimulationManager.retrievePerFrequencyConcentrations(dummySimulationId))
          .andReturn(dummyPreviousPerFreqConcs);
    // >> experimentalDataDiffers()
    // << experimentalDataDiffers()

    mocksControl.replay();

    final boolean requiresReRun = experimentalDataChange.requiresReRun(mockSimulation);

    mocksControl.verify();

    assertTrue(requiresReRun);
  }

  @Test
  public void testRequiresReRunReturnsTrueIfDifferentPerFrequencyConcentrationsData2() {
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockSimulationProcessingGateway.inputDataGathering(mockSimulation))
          .andReturn(mockProcessedData);
    dummyNewPerFreqConcs = new HashMap<BigDecimal, Set<BigDecimal>>();
    dummyNewPerFreqConcs.put(BigDecimal.ZERO,
                             new HashSet<BigDecimal>(Arrays.asList(BigDecimal.TEN, BigDecimal.ZERO, BigDecimal.ONE)));
    dummyNewPerFreqConcs.put(BigDecimal.TEN,
                             new HashSet<BigDecimal>(Arrays.asList(BigDecimal.TEN, BigDecimal.ONE, BigDecimal.ZERO)));
    expect(mockProcessedData.getPerFrequencyConcentrations())
          .andReturn(dummyNewPerFreqConcs);
    dummyPreviousPerFreqConcs = new HashMap<BigDecimal, Set<BigDecimal>>();
    dummyPreviousPerFreqConcs.put(BigDecimal.TEN,
                                  new HashSet<BigDecimal>(Arrays.asList(new BigDecimal("4.3"), BigDecimal.TEN, BigDecimal.ONE)));
    dummyPreviousPerFreqConcs.put(BigDecimal.ZERO,
                                  new HashSet<BigDecimal>(Arrays.asList(BigDecimal.ONE, BigDecimal.ZERO, BigDecimal.TEN)));
    expect(mockSimulationManager.retrievePerFrequencyConcentrations(dummySimulationId))
          .andReturn(dummyPreviousPerFreqConcs);
    // >> experimentalDataDiffers()
    // << experimentalDataDiffers()

    mocksControl.replay();

    final boolean requiresReRun = experimentalDataChange.requiresReRun(mockSimulation);

    mocksControl.verify();

    assertTrue(requiresReRun);
  }

  @Test
  public void testRequiresReRunReturnsTrueIfDifferentPerFrequencyConcentrationsData3() {
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockSimulationProcessingGateway.inputDataGathering(mockSimulation))
          .andReturn(mockProcessedData);
    dummyNewPerFreqConcs = new HashMap<BigDecimal, Set<BigDecimal>>();
    dummyNewPerFreqConcs.put(BigDecimal.ZERO,
                             new HashSet<BigDecimal>(Arrays.asList(BigDecimal.TEN, BigDecimal.ZERO, BigDecimal.ONE)));
    dummyNewPerFreqConcs.put(BigDecimal.TEN,
                             new HashSet<BigDecimal>(Arrays.asList(BigDecimal.TEN, BigDecimal.ONE, BigDecimal.ZERO)));
    expect(mockProcessedData.getPerFrequencyConcentrations())
          .andReturn(dummyNewPerFreqConcs);
    dummyPreviousPerFreqConcs = new HashMap<BigDecimal, Set<BigDecimal>>();
    dummyPreviousPerFreqConcs.put(BigDecimal.TEN,
                                  new HashSet<BigDecimal>(Arrays.asList(BigDecimal.ZERO, BigDecimal.TEN, BigDecimal.ONE)));
    expect(mockSimulationManager.retrievePerFrequencyConcentrations(dummySimulationId))
          .andReturn(dummyPreviousPerFreqConcs);
    // >> experimentalDataDiffers()
    // << experimentalDataDiffers()

    mocksControl.replay();

    final boolean requiresReRun = experimentalDataChange.requiresReRun(mockSimulation);

    mocksControl.verify();

    assertTrue(requiresReRun);
  }

  @Test
  public void testRequiresReRunReturnsTrueIfDifferentPerFrequencyConcentrationsData4() {
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockSimulationProcessingGateway.inputDataGathering(mockSimulation))
          .andReturn(mockProcessedData);
    dummyNewPerFreqConcs = new HashMap<BigDecimal, Set<BigDecimal>>();
    dummyNewPerFreqConcs.put(BigDecimal.ZERO,
                             new HashSet<BigDecimal>(Arrays.asList(BigDecimal.TEN, BigDecimal.ZERO, BigDecimal.ONE)));
    dummyNewPerFreqConcs.put(BigDecimal.TEN,
                             new HashSet<BigDecimal>(Arrays.asList(BigDecimal.TEN, BigDecimal.ONE, BigDecimal.ZERO)));
    expect(mockProcessedData.getPerFrequencyConcentrations())
          .andReturn(dummyNewPerFreqConcs);
    dummyPreviousPerFreqConcs = new HashMap<BigDecimal, Set<BigDecimal>>();
    dummyPreviousPerFreqConcs.put(BigDecimal.TEN,
                                  new HashSet<BigDecimal>(Arrays.asList(BigDecimal.ZERO, BigDecimal.TEN, BigDecimal.ONE)));
    dummyPreviousPerFreqConcs.put(BigDecimal.ONE,
                                  new HashSet<BigDecimal>(Arrays.asList(BigDecimal.ONE, BigDecimal.ZERO, BigDecimal.TEN)));
    expect(mockSimulationManager.retrievePerFrequencyConcentrations(dummySimulationId))
          .andReturn(dummyPreviousPerFreqConcs);
    // >> experimentalDataDiffers()
    // << experimentalDataDiffers()

    mocksControl.replay();

    final boolean requiresReRun = experimentalDataChange.requiresReRun(mockSimulation);

    mocksControl.verify();

    assertTrue(requiresReRun);
  }

  @Test
  public void testRequiresReRunReturnsTrueIfDifferentPerFrequencyConcentrationsData5() {
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockSimulationProcessingGateway.inputDataGathering(mockSimulation))
          .andReturn(mockProcessedData);
    dummyNewPerFreqConcs = new HashMap<BigDecimal, Set<BigDecimal>>();
    expect(mockProcessedData.getPerFrequencyConcentrations())
          .andReturn(dummyNewPerFreqConcs);
    final Map<BigDecimal, Set<BigDecimal>> dummyConfigurationPerFrequencyConcs = new HashMap<BigDecimal, Set<BigDecimal>>();
    final BigDecimal dummyFrequency = BigDecimal.ONE;
    final Set<BigDecimal> dummyConcentrations = new HashSet<BigDecimal>();
    final BigDecimal dummyConcentration = BigDecimal.TEN;
    dummyConcentrations.add(dummyConcentration);
    dummyConfigurationPerFrequencyConcs.put(dummyFrequency, dummyConcentrations);
    expect(mockConfigurationAP.getDefaultPerFrequencyConcentrations())
          .andReturn(dummyConfigurationPerFrequencyConcs);

    dummyPreviousPerFreqConcs = new HashMap<BigDecimal, Set<BigDecimal>>();
    dummyPreviousPerFreqConcs.put(BigDecimal.TEN,
                                  new HashSet<BigDecimal>(Arrays.asList(BigDecimal.ZERO, BigDecimal.TEN, BigDecimal.ONE)));
    dummyPreviousPerFreqConcs.put(BigDecimal.ONE,
                                  new HashSet<BigDecimal>(Arrays.asList(BigDecimal.ONE, BigDecimal.ZERO, BigDecimal.TEN)));
    expect(mockSimulationManager.retrievePerFrequencyConcentrations(dummySimulationId))
          .andReturn(dummyPreviousPerFreqConcs);
    // >> experimentalDataDiffers()
    // << experimentalDataDiffers()

    mocksControl.replay();

    final boolean requiresReRun = experimentalDataChange.requiresReRun(mockSimulation);

    mocksControl.verify();

    assertTrue(requiresReRun);
  }

  @Test
  public void testRequiresReRunReturnsTrueIfInputDataGatheringTimesOut() {
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockSimulationProcessingGateway.inputDataGathering(mockSimulation)).andReturn(null);
    final Capture<OverallProgress> captureOverallProgress = newCapture();
    mockJMSTemplate.sendProgress(capture(captureOverallProgress));

    mocksControl.replay();

    final boolean requiresReRun = experimentalDataChange.requiresReRun(mockSimulation);

    mocksControl.verify();

    assertFalse(requiresReRun);
    final OverallProgress overallProgress = captureOverallProgress.getValue();
    assertSame(InformationLevel.DEBUG, overallProgress.getLevel());
    assertEquals("Input data gathering timed out.", overallProgress.getText());
    assertSame(dummySimulationId, overallProgress.getSimulationId());
  }
}