/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 * 
 */
package uk.ac.ox.cs.epsrc.business_manager.entity;

import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

/**
 * Unit test IonChannelValues object.
 *
 * @author geoff
 */
public class IonChannelValuesTest {

  private IMocksControl mocksControl;
  private IonChannelValues ionChannelValues;

  @Before
  public void setUp() {
    mocksControl = createControl();
  }

  @Test
  public void testDefaultConstructor() throws CloneNotSupportedException {
    ionChannelValues = new IonChannelValues();

    assertNull(ionChannelValues.getIonChannelName());
    assertTrue(ionChannelValues.getpIC50Data().isEmpty());
    assertNull(ionChannelValues.getSourceAssayName());
    assertTrue(ionChannelValues.retrieveClonedPIC50Data().isEmpty());
    assertNull(ionChannelValues.getSourceAssayName());
    assertTrue(ionChannelValues.hashCode() > 0);
    assertNotNull(ionChannelValues.toString());
    IonChannelValues cloned = ionChannelValues.clone();
    assertNotNull(cloned);
    assertEquals(cloned, ionChannelValues);
  }

  @Test
  public void testInitialisingConstructor() throws CloneNotSupportedException {
    String dummyIonChannelName = null;
    String dummySourceAssayName = null;
    List<PIC50Data> dummyPIC50Data = null;

    ionChannelValues = new IonChannelValues(dummyIonChannelName, dummySourceAssayName,
                                            dummyPIC50Data);

    assertNull(ionChannelValues.getIonChannelName());
    assertTrue(ionChannelValues.getpIC50Data().isEmpty());
    assertNull(ionChannelValues.getSourceAssayName());
    assertTrue(ionChannelValues.retrieveClonedPIC50Data().isEmpty());
    assertNull(ionChannelValues.getSourceAssayName());
    assertTrue(ionChannelValues.hashCode() > 0);
    assertNotNull(ionChannelValues.toString());
    IonChannelValues cloned = ionChannelValues.clone();
    assertNotNull(cloned);
    assertEquals(cloned, ionChannelValues);

    dummyIonChannelName = "dummyIonChannelName";
    dummySourceAssayName = "dummySourceAssayName";
    dummyPIC50Data = new ArrayList<PIC50Data>();

    final PIC50Data mockPIC50Data1 = mocksControl.createMock(PIC50Data.class);
    dummyPIC50Data.add(mockPIC50Data1);
    final PIC50Data mockPIC50Data2 = mocksControl.createMock(PIC50Data.class);
    dummyPIC50Data.add(mockPIC50Data2);

    mockPIC50Data1.setIonChannelValues(isA(IonChannelValues.class));
    mockPIC50Data2.setIonChannelValues(isA(IonChannelValues.class));

    mocksControl.replay();

    ionChannelValues = new IonChannelValues(dummyIonChannelName, dummySourceAssayName,
                                            dummyPIC50Data);

    mocksControl.verify();

    mocksControl.reset();

    mockPIC50Data1.setIonChannelValues(isA(IonChannelValues.class));
    mockPIC50Data2.setIonChannelValues(isA(IonChannelValues.class));

    final PIC50Data clonedPIC50Data1 = mocksControl.createMock(PIC50Data.class);
    final PIC50Data clonedPIC50Data2 = mocksControl.createMock(PIC50Data.class);

    // This is ionChannelValues.retrieveCloendPIC50Data() test below
    expect(mockPIC50Data1.clone()).andReturn(clonedPIC50Data1);
    expect(mockPIC50Data2.clone()).andReturn(clonedPIC50Data2);

    // This is ionChannelValues.clone() test below
    expect(mockPIC50Data1.clone()).andReturn(clonedPIC50Data1);
    expect(mockPIC50Data2.clone()).andReturn(clonedPIC50Data2);
    clonedPIC50Data1.setIonChannelValues(isA(IonChannelValues.class));
    clonedPIC50Data2.setIonChannelValues(isA(IonChannelValues.class));

    mocksControl.replay();

    ionChannelValues = new IonChannelValues(dummyIonChannelName, dummySourceAssayName,
                                            dummyPIC50Data);

    final List<PIC50Data> clonedPIC50Data = ionChannelValues.retrieveClonedPIC50Data();
    assertEquals(2, clonedPIC50Data.size());

    cloned = ionChannelValues.clone();
    assertEquals(cloned, ionChannelValues);

    mocksControl.verify();

    assertEquals(dummyIonChannelName, ionChannelValues.getIonChannelName());
    assertFalse(ionChannelValues.getpIC50Data().isEmpty());
    assertEquals(dummySourceAssayName, ionChannelValues.getSourceAssayName());
    assertEquals(dummySourceAssayName, ionChannelValues.getSourceAssayName());
    assertNotNull(ionChannelValues.toString());
  }

  @Test
  public void testSetParent() {
    String dummyIonChannelName = null;
    String dummySourceAssayName = null;
    List<PIC50Data> dummyPIC50Data = null;

    ionChannelValues = new IonChannelValues(dummyIonChannelName, dummySourceAssayName,
                                            dummyPIC50Data);

    // Won't currently fail
    ionChannelValues.setParent(null);

    GroupedInvocationInput mockGroupedInvocationInput = mocksControl.createMock(GroupedInvocationInput.class);

    ionChannelValues.setParent(mockGroupedInvocationInput);
    // Will only fail when the id != null
    ionChannelValues.setParent(mockGroupedInvocationInput);
    ReflectionTestUtils.setField(ionChannelValues, "id", 1L);
    try {
      ionChannelValues.setParent(mockGroupedInvocationInput);
      fail("Should not accept resetting a parent once persisted!");
    } catch (UnsupportedOperationException e) {}
  }
}