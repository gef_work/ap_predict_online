/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.selection.c50;

import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.business.selection.c50.C50DataSelector;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.selection.ic.ICDataSelectionStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.NoSuchDataException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;

/**
 * Unit test the IC50 / pIC50 / pXC50 data selector.
 *
 * @author geoff
 */
public class C50DataSelectorTest {

  @Test
  public void testConstructor() {
    new C50DataSelector();
  }

  /**
   * Test null or empty eligible collection processing.
   */
  @Test
  public void testNullOrEmptyEligibleCollection() {
    List<ICData> eligibleC50Data = null;
    try {
      C50DataSelector.selectFrom(eligibleC50Data);
      fail("Should not allow a null eligibleC50Data parameter to be used.");
    } catch (IllegalArgumentException e) {}

    eligibleC50Data = new ArrayList<ICData>();
    final ICData icData = C50DataSelector.selectFrom(eligibleC50Data);
    assertNull(icData);
  }

  /**
   * Test that valid data is processed appropriately.
   * @throws NoSuchDataException 
   */
  @Test
  public void testValidInputDataProcessing() throws NoSuchDataException {
    final List<ICData> eligibleC50Data = new ArrayList<ICData>();
    final IMocksControl mocksControl = createControl();
    final ICData mockIC50Data = mocksControl.createMock(ICData.class);
    final ICData mockPC50Data = mocksControl.createMock(ICData.class);
    eligibleC50Data.add(mockIC50Data);
    eligibleC50Data.add(mockPC50Data);
    // Traverse the strategies, for each one traverse the eligible ICData.
    // 1. PC50+Equality on mockIC50
    // 2. PC50+Equality on mockPC50
    // 3. PC50+Non-equality on mockIC50
    // 4. PC50+Non-equality on mockPC50 success!
    // 5. Any IC20 on mockIC50
    expect(mockIC50Data.hasPC50Source()).andReturn(false).times(2);
    expect(mockPC50Data.hasPC50Source()).andReturn(true).times(2);
    expect(mockPC50Data.isModifierEquals(ICPercent.PCT50)).andReturn(false).times(2);
    expect(mockIC50Data.hasICPctData(ICPercent.PCT20)).andReturn(false);
    expect(mockPC50Data.hasICPctData(ICPercent.PCT20)).andReturn(false);
    mocksControl.replay();
    final ICData icData = C50DataSelector.selectFrom(eligibleC50Data);
    mocksControl.verify();
    assertNotNull(icData);
    assertSame(mockPC50Data, icData);
  }

  @Test
  public void testSettingC50DataSelectionStrategies() {
    List<ICDataSelectionStrategy> dummyC50DataSelectionStrategies = null;
    try {
      C50DataSelector.setC50DataSelectionStrategies(dummyC50DataSelectionStrategies);
      fail("Should not permit a null C50 data selection strategy collection.");
    } catch (IllegalArgumentException e) {}

    dummyC50DataSelectionStrategies = new ArrayList<ICDataSelectionStrategy>(); 
    try {
      C50DataSelector.setC50DataSelectionStrategies(dummyC50DataSelectionStrategies);
      fail("Should not permit an empty C50 data selection strategy collection.");
    } catch (IllegalArgumentException e) {}

    final IMocksControl mocksControl = createControl();
    final ICDataSelectionStrategy mockC50DataSelectionStrategy = mocksControl.createMock(ICDataSelectionStrategy.class);
    dummyC50DataSelectionStrategies.add(mockC50DataSelectionStrategy);

    C50DataSelector.setC50DataSelectionStrategies(dummyC50DataSelectionStrategies);
    // Can't really test strategies are set!
  }
}