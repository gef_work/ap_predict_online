/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/**
 * 
 */
package uk.ac.ox.cs.epsrc.business_manager.manager;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.AbstractProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.JobProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.AbstractProvenance;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.DoseResponseProvenance;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.FundamentalProvenance;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.IndividualProvenance;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.SummaryProvenance;
import uk.ac.ox.cs.epsrc.business_manager.dao.information.InformationDAO;
import uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager.INFORMATION_FORMAT;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.DoseResponseData;
import uk.ac.ox.cs.epsrc.business_manager.value.object.InformationVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Unit test the InformationManagerImpl object.
 *
 * @author geoff
 */
public class InformationManagerImplTest {

  private IMocksControl mocksControl;
  private InformationDAO mockInformationDAO;
  private InformationManager informationManager;

  private class TestProgress extends AbstractProgress {
    private static final long serialVersionUID = -7242731639302486398L;
    protected TestProgress(Builder builder) {
      super(builder);
    }
  }

  private class TestProvenance extends AbstractProvenance {
    private static final long serialVersionUID = 4275110012884043756L;
    protected TestProvenance(Builder builder) {
      super(builder);
    }
  }

  @Before
  public void setUp() {
    informationManager = new InformationManagerImpl();

    mocksControl = createStrictControl();

    mockInformationDAO = mocksControl.createMock(InformationDAO.class);

    ReflectionTestUtils.setField(informationManager, "informationDAO", mockInformationDAO);
  }

  @Test
  public void testPurge() {
    final long dummySimulationId = 1l;

    mockInformationDAO.purge(dummySimulationId);

    mocksControl.replay();

    informationManager.purge(dummySimulationId);

    mocksControl.verify();
  }

  @Test
  public void testRecordProgress() {
    final TestProgress mockTestProgress = mocksControl.createMock(TestProgress.class);
    final Long dummySimulationId = 1l;
    expect(mockTestProgress.getSimulationId()).andReturn(dummySimulationId);
    expect(((AbstractProgress) mockTestProgress).getTimestamp()).andReturn(null);
    expect(((AbstractProgress) mockTestProgress).getLevel()).andReturn(null);
    expect(((AbstractProgress) mockTestProgress).getText()).andReturn(null);
    expect(((AbstractProgress) mockTestProgress).getArgs()).andReturn(null);

    mocksControl.replay();

    informationManager.recordProgress(mockTestProgress);

    mocksControl.verify();
  }

  @Test
  public void testRecordProgressOverall() {
    /*
     * 1. Test creating overall progress
     */
    final OverallProgress mockOverallProgress = mocksControl.createMock(OverallProgress.class);

    final Date dummyTimestamp = new Date();
    final InformationLevel dummyLevel = InformationLevel.DEBUG;
    final String dummyText = "dummyText";
    final List<String> dummyArgs = new ArrayList<String>();
    final String dummyArg = "dummyArg";
    dummyArgs.add(dummyArg);
    final Long dummySimulationId = 1l;
    expect(((AbstractProgress) mockOverallProgress).getSimulationId()).andReturn(dummySimulationId);
    expect(((AbstractProgress) mockOverallProgress).getTimestamp()).andReturn(dummyTimestamp);
    expect(((AbstractProgress) mockOverallProgress).getLevel()).andReturn(dummyLevel);
    expect(((AbstractProgress) mockOverallProgress).getText()).andReturn(dummyText);
    expect(((AbstractProgress) mockOverallProgress).getArgs()).andReturn(dummyArgs);

    expect(mockOverallProgress.getSimulationId()).andReturn(dummySimulationId);
    boolean dummyHasJustBeenCreated = true;
    expect(mockOverallProgress.hasJustBeenCreated()).andReturn(dummyHasJustBeenCreated);
    final String dummyCompoundIdentifier = "dummyCompoundIdentifier";
    expect(mockOverallProgress.getCompoundIdentifier()).andReturn(dummyCompoundIdentifier);
    mockInformationDAO.createProgressAll(dummyCompoundIdentifier, dummySimulationId, dummyTimestamp,
                                         dummyLevel, dummyText, dummyArgs);
    boolean dummyIsTerminating = false;
    expect(mockOverallProgress.isTerminating()).andReturn(dummyIsTerminating);

    mocksControl.replay();

    informationManager.recordProgress(mockOverallProgress);

    mocksControl.verify();

    mocksControl.reset();

    /*
     * 2. Test appending overall progress and terminating.
     */
    expect(((AbstractProgress) mockOverallProgress).getSimulationId()).andReturn(dummySimulationId);
    expect(((AbstractProgress) mockOverallProgress).getTimestamp()).andReturn(dummyTimestamp);
    expect(((AbstractProgress) mockOverallProgress).getLevel()).andReturn(dummyLevel);
    expect(((AbstractProgress) mockOverallProgress).getText()).andReturn(dummyText);
    expect(((AbstractProgress) mockOverallProgress).getArgs()).andReturn(dummyArgs);

    expect(mockOverallProgress.getSimulationId()).andReturn(dummySimulationId);
    dummyHasJustBeenCreated = false;
    expect(mockOverallProgress.hasJustBeenCreated()).andReturn(dummyHasJustBeenCreated);
    mockInformationDAO.updateProgressAll(dummySimulationId, dummyTimestamp, dummyLevel, dummyText,
                                         dummyArgs);
    dummyIsTerminating = true;
    expect(mockOverallProgress.isTerminating()).andReturn(dummyIsTerminating);
    mockInformationDAO.updateProgressAll(dummySimulationId, dummyTimestamp,
                                         InformationLevel.TRACE,
                                         MessageKey.SYSTEM_TERMINATION_KEY.getBundleIdentifier(),
                                         new ArrayList<String>());

    mocksControl.replay();

    informationManager.recordProgress(mockOverallProgress);

    mocksControl.verify();
  }

  @Test
  public void testRecordProgressJob() {
    final JobProgress mockJobProgress = mocksControl.createMock(JobProgress.class);

    /*
     * 1. Test job progress just been created.
     */
    final Date dummyTimestamp = new Date();
    final InformationLevel dummyLevel = InformationLevel.DEBUG;
    final String dummyText = "dummyText";
    final String dummyArg = "dummyArg";
    final List<String> dummyArgs = new ArrayList<String>();
    final Long dummyJobId = 1L;
    final Long dummySimulationId = 2L;
    boolean dummyHasJustBeenCreated = true;

    final String dummyGroupName = "dummyGroupName";
    final Short dummyGroupLevel = 2;
    final Float dummyPacingFrequency = 1.0F;

    expect(mockJobProgress.getSimulationId()).andReturn(dummySimulationId);
    expect(((AbstractProgress) mockJobProgress).getTimestamp()).andReturn(dummyTimestamp);
    expect(((AbstractProgress) mockJobProgress).getLevel()).andReturn(dummyLevel);
    expect(((AbstractProgress) mockJobProgress).getText()).andReturn(dummyText);
    dummyArgs.add(dummyArg);
    expect(((AbstractProgress) mockJobProgress).getArgs()).andReturn(dummyArgs);
    expect(mockJobProgress.getJobId()).andReturn(dummyJobId);
    expect(mockJobProgress.hasJustBeenCreated()).andReturn(dummyHasJustBeenCreated);
    expect(mockJobProgress.getSimulationId()).andReturn(dummySimulationId);
    expect(mockJobProgress.getGroupName()).andReturn(dummyGroupName);
    expect(mockJobProgress.getGroupLevel()).andReturn(dummyGroupLevel);
    expect(mockJobProgress.getPacingFrequency()).andReturn(dummyPacingFrequency);

    mockInformationDAO.createProgressJob(dummySimulationId, dummyJobId, dummyGroupName,
                                         dummyGroupLevel, dummyPacingFrequency, dummyTimestamp,
                                         dummyLevel, dummyText, dummyArgs);

    mocksControl.replay();

    informationManager.recordProgress(mockJobProgress);

    mocksControl.verify();

    mocksControl.reset();

    /*
     * 2. Test post-creation job progress (but exception thrown)
     */
    expect(((AbstractProgress) mockJobProgress).getSimulationId()).andReturn(dummySimulationId);
    expect(((AbstractProgress) mockJobProgress).getTimestamp()).andReturn(dummyTimestamp);
    expect(((AbstractProgress) mockJobProgress).getLevel()).andReturn(dummyLevel);
    expect(((AbstractProgress) mockJobProgress).getText()).andReturn(dummyText);
    expect(((AbstractProgress) mockJobProgress).getArgs()).andReturn(dummyArgs);
    expect(mockJobProgress.getJobId()).andReturn(dummyJobId);
    dummyHasJustBeenCreated = false;
    expect(mockJobProgress.hasJustBeenCreated()).andReturn(dummyHasJustBeenCreated);

    mockInformationDAO.updateProgressJob(dummyJobId, dummyTimestamp, dummyLevel, dummyText, dummyArgs);

    expectLastCall().andThrow(new IllegalStateException());

    mocksControl.replay();

    informationManager.recordProgress(mockJobProgress);

    mocksControl.verify();

    mocksControl.reset();

    /*
     * 3. Test post-creation job progress.
     */
    expect(((AbstractProgress) mockJobProgress).getSimulationId()).andReturn(dummySimulationId);
    expect(((AbstractProgress) mockJobProgress).getTimestamp()).andReturn(dummyTimestamp);
    expect(((AbstractProgress) mockJobProgress).getLevel()).andReturn(dummyLevel);
    expect(((AbstractProgress) mockJobProgress).getText()).andReturn(dummyText);
    expect(((AbstractProgress) mockJobProgress).getArgs()).andReturn(dummyArgs);
    expect(mockJobProgress.getJobId()).andReturn(dummyJobId);
    expect(mockJobProgress.hasJustBeenCreated()).andReturn(dummyHasJustBeenCreated);

    mockInformationDAO.updateProgressJob(dummyJobId, dummyTimestamp, dummyLevel, dummyText, dummyArgs);

    mocksControl.replay();

    informationManager.recordProgress(mockJobProgress);

    mocksControl.verify();
  }

  @Test
  public void testRecordProvenance() {
    final TestProvenance mockTestProvenance = mocksControl.createMock(TestProvenance.class);
    final InformationLevel dummyLevel = null;
    expect(((AbstractProvenance) mockTestProvenance).getLevel()).andReturn(dummyLevel);
    final String dummyText = null;
    expect(((AbstractProvenance) mockTestProvenance).getText()).andReturn(dummyText);
    final List<String> dummyArgs = new ArrayList<String>();
    expect(((AbstractProvenance) mockTestProvenance).getArgs()).andReturn(dummyArgs);

    mocksControl.replay();

    informationManager.recordProvenance(mockTestProvenance);

    mocksControl.verify();
  }

  @Test
  public void testRecordProvenanceFundamental() {
    final FundamentalProvenance mockFundamentalProvenance = mocksControl.createMock(FundamentalProvenance.class);

    // 1. Null compound identifier and null simulation id
    final InformationLevel dummyLevel = InformationLevel.DEBUG;
    expect(((AbstractProvenance) mockFundamentalProvenance).getLevel()).andReturn(dummyLevel);
    final String dummyText = "dummyText";
    expect(((AbstractProvenance) mockFundamentalProvenance).getText()).andReturn(dummyText);
    final List<String> dummyArgs = new ArrayList<String>();
    expect(((AbstractProvenance) mockFundamentalProvenance).getArgs()).andReturn(dummyArgs);

    String dummyCompoundIdentifier = null;
    expect(mockFundamentalProvenance.getCompoundIdentifier()).andReturn(dummyCompoundIdentifier);
    Long dummySimulationId = null;
    expect(mockFundamentalProvenance.getSimulationId()).andReturn(dummySimulationId);

    mocksControl.replay();

    try {
      informationManager.recordProvenance(mockFundamentalProvenance);
      fail("Should not accept a null compound identifier / simulation id fundamental provenance");
    } catch (UnsupportedOperationException e) {}

    mocksControl.verify();

    mocksControl.reset();

    // 2. Compound identifier, but null simulation id
    expect(((AbstractProvenance) mockFundamentalProvenance).getLevel()).andReturn(dummyLevel);
    expect(((AbstractProvenance) mockFundamentalProvenance).getText()).andReturn(dummyText);
    expect(((AbstractProvenance) mockFundamentalProvenance).getArgs()).andReturn(dummyArgs);

    dummyCompoundIdentifier = "dummyCompoundIdentifier";
    expect(mockFundamentalProvenance.getCompoundIdentifier()).andReturn(dummyCompoundIdentifier);
    expect(mockFundamentalProvenance.getSimulationId()).andReturn(dummySimulationId);

    mocksControl.replay();

    try {
      informationManager.recordProvenance(mockFundamentalProvenance);
      fail("Should not accept a null simulation id fundamental provenance");
    } catch (UnsupportedOperationException e) {}

    mocksControl.verify();

    mocksControl.reset();

    // 3. Simulation id, but null compound identifier.
    expect(((AbstractProvenance) mockFundamentalProvenance).getLevel()).andReturn(dummyLevel);
    expect(((AbstractProvenance) mockFundamentalProvenance).getText()).andReturn(dummyText);
    expect(((AbstractProvenance) mockFundamentalProvenance).getArgs()).andReturn(dummyArgs);

    dummyCompoundIdentifier = null;
    expect(mockFundamentalProvenance.getCompoundIdentifier()).andReturn(dummyCompoundIdentifier);
    dummySimulationId = 1L;
    expect(mockFundamentalProvenance.getSimulationId()).andReturn(dummySimulationId);
    mockInformationDAO.writeFundamental(dummyCompoundIdentifier, dummySimulationId, dummyLevel,
                                        dummyText, dummyArgs); 

    mocksControl.replay();

    informationManager.recordProvenance(mockFundamentalProvenance);

    mocksControl.verify();

    mocksControl.reset();

    // 4. Simulation id and compound identifier.
    expect(((AbstractProvenance) mockFundamentalProvenance).getLevel()).andReturn(dummyLevel);
    expect(((AbstractProvenance) mockFundamentalProvenance).getText()).andReturn(dummyText);
    expect(((AbstractProvenance) mockFundamentalProvenance).getArgs()).andReturn(dummyArgs);

    dummyCompoundIdentifier = "dummyCompoundIdentifier";
    expect(mockFundamentalProvenance.getCompoundIdentifier()).andReturn(dummyCompoundIdentifier);
    dummySimulationId = 1L;
    expect(mockFundamentalProvenance.getSimulationId()).andReturn(dummySimulationId);
    mockInformationDAO.writeFundamental(dummyCompoundIdentifier, dummySimulationId, dummyLevel,
                                        dummyText, dummyArgs); 

    mocksControl.replay();

    informationManager.recordProvenance(mockFundamentalProvenance);

    mocksControl.verify();

    mocksControl.reset();
  }

  @Test
  public void testRecordProvenanceSummary() {
    final SummaryProvenance mockSummaryProvenance = mocksControl.createMock(SummaryProvenance.class);

    final long dummySimulationId = 1l;

    final InformationLevel dummyLevel = InformationLevel.DEBUG;
    expect(((AbstractProvenance) mockSummaryProvenance).getLevel()).andReturn(dummyLevel);
    final String dummyText = "dummyText";
    expect(((AbstractProvenance) mockSummaryProvenance).getText()).andReturn(dummyText);
    final List<String> dummyArgs = new ArrayList<String>();
    expect(((AbstractProvenance) mockSummaryProvenance).getArgs()).andReturn(dummyArgs);

    // 1. No data
    expect(mockSummaryProvenance.getSimulationId()).andReturn(dummySimulationId);
    String dummyAssayName = null;
    expect(mockSummaryProvenance.getAssayName()).andReturn(dummyAssayName);
    String dummyIonChannelName = null;
    expect(mockSummaryProvenance.getIonChannelName()).andReturn(dummyIonChannelName);
    final String dummySummaryDataId = null;
    expect(mockSummaryProvenance.getSummaryDataId()).andReturn(dummySummaryDataId);
    final List<BigDecimal> dummyPIC50s = new ArrayList<BigDecimal>();
    expect(mockSummaryProvenance.getpIC50s()).andReturn(dummyPIC50s);

    mockInformationDAO.updateSummary(dummySimulationId, dummySummaryDataId, dummyLevel, dummyText,
                                     dummyArgs);

    mocksControl.replay();

    informationManager.recordProvenance(mockSummaryProvenance);

    mocksControl.verify();

    mocksControl.reset();

    // 2. A pIC50 present
    expect(((AbstractProvenance) mockSummaryProvenance).getLevel()).andReturn(dummyLevel);
    expect(((AbstractProvenance) mockSummaryProvenance).getText()).andReturn(dummyText);
    expect(((AbstractProvenance) mockSummaryProvenance).getArgs()).andReturn(dummyArgs);
    expect(mockSummaryProvenance.getSimulationId()).andReturn(dummySimulationId);
    expect(mockSummaryProvenance.getAssayName()).andReturn(dummyAssayName);
    expect(mockSummaryProvenance.getIonChannelName()).andReturn(dummyIonChannelName);
    expect(mockSummaryProvenance.getSummaryDataId()).andReturn(dummySummaryDataId);
    final BigDecimal dummyPIC50 = BigDecimal.ONE;
    dummyPIC50s.add(dummyPIC50);
    expect(mockSummaryProvenance.getpIC50s()).andReturn(dummyPIC50s);
    final BigDecimal dummyHillCoefficient = null;
    expect(mockSummaryProvenance.getHillCoefficient()).andReturn(dummyHillCoefficient);

    mockInformationDAO.updateSummary(dummySimulationId, dummySummaryDataId, dummyLevel, dummyText,
                                     dummyArgs, dummyPIC50s, dummyHillCoefficient);

    mocksControl.replay();

    informationManager.recordProvenance(mockSummaryProvenance);

    mocksControl.verify();

    mocksControl.reset();

    // 3. Assay name and ion channel
    expect(((AbstractProvenance) mockSummaryProvenance).getLevel()).andReturn(dummyLevel);
    expect(((AbstractProvenance) mockSummaryProvenance).getText()).andReturn(dummyText);
    expect(((AbstractProvenance) mockSummaryProvenance).getArgs()).andReturn(dummyArgs);
    expect(mockSummaryProvenance.getSimulationId()).andReturn(dummySimulationId);
    dummyAssayName = "dummyAssayName";
    expect(mockSummaryProvenance.getAssayName()).andReturn(dummyAssayName);
    dummyIonChannelName = "dummyIonChannelName";
    expect(mockSummaryProvenance.getIonChannelName()).andReturn(dummyIonChannelName);
    expect(mockSummaryProvenance.getSummaryDataId()).andReturn(dummySummaryDataId);
    expect(mockSummaryProvenance.getpIC50s()).andReturn(dummyPIC50s);
    final Map<String, Object> dummyRawData = new HashMap<String, Object>();
    expect(mockSummaryProvenance.getRawData()).andReturn(dummyRawData);

    mockInformationDAO.writeSummary(dummySimulationId, dummySummaryDataId, dummyAssayName,
                                    dummyIonChannelName, dummyRawData, dummyLevel, dummyText,
                                    dummyArgs);

    mocksControl.replay();

    informationManager.recordProvenance(mockSummaryProvenance);

    mocksControl.verify();

    mocksControl.reset();
  }

  @Test
  public void testRecordProvenanceIndividual() {
    final IndividualProvenance mockIndividualProvenance = mocksControl.createMock(IndividualProvenance.class);

    final InformationLevel dummyLevel = InformationLevel.DEBUG;
    expect(((AbstractProvenance) mockIndividualProvenance).getLevel()).andReturn(dummyLevel);
    final String dummyText = "dummyText";
    expect(((AbstractProvenance) mockIndividualProvenance).getText()).andReturn(dummyText);
    final List<String> dummyArgs = new ArrayList<String>();
    expect(((AbstractProvenance) mockIndividualProvenance).getArgs()).andReturn(dummyArgs);

    final Long dummySimulationId = 4L;

    // 1. No data
    String dummyAssayName = null;
    expect(mockIndividualProvenance.getAssayName()).andReturn(dummyAssayName);
    String dummyIndividualDataId = null;
    expect(mockIndividualProvenance.getIndividualDataId()).andReturn(dummyIndividualDataId);
    String dummyIonChannelName = null;
    expect(mockIndividualProvenance.getIonChannelName()).andReturn(dummyIonChannelName);
    final Map<String, Object> dummyRawData = new HashMap<String, Object>();
    expect(mockIndividualProvenance.getRawData()).andReturn(dummyRawData);
    final BigDecimal dummyRawHill = null;
    expect(mockIndividualProvenance.getRawHill()).andReturn(dummyRawHill);
    final List<BigDecimal> dummyPIC50s = new ArrayList<BigDecimal>();
    expect(mockIndividualProvenance.getpIC50s()).andReturn(dummyPIC50s);
    String dummySummarydataId = null;
    expect(mockIndividualProvenance.getSummaryDataId()).andReturn(dummySummarydataId);
    DoseResponseData mockDoseResponseData = null;
    expect(mockIndividualProvenance.getDoseResponseData()).andReturn(mockDoseResponseData);
    expect(mockIndividualProvenance.getSimulationId()).andReturn(dummySimulationId);

    mockInformationDAO.updateIndividual(dummySimulationId, dummyIndividualDataId, dummyLevel,
                                        dummyText, dummyArgs);

    mocksControl.replay();

    informationManager.recordProvenance(mockIndividualProvenance);

    mocksControl.verify();

    mocksControl.reset();

    // 2. Dose-Response data, not recording.
    expect(((AbstractProvenance) mockIndividualProvenance).getLevel()).andReturn(dummyLevel);
    expect(((AbstractProvenance) mockIndividualProvenance).getText()).andReturn(dummyText);
    expect(((AbstractProvenance) mockIndividualProvenance).getArgs()).andReturn(dummyArgs);
    expect(mockIndividualProvenance.getAssayName()).andReturn(dummyAssayName);
    expect(mockIndividualProvenance.getIndividualDataId()).andReturn(dummyIndividualDataId);
    expect(mockIndividualProvenance.getIonChannelName()).andReturn(dummyIonChannelName);
    expect(mockIndividualProvenance.getRawData()).andReturn(dummyRawData);
    expect(mockIndividualProvenance.getRawHill()).andReturn(dummyRawHill);
    expect(mockIndividualProvenance.getpIC50s()).andReturn(dummyPIC50s);
    expect(mockIndividualProvenance.getSummaryDataId()).andReturn(dummySummarydataId);
    mockDoseResponseData = mocksControl.createMock(DoseResponseData.class);
    expect(mockIndividualProvenance.getDoseResponseData()).andReturn(mockDoseResponseData);
    expect(mockIndividualProvenance.getSimulationId()).andReturn(dummySimulationId);

    boolean dummyIsRecordingDoseResponses = false;
    expect(mockIndividualProvenance.isRecordingDoseResponses()).andReturn(dummyIsRecordingDoseResponses);
    final String dummyDoseResponseFittingStrategyIdentifier = null;
    expect(mockIndividualProvenance.getDrFittingStrategyIdentifier()).andReturn(dummyDoseResponseFittingStrategyIdentifier);

    mockInformationDAO.updateIndividual(dummySimulationId, dummyIndividualDataId, dummyLevel,
                                        dummyText, dummyArgs, mockDoseResponseData,
                                        dummyDoseResponseFittingStrategyIdentifier);

    mocksControl.replay();

    informationManager.recordProvenance(mockIndividualProvenance);

    mocksControl.verify();

    mocksControl.reset();

    // 3. Dose-Response data, recording.
    expect(((AbstractProvenance) mockIndividualProvenance).getLevel()).andReturn(dummyLevel);
    expect(((AbstractProvenance) mockIndividualProvenance).getText()).andReturn(dummyText);
    expect(((AbstractProvenance) mockIndividualProvenance).getArgs()).andReturn(dummyArgs);
    expect(mockIndividualProvenance.getAssayName()).andReturn(dummyAssayName);
    expect(mockIndividualProvenance.getIndividualDataId()).andReturn(dummyIndividualDataId);
    expect(mockIndividualProvenance.getIonChannelName()).andReturn(dummyIonChannelName);
    expect(mockIndividualProvenance.getRawData()).andReturn(dummyRawData);
    expect(mockIndividualProvenance.getRawHill()).andReturn(dummyRawHill);
    expect(mockIndividualProvenance.getpIC50s()).andReturn(dummyPIC50s);
    expect(mockIndividualProvenance.getSummaryDataId()).andReturn(dummySummarydataId);
    mockDoseResponseData = mocksControl.createMock(DoseResponseData.class);
    expect(mockIndividualProvenance.getDoseResponseData()).andReturn(mockDoseResponseData);
    expect(mockIndividualProvenance.getSimulationId()).andReturn(dummySimulationId);
    dummyIsRecordingDoseResponses = true;
    expect(mockIndividualProvenance.isRecordingDoseResponses()).andReturn(dummyIsRecordingDoseResponses);

    mockInformationDAO.updateIndividual(dummySimulationId, dummyIndividualDataId, dummyLevel,
                                        dummyText, dummyArgs, mockDoseResponseData);
    mocksControl.replay();

    informationManager.recordProvenance(mockIndividualProvenance);

    mocksControl.verify();

    mocksControl.reset();

    // 4. Some pIC50 data.

    expect(((AbstractProvenance) mockIndividualProvenance).getLevel()).andReturn(dummyLevel);
    expect(((AbstractProvenance) mockIndividualProvenance).getText()).andReturn(dummyText);
    expect(((AbstractProvenance) mockIndividualProvenance).getArgs()).andReturn(dummyArgs);
    expect(mockIndividualProvenance.getAssayName()).andReturn(dummyAssayName);
    expect(mockIndividualProvenance.getIndividualDataId()).andReturn(dummyIndividualDataId);
    expect(mockIndividualProvenance.getIonChannelName()).andReturn(dummyIonChannelName);
    expect(mockIndividualProvenance.getRawData()).andReturn(dummyRawData);
    expect(mockIndividualProvenance.getRawHill()).andReturn(dummyRawHill);
    final BigDecimal dummyPIC50 = null;
    dummyPIC50s.add(dummyPIC50);
    expect(mockIndividualProvenance.getpIC50s()).andReturn(dummyPIC50s);
    expect(mockIndividualProvenance.getSummaryDataId()).andReturn(dummySummarydataId);
    mockDoseResponseData = mocksControl.createMock(DoseResponseData.class);
    expect(mockIndividualProvenance.getDoseResponseData()).andReturn(mockDoseResponseData);
    expect(mockIndividualProvenance.getSimulationId()).andReturn(dummySimulationId);
    final BigDecimal dummyWorkflowHill = null;
    expect(mockIndividualProvenance.getWorkflowHill()).andReturn(dummyWorkflowHill);

    mockInformationDAO.updateIndividual(dummySimulationId, dummyIndividualDataId, dummyLevel,
                                        dummyText, dummyArgs, dummyPIC50, dummyWorkflowHill);

    mocksControl.replay();

    informationManager.recordProvenance(mockIndividualProvenance);

    mocksControl.verify();

    mocksControl.reset();

    // 5. Assay name and ion channel

    expect(((AbstractProvenance) mockIndividualProvenance).getLevel()).andReturn(dummyLevel);
    expect(((AbstractProvenance) mockIndividualProvenance).getText()).andReturn(dummyText);
    expect(((AbstractProvenance) mockIndividualProvenance).getArgs()).andReturn(dummyArgs);
    dummyAssayName = "dummyAssayName";
    expect(mockIndividualProvenance.getAssayName()).andReturn(dummyAssayName);
    expect(mockIndividualProvenance.getIndividualDataId()).andReturn(dummyIndividualDataId);
    dummyIonChannelName = "dummyIonChannelName";
    expect(mockIndividualProvenance.getIonChannelName()).andReturn(dummyIonChannelName);
    expect(mockIndividualProvenance.getRawData()).andReturn(dummyRawData);
    expect(mockIndividualProvenance.getRawHill()).andReturn(dummyRawHill);
    expect(mockIndividualProvenance.getpIC50s()).andReturn(dummyPIC50s);
    expect(mockIndividualProvenance.getSummaryDataId()).andReturn(dummySummarydataId);
    expect(mockIndividualProvenance.getDoseResponseData()).andReturn(mockDoseResponseData);
    expect(mockIndividualProvenance.getSimulationId()).andReturn(dummySimulationId);

    mockInformationDAO.writeIndividual(dummySimulationId, dummyIndividualDataId, dummyAssayName,
                                       dummyIonChannelName, dummyRawHill, dummyRawData, dummyLevel,
                                       dummyText, dummyArgs);

    mocksControl.replay();

    informationManager.recordProvenance(mockIndividualProvenance);

    mocksControl.verify();

    mocksControl.reset();

    // 6. Summary data id

    expect(((AbstractProvenance) mockIndividualProvenance).getLevel()).andReturn(dummyLevel);
    expect(((AbstractProvenance) mockIndividualProvenance).getText()).andReturn(dummyText);
    expect(((AbstractProvenance) mockIndividualProvenance).getArgs()).andReturn(dummyArgs);
    expect(mockIndividualProvenance.getAssayName()).andReturn(dummyAssayName);
    expect(mockIndividualProvenance.getIndividualDataId()).andReturn(dummyIndividualDataId);
    expect(mockIndividualProvenance.getIonChannelName()).andReturn(dummyIonChannelName);
    expect(mockIndividualProvenance.getRawData()).andReturn(dummyRawData);
    expect(mockIndividualProvenance.getRawHill()).andReturn(dummyRawHill);
    expect(mockIndividualProvenance.getpIC50s()).andReturn(dummyPIC50s);
    dummySummarydataId = "dummySummaryDataId";
    expect(mockIndividualProvenance.getSummaryDataId()).andReturn(dummySummarydataId);
    expect(mockIndividualProvenance.getDoseResponseData()).andReturn(mockDoseResponseData);
    expect(mockIndividualProvenance.getSimulationId()).andReturn(dummySimulationId);

    mockInformationDAO.writeIndividual(dummySimulationId, dummySummarydataId, dummyIndividualDataId,
                                       dummyRawHill, dummyRawData, dummyLevel, dummyText, dummyArgs);

    mocksControl.replay();

    informationManager.recordProvenance(mockIndividualProvenance);

    mocksControl.verify();
  }

  @Test
  public void testRecordProvenanceDoseResponse() {
    final long dummySimulationId = 1l;

    final DoseResponseProvenance mockDoseResponseProvenance = mocksControl.createMock(DoseResponseProvenance.class);

    final InformationLevel dummyLevel = InformationLevel.DEBUG;
    expect(((AbstractProvenance) mockDoseResponseProvenance).getLevel()).andReturn(dummyLevel);
    final String dummyText = "dummyText";
    expect(((AbstractProvenance) mockDoseResponseProvenance).getText()).andReturn(dummyText);
    final List<String> dummyArgs = new ArrayList<String>();
    expect(((AbstractProvenance) mockDoseResponseProvenance).getArgs()).andReturn(dummyArgs);

    // 1. No data
    expect(mockDoseResponseProvenance.getSimulationId()).andReturn(dummySimulationId);
    String dummyDoseInUM = null;
    expect(mockDoseResponseProvenance.getDose_in_uM()).andReturn(dummyDoseInUM);
    String dummyDoseResponseDataId = null;
    expect(mockDoseResponseProvenance.getDoseResponseDataId()).andReturn(dummyDoseResponseDataId);
    String dummyIndividualDataId = null;
    expect(mockDoseResponseProvenance.getIndividualDataId()).andReturn(dummyIndividualDataId);
    String dummyResponse = null;
    expect(mockDoseResponseProvenance.getResponse()).andReturn(dummyResponse);

    mockInformationDAO.updateDoseResponse(dummySimulationId, dummyDoseResponseDataId, dummyLevel,
                                          dummyText, dummyArgs);

    mocksControl.replay();

    informationManager.recordProvenance(mockDoseResponseProvenance);

    mocksControl.verify();

    mocksControl.reset();

    // 2. Dose in uM and a response
    expect(((AbstractProvenance) mockDoseResponseProvenance).getLevel()).andReturn(dummyLevel);
    expect(((AbstractProvenance) mockDoseResponseProvenance).getText()).andReturn(dummyText);
    expect(((AbstractProvenance) mockDoseResponseProvenance).getArgs()).andReturn(dummyArgs);
    expect(mockDoseResponseProvenance.getSimulationId()).andReturn(dummySimulationId);
    dummyDoseInUM = "dummyDoseInUM";
    expect(mockDoseResponseProvenance.getDose_in_uM()).andReturn(dummyDoseInUM);
    expect(mockDoseResponseProvenance.getDoseResponseDataId()).andReturn(dummyDoseResponseDataId);
    expect(mockDoseResponseProvenance.getIndividualDataId()).andReturn(dummyIndividualDataId);
    dummyResponse = "dummyResponse";
    expect(mockDoseResponseProvenance.getResponse()).andReturn(dummyResponse);

    mockInformationDAO.updateDoseResponse(dummySimulationId, dummyDoseResponseDataId, dummyLevel,
                                          dummyText, dummyArgs, dummyDoseInUM, dummyResponse);

    mocksControl.replay();

    informationManager.recordProvenance(mockDoseResponseProvenance);

    mocksControl.verify();

    mocksControl.reset();

    // 3. Individual data id.
    expect(((AbstractProvenance) mockDoseResponseProvenance).getLevel()).andReturn(dummyLevel);
    expect(((AbstractProvenance) mockDoseResponseProvenance).getText()).andReturn(dummyText);
    expect(((AbstractProvenance) mockDoseResponseProvenance).getArgs()).andReturn(dummyArgs);
    expect(mockDoseResponseProvenance.getSimulationId()).andReturn(dummySimulationId);
    expect(mockDoseResponseProvenance.getDose_in_uM()).andReturn(dummyDoseInUM);
    expect(mockDoseResponseProvenance.getDoseResponseDataId()).andReturn(dummyDoseResponseDataId);
    dummyIndividualDataId = "dummyIndividualDataId";
    expect(mockDoseResponseProvenance.getIndividualDataId()).andReturn(dummyIndividualDataId);
    expect(mockDoseResponseProvenance.getResponse()).andReturn(dummyResponse);
    final Map<String, Object> dummyRawData = null;
    expect(mockDoseResponseProvenance.getRawData()).andReturn(dummyRawData);

    mockInformationDAO.writeDoseResponse(dummySimulationId, dummyIndividualDataId,
                                         dummyDoseResponseDataId, dummyRawData, dummyLevel,
                                         dummyText, dummyArgs);

    mocksControl.replay();

    informationManager.recordProvenance(mockDoseResponseProvenance);

    mocksControl.verify();
  }

  @Test
  public void testRetrieveProgress() {
    final long dummySimulationId = 1l;

    final InformationVO mockInformationVO = mocksControl.createMock(InformationVO.class);
    expect(mockInformationDAO.retrieveProgress(dummySimulationId, INFORMATION_FORMAT.JSON))
          .andReturn(mockInformationVO);

    mocksControl.replay();

    final InformationVO retrievedInformationVO = informationManager.retrieveProgress(dummySimulationId);

    mocksControl.verify();
    assertSame(mockInformationVO, retrievedInformationVO);
  }

  @Test
  public void testRetrieveProvenance() {
    final long dummySimulationId = 1l;

    final InformationVO mockInformationVO = mocksControl.createMock(InformationVO.class);
    expect(mockInformationDAO.retrieveProvenance(dummySimulationId, INFORMATION_FORMAT.JSON))
          .andReturn(mockInformationVO);

    mocksControl.replay();

    final InformationVO retrievedInformationVO = informationManager.retrieveProvenance(dummySimulationId);

    mocksControl.verify();
    assertSame(mockInformationVO, retrievedInformationVO);
  }
}