/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.business_manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.ws.business_manager.HandlerUtil;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ObjectFactory;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProcessSimulationsRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.SimulationDetail;

/**
 * Unit test the request handler utility class.
 *
 * @author geoff
 */
public class HandlerUtilTest {

  private static final ObjectFactory objectFactory = new ObjectFactory();

  /**
   * Test the removal of duplicates and trimming.
   */
  @Test
  public void testRemoveDuplicatesAndTrim() {
    try {
      HandlerUtil.removeDuplicatesNullAndTrim(null);
      fail("Should not accept null incoming parameter removing duplicates, null and trim.");
    } catch (AssertionError e) {}

    // test empty array isn't a problem
    final List<String> incoming = new ArrayList<String>();
    HandlerUtil.removeDuplicatesNullAndTrim(incoming);

    incoming.add(null);
    Set<String> processed = HandlerUtil.removeDuplicatesNullAndTrim(incoming);

    assertNotNull(processed);
    assertSame(0, processed.size());

    incoming.clear();
    final String value1 = "value_1";
    incoming.add(value1);

    processed = HandlerUtil.removeDuplicatesNullAndTrim(incoming);
    assertSame(1, processed.size());
    assertTrue(processed.contains(value1));

    final String value2 = "value_2";
    incoming.add(value2);
    processed = HandlerUtil.removeDuplicatesNullAndTrim(incoming);

    assertSame(2, processed.size());
    assertTrue(processed.contains(value2));

    incoming.add(value1);
    processed = HandlerUtil.removeDuplicatesNullAndTrim(incoming);
    assertSame(2, processed.size());

    incoming.add(value2.concat(" "));
    processed = HandlerUtil.removeDuplicatesNullAndTrim(incoming);
    assertSame(2, processed.size());
  }

  /**
   * Test the retrieval of raw ordered compound names on process simulations request.
   */
  @Test
  public void testRetrieveOrderedRawCompoundIdentifiersWithProcessSimulationsRequest() {
    try {
      HandlerUtil.retrieveOrderedRawCompoundIdentifiers(null);
      fail("Should not accept null incoming parameter retrieving raw compound names.");
    } catch (AssertionError e) {}

    final ProcessSimulationsRequest processSimulationsRequest = objectFactory.createProcessSimulationsRequest();
    List<String> rawCompoundIdentifiers = HandlerUtil.retrieveOrderedRawCompoundIdentifiers(processSimulationsRequest);

    assertNotNull(rawCompoundIdentifiers);
    assertSame(0, rawCompoundIdentifiers.size());

    // nulls aren't influencing
    final SimulationDetail simulationDetail1 = objectFactory.createSimulationDetail();
    simulationDetail1.setCompoundIdentifier(null);
    processSimulationsRequest.getSimulationDetail().add(simulationDetail1);
    rawCompoundIdentifiers = HandlerUtil.retrieveOrderedRawCompoundIdentifiers(processSimulationsRequest);
    assertSame(1, rawCompoundIdentifiers.size());
    assertEquals(null, rawCompoundIdentifiers.get(0));

    final SimulationDetail simulationDetail2 = objectFactory.createSimulationDetail();
    simulationDetail2.setCompoundIdentifier(null);
    processSimulationsRequest.getSimulationDetail().add(simulationDetail2);
    rawCompoundIdentifiers = HandlerUtil.retrieveOrderedRawCompoundIdentifiers(processSimulationsRequest);
    assertSame(2, rawCompoundIdentifiers.size());
    assertEquals(null, rawCompoundIdentifiers.get(0));
    assertEquals(null, rawCompoundIdentifiers.get(1));

    final SimulationDetail simulationDetail3 = objectFactory.createSimulationDetail();
    final String compoundIdentifier1 = "CD1";
    simulationDetail3.setCompoundIdentifier(compoundIdentifier1);
    processSimulationsRequest.getSimulationDetail().add(simulationDetail3);
    rawCompoundIdentifiers = HandlerUtil.retrieveOrderedRawCompoundIdentifiers(processSimulationsRequest);
    assertSame(3, rawCompoundIdentifiers.size());
    assertEquals(null, rawCompoundIdentifiers.get(0));
    assertEquals(null, rawCompoundIdentifiers.get(1));
    assertEquals(compoundIdentifier1, rawCompoundIdentifiers.get(2));
  }

  /**
   * Test the passing of an unrecognised object to retrieve ordered compound names fails.
   */
  @Test(expected=UnsupportedOperationException.class)
  public void testRetrieveOrderedRawCompoundIdentifiersWithUnrecognisedClass() {
    HandlerUtil.retrieveOrderedRawCompoundIdentifiers(new Integer("2"));
  }

  /**
   * Test the tidying of incoming.
   */
  @Test
  public void testTidyIncoming() {
    // test null incoming not acceptable
    try {
      HandlerUtil.tidyIncoming(null);
      fail("Should not accept null incoming parameter tidying incoming values.");
    } catch (AssertionError e) {}

    // check handling of the known classes. 
    final ProcessSimulationsRequest processSimulationsRequest = objectFactory.createProcessSimulationsRequest();
    HandlerUtil.tidyIncoming(processSimulationsRequest);

    // check that the tidying takes place.
    final SimulationDetail simulationDetail1 = objectFactory.createSimulationDetail();
    simulationDetail1.setCompoundIdentifier(null);
    processSimulationsRequest.getSimulationDetail().add(simulationDetail1);
    final SimulationDetail simulationDetail2 = objectFactory.createSimulationDetail();
    final String compoundIdentifier1 = "CD1";
    simulationDetail2.setCompoundIdentifier(compoundIdentifier1);
    final SimulationDetail simulationDetail3 = objectFactory.createSimulationDetail();
    simulationDetail3.setCompoundIdentifier(compoundIdentifier1);
    final SimulationDetail simulationDetail4 = objectFactory.createSimulationDetail();
    final String compoundIdentifier2 = "CD2";
    simulationDetail4.setCompoundIdentifier(compoundIdentifier2);

    processSimulationsRequest.getSimulationDetail().add(simulationDetail1);
    processSimulationsRequest.getSimulationDetail().add(simulationDetail2);
    processSimulationsRequest.getSimulationDetail().add(simulationDetail3);
    processSimulationsRequest.getSimulationDetail().add(simulationDetail4);

    final Set<String> tidied = HandlerUtil.tidyIncoming(processSimulationsRequest);
    assertSame(2, tidied.size());
    assertTrue(tidied.contains(compoundIdentifier1));
    assertTrue(tidied.contains(compoundIdentifier2));
  }

  /**
   * Test the passing of an unrecognised object to tidy incoming fails.
   */
  @Test(expected=UnsupportedOperationException.class)
  public void testTidyIncomingWithUnrecognisedClass() {
    HandlerUtil.tidyIncoming(new Integer("2"));
  }
}