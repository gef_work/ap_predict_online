/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual;

import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual.C50EqualityModifier;
import uk.ac.ox.cs.epsrc.business_manager.business.util.DataUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;

/**
 * Unit test the C50 equality modifier testing.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(DataUtil.class)
public class C50EqualityModifierTest {

  private C50EqualityModifier c50EqualityModifier;
  private final String dummyDescription = "dummyDescription";
  private final boolean dummyDefaultActive = true;
  private final int dummyDefaultInvocationOrder = 0;

  @Before
  public void setUp() {
    c50EqualityModifier = new C50EqualityModifier(dummyDescription, dummyDefaultActive,
                                                  dummyDefaultInvocationOrder);
  }

  @Test
  public void testConstructor() {
    assertSame(dummyDescription, c50EqualityModifier.getDescription());
    assertSame(dummyDefaultActive, c50EqualityModifier.getDefaultActive());
    assertSame(dummyDefaultInvocationOrder, c50EqualityModifier.getOrder());
  }

  @Test
  public void testPC50EvaluationFailsOnNullC50Data() {
    final IMocksControl mocksControl = createControl();
    final ICData mockC50Data = mocksControl.createMock(ICData.class);
    final String dummyRecordIdentifier = "dummyRecordIdentifier";

    mockStatic(DataUtil.class);
    final BigDecimal dummyPC50 = BigDecimal.ONE;
    expect(DataUtil.pc50ByC50EqualityModifier(mockC50Data, dummyRecordIdentifier))
          .andReturn(dummyPC50);
    replayAll();
    mocksControl.replay();
    final BigDecimal evaluatedPC50 = c50EqualityModifier.evaluatePC50(mockC50Data,
                                                                      dummyRecordIdentifier);
    mocksControl.verify();
    verifyAll();

    assertTrue(dummyPC50.compareTo(evaluatedPC50) == 0);
  }

  @Test
  public void testEqualityModifierUse() {
    assertTrue(c50EqualityModifier.usesEqualityModifier());
  }
}