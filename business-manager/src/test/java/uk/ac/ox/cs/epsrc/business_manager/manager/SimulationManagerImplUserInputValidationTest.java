/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.manager;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.powermock.api.easymock.PowerMock.createMockAndExpectNew;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.epsrc.business_manager.config.ConfigurationActionPotential;
import uk.ac.ox.cs.epsrc.business_manager.config.ConfigurationDoseResponseFitting;
import uk.ac.ox.cs.epsrc.business_manager.value.object.SimulationRequestVO;

/**
 * User input validation testing.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { SimulationManagerImpl.class } )
public class SimulationManagerImplUserInputValidationTest {

  private BusinessManagerJMSTemplate mockJMSTemplate;
  private Configuration mockConfiguration;
  private ConfigurationActionPotential mockConfigurationActionPotential;
  private ConfigurationDoseResponseFitting mockConfigurationDoseResponseFitting;
  private IMocksControl mocksControl;
  private SimulationManager simulationManager;

  private String dummyCompoundIdentifier;
  private Boolean dummyForceReRun;
  private Boolean dummyReset;
  private Boolean dummyAssayGrouping;
  private Boolean dummyValueInheriting;
  private Boolean dummyBetweenGroups;
  private Boolean dummyWithinGroups;
  private String dummyPC50EvaluationStrategies;
  private String dummyDoseResponseFittingStrategy;
  private Boolean dummyDoseResponseFittingRounding;
  private Boolean dummyDoseResponseFittingHillMinMax;
  private Float dummyDoseResponseFittingHillMax;
  private Float dummyDoseResponseFittingHillMin;
  private Short dummyCellModelIdentifier;
  private Boolean dummyInputDataOnly;
  private BigDecimal dummyPacingMaxTime;
  private String dummyUserId;

  @Before
  public void setUp() {
    simulationManager = new SimulationManagerImpl();

    mocksControl = createStrictControl();
    mockConfiguration = mocksControl.createMock(Configuration.class);
    mockConfigurationActionPotential = mocksControl.createMock(ConfigurationActionPotential.class);
    mockConfigurationDoseResponseFitting = mocksControl.createMock(ConfigurationDoseResponseFitting.class);
    mockJMSTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);

    ReflectionTestUtils.setField(simulationManager, BusinessIdentifiers.COMPONENT_CONFIGURATION,
                                 mockConfiguration);
    ReflectionTestUtils.setField(simulationManager, BusinessIdentifiers.COMPONENT_CONFIGURATION_ACTIONPOTENTIAL,
                                 mockConfigurationActionPotential);
    ReflectionTestUtils.setField(simulationManager, BusinessIdentifiers.COMPONENT_CONFIGURATION_DOSERESPONSE_FITTING,
                                 mockConfigurationDoseResponseFitting);
    ReflectionTestUtils.setField(simulationManager, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockJMSTemplate);
  }

  private void setStandardValues() {
    dummyCompoundIdentifier = "dummyCompoundIdentifier";
    dummyForceReRun = true;
    dummyReset = false;
    dummyAssayGrouping = true;
    dummyValueInheriting = false;
    dummyBetweenGroups = true;
    dummyWithinGroups = false;
    dummyPC50EvaluationStrategies = "dummyPC50EvaluationStrategies";
    dummyDoseResponseFittingStrategy = "dummyDoseResponseFittingStrategy";
    dummyDoseResponseFittingRounding = true;
    dummyDoseResponseFittingHillMinMax = false;
    dummyDoseResponseFittingHillMax = 2.2F;
    dummyDoseResponseFittingHillMin = 3.3F;
    dummyCellModelIdentifier = SimulationManagerImpl.anticipatedCellMLModelIdentifierMinValue;
    dummyInputDataOnly = true;
    dummyPacingMaxTime = BigDecimal.TEN;
    dummyUserId = "dummyUserId";
  }

  @Test
  public void testNullValues() throws Exception {
    final boolean dummyDefaultForceReRun = false;
    expect(mockConfiguration.isDefaultForceReRun())
          .andReturn(dummyDefaultForceReRun);
    final boolean dummyDefaultAssayGrouping = false;
    expect(mockConfiguration.isDefaultAssayGrouping())
          .andReturn(dummyDefaultAssayGrouping);
    final boolean dummyDefaultValueInheriting = true;
    expect(mockConfiguration.isDefaultValueInheriting())
          .andReturn(dummyDefaultValueInheriting);
    final boolean dummyDefaultWithinGroups = false;
    expect(mockConfiguration.isDefaultWithinGroups())
          .andReturn(dummyDefaultWithinGroups);
    final boolean dummyDefaultBetweenGroups = true;
    expect(mockConfiguration.isDefaultBetweenGroups())
          .andReturn(dummyDefaultBetweenGroups);
    final String dummyDefaultOrderedStrategiesAsCSV = "dummyDefaultOrderedStrategiesAsCSV";
    expect(mockConfiguration.retrieveOrderedStrategiesAsCSV())
          .andReturn(dummyDefaultOrderedStrategiesAsCSV);

    final String dummyDefaultDoseResponseFittingStrategy = "dummyDefaultDoseResponseFittingStrategy";
    expect(mockConfigurationDoseResponseFitting.getDefaultDoseResponseFittingStrategy())
          .andReturn(dummyDefaultDoseResponseFittingStrategy);
    final boolean dummyDefaultDoseResponseFittingRounded = false;
    expect(mockConfigurationDoseResponseFitting.isDefaultDoseResponseFittingRounded())
          .andReturn(dummyDefaultDoseResponseFittingRounded);
    final short dummyDefaultCellModelIdentifier = 3;
    expect(mockConfigurationActionPotential.getDefaultModelIdentifier())
          .andReturn(dummyDefaultCellModelIdentifier);

    createMockAndExpectNew(SimulationRequestVO.class, dummyCompoundIdentifier,
                           dummyDefaultForceReRun,
                           Configuration.DEFAULT_FORCE_RESET,
                           dummyDefaultAssayGrouping,
                           dummyDefaultValueInheriting,
                           dummyDefaultWithinGroups, dummyDefaultBetweenGroups,
                           dummyDefaultOrderedStrategiesAsCSV,
                           dummyDefaultDoseResponseFittingStrategy,
                           dummyDefaultDoseResponseFittingRounded, false, null,
                           null, dummyDefaultCellModelIdentifier,
                           dummyPacingMaxTime, false, dummyUserId);

    mocksControl.replay();
    replayAll();

    simulationManager.userInputValidation(dummyCompoundIdentifier,
                                          dummyForceReRun, dummyReset,
                                          dummyAssayGrouping,
                                          dummyValueInheriting,
                                          dummyBetweenGroups, dummyWithinGroups,
                                          dummyPC50EvaluationStrategies,
                                          dummyDoseResponseFittingStrategy,
                                          dummyDoseResponseFittingRounding,
                                          dummyDoseResponseFittingHillMinMax,
                                          dummyDoseResponseFittingHillMax,
                                          dummyDoseResponseFittingHillMin,
                                          dummyCellModelIdentifier,
                                          dummyPacingMaxTime,
                                          dummyInputDataOnly, dummyUserId);
    // Not verifying SimulationRequestVO output of validation bec. it's a mock object!

    mocksControl.verify();
    verifyAll();
  }

  @Test
  public void testNoDoseResponseMinMax() throws Exception {
    setStandardValues();

    createMockAndExpectNew(SimulationRequestVO.class, dummyCompoundIdentifier,
                           dummyForceReRun, dummyReset, dummyAssayGrouping,
                           dummyValueInheriting, dummyWithinGroups,
                           dummyBetweenGroups, dummyPC50EvaluationStrategies,
                           dummyDoseResponseFittingStrategy,
                           dummyDoseResponseFittingRounding, false, null, null,
                           dummyCellModelIdentifier, dummyPacingMaxTime,
                           dummyInputDataOnly, dummyUserId);

    mocksControl.replay();
    replayAll();

    simulationManager.userInputValidation(dummyCompoundIdentifier,
                                          dummyForceReRun, dummyReset,
                                          dummyAssayGrouping,
                                          dummyValueInheriting,
                                          dummyBetweenGroups, dummyWithinGroups,
                                          dummyPC50EvaluationStrategies,
                                          dummyDoseResponseFittingStrategy,
                                          dummyDoseResponseFittingRounding,
                                          dummyDoseResponseFittingHillMinMax,
                                          dummyDoseResponseFittingHillMax,
                                          dummyDoseResponseFittingHillMin,
                                          dummyCellModelIdentifier,
                                          dummyPacingMaxTime,
                                          dummyInputDataOnly, dummyUserId);
    // Not verifying SimulationRequestVO output of validation bec. it's a mock object!

    mocksControl.verify();
    verifyAll();
  }

  @Test
  public void testWithDoseResponseMinMax() throws Exception {
    setStandardValues();
    dummyForceReRun = false;
    dummyReset = true;
    dummyAssayGrouping = false;
    dummyValueInheriting = true;
    dummyBetweenGroups = false;
    dummyWithinGroups = true;
    dummyDoseResponseFittingRounding = false;
    dummyDoseResponseFittingHillMinMax = true;
    dummyDoseResponseFittingHillMax = null;
    dummyDoseResponseFittingHillMin = null;
    dummyInputDataOnly = false;
    dummyUserId = null;

    // Emulate not hill fitting strategy
    boolean dummyIsNotHillFittingStrategy = false;
    expect(mockConfigurationDoseResponseFitting.isHillFittingStrategy(dummyDoseResponseFittingStrategy))
          .andReturn(dummyIsNotHillFittingStrategy);

    createMockAndExpectNew(SimulationRequestVO.class, dummyCompoundIdentifier,
                           dummyForceReRun, dummyReset, dummyAssayGrouping,
                           dummyValueInheriting, dummyWithinGroups,
                           dummyBetweenGroups, dummyPC50EvaluationStrategies,
                           dummyDoseResponseFittingStrategy,
                           dummyDoseResponseFittingRounding,
                           dummyDoseResponseFittingHillMinMax, null, null,
                           dummyCellModelIdentifier, dummyPacingMaxTime,
                           dummyInputDataOnly, dummyUserId);

    mocksControl.replay();
    replayAll();

    simulationManager.userInputValidation(dummyCompoundIdentifier,
                                          dummyForceReRun, dummyReset,
                                          dummyAssayGrouping,
                                          dummyValueInheriting,
                                          dummyBetweenGroups, dummyWithinGroups,
                                          dummyPC50EvaluationStrategies,
                                          dummyDoseResponseFittingStrategy,
                                          dummyDoseResponseFittingRounding,
                                          dummyDoseResponseFittingHillMinMax,
                                          dummyDoseResponseFittingHillMax,
                                          dummyDoseResponseFittingHillMin,
                                          dummyCellModelIdentifier,
                                          dummyPacingMaxTime,
                                          dummyInputDataOnly, dummyUserId);
    // Not verifying SimulationRequestVO output of validation bec. it's a mock object!

    mocksControl.verify();
    verifyAll();

    mocksControl.reset();
    resetAll();

    // Emulate hill fitting strategy using default Hill Min and Max
    boolean dummyIsHillFittingStrategy = true;
    expect(mockConfigurationDoseResponseFitting.isHillFittingStrategy(dummyDoseResponseFittingStrategy))
          .andReturn(dummyIsHillFittingStrategy);
    Float dummyDefaultDoseResponseFittingHillMax = 1.1F;
    expect(mockConfigurationDoseResponseFitting.getDefaultDoseResponseFittingHillMax())
          .andReturn(dummyDefaultDoseResponseFittingHillMax);
    Float dummyDefaultDoseResponseFittingHillMin = 2.2F;
    expect(mockConfigurationDoseResponseFitting.getDefaultDoseResponseFittingHillMin())
          .andReturn(dummyDefaultDoseResponseFittingHillMin);

    createMockAndExpectNew(SimulationRequestVO.class, dummyCompoundIdentifier,
                           dummyForceReRun, dummyReset, dummyAssayGrouping,
                           dummyValueInheriting, dummyWithinGroups,
                           dummyBetweenGroups, dummyPC50EvaluationStrategies,
                           dummyDoseResponseFittingStrategy,
                           dummyDoseResponseFittingRounding,
                           dummyDoseResponseFittingHillMinMax,
                           dummyDefaultDoseResponseFittingHillMax,
                           dummyDefaultDoseResponseFittingHillMin,
                           dummyCellModelIdentifier, dummyPacingMaxTime,
                           dummyInputDataOnly, dummyUserId);

    mocksControl.replay();
    replayAll();

    simulationManager.userInputValidation(dummyCompoundIdentifier,
                                          dummyForceReRun, dummyReset,
                                          dummyAssayGrouping,
                                          dummyValueInheriting,
                                          dummyBetweenGroups, dummyWithinGroups,
                                          dummyPC50EvaluationStrategies,
                                          dummyDoseResponseFittingStrategy,
                                          dummyDoseResponseFittingRounding,
                                          dummyDoseResponseFittingHillMinMax,
                                          dummyDoseResponseFittingHillMax,
                                          dummyDoseResponseFittingHillMin,
                                          dummyCellModelIdentifier,
                                          dummyPacingMaxTime,
                                          dummyInputDataOnly, dummyUserId);
    // Not verifying SimulationRequestVO output of validation bec. it's a mock object!

    mocksControl.verify();
    verifyAll();

    mocksControl.reset();
    resetAll();

    dummyDoseResponseFittingHillMax = 5.5F;
    dummyDoseResponseFittingHillMin = 6.6F;

    // Emulate hill fitting strategy using assigned Hill Min and Max
    expect(mockConfigurationDoseResponseFitting.isHillFittingStrategy(dummyDoseResponseFittingStrategy))
          .andReturn(dummyIsHillFittingStrategy);

    createMockAndExpectNew(SimulationRequestVO.class, dummyCompoundIdentifier,
                           dummyForceReRun, dummyReset, dummyAssayGrouping,
                           dummyValueInheriting, dummyWithinGroups,
                           dummyBetweenGroups, dummyPC50EvaluationStrategies,
                           dummyDoseResponseFittingStrategy,
                           dummyDoseResponseFittingRounding,
                           dummyDoseResponseFittingHillMinMax,
                           dummyDoseResponseFittingHillMax,
                           dummyDoseResponseFittingHillMin,
                           dummyCellModelIdentifier, dummyPacingMaxTime,
                           dummyInputDataOnly, dummyUserId);

    mocksControl.replay();
    replayAll();

    simulationManager.userInputValidation(dummyCompoundIdentifier,
                                          dummyForceReRun, dummyReset,
                                          dummyAssayGrouping,
                                          dummyValueInheriting,
                                          dummyBetweenGroups,
                                          dummyWithinGroups,
                                          dummyPC50EvaluationStrategies,
                                          dummyDoseResponseFittingStrategy,
                                          dummyDoseResponseFittingRounding,
                                          dummyDoseResponseFittingHillMinMax,
                                          dummyDoseResponseFittingHillMax,
                                          dummyDoseResponseFittingHillMin,
                                          dummyCellModelIdentifier,
                                          dummyPacingMaxTime,
                                          dummyInputDataOnly, dummyUserId);
    // Not verifying SimulationRequestVO output of validation bec. it's a mock object!

    mocksControl.verify();
    verifyAll();
  }

  @Test(expected=IllegalArgumentException.class)
  public void testDodgyCellIdentifier1() throws Exception {
    setStandardValues();
    dummyCellModelIdentifier = 0;

    mocksControl.replay();

    simulationManager.userInputValidation(dummyCompoundIdentifier,
                                          dummyForceReRun, dummyReset,
                                          dummyAssayGrouping,
                                          dummyValueInheriting,
                                          dummyBetweenGroups, dummyWithinGroups,
                                          dummyPC50EvaluationStrategies,
                                          dummyDoseResponseFittingStrategy,
                                          dummyDoseResponseFittingRounding,
                                          dummyDoseResponseFittingHillMinMax,
                                          dummyDoseResponseFittingHillMax,
                                          dummyDoseResponseFittingHillMin,
                                          dummyCellModelIdentifier,
                                          dummyPacingMaxTime,
                                          dummyInputDataOnly, dummyUserId);

    mocksControl.verify();
  }

  @Test(expected=IllegalArgumentException.class)
  public void testDodgyCellIdentifier2() throws Exception {
    setStandardValues();
    dummyCellModelIdentifier = -1;

    mocksControl.replay();

    simulationManager.userInputValidation(dummyCompoundIdentifier,
                                          dummyForceReRun, dummyReset,
                                          dummyAssayGrouping,
                                          dummyValueInheriting,
                                          dummyBetweenGroups, dummyWithinGroups,
                                          dummyPC50EvaluationStrategies,
                                          dummyDoseResponseFittingStrategy,
                                          dummyDoseResponseFittingRounding,
                                          dummyDoseResponseFittingHillMinMax,
                                          dummyDoseResponseFittingHillMax,
                                          dummyDoseResponseFittingHillMin,
                                          dummyCellModelIdentifier,
                                          dummyPacingMaxTime,
                                          dummyInputDataOnly, dummyUserId);

    mocksControl.verify();
  }

  @Test(expected=IllegalArgumentException.class)
  public void testDodgyPacingMaxTime() throws Exception {
    setStandardValues();
    dummyPacingMaxTime = BigDecimal.ZERO;

    mocksControl.replay();

    simulationManager.userInputValidation(dummyCompoundIdentifier,
                                          dummyForceReRun, dummyReset,
                                          dummyAssayGrouping,
                                          dummyValueInheriting,
                                          dummyBetweenGroups, dummyWithinGroups,
                                          dummyPC50EvaluationStrategies,
                                          dummyDoseResponseFittingStrategy,
                                          dummyDoseResponseFittingRounding,
                                          dummyDoseResponseFittingHillMinMax,
                                          dummyDoseResponseFittingHillMax,
                                          dummyDoseResponseFittingHillMin,
                                          dummyCellModelIdentifier,
                                          dummyPacingMaxTime, dummyInputDataOnly,
                                          dummyUserId);

    mocksControl.verify();
  }
}