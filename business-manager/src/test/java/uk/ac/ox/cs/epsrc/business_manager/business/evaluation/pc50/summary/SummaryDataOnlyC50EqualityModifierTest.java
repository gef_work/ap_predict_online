/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.summary;

import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.summary.SummaryDataOnlyC50EqualityModifier;
import uk.ac.ox.cs.epsrc.business_manager.business.util.DataUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;

/**
 * Unit test the C50 equality modifier and no individual data summary PC50 evaluation strategy.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(DataUtil.class)
public class SummaryDataOnlyC50EqualityModifierTest {

  private ICData mockSummaryC50Data;
  private SummaryDataOnlyC50EqualityModifier strategy;
  private IMocksControl mocksControl;
  private String dummyDescription = "dummyDescription";
  private boolean dummyDefaultActive = true;
  private int dummyDefaultInvocationOrder = 3;
  private String dummyRecordIdentifier = "dummyRecordIdentifier";

  @Before
  public void setUp() {
    mocksControl = createControl();
    mockSummaryC50Data = mocksControl.createMock(ICData.class);
    strategy = new SummaryDataOnlyC50EqualityModifier(dummyDescription, dummyDefaultActive,
                                                      dummyDefaultInvocationOrder);
  }

  @Test
  public void testPC50Retrieval() {
    mockStatic(DataUtil.class);
    expect(DataUtil.pc50ByC50EqualityModifier(mockSummaryC50Data, dummyRecordIdentifier))
          .andReturn(null);
    replayAll();
    BigDecimal evaluatedPC50 = strategy.retrievePC50ByStrategy(mockSummaryC50Data,
                                                               dummyRecordIdentifier);
    verifyAll();
    assertNull(evaluatedPC50);
  }

  @Test
  public void testEqualityModifierUse() {
    assertTrue(strategy.usesEqualityModifier());
  }
}