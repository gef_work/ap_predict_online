/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.QualityControlFailureVO;

/**
 * Unit test data marked as inactive strategy.
 *
 * @author geoff
 */
public class DataMarkedAsInactiveTest {

  private static final boolean dummyDefaultActive = true;
  private static final int dummyDefaultInvocationOrder = 0;
  private static final String dummyDescription = "dummyDescription";

  private DataMarkedAsInactive strategy;
  private IMocksControl mocksControl;
  private IndividualDataRecord mockIndividualData;

  @Before
  public  void setUp() {
    strategy = new DataMarkedAsInactive(dummyDescription, dummyDefaultActive,
                                        dummyDefaultInvocationOrder);
    mocksControl = createStrictControl();
    mockIndividualData = mocksControl.createMock(IndividualDataRecord.class);
  }

  @Test
  public void testModellingAsActive() throws InvalidValueException {
    final QualityControlFailureVO dummyQCFailure = null;
    expect(mockIndividualData.isMarkedInactive()).andReturn(dummyQCFailure);

    mocksControl.replay();

    final String reason = strategy.modelAsInactive(null, mockIndividualData, null, null);

    mocksControl.verify();

    assertNull(reason);
  }

  @Test
  public void testModellingAsInactive() throws InvalidValueException {
    final QualityControlFailureVO mockQCFailure = mocksControl.createMock(QualityControlFailureVO.class);
    expect(mockIndividualData.isMarkedInactive()).andReturn(mockQCFailure);
    final String dummyReason = "dummyReason";
    expect(mockQCFailure.getReason()).andReturn(dummyReason);

    mocksControl.replay();

    final String reason = strategy.modelAsInactive(null, mockIndividualData, null, null);

    mocksControl.verify();

    assertEquals(dummyReason, reason);
  }

  @Test
  public void testUsesEqualityModifier() {
    assertFalse(strategy.usesEqualityModifier());
  }
}