/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.config;

import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.AssaySpread;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayGroupVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 * Unit test the Configuration object.
 *
 * @author geoff
 */
public class ConfigurationTest {

  private static final int arbitraryDefaultSimulationRequestProcessingPollingPeriod = 1000;

  private Configuration configuration;
  private IMocksControl mocksControl;
  private boolean dummyDefaultForceReRun;
  private boolean dummyDefaultAssayGrouping;
  private boolean dummyDefaultValueInheriting;
  private boolean dummyDefaultBetweenGroups;
  private boolean dummyDefaultWithinGroups;
  private Set<AssayGroupVO> dummyAssayGroups;
  private Set<AssayVO> dummyAssays;
  private Set<IonChannel> dummyIonChannels;

  private short dummyDefaultMinInformationLevel;
  private short dummyDefaultMaxInformationLevel;
  private short dummyDefaultQuietPeriod;

  @Before
  public void setUp() {
    dummyAssayGroups = new HashSet<AssayGroupVO>();
    dummyAssays = new HashSet<AssayVO>();
    dummyIonChannels = new HashSet<IonChannel>();
    dummyDefaultForceReRun = false;
    dummyDefaultAssayGrouping = false;
    dummyDefaultValueInheriting = false;
    dummyDefaultBetweenGroups = false;
    dummyDefaultWithinGroups = false;
    dummyDefaultMinInformationLevel = 2;
    dummyDefaultMaxInformationLevel = 5;
    dummyDefaultQuietPeriod = 82;

    configuration = newConfiguration();
  }

  private Configuration newConfiguration() {
    return new Configuration(dummyDefaultForceReRun, dummyDefaultAssayGrouping,
                             dummyDefaultValueInheriting,
                             dummyDefaultBetweenGroups,
                             dummyDefaultWithinGroups,
                             dummyDefaultMinInformationLevel,
                             dummyDefaultMaxInformationLevel,
                             dummyDefaultQuietPeriod, dummyAssays,
                             dummyAssayGroups, dummyIonChannels);
  }

  @Test
  public void testConstructor() {
    assertEquals(dummyDefaultForceReRun, configuration.isDefaultForceReRun());
    assertEquals(dummyDefaultAssayGrouping,
                 configuration.isDefaultAssayGrouping());
    assertEquals(dummyDefaultValueInheriting,
                 configuration.isDefaultValueInheriting());
    assertEquals(dummyDefaultBetweenGroups,
                 configuration.isDefaultBetweenGroups());
    assertEquals(dummyDefaultWithinGroups,
                 configuration.isDefaultWithinGroups());
    assertSame(dummyDefaultMinInformationLevel,
               configuration.getDefaultMinInformationLevel());
    assertSame(dummyDefaultMaxInformationLevel,
               configuration.getDefaultMaxInformationLevel());
    assertSame(dummyDefaultQuietPeriod, configuration.getDefaultQuietPeriod());
    assertTrue(configuration.toString()
              .contains(String.valueOf(dummyDefaultQuietPeriod)));

    dummyDefaultForceReRun = true;
    dummyDefaultAssayGrouping = true;
    dummyDefaultValueInheriting = true;
    dummyDefaultBetweenGroups = true;
    dummyDefaultWithinGroups = true;

    configuration = newConfiguration();

    assertEquals(dummyDefaultForceReRun, configuration.isDefaultForceReRun());
    assertEquals(dummyDefaultAssayGrouping,
                 configuration.isDefaultAssayGrouping());
    assertEquals(dummyDefaultValueInheriting,
                 configuration.isDefaultValueInheriting());
    assertEquals(dummyDefaultBetweenGroups,
                 configuration.isDefaultBetweenGroups());
    assertEquals(dummyDefaultWithinGroups,
                 configuration.isDefaultWithinGroups());

    assertTrue(configuration.getAssayGroups().isEmpty());
    assertTrue(configuration.getAssays().isEmpty());
    assertTrue(configuration.getChangeCheckers().isEmpty());
    assertTrue(configuration.getDefaultPC50EvaluationStrategies().isEmpty());
    assertTrue(configuration.getIonChannels().isEmpty());
    assertTrue(configuration.retrieveAssayGroupLevelByAssayName().isEmpty());
    assertTrue(configuration.retrieveAssayLevelByAssayName().isEmpty());
    assertTrue(configuration.retrieveAssayNameByAssayLevel().isEmpty());
    assertTrue(configuration.retrieveAssayGroupNameByGroupLevelMap().isEmpty());
    assertTrue(configuration.retrieveOrderedStrategiesAsCSV().length() == 0);
  }

  @Test
  public void testAssayConfiguration() {
    /*
     * Null value for constructor.
     */
    dummyAssays = null;
    configuration = newConfiguration();
    assertTrue(configuration.getAssays().isEmpty());
    assertTrue(configuration.retrieveAssayGroupLevelByAssayName().isEmpty());
    assertTrue(configuration.retrieveAssayLevelByAssayName().isEmpty());
    assertTrue(configuration.retrieveAssayNameByAssayLevel().isEmpty());

    /*
     * Empty value for constructor.
     */
    dummyAssays = new HashSet<AssayVO>();
    configuration = newConfiguration();
    assertTrue(configuration.getAssays().isEmpty());
    assertTrue(configuration.retrieveAssayGroupLevelByAssayName().isEmpty());
    assertTrue(configuration.retrieveAssayLevelByAssayName().isEmpty());
    assertTrue(configuration.retrieveAssayNameByAssayLevel().isEmpty());

    mocksControl = createControl();

    /*
     * Assay supplied.
     */
    final AssayVO mockAssayVO1 = mocksControl.createMock(AssayVO.class);
    dummyAssays.add(mockAssayVO1);

    final String dummyAssayName1 = "dummyAssayName1";
    expect(mockAssayVO1.getName()).andReturn(dummyAssayName1);
    // With confidence interval spreads
    boolean dummyConfidenceIntervalSpreads1 = true;
    expect(mockAssayVO1.hasConfidenceIntervalSpreads())
          .andReturn(dummyConfidenceIntervalSpreads1);
    final Set<AssaySpread> dummyAssaySpreads = new HashSet<AssaySpread>();
    expect(mockAssayVO1.getAssaySpreads()).andReturn(dummyAssaySpreads);

    mocksControl.replay();

    configuration = newConfiguration();

    mocksControl.verify();

    assertEquals(dummyAssays.size(), configuration.getAssays().size());

    mocksControl.reset();

    /*
     * When no confidence interval spreads.
     */
    expect(mockAssayVO1.getName()).andReturn(dummyAssayName1);
    dummyConfidenceIntervalSpreads1 = false;
    expect(mockAssayVO1.hasConfidenceIntervalSpreads())
          .andReturn(dummyConfidenceIntervalSpreads1);

    mocksControl.replay();

    configuration = newConfiguration();

    mocksControl.verify();

    mocksControl.reset();

    /*
     * More than one assay
     */
    final AssayVO mockAssayVO2 = mocksControl.createMock(AssayVO.class);
    dummyAssays.add(mockAssayVO2);

    expect(mockAssayVO1.getName()).andReturn(dummyAssayName1);
    expect(mockAssayVO1.hasConfidenceIntervalSpreads())
          .andReturn(dummyConfidenceIntervalSpreads1);
    final String dummyAssayName2 = "dummyAssayName2";
    expect(mockAssayVO2.getName()).andReturn(dummyAssayName2);
    final boolean dummyConfidenceIntervalSpreads2 = false;
    expect(mockAssayVO2.hasConfidenceIntervalSpreads())
          .andReturn(dummyConfidenceIntervalSpreads2);

    mocksControl.replay();

    configuration = newConfiguration();

    mocksControl.verify();

    assertEquals(2, configuration.getAssays().size());
    try {
      final Set<AssayVO> configuredAssays = configuration.getAssays();
      configuredAssays.clear();
      fail("Should not permit the modification of configured assays.");
    } catch (UnsupportedOperationException e) {}

    mocksControl.reset();

    /*
     * Test the assay data retrieval in mapped form
     * Note: We don't know what order the assays will be processed!!
     */
    // Test group level by name
    expect(mockAssayVO1.getName()).andReturn(dummyAssayName1);
    final AssayGroupVO mockAssayGroup1 = mocksControl.createMock(AssayGroupVO.class);
    expect(mockAssayVO1.getGroup()).andReturn(mockAssayGroup1);
    final Short dummyAssayGroup1Level = 2;
    expect(mockAssayGroup1.getLevel()).andReturn(dummyAssayGroup1Level);

    expect(mockAssayVO2.getName()).andReturn(dummyAssayName2);
    final AssayGroupVO mockAssayGroup2 = mocksControl.createMock(AssayGroupVO.class);
    expect(mockAssayVO2.getGroup()).andReturn(mockAssayGroup2);
    final Short dummyAssayGroup2Level = 4;
    expect(mockAssayGroup2.getLevel()).andReturn(dummyAssayGroup2Level);

    mocksControl.replay();

    final Map<String, Short> configuredGroupLevelByName = configuration.retrieveAssayGroupLevelByAssayName();

    mocksControl.verify();

    try {
      configuredGroupLevelByName.clear();
      fail("Should not permit modification of assay group level by assay name collection.");
    } catch (UnsupportedOperationException e) {}

    assertEquals(dummyAssayGroup1Level, configuredGroupLevelByName.get(dummyAssayName1));
    assertEquals(dummyAssayGroup2Level, configuredGroupLevelByName.get(dummyAssayName2));

    mocksControl.reset();

    /*
     * Test level by name
     */
    expect(mockAssayVO1.getName()).andReturn(dummyAssayName1);
    final Short dummyAssayLevel1 = 5;
    expect(mockAssayVO1.getLevel()).andReturn(dummyAssayLevel1);

    expect(mockAssayVO2.getName()).andReturn(dummyAssayName2);
    final Short dummyAssayLevel2 = 6;
    expect(mockAssayVO2.getLevel()).andReturn(dummyAssayLevel2);

    mocksControl.replay();
    final Map<String, Short> configuredAssayLevelByName = configuration.retrieveAssayLevelByAssayName();

    mocksControl.verify();

    try {
      configuredAssayLevelByName.clear();
      fail("Should not permit modification of assay level by name collection.");
    } catch (UnsupportedOperationException e) {}

    assertEquals(dummyAssayLevel1, configuredAssayLevelByName.get(dummyAssayName1));
    assertEquals(dummyAssayLevel2, configuredAssayLevelByName.get(dummyAssayName2));

    mocksControl.reset();

    /*
     * Test name by level
     */
    expect(mockAssayVO1.getName()).andReturn(dummyAssayName1);
    expect(mockAssayVO1.getLevel()).andReturn(dummyAssayLevel1);
    expect(mockAssayVO2.getName()).andReturn(dummyAssayName2);
    expect(mockAssayVO2.getLevel()).andReturn(dummyAssayLevel2);

    mocksControl.replay();
    final Map<Short, String> configuredAssayNameByLevel = configuration.retrieveAssayNameByAssayLevel();

    mocksControl.verify();

    try {
      configuredAssayNameByLevel.clear();
      fail("Should not permit modification of assay name by level collection.");
    } catch (UnsupportedOperationException e) {}

    assertTrue(dummyAssayName1.equals(configuredAssayNameByLevel.get(dummyAssayLevel1)));
    assertTrue(dummyAssayName2.equals(configuredAssayNameByLevel.get(dummyAssayLevel2)));
  }

  @Test
  public void testAssayGroupConfiguration() {
    /*
     * Test null constructor arg
     */
    dummyAssayGroups = null;

    configuration = newConfiguration();

    assertTrue(configuration.getAssayGroups().isEmpty());
    assertTrue(configuration.retrieveAssayGroupLevelByAssayName().isEmpty());

    /*
     * Test empty constructor arg
     */
    dummyAssayGroups  = new HashSet<AssayGroupVO>();

    configuration = newConfiguration();

    assertTrue(configuration.getAssayGroups().isEmpty());
    assertTrue(configuration.retrieveAssayGroupLevelByAssayName().isEmpty());

    // Non-strict used because using HashMaps in processing.
    mocksControl = createControl();

    /*
     * Test setting a single assay group
     */
    final AssayGroupVO mockAssayGroup1 = mocksControl.createMock(AssayGroupVO.class);
    dummyAssayGroups.add(mockAssayGroup1);

    final String dummyAssayGroup1Name = "dummyAssayGroup1Name";
    expect(mockAssayGroup1.getName()).andReturn(dummyAssayGroup1Name);

    mocksControl.replay();

    configuration = newConfiguration();

    mocksControl.verify();

    assertEquals(dummyAssayGroups.size(), configuration.getAssayGroups().size());

    mocksControl.reset();

    /*
     * Two assay groups.
     */
    final AssayGroupVO mockAssayGroup2 = mocksControl.createMock(AssayGroupVO.class);
    dummyAssayGroups.add(mockAssayGroup2);

    expect(mockAssayGroup1.getName()).andReturn(dummyAssayGroup1Name);
    final String dummyAssayGroup2Name = "dummyAssayGroup2Name";
    expect(mockAssayGroup2.getName()).andReturn(dummyAssayGroup2Name);

    mocksControl.replay();

    configuration = newConfiguration();

    mocksControl.verify();

    assertEquals(dummyAssayGroups.size(), configuration.getAssayGroups().size());

    try {
       final Set<AssayGroupVO> configuredAssayGroups = configuration.getAssayGroups();
       configuredAssayGroups.clear();
       fail("Should not permit modification of configured assay groups.");
    } catch (UnsupportedOperationException e) {}

    mocksControl.reset();

    /*
     * Test the assay group retrieval in mapped form
     */
    // Note: We don't know what order the retrieveAssayGroupByGroupLevelMap will process!!
    final Short dummyAssayGroup1Level = 1;
    expect(mockAssayGroup1.getLevel()).andReturn(dummyAssayGroup1Level);
    expect(mockAssayGroup1.getName()).andReturn(dummyAssayGroup1Name);
    final Short dummyAssayGroup2Level = 2;
    expect(mockAssayGroup2.getLevel()).andReturn(dummyAssayGroup2Level);
    expect(mockAssayGroup2.getName()).andReturn(dummyAssayGroup2Name);
 
    mocksControl.replay();

    final Map<Short, String> groupsByName = configuration.retrieveAssayGroupNameByGroupLevelMap();

    mocksControl.verify();

    try {
      groupsByName.clear();
      fail("Should not permit the modification of retrieved assay group names by level map.");
    } catch (UnsupportedOperationException e) {}

    assertTrue(dummyAssayGroup1Name.equals(groupsByName.get(dummyAssayGroup1Level)));
    assertTrue(dummyAssayGroup2Name.equals(groupsByName.get(dummyAssayGroup2Level)));
  }

  @Test
  public void testDefaultRequestProcessingPolling() {
    configuration = newConfiguration();

    assertEquals(arbitraryDefaultSimulationRequestProcessingPollingPeriod,
                 configuration.getDefaultRequestProcessingPolling());

    int dummyDefaultRequestProcessingPolling = -5;
    configuration.setDefaultRequestProcessingPolling(dummyDefaultRequestProcessingPolling);

    assertEquals(arbitraryDefaultSimulationRequestProcessingPollingPeriod,
                 configuration.getDefaultRequestProcessingPolling());

    dummyDefaultRequestProcessingPolling = arbitraryDefaultSimulationRequestProcessingPollingPeriod + 4;
    configuration.setDefaultRequestProcessingPolling(dummyDefaultRequestProcessingPolling);

    assertEquals(dummyDefaultRequestProcessingPolling,
                 configuration.getDefaultRequestProcessingPolling());
  }

  @Test
  public void testIonChannelConfiguration() {
    /*
     * Test null constructor arg.
     */
    dummyIonChannels = null;

    configuration = newConfiguration();

    assertTrue(configuration.getIonChannels().isEmpty());

    /*
     * Test empty constructor arg.
     */
    dummyIonChannels = new HashSet<IonChannel>();

    configuration = newConfiguration();

    assertTrue(configuration.getIonChannels().isEmpty());

    /*
     * Add an ion channel (or two).
     */
    dummyIonChannels.add(IonChannel.CaV1_2);

    configuration = newConfiguration();

    assertEquals(dummyIonChannels.size(), configuration.getIonChannels().size());

    dummyIonChannels.add(IonChannel.hERG);

    configuration = newConfiguration();

    assertEquals(dummyIonChannels.size(), configuration.getIonChannels().size());

    try {
      final Set<IonChannel> configuredIonChannels = configuration.getIonChannels();
      configuredIonChannels.clear();
      fail("Should not permit modification of configured ion channels.");
    } catch (UnsupportedOperationException e) {}
  }

  @Test
  public void testStrategyConfiguration() {
    assertTrue("".equals(configuration.retrieveOrderedStrategiesAsCSV()));
    try {
      configuration.retrieveStrategiesByCSV(null);
      fail("Should not permit strategy retrieval using a null invocation order CSV.");
    } catch (IllegalArgumentException e) {}
    String dummyDefaultInvocationOrdersCSV = "";
    assertTrue(configuration.retrieveStrategiesByCSV(dummyDefaultInvocationOrdersCSV).isEmpty());

    dummyDefaultInvocationOrdersCSV = "1,2,3";
    assertTrue(configuration.retrieveStrategiesByCSV(dummyDefaultInvocationOrdersCSV).isEmpty());

    mocksControl = createStrictControl();
    // Test the retrieval of ordered strategies as CSV
    final List<PC50EvaluationStrategy> dummyDefaultPC50EvaluationStrategies = new ArrayList<PC50EvaluationStrategy>();
    final PC50EvaluationStrategy mockPC50EvaluationStrategy1 = mocksControl.createMock(PC50EvaluationStrategy.class);
    final PC50EvaluationStrategy mockPC50EvaluationStrategy2 = mocksControl.createMock(PC50EvaluationStrategy.class);
    final PC50EvaluationStrategy mockPC50EvaluationStrategy3 = mocksControl.createMock(PC50EvaluationStrategy.class);
    final PC50EvaluationStrategy mockPC50EvaluationStrategy4 = mocksControl.createMock(PC50EvaluationStrategy.class);
    dummyDefaultPC50EvaluationStrategies.add(mockPC50EvaluationStrategy1);
    dummyDefaultPC50EvaluationStrategies.add(mockPC50EvaluationStrategy2);
    dummyDefaultPC50EvaluationStrategies.add(mockPC50EvaluationStrategy3);
    dummyDefaultPC50EvaluationStrategies.add(mockPC50EvaluationStrategy4);

    ReflectionTestUtils.setField(configuration, "defaultPC50EvaluationStrategies",
                                 dummyDefaultPC50EvaluationStrategies);

    boolean dummyPC50EvaluationStrategy1DefaultActive = true;
    expect(mockPC50EvaluationStrategy1.getDefaultActive()).andReturn(dummyPC50EvaluationStrategy1DefaultActive);
    int dummyPC50EvaluationStrategy1Order = 1;
    expect(mockPC50EvaluationStrategy1.getOrder()).andReturn(dummyPC50EvaluationStrategy1Order);
    boolean dummyPC50EvaluationStrategy2DefaultActive = false;
    expect(mockPC50EvaluationStrategy2.getDefaultActive()).andReturn(dummyPC50EvaluationStrategy2DefaultActive);
    boolean dummyPC50EvaluationStrategy3DefaultActive = true;
    expect(mockPC50EvaluationStrategy3.getDefaultActive()).andReturn(dummyPC50EvaluationStrategy3DefaultActive);
    int dummyPC50EvaluationStrategy3Order = 3;
    expect(mockPC50EvaluationStrategy3.getOrder()).andReturn(dummyPC50EvaluationStrategy3Order);
    boolean dummyPC50EvaluationStrategy4DefaultActive = true;
    expect(mockPC50EvaluationStrategy4.getDefaultActive()).andReturn(dummyPC50EvaluationStrategy4DefaultActive);
    int dummyPC50EvaluationStrategy4Order = 4;
    expect(mockPC50EvaluationStrategy4.getOrder()).andReturn(dummyPC50EvaluationStrategy4Order);

    mocksControl.replay();
    String configuredOrderedStrategiesAsCSV = configuration.retrieveOrderedStrategiesAsCSV();

    mocksControl.verify();
    assertTrue("1,3,4".equals(configuredOrderedStrategiesAsCSV));

    mocksControl.reset();

    // Test the retrieve of strategies by CSV
    expect(mockPC50EvaluationStrategy1.getOrder()).andReturn(dummyPC50EvaluationStrategy1Order);
    int dummyPC50EvaluationStrategy2Order = 2;
    expect(mockPC50EvaluationStrategy2.getOrder()).andReturn(dummyPC50EvaluationStrategy2Order);
    expect(mockPC50EvaluationStrategy3.getOrder()).andReturn(dummyPC50EvaluationStrategy3Order);
    mocksControl.replay();
    List<PC50EvaluationStrategy> configuredStrategies = configuration.retrieveStrategiesByCSV("3");

    mocksControl.verify();
    assertEquals(1, configuredStrategies.size());
    assertEquals(mockPC50EvaluationStrategy3, configuredStrategies.get(0));

    mocksControl.reset();

    // Test the retrieve of strategies by CSV
    expect(mockPC50EvaluationStrategy1.getOrder()).andReturn(dummyPC50EvaluationStrategy1Order);
    expect(mockPC50EvaluationStrategy2.getOrder()).andReturn(dummyPC50EvaluationStrategy2Order);
    expect(mockPC50EvaluationStrategy3.getOrder()).andReturn(dummyPC50EvaluationStrategy3Order);
    expect(mockPC50EvaluationStrategy1.getOrder()).andReturn(dummyPC50EvaluationStrategy1Order).times(2);
    expect(mockPC50EvaluationStrategy2.getOrder()).andReturn(dummyPC50EvaluationStrategy2Order);
    mocksControl.replay();
    configuredStrategies = configuration.retrieveStrategiesByCSV("3,1,2");

    mocksControl.verify();
    assertEquals(3, configuredStrategies.size());
    assertEquals(mockPC50EvaluationStrategy3, configuredStrategies.get(0));
    assertEquals(mockPC50EvaluationStrategy1, configuredStrategies.get(1));
    assertEquals(mockPC50EvaluationStrategy2, configuredStrategies.get(2));
  }
}