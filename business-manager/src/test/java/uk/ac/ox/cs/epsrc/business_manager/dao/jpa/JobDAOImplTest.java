/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.dao.jpa;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.dao.JobDAO;
import uk.ac.ox.cs.epsrc.business_manager.entity.Job;

/**
 * Unit test the Job DAO implementation.
 *
 * @author geoff
 */
public class JobDAOImplTest {
  private EntityManager mockEntityManager;
  private IMocksControl mocksControl;
  private long dummyJobId;
  private long dummySimulationId;
  private Query mockQuery;
  private Job mockJob;
  private JobDAO jobDAO;

  @Before
  public void setUp() {
    dummyJobId = 11l;
    dummySimulationId = 33l;
    jobDAO = new JobDAOImpl();

    mocksControl = createStrictControl();
    mockEntityManager = mocksControl.createMock(EntityManager.class);
    mockQuery = mocksControl.createMock(Query.class);

    ReflectionTestUtils.setField(jobDAO, "entityManager", mockEntityManager);

    mockJob = mocksControl.createMock(Job.class);
  }

  @Test
  public void testAssignAppManagerIdToJob() {
    String dummyAppManagerId = null;

    // Emulate not finding Job by id
    expect(mockEntityManager.find(Job.class, dummyJobId)).andReturn(null);

    mocksControl.replay();

    jobDAO.assignAppManagerIdToJob(dummyJobId, dummyAppManagerId);

    mocksControl.verify();

    mocksControl.reset();

    // Emulate finding Job 
    expect(mockEntityManager.find(Job.class, dummyJobId)).andReturn(mockJob);
    mockJob.setAppManagerId(dummyAppManagerId);
    expect(mockEntityManager.merge(mockJob)).andReturn(mockJob);

    mocksControl.replay();

    jobDAO.assignAppManagerIdToJob(dummyJobId, dummyAppManagerId);

    mocksControl.verify();
  }

  @Test
  public void testFindJobByJobId() {
    /*
     * Emulate not found.
     */
    expect(mockEntityManager.find(Job.class, dummyJobId)).andReturn(null);

    mocksControl.replay();

    Job returnedJob = jobDAO.findByJobId(dummyJobId);

    mocksControl.verify();

    assertNull(returnedJob);

    mocksControl.reset();

    /*
     * Emulate found.
     */
    expect(mockEntityManager.find(Job.class, dummyJobId)).andReturn(mockJob);

    mocksControl.replay();

    returnedJob = jobDAO.findByJobId(dummyJobId);

    mocksControl.verify();

    assertSame(mockJob, returnedJob);
  }

  @Test
  public void testFindJobByAppManagerId() {
    /*
      TODO:  Test null AppManager id supplied.
    String dummyAppManagerId = null;
    Job job = null;

    try {
      job = simulationDAO.findByCompoundIdentifier(dummyCompoundIdentifier);
    } catch (AssertionError e) {}

    assertNull(simulation);
    */

    String dummyAppManagerId = "dummyAppManagerId";
    Job returnedJob = null;

    // Test capturing NoResultException if no job encountered.
    expect(mockEntityManager.createNamedQuery(Job.QUERY_JOB_BY_APP_MANAGER_ID))
          .andReturn(mockQuery);
    expect(mockQuery.setParameter(Job.PROPERTY_APP_MANAGER_ID, dummyAppManagerId))
          .andReturn(mockQuery);

    expect(mockQuery.getSingleResult()).andThrow(new NoResultException());

    mocksControl.replay();

    returnedJob = jobDAO.findJobByAppManagerId(dummyAppManagerId);

    mocksControl.verify();

    assertNull(returnedJob);

    mocksControl.reset();

    // Test that Job object returned.
    expect(mockEntityManager.createNamedQuery(Job.QUERY_JOB_BY_APP_MANAGER_ID))
          .andReturn(mockQuery);
    expect(mockQuery.setParameter(Job.PROPERTY_APP_MANAGER_ID, dummyAppManagerId))
          .andReturn(mockQuery);

    expect(mockQuery.getSingleResult()).andReturn(mockJob);

    mocksControl.replay();

    returnedJob = jobDAO.findJobByAppManagerId(dummyAppManagerId);

    mocksControl.verify();

    assertNotNull(returnedJob);
  }

  @Test
  public void testFindJobsBySimulationId() {
    // Test that Jobs are returned.
    expect(mockEntityManager.createNamedQuery(Job.QUERY_JOB_DATA_BY_SIMULATION_ID))
          .andReturn(mockQuery);
    expect(mockQuery.setParameter(Job.PROPERTY_SIMULATION_ID, dummySimulationId))
          .andReturn(mockQuery);
    List<Object> foundObjects = new ArrayList<Object>();
    expect(mockQuery.getResultList()).andReturn(foundObjects);

    mocksControl.replay();

    List<Job> foundJobs = jobDAO.findJobsBySimulationId(dummySimulationId);

    mocksControl.verify();

    assertTrue(foundObjects.size() == foundJobs.size());
  }

  @Test
  public void testIncompleteJobsForSimulationType() {
    // Test that Jobs are returned.
    expect(mockEntityManager.createNamedQuery(Job.QUERY_JOB_BY_COMPLETED_FOR_SIMULATION))
          .andReturn(mockQuery);
    expect(mockQuery.setParameter(Job.PROPERTY_SIMULATION_ID, dummySimulationId))
          .andReturn(mockQuery);
    Object incompleteCount = 0L;
    expect(mockQuery.getSingleResult()).andReturn(incompleteCount);

    mocksControl.replay();

    Long returnedIncompleteCount = jobDAO.incompleteJobsForSimulationCount(dummySimulationId);

    mocksControl.verify();

    assertEquals(incompleteCount, returnedIncompleteCount);
  }

  @Test
  public void testRemoveJobs() {
    // Pass a null parameter
    List<Job> dummyJobs = null;

    mocksControl.replay();

    boolean jobsRemoved = jobDAO.removeJobs(dummyJobs);

    mocksControl.verify();

    assertFalse(jobsRemoved);

    mocksControl.reset();

    // Pass an empty collection.
    dummyJobs = new ArrayList<Job>();

    mocksControl.replay();

    jobsRemoved = jobDAO.removeJobs(dummyJobs);

    mocksControl.verify();

    assertFalse(jobsRemoved);

    mocksControl.reset();
    
    // Test when a job is specified.
    final Job mockJob = mocksControl.createMock(Job.class);
    dummyJobs.add(mockJob);

    mockEntityManager.remove(mockJob);

    mocksControl.replay();

    jobsRemoved = jobDAO.removeJobs(dummyJobs);

    mocksControl.verify();

    assertTrue(jobsRemoved);
  }

  @Test
  public void testSave() {
    // Test when job is not yet persisted.
    expect(mockJob.getId()).andReturn(null);
    mockEntityManager.persist(mockJob);

    mocksControl.replay();

    jobDAO.save(mockJob);

    mocksControl.verify();

    mocksControl.reset();

    // Test when job persisted
    expect(mockJob.getId()).andReturn(dummyJobId);
    expect(mockEntityManager.merge(mockJob)).andReturn(mockJob);

    mocksControl.replay();

    jobDAO.save(mockJob);

    mocksControl.verify();
  }

  @Test
  public void testStoreJobResults() {
    /*
     * Store job as incomplete.
     */
    boolean dummyAssignCompleted = false;

    expect(mockEntityManager.merge(mockJob)).andReturn(mockJob);

    mocksControl.replay();

    jobDAO.storeJobResults(mockJob, dummyAssignCompleted);

    mocksControl.verify();

    mocksControl.reset();

    /*
     * Store job as completed.
     */
    dummyAssignCompleted = true;

    mockJob.assignCompleted();
    expect(mockEntityManager.merge(mockJob)).andReturn(mockJob);

    mocksControl.replay();

    jobDAO.storeJobResults(mockJob, dummyAssignCompleted);

    mocksControl.verify();
  }

  @Test
  public void testStoreJobs() {
    final Set<Job> dummyJobs = new HashSet<Job>();
    dummyJobs.add(mockJob);

    mockEntityManager.persist(mockJob);

    mocksControl.replay();

    jobDAO.storeJobs(dummyJobs);

    mocksControl.verify();
  }
}