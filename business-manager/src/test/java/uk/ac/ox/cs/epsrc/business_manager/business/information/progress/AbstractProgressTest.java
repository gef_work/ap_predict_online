/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.information.progress;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.AbstractProgress;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Unit test the abstract progress.
 *
 * @author geoff
 */
public class AbstractProgressTest {

  private InformationLevel dummyInformationLevel;
  private String[] dummyArgs;
  private String dummyText;
  private Long dummySimulationId;

  @Before
  public void setUp() {
    dummyArgs = null;
    dummyInformationLevel = null;
    dummyText = null;
    dummySimulationId = null;
  }

  private class TestAbstractProgress extends AbstractProgress {
    private static final long serialVersionUID = 1L;

    protected TestAbstractProgress(final Builder builder) {
      super(builder);
    }
  }

  @Test
  public void testThreeArgConstructor() {
    dummyText = "dummyText";
    dummySimulationId = 1L;
    try {
      new AbstractProgress.Builder(dummyInformationLevel, dummyText, dummySimulationId);
      fail("Should not permit the assignment of null information level in constructor.");
    } catch (IllegalArgumentException e) {}

    dummyInformationLevel = InformationLevel.TRACE;
    dummyText = null;
    try {
      new AbstractProgress.Builder(dummyInformationLevel, dummyText, dummySimulationId);
      fail("Should not permit the assignment of null text in constructor.");
    } catch (IllegalArgumentException e) {}

    dummyText = "  ";
    try {
      new AbstractProgress.Builder(dummyInformationLevel, dummyText, dummySimulationId);
      fail("Should not permit the assignment of blank text in constructor.");
    } catch (IllegalArgumentException e) {}

    dummySimulationId = null;
    dummyText = "dummyText";
    try {
      new AbstractProgress.Builder(dummyInformationLevel, dummyText, dummySimulationId);
      fail("Should not permit the assignment of null simulation id in constructor.");
    } catch (IllegalArgumentException e) {}

    dummySimulationId = 1L;
    final AbstractProgress.Builder builder = new AbstractProgress.Builder(dummyInformationLevel,
                                                                          dummyText,
                                                                          dummySimulationId);
    final TestAbstractProgress testProgress = new TestAbstractProgress(builder);
    assertEquals(dummyInformationLevel, testProgress.getLevel());
    assertEquals(dummyText, testProgress.getText());
    assertEquals(dummySimulationId, testProgress.getSimulationId());
    assertNotNull(testProgress.getTimestamp());
    assertTrue(testProgress.getArgs().isEmpty());
    assertNotNull(testProgress.toString());
  }

  @Test
  public void testFourArgConstructor() {
    dummyInformationLevel = InformationLevel.TRACE;
    dummyText = "dummyText";
    dummySimulationId = 1L;
    AbstractProgress.Builder builder = new AbstractProgress.Builder(dummyInformationLevel, dummyText,
                                                                    dummyArgs, dummySimulationId);
    TestAbstractProgress testProgress = new TestAbstractProgress(builder);
    assertTrue(testProgress.getArgs().isEmpty());

    dummyArgs = new String[] {};
    builder = new AbstractProgress.Builder(dummyInformationLevel, dummyText, dummyArgs,
                                           dummySimulationId);
    testProgress = new TestAbstractProgress(builder);
    assertTrue(testProgress.getArgs().isEmpty());

    final String arg1 = "fish";
    final String arg2 = "fish";
    final String arg3 = "chips";

    dummyArgs = new String[] { arg1, arg2, arg3 };
    builder = new AbstractProgress.Builder(dummyInformationLevel, dummyText, dummyArgs,
                                           dummySimulationId);
    testProgress = new TestAbstractProgress(builder);

    assertEquals(3, testProgress.getArgs().size());
    assertTrue(arg1.equalsIgnoreCase(testProgress.getArgs().get(0)));
    assertTrue(arg3.equalsIgnoreCase(testProgress.getArgs().get(2)));
  }
}