/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.workflow;

import static org.easymock.EasyMock.anyBoolean;
import static org.easymock.EasyMock.anyLong;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.selection.c50.C50DataSelector;
import uk.ac.ox.cs.epsrc.business_manager.business.workflow.WorkflowProcessor;
import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.service.SimulationService;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.IndividualIVS;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.InputValueSource;
import uk.ac.ox.cs.epsrc.business_manager.value.object.workflow.OutcomeVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.IndividualC50DataStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.ModelAsInactiveStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.doseresponse.FittingStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.summary.SummaryOnlyC50DataStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.NoSuchDataException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingParams;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingResult;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.IndividualProcessing;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;

/**
 * Unit test a complete run-through.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { C50DataSelector.class } )
public class WorkflowProcessorCompleteTest {
  private BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate;
  private Configuration mockConfiguration;
  private IMocksControl mocksControl;
  private SimulationService mockSimulationService;
  private WorkflowProcessor workflowProcessor;
  private final Long dummySimulationId = 4L;
  private ProcessingType dummyProcessingType = ProcessingType.INPUT_DATA_GATHERING;
  private final String dummyCompoundIdentifier = "dummyCompoundIdentifier";

  @Before
  public void setUp() {
    workflowProcessor = new WorkflowProcessor();
    mocksControl = createStrictControl();
    mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    mockConfiguration = mocksControl.createMock(Configuration.class);
    mockSimulationService = mocksControl.createMock(SimulationService.class);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_CONFIGURATION,
                                 mockConfiguration);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE,
                                 mockSimulationService);
  }

  @Test
  public void testRunThrough() throws IllegalArgumentException, InvalidValueException,
                                      NoSuchDataException {
    final ICPercent dummyPctIC = ICPercent.PCT50;
    final SiteDataHolder mockSiteDataHolder = mocksControl.createMock(SiteDataHolder.class);
    final AssayVO mockAssayVO = mocksControl.createMock(AssayVO.class);
    expect(mockSiteDataHolder.getAssay()).andReturn(mockAssayVO);
    final IonChannel dummyIonChannel = IonChannel.hERG;
    expect(mockSiteDataHolder.getIonChannel()).andReturn(dummyIonChannel);
    final String dummyAssayName = "dummyAssayName";
    expect(mockAssayVO.getName()).andReturn(dummyAssayName);

    final SummaryDataRecord mockSummaryDataRecord = mocksControl.createMock(SummaryDataRecord.class);
    expect(mockSiteDataHolder.getSummaryDataRecord(false)).andReturn(mockSummaryDataRecord);
    final String dummySummaryDataRecordId = "dummySummaryDataRecordId";
    expect(mockSummaryDataRecord.getId()).andReturn(dummySummaryDataRecordId);

    // Create the mock PC50 evaluation strategies to iterate through.
    final Simulation mockInputDataGatheringSimulation = mocksControl.createMock(Simulation.class);
    final String dummyInvocationOrdersCSV = "";
    expect(mockInputDataGatheringSimulation.getPc50EvaluationStrategies()).andReturn(dummyInvocationOrdersCSV);
    final List<PC50EvaluationStrategy> dummyPC50EvaluationStrategies = new ArrayList<PC50EvaluationStrategy>();
    final SummaryOnlyC50DataStrategy mockSummaryOnlyC50DataStrategy1 = mocksControl.createMock(TestSummaryOnlyC50DataStrategy.class);
    final SummaryOnlyC50DataStrategy mockSummaryOnlyC50DataStrategy2 = mocksControl.createMock(TestSummaryOnlyC50DataStrategy.class);
    final FittingStrategy mockFittingStrategy = mocksControl.createMock(TestFittingStrategy.class);
    final IndividualC50DataStrategy mockIndividualC50DataStrategy = mocksControl.createMock(TestIndividualC50DataStrategy.class);
    final ModelAsInactiveStrategy mockModelAsInactiveStrategy = mocksControl.createMock(ModelAsInactiveStrategy.class);
    dummyPC50EvaluationStrategies.add((PC50EvaluationStrategy) mockSummaryOnlyC50DataStrategy1);
    dummyPC50EvaluationStrategies.add((PC50EvaluationStrategy) mockModelAsInactiveStrategy);
    dummyPC50EvaluationStrategies.add((PC50EvaluationStrategy) mockFittingStrategy);
    dummyPC50EvaluationStrategies.add((PC50EvaluationStrategy) mockIndividualC50DataStrategy);
    dummyPC50EvaluationStrategies.add((PC50EvaluationStrategy) mockSummaryOnlyC50DataStrategy2);

    expect(mockConfiguration.retrieveStrategiesByCSV(dummyInvocationOrdersCSV)).andReturn(dummyPC50EvaluationStrategies);

    final String dummyDoseResponseFittingStrategy = "dummyDoseResponseFittingStrategy";
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingStrategy()).andReturn(dummyDoseResponseFittingStrategy);
    final boolean isDoseResponseFittingRounding = true;
    expect(mockInputDataGatheringSimulation.isDoseResponseFittingRounding()).andReturn(isDoseResponseFittingRounding);
    final Float dummyDoseResponseFittingHillMax = 0.5F;
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingHillMax()).andReturn(dummyDoseResponseFittingHillMax);
    final Float dummyDoseResponseFittingHillMin = 2F;
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingHillMin()).andReturn(dummyDoseResponseFittingHillMin);

    // 4 individual data records (provisionally use linked hash map).
    final Map<IndividualDataRecord, List<DoseResponseDataRecord>> mockNonSummaryData = 
          new LinkedHashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();
    final IndividualDataRecord mockIndividualDataRecord1 = mocksControl.createMock(IndividualDataRecord.class);
    final IndividualDataRecord mockIndividualDataRecord2 = mocksControl.createMock(IndividualDataRecord.class);
    final IndividualDataRecord mockIndividualDataRecord3 = mocksControl.createMock(IndividualDataRecord.class);
    final IndividualDataRecord mockIndividualDataRecord4 = mocksControl.createMock(IndividualDataRecord.class);
    final IndividualDataRecord mockIndividualDataRecord5 = mocksControl.createMock(IndividualDataRecord.class);
    final IndividualDataRecord mockIndividualDataRecord6 = mocksControl.createMock(IndividualDataRecord.class);
    // Individual data 1 will emulate not returning any C50 data
    mockNonSummaryData.put(mockIndividualDataRecord1, null);
    // Individual data 2 will emulate being inactive
    mockNonSummaryData.put(mockIndividualDataRecord2, null);
    // Individual data 3 will emulate being unable to dose-response fit a pIC50
    mockNonSummaryData.put(mockIndividualDataRecord3, null);
    // Individual data 4 will emulate being able to dose-response fit a pIC50
    mockNonSummaryData.put(mockIndividualDataRecord4, null);
    mockNonSummaryData.put(mockIndividualDataRecord5, null);
    mockNonSummaryData.put(mockIndividualDataRecord6, null);
    expect(mockSiteDataHolder.getNonSummaryDataRecords()).andReturn(mockNonSummaryData);

    // Enter IndividualPC50s
    final List<ICData> dummySummaryEligibleC50Data = new ArrayList<ICData>();
    expect(mockSummaryDataRecord.eligibleICData()).andReturn(dummySummaryEligibleC50Data);
    final ICData mockSummaryC50Data = mocksControl.createMock(ICData.class);
    mockStatic(C50DataSelector.class);
    expect(C50DataSelector.selectFrom(dummySummaryEligibleC50Data)).andReturn(mockSummaryC50Data);
    expect(mockSummaryC50Data.hasICPctData(dummyPctIC)).andReturn(true);
    final Integer dummyCountOfRecordsUsedToDetermineC50Value = null;
    expect(mockSummaryC50Data.retrieveCountOfRecordsUsedToDetermineICValue(ICPercent.PCT50)).andReturn(dummyCountOfRecordsUsedToDetermineC50Value);
    // Count indicates an anonymous individual will need to be created
    final Integer dummyCountOfAvailableRecords = mockNonSummaryData.size() + 1;
    expect(mockSummaryC50Data.retrieveCountOfAvailableRecords(ICPercent.PCT50)).andReturn(dummyCountOfAvailableRecords);

    final String dummyIndividualDataRecord1Id = "dummyIndividualDataRecord1";
    final String dummyIndividualDataRecord2Id = "dummyIndividualDataRecord2";
    final String dummyIndividualDataRecord3Id = "dummyIndividualDataRecord3";
    final String dummyIndividualDataRecord4Id = "dummyIndividualDataRecord4";
    final String dummyIndividualDataRecord5Id = "dummyIndividualDataRecord5";
    final String dummyIndividualDataRecord6Id = "dummyIndividualDataRecord6";
    expect(mockIndividualDataRecord1.getId()).andReturn(dummyIndividualDataRecord1Id);
    expect(mockIndividualDataRecord2.getId()).andReturn(dummyIndividualDataRecord2Id);
    expect(mockIndividualDataRecord3.getId()).andReturn(dummyIndividualDataRecord3Id);
    expect(mockIndividualDataRecord4.getId()).andReturn(dummyIndividualDataRecord4Id);
    expect(mockIndividualDataRecord5.getId()).andReturn(dummyIndividualDataRecord5Id);
    expect(mockIndividualDataRecord6.getId()).andReturn(dummyIndividualDataRecord6Id);
    // Exit IndividualPC50s

    // Enter pIC50-determining strategy iteration.
    final Integer dummyDefaultInvocationOrder1 = 1;
    final Integer dummyDefaultInvocationOrder2 = 2;
    final Integer dummyDefaultInvocationOrder3 = 3;
    final Integer dummyDefaultInvocationOrder4 = 4;
    final Integer dummyDefaultInvocationOrder5 = 5;
    final String dummyDescription1 = "dummyDescription1";
    final String dummyDescription2 = "dummyDescription2";
    final String dummyDescription3 = "dummyDescription3";
    final String dummyDescription4 = "dummyDescription4";
    final String dummyDescription5 = "dummyDescription5";

    /*
     * Strategy 1 .... there's individual data, so evaluate returns null!
     */
    expect(((PC50EvaluationStrategy) mockSummaryOnlyC50DataStrategy1).getOrder()).andReturn(dummyDefaultInvocationOrder1);
    expect(((PC50EvaluationStrategy) mockSummaryOnlyC50DataStrategy1).getDescription()).andReturn(dummyDescription1);

    final BigDecimal dummyStrategy1EvaluatedPC50 = null;
    expect(mockSummaryOnlyC50DataStrategy1.evaluatePC50(isA(SiteDataHolder.class), isA(String.class)))
           .andReturn(dummyStrategy1EvaluatedPC50);

    /*
     * Strategy 2 .... an model as inactive strategy.
     *                 Note the first non-summary strategy encountered fills up the
     *                   IndividualC50DataCache c50DataCache via getICData()!
     */
    expect(((PC50EvaluationStrategy) mockModelAsInactiveStrategy).getOrder()).andReturn(dummyDefaultInvocationOrder2);
    expect(((PC50EvaluationStrategy) mockModelAsInactiveStrategy).getDescription()).andReturn(dummyDescription2);

    // Strategy 2, individual data record 1 (no valid C50 data in individual record)
    expect(mockIndividualDataRecord1.getId()).andReturn(dummyIndividualDataRecord1Id);
    // Enter IndividualC50CacheManager.getICData
    expect(mockIndividualDataRecord1.getId()).andReturn(dummyIndividualDataRecord1Id);
    final List<ICData> dummyIndividualDataRecord1EligibleC50Data = new ArrayList<ICData>();
    expect(mockIndividualDataRecord1.eligibleICData()).andReturn(dummyIndividualDataRecord1EligibleC50Data);
    final ICData dummySelectedIndividualDataRecord1C50Data = null;
    expect(C50DataSelector.selectFrom(dummyIndividualDataRecord1EligibleC50Data))
          .andReturn(dummySelectedIndividualDataRecord1C50Data);
    // Exit IndividualC50CacheManager.getICData
    // Strategy 2, individual data record 2 (model individual record as inactive -- assign default vals)
    expect(mockIndividualDataRecord2.getId()).andReturn(dummyIndividualDataRecord2Id);
    // Enter IndividualC50CacheManager.getICData
    expect(mockIndividualDataRecord2.getId()).andReturn(dummyIndividualDataRecord2Id);
    final List<ICData> dummyIndividualDataRecord2EligibleC50Data = new ArrayList<ICData>();
    expect(mockIndividualDataRecord2.eligibleICData()).andReturn(dummyIndividualDataRecord2EligibleC50Data);
    final ICData mockSelectedIndividualDataRecord2C50Data = mocksControl.createMock(ICData.class);
    expect(C50DataSelector.selectFrom(dummyIndividualDataRecord2EligibleC50Data))
          .andReturn(mockSelectedIndividualDataRecord2C50Data);
    // Exit IndividualC50CacheManager.getICData
    final String dummyIndividualDataRecord2ConsiderInactive = "fakeReason";
    expect(mockModelAsInactiveStrategy.modelAsInactive(isA(AssayVO.class), isA(IndividualDataRecord.class),
                                                       isA(ICData.class), isA(String.class)))
          .andReturn(dummyIndividualDataRecord2ConsiderInactive);
    // Strategy 2, individual data record 3 (active data)
    expect(mockIndividualDataRecord3.getId()).andReturn(dummyIndividualDataRecord3Id);
    // Enter IndividualC50Cachemanager.getICData
    expect(mockIndividualDataRecord3.getId()).andReturn(dummyIndividualDataRecord3Id);
    final List<ICData> dummyIndividualDataRecord3EligibleC50Data = new ArrayList<ICData>();
    expect(mockIndividualDataRecord3.eligibleICData()).andReturn(dummyIndividualDataRecord3EligibleC50Data);
    final ICData mockSelectedIndividualDataRecord3C50Data = mocksControl.createMock(ICData.class);
    expect(C50DataSelector.selectFrom(dummyIndividualDataRecord3EligibleC50Data))
          .andReturn(mockSelectedIndividualDataRecord3C50Data);
    // Exit IndividualC50CacheManager.getICData
    final String dummyIndividualDataRecord3ConsiderInactive = null;
    expect(mockModelAsInactiveStrategy.modelAsInactive(isA(AssayVO.class), isA(IndividualDataRecord.class),
                                                       isA(ICData.class), isA(String.class)))
          .andReturn(dummyIndividualDataRecord3ConsiderInactive);
    // Strategy 2, individual data record 4 (active data)
    expect(mockIndividualDataRecord4.getId()).andReturn(dummyIndividualDataRecord4Id);
    // Enter IndividualC50Cachemanager.getICData
    expect(mockIndividualDataRecord4.getId()).andReturn(dummyIndividualDataRecord4Id);
    final List<ICData> dummyIndividualDataRecord4EligibleC50Data = new ArrayList<ICData>();
    expect(mockIndividualDataRecord4.eligibleICData()).andReturn(dummyIndividualDataRecord4EligibleC50Data);
    final ICData mockSelectedIndividualDataRecord4C50Data = mocksControl.createMock(ICData.class);
    expect(C50DataSelector.selectFrom(dummyIndividualDataRecord4EligibleC50Data))
          .andReturn(mockSelectedIndividualDataRecord4C50Data);
    // Exit IndividualC50CacheManager.getICData
    final String dummyIndividualDataRecord4ConsiderInactive = null;
    expect(mockModelAsInactiveStrategy.modelAsInactive(isA(AssayVO.class), isA(IndividualDataRecord.class),
                                                       isA(ICData.class), isA(String.class)))
          .andReturn(dummyIndividualDataRecord4ConsiderInactive);
    // Strategy 2, individual data record 5 (active data)
    expect(mockIndividualDataRecord5.getId()).andReturn(dummyIndividualDataRecord5Id);
    // Enter IndividualC50Cachemanager.getICData
    expect(mockIndividualDataRecord5.getId()).andReturn(dummyIndividualDataRecord5Id);
    final List<ICData> dummyIndividualDataRecord5EligibleC50Data = new ArrayList<ICData>();
    expect(mockIndividualDataRecord5.eligibleICData()).andReturn(dummyIndividualDataRecord5EligibleC50Data);
    final ICData mockSelectedIndividualDataRecord5C50Data = mocksControl.createMock(ICData.class);
    expect(C50DataSelector.selectFrom(dummyIndividualDataRecord5EligibleC50Data))
          .andReturn(mockSelectedIndividualDataRecord5C50Data);
    // Exit IndividualC50CacheManager.getICData
    final String dummyIndividualDataRecord5ConsiderInactive = null;
    expect(mockModelAsInactiveStrategy.modelAsInactive(isA(AssayVO.class), isA(IndividualDataRecord.class),
                                                       isA(ICData.class), isA(String.class)))
          .andReturn(dummyIndividualDataRecord5ConsiderInactive);
    // Strategy 2, individual data record 6 (active data)
    expect(mockIndividualDataRecord6.getId()).andReturn(dummyIndividualDataRecord6Id);
    // Enter IndividualC50Cachemanager.getICData
    expect(mockIndividualDataRecord6.getId()).andReturn(dummyIndividualDataRecord6Id);
    final List<ICData> dummyIndividualDataRecord6EligibleC50Data = new ArrayList<ICData>();
    expect(mockIndividualDataRecord6.eligibleICData()).andReturn(dummyIndividualDataRecord6EligibleC50Data);
    final ICData mockSelectedIndividualDataRecord6C50Data = mocksControl.createMock(ICData.class);
    expect(C50DataSelector.selectFrom(dummyIndividualDataRecord6EligibleC50Data))
          .andReturn(mockSelectedIndividualDataRecord6C50Data);
    // Exit IndividualC50CacheManager.getICData
    final String dummyIndividualDataRecord6ConsiderInactive = null;
    expect(mockModelAsInactiveStrategy.modelAsInactive(isA(AssayVO.class), isA(IndividualDataRecord.class),
                                                       isA(ICData.class), isA(String.class)))
          .andReturn(dummyIndividualDataRecord6ConsiderInactive);

    /*
     * Strategy 3 .... Fitting strategy
     */
    expect(((PC50EvaluationStrategy) mockFittingStrategy).getOrder()).andReturn(dummyDefaultInvocationOrder3);
    expect(((PC50EvaluationStrategy) mockFittingStrategy).getDescription()).andReturn(dummyDescription3);

    // Strategy 3, individual data record 1 (no ICData value available)
    expect(mockIndividualDataRecord1.getId()).andReturn(dummyIndividualDataRecord1Id).times(2);
    // Strategy 3, individual data record 2 (value already assigned -- continue)
    expect(mockIndividualDataRecord2.getId()).andReturn(dummyIndividualDataRecord2Id);
    // Strategy 3, individual data record 3 (active data, but can't fit a pIC50)
    expect(mockIndividualDataRecord3.getId()).andReturn(dummyIndividualDataRecord3Id).times(2);
    final DoseResponseFittingResult dummyIndividualDataRecord3DRFR = null;
    expect(mockFittingStrategy.fitDoseResponseData(anyLong(), anyBoolean(),
                                                   isA(IndividualProcessing.class),
                                                   isA(ICData.class), isA(String.class),
                                                   isA(DoseResponseFittingParams.class),
                                                   isA(ICPercent.class)))
          .andReturn(dummyIndividualDataRecord3DRFR);
    // Strategy 3, individual data record 4 (active data, can fit a pIC50);
    expect(mockIndividualDataRecord4.getId()).andReturn(dummyIndividualDataRecord4Id).times(2);
    final DoseResponseFittingResult mockIndividualDataRecord4DRFR = mocksControl.createMock(DoseResponseFittingResult.class);
    expect(mockFittingStrategy.fitDoseResponseData(anyLong(), anyBoolean(),
                                                   isA(IndividualProcessing.class),
                                                   isA(ICData.class), isA(String.class),
                                                   isA(DoseResponseFittingParams.class),
                                                   isA(ICPercent.class)))
          .andReturn(mockIndividualDataRecord4DRFR);
    final String dummyFittingErrorMessage = "dummyFittingErrorMessage";
    expect(mockIndividualDataRecord4DRFR.getError()).andReturn(dummyFittingErrorMessage);
    final BigDecimal dummyFittingResultPIC50 = BigDecimal.TEN;
    expect(mockIndividualDataRecord4DRFR.getPC50Value()).andReturn(dummyFittingResultPIC50);
    final BigDecimal dummyFittingResult4HillCoefficient = BigDecimal.ONE;
    expect(mockIndividualDataRecord4DRFR.getHillCoefficient()).andReturn(dummyFittingResult4HillCoefficient);
    // Strategy 3, individual data record 5 (active data, but can't fit a pIC50)
    expect(mockIndividualDataRecord5.getId()).andReturn(dummyIndividualDataRecord5Id).times(2);
    final DoseResponseFittingResult dummyIndividualDataRecord5DRFR = null;
    expect(mockFittingStrategy.fitDoseResponseData(anyLong(), anyBoolean(),
                                                   isA(IndividualProcessing.class),
                                                   isA(ICData.class), isA(String.class),
                                                   isA(DoseResponseFittingParams.class),
                                                   isA(ICPercent.class)))
          .andReturn(dummyIndividualDataRecord5DRFR);
    // Strategy 3, individual data record 6 (active data, but can't fit a pIC50)
    expect(mockIndividualDataRecord6.getId()).andReturn(dummyIndividualDataRecord6Id).times(2);
    final DoseResponseFittingResult dummyIndividualDataRecord6DRFR = null;
    expect(mockFittingStrategy.fitDoseResponseData(anyLong(), anyBoolean(),
                                                   isA(IndividualProcessing.class),
                                                   isA(ICData.class), isA(String.class),
                                                   isA(DoseResponseFittingParams.class),
                                                   isA(ICPercent.class)))
          .andReturn(dummyIndividualDataRecord6DRFR);

    /*
     * Strategy 4 .... Individual C50 data strategy.
     */
    expect(((PC50EvaluationStrategy) mockIndividualC50DataStrategy).getOrder()).andReturn(dummyDefaultInvocationOrder4);
    expect(((PC50EvaluationStrategy) mockIndividualC50DataStrategy).getDescription()).andReturn(dummyDescription4);

    // Strategy 4, individual data record 1 (value already assigned -- continue)
    expect(mockIndividualDataRecord1.getId()).andReturn(dummyIndividualDataRecord1Id).times(2);
    // Strategy 4, individual data record 2 (value already assigned -- continue)
    expect(mockIndividualDataRecord2.getId()).andReturn(dummyIndividualDataRecord2Id);
    // Strategy 4, individual data record 3 (look for a value)
    expect(mockIndividualDataRecord3.getId()).andReturn(dummyIndividualDataRecord3Id).times(2);
    final BigDecimal dummyDataRecord3C50DataPIC50 = null;
    expect(mockIndividualC50DataStrategy.evaluatePC50(isA(ICData.class), isA(String.class)))
          .andReturn(dummyDataRecord3C50DataPIC50);
    // Strategy 4, individual data record 4 (value already assigned -- continue)
    expect(mockIndividualDataRecord4.getId()).andReturn(dummyIndividualDataRecord4Id);
    // Strategy 4, individual data record 5 (look for a value)
    expect(mockIndividualDataRecord5.getId()).andReturn(dummyIndividualDataRecord5Id).times(2);
    final BigDecimal dummyDataRecord5C50DataPIC50 = new BigDecimal("2.0");
    expect(mockIndividualC50DataStrategy.evaluatePC50(isA(ICData.class), isA(String.class)))
          .andReturn(dummyDataRecord5C50DataPIC50);
    final BigDecimal dummyDataRecord5EligibleHill = new BigDecimal("0.777");
    expect(mockIndividualDataRecord5.eligibleHillCoefficient()).andReturn(dummyDataRecord5EligibleHill);
    // Strategy 4, individual data record 6 (look for a value)
    expect(mockIndividualDataRecord6.getId()).andReturn(dummyIndividualDataRecord6Id).times(2);
    final BigDecimal dummyDataRecord6C50DataPIC50 = null;
    expect(mockIndividualC50DataStrategy.evaluatePC50(isA(ICData.class), isA(String.class)))
          .andReturn(dummyDataRecord6C50DataPIC50);

    /*
     * Strategy 5 .... Summary data, nearest integer-style.
     */
    expect(((PC50EvaluationStrategy) mockSummaryOnlyC50DataStrategy2).getOrder()).andReturn(dummyDefaultInvocationOrder5);
    expect(((PC50EvaluationStrategy) mockSummaryOnlyC50DataStrategy2).getDescription()).andReturn(dummyDescription5);

    final BigDecimal dummyStrategy2EvaluatedPC50 = null;
    expect(mockSummaryOnlyC50DataStrategy2.evaluatePC50(isA(SiteDataHolder.class), isA(String.class)))
           .andReturn(dummyStrategy2EvaluatedPC50);

    replayAll();
    mocksControl.replay();

    OutcomeVO outcome = workflowProcessor.processWorkflow(dummyCompoundIdentifier, dummyProcessingType,
                                                          dummySimulationId,
                                                          mockInputDataGatheringSimulation,
                                                          mockSiteDataHolder);

    verifyAll();
    mocksControl.verify();

    assertNotNull(outcome);
    final List<InputValueSource> inputValueSources = outcome.getInputValueSources();
    for (final InputValueSource inputValueSource : inputValueSources) {
      assertTrue(inputValueSource instanceof IndividualIVS);
      final IndividualIVS individualIVS = (IndividualIVS) inputValueSource;
      final BigDecimal pIC50 = individualIVS.getpIC50();
      final BigDecimal hillCoefficient = individualIVS.getHillCoefficient();

      if (dummyDataRecord5C50DataPIC50.compareTo(pIC50) == 0) {
        assertTrue(dummyDataRecord5EligibleHill.compareTo(hillCoefficient) == 0);
      } else if (dummyFittingResultPIC50.compareTo(pIC50) == 0) {
        assertTrue(dummyFittingResult4HillCoefficient.compareTo(hillCoefficient) == 0);
      } else if (BigDecimal.ONE.compareTo(pIC50) == 0) {
        assertTrue(BigDecimal.ZERO.compareTo(hillCoefficient) == 0);
      }
    }
  }
}