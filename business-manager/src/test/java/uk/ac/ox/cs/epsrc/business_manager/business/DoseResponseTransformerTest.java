/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.business.DoseResponseTransformer;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.DoseResponseData;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.IndividualProcessing;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.doseresponse.DoseResponsePairVO;

/**
 * Unit test the Spring Integration dose-response transformer.
 *
 * @author geoff
 */
public class DoseResponseTransformerTest {


  /**
   * Test the transforming to nearest sub-50 response fails on null.
   */
  @Test(expected=IllegalArgumentException.class)
  public void testNearestSub50FailsOnNull() throws IllegalArgumentException, InvalidValueException {
    final IndividualProcessing individualProcessing = null;
    DoseResponseTransformer.nearestSub50ResponseTransform(individualProcessing);
  }

  /**
   * Test the transforming to nearest sub-50 response success.
   */
  @Test
  public void testNearestSub50TransformResponse() throws IllegalArgumentException,
                                                         InvalidValueException {
    final IMocksControl mocksControl = createStrictControl();
    final IndividualProcessing mockIndividualProcessing = mocksControl.createMock(IndividualProcessing.class);
    expect(mockIndividualProcessing.getDoseResponseData()).andReturn(null);
    mocksControl.replay();
    try {
      DoseResponseTransformer.nearestSub50ResponseTransform(mockIndividualProcessing);
      fail("Should not accept null dose-response data content in individual processing object.");
    } catch (IllegalArgumentException e) {}
    mocksControl.verify();

    mocksControl.reset();

    final List<DoseResponseDataRecord> doseResponseDataRecords =
          new ArrayList<DoseResponseDataRecord>();
    expect(mockIndividualProcessing.getDoseResponseData()).andReturn(doseResponseDataRecords);
    mocksControl.replay();
    try {
      DoseResponseTransformer.nearestSub50ResponseTransform(mockIndividualProcessing);
      fail("Should not accept empty dose-response data content in individual processing object.");
    } catch (IllegalArgumentException e) {}
    mocksControl.verify();

    mocksControl.reset();

    /* Return a singular dose-response pair representing a sub-50% response. Expecting the
       transformation request to return that dose-response pair.
       Note: This test effectively tests the DoseResponseDataVO object. */
    DoseResponseDataRecord mockDoseResponseDataRecord = mocksControl.createMock(DoseResponseDataRecord.class);
    doseResponseDataRecords.add(mockDoseResponseDataRecord);
    final DoseResponsePairVO nearestSub50ResponseDoseResponsePair = new DoseResponsePairVO(BigDecimal.ONE,
                                                                                           BigDecimal.TEN);

    expect(mockIndividualProcessing.getDoseResponseData()).andReturn(doseResponseDataRecords);
    expect(mockDoseResponseDataRecord.eligibleDoseResponseData()).andReturn(nearestSub50ResponseDoseResponsePair);

    mocksControl.replay();
    DoseResponseData nearestSub50DoseResponseData = DoseResponseTransformer.nearestSub50ResponseTransform(mockIndividualProcessing);
    mocksControl.verify();
    assertEquals(1, nearestSub50DoseResponseData.doseResponsePoints().size());

    mocksControl.reset();

    /* Return a singular dose-response pair representing an over-50% response. Expecting the
       transformation request to return an empty collection.
       Note: This test effectively tests the DoseResponseDataVO object. */
    doseResponseDataRecords.clear();
    doseResponseDataRecords.add(mockDoseResponseDataRecord);
    final DoseResponsePairVO over50ResponseDoseResponsePair = new DoseResponsePairVO(BigDecimal.ONE,
                                                                                     new BigDecimal("51"));

    expect(mockIndividualProcessing.getDoseResponseData()).andReturn(doseResponseDataRecords);
    expect(mockDoseResponseDataRecord.eligibleDoseResponseData()).andReturn(over50ResponseDoseResponsePair);

    mocksControl.replay();

    nearestSub50DoseResponseData = DoseResponseTransformer.nearestSub50ResponseTransform(mockIndividualProcessing);
    mocksControl.verify();

    assertTrue(nearestSub50DoseResponseData.doseResponsePoints().isEmpty());
  }

  /**
   * Test the transforming incoming dose-response data (derived from dose-response records) fails
   * on null parameter.
   */
  @Test(expected=IllegalArgumentException.class)
  public void testAllDoseResponseTransformFailsOnNull() throws IllegalArgumentException,
                                                                 InvalidValueException {
    DoseResponseTransformer.allDoseResponseTransform(null);
  }

  /**
   * Test the transforming incoming dose-response data (derived from dose-response records) fails
   * on null dose-response content.
   */
  @Test(expected=IllegalArgumentException.class)
  public void testAllDoseResponseTransformFailsOnNullDoseResponses()
              throws IllegalArgumentException, InvalidValueException {
    final IMocksControl mocksControl = createStrictControl();
    final IndividualProcessing mockIndividualProcessing = mocksControl.createMock(IndividualProcessing.class);
    expect(mockIndividualProcessing.getDoseResponseData()).andReturn(null);
    mocksControl.replay();
    DoseResponseTransformer.allDoseResponseTransform(mockIndividualProcessing);
    mocksControl.verify();
  }

  /**
   * Test the transforming incoming dose-response data (derived from dose-response records) fails
   * on empty dose-response content.
   */
  @Test(expected=IllegalArgumentException.class)
  public void testAllDoseResponseTransformFailsOnEmptyDoseResponses()
              throws IllegalArgumentException, InvalidValueException {
    final IMocksControl mocksControl = createStrictControl();
    final IndividualProcessing mockIndividualProcessing = mocksControl.createMock(IndividualProcessing.class);
    expect(mockIndividualProcessing.getDoseResponseData()).andReturn(new ArrayList<DoseResponseDataRecord>());
    mocksControl.replay();
    DoseResponseTransformer.allDoseResponseTransform(mockIndividualProcessing);
    mocksControl.verify();
  }

  /**
   * Test the transforming incoming dose-response data works.
   */
  @Test
  public void testAllDoseResponseTransformSuccess() throws InvalidValueException {
    final IMocksControl mocksControl = createControl();
    final IndividualProcessing mockIndividualProcessing = mocksControl.createMock(IndividualProcessing.class);
    final DoseResponseDataRecord mockDoseResponseDataRecord = mocksControl.createMock(DoseResponseDataRecord.class);
    final List<DoseResponseDataRecord> doseResponseDataRecords = new ArrayList<DoseResponseDataRecord>();
    doseResponseDataRecords.add(mockDoseResponseDataRecord);
    expect(mockIndividualProcessing.getDoseResponseData()).andReturn(doseResponseDataRecords);
    final DoseResponsePairVO mockIndividualDoseResponse = mocksControl.createMock(DoseResponsePairVO.class);
    expect(mockDoseResponseDataRecord.eligibleDoseResponseData()).andReturn(mockIndividualDoseResponse);
    expect(mockIndividualDoseResponse.getResponse()).andReturn(BigDecimal.ONE).atLeastOnce();
    expect(mockIndividualDoseResponse.getDose()).andReturn(BigDecimal.ONE).atLeastOnce();
    mocksControl.replay();
    final DoseResponseData doseResponseData = DoseResponseTransformer.allDoseResponseTransform(mockIndividualProcessing);
    mocksControl.verify();
    assertNotNull(doseResponseData.doseResponsePoints());
    assertSame(1, doseResponseData.doseResponsePoints().size());
  }
}