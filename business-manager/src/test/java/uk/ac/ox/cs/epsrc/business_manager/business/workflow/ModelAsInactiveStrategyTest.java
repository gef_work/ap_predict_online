/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.workflow;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual.DataMarkedAsInactive;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.IndividualProvenance;
import uk.ac.ox.cs.epsrc.business_manager.business.selection.c50.C50DataSelector;
import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.service.SimulationService;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.ModelAsInactiveStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;

/**
 * 
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { C50DataSelector.class } )
public class ModelAsInactiveStrategyTest {

  private AssayVO mockAssayVO;
  private BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate;
  private Configuration mockConfiguration;
  private ICData mockICData;
  private IMocksControl mocksControl;
  private IndividualDataRecord mockIndividualDataRecord;
  private IonChannel dummyIonChannel;
  private PC50EvaluationStrategy mockStrategy;
  private Simulation mockSimulation;
  private SimulationService mockSimulationService;
  private SiteDataHolder mockSiteDataHolder;
  private String dummyAssayName;
  private String dummyIndividualDataRecordId;
  private WorkflowProcessor workflowProcessor;
  private final Long dummySimulationId = 4L;
  private ProcessingType dummyProcessingType;
  private final String dummyCompoundIdentifier = "dummyCompoundIdentifier";

  @Before
  public void setUp() throws InvalidValueException {
    workflowProcessor = new WorkflowProcessor();
    mocksControl = createStrictControl();
    mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    mockConfiguration = mocksControl.createMock(Configuration.class);
    mockSimulationService = mocksControl.createMock(SimulationService.class);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_CONFIGURATION,
                                 mockConfiguration);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE,
                                 mockSimulationService);
  }

  private void setCommonOptions(final ProcessingType dummyProcessingType) throws InvalidValueException {
    mockSiteDataHolder = mocksControl.createMock(SiteDataHolder.class);
    mockAssayVO = mocksControl.createMock(AssayVO.class);
    expect(mockSiteDataHolder.getAssay()).andReturn(mockAssayVO);
    dummyIonChannel = IonChannel.hERG;
    expect(mockSiteDataHolder.getIonChannel()).andReturn(dummyIonChannel);
    dummyAssayName = "dummyAssayName";
    expect(mockAssayVO.getName()).andReturn(dummyAssayName);

    expect(mockSiteDataHolder.getSummaryDataRecord(false)).andReturn(null);

    mockSimulation = mocksControl.createMock(Simulation.class);
    if (!ProcessingType.isInputDataGathering(dummyProcessingType)) {
      expect(mockSimulationService.findBySimulationId(dummySimulationId))
            .andReturn(mockSimulation);
    }

    final String dummyInvocationOrdersCSV = "";
    expect(mockSimulation.getPc50EvaluationStrategies()).andReturn(dummyInvocationOrdersCSV);
    final List<PC50EvaluationStrategy> dummyPC50EvaluationStrategies = new ArrayList<PC50EvaluationStrategy>();
    mockStrategy = mocksControl.createMock(DataMarkedAsInactive.class);
    dummyPC50EvaluationStrategies.add((PC50EvaluationStrategy) mockStrategy);

    expect(mockConfiguration.retrieveStrategiesByCSV(dummyInvocationOrdersCSV)).andReturn(dummyPC50EvaluationStrategies);

    final String dummyDoseResponseFittingStrategy = "dummyDoseResponseFittingStrategy";
    expect(mockSimulation.getDoseResponseFittingStrategy()).andReturn(dummyDoseResponseFittingStrategy);
    final boolean isDoseResponseFittingRounding = true;
    expect(mockSimulation.isDoseResponseFittingRounding()).andReturn(isDoseResponseFittingRounding);
    final Float dummyDoseResponseFittingHillMax = 0.5F;
    expect(mockSimulation.getDoseResponseFittingHillMax()).andReturn(dummyDoseResponseFittingHillMax);
    final Float dummyDoseResponseFittingHillMin = 2F;
    expect(mockSimulation.getDoseResponseFittingHillMin()).andReturn(dummyDoseResponseFittingHillMin);

    final Map<IndividualDataRecord, List<DoseResponseDataRecord>> mockNonSummaryData = 
          new LinkedHashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();
    mockIndividualDataRecord = mocksControl.createMock(IndividualDataRecord.class);
    mockNonSummaryData.put(mockIndividualDataRecord, null);
    expect(mockSiteDataHolder.getNonSummaryDataRecords()).andReturn(mockNonSummaryData);

    dummyIndividualDataRecordId = "dummyIndividualDataRecord1";
    expect(mockIndividualDataRecord.getId()).andReturn(dummyIndividualDataRecordId);

    // Enter pIC50-determining strategy iteration.
    final Integer dummyDefaultInvocationOrder = 1;
    final String dummyDescription = "dummyDescription";
    expect(((PC50EvaluationStrategy) mockStrategy).getOrder()).andReturn(dummyDefaultInvocationOrder);
    expect(((PC50EvaluationStrategy) mockStrategy).getDescription()).andReturn(dummyDescription);

    expect(mockIndividualDataRecord.getId()).andReturn(dummyIndividualDataRecordId);
    // Enter IndividualC50CacheManager.getICData
    expect(mockIndividualDataRecord.getId()).andReturn(dummyIndividualDataRecordId);
    final List<ICData> dummyIndividualDataRecordEligibleC50Data = new ArrayList<ICData>();
    expect(mockIndividualDataRecord.eligibleICData()).andReturn(dummyIndividualDataRecordEligibleC50Data);
    mockICData = mocksControl.createMock(ICData.class);
    mockStatic(C50DataSelector.class);
    expect(C50DataSelector.selectFrom(dummyIndividualDataRecordEligibleC50Data))
          .andReturn(mockICData);
  }

  @Test
  public void testModelAsInactiveStrategyConsiderInactiveInputDataGathering() throws InvalidValueException {

    dummyProcessingType = ProcessingType.INPUT_DATA_GATHERING;

    setCommonOptions(dummyProcessingType);

    final Capture<AssayVO> captureAssayVO = newCapture();
    final Capture<IndividualDataRecord> captureIndividualDataRecord = newCapture();
    final Capture<ICData> captureICData = newCapture();
    final Capture<String> captureIdentifyingPrefix = newCapture();

    final String dummyReason = "dummyReason";
    expect(((ModelAsInactiveStrategy) mockStrategy).modelAsInactive(capture(captureAssayVO),
                                                                    capture(captureIndividualDataRecord),
                                                                    capture(captureICData),
                                                                    capture(captureIdentifyingPrefix)))
          .andReturn(dummyReason);

    replayAll();
    mocksControl.replay();

    workflowProcessor.processWorkflow(dummyCompoundIdentifier, dummyProcessingType,
                                      dummySimulationId, mockSimulation,
                                      mockSiteDataHolder);

    verifyAll();
    mocksControl.verify();

    // Test the correct objects were received by the strategy call.
    final AssayVO capturedAssayVO = (AssayVO) captureAssayVO.getValue();
    final IndividualDataRecord capturedIndividualDataRecord = (IndividualDataRecord) captureIndividualDataRecord.getValue();
    final ICData capturedICData = (ICData) captureICData.getValue();
    final String capturedIdentifyingPrefix = (String) captureIdentifyingPrefix.getValue();

    assertSame(mockAssayVO, capturedAssayVO);
    assertSame(mockIndividualDataRecord, capturedIndividualDataRecord);
    assertSame(mockICData, capturedICData);
    assertTrue(capturedIdentifyingPrefix.contains(dummySimulationId.toString()));
    assertTrue(capturedIdentifyingPrefix.contains(dummyCompoundIdentifier));
    assertTrue(capturedIdentifyingPrefix.contains(dummyAssayName));
    assertTrue(capturedIdentifyingPrefix.contains(dummyIonChannel.toString()));

  }

  @Test
  public void testModelAsInactiveStrategyConsiderActiveInputDataGathering()
              throws InvalidValueException {

    dummyProcessingType = ProcessingType.INPUT_DATA_GATHERING;

    setCommonOptions(dummyProcessingType);

    final String dummyReason = null;
    expect(((ModelAsInactiveStrategy) mockStrategy).modelAsInactive(isA(AssayVO.class),
                                                                  isA(IndividualDataRecord.class),
                                                                  isA(ICData.class),
                                                                  isA(String.class)))
          .andReturn(dummyReason);

    replayAll();
    mocksControl.replay();

    workflowProcessor.processWorkflow(dummyCompoundIdentifier, dummyProcessingType,
                                      dummySimulationId, mockSimulation,
                                      mockSiteDataHolder);

    verifyAll();
    mocksControl.verify();
  }

  @Test
  public void testModelAsInactiveStrategyConsiderInactiveNoInputDataGathering()
              throws InvalidValueException {

    dummyProcessingType = ProcessingType.REGULAR_SIMULATION;

    setCommonOptions(dummyProcessingType);

    final String dummyReason = "dummyReason";
    expect(((ModelAsInactiveStrategy) mockStrategy).modelAsInactive(isA(AssayVO.class),
                                                                    isA(IndividualDataRecord.class),
                                                                    isA(ICData.class),
                                                                    isA(String.class)))
          .andReturn(dummyReason);

    final Capture<IndividualProvenance> captureProvenance1 = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(captureProvenance1));
    final Capture<IndividualProvenance> captureProvenance2 = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(captureProvenance2));

    replayAll();
    mocksControl.replay();

    workflowProcessor.processWorkflow(dummyCompoundIdentifier, dummyProcessingType,
                                      dummySimulationId, mockSimulation,
                                      mockSiteDataHolder);

    verifyAll();
    mocksControl.verify();

    final IndividualProvenance capturedIndividualProvenance1 = (IndividualProvenance) captureProvenance1.getValue();
    assertEquals(dummyIndividualDataRecordId, capturedIndividualProvenance1.getIndividualDataId());
    assertSame(InformationLevel.INFO, capturedIndividualProvenance1.getLevel());
    final IndividualProvenance capturedIndividualProvenance2 = (IndividualProvenance) captureProvenance2.getValue();
    assertEquals(dummyIndividualDataRecordId, capturedIndividualProvenance2.getIndividualDataId());
    assertSame(InformationLevel.INFO, capturedIndividualProvenance2.getLevel());
  }

  @Test
  public void testModelAsInactiveStrategyConsiderActiveNoInputDataGathering()
              throws InvalidValueException {

    dummyProcessingType = ProcessingType.REGULAR_SIMULATION;

    setCommonOptions(dummyProcessingType);

    final String dummyReason = null;
    expect(((ModelAsInactiveStrategy) mockStrategy).modelAsInactive(isA(AssayVO.class),
                                                                    isA(IndividualDataRecord.class),
                                                                    isA(ICData.class),
                                                                    isA(String.class)))
          .andReturn(dummyReason);

    final Capture<IndividualProvenance> captureProvenance = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(captureProvenance));

    replayAll();
    mocksControl.replay();

    workflowProcessor.processWorkflow(dummyCompoundIdentifier, dummyProcessingType,
                                      dummySimulationId, mockSimulation,
                                      mockSiteDataHolder);

    verifyAll();
    mocksControl.verify();

    final IndividualProvenance capturedProvenance = (IndividualProvenance) captureProvenance.getValue();
    assertSame(InformationLevel.TRACE, capturedProvenance.getLevel());
    assertTrue(capturedProvenance.getText().contains("No input value for : "));
    assertSame(dummyIndividualDataRecordId, capturedProvenance.getIndividualDataId());
  }

}