/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.NoSuchDataException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ICHillVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;

/**
 * Unit test C50 from C20 strategy.
 *
 * @author geoff
 */
public class C50NonEqualityC20EqualityTest {

  private static final boolean dummyDefaultActive = true;
  private static final int dummyDefaultInvocationOrder = 0;
  private static final String dummyDescription = "dummyDescription";
  private static final String dummyIdentifyingPrefix = "dummyIdentifyingPrefix";

  private C50NonEqualityC20Equality strategy;
  private ICData mockICData;
  private IMocksControl mocksControl;
  private IndividualDataRecord mockIndividualData;

  @Before
  public  void setUp() {
    strategy = new C50NonEqualityC20Equality(dummyDescription, dummyDefaultActive,
                                             dummyDefaultInvocationOrder);
    mocksControl = createStrictControl();
    mockICData = mocksControl.createMock(ICData.class);
    mockIndividualData = mocksControl.createMock(IndividualDataRecord.class);
  }

  @Test
  public void testInsufficientDataReturnsNull() throws InvalidValueException, NoSuchDataException {
    // 1. No IC50 data.
    expect(mockICData.hasICPctData(ICPercent.PCT50)).andReturn(false);

    mocksControl.replay();

    ICHillVO icHillVO = strategy.deriveFromC20(mockIndividualData, mockICData, dummyIdentifyingPrefix);

    mocksControl.verify();

    assertNull(icHillVO);

    mocksControl.reset();

    // 2. IC50 data, but no IC20 data.
    expect(mockICData.hasICPctData(ICPercent.PCT50)).andReturn(true);
    expect(mockICData.hasICPctData(ICPercent.PCT20)).andReturn(false);

    mocksControl.replay();

    icHillVO = strategy.deriveFromC20(mockIndividualData, mockICData, dummyIdentifyingPrefix);

    mocksControl.verify();

    assertNull(icHillVO);

    mocksControl.reset();

    // 3. Both inh. conc. IC data, but 50% inh.conc has an equality modifier
    expect(mockICData.hasICPctData(ICPercent.PCT50)).andReturn(true);
    expect(mockICData.hasICPctData(ICPercent.PCT20)).andReturn(true);
    expect(mockICData.isModifierEquals(ICPercent.PCT50)).andReturn(true);

    mocksControl.replay();

    icHillVO = strategy.deriveFromC20(mockIndividualData, mockICData, dummyIdentifyingPrefix);

    mocksControl.verify();

    assertNull(icHillVO);

    mocksControl.reset();

    // 3. Both inh. conc. IC data, and both have non-equality modifier.
    expect(mockICData.hasICPctData(ICPercent.PCT50)).andReturn(true);
    expect(mockICData.hasICPctData(ICPercent.PCT20)).andReturn(true);
    expect(mockICData.isModifierEquals(ICPercent.PCT50)).andReturn(false);
    expect(mockICData.isModifierEquals(ICPercent.PCT20)).andReturn(false);

    mocksControl.replay();

    icHillVO = strategy.deriveFromC20(mockIndividualData, mockICData, dummyIdentifyingPrefix);

    mocksControl.verify();

    assertNull(icHillVO);
  }

  @Test
  public void testSufficientData() throws NoSuchDataException, InvalidValueException {
    // 1. No Hill Coefficient available (so default used), pIC50 not available.
    expect(mockICData.hasICPctData(ICPercent.PCT50)).andReturn(true);
    expect(mockICData.hasICPctData(ICPercent.PCT20)).andReturn(true);
    expect(mockICData.isModifierEquals(ICPercent.PCT50)).andReturn(false);
    expect(mockICData.isModifierEquals(ICPercent.PCT20)).andReturn(true);

    BigDecimal dummyEligibleHillCoefficient = null;
    expect(mockIndividualData.eligibleHillCoefficient()).andReturn(dummyEligibleHillCoefficient);
    BigDecimal dummyPIC50 = null;
    expect(mockICData.retrievePC50BasedOnIC20(C50NonEqualityC20Equality.defaultHillCoefficient)).
           andReturn(dummyPIC50);

    mocksControl.replay();

    ICHillVO icHillVO = strategy.deriveFromC20(mockIndividualData, mockICData, dummyIdentifyingPrefix);

    mocksControl.verify();

    assertNull(icHillVO);

    mocksControl.reset();

    // 2. No Hill Coefficient available so default used), pIC50 available.
    expect(mockICData.hasICPctData(ICPercent.PCT50)).andReturn(true);
    expect(mockICData.hasICPctData(ICPercent.PCT20)).andReturn(true);
    expect(mockICData.isModifierEquals(ICPercent.PCT50)).andReturn(false);
    expect(mockICData.isModifierEquals(ICPercent.PCT20)).andReturn(true);

    expect(mockIndividualData.eligibleHillCoefficient()).andReturn(dummyEligibleHillCoefficient);
    dummyPIC50 = BigDecimal.TEN;
    expect(mockICData.retrievePC50BasedOnIC20(C50NonEqualityC20Equality.defaultHillCoefficient)).
           andReturn(dummyPIC50);

    mocksControl.replay();

    icHillVO = strategy.deriveFromC20(mockIndividualData, mockICData, dummyIdentifyingPrefix);

    mocksControl.verify();

    assertTrue(dummyPIC50.compareTo(icHillVO.getIcValue()) == 0);
    assertTrue(C50NonEqualityC20Equality.defaultHillCoefficient.compareTo(icHillVO.getHillValue()) == 0);

    mocksControl.reset();

    // 3. Hill Coefficient available.
    expect(mockICData.hasICPctData(ICPercent.PCT50)).andReturn(true);
    expect(mockICData.hasICPctData(ICPercent.PCT20)).andReturn(true);
    expect(mockICData.isModifierEquals(ICPercent.PCT50)).andReturn(false);
    expect(mockICData.isModifierEquals(ICPercent.PCT20)).andReturn(true);

    dummyEligibleHillCoefficient = BigDecimal.TEN;
    expect(mockIndividualData.eligibleHillCoefficient()).andReturn(dummyEligibleHillCoefficient);
    dummyPIC50 = BigDecimal.ONE;
    expect(mockICData.retrievePC50BasedOnIC20(dummyEligibleHillCoefficient)).andReturn(dummyPIC50);

    mocksControl.replay();

    icHillVO = strategy.deriveFromC20(mockIndividualData, mockICData, dummyIdentifyingPrefix);

    mocksControl.verify();

    assertTrue(dummyPIC50.compareTo(icHillVO.getIcValue()) == 0);
    assertTrue(dummyEligibleHillCoefficient.compareTo(icHillVO.getHillValue()) == 0);
  }

  @Test
  public void testUsesEqualityModifier() {
    assertTrue(strategy.usesEqualityModifier());
  }
}