/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.config;

import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import uk.ac.ox.cs.epsrc.business_manager.config.ConfigurationActionPotential;
import uk.ac.ox.cs.epsrc.business_manager.value.object.CellModelVO;

/**
 * Unit test the action potential configuration.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { Collections.class, ConfigurationActionPotential.class })
public class ConfigurationActionPotentialTest {

  private static final BigDecimal illegalMaxPctileVal = new BigDecimal("100");

  private ConfigurationActionPotential configurationActionPotential;
  private IMocksControl mocksControl;
  private BigDecimal dummyDefaultPacingMaxTime;
  private short dummyDefaultPlasmaConcCount;
  private boolean dummyDefaultPlasmaConcLogScale;

  @Before
  public void setUp() {
    dummyDefaultPacingMaxTime = null;
    dummyDefaultPlasmaConcCount = 7;
    dummyDefaultPlasmaConcLogScale = true;
  }

  @Test
  public void testConstructor() {
    configurationActionPotential = new ConfigurationActionPotential(dummyDefaultPacingMaxTime,
                                                                    dummyDefaultPlasmaConcCount,
                                                                    dummyDefaultPlasmaConcLogScale);
    assertNull(configurationActionPotential.getDefaultPacingMaxTime());
    assertTrue(configurationActionPotential.getCellModels().isEmpty());
    assertTrue(configurationActionPotential.getDefaultPerFrequencyConcentrations()
                                           .isEmpty());
    try {
      configurationActionPotential.getDefaultModelIdentifier();
      fail("Should not be able to return the default model identifier if no CellML models assigned.");
    } catch (IllegalStateException e) {}
    assertEquals(dummyDefaultPlasmaConcCount,
                 configurationActionPotential.getDefaultPlasmaConcCount());
    assertTrue(configurationActionPotential.isDefaultPlasmaConcLogScale());
    assertTrue(configurationActionPotential.toString()
               .contains(String.valueOf(dummyDefaultPlasmaConcCount)));

    dummyDefaultPacingMaxTime = BigDecimal.ZERO;
    try {
      new ConfigurationActionPotential(dummyDefaultPacingMaxTime,
                                       dummyDefaultPlasmaConcCount,
                                       dummyDefaultPlasmaConcLogScale);
      fail("Should not permit 0 value for default maximum pacing time.");
    } catch (IllegalArgumentException e) {}

    dummyDefaultPacingMaxTime = BigDecimal.ZERO.subtract(BigDecimal.ONE);
    try {
      new ConfigurationActionPotential(dummyDefaultPacingMaxTime,
                                       dummyDefaultPlasmaConcCount,
                                       dummyDefaultPlasmaConcLogScale);
      fail("Should not permit -ve value for default maximum pacing time.");
    } catch (IllegalArgumentException e) {}

    dummyDefaultPacingMaxTime = BigDecimal.ONE;
    configurationActionPotential = new ConfigurationActionPotential(dummyDefaultPacingMaxTime,
                                                                    dummyDefaultPlasmaConcCount,
                                                                    dummyDefaultPlasmaConcLogScale);
    assertTrue(dummyDefaultPacingMaxTime
              .compareTo(configurationActionPotential.getDefaultPacingMaxTime()) == 0);
  }

  @Test
  public void testExperimentalPerFrequencies() {
    configurationActionPotential = new ConfigurationActionPotential(dummyDefaultPacingMaxTime,
                                                                    dummyDefaultPlasmaConcCount,
                                                                    dummyDefaultPlasmaConcLogScale);
    Map<BigDecimal, Set<BigDecimal>> dummyPerFreqConcs = null;
    try {
      configurationActionPotential.setDefaultPerFrequencyConcentrations(dummyPerFreqConcs);
      fail("Should not permit the assignment of per freq concs using a null arg.");
    } catch (IllegalArgumentException e) {}

    dummyPerFreqConcs = new HashMap<BigDecimal, Set<BigDecimal>>();
    try {
      configurationActionPotential.setDefaultPerFrequencyConcentrations(dummyPerFreqConcs);
      fail("Should not permit the assignment of per freq concs using an empty collection arg.");
    } catch (IllegalArgumentException e) {}

    final BigDecimal dummyFrequency = BigDecimal.ONE;
    dummyPerFreqConcs.put(dummyFrequency, null);
    configurationActionPotential.setDefaultPerFrequencyConcentrations(dummyPerFreqConcs);

    assertEquals(1, configurationActionPotential.getDefaultPerFrequencyConcentrations().size());

    final Map<BigDecimal, Set<BigDecimal>> configured = configurationActionPotential.getDefaultPerFrequencyConcentrations();
    try {
      configured.clear();
      fail("Should not permit the modification of configured per-frequency concentrations.");
    } catch (UnsupportedOperationException e) {}
  }

  @Test
  public void testModelHandling1() {
    configurationActionPotential = new ConfigurationActionPotential(dummyDefaultPacingMaxTime,
                                                                    dummyDefaultPlasmaConcCount,
                                                                    dummyDefaultPlasmaConcLogScale);
    Set<CellModelVO> dummyConfiguredCellModels = null;
    try {
      configurationActionPotential.setCellModels(dummyConfiguredCellModels);
      fail("Should not permit assignment of CellML models using a null arg.");
    } catch(IllegalArgumentException e) {}
    // Note: Using a linked hash set so that we can use strict control.
    dummyConfiguredCellModels = new LinkedHashSet<CellModelVO>();
    try {
      configurationActionPotential.setCellModels(dummyConfiguredCellModels);
      fail("Should not permit assignment of CellML models using an empty collection.");
    } catch (IllegalArgumentException e) {}

    dummyConfiguredCellModels.add(null);
    try {
      configurationActionPotential.setCellModels(dummyConfiguredCellModels);
      fail("Should ignore a null value within the CellML collection.");
    } catch (IllegalArgumentException e) {}

    dummyConfiguredCellModels.clear();

    mocksControl = createStrictControl();

    final CellModelVO mockCellModelVO1 = mocksControl.createMock(CellModelVO.class);
    dummyConfiguredCellModels.add(mockCellModelVO1);

    final short dummyCellModelVO1Identifier = 4;
    expect(mockCellModelVO1.getIdentifier()).andReturn(dummyCellModelVO1Identifier);
    boolean dummyCellModelVO1DefaultModel = false; 
    expect(mockCellModelVO1.isDefaultModel()).andReturn(dummyCellModelVO1DefaultModel);
    // Couldn't find the default, so use first available one as default.
    expect(mockCellModelVO1.getIdentifier()).andReturn(dummyCellModelVO1Identifier);

    mocksControl.replay();
    configurationActionPotential.setCellModels(dummyConfiguredCellModels);

    mocksControl.verify();
    assertEquals(dummyCellModelVO1Identifier,
                 configurationActionPotential.getDefaultModelIdentifier());
    assertEquals(dummyConfiguredCellModels.size(),
                 configurationActionPotential.getCellModels().size());

    try {
      configurationActionPotential.getCellModels().clear();
      fail("Should not permit the modification of CellML models once assigned.");
    } catch (UnsupportedOperationException e) {}

    mocksControl.reset();
  }

  @Test
  public void testModelHandling2() {
    configurationActionPotential = new ConfigurationActionPotential(dummyDefaultPacingMaxTime,
                                                                    dummyDefaultPlasmaConcCount,
                                                                    dummyDefaultPlasmaConcLogScale);
    final Set<CellModelVO> dummyConfiguredCellModels = new HashSet<CellModelVO>();
    mocksControl = createControl();

    final CellModelVO mockCellModelVO1 = mocksControl.createMock(CellModelVO.class);
    final CellModelVO mockCellModelVO2 = mocksControl.createMock(CellModelVO.class);
    dummyConfiguredCellModels.add(mockCellModelVO1);
    dummyConfiguredCellModels.add(mockCellModelVO2);

    final short dummyCellModelVO1Identifier = 4;
    boolean dummyCellModelVO1DefaultModel = false;
    boolean dummyCellModelVO2DefaultModel = true;

    // Try with 2 CellML models, one is the default, but duplicated model identifiers.
    expect(mockCellModelVO1.getIdentifier())
          .andReturn(dummyCellModelVO1Identifier).atLeastOnce();
    expect(mockCellModelVO1.isDefaultModel())
          .andReturn(dummyCellModelVO1DefaultModel).anyTimes();
    // Note: Reusing model 1's identifier.
    expect(mockCellModelVO2.getIdentifier())
          .andReturn(dummyCellModelVO1Identifier).atLeastOnce();
    expect(mockCellModelVO2.isDefaultModel())
          .andReturn(dummyCellModelVO2DefaultModel).anyTimes();

    mocksControl.replay();
    try {
      configurationActionPotential.setCellModels(dummyConfiguredCellModels);
      fail("Should not permit duplication of model identifiers.");
    } catch (IllegalArgumentException e) {}

    mocksControl.verify();

    mocksControl.reset();

    // Using the 2 CellML models, both are defaults(!), but different model identifiers.
    expect(mockCellModelVO1.getIdentifier())
          .andReturn(dummyCellModelVO1Identifier).atLeastOnce();
    dummyCellModelVO1DefaultModel = true;
    expect(mockCellModelVO1.isDefaultModel())
          .andReturn(dummyCellModelVO1DefaultModel).anyTimes();
    final short dummyCellModelVO2Identifier = 1;
    expect(mockCellModelVO2.getIdentifier())
          .andReturn(dummyCellModelVO2Identifier).atLeastOnce();
    expect(mockCellModelVO2.isDefaultModel())
          .andReturn(dummyCellModelVO2DefaultModel).anyTimes();

    // Emulating Collections.sort().... urgh! Need to find a way of mocking the Collections.sort()
    expect(mockCellModelVO1.compareTo(isA(CellModelVO.class)))
          .andReturn(1).atLeastOnce();
    expect(mockCellModelVO2.compareTo(isA(CellModelVO.class)))
          .andReturn(1).atLeastOnce();

    mocksControl.replay();
    configurationActionPotential.setCellModels(dummyConfiguredCellModels);

    mocksControl.verify();
  }

  @Test
  public void testSetDefaultCredibleIntervalPctiles() {
    configurationActionPotential = new ConfigurationActionPotential(dummyDefaultPacingMaxTime,
                                                                    dummyDefaultPlasmaConcCount,
                                                                    dummyDefaultPlasmaConcLogScale);
    assertTrue(configurationActionPotential.getDefaultCredibleIntervalPctiles().isEmpty());

    /*
     * 1. Fail on null/empty arg.
     */
    try {
      configurationActionPotential.setDefaultCredibleIntervalPctiles(null);
      fail("Should not permit null arg!");
    } catch (IllegalArgumentException e) {}

    Set<BigDecimal> dummyDefaultCIPctiles = new HashSet<BigDecimal>();
    try {
      configurationActionPotential.setDefaultCredibleIntervalPctiles(dummyDefaultCIPctiles);
      fail("Should not permit empty collection arg!");
    } catch (IllegalArgumentException e) {}

    /*
     * 2. Test value validation.
     */
    dummyDefaultCIPctiles.add(null);
    try {
      configurationActionPotential.setDefaultCredibleIntervalPctiles(dummyDefaultCIPctiles);
      fail("Should not permit null arg in collection!");
    } catch (IllegalArgumentException e) {
      assertTrue(e.getMessage().contains("is not valid"));
    }

    dummyDefaultCIPctiles.clear();
    dummyDefaultCIPctiles.add(BigDecimal.ZERO.subtract(BigDecimal.ONE));

    try {
      configurationActionPotential.setDefaultCredibleIntervalPctiles(dummyDefaultCIPctiles);
      fail("Should not permit -ve arg in collection!");
    } catch (IllegalArgumentException e) {
      assertTrue(e.getMessage().contains("is not valid"));
    }

    dummyDefaultCIPctiles.clear();
    dummyDefaultCIPctiles.add(BigDecimal.ZERO);

    try {
      configurationActionPotential.setDefaultCredibleIntervalPctiles(dummyDefaultCIPctiles);
      fail("Should not permit 0 arg in collection!");
    } catch (IllegalArgumentException e) {
      assertTrue(e.getMessage().contains("is not valid"));
    }

    dummyDefaultCIPctiles.clear();
    dummyDefaultCIPctiles.add(illegalMaxPctileVal);

    try {
      configurationActionPotential.setDefaultCredibleIntervalPctiles(dummyDefaultCIPctiles);
      fail("Should not permit illegal max pctile value arg in collection!");
    } catch (IllegalArgumentException e) {
      assertTrue(e.getMessage().contains("is not valid"));
    }

    dummyDefaultCIPctiles.clear();
    dummyDefaultCIPctiles.add(illegalMaxPctileVal.add(BigDecimal.ONE));

    try {
      configurationActionPotential.setDefaultCredibleIntervalPctiles(dummyDefaultCIPctiles);
      fail("Should not permit value exceeding illegal max pctile value arg in collection!");
    } catch (IllegalArgumentException e) {
      assertTrue(e.getMessage().contains("is not valid"));
    }

    dummyDefaultCIPctiles.clear();
    final BigDecimal dummyValidPctile1 = illegalMaxPctileVal.subtract(BigDecimal.ONE);
    dummyDefaultCIPctiles.add(dummyValidPctile1);

    configurationActionPotential.setDefaultCredibleIntervalPctiles(dummyDefaultCIPctiles);

    Set<BigDecimal> defaultCIPctiles = configurationActionPotential.getDefaultCredibleIntervalPctiles();
    assertSame(1, defaultCIPctiles.size());
    assertTrue(defaultCIPctiles.contains(dummyValidPctile1));

    try {
      defaultCIPctiles.clear();
      fail("Should not permit modification of default credible interval pctiles collection!");
    } catch (UnsupportedOperationException e) {}

    dummyDefaultCIPctiles.add(dummyValidPctile1);

    configurationActionPotential.setDefaultCredibleIntervalPctiles(dummyDefaultCIPctiles);

    defaultCIPctiles = configurationActionPotential.getDefaultCredibleIntervalPctiles();
    assertSame(1, defaultCIPctiles.size());
    assertTrue(defaultCIPctiles.contains(dummyValidPctile1));

    final BigDecimal dummyValidPctile2 = BigDecimal.TEN;
    dummyDefaultCIPctiles.add(dummyValidPctile2);

    configurationActionPotential.setDefaultCredibleIntervalPctiles(dummyDefaultCIPctiles);

    defaultCIPctiles = configurationActionPotential.getDefaultCredibleIntervalPctiles();
    assertSame(2, defaultCIPctiles.size());
    assertTrue(defaultCIPctiles.contains(dummyValidPctile1));
    assertTrue(defaultCIPctiles.contains(dummyValidPctile2));
  }
}