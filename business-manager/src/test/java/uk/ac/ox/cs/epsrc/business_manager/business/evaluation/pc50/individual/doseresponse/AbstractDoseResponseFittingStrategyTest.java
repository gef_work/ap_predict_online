/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual.doseresponse;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Test;
import org.springframework.core.Ordered;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual.doseresponse.AbstractDoseResponseFittingStrategy;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.Provenance;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.DoseResponseData;
import uk.ac.ox.cs.epsrc.business_manager.ws.FDRServicesProxy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.doseresponse.FittingStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.NoSuchDataException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingParams;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingResult;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.IndividualProcessing;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;

/**
 * Unit test the abstract dose-response fitting strategy.
 *
 * @author geoff
 */
public class AbstractDoseResponseFittingStrategyTest {

  private final long dummySimulationId = 1l;

  private class TestDRFittingStrategyNoDoseResponseData extends AbstractDoseResponseFittingStrategy {
    private static final long serialVersionUID = 1L;

    public TestDRFittingStrategyNoDoseResponseData(final String description,
                                                   final boolean defaultActive,
                                                   final int defaultInvocationOrder) {
      super(description, defaultActive, defaultInvocationOrder);
    }

    @Override
    protected DoseResponseData retrieveDoseResponseData(final long simulationId,
                                                        final IndividualProcessing individualProcessing,
                                                        final ICData individualC50Data,
                                                        final String recordIdentifier,
                                                        final DoseResponseFittingParams doseResponseFittingParams,
                                                        final String individualDataId,
                                                        final boolean provenanceable)
                                                        throws InvalidValueException {
      return null;
    }

    @Override
    protected String retrieveFittingNature() { return "dummyFittingNature"; }
    @Override
    protected String retrieveSysProvenanceSuffix() { return "dummySysProvenanceSuffix"; }
  }

  private class TestDRFittingStrategyDoseResponseData extends AbstractDoseResponseFittingStrategy {
    private static final long serialVersionUID = 1L;

    public TestDRFittingStrategyDoseResponseData(final String description,
                                                 final boolean defaultActive,
                                                 final int defaultInvocationOrder) {
      super(description, defaultActive, defaultInvocationOrder);
    }

    @Override
    protected DoseResponseData retrieveDoseResponseData(final long simulationId,
                                                        final IndividualProcessing individualProcessing,
                                                        final ICData individualC50Data,
                                                        final String recordIdentifier,
                                                        final DoseResponseFittingParams doseResponseFittingParams,
                                                        final String individualDataId,
                                                        final boolean provenanceable)
                                                        throws InvalidValueException {
      return createStrictControl().createMock(DoseResponseData.class);
    }

    @Override
    protected String retrieveFittingNature() { return "dummyFittingNature"; }
    @Override
    protected String retrieveSysProvenanceSuffix() { return "dummySysProvenanceSuffix"; }
  }

  @Test
  public void testConstructor() {
    String dummyDescription = null;
    TestDRFittingStrategyNoDoseResponseData testDRFSNoDRData = null;
    try {
      new TestDRFittingStrategyNoDoseResponseData(dummyDescription, true, 0);
      fail("Should not permit null description in constructor.");
    } catch (IllegalArgumentException e) {}

    dummyDescription = "dummyDescription";
    boolean dummyDefaultActive = true;
    int dummyOrder = 283;
    testDRFSNoDRData = new TestDRFittingStrategyNoDoseResponseData(dummyDescription,
                                                                   dummyDefaultActive,
                                                                   dummyOrder);
    assertTrue(testDRFSNoDRData instanceof FittingStrategy);
    assertTrue(testDRFSNoDRData instanceof PC50EvaluationStrategy);
    assertTrue(testDRFSNoDRData instanceof Ordered);
    assertNull(testDRFSNoDRData.getFdrServicesProxy());
    assertNull(testDRFSNoDRData.getJmsTemplate());
    assertTrue(testDRFSNoDRData.getDefaultActive());
    assertTrue(dummyDescription.equals(testDRFSNoDRData.getDescription()));
    assertTrue(dummyOrder == testDRFSNoDRData.getOrder());
    assertNotNull(testDRFSNoDRData.getSysProvenanceStrategyIdentifier());
    assertFalse(testDRFSNoDRData.usesEqualityModifier());
  }

  @Test
  public void testInputVerification() throws InvalidValueException {
    final String dummyDescription = "dummyDescription";
    boolean dummyDefaultActive = true;
    final TestDRFittingStrategyNoDoseResponseData testDRFSNoDRData = new TestDRFittingStrategyNoDoseResponseData(dummyDescription,
                                                                                                                 dummyDefaultActive, 0);
    boolean dummyInputDataGathering = false;
    final ICPercent dummyPctIC = ICPercent.PCT50;
    try {
      testDRFSNoDRData.fitDoseResponseData(dummySimulationId, dummyInputDataGathering, null, null,
                                           null, null, dummyPctIC);
      fail("Should not permit a null object for individual processing.");
    } catch (IllegalArgumentException e) {}

    final IMocksControl mocksControl = createControl();
    final IndividualProcessing mockIndividualProcessing = mocksControl.createMock(IndividualProcessing.class); 

    try {
      testDRFSNoDRData.fitDoseResponseData(dummySimulationId, dummyInputDataGathering,
                                           mockIndividualProcessing, null, null, null, dummyPctIC);
      fail("Should not permit a null object for individual C50 data.");
    } catch (IllegalArgumentException e) {}

    final ICData mockIndividualC50Data = mocksControl.createMock(ICData.class);

    try {
      testDRFSNoDRData.fitDoseResponseData(dummySimulationId, dummyInputDataGathering,
                                           mockIndividualProcessing, mockIndividualC50Data, null,
                                           null, dummyPctIC);
      fail("Should not permit a null object for dose-response fitting params.");
    } catch (IllegalArgumentException e) {}
  }

  @Test
  public void testFitDoseResponseDataWhenNoData() throws IllegalArgumentException, InvalidValueException {
    // No dose-response data
    final String dummyDescription = "dummyDescription";
    boolean dummyDefaultActive = true;
    final TestDRFittingStrategyNoDoseResponseData testDRFSNoDRData = new TestDRFittingStrategyNoDoseResponseData(dummyDescription,
                                                                                                                    dummyDefaultActive, 0);
    boolean dummyInputDataGathering = false;
    final String dummyRecordIdentifier = "dummyRecordIdentifier";
    final ICPercent dummyPctIC = ICPercent.PCT50;

    final IMocksControl mocksControl = createStrictControl();
    final IndividualProcessing mockIndividualProcessing = mocksControl.createMock(IndividualProcessing.class);
    final ICData mockC50Data = mocksControl.createMock(ICData.class);
    final DoseResponseFittingParams mockDoseResponseFittingParams = mocksControl.createMock(DoseResponseFittingParams.class);

    expect(mockC50Data.hasICPctData(dummyPctIC)).andReturn(true);
    IndividualDataRecord mockIndividualDataRecord = mocksControl.createMock(IndividualDataRecord.class);
    expect(mockIndividualProcessing.getIndividualData()).andReturn(mockIndividualDataRecord);
    expect(mockIndividualDataRecord.getId()).andReturn(dummyRecordIdentifier);

    mocksControl.replay();

    DoseResponseFittingResult result = testDRFSNoDRData.fitDoseResponseData(dummySimulationId,
                                                                            dummyInputDataGathering,
                                                                            mockIndividualProcessing,
                                                                            mockC50Data,
                                                                            dummyRecordIdentifier,
                                                                            mockDoseResponseFittingParams,
                                                                            dummyPctIC);
    mocksControl.verify();
    assertNull(result);

    mocksControl.reset();
  }

  @Test
  public void testFitDoseResponseDataWhenDataButWSFail() throws IllegalArgumentException, InvalidValueException {
    // With dose-response data 
    final String dummyDescription = "dummyDescription";
    boolean dummyDefaultActive = true;
    final TestDRFittingStrategyDoseResponseData testDRFSDRData = new TestDRFittingStrategyDoseResponseData(dummyDescription, 
                                                                                                              dummyDefaultActive, 0);
    final ICPercent dummyPctIC = ICPercent.PCT50;

    final IMocksControl mocksControl = createStrictControl();
    final BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    final FDRServicesProxy mockFDRServicesProxy = mocksControl.createMock(FDRServicesProxy.class);
    ReflectionTestUtils.setField(testDRFSDRData, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
    ReflectionTestUtils.setField(testDRFSDRData, BusinessIdentifiers.COMPONENT_FDR_SERVICES_PROXY,
                                 mockFDRServicesProxy);
    boolean dummyInputDataGathering = true;
    final String dummyRecordIdentifier = "dummyRecordIdentifier";
    final IndividualProcessing mockIndividualProcessing = mocksControl.createMock(IndividualProcessing.class);
    final ICData mockC50Data = mocksControl.createMock(ICData.class);
    final DoseResponseFittingParams mockDoseResponseFittingParams = mocksControl.createMock(DoseResponseFittingParams.class);

    expect(mockC50Data.hasICPctData(dummyPctIC)).andReturn(true);
    IndividualDataRecord mockIndividualDataRecord = mocksControl.createMock(IndividualDataRecord.class);
    expect(mockIndividualProcessing.getIndividualData()).andReturn(mockIndividualDataRecord);
    expect(mockIndividualDataRecord.getId()).andReturn(dummyRecordIdentifier);

    // We're expecting the retrieveDoseResponseData to return a mock object, so use isA
    expect(mockFDRServicesProxy.callFDRWebService(isA(DoseResponseData.class), isA(DoseResponseFittingParams.class)))
                               .andReturn(null);

    mocksControl.replay();

    DoseResponseFittingResult result = testDRFSDRData.fitDoseResponseData(dummySimulationId,
                                                                          dummyInputDataGathering,
                                                                          mockIndividualProcessing,
                                                                          mockC50Data,
                                                                          dummyRecordIdentifier,
                                                                          mockDoseResponseFittingParams,
                                                                          dummyPctIC);
    mocksControl.verify();
    assertNull(result);
  }

  @Test
  public void testProvenanceableCallWSOnWSFail() {
    final String dummyDescription = "dummyDescription";
    boolean dummyDefaultActive = true;
    final TestDRFittingStrategyDoseResponseData testDRFSDRData = new TestDRFittingStrategyDoseResponseData(dummyDescription, 
                                                                                                              dummyDefaultActive, 0);
    final boolean dummyProvenanceable = true;
    final String dummyIndividualDataId = "dummyIndividualDataId";
    final ICPercent dummyPctIC = ICPercent.PCT50;

    final IMocksControl mocksControl = createStrictControl();
    final ICData mockC50Data = mocksControl.createMock(ICData.class);
    final DoseResponseData mockDoseResponseData = mocksControl.createMock(DoseResponseData.class);
    final DoseResponseFittingParams mockDoseResponseFittingParams = mocksControl.createMock(DoseResponseFittingParams.class);
    final BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    final FDRServicesProxy mockFDRServicesProxy = mocksControl.createMock(FDRServicesProxy.class);
    ReflectionTestUtils.setField(testDRFSDRData, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
    ReflectionTestUtils.setField(testDRFSDRData, BusinessIdentifiers.COMPONENT_FDR_SERVICES_PROXY,
                                 mockFDRServicesProxy);

    final Capture<Provenance> dummyProvenance1 = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(dummyProvenance1));

    expect(mockFDRServicesProxy.callFDRWebService(mockDoseResponseData, mockDoseResponseFittingParams))
                               .andReturn(null);

    final Capture<Provenance> dummyProvenance2 = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(dummyProvenance2));

    mocksControl.replay();

    final DoseResponseFittingResult result = testDRFSDRData.callWS(dummySimulationId,
                                                                   dummyProvenanceable,
                                                                   dummyIndividualDataId, null,
                                                                   mockC50Data,
                                                                   mockDoseResponseData,
                                                                   mockDoseResponseFittingParams,
                                                                   dummyPctIC);
    mocksControl.verify();

    assertNull(result);
  }

  @Test
  public void testNonProvenanceableCallWSOnWSFail() {
    final String dummyDescription = "dummyDescription";
    boolean dummyDefaultActive = true;
    final TestDRFittingStrategyDoseResponseData testDRFSDRData = new TestDRFittingStrategyDoseResponseData(dummyDescription, 
                                                                                                              dummyDefaultActive, 0);
    final boolean dummyProvenanceable = false;
    final String dummyIndividualDataId = "dummyIndividualDataId";
    final ICPercent dummyPctIC = ICPercent.PCT50;

    final IMocksControl mocksControl = createStrictControl();
    final ICData mockC50Data = mocksControl.createMock(ICData.class);
    final DoseResponseData mockDoseResponseData = mocksControl.createMock(DoseResponseData.class);
    final DoseResponseFittingParams mockDoseResponseFittingParams = mocksControl.createMock(DoseResponseFittingParams.class);
    final BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    final FDRServicesProxy mockFDRServicesProxy = mocksControl.createMock(FDRServicesProxy.class);
    ReflectionTestUtils.setField(testDRFSDRData, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
    ReflectionTestUtils.setField(testDRFSDRData, BusinessIdentifiers.COMPONENT_FDR_SERVICES_PROXY,
                                 mockFDRServicesProxy);

    expect(mockFDRServicesProxy.callFDRWebService(mockDoseResponseData, mockDoseResponseFittingParams))
                               .andReturn(null);

    mocksControl.replay();

    final DoseResponseFittingResult result = testDRFSDRData.callWS(dummySimulationId,
                                                                   dummyProvenanceable,
                                                                   dummyIndividualDataId, null,
                                                                   mockC50Data,
                                                                   mockDoseResponseData,
                                                                   mockDoseResponseFittingParams,
                                                                   dummyPctIC);
    mocksControl.verify();

    assertNull(result);
  }

  @Test
  public void testProvenanceableCallWSOnWSSuccess() throws NoSuchDataException {
    /*
     * Test 1: Response from WS is a potentially valid IC50 (according to C50 data vals).
     */
    final String dummyDescription = "dummyDescription";
    boolean dummyDefaultActive = true;
    final TestDRFittingStrategyDoseResponseData testDRFSDRData = new TestDRFittingStrategyDoseResponseData(dummyDescription, 
                                                                                                              dummyDefaultActive, 0);
    final boolean dummyProvenanceable = true;
    final String dummyIndividualDataId = "dummyIndividualDataId";
    final ICPercent dummyPctIC = ICPercent.PCT50;

    final IMocksControl mocksControl = createStrictControl();
    final ICData mockC50Data = mocksControl.createMock(ICData.class);
    final DoseResponseData mockDoseResponseData = mocksControl.createMock(DoseResponseData.class);
    final DoseResponseFittingParams mockDoseResponseFittingParams = mocksControl.createMock(DoseResponseFittingParams.class);
    final BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    final FDRServicesProxy mockFDRServicesProxy = mocksControl.createMock(FDRServicesProxy.class);
    ReflectionTestUtils.setField(testDRFSDRData, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
    ReflectionTestUtils.setField(testDRFSDRData, BusinessIdentifiers.COMPONENT_FDR_SERVICES_PROXY,
                                 mockFDRServicesProxy);

    final Capture<Provenance> dummyProvenance1 = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(dummyProvenance1));

    final DoseResponseFittingResult mockDoseResponseFittingResult = mocksControl.createMock(DoseResponseFittingResult.class);

    expect(mockFDRServicesProxy.callFDRWebService(mockDoseResponseData, mockDoseResponseFittingParams))
                               .andReturn(mockDoseResponseFittingResult);

    final String dummyPC50ValueShortened = "dummyPC50ValueShortened";
    expect(mockDoseResponseFittingResult.getPC50ValueShortened()).andReturn(dummyPC50ValueShortened);
    final BigDecimal dummyFittedIC50 = BigDecimal.ONE;
    expect(mockDoseResponseFittingResult.getIC50()).andReturn(dummyFittedIC50);
    expect(mockC50Data.isPotentialDose(dummyPctIC, dummyFittedIC50)).andReturn(true);

    final Capture<Provenance> dummyProvenance2 = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(dummyProvenance2));

    replayAll();
    mocksControl.replay();

    DoseResponseFittingResult result = testDRFSDRData.callWS(dummySimulationId,
                                                             dummyProvenanceable,
                                                             dummyIndividualDataId, null,
                                                             mockC50Data, mockDoseResponseData,
                                                             mockDoseResponseFittingParams,
                                                             dummyPctIC);
    verifyAll();
    mocksControl.verify();

    assertNotNull(result);

    resetAll();
    mocksControl.reset();

    final Capture<Provenance> dummyProvenance3 = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(dummyProvenance3));

    /*
     * Now try it when saying the response isn't a potential dose!
     */
    expect(mockFDRServicesProxy.callFDRWebService(mockDoseResponseData, mockDoseResponseFittingParams))
          .andReturn(mockDoseResponseFittingResult);
    expect(mockDoseResponseFittingResult.getPC50ValueShortened()).andReturn(dummyPC50ValueShortened);
    expect(mockDoseResponseFittingResult.getIC50()).andReturn(dummyFittedIC50);
    expect(mockC50Data.isPotentialDose(dummyPctIC, dummyFittedIC50)).andReturn(false);

    final Capture<Provenance> dummyProvenance4 = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(dummyProvenance4));

    replayAll();
    mocksControl.replay();

    result = testDRFSDRData.callWS(dummySimulationId, dummyProvenanceable, dummyIndividualDataId,
                                   null, mockC50Data, mockDoseResponseData,
                                   mockDoseResponseFittingParams, dummyPctIC);
    verifyAll();
    mocksControl.verify();

    assertNull(result);
  }

  @Test
  public void testNonProvenanceableCallWSOnWSSuccess() throws NoSuchDataException {
    /*
     * Test 1: Response from WS is a potentially valid IC50 (according to C50 data vals).
     */
    final String dummyDescription = "dummyDescription";
    boolean dummyDefaultActive = true;
    final TestDRFittingStrategyDoseResponseData testDRFSDRData = new TestDRFittingStrategyDoseResponseData(dummyDescription, 
                                                                                                           dummyDefaultActive, 0);
    final boolean dummyProvenanceable = false;
    final String dummyIndividualDataId = "dummyIndividualDataId";
    final ICPercent dummyPctIC = ICPercent.PCT50;

    final IMocksControl mocksControl = createStrictControl();
    final ICData mockC50Data = mocksControl.createMock(ICData.class);
    final DoseResponseData mockDoseResponseData = mocksControl.createMock(DoseResponseData.class);
    final DoseResponseFittingParams mockDoseResponseFittingParams = mocksControl.createMock(DoseResponseFittingParams.class);
    final BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    final FDRServicesProxy mockFDRServicesProxy = mocksControl.createMock(FDRServicesProxy.class);
    ReflectionTestUtils.setField(testDRFSDRData, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
    ReflectionTestUtils.setField(testDRFSDRData, BusinessIdentifiers.COMPONENT_FDR_SERVICES_PROXY,
                                 mockFDRServicesProxy);

    final DoseResponseFittingResult mockDoseResponseFittingResult = mocksControl.createMock(DoseResponseFittingResult.class);

    expect(mockFDRServicesProxy.callFDRWebService(mockDoseResponseData, mockDoseResponseFittingParams))
                               .andReturn(mockDoseResponseFittingResult);

    final String dummyPC50ValueShortened = "dummyPC50ValueShortened";
    expect(mockDoseResponseFittingResult.getPC50ValueShortened()).andReturn(dummyPC50ValueShortened);
    final BigDecimal dummyFittedIC50 = BigDecimal.ONE;
    expect(mockDoseResponseFittingResult.getIC50()).andReturn(dummyFittedIC50);
    expect(mockC50Data.isPotentialDose(dummyPctIC, dummyFittedIC50)).andReturn(true);

    replayAll();
    mocksControl.replay();

    DoseResponseFittingResult result = testDRFSDRData.callWS(dummySimulationId, dummyProvenanceable,
                                                             dummyIndividualDataId, null,
                                                             mockC50Data, mockDoseResponseData,
                                                             mockDoseResponseFittingParams, 
                                                             dummyPctIC);
    verifyAll();
    mocksControl.verify();

    assertNotNull(result);

    resetAll();
    mocksControl.reset();

    /*
     * Now try it when saying the response isn't a potential dose!
     */
    expect(mockFDRServicesProxy.callFDRWebService(mockDoseResponseData, mockDoseResponseFittingParams))
          .andReturn(mockDoseResponseFittingResult);
    expect(mockDoseResponseFittingResult.getPC50ValueShortened()).andReturn(dummyPC50ValueShortened);
    expect(mockDoseResponseFittingResult.getIC50()).andReturn(dummyFittedIC50);
    expect(mockC50Data.isPotentialDose(dummyPctIC, dummyFittedIC50)).andReturn(false);

    replayAll();
    mocksControl.replay();

    result = testDRFSDRData.callWS(dummySimulationId, dummyProvenanceable, dummyIndividualDataId,
                                   null, mockC50Data, mockDoseResponseData,
                                   mockDoseResponseFittingParams, dummyPctIC);
    verifyAll();
    mocksControl.verify();

    assertNull(result);
  }
}