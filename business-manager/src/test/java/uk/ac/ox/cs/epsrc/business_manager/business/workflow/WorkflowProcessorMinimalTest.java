/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.workflow;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.selection.c50.C50DataSelector;
import uk.ac.ox.cs.epsrc.business_manager.business.workflow.WorkflowProcessor;
import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.service.SimulationService;
import uk.ac.ox.cs.epsrc.business_manager.value.object.workflow.OutcomeVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.util.ConverterUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;

/**
 * Unit test the very basics of the workflow processor.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { ConverterUtil.class, C50DataSelector.class } )
public class WorkflowProcessorMinimalTest {

  private BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate;
  private Configuration mockConfiguration;
  private IMocksControl mocksControl;
  private SimulationService mockSimulationService;
  private WorkflowProcessor workflowProcessor;
  private final Long dummySimulationId = 4L;
  private ProcessingType dummyProcessingType = ProcessingType.INPUT_DATA_GATHERING;
  private final String dummyCompoundIdentifier = "dummyCompoundIdentifier";


  @Before
  public void setUp() {
    workflowProcessor = new WorkflowProcessor();
    mocksControl = createStrictControl();
    mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    mockConfiguration = mocksControl.createMock(Configuration.class);
    mockSimulationService = mocksControl.createMock(SimulationService.class);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_CONFIGURATION,
                                 mockConfiguration);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE,
                                 mockSimulationService);
  }

  @Test(expected=IllegalArgumentException.class)
  public void testProcessingFailsOnNullArg() throws IllegalArgumentException, InvalidValueException {
    final Simulation dummyInputDataGatheringSimulation = null;
    final SiteDataHolder dummySiteDataHolder = null;
    workflowProcessor.processWorkflow(dummyCompoundIdentifier, dummyProcessingType,
                                      dummySimulationId, dummyInputDataGatheringSimulation,
                                      dummySiteDataHolder);
  }

  @Test
  public void testMinimalNoUsefulDataRunThrough() throws IllegalArgumentException, InvalidValueException {
    final SiteDataHolder mockSiteDataHolder = mocksControl.createMock(SiteDataHolder.class);
    final AssayVO mockAssayVO = mocksControl.createMock(AssayVO.class);
    expect(mockSiteDataHolder.getAssay()).andReturn(mockAssayVO);
    final IonChannel dummyIonChannel = IonChannel.hERG;
    expect(mockSiteDataHolder.getIonChannel()).andReturn(dummyIonChannel);
    final String dummyAssayName = "dummyAssayName";
    expect(mockAssayVO.getName()).andReturn(dummyAssayName);
    final SummaryDataRecord mockSummaryDataRecord = null;
    expect(mockSiteDataHolder.getSummaryDataRecord(false)).andReturn(mockSummaryDataRecord);
    final Simulation mockInputDataGatheringSimulation = mocksControl.createMock(Simulation.class);
    final String dummyInvocationOrdersCSV = "";
    expect(mockInputDataGatheringSimulation.getPc50EvaluationStrategies()).andReturn(dummyInvocationOrdersCSV);
    final List<PC50EvaluationStrategy> dummyPC50EvaluationStrategies = new ArrayList<PC50EvaluationStrategy>();
    expect(mockConfiguration.retrieveStrategiesByCSV(dummyInvocationOrdersCSV)).andReturn(dummyPC50EvaluationStrategies);
    final String dummyDoseResponseFittingStrategy = "dummyDoseResponseFittingStrategy";
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingStrategy()).andReturn(dummyDoseResponseFittingStrategy);
    final boolean isDoseResponseFittingRounding = true;
    expect(mockInputDataGatheringSimulation.isDoseResponseFittingRounding()).andReturn(isDoseResponseFittingRounding);
    final Float dummyDoseResponseFittingHillMax = 0.5F;
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingHillMax()).andReturn(dummyDoseResponseFittingHillMax);
    final Float dummyDoseResponseFittingHillMin = 2F;
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingHillMin()).andReturn(dummyDoseResponseFittingHillMin);
    final Map<IndividualDataRecord, List<DoseResponseDataRecord>> mockNonSummaryData = 
          new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();
    expect(mockSiteDataHolder.getNonSummaryDataRecords()).andReturn(mockNonSummaryData);

    replayAll();
    mocksControl.replay();

    OutcomeVO outcome = workflowProcessor.processWorkflow(dummyCompoundIdentifier, dummyProcessingType,
                                                          dummySimulationId,
                                                          mockInputDataGatheringSimulation,
                                                          mockSiteDataHolder);

    verifyAll();
    mocksControl.verify();

    assertNotNull(outcome);
    assertTrue(outcome.getInputValueSources().isEmpty());
  }
}