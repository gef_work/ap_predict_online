/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.dao.JobDAO;
import uk.ac.ox.cs.epsrc.business_manager.entity.GroupedInvocationInput;
import uk.ac.ox.cs.epsrc.business_manager.entity.IonChannelValues;
import uk.ac.ox.cs.epsrc.business_manager.entity.Job;
import uk.ac.ox.cs.epsrc.business_manager.entity.PIC50Data;

/**
 * Job Data Access Object integration testing.
 *
 * @author geoff
 */
@ContextConfiguration(locations = { "classpath:/uk/ac/ox/cs/epsrc/business_manager/dao/appCtx.dao.xml",
                                    "classpath:/META-INF/spring/ctx/data/business_manager/appCtx.database.business-manager.xml" })
@ActiveProfiles(profiles="business_manager_embedded")
public class JobDAOIT extends AbstractTransactionalJUnit4SpringContextTests {

  @Autowired(required=true)
  @Qualifier(BusinessIdentifiers.COMPONENT_JOB_DAO)
  private JobDAO jobDAO;

  /**
   * Test <code>Job</code> store.
   */
  @Test
  public void testJobCreation() {
    final Long simulationId = 1L;
    final Float pacingFrequency = 0.5F;
    final Set<Float> compoundConcentrations = new TreeSet<Float>();
    compoundConcentrations.add(0.0F);
    compoundConcentrations.add(0.001F);
    compoundConcentrations.add(0.003F);
    compoundConcentrations.add(0.01F);
    compoundConcentrations.add(0.03F);
    compoundConcentrations.add(0.1F);
    compoundConcentrations.add(0.3F);
    compoundConcentrations.add(1.0F);
    compoundConcentrations.add(3.0F);

    final Short groupingLevel_1 = 1;
    final String groupingName_1 = "Iwks/FLIPR/Barracuda";
    final String ionChannelName_1 = "NaV1_5";
    final String sourceAssayName_1 = "Barracuda";
    final List<PIC50Data> allPIC50Data_1 = new ArrayList<PIC50Data>();
    final BigDecimal value_1_1 = new BigDecimal("4");
    final BigDecimal hill_1_1 = null;
    final Integer strategyOrder_1_1 = 11;
    final PIC50Data pIC50Data_1 = new PIC50Data(value_1_1, hill_1_1, false, strategyOrder_1_1);
    allPIC50Data_1.add(pIC50Data_1);

    final String ionChannelName_2 = "hERG";
    final String sourceAssayName_2 = "Quattro_FLIPR";
    final BigDecimal value_2_1 = new BigDecimal("4.3");
    final BigDecimal hill_2_1 = null;
    final BigDecimal value_2_2 = new BigDecimal("4.3");
    final BigDecimal hill_2_2 = null;
    final Integer strategyOrder_2_1 = 21;
    final Integer strategyOrder_2_2 = 22;
    final PIC50Data pIC50Data_2_1 = new PIC50Data(value_2_1, hill_2_1, false, strategyOrder_2_1);
    final PIC50Data pIC50Data_2_2 = new PIC50Data(value_2_2, hill_2_2, false, strategyOrder_2_2);
    final List<PIC50Data> allPIC50Data_2 = new ArrayList<PIC50Data>();
    allPIC50Data_2.add(pIC50Data_2_1);
    allPIC50Data_2.add(pIC50Data_2_2);

    final String ionChannelName_3 = "CaV1_2";
    final String sourceAssayName_3 = "Barracuda";
    final BigDecimal value_3_1 = new BigDecimal("4");
    final BigDecimal hill_3_1 = null;
    final BigDecimal value_3_2 = new BigDecimal("4");
    final BigDecimal hill_3_2 = null;
    final Integer strategyOrder_3_1 = 31;
    final Integer strategyOrder_3_2 = 32;
    final PIC50Data pIC50Data_3_1 = new PIC50Data(value_3_1, hill_3_1, false, strategyOrder_3_1);
    final PIC50Data pIC50Data_3_2 = new PIC50Data(value_3_2, hill_3_2, false, strategyOrder_3_2);
    final List<PIC50Data> allPIC50Data_3 = new ArrayList<PIC50Data>();
    allPIC50Data_3.add(pIC50Data_3_1);
    allPIC50Data_3.add(pIC50Data_3_2);

    final String ionChannelName_4 = "KCNQ1";
    final String sourceAssayName_4 = "Quattro_FLIPR";
    final BigDecimal value_4_1 = new BigDecimal("4.3");
    final BigDecimal hill_4_1 = null;
    final BigDecimal value_4_2 = new BigDecimal("4.3");
    final BigDecimal hill_4_2 = null;
    final BigDecimal value_4_3 = new BigDecimal("4.3");
    final BigDecimal hill_4_3 = null;
    final BigDecimal value_4_4 = new BigDecimal("4.3");
    final BigDecimal hill_4_4 = null;
    final Integer strategyOrder_4_1 = 41;
    final Integer strategyOrder_4_2 = 42;
    final Integer strategyOrder_4_3 = 43;
    final Integer strategyOrder_4_4 = 44;
    final PIC50Data pIC50Data_4_1 = new PIC50Data(value_4_1, hill_4_1, false, strategyOrder_4_1);
    final PIC50Data pIC50Data_4_2 = new PIC50Data(value_4_2, hill_4_2, false, strategyOrder_4_2);
    final PIC50Data pIC50Data_4_3 = new PIC50Data(value_4_3, hill_4_3, false, strategyOrder_4_3);
    final PIC50Data pIC50Data_4_4 = new PIC50Data(value_4_4, hill_4_4, false, strategyOrder_4_4);
    final List<PIC50Data> allPIC50Data_4 = new ArrayList<PIC50Data>();
    allPIC50Data_4.add(pIC50Data_4_1);
    allPIC50Data_4.add(pIC50Data_4_2);
    allPIC50Data_4.add(pIC50Data_4_3);
    allPIC50Data_4.add(pIC50Data_4_4);

    final IonChannelValues ionChannelValues_1 = new IonChannelValues(ionChannelName_1,
                                                                     sourceAssayName_1,
                                                                     allPIC50Data_1);
    final IonChannelValues ionChannelValues_2 = new IonChannelValues(ionChannelName_2,
                                                                     sourceAssayName_2,
                                                                     allPIC50Data_2);
    final IonChannelValues ionChannelValues_3 = new IonChannelValues(ionChannelName_3,
                                                                     sourceAssayName_3,
                                                                     allPIC50Data_3);
    final IonChannelValues ionChannelValues_4 = new IonChannelValues(ionChannelName_4,
                                                                     sourceAssayName_4,
                                                                     allPIC50Data_4);
    final Set<IonChannelValues> allIonChannelValues_1 = new HashSet<IonChannelValues>();
    allIonChannelValues_1.add(ionChannelValues_1);
    allIonChannelValues_1.add(ionChannelValues_2);
    allIonChannelValues_1.add(ionChannelValues_3);
    allIonChannelValues_1.add(ionChannelValues_4);

    final GroupedInvocationInput groupedInvocationInput_1 = new GroupedInvocationInput(simulationId,
                                                                                       groupingLevel_1,
                                                                                       groupingName_1,
                                                                                       allIonChannelValues_1);
    final Job job_1 = new Job(simulationId, pacingFrequency, compoundConcentrations, true,
                            groupedInvocationInput_1);
    final Set<Job> jobs = new HashSet<Job>();
    System.out.println(job_1);

    final Short groupingLevel_2 = 2;
    final String groupingName_2 = "PatchXpress/Qpatch";
    final Set<IonChannelValues> allIonChannelValues_2 = new HashSet<IonChannelValues>();
    final String ionChannelName_5 = "KCNQ1";
    final String sourceAssayName_5 = "Quattro_FLIPR";
    final BigDecimal value_5_1 = new BigDecimal("4.3");
    final BigDecimal hill_5_1 = null;
    final BigDecimal value_5_2 = new BigDecimal("4.3");
    final BigDecimal hill_5_2 = null;
    final BigDecimal value_5_3 = new BigDecimal("4.3");
    final BigDecimal hill_5_3 = null;
    final BigDecimal value_5_4 = new BigDecimal("4.3");
    final BigDecimal hill_5_4 = null;
    final Integer strategyOrder_5_1 = 51;
    final Integer strategyOrder_5_2 = 52;
    final Integer strategyOrder_5_3 = 53;
    final Integer strategyOrder_5_4 = 54;
    final PIC50Data pIC50Data_5_1 = new PIC50Data(value_5_1, hill_5_1, false, strategyOrder_5_1);
    final PIC50Data pIC50Data_5_2 = new PIC50Data(value_5_2, hill_5_2, false, strategyOrder_5_2);
    final PIC50Data pIC50Data_5_3 = new PIC50Data(value_5_3, hill_5_3, false, strategyOrder_5_3);
    final PIC50Data pIC50Data_5_4 = new PIC50Data(value_5_4, hill_5_4, false, strategyOrder_5_4);
    final List<PIC50Data> allPIC50Data_5 = new ArrayList<PIC50Data>();
    allPIC50Data_5.add(pIC50Data_5_1);
    allPIC50Data_5.add(pIC50Data_5_2);
    allPIC50Data_5.add(pIC50Data_5_3);
    allPIC50Data_5.add(pIC50Data_5_4);

    final IonChannelValues ionChannelValues_5 = new IonChannelValues(ionChannelName_5,
                                                                     sourceAssayName_5,
                                                                     allPIC50Data_5);
    allIonChannelValues_2.add(ionChannelValues_5);

    final GroupedInvocationInput groupedInvocationInput_2 = new GroupedInvocationInput(simulationId,
                                                                                       groupingLevel_2,
                                                                                       groupingName_2,
                                                                                       allIonChannelValues_2);

    final Job job_2 = new Job(simulationId, pacingFrequency, compoundConcentrations, true,
                              groupedInvocationInput_2);
    jobs.add(job_1);
    jobs.add(job_2);

    jobDAO.storeJobs(jobs);
  }
}