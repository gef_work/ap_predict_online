/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.manager;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.isNull;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.JobProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.config.ConfigurationActionPotential;
import uk.ac.ox.cs.epsrc.business_manager.dao.JobDAO;
import uk.ac.ox.cs.epsrc.business_manager.entity.GroupedInvocationInput;
import uk.ac.ox.cs.epsrc.business_manager.entity.Job;
import uk.ac.ox.cs.epsrc.business_manager.entity.JobResult;
import uk.ac.ox.cs.epsrc.business_manager.manager.JobManager;
import uk.ac.ox.cs.epsrc.business_manager.manager.JobManagerImpl;
import uk.ac.ox.cs.epsrc.business_manager.value.object.PerJobConcentrationResultsVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedDataVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.results.JobDiagnosticsVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Test the job manager implementation.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { JobManagerImpl.class, JobDiagnosticsVO.class,
                   JobResult.class } )
public class JobManagerImplTest {

  private ConfigurationActionPotential mockConfigurationActionPotential;
  private IMocksControl mocksControl;
  private BusinessManagerJMSTemplate mockJmsTemplate;
  private Job mockJob;
  private JobDAO mockJobDAO;
  private JobDiagnosticsVO mockJobDiagnosticsVO;
  private JobManager jobManager;
  private long dummyJobId = 33l;
  private long dummySimulationId = 23l;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockConfigurationActionPotential = mocksControl.createMock(ConfigurationActionPotential.class);
    mockJobDAO = mocksControl.createMock(JobDAO.class);
    mockJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);

    jobManager = new JobManagerImpl();
    ReflectionTestUtils.setField(jobManager,
                                 BusinessIdentifiers.COMPONENT_CONFIGURATION_ACTIONPOTENTIAL,
                                 mockConfigurationActionPotential);
    ReflectionTestUtils.setField(jobManager,
                                 BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockJmsTemplate);
    ReflectionTestUtils.setField(jobManager,
                                 BusinessIdentifiers.COMPONENT_JOB_DAO,
                                 mockJobDAO);

    mockJob = mocksControl.createMock(Job.class);
    mockJobDiagnosticsVO = mocksControl.createMock(JobDiagnosticsVO.class);
  }

  @Test
  public void testAssignAppManagerIdToJob() {
    final String dummyAppManagerId = "dummyAppManagerId";

    mockJobDAO.assignAppManagerIdToJob(dummyJobId, dummyAppManagerId);

    mocksControl.replay();

    jobManager.assignAppManagerIdToJob(dummyJobId, dummyAppManagerId);

    mocksControl.verify();
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testCreateAndStore() {
    final ProcessedDataVO processedDataVO = mocksControl.createMock(ProcessedDataVO.class);

    // 1. No experimental per-freq concentrations in processed data.
    final Map<BigDecimal, Set<BigDecimal>> dummyPerFreqConcs = new HashMap<BigDecimal, Set<BigDecimal>>();
    expect(processedDataVO.getPerFrequencyConcentrations())
          .andReturn(dummyPerFreqConcs);

    // We're going to use the default experimental per-freq concentrations.
    expect(mockConfigurationActionPotential.getDefaultPerFrequencyConcentrations())
          .andReturn(retrieveDummyDefaultExperimentPerFrequencyConcentrations());

    // 1.1. First emulate having no grouped invocation input.
    final Set<GroupedInvocationInput> dummyProcessedData = new HashSet<GroupedInvocationInput>();
    final GroupedInvocationInput mockGroupedInvocationInput = mocksControl.createMock(GroupedInvocationInput.class);
    expect(processedDataVO.getProcessedData()).andReturn(dummyProcessedData);
    mockJobDAO.storeJobs(isA(Set.class));

    mocksControl.replay();

    Set<Job> createdAndStored = jobManager.createAndStore(dummySimulationId,
                                                          processedDataVO);

    mocksControl.verify();
    assertEquals(0, createdAndStored.size());

    mocksControl.reset();

    // 1.2. Emulate having a single grouped invocation input.
    expect(processedDataVO.getPerFrequencyConcentrations())
          .andReturn(dummyPerFreqConcs);
    expect(mockConfigurationActionPotential.getDefaultPerFrequencyConcentrations())
          .andReturn(retrieveDummyDefaultExperimentPerFrequencyConcentrations());
    dummyProcessedData.add(mockGroupedInvocationInput);
    expect(processedDataVO.getProcessedData()).andReturn(dummyProcessedData);

    mockJobDAO.storeJobs(isA(Set.class));

    mocksControl.replay();

    createdAndStored = jobManager.createAndStore(dummySimulationId,
                                                 processedDataVO);

    mocksControl.verify();
    assertEquals(1, createdAndStored.size());

    mocksControl.reset();

    // 2. Per-freq concentrations in processed data.
    dummyPerFreqConcs.putAll(retrieveDummyDefaultExperimentPerFrequencyConcentrations());
    expect(processedDataVO.getPerFrequencyConcentrations())
          .andReturn(dummyPerFreqConcs);
    expect(processedDataVO.getProcessedData()).andReturn(dummyProcessedData);

    mockJobDAO.storeJobs(isA(Set.class));

    mocksControl.replay();

    createdAndStored = jobManager.createAndStore(dummySimulationId,
                                                 processedDataVO);

    mocksControl.verify();
    assertEquals(1, createdAndStored.size());
  }

  @Test
  public void testJobsCompletedForSimulation() {
    long dummyIncompleteJobsForSimulationCount = 0l;
    expect(mockJobDAO.incompleteJobsForSimulationCount(dummySimulationId))
          .andReturn(dummyIncompleteJobsForSimulationCount);

    mocksControl.replay();

    boolean jobsCompletedForSimulation = jobManager.determineJobsCompletedForSimulation(dummySimulationId);

    mocksControl.verify();
    assertTrue(jobsCompletedForSimulation);

    mocksControl.reset();

    dummyIncompleteJobsForSimulationCount = 1l;
    expect(mockJobDAO.incompleteJobsForSimulationCount(dummySimulationId))
          .andReturn(dummyIncompleteJobsForSimulationCount);

    mocksControl.replay();

    jobsCompletedForSimulation = jobManager.determineJobsCompletedForSimulation(dummySimulationId);

    mocksControl.verify();
    assertFalse(jobsCompletedForSimulation);
  }

  @Test
  public void testRetrieveDiagnostics() throws Exception {
    expect(mockJobDAO.findByJobId(dummyJobId)).andReturn(null);
    expectNew(JobDiagnosticsVO.class, isNull(), isNull())
             .andReturn(mockJobDiagnosticsVO);

    replayAll();
    mocksControl.replay();

    JobDiagnosticsVO returnedJobDiagnostics = jobManager.retrieveDiagnostics(dummyJobId);

    verifyAll();
    mocksControl.verify();

    assertNotNull(returnedJobDiagnostics);

    resetAll();
    mocksControl.reset();

    expect(mockJobDAO.findByJobId(dummyJobId)).andReturn(mockJob);
    final String dummyInfo = "dummyInfo";
    final String dummyOutput = "dummyOutput";
    expect(mockJob.getInfo()).andReturn(dummyInfo);
    expect(mockJob.getOutput()).andReturn(dummyOutput);
    expectNew(JobDiagnosticsVO.class, dummyInfo, dummyOutput)
             .andReturn(mockJobDiagnosticsVO);

    replayAll();
    mocksControl.replay();

    returnedJobDiagnostics = jobManager.retrieveDiagnostics(dummyJobId);

    verifyAll();
    mocksControl.verify();

    assertSame(mockJobDiagnosticsVO, returnedJobDiagnostics);


    
  }

  @Test
  public void testFindJobsBySimulationId() {
    final List<Job> dummyJobsFound = new ArrayList<Job>();
    expect(mockJobDAO.findJobsBySimulationId(dummySimulationId))
          .andReturn(dummyJobsFound);

    mocksControl.replay();

    List<Job> jobsFound = jobManager.findJobsBySimulationId(dummySimulationId);

    mocksControl.verify();
    assertEquals(0, jobsFound.size());
  }

  @Test
  public void testPersistDiagnostics() {
    String dummyAppManagerId = null;
    String dummyInfo = null;
    String dummyOutput = null;

    try {
      jobManager.persistDiagnostics(dummyAppManagerId, dummyInfo, dummyOutput);
      fail("Should not permit the use of an invalid app manager id!");
    } catch (IllegalArgumentException e) {}

    dummyAppManagerId = "dummyAppManagerId";

    // Emulate no job found for app manager id.
    Job mockFoundJob = null;
    expect(mockJobDAO.findJobByAppManagerId(dummyAppManagerId))
          .andReturn(mockFoundJob);

    mocksControl.replay();

    jobManager.persistDiagnostics(dummyAppManagerId, dummyInfo, dummyOutput);

    mocksControl.verify();

    mocksControl.reset();

    // Emulate job found for app manager id
    mockFoundJob = mocksControl.createMock(Job.class);
    expect(mockJobDAO.findJobByAppManagerId(dummyAppManagerId)).andReturn(mockFoundJob);
    mockFoundJob.setInfo(dummyInfo);
    mockFoundJob.setOutput(dummyOutput);
    mockJobDAO.save(mockFoundJob);

    mocksControl.replay();

    jobManager.persistDiagnostics(dummyAppManagerId, dummyInfo, dummyOutput);

    mocksControl.verify();
  }

  @Test
  public void testPersistResults() throws Exception {
    String dummyAppManagerId = null;
    String dummyMessages = null;
    String dummyDeltaAPD90PctileNames = null;
    Set<PerJobConcentrationResultsVO> dummyResults = null;

    try {
      jobManager.persistResults(dummyAppManagerId, dummyMessages,
                                dummyDeltaAPD90PctileNames, dummyResults);
      fail("Should not permit the use of an invalid app manager id!");
    } catch (IllegalArgumentException e) {}

    dummyAppManagerId = "dummyAppManagerId";

    // 1. Emulate no job found for app manager id.
    mockJob = null;

    expect(mockJobDAO.findJobByAppManagerId(dummyAppManagerId))
          .andReturn(mockJob);

    mocksControl.replay();

    Job persistedResultsJob = jobManager.persistResults(dummyAppManagerId, 
                                                        dummyMessages,
                                                        dummyDeltaAPD90PctileNames,
                                                        dummyResults);

    mocksControl.verify();
    assertNull(persistedResultsJob);

    mocksControl.reset();

    // 2. Emulate job found, no messages, empty results.
    dummyResults = new HashSet<PerJobConcentrationResultsVO>();
    mockJob = mocksControl.createMock(Job.class);

    expect(mockJobDAO.findJobByAppManagerId(dummyAppManagerId))
          .andReturn(mockJob);
    expect(mockJob.getSimulationId()).andReturn(dummySimulationId);
    expect(mockJob.getId()).andReturn(dummyJobId);
    mockJob.setDeltaAPD90PercentileNames(dummyDeltaAPD90PctileNames);
    Capture<JobProgress> captureJobProgress = newCapture();
    mockJmsTemplate.sendProgress(capture(captureJobProgress));
    mockJob.considerFailed();
    mockJobDAO.storeJobResults(mockJob, true);

    mocksControl.replay();

    persistedResultsJob = jobManager.persistResults(dummyAppManagerId,
                                                    dummyMessages,
                                                    dummyDeltaAPD90PctileNames,
                                                    dummyResults);

    mocksControl.verify();

    assertSame(mockJob, persistedResultsJob);
    JobProgress capturedJobProgress = (JobProgress) captureJobProgress.getValue();
    assertEquals(dummyJobId, capturedJobProgress.getJobId().longValue());
    assertSame(InformationLevel.ERROR, capturedJobProgress.getLevel());
    assertEquals(MessageKey.JOB_RUN_FAILURE.getBundleIdentifier(),
                 capturedJobProgress.getText());

    // 3. Emulate job found, messages, and results
    mocksControl.reset();

    dummyMessages = "dummyMessages";

    expect(mockJobDAO.findJobByAppManagerId(dummyAppManagerId))
          .andReturn(mockJob);
    expect(mockJob.getSimulationId()).andReturn(dummySimulationId);
    expect(mockJob.getId()).andReturn(dummyJobId);
    captureJobProgress = newCapture();
    mockJmsTemplate.sendProgress(capture(captureJobProgress));
    mockJob.setMessages(dummyMessages);
    mockJob.setDeltaAPD90PercentileNames(dummyDeltaAPD90PctileNames);
    final PerJobConcentrationResultsVO mockResultsVO = mocksControl.createMock(PerJobConcentrationResultsVO.class);
    dummyResults.add(mockResultsVO);
    final String dummyCompoundConcentration = "1";
    final String dummyDeltaAPD90 = "dummyDeltaAPD90";
    final String dummyTimes = "dummyTimes";
    final String dummyAPVoltages = "dummyAPVoltages";
    final String dummyQNet = "dummyQNet";
    expect(mockResultsVO.getCompoundConcentration())
          .andReturn(dummyCompoundConcentration);
    expect(mockResultsVO.getDeltaAPD90()).andReturn(dummyDeltaAPD90);
    expect(mockResultsVO.getTimes()).andReturn(dummyTimes);
    expect(mockResultsVO.getVoltages()).andReturn(dummyAPVoltages);
    expect(mockResultsVO.getQNet()).andReturn(dummyQNet);

    final JobResult mockJobResult = mocksControl.createMock(JobResult.class);
    expectNew(JobResult.class, mockJob, Float.valueOf(dummyCompoundConcentration),
                               dummyDeltaAPD90, (String) null, dummyTimes,
                               dummyAPVoltages, dummyQNet)
             .andReturn(mockJobResult);
    //mockJob.addJobResult(isA(JobResult.class));
    mockJobDAO.storeJobResults(mockJob, true);

    final Capture<Progress> captureOverallProgress = newCapture();
    mockJmsTemplate.sendProgress(capture(captureOverallProgress));

    mocksControl.replay();
    replayAll();

    persistedResultsJob = jobManager.persistResults(dummyAppManagerId,
                                                    dummyMessages,
                                                    dummyDeltaAPD90PctileNames,
                                                    dummyResults);

    mocksControl.verify();
    verifyAll();

    assertSame(mockJob, persistedResultsJob);
    capturedJobProgress = (JobProgress) captureJobProgress.getValue();

    assertEquals(dummyJobId, capturedJobProgress.getJobId().longValue());
    assertSame(InformationLevel.WARN, capturedJobProgress.getLevel());
    assertEquals(MessageKey.JOB_GENERATED_MESSAGES.getBundleIdentifier(),
                 capturedJobProgress.getText());
    final OverallProgress capturedOverallProgress = (OverallProgress) captureOverallProgress.getValue();
    assertSame(dummySimulationId, capturedOverallProgress.getSimulationId()
                                                         .longValue());
    assertSame(InformationLevel.DEBUG, capturedOverallProgress.getLevel());
    assertEquals(MessageKey.JOB_RESULTS_RECEIVED.getBundleIdentifier(),
                 capturedOverallProgress.getText());
  }

  @Test
  public void testPurgeSimulationJobData() {
    final boolean dummySuccessful = true;

    final Job mockJob1 = mocksControl.createMock(Job.class);
    final Job mockJob2 = mocksControl.createMock(Job.class);
    final List<Job> dummySimulationJobs = new ArrayList<Job>();
    dummySimulationJobs.add(mockJob1);
    dummySimulationJobs.add(mockJob2);

    expect(mockJobDAO.findJobsBySimulationId(dummySimulationId))
          .andReturn(dummySimulationJobs);
    final boolean dummyJob1Unfinished = true;
    expect(mockJob1.isUnfinished()).andReturn(dummyJob1Unfinished);
    final String dummyAppManagerId = "dummyAppManagerId";
    expect(mockJob1.getAppManagerId()).andReturn(dummyAppManagerId);

    final boolean dummyJob2Unfinished = false;
    expect(mockJob2.isUnfinished()).andReturn(dummyJob2Unfinished);

    expect(mockJobDAO.removeJobs(dummySimulationJobs)).andReturn(dummySuccessful);

    mocksControl.replay();

    final Set<String> appManagerIds = jobManager.purgeSimulationJobData(dummySimulationId);

    mocksControl.verify();

    assertEquals(1, appManagerIds.size());
    assertEquals(dummyAppManagerId, appManagerIds.iterator().next());
  }

  // retrieve dummy default experiment per-frequency concentrations
  final Map<BigDecimal, Set<BigDecimal>> retrieveDummyDefaultExperimentPerFrequencyConcentrations() {
    final Map<BigDecimal, Set<BigDecimal>> dummyDefaultExpPerFreqConcs = new HashMap<BigDecimal, Set<BigDecimal>>();
    final Set<BigDecimal> concs = new TreeSet<BigDecimal>();
    concs.add(BigDecimal.ZERO);
    concs.add(BigDecimal.ONE);
    concs.add(BigDecimal.TEN);
    dummyDefaultExpPerFreqConcs.put(new BigDecimal("0.5"), concs);

    return dummyDefaultExpPerFreqConcs;
  }
}