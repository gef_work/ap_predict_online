/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.ws.test.server.MockWebServiceClient;

import uk.ac.ox.cs.epsrc.business_manager.BusinessTestIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ObjectFactory;


/**
 * Abstracted WS message endpoint integration testing.
 * <p>
 * Note: All integration test persistence consequences are automatically rolled back.
 * 
 * @author geoff
 */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration( { "classpath:/META-INF/spring/ctx/appCtx.business.xml",
//                         "classpath:/META-INF/spring/ctx/config/appCtx.config.xml",
//                         "classpath:/META-INF/spring/ctx/data/business_manager/appCtx.database.business-manager.test.xml",
//                         "classpath:/META-INF/spring/ctx/integration/appCtx.int.xml",
//                         "classpath:/META-INF/spring/ctx/jms/appCtx.jms.ActiveMQ.xml",
//                         "classpath:/META-INF/spring/ctx/ws/appCtx.ws.xml" } )
//@TransactionConfiguration(transactionManager=BusinessTestIdentifiers.COMPONENT_TRANSACTION_MANAGER)
public abstract class AbstractMessageEndpointViaWSCall { // extends AbstractTransactionalJUnit4SpringContextTests {

  protected static final String NAMESPACE = "http://www.cs.ox.ac.uk/epsrc/business_manager/ws/1";

  //@Autowired
  private ApplicationContext applicationContext;
  private MockWebServiceClient mockClient;
  protected final static ObjectFactory objectFactory = new ObjectFactory();

  /**
   * Base SOAP Fault response string (with placeholder for fault string).
   */
  protected static final String baseSOAPFaultResponseString = 
    "<SOAP-ENV:Fault xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/'>" +
    "  <faultcode>SOAP-ENV:Server</faultcode>" +
    "  <faultstring xml:lang='en'>%s</faultstring>" +
    "</SOAP-ENV:Fault>";

  /**
   * @return the applicationContext
   */
  protected ApplicationContext getApplicationContext() {
    return applicationContext;
  }

  /**
   * @return the mockClient
   */
  protected MockWebServiceClient getMockClient() {
    return mockClient;
  }

  /**
   * @param mockClient the mockClient to set
   */
  protected void setMockClient(MockWebServiceClient mockClient) {
    this.mockClient = mockClient;
  }
}