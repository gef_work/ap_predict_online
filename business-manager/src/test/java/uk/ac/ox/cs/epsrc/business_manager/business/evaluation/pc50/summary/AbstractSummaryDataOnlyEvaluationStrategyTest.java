/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.summary;

import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.core.Ordered;

import uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.summary.AbstractSummaryDataOnlyEvaluationStrategy;
import uk.ac.ox.cs.epsrc.business_manager.business.selection.c50.C50DataSelector;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.summary.SummaryOnlyC50DataStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;

/**
 * Unit test the abstract summary data PC50 evaluation strategy when there is summary but no
 * individual data.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(C50DataSelector.class)
public class AbstractSummaryDataOnlyEvaluationStrategyTest {

  private static final boolean dummyUsingEqualityModifier = true;
  private static final BigDecimal dummyPC50 = BigDecimal.ONE;

  private class TestAbstractNoIndividualDataEvaluationStrategy extends AbstractSummaryDataOnlyEvaluationStrategy {

    private static final long serialVersionUID = 1L;

    protected TestAbstractNoIndividualDataEvaluationStrategy(final String description,
                                                              final boolean defaultActive,
                                                              final int defaultInvocationOrder) {
      super(description, defaultActive, defaultInvocationOrder);
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy#usesEqualityModifier()
     */
    @Override
    public boolean usesEqualityModifier() {
      return dummyUsingEqualityModifier;
    }

    /* (non-Javadoc)
     * @see uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.summary.AbstractSummaryDataOnlyEvaluationStrategy#retrievePC50ByStrategy(uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData, java.lang.String)
     */
    @Override
    protected BigDecimal retrievePC50ByStrategy(final ICData summaryC50Data,
                                                final String recordIdentifier) {
      return dummyPC50;
    }
  }

  private TestAbstractNoIndividualDataEvaluationStrategy testStrategy;
  private String dummyDescription = "dummyDescription";
  private boolean dummyDefaultActive = true;
  private int dummyOrder = 9;

  @Before
  public void setUp() {
    testStrategy = new TestAbstractNoIndividualDataEvaluationStrategy(dummyDescription,
                                                                      dummyDefaultActive,
                                                                      dummyOrder);
  }

  @Test
  public void testConstructor() {
    assertTrue(testStrategy instanceof SummaryOnlyC50DataStrategy);
    assertTrue(testStrategy instanceof PC50EvaluationStrategy);
    assertTrue(testStrategy instanceof Ordered);
    assertTrue(testStrategy.getDefaultActive());
    assertSame(dummyDescription, testStrategy.getDescription());
    assertSame(dummyOrder, testStrategy.getOrder());
  }

  @Test
  public void testInputVerification() throws InvalidValueException {
    SiteDataHolder dummySiteDataHolder = null;
    String dummyRecordIdentifier = null;
    try {
      testStrategy.evaluatePC50(dummySiteDataHolder, dummyRecordIdentifier);
      fail("PC50 evaluation should not permit the passing of a null site data holder as an arg.");
    } catch (IllegalArgumentException e) {}
  }

  @Test
  public void testPC50Evaluation() throws IllegalArgumentException, InvalidValueException {
    final IMocksControl mocksControl = createControl();
    final SiteDataHolder mockSiteDataHolder = mocksControl.createMock(SiteDataHolder.class);

    // Situation where no summary data record.
    expect(mockSiteDataHolder.getSummaryDataRecord(true)).andReturn(null);

    mocksControl.replay();

    BigDecimal evaluatedPC50 = testStrategy.evaluatePC50(mockSiteDataHolder, null);

    mocksControl.verify();

    assertNull(evaluatedPC50);

    mocksControl.reset();

    // Situation where summary data and non-summary data.
    expect(mockSiteDataHolder.getSummaryDataRecord(true)).andReturn(null);

    mocksControl.replay();

    evaluatedPC50 = testStrategy.evaluatePC50(mockSiteDataHolder, null);

    mocksControl.verify();

    assertNull(evaluatedPC50);

    mocksControl.reset();

    // Situation where summary data only but no summary C50 data.
    final SummaryDataRecord mockSummaryDataRecord = mocksControl.createMock(SummaryDataRecord.class);
    expect(mockSiteDataHolder.getSummaryDataRecord(true)).andReturn(mockSummaryDataRecord);
    final List<ICData> dummyEligibleC50Data = new ArrayList<ICData>();
    expect(mockSummaryDataRecord.eligibleICData()).andReturn(dummyEligibleC50Data);
    mockStatic(C50DataSelector.class);
    // No summary C50 data.
    expect(C50DataSelector.selectFrom(dummyEligibleC50Data)).andReturn(null);

    replayAll();
    mocksControl.replay();

    evaluatedPC50 = testStrategy.evaluatePC50(mockSiteDataHolder, null);

    verifyAll();
    mocksControl.verify();

    assertNull(evaluatedPC50);

    resetAll();
    mocksControl.reset();

    // Situation where summary data only and a summary C50 value.
    expect(mockSiteDataHolder.getSummaryDataRecord(true)).andReturn(mockSummaryDataRecord);
    expect(mockSummaryDataRecord.eligibleICData()).andReturn(dummyEligibleC50Data);
    final ICData mockC50Data = mocksControl.createMock(ICData.class);
    expect(C50DataSelector.selectFrom(dummyEligibleC50Data)).andReturn(mockC50Data);

    replayAll();
    mocksControl.replay();

    evaluatedPC50 = testStrategy.evaluatePC50(mockSiteDataHolder, null);

    verifyAll();
    mocksControl.verify();

    assertNotNull(evaluatedPC50);
  }
}