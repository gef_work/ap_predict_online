/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.dao.information;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.easymock.EasyMock;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.dao.information.InformationDAO;
import uk.ac.ox.cs.epsrc.business_manager.dao.information.OrientDAOImpl;
import uk.ac.ox.cs.epsrc.business_manager.manager.InformationManager.INFORMATION_FORMAT;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.DoseResponseData;
import uk.ac.ox.cs.epsrc.business_manager.value.object.InformationVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Unit test the information data access object.
 *
 * @author geoff
 */
public class OrientDAOImplTest {

  // These values correspond to the private vars defined in OrientDAOImpl
  private static final String keyCompoundIdentifier = "compoundIdentifier";
  private static final String keyLevel = "level";
  private static final String keyProvenance = "provenance";
  private static final String keySimulationId = "simulationId";
  private static final String keyText = "text";

  private static OrientDAOImpl orientDAOImpl;
  private static InformationDAO informationDAO;
  private BigDecimal dummyHill;
  private BigDecimal dummyPIC50;
  private BigDecimal dummyRawHill;
  private Date dummyTimestamp;
  private DoseResponseData mockDoseResponseData;
  private float dummyPacingFrequency;
  private InformationLevel dummyInformationLevel;
  private List<BigDecimal> dummyPIC50s;
  private List<String> dummyArgs;
  private long dummyJobId;
  private long dummySimulationId;
  private Map<String, Object> dummyRawData;
  private short dummyGroupLevel;
  private String dummyAssayName;
  private String dummyCompoundIdentifier;
  private String dummyDoseInUM;
  private String dummyIndividualDataId;
  private String dummyIonChannelName;
  private String dummyResponse;
  private String dummyDoseResponseDataId;
  private String dummyGroupName;
  private String dummyDoseResponseFittingStrategyIdentifier;
  private String dummySummaryDataId;
  private String dummyText;

  @Before
  public void setUp() {
    dummySimulationId = 1l;
    dummyTimestamp = null;
    dummyInformationLevel = null;
    dummyArgs = null;
    dummyAssayName = null;
    dummyCompoundIdentifier = null;
    dummyDoseResponseDataId = null;
    dummyDoseResponseFittingStrategyIdentifier = null;
    dummyGroupName = null;
    dummyIonChannelName = null;
    dummyPIC50s = null;
    dummyRawHill = null;
    dummySummaryDataId = null;
    dummyText = null;
    dummyDoseInUM = null;
    dummyRawData = null;
    dummyResponse = null;
    dummyIndividualDataId = null;
    dummyPIC50 = null;
    dummyHill = null;
    mockDoseResponseData = null;
  }

  @BeforeClass
  public static void oneTimeSetUp() {
    orientDAOImpl = new OrientDAOImpl();
    informationDAO = orientDAOImpl;
    orientDAOImpl.initialiseOrient();
  }

  @After
  public void tearDown() {
  }

  @AfterClass
  public static void onTimeTearDown() {
    orientDAOImpl.closeOrient();
    orientDAOImpl.dropOrient();
  }

  @Test
  public void testSetupDropdown() { }

  /*
   * PROVENANCE
   */ 
  @Test
  public void testReadWriteProvenance() throws JSONException {
    // NOTE : A null/empty compound identifier value is ok!
    dummyInformationLevel = InformationLevel.DEBUG;
    dummyText = "dummyText";
    dummyCompoundIdentifier = "dummyCompoundIdentifier";

    informationDAO.writeFundamental(dummyCompoundIdentifier, dummySimulationId,
                                    dummyInformationLevel, dummyText, null);

    // e.g. {"@type":"d","@rid":"#9:0","@version":0,"@class":"Fundamental","compoundIdentifier":"dummyCompoundIdentifier",
    //       "simulationId":1,"provenance":[{"@type":"d","@version":0,"level":"DEBUG",
    //                                       "text":"dummyText"}],"@fieldTypes":"simulationId=l"}
    InformationVO informationVO1 = informationDAO.retrieveProvenance(dummySimulationId,
                                                                    INFORMATION_FORMAT.JSON);
    assertTrue(informationVO1.hasInformation());
    assertEquals(Long.valueOf(dummySimulationId), informationVO1.getSimulationId());
    assertTrue(informationVO1.getAppManagerJobProgress().isEmpty());
 
    final JSONObject provenance1 = new JSONObject(informationVO1.getInformation());
    assertEquals(dummyCompoundIdentifier, provenance1.getString(keyCompoundIdentifier));
    assertEquals(dummySimulationId, provenance1.getLong(keySimulationId));
    final JSONArray provenanceCollection1 = provenance1.getJSONArray(keyProvenance);
    assertEquals(1, provenanceCollection1.length());

    final JSONObject provenanceItem1 = provenanceCollection1.getJSONObject(0);
    final Iterator<?> provenanceItem1KeysIterator = provenanceItem1.keys();
    final Set<String> provenanceAllKeys = new HashSet<String>();
    while (provenanceItem1KeysIterator.hasNext()) {
      provenanceAllKeys.add(provenanceItem1KeysIterator.next().toString());
    }
    assertTrue(provenanceAllKeys.contains(keyText));
    assertTrue(provenanceAllKeys.contains(keyLevel));

    // Purge the data
    informationDAO.purge(dummySimulationId);
  }

  @Test(expected=IllegalStateException.class)
  public void testUpdateDoseResponse1() {
    dummyDoseResponseDataId = "dummyDoseResponseDataId";
    dummyInformationLevel = InformationLevel.INFO;
    dummyText = "dummyText";

    informationDAO.updateDoseResponse(dummySimulationId, dummyDoseResponseDataId,
                                      dummyInformationLevel, dummyText, dummyArgs);
    fail("Should not permit the updating of a non-existent dose-response provenance structure!");
  }

  @Test(expected=IllegalStateException.class)
  public void testUpdateDoseResponse2() {
    dummyDoseResponseDataId = "dummyDoseResponseDataId";
    dummyInformationLevel = InformationLevel.INFO;
    dummyText = "dummyText";
    try {
      informationDAO.updateDoseResponse(dummySimulationId, dummyDoseResponseDataId,
                                        dummyInformationLevel, dummyText, dummyArgs, dummyDoseInUM,
                                        dummyResponse);
      fail("Should not permit an invalid dose in uM value.");
    } catch (IllegalArgumentException e) {}
    dummyDoseInUM = "dummyDoseInuM";
    try {
      informationDAO.updateDoseResponse(dummySimulationId, dummyDoseResponseDataId,
                                        dummyInformationLevel, dummyText, dummyArgs, dummyDoseInUM,
                                        dummyResponse);
      fail("Should not permit an invalid dose response value.");
    } catch (IllegalArgumentException e) {}
    dummyResponse = "dummyResponse";

    informationDAO.updateDoseResponse(dummySimulationId, dummyDoseResponseDataId,
                                      dummyInformationLevel, dummyText, dummyArgs, dummyDoseInUM,
                                      dummyResponse);
    fail("Should not permit the updating of a non-existent dose-response structure!");
  }

  @Test(expected=IllegalStateException.class)
  public void testUpdateIndividual1() {
    dummyIndividualDataId = "dummyIndividualId";
    dummyInformationLevel = InformationLevel.WARN;
    dummyText = "dummyText";
    informationDAO.updateIndividual(dummySimulationId, dummyIndividualDataId, dummyInformationLevel,
                                    dummyText, dummyArgs);
    fail("Should not permit the updating of a non-existent individual provenance structure!");
  }

  @Test(expected=IllegalStateException.class)
  public void testUpdateIndividual2() {
    dummyIndividualDataId = "dummyIndividualId";
    dummyInformationLevel = InformationLevel.WARN;
    dummyText = "dummyText";
    try {
      informationDAO.updateIndividual(dummySimulationId, dummyIndividualDataId,
                                      dummyInformationLevel, dummyText, dummyArgs, dummyPIC50,
                                      dummyHill);
      fail("Should not permit a null pIC50 value.");
    } catch (IllegalArgumentException e) {}
    dummyPIC50 = BigDecimal.ONE;
    try {
      informationDAO.updateIndividual(dummySimulationId, dummyIndividualDataId,
                                      dummyInformationLevel, dummyText, dummyArgs, dummyPIC50,
                                      dummyHill);
      fail("Should not permit a null Hill value.");
    } catch (IllegalArgumentException e) {}
    dummyHill = BigDecimal.ZERO;
    informationDAO.updateIndividual(dummySimulationId, dummyIndividualDataId, dummyInformationLevel,
                                    dummyText, dummyArgs, dummyPIC50, dummyHill);
    fail("Should not permit the updating of a non-existent individual provenance structure.");
  }

  @Test(expected=IllegalStateException.class)
  public void testUpdateIndividual3() {
    dummyIndividualDataId = "dummyIndividualId";
    dummyInformationLevel = InformationLevel.WARN;
    dummyText = "dummyText";
    try {
      informationDAO.updateIndividual(dummySimulationId, dummyIndividualDataId,
                                      dummyInformationLevel, dummyText, dummyArgs,
                                      mockDoseResponseData);
      fail("Should not permit a null dummy dose-response data value.");
    } catch (IllegalArgumentException e) {}
    mockDoseResponseData = EasyMock.createMock(DoseResponseData.class);
    informationDAO.updateIndividual(dummySimulationId, dummyIndividualDataId, dummyInformationLevel,
                                    dummyText, dummyArgs, mockDoseResponseData);
    fail("Should not permit the updating of a non-existent individual provenance structure.");
  }

  @Test(expected=IllegalStateException.class)
  public void testUpdateIndividual4() {
    dummyIndividualDataId = "dummyIndividualId";
    dummyInformationLevel = InformationLevel.WARN;
    dummyText = "dummyText";
    try {
      informationDAO.updateIndividual(dummySimulationId, dummyIndividualDataId,
                                      dummyInformationLevel, dummyText, dummyArgs,
                                      mockDoseResponseData,
                                      dummyDoseResponseFittingStrategyIdentifier);
      fail("Should not permit a null dummy dose-response data value.");
    } catch (IllegalArgumentException e) {}
    mockDoseResponseData = EasyMock.createMock(DoseResponseData.class);
    try {
      informationDAO.updateIndividual(dummySimulationId, dummyIndividualDataId,
                                      dummyInformationLevel, dummyText, dummyArgs,
                                      mockDoseResponseData,
                                      dummyDoseResponseFittingStrategyIdentifier);
      fail("Should not permit a null fitting strategy value.");
    } catch (IllegalArgumentException e) {}
    dummyDoseResponseFittingStrategyIdentifier = "dummyDoseResponseFittingStrategyIdentifier";
    informationDAO.updateIndividual(dummySimulationId, dummyIndividualDataId, dummyInformationLevel,
                                    dummyText, dummyArgs, mockDoseResponseData,
                                    dummyDoseResponseFittingStrategyIdentifier);
    fail("Should not permit the updating of a non-existent individual provenance structure.");
  }

  @Test(expected=IllegalStateException.class)
  public void testUpdateSummary1() {
    dummySummaryDataId = "dummySummaryDataId";
    dummyInformationLevel = InformationLevel.WARN;
    dummyText = "dummyText";
    informationDAO.updateSummary(dummySimulationId, dummySummaryDataId, dummyInformationLevel,
                                 dummyText, dummyArgs);
    fail("Should not permit the updating of a non-existent summary provenance structure.");
  }

  @Test(expected=IllegalStateException.class)
  public void testUpdateSummary2() {
    dummySummaryDataId = "dummySummaryDataId";
    dummyInformationLevel = InformationLevel.WARN;
    dummyText = "dummyText";
    try {
      informationDAO.updateSummary(dummySimulationId, dummySummaryDataId, dummyInformationLevel,
                                   dummyText, dummyArgs, dummyPIC50s, dummyHill);
      fail("Should not permit a null pIC50s collection value.");
    } catch (IllegalArgumentException e) {}
    dummyPIC50s = new ArrayList<BigDecimal>();
    try {
      informationDAO.updateSummary(dummySimulationId, dummySummaryDataId, dummyInformationLevel,
                                   dummyText, dummyArgs, dummyPIC50s, dummyHill);
      fail("Should not permit a null Hill value.");
    } catch (IllegalArgumentException e) {}
    dummyHill = BigDecimal.ONE;

    informationDAO.updateSummary(dummySimulationId, dummySummaryDataId, dummyInformationLevel,
                                 dummyText, dummyArgs, dummyPIC50s, dummyHill);
    fail("Should not permit the updating of a non-existent summary provenance structure.");
  }

  @Test(expected=IllegalStateException.class)
  public void testWriteDoseResponse() {
    try {
      informationDAO.writeDoseResponse(dummySimulationId, dummyIndividualDataId,
                                       dummyDoseResponseDataId, dummyRawData, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit a null value for individual data identifier.");
    } catch (IllegalArgumentException e) {}
    dummyIndividualDataId = "dummyIndividualDataId";
    try {
      informationDAO.writeDoseResponse(dummySimulationId, dummyIndividualDataId,
                                       dummyDoseResponseDataId, dummyRawData, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit a null value for dose-response data identifier.");
    } catch (IllegalArgumentException e) {}
    dummyDoseResponseDataId = "dummyDoseResponseDataId";
    try {
      informationDAO.writeDoseResponse(dummySimulationId, dummyIndividualDataId,
                                       dummyDoseResponseDataId, dummyRawData, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit a null value raw data.");
    } catch (IllegalArgumentException e) {}
    dummyRawData = new HashMap<String, Object>();
    try {
      informationDAO.writeDoseResponse(dummySimulationId, dummyIndividualDataId,
                                       dummyDoseResponseDataId, dummyRawData, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit an empty raw data value.");
    } catch (IllegalArgumentException e) {}
    dummyRawData.put("dummyKey", new Object());
    try {
      informationDAO.writeDoseResponse(dummySimulationId, dummyIndividualDataId,
                                       dummyDoseResponseDataId, dummyRawData, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit a null information level.");
    } catch (IllegalArgumentException e) {}
    dummyInformationLevel = InformationLevel.DEBUG;
    try {
      informationDAO.writeDoseResponse(dummySimulationId, dummyIndividualDataId,
                                       dummyDoseResponseDataId, dummyRawData, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit an empty text value.");
    } catch (IllegalArgumentException e) {}
    dummyText = "dummyText";
    informationDAO.writeDoseResponse(dummySimulationId, dummyIndividualDataId,
                                     dummyDoseResponseDataId, dummyRawData, dummyInformationLevel,
                                     dummyText, dummyArgs);
    fail("Should not permit the updating of dose-response provenance structure if no individual provenance structure.");
  }

  @Test
  public void testWriteFundamental() {
    // NOTE : A null/empty compound identifier value is ok!
    dummySimulationId = -1l;
    try {
      informationDAO.writeFundamental(dummyCompoundIdentifier, dummySimulationId,
                                      dummyInformationLevel, dummyText, dummyArgs);
      fail("Should not permit an invalid value for simulation id.");
    } catch (IllegalArgumentException e) {}
    dummySimulationId = 1L;
    try {
      informationDAO.writeFundamental(dummyCompoundIdentifier, dummySimulationId,
                                      dummyInformationLevel, dummyText, dummyArgs);
      fail("Should not permit a null information level.");
    } catch (IllegalArgumentException e) {}
    dummyInformationLevel = InformationLevel.DEBUG;
    try {
      informationDAO.writeFundamental(dummyCompoundIdentifier, dummySimulationId,
                                      dummyInformationLevel, dummyText, dummyArgs);
      fail("Should not permit a null text value.");
    } catch (IllegalArgumentException e) {}
    dummyText = "dummyText";    
    try {
      informationDAO.writeFundamental(dummyCompoundIdentifier, dummySimulationId,
                                      dummyInformationLevel, dummyText, dummyArgs);
      fail("Should not permit the creation of a new fundamental provenance structure if no compound identifier.");
    } catch (IllegalArgumentException e) {}
    // Don't actually write anything as it may cause other tests to fail 
  }

  @Test
  public void testWriteIndividual1() {
    dummySimulationId = -1l;
    try {
      informationDAO.writeIndividual(dummySimulationId, dummyIndividualDataId, dummyAssayName,
                                     dummyIonChannelName, dummyRawHill, dummyRawData,
                                     dummyInformationLevel, dummyText, dummyArgs);
      fail("Should not permit an invalid simulation id.");
    } catch (IllegalArgumentException e) {}
    dummySimulationId = 1l;
    try {
      informationDAO.writeIndividual(dummySimulationId, dummyIndividualDataId, dummyAssayName,
                                     dummyIonChannelName, dummyRawHill, dummyRawData,
                                     dummyInformationLevel, dummyText, dummyArgs);
      fail("Should not permit an invalid individual data id.");
    } catch (IllegalArgumentException e) {}
    dummyIndividualDataId = "dummyIndividualDataId";
    try {
      informationDAO.writeIndividual(dummySimulationId, dummyIndividualDataId, dummyAssayName,
                                     dummyIonChannelName, dummyRawHill, dummyRawData,
                                     dummyInformationLevel, dummyText, dummyArgs);
      fail("Should not permit an invalid assay name.");
    } catch (IllegalArgumentException e) {}
    dummyAssayName = "dummyAssayName";
    try {
      informationDAO.writeIndividual(dummySimulationId, dummyIndividualDataId, dummyAssayName,
                                     dummyIonChannelName, dummyRawHill, dummyRawData,
                                     dummyInformationLevel, dummyText, dummyArgs);
      fail("Should not permit an invalid ion channel name.");
    } catch (IllegalArgumentException e) {}
    dummyIonChannelName = "dummyIonChannelName";
    try {
      informationDAO.writeIndividual(dummySimulationId, dummyIndividualDataId, dummyAssayName,
                                     dummyIonChannelName, dummyRawHill, dummyRawData,
                                     dummyInformationLevel, dummyText, dummyArgs);
      fail("Should not permit an invalid raw hill value.");
    } catch (IllegalArgumentException e) {}
    dummyRawHill = BigDecimal.ONE;
    try {
      informationDAO.writeIndividual(dummySimulationId, dummyIndividualDataId, dummyAssayName,
                                     dummyIonChannelName, dummyRawHill, dummyRawData,
                                     dummyInformationLevel, dummyText, dummyArgs);
      fail("Should not permit an invalid raw data value.");
    } catch (IllegalArgumentException e) {}
    dummyRawData = new HashMap<String, Object>();
    dummyRawData.put("dummyKey", new Object());
    try {
      informationDAO.writeIndividual(dummySimulationId, dummyIndividualDataId, dummyAssayName,
                                     dummyIonChannelName, dummyRawHill, dummyRawData,
                                     dummyInformationLevel, dummyText, dummyArgs);
      fail("Should not permit an invalid information level.");
    } catch (IllegalArgumentException e) {}
    dummyInformationLevel = InformationLevel.DEBUG;
    try {
      informationDAO.writeIndividual(dummySimulationId, dummyIndividualDataId, dummyAssayName,
                                     dummyIonChannelName, dummyRawHill, dummyRawData,
                                     dummyInformationLevel, dummyText, dummyArgs);
      fail("Should not permit an invalid text.");
    } catch (IllegalArgumentException e) {}
    dummyText = "dummyText";
    // Don't actually write anything as it may cause other tests to fail 
  }

  @Test
  public void testWriteIndividual2() {
    try {
      informationDAO.writeIndividual(dummySimulationId, dummySummaryDataId, dummyIndividualDataId,
                                     dummyRawHill, dummyRawData, dummyInformationLevel, dummyText,
                                     dummyArgs);
      fail("Should not permit an invalid summary data id.");
    } catch (IllegalArgumentException e) {}
    dummySummaryDataId = "dummySummaryDataId";
    try {
      informationDAO.writeIndividual(dummySimulationId, dummySummaryDataId, dummyIndividualDataId,
                                     dummyRawHill, dummyRawData, dummyInformationLevel, dummyText,
                                     dummyArgs);
      fail("Should not permit an invalid individual data id.");
    } catch (IllegalArgumentException e) {}
    dummyIndividualDataId = "dummyIndividualDataId";
    try {
      informationDAO.writeIndividual(dummySimulationId, dummySummaryDataId, dummyIndividualDataId,
                                     dummyRawHill, dummyRawData, dummyInformationLevel, dummyText,
                                     dummyArgs);
      fail("Should not permit an invalid raw hill value.");
    } catch (IllegalArgumentException e) {}
    dummyRawHill = BigDecimal.ONE;
    try {
      informationDAO.writeIndividual(dummySimulationId, dummySummaryDataId, dummyIndividualDataId,
                                     dummyRawHill, dummyRawData, dummyInformationLevel, dummyText,
                                     dummyArgs);
      fail("Should not permit an invalid raw data.");
    } catch (IllegalArgumentException e) {}
    dummyRawData = new HashMap<String, Object>();
    try {
      informationDAO.writeIndividual(dummySimulationId, dummySummaryDataId, dummyIndividualDataId,
                                     dummyRawHill, dummyRawData, dummyInformationLevel, dummyText,
                                     dummyArgs);
      fail("Should not permit an invalid raw data.");
    } catch (IllegalArgumentException e) {}
    dummyRawData.put("dummyKey", new Object());
    try {
      informationDAO.writeIndividual(dummySimulationId, dummySummaryDataId, dummyIndividualDataId,
                                     dummyRawHill, dummyRawData, dummyInformationLevel, dummyText,
                                     dummyArgs);
      fail("Should not permit an invalid information level.");
    } catch (IllegalArgumentException e) {}
    dummyInformationLevel = InformationLevel.DEBUG;
    try {
      informationDAO.writeIndividual(dummySimulationId, dummySummaryDataId, dummyIndividualDataId,
                                     dummyRawHill, dummyRawData, dummyInformationLevel, dummyText,
                                     dummyArgs);
      fail("Should not permit an text.");
    } catch (IllegalArgumentException e) {}
    // Don't actually write anything as it may cause other tests to fail 
  }

  @Test
  public void testWriteSummary() {
    dummySimulationId = -1l;
    try {
      informationDAO.writeSummary(dummySimulationId, dummySummaryDataId, dummyAssayName,
                                  dummyIonChannelName, dummyRawData, dummyInformationLevel,
                                  dummyText, dummyArgs);
      fail("Should not permit an invalid simulation id.");
    } catch (IllegalArgumentException e) {}
    dummySimulationId = 1l;
    try {
      informationDAO.writeSummary(dummySimulationId, dummySummaryDataId, dummyAssayName,
                                  dummyIonChannelName, dummyRawData, dummyInformationLevel,
                                  dummyText, dummyArgs);
      fail("Should not permit an invalid summary data id.");
    } catch (IllegalArgumentException e) {}
    dummySummaryDataId = "dummySummaryDataId";
    try {
      informationDAO.writeSummary(dummySimulationId, dummySummaryDataId, dummyAssayName,
                                  dummyIonChannelName, dummyRawData, dummyInformationLevel,
                                  dummyText, dummyArgs);
      fail("Should not permit an invalid assay name.");
    } catch (IllegalArgumentException e) {}
    dummyAssayName = "dummyAssayName";
    try {
      informationDAO.writeSummary(dummySimulationId, dummySummaryDataId, dummyAssayName,
                                  dummyIonChannelName, dummyRawData, dummyInformationLevel,
                                  dummyText, dummyArgs);
      fail("Should not permit an invalid ion channel name.");
    } catch (IllegalArgumentException e) {}
    dummyIonChannelName = "dummyIonChannelName";
    try {
      informationDAO.writeSummary(dummySimulationId, dummySummaryDataId, dummyAssayName,
                                  dummyIonChannelName, dummyRawData, dummyInformationLevel,
                                  dummyText, dummyArgs);
      fail("Should not permit an invalid raw data value.");
    } catch (IllegalArgumentException e) {}
    dummyRawData = new HashMap<String, Object>();
    try {
      informationDAO.writeSummary(dummySimulationId, dummySummaryDataId, dummyAssayName,
                                  dummyIonChannelName, dummyRawData, dummyInformationLevel,
                                  dummyText, dummyArgs);
      fail("Should not permit an invalid raw data value.");
    } catch (IllegalArgumentException e) {}
    dummyRawData.put("dummyKey", new Object());
    try {
      informationDAO.writeSummary(dummySimulationId, dummySummaryDataId, dummyAssayName,
                                  dummyIonChannelName, dummyRawData, dummyInformationLevel,
                                  dummyText, dummyArgs);
      fail("Should not permit an invalid information level.");
    } catch (IllegalArgumentException e) {}
    dummyInformationLevel = InformationLevel.WARN;
    try {
      informationDAO.writeSummary(dummySimulationId, dummySummaryDataId, dummyAssayName,
                                  dummyIonChannelName, dummyRawData, dummyInformationLevel,
                                  dummyText, dummyArgs);
      fail("Should not permit an invalid text value.");
    } catch (IllegalArgumentException e) {}
    // Don't actually write anything as it may cause other tests to fail 
  }

  @Test
  public void testProvenanceReadWrite() {
    
  }

  /*
   * PROGRESS
   */
  // Add progress is not argument aspect-protected so needs to be tested here. 
  @Test(expected=IllegalStateException.class)
  public void testAddProgressJobExceptions() {
    dummyJobId = -99l;
    try {
      informationDAO.updateProgressJob(dummyJobId, dummyTimestamp, dummyInformationLevel, dummyText,
                                       dummyArgs);
      fail("Should not permit invalid job id.");
    } catch (IllegalArgumentException e) {}
    dummyJobId = 99l;
    try {
      informationDAO.updateProgressJob(dummyJobId, dummyTimestamp, dummyInformationLevel, dummyText,
                                       dummyArgs);
      fail("Should not permit null timestamp.");
    } catch (IllegalArgumentException e) {}
    dummyTimestamp = new Date();
    try {
      informationDAO.updateProgressJob(dummyJobId, dummyTimestamp, dummyInformationLevel, dummyText,
                                       dummyArgs);
      fail("Should not permit unassigned information level.");
    } catch (IllegalArgumentException e) {}
    dummyInformationLevel = InformationLevel.DEBUG;
    try {
      informationDAO.updateProgressJob(dummyJobId, dummyTimestamp, dummyInformationLevel, dummyText,
                                       dummyArgs);
      fail("Should not permit unassigned progress text.");
    } catch (IllegalArgumentException e) {}
    dummyText = "dummyText";
    dummyArgs = null;
    informationDAO.updateProgressJob(dummyJobId, dummyTimestamp, dummyInformationLevel, dummyText,
                                     dummyArgs);
  }

  // Wrote progress job is not argument aspect-protected so needs to be tested here. 
  @Test(expected=IllegalStateException.class)
  public void testWriteProgressJobExceptions() {
    dummySimulationId = -1l;
    try {
      informationDAO.createProgressJob(dummySimulationId, dummyJobId, dummyGroupName, dummyGroupLevel,
                                       dummyPacingFrequency, dummyTimestamp, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit invalid simulation id.");
    } catch (IllegalArgumentException e) {}
    dummySimulationId = 77l;
    dummyJobId = -99l;
    try {
      informationDAO.createProgressJob(dummySimulationId, dummyJobId, dummyGroupName, dummyGroupLevel,
                                       dummyPacingFrequency, dummyTimestamp, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit invalid job id.");
    } catch (IllegalArgumentException e) {}
    dummyJobId = 99l;
    try {
      informationDAO.createProgressJob(dummySimulationId, dummyJobId, dummyGroupName, dummyGroupLevel,
                                       dummyPacingFrequency, dummyTimestamp, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit null group name.");
    } catch (IllegalArgumentException e) {}
    dummyGroupName = "dummyGroupName";
    try {
      informationDAO.createProgressJob(dummySimulationId, dummyJobId, dummyGroupName, dummyGroupLevel,
                                       dummyPacingFrequency, dummyTimestamp, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit unassigned group level.");
    } catch (IllegalArgumentException e) {}
    dummyGroupLevel = -4;
    try {
      informationDAO.createProgressJob(dummySimulationId, dummyJobId, dummyGroupName, dummyGroupLevel,
                                       dummyPacingFrequency, dummyTimestamp, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit -ve group level.");
    } catch (IllegalArgumentException e) {}
    dummyGroupLevel = 4;
    try {
      informationDAO.createProgressJob(dummySimulationId, dummyJobId, dummyGroupName, dummyGroupLevel,
                                       dummyPacingFrequency, dummyTimestamp, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit unassigned frequency.");
    } catch (IllegalArgumentException e) {}
    dummyPacingFrequency = -3.2f;
    try {
      informationDAO.createProgressJob(dummySimulationId, dummyJobId, dummyGroupName, dummyGroupLevel,
                                       dummyPacingFrequency, dummyTimestamp, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit -ve pacing frequency.");
    } catch (IllegalArgumentException e) {}
    dummyPacingFrequency = 3.4f;
    try {
      informationDAO.createProgressJob(dummySimulationId, dummyJobId, dummyGroupName, dummyGroupLevel,
                                       dummyPacingFrequency, dummyTimestamp, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit null timestamp.");
    } catch (IllegalArgumentException e) {}
    dummyTimestamp = new Date();
    try {
      informationDAO.createProgressJob(dummySimulationId, dummyJobId, dummyGroupName, dummyGroupLevel,
                                       dummyPacingFrequency, dummyTimestamp, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit unassigned information level.");
    } catch (IllegalArgumentException e) {}
    dummyInformationLevel = InformationLevel.DEBUG;
    try {
      informationDAO.createProgressJob(dummySimulationId, dummyJobId, dummyGroupName, dummyGroupLevel,
                                       dummyPacingFrequency, dummyTimestamp, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit unassigned progress text.");
    } catch (IllegalArgumentException e) {}
    dummyText = "dummyText";
    dummyArgs = null;
    informationDAO.createProgressJob(dummySimulationId, dummyJobId, dummyGroupName, dummyGroupLevel,
                                     dummyPacingFrequency, dummyTimestamp, dummyInformationLevel,
                                     dummyText, dummyArgs);
  }

  @Test
  public void testCreateProgressAll() {
    dummyCompoundIdentifier = null;
    dummySimulationId = 1l;
    dummyTimestamp = new Date();
    dummyInformationLevel = InformationLevel.DEBUG;
    dummyText = "dummyText";
    dummyArgs = null;
    try {
      informationDAO.createProgressAll(dummyCompoundIdentifier, dummySimulationId, dummyTimestamp,
                                       dummyInformationLevel, dummyText, dummyArgs);
      fail("Should not permit invalid compound identifier value.");
    } catch (IllegalArgumentException e) {}
    dummyCompoundIdentifier = "dummyCompoundIdentifier";
    dummySimulationId = 0l;
    try {
      informationDAO.createProgressAll(dummyCompoundIdentifier, dummySimulationId, dummyTimestamp,
                                       dummyInformationLevel, dummyText, dummyArgs);
      fail("Should not permit invalid simulation id value.");
    } catch (IllegalArgumentException e) {}
    dummySimulationId = 29l;
    dummyTimestamp = null;
    try {
      informationDAO.createProgressAll(dummyCompoundIdentifier, dummySimulationId, dummyTimestamp,
                                       dummyInformationLevel, dummyText, dummyArgs);
      fail("Should not permit unassigned timestamp.");
    } catch (IllegalArgumentException e) {}
    dummyTimestamp = new Date();
    dummyInformationLevel = null;
    try {
      informationDAO.createProgressAll(dummyCompoundIdentifier, dummySimulationId, dummyTimestamp,
                                      dummyInformationLevel, dummyText, dummyArgs);
      fail("Should not permit unassigned information level.");
    } catch (IllegalArgumentException e) {}
    dummyInformationLevel = InformationLevel.DEBUG;
    dummyText = null;
    try {
      informationDAO.createProgressAll(dummyCompoundIdentifier, dummySimulationId, dummyTimestamp,
                                       dummyInformationLevel, dummyText, dummyArgs);
      fail("Should not permit unassigned progress text.");
    } catch (IllegalArgumentException e) {}

    dummyText = "dummyText";
    informationDAO.createProgressAll(dummyCompoundIdentifier, dummySimulationId, dummyTimestamp,
                                     dummyInformationLevel, dummyText, dummyArgs);
  }

  @Test
  public void testUpdateProgressAll() {
    dummyTimestamp = new Date();
    dummyInformationLevel = InformationLevel.DEBUG;
    dummyText = "dummyText";
    dummyArgs = null;
    dummySimulationId = 0l;
    try {
      informationDAO.updateProgressAll(dummySimulationId, dummyTimestamp, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit invalid simulation id value.");
    } catch (IllegalArgumentException e) {}
    dummySimulationId = 29l;
    dummyTimestamp = null;
    try {
      informationDAO.updateProgressAll(dummySimulationId, dummyTimestamp, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit unassigned timestamp.");
    } catch (IllegalArgumentException e) {}
    dummyTimestamp = new Date();
    dummyInformationLevel = null;
    try {
      informationDAO.updateProgressAll(dummySimulationId, dummyTimestamp, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit unassigned information level.");
    } catch (IllegalArgumentException e) {}
    dummyInformationLevel = InformationLevel.DEBUG;
    dummyText = null;
    try {
      informationDAO.updateProgressAll(dummySimulationId, dummyTimestamp, dummyInformationLevel,
                                       dummyText, dummyArgs);
      fail("Should not permit unassigned progress text.");
    } catch (IllegalArgumentException e) {}

    dummyText = "dummyText";
    informationDAO.updateProgressAll(dummySimulationId, dummyTimestamp, dummyInformationLevel,
                                     dummyText, dummyArgs);
  }
}