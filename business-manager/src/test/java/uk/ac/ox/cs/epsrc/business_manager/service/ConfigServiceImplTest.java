/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.service;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.epsrc.business_manager.config.ConfigurationActionPotential;
import uk.ac.ox.cs.epsrc.business_manager.config.ConfigurationDoseResponseFitting;
import uk.ac.ox.cs.epsrc.business_manager.value.object.CellModelVO;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.DefConAssayGroupStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.DefConAssayStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.DefConCellModelStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.DefConDRFStrategyHillFitting;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.DefConDoseResponseFittingStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.DefConIonChannelStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.DefConPC50EvaluationStrategyStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.DefConResponse;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayGroupVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 * Unit test the configuration service implementation object.
 *
 * @author 
 */
public class ConfigServiceImplTest {

  private static final float defaultDelta = 0.01f;

  private ConfigService configService;
  private Configuration mockConfiguration;
  private ConfigurationActionPotential mockConfigurationActionPotential;
  private ConfigurationDoseResponseFitting mockConfigurationDoseResponseFitting;
  private IMocksControl mocksControl;

  @Before
  public void setUp() {
    configService = new ConfigServiceImpl();

    mocksControl = createStrictControl();

    mockConfiguration = mocksControl.createMock(Configuration.class);
    mockConfigurationActionPotential = mocksControl.createMock(ConfigurationActionPotential.class);
    mockConfigurationDoseResponseFitting = mocksControl.createMock(ConfigurationDoseResponseFitting.class);

    ReflectionTestUtils.setField(configService, "configuration", mockConfiguration);
    ReflectionTestUtils.setField(configService, "configurationActionPotential",
                                 mockConfigurationActionPotential);
    ReflectionTestUtils.setField(configService, "configurationDoseResponseFitting",
                                 mockConfigurationDoseResponseFitting);
  }

  @Test
  public void testRetrieveDefaultConfigurations() {

    /*
     *  1. Emulate no default PC50 evaluation strategies, no dose response fitting strategy hill
     *       fitting, no assays, no assay groups, no ion channels, no cell models!
     */
    final List<PC50EvaluationStrategy> dummyDefaultPC50EvaluationStrategies = new ArrayList<PC50EvaluationStrategy>();
    expect(mockConfiguration.getDefaultPC50EvaluationStrategies()).andReturn(dummyDefaultPC50EvaluationStrategies);

    boolean dummyIsDefaultForceReRun = false;
    boolean dummyIsDefaultAssayGrouping = false;
    boolean dummyIsDefaultValueInheriting = true;
    boolean dummyIsDefaultBetweenGroups = false;
    boolean dummyIsDefaultWithinGroups = true;
    short dummyDefaultMinInformationLevel = 1;
    short dummyDefaultMaxInformationLevel = 2;

    expect(mockConfiguration.isDefaultForceReRun()).andReturn(dummyIsDefaultForceReRun);
    expect(mockConfiguration.isDefaultAssayGrouping()).andReturn(dummyIsDefaultAssayGrouping);
    expect(mockConfiguration.isDefaultValueInheriting()).andReturn(dummyIsDefaultValueInheriting);
    expect(mockConfiguration.isDefaultBetweenGroups()).andReturn(dummyIsDefaultBetweenGroups);
    expect(mockConfiguration.isDefaultWithinGroups()).andReturn(dummyIsDefaultWithinGroups);
    expect(mockConfiguration.getDefaultMinInformationLevel()).andReturn(dummyDefaultMinInformationLevel);
    expect(mockConfiguration.getDefaultMaxInformationLevel()).andReturn(dummyDefaultMaxInformationLevel);

    Map<String, Boolean> dummyDRFStrategyHillFitting = new HashMap<String, Boolean>();
    expect(mockConfigurationDoseResponseFitting.getDRFStrategyHillFitting())
          .andReturn(dummyDRFStrategyHillFitting);

    String dummyDefaultDoseResponseFittingStrategy = "dummyDefaultDoseResponseFittingStrategy";
    boolean dummyDefaultDoseResponseFittingRounded = false;
    float dummyDefaultDoseResponseFittingHillMax = 2.0f;
    float dummyDefaultDoseResponseFittingHillMin = 1.0f;
    expect(mockConfigurationDoseResponseFitting.getDefaultDoseResponseFittingStrategy())
          .andReturn(dummyDefaultDoseResponseFittingStrategy);
    expect(mockConfigurationDoseResponseFitting.isDefaultDoseResponseFittingRounded())
          .andReturn(dummyDefaultDoseResponseFittingRounded);
    expect(mockConfigurationDoseResponseFitting.getDefaultDoseResponseFittingHillMax())
          .andReturn(dummyDefaultDoseResponseFittingHillMax);
    expect(mockConfigurationDoseResponseFitting.getDefaultDoseResponseFittingHillMin())
          .andReturn(dummyDefaultDoseResponseFittingHillMin);

    Set<AssayVO> dummyAssays = new HashSet<AssayVO>();
    expect(mockConfiguration.getAssays()).andReturn(dummyAssays);

    Set<AssayGroupVO> dummyAssayGroups = new HashSet<AssayGroupVO>();
    expect(mockConfiguration.getAssayGroups()).andReturn(dummyAssayGroups);

    Set<IonChannel> dummyIonChannels = new HashSet<IonChannel>();
    expect(mockConfiguration.getIonChannels()).andReturn(dummyIonChannels);

    List<CellModelVO> dummyCellModels = new ArrayList<CellModelVO>();
    expect(mockConfigurationActionPotential.getCellModels()).andReturn(dummyCellModels);

    BigDecimal dummyDefaultPacingMaxTime = null;
    expect(mockConfigurationActionPotential.getDefaultPacingMaxTime())
          .andReturn(dummyDefaultPacingMaxTime);

    int dummyDefaultSimulationRequestProcessingPolling = 4500;
    expect(mockConfiguration.getDefaultRequestProcessingPolling())
          .andReturn(dummyDefaultSimulationRequestProcessingPolling);

    mocksControl.replay();

    DefConResponse response = configService.retrieveDefaultConfigurations(null);

    mocksControl.verify();

    assertSame(dummyDefaultMaxInformationLevel, response.getDefaultMaxInformationLevel());
    assertSame(dummyDefaultMinInformationLevel, response.getDefaultMinInformationLevel());
    assertSame(0, response.getDefConAssayGroupStructure().size());
    assertSame(0, response.getDefConAssayStructure().size());
    assertSame(0, response.getDefConCellModelStructure().size());
    DefConDoseResponseFittingStructure defConDoseResponseFittingStructure = response.getDefConDoseResponseFittingStructure();
    assertEquals(dummyDefaultDoseResponseFittingHillMax,
                 defConDoseResponseFittingStructure.getDefaultDoseResponseFittingHillMax(),
                 defaultDelta);
    assertEquals(dummyDefaultDoseResponseFittingHillMin,
                 defConDoseResponseFittingStructure.getDefaultDoseResponseFittingHillMin(),
                 defaultDelta);
    assertEquals(dummyDefaultDoseResponseFittingStrategy,
                 defConDoseResponseFittingStructure.getDefaultDoseResponseFittingStrategy());
    assertSame(dummyDefaultDoseResponseFittingRounded,
               defConDoseResponseFittingStructure.isDefaultDoseResponseFittingRounded());
    assertSame(0, response.getDefConIonChannelStructure().size());
    assertSame(0, response.getDefConPC50EvaluationStrategyStructure().size());

    assertFalse(response.isDefaultAssayGrouping());
    assertFalse(response.isDefaultBetweenGroups());
    assertFalse(response.isDefaultForceReRun());
    assertSame(Configuration.DEFAULT_FORCE_RESET, response.isDefaultReset());
    assertTrue(response.isDefaultValueInheriting());
    assertTrue(response.isDefaultWithinGroups());

    mocksControl.reset();

    /*
     * 2. Emulate all values assigned.
     */
    final PC50EvaluationStrategy mockPC50EvaluationStrategy = mocksControl.createMock(PC50EvaluationStrategy.class);
    dummyDefaultPC50EvaluationStrategies.add(mockPC50EvaluationStrategy);

    expect(mockConfiguration.getDefaultPC50EvaluationStrategies()).andReturn(dummyDefaultPC50EvaluationStrategies);

    final String dummyPC50EvaluationStrategyDesc = "dummyPC50EvaluationStrategyDesc";
    final int dummyPC50EvaluationStrategyInvOrd = 5;
    final boolean dummyPC50EvaluationStrategyAction = true;
    expect(mockPC50EvaluationStrategy.getOrder()).andReturn(dummyPC50EvaluationStrategyInvOrd);
    expect(mockPC50EvaluationStrategy.getDescription()).andReturn(dummyPC50EvaluationStrategyDesc);
    expect(mockPC50EvaluationStrategy.getOrder()).andReturn(dummyPC50EvaluationStrategyInvOrd);
    expect(mockPC50EvaluationStrategy.getDefaultActive()).andReturn(dummyPC50EvaluationStrategyAction);

    dummyIsDefaultForceReRun = true;
    dummyIsDefaultAssayGrouping = true;
    dummyIsDefaultBetweenGroups = true;

    expect(mockConfiguration.isDefaultForceReRun()).andReturn(dummyIsDefaultForceReRun);
    expect(mockConfiguration.isDefaultAssayGrouping()).andReturn(dummyIsDefaultAssayGrouping);
    expect(mockConfiguration.isDefaultValueInheriting()).andReturn(dummyIsDefaultValueInheriting);
    expect(mockConfiguration.isDefaultBetweenGroups()).andReturn(dummyIsDefaultBetweenGroups);
    expect(mockConfiguration.isDefaultWithinGroups()).andReturn(dummyIsDefaultWithinGroups);
    expect(mockConfiguration.getDefaultMinInformationLevel()).andReturn(dummyDefaultMinInformationLevel);
    expect(mockConfiguration.getDefaultMaxInformationLevel()).andReturn(dummyDefaultMaxInformationLevel);

    final String dummyDRFStrategy = "dummyDRFStrategy";
    final Boolean dummyDRFHillFitting = Boolean.TRUE;
    dummyDRFStrategyHillFitting.put(dummyDRFStrategy, dummyDRFHillFitting);
    expect(mockConfigurationDoseResponseFitting.getDRFStrategyHillFitting())
          .andReturn(dummyDRFStrategyHillFitting);
    dummyDefaultDoseResponseFittingRounded = true;

    expect(mockConfigurationDoseResponseFitting.getDefaultDoseResponseFittingStrategy())
          .andReturn(dummyDefaultDoseResponseFittingStrategy);
    expect(mockConfigurationDoseResponseFitting.isDefaultDoseResponseFittingRounded())
          .andReturn(dummyDefaultDoseResponseFittingRounded);
    expect(mockConfigurationDoseResponseFitting.getDefaultDoseResponseFittingHillMax())
          .andReturn(dummyDefaultDoseResponseFittingHillMax);
    expect(mockConfigurationDoseResponseFitting.getDefaultDoseResponseFittingHillMin())
          .andReturn(dummyDefaultDoseResponseFittingHillMin);

    final AssayVO mockAssayVO = mocksControl.createMock(AssayVO.class);
    dummyAssays.add(mockAssayVO);
    expect(mockConfiguration.getAssays()).andReturn(dummyAssays);
    final short dummyAssayLevel = 2;
    final String dummyAssayName = "dummyAssayName";
    expect(mockAssayVO.getLevel()).andReturn(dummyAssayLevel);
    expect(mockAssayVO.getName()).andReturn(dummyAssayName);

    final AssayGroupVO mockAssayGroupVO = mocksControl.createMock(AssayGroupVO.class);
    dummyAssayGroups.add(mockAssayGroupVO);
    expect(mockConfiguration.getAssayGroups()).andReturn(dummyAssayGroups);
    final short dummyAssayGroupLevel = 3;
    final String dummyAssayGroupName = "dummyAssayGroupName";
    expect(mockAssayGroupVO.getLevel()).andReturn(dummyAssayGroupLevel);
    expect(mockAssayGroupVO.getName()).andReturn(dummyAssayGroupName);

    final IonChannel fakeIonChannel = IonChannel.NaV1_5;
    dummyIonChannels.add(fakeIonChannel);

    expect(mockConfiguration.getIonChannels()).andReturn(dummyIonChannels);

    expect(mockConfigurationActionPotential.getCellModels()).andReturn(dummyCellModels);

    expect(mockConfigurationActionPotential.getDefaultPacingMaxTime())
          .andReturn(dummyDefaultPacingMaxTime);

    expect(mockConfiguration.getDefaultRequestProcessingPolling())
          .andReturn(dummyDefaultSimulationRequestProcessingPolling);

    mocksControl.replay();

    response = configService.retrieveDefaultConfigurations(null);

    mocksControl.verify();

    final List<DefConPC50EvaluationStrategyStructure> pc50EvaluationStrategies = response.getDefConPC50EvaluationStrategyStructure();
    assertSame(1, pc50EvaluationStrategies.size());
    final DefConPC50EvaluationStrategyStructure pc50EvaluationStrategy = pc50EvaluationStrategies.get(0);
    assertEquals(dummyPC50EvaluationStrategyInvOrd, pc50EvaluationStrategy.getId());
    assertEquals(dummyPC50EvaluationStrategyDesc, pc50EvaluationStrategy.getDescription());
    assertSame(dummyPC50EvaluationStrategyInvOrd, pc50EvaluationStrategy.getDefaultInvocationOrder());
    assertTrue(pc50EvaluationStrategy.isDefaultActive());
 
    assertTrue(response.isDefaultAssayGrouping());
    assertTrue(response.isDefaultBetweenGroups());
    assertTrue(response.isDefaultForceReRun());
    assertTrue(response.isDefaultValueInheriting());
    assertTrue(response.isDefaultWithinGroups());

    assertSame(dummyDefaultMaxInformationLevel, response.getDefaultMaxInformationLevel());
    assertSame(dummyDefaultMinInformationLevel, response.getDefaultMinInformationLevel());

    defConDoseResponseFittingStructure = response.getDefConDoseResponseFittingStructure();
    final List<DefConDRFStrategyHillFitting> drfStrategyHillFittings = defConDoseResponseFittingStructure.getDefConDRFStrategyHillFitting();
    assertSame(1, drfStrategyHillFittings.size());
    final DefConDRFStrategyHillFitting drfStrategyHillFitting = drfStrategyHillFittings.get(0);
    assertEquals(dummyDRFStrategy, drfStrategyHillFitting.getStrategy());
    assertTrue(drfStrategyHillFitting.isHillFitting());
    assertEquals(dummyDefaultDoseResponseFittingHillMax,
                 defConDoseResponseFittingStructure.getDefaultDoseResponseFittingHillMax(),
                 defaultDelta);
    assertEquals(dummyDefaultDoseResponseFittingHillMin,
                 defConDoseResponseFittingStructure.getDefaultDoseResponseFittingHillMin(),
                 defaultDelta);
    assertEquals(dummyDefaultDoseResponseFittingStrategy,
                 defConDoseResponseFittingStructure.getDefaultDoseResponseFittingStrategy());
    assertSame(dummyDefaultDoseResponseFittingRounded,
               defConDoseResponseFittingStructure.isDefaultDoseResponseFittingRounded());

    final List<DefConAssayStructure> assayStructures = response.getDefConAssayStructure();
    assertSame(1, assayStructures.size());
    final DefConAssayStructure assayStructure = assayStructures.get(0);
    assertSame(dummyAssayLevel, assayStructure.getLevel());
    assertEquals(dummyAssayName, assayStructure.getName());

    final List<DefConAssayGroupStructure> assayGroupStructures = response.getDefConAssayGroupStructure();
    assertSame(1, assayGroupStructures.size());
    final DefConAssayGroupStructure assayGroupStructure = assayGroupStructures.get(0);
    assertSame(dummyAssayGroupLevel, assayGroupStructure.getLevel());
    assertEquals(dummyAssayGroupName, assayGroupStructure.getName());

    final List<DefConIonChannelStructure> ionChannels = response.getDefConIonChannelStructure();
    assertSame(1, ionChannels.size());
    final DefConIonChannelStructure ionChannel = ionChannels.get(0);
    assertSame(fakeIonChannel.getDescription(), ionChannel.getDescription());
    assertSame(fakeIonChannel.name(), ionChannel.getName());
    assertSame(fakeIonChannel.getDisplayOrder(), ionChannel.getDisplayOrder());
  }

  @Test
  public void testRetrieveDefaultConfigurationsCellMLStuff() {
    /*
     * 1. Emulate CellML overriding default by specifying unlimited pacing time. 
     */
    final List<PC50EvaluationStrategy> dummyDefaultPC50EvaluationStrategies = new ArrayList<PC50EvaluationStrategy>();
    expect(mockConfiguration.getDefaultPC50EvaluationStrategies()).andReturn(dummyDefaultPC50EvaluationStrategies);

    boolean dummyIsDefaultForceReRun = false;
    boolean dummyIsDefaultAssayGrouping = false;
    boolean dummyIsDefaultValueInheriting = true;
    boolean dummyIsDefaultBetweenGroups = false;
    boolean dummyIsDefaultWithinGroups = true;
    short dummyDefaultMinInformationLevel = 1;
    short dummyDefaultMaxInformationLevel = 2;

    expect(mockConfiguration.isDefaultForceReRun()).andReturn(dummyIsDefaultForceReRun);
    expect(mockConfiguration.isDefaultAssayGrouping()).andReturn(dummyIsDefaultAssayGrouping);
    expect(mockConfiguration.isDefaultValueInheriting()).andReturn(dummyIsDefaultValueInheriting);
    expect(mockConfiguration.isDefaultBetweenGroups()).andReturn(dummyIsDefaultBetweenGroups);
    expect(mockConfiguration.isDefaultWithinGroups()).andReturn(dummyIsDefaultWithinGroups);
    expect(mockConfiguration.getDefaultMinInformationLevel()).andReturn(dummyDefaultMinInformationLevel);
    expect(mockConfiguration.getDefaultMaxInformationLevel()).andReturn(dummyDefaultMaxInformationLevel);

    Map<String, Boolean> dummyDRFStrategyHillFitting = new HashMap<String, Boolean>();
    expect(mockConfigurationDoseResponseFitting.getDRFStrategyHillFitting())
          .andReturn(dummyDRFStrategyHillFitting);

    String dummyDefaultDoseResponseFittingStrategy = "dummyDefaultDoseResponseFittingStrategy";
    boolean dummyDefaultDoseResponseFittingRounded = false;
    float dummyDefaultDoseResponseFittingHillMax = 2.0f;
    float dummyDefaultDoseResponseFittingHillMin = 1.0f;
    expect(mockConfigurationDoseResponseFitting.getDefaultDoseResponseFittingStrategy())
          .andReturn(dummyDefaultDoseResponseFittingStrategy);
    expect(mockConfigurationDoseResponseFitting.isDefaultDoseResponseFittingRounded())
          .andReturn(dummyDefaultDoseResponseFittingRounded);
    expect(mockConfigurationDoseResponseFitting.getDefaultDoseResponseFittingHillMax())
          .andReturn(dummyDefaultDoseResponseFittingHillMax);
    expect(mockConfigurationDoseResponseFitting.getDefaultDoseResponseFittingHillMin())
          .andReturn(dummyDefaultDoseResponseFittingHillMin);

    Set<AssayVO> dummyAssays = new HashSet<AssayVO>();
    expect(mockConfiguration.getAssays()).andReturn(dummyAssays);

    Set<AssayGroupVO> dummyAssayGroups = new HashSet<AssayGroupVO>();
    expect(mockConfiguration.getAssayGroups()).andReturn(dummyAssayGroups);

    Set<IonChannel> dummyIonChannels = new HashSet<IonChannel>();
    expect(mockConfiguration.getIonChannels()).andReturn(dummyIonChannels);

    List<CellModelVO> dummyCellModels = new ArrayList<CellModelVO>();
    final CellModelVO mockCellModelVO = mocksControl.createMock(CellModelVO.class);
    dummyCellModels.add(mockCellModelVO);
    expect(mockConfigurationActionPotential.getCellModels()).andReturn(dummyCellModels);
    BigDecimal dummyDefaultPacingMaxTime = null;
    expect(mockConfigurationActionPotential.getDefaultPacingMaxTime())
          .andReturn(dummyDefaultPacingMaxTime);

    final String dummyCellMLURL = "dummyCellMLURL";
    final boolean dummyCellMLIsDefaultModel = true;
    final String dummyCellMLDescription = "dummyCellMLDescription";
    final short dummyCellMLIdentifier = 5;
    final String dummyCellMLName = "dummyCellMLName";
    final String dummyCellMLPaperURL = "dummyCellMLPaperURL";
    boolean dummyIsUnlimitedPacingTime = true;
    expect(mockCellModelVO.getCellmlURL()).andReturn(dummyCellMLURL);
    expect(mockCellModelVO.isDefaultModel()).andReturn(dummyCellMLIsDefaultModel);
    expect(mockCellModelVO.getDescription()).andReturn(dummyCellMLDescription);
    expect(mockCellModelVO.getIdentifier()).andReturn(dummyCellMLIdentifier);
    expect(mockCellModelVO.getName()).andReturn(dummyCellMLName);
    expect(mockCellModelVO.getPaperURL()).andReturn(dummyCellMLPaperURL);
    expect(mockCellModelVO.isUnlimitedPacingTime()).andReturn(dummyIsUnlimitedPacingTime);


    int dummyDefaultSimulationRequestProcessingPolling = 3500;
    expect(mockConfiguration.getDefaultRequestProcessingPolling())
          .andReturn(dummyDefaultSimulationRequestProcessingPolling);

    mocksControl.replay();

    DefConResponse response = configService.retrieveDefaultConfigurations(null);

    mocksControl.verify();

    List<DefConCellModelStructure> cellModels = response.getDefConCellModelStructure();
    assertSame(1, cellModels.size());
    DefConCellModelStructure cellModel = cellModels.get(0);
    assertEquals(dummyCellMLURL, cellModel.getCellMLURL());
    assertTrue(cellModel.isDefault());
    assertEquals(dummyCellMLDescription, cellModel.getDescription());
    assertSame(dummyCellMLIdentifier, cellModel.getIdentifier());
    assertEquals(dummyCellMLName, cellModel.getName());
    assertEquals(dummyCellMLPaperURL, cellModel.getPaperURL());

    // Not asserting as have hardcoded a mock object response! 
    cellModel.getPacingMaxTime();

    /*
     * 2. Emulate CellML defining a maximum pacing time.
     */
    mocksControl.reset();

    expect(mockConfiguration.getDefaultPC50EvaluationStrategies()).andReturn(dummyDefaultPC50EvaluationStrategies);
    expect(mockConfiguration.isDefaultForceReRun()).andReturn(dummyIsDefaultForceReRun);
    expect(mockConfiguration.isDefaultAssayGrouping()).andReturn(dummyIsDefaultAssayGrouping);
    expect(mockConfiguration.isDefaultValueInheriting()).andReturn(dummyIsDefaultValueInheriting);
    expect(mockConfiguration.isDefaultBetweenGroups()).andReturn(dummyIsDefaultBetweenGroups);
    expect(mockConfiguration.isDefaultWithinGroups()).andReturn(dummyIsDefaultWithinGroups);
    expect(mockConfiguration.getDefaultMinInformationLevel()).andReturn(dummyDefaultMinInformationLevel);
    expect(mockConfiguration.getDefaultMaxInformationLevel()).andReturn(dummyDefaultMaxInformationLevel);
    expect(mockConfigurationDoseResponseFitting.getDRFStrategyHillFitting())
          .andReturn(dummyDRFStrategyHillFitting);
    expect(mockConfigurationDoseResponseFitting.getDefaultDoseResponseFittingStrategy())
          .andReturn(dummyDefaultDoseResponseFittingStrategy);
    expect(mockConfigurationDoseResponseFitting.isDefaultDoseResponseFittingRounded())
          .andReturn(dummyDefaultDoseResponseFittingRounded);
    expect(mockConfigurationDoseResponseFitting.getDefaultDoseResponseFittingHillMax())
          .andReturn(dummyDefaultDoseResponseFittingHillMax);
    expect(mockConfigurationDoseResponseFitting.getDefaultDoseResponseFittingHillMin())
          .andReturn(dummyDefaultDoseResponseFittingHillMin);
    expect(mockConfiguration.getAssays()).andReturn(dummyAssays);
    expect(mockConfiguration.getAssayGroups()).andReturn(dummyAssayGroups);
    expect(mockConfiguration.getIonChannels()).andReturn(dummyIonChannels);
    expect(mockConfigurationActionPotential.getCellModels()).andReturn(dummyCellModels);
    expect(mockConfigurationActionPotential.getDefaultPacingMaxTime())
          .andReturn(dummyDefaultPacingMaxTime);

    dummyIsUnlimitedPacingTime = false;
    BigDecimal dummyCellMLPacingMaxTime = BigDecimal.ONE;
    expect(mockCellModelVO.getCellmlURL()).andReturn(dummyCellMLURL);
    expect(mockCellModelVO.isDefaultModel()).andReturn(dummyCellMLIsDefaultModel);
    expect(mockCellModelVO.getDescription()).andReturn(dummyCellMLDescription);
    expect(mockCellModelVO.getIdentifier()).andReturn(dummyCellMLIdentifier);
    expect(mockCellModelVO.getName()).andReturn(dummyCellMLName);
    expect(mockCellModelVO.getPaperURL()).andReturn(dummyCellMLPaperURL);
    expect(mockCellModelVO.isUnlimitedPacingTime()).andReturn(dummyIsUnlimitedPacingTime);
    expect(mockCellModelVO.getPacingMaxTime()).andReturn(dummyCellMLPacingMaxTime);

    expect(mockConfiguration.getDefaultRequestProcessingPolling())
          .andReturn(dummyDefaultSimulationRequestProcessingPolling);

    mocksControl.replay();

    response = configService.retrieveDefaultConfigurations(null);

    mocksControl.verify();

    cellModels = response.getDefConCellModelStructure();
    assertSame(1, cellModels.size());
    cellModel = cellModels.get(0);

    assertTrue(dummyCellMLPacingMaxTime.compareTo(cellModel.getPacingMaxTime()) == 0);

    /*
     * 3. Emulate CellML not defining a maximum pacing time, therefore using default.
     */
    mocksControl.reset();

    expect(mockConfiguration.getDefaultPC50EvaluationStrategies()).andReturn(dummyDefaultPC50EvaluationStrategies);
    expect(mockConfiguration.isDefaultForceReRun()).andReturn(dummyIsDefaultForceReRun);
    expect(mockConfiguration.isDefaultAssayGrouping()).andReturn(dummyIsDefaultAssayGrouping);
    expect(mockConfiguration.isDefaultValueInheriting()).andReturn(dummyIsDefaultValueInheriting);
    expect(mockConfiguration.isDefaultBetweenGroups()).andReturn(dummyIsDefaultBetweenGroups);
    expect(mockConfiguration.isDefaultWithinGroups()).andReturn(dummyIsDefaultWithinGroups);
    expect(mockConfiguration.getDefaultMinInformationLevel()).andReturn(dummyDefaultMinInformationLevel);
    expect(mockConfiguration.getDefaultMaxInformationLevel()).andReturn(dummyDefaultMaxInformationLevel);
    expect(mockConfigurationDoseResponseFitting.getDRFStrategyHillFitting())
          .andReturn(dummyDRFStrategyHillFitting);
    expect(mockConfigurationDoseResponseFitting.getDefaultDoseResponseFittingStrategy())
          .andReturn(dummyDefaultDoseResponseFittingStrategy);
    expect(mockConfigurationDoseResponseFitting.isDefaultDoseResponseFittingRounded())
          .andReturn(dummyDefaultDoseResponseFittingRounded);
    expect(mockConfigurationDoseResponseFitting.getDefaultDoseResponseFittingHillMax())
          .andReturn(dummyDefaultDoseResponseFittingHillMax);
    expect(mockConfigurationDoseResponseFitting.getDefaultDoseResponseFittingHillMin())
          .andReturn(dummyDefaultDoseResponseFittingHillMin);
    expect(mockConfiguration.getAssays()).andReturn(dummyAssays);
    expect(mockConfiguration.getAssayGroups()).andReturn(dummyAssayGroups);
    expect(mockConfiguration.getIonChannels()).andReturn(dummyIonChannels);
    expect(mockConfigurationActionPotential.getCellModels()).andReturn(dummyCellModels);
    dummyDefaultPacingMaxTime = BigDecimal.TEN;
    expect(mockConfigurationActionPotential.getDefaultPacingMaxTime())
          .andReturn(dummyDefaultPacingMaxTime);

    dummyCellMLPacingMaxTime = null;
    expect(mockCellModelVO.getCellmlURL()).andReturn(dummyCellMLURL);
    expect(mockCellModelVO.isDefaultModel()).andReturn(dummyCellMLIsDefaultModel);
    expect(mockCellModelVO.getDescription()).andReturn(dummyCellMLDescription);
    expect(mockCellModelVO.getIdentifier()).andReturn(dummyCellMLIdentifier);
    expect(mockCellModelVO.getName()).andReturn(dummyCellMLName);
    expect(mockCellModelVO.getPaperURL()).andReturn(dummyCellMLPaperURL);
    expect(mockCellModelVO.isUnlimitedPacingTime()).andReturn(dummyIsUnlimitedPacingTime);
    expect(mockCellModelVO.getPacingMaxTime()).andReturn(dummyCellMLPacingMaxTime);

    expect(mockConfiguration.getDefaultRequestProcessingPolling())
          .andReturn(dummyDefaultSimulationRequestProcessingPolling);

    mocksControl.replay();

    response = configService.retrieveDefaultConfigurations(null);

    mocksControl.verify();

    cellModels = response.getDefConCellModelStructure();
    assertSame(1, cellModels.size());
    cellModel = cellModels.get(0);

    assertEquals(dummyDefaultPacingMaxTime, cellModel.getPacingMaxTime());
  }
}