/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.selection.maxrespconc;

import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.business.selection.maxrespconc.AnyMaxRespData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.individual.MaxRespConcData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;

/**
 * Unit test the selector whereby there's any max response data present.
 *
 * @author geoff
 */
public class AnyMaxRespDataTest {

  private AnyMaxRespData anyMaxRespData;

  @Before
  public void setUp() {
    anyMaxRespData = new AnyMaxRespData();
  }

  @Test(expected=IllegalArgumentException.class)
  public void testSelectFailsOnNullOptionsArg() {
    anyMaxRespData.select(null);
  }

  @Test
  public void testEmptyOptionsArg() {
    final List<MaxRespConcData> dummyMaxRespConcData = new ArrayList<MaxRespConcData>();
    final MaxRespConcData selected = anyMaxRespData.select(dummyMaxRespConcData);
    assertNull(selected);
  }

  @Test
  public void testSelectWithInitialisedCollection() {
    final List<MaxRespConcData> dummyMaxRespConcData = new ArrayList<MaxRespConcData>();
    final IMocksControl mocksControl = createControl();
    final MaxRespConcData mockMaxRespConcData = mocksControl.createMock(MaxRespConcData.class);
    dummyMaxRespConcData.add(mockMaxRespConcData);

    expect(mockMaxRespConcData.getMaxResp()).andReturn(null);
    mocksControl.replay();
    MaxRespConcData selected = anyMaxRespData.select(dummyMaxRespConcData);
    mocksControl.verify();

    assertNull(selected);

    mocksControl.reset();

    final BigDecimal dummyMaxResp = BigDecimal.TEN;
    expect(mockMaxRespConcData.getMaxResp()).andReturn(dummyMaxResp);
    expect(mockMaxRespConcData.getMaxRespConc(Unit.M)).andReturn(null);
    mocksControl.replay();

    selected = anyMaxRespData.select(dummyMaxRespConcData);
    mocksControl.verify();

    assertNull(selected);

    mocksControl.reset();

    expect(mockMaxRespConcData.getMaxResp()).andReturn(dummyMaxResp);
    final BigDecimal dummyMaxRespConc = BigDecimal.ONE; 
    expect(mockMaxRespConcData.getMaxRespConc(Unit.M)).andReturn(dummyMaxRespConc);
    mocksControl.replay();

    selected = anyMaxRespData.select(dummyMaxRespConcData);
    mocksControl.verify();

    assertNotNull(selected);
  }
}