/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.business_manager.request_validator;

import static org.easymock.EasyMock.createControl;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.ws.business_manager.request_validator.SimulationsRequestValidatorChain;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProcessSimulationsRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.SchemaObjectBuilder;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidCompoundIdentifierException;
import uk.ac.ox.cs.nc3rs.business_manager.api.request.validator.CompoundIdentifiersValidator;

/**
 * Unit test the simulations request validator chain activities.
 *
 * @author geoff
 */
public class SimulationsRequestValidatorChainTest {

  private SimulationsRequestValidatorChain validatorChain;

  @Before
  public void setUp() {
    validatorChain = new SimulationsRequestValidatorChain();
  }

  /**
   * Test that null requests are rejected.
   */
  @Test(expected=IllegalArgumentException.class)
  public void testValidationFailsOnNullRequest() throws InvalidCompoundIdentifierException {
    validatorChain.simulationsRequestValidation(null);
    fail("A null process simulation request should not be permitted.");
  }

  /**
   * Test that doesn't fail if no validators are configured for the chain.
   */
  @Test
  public void testSuccessEvenWhenNoValidatorsDefined() throws InvalidCompoundIdentifierException {
    final ProcessSimulationsRequest processSimulationsRequest = SchemaObjectBuilder.createValidProcessSimulationsRequest();
    validatorChain.simulationsRequestValidation(processSimulationsRequest);
  }

  /**
   * Test that doesn't fail if no validators are configured for the chain.
   */
  @Test
  public void testSuccessWhenValidatorsDefined() throws InvalidCompoundIdentifierException {
    final ProcessSimulationsRequest processSimulationsRequest = SchemaObjectBuilder.createValidProcessSimulationsRequest();

    final CompoundIdentifiersValidator mockSimulationsRequestValidator = createControl().createMock(CompoundIdentifiersValidator.class);

    final List<CompoundIdentifiersValidator> simulationRequestValidators = new ArrayList<CompoundIdentifiersValidator>();
    simulationRequestValidators.add(mockSimulationsRequestValidator);
    ReflectionTestUtils.setField(validatorChain, "compoundIdentifiersValidators", 
                                 simulationRequestValidators);
    validatorChain.simulationsRequestValidation(processSimulationsRequest);
  }
}