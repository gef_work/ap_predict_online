/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.config.ConfigurationDoseResponseFitting;

/**
 * Unit test the dose-response fitting configuration.
 *
 * @author geoff
 */
public class ConfigurationDoseResponseFittingTest {

  private ConfigurationDoseResponseFitting configurationDoseResponseFitting;
  private String dummyDefaultDoseResponseFittingStrategy;
  private boolean dummyDefaultDoseResponseFittingRounded;
  private float dummyDefaultDoseResponseFittingHillMax;
  private float dummyDefaultDoseResponseFittingHillMin;

  @Before
  public void setUp() {
    dummyDefaultDoseResponseFittingStrategy = "dummyDefaultDoseResponseFittingStrategy";
    dummyDefaultDoseResponseFittingRounded = false;
    dummyDefaultDoseResponseFittingHillMax = 1.0f;
    dummyDefaultDoseResponseFittingHillMin = 3.0f;
  }

  @Test
  public void testConstructor() {
    configurationDoseResponseFitting = new ConfigurationDoseResponseFitting(dummyDefaultDoseResponseFittingStrategy,
                                                                            dummyDefaultDoseResponseFittingRounded,
                                                                            dummyDefaultDoseResponseFittingHillMin,
                                                                            dummyDefaultDoseResponseFittingHillMax);
    assertTrue(dummyDefaultDoseResponseFittingHillMax ==
               configurationDoseResponseFitting.getDefaultDoseResponseFittingHillMax());
    assertTrue(dummyDefaultDoseResponseFittingHillMin ==
               configurationDoseResponseFitting.getDefaultDoseResponseFittingHillMin());
    assertEquals(dummyDefaultDoseResponseFittingStrategy,
                 configurationDoseResponseFitting.getDefaultDoseResponseFittingStrategy());
    assertTrue(configurationDoseResponseFitting.getDRFStrategyHillFitting().isEmpty());
    assertEquals(dummyDefaultDoseResponseFittingRounded,
                 configurationDoseResponseFitting.isDefaultDoseResponseFittingRounded());
  }

  @Test
  public void testStrategies() {
    configurationDoseResponseFitting = new ConfigurationDoseResponseFitting(dummyDefaultDoseResponseFittingStrategy,
                                                                            dummyDefaultDoseResponseFittingRounded,
                                                                            dummyDefaultDoseResponseFittingHillMin,
                                                                            dummyDefaultDoseResponseFittingHillMax);

    Map<String, Boolean> dummyDRFStrategyHillFitting = null;
    configurationDoseResponseFitting.setDRFStrategyHillFitting(dummyDRFStrategyHillFitting);
    assertTrue(configurationDoseResponseFitting.getDRFStrategyHillFitting().isEmpty());

    final String dummyStrategy1 = "dummyStrategy1";
    final String dummyStrategy2 = "dummyStrategy2";
    final Boolean dummyIsHillFittingStrategy1 = Boolean.FALSE;
    final Boolean dummyIsHillFittingStrategy2 = Boolean.TRUE;

    dummyDRFStrategyHillFitting = new HashMap<String, Boolean>();
    dummyDRFStrategyHillFitting.put(dummyStrategy1, dummyIsHillFittingStrategy1);
    dummyDRFStrategyHillFitting.put(dummyStrategy2, dummyIsHillFittingStrategy2);
    configurationDoseResponseFitting.setDRFStrategyHillFitting(dummyDRFStrategyHillFitting);
    assertEquals(dummyDRFStrategyHillFitting.size(),
                 configurationDoseResponseFitting.getDRFStrategyHillFitting().size());

    String dummyFittingStrategy = null;
    try {
      configurationDoseResponseFitting.isHillFittingStrategy(dummyFittingStrategy);
      fail("Should not permit the querying of fitting strategies with a null arg.");
    } catch (IllegalArgumentException e) {}

    dummyFittingStrategy = "dummyFittingStrategy";
    try {
      configurationDoseResponseFitting.isHillFittingStrategy(dummyFittingStrategy);
      fail("Should not permit the querying of fitting strategies with an unrecognised strategy arg.");
    } catch (IllegalArgumentException e) {}

    dummyFittingStrategy = dummyStrategy2;
    assertTrue(configurationDoseResponseFitting.isHillFittingStrategy(dummyFittingStrategy));

    final Map<String, Boolean> dummyDRFStrategyHillFitting2 = new HashMap<String, Boolean>();
    try {
      configurationDoseResponseFitting.setDRFStrategyHillFitting(dummyDRFStrategyHillFitting2);
      fail("Should not permit the reassignment of the dose-response, hill-fitting strategies.");
    } catch (IllegalArgumentException e) {}
  }
}