/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual.doseresponse;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50.individual.doseresponse.MaxRespAndConc;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.IndividualProvenance;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.Provenance;
import uk.ac.ox.cs.epsrc.business_manager.business.selection.maxrespconc.IndividualMaxRespAndConcDataProcessor;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.DoseResponseData;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.DoseResponseFittingParams;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.individual.MaxRespConcData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.IndividualProcessing;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;

/**
 * Unit test the max response and concentration individual PC50 evaluation strategy. 
 *
 * @author geoff
 */
public class MaxRespAndConcTest {
  private BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate;
  private IMocksControl mocksControl;
  private IndividualMaxRespAndConcDataProcessor mockDataProcessor;
  private MaxRespAndConc maxRespAndConc;
  private final String dummyDescription = "dummyDescription";
  private final boolean dummyDefaultActive = true;
  private final int dummyDefaultInvocationOrder = 1;
  private final long dummySimulationId = 1l;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    mockDataProcessor = mocksControl.createMock(IndividualMaxRespAndConcDataProcessor.class);

    maxRespAndConc = new MaxRespAndConc(dummyDescription, dummyDefaultActive,
                                        dummyDefaultInvocationOrder);
    ReflectionTestUtils.setField(maxRespAndConc, "processor", mockDataProcessor);
    ReflectionTestUtils.setField(maxRespAndConc, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
  }

  @Test
  public void testConstructor() {
    assertNotNull(maxRespAndConc.getSysProvenanceStrategyIdentifier());
  }

  @Test
  public void testRetrieveDoseResponseDataWhenNoAvailableData() throws InvalidValueException {
    // Non-provenanceable processing (no individual data id).
    IndividualProcessing mockIndividualProcessing = mocksControl.createMock(IndividualProcessing.class); 
    ICData mockIndividualC50Data = mocksControl.createMock(ICData.class);
    final String dummyRecordIdentifier = "dummyRecordIdentifier";
    DoseResponseFittingParams mockDoseResponseFittingParams = mocksControl.createMock(DoseResponseFittingParams.class);
    final String dummyIndividualDataId = "dummyIndividualDataId";
    boolean dummyProvenanceable = false;

    expect(mockDataProcessor.individualDataMaxRespAndConc(mockIndividualProcessing)).andReturn(null);

    mocksControl.replay();

    DoseResponseData data = maxRespAndConc.retrieveDoseResponseData(dummySimulationId,
                                                                    mockIndividualProcessing,
                                                                    mockIndividualC50Data,
                                                                    dummyRecordIdentifier,
                                                                    mockDoseResponseFittingParams,
                                                                    dummyIndividualDataId,
                                                                    dummyProvenanceable);

    mocksControl.verify();

    assertNull(data);

    mocksControl.reset();

    // Now try with provenanceable data (with individual data id).
    dummyProvenanceable = true;

    expect(mockDataProcessor.individualDataMaxRespAndConc(mockIndividualProcessing)).andReturn(null);
    final Capture<Provenance> dummyProvenance = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(dummyProvenance));

    mocksControl.replay();

    data = maxRespAndConc.retrieveDoseResponseData(dummySimulationId, mockIndividualProcessing,
                                                   mockIndividualC50Data, dummyRecordIdentifier,
                                                   mockDoseResponseFittingParams,
                                                   dummyIndividualDataId, dummyProvenanceable);

    mocksControl.verify();

    IndividualProvenance capturedProvenance = (IndividualProvenance) dummyProvenance.getValue();
    assertEquals(dummyIndividualDataId, capturedProvenance.getIndividualDataId());
    assertTrue(InformationLevel.TRACE.compareTo(capturedProvenance.getLevel()) == 0);
  }

  @Test
  public void testFittingDoseResponseDataWhenAvailableData() throws InvalidValueException {
    IndividualProcessing mockIndividualProcessing = mocksControl.createMock(IndividualProcessing.class);
    ICData mockIndividualC50Data = mocksControl.createMock(ICData.class);
    String dummyRecordIdentifier = "dummyRecordIdentifier";
    DoseResponseFittingParams mockDoseResponseFittingParams = mocksControl.createMock(DoseResponseFittingParams.class);
    final String dummyIndividualDataId = "dummyIndividualDataId";
    boolean dummyProvenanceable = false;

    final MaxRespConcData mockMaxRespConcData = mocksControl.createMock(MaxRespConcData.class);
    expect(mockDataProcessor.individualDataMaxRespAndConc(mockIndividualProcessing))
          .andReturn(mockMaxRespConcData);

    expect(mockMaxRespConcData.getMaxRespConc(Unit.uM)).andReturn(BigDecimal.ONE);
    expect(mockMaxRespConcData.getMaxResp()).andReturn(BigDecimal.TEN);

    mocksControl.replay();

    DoseResponseData data = maxRespAndConc.retrieveDoseResponseData(dummySimulationId,
                                                                    mockIndividualProcessing,
                                                                    mockIndividualC50Data,
                                                                    dummyRecordIdentifier,
                                                                    mockDoseResponseFittingParams,
                                                                    dummyIndividualDataId,
                                                                    dummyProvenanceable);

    mocksControl.verify();

    assertNotNull(data);
  }
}