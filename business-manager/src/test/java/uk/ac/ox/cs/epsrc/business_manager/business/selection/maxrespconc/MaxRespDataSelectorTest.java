/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.selection.maxrespconc;

import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.business.selection.maxrespconc.MaxRespDataSelector;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.selection.maxrespconc.MaxRespDataSelectionStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.individual.MaxRespConcData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.Unit;

/**
 * Unit test the max response data selection.
 *
 * @author geoff
 */
public class MaxRespDataSelectorTest {

  @Test
  public void testConstructor() {
    new MaxRespDataSelector();
  }

  /**
   * Test null or empty eligible collection reactions.
   */
  @Test
  public void testNullOrEmptyEligibleCollection() {
    List<MaxRespConcData> dummyEligibleMaxRespData = null;
    try {
      MaxRespDataSelector.chooseMaxRespData(dummyEligibleMaxRespData);
      fail("Should not allow a null eligibleMaxRespData parameter to be used.");
    } catch (IllegalArgumentException e) {}

    dummyEligibleMaxRespData = new ArrayList<MaxRespConcData>();
    final MaxRespConcData maxRespConcData = MaxRespDataSelector.chooseMaxRespData(dummyEligibleMaxRespData);
    assertNull(maxRespConcData);
  }

  /**
   * Test valid eligible data processing.
   */
  @Test
  public void testAssignDataValidEligibleCollectionProcessing() {
    final List<MaxRespConcData> dummyEligibleMaxRespData = new ArrayList<MaxRespConcData>();
    final IMocksControl mocksControl = createControl();
    final MaxRespConcData mockMaxRespConcData = mocksControl.createMock(MaxRespConcData.class);
    dummyEligibleMaxRespData.add(mockMaxRespConcData);
    // mock there being no max resp and conc data!
    expect(mockMaxRespConcData.getMaxResp()).andReturn(null).anyTimes();
    expect(mockMaxRespConcData.getMaxRespConc(Unit.M)).andReturn(null).anyTimes();

    mocksControl.replay();
    MaxRespConcData maxRespConcData = MaxRespDataSelector.chooseMaxRespData(dummyEligibleMaxRespData);
    mocksControl.verify();

    assertNull(maxRespConcData);

    mocksControl.reset();

    // mock there being max resp and conc data!
    final BigDecimal dummyResponse = BigDecimal.TEN;
    expect(mockMaxRespConcData.getMaxResp()).andReturn(dummyResponse).anyTimes();
    final BigDecimal dummyResponseConc = BigDecimal.ONE;
    expect(mockMaxRespConcData.getMaxRespConc(Unit.M)).andReturn(dummyResponseConc).anyTimes();

    mocksControl.replay();
    maxRespConcData = MaxRespDataSelector.chooseMaxRespData(dummyEligibleMaxRespData);
    mocksControl.verify();

    assertNotNull(maxRespConcData);
  }

  /**
   * Test setting the strategies
   */
  @Test
  public void testSettingMaxRespDataSelectionStrategies() {
    List<MaxRespDataSelectionStrategy> dummyMaxRespDataSelectionStrategies = null;
    try {
      MaxRespDataSelector.setMaxRespDataSelectionStrategies(dummyMaxRespDataSelectionStrategies);
      fail("Should not permit assignment of a null collection of new strategies.");
    } catch (IllegalArgumentException e) {}

    dummyMaxRespDataSelectionStrategies = new ArrayList<MaxRespDataSelectionStrategy>();
    try {
      MaxRespDataSelector.setMaxRespDataSelectionStrategies(dummyMaxRespDataSelectionStrategies);
      fail("Should not permit assignment of an empty collection of new strategies.");
    } catch (IllegalArgumentException e) {}

    final IMocksControl mocksControl = createControl();
    final MaxRespDataSelectionStrategy mockMaxRespDataSelectionStrategy = mocksControl.createMock(MaxRespDataSelectionStrategy.class);
    dummyMaxRespDataSelectionStrategies.add(mockMaxRespDataSelectionStrategy);

    MaxRespDataSelector.setMaxRespDataSelectionStrategies(dummyMaxRespDataSelectionStrategies);
  }
}