/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.app_manager.manager;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.entity.GroupedInvocationInput;
import uk.ac.ox.cs.epsrc.business_manager.entity.IonChannelValues;
import uk.ac.ox.cs.epsrc.business_manager.entity.Job;
import uk.ac.ox.cs.epsrc.business_manager.entity.PIC50Data;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.manager.SimulationManager;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ApPredictRunRequest;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.AssociatedItem;
import uk.ac.ox.cs.nc3rs.app_manager.ws._1.ChannelData;
import uk.ac.ox.cs.nc3rs.business_manager.api.util.AssayUtil;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 *
 *
 * @author geoff
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( { "classpath:/uk/ac/ox/cs/epsrc/business_manager/jms/mockJMS-context.xml",
                         "classpath:/uk/ac/ox/cs/epsrc/business_manager/service/appCtx.experimentalService.xml",
                         "classpath:/uk/ac/ox/cs/epsrc/business_manager/ws/app_manager/manager/mockContext.xml",
                         "classpath:/META-INF/spring/ctx/config/appCtx.config.xml",
                         "classpath:/META-INF/spring/ctx/config/appCtx.config.actionPotential.site.xml",
                         "classpath:/META-INF/spring/ctx/config/appCtx.config.site.xml",
                         "classpath:/META-INF/spring/ctx/config/appCtx.config.assays.site.xml",
                         "classpath:/META-INF/spring/ctx/config/appCtx.config.cellModels.site.xml",
                         "classpath:/META-INF/spring/ctx/config/appCtx.config.changeCheckers.site.xml",
                         "classpath:/META-INF/spring/ctx/config/appCtx.config.doseResponseFitting.site.xml",
                         "classpath:/META-INF/spring/ctx/config/appCtx.config.ionChannels.site.xml",
                         "classpath:/META-INF/spring/ctx/config/appCtx.config.pc50Evaluators.site.xml",
                         "classpath:/META-INF/spring/ctx/integration/simulation/processing/screening/appCtx.proc.dose_response_fitting.xml" } )
public class AppManagerManagerImplIT {

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_APP_MANAGER_MANAGER)
  private AppManagerManager appManagerManager;

  private BigDecimal dummyMaxPacingTime = BigDecimal.ONE;
  private boolean dummyIsUsingDefaultCompoundConcentrations = true;
  private Float dummyPacingFrequency = 0.5F;
  private GroupedInvocationInput mockGroupedInvocationInput;
  private IMocksControl mocksControl;
  private Job mockJob;
  private Long dummySimulationId = 1L;
  private Set<Float> dummyCompoundConcentrations = new HashSet<Float>();
  private Set<IonChannelValues> dummyIonChannelValues = new HashSet<IonChannelValues>();
  private short dummyCellModelIdentifier = 1;
  private Simulation mockSimulation;
  private SimulationManager mockSimulationManager;
  private String dummyAssayGroupName = "dummyAssayGroupName";

  @Before
  public void setUp() {
    dummyIonChannelValues = new HashSet<IonChannelValues>();

    mocksControl = createStrictControl();
    mockGroupedInvocationInput = mocksControl.createMock(GroupedInvocationInput.class);
    mockJob = mocksControl.createMock(Job.class);
    mockSimulation = mocksControl.createMock(Simulation.class);
    mockSimulationManager = mocksControl.createMock(SimulationManager.class);
    // Replacing the context-defined mock simulation manager with the IMocksControl'led one.
    ReflectionTestUtils.setField(appManagerManager, BusinessIdentifiers.COMPONENT_SIMULATION_MANAGER,
                                 mockSimulationManager);
  }

  private void setCommonOptions() {
    expect(mockJob.getPacingFrequency()).andReturn(dummyPacingFrequency);
    final Float dummyCompoundConcentration1 = Float.valueOf("0.001");
    final Float dummyCompoundConcentration2 = Float.valueOf("0.01");
    dummyCompoundConcentrations.add(dummyCompoundConcentration1);
    dummyCompoundConcentrations.add(dummyCompoundConcentration2);
    expect(mockJob.getCompoundConcentrations()).andReturn(dummyCompoundConcentrations);
    expect(mockJob.getGroupedInvocationInput()).andReturn(mockGroupedInvocationInput);
    expect(mockGroupedInvocationInput.getAssayGroupName()).andReturn(dummyAssayGroupName);
  }

  @Test
  public void testCreateRunRequestUnspreadable() {
    setCommonOptions();

    final IonChannelValues mockIonChannelValuesUnspreadable = mocksControl.createMock(IonChannelValues.class);
    dummyIonChannelValues.add(mockIonChannelValuesUnspreadable);

    expect(mockGroupedInvocationInput.getIonChannelValues()).andReturn(dummyIonChannelValues);
    expect(mockSimulationManager.find(dummySimulationId)).andReturn(mockSimulation);
    expect(mockSimulation.getCellModelIdentifier()).andReturn(dummyCellModelIdentifier);
    expect(mockSimulation.getPacingMaxTime()).andReturn(dummyMaxPacingTime);
    expect(mockJob.isUsingConfigurationCompoundConcentrations()).andReturn(dummyIsUsingDefaultCompoundConcentrations);

    // Testing to determine if all source assays are spreadable, which it is not.
    final String dummySourceAssayNameUnspreadable = AssayUtil.ASSAY_NAME_QSAR;
    expect(mockIonChannelValuesUnspreadable.getSourceAssayName()).andReturn(dummySourceAssayNameUnspreadable);
    final String dummyIonChannelNameUnspreadableCaV = IonChannel.CaV1_2.toString();
    expect(mockIonChannelValuesUnspreadable.getIonChannelName()).andReturn(dummyIonChannelNameUnspreadableCaV).times(2);

    // Iterating through the incoming ion channel values.
    expect(mockIonChannelValuesUnspreadable.getSourceAssayName()).andReturn(dummySourceAssayNameUnspreadable);
    final List<PIC50Data> dummyAllPIC50DataUnspreadableCaV = new ArrayList<PIC50Data>();
    final PIC50Data mockPIC50DataUnspreadableCaVPIC50Data = mocksControl.createMock(PIC50Data.class);
    dummyAllPIC50DataUnspreadableCaV.add(mockPIC50DataUnspreadableCaVPIC50Data);
    expect(mockIonChannelValuesUnspreadable.getpIC50Data()).andReturn(dummyAllPIC50DataUnspreadableCaV);
    final BigDecimal dummyCaVPIC50 = BigDecimal.TEN;
    expect(mockPIC50DataUnspreadableCaVPIC50Data.getValue()).andReturn(dummyCaVPIC50);
    final BigDecimal dummyCaVHill = BigDecimal.ZERO;
    expect(mockPIC50DataUnspreadableCaVPIC50Data.getHill()).andReturn(dummyCaVHill);

    mocksControl.replay();

    final ApPredictRunRequest request = appManagerManager.createRunRequest(dummySimulationId,
                                                                           mockJob);

    mocksControl.verify();

    assertNotNull(request.getICaL());
    final ChannelData channelDataICaL = request.getICaL();
    final BigDecimal channelDataICaLC50Spread = channelDataICaL.getC50Spread();
    assertNull(channelDataICaLC50Spread);                  // not spreadable!
    final BigDecimal channelDataICaLHillSpread = channelDataICaL.getHillSpread();
    assertNull(channelDataICaLHillSpread);                 // not spreadable!
    final List<AssociatedItem> channelDataICaLIC50Data = channelDataICaL.getIC50Data();
    assertTrue(channelDataICaLIC50Data.isEmpty());
    final List<AssociatedItem> channelDataICaLPIC50Data = channelDataICaL.getPIC50Data();
    assertSame(1, channelDataICaLPIC50Data.size());
    final AssociatedItem retrievedPIC50DataUnspreadableCaVPIC50Data  = channelDataICaLPIC50Data.get(0);
    assertTrue(dummyCaVPIC50.compareTo(retrievedPIC50DataUnspreadableCaVPIC50Data.getC50()) == 0);
    assertTrue(dummyCaVHill.compareTo(retrievedPIC50DataUnspreadableCaVPIC50Data.getHill()) == 0);
    assertNotNull(retrievedPIC50DataUnspreadableCaVPIC50Data.getSaturation()); // spring.properties assigned!
    assertNull(request.getIK1());
    assertNull(request.getIKr());
    assertNull(request.getIKs());
    assertNull(request.getINa());
    assertNull(request.getIto());
  }

  @Test
  public void testCreateRunRequestSpreadable() {
    setCommonOptions();
    final IonChannelValues mockIonChannelValuesSpreadable = mocksControl.createMock(IonChannelValues.class);
    dummyIonChannelValues.add(mockIonChannelValuesSpreadable);

    expect(mockGroupedInvocationInput.getIonChannelValues()).andReturn(dummyIonChannelValues);
    expect(mockSimulationManager.find(dummySimulationId)).andReturn(mockSimulation);
    expect(mockSimulation.getCellModelIdentifier()).andReturn(dummyCellModelIdentifier);
    expect(mockSimulation.getPacingMaxTime()).andReturn(dummyMaxPacingTime);
    expect(mockJob.isUsingConfigurationCompoundConcentrations()).andReturn(dummyIsUsingDefaultCompoundConcentrations);

    // Testing to determine if all source assays are spreadable, which it is!
    final String dummySourceAssayNameSpreadable = AssayUtil.ASSAY_NAME_BARRACUDA;
    expect(mockIonChannelValuesSpreadable.getSourceAssayName()).andReturn(dummySourceAssayNameSpreadable);
    final String dummyIonChannelNameSpreadableCaV = IonChannel.CaV1_2.toString();
    expect(mockIonChannelValuesSpreadable.getIonChannelName()).andReturn(dummyIonChannelNameSpreadableCaV);
    // Testing to determine if all assigned Hill values are estimated and assigned with the default value.
    final List<PIC50Data> dummyAllPIC50DataSpreadableCaV = new ArrayList<PIC50Data>();
    final PIC50Data mockPIC50DataSpreadableCaVPIC50Data = mocksControl.createMock(PIC50Data.class);
    dummyAllPIC50DataSpreadableCaV.add(mockPIC50DataSpreadableCaVPIC50Data);
    expect(mockIonChannelValuesSpreadable.getpIC50Data()).andReturn(dummyAllPIC50DataSpreadableCaV);
    final BigDecimal dummyCaVHill = BigDecimal.ZERO;
    expect(mockPIC50DataSpreadableCaVPIC50Data.getHill()).andReturn(dummyCaVHill);
    final boolean dummyCaVIsOriginal = true;
    expect(mockPIC50DataSpreadableCaVPIC50Data.getOriginal()).andReturn(dummyCaVIsOriginal);

    // Iterating through the incoming ion channel values.
    expect(mockIonChannelValuesSpreadable.getIonChannelName()).andReturn(dummyIonChannelNameSpreadableCaV);
    expect(mockIonChannelValuesSpreadable.getSourceAssayName()).andReturn(dummySourceAssayNameSpreadable);
    expect(mockIonChannelValuesSpreadable.getpIC50Data()).andReturn(dummyAllPIC50DataSpreadableCaV);
    final BigDecimal dummyCaVPIC50 = BigDecimal.TEN;
    expect(mockPIC50DataSpreadableCaVPIC50Data.getValue()).andReturn(dummyCaVPIC50);
    expect(mockPIC50DataSpreadableCaVPIC50Data.getHill()).andReturn(dummyCaVHill);

    mocksControl.replay();

    final ApPredictRunRequest request = appManagerManager.createRunRequest(dummySimulationId,
                                                                           mockJob);

    mocksControl.verify();

    assertNotNull(request.getICaL());
    final ChannelData channelDataICaL = request.getICaL();
    final BigDecimal channelDataICaLC50Spread = channelDataICaL.getC50Spread();
    assertNotNull(channelDataICaLC50Spread);                  // spreadable!
    final BigDecimal channelDataICaLHillSpread = channelDataICaL.getHillSpread();
    assertNotNull(channelDataICaLHillSpread);                 // spreadable!
    final List<AssociatedItem> channelDataICaLIC50Data = channelDataICaL.getIC50Data();
    assertTrue(channelDataICaLIC50Data.isEmpty());
    final List<AssociatedItem> channelDataICaLPIC50Data = channelDataICaL.getPIC50Data();
    assertSame(1, channelDataICaLPIC50Data.size());
    final AssociatedItem retrievedPIC50DataSpreadableCaVPIC50Data  = channelDataICaLPIC50Data.get(0);
    assertTrue(dummyCaVPIC50.compareTo(retrievedPIC50DataSpreadableCaVPIC50Data.getC50()) == 0);
    assertTrue(dummyCaVHill.compareTo(retrievedPIC50DataSpreadableCaVPIC50Data.getHill()) == 0);
    assertNotNull(retrievedPIC50DataSpreadableCaVPIC50Data.getSaturation()); // spring.properties assigned!
    assertNull(request.getIK1());
    assertNull(request.getIKr());
    assertNull(request.getIKs());
    assertNull(request.getINa());
    assertNull(request.getIto());
  }

  @Test
  public void testCreateRunRequestMultiSpreadable() {
    setCommonOptions();
    final IonChannelValues mockIonChannelValuesSpreadable = mocksControl.createMock(IonChannelValues.class);
    dummyIonChannelValues.add(mockIonChannelValuesSpreadable);

    expect(mockGroupedInvocationInput.getIonChannelValues()).andReturn(dummyIonChannelValues);
    expect(mockSimulationManager.find(dummySimulationId)).andReturn(mockSimulation);
    expect(mockSimulation.getCellModelIdentifier()).andReturn(dummyCellModelIdentifier);
    expect(mockSimulation.getPacingMaxTime()).andReturn(dummyMaxPacingTime);
    expect(mockJob.isUsingConfigurationCompoundConcentrations()).andReturn(dummyIsUsingDefaultCompoundConcentrations);

    // Testing to determine if all source assays are spreadable, which it is!
    final String dummySourceAssayNameSpreadable = AssayUtil.ASSAY_NAME_BARRACUDA;
    expect(mockIonChannelValuesSpreadable.getSourceAssayName()).andReturn(dummySourceAssayNameSpreadable);
    final String dummyIonChannelNameSpreadableCaV = IonChannel.CaV1_2.toString();
    expect(mockIonChannelValuesSpreadable.getIonChannelName()).andReturn(dummyIonChannelNameSpreadableCaV);

    // Testing to determine if all assigned Hill values are estimated and assigned with the default value.
    final List<PIC50Data> dummyAllPIC50DataSpreadableCaV = new ArrayList<PIC50Data>();
    final PIC50Data mockPIC50DataSpreadableCaVPIC50Data1 = mocksControl.createMock(PIC50Data.class);
    final PIC50Data mockPIC50DataSpreadableCaVPIC50Data2 = mocksControl.createMock(PIC50Data.class);
    dummyAllPIC50DataSpreadableCaV.add(mockPIC50DataSpreadableCaVPIC50Data1);
    dummyAllPIC50DataSpreadableCaV.add(mockPIC50DataSpreadableCaVPIC50Data2);
    expect(mockIonChannelValuesSpreadable.getpIC50Data()).andReturn(dummyAllPIC50DataSpreadableCaV);
    final BigDecimal dummyCaVHill1 = BigDecimal.ONE;
    expect(mockPIC50DataSpreadableCaVPIC50Data1.getHill()).andReturn(dummyCaVHill1);
    final boolean dummyCaVIsOriginal1 = true; // It's an original Hill value, all Hills to be declared.
    expect(mockPIC50DataSpreadableCaVPIC50Data1.getOriginal()).andReturn(dummyCaVIsOriginal1);

    // Iterating through the incoming ion channel values.
    expect(mockIonChannelValuesSpreadable.getIonChannelName()).andReturn(dummyIonChannelNameSpreadableCaV);
    expect(mockIonChannelValuesSpreadable.getSourceAssayName()).andReturn(dummySourceAssayNameSpreadable);
    expect(mockIonChannelValuesSpreadable.getpIC50Data()).andReturn(dummyAllPIC50DataSpreadableCaV);

    final BigDecimal dummyCaVPIC501 = BigDecimal.TEN;
    expect(mockPIC50DataSpreadableCaVPIC50Data1.getValue()).andReturn(dummyCaVPIC501);
    expect(mockPIC50DataSpreadableCaVPIC50Data1.getHill()).andReturn(dummyCaVHill1);
    final BigDecimal dummyCaVPIC502 = BigDecimal.ZERO;
    expect(mockPIC50DataSpreadableCaVPIC50Data2.getValue()).andReturn(dummyCaVPIC502);
    final BigDecimal dummyCaVHill2 = BigDecimal.ONE;
    expect(mockPIC50DataSpreadableCaVPIC50Data2.getHill()).andReturn(dummyCaVHill2);

    mocksControl.replay();

    final ApPredictRunRequest request = appManagerManager.createRunRequest(dummySimulationId,
                                                                           mockJob);

    mocksControl.verify();

    assertNotNull(request.getICaL());
    final ChannelData channelDataICaL = request.getICaL();
    final BigDecimal channelDataICaLC50Spread = channelDataICaL.getC50Spread();
    assertNotNull(channelDataICaLC50Spread);                  // spreadable!
    final BigDecimal channelDataICaLHillSpread = channelDataICaL.getHillSpread();
    assertNotNull(channelDataICaLHillSpread);                 // spreadable!
    final List<AssociatedItem> channelDataICaLIC50Data = channelDataICaL.getIC50Data();
    assertTrue(channelDataICaLIC50Data.isEmpty());
    final List<AssociatedItem> channelDataICaLPIC50Data = channelDataICaL.getPIC50Data();
    assertSame(2, channelDataICaLPIC50Data.size());
    final AssociatedItem retrievedPIC50DataSpreadableCaVPIC50Data1  = channelDataICaLPIC50Data.get(0);
    assertTrue(dummyCaVPIC501.compareTo(retrievedPIC50DataSpreadableCaVPIC50Data1.getC50()) == 0);
    assertTrue(dummyCaVHill1.compareTo(retrievedPIC50DataSpreadableCaVPIC50Data1.getHill()) == 0);
    assertNotNull(retrievedPIC50DataSpreadableCaVPIC50Data1.getSaturation()); // spring.properties assigned!
    final AssociatedItem retrievedPIC50DataSpreadableCaVPIC50Data2  = channelDataICaLPIC50Data.get(1);
    assertTrue(dummyCaVPIC502.compareTo(retrievedPIC50DataSpreadableCaVPIC50Data2.getC50()) == 0);
    assertTrue(dummyCaVHill2.compareTo(retrievedPIC50DataSpreadableCaVPIC50Data2.getHill()) == 0);
    assertNotNull(retrievedPIC50DataSpreadableCaVPIC50Data2.getSaturation()); // spring.properties assigned!
    assertNull(request.getIK1());
    assertNull(request.getIKr());
    assertNull(request.getIKs());
    assertNull(request.getINa());
    assertNull(request.getIto());
  }

  @Test
  public void testCreateRunRequestMultiSpreadableIgnoreDefaultedHills() {
    setCommonOptions();
    final IonChannelValues mockIonChannelValuesSpreadable = mocksControl.createMock(IonChannelValues.class);
    dummyIonChannelValues.add(mockIonChannelValuesSpreadable);

    expect(mockGroupedInvocationInput.getIonChannelValues()).andReturn(dummyIonChannelValues);
    expect(mockSimulationManager.find(dummySimulationId)).andReturn(mockSimulation);
    expect(mockSimulation.getCellModelIdentifier()).andReturn(dummyCellModelIdentifier);
    expect(mockSimulation.getPacingMaxTime()).andReturn(dummyMaxPacingTime);
    expect(mockJob.isUsingConfigurationCompoundConcentrations()).andReturn(dummyIsUsingDefaultCompoundConcentrations);

    // Testing to determine if all source assays are spreadable, which it is!
    final String dummySourceAssayNameSpreadable = AssayUtil.ASSAY_NAME_BARRACUDA;
    expect(mockIonChannelValuesSpreadable.getSourceAssayName()).andReturn(dummySourceAssayNameSpreadable);
    final String dummyIonChannelNameSpreadableCaV = IonChannel.CaV1_2.toString();
    expect(mockIonChannelValuesSpreadable.getIonChannelName()).andReturn(dummyIonChannelNameSpreadableCaV);

    // Testing to determine if all assigned Hill values are estimated and assigned with the default value.
    final List<PIC50Data> dummyAllPIC50DataSpreadableCaV = new ArrayList<PIC50Data>();
    final PIC50Data mockPIC50DataSpreadableCaVPIC50Data1 = mocksControl.createMock(PIC50Data.class);
    final PIC50Data mockPIC50DataSpreadableCaVPIC50Data2 = mocksControl.createMock(PIC50Data.class);
    dummyAllPIC50DataSpreadableCaV.add(mockPIC50DataSpreadableCaVPIC50Data1);
    dummyAllPIC50DataSpreadableCaV.add(mockPIC50DataSpreadableCaVPIC50Data2);
    expect(mockIonChannelValuesSpreadable.getpIC50Data()).andReturn(dummyAllPIC50DataSpreadableCaV);
    final BigDecimal dummyCaVHill1 = BigDecimal.ONE;
    expect(mockPIC50DataSpreadableCaVPIC50Data1.getHill()).andReturn(dummyCaVHill1);
    final boolean dummyCaVIsOriginal1 = false;
    expect(mockPIC50DataSpreadableCaVPIC50Data1.getOriginal()).andReturn(dummyCaVIsOriginal1);
    final BigDecimal dummyCaVHill2 = BigDecimal.ONE;
    expect(mockPIC50DataSpreadableCaVPIC50Data2.getHill()).andReturn(dummyCaVHill2);
    final boolean dummyCaVIsOriginal2 = false;
    expect(mockPIC50DataSpreadableCaVPIC50Data2.getOriginal()).andReturn(dummyCaVIsOriginal2);
    expect(mockIonChannelValuesSpreadable.getIonChannelName()).andReturn(dummyIonChannelNameSpreadableCaV);

    // Iterating through the incoming ion channel values.
    expect(mockIonChannelValuesSpreadable.getIonChannelName()).andReturn(dummyIonChannelNameSpreadableCaV);
    expect(mockIonChannelValuesSpreadable.getSourceAssayName()).andReturn(dummySourceAssayNameSpreadable);
    expect(mockIonChannelValuesSpreadable.getpIC50Data()).andReturn(dummyAllPIC50DataSpreadableCaV);

    final BigDecimal dummyCaVPIC501 = BigDecimal.TEN;
    expect(mockPIC50DataSpreadableCaVPIC50Data1.getValue()).andReturn(dummyCaVPIC501);
    //expect(mockPIC50DataSpreadableCaVPIC50Data1.getHill()).andReturn(dummyCaVHill1);
    final BigDecimal dummyCaVPIC502 = BigDecimal.ZERO;
    expect(mockPIC50DataSpreadableCaVPIC50Data2.getValue()).andReturn(dummyCaVPIC502);
    //expect(mockPIC50DataSpreadableCaVPIC50Data2.getHill()).andReturn(dummyCaVHill2);

    mocksControl.replay();

    final ApPredictRunRequest request = appManagerManager.createRunRequest(dummySimulationId,
                                                                           mockJob);

    mocksControl.verify();

    assertNotNull(request.getICaL());
    final ChannelData channelDataICaL = request.getICaL();
    final BigDecimal channelDataICaLC50Spread = channelDataICaL.getC50Spread();
    assertNotNull(channelDataICaLC50Spread);                  // spreadable!
    final BigDecimal channelDataICaLHillSpread = channelDataICaL.getHillSpread();
    assertNull(channelDataICaLHillSpread);                    // spreadable, but all defaulted estimated values!
    final List<AssociatedItem> channelDataICaLIC50Data = channelDataICaL.getIC50Data();
    assertTrue(channelDataICaLIC50Data.isEmpty());
    final List<AssociatedItem> channelDataICaLPIC50Data = channelDataICaL.getPIC50Data();
    assertSame(2, channelDataICaLPIC50Data.size());
    final AssociatedItem retrievedPIC50DataSpreadableCaVPIC50Data1  = channelDataICaLPIC50Data.get(0);
    assertTrue(dummyCaVPIC501.compareTo(retrievedPIC50DataSpreadableCaVPIC50Data1.getC50()) == 0);
    assertNull(retrievedPIC50DataSpreadableCaVPIC50Data1.getHill());
    assertNotNull(retrievedPIC50DataSpreadableCaVPIC50Data1.getSaturation()); // spring.properties assigned!
    final AssociatedItem retrievedPIC50DataSpreadableCaVPIC50Data2  = channelDataICaLPIC50Data.get(1);
    assertTrue(dummyCaVPIC502.compareTo(retrievedPIC50DataSpreadableCaVPIC50Data2.getC50()) == 0);
    assertNull(retrievedPIC50DataSpreadableCaVPIC50Data2.getHill());
    assertNotNull(retrievedPIC50DataSpreadableCaVPIC50Data2.getSaturation()); // spring.properties assigned!
    assertNull(request.getIK1());
    assertNull(request.getIKr());
    assertNull(request.getIKs());
    assertNull(request.getINa());
    assertNull(request.getIto());
  }
}
