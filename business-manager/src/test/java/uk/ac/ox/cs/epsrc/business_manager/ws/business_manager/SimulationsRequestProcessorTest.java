/*

  Original work: Copyright (c) 2017, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws.business_manager;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.epsrc.business_manager.config.ConfigurationDoseResponseFitting;
import uk.ac.ox.cs.epsrc.business_manager.entity.GroupedInvocationInput;
import uk.ac.ox.cs.epsrc.business_manager.service.SimulationService;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedDataVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedSimulationRequestVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.SimulationRequestVO;
import uk.ac.ox.cs.epsrc.business_manager.ws.business_manager.SimulationsRequestProcessor;
import uk.ac.ox.cs.epsrc.business_manager.ws.business_manager.request_validator.SimulationsRequestValidatorChain;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProcessSimulationsRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProcessSimulationsResponse;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProcessSimulationsResponseStructure;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.SimulationDetail;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.ConflictingRequestException;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidCompoundIdentifierException;

/**
 * Unit test the component which processes simulations requests.
 *
 * @author geoff
 */
public class SimulationsRequestProcessorTest {

  private static final Long dummySimulationId = 1L;

  private Configuration mockConfiguration;
  private ConfigurationDoseResponseFitting mockConfigurationDoseResponseFitting;
  private IMocksControl mocksControl;
  private SimulationService mockSimulationService;
  private SimulationsRequestProcessor simulationsRequestProcessor;

  @Before
  public void setUp() {
    simulationsRequestProcessor = new SimulationsRequestProcessor(new SimulationsRequestValidatorChain());

    mocksControl = createStrictControl();

    mockConfiguration = mocksControl.createMock(Configuration.class);
    mockConfigurationDoseResponseFitting = mocksControl.createMock(ConfigurationDoseResponseFitting.class);
    mockSimulationService = mocksControl.createMock(SimulationService.class);
    ReflectionTestUtils.setField(simulationsRequestProcessor,
                                 BusinessIdentifiers.COMPONENT_CONFIGURATION, mockConfiguration);
    ReflectionTestUtils.setField(simulationsRequestProcessor,
                                 BusinessIdentifiers.COMPONENT_CONFIGURATION_DOSERESPONSE_FITTING,
                                 mockConfigurationDoseResponseFitting);
    ReflectionTestUtils.setField(simulationsRequestProcessor,
                                 BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE,
                                 mockSimulationService);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testProcessSimulationsRequest() {
    /*
     * 1. Test no simulation detail supplied in request.
     */
    ProcessSimulationsRequest mockRequest = mocksControl.createMock(ProcessSimulationsRequest.class);
    final List<SimulationDetail> dummySimulationDetail = new ArrayList<SimulationDetail>();
    expect(mockRequest.getSimulationDetail()).andReturn(dummySimulationDetail);

    mocksControl.replay();

    ProcessSimulationsResponse requestResponse = null;
    try {
      requestResponse = simulationsRequestProcessor.processSimulationsRequest(mockRequest);
    } catch (ConflictingRequestException e) {
      fail("Should not fail with ConflictingRequestException!");
    } catch (InvalidCompoundIdentifierException e) {
      fail("Should not fail with InvalidCompoundIdentifierException!");
    }

    mocksControl.verify();

    List<ProcessSimulationsResponseStructure> structures = requestResponse.getProcessSimulationsResponseStructure();
    assertTrue(structures.isEmpty());

    mocksControl.reset();

    /*
     * 2. SimulationDetail supplied in request.
     */
    final SimulationDetail mockSimulationDetail = mocksControl.createMock(SimulationDetail.class);
    dummySimulationDetail.add(mockSimulationDetail);
    expect(mockRequest.getSimulationDetail()).andReturn(dummySimulationDetail);

    final String dummyCompoundIdentifier = "dummyCompoundIdentifier";
    final boolean dummyForceReRun = true;
    final boolean dummyReset = false;
    final boolean dummyAssayGrouping = true;
    final boolean dummyValueInheriting = false;
    final boolean dummyBetweenGroups = true;
    final boolean dummyWithinGroups = false;
    final String dummyPC50EvaluationStrategies = "dummyPC50EvaluationStrategies";
    final String dummyDoseResponseFittingStrategy = "dummyDoseResponseFittingStrategy";
    final boolean dummyDoseResponseFittingRounded = true;
    final boolean dummyDoseResponseFittingHillMinMax = false;
    final Float dummyDoseResponseFittingHillMax = 1.4F;
    final Float dummyDoseResponseFittingHillMin = 2.3F;
    final Short dummyCellMLModelIdentifier = Short.valueOf("3");
    final BigDecimal dummyPacingMaxTime = BigDecimal.ONE;
    final boolean dummyInputDataOnly = true;
    final String dummyUserId = null;

    expect(mockSimulationDetail.getCompoundIdentifier())
          .andReturn(dummyCompoundIdentifier);
    expect(mockSimulationDetail.isForceReRun()).andReturn(dummyForceReRun);
    expect(mockSimulationDetail.isReset()).andReturn(dummyReset);
    expect(mockSimulationDetail.isAssayGrouping()).andReturn(dummyAssayGrouping);
    expect(mockSimulationDetail.isValueInheriting())
          .andReturn(dummyValueInheriting);
    expect(mockSimulationDetail.isBetweenGroups()).andReturn(dummyBetweenGroups);
    expect(mockSimulationDetail.isWithinGroups()).andReturn(dummyWithinGroups);
    expect(mockSimulationDetail.getPC50EvaluationStrategies())
          .andReturn(dummyPC50EvaluationStrategies);
    expect(mockSimulationDetail.getDoseResponseFittingStrategy())
          .andReturn(dummyDoseResponseFittingStrategy);
    expect(mockSimulationDetail.isDoseResponseFittingRounding())
          .andReturn(dummyDoseResponseFittingRounded);
    expect(mockSimulationDetail.isDoseResponseFittingHillMinMax())
          .andReturn(dummyDoseResponseFittingHillMinMax);
    expect(mockSimulationDetail.getDoseResponseFittingHillMax())
          .andReturn(dummyDoseResponseFittingHillMax);
    expect(mockSimulationDetail.getDoseResponseFittingHillMin())
          .andReturn(dummyDoseResponseFittingHillMin);
    expect(mockSimulationDetail.getCellMLModelIdentifier())
          .andReturn(dummyCellMLModelIdentifier);
    expect(mockSimulationDetail.getPacingMaxTime())
          .andReturn(dummyPacingMaxTime);
    expect(mockSimulationDetail.isInputDataOnly())
          .andReturn(dummyInputDataOnly);
    expect(mockSimulationDetail.getUserId()).andReturn(dummyUserId);

    final SimulationRequestVO mockSimulationRequestVO = mocksControl.createMock(SimulationRequestVO.class);
    expect(mockSimulationService.userInputValidation(dummyCompoundIdentifier,
                                                     dummyForceReRun,
                                                     dummyReset,
                                                     dummyAssayGrouping,
                                                     dummyValueInheriting,
                                                     dummyBetweenGroups,
                                                     dummyWithinGroups,
                                                     dummyPC50EvaluationStrategies,
                                                     dummyDoseResponseFittingStrategy,
                                                     dummyDoseResponseFittingRounded,
                                                     dummyDoseResponseFittingHillMinMax,
                                                     dummyDoseResponseFittingHillMax,
                                                     dummyDoseResponseFittingHillMin,
                                                     dummyCellMLModelIdentifier,
                                                     dummyPacingMaxTime,
                                                     dummyInputDataOnly,
                                                     dummyUserId))
          .andReturn(mockSimulationRequestVO);
    final List<ProcessedSimulationRequestVO> dummyProcessedSimulations = new ArrayList<ProcessedSimulationRequestVO>();
    final ProcessedSimulationRequestVO mockProcessedSimulationRequestVO = mocksControl.createMock(ProcessedSimulationRequestVO.class);
    dummyProcessedSimulations.add(mockProcessedSimulationRequestVO);
    expect(mockSimulationService.processSimulationsRequest(isA(List.class)))
          .andReturn(dummyProcessedSimulations);

    expect(mockProcessedSimulationRequestVO.getCompoundIdentifier())
          .andReturn(dummyCompoundIdentifier);
    expect(mockProcessedSimulationRequestVO.getSimulationId())
          .andReturn(dummySimulationId);
    final Map<BigDecimal, Set<BigDecimal>> fakePerFrequencyConcentrations = new HashMap<BigDecimal, Set<BigDecimal>>();
    final Set<GroupedInvocationInput> fakeProcessedData = new HashSet<GroupedInvocationInput>();
    final ProcessedDataVO fakeProcessedDataVO = new ProcessedDataVO(fakePerFrequencyConcentrations,
                                                                    fakeProcessedData);
    expect(mockProcessedSimulationRequestVO.getInputData())
          .andReturn(fakeProcessedDataVO);

    mocksControl.replay();

    try {
      requestResponse = simulationsRequestProcessor.processSimulationsRequest(mockRequest);
    } catch (Exception e) {
      fail("Should not be throwing an exception!");
    }

    mocksControl.verify();

    structures = requestResponse.getProcessSimulationsResponseStructure();
    assertEquals(1, structures.size());
    final ProcessSimulationsResponseStructure structure = structures.get(0);
    assertEquals(dummyCompoundIdentifier, structure.getCompoundIdentifier());
    assertSame(dummySimulationId, structure.getSimulationId());
    assertNotNull(structure.getInputData());

    mocksControl.reset();

    /*
     * 3. Emulate conflicting request.
     */
    dummySimulationDetail.add(mockSimulationDetail);

    expect(mockRequest.getSimulationDetail()).andReturn(dummySimulationDetail);
    expect(mockSimulationDetail.getCompoundIdentifier())
          .andReturn(dummyCompoundIdentifier);
    expect(mockSimulationDetail.isForceReRun()).andReturn(dummyForceReRun);
    expect(mockSimulationDetail.isReset()).andReturn(dummyReset);
    expect(mockSimulationDetail.isAssayGrouping()).andReturn(dummyAssayGrouping);
    expect(mockSimulationDetail.isValueInheriting())
          .andReturn(dummyValueInheriting);
    expect(mockSimulationDetail.isBetweenGroups()).andReturn(dummyBetweenGroups);
    expect(mockSimulationDetail.isWithinGroups()).andReturn(dummyWithinGroups);
    expect(mockSimulationDetail.getPC50EvaluationStrategies())
          .andReturn(dummyPC50EvaluationStrategies);
    expect(mockSimulationDetail.getDoseResponseFittingStrategy())
          .andReturn(dummyDoseResponseFittingStrategy);
    expect(mockSimulationDetail.isDoseResponseFittingRounding())
          .andReturn(dummyDoseResponseFittingRounded);
    expect(mockSimulationDetail.isDoseResponseFittingHillMinMax())
          .andReturn(dummyDoseResponseFittingHillMinMax);
    expect(mockSimulationDetail.getDoseResponseFittingHillMax())
          .andReturn(dummyDoseResponseFittingHillMax);
    expect(mockSimulationDetail.getDoseResponseFittingHillMin())
          .andReturn(dummyDoseResponseFittingHillMin);
    expect(mockSimulationDetail.getCellMLModelIdentifier())
          .andReturn(dummyCellMLModelIdentifier);
    expect(mockSimulationDetail.getPacingMaxTime())
          .andReturn(dummyPacingMaxTime);
    expect(mockSimulationDetail.isInputDataOnly()).andReturn(dummyInputDataOnly);
    expect(mockSimulationDetail.getUserId()).andReturn(dummyUserId);

    expect(mockSimulationService.userInputValidation(dummyCompoundIdentifier,
                                                     dummyForceReRun,
                                                     dummyReset,
                                                     dummyAssayGrouping,
                                                     dummyValueInheriting,
                                                     dummyBetweenGroups,
                                                     dummyWithinGroups,
                                                     dummyPC50EvaluationStrategies,
                                                     dummyDoseResponseFittingStrategy,
                                                     dummyDoseResponseFittingRounded,
                                                     dummyDoseResponseFittingHillMinMax,
                                                     dummyDoseResponseFittingHillMax,
                                                     dummyDoseResponseFittingHillMin,
                                                     dummyCellMLModelIdentifier,
                                                     dummyPacingMaxTime,
                                                     dummyInputDataOnly,
                                                     dummyUserId))
          .andReturn(mockSimulationRequestVO);
    expect(mockSimulationDetail.getCompoundIdentifier())
          .andReturn(dummyCompoundIdentifier);
    expect(mockSimulationDetail.isForceReRun()).andReturn(dummyForceReRun);
    expect(mockSimulationDetail.isReset()).andReturn(dummyReset);
    expect(mockSimulationDetail.isAssayGrouping()).andReturn(dummyAssayGrouping);
    expect(mockSimulationDetail.isValueInheriting())
          .andReturn(dummyValueInheriting);
    expect(mockSimulationDetail.isBetweenGroups()).andReturn(dummyBetweenGroups);
    expect(mockSimulationDetail.isWithinGroups()).andReturn(dummyWithinGroups);
    expect(mockSimulationDetail.getPC50EvaluationStrategies())
          .andReturn(dummyPC50EvaluationStrategies);
    expect(mockSimulationDetail.getDoseResponseFittingStrategy())
          .andReturn(dummyDoseResponseFittingStrategy);
    expect(mockSimulationDetail.isDoseResponseFittingRounding())
          .andReturn(dummyDoseResponseFittingRounded);
    expect(mockSimulationDetail.isDoseResponseFittingHillMinMax())
          .andReturn(dummyDoseResponseFittingHillMinMax);
    expect(mockSimulationDetail.getDoseResponseFittingHillMax())
          .andReturn(dummyDoseResponseFittingHillMax);
    expect(mockSimulationDetail.getDoseResponseFittingHillMin())
          .andReturn(dummyDoseResponseFittingHillMin);
    expect(mockSimulationDetail.getCellMLModelIdentifier())
          .andReturn(dummyCellMLModelIdentifier);
    expect(mockSimulationDetail.getPacingMaxTime()).andReturn(dummyPacingMaxTime);
    expect(mockSimulationDetail.isInputDataOnly()).andReturn(dummyInputDataOnly);
    expect(mockSimulationDetail.getUserId()).andReturn(dummyUserId);

    expect(mockSimulationService.userInputValidation(dummyCompoundIdentifier,
                                                     dummyForceReRun,
                                                     dummyReset,
                                                     dummyAssayGrouping,
                                                     dummyValueInheriting,
                                                     dummyBetweenGroups,
                                                     dummyWithinGroups,
                                                     dummyPC50EvaluationStrategies,
                                                     dummyDoseResponseFittingStrategy,
                                                     dummyDoseResponseFittingRounded,
                                                     dummyDoseResponseFittingHillMinMax,
                                                     dummyDoseResponseFittingHillMax,
                                                     dummyDoseResponseFittingHillMin,
                                                     dummyCellMLModelIdentifier,
                                                     dummyPacingMaxTime,
                                                     dummyInputDataOnly,
                                                     dummyUserId))
          .andReturn(mockSimulationRequestVO);

    mocksControl.replay();

    try {
      requestResponse = simulationsRequestProcessor.processSimulationsRequest(mockRequest);
      fail("Should not accept conflicting requests!");
    } catch (ConflictingRequestException e) {
      // 
    } catch (InvalidCompoundIdentifierException e) {
      fail("Should be complaining about conflicting requests!");
    }

    mocksControl.verify();
  }
}