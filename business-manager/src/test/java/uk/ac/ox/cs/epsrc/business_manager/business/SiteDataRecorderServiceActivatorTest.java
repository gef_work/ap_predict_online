/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertSame;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.SiteDataRecorderServiceActivator;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.Provenance;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.ExperimentalDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.IrregularSiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.QSARDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.ProblemVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.doseresponse.DoseResponsePairVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.transfer.screening.AggregatedSiteDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProblemType;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;

/**
 * Unit test the site data recording service activator.
 *
 * @author geoff
 */
public class SiteDataRecorderServiceActivatorTest {

  private BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate;
  private IMocksControl mocksControl;
  private final Long dummySimulationId = 1L;
  private SiteDataRecorderServiceActivator siteDataRecorder;

  @Before
  public void setUp() {
    siteDataRecorder = new SiteDataRecorderServiceActivator();
    mocksControl = createStrictControl();
    mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    ReflectionTestUtils.setField(siteDataRecorder, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
  }

  @Test(expected=IllegalArgumentException.class)
  public void testRecordingExperimentalDataFailsOnNullArg() {
    final ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    siteDataRecorder.recordExperimentalSiteData(dummyProcessingType, dummySimulationId, null);
  }

  @Test
  public void testRecordingExperimentalDataSuccess() {
    // Emulate input data gathering processing.
    ProcessingType dummyProcessingType = ProcessingType.INPUT_DATA_GATHERING;
    ExperimentalDataHolder mockExperimentalData = mocksControl.createMock(ExperimentalDataHolder.class);
    ExperimentalDataHolder returnedExperimentalData = siteDataRecorder.recordExperimentalSiteData(dummyProcessingType,
                                                                                                  dummySimulationId,
                                                                                                  mockExperimentalData);
    assertSame(returnedExperimentalData, mockExperimentalData);

    // Emulate regular simulation processing.
    dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    expect(mockExperimentalData.containsData()).andReturn(true);

    mocksControl.replay();

    returnedExperimentalData = siteDataRecorder.recordExperimentalSiteData(dummyProcessingType,
                                                                           dummySimulationId,
                                                                           mockExperimentalData);

    mocksControl.verify();

    assertSame(returnedExperimentalData, mockExperimentalData);
  }

  @Test(expected=IllegalArgumentException.class)
  public void testRecordingQSARDataFailsOnNullArg() {
    final ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    siteDataRecorder.recordQSARSiteData(dummyProcessingType, dummySimulationId, null);
  }

  @Test
  public void testRecordingQSARDataSuccess() {
    // Emulate input data gathering processing.
    ProcessingType dummyProcessingType = ProcessingType.INPUT_DATA_GATHERING;
    QSARDataHolder mockQSARData = mocksControl.createMock(QSARDataHolder.class);
    QSARDataHolder returnedQSARData = siteDataRecorder.recordQSARSiteData(dummyProcessingType,
                                                                          dummySimulationId,
                                                                          mockQSARData);
    assertSame(returnedQSARData, mockQSARData);

    // Emulate regular simulation processing.
    dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    expect(mockQSARData.containsData()).andReturn(true);

    mocksControl.replay();

    returnedQSARData = siteDataRecorder.recordQSARSiteData(dummyProcessingType, dummySimulationId,
                                                           mockQSARData);

    mocksControl.verify();

    assertSame(returnedQSARData, mockQSARData);
  }

  @Test(expected=IllegalArgumentException.class)
  public void testRecordingScreeningSiteDataFailsOnNullArg() throws IllegalArgumentException,
                                                                    InvalidValueException {
    final ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    siteDataRecorder.recordScreeningSiteData(dummyProcessingType, dummySimulationId, null);
  }

  @Test
  public void testRecordingScreeningSiteDataWhenInputDataGathering() throws IllegalArgumentException,
                                                                            InvalidValueException {
    final ProcessingType dummyProcessingType = ProcessingType.INPUT_DATA_GATHERING;
    AggregatedSiteDataVO mockSiteData = mocksControl.createMock(AggregatedSiteDataVO.class);

    mocksControl.replay();

    AggregatedSiteDataVO returnedSiteData = siteDataRecorder.recordScreeningSiteData(dummyProcessingType,
                                                                                     dummySimulationId,
                                                                                     mockSiteData);
    mocksControl.verify();

    assertSame(returnedSiteData, mockSiteData);
  }

  @Test
  public void testRecordingScreeningSiteDataWhenNonIrregularRegularSimulation() throws IllegalArgumentException,
                                                                                       InvalidValueException {
    final ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    AggregatedSiteDataVO mockSiteData = mocksControl.createMock(AggregatedSiteDataVO.class);

    final List<String> dummyProblems = new ArrayList<String>();
    final String dummyProblem1 = "dummyProblem1";
    final String dummyProblem2 = "dummyProblem2";
    dummyProblems.add(dummyProblem1);
    dummyProblems.add(dummyProblem2);

    expect(mockSiteData.getProblems()).andReturn(dummyProblems);

    final Capture<Progress> dummyOverallProgress1 = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress1));
    final Capture<Progress> dummyOverallProgress2 = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress2));

    final List<SiteDataHolder> dummySiteDataHolders = new ArrayList<SiteDataHolder>();
    final SiteDataHolder mockSiteDataHolder1 = mocksControl.createMock(SiteDataHolder.class);
    dummySiteDataHolders.add(mockSiteDataHolder1);
    expect(mockSiteData.getSiteDataHolders()).andReturn(dummySiteDataHolders);

    final List<IrregularSiteDataHolder> dummyIrregularSiteDataHolders = new ArrayList<IrregularSiteDataHolder>();
    expect(mockSiteData.getIrregularSiteDataHolders()).andReturn(dummyIrregularSiteDataHolders);

    final Capture<Progress> dummyOverallProgress3 = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress3));

    final Capture<Progress> dummyOverallProgress4 = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress4));

    final AssayVO mockAssayVO1 = mocksControl.createMock(AssayVO.class);
    expect(mockSiteDataHolder1.getAssay()).andReturn(mockAssayVO1);
    final String dummyAssay1Name = "dummyAssay1Name";
    expect(mockAssayVO1.getName()).andReturn(dummyAssay1Name);
    final IonChannel dummyIonChannel1 = IonChannel.CaV1_2;
    expect(mockSiteDataHolder1.getIonChannel()).andReturn(dummyIonChannel1);

    final SummaryDataRecord mockSummaryDataRecord1 = mocksControl.createMock(SummaryDataRecord.class);
    expect(mockSiteDataHolder1.getSummaryDataRecord(false)).andReturn(mockSummaryDataRecord1);

    final String dummySummaryData1Id = "dummySummaryData1Id";
    expect(mockSummaryDataRecord1.getId()).andReturn(dummySummaryData1Id);
    final String dummySummaryData1SourceName = "dummySummaryData1SourceName";
    expect(mockSummaryDataRecord1.getSourceName()).andReturn(dummySummaryData1SourceName);
    final Map<String, Object> dummySummaryData1RowData = new HashMap<String, Object>();
    expect(mockSummaryDataRecord1.getRawData()).andReturn(dummySummaryData1RowData);

    final Capture<Provenance> dummyProvenance1 = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(dummyProvenance1));

    final Map<IndividualDataRecord, List<DoseResponseDataRecord>> dummyNonSummaryData1 = new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();
    final IndividualDataRecord mockIndividualDataRecord1 = mocksControl.createMock(IndividualDataRecord.class);
    final DoseResponseDataRecord mockDoseResponseDataRecord1 = mocksControl.createMock(DoseResponseDataRecord.class);
    final List<DoseResponseDataRecord> dummyDoseResponseDataRecords = new ArrayList<DoseResponseDataRecord>();
    dummyDoseResponseDataRecords.add(mockDoseResponseDataRecord1);
    dummyNonSummaryData1.put(mockIndividualDataRecord1, dummyDoseResponseDataRecords);
    expect(mockSiteDataHolder1.getNonSummaryDataRecords()).andReturn(dummyNonSummaryData1);

    final Capture<Provenance> dummyProvenance2 = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(dummyProvenance2));

    final String dummyIndividualData1Id = "dummyIndividualData1Id";
    expect(mockIndividualDataRecord1.getId()).andReturn(dummyIndividualData1Id);
    final String dummyIndividualData1SourceName = "dummyIndividualData1SourceName";
    expect(mockIndividualDataRecord1.getSourceName()).andReturn(dummyIndividualData1SourceName);
    final Map<String, Object> dummyIndividualData1RawData = new HashMap<String, Object>();
    expect(mockIndividualDataRecord1.getRawData()).andReturn(dummyIndividualData1RawData);
    final BigDecimal dummyIndividualData1HillCoefficient = BigDecimal.ONE;
    expect(mockIndividualDataRecord1.eligibleHillCoefficient()).andReturn(dummyIndividualData1HillCoefficient);

    final Capture<Provenance> dummyProvenance3 = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(dummyProvenance3));

    final String dummyDoseResponseData1Id = "dummyDoseResponseData1Id";
    expect(mockDoseResponseDataRecord1.getId()).andReturn(dummyDoseResponseData1Id);
    final String dummyDoseResponseData1SourceName = "dummyDoseResponseData1SourceName";
    expect(mockDoseResponseDataRecord1.getSourceName()).andReturn(dummyDoseResponseData1SourceName);
    final Map<String, Object> dummyDoseResponseData1RawData = new HashMap<String, Object>();
    expect(mockDoseResponseDataRecord1.getRawData()).andReturn(dummyDoseResponseData1RawData);

    final Capture<Provenance> dummyProvenance4 = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(dummyProvenance4));

    final DoseResponsePairVO mockDoseResponseData1DoseResponsePair1 = mocksControl.createMock(DoseResponsePairVO.class);
    expect(mockDoseResponseDataRecord1.eligibleDoseResponseData()).andReturn(mockDoseResponseData1DoseResponsePair1);

    final BigDecimal dummyDoseResponseData1Dose1 = BigDecimal.ONE;
    expect(mockDoseResponseData1DoseResponsePair1.getDose()).andReturn(dummyDoseResponseData1Dose1);
    final BigDecimal dummyDoseResponseData1Response1 = BigDecimal.TEN;
    expect(mockDoseResponseData1DoseResponsePair1.getResponse()).andReturn(dummyDoseResponseData1Response1);

    final Capture<Provenance> dummyProvenance5 = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(dummyProvenance5));

    // Creation of new DoseResponseDataVO in provenance send.
    expect(mockDoseResponseData1DoseResponsePair1.getDose()).andReturn(dummyDoseResponseData1Dose1);
    expect(mockDoseResponseData1DoseResponsePair1.getResponse()).andReturn(dummyDoseResponseData1Response1);

    final Capture<Provenance> dummyProvenance6 = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(dummyProvenance6));

    mocksControl.replay();

    AggregatedSiteDataVO returnedSiteData = siteDataRecorder.recordScreeningSiteData(dummyProcessingType,
                                                                                     dummySimulationId,
                                                                                     mockSiteData);
    mocksControl.verify();
  }

  @Test
  public void testRecordingScreeningSiteDataWhenOnlyIrregularData() throws IllegalArgumentException,
                                                                           InvalidValueException {
    final ProcessingType dummyProcessingType = ProcessingType.REGULAR_SIMULATION;
    AggregatedSiteDataVO mockSiteData = mocksControl.createMock(AggregatedSiteDataVO.class);

    final List<String> dummyProblems = new ArrayList<String>();
    expect(mockSiteData.getProblems()).andReturn(dummyProblems);

    final List<SiteDataHolder> dummySiteDataHolders = new ArrayList<SiteDataHolder>();
    expect(mockSiteData.getSiteDataHolders()).andReturn(dummySiteDataHolders);

    final List<IrregularSiteDataHolder> dummyIrregularSiteDataHolders = new ArrayList<IrregularSiteDataHolder>();
    final IrregularSiteDataHolder mockIrregularSiteDataHolder = mocksControl.createMock(IrregularSiteDataHolder.class);
    dummyIrregularSiteDataHolders.add(mockIrregularSiteDataHolder);

    expect(mockSiteData.getIrregularSiteDataHolders()).andReturn(dummyIrregularSiteDataHolders);

    final Capture<Progress> dummyOverallProgress1 = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress1));

    final AssayVO mockAssayVO1 = mocksControl.createMock(AssayVO.class);
    expect(mockIrregularSiteDataHolder.getAssay()).andReturn(mockAssayVO1);
    final String dummyAssay1Name = "dummyAssay1Name";
    expect(mockAssayVO1.getName()).andReturn(dummyAssay1Name);
    final IonChannel dummyIonChannel1 = IonChannel.CaV1_2;
    expect(mockIrregularSiteDataHolder.getIonChannel()).andReturn(dummyIonChannel1);
    final List<ProblemVO> problems = new ArrayList<ProblemVO>();
    final ProblemVO mockProblemVO = mocksControl.createMock(ProblemVO.class);
    problems.add(mockProblemVO);
    expect(mockIrregularSiteDataHolder.getProblems()).andReturn(problems);
    final ProblemType dummyProblemType = ProblemType.NO_DOSE_RESPONSES_MATCHED;
    expect(mockProblemVO.getProblem()).andReturn(dummyProblemType);
    final String[] problemTexts = new String[] { "dummyProblem" };
    expect(mockProblemVO.retrieveParamsAsStringArray()).andReturn(problemTexts);

    final Capture<Progress> dummyOverallProgress2 = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(dummyOverallProgress2));

    final Capture<Provenance> dummyProvenance = newCapture();
    mockBusinessManagerJmsTemplate.sendProvenance(capture(dummyProvenance));

    mocksControl.replay();

    AggregatedSiteDataVO returnedSiteData = siteDataRecorder.recordScreeningSiteData(dummyProcessingType,
                                                                                     dummySimulationId,
                                                                                     mockSiteData);
    mocksControl.verify();
  }
}