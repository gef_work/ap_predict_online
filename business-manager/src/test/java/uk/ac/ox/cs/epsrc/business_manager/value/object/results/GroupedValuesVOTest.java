/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object.results;

import static org.easymock.EasyMock.createStrictControl;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Test;

/**
 * Unit test the grouped values value object.
 *
 * @author geoff
 */
public class GroupedValuesVOTest {

  @Test
  public void testConstructor() {
    String dummyName = null;
    short dummyLevel = 4;
    List<IonChannelValuesVO> dummyIonChannelValues = null;

    GroupedValuesVO groupedValuesVO = new GroupedValuesVO(dummyName,
                                                          dummyLevel,
                                                          dummyIonChannelValues); 

    assertNotNull(groupedValuesVO.toString());
    assertNotNull(groupedValuesVO.hashCode());
    assertFalse(groupedValuesVO.equals(null));
    assertNull(groupedValuesVO.getName());
    assertSame(dummyLevel, groupedValuesVO.getLevel());
    assertTrue(groupedValuesVO.getIonChannelValues().isEmpty());

    dummyName = "dummyName";
    dummyLevel = 5;
    final IMocksControl mocksControl = createStrictControl();
    final IonChannelValuesVO mockIonChannelValuesVO = mocksControl.createMock(IonChannelValuesVO.class);
    dummyIonChannelValues = new ArrayList<IonChannelValuesVO>();
    dummyIonChannelValues.add(mockIonChannelValuesVO);

    groupedValuesVO = new GroupedValuesVO(dummyName, dummyLevel,
                                          dummyIonChannelValues);
    assertEquals(dummyName, groupedValuesVO.getName());
    assertSame(dummyLevel, groupedValuesVO.getLevel());
    assertSame(1, groupedValuesVO.getIonChannelValues().size());
    try {
      groupedValuesVO.getIonChannelValues().clear();
      fail("Should not permit modification of returned collection!");
    } catch (UnsupportedOperationException e) {}
  }
}