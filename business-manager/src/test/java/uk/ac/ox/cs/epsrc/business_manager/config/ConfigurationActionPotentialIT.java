/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.config.ConfigurationActionPotential;

/**
 * Integration test the ActionPotential configuration component.
 *
 * @author geoff
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( { "classpath:/uk/ac/ox/cs/epsrc/business_manager/jms/mockJMS-context.xml",
                         "classpath:/uk/ac/ox/cs/epsrc/business_manager/config/ConfigurationActionPotentialIT-context.xml",
                         "classpath:/uk/ac/ox/cs/epsrc/business_manager/service/appCtx.experimentalService.xml",
                         "classpath:/META-INF/spring/ctx/config/appCtx.config.xml",
                         "classpath:/META-INF/spring/ctx/config/appCtx.config.actionPotential.site.xml",
                         "classpath:/META-INF/spring/ctx/config/appCtx.config.site.xml",
                         "classpath:/META-INF/spring/ctx/config/appCtx.config.assays.site.xml",
                         "classpath:/META-INF/spring/ctx/config/appCtx.config.cellModels.site.xml",
                         "classpath:/META-INF/spring/ctx/config/appCtx.config.changeCheckers.site.xml",
                         "classpath:/META-INF/spring/ctx/config/appCtx.config.doseResponseFitting.site.xml",
                         "classpath:/META-INF/spring/ctx/config/appCtx.config.ionChannels.site.xml",
                         "classpath:/META-INF/spring/ctx/config/appCtx.config.pc50Evaluators.site.xml",
                         "classpath:/META-INF/spring/ctx/integration/simulation/processing/screening/appCtx.proc.dose_response_fitting.xml" } )

public class ConfigurationActionPotentialIT {

  @Autowired @Qualifier(BusinessIdentifiers.COMPONENT_CONFIGURATION_ACTIONPOTENTIAL)
  private ConfigurationActionPotential configurationActionPotential;

  /**
   * Test the initialisation has succeeded.
   */
  @Test
  public void testInitialisation() {
    assertEquals(1, configurationActionPotential.getDefaultPerFrequencyConcentrations().size());
    assertEquals(7, configurationActionPotential.getDefaultPerFrequencyConcentrations().get(new BigDecimal("0.5")).size());
    assertEquals(2, configurationActionPotential.getDefaultPlasmaConcCount());
    assertTrue(configurationActionPotential.isDefaultPlasmaConcLogScale());
    assertEquals(2, configurationActionPotential.getDefaultModelIdentifier());
    assertEquals(8, configurationActionPotential.getCellModels().size());
    assertNotNull(configurationActionPotential.toString());
  }
}