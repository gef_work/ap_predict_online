/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.selection.c50;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import uk.ac.ox.cs.epsrc.business_manager.business.selection.c50.C50DataProcessor;
import uk.ac.ox.cs.epsrc.business_manager.business.selection.c50.C50DataSelector;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.processing.IndividualProcessing;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;

/**
 * Unit test the IC50 / pIC50 / pXC50 data processor.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(C50DataSelector.class)
public class C50DataProcessorTest {

  private C50DataProcessor c50DataProcessor;

  @Before
  public void setUp() {
    c50DataProcessor = new C50DataProcessor();
  }

  /**
   * Test passing null fails
   */
  @Test(expected=IllegalArgumentException.class)
  public void testIndividualPC50FailOnNull() throws IllegalArgumentException,
                                                    InvalidValueException {
    IndividualProcessing individualProcessing = null;
    c50DataProcessor.individualDataPC50(individualProcessing);
  }

  /**
   * Test passing valid individual data success.
   */
  @Test
  public void testIndividualDataPC50() throws InvalidValueException {
    IMocksControl mocksControl = createStrictControl();
    IndividualProcessing mockIndividualProcessing = mocksControl.createMock(IndividualProcessing.class);
    IndividualDataRecord mockIndividualDataRecord = mocksControl.createMock(IndividualDataRecord.class);
    List<ICData> eligibleICData = new ArrayList<ICData>();
    expect(mockIndividualProcessing.getIndividualData()).andReturn(mockIndividualDataRecord);
    expect(mockIndividualDataRecord.eligibleICData()).andReturn(eligibleICData);
    mocksControl.replay();
    c50DataProcessor.individualDataPC50(mockIndividualProcessing);
    mocksControl.verify();
  }

  /**
   * Test passing valid summary data success.
   * @throws InvalidValueException 
   */
  @Test
  public void testSummaryDataPC50() throws InvalidValueException {
    // Make sure null arg fails
    SiteDataHolder mockSiteData = null;
    try {
      c50DataProcessor.summaryDataPC50(mockSiteData);
      fail("Should not permit a null site data holder arg.");
    } catch (IllegalArgumentException e) {}

    // Handle null ICDataHolder in site data
    IMocksControl mocksControl = createStrictControl();
    mockSiteData = mocksControl.createMock(SiteDataHolder.class);
    expect(mockSiteData.getSummaryDataRecord(false)).andReturn(null);
    mocksControl.replay();
    ICData icData = c50DataProcessor.summaryDataPC50(mockSiteData);
    mocksControl.verify();
    assertNull(icData);

    mocksControl.reset();

    // All data present
    SummaryDataRecord mockSummaryDataRecord = mocksControl.createMock(SummaryDataRecord.class);
    List<ICData> dummyEligibleC50Data = new ArrayList<ICData>();
    expect(mockSiteData.getSummaryDataRecord(false)).andReturn(mockSummaryDataRecord);
    expect(mockSummaryDataRecord.eligibleICData()).andReturn(dummyEligibleC50Data);
    final ICData mockC50Data = mocksControl.createMock(ICData.class);
    mockStatic(C50DataSelector.class);
    expect(C50DataSelector.selectFrom(dummyEligibleC50Data)).andReturn(mockC50Data);

    replayAll();
    mocksControl.replay();

    icData = c50DataProcessor.summaryDataPC50(mockSiteData);

    verifyAll();
    mocksControl.verify();
    assertNotNull(icData);
  }
}