/*

  Copyright (c) 2016, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.evaluation.pc50;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.nc3rs.business_manager.api.exception.NoSuchDataException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ic.ICPercent;

/**
 *
 *
 * @author geoff
 */
public class AbstractNonEqualityModifierNonInclusionAlertingTest {

  private class ConcreteEvaluator extends AbstractNonEqualityModifierNonInclusionAlerting {
    private static final long serialVersionUID = 1L;
    protected ConcreteEvaluator(final String description, final boolean defaultActive,
                                final int defaultInvocationOrder) throws IllegalArgumentException {
      super(description, defaultActive, defaultInvocationOrder);
    }
  }

  private ConcreteEvaluator testEvaluator;
  private ICData mockICData;
  private IMocksControl mocksControl;

  @Before
  public void setUp() {
    final String dummyDescription = "dummyDescription";
    final boolean dummyDefaultActive = true;
    final int dummyDefaultInvocationOrder = 99;
    testEvaluator = new ConcreteEvaluator(dummyDescription, dummyDefaultActive,
                                          dummyDefaultInvocationOrder);
    mocksControl = createStrictControl();
    mockICData = mocksControl.createMock(ICData.class);
  }

  @Test
  public void testPresenceOfEqualityModifiers() throws NoSuchDataException {
    /*
     * Alerting highly active.
     */
    expect(mockICData.hasICPctData(ICPercent.PCT20)).andReturn(true);
    expect(mockICData.hasICPctData(ICPercent.PCT50)).andReturn(true);
    expect(mockICData.isModifierEquals(ICPercent.PCT20)).andReturn(true);

    mocksControl.replay();

    String alert = testEvaluator.soundAlert(mockICData);

    mocksControl.verify();

    assertNull(alert);

    mocksControl.reset();

    expect(mockICData.hasICPctData(ICPercent.PCT20)).andReturn(true);
    expect(mockICData.hasICPctData(ICPercent.PCT50)).andReturn(true);
    expect(mockICData.isModifierEquals(ICPercent.PCT20)).andReturn(false);
    expect(mockICData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT20)).andReturn(false);
    expect(mockICData.isModifierEquals(ICPercent.PCT50)).andReturn(true);

    mocksControl.replay();

    alert = testEvaluator.soundAlert(mockICData);

    mocksControl.verify();

    assertNull(alert);
}

  @Test
  public void testPresenceOfIC50AndIC20Alerting() throws NoSuchDataException {
    /*
     * Alerting highly active.
     */
    expect(mockICData.hasICPctData(ICPercent.PCT20)).andReturn(true);
    expect(mockICData.hasICPctData(ICPercent.PCT50)).andReturn(true);
    expect(mockICData.isModifierEquals(ICPercent.PCT20)).andReturn(false);
    expect(mockICData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT20)).andReturn(true); // active
    expect(mockICData.isModifierEquals(ICPercent.PCT50)).andReturn(false);
    expect(mockICData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50)).andReturn(true); // active

    String dummyPct20OriginalInfo = "dummyPC20OriginalInfo";
    expect(mockICData.retrieveOriginalInfo(ICPercent.PCT20)).andReturn(dummyPct20OriginalInfo);
    String dummyPct50OriginalInfo = "dummyPCT50OriginalInfo";
    expect(mockICData.retrieveOriginalInfo(ICPercent.PCT50)).andReturn(dummyPct50OriginalInfo);

    mocksControl.replay();

    String alert = testEvaluator.soundAlert(mockICData);

    mocksControl.verify();

    assertNotNull(alert);
    assertTrue(alert.contains("Compound highly active at lowest concentration"));

    mocksControl.reset();

    /*
     * Alerting inactive.
     */
    expect(mockICData.hasICPctData(ICPercent.PCT20)).andReturn(true);
    expect(mockICData.hasICPctData(ICPercent.PCT50)).andReturn(true);
    expect(mockICData.isModifierEquals(ICPercent.PCT20)).andReturn(false);
    expect(mockICData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT20)).andReturn(false); // inactive
    expect(mockICData.isModifierEquals(ICPercent.PCT50)).andReturn(false);
    expect(mockICData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50)).andReturn(false); // inactive

    expect(mockICData.retrieveOriginalInfo(ICPercent.PCT20)).andReturn(dummyPct20OriginalInfo);
    expect(mockICData.retrieveOriginalInfo(ICPercent.PCT50)).andReturn(dummyPct50OriginalInfo);

    mocksControl.replay();

    alert = testEvaluator.soundAlert(mockICData);

    mocksControl.verify();

    assertNotNull(alert);
    assertTrue(alert.contains("Compound inactive at highest concentration"));
  }

  @Test
  public void testPresenceOfIC50AndIC20NonAlerting() throws NoSuchDataException {
    expect(mockICData.hasICPctData(ICPercent.PCT20)).andReturn(true);
    expect(mockICData.hasICPctData(ICPercent.PCT50)).andReturn(true);
    expect(mockICData.isModifierEquals(ICPercent.PCT20)).andReturn(false);
    expect(mockICData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT20)).andReturn(true); // active
    expect(mockICData.isModifierEquals(ICPercent.PCT50)).andReturn(false);
    expect(mockICData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50)).andReturn(false); // inactive

    mocksControl.replay();

    String alert = testEvaluator.soundAlert(mockICData);

    mocksControl.verify();

    assertNull(alert);

    mocksControl.reset();

    expect(mockICData.hasICPctData(ICPercent.PCT20)).andReturn(true);
    expect(mockICData.hasICPctData(ICPercent.PCT50)).andReturn(true);
    expect(mockICData.isModifierEquals(ICPercent.PCT20)).andReturn(false);
    expect(mockICData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT20)).andReturn(false); // inactive
    expect(mockICData.isModifierEquals(ICPercent.PCT50)).andReturn(false);
    expect(mockICData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50)).andReturn(true); // active

    mocksControl.replay();

    alert = testEvaluator.soundAlert(mockICData);

    mocksControl.verify();

    assertNull(alert);
  }

  @Test
  public void testPresenceOfIC20OnlyAlerting() throws NoSuchDataException {
    /*
     * Alerting highly active.
     */
    expect(mockICData.hasICPctData(ICPercent.PCT20)).andReturn(true);
    expect(mockICData.hasICPctData(ICPercent.PCT50)).andReturn(false);
    expect(mockICData.isModifierEquals(ICPercent.PCT20)).andReturn(false);
    expect(mockICData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT20)).andReturn(true); // active
    String dummyPct20OriginalInfo = "dummyPCT20OriginalInfo";
    expect(mockICData.retrieveOriginalInfo(ICPercent.PCT20)).andReturn(dummyPct20OriginalInfo);

    mocksControl.replay();

    String alert = testEvaluator.soundAlert(mockICData);

    mocksControl.verify();

    assertNotNull(alert);
    assertTrue(alert.contains("Compound highly active at lowest concentration"));

    mocksControl.reset();

    /*
     * Alerting inactive.
     */
    expect(mockICData.hasICPctData(ICPercent.PCT20)).andReturn(true);
    expect(mockICData.hasICPctData(ICPercent.PCT50)).andReturn(false);
    expect(mockICData.isModifierEquals(ICPercent.PCT20)).andReturn(false);
    expect(mockICData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT20)).andReturn(false); // inactive
    expect(mockICData.retrieveOriginalInfo(ICPercent.PCT20)).andReturn(dummyPct20OriginalInfo);

    mocksControl.replay();

    alert = testEvaluator.soundAlert(mockICData);

    mocksControl.verify();

    assertNotNull(alert);
    assertTrue(alert.contains("Compound inactive at highest concentration"));
  }

  @Test
  public void testPresenceOfIC50OnlyAlerting() throws NoSuchDataException {
    /*
     * Alerting highly active.
     */
    expect(mockICData.hasICPctData(ICPercent.PCT20)).andReturn(false);
    expect(mockICData.hasICPctData(ICPercent.PCT50)).andReturn(true);
    expect(mockICData.isModifierEquals(ICPercent.PCT50)).andReturn(false);
    expect(mockICData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50)).andReturn(true); // active
    String dummyPct50OriginalInfo = "dummyPCT50OriginalInfo";
    expect(mockICData.retrieveOriginalInfo(ICPercent.PCT50)).andReturn(dummyPct50OriginalInfo);

    mocksControl.replay();

    String alert = testEvaluator.soundAlert(mockICData);

    mocksControl.verify();

    assertNotNull(alert);
    assertTrue(alert.contains("Compound highly active at lowest concentration"));

    mocksControl.reset();

    /*
     * Alerting inactive.
     */
    expect(mockICData.hasICPctData(ICPercent.PCT20)).andReturn(false);
    expect(mockICData.hasICPctData(ICPercent.PCT50)).andReturn(true);
    expect(mockICData.isModifierEquals(ICPercent.PCT50)).andReturn(false);
    expect(mockICData.isPCComparisonEffectiveGreaterModifier(ICPercent.PCT50)).andReturn(false); // inactive
    expect(mockICData.retrieveOriginalInfo(ICPercent.PCT50)).andReturn(dummyPct50OriginalInfo);

    mocksControl.replay();

    alert = testEvaluator.soundAlert(mockICData);

    mocksControl.verify();

    assertNotNull(alert);
    assertTrue(alert.contains("Compound inactive at highest concentration"));
  }

  @Test
  public void testUsesEqualityModifier() {
    assertFalse(testEvaluator.usesEqualityModifier());
  }
}