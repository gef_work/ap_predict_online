/*

  Original work: Copyright (c) 2018, University of Oxford.
  Modified work: Copyright (c) 2020, University of Nottingham.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the copyright holders nor the names of their
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.manager;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.powermock.api.easymock.PowerMock.createMockAndExpectNew;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.SimulationProcessingGateway;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.business.information.provenance.FundamentalProvenance;
import uk.ac.ox.cs.epsrc.business_manager.dao.SimulationDAO;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedDataVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.ProcessedSimulationRequestVO;
import uk.ac.ox.cs.epsrc.business_manager.value.object.SimulationRequestVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Unit test the simulation manager request processing.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { Simulation.class, SimulationManagerImpl.class } )
public class SimulationManagerImplRequestProcessingTest {

  private BusinessManagerJMSTemplate mockJMSTemplate;
  private IMocksControl mocksControl;
  private static final long dummySimulationId = 4l;
  private Simulation mockSimulation;
  private SimulationDAO mockSimulationDAO;
  private SimulationManager simulationManager;
  private SimulationProcessingGateway mockSimulationProcessingGateway;
  private SimulationRequestVO mockSimulationRequest;

  private String dummyCompoundIdentifier;
  private boolean dummyForceReRun;
  private boolean dummyReset;
  private boolean dummyAssayGrouping;
  private boolean dummyValueInheriting;
  private boolean dummyBetweenGroups;
  private boolean dummyWithinGroups;
  private String dummyPC50EvaluationStrategies;
  private String dummyDoseResponseFittingStrategy;
  private boolean dummyDoseResponseFittingRounding;
  private boolean dummyDoseResponseFittingHillMinMax;
  private Float dummyDoseResponseFittingHillMax;
  private Float dummyDoseResponseFittingHillMin;
  private short dummyCellModelIdentifier;
  private BigDecimal dummyPacingMaxTime;
  private String dummyUserId;

  @Before
  public void setUp() {
    simulationManager = new SimulationManagerImpl();

    mocksControl = createStrictControl();
    mockJMSTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    mockSimulation = mocksControl.createMock(Simulation.class);
    mockSimulationDAO = mocksControl.createMock(SimulationDAO.class);
    mockSimulationProcessingGateway = mocksControl.createMock(SimulationProcessingGateway.class);
    mockSimulationRequest = mocksControl.createMock(SimulationRequestVO.class);

    ReflectionTestUtils.setField(simulationManager,
                                 BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockJMSTemplate);
    ReflectionTestUtils.setField(simulationManager,
                                 BusinessIdentifiers.COMPONENT_SIMULATION_DAO,
                                 mockSimulationDAO);
    ReflectionTestUtils.setField(simulationManager,
                                 BusinessIdentifiers.COMPONENT_SIMULATION_PROCESSING_GATEWAY,
                                 mockSimulationProcessingGateway);
  }

  @Test
  public void testInputDataOnly() throws Exception {
    expect(mockSimulationRequest.getCompoundIdentifier())
          .andReturn(dummyCompoundIdentifier);
    expect(mockSimulationRequest.isAssayGrouping())
          .andReturn(dummyAssayGrouping);
    expect(mockSimulationRequest.isValueInheriting())
          .andReturn(dummyValueInheriting);
    expect(mockSimulationRequest.isBetweenGroups())
          .andReturn(dummyBetweenGroups);
    expect(mockSimulationRequest.isWithinGroups()).andReturn(dummyWithinGroups);
    expect(mockSimulationRequest.getPc50EvaluationStrategies())
          .andReturn(dummyPC50EvaluationStrategies);
    expect(mockSimulationRequest.getDoseResponseFittingStrategy())
          .andReturn(dummyDoseResponseFittingStrategy);
    expect(mockSimulationRequest.isDoseResponseFittingRounding())
          .andReturn(dummyDoseResponseFittingRounding);
    expect(mockSimulationRequest.isDoseResponseFittingHillMinMax())
          .andReturn(dummyDoseResponseFittingHillMinMax);
    expect(mockSimulationRequest.getDoseResponseFittingHillMax())
          .andReturn(dummyDoseResponseFittingHillMax);
    expect(mockSimulationRequest.getDoseResponseFittingHillMin())
          .andReturn(dummyDoseResponseFittingHillMin);
    expect(mockSimulationRequest.getCellModelIdentifier())
          .andReturn(dummyCellModelIdentifier);
    expect(mockSimulationRequest.getPacingMaxTime())
          .andReturn(dummyPacingMaxTime);
    expect(mockSimulationRequest.getUserId()).andReturn(dummyUserId);

    // >> createNewSimulation()
    final Simulation mockSimulation = createMockAndExpectNew(Simulation.class,
                                                             ProcessedSimulationRequestVO.INVALID_SIMULATION_ID,
                                                             dummyCompoundIdentifier,
                                                             dummyAssayGrouping,
                                                             dummyValueInheriting,
                                                             dummyBetweenGroups,
                                                             dummyWithinGroups,
                                                             dummyPC50EvaluationStrategies,
                                                             dummyDoseResponseFittingStrategy,
                                                             dummyDoseResponseFittingRounding,
                                                             dummyDoseResponseFittingHillMinMax,
                                                             dummyDoseResponseFittingHillMax,
                                                             dummyDoseResponseFittingHillMin,
                                                             dummyCellModelIdentifier,
                                                             dummyPacingMaxTime,
                                                             dummyUserId);
    // << createNewSimulation()

    final ProcessedDataVO mockInputData = mocksControl.createMock(ProcessedDataVO.class);
    expect(mockSimulationProcessingGateway.inputDataGathering(mockSimulation))
          .andReturn(mockInputData);
    final ProcessedSimulationRequestVO mockProcessedSimulationRequest =
          createMockAndExpectNew(ProcessedSimulationRequestVO.class,
                                 dummyCompoundIdentifier, mockInputData);

    mocksControl.replay();
    replayAll();

    final ProcessedSimulationRequestVO returned = simulationManager.requestProcessingInputDataOnly(mockSimulationRequest);

    mocksControl.verify();
    verifyAll();

    assertSame(mockProcessedSimulationRequest, returned);
  }

  @Test
  public void testNewSimulation() throws Exception {
    dummyCompoundIdentifier = "dummyCompoundIdentifier";
    expect(mockSimulationRequest.getCompoundIdentifier())
          .andReturn(dummyCompoundIdentifier);
    expect(mockSimulationRequest.isAssayGrouping())
          .andReturn(dummyAssayGrouping);
    expect(mockSimulationRequest.isValueInheriting())
          .andReturn(dummyValueInheriting);
    expect(mockSimulationRequest.isBetweenGroups())
          .andReturn(dummyBetweenGroups);
    expect(mockSimulationRequest.isWithinGroups()).andReturn(dummyWithinGroups);
    expect(mockSimulationRequest.getPc50EvaluationStrategies())
          .andReturn(dummyPC50EvaluationStrategies);
    expect(mockSimulationRequest.getDoseResponseFittingStrategy())
          .andReturn(dummyDoseResponseFittingStrategy);
    expect(mockSimulationRequest.isDoseResponseFittingRounding())
          .andReturn(dummyDoseResponseFittingRounding);
    expect(mockSimulationRequest.isDoseResponseFittingHillMinMax())
          .andReturn(dummyDoseResponseFittingHillMinMax);
    expect(mockSimulationRequest.getDoseResponseFittingHillMax())
          .andReturn(dummyDoseResponseFittingHillMax);
    expect(mockSimulationRequest.getDoseResponseFittingHillMin())
          .andReturn(dummyDoseResponseFittingHillMin);
    expect(mockSimulationRequest.getCellModelIdentifier())
          .andReturn(dummyCellModelIdentifier);
    expect(mockSimulationRequest.getPacingMaxTime())
          .andReturn(dummyPacingMaxTime);
    expect(mockSimulationRequest.getUserId()).andReturn(dummyUserId);

    // >> createNewSimulation()
    final Simulation mockSimulation = createMockAndExpectNew(Simulation.class,
                                                             dummyCompoundIdentifier,
                                                             dummyAssayGrouping,
                                                             dummyValueInheriting,
                                                             dummyBetweenGroups,
                                                             dummyWithinGroups,
                                                             dummyPC50EvaluationStrategies,
                                                             dummyDoseResponseFittingStrategy,
                                                             dummyDoseResponseFittingRounding,
                                                             dummyDoseResponseFittingHillMinMax,
                                                             dummyDoseResponseFittingHillMax,
                                                             dummyDoseResponseFittingHillMin,
                                                             dummyCellModelIdentifier,
                                                             dummyPacingMaxTime,
                                                             dummyUserId);
    // << createNewSimulation()
    // >> save()
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockSimulationDAO.store(mockSimulation)).andReturn(mockSimulation);
    // << save()
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    final Capture<FundamentalProvenance> captureFundamentalProvenence1 = newCapture();
    mockJMSTemplate.sendProvenance(capture(captureFundamentalProvenence1));
    final Capture<OverallProgress> captureOverallProgress1 = newCapture();
    mockJMSTemplate.sendProgress(capture(captureOverallProgress1));

    final ProcessedSimulationRequestVO mockProcessedSimulationRequest = createMockAndExpectNew(ProcessedSimulationRequestVO.class,
                                                                                               dummyCompoundIdentifier,
                                                                                               dummySimulationId, null);

    mocksControl.replay();
    replayAll();

    final ProcessedSimulationRequestVO processedSimulationRequest = simulationManager.requestProcessingNewSimulation(mockSimulationRequest);

    mocksControl.verify();
    verifyAll();

    assertSame(mockProcessedSimulationRequest, processedSimulationRequest);

    final FundamentalProvenance capturedFundamentalProvenance1 = (FundamentalProvenance) captureFundamentalProvenence1.getValue();
    assertSame(dummySimulationId,
               capturedFundamentalProvenance1.getSimulationId());
    assertSame(dummyCompoundIdentifier,
               capturedFundamentalProvenance1.getCompoundIdentifier());
    assertSame(InformationLevel.TRACE,
               capturedFundamentalProvenance1.getLevel());
    assertEquals(MessageKey.SIMULATION_IDENTIFIER_CREATED.getBundleIdentifier(),
                 capturedFundamentalProvenance1.getText());

    final OverallProgress capturedOverallProgress1 = captureOverallProgress1.getValue();
    assertSame(dummyCompoundIdentifier,
               capturedOverallProgress1.getCompoundIdentifier());
    assertSame(dummySimulationId, capturedOverallProgress1.getSimulationId());
    assertSame(InformationLevel.TRACE, capturedOverallProgress1.getLevel());
    assertEquals(MessageKey.SIMULATION_IDENTIFIER_CREATED.getBundleIdentifier(),
                 capturedOverallProgress1.getText());
  }

  @Test
  public void testExistingSimulationForceReRun() throws Exception {
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    dummyForceReRun = true;
    expect(mockSimulationRequest.isForceReRun()).andReturn(dummyForceReRun);
    dummyReset = false;
    expect(mockSimulationRequest.isReset()).andReturn(dummyReset);
    expect(mockSimulationRequest.getUserId()).andReturn(dummyUserId);
    expect(mockSimulation.getCompoundIdentifier())
          .andReturn(dummyCompoundIdentifier);
    final Capture<OverallProgress> captureOverallProgress1 = newCapture();
    mockJMSTemplate.sendProgress(capture(captureOverallProgress1));
    final boolean dummyIsBusy = true;
    expect(mockSimulation.isBusy()).andReturn(dummyIsBusy);
    final boolean dummyAssignedProcessTasks = true;
    expect(mockSimulation.assignProcessTask(dummyForceReRun, false))
          .andReturn(dummyAssignedProcessTasks);
    final Capture<OverallProgress> captureOverallProgress2 = newCapture();
    mockJMSTemplate.sendProgress(capture(captureOverallProgress2));
    // >> makeAvailableForProcessingSelection
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    mockSimulation.makeAvailableForProcessing(dummyUserId);
    //                                        >> save()
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockSimulationDAO.store(mockSimulation)).andReturn(mockSimulation);
    //                                        << save()
    // << makeAvailableForProcessingSelection

    final ProcessedSimulationRequestVO mockProcessedSimulationRequest =
          createMockAndExpectNew(ProcessedSimulationRequestVO.class,
                                 dummyCompoundIdentifier, dummySimulationId,
                                 null);

    mocksControl.replay();
    replayAll();

    final ProcessedSimulationRequestVO request = simulationManager.requestProcessingExistingSimulation(mockSimulation,
                                                                                                       mockSimulationRequest);

    mocksControl.verify();
    verifyAll();

    assertSame(mockProcessedSimulationRequest, request);

    final OverallProgress capturedOverallProgress1 = captureOverallProgress1.getValue();
    assertSame(dummyCompoundIdentifier,
               capturedOverallProgress1.getCompoundIdentifier());
    assertSame(dummySimulationId, capturedOverallProgress1.getSimulationId());
    assertSame(InformationLevel.DEBUG, capturedOverallProgress1.getLevel());
    assertEquals(MessageKey.SIMULATION_ALREADY_EXISTS.getBundleIdentifier(),
                 capturedOverallProgress1.getText());

    final OverallProgress capturedOverallProgress2 = captureOverallProgress2.getValue();
    assertNull(capturedOverallProgress2.getCompoundIdentifier());
    assertSame(dummySimulationId, capturedOverallProgress2.getSimulationId());
    assertSame(InformationLevel.DEBUG, capturedOverallProgress2.getLevel());
    assertEquals(MessageKey.SIMULATION_CHANGED.getBundleIdentifier(),
                 capturedOverallProgress2.getText());
  }

  @Test
  public void testExistingSimulationResetAndNotBusy() throws Exception {
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    dummyForceReRun = false;
    expect(mockSimulationRequest.isForceReRun()).andReturn(dummyForceReRun);
    dummyReset = true;
    expect(mockSimulationRequest.isReset()).andReturn(dummyReset);
    expect(mockSimulationRequest.getUserId()).andReturn(dummyUserId);
    expect(mockSimulation.getCompoundIdentifier())
          .andReturn(dummyCompoundIdentifier);
    final Capture<OverallProgress> captureOverallProgress1 = newCapture();
    mockJMSTemplate.sendProgress(capture(captureOverallProgress1));
    final boolean dummyIsBusy = false;
    expect(mockSimulation.isBusy()).andReturn(dummyIsBusy);
    final Capture<OverallProgress> captureOverallProgress2 = newCapture();
    mockJMSTemplate.sendProgress(capture(captureOverallProgress2));

    final ProcessedSimulationRequestVO mockProcessedSimulationRequest =
          createMockAndExpectNew(ProcessedSimulationRequestVO.class,
                                 dummyCompoundIdentifier, dummySimulationId,
                                 null);

    mocksControl.replay();
    replayAll();

    final ProcessedSimulationRequestVO request = simulationManager.requestProcessingExistingSimulation(mockSimulation,
                                                                                                       mockSimulationRequest);

    mocksControl.verify();
    verifyAll();

    assertSame(mockProcessedSimulationRequest, request);

    final OverallProgress capturedOverallProgress1 = captureOverallProgress1.getValue();
    assertSame(dummyCompoundIdentifier, capturedOverallProgress1.getCompoundIdentifier());
    assertSame(dummySimulationId, capturedOverallProgress1.getSimulationId());
    assertSame(InformationLevel.DEBUG, capturedOverallProgress1.getLevel());
    assertEquals(MessageKey.SIMULATION_ALREADY_EXISTS.getBundleIdentifier(),
                 capturedOverallProgress1.getText());

    final OverallProgress capturedOverallProgress2 = captureOverallProgress2.getValue();
    assertNull(capturedOverallProgress2.getCompoundIdentifier());
    assertSame(dummySimulationId, capturedOverallProgress2.getSimulationId());
    assertSame(InformationLevel.TRACE, capturedOverallProgress2.getLevel());
    assertEquals("Ignoring request to reset a 'free' simulation.",
                 capturedOverallProgress2.getText());
  }

  @Test
  public void testExistingSimulationResetButNoSimulationStateChange() throws Exception {
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    dummyForceReRun = false;
    expect(mockSimulationRequest.isForceReRun()).andReturn(dummyForceReRun);
    dummyReset = true;
    expect(mockSimulationRequest.isReset()).andReturn(dummyReset);
    expect(mockSimulationRequest.getUserId()).andReturn(dummyUserId);
    expect(mockSimulation.getCompoundIdentifier()).andReturn(dummyCompoundIdentifier);
    final Capture<OverallProgress> captureOverallProgress1 = newCapture();
    mockJMSTemplate.sendProgress(capture(captureOverallProgress1));
    final boolean dummyIsBusy = true;
    expect(mockSimulation.isBusy()).andReturn(dummyIsBusy);
    final boolean dummyAssignedProcessTasks = false;
    expect(mockSimulation.assignProcessTask(dummyForceReRun, dummyReset))
          .andReturn(dummyAssignedProcessTasks);
    final Capture<OverallProgress> captureOverallProgress2 = newCapture();
    mockJMSTemplate.sendProgress(capture(captureOverallProgress2));

    final ProcessedSimulationRequestVO mockProcessedSimulationRequest =
          createMockAndExpectNew(ProcessedSimulationRequestVO.class, dummyCompoundIdentifier,
                                 dummySimulationId, null);

    mocksControl.replay();
    replayAll();

    final ProcessedSimulationRequestVO request = simulationManager.requestProcessingExistingSimulation(mockSimulation,
                                                                                                       mockSimulationRequest);

    mocksControl.verify();
    verifyAll();

    assertSame(mockProcessedSimulationRequest, request);

    final OverallProgress capturedOverallProgress1 = captureOverallProgress1.getValue();
    assertSame(dummyCompoundIdentifier,
               capturedOverallProgress1.getCompoundIdentifier());
    assertSame(dummySimulationId, capturedOverallProgress1.getSimulationId());
    assertSame(InformationLevel.DEBUG, capturedOverallProgress1.getLevel());
    assertEquals(MessageKey.SIMULATION_ALREADY_EXISTS.getBundleIdentifier(),
                 capturedOverallProgress1.getText());

    final OverallProgress capturedOverallProgress2 = captureOverallProgress2.getValue();
    assertNull(capturedOverallProgress2.getCompoundIdentifier());
    assertSame(dummySimulationId, capturedOverallProgress2.getSimulationId());
    assertSame(InformationLevel.TRACE, capturedOverallProgress2.getLevel());
    assertEquals("Ignoring request to re-run or reset simulation as no change imposed.",
                 capturedOverallProgress2.getText());
  }

  @Test
  public void testExistingSimulationBusyOrNotRequestProcessed() throws Exception {
    /*
     * 1. Simulation is busy 
     */
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    dummyForceReRun = false;
    expect(mockSimulationRequest.isForceReRun()).andReturn(dummyForceReRun);
    dummyReset = false;
    expect(mockSimulationRequest.isReset()).andReturn(dummyReset);
    expect(mockSimulationRequest.getUserId()).andReturn(dummyUserId);
    expect(mockSimulation.getCompoundIdentifier())
          .andReturn(dummyCompoundIdentifier);
    Capture<OverallProgress> captureOverallProgress1 = newCapture();
    mockJMSTemplate.sendProgress(capture(captureOverallProgress1));
    boolean dummyIsBusy = true;
    expect(mockSimulation.isBusy()).andReturn(dummyIsBusy);

    Capture<OverallProgress> captureOverallProgress2 = newCapture();
    mockJMSTemplate.sendProgress(capture(captureOverallProgress2));

    ProcessedSimulationRequestVO mockProcessedSimulationRequest = createMockAndExpectNew(ProcessedSimulationRequestVO.class,
                                                                                         dummyCompoundIdentifier,
                                                                                         dummySimulationId, null);

    mocksControl.replay();
    replayAll();

    ProcessedSimulationRequestVO request = simulationManager.requestProcessingExistingSimulation(mockSimulation,
                                                                                                 mockSimulationRequest);

    mocksControl.verify();
    verifyAll();

    assertSame(mockProcessedSimulationRequest, request);

    OverallProgress capturedOverallProgress1 = captureOverallProgress1.getValue();
    assertSame(dummyCompoundIdentifier,
               capturedOverallProgress1.getCompoundIdentifier());
    assertSame(dummySimulationId, capturedOverallProgress1.getSimulationId());
    assertSame(InformationLevel.DEBUG, capturedOverallProgress1.getLevel());
    assertEquals(MessageKey.SIMULATION_ALREADY_EXISTS.getBundleIdentifier(),
                 capturedOverallProgress1.getText());

    OverallProgress capturedOverallProgress2 = captureOverallProgress2.getValue();
    assertNull(capturedOverallProgress2.getCompoundIdentifier());
    assertSame(dummySimulationId, capturedOverallProgress2.getSimulationId());
    assertSame(InformationLevel.TRACE, capturedOverallProgress2.getLevel());
    assertEquals("Simulation is busy.", capturedOverallProgress2.getText());
  }

  @Test
  public void testExistingSimulationFree() throws Exception {
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    dummyForceReRun = false;
    expect(mockSimulationRequest.isForceReRun()).andReturn(dummyForceReRun);
    dummyReset = false;
    expect(mockSimulationRequest.isReset()).andReturn(dummyReset);
    expect(mockSimulationRequest.getUserId()).andReturn(dummyUserId);
    expect(mockSimulation.getCompoundIdentifier())
          .andReturn(dummyCompoundIdentifier);
    Capture<OverallProgress> captureOverallProgress1 = newCapture();
    mockJMSTemplate.sendProgress(capture(captureOverallProgress1));
    boolean dummyIsBusy = false;
    expect(mockSimulation.isBusy()).andReturn(dummyIsBusy);
    // >> makeAvailableForProcessingSelection
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    mockSimulation.makeAvailableForProcessing(dummyUserId);
    //    >> save()
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockSimulationDAO.store(mockSimulation)).andReturn(mockSimulation);
    //    << save()
    // << makeAvailableForProcessingSelection
    Capture<OverallProgress> captureOverallProgress2 = newCapture();
    mockJMSTemplate.sendProgress(capture(captureOverallProgress2));

    ProcessedSimulationRequestVO mockProcessedSimulationRequest = createMockAndExpectNew(ProcessedSimulationRequestVO.class,
                                                                                         dummyCompoundIdentifier,
                                                                                         dummySimulationId, null);

    mocksControl.replay();
    replayAll();

    ProcessedSimulationRequestVO request = simulationManager.requestProcessingExistingSimulation(mockSimulation,
                                                                                                       mockSimulationRequest);

    mocksControl.verify();
    verifyAll();

    assertSame(mockProcessedSimulationRequest, request);

    OverallProgress capturedOverallProgress1 = captureOverallProgress1.getValue();
    assertSame(dummyCompoundIdentifier,
               capturedOverallProgress1.getCompoundIdentifier());
    assertSame(dummySimulationId, capturedOverallProgress1.getSimulationId());
    assertSame(InformationLevel.DEBUG, capturedOverallProgress1.getLevel());
    assertEquals(MessageKey.SIMULATION_ALREADY_EXISTS.getBundleIdentifier(),
                 capturedOverallProgress1.getText());

    OverallProgress capturedOverallProgress2 = captureOverallProgress2.getValue();
    assertNull(capturedOverallProgress2.getCompoundIdentifier());
    assertSame(dummySimulationId, capturedOverallProgress2.getSimulationId());
    assertSame(InformationLevel.DEBUG, capturedOverallProgress2.getLevel());
    assertEquals("Setting flag for simulation to be processed.",
                 capturedOverallProgress2.getText());
  }
}