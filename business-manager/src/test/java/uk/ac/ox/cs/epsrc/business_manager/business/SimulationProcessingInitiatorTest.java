/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.SimulationProcessingInitiator;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.service.SimulationService;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;

/**
 * Unit test the simulation processing initiator object.
 *
 * @author geoff
 */
public class SimulationProcessingInitiatorTest {

  private BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate;
  private IMocksControl mocksControl;
  private Integer dummySQLUpdateResponse;
  private Long dummySimulationId = 1L;
  private SimulationService mockSimulationService;
  private SimulationProcessingInitiator simulationProcessingInitiator;

  @Before
  public void setUp() {
    simulationProcessingInitiator = new SimulationProcessingInitiator();
    mocksControl = createStrictControl();
    mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    mockSimulationService = mocksControl.createMock(SimulationService.class);
    ReflectionTestUtils.setField(simulationProcessingInitiator,
                                 BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
    ReflectionTestUtils.setField(simulationProcessingInitiator,
                                 BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE,
                                 mockSimulationService);

    dummySQLUpdateResponse = null;
  }

  @Test(expected=IllegalArgumentException.class)
  public void testProcessSimulationFailsOnNullArg() {
    simulationProcessingInitiator.initiateSimulationProcessing(dummySimulationId,
                                                               dummySQLUpdateResponse);
  }

  @Test
  public void testProcessSimulation() {
    dummySQLUpdateResponse = 1;

    final Capture<Progress> captureOverallProgress = newCapture();
    mockBusinessManagerJmsTemplate.sendProgress(capture(captureOverallProgress));

    final Simulation mockSimulation = mocksControl.createMock(Simulation.class);
    expect(mockSimulationService.processSimulation(dummySimulationId)).andReturn(mockSimulation);

    mocksControl.replay();

    simulationProcessingInitiator.initiateSimulationProcessing(dummySimulationId,
                                                               dummySQLUpdateResponse);

    mocksControl.verify();

    final OverallProgress capturedOverallProgress = (OverallProgress) captureOverallProgress.getValue();
    assertSame(dummySimulationId, capturedOverallProgress.getSimulationId());
    assertSame(InformationLevel.TRACE, capturedOverallProgress.getLevel());
    assertEquals(MessageKey.SIMULATION_INITIATION.getBundleIdentifier(),
                 capturedOverallProgress.getText());
  }
}