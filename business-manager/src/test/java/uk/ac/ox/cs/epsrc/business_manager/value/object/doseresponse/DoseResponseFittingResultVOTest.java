/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.value.object.doseresponse;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.value.object.doseresponse.DoseResponseFittingResultVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;

/**
 * Unit test the dose response fitting result VO operations.
 *
 * @author geoff
 */
public class DoseResponseFittingResultVOTest {

  private DoseResponseFittingResultVO doseResponseFittingResult;

  /**
   * Test constructor throws exception on invalid params.
   * 
   * @throws InvalidValueException
   */
  @Test(expected=InvalidValueException.class)
  public void testConstructorThrowsException1() throws InvalidValueException {
    doseResponseFittingResult = new DoseResponseFittingResultVO(null, null, null);
  }

  /**
   * Test constructor throws exception on invalid params.
   * 
   * @throws InvalidValueException
   */
  @Test(expected=InvalidValueException.class)
  public void testConstructorThrowsException2() throws InvalidValueException {
    doseResponseFittingResult = new DoseResponseFittingResultVO(BigDecimal.ZERO, null, null);
  }
  /**
   * Test the constructor on valid params.
   * 
   * @throws InvalidValueException 
   */
  @Test
  public void testConstructor() throws InvalidValueException {
    final BigDecimal ic50 = BigDecimal.TEN;
    final BigDecimal hillCoefficient = BigDecimal.ONE;
    final String error = "error";
    doseResponseFittingResult = new DoseResponseFittingResultVO(ic50, hillCoefficient, error);
    assertEquals(ic50, doseResponseFittingResult.getIC50());
    assertEquals(hillCoefficient, doseResponseFittingResult.getHillCoefficient());
    assertEquals(error, doseResponseFittingResult.getError());
  }
}