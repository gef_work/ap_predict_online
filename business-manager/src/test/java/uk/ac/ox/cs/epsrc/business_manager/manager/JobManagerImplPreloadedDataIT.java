/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.manager;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.transaction.annotation.Transactional;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.BusinessTestIdentifiers;

/**
 * Integration test the JobManager implementation.
 * 
 * @author geoff
 */
//@ContextConfiguration(locations = { "classpath:/uk/ac/ox/cs/epsrc/business_manager/config/mockConfig-context.xml",
//                                    "classpath:/uk/ac/ox/cs/epsrc/business_manager/jms/mockJMS-context.xml",
//                                    "classpath:/uk/ac/ox/cs/epsrc/business_manager/manager/appCtx.jobManager.xml",
//                                    "classpath:/uk/ac/ox/cs/epsrc/business_manager/dao/appCtx.dao.xml",
//                                    "classpath:/META-INF/spring/ctx/data/business_manager/appCtx.database.business-manager.xml" })
//@ActiveProfiles(profiles="business_manager_embedded")
public class JobManagerImplPreloadedDataIT { //extends AbstractJUnit4SpringContextTests {

  /*
  @Autowired(required=true)
  @Qualifier(BusinessIdentifiers.COMPONENT_JOB_MANAGER)
  private JobManager jobManager;

  @Before
  public void setUp() throws SQLException {
    final DataSource dataSource = (DataSource) applicationContext.getBean(BusinessTestIdentifiers.COMPONENT_BUSINESS_DATA_SOURCE);
    final ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator();
    //resourceDatabasePopulator.addScript(new ClassPathResource("/META-INF/sql/business_manager/test1.sql"));
    resourceDatabasePopulator.populate(dataSource.getConnection());
  }

  */
  @Test()
  //@Transactional()
  //@Rollback(false)
  public void testSomething() {
    //jobManager.purgeSimulationJobData(1);
  }
}