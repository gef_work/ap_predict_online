/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.information.messaging;

import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.InformationTransformer;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.OverallProgress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress;
import uk.ac.ox.cs.epsrc.business_manager.business.information.progress.Progress.MessageKey;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.value.object.sitedata.ProcessedSiteDataVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.InformationLevel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;

/**
 * Unit test the information transformer.
 *
 * @author geoff
 */
public class InformationTransformerTest {

  private InformationTransformer informationTransformer;
  private IMocksControl mocksControl;
  private ProcessedSiteDataVO mockProcessedSiteDataVO;
  private Long dummySimulationId = 3L;
  private String dummyCompoundIdentifier = "dummyCompoundIdentifier";
  private MessageKey dummyMessageKey;

  @Before
  public void setUp() {
    mocksControl =  createControl();
    mockProcessedSiteDataVO = mocksControl.createMock(ProcessedSiteDataVO.class);
    informationTransformer = new InformationTransformer();
  }

  @Test
  public void testProcessedSiteDataToProgressWithNullArg() {
    mockProcessedSiteDataVO = null;
    try {
      informationTransformer.processedSiteDataToProgress(dummySimulationId, dummyCompoundIdentifier,
                                                         dummyMessageKey, mockProcessedSiteDataVO);
      fail("Should not accept a null processed site data value object.");
    } catch (IllegalArgumentException e) {}
  }

  @Test
  public void testProcessedSiteDataToProgress() {
    // Should fail when site data contains data.
    expect(mockProcessedSiteDataVO.isContainingSiteData()).andReturn(true);
    mocksControl.replay();
    try {
      informationTransformer.processedSiteDataToProgress(dummySimulationId, dummyCompoundIdentifier,
                                                         dummyMessageKey, mockProcessedSiteDataVO);
      fail("Should not accept a site data value object containing data.");
    } catch (IllegalArgumentException e) {}
    mocksControl.verify();

    mocksControl.reset();

    dummyMessageKey = MessageKey.SIMULATION_TERMINATING;
    // No site data content and simulation is terminating
    expect(mockProcessedSiteDataVO.isContainingSiteData()).andReturn(false);
    mocksControl.replay();
    Progress progress = informationTransformer.processedSiteDataToProgress(dummySimulationId,
                                                                           dummyCompoundIdentifier,
                                                                           dummyMessageKey,
                                                                           mockProcessedSiteDataVO);
    mocksControl.verify();
    assertNotNull(progress);
    assertTrue(InformationLevel.WARN == ((OverallProgress) progress).getLevel());

    mocksControl.reset();

    // No site data content and simulation isn't terminating
    dummyMessageKey = MessageKey.JOB_CREATED;
    expect(mockProcessedSiteDataVO.isContainingSiteData()).andReturn(false);
    mocksControl.replay();
    try {
      informationTransformer.processedSiteDataToProgress(dummySimulationId,
                                                         dummyCompoundIdentifier,
                                                         dummyMessageKey,
                                                         mockProcessedSiteDataVO);
      fail("Should not process a non-simulation terminating message key.");
    } catch (IllegalArgumentException e) {}
    mocksControl.verify();
  }

  @Test
  public void testSimulationToProgress() {
    final Simulation dummySimulation = null;
    dummyMessageKey = MessageKey.SIMULATION_ALREADY_EXISTS;
    try {
      informationTransformer.simulationToProgress(dummySimulationId, dummyCompoundIdentifier,
                                                  dummyMessageKey, dummySimulation);
      fail("Should not permit any other progress message than DISTRIBUTING.");
    } catch (IllegalArgumentException e) {}

    dummyMessageKey = MessageKey.DISTRIBUTING;
    final Progress progress = informationTransformer.simulationToProgress(dummySimulationId,
                                                                    dummyCompoundIdentifier,
                                                                    dummyMessageKey,
                                                                    dummySimulation);
    assertNotNull(progress);
    assertTrue(InformationLevel.TRACE == ((OverallProgress) progress).getLevel());
  }

  @Test
  public void testSiteDataToProgressFailsOnNullArg() {
    final SiteDataHolder dummySiteDataHolder = null;
    try {
      informationTransformer.siteDataToProgress(dummySimulationId, dummyCompoundIdentifier,
                                                dummyMessageKey, dummySiteDataHolder);
      fail("Should not allow a null site data holder object.");
    } catch (IllegalArgumentException e) {}
  }

  @Test
  public void testSiteDataToProgress() {
    // First try with an inappropriate progress message key
    final SiteDataHolder mockSiteDataHolder = mocksControl.createMock(SiteDataHolder.class);
    final AssayVO mockAssayVO = mocksControl.createMock(AssayVO.class);
    expect(mockSiteDataHolder.getAssay()).andReturn(mockAssayVO);
    final IonChannel fakeIonChannel = IonChannel.hERG;
    expect(mockSiteDataHolder.getIonChannel()).andReturn(fakeIonChannel);
    final SummaryDataRecord mockSummaryDataRecord = mocksControl.createMock(SummaryDataRecord.class);
    expect(mockSiteDataHolder.getSummaryDataRecord(false)).andReturn(mockSummaryDataRecord);
    final Map<IndividualDataRecord, List<DoseResponseDataRecord>> dummyNonSummaryDataRecords = 
      new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();
    expect(mockSiteDataHolder.getNonSummaryDataRecords()).andReturn(dummyNonSummaryDataRecords);
    dummyMessageKey = MessageKey.AGGR_SITEDATA_ANALYSIS;
    Progress progress = null;
    mocksControl.replay();
    try {
      progress = informationTransformer.siteDataToProgress(dummySimulationId, dummyCompoundIdentifier,
                                                           dummyMessageKey, mockSiteDataHolder);
      fail("Should not accept an inappropriate message key.");
    } catch (IllegalArgumentException e) {}
    mocksControl.verify();

    mocksControl.reset();

    // Using an appropriate message key (summary data record present)
    expect(mockSiteDataHolder.getAssay()).andReturn(mockAssayVO);
    expect(mockSiteDataHolder.getIonChannel()).andReturn(fakeIonChannel);
    expect(mockSiteDataHolder.getSummaryDataRecord(false)).andReturn(mockSummaryDataRecord);
    expect(mockSiteDataHolder.getNonSummaryDataRecords()).andReturn(dummyNonSummaryDataRecords);
    final String dummyAssayName = "dummyAssayName";
    expect(mockAssayVO.getName()).andReturn(dummyAssayName);
    dummyMessageKey = MessageKey.SITEDATA_BREAKDOWN;
    mocksControl.replay();
    progress = informationTransformer.siteDataToProgress(dummySimulationId, dummyCompoundIdentifier,
                                                         dummyMessageKey, mockSiteDataHolder);
    mocksControl.verify();
    assertNotNull(progress);
    assertTrue(InformationLevel.TRACE == ((OverallProgress) progress).getLevel());

    mocksControl.reset();

    // Using an appropriate message key (no summary data record present)
    expect(mockSiteDataHolder.getAssay()).andReturn(mockAssayVO);
    expect(mockSiteDataHolder.getIonChannel()).andReturn(fakeIonChannel);
    expect(mockSiteDataHolder.getSummaryDataRecord(false)).andReturn(null);
    expect(mockSiteDataHolder.getNonSummaryDataRecords()).andReturn(dummyNonSummaryDataRecords);
    expect(mockAssayVO.getName()).andReturn(dummyAssayName);
    dummyMessageKey = MessageKey.SITEDATA_BREAKDOWN;
    mocksControl.replay();
    progress = informationTransformer.siteDataToProgress(dummySimulationId, dummyCompoundIdentifier,
                                                         dummyMessageKey, mockSiteDataHolder);
    mocksControl.verify();
    assertNotNull(progress);
    assertTrue(InformationLevel.TRACE == ((OverallProgress) progress).getLevel());

  }
}