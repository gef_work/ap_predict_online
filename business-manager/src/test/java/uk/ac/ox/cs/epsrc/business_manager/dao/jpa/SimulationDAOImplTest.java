/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.dao.jpa;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

import java.math.BigDecimal;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.dao.SimulationDAO;
import uk.ac.ox.cs.epsrc.business_manager.dao.jpa.SimulationDAOImpl;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.value.object.CellModelVO;

/**
 * Unit test of the Simulation DAO implementation.
 *
 * @author geoff
 */
public class SimulationDAOImplTest {

  private EntityManager mockEntityManager;
  private IMocksControl mocksControl;
  private Simulation mockSimulation;
  private SimulationDAO simulationDAO;

  @Before
  public void setUp() {
    simulationDAO = new SimulationDAOImpl();

    mocksControl = createStrictControl();
    mockEntityManager = mocksControl.createMock(EntityManager.class);
    mockSimulation = mocksControl.createMock(Simulation.class);

    ReflectionTestUtils.setField(simulationDAO, "entityManager", mockEntityManager);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testFindByAllProperties() {
    /*
     * 1. Test if all arguments are non-null.
     */
    String dummyCompoundIdentifier = "dummyCompoundIdentifier";
    boolean dummyAssayGrouping = true;
    boolean dummyValueInheriting = true;
    boolean dummyBetweenGroups = true;
    boolean dummyWithinGroups = true;
    String dummyPC50EvaluationStrategies = "dummyPC50EvaluationStrategies";
    String dummyDoseResponseFittingStrategy = "dummyDoseResponseFittingStrategy";
    boolean dummyDoseResponseFittingRounded = true;
    boolean dummyDoseResponseFittingHillMinMax = true;
    Float dummyDoseResponseFittingHillMax = 2.4F;
    Float dummyDoseResponseFittingHillMin = 4.2F;
    short dummyCellModelIdentifier = CellModelVO.MIN_MODEL_IDENTIFIER;
    BigDecimal dummyPacingMaxTime = BigDecimal.ONE;

    final CriteriaBuilder mockCriteriaBuilder = mocksControl.createMock(CriteriaBuilder.class);
    expect(mockEntityManager.getCriteriaBuilder()).andReturn(mockCriteriaBuilder);
    final CriteriaQuery<Simulation> mockCriteriaQuery = mocksControl.createMock(CriteriaQuery.class);
    expect(mockCriteriaBuilder.createQuery(Simulation.class)).andReturn(mockCriteriaQuery);
    final Root<Simulation> mockRoot = mocksControl.createMock(Root.class);
    expect(mockCriteriaQuery.from(Simulation.class)).andReturn(mockRoot);

    Path<Object> fakePath = null;
    Predicate fakePredicate = null;

    expect(mockRoot.get(Simulation.PROPERTY_COMPOUND_IDENTIFIER)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyCompoundIdentifier)).andStubReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_ASSAY_GROUPING)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyAssayGrouping)).andStubReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_VALUE_INHERITING)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyValueInheriting)).andStubReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_BETWEEN_GROUPS)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyBetweenGroups)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_WITHIN_GROUPS)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyWithinGroups)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_PC50_EVALUATION_STRATEGIES)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyPC50EvaluationStrategies)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_STRATEGY)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyDoseResponseFittingStrategy)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_ROUNDING)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyDoseResponseFittingRounded)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MINMAX)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyDoseResponseFittingHillMinMax)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MIN)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyDoseResponseFittingHillMin)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MAX)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyDoseResponseFittingHillMax)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_CELLML_MODEL_IDENTIFIER)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyCellModelIdentifier)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_PACING_MAX_TIME)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyPacingMaxTime)).andReturn(fakePredicate);

    expect(mockCriteriaQuery.where(fakePredicate, fakePredicate, fakePredicate, fakePredicate,
                                   fakePredicate, fakePredicate, fakePredicate, fakePredicate,
                                   fakePredicate, fakePredicate, fakePredicate, fakePredicate,
                                   fakePredicate)).andReturn(mockCriteriaQuery);
    TypedQuery<Simulation> fakeTypedQuery = mocksControl.createMock(TypedQuery.class);
    expect(mockEntityManager.createQuery(mockCriteriaQuery)).andStubReturn(fakeTypedQuery);
    expect(fakeTypedQuery.getSingleResult()).andStubReturn(mockSimulation);

    mocksControl.replay();

    Simulation found = simulationDAO.findByAllProperties(dummyCompoundIdentifier, dummyAssayGrouping,
                                                         dummyValueInheriting, dummyBetweenGroups,
                                                         dummyWithinGroups,
                                                         dummyPC50EvaluationStrategies,
                                                         dummyDoseResponseFittingStrategy,
                                                         dummyDoseResponseFittingRounded,
                                                         dummyDoseResponseFittingHillMinMax,
                                                         dummyDoseResponseFittingHillMax,
                                                         dummyDoseResponseFittingHillMin,
                                                         dummyCellModelIdentifier,
                                                         dummyPacingMaxTime);

    mocksControl.verify();

    assertNotNull(found);

    mocksControl.reset();

    /*
     * 2. Test if optional null args are specified.
     */
    dummyDoseResponseFittingHillMax = null;
    dummyDoseResponseFittingHillMin = null;
    dummyPacingMaxTime = null;

    expect(mockEntityManager.getCriteriaBuilder()).andReturn(mockCriteriaBuilder);
    expect(mockCriteriaBuilder.createQuery(Simulation.class)).andReturn(mockCriteriaQuery);
    expect(mockCriteriaQuery.from(Simulation.class)).andReturn(mockRoot);

    expect(mockRoot.get(Simulation.PROPERTY_COMPOUND_IDENTIFIER)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyCompoundIdentifier)).andStubReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_ASSAY_GROUPING)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyAssayGrouping)).andStubReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_VALUE_INHERITING)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyValueInheriting)).andStubReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_BETWEEN_GROUPS)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyBetweenGroups)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_WITHIN_GROUPS)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyWithinGroups)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_PC50_EVALUATION_STRATEGIES)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyPC50EvaluationStrategies)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_STRATEGY)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyDoseResponseFittingStrategy)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_ROUNDING)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyDoseResponseFittingRounded)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MINMAX)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyDoseResponseFittingHillMinMax)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MIN)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.isNull(fakePath)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MAX)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.isNull(fakePath)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_CELLML_MODEL_IDENTIFIER)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyCellModelIdentifier)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_PACING_MAX_TIME)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.isNull(fakePath)).andReturn(fakePredicate);

    expect(mockCriteriaQuery.where(fakePredicate, fakePredicate, fakePredicate, fakePredicate,
                                   fakePredicate, fakePredicate, fakePredicate, fakePredicate,
                                   fakePredicate, fakePredicate, fakePredicate, fakePredicate,
                                   fakePredicate)).andReturn(mockCriteriaQuery);
    expect(mockEntityManager.createQuery(mockCriteriaQuery)).andStubReturn(fakeTypedQuery);
    expect(fakeTypedQuery.getSingleResult()).andStubReturn(mockSimulation);

    mocksControl.replay();

    found = simulationDAO.findByAllProperties(dummyCompoundIdentifier, dummyAssayGrouping,
                                              dummyValueInheriting, dummyBetweenGroups,
                                              dummyWithinGroups, dummyPC50EvaluationStrategies,
                                              dummyDoseResponseFittingStrategy,
                                              dummyDoseResponseFittingRounded,
                                              dummyDoseResponseFittingHillMinMax,
                                              dummyDoseResponseFittingHillMax,
                                              dummyDoseResponseFittingHillMin,
                                              dummyCellModelIdentifier,
                                              dummyPacingMaxTime);

    mocksControl.verify();

    assertNotNull(found);

    mocksControl.reset();

    /*
     * 3. Test if no results exception thrown
     */

    expect(mockEntityManager.getCriteriaBuilder()).andReturn(mockCriteriaBuilder);
    expect(mockCriteriaBuilder.createQuery(Simulation.class)).andReturn(mockCriteriaQuery);
    expect(mockCriteriaQuery.from(Simulation.class)).andReturn(mockRoot);

    expect(mockRoot.get(Simulation.PROPERTY_COMPOUND_IDENTIFIER)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyCompoundIdentifier)).andStubReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_ASSAY_GROUPING)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyAssayGrouping)).andStubReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_VALUE_INHERITING)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyValueInheriting)).andStubReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_BETWEEN_GROUPS)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyBetweenGroups)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_WITHIN_GROUPS)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyWithinGroups)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_PC50_EVALUATION_STRATEGIES)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyPC50EvaluationStrategies)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_STRATEGY)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyDoseResponseFittingStrategy)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_ROUNDING)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyDoseResponseFittingRounded)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MINMAX)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyDoseResponseFittingHillMinMax)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MIN)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.isNull(fakePath)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_DOSE_RESPONSE_FITTING_HILL_MAX)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.isNull(fakePath)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_CELLML_MODEL_IDENTIFIER)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.equal(fakePath, dummyCellModelIdentifier)).andReturn(fakePredicate);
    expect(mockRoot.get(Simulation.PROPERTY_PACING_MAX_TIME)).andStubReturn(fakePath);
    expect(mockCriteriaBuilder.isNull(fakePath)).andReturn(fakePredicate);

    expect(mockCriteriaQuery.where(fakePredicate, fakePredicate, fakePredicate, fakePredicate,
                                   fakePredicate, fakePredicate, fakePredicate, fakePredicate,
                                   fakePredicate, fakePredicate, fakePredicate, fakePredicate,
                                   fakePredicate)).andReturn(mockCriteriaQuery);
    expect(mockEntityManager.createQuery(mockCriteriaQuery)).andStubReturn(fakeTypedQuery);
    expect(fakeTypedQuery.getSingleResult()).andThrow(new NoResultException());

    mocksControl.replay();

    found = simulationDAO.findByAllProperties(dummyCompoundIdentifier, dummyAssayGrouping,
                                              dummyValueInheriting, dummyBetweenGroups,
                                              dummyWithinGroups, dummyPC50EvaluationStrategies,
                                              dummyDoseResponseFittingStrategy,
                                              dummyDoseResponseFittingRounded,
                                              dummyDoseResponseFittingHillMinMax,
                                              dummyDoseResponseFittingHillMax,
                                              dummyDoseResponseFittingHillMin,
                                              dummyCellModelIdentifier,
                                              dummyPacingMaxTime);

    mocksControl.verify();

    assertNull(found);
  }

  @Test
  public void testFindBySimulationId() {
    Long dummySimulationId = 1L;
    mockSimulation = mocksControl.createMock(Simulation.class);

    expect(mockEntityManager.find(Simulation.class, dummySimulationId)).andReturn(mockSimulation);

    mocksControl.replay();

    Simulation returnedSimulation = simulationDAO.findBySimulationId(dummySimulationId);

    mocksControl.verify();

    assertSame(mockSimulation, returnedSimulation);

    mocksControl.reset();
  }

  @Test
  public void testStore() {
    // Test a non-persisted simulation
    Long dummySimulationId = null;
    mockSimulation = mocksControl.createMock(Simulation.class);

    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    mockEntityManager.persist(mockSimulation);

    mocksControl.replay();

    Simulation returnedSimulation = simulationDAO.store(mockSimulation);

    mocksControl.verify();

    assertSame(mockSimulation, returnedSimulation);

    mocksControl.reset();

    // Test a persisted simulation.
    dummySimulationId = 1L;
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockEntityManager.merge(mockSimulation)).andReturn(mockSimulation);

    mocksControl.replay();

    returnedSimulation = simulationDAO.store(mockSimulation);

    mocksControl.verify();

    assertSame(mockSimulation, returnedSimulation);
  }

  @Test
  public void testUpdateOnJobsCompletion() {
    Long dummySimulationId = 1L;
    mockSimulation = mocksControl.createMock(Simulation.class);

    // Test situation whereby 
    expect(mockEntityManager.find(Simulation.class, dummySimulationId)).andReturn(mockSimulation);
    mockSimulation.assignCompleted();
    expect(mockSimulation.getId()).andReturn(dummySimulationId);
    expect(mockEntityManager.merge(mockSimulation)).andReturn(mockSimulation);

    mocksControl.replay();

    simulationDAO.updateOnJobsCompletion(dummySimulationId);

    mocksControl.verify();

    mocksControl.reset();

    // Test situation where Simulation not found
    expect(mockEntityManager.find(Simulation.class, dummySimulationId)).andReturn(null);

    mocksControl.replay();

    simulationDAO.updateOnJobsCompletion(dummySimulationId);

    mocksControl.verify();
  }
}