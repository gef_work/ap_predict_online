/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.business.workflow;

import static org.easymock.EasyMock.createControl;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.easymock.IMocksControl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.epsrc.business_manager.BusinessIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.business.information.messaging.BusinessManagerJMSTemplate;
import uk.ac.ox.cs.epsrc.business_manager.business.selection.c50.C50DataSelector;
import uk.ac.ox.cs.epsrc.business_manager.business.workflow.WorkflowProcessor;
import uk.ac.ox.cs.epsrc.business_manager.config.Configuration;
import uk.ac.ox.cs.epsrc.business_manager.entity.Simulation;
import uk.ac.ox.cs.epsrc.business_manager.service.SimulationService;
import uk.ac.ox.cs.epsrc.business_manager.value.datainterface.inputvaluesource.IndividualIVS;
import uk.ac.ox.cs.epsrc.business_manager.value.object.workflow.OutcomeVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.PC50EvaluationStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.business.evaluation.pc50.individual.IndividualC50DataStrategy;
import uk.ac.ox.cs.nc3rs.business_manager.api.exception.InvalidValueException;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.ICData;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.holder.SiteDataHolder;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.DoseResponseDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.IndividualDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.datainterface.record.SummaryDataRecord;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.object.AssayVO;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.IonChannel;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProcessingType;

/**
 * Test the IndividualPC50s inner class when only non-summary data available. 
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { C50DataSelector.class } )
public class IndividualPC50CountNonSummaryDataTest {

  private BigDecimal dummyEvaluatedPC501 = BigDecimal.TEN;
  private BigDecimal dummyEvaluatedPC502 = BigDecimal.ZERO;
  private BusinessManagerJMSTemplate mockBusinessManagerJmsTemplate;
  private Configuration mockConfiguration;
  private IMocksControl mocksControl;
  private IndividualDataRecord mockIndividualDataRecord1;
  private IndividualDataRecord mockIndividualDataRecord2;
  private Map<IndividualDataRecord, List<DoseResponseDataRecord>> mockNonSummaryData;
  private Simulation mockInputDataGatheringSimulation;
  private SimulationService mockSimulationService;
  private SiteDataHolder mockSiteDataHolder;
  private String dummyIndividualDataId1 = "dummyIndividualDataId1";
  private String dummyIndividualDataId2 = "dummyIndividualDataId2";
  private TestIndividualC50DataStrategy mockIndividualC50DataStrategy;
  private WorkflowProcessor workflowProcessor;
  private final Long dummySimulationId = 4L;
  private ProcessingType dummyProcessingType = ProcessingType.INPUT_DATA_GATHERING;
  private final String dummyCompoundIdentifier = "dummyCompoundIdentifier";

  @Test
  public void testEmptySummaryWithNonSummarySingleIndividual() throws IllegalArgumentException, InvalidValueException {
    workflowProcessor = new WorkflowProcessor();
    mocksControl = createStrictControl();
    mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    mockConfiguration = mocksControl.createMock(Configuration.class);
    mockSimulationService = mocksControl.createMock(SimulationService.class);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_CONFIGURATION,
                                 mockConfiguration);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE,
                                 mockSimulationService);

    final AssayVO mockAssayVO = mocksControl.createMock(AssayVO.class);
    mockSiteDataHolder = mocksControl.createMock(SiteDataHolder.class);
    expect(mockSiteDataHolder.getAssay()).andReturn(mockAssayVO);
    final IonChannel dummyIonChannel = IonChannel.hERG;
    expect(mockSiteDataHolder.getIonChannel()).andReturn(dummyIonChannel);
    final String dummyAssayName = "dummyAssayName";
    expect(mockAssayVO.getName()).andReturn(dummyAssayName);
    final SummaryDataRecord dummySummaryDataRecord = null;
    expect(mockSiteDataHolder.getSummaryDataRecord(false)).andReturn(dummySummaryDataRecord);

    final String dummyInvocationOrdersCSV = "";
    mockInputDataGatheringSimulation = mocksControl.createMock(Simulation.class);
    expect(mockInputDataGatheringSimulation.getPc50EvaluationStrategies()).andReturn(dummyInvocationOrdersCSV);
    final List<PC50EvaluationStrategy> dummyPC50EvaluationStrategies = new ArrayList<PC50EvaluationStrategy>();
    mockIndividualC50DataStrategy = mocksControl.createMock(TestIndividualC50DataStrategy.class);
    dummyPC50EvaluationStrategies.add(mockIndividualC50DataStrategy);
    expect(mockConfiguration.retrieveStrategiesByCSV(dummyInvocationOrdersCSV)).andReturn(dummyPC50EvaluationStrategies);

    final String dummyDoseResponseFittingStrategy = "dummyDoseResponseFittingStrategy";
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingStrategy()).andReturn(dummyDoseResponseFittingStrategy);
    final boolean isDoseResponseFittingRounding = true;
    expect(mockInputDataGatheringSimulation.isDoseResponseFittingRounding()).andReturn(isDoseResponseFittingRounding);
    final Float dummyDoseResponseFittingHillMax = 0.5F;
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingHillMax()).andReturn(dummyDoseResponseFittingHillMax);
    final Float dummyDoseResponseFittingHillMin = 2F;
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingHillMin()).andReturn(dummyDoseResponseFittingHillMin);

    mockNonSummaryData = new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();
    // Only 1 individual data record
    mockIndividualDataRecord1 = mocksControl.createMock(IndividualDataRecord.class);
    mockNonSummaryData.put(mockIndividualDataRecord1, null);
    expect(mockSiteDataHolder.getNonSummaryDataRecords()).andReturn(mockNonSummaryData);
    // In IndividualPC50s
    expect(mockIndividualDataRecord1.getId()).andReturn(dummyIndividualDataId1);
    // Out of IndividualPC50s

    // Emulate strategy traversal
    final Integer dummyDefaultInvocationOrder = 0;
    expect(((PC50EvaluationStrategy) mockIndividualC50DataStrategy).getOrder()).andReturn(dummyDefaultInvocationOrder);
    final String dummyDescription = "dummyDescription";
    expect(((PC50EvaluationStrategy) mockIndividualC50DataStrategy).getDescription()).andReturn(dummyDescription);
    expect(mockIndividualDataRecord1.getId()).andReturn(dummyIndividualDataId1).times(2);
    // In IndividualC50CacheManager
    final List<ICData> dummyEligibleC50Data1 = new ArrayList<ICData>();
    expect(mockIndividualDataRecord1.eligibleICData()).andReturn(dummyEligibleC50Data1);
    mockStatic(C50DataSelector.class);
    final ICData mockC50Data = mocksControl.createMock(ICData.class);
    expect(C50DataSelector.selectFrom(dummyEligibleC50Data1)).andReturn(mockC50Data);
    // Out of IndividualC50CacheManager
    expect(((IndividualC50DataStrategy) mockIndividualC50DataStrategy).evaluatePC50(isA(ICData.class),
                                                                                    isA(String.class)))
          .andReturn(dummyEvaluatedPC501);
    final BigDecimal dummyEligibleHillCoefficient = BigDecimal.TEN;
    expect(mockIndividualDataRecord1.eligibleHillCoefficient()).andReturn(dummyEligibleHillCoefficient);
    // End emulating strategy traversal

    replayAll();
    mocksControl.replay();

    OutcomeVO outcome = workflowProcessor.processWorkflow(dummyCompoundIdentifier, dummyProcessingType,
                                                          dummySimulationId,
                                                          mockInputDataGatheringSimulation,
                                                          mockSiteDataHolder);

    verifyAll();
    mocksControl.verify();

    assertEquals(mockNonSummaryData.size(), outcome.getInputValueSources().size());
    final IndividualIVS individualIVS = (IndividualIVS) outcome.getInputValueSources().get(0); 
    assertTrue(dummyEvaluatedPC501.compareTo(individualIVS.getpIC50()) == 0);
  }

  @Test
  public void testEmptySummaryWithNonSummaryMoreThanOneIndividual() throws IllegalArgumentException, InvalidValueException {
    workflowProcessor = new WorkflowProcessor();
    // The individual data records are going into a hashmap so not checking the execution order.
    mocksControl = createControl();
    mockBusinessManagerJmsTemplate = mocksControl.createMock(BusinessManagerJMSTemplate.class);
    mockConfiguration = mocksControl.createMock(Configuration.class);
    mockSimulationService = mocksControl.createMock(SimulationService.class);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_JMS_TEMPLATE,
                                 mockBusinessManagerJmsTemplate);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_CONFIGURATION,
                                 mockConfiguration);
    ReflectionTestUtils.setField(workflowProcessor, BusinessIdentifiers.COMPONENT_SIMULATION_SERVICE,
                                 mockSimulationService);

    final AssayVO mockAssayVO = mocksControl.createMock(AssayVO.class);
    mockSiteDataHolder = mocksControl.createMock(SiteDataHolder.class);
    expect(mockSiteDataHolder.getAssay()).andReturn(mockAssayVO);
    final IonChannel dummyIonChannel = IonChannel.hERG;
    expect(mockSiteDataHolder.getIonChannel()).andReturn(dummyIonChannel);
    final String dummyAssayName = "dummyAssayName";
    expect(mockAssayVO.getName()).andReturn(dummyAssayName);
    final SummaryDataRecord dummySummaryDataRecord = null;
    expect(mockSiteDataHolder.getSummaryDataRecord(false)).andReturn(dummySummaryDataRecord);

    final String dummyInvocationOrdersCSV = "";
    mockInputDataGatheringSimulation = mocksControl.createMock(Simulation.class);
    expect(mockInputDataGatheringSimulation.getPc50EvaluationStrategies()).andReturn(dummyInvocationOrdersCSV);
    final List<PC50EvaluationStrategy> dummyPC50EvaluationStrategies = new ArrayList<PC50EvaluationStrategy>();
    mockIndividualC50DataStrategy = mocksControl.createMock(TestIndividualC50DataStrategy.class);
    dummyPC50EvaluationStrategies.add(mockIndividualC50DataStrategy);
    expect(mockConfiguration.retrieveStrategiesByCSV(dummyInvocationOrdersCSV)).andReturn(dummyPC50EvaluationStrategies);

    final String dummyDoseResponseFittingStrategy = "dummyDoseResponseFittingStrategy";
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingStrategy()).andReturn(dummyDoseResponseFittingStrategy);
    final boolean isDoseResponseFittingRounding = true;
    expect(mockInputDataGatheringSimulation.isDoseResponseFittingRounding()).andReturn(isDoseResponseFittingRounding);
    final Float dummyDoseResponseFittingHillMax = 0.5F;
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingHillMax()).andReturn(dummyDoseResponseFittingHillMax);
    final Float dummyDoseResponseFittingHillMin = 2F;
    expect(mockInputDataGatheringSimulation.getDoseResponseFittingHillMin()).andReturn(dummyDoseResponseFittingHillMin);

    mockNonSummaryData = new HashMap<IndividualDataRecord, List<DoseResponseDataRecord>>();
    // 2 individual data records - Note that HashMap used so unit testing output will be inconsistent.
    mockIndividualDataRecord1 = mocksControl.createMock(IndividualDataRecord.class);
    mockIndividualDataRecord2 = mocksControl.createMock(IndividualDataRecord.class);
    mockNonSummaryData.put(mockIndividualDataRecord1, null);
    mockNonSummaryData.put(mockIndividualDataRecord2, null);
    expect(mockSiteDataHolder.getNonSummaryDataRecords()).andReturn(mockNonSummaryData);
    // In IndividualPC50s
    expect(mockIndividualDataRecord1.getId()).andReturn(dummyIndividualDataId1);
    expect(mockIndividualDataRecord2.getId()).andReturn(dummyIndividualDataId2);
    // Out of IndividualPC50s

    // Emulate strategy traversal
    final Integer dummyDefaultInvocationOrder = 0;
    expect(((PC50EvaluationStrategy) mockIndividualC50DataStrategy).getOrder()).andReturn(dummyDefaultInvocationOrder);
    final String dummyDescription = "dummyDescription";
    expect(((PC50EvaluationStrategy) mockIndividualC50DataStrategy).getDescription()).andReturn(dummyDescription);
    expect(mockIndividualDataRecord1.getId()).andReturn(dummyIndividualDataId1).times(2);
    expect(mockIndividualDataRecord2.getId()).andReturn(dummyIndividualDataId2).times(2);
    // In IndividualC50CacheManager
    final List<ICData> dummyEligibleC50Data1 = new ArrayList<ICData>();
    expect(mockIndividualDataRecord1.eligibleICData()).andReturn(dummyEligibleC50Data1);
    final List<ICData> dummyEligibleC50Data2 = new ArrayList<ICData>();
    expect(mockIndividualDataRecord2.eligibleICData()).andReturn(dummyEligibleC50Data2);
    mockStatic(C50DataSelector.class);
    final ICData mockC50Data1 = mocksControl.createMock(ICData.class);
    expect(C50DataSelector.selectFrom(dummyEligibleC50Data1)).andReturn(mockC50Data1);
    final ICData mockC50Data2 = mocksControl.createMock(ICData.class);
    expect(C50DataSelector.selectFrom(dummyEligibleC50Data2)).andReturn(mockC50Data2);
    // Out of IndividualC50CacheManager
    expect(((IndividualC50DataStrategy) mockIndividualC50DataStrategy).evaluatePC50(isA(ICData.class),
                                                                                    isA(String.class)))
           /* At this unit test point on replay we don't know which individual data record called 
              evaluatePC50, i.e. data record 2 may have been processed first but we're returning
              data record 1's pIC50! */
          .andReturn(dummyEvaluatedPC501);
    expect(((IndividualC50DataStrategy) mockIndividualC50DataStrategy).evaluatePC50(isA(ICData.class),
                                                                                    isA(String.class)))
          .andReturn(dummyEvaluatedPC502);
    final BigDecimal dummyEligibleHillCoefficient1 = new BigDecimal("0.5");
    expect(mockIndividualDataRecord1.eligibleHillCoefficient()).andReturn(dummyEligibleHillCoefficient1);
    final BigDecimal dummyEligibleHillCoefficient2 = new BigDecimal("0.75");
    expect(mockIndividualDataRecord2.eligibleHillCoefficient()).andReturn(dummyEligibleHillCoefficient2);
    // End emulating strategy traversal

    replayAll();
    mocksControl.replay();

    OutcomeVO outcome = workflowProcessor.processWorkflow(dummyCompoundIdentifier, dummyProcessingType,
                                                          dummySimulationId,
                                                          mockInputDataGatheringSimulation,
                                                          mockSiteDataHolder);

    verifyAll();
    mocksControl.verify();

    // We're only interested in the number of IndividualPC50s created.
    assertEquals(mockNonSummaryData.size(), outcome.getInputValueSources().size());
  }
}