/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.epsrc.business_manager.ws;

import java.io.StringWriter;
import java.util.Set;
import java.util.LinkedHashSet;

import javax.sql.DataSource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Source;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.transaction.AfterTransaction;
import org.springframework.ws.test.server.MockWebServiceClient;

import static org.springframework.ws.test.server.RequestCreators.*;
import static org.springframework.ws.test.server.ResponseMatchers.*; 

import org.springframework.xml.transform.StringSource;

import uk.ac.ox.cs.epsrc.business_manager.BusinessTestIdentifiers;
import uk.ac.ox.cs.epsrc.business_manager.util.DatabaseUtil;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.ProcessSimulationsRequest;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.SchemaObjectBuilder;
import uk.ac.ox.cs.epsrc.business_manager.ws.schema.jaxb.SimulationDetail;
import uk.ac.ox.cs.nc3rs.business_manager.api.value.type.ProblemType;

/**
 * Integration test which emulates a mock WS client sending a process simulations request which is
 * received by the system and mapped to an endpoint <code>wsInboundGateway</code>.
 *
 * @author geoff
 */
public class MessageEndpointViaProcessSimulationsWSCallIT extends AbstractMessageEndpointViaWSCall {

  //@Autowired @Qualifier(BusinessTestIdentifiers.COMPONENT_BUSINESS_DATA_SOURCE)
  private DataSource dataSource;

  private Marshaller processSimulationsJAXBMarshaller;

  private static final String multipleStructure = 
    "  <ns2:ProcessSimulationsResponseStructure>" +
    "    <ns2:CompoundIdentifier>%s</ns2:CompoundIdentifier>" +
    "    <ns2:SimulationId>%s</ns2:SimulationId>" +
//    "    <ns2:NewData>true</ns2:NewData>" +
    "  </ns2:ProcessSimulationsResponseStructure>";

  // response string prefix used in preference a marshaller (as the namespace in the response differs).
  private static final String processSimulationsResponseStringCompletePre = 
    "<ns2:ProcessSimulationsResponse xmlns:ns2='" + NAMESPACE + "'>";
  // response string postfix used in preference a marshaller (as the namespace in the response differs).
  private static final String processSimulationsResponseStringCompletePost = 
    "</ns2:ProcessSimulationsResponse>";

  private static String responseCreator(final int times) {
    final StringBuffer response = new StringBuffer();
    response.append(processSimulationsResponseStringCompletePre);
    for (int time = 0; time < times; time++) {
      response.append(multipleStructure);
    }
    response.append(processSimulationsResponseStringCompletePost);
    return response.toString();
  }

  /**
   * Create the mock client and JAXB marshaller.
   */
  //@Before
  public void setUp() throws JAXBException {
    final JAXBContext processSimulationsJAXBContext = JAXBContext.newInstance(ProcessSimulationsRequest.class);
    processSimulationsJAXBMarshaller = processSimulationsJAXBContext.createMarshaller();

    setMockClient(MockWebServiceClient.createClient(getApplicationContext()));
  }

  //@AfterTransaction
  public void tearDown() {
    DatabaseUtil.resetSequence(dataSource, "sequence_pks_business_manager", "simulation_id", 1L);
  }

  // Create the JAXB request for the compound.
  private Source processSingleSimulationsRequestPayloadCreator(final Set<String> compoundIdentifiers)
                                                               throws JAXBException {
    final ProcessSimulationsRequest simulationRequest = objectFactory.createProcessSimulationsRequest();
    if (compoundIdentifiers != null) {
      for (final String compoundIdentifier : compoundIdentifiers) {
        final SimulationDetail simulationDetail = SchemaObjectBuilder.createSimulationDetail(compoundIdentifier,
                                                                                             false, false);
        simulationRequest.getSimulationDetail().add(simulationDetail);
      }
    }

    final StringWriter stringWriter = new StringWriter();
    processSimulationsJAXBMarshaller.marshal(simulationRequest, stringWriter);
    final String simulationRequestXml = stringWriter.toString();

    return new StringSource(simulationRequestXml);
  }

  @Test
  public void doNothing() {
    
  }

  /**
   * Emulate a client request to process simulations.
   */
  //@Test
  public void testProcessSingleSimulationsRequest() throws JAXBException {
    final Set<String> dummyCompounds = new LinkedHashSet<String>();
    dummyCompounds.add("DC1");
    final Source requestPayload = processSingleSimulationsRequestPayloadCreator(dummyCompounds);

    final Source responsePayload = new StringSource(String.format(responseCreator(1),
                                                                  new Object[] { dummyCompounds.iterator().next(),
                                                                                 1L } ));

    getMockClient().sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
  }

  /**
   * Emulate a client request to process multiple compound identifiers.
   */
  //@Test
  public void testProcessMultipleSimulationRequests() throws JAXBException {
    final Set<String> dummyCompounds = new LinkedHashSet<String>();
    dummyCompounds.add("DC1");
    dummyCompounds.add("DC5");

    final Source requestPayload = processSingleSimulationsRequestPayloadCreator(dummyCompounds);

    final Source responsePayload = new StringSource(String.format(responseCreator(2),
                                                                  new Object[] { "DC1", 1L, "DC5", 2L }));
    getMockClient().sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
  }

  /**
   * Emulate a client request to process simulations without providing a compound identifier.
   */
  //@Test
  public void testProcessSingleSimulationsRequestButNoCompoundIdentifiers() throws JAXBException {
    final Source requestPayload = processSingleSimulationsRequestPayloadCreator(null);

    final Source responsePayload = new StringSource(String.format(baseSOAPFaultResponseString,
                                                    new Object[] { ProblemType.INVALID_COMPOUND_NAME.getDescription() } ));

    getMockClient().sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
  }

  /**
   * Test that a SOAP fault is returned if there's a nonsensical compound sent.
   */
  //@Test
  public void testWSCallReturnsSOAPFaultOnInvalidCompound() throws JAXBException {
    final Set<String> dummyCompounds = new LinkedHashSet<String>();
    dummyCompounds.add("");
    final Source requestPayload = processSingleSimulationsRequestPayloadCreator(dummyCompounds);

    final Source responsePayload = new StringSource(String.format(baseSOAPFaultResponseString,
                                                                  new Object[] { ProblemType.INVALID_COMPOUND_NAME.getDescription() } ));

    getMockClient().sendRequest(withPayload(requestPayload)).andExpect(payload(responsePayload));
  }
}