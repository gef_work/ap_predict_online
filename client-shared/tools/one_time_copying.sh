#!/bin/bash -e

base=`pwd`
cmd="cp -iv"

${cmd} ${base}/src/main/resources/META-INF/data/spring-security/local/sample.users.sql ${base}/src/main/resources/META-INF/data/spring-security/local/users.sql
${cmd} ${base}/src/main/resources/META-INF/resources/resources/css/site/sample.client-shared-site.css ${base}/src/main/resources/META-INF/resources/resources/css/site/client-shared-site.css
${cmd} ${base}/src/main/resources/META-INF/resources/WEB-INF/spring/ctx/config/sample.appCtx.features.xml ${base}/src/main/resources/META-INF/resources/WEB-INF/spring/ctx/config/appCtx.features.xml
${cmd} ${base}/src/main/resources/META-INF/resources/WEB-INF/tiles/layout/common/sample.logo.jsp ${base}/src/main/resources/META-INF/resources/WEB-INF/tiles/layout/common/logo.jsp
${cmd} ${base}/src/main/resources/META-INF/resources/WEB-INF/tiles/layout/common/sample.logout.jsp ${base}/src/main/resources/META-INF/resources/WEB-INF/tiles/layout/common/logout.jsp
${cmd} ${base}/src/main/resources/META-INF/resources/WEB-INF/tiles/layout/contact/sample.contact.jsp ${base}/src/main/resources/META-INF/resources/WEB-INF/tiles/layout/contact/contact.jsp
${cmd} ${base}/src/main/resources/WEB-INF/spring/authn/sample.appCtx.bespoke.xml ${base}/src/main/resources/WEB-INF/spring/authn/appCtx.bespoke.xml
${cmd} ${base}/src/main/resources/WEB-INF/spring/authn/sample.appCtx.sitePreauthFilter.xml ${base}/src/main/resources/WEB-INF/spring/authn/appCtx.sitePreauthFilter.xml
${cmd} ${base}/src/main/resources/WEB-INF/spring/sample.root-context.site.xml ${base}/src/main/resources/WEB-INF/spring/root-context.site.xml
${cmd} ${base}/src/properties/sample.cs-filter.properties ${base}/src/properties/cs-filter.properties
${cmd} ${base}/sample.pom.xml ${base}/pom.xml