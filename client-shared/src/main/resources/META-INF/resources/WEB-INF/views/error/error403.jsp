<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="generic_top.jsp" %>
    <div style="margin: auto; width: 40%;">
      <spring:message code="cs_error.403_start" />
      <ul>
        <li><spring:message code="cs_error.403_1" /></li>
        <li><spring:message code="cs_error.403_2" /></li>
        <li><spring:message code="cs_error.403_3" /></li>
      </ul>
      <spring:message code="cs_error.403_end" />
    </div>
<%@include file="generic_bottom.jsp" %>