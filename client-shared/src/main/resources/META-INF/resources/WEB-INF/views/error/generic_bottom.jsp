<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers" %>
<c:set var="view_contact"><%=ClientSharedIdentifiers.VIEW_CONTACT%></c:set>
<c:set var="contact_name"><spring:message code="cs_general.contact" /></c:set>
    <p style="text-align: center;">
      <spring:message code="cs_error.contact_admin"
                      arguments="${pageContext.request.contextPath},${view_contact},${contact_name}"
                      htmlEscape="false" />
    </p>
    <p style="text-align: center;">
      <a href="${pageContext.request.contextPath}/" 
         title="<spring:message code="cs_general.go_to" />&nbsp;<spring:message code="cs_general.home" />">
        - <spring:message code="cs_general.home" /> -
      </a>
    </p>
  </body>
</html>
