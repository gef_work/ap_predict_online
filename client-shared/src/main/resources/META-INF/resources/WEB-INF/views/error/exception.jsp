<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="generic_top.jsp" %>
    <p style="text-align: center;">
      <spring:message code="cs_error.problem" />:
<pre style="border-top: solid grey 1px; border-bottom: solid grey 1px; padding: 3px;">
<c:out value="${exception}" />
</pre>
    </p>
    <p style="text-align: center;">
      <spring:message code="cs_error.message" />:
<pre style="border-top: solid grey 1px; border-bottom: solid grey 1px; padding: 3px;">
<c:out value="${exception.message}" />.
</pre>
    </p>
<%@include file="generic_bottom.jsp" %>