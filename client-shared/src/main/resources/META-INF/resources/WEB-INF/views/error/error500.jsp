<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="generic_top.jsp" %>
    <p class="cs_centre">
      <spring:message code="cs_error.500_1" />
    </p>
    <p class="cs_centre">
      <spring:message code="cs_error.500_2" />
    </p>
<%@include file="generic_bottom.jsp" %>