<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
  <head>
    <title><spring:message code="cs_error.problem" /> !</title>
    <link href="<c:url value="/resources/css/client-shared.css" />"
          rel="stylesheet" 
          type="text/css" />
    <link href="<c:url value="/resources/css/site/client-shared-site.css" />"
          rel="stylesheet"
          type="text/css" />
  </head>
  <body>
    <div class="cs_top_header_float"
         id="cs_top_header_logo">
      &nbsp;
    </div>
    <p class="cs_clearing" />