<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
  /* <![CDATA[ */
  /* show a message temporarily */
  function cs_show_message(message_text) {
    var div = jQuery('#message_text');
    div.empty();
    div.html(message_text);
    div.show();
    setTimeout(function() { cs_hide_message() }, 5000);
  }
  /* hide the message */
  function cs_hide_message() {
    var div = jQuery('#message_text');
    div.hide();
  }
  /* show an error message */
  function cs_show_message_error(error_text) {
    var div = jQuery('#error_text');
    div.empty();
    div.html(error_text);
    div.show();
  }
  /* hide an error message */
  function cs_hide_message_error() {
    var div = jQuery('#error_text');
    div.hide();
  }
  /* ]]> */
</script>
<c:choose>
  <c:when test="${not empty ma_message_error}">
<div class="error_text cs_rounded_5"
     id="error_text"><c:out value="${ma_message_error}"/> </div>
  </c:when>
  <c:otherwise>
<div class="error_text cs_rounded_5"
     id="error_text"
     style="display: none;"></div>
  </c:otherwise>
</c:choose>
<div class="error_text cs_rounded_5"
     id="js_error"
     style="display: none;"></div>
<div id="js_error_workload"
     class="error_text cs_rounded_5"
     style="display: none;"></div>
<div class="error_text cs_rounded_5"
     id="js_error_results"
     style="display: none;"></div>
<div id="js_error_progress"
     class="error_text cs_rounded_5" 
     style="display: none;"></div>
<div id="message_text"
     class="message_text cs_rounded_5"
     style="display:none;"></div>
