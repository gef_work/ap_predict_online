<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div>
  <p>
    <spring:message code="cs_contact.line1" />
  </p>

  <div style="margin-left: 20px;">
    <%@include file="contact.jsp" %>
  </div>

  <p style="margin-left: 40px;">
    <button type="button"
            onclick="history.go(-1)"><spring:message code="cs_general.button_back" /></button>
  </p>
</div>
