      <form class="cs_inline" action="${logoutURL}" method="post">
        <input type="submit"
               value="<spring:message code="cs_general.log_out" />" />
        <input type="hidden"
               name="${_csrf.parameterName}"
               value="${_csrf.token}" />
      </form>
