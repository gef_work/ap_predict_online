<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="shadowed">
  <div class="cs_default_width">
    <h3><spring:message code="cs_privacy.title" /></h3>
    <p>
      <spring:message code="cs_privacy.line_1" htmlEscape="false" />
    </p>
    <p>
      Information Commissioner's Office : <a href="http://ico.org.uk/for_organisations/privacy_and_electronic_communications/the_guide/cookies"
                                             title="The reason this page exists">Cookies Regulations and the New EU Cookie Law</a>.
    </p>
    <h4><spring:message code="cs_privacy.important_security" /></h4>
    <p>
      <spring:message code="cs_privacy.line_2" htmlEscape="false" />
    </p>
    <p style="margin-left: 40px;">
      <button type="button"
              onclick="history.go(-1)"><spring:message code="cs_general.button_back" /></button>
    </p>
  </div>
</div>