<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<tiles:insertAttribute name="http" />
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <tiles:insertAttribute name="page.title" />
    <tiles:insertAttribute name="html_head.general" />
    <tiles:insertAttribute name="html_head.cs_javascript" />
    <tiles:insertAttribute name="html_head.javascript" />
    <tiles:insertAttribute name="page.javascript" />
    <tiles:insertAttribute name="html_head.style" />
    <tiles:insertAttribute name="page.style" />
  </head>
  <body>
    <div class="body cs_clearing">
      <tiles:insertAttribute name="top" />
    </div>
    <div class="body cs_clearing">
      <tiles:insertAttribute name="notification" />
    </div>
    <div class="body cs_clearing">
      <tiles:insertAttribute name="subnavigation" />
    </div>
    <div class="body cs_clearing">
      <tiles:insertAttribute name="middle" />
    </div>
    <div class="body cs_clearing">
      <tiles:insertAttribute name="bottom" />
    </div>
  </body>
</html>