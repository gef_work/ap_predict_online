<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="shadowed">
  <div class="cs_default_width">
    <p>
      <spring:message code="cs_about.introduction" htmlEscape="false" />
    </p>
    <p>
      <img style="border:2px solid #000000; padding:10px;" 
           alt="Flowchart of information from ion channel screening informing action potential models."
           src="<c:url value="/resources/img/sim_flowchart_web.png" />" />
    </p>
    <p>
      <spring:message code="cs_about.please_read" /> :
      <a href="http://dx.doi.org/10.6084/m9.figshare.1039436"
         title="<spring:message code="cs_about.doc_title" />"
         target="blank"><spring:message code="cs_about.doc_title"/></a> (pdf).
      <br />
      <spring:message code="cs_about.journal_information" /> :
      <a href="http://dx.doi.org/10.1016/j.vascn.2015.05.002"
         title="<spring:message code="cs_about.journal_title" />"
         target="blank"><spring:message code="cs_about.journal_title" /></a>.
      <br />
      <spring:message code="cs_about.portal_here" /> :
      <a href="https://bitbucket.org/gef_work/ap_predict_online" target="blank">Bitbucket</a>.
    </p>
    <p>
      <spring:message code="cs_about.gary_funding" />
    </p>
    <a href="http://www.epsrc.ac.uk/"
          title="Engineering and Physical Sciences Research Council"
          target="blank"><img style="padding:10px;" 
                              alt="Engineering and Physical Sciences Research Council logo"
                              src="<c:url value="/resources/img/epsrc_logo.png" />" /></a>

    <a href="http://www.nc3rs.org.uk/"
          title="National Centre for the Replacement, Refinement &amp; Reduction of Animals in Research"
          target="blank"><img style="padding:10px;" 
                              alt="National Centre for the Replacement, Refinement &amp; Reduction of Animals in Research logo"
                              src="<c:url value="/resources/img/nc3rs_logo.gif" />" /></a>

    <p style="margin-left: 40px;">
      <button type="button"
              onclick="history.go(-1)"><spring:message code="cs_general.button_back" /></button>
    </p>
  </div>
</div>