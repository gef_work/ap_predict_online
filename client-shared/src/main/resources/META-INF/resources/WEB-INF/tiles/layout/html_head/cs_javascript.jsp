<%@taglib prefix="c"
          uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="spring"
          uri="http://www.springframework.org/tags" %>
<%@page import="uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers" %>
      <%-- Correspond to properties defined in AJAX controller inner class --%>
<c:set value="<%= ClientSharedIdentifiers.CONTROLLER_KEY_EXCEPTION %>"
       var="cs_key_exception" />
<c:set value="<%= ClientSharedIdentifiers.CONTROLLER_KEY_JSON %>"
       var="cs_key_json" />
<sec:authorize access="hasAnyRole('ROLE_USER','ROLE_POWER_USER')">
<c:set value="<%= ClientSharedIdentifiers.MODEL_ATTRIBUTE_WORKLOAD %>"
       var="ma_workload" />
</sec:authorize>
    <script type="text/javascript">
      /* <![CDATA[ */

      var CS_KEY_EXCEPTION = '<c:out value="${cs_key_exception}" />';
      var CS_KEY_JSON = '<c:out value="${cs_key_json}" />';
      var MODEL_ATTRIBUTE_WORKLOAD = '<c:out value="${ma_workload}" />';
      var URL_WORKLOAD = '<c:url value="<%=ClientSharedIdentifiers.URL_PREFIX_AJAX_WORKLOAD%>" />';

      /* ]]> */
    </script>
    <script src="<c:url value="/resources/js/jquery/jquery-1.8.3.min.js" />"
            type="text/javascript"></script>
    <script src="<c:url value="/resources/js/jquery/ui/jquery-ui-1.9.2.custom.min.js" />"
            type="text/javascript"></script>
    <script src="<c:url value="/resources/js/cs-general.js" />"
            type="text/javascript"></script>
    <script src="<c:url value="/resources/js/cs-error.js" />"
            type="text/javascript"></script>
    <script src="<c:url value="/resources/js/cs-graph.js" />"
            type="text/javascript"></script>
<sec:authorize access="hasAnyRole('ROLE_USER','ROLE_POWER_USER')">
    <script src="<c:url value="/resources/js/cs-workload.js" />"
            type="text/javascript"></script>
</sec:authorize>
    <script type="text/javascript">
     /* <![CDATA[ */
     jQuery(document).ready(
       function() {
<sec:authorize access="hasAnyRole('ROLE_USER','ROLE_POWER_USER')">
         <%-- notification.jsp --%>
         js_error_workload_el = jQuery('#js_error_workload');
         workload_el = jQuery('#workload');

         ajax_for_workload();
</sec:authorize>
       }
     );
     /* ]]> */
    </script>