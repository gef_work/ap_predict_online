<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers" %>
<c:url value="<%=ClientSharedIdentifiers.LOGOUT%>"
       var="logoutURL" />
<div class="cs_top_header_float"
     id="cs_top_header_logo">
<%@include file="logo.jsp" %>
</div>
<div class="cs_top_header_float"
     id="cs_top_header_title" >&nbsp;</div>
<div class="cs_top_header_float"
     id="cs_top_header_links">
  <div>
    <a href="<c:url value="/" />"
       title="<spring:message code="cs_general.go_to" />&nbsp;<spring:message code="cs_general.home" />"><spring:message code="cs_general.home" /></a>
    &nbsp;&nbsp;
    <a href="<%=ClientSharedIdentifiers.VIEW_ABOUT%>"
       title="<spring:message code="cs_general.go_to" />&nbsp;<spring:message code="cs_general.about" />"><spring:message code="cs_general.about" /></a>
    |
    <a href="<%=ClientSharedIdentifiers.VIEW_CONTACT%>"
       title="<spring:message code="cs_general.go_to" />&nbsp;<spring:message code="cs_general.contact" />"><spring:message code="cs_general.contact" /></a>
    |
    <a href="<%=ClientSharedIdentifiers.VIEW_PRIVACY%>"
       title="<spring:message code="cs_general.go_to" />&nbsp;<spring:message code="cs_general.privacy" />"><spring:message code="cs_general.privacy" /></a>
<sec:authorize access="hasAnyRole('ROLE_USER','ROLE_POWER_USER')">
    <div class="cs_inline"
         style="padding-left: 30px;">
      <img class="cs_user_icon"
           src="<c:url value="/resources/img/user.png" />" />
      <sec:authentication property="principal.username" />&nbsp;<sec:authorize access="hasRole('ROLE_POWER_USER')"><sup>(Pwr)</sup></sec:authorize>
      <%@include file="logout.jsp" %>
    </div>
    <div>
      <p>
  <spring:message code="cs_general.workload" />: <span id="workload"></span>
      </p>
    </div>
</sec:authorize>
  </div>
</div>
