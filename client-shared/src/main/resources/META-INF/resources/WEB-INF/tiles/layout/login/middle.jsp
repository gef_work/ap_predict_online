<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers" %>
<c:set var="view_about"><%=ClientSharedIdentifiers.VIEW_ABOUT%></c:set>
<c:set var="about_name"><spring:message code="cs_general.about" htmlEscape="false" /></c:set>
<c:set var="contact_name"><spring:message code="cs_general.contact" htmlEscape="false" /></c:set>
<c:set var="view_contact"><%=ClientSharedIdentifiers.VIEW_CONTACT%></c:set>
<c:set var="registration_result" value="${not empty ma_registration_error || not empty ma_registration_success}" />
  <p>
    <spring:message code="cs_login.note" />
    (<spring:message code="cs_general.see" />&nbsp;
     <a href="<%=ClientSharedIdentifiers.VIEW_PRIVACY%>"
        title="<spring:message code="cs_privacy.title" />"><spring:message code="cs_privacy.title" /></a>
     <spring:message code="cs_general.for_more_information" />.)
  </p>
  <div class="shadowed">
    <%-- top row : login/registration + welcome text --%>
    <div>
      <div id="cs_login_registration">
        <div id="tabbed" style="padding-top: 15px;">
          <button id="login"
                  title="<spring:message code="cs_login.login_click" />"
<c:if test="${!registration_result}">class="cs_no_display"</c:if>><spring:message code="cs_login.login_form" /></button>
          <button id="register"
                  title="<spring:message code="cs_login.registration_click" />"
<c:if test="${registration_result}">class="cs_no_display"</c:if>><spring:message code="cs_login.registration_form" /></button>
        </div>

        <div id="login_tab" <c:if test="${registration_result}">class="cs_no_display"</c:if>>
<c:choose>
  <c:when test="${not empty _csrf.parameterName}">
    <%-- On page display or refresh a new session started and the CSRF token is available --%>
          <form method="post"
                name="login"
                action="j_spring_security_check">
            <input type="hidden"
                   name="${_csrf.parameterName}"
                   value="${_csrf.token}" />
<%-- authenication-failure_url= in appCtx.prepopulated.xml --%>
<%
if (request.getParameter("login_error") != null) {
%>
            <div class="error_text center">
              <p>
                <b><spring:message code="cs_login.invalid_login" /></b>
              </p>
            </div>
<%
}
%>
            <p><spring:message code="cs_login.login_details" /> :</p>
            <table>
              <tr>
                <td align="right"><spring:message code="cs_login.username" /></td>
                <td><input type="text" name="username" /></td>
              </tr>
              <tr>
                <td align="right"><spring:message code="cs_login.password" /></td>
                <td><input type="password" name="password" /></td>
              </tr>
              <tr>
                <td colspan="2" align="right">
                  <input type="submit" value="<spring:message code="cs_login.login" />" />
                  <input type="reset" value="<spring:message code="cs_general.reset" />" />
                </td>
              </tr>
            </table>
          </form>
  </c:when>
  <c:otherwise>
    <%-- If session has been lost prior to viewing page, e.g. after registration, or if GET to /registration --%>
          <p>
            <spring:message code="cs_login.page_refresh_required" /> -
            <a href="<c:url value="/" />"
               title="<spring:message code="cs_general.click_here" />"><spring:message code="cs_general.click_here" /></a>.
          </p>
  </c:otherwise>
</c:choose>
        </div>
        <div id="register_tab" <c:if test="${!registration_result}">class="cs_no_display"</c:if>>
<c:choose>
  <c:when test="${not empty ma_registration_success}">
        <div class="error_text center">
          <p>
            <b><spring:message code="cs_login.registration_recorded" /></b>
          </p>
          <%-- Need to reset CSRF token --%>
          <p>
            <spring:message code="cs_login.page_refresh_required" /> - 
            <a href="<c:url value="/" />"
               title="<spring:message code="cs_general.click_here" />"><spring:message code="cs_general.click_here" /></a>.
          </p>
        </div>
  </c:when>
  <c:when test="${empty ma_registration_success && not ma_mailer_configured}">
        <%-- If not a successful prior registration and no SMTP mailer configured (so no email
             sending possible), suggest the contact page. --%>
        <div>
          <p>
            <spring:message code="cs_login.use_contact" htmlEscape="false"
                            arguments="${view_contact},${contact_name}"/>
          </p>
        </div>
  </c:when>
  <c:otherwise>
        <form action="<%=ClientSharedIdentifiers.ACTION_REGISTRATION%>"
              id="registration_form"
              method="post">
    <c:if test="${not empty ma_registration_error}">
          <div class="error_text center">
            <p>
              <b><spring:message code="cs_general.problem" /> : <c:out value="${ma_registration_error}" /></b>
            </p>
          </div>
    </c:if>
          <p>
            <spring:message code="cs_login.registration_details" /> : <br />
            <span style="font-size: smaller;">(* <spring:message code="cs_general.required_field" />)</span>
          </p>
          <table>
            <tr>
              <td style="align: right;">* <spring:message code="cs_login.email_address" /> </td>
              <td><input type="text" name="<%=ClientSharedIdentifiers.PARAM_NAME_EMAIL_ADDRESS%>"
                         size="30" maxlength="100"
                         value="<c:out value="${ma_email_address}" />" /></td>
            </tr>
    <c:if test="${not empty ma_public_recaptcha_key}">
            <%-- If no public recaptcha key then don't show the recaptcha. --%>
            <tr>
              <td colspan="2">
                <div class="g-recaptcha" data-sitekey="${ma_public_recaptcha_key}"></div>
              </td>
            </tr>
            <tr>
              <td colspan="2">* <spring:message code="cs_login.enter_recaptcha" /></td>
            </tr>
    </c:if>
            <tr>
              <td colspan="2" style="text-align: right">
                <input type="submit" value="<spring:message code="cs_login.register" />" />
                <input type="reset" value="<spring:message code="cs_general.reset" />" />
              </td>
            </tr>
          </table>
        </form>
  </c:otherwise>
</c:choose>
        </div>
      </div>

      <%-- Front page welcome text --%>
      <div id="cs_welcome">
        <p style="text-align: center; font-weight: bold;"><spring:message code="cs_login.welcome_1" /></p>
        <p><spring:message code="cs_login.welcome_2" htmlEscape="false" arguments="${view_contact},${contact_name}"/></p>
        <p><spring:message code="cs_login.welcome_3" htmlEscape="false" arguments="${view_about},${about_name}"/></p>
      </div>
    </div>

    <%-- End the 'two column' view --%>
    <div style="clear: both;">&nbsp;</div>

    <%-- bottom row slideshow --%>
    <div id="cs_slideshow" 
         class="theme-default">
      <div id="slider" class="nivoSlider">
        <img src="<c:url value="/resources/img/step1.png" />" data-thumb="<c:url value="/resources/img/step1.png" />"
             alt="" title="#caption_1" />
        <img src="<c:url value="/resources/img/step2.png" />" data-thumb="<c:url value="/resources/img/step2.png" />"
             alt="" title="#caption_2" />
        <img src="<c:url value="/resources/img/step3.png" />" data-thumb="<c:url value="/resources/img/step3.png" />"
             alt="" title="#caption_3" />
        <img src="<c:url value="/resources/img/step4.png" />" data-thumb="<c:url value="/resources/img/step4.png" />"
             alt="" title="#caption_4" />
      </div>
      <div id="caption_1" class="nivo-html-caption">
        1. <spring:message code="login.caption_1" htmlEscape="false" /> ...
      </div>
      <div id="caption_2" class="nivo-html-caption">
        2. <spring:message code="login.caption_2" htmlEscape="false" /> ...
      </div>
      <div id="caption_3" class="nivo-html-caption">
        3. <spring:message code="login.caption_3" htmlEscape="false" /> ...
      </div>
      <div id="caption_4" class="nivo-html-caption">
        4. <spring:message code="login.caption_4" htmlEscape="false" />.
      </div>
    </div>
  </div>

  <script type="text/javascript">
    // <!--
    jQuery(document).ready(
      function() {
        jQuery('#slider').nivoSlider({
          effect: 'fade',
          pauseTime: 3500
        });
        jQuery('#login').click(function(event) {
          if (jQuery('#login_tab').is(':hidden')) {
            jQuery('#login_tab').show();
            jQuery('#login').hide();
            jQuery('#register_tab').hide();
            jQuery('#register').show();
          }
          event.preventDefault();
        });
        jQuery('#register').click(function(event) {
          if (jQuery('#register_tab').is(':hidden')) {
            jQuery('#register_tab').show();
            jQuery('#register').hide();
            jQuery('#login_tab').hide();
            jQuery('#login').show();
          }
          event.preventDefault();
        });
        if (jQuery('#login_tab').is(':visible')) {
          document.forms['login'].elements['username'].focus();
        }
      }
    );
    //-->
  </script>
