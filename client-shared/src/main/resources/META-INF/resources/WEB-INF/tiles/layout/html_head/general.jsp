<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <!-- In case intranet sites have Compatibility View defaulted -->
    <meta content="IE=edge" 
          http-equiv="X-UA-Compatible" />
    <%-- Content-Type not strictly necessary as (request|response).setCharacterEncoding(UTF-8) used --%>
    <meta content="text/html; charset=UTF-8"
          http-equiv="Content-Type"  />
    <meta content="Action Potential prediction software"
          name="description" />
    <meta content="ApPredict, AP-Portal, Action Potential prediction, Cardiac Toxicity, Cardiac Safety"
          name="keywords"  />
    <link href="<c:url value="/resources/img/favicon.ico" />"
          rel="icon"
          type="image/x-icon" />
