<%@ taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<p style="text-align: center;">
  <spring:message code="cs_error.message" /> : <c:out value="${exception.message}" />

  <br />
  <br />

  <a href="${pageContext.request.contextPath}"
     title="<spring:message code="cs_general.go_to" />&nbsp;<spring:message code="cs_general.home" />">
     - <spring:message code="cs_general.home" /> -
  </a>
</p>
