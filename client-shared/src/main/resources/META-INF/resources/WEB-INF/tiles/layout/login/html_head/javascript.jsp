<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers" %>
<%-- name_? vars are not escaped because the values are arguments to a subsequent spring:message --%>
<c:set var="name_email_address"><spring:message code="cs_login.email_address" htmlEscape="false" /></c:set>
<c:set var="locale" value="${fn:toLowerCase(pageContext.response.locale)}" />
<c:set var="available_langs"><%=ClientSharedIdentifiers.I18N_LANGS%></c:set>

    <script type="text/javascript" src="<c:url value="/resources/js/nivo-slider/jquery.nivo.slider.pack.js" />"></script>

    <script type="text/javascript" src="<c:url value="/resources/js/jquery/validate/jquery.validate-1.14.0.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/validate/additional-methods.1.14.0.min.js" />"></script>
<c:if test="${not empty ma_public_recaptcha_key}">
    <script type="text/javascript" src='https://www.google.com/recaptcha/api.js'></script>
</c:if>
<c:if test="${fn:contains(available_langs,locale)}">
    <!-- https://github.com/jzaefferer/jquery-validation/tree/master/src/localization -->
    <script type="text/javascript" src="<c:url value="/resources/js/jquery/validate/messages_${locale}.js" />"></script>
</c:if>
    <script type="text/javascript">
      /* <![CDATA[ */
      jQuery(document).ready(
        function() {
          var registration_form = jQuery('#registration_form');
          registration_form.validate({
            rules: {
              '<%=ClientSharedIdentifiers.PARAM_NAME_EMAIL_ADDRESS%>' : { required: true }
            },
            messages: {
              '<%=ClientSharedIdentifiers.PARAM_NAME_EMAIL_ADDRESS%>' :
                { required: '<spring:message code="cs_input.form_value_required" arguments="${name_email_address}" />' }
            }
          })
        }
      );

      var RecaptchaOptions = {
        theme: 'white'
      };
      /* ]]> */
    </script>
