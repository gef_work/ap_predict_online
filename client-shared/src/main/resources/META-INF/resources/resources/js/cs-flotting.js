var PLOT_AP = 'ap';
var PLOT_PCT_CHANGE = 'pct_change';
var PLOT_QNET = 'qnet';

var cs_default_log_conc_ticks = [ [-2.0, '0.01'],
                                  [-1.523, '0.03'],
                                  [-1.0, '0.1'],
                                  [-0.523, '0.3'],
                                  [0.0 , '1.0'],
                                  [0.477 , '3.0'],
                                  [1.0 , '10.0'],
                                  [1.477 , '30.0'],
                                  [2.0 , '100.0'],
                                  [2.477 , '300.0'],
                                  [3.0 , '1000.0'] ];

/* https://stackoverflow.com/questions/5427725/flot-graph-does-not-render-when-parent-container-is-hidden */
var cs_fplot = function(e, data, options) {
  var jqParent, jqHidden;
  var e = e[0];
  if (e.offsetWidth <=0 || e.offsetHeight <=0) {
    // lets attempt to compensate for an ancestor with display:none
    jqParent = $(e).parent();
    jqHidden = $("<div style='visibility:hidden'></div>");
    $('body').append(jqHidden);
    jqHidden.append(e);
  }

  var plot=$.plot(e, data, options);

  // if we moved it above, lets put it back
  if (jqParent) {
    jqParent.append(e);
    jqHidden.remove();
  }

  return plot;
};

var cs_universal_plot_options = {
  legend: { show: false },
  yaxis: { axisLabelUseCanvas: true },
  xaxis: { axisLabelPadding: 10,
           axisLabelUseCanvas: true },
  selection: { mode: 'xy' },
  grid: { hoverable: true }
};

function cs_plot_options(plot_type) {
  var additional_options = {};
  switch (plot_type) {
    case 'ap':
      additional_options = {
        yaxis: { axisLabel: GRAPH_LABEL_MEMBRANE_VOLTAGE_MV },
        xaxis: { axisLabel: GRAPH_LABEL_TIME_MS },
        series: { lines: { show: true },
                  points: { show: false } }
      };
      break;
    case 'pct_change':
      additional_options = {
        yaxis: { axisLabel: GRAPH_LABEL_PCT_CHANGE },
        xaxis: {
          autoscaleMargin: 0.05,
          axisLabel: GRAPH_LABEL_CONC_UM
        }
      };
      break;
    case 'qnet':
      additional_options = {
        yaxis: { axisLabel: GRAPH_LABEL_QNET },
        xaxis: {
          autoscaleMargin: 0.05,
          axisLabel: GRAPH_LABEL_CONC_UM
        }
      };
      break;
    default:
      alert('Unrecognised plot type of [' + plot_type + '] encountered!')
      break;
  }
  return jQuery.extend(true, additional_options, cs_universal_plot_options);
}

function cs_process_plothover(inputs) {
  var pos = inputs.pos;

  if (typeof pos.y != 'undefined') {
    var previous_point = inputs.previous_point;
    var item = inputs.item;
    var fixed_decimal_place = inputs.fixed_decimal_place;
    var inversing = inputs.inversing;
    var x_input = inputs.x;
    var y_input = inputs.y;
    var axes = inputs.plot.getAxes();

    var x_axis = axes.xaxis;
    var y_axis = axes.yaxis;

    var text_x = text_y = '';
    if (pos.x >= x_axis.min && pos.x <= x_axis.max && pos.y >= y_axis.min && pos.y <= y_axis.max) {
      if (inversing) {
        /* Used if pos.x is the non-log value of the cursor x pos */
        text_x = cs_inverse(pos.x).toFixed(fixed_decimal_place);
      } else {
        text_x = pos.x.toFixed(fixed_decimal_place);
      }
      text_y = pos.y.toFixed(fixed_decimal_place); 
    }
    x_input.element.text(text_x);
    y_input.element.prev().text(y_input.label);
    y_input.element.text(text_y);
    y_input.element.next().text(y_input.units);

    if (item) {
      if (previous_point != item.datapoint) {
        previous_point = item.datapoint;

        $('#tooltip').remove();
        var x = item.datapoint[0],
            y = item.datapoint[1].toFixed(fixed_decimal_place);
        var tooltip_label = '';
        if (item.series.label === undefined) {
          /* Assumption that it's spread data if no series label! */
          tooltip_label = item.series.id;
        } else {
          tooltip_label = item.series.label;
        }
        var use_x = inversing ? cs_inverse(x) : x;
        var contents = '[' + tooltip_label + '] : ' +
                       x_input.label + ' ' + use_x.toFixed(fixed_decimal_place) + ' ' + x_input.units +
                       ' - ' +
                       y_input.label + ' ' + y + ' ' + y_input.units;
        show_tooltip(item.pageX, item.pageY, contents);
      }
    } else {
      $('#tooltip').remove();
      previous_point = null;
    }
  }

  return previous_point;
}