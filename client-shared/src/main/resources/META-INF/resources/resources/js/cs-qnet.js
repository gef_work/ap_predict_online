const APPREDICT_NEGATIVE_DOUBLE_MAX = "-1.79769e+308";

var qnet_display = {
  qnet_placeholder_trace_id : 'qnet_placeholder_trace',
  default_qnet_ticks : [ [ 0, '0'],
                         [ 0.05, '0.05' ],
                         [ 0.1, '0.1'],
                         [ 0.15, '0.15'] ],
  qnet_data : null,
  qnet_main_options : null,
  qnet_plot : null,
  qnet_specific_qnet_ticks: null,
  qnet_x_ticks: null,
  /* y-axis cutoff when ApPredict qNet spread has -ve DOUBLE_MAX */
  qnet_y_axis_cutoff: undefined,

  legacy_pctile_lower : 'delta_APD90_lower',
  legacy_median : 'delta_APD90',
  legacy_pctile_upper : 'delta_APD90_upper',
  legacy_pctile_value : '95',

  qnet_get_default_main_panel_options :
    function() {
      return jQuery.extend(true,
                           cs_plot_options(PLOT_QNET),
                           { yaxis : { ticks: this.qnet_specific_qnet_ticks,
                                       min: this.qnet_y_axis_cutoff },
                             xaxis : { ticks: this.qnet_x_ticks } } );
    },
  qnet_plot_according_to_choices_and_zooming :
    function() {
      console.log('qnet_plot_according_to_choices_and_zooming in base')
    },
  qnet_reset_everything :
    function() {
      this.qnet_main_options = this.qnet_get_default_main_panel_options();
      this.qnet_plot_according_to_choices_and_zooming();
    },
  qnet_reset_display :
    function() {
      var default_text = jQuery('#' + pct_change_default_text_id).clone();
      default_text.removeAttr('id');
      default_text.show();

      jQuery('#' + this.qnet_placeholder_trace_id).html(default_text);

      /* Reset the events */
      jQuery('#' + this.qnet_placeholder_trace_id).off();
    },
  show_qnet_graph :
    function(component) {

      var self = this;

      if (component == 'client-direct') {
        if (Object.size(QNET_DATASETS) == 0) {
          jQuery('#tab1').html(jQuery('<span />').text(TEXT_QNET_AVAILABILITY)
                                                 .css({ 'border': 'solid black 1px', 'padding': '5px' })
                                                 .addClass('cs_bold cs_rounded_3'));

          return;
        }
      } else {
        if (Object.size(QNET_DATASETS) == 0) {
          return;
        }
      }

      self.qnet_reset_display();

      var qnet_data_store = [];
      jQuery.each(QNET_DATASETS, function(key, val) {
        qnet_data_store.push(jQuery.extend(true, {}, val));
      });
      self.qnet_data = cs_sort_array_on_group_level_and_pacing_frequency(qnet_data_store);

      /* Set y-axis max to max default qnet assigned, x-axis to min to min default qnet assigned. */
      var qnet_max = self.default_qnet_ticks[self.default_qnet_ticks.length - 1][0];
      var qnet_min = self.default_qnet_ticks[0][0];

      /* Keep a record of highest and lowest numeric (but not -ve DOUBLE_MAX) values. */
      var measured_qnet_max = Number.MIN_VALUE;
      var measured_qnet_min = Number.MAX_VALUE;

      /* Set x-axis max to lowest default val, and x-axis min to highest default val! */
      var log_conc_max = cs_round_3dp(cs_default_log_conc_ticks[0][0]);
      var log_conc_min = cs_round_3dp(cs_default_log_conc_ticks[cs_default_log_conc_ticks.length - 1][0]);

      var set_default_qnet_axis = true;
      var has_appredict_double_max = false;

      jQuery.each(self.qnet_data, function(key, val) {
        var existing_data = val.data;
        var confidence_data = val.confidenceData;
        jQuery.each(existing_data, function(key, val) {
          var log_conc = val[0];
          var qnet = val[1];

          log_conc = cs_round_3dp(log_conc * 1.0);
          if (log_conc > log_conc_max) log_conc_max = log_conc;
          if (log_conc < log_conc_min) log_conc_min = log_conc;

          /* If there's confidence data then take that into consideration */
          if (confidence_data) {
            /* Treat as a string to enact a split */
            var spreads = qnet.toString().split(',');
            jQuery.each(spreads, function() {
              var each_qnet_spread_value = this;

              var is_appredict_double_max = (each_qnet_spread_value == APPREDICT_NEGATIVE_DOUBLE_MAX);
              if (is_appredict_double_max) {
                /* If ApPredict DOUBLE_MAX value, make a note, but ignore */
                has_appredict_double_max = true;
                return;
              }

              if (jQuery.isNumeric(each_qnet_spread_value)) {
                each_qnet_spread_value *= 1;

                /* It's a number! */
                if (each_qnet_spread_value > measured_qnet_max) {
                  measured_qnet_max = each_qnet_spread_value;
                } else if (each_qnet_spread_value < measured_qnet_min) {
                  measured_qnet_min = each_qnet_spread_value;
                }

                if (set_default_qnet_axis &&
                    !(each_qnet_spread_value >= qnet_min && 
                      each_qnet_spread_value <= qnet_max)) {
                  /* It's the first time we've encountered a point outside
                   * the default plot min/max y-axis value */
                  set_default_qnet_axis = false;
                }
              }
            });
          } else {
            if (jQuery.isNumeric(qnet)) {
              var is_appredict_double_max = (qnet == APPREDICT_NEGATIVE_DOUBLE_MAX);
              if (is_appredict_double_max) {
                has_appredict_double_max = true;
                return;
              } else {
                qnet *= 1;
                if (qnet > measured_qnet_max) {
                  measured_qnet_max = qnet;
                } else if (qnet < measured_qnet_min) {
                  measured_qnet_min = qnet;
                }

                if (set_default_qnet_axis) {
                  set_default_qnet_axis = (qnet >= qnet_min && qnet <= qnet_max);
                }
              }
            }
          }
        });
      });

      self.qnet_specific_qnet_ticks = set_default_qnet_axis ? self.default_qnet_ticks : undefined;

      /* If there's an ApPredict -ve double max value found then
         determine cutoff point on qNet y-axis */
      if (has_appredict_double_max) {
        if (measured_qnet_min != Number.MAX_VALUE &&
            measured_qnet_max != Number.MIN_VALUE) {
          var y_axis_diff = measured_qnet_max - measured_qnet_min;
          var y_axis_cutoff = measured_qnet_min - (y_axis_diff * 0.25);

          /* Keep a record for if "Reset Graph" pressed */
          self.qnet_y_axis_cutoff = y_axis_cutoff;
        }
      }

      if (component == 'client') {
        var qnet_specific_conc_ticks = [];
        jQuery.each(cs_default_log_conc_ticks, function() {
          var this_tick_log = cs_round_3dp(this[0] * 1.0);
          var this_tick_conc = this[1] * 1.0;
          if (this_tick_log >= log_conc_min && this_tick_log <= log_conc_max) {
            qnet_specific_conc_ticks.push([this_tick_log, this_tick_conc]);
          }
        });

        self.qnet_x_ticks = qnet_specific_conc_ticks;
      }

      self.qnet_main_options = self.qnet_get_default_main_panel_options();

      var qnet_colour_idx;
      if (component == 'client-direct') {
        qnet_colour_idx = 0;
      } else {
        qnet_colour_idx = Object.size(EXPERIMENTAL_DATASETS);
      }

      jQuery.each(self.qnet_data, function(key, val) {
        val.color = qnet_colour_idx;
        ++qnet_colour_idx;

        if (val.confidenceData) {
          cs_graph_percentiles.pre_process_pctiles(val, self.qnet_data);
        }
      });

      var data = [];

      if (component == 'client') {
        /* Default for client is to only show median */
        $.each(self.qnet_data, function(key, val) {
          var data_label = val.label;
          if (typeof data_label !== 'undefined') {
            /* Regular assay/freq qNet data */
            data.push(val);
          }
        });
      } else {
        /* Default for client-direct is to show median and %iles */
        data = self.qnet_data;
      }

      self.qnet_plot = cs_fplot($('#' + self.qnet_placeholder_trace_id),
                                data, self.qnet_main_options);

      $('#' + self.qnet_placeholder_trace_id).bind('plotselected',
                                                   function(event, ranges) {
        /* clamp the zooming to prevent eternal zoom */
        if (ranges.xaxis.to - ranges.xaxis.from < 0.00001)
          ranges.xaxis.to = ranges.xaxis.from + 0.00001;
        if (ranges.yaxis.to - ranges.yaxis.from < 0.00001)
          ranges.yaxis.to = ranges.yaxis.from + 0.00001;
        self.qnet_main_options = $.extend(true,
                                          {},
                                          self.qnet_main_options,
                                          { xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to },
                                            yaxis: { min: ranges.yaxis.from, max: ranges.yaxis.to } });
        self.qnet_plot_according_to_choices_and_zooming();
      });

      /* Reset button actions defined in graph_pct_change.js */

      var previous_point = null;
      var fixed_decimal_place = 3;
      $('#' + self.qnet_placeholder_trace_id).bind('plothover',
                                                   function(event, pos, item) {
        previous_point = cs_process_plothover({ plot: self.qnet_plot,
                                                pos: pos,
                                                item: item,
                                                previous_point: previous_point,
                                                fixed_decimal_place: fixed_decimal_place,
                                                inversing: true,
                                                x: { element: $('#point_val_x'),
                                                     label: TEXT_CONC,
                                                     units: '&micro;M' },
                                                y: { element: $('#point_val_y'),
                                                     label: TEXT_QNET,
                                                     units: 'C/F' } });
      });

    }
}
