var cs_graph_pctiles_to_show = [];
var cs_graph_pctile_checkbox_class = 'pctile';

var cs_graph_pct_change = {
  legacy_pctile_lower : 'delta_APD90_lower',
  legacy_median : 'delta_APD90',
  legacy_pctile_upper : 'delta_APD90_upper',
  legacy_pctile_value : '95',

  append_pctile_checkboxes :
    /**
     * Append %ile checkboxes to the choice container.
     * 
     * @param new_elements Elements containing created objects.
     */
    function(new_elements) {
      var confidence_checkbox = jQuery('<input />').attr( { id: 'pct_change_confidence_checkbox',
                                                            type: 'checkbox' } );
      /* Embed the %ile checkboxes in a 'span' element. */
      var pctile_span = jQuery('<span />');

      /* For each available %ile to show, create a checkbox. */
      jQuery.each(cs_graph_pctiles_to_show, function() {
        var pctile_amount = this;
        var pctile_id = 'pctile_' + pctile_amount;
        var pctile_checkbox = jQuery('<input />').attr( { type: 'checkbox',
                                                          id: pctile_id } )
                                                 .addClass(cs_graph_pctile_checkbox_class)
                                                 .prop('checked', true)
                                                 .val(pctile_amount);
        var pctile_label = jQuery('<label />').attr( { 'for': pctile_id } )
                                              .text(pctile_amount);
        pctile_span.append('&nbsp;|&nbsp;');
        pctile_span.append(pctile_label);
        pctile_span.append(pctile_checkbox);
      });

      new_elements.confidence_checkbox = confidence_checkbox;
      new_elements.pctile_span = pctile_span;
    }
}

var cs_graph_percentiles = {
  legacy_pctile_lower : 'delta_APD90_lower',
  legacy_median : 'delta_APD90',
  legacy_pctile_upper : 'delta_APD90_upper',
  legacy_pctile_value : '95',

  retrieve_legacy_pctiles_concatenated :
    /**
     * Retrieve a concatenation of the legacy DeltaAP90 percentile labels.
     * @returns Concatenation of the legacy DeltaAP90 percentile labels.
     */
    function() {
      return this.legacy_pctile_lower + ',' + this.legacy_median + ',' +
             this.legacy_pctile_upper;
    },

  pre_process_pctiles :
    /**
     * Undertake some %ile pre-processing, populating data stores.
     * Note that both qNet and Delta APD90 data objects are processed, but the
     * %ile name is derived from the Delta APD90 names.
     * 
     * @param val Data object containing simulation results.
     * @param data_store Data store to populate with lower and upper %ile data. 
     */
    function(val, data_store) {
      var store_data_in_array = (Array.isArray(data_store)); 

      var delta_apd90_pctile_names;
      if (val.deltaAPD90PctileNames === undefined || val.deltaAPD90PctileNames === null) {
        /* If value is undefined assume simulation results persisted in days when percentile
           names weren't persisted. */
        delta_apd90_pctile_names = this.retrieve_legacy_pctiles_concatenated();
      } else {
        /* Percentile names persisted. */
        delta_apd90_pctile_names = val.deltaAPD90PctileNames;
      }
      /* Replace any occurrence of the original percentile names with their numerical values. */
      delta_apd90_pctile_names = delta_apd90_pctile_names.replace(this.legacy_pctile_lower,
                                                                  this.legacy_pctile_value);
      delta_apd90_pctile_names = delta_apd90_pctile_names.replace(this.legacy_pctile_upper,
                                                                  this.legacy_pctile_value);
      delta_apd90_pctile_names = delta_apd90_pctile_names.replace(/[a-zA-Z_\-]+/g, '');

      var data = val.data;
      var label = val.label;

      var lower_coords = {};
      var mid_coords = [];
      var upper_coords = {};

      /* Initialise the lower- and upper- delta APD90 %ile data objects coords arrays. */
      var names = delta_apd90_pctile_names.split(',');
      while (names.length > 1) {
        var lower_pctile_name = names.shift();
        var upper_pctile_name = names.pop();

        /* Load unique percentile amounts into an array. */
        if (jQuery.inArray(lower_pctile_name, cs_graph_pctiles_to_show) == -1) {
          cs_graph_pctiles_to_show.push(lower_pctile_name);
        }

        lower_coords[lower_pctile_name] = [];
        upper_coords[upper_pctile_name] = [];
      }

      /* Traverse spread-containing (CSV format) coord data and split into all available lower
         and upper percentile spreads (and the median) to load into coords arrays. */
      jQuery.each(data, function(key, val) {
        var conc = val[0];
        var spreads = val[1].toString().split(',');
        names = delta_apd90_pctile_names.toString().split(',');

        /* Gobble up the lower- and upper- percentiles data, leaving the median. */
        while (spreads.length > 1) {
          var lower_spread = spreads.shift();
          var upper_spread = spreads.pop();

          var lower_pctile_name = names.shift();
          var upper_pctile_name = names.pop();

          lower_coords[lower_pctile_name].push( [ conc, lower_spread ] );
          upper_coords[upper_pctile_name].push( [ conc, upper_spread ] );
        }

        /* Whatever remains should be the median. */
        var median = spreads.pop();
        mid_coords.push( [ conc, median ] );
      });

      /* Replace the spread point data with the mid-point plot data. */
      val.data = mid_coords;

      /* Now go through the names again, create some plot labels, and transfer the data loaded in
         the previous step into data objects. */
      names = delta_apd90_pctile_names.split(',');
      var fill_increment = 0.025;
      var fill_value = 0.1;
      while (names.length > 1) {
        var lower_pctile_name = names.shift();
        var upper_pctile_name = names.pop();

        var lower_id = label + ' ' + lower_pctile_name + ' lower';
        var upper_id = label + ' ' + upper_pctile_name + ' upper';

        /* Note: No 'label' property used. */
        var lower_data = {
          'id' : lower_id,
          'data' : lower_coords[lower_pctile_name],
          'lines' : { show: true, lineWidth: 0, fill: false },
          'color': val.color,
          'pctile': lower_pctile_name
        }

        var upper_data = {
          'id': upper_id,
          'data' : upper_coords[upper_pctile_name],
          'lines': { show: true, lineWidth: 0, fill: fill_value },
          'color': val.color,
          'pctile': upper_pctile_name,
          'fillBetween': lower_id
        };

        if (store_data_in_array) {
          data_store.push(lower_data);
          data_store.push(upper_data);
        } else {
          data_store[lower_id] = lower_data;
          data_store[upper_id] = upper_data;
        }

        fill_value += fill_increment;
      }
    }
}
