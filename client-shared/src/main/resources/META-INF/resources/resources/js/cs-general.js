/**
 * Generic AJAX query service.
 * 
 * @param method HTTP method : {@code 'GET'} or {@code 'POST'}.
 * @param async Asynchronous? {@code true} or {@code false}.
 * @param url URL.
 * @param data JSON structured data, e.g. {}.
 * @param on_success On success callback.
 * @param on_error On error callback.
 */
function cs_ajax_json_query(method, async, url, data, timeout, on_success, on_error) {
  jQuery.ajax({
    url: url,
    async: async,
    type: method,
    data: data,
    dataType: 'json',
    timeout: timeout,
    contentType: 'application/json; charset=utf-8',
    success: on_success,
    error: on_error
  });
}

/**
 * Populate the job details variable.
 * 
 * @param job_id Job identifier.
 * @param progress_job Data structure holding job information, e.g. job group name, job group level.
 */
function cs_assign_job_details(job_id, progress_job) {
  var key = cs_id_as_key(job_id);
  JOB_DETAILS[key] = { 'groupName' : progress_job[KEY_SHARED_GROUP_NAME],
                       'groupLevel' : progress_job[KEY_SHARED_GROUP_LEVEL],
                       'pacingFrequency' : +progress_job[KEY_SHARED_PACING_FREQUENCY] };
}

/**
 * Retrieve an identifier as an object key value.
 * 
 * @param id
 * @returns {String}
 */
function cs_id_as_key(id) {
  return id + '';
}

/**
 * Retrieve the inverse common log (base 10) value.
 * 
 * @param value Value to invert.
 * @return Inverted value.
 */
function cs_inverse(value) {
  return Math.pow(10, value);
}

/**
 * Retrieve the log 10 value.
 * 
 * @param value Value to log.
 * @return Logged value.
 */
function cs_log_10(value) {
  return Math.log(value) / Math.log(10);
}

/**
 * Retrieve group level for job.
 * 
 * @param job_id Job identifier.
 * @return Job's group level.
 */
function cs_retrieve_group_level(job_id) {
  var key = cs_id_as_key(job_id);
  var job_detail = JOB_DETAILS[key];
  if (job_detail != undefined) {
    return job_detail[KEY_SHARED_GROUP_LEVEL];
  } else {
    /* debug('No job_detail for job_id ' + job_id + ' when retrieving job group level.'); */
  }
}

/**
 * Retrieve JSON property from the AJAX response (if it's available).
 * <p>
 * If there's no JSON property in the model attribute, but instead an exception property (indicating
 * that the application's figured that something's wrong and has caught it), then display that
 * error message.
 *
 * @param response AJAX response.
 * @param model_attr The model attribute being sought.
 * @param descriptive Textual representation of the AJAX request which was made.
 * @return JSON object, or nothing if no JSON property found).
 */
function cs_retrieve_json(response, model_attr, descriptive, error_element) {
  var response_obj = response[model_attr];
  if (response_obj != undefined) {
    var response_exception = response_obj[CS_KEY_EXCEPTION];
    if (response_exception != undefined) {
      cs_show_js_error(response_exception, error_element);
    } else {
      var json_object = response_obj[CS_KEY_JSON];
      if (json_object != undefined) {
        return json_object;
      } else {
        cs_show_not_found(CS_KEY_JSON, descriptive, error_element);
      }
    }
  } else {
    cs_show_not_found(model_attr, descriptive, error_element);
  }
}


/**
 * Retrieve a pacing frequency for the job.
 * 
 * @param job_id Job identifier.
 * @return Job's pacing frequency.
 */
function cs_retrieve_pacing_frequency(job_id) {
  var key = cs_id_as_key(job_id);
  var job_detail = JOB_DETAILS[key];
  if (job_detail != undefined) {
    return job_detail[KEY_SHARED_PACING_FREQUENCY];
  } else {
    /* debug('No job_detail for job_id ' + job_id + ' when retrieving job pacing frequency.'); */
  }
}

/**
 * Round a value to 3 decimal places.
 * 
 * @param number Value to round to three decimal places.
 * @return Rounded value.
 */
function cs_round_3dp(number) {
  return cs_round_number(number, 3);
}


/**
 * Round number to specified number of decimal places.
 * 
 * @param number Value to round.
 * @param decimal_places Number of decimal places to round to.
 * @return Value rounded to specified decimal places.
 */
function cs_round_number(number, decimal_places) {
  return Math.round(number * Math.pow(10, decimal_places)) / Math.pow(10, decimal_places);
}

/**
 * Sort array on group level and pacing frequency.<br />
 * Assumes that array objects have "groupLevel" and "pacingFrequency" keys.
 * 
 * @param Array to sort.
 * @return Sorted array.
 */
function cs_sort_array_on_group_level_and_pacing_frequency(array) {
  array.sort(function(a, b) {
    var agl = +a[KEY_SHARED_GROUP_LEVEL];
    var bgl = +b[KEY_SHARED_GROUP_LEVEL];
    var apf = +a[KEY_SHARED_PACING_FREQUENCY];
    var bpf = +b[KEY_SHARED_PACING_FREQUENCY];
    if (agl < bgl) return -1;
    if (agl > bgl) return 1;
    if (apf < bpf) return -1;
    if (apf > bpf) return 1;
    return 0;
  });
  return array;
}