/**
 * Handle a situation when a AJAX call has returned an unhandled error.
 *
 * @param error AJAX-generated error (or empty string!).
 * @param descriptive Textual representation of the AJAX request which was made.
 * @param element Error element to display.
 */
function cs_handle_error(error, descriptive, element) {
  if (error != undefined) {
    if (error == '') {
      /* May imply client has been switched off! */
      cs_show_js_error('Application problem! An unspecified error was returned when querying for \'' + descriptive +
                       '\' which is indicative of the application having been turned off!. Please reload/refresh the page!', element);
    } else if (error == 'SyntaxError: JSON.parse: unexpected character') {
      /* Could be app manager failing but NOT generating a SOAP fault, e.g. a db connectivity problem */
      cs_show_js_error('Application problem! An error has occured when querying for \'' + descriptive + '\'.' +
                       ' A page reload/refresh may resolve the problem!', element);
    } else if (error == 'SyntaxError: JSON.parse: unexpected character at line 1 column 1 of the JSON data') {
      // If the AJAX call responds with a redirection to the login page.
      cs_show_js_error('Application problem! The response to a \'' + descriptive +
                       '\' query indicates that you may need to try logging in again.', element);
    } else {
      cs_show_js_error('Application problem! The following error text has been returned when querying for \'' + descriptive +
                       '\' data: ' + error, element);
    }
  }
}

/**
 * Hide the javascript error element.
 *
 * @param element Element to hide.
 */
function cs_hide_js_error(element) {
  element.hide();
}

/**
 * Show a javascript error message.
 *
 * @param error_text to show in error message.
 * @param element Element to display message in.
 */
function cs_show_js_error(error_text, element) {
  element.text(error_text).show();
}

/**
 * Show the "object not found" text.
 *
 * @param not_found Object which wasn't found (e.g. perhaps a model attribute or object property).
 * @param descriptive Textual description of what was being sought (e.g. workload data).
 * @param error_element Error element to display message in.
 */
function cs_show_not_found(not_found, descriptive, error_element) {
  cs_show_js_error('Application problem! Could not find object ' + not_found + 
                   ' when querying for ' + descriptive + '.', error_element);
}

