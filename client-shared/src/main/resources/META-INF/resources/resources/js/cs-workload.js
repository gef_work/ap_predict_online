var js_error_workload_el;
var workload_call_failure_count = 0;
var workload_el;

/**
 * AJAX query to retrieve app manager workload.
 */
function ajax_for_workload() {
  cs_ajax_json_query('GET', true, URL_WORKLOAD, {}, 6000, workload_on_success, workload_on_fail);
}

/**
 * Callback on successful AJAX workload query.
 * 
 * @param response Response to AJAX query.
 */
function workload_on_success(response) {
  var json = cs_retrieve_json(response, MODEL_ATTRIBUTE_WORKLOAD,
                              'workload', js_error_workload_el);
  if (json != undefined) {
    /* No JSON property returned from controller -- which probably means CS_KEY_EXCEPTION property
         was found instead, indicating system caught an exception to be displayed.
       Reset the js error element and count (if it's visible, i.e. there was previously an error) */
    if (js_error_workload_el.is(':visible')) {
      cs_hide_js_error(js_error_workload_el);
      workload_call_failure_count = 0;
    }
    workload_el.html(json);
    setTimeout(function() { ajax_for_workload(); }, 10000);
  } else {
    workload_el.html('?');
  }
}

/**
 * Callback on unsuccessful AJAX workload query.
 * 
 * @param xhr
 * @param status
 * @param error
 */
function workload_on_fail(xhr, status, error) {
  workload_el.html('?');
  cs_handle_error(error, 'workload', js_error_workload_el);
  /* Try a few times to reconnect but give up eventually! */
  if (workload_call_failure_count++ < 5) {
    setTimeout(function() { ajax_for_workload(); }, 10000);
  }
}