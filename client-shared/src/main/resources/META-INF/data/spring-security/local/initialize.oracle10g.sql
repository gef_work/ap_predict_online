drop table users cascade constraints;
drop table authorities cascade constraints;

create table users(
  username nvarchar2(20) not null primary key,
  password nvarchar2(60) not null,
  enabled number(1) not null check (enabled in (0, 1))
);

create table authorities (
  username nvarchar2(20) not null,
  authority nvarchar2(20) not null,
  constraint fk_authorities_users foreign key(username) references users(username),
  constraint ix_auth_username unique (username, authority)
);
