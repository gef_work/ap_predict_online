/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.value;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import uk.ac.ox.cs.compbio.client_shared.value.security.Role;

/**
 * Value object for handling portal feature data.
 *
 * @author geoff
 */
public class PortalFeatureVO {

  private final PortalFeature portalFeature;
  private final Set<Role> restrictedRoles = new HashSet<Role>();

  /**
   * Initialising constructor.
   * 
   * @param portalFeature Portal feature.
   * @param restrictedRoles Roles which the user must have in order to access
   *                        the feature, or {@code null} or empty collection
   *                        for unrestricted access.
   * @throws IllegalArgumentException
   */
  public PortalFeatureVO(final PortalFeature portalFeature,
                         final Set<Role> restrictedRoles)
                         throws IllegalArgumentException {
    if (portalFeature == null) {
      throw new IllegalArgumentException("A portal feature must be specified!");
    }
    this.portalFeature = portalFeature;

    if (restrictedRoles != null) {
      this.restrictedRoles.addAll(restrictedRoles);
    }
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "PortalFeatureVO [portalFeature=" + portalFeature
        + ", restrictedRoles=" + restrictedRoles + "]";
  }

  /**
   * Retrieve the portal feature.
   * 
   * @return The portal feature.
   */
  public PortalFeature getPortalFeature() {
    return portalFeature;
  }

  /**
   * Retrieve an unmodifiable collection of the roles required to access the
   * feature, an empty collection if feature is available to anybody.
   * 
   * @return Roles required to access the feature, or empty collection if 
   *         feature is available to all. 
   */
  public Set<Role> getRestrictedRoles() {
    return Collections.unmodifiableSet(restrictedRoles);
  }
}