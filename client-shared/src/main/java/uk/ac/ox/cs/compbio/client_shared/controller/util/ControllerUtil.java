/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.controller.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;

/**
 * Utility class for controllers.
 * 
 * @author Geoff Williams
 */
public final class ControllerUtil {

  private static final int httpOK = 200;

  // https://developers.google.com/recaptcha/docs/verify
  private static final String recaptchaParamResponse = "response";
  private static final String recaptchaParamSecret = "secret";
  private static final String recaptchaParamTrue = "true";
  private static final String recaptchaURL = "https://www.google.com/recaptcha/api/siteverify";

  private static final Map<String, String> recaptchaMessages = new HashMap<String, String>();

  private static final Log log = LogFactory.getLog(ControllerUtil.class);

  static {
    // https://developers.google.com/recaptcha/docs/verify
    recaptchaMessages.put("missing-input-secret", ClientSharedIdentifiers.BUNDLE_ID_RECAPTCHA_ERR_MISSING_INPUT_SECRET);
    recaptchaMessages.put("invalid-input-secret", ClientSharedIdentifiers.BUNDLE_ID_RECAPTCHA_ERR_INVALID_INPUT_SECRET);
    recaptchaMessages.put("missing-input-response", ClientSharedIdentifiers.BUNDLE_ID_RECAPTCHA_ERR_MISSING_INPUT_RESPONSE);
    recaptchaMessages.put("invalid-input-response", ClientSharedIdentifiers.BUNDLE_ID_RECAPTCHA_ERR_INVALID_INPUT_RESPONSE);
  }

  // Hidden constructor
  private ControllerUtil() {}

  /**
   * Call out to reCaptcha.
   * 
   * @param recaptchaPrivateKey Private key assigned by google.
   * @param recaptchaResponseField User's response.
   * @return {@code null} if successful, otherwise error message bundle id. 
   */
  public static String callRecaptcha(final String recaptchaPrivateKey,
                                     final String recaptchaResponseField) {
    log.debug("~callRecaptcha() : Invoked.");

    // Don't think that remote IP is essential!
    if (recaptchaPrivateKey == null) {
      log.error("~callRecaptcha() : Key '" + recaptchaPrivateKey + " was null!"); 
      return ClientSharedIdentifiers.BUNDLE_ID_REGN_FAIL;
    }

    String outcome = ClientSharedIdentifiers.BUNDLE_ID_RECAPTCHA_ERR_UNKNOWN;
    final CloseableHttpClient httpClient = HttpClients.createDefault();
    try {
      try {
        final HttpPost postRequest = new HttpPost(recaptchaURL);
        final List<NameValuePair> params = new ArrayList<NameValuePair>(3);
        params.add(new BasicNameValuePair(recaptchaParamSecret, recaptchaPrivateKey));
        params.add(new BasicNameValuePair(recaptchaParamResponse, recaptchaResponseField));
        postRequest.setEntity(new UrlEncodedFormEntity(params));

        final RequestConfig requestConfig = RequestConfig.custom().
          setSocketTimeout(5000).
          setConnectTimeout(5000).
          setConnectionRequestTimeout(5000).
          build();
        postRequest.setConfig(requestConfig);
        log.debug("~callRecaptcha() : About to execute POST.");
        final CloseableHttpResponse postResponse = httpClient.execute(postRequest);
        log.debug("~callRecaptcha() : Response received.");

        final int statusCode = postResponse.getStatusLine().getStatusCode();
        if (statusCode == httpOK) {
          log.debug("~callRecaptcha() : HTTP OK.");
          try {
            final HttpEntity entity = postResponse.getEntity();
            if (entity != null) {
              final String response = EntityUtils.toString(entity);
              log.debug("~callRecaptcha() : Response '" + response + "'.");
              boolean recaptchaSuccess = false;
              try {
                final JSONObject jsonResponse = new JSONObject(response);
                final String success = jsonResponse.getString("success");
                recaptchaSuccess = recaptchaParamTrue.equals(success);
                if (recaptchaSuccess) {
                  log.debug("~callRecaptcha() : Success!");
                  outcome = null;
                } else {
                  log.debug("~callRecaptcha() : Failed!");
                  final JSONArray errorCodes = jsonResponse.getJSONArray("error-codes");
                  final int errorCodeCount = errorCodes.length();
                  for (int errorIdx = 0; errorIdx < errorCodeCount; ++errorIdx) {
                    final String errorCode = (String) errorCodes.get(errorIdx);
                    log.error("~callRecaptcha() : Error '" + errorCode + "'.");
                    if (errorIdx == 0) {
                      // Just use the first of the errors!
                      outcome = recaptchaMessages.get(errorCode);
                    }
                  }
                }
              } catch (JSONException e) {
                log.error("~callRecaptcha() : JSON exception '" + e.getMessage() + "'.");
                outcome = e.getMessage();
              }
            }
            EntityUtils.consume(entity);
          } finally {
            postResponse.close();
          }
        }
      } finally {
        httpClient.close();
      }
    } catch (IOException e) {
      log.error("~callRecaptcha() : IOException '" + e.getMessage() + "'.");
    }

    return outcome;
  }

  /**
   * Validate that an email address has a valid structure.
   * 
   * @param emailAddress Address to validate.
   * @return Compacted email address (i.e. no internal spaces) if valid address, otherwise null.
   */
  public static String validEmailAddress(final String emailAddress) {
    log.debug("~validEmailAddress() : Invoked with '" + emailAddress + "'.");

    if (emailAddress == null) {
      return null;
    }

    // Get rid of all spaces
    final String compactEmailAddress = emailAddress.replace(" ", "");
    // At least one @
    if (!compactEmailAddress.contains("@")) {
      return null;
    }
    final String[] components = compactEmailAddress.split("@");
    // No more than one @
    if (components.length != 2) {
      return null;
    }
    final String namePart = components[0];
    final String addressPart = components[1];
    // At least one char in name and address
    if (namePart.length() == 0 || addressPart.length() == 0) {
      return null;
    }
    // At least one "." in address part
    if (!addressPart.contains(".")) {
      return null;
    }
    // But "." not at start or end
    if (addressPart.startsWith(".") || addressPart.endsWith(".")) {
      return null;
    }
    final String[] addressParts = addressPart.split("\\.");
    for (final String eachAddressPart : addressParts) {
      if (eachAddressPart.length() == 0) {
        return null;
      }
    }

    return compactEmailAddress;
  }
}