/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.business.file;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.stax2.XMLStreamReader2;
import org.codehaus.stax2.XMLStreamWriter2;
import org.codehaus.stax2.validation.XMLValidationSchema;
import org.codehaus.stax2.validation.XMLValidationSchemaFactory;
import org.springframework.stereotype.Component;

import com.ctc.wstx.stax.WstxInputFactory;
import com.ctc.wstx.stax.WstxOutputFactory;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.value.file.FILE_DATA_TYPE;

/**
 * CellML file storage facility.
 *
 * @author geoff
 */
@Component(ClientSharedIdentifiers.COMPONENT_FILE_STORAGE_CELLML)
public class FileStorageCellML extends FileStorage {

  /*
   * Hack! Provisionally mathml2.rng has needed to be placed in CATALINA_HOME
   *       as can't find a way of combining the mathml2.rng and cellml1.0.rng
   *       in the below validation process.
   */
  private static final String cellMLRNGLocation = "META-INF/schema/cellml/cellml1.0.rng";

  private static final Log log = LogFactory.getLog(FileStorageCellML.class);

  /**
   * Initialising constructor.
   */
  protected FileStorageCellML() {
    super.dataType = FILE_DATA_TYPE.CELLML;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.compbio.client_shared.business.file.FileStorage#contentCheck(java.io.InputStream)
   */
  @Override
  protected List<String> contentCheck(final InputStream inputStream)
                                      throws IOException {
    log.debug("~contentCheck() : Invoked.");

    final List<String> problems = new ArrayList<String>();

    final ClassLoader classLoader = getClass().getClassLoader();
    InputStream schemaStreamCellMLRNG = null;
    try {
      /* https://www.cellml.org/cellml/cellml1-0.rng */
      schemaStreamCellMLRNG = classLoader.getResourceAsStream(cellMLRNGLocation);
      /* https://www.cellml.org/cellml/mathml2.rng */
      //schemaStreamMathMLRNG = classLoader.getResourceAsStream("META-INF/schema/cellml/mathml2.rng");
    } catch (Exception e) {
      final String errorMessage = "a error locating the CellML RelaxNG file (expected here: '" + cellMLRNGLocation + "')";
      log.error("~contentCheck() : ".concat(errorMessage));

      problems.add(errorMessage);
      return problems;
    }

    final XMLValidationSchemaFactory relaxNGValidationSchemaFactory =
          XMLValidationSchemaFactory.newInstance(XMLValidationSchema.SCHEMA_ID_RELAXNG);
    XMLValidationSchema relaxNGCellMLValidationSchema = null;
    try {
      relaxNGCellMLValidationSchema = relaxNGValidationSchemaFactory.createSchema(schemaStreamCellMLRNG);
    } catch (XMLStreamException e) {
      final String errorMessage = "an inability to create a RelaxNG schema from '" + cellMLRNGLocation + "'.";
      log.error("~contentCheck() : ".concat(errorMessage).concat(" due to '" + e.getMessage() + "'."));

      problems.add(errorMessage);
      return problems;
    }

    XMLStreamReader2 annotatedStreamReader = null;
    try {
      annotatedStreamReader = (XMLStreamReader2) new WstxInputFactory().createXMLStreamReader(inputStream);
    } catch (XMLStreamException e) {
      final String errorMessage = "a problem creating annotated CellML XML StreamReader from supplied input";
      log.info("~contentCheck() : ".concat(errorMessage).concat(" due to '" + e.getMessage() + "'."));

      problems.add(errorMessage);
      return problems;
    }

    try {
      annotatedStreamReader.validateAgainst(relaxNGCellMLValidationSchema);
    } catch (XMLStreamException e) {
      final String errorMessage = "an error validating supplied input against the CellML RNG schema";
      log.info("~contentCheck() : ".concat(errorMessage).concat(" due to '" + e.getMessage() + "'."));

      problems.add(errorMessage);
      return problems;
    }
 
    final StringWriter writer = new StringWriter();
    XMLStreamWriter2 xmlWriter = null;
    try {
      xmlWriter = (XMLStreamWriter2) new WstxOutputFactory().createXMLStreamWriter(writer);
    } catch (XMLStreamException e) {
      final String errorMessage = "an error creating temporary XML output writer";
      log.info("~contentCheck() : ".concat(errorMessage).concat(" due to '" + e.getMessage() + "'."));

      problems.add(errorMessage);
      return problems;
    }

    try {
      xmlWriter.copyEventFromReader(annotatedStreamReader, false);
    } catch (XMLStreamException e) {
      final String errorMessage = "an error copying events from input stream reader to temporary XML output writer";
      log.info("~contentCheck() : ".concat(errorMessage).concat(" due to '" + e.getMessage() + "'."));

      problems.add(errorMessage);
      return problems;
    }

    try {
      while (annotatedStreamReader.hasNext()) {
        annotatedStreamReader.next();
        xmlWriter.copyEventFromReader(annotatedStreamReader, false);
      }
    } catch (XMLStreamException e) {
      final String errorMessage = "a failed validation of the CellML due to '" + e.getMessage() + "'";
      log.info("~contentCheck() : ".concat(errorMessage));

      problems.add(errorMessage);
      return problems;
    }

    // System.out.println(writer.toString());

    return problems;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.compbio.client_shared.business.file.FileStorage#writeToDatabase()
   */
  @Override
  protected boolean writeToDatabase() {
    return false;
  }
}