/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.controller;

import java.io.Serializable;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.api.exception.DownstreamCommunicationException;
import uk.ac.ox.cs.compbio.client_shared.service.ClientSharedService;

/**
 *
 *
 * @author geoff
 */
@Controller
public class CSAJAX extends AbstractMessageSourceAwareController {

  @Autowired @Qualifier(ClientSharedIdentifiers.COMPONENT_CLIENT_SHARED_SERVICE)
  private ClientSharedService clientSharedService;

  private static final Log log = LogFactory.getLog(CSAJAX.class);

  /**
   * Bean to place into the return MVC model.
   * 
   * @author geoff
   */
  public class JSON implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String json;
    private final String exception;

    public JSON(final String json, final String exception) {
      this.json = json;
      this.exception = exception;
    }

    /** @return the json */
    public String getJson() {
      return json;
    }

    /** @return the exception */
    public String getException() {
      return exception;
    }
  }

  /**
   * Retrieve the App Manager component workload.
   * 
   * @param locale Locale.
   * @param model MVC model.
   * @return Workload (in {@link JSON} format).
   */
  @RequestMapping(method=RequestMethod.GET,
                  value=ClientSharedIdentifiers.URL_PREFIX_AJAX_WORKLOAD)
  public String retrieveAppManagerWorkload(final Locale locale, final Model model) {
    log.debug("~retrieveAppManagerWorkload() : Invoked.");

    JSON json = null;
    try {
      json = new JSON(clientSharedService.retrieveAppManagerWorkload(), null);
    } catch (DownstreamCommunicationException e) {
      json = new JSON(null, queryMessageSource(ClientSharedIdentifiers.BUNDLE_ID_ERROR_WORKLOAD_RETRIEVAL,
                                               new Object[] { e.getMessage() }, locale));
    } catch (Exception e) {
      json = new JSON(null, queryMessageSource(ClientSharedIdentifiers.BUNDLE_ID_ERROR_UNEXPECTED,
                                               new Object[] { e.getMessage() }, locale));
    }
    model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_WORKLOAD, json);

    return ClientSharedIdentifiers.VIEW_NAME_JSON;
  }
}