/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.controller;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.controller.util.ControllerUtil;
import uk.ac.ox.cs.compbio.client_shared.service.ClientSharedService;

/**
 * Controller handling user registration.
 * 
 * @author geoff
 */
@Controller
public class Register extends AbstractMessageSourceAwareController {

  // Spring property configuration
  @Value("${recaptcha.public.key}")
  private String recaptchaPublicKey;

  // Spring property configuration
  @Value("${recaptcha.private.key}")
  private String recaptchaPrivateKey;

  @Autowired @Qualifier(ClientSharedIdentifiers.COMPONENT_CLIENT_SHARED_SERVICE)
  private ClientSharedService clientSharedService;

  protected static final String success = "success";

  private static final Log log = LogFactory.getLog(Register.class);

  private void populateForm(final Model model, final Locale locale, final String msgBundleId,
                            final String emailAddress) {
    final String useText = queryMessageSource(msgBundleId, new Object[] {}, locale);
    model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_REGISTRATION_ERROR,
                       useText);
    model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_EMAIL_ADDRESS, emailAddress);
  }

  /**
   * Process a registration request.
   * 
   * @param recaptchaResponseField User's reCAPTCHA response.
   * @param emailAddress Email address.
   * @param locale Locale.
   * @param model UI model.
   * @return Page to view after processing.
   */
  @RequestMapping(method=RequestMethod.POST,
                  value=ClientSharedIdentifiers.ACTION_REGISTRATION)
  public String processRegistration(final @RequestParam(required=false,
                                                        value="g-recaptcha-response")
                                          String recaptchaResponseField,
                                    final @RequestParam(required=true,
                                                        value=ClientSharedIdentifiers.PARAM_NAME_EMAIL_ADDRESS)
                                          String emailAddress,
                                    final Locale locale,
                                    final Model model) {
    log.debug("~processRegistration() : Invoked.");

    final String validatedEmailAddress = ControllerUtil.validEmailAddress(emailAddress);
    if (validatedEmailAddress != null) {
      if (!StringUtils.isEmpty(recaptchaPublicKey)) {
        // reCAPTCHA processing should assign appropriate error for all circumstances.
        final String errorMessageBundleId = ControllerUtil.callRecaptcha(recaptchaPrivateKey,
                                                                         recaptchaResponseField);
        if (errorMessageBundleId == null) {
          log.debug("~processRegistration() : reCAPTCHA success.");
          try {
            clientSharedService.sendRegistrationEmail(validatedEmailAddress);
            model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_REGISTRATION_SUCCESS,
                               success);
          } catch (MailException e) {
            log.error("~processRegistration() : Error sending email '" + e.getMessage() + "'.");
            // Place email back into form if failed email sending.
            populateForm(model, locale, ClientSharedIdentifiers.BUNDLE_ID_REGN_FAIL,
                         validatedEmailAddress);
          }
        } else {
          log.debug("~processRegistration() : reCAPTCHA failure bundle id '" + errorMessageBundleId + "'.");
          // Place email back into form if failed reCaptcha challenge.
          populateForm(model, locale, errorMessageBundleId, emailAddress);
        }
      } else {
        // No recaptcha public key, accept solely an email address.
        try {
          clientSharedService.sendRegistrationEmail(validatedEmailAddress);
        } catch (MailException e) {
          log.error("~processRegistration() : Error sending email '" + e.getMessage() + "'.");
          // Place email back into form if failed email sending.
          populateForm(model, locale, ClientSharedIdentifiers.BUNDLE_ID_REGN_FAIL,
                       validatedEmailAddress);
        }
      }
    } else {
      // Invalid email address received.
      populateForm(model, locale, ClientSharedIdentifiers.BUNDLE_ID_INVALID_EMAIL_ADDR,
                   emailAddress);
    }

    Login.setModelPropertiesForLogin(model, recaptchaPublicKey,
                                     clientSharedService.isMailerConfigured());

    return ClientSharedIdentifiers.PAGE_LOGIN;
  }

  /**
   * User's used browser location bar to get to registration page.
   *  
   * @param model UI model.
   * @return Page to view.
   */
  @RequestMapping(method=RequestMethod.GET,
                  value=ClientSharedIdentifiers.ACTION_REGISTRATION)
  public String redirectToLogin(final Model model) {
    log.debug("~redirectToLogin() : Invoked.");

    // Put the public key back (in case they want to register more than once!)
    Login.setModelPropertiesForLogin(model, recaptchaPublicKey,
                                     clientSharedService.isMailerConfigured());

    return ClientSharedIdentifiers.PAGE_LOGIN;
  }
}