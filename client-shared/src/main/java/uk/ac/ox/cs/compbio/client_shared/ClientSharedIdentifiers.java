/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared;

/**
 * Shared client identifiers.
 *
 * @author geoff
 */
public class ClientSharedIdentifiers {

  public static final String ACTION_REGISTRATION = "registration";

  public static final String BUNDLE_ID_ERROR_UNEXPECTED = "cs_error.unexpected";
  public static final String BUNDLE_ID_ERROR_WORKLOAD_RETRIEVAL = "cs_error.workload_retrieval";
  public static final String BUNDLE_ID_INVALID_EMAIL_ADDR = "cs_error.invalid_email_address";
  public static final String BUNDLE_ID_RECAPTCHA_ERR_INVALID_INPUT_RESPONSE = "cs_error.recaptcha_invalid-input-response";
  public static final String BUNDLE_ID_RECAPTCHA_ERR_INVALID_INPUT_SECRET = "cs_error.recaptcha_invalid-input-secret";
  public static final String BUNDLE_ID_RECAPTCHA_ERR_MISSING_INPUT_RESPONSE = "cs_error.recaptcha_missing-input-response";
  public static final String BUNDLE_ID_RECAPTCHA_ERR_MISSING_INPUT_SECRET = "cs_error.recaptcha_missing-input-secret";
  public static final String BUNDLE_ID_RECAPTCHA_ERR_UNKNOWN="cs_error.recaptcha_unknown";
  public static final String BUNDLE_ID_REGN_FAIL = "cs_error.registration_failure";

  public static final String CONTROLLER_KEY_EXCEPTION = "exception";
  public static final String CONTROLLER_KEY_JSON = "json";

  public static final String COMPONENT_CLIENT_SHARED_CONFIGURATION_DAO = "csConfigurationDAO";
  public static final String COMPONENT_CLIENT_SHARED_CONFIGURATION_MANAGER = "csConfigurationManager";
  public static final String COMPONENT_CLIENT_SHARED_MANAGER = "clientSharedManager";
  public static final String COMPONENT_CLIENT_SHARED_SERVICE = "clientSharedService";
  public static final String COMPONENT_FILE_DAO = "fileDAO";
  public static final String COMPONENT_FILE_MANAGER = "fileManager";
  // see appCtx.multipart.xml
  public static final String COMPONENT_FILE_STORAGE_CELLML = "fileStorageCellML";
  // see appCtx.multipart.xml
  public static final String COMPONENT_FILE_STORAGE_PK = "fileStoragePK";
  public static final String COMPONENT_MAIL_MANAGER = "mailManager";
  // see appCtx.mail.xml
  public static final String COMPONENT_MAIL_REGISTRATION_TEMPLATE = "templateRegistrationMail";
  // see appCtx.mail.xml
  public static final String COMPONENT_MAIL_SENDER = "mailSender";

  /** This identifies a placeholder model identifier value for dynamic CellML 
      purposes. */
  public static final short IDENTIFIER_DYNAMIC_CELLML = Short.MAX_VALUE;

  /** For drawing in additional resources, e.g. .js files, at runtime. */
  public static final String I18N_LANGS = "es,zh"; // TODO : Make configurable.

  /** Log out URL (Prefix important. Update appCtx.root-security.xml if modified) */
  public static final String LOGOUT = "/logout";

  public static final String MODEL_ATTRIBUTE_EMAIL_ADDRESS = "ma_email_address";
  public static final String MODEL_ATTRIBUTE_MAILER_CONFIGURED = "ma_mailer_configured";
  public static final String MODEL_ATTRIBUTE_MESSAGE_ERROR = "ma_message_error";
  public static final String MODEL_ATTRIBUTE_MODEL_NAMES = "ma_model_names";
  public static final String MODEL_ATTRIBUTE_PUBLIC_RECAPTCHA_KEY = "ma_public_recaptcha_key";
  public static final String MODEL_ATTRIBUTE_QNET_AVAILABLE = "ma_qnet_available";
  public static final String MODEL_ATTRIBUTE_REGISTRATION_ERROR = "ma_registration_error";
  public static final String MODEL_ATTRIBUTE_REGISTRATION_SUCCESS = "ma_registration_success";
  public static final String MODEL_ATTRIBUTE_SIMULATION_ID = "ma_simulation_id";
  public static final String MODEL_ATTRIBUTE_SHOW_DYNAMIC_CELLML = "ma_show_dynamic_cellml";
  public static final String MODEL_ATTRIBUTE_SHOW_UNCERTAINTIES = "ma_show_uncertainties";
  public static final String MODEL_ATTRIBUTE_RESULTS = "ma_results";
  public static final String MODEL_ATTRIBUTE_WORKLOAD = "ma_workload";

  public static final String PAGE_ABOUT = "page_about";
  public static final String PAGE_CONTACT = "page_contact";
  public static final String PAGE_LOGIN = "page_login";
  public static final String PAGE_PRIVACY = "page_privacy";

  public static final String PARAM_NAME_EMAIL_ADDRESS = "email_address";
  public static final String PARAM_NAME_FILE_CELLML="file_cellml";
  public static final String PARAM_NAME_FILE_PK="file_pk";
  public static final String PARAM_NAME_SIMULATION_ID="simulation_id";

  public static final String URL_PREFIX_AJAX_WORKLOAD = "ajax/workload";

  public static final String VIEW_ABOUT = "about";
  public static final String VIEW_CONTACT = "contact";
  public static final String VIEW_LOGIN = "login";
  /* See appCtx.view.xml.
     viewJSON is a MappingJacksonJsonView where the returned MVC model is converted to JSON. */
  public static final String VIEW_NAME_JSON = "viewJSON";
  public static final String VIEW_PRIVACY = "privacy";

  private ClientSharedIdentifiers() {}

}