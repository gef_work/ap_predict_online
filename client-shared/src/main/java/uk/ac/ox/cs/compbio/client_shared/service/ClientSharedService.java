/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.service;

import org.springframework.mail.MailException;

import uk.ac.ox.cs.compbio.client_shared.api.exception.DownstreamCommunicationException;
import uk.ac.ox.cs.compbio.client_shared.api.manager.ClientSharedManager;
import uk.ac.ox.cs.compbio.client_shared.api.manager.MailManager;

/**
 * Methods defined here permit components to interact with manager classes
 * which may have their implementations either within {@code client-shared} (as
 * in the case of {@link MailManager}) or within {@code client} and 
 * {@code client-direct} components (as in the case of
 * {@link ClientSharedManager}).
 *
 * @author geoff
 */
public interface ClientSharedService {


  /**
   * Retrieve flag to indicate if the mailer has been correctly configured.
   * 
   * @return {@code true} if mailer has been configured, otherwise {@code false}.
   */
  boolean isMailerConfigured();

  /**
   * Retrieve data of app manager component workload.
   * 
   * @return Current workload.
   * @throws DownstreamCommunicationException If a problem downstream in communicating.
   */
  String retrieveAppManagerWorkload() throws DownstreamCommunicationException;

  /**
   * Send a registration email.
   * 
   * @param emailAddress User's email address.
   * @throws MailException If there's been a problem sending mail.
   */
  void sendRegistrationEmail(String emailAddress) throws MailException;

}