/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.manager;

import java.io.InputStream;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.business.file.FileStorageCellML;
import uk.ac.ox.cs.compbio.client_shared.business.file.FileStoragePK;
import uk.ac.ox.cs.compbio.client_shared.dao.FileDAO;
import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.compbio.client_shared.value.file.FILE_DATA_TYPE;
import uk.ac.ox.cs.compbio.client_shared.value.file.FileStoreActionOutcomeVO;

/**
 * File management implementation.
 *
 * @author geoff
 */
@Component(ClientSharedIdentifiers.COMPONENT_FILE_MANAGER)
@Transactional(readOnly=true)
public class FileManagerImpl implements FileManager {

  @Autowired @Qualifier(ClientSharedIdentifiers.COMPONENT_FILE_DAO)
  private FileDAO fileDAO;

  @Autowired @Qualifier(ClientSharedIdentifiers.COMPONENT_FILE_STORAGE_CELLML)
  private FileStorageCellML fileStorageCellML;

  @Autowired @Qualifier(ClientSharedIdentifiers.COMPONENT_FILE_STORAGE_PK)
  private FileStoragePK fileStoragePK;

  private static final Log log = LogFactory.getLog(FileManagerImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.compbio.client_shared.manager.FileManager#retrieveAllFiles()
   */
  @Override
  public List<PortalFile> retrieveAllFiles() {
    log.debug("~retrieveAllFiles() : Invoked.");

    return fileDAO.findPortalFiles();
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.compbio.client_shared.manager.FileManager#storeFile(uk.ac.ox.cs.compbio.client_shared.value.file.FILE_DATA_TYPE, java.io.InputStream, java.lang.String, java.lang.String)
   */
  @Override
  @Transactional(readOnly=false)
  public FileStoreActionOutcomeVO storeFile(final FILE_DATA_TYPE fileDataType,
                                            final InputStream file,
                                            final String name,
                                            final String uploader) {
    log.debug("~storeFile() : Invoked.");

    FileStoreActionOutcomeVO outcome = null;

    switch (fileDataType) {
      case PK :
        outcome = fileStoragePK.storeFile(file, name, uploader);
        break;
      case CELLML :
        outcome = fileStorageCellML.storeFile(file, name, uploader);
        break;
      default :
        throw new UnsupportedOperationException("Unrecognized file type");
    }

    return outcome;
  }
}