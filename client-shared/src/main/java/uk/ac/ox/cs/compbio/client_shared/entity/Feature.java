/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.entity;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;

import uk.ac.ox.cs.compbio.client_shared.value.PortalFeature;
import uk.ac.ox.cs.compbio.client_shared.value.security.Role;

/**
 * Portal features.
 *
 * @author geoff
 */
@Entity
// Ensure consistency between unique name 
@NamedQueries({
  @NamedQuery(name=Feature.QUERY_BY_PORTALFEATURE,
              query="SELECT feature FROM Feature AS feature " +
                    "  WHERE " + Feature.PROPERTY_PORTALFEATURE + " = :" + Feature.PROPERTY_PORTALFEATURE)
})
@Table(uniqueConstraints = {
  @UniqueConstraint(columnNames = { "portalFeature" },
                    name="unique_portal_feature")
})
public class Feature implements Serializable {

  private static final long serialVersionUID = -805551204133912226L;

  // Persistence identity (Surrogate primary key)
  @TableGenerator(name="feature_id_gen", table="sequence_pks_client_shared",
                  pkColumnName="pk_seq_name", pkColumnValue="feature_id",
                  valueColumnName="pk_seq_value", allocationSize=1)
  @Id @GeneratedValue(strategy=GenerationType.TABLE, generator="feature_id_gen")
  private Long id;

  @Column(insertable=true, nullable=false, updatable=false)
  @Enumerated(EnumType.STRING)
  private PortalFeature portalFeature;

  @ElementCollection(targetClass = Role.class)
  @CollectionTable(name="FeatureRoles",
                   joinColumns = @JoinColumn(name = "feature_id"))
  @Column(nullable=true)
  @Enumerated(EnumType.STRING)
  private Set<Role> restrictedRoles = new HashSet<Role>();

  /* Consistent property name */
  public static final String PROPERTY_PORTALFEATURE = "portalFeature";

  /** Consistent query naming - Query by portal feature */
  public static final String QUERY_BY_PORTALFEATURE = "feature.queryByPortalFeature";

  /** <b>Do not invoke directly.</b> */
  protected Feature() {}

  /**
   * Initialising constructor.
   * 
   * @param portalFeature Portal feature.
   * @param restrictedRoles Roles of users who are allowed to use the feature.
   *                        {@code null} implies feature available to any user.
   * @throws IllegalArgumentException If portal feature is {@code null}.
   */
  public Feature(final PortalFeature portalFeature,
                 final Set<Role> restrictedRoles)
                 throws IllegalArgumentException {
    if (portalFeature == null) {
      throw new IllegalArgumentException("A portal feature must be named!");
    }
    this.portalFeature = portalFeature;
    if (restrictedRoles != null) {
      this.restrictedRoles.addAll(restrictedRoles);
    }
  }

  /**
   * Retrieve indicator as to whether the user can access the feature.
   * 
   * @param userRoles User's roles.
   * @return {@code true} if user can access the feature, otherwise
   *         {@code false}.
   */
  public boolean accessible(final Set<Role> userRoles) {
    if (restrictedRoles.isEmpty()) {
      return true;
    }

    boolean accessible = true;
    // Traverse the roles required to access the feature and deny if user 
    for (final Role restrictedRole : getRestrictedRoles()) {
      if (!userRoles.contains(restrictedRole)) {
        accessible = false;
        break;
      }
    }

    return accessible;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Feature [id=" + id + ", portalFeature=" + portalFeature + ", restrictedRoles=" + restrictedRoles
        + "]";
  }

  /**
   * Retrieve an unmodifiable collection of the roles needed in order to access
   * the feature.
   *  
   * @return Roles required to access the feature. An empty collection implies
   *         anybody can access the feature.
   */
  public Set<Role> getRestrictedRoles() {
    return Collections.unmodifiableSet(restrictedRoles);
  }

  /**
   * Assign the roles of users who can access the feature. An empty collection
   * implies that anybody can use the feature.
   * 
   * @param restrictedRoles The roles which the feature is restricted to, or 
   *                        {@code null} if unrestricted access to feature.
   */
  public void setUserRoles(final Set<Role> restrictedRoles) {
    if (restrictedRoles == null || restrictedRoles.isEmpty()) {
      this.restrictedRoles.clear();
    } else {
      this.restrictedRoles.addAll(restrictedRoles);
    }
  }

  /**
   * Retrieve the portal feature.
   * 
   * @return The portal feature.
   */
  public PortalFeature getPortalFeature() {
    return portalFeature;
  }
}