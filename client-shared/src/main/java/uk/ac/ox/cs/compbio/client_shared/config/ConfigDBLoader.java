/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.config;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import uk.ac.ox.cs.compbio.client_shared.entity.Feature;
import uk.ac.ox.cs.compbio.client_shared.value.PortalFeature;
import uk.ac.ox.cs.compbio.client_shared.value.PortalFeatureVO;

/**
 * Load configuration objects into the database on application startup.
 *
 * @author geoff
 */
@Component
@Transactional
public class ConfigDBLoader implements ApplicationListener<ContextRefreshedEvent> {

  private static final String springWebAppCtx = "org.springframework.web.context.WebApplicationContext:";

  @PersistenceContext
  private EntityManager entityManager;

  private Set<PortalFeatureVO> portalFeatures = new HashSet<PortalFeatureVO>();

  private static final Log log = LogFactory.getLog(ConfigDBLoader.class);

  // Values declared in client-shared src/main/resources/META-INF/resources/WEB-INF/spring/ctx/config/appCtx.features.xml
  @Autowired(required=true)
  public ConfigDBLoader(final Set<PortalFeatureVO> portalFeatures) {
    log.debug("~ConfigDBLoader() : Invoked.");

    if (portalFeatures != null) {
      log.debug("~ConfigDBLoader() : Portal feature configuration values found!");
      this.portalFeatures.addAll(portalFeatures);
    }
  }

  /* (non-Javadoc)
   * @see org.springframework.context.ApplicationListener#onApplicationEvent(org.springframework.context.ApplicationEvent)
   */
  @Override
  public void onApplicationEvent(final ContextRefreshedEvent event) {
    /*
     * ContextRefreshedEvent may happen twice on application start (e.g.
     * WebApplicationContext and GenericWebApplicationContext), or on 
     * integration testing just the once (GenericApplicationContext).
     * For db loading we're only interested in when the dispatcher servlet
     * starts (for Tomcat anyway, may differ for others!?).
     */
    final String appCtxId = event.getApplicationContext().getId();
    log.debug("~onApplicationEvent() : Invoked for '" + appCtxId + "'.");
    if (appCtxId.startsWith(springWebAppCtx)) {
      load();
    }
  }

  /**
   * Load items into database.
   */
  @Transactional(readOnly=false)
  protected void load() {
    log.debug("~load() : Invoked.");

    final Query query = entityManager.createNamedQuery(Feature.QUERY_BY_PORTALFEATURE);

    for (final PortalFeatureVO portalFeatureVO : portalFeatures) {
      final PortalFeature portalFeature = portalFeatureVO.getPortalFeature();
      final Feature feature = new Feature(portalFeature,
                                          portalFeatureVO.getRestrictedRoles());

      query.setParameter(Feature.PROPERTY_PORTALFEATURE,
                         feature.getPortalFeature());

      // Query to see if the feature already exists in the database.
      @SuppressWarnings("unchecked")
      final List<Feature> features = query.getResultList();

      if (features.isEmpty()) {
        entityManager.persist(feature);
        log.warn("~load() : Loading portal feature '" + feature + "'.");
      } else {
        log.warn("~load() : Ignoring load of portal features, using existing!");
      }
    }
  }
}