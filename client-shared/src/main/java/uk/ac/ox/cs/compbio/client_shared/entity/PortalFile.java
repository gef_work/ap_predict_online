/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.StringUtils;

import uk.ac.ox.cs.compbio.client_shared.api.entity.SecuredEntity;

/**
 * Portal file entity.
 *
 * @author geoff
 */
@Entity
@NamedQueries({
  @NamedQuery(name=PortalFile.QUERY_PORTALFILE_ALL,
              query="SELECT portalfile FROM PortalFile AS portalfile")
})
public class PortalFile implements SecuredEntity, Serializable {

  private static final long serialVersionUID = -8733969615323219928L;

  /**
   * <b>
   *   If the length of file content exceeds this then the file is not
   *   persisted
   * </b>.
   */
  /* Upload file size value also determined by 
     client-shared/src/main/resources/META-INF/resources/WEB-INF/spring/ctx/appCtx.multipart.xml

     42000000 allows for 40Mb (41943040 bytes) + a couple of other (e.g. approx
     10k min) bytes. */
  public static final int MAX_SIZE = 42000000;

  /** Consistent query naming - Query all portal files */
  public static final String QUERY_PORTALFILE_ALL = "portalFile.queryAll";

  // Persistence identity (Surrogate primary key)
  @TableGenerator(name="portalfile_id_gen", table="sequence_pks_client_shared",
                  pkColumnName="pk_seq_name", pkColumnValue="portalfile_id",
                  valueColumnName="pk_seq_value", allocationSize=1)
  @Id @GeneratedValue(strategy=GenerationType.TABLE, generator="portalfile_id_gen")
  private Long id;

  /* String of content (MySQL seems to create a "longtext" (4Gb) field, HSQL a varchar(xxxx)!)
     For MySQL (or general info!) see https://dev.mysql.com/doc/refman/5.7/en/server-system-variables.html#sysvar_max_allowed_packet */
  @Lob
  @Column(insertable=true, length=MAX_SIZE, nullable=false, updatable=false)
  private String content;

  @Column(insertable=true, nullable=true, updatable=false)
  private String name;

  @Column(insertable=true, nullable=false, updatable=false)
  private String creator;

  @Temporal(value=TemporalType.TIMESTAMP)
  private Date persisted;

  /** <b>Do not invoke directly.</b> */
  protected PortalFile() {}

  /**
   * Initialising constructor.
   * 
   * @param content Portal file content.
   * @param name Optional portal file name.
   * @param creator Name of file uploader.
   * @throws IllegalArgumentException If {@linkplain #content} or
   *                                  {@linkplain #creator} is blank.
   */
  public PortalFile(final String content, final String name,
                    final String creator) throws IllegalArgumentException {
    if (StringUtils.isBlank(content) || StringUtils.isBlank(creator)) {
      throw new IllegalArgumentException("A portal file requires both content and creator data!");
    }

    this.content = content;
    this.name = name;
    this.creator = creator;
  }

  // Internal callback method
  @PrePersist
  protected void onCreate() {
    persisted = new Date();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "PortalFile [id=" + id + ", name=" + name + ", creator=" + creator
        + ", persisted=" + persisted + "]";
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.compbio.client_shared.entity.SecuredEntity#getId()
   */
  @Override
  public Long getId() {
    return id;
  }

  /**
   * Retrieve the file content.
   * 
   * @return PortalFile content.
   */
  public String getContent() {
    return content;
  }

  /**
   * Retrieve the original file name.
   * 
   * @return Original file name (or {@code null} if not supplied).
   */
  public String getName() {
    return name;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.compbio.client_shared.entity.SecuredEntity#getCreator()
   */
  @Override
  public String getCreator() {
    return creator;
  }

  /**
   * Retrieve the timestamp of when file was stored.
   * 
   * @return The persisted date.
   */
  public Date getPersisted() {
    return persisted;
  }
}