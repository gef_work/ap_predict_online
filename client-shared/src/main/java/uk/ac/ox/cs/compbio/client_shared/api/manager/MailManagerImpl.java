/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.api.manager;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;

/**
 * Implementation of the mail management operations.
 *
 * @author geoff
 */
@Component(ClientSharedIdentifiers.COMPONENT_MAIL_MANAGER)
public class MailManagerImpl implements MailManager {

  private boolean isMailerConfigured = false;

  @Autowired @Qualifier(ClientSharedIdentifiers.COMPONENT_MAIL_SENDER)
  private MailSender mailSender;

  @Autowired @Qualifier(ClientSharedIdentifiers.COMPONENT_MAIL_REGISTRATION_TEMPLATE)
  private SimpleMailMessage templateRegistrationMail;

  private static final Log log = LogFactory.getLog(MailManagerImpl.class);

  /**
   * Initialise the bean.
   */
  @PostConstruct
  protected void mailManagerInit() {
    final JavaMailSenderImpl mailSenderImpl = (JavaMailSenderImpl) mailSender;

    if (mailSenderImpl == null) {
      log.warn("~mailManagerInit() : No mail sender configured!");
      isMailerConfigured = false;
    }

    final String host = mailSenderImpl.getHost();
    final int port = mailSenderImpl.getPort();

    final String from = templateRegistrationMail.getFrom();
    final String[] to = templateRegistrationMail.getTo();

    final boolean hasValidHost = !StringUtils.isEmpty(host);
    final boolean hasValidPort = port > -1 && port < 65536;
    final boolean hasValidFrom = !StringUtils.isEmpty(from);
    final boolean hasValidTo = (to != null) && (to.length > 0) && (!StringUtils.isEmpty(to[0]));

    isMailerConfigured = hasValidHost && hasValidPort && hasValidFrom && hasValidTo;

    if (!isMailerConfigured) {
      log.warn("~mailManagerInit() : Mailer is not configured due to ....");
      if (!hasValidHost) {
        log.warn("~mailManagerInit() : Invalid mail 'host' of '" + host + "'.");
      }
      if (!hasValidPort) {
        log.warn("~mailManagerInit() : Invalid mail 'port' of " + port + "'.");
      }
      if (!hasValidFrom) {
        log.warn("~mailManagerInit() : Invalid mail 'from' of '" + from + "'.");
      }
      if (!hasValidTo) {
        // May be showing an empty array, e.g. [Ljava.lang.String;@76b9591e
        log.warn("~mailManagerInit() : Invalid mail 'to' of  '" + to + "'.");
      }
    }
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.compbio.client_shared.manager.MailManager#isMailerConfigured()
   */
  public boolean isMailerConfigured() {
    log.debug("~mailerConfigured() : Invoked.");

    return isMailerConfigured;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.compbio.client_shared.manager.MailManager#sendEmail(java.lang.String, uk.ac.ox.cs.compbio.client_shared.manager.MailManager.EMAIL_TYPE)
   */
  public void sendEmail(final String address, final EMAIL_TYPE emailType) throws MailException {
    log.debug("~sendEmail() : Invoked for type '" + emailType + "'.");

    switch (emailType) {
      case REGISTRATION :
        final SimpleMailMessage registrationMail = new SimpleMailMessage(templateRegistrationMail);
        registrationMail.setText("Request from '" + address  + "'.");
        mailSender.send(registrationMail);

        break;
      default :
        log.error("~sendEmail() : Unrecognized email type of '" + emailType + "' requested.");
        break;
    }
  }
}