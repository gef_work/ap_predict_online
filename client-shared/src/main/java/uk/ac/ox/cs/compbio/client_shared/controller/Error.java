/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller handling view resolution when unexpected errors occur.
 *
 * @author geoff
 */
@Controller
public class Error {

  protected static final String pageError400 = "error/error400";
  protected static final String pageError403 = "error/error403";
  protected static final String pageError404 = "error/error404";
  protected static final String pageError500 = "error/error500";

  private static final Log log = LogFactory.getLog(Error.class);

  /**
   * Handle a HTTP 400. Most likely arrived here via web.xml's <code>&lt;error-page&gt;</code>
   * instruction. 
   * 
   * @return View to show (in WEB-INF/views).
   */
  @RequestMapping(value="error400")
  public String handle400() {
    log.warn("~handle400() : Invoked.");

    return pageError400;
  }

  /**
   * Handle a HTTP 403. Most likely arrived here via web.xml's <code>&lt;error-page&gt;</code>
   * instruction. 
   * 
   * @return View to show (in WEB-INF/views).
   */
  @RequestMapping(value="error403")
  public String handle403() {
    log.warn("~handle403() : Invoked.");

    return pageError403;
  }

  /**
   * Handle a HTTP 404. Most likely arrived here via web.xml's <code>&lt;error-page&gt;</code>
   * instruction. 
   * 
   * @return View to show (in WEB-INF/views).
   */
  @RequestMapping(value="error404")
  public String handle404() {
    log.warn("~handle404() : Invoked.");

    return pageError404;
  }

  /**
   * Handle a HTTP 500 (Internal Server Error).
   * 
   * @return View to show (in WEB-INF/views).
   */
  @RequestMapping(value="error500")
  public String handle500() {
    log.warn("~handle500() : Invoked.");

    return pageError500;
  }

}