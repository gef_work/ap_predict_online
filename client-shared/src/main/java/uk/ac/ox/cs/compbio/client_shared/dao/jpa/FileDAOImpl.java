/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.dao.FileDAO;
import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;

/**
 * Implementation of the portal file DAO.
 *
 * @author geoff
 */
@Repository(ClientSharedIdentifiers.COMPONENT_FILE_DAO)
public class FileDAOImpl implements FileDAO {

  @PersistenceContext
  private EntityManager entityManager;

  private static final Log log = LogFactory.getLog(FileDAOImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.compbio.client_shared.dao.PortalFileDAO#findPortalFiles()
   */
  @Override
  public List<PortalFile> findPortalFiles() {
    log.debug("~findPortalFiles() : Invoked.");

    final Query query = entityManager.createNamedQuery(PortalFile.QUERY_PORTALFILE_ALL);
    @SuppressWarnings("unchecked")
    final List<PortalFile> portalFiles = query.getResultList();

    log.debug("~findPortalFiles() : '" + portalFiles.size() + "' retrieved.");

    return portalFiles;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.compbio.client_shared.dao.PortalFileDAO#store(uk.ac.ox.cs.compbio.client_shared.entity.PortalFile)
   */
  @Override
  public PortalFile store(final PortalFile portalFile) {
    log.debug("~store() : Invoked for '" + portalFile.toString() + "'.");

    if (portalFile.getId() != null) {
      return entityManager.merge(portalFile);
    } else {
      entityManager.persist(portalFile);
      return portalFile;
    }
  }
}