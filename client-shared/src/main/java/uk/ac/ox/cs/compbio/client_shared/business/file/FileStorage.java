/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.business.file;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.dao.FileDAO;
import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.compbio.client_shared.value.file.FILE_DATA_TYPE;
import uk.ac.ox.cs.compbio.client_shared.value.file.FileCharacteristicsVO;
import uk.ac.ox.cs.compbio.client_shared.value.file.FileStoreActionOutcomeVO;

import com.ibm.icu.text.CharsetDetector;
import com.ibm.icu.text.CharsetMatch;

/**
 * Template Method class determining file storage technique.
 *
 * @author geoff
 */
@Component()
public abstract class FileStorage {

  private boolean demandUTF8 = false;
  protected FILE_DATA_TYPE dataType;

  private static final int defaultExpectedConfidenceLevel = 90;
  private int expectedConfidenceLevel = defaultExpectedConfidenceLevel;

  public static final String UTF8_NAME = StandardCharsets.UTF_8.name();

  @Autowired @Qualifier(ClientSharedIdentifiers.COMPONENT_FILE_DAO)
  private FileDAO fileDAO;

  private static final Log log = LogFactory.getLog(FileStorage.class);

  /**
   * Store a file.
   * 
   * @param inputStream File input stream.
   * @param uploader Person storing the file.
   * @return File store action outcome
   */
  public FileStoreActionOutcomeVO storeFile(final InputStream inputStream,
                                            final String name,
                                            final String uploader) {
    log.debug("~storeFile() : Invoked.");

    final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

    try {
      IOUtils.copy(inputStream, byteArrayOutputStream);
    } catch (IOException e) {
      final String warnMessage = "Error copying file internals '" + e.getMessage() + "'.";
      log.warn("~storeFile() : ".concat(warnMessage));
      return new FileStoreActionOutcomeVO(null, false, warnMessage);
    }
    final byte[] bytes = byteArrayOutputStream.toByteArray();
    final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);

    if (isDemandUTF8()) {
      /*
       * Determine the file characteristics and verify against preferences.
       */
      FileCharacteristicsVO fileCharacteristics = null;
      byteArrayInputStream.reset();
      try {
        fileCharacteristics = determineCharacteristics(byteArrayInputStream);
      } catch (IOException e) {
        final String warnMessage = "Error determining file characteristics '" + e.getMessage() + "'.";
        log.warn("~storeFile() : ".concat(warnMessage));
        return new FileStoreActionOutcomeVO(null, false, warnMessage);
      }

      // Check to see if uploaded file is compliant with expectations, e.g. 76% confidence of UTF-8
      if (!isFileCompliant(fileCharacteristics)) {
        final String information = "Confidence '" + getExpectedConfidenceLevel() + "' of '"
                                                  + UTF8_NAME + "' encoding required, but encountered confidence '"
                                                  + fileCharacteristics.getConfidence() + "' of '"
                                                  + fileCharacteristics.getCharset() + "'.";
        // Rejecting non-compliant
        return new FileStoreActionOutcomeVO(null, false, information);
      }
    }

    /*
     * Determine the structure and verify against preferences.
     */
    final List<String> problems = new ArrayList<String>();
    byteArrayInputStream.reset();
    try {
      problems.addAll(contentCheck(byteArrayInputStream));
    } catch (IOException e) {
      final String warnMessage = "Error reading uploaded file '" + e.getMessage() + "'";
      log.warn("~storeFile() : ".concat(warnMessage));
      return new FileStoreActionOutcomeVO(null, false, warnMessage);
    }

    // Go no further if problems have been encountered in the PK file.
    if (!problems.isEmpty()) {
      final String information = "Uploaded file had " + StringUtils.join(problems, " and "); 
      return new FileStoreActionOutcomeVO(null, false, information);
    }

    /*
     * Write a copy of the file to the database.
     */
    byteArrayInputStream.reset();
    final FileStoreActionOutcomeVO outcome = writeFileToDatabase(byteArrayInputStream,
                                                                 name, uploader,
                                                                 writeToDatabase());

    return outcome;
  }

  /**
   * Check the content of the files.
   * 
   * @param inputStream File input stream.
   * @return Unmodifiable collection of problems, or empty collection if no problems.
   * @throws IOException If problems reading from input stream's buffered reader.
   */
  protected abstract List<String> contentCheck(InputStream inputStream)
                                               throws IOException;

  /**
   * Instruct whether to write this type of file to the database or not.
   * 
   * @return {@code true} if to persist, otherwise {@code false}.
   */
  protected abstract boolean writeToDatabase();

  private FileCharacteristicsVO determineCharacteristics(final InputStream inputStream)
                                                         throws IOException {
    log.debug("~determineCharacteristics() : Invoked.");

    if (inputStream == null) {
      return null;
    }

    final CharsetDetector charsetDetector = new CharsetDetector();
    charsetDetector.setText(inputStream);

    final CharsetMatch charsetMatch = charsetDetector.detect();

    return new FileCharacteristicsVO(charsetMatch.getName(),
                                     charsetMatch.getConfidence());
  };

  private boolean isFileCompliant(final FileCharacteristicsVO fileCharacteristics) {
    log.debug("~isFileCompliant() : Invoked.");

    if (fileCharacteristics == null) {
      throw new IllegalArgumentException("File characteristics must be provided");
    }

    final String charset = fileCharacteristics.getCharset();
    final Integer confidence = fileCharacteristics.getConfidence();
    log.debug("~isFileCompliant() : Charset '" + charset + "', confidence '" + confidence + "'.");

    if (isDemandUTF8()) {
      // We must have UTF-8
      if (charset == null || !charset.equals(UTF8_NAME)) {
        return false;
      }
      // And we must be confident!
      if (confidence == null || confidence < getExpectedConfidenceLevel()) {
        return false;
      }
    }

    return true;
  }

  private FileStoreActionOutcomeVO writeFileToDatabase(final InputStream inputStream,
                                                       final String name,
                                                       final String uploader,
                                                       final boolean persist) {
    log.debug("~writeFileToDatabase() : Invoked.");

    final StringWriter stringWriter = new StringWriter();
    try {
      IOUtils.copy(inputStream, stringWriter, UTF8_NAME);
    } catch (IOException e) {
      final String errorMessage = "Error copying file to text stream for database persistence '" + e.getMessage() + "'.";
      log.error("~writeFileToDatabase() : ".concat(errorMessage));
      return new FileStoreActionOutcomeVO(null, false, errorMessage);
    }

    final PortalFile portalFile = new PortalFile(stringWriter.toString(), name,
                                                 uploader);

    FileStoreActionOutcomeVO outcome = null;
    if (persist) {
      final PortalFile storedFile = getFileDAO().store(portalFile);

      if (storedFile == null || storedFile.getId() == null) {
        log.error("~writeFileToDatabase() : Failed to assign surrogate primary key.");
        outcome = new FileStoreActionOutcomeVO(null, false, "Failed to write file to database");
      } else {
        outcome = new FileStoreActionOutcomeVO(storedFile, true, "Success");
      }
    } else {
      outcome = new FileStoreActionOutcomeVO(portalFile, true, "Success");
    }

    return outcome;
  }

  /**
   * Retrieve flag to indicate if file must be UTF-8 encoded.
   * 
   * @return {@code true} if UTF-8 required, otherwise {@code false}.
   */
  protected boolean isDemandUTF8() {
    return this.demandUTF8;
  }

  /**
   * Assign the flag to determine whether demanding UTF-8.
   * 
   * @param demandUTF8 {@code true} if demanding UTF-8 in files, otherwise {@code false}.
   */
  public void setDemandUTF8(final Boolean demandUTF8) {
    if (demandUTF8 != null) {
      this.demandUTF8 = demandUTF8;
    }
    log.info("~setDemandUTF8() : Demanding UTF-8 '" + this.demandUTF8 + "'.");
  }

  /**
   * Retrieve the expected character set confidence level.
   *  
   * @return Expected character set confidence level.
   */
  protected int getExpectedConfidenceLevel() {
    return expectedConfidenceLevel;
  }

  /**
   * Assign the expected character set confidence level.
   * 
   * @param expectedConfidenceLevel Expected character set confidence level.
   */
  public void setExpectedConfidenceLevel(final Integer expectedConfidenceLevel) {
    if (expectedConfidenceLevel != null) {
      this.expectedConfidenceLevel = expectedConfidenceLevel;
    }
    log.info("~setExpectedConfidenceLevel() : Confidence level '" + this.expectedConfidenceLevel + "'.");
  }

  /**
   * Retrieve the PortalFile DAO.
   * 
   * @return Portal file DAO.
   */
  protected FileDAO getFileDAO() {
    return fileDAO;
  }

  /**
   * Retrieve the file data type.
   * 
   * @return File data type.
   */
  public FILE_DATA_TYPE getDataType() {
    return this.dataType;
  }
}