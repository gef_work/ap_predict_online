/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.controller.util;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import uk.ac.ox.cs.compbio.client_shared.value.security.Role;

/**
 * Utility class for security operations.
 *
 * @author geoff
 */
public class SecurityUtil {

  private static final Log log = LogFactory.getLog(SecurityUtil.class);

  // Hidden constructor.
  private SecurityUtil() {}

  /**
   * Indicate if user has specified roles.
   * 
   * @param roles Roles to check for.
   * @return {@code true} if user has roles, otherwise {@code false}. 
   */
  public static boolean hasRoles(final Set<Role> roles) {
    if (roles == null) {
      return false;
    }

    // Security context retrieved from local thread.
    final SecurityContext context = SecurityContextHolder.getContext();
    if (context == null) {
      return false;
    }

    final Authentication authentication = context.getAuthentication();
    if (authentication == null) {
      return false;
    }

    // Convert the users granted authorities to portal roles.
    final Set<Role> usersRoles = new HashSet<Role>();
    for (final GrantedAuthority usersGrantedAuthority : authentication.getAuthorities()) {
      final String authority = usersGrantedAuthority.getAuthority();
      try {
        usersRoles.add(Role.valueOf(authority));
      } catch (IllegalArgumentException e) {
        log.info("~hasRoles() : Encountered unrecognised granted authority of '" + authority + "'!");
      }
    }

    for (final Role role : roles) {
      log.debug("~hasRoles() : Checking for '" + role + "' in '" + usersRoles + "'.");
      if (!usersRoles.contains(role)) {
        log.debug("~hasRoles() : User didn't have the required role '" + role + "'.");
        return false;
      }
    }

    return true;
  }
}