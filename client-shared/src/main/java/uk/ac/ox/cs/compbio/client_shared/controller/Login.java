/*

  Copyright (c) 2015, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.service.ClientSharedService;

/**
 * MVC Controller handling the login / registration viewing.
 * 
 * @author geoff
 */
@Controller
public class Login {

  // Spring property configuration
  @Value("${recaptcha.public.key}")
  private String recaptchaPublicKey;

  @Autowired @Qualifier(ClientSharedIdentifiers.COMPONENT_CLIENT_SHARED_SERVICE)
  private ClientSharedService clientSharedService;

  private static final Log log = LogFactory.getLog(Login.class);

  /**
   * Request received to show the login page.
   * 
   * @param model UI model.
   * @return Login page name.
   */
  @RequestMapping(method=RequestMethod.GET,
                  value=ClientSharedIdentifiers.VIEW_LOGIN)
  public String showLogin(final Model model) {
    log.debug("~showLogin() : Invoked.");

    setModelPropertiesForLogin(model, recaptchaPublicKey, clientSharedService.isMailerConfigured());

    return ClientSharedIdentifiers.PAGE_LOGIN;
  }

  /**
   * Ensure that the correct model attributes are assigned for when the login page is to be shown.
   * 
   * @param model UI model (for population).
   * @param recaptchaPublicKey reCAPTCHA public key.
   * @param isMailerConfigured {@code true} if emailer is configured, otherwise {@code false}.
   */
  public static void setModelPropertiesForLogin(final Model model,
                                                final String recaptchaPublicKey,
                                                final boolean isMailerConfigured) {
    log.debug("~setModelPropertiesForLogin() : Invoked.");
    model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_PUBLIC_RECAPTCHA_KEY,
                       recaptchaPublicKey);
    model.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MAILER_CONFIGURED,
                       isMailerConfigured);
  }
}