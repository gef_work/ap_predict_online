/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.dao.jpa;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.dao.CSConfigurationDAO;
import uk.ac.ox.cs.compbio.client_shared.entity.Feature;
import uk.ac.ox.cs.compbio.client_shared.value.PortalFeature;
import uk.ac.ox.cs.compbio.client_shared.value.security.Role;

/**
 * client-shared configuration DAO implementation.
 *
 * @author geoff
 */
@Repository(ClientSharedIdentifiers.COMPONENT_CLIENT_SHARED_CONFIGURATION_DAO)
public class CSConfigurationDAOImpl implements CSConfigurationDAO {

  @PersistenceContext
  private EntityManager entityManager;

  private static final Log log = LogFactory.getLog(CSConfigurationDAOImpl.class);

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.compbio.client_shared.dao.CSConfigurationDAO#retrieveRequiredRoles(uk.ac.ox.cs.compbio.client_shared.value.PortalFeature)
   */
  @Override
  public Set<Role> retrieveRequiredRoles(final PortalFeature portalFeature) {
    log.debug("~retrieveRequiredRoles() : Invoked.");

    final Query query = entityManager.createNamedQuery(Feature.QUERY_BY_PORTALFEATURE);

    query.setParameter(Feature.PROPERTY_PORTALFEATURE, portalFeature);

    @SuppressWarnings("unchecked")
    final List<Feature> features = query.getResultList();

    final Set<Role> restrictedRoles = new HashSet<Role>();
    final int featuresFoundCount = features.size();
    switch (featuresFoundCount) {
      case 0 :
        // Unrestricted access!
        break;
      case 1 :
        restrictedRoles.addAll(features.get(0).getRestrictedRoles());
        break;
      default :
        final String errorMessage = portalFeature.toString().concat(" returns '" + featuresFoundCount + "' records, whereas there should only ever be 0 or 1!");
        log.error("~retrieveRequiredRoles() : ".concat(errorMessage));
        throw new IllegalStateException(errorMessage);
    }

    return restrictedRoles;
  }

}
