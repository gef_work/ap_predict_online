/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.business.file;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.value.file.FILE_DATA_TYPE;

/**
 * PK file storage.
 *
 * @author geoff
 */
@Component(ClientSharedIdentifiers.COMPONENT_FILE_STORAGE_PK)
public class FileStoragePK extends FileStorage {

  /** Maximum number of columns - col 1 the time, cols 2-31 the concentrations. */
  // This value is used as a bundle arg in client-direct
  public static final int MAX_ALL_COLUMNS = 31;
  // Count up to this number of problems before barfing!
  private static final int arbitraryMaxProblemCount = 5;

  private static final Log log = LogFactory.getLog(FileStoragePK.class);

  /**
   * Initialising constructor.
   */
  protected FileStoragePK() {
    super.dataType = FILE_DATA_TYPE.PK;
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.compbio.client_shared.business.file.FileStorage#contentCheck(java.io.InputStream)
   */
  @Override
  protected List<String> contentCheck(final InputStream inputStream)
                                      throws IOException {
    log.debug("~contentCheck() : Invoked.");

    final List<String> problems = new ArrayList<String>();
    final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,
                                                                                   UTF8_NAME));
    String line = "";

    int lineCount = 0;
    while ((line = bufferedReader.readLine()) != null) {
      int lineNumber = lineCount + 1;
      final String[] cols = line.split("\t");
      final int colCount = cols.length;
      if (colCount <= 1) {
        problems.add("missing values at line '" + lineNumber + "'");
      } else if (colCount > MAX_ALL_COLUMNS) {
        problems.add("too many concentrations (max. " + (MAX_ALL_COLUMNS - 1) + ") at line '" + lineNumber + "'");
      }

      for (int colIdx = 0; colIdx < colCount; colIdx++) {
        final String colValue = cols[colIdx];
        try {
          new BigDecimal(colValue);
        } catch (NumberFormatException e) {
          problems.add("non-numeric content at line '" + lineNumber + "', col '" + (colIdx + 1) + "'");
        }
      }
      lineCount++;

      if (problems.size() == arbitraryMaxProblemCount) {
        // Without this you may end up with a list of problems a mile long! 
        problems.add("Has reached max. number of reportable problems. Exiting processing!");
        break;
      }
    }

    if (lineCount == 0) {
      problems.add("Empty file content encountered");
    }

    return Collections.unmodifiableList(problems);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.compbio.client_shared.business.file.FileStorage#writeToDatabase()
   */
  @Override
  protected boolean writeToDatabase() {
    return true;
  }
}