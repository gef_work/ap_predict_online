/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.dao.jpa;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertSame;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.compbio.client_shared.dao.FileDAO;
import uk.ac.ox.cs.compbio.client_shared.dao.jpa.FileDAOImpl;
import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;

/**
 * Unit test the portal File DAO.
 *
 * @author geoff
 */
public class FileDAOImplTest {

  private EntityManager mockEntityManager;
  private FileDAO fileDAO;
  private IMocksControl mocksControl;

  @Before
  public void setUp() {
    fileDAO = new FileDAOImpl();

    mocksControl = createStrictControl();
    mockEntityManager = mocksControl.createMock(EntityManager.class);

    ReflectionTestUtils.setField(fileDAO, "entityManager", mockEntityManager);
  }

  @Test
  public void testFindPortalFiles() {
    final Query mockQuery = mocksControl.createMock(Query.class);
    expect(mockEntityManager.createNamedQuery(PortalFile.QUERY_PORTALFILE_ALL))
          .andReturn(mockQuery);
    final List<PortalFile> dummyPortalFiles = new ArrayList<PortalFile>();
    expect(mockQuery.getResultList()).andReturn(dummyPortalFiles);

    mocksControl.replay();

    final List<PortalFile> retrieved = fileDAO.findPortalFiles();

    mocksControl.verify();

    assertSame(dummyPortalFiles, retrieved);
  }

  @Test
  public void testStore() {
    /*
     * Transient entity store.
     */
    PortalFile mockPortalFile = mocksControl.createMock(PortalFile.class);
    expect(mockPortalFile.getId()).andReturn(null);
    mockEntityManager.persist(mockPortalFile);

    mocksControl.replay();

    PortalFile retrieved = fileDAO.store(mockPortalFile);

    mocksControl.verify();

    assertSame(mockPortalFile, retrieved);

    mocksControl.reset();

    /*
     * Entity merge
     */
    mockPortalFile = mocksControl.createMock(PortalFile.class);
    final Long dummyFileId = 4l;
    expect(mockPortalFile.getId()).andReturn(dummyFileId);
    expect(mockEntityManager.merge(mockPortalFile)).andReturn(mockPortalFile);

    mocksControl.replay();

    retrieved = fileDAO.store(mockPortalFile);

    mocksControl.verify();

    assertSame(mockPortalFile, retrieved);
  }
}