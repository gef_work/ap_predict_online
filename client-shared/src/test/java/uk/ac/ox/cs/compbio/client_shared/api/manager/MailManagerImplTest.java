/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.api.manager;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.api.manager.MailManager;
import uk.ac.ox.cs.compbio.client_shared.api.manager.MailManagerImpl;
import uk.ac.ox.cs.compbio.client_shared.api.manager.MailManager.EMAIL_TYPE;

/**
 * Unit test the mail manager implementation.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { MailManagerImpl.class } )
public class MailManagerImplTest {

  private IMocksControl mocksControl;
  private MailManager mailManager;
  private JavaMailSenderImpl mockMailSender;
  private SimpleMailMessage mockSimpleMailMessage;
  private String dummyFrom;
  private String dummyHost;
  private String[] dummyTo;
  private int dummyPort;

  @Before
  public void setUp() {
    mailManager = new MailManagerImpl();

    mocksControl = createStrictControl();
    mockMailSender = mocksControl.createMock(JavaMailSenderImpl.class);
    mockSimpleMailMessage = mocksControl.createMock(SimpleMailMessage.class);
    ReflectionTestUtils.setField(mailManager, ClientSharedIdentifiers.COMPONENT_MAIL_SENDER,
                                 mockMailSender);
    ReflectionTestUtils.setField(mailManager, ClientSharedIdentifiers.COMPONENT_MAIL_REGISTRATION_TEMPLATE,
                                 mockSimpleMailMessage);

    dummyFrom = null;
    dummyHost = null;
    dummyTo = null;
    dummyPort = -1;
  }

  @Test
  public void testMailManagerInit() {
    final MailManagerImpl mailManagerImpl = (MailManagerImpl) mailManager;

    expect(mockMailSender.getHost()).andReturn(dummyHost);
    expect(mockMailSender.getPort()).andReturn(dummyPort);
    expect(mockSimpleMailMessage.getFrom()).andReturn(dummyFrom);
    expect(mockSimpleMailMessage.getTo()).andReturn(dummyTo);

    mocksControl.replay();

    mailManagerImpl.mailManagerInit();

    mocksControl.verify();
    assertFalse(mailManagerImpl.isMailerConfigured());

    mocksControl.reset();

    dummyHost = "dummyHost";
    expect(mockMailSender.getHost()).andReturn(dummyHost);
    expect(mockMailSender.getPort()).andReturn(dummyPort);
    expect(mockSimpleMailMessage.getFrom()).andReturn(dummyFrom);
    expect(mockSimpleMailMessage.getTo()).andReturn(dummyTo);

    mocksControl.replay();

    mailManagerImpl.mailManagerInit();

    mocksControl.verify();
    assertFalse(mailManagerImpl.isMailerConfigured());

    mocksControl.reset();

    expect(mockMailSender.getHost()).andReturn(dummyHost);
    dummyPort = 65536;
    expect(mockMailSender.getPort()).andReturn(dummyPort);
    expect(mockSimpleMailMessage.getFrom()).andReturn(dummyFrom);
    expect(mockSimpleMailMessage.getTo()).andReturn(dummyTo);

    mocksControl.replay();

    mailManagerImpl.mailManagerInit();

    mocksControl.verify();
    assertFalse(mailManagerImpl.isMailerConfigured());

    mocksControl.reset();

    expect(mockMailSender.getHost()).andReturn(dummyHost);
    dummyPort = 1;
    expect(mockMailSender.getPort()).andReturn(dummyPort);
    expect(mockSimpleMailMessage.getFrom()).andReturn(dummyFrom);
    expect(mockSimpleMailMessage.getTo()).andReturn(dummyTo);

    mocksControl.replay();

    mailManagerImpl.mailManagerInit();

    mocksControl.verify();
    assertFalse(mailManagerImpl.isMailerConfigured());

    mocksControl.reset();

    expect(mockMailSender.getHost()).andReturn(dummyHost);
    expect(mockMailSender.getPort()).andReturn(dummyPort);
    dummyFrom = "dummyFrom";
    expect(mockSimpleMailMessage.getFrom()).andReturn(dummyFrom);
    expect(mockSimpleMailMessage.getTo()).andReturn(dummyTo);

    mocksControl.replay();

    mailManagerImpl.mailManagerInit();

    mocksControl.verify();
    assertFalse(mailManagerImpl.isMailerConfigured());

    mocksControl.reset();

    expect(mockMailSender.getHost()).andReturn(dummyHost);
    expect(mockMailSender.getPort()).andReturn(dummyPort);
    dummyFrom = null;
    expect(mockSimpleMailMessage.getFrom()).andReturn(dummyFrom);
    dummyTo = new String[] { "dummyTo" };
    expect(mockSimpleMailMessage.getTo()).andReturn(dummyTo);

    mocksControl.replay();

    mailManagerImpl.mailManagerInit();

    mocksControl.verify();
    assertFalse(mailManagerImpl.isMailerConfigured());

    mocksControl.reset();

    expect(mockMailSender.getHost()).andReturn(dummyHost);
    expect(mockMailSender.getPort()).andReturn(dummyPort);
    dummyFrom = "dummyFrom";
    expect(mockSimpleMailMessage.getFrom()).andReturn(dummyFrom);
    expect(mockSimpleMailMessage.getTo()).andReturn(dummyTo);

    mocksControl.replay();

    mailManagerImpl.mailManagerInit();

    mocksControl.verify();
    assertTrue(mailManagerImpl.isMailerConfigured());
  }

  @Test
  public void testMailerConfigured() {
    assertFalse(mailManager.isMailerConfigured());
  }

  @Test
  public void testSendEmail() throws Exception {
    String dummyAddress = "dummyAddress";
    EMAIL_TYPE dummyEmailType = EMAIL_TYPE.REGISTRATION;

    expectNew(SimpleMailMessage.class, isA(SimpleMailMessage.class)).
              andReturn(mockSimpleMailMessage);
    final Capture<String> captureText = newCapture();
    mockSimpleMailMessage.setText(capture(captureText));
    mockMailSender.send(mockSimpleMailMessage);

    replayAll();
    mocksControl.replay();

    mailManager.sendEmail(dummyAddress, dummyEmailType);
    assertTrue(captureText.getValue().contains(dummyAddress));

    verifyAll();
    mocksControl.verify();
  }
}