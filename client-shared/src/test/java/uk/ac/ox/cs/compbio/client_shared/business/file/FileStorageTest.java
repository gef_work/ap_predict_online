/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.business.file;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.isA;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.expectLastCall;
import static org.powermock.api.easymock.PowerMock.expectNew;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.dao.FileDAO;
import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.compbio.client_shared.value.file.FileStoreActionOutcomeVO;

/**
 * Unit test the file storage.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ FileStorage.class, ByteArrayOutputStream.class,
                  IOUtils.class })
public class FileStorageTest {

  private class FakeFileStorage extends FileStorage {
    protected List<String> contentCheck(final InputStream inputStream)
                                        throws IOException {
      return new ArrayList<String>();
    }

    @Override
    protected boolean writeToDatabase() {
      return true;
    }
  }

  private class FakeFileStorageWithProblems extends FileStorage {
    protected List<String> contentCheck(final InputStream inputStream)
                                        throws IOException {
      final List<String> problems = new ArrayList<String>();
      problems.add(dummyProblem);
      return problems;
    }

    @Override
    protected boolean writeToDatabase() {
      return true;
    }
  }

  private class FakeFileStorageExceptionThrower extends FileStorage {
    protected List<String> contentCheck(final InputStream inputStream)
                                        throws IOException {
      throw new IOException(dummyIOException);
    }

    @Override
    protected boolean writeToDatabase() {
      return true;
    }
  }

  private class FakeFireStorageNoPersist extends FileStorage {
    protected List<String> contentCheck(InputStream inputStream)
                           throws IOException {
      return new ArrayList<String>();
    }

    protected boolean writeToDatabase() {
      return false;
    }
  }

  private static final int defaultExpectedConfidenceLevel = 90;
  private static final int dummyIOUtilsCopyInt = 4;
  private static final String dummyIOException = "dummyIOException";
  private static final String dummyName = "dummyName";
  private static final String dummyProblem = "dummyProblem";
  private static final String dummyUploader = "dummyUploader";
  private IMocksControl mocksControl;
  private InputStream mockInputStream;
  private FileDAO mockFileDAO;
  private FileStorage fakeFileStorage;
  private FileStoreActionOutcomeVO mockFileStoreActionOutcomeVO;

  @Before
  public void setUp() {
    fakeFileStorage = new FakeFileStorage();

    mocksControl = createStrictControl();
    mockInputStream = mocksControl.createMock(InputStream.class);
    mockFileDAO = mocksControl.createMock(FileDAO.class);
    mockFileStoreActionOutcomeVO = mocksControl.createMock(FileStoreActionOutcomeVO.class);

    ReflectionTestUtils.setField(fakeFileStorage,
                                 ClientSharedIdentifiers.COMPONENT_FILE_DAO,
                                 mockFileDAO);
  }

  @Test
  public void testGettersSetters() {
    assertSame(defaultExpectedConfidenceLevel,
               fakeFileStorage.getExpectedConfidenceLevel());
    assertSame(mockFileDAO, fakeFileStorage.getFileDAO());
    assertFalse(fakeFileStorage.isDemandUTF8());
    assertNull(fakeFileStorage.getDataType());

    fakeFileStorage.setExpectedConfidenceLevel(null);
    assertSame(defaultExpectedConfidenceLevel,
               fakeFileStorage.getExpectedConfidenceLevel());

    final Integer dummyExpectedConfidenceLevel = 3;
    fakeFileStorage.setExpectedConfidenceLevel(dummyExpectedConfidenceLevel);
    assertSame(dummyExpectedConfidenceLevel,
               fakeFileStorage.getExpectedConfidenceLevel());

    fakeFileStorage.setDemandUTF8(null);
    assertFalse(fakeFileStorage.isDemandUTF8());

    fakeFileStorage.setDemandUTF8(true);
    assertTrue(fakeFileStorage.isDemandUTF8());
  }

  /*
   * Avoiding making these part of the official testing as they're hitting
   * the CharsetDetector and CharsetMatch classes!
   */
  //@Test
  public void testNonUTF8() {
    fakeFileStorage.setDemandUTF8(true);

    final String dummyString = "怎麼現在棕色母牛 她在海邊賣海貝殼";
    final String infoPrefix = "Confidence '" + defaultExpectedConfidenceLevel + "' of 'UTF-8' encoding required, but encountered confidence";
    /*
     * Test used ISO-8859-1 encoded input.
     */
    InputStream dummyInputStream = new ByteArrayInputStream(dummyString.getBytes(StandardCharsets.ISO_8859_1));

    FileStoreActionOutcomeVO outcome = fakeFileStorage.storeFile(dummyInputStream,
                                                                 dummyName,
                                                                 dummyUploader);


    assertNull(outcome.getPortalFile());
    assertFalse(outcome.isSuccess());
    assertEquals(infoPrefix.concat(" '15' of 'UTF-8'."),
                 outcome.getInformation());

    /*
     * Test used UTF-16 encoded input.
     */
    dummyInputStream = new ByteArrayInputStream(dummyString.getBytes(StandardCharsets.UTF_16));

    outcome = fakeFileStorage.storeFile(dummyInputStream, dummyName,
                                        dummyUploader);

    assertNull(outcome.getPortalFile());
    assertFalse(outcome.isSuccess());
    assertEquals(infoPrefix.concat(" '100' of 'UTF-16BE'."),
                 outcome.getInformation());
  }

  @Test
  public void testIsDemandUTF8() throws Exception {
    fakeFileStorage.setDemandUTF8(true);

    final String dummyString = "怎麼現在棕色母牛 她在海邊賣海貝殼";

    /*
     * Test used UTF-8 encoded input.
     */
    InputStream dummyInputStream = new ByteArrayInputStream(dummyString.getBytes(StandardCharsets.UTF_8));

    // >> writeFileToDatabase()
    final PortalFile mockPortalFile = mocksControl.createMock(PortalFile.class); 
    expectNew(PortalFile.class, isA(String.class), isA(String.class),
                                isA(String.class))
             .andReturn(mockPortalFile);
    final PortalFile mockStoredFile = mocksControl.createMock(PortalFile.class); 
    expect(mockFileDAO.store(isA(PortalFile.class))).andReturn(mockStoredFile);
    final Long dummyStoredFileId = 4L;
    expect(mockStoredFile.getId()).andReturn(dummyStoredFileId);
    Capture<PortalFile> capturePortalFile = newCapture();
    Capture<String> captureInformation = newCapture();
    expectNew(FileStoreActionOutcomeVO.class, capture(capturePortalFile),
                                              eq(true),
                                              capture(captureInformation))
              .andStubReturn(mockFileStoreActionOutcomeVO);
    // << writeFileToDatabase()

    replayAll();
    mocksControl.replay();

    FileStoreActionOutcomeVO outcome = fakeFileStorage.storeFile(dummyInputStream,
                                                                 dummyName,
                                                                 dummyUploader);

    verifyAll();
    mocksControl.verify();

    assertSame(mockFileStoreActionOutcomeVO, outcome);
    PortalFile capturedPortalFile = capturePortalFile.getValue();
    String capturedInformation = captureInformation.getValue();
    assertSame(mockStoredFile, capturedPortalFile);
    assertEquals("Success", capturedInformation);
  }

  @Test
  public void testStoreFileMinimal() throws Exception {
    mockStatic(IOUtils.class);
    expect(IOUtils.copy(isA(InputStream.class),
                        isA(ByteArrayOutputStream.class)))
          .andReturn(dummyIOUtilsCopyInt);
    // >> writeFileToDatabase()
    IOUtils.copy(isA(InputStream.class), isA(StringWriter.class),
                 isA(String.class));
    expectLastCall();
    final PortalFile mockPortalFile = mocksControl.createMock(PortalFile.class);
    expectNew(PortalFile.class, isA(String.class), isA(String.class),
                                isA(String.class))
             .andReturn(mockPortalFile);
    final PortalFile mockStoredFile = mocksControl.createMock(PortalFile.class); 
    expect(mockFileDAO.store(mockPortalFile)).andReturn(mockStoredFile);
    expect(mockStoredFile.getId()).andReturn(null);
    Capture<PortalFile> capturePortalFile = newCapture();
    Capture<String> captureInformation = newCapture();
    expectNew(FileStoreActionOutcomeVO.class, capture(capturePortalFile),
                                              eq(false),
                                              capture(captureInformation))
              .andStubReturn(mockFileStoreActionOutcomeVO);
    // << writeFileToDatabase()

    replayAll();
    mocksControl.replay();

    FileStoreActionOutcomeVO outcome = fakeFileStorage.storeFile(mockInputStream,
                                                                 dummyName,
                                                                 dummyUploader);

    verifyAll();
    mocksControl.verify();

    assertSame(mockFileStoreActionOutcomeVO, outcome);
    PortalFile capturedPortalFile = capturePortalFile.getValue();
    String capturedInformation = captureInformation.getValue();
    assertNull(capturedPortalFile);
    assertEquals("Failed to write file to database", capturedInformation);
  }

  @Test
  public void testStoreFileMinimalWithExceptionsOrProblems() throws Exception {
    /*
     * Fail on first IOUtils.copy
     */
    mockStatic(IOUtils.class);
    expect(IOUtils.copy(isA(InputStream.class), isA(ByteArrayOutputStream.class)))
          .andThrow(new IOException(dummyIOException));
    Capture<PortalFile> capturePortalFile = newCapture();
    Capture<String> captureInformation = newCapture();
    expectNew(FileStoreActionOutcomeVO.class, capture(capturePortalFile),
                                              eq(false),
                                              capture(captureInformation))
              .andStubReturn(mockFileStoreActionOutcomeVO);

    replayAll();
    mocksControl.replay();

    FileStoreActionOutcomeVO outcome = fakeFileStorage.storeFile(mockInputStream,
                                                                 dummyName,
                                                                 dummyUploader);

    verifyAll();
    mocksControl.verify();

    assertSame(mockFileStoreActionOutcomeVO, outcome);
    PortalFile capturedPortalFile = capturePortalFile.getValue();
    String capturedInformation = captureInformation.getValue();
    assertNull(capturedPortalFile);
    assertEquals("Error copying file internals '" + dummyIOException + "'.",
                 capturedInformation);

    resetAll();
    mocksControl.reset();

    /*
     * Exception on content checking
     */
    expect(IOUtils.copy(isA(InputStream.class), isA(ByteArrayOutputStream.class)))
          .andReturn(dummyIOUtilsCopyInt);
    // >> contentCheck()
    // Throws Exception!
    // << contentCheck()
    capturePortalFile = newCapture();
    captureInformation = newCapture();
    expectNew(FileStoreActionOutcomeVO.class, capture(capturePortalFile),
                                              eq(false),
                                              capture(captureInformation))
              .andStubReturn(mockFileStoreActionOutcomeVO);

    replayAll();
    mocksControl.replay();

    outcome = new FakeFileStorageExceptionThrower().storeFile(mockInputStream,
                                                              dummyName,
                                                              dummyUploader);

    verifyAll();
    mocksControl.verify();

    capturedPortalFile = capturePortalFile.getValue();
    capturedInformation = captureInformation.getValue();
    assertNull(capturedPortalFile);
    assertEquals("Error reading uploaded file '" + dummyIOException + "'",
                 capturedInformation);

    resetAll();
    mocksControl.reset();

    /*
     * Problems on content checking
     */
    expect(IOUtils.copy(isA(InputStream.class), isA(ByteArrayOutputStream.class)))
                  .andReturn(dummyIOUtilsCopyInt);
    // >> contentCheck()
    // Returns problems
    // << contentCheck()
    capturePortalFile = newCapture();
    captureInformation = newCapture();
    expectNew(FileStoreActionOutcomeVO.class, capture(capturePortalFile),
                                              eq(false),
                                              capture(captureInformation))
              .andStubReturn(mockFileStoreActionOutcomeVO);

    replayAll();
    mocksControl.replay();

    outcome = new FakeFileStorageWithProblems().storeFile(mockInputStream,
                                                          dummyName,
                                                          dummyUploader);


    verifyAll();
    mocksControl.verify();

    capturedPortalFile = capturePortalFile.getValue();
    capturedInformation = captureInformation.getValue();
    assertNull(capturedPortalFile);
    assertEquals("Uploaded file had " + dummyProblem, capturedInformation);

    resetAll();
    mocksControl.reset();

    /*
     * Exception writing to database
     */
    expect(IOUtils.copy(isA(InputStream.class), isA(ByteArrayOutputStream.class)))
          .andReturn(dummyIOUtilsCopyInt);
    // >> writeFileToDatabase()
    IOUtils.copy(isA(InputStream.class), isA(StringWriter.class),
                 isA(String.class));
    expectLastCall().andThrow(new IOException(dummyIOException));
    capturePortalFile = newCapture();
    captureInformation = newCapture();
    expectNew(FileStoreActionOutcomeVO.class, capture(capturePortalFile),
                                              eq(false),
                                              capture(captureInformation))
              .andStubReturn(mockFileStoreActionOutcomeVO);
    // << writeFileToDatabase()

    replayAll();
    mocksControl.replay();

    fakeFileStorage = new FakeFileStorage();
    outcome = fakeFileStorage.storeFile(mockInputStream, dummyName,
                                        dummyUploader);

    verifyAll();
    mocksControl.verify();

    capturedPortalFile = capturePortalFile.getValue();
    capturedInformation = captureInformation.getValue();
    assertNull(capturedPortalFile);
    assertEquals("Error copying file to text stream for database persistence '" + dummyIOException + "'.",
                 capturedInformation);
  }

  @Test
  public void testStoreFileNoPersist() throws Exception {
    mockStatic(IOUtils.class);
    expect(IOUtils.copy(isA(InputStream.class),
                        isA(ByteArrayOutputStream.class)))
          .andReturn(dummyIOUtilsCopyInt);
    // >> writeFileToDatabase()
    IOUtils.copy(isA(InputStream.class), isA(StringWriter.class),
                 isA(String.class));
    expectLastCall();
    final PortalFile mockPortalFile = mocksControl.createMock(PortalFile.class);
    expectNew(PortalFile.class, isA(String.class), isA(String.class),
                                isA(String.class))
             .andReturn(mockPortalFile);
    expectNew(FileStoreActionOutcomeVO.class, mockPortalFile, true, "Success")
             .andStubReturn(mockFileStoreActionOutcomeVO);
    // << writeFileToDatabase()

    replayAll();
    mocksControl.replay();

    FileStoreActionOutcomeVO outcome = new FakeFireStorageNoPersist().storeFile(mockInputStream,
                                                                                dummyName,
                                                                                dummyUploader);

    verifyAll();
    mocksControl.verify();

    assertSame(mockFileStoreActionOutcomeVO, outcome);
  }
}