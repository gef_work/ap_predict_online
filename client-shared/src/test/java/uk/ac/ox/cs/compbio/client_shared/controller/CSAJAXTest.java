/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.controller;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.Locale;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.Model;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.api.exception.DownstreamCommunicationException;
import uk.ac.ox.cs.compbio.client_shared.controller.CSAJAX.JSON;
import uk.ac.ox.cs.compbio.client_shared.service.ClientSharedService;

/**
 * Unit test the AJAX Controller.
 * 
 * @author geoff
 */
public class CSAJAXTest {

  private CSAJAX ajaxController;
  private ClientSharedService mockClientSharedService;
  private IMocksControl mocksControl;
  private Locale fakeLocale;
  private MessageSource mockMessageSource;
  private Model mockModel;

  @Before
  public void setUp() {
    ajaxController = new CSAJAX();
    mocksControl = createStrictControl();
    mockClientSharedService = mocksControl.createMock(ClientSharedService.class);
    ReflectionTestUtils.setField(ajaxController,
                                 ClientSharedIdentifiers.COMPONENT_CLIENT_SHARED_SERVICE,
                                 mockClientSharedService);
    mockModel = mocksControl.createMock(Model.class);
    mockMessageSource = mocksControl.createMock(MessageSource.class);
    ajaxController.setMessageSource(mockMessageSource);

    fakeLocale = new Locale("en");
  }

  @Test
  public void testJSONClass() {
    String jsonStr = null;
    String exceptionStr = null;

    JSON json = ajaxController.new JSON(jsonStr, exceptionStr);
    assertNull(json.getJson());
    assertNull(json.getException());

    jsonStr = "jsonStr";
    exceptionStr = "exceptionStr";
    json = ajaxController.new JSON(jsonStr, exceptionStr);
    assertTrue(jsonStr.equals(json.getJson()));
    assertTrue(exceptionStr.equals(json.getException()));
  }

  @Test
  public void testAppManagerWorkloadRetrieval() throws DownstreamCommunicationException {
    final String dummyWorkload = "dummyWorkload";
    expect(mockClientSharedService.retrieveAppManagerWorkload()).andReturn(dummyWorkload);
    final Capture<String> captureMessageKey = newCapture();
    final Capture<JSON> captureJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureMessageKey), capture(captureJSON)))
          .andReturn(mockModel);

    mocksControl.replay();

    final String workload = ajaxController.retrieveAppManagerWorkload(fakeLocale, mockModel);

    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, workload);

    final String capturedMessageKey = captureMessageKey.getValue();
    final JSON capturedJSON = captureJSON.getValue();
    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_WORKLOAD, capturedMessageKey);
    assertNotNull(dummyWorkload, capturedJSON.getJson());
    assertNull(capturedJSON.getException());
  }

  @Test
  public void testAppManagerWorkloadRetrievalExceptionHandling() throws DownstreamCommunicationException {
    final String dummyExceptionMessage = "dummyExceptionMessage";
    expect(mockClientSharedService.retrieveAppManagerWorkload())
          .andThrow(new DownstreamCommunicationException(dummyExceptionMessage));
    Capture<String> captureMessageKey = newCapture();
    Capture<String[]> captureArgs = newCapture();
    Capture<Locale> captureLocale = newCapture();
    expect(mockMessageSource.getMessage(capture(captureMessageKey), capture(captureArgs), 
                                        capture(captureLocale)))
          .andThrow(new NoSuchMessageException(null)).atLeastOnce();

    Capture<String> captureModelAttrName = newCapture();
    Capture<JSON> captureModelAttrJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureModelAttrName), capture(captureModelAttrJSON)))
          .andReturn(mockModel);

    mocksControl.replay();

    String viewName = ajaxController.retrieveAppManagerWorkload(fakeLocale, mockModel);

    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);
    String capturedMessageKey = captureMessageKey.getValue();
    Object[] capturedArgs = captureArgs.getValue();
    Locale capturedLocale = captureLocale.getValue();
    assertEquals(ClientSharedIdentifiers.BUNDLE_ID_ERROR_WORKLOAD_RETRIEVAL, capturedMessageKey);
    assertEquals((String) capturedArgs[0], dummyExceptionMessage);
    assertSame(fakeLocale, capturedLocale);

    String capturedModelAttrName = captureModelAttrName.getValue();
    JSON capturedModelAttrJSON = captureModelAttrJSON.getValue();
    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_WORKLOAD, capturedModelAttrName);
    assertNull(capturedModelAttrJSON.getJson());
    assertNotNull(capturedModelAttrJSON.getException());

    mocksControl.reset();

    expect(mockClientSharedService.retrieveAppManagerWorkload())
          .andThrow(new RuntimeException(dummyExceptionMessage));
    captureMessageKey = newCapture();
    captureArgs = newCapture();
    captureLocale = newCapture();
    expect(mockMessageSource.getMessage(capture(captureMessageKey), capture(captureArgs), 
                                        capture(captureLocale)))
          .andThrow(new NoSuchMessageException(null)).atLeastOnce();

    captureModelAttrName = newCapture();
    captureModelAttrJSON = newCapture();
    expect(mockModel.addAttribute(capture(captureModelAttrName), capture(captureModelAttrJSON)))
          .andReturn(mockModel);

    mocksControl.replay();

    viewName = ajaxController.retrieveAppManagerWorkload(fakeLocale, mockModel);

    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.VIEW_NAME_JSON, viewName);
    capturedMessageKey = captureMessageKey.getValue();
    capturedArgs = captureArgs.getValue();
    capturedLocale = captureLocale.getValue();
    assertEquals(ClientSharedIdentifiers.BUNDLE_ID_ERROR_UNEXPECTED, capturedMessageKey);
    assertEquals((String) capturedArgs[0], dummyExceptionMessage);
    assertSame(fakeLocale, capturedLocale);

    capturedModelAttrName = captureModelAttrName.getValue();
    capturedModelAttrJSON = captureModelAttrJSON.getValue();
    assertEquals(ClientSharedIdentifiers.MODEL_ATTRIBUTE_WORKLOAD, capturedModelAttrName);
    assertNull(capturedModelAttrJSON.getJson());
    assertNotNull(capturedModelAttrJSON.getException());
  }
}