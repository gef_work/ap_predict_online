/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import uk.ac.ox.cs.compbio.client_shared.value.PortalFeature;
import uk.ac.ox.cs.compbio.client_shared.value.security.Role;

/**
 * Unit test the {@code Feature} entity.
 *
 * @author geoff
 */
public class FeatureTest {

  @Test
  public void testEverything() {
    /*
     * Null feature arg throws exception
     */
    PortalFeature dummyPortalFeature = null;
    Set<Role> dummyRestrictedRoles1 = null;

    Feature feature = null;

    try {
      new Feature(dummyPortalFeature, dummyRestrictedRoles1);
      fail("Should not permit null portal feature arg!");
    } catch (IllegalArgumentException e) {}

    /*
     * Null restricted roles isn't a problem.
     */
    dummyPortalFeature = PortalFeature.UNCERTAINTIES;

    feature = new Feature(dummyPortalFeature, dummyRestrictedRoles1);

    assertSame(dummyPortalFeature, feature.getPortalFeature());
    assertTrue(feature.getRestrictedRoles().isEmpty());
    assertTrue(feature.accessible(null));
    assertNotNull(feature.toString());

    /*
     * Modification of returned roles isn't allowed.
     */
    final Role dummyRole1 = Role.ROLE_USER;
    try {
      feature.getRestrictedRoles().add(dummyRole1);
      fail("Should not permit modification of returned roles");
    } catch (UnsupportedOperationException e) {}

    /*
     * Set restricted roles and check accessibility
     */
    final Role dummyRole2 = Role.ROLE_POWER_USER;
    final Set<Role> dummyRestrictedRoles2 = new HashSet<Role>();
    dummyRestrictedRoles2.add(dummyRole1);
    dummyRestrictedRoles2.add(dummyRole2);
    feature.setUserRoles(dummyRestrictedRoles2);

    assertEquals(dummyRestrictedRoles2, feature.getRestrictedRoles());

    final Set<Role> dummyUserRoles = new HashSet<Role>();
    dummyUserRoles.add(dummyRole1);

    assertFalse(feature.accessible(dummyUserRoles));

    dummyUserRoles.add(dummyRole2);

    assertTrue(feature.accessible(dummyUserRoles));

    final Role dummyRole3 = Role.ROLE_ADMIN;
    dummyUserRoles.add(dummyRole3);

    assertTrue(feature.accessible(dummyUserRoles));
    assertNotNull(feature.toString());
    assertFalse(feature.getRestrictedRoles().isEmpty());

    dummyRestrictedRoles1 = new HashSet<Role>();
    dummyRestrictedRoles1.add(dummyRole1);
    feature = new Feature(dummyPortalFeature, dummyRestrictedRoles1);
    assertFalse(feature.getRestrictedRoles().isEmpty());

    dummyUserRoles.clear();
    feature.setUserRoles(dummyUserRoles);
    assertTrue(feature.getRestrictedRoles().isEmpty());

    feature.setUserRoles(null);
    assertTrue(feature.getRestrictedRoles().isEmpty());
  }
}