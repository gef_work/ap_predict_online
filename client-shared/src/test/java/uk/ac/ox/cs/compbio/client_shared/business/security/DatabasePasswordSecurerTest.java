/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.business.security;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;

/**
 * Unit test the database password securer.
 *
 * @author geoff
 */
public class DatabasePasswordSecurerTest {

  private DatabasePasswordSecurer pwSecurer;
  private IMocksControl mocksControl;
  private JdbcTemplate mockJdbcTemplate;

  @Before
  public void setUp() {
    pwSecurer = new DatabasePasswordSecurer();

    mocksControl = createStrictControl();
    mockJdbcTemplate = mocksControl.createMock(JdbcTemplate.class);

    pwSecurer.setJdbcTemplate(mockJdbcTemplate);
  }

  @Test
  public void testEncoding() {
    final Capture<String> capturedSQL = newCapture();
    final Capture<RowCallbackHandler> capturedRCH = newCapture();
    mockJdbcTemplate.query(capture(capturedSQL), capture(capturedRCH));

    mocksControl.replay();

    pwSecurer.encodePasswords();

    mocksControl.verify();

    assertEquals(DatabasePasswordSecurer.DEF_USERS, capturedSQL.getValue());

    // TODO : Integration test for better testing.
  }
}