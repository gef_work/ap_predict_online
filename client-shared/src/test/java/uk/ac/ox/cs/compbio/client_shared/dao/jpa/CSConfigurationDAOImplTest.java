/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.dao.jpa;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.compbio.client_shared.dao.CSConfigurationDAO;
import uk.ac.ox.cs.compbio.client_shared.entity.Feature;
import uk.ac.ox.cs.compbio.client_shared.value.PortalFeature;
import uk.ac.ox.cs.compbio.client_shared.value.security.Role;

/**
 * Unit test the client-shared configuration DAO.
 *
 * @author geoff
 */
public class CSConfigurationDAOImplTest {

  private CSConfigurationDAO csConfigurationDAO;
  private EntityManager mockEntityManager;
  private IMocksControl mocksControl;

  @Before
  public void setUp() {
    csConfigurationDAO = new CSConfigurationDAOImpl();

    mocksControl = createStrictControl();
    mockEntityManager = mocksControl.createMock(EntityManager.class);

    ReflectionTestUtils.setField(csConfigurationDAO, "entityManager",
                                 mockEntityManager);
  }

  @Test
  public void testFindPortalFiles() {
    /*
     * Test unrestricted access.
     */
    final PortalFeature dummyPortalFeature = PortalFeature.UNCERTAINTIES;
    final Query mockQuery = mocksControl.createMock(Query.class);
    expect(mockEntityManager.createNamedQuery(Feature.QUERY_BY_PORTALFEATURE))
          .andReturn(mockQuery);
    expect(mockQuery.setParameter(Feature.PROPERTY_PORTALFEATURE,
                                  dummyPortalFeature))
          .andReturn(mockQuery);
    final List<Feature> dummyFeatures = new ArrayList<Feature>();
    expect(mockQuery.getResultList()).andReturn(dummyFeatures);

    mocksControl.replay();

    Set<Role> retrieved = csConfigurationDAO.retrieveRequiredRoles(dummyPortalFeature);

    mocksControl.verify();

    assertEquals(0, retrieved.size());

    mocksControl.reset();

    /*
     * Test restricted access.
     */
    expect(mockEntityManager.createNamedQuery(Feature.QUERY_BY_PORTALFEATURE))
          .andReturn(mockQuery);
    expect(mockQuery.setParameter(Feature.PROPERTY_PORTALFEATURE,
                                  dummyPortalFeature))
          .andReturn(mockQuery);
    final Feature mockFeature1 = mocksControl.createMock(Feature.class);
    dummyFeatures.add(mockFeature1);
    expect(mockQuery.getResultList()).andReturn(dummyFeatures);
    final Set<Role> dummyRestrictedRoles = new HashSet<Role>();
    final Role dummyRestrictedRole = Role.ROLE_USER;
    dummyRestrictedRoles.add(dummyRestrictedRole);
    expect(mockFeature1.getRestrictedRoles()).andReturn(dummyRestrictedRoles);

    mocksControl.replay();

    retrieved = csConfigurationDAO.retrieveRequiredRoles(dummyPortalFeature);

    mocksControl.verify();

    assertEquals(dummyRestrictedRoles, retrieved);

    mocksControl.reset();

    /*
     * Illegal state exception!
     */
    expect(mockEntityManager.createNamedQuery(Feature.QUERY_BY_PORTALFEATURE))
          .andReturn(mockQuery);
    expect(mockQuery.setParameter(Feature.PROPERTY_PORTALFEATURE,
                                  dummyPortalFeature))
          .andReturn(mockQuery);
    final Feature mockFeature2 = mocksControl.createMock(Feature.class);
    dummyFeatures.add(mockFeature2);
    expect(mockQuery.getResultList()).andReturn(dummyFeatures);

    mocksControl.replay();

    try {
      csConfigurationDAO.retrieveRequiredRoles(dummyPortalFeature);
      fail("Should not permit retrieval of multiple features by name!");
    } catch (IllegalStateException e) {}

    mocksControl.verify();
  }

}