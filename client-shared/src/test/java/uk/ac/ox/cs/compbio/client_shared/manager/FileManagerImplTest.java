/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.manager;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.partialMockBuilder;
import static org.junit.Assert.assertSame;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.business.file.FileStorageCellML;
import uk.ac.ox.cs.compbio.client_shared.business.file.FileStoragePK;
import uk.ac.ox.cs.compbio.client_shared.dao.FileDAO;
import uk.ac.ox.cs.compbio.client_shared.entity.PortalFile;
import uk.ac.ox.cs.compbio.client_shared.value.file.FILE_DATA_TYPE;
import uk.ac.ox.cs.compbio.client_shared.value.file.FileStoreActionOutcomeVO;

/**
 * File manager implementation unit test.
 *
 * @author geoff
 */
public class FileManagerImplTest {

  private FileDAO mockFileDAO;
  private FileManager fileManager;
  private FileStorageCellML mockFileStorageCellML;
  private FileStoragePK mockFileStoragePK;
  private IMocksControl mocksControl;

  @Before
  public void setUp() {
    fileManager = new FileManagerImpl();

    mocksControl = createStrictControl();
    mockFileDAO = mocksControl.createMock(FileDAO.class);

    mockFileStorageCellML = partialMockBuilder(FileStorageCellML.class)
                                              .addMockedMethod("storeFile")
                                              .createMock(mocksControl);
    mockFileStoragePK = partialMockBuilder(FileStoragePK.class)
                                          .addMockedMethod("storeFile")
                                          .createMock(mocksControl);

    ReflectionTestUtils.setField(fileManager, ClientSharedIdentifiers.COMPONENT_FILE_DAO,
                                 mockFileDAO);
    ReflectionTestUtils.setField(fileManager, ClientSharedIdentifiers.COMPONENT_FILE_STORAGE_CELLML,
                                 mockFileStorageCellML);
    ReflectionTestUtils.setField(fileManager, ClientSharedIdentifiers.COMPONENT_FILE_STORAGE_PK,
                                 mockFileStoragePK);
  }

  @Test
  public void testRetrieveAllFiles() {
    final List<PortalFile> dummyPortalFiles = new ArrayList<PortalFile>();
    expect(mockFileDAO.findPortalFiles()).andReturn(dummyPortalFiles);

    mocksControl.replay();

    final List<PortalFile> retrieved = fileManager.retrieveAllFiles();

    mocksControl.verify();

    assertSame(dummyPortalFiles, retrieved);
  }

  @Test
  public void testStoreFile() {
    /*
     * Test PK file store.
     */
    final InputStream mockInputStream = mocksControl.createMock(InputStream.class);
    final String dummyName = "dummyName";
    final String dummyUploader = "dummyUploader";
    final FileStoreActionOutcomeVO mockOutcome = mocksControl.createMock(FileStoreActionOutcomeVO.class);
    expect(mockFileStoragePK.storeFile(mockInputStream, dummyName,
                                       dummyUploader))
          .andReturn(mockOutcome);

    mocksControl.replay();

    FileStoreActionOutcomeVO outcome = fileManager.storeFile(FILE_DATA_TYPE.PK,
                                                             mockInputStream,
                                                             dummyName,
                                                             dummyUploader);

    mocksControl.verify();

    assertSame(mockOutcome, outcome);

    mocksControl.reset();

    /*
     * Test CellML file store.
     */
    expect(mockFileStorageCellML.storeFile(mockInputStream, dummyName,
                                           dummyUploader))
          .andReturn(mockOutcome);

    mocksControl.replay();

    outcome = fileManager.storeFile(FILE_DATA_TYPE.CELLML, mockInputStream,
                                    dummyName, dummyUploader);

    mocksControl.verify();

    assertSame(mockOutcome, outcome);
  }
}