/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.controller;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.newCapture;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.util.Locale;

import org.easymock.Capture;
import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.mail.MailException;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.Model;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.controller.util.ControllerUtil;
import uk.ac.ox.cs.compbio.client_shared.service.ClientSharedService;

/**
 * Unit test the Register controller.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest( { ControllerUtil.class, Login.class } )
public class RegisterTest {

  private ClientSharedService mockClientSharedService;
  private IMocksControl mocksControl;
  private Locale dummyLocale;
  private MessageSource mockMessageSource;
  private Model mockModel;
  private Register register;
  private String dummyRecaptchaPublicKey;
  private String dummyRecaptchaPrivateKey;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    register = new Register();
    mockClientSharedService = mocksControl.createMock(ClientSharedService.class);
    mockMessageSource = mocksControl.createMock(MessageSource.class);
    mockModel = mocksControl.createMock(Model.class);
    ReflectionTestUtils.setField(register, "messageSource", mockMessageSource);
    ReflectionTestUtils.setField(register,
                                 ClientSharedIdentifiers.COMPONENT_CLIENT_SHARED_SERVICE,
                                 mockClientSharedService);
    dummyRecaptchaPublicKey = "dummyRecaptchaPublicKey";
    dummyRecaptchaPrivateKey = "dummyRecaptchaPrivateKey";
    ReflectionTestUtils.setField(register, "recaptchaPublicKey",
                                 dummyRecaptchaPublicKey);
    ReflectionTestUtils.setField(register, "recaptchaPrivateKey",
                                 dummyRecaptchaPrivateKey);
    dummyLocale = Locale.getDefault();
  }

  @Test
  public void testProcessRegistration() {
    /*
     * Scenario 1 - Invalid email address.
     */
    String dummyEmailAddress = "dummyEmailAddress";
    String dummyRecaptchaResponseField = "dummyRecaptchaResponseField";
    mockStatic(ControllerUtil.class);
    String dummyValidatedEmailAddress = null;
    expect(ControllerUtil.validEmailAddress(dummyEmailAddress)).andReturn(dummyValidatedEmailAddress);
    // > populateForm(Model, Locale, String, String)
    // anyTimes() used as no order of test execution and message queue system may have been populated.
    Capture<String> captureMessageKey = newCapture();
    Capture<Object[]> captureArgs = newCapture();
    Capture<Locale> captureLocale = newCapture();
    expect(mockMessageSource.getMessage(capture(captureMessageKey),
                                        capture(captureArgs), 
                                        capture(captureLocale)))
          .andThrow(new NoSuchMessageException(null)).anyTimes();
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_REGISTRATION_ERROR,
                                  ClientSharedIdentifiers.BUNDLE_ID_INVALID_EMAIL_ADDR))
          .andReturn(mockModel);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_EMAIL_ADDRESS,
                                  dummyEmailAddress))
          .andReturn(mockModel);
    // < populateForm
    boolean dummyIsMailerConfigured = false;
    expect(mockClientSharedService.isMailerConfigured()).andReturn(dummyIsMailerConfigured);
    mockStatic(Login.class);
    Login.setModelPropertiesForLogin(mockModel, dummyRecaptchaPublicKey, dummyIsMailerConfigured);

    replayAll();
    mocksControl.replay();

    String outcome = register.processRegistration(dummyRecaptchaResponseField, dummyEmailAddress,
                                                  dummyLocale, mockModel);

    verifyAll();
    mocksControl.verify();

    assertNull(captureArgs.getValue());
    assertSame(dummyLocale, captureLocale.getValue());
    assertEquals(ClientSharedIdentifiers.PAGE_LOGIN, outcome);

    resetAll();
    mocksControl.reset();

    /*
     * Scenario 2 - Valid email, no public recaptcha key, mail send failure
     */
    ReflectionTestUtils.setField(register, "recaptchaPublicKey", null);
    dummyValidatedEmailAddress = "dummyValidatedEmailAddress";
    expect(ControllerUtil.validEmailAddress(dummyEmailAddress)).andReturn(dummyValidatedEmailAddress);
    mockClientSharedService.sendRegistrationEmail(dummyValidatedEmailAddress);
    expectLastCall().andThrow(new MailException("") { });
    // > populateForm(Model, Locale, String, String)
    // anyTimes() used as no order of test execution and message queue system may have been populated.
    expect(mockMessageSource.getMessage(capture(captureMessageKey), capture(captureArgs), 
                                        capture(captureLocale)))
          .andThrow(new NoSuchMessageException(null)).anyTimes();
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_REGISTRATION_ERROR,
                                  ClientSharedIdentifiers.BUNDLE_ID_REGN_FAIL))
          .andReturn(mockModel);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_EMAIL_ADDRESS,
                                  dummyValidatedEmailAddress))
          .andReturn(mockModel);
    // < populateForm
    expect(mockClientSharedService.isMailerConfigured()).andReturn(dummyIsMailerConfigured);
    Login.setModelPropertiesForLogin(mockModel, null, dummyIsMailerConfigured);

    replayAll();
    mocksControl.replay();

    outcome = register.processRegistration(dummyRecaptchaResponseField, dummyEmailAddress,
                                           dummyLocale, mockModel);

    verifyAll();
    mocksControl.verify();

    resetAll();
    mocksControl.reset();

    /*
     * Scenario 3 - Valid email, no public recaptcha key, mail sent ok.
     */
    ReflectionTestUtils.setField(register, "recaptchaPublicKey", null);
    dummyValidatedEmailAddress = "dummyValidatedEmailAddress";
    expect(ControllerUtil.validEmailAddress(dummyEmailAddress)).andReturn(dummyValidatedEmailAddress);
    mockClientSharedService.sendRegistrationEmail(dummyValidatedEmailAddress);
    expect(mockClientSharedService.isMailerConfigured()).andReturn(dummyIsMailerConfigured);
    Login.setModelPropertiesForLogin(mockModel, null, dummyIsMailerConfigured);

    replayAll();
    mocksControl.replay();

    outcome = register.processRegistration(dummyRecaptchaResponseField, dummyEmailAddress,
                                           dummyLocale, mockModel);

    verifyAll();
    mocksControl.verify();

    resetAll();
    mocksControl.reset();

    /*
     * Scenario 4 - Valid email, public recaptcha key, recaptcha check error
     */
    ReflectionTestUtils.setField(register, "recaptchaPublicKey", dummyRecaptchaPublicKey);
    expect(ControllerUtil.validEmailAddress(dummyEmailAddress)).andReturn(dummyValidatedEmailAddress);
    String dummyErrorMessageBundleId = "dummyErrorMessageBundleId";
    expect(ControllerUtil.callRecaptcha(dummyRecaptchaPrivateKey, dummyRecaptchaResponseField))
          .andReturn(dummyErrorMessageBundleId);
    // > populateForm(Model, Locale, String, String)
    // anyTimes() used as no order of test execution and message queue system may have been populated.
    expect(mockMessageSource.getMessage(capture(captureMessageKey), capture(captureArgs), 
                                        capture(captureLocale)))
          .andThrow(new NoSuchMessageException(null)).anyTimes();
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_REGISTRATION_ERROR,
                                  dummyErrorMessageBundleId))
          .andReturn(mockModel);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_EMAIL_ADDRESS,
                                  dummyEmailAddress))
          .andReturn(mockModel);
    // < populateForm
    dummyIsMailerConfigured = false;
    expect(mockClientSharedService.isMailerConfigured()).andReturn(dummyIsMailerConfigured);
    Login.setModelPropertiesForLogin(mockModel, dummyRecaptchaPublicKey, dummyIsMailerConfigured);

    replayAll();
    mocksControl.replay();

    outcome = register.processRegistration(dummyRecaptchaResponseField, dummyEmailAddress,
                                           dummyLocale, mockModel);

    verifyAll();
    mocksControl.verify();

    resetAll();
    mocksControl.reset();

    /*
     * Scenario 5 - Valid email, public recaptcha key, successful recaptcha check, mail send failure
     */
    expect(ControllerUtil.validEmailAddress(dummyEmailAddress)).andReturn(dummyValidatedEmailAddress);
    dummyErrorMessageBundleId = null;
    expect(ControllerUtil.callRecaptcha(dummyRecaptchaPrivateKey, dummyRecaptchaResponseField))
          .andReturn(dummyErrorMessageBundleId);
    mockClientSharedService.sendRegistrationEmail(dummyValidatedEmailAddress);
    expectLastCall().andThrow(new MailException("") {});
    // > populateForm(Model, Locale, String, String)
    // anyTimes() used as no order of test execution and message queue system may have been populated.
    expect(mockMessageSource.getMessage(capture(captureMessageKey),
                                        capture(captureArgs), 
                                        capture(captureLocale)))
          .andThrow(new NoSuchMessageException(null)).anyTimes();
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_REGISTRATION_ERROR,
                                  ClientSharedIdentifiers.BUNDLE_ID_REGN_FAIL))
          .andReturn(mockModel);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_EMAIL_ADDRESS,
                                  dummyValidatedEmailAddress))
          .andReturn(mockModel);
    // < populateForm
    expect(mockClientSharedService.isMailerConfigured()).andReturn(dummyIsMailerConfigured);
    Login.setModelPropertiesForLogin(mockModel, dummyRecaptchaPublicKey, dummyIsMailerConfigured);

    replayAll();
    mocksControl.replay();

    outcome = register.processRegistration(dummyRecaptchaResponseField, dummyEmailAddress,
                                           dummyLocale, mockModel);

    verifyAll();
    mocksControl.verify();

    resetAll();
    mocksControl.reset();

    /*
     * Scenario 6 - Valid email, public recaptcha key, successful recaptcha check, mail sent ok
     */
    expect(ControllerUtil.validEmailAddress(dummyEmailAddress)).andReturn(dummyValidatedEmailAddress);
    dummyErrorMessageBundleId = null;
    expect(ControllerUtil.callRecaptcha(dummyRecaptchaPrivateKey, dummyRecaptchaResponseField))
          .andReturn(dummyErrorMessageBundleId);
    mockClientSharedService.sendRegistrationEmail(dummyValidatedEmailAddress);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_REGISTRATION_SUCCESS,
                                  Register.success))
          .andReturn(mockModel);
    expect(mockClientSharedService.isMailerConfigured()).andReturn(dummyIsMailerConfigured);
    Login.setModelPropertiesForLogin(mockModel, dummyRecaptchaPublicKey, dummyIsMailerConfigured);

    replayAll();
    mocksControl.replay();

    outcome = register.processRegistration(dummyRecaptchaResponseField, dummyEmailAddress,
                                           dummyLocale, mockModel);

    verifyAll();
    mocksControl.verify();
  }

  @Test
  public void testRedirectToLogin() {
    boolean dummyIsMailerConfigured = false;
    expect(mockClientSharedService.isMailerConfigured()).andReturn(dummyIsMailerConfigured);
    mockStatic(Login.class);
    Login.setModelPropertiesForLogin(mockModel, dummyRecaptchaPublicKey, dummyIsMailerConfigured);

    replayAll();
    mocksControl.replay();

    String outcome = register.redirectToLogin(mockModel);

    verifyAll();
    mocksControl.verify();

    assertEquals(ClientSharedIdentifiers.PAGE_LOGIN, outcome);
  }
}