/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.controller.util;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.easymock.PowerMock.createMock;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.resetAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import uk.ac.ox.cs.compbio.client_shared.value.security.Role;

/**
 * Unit test the security utility class.
 *
 * @author geoff
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.security.auth.Subject")
@PrepareForTest( { SecurityContextHolder.class, SimpleGrantedAuthority.class } )
public class SecurityUtilTest {

  private Authentication mockAuthentication;
  private IMocksControl mocksControl;
  private SecurityContext mockSecurityContext;

  @Before
  public void setUp() {
    mocksControl = createStrictControl();
    mockAuthentication = mocksControl.createMock(Authentication.class);
    mockSecurityContext = mocksControl.createMock(SecurityContext.class);
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  @Test
  public void testHasRoles() {
    boolean hasRoles = SecurityUtil.hasRoles(null);

    assertFalse(hasRoles);

    /*
     * No security context.
     */
    final Set<Role> dummyRoles = new HashSet<Role>();

    mockStatic(SecurityContextHolder.class);
    expect(SecurityContextHolder.getContext()).andReturn(null);

    replayAll();

    hasRoles = SecurityUtil.hasRoles(dummyRoles);

    verifyAll();

    assertFalse(hasRoles);

    resetAll();

    /*
     * Security context, but no authentication.
     */
    expect(SecurityContextHolder.getContext()).andReturn(mockSecurityContext);
    expect(mockSecurityContext.getAuthentication()).andReturn(null);

    replayAll();
    mocksControl.replay();

    hasRoles = SecurityUtil.hasRoles(dummyRoles);

    verifyAll();
    mocksControl.verify();

    assertFalse(hasRoles);

    resetAll();
    mocksControl.reset();

    /*
     * Security context and authentication, user has roles.
     */
    dummyRoles.add(Role.ROLE_USER);
    dummyRoles.add(Role.ROLE_ADMIN);

    final Collection dummyAuthorities = new ArrayList<GrantedAuthority>();
    final SimpleGrantedAuthority mockSimpleGrantedAuthority1 = createMock(SimpleGrantedAuthority.class);
    final SimpleGrantedAuthority mockSimpleGrantedAuthority2 = createMock(SimpleGrantedAuthority.class);
    final SimpleGrantedAuthority mockSimpleGrantedAuthority3 = createMock(SimpleGrantedAuthority.class);
    dummyAuthorities.add(mockSimpleGrantedAuthority1);
    dummyAuthorities.add(mockSimpleGrantedAuthority2);
    dummyAuthorities.add(mockSimpleGrantedAuthority3);
    expect(SecurityContextHolder.getContext()).andReturn(mockSecurityContext);
    expect(mockSecurityContext.getAuthentication())
          .andReturn(mockAuthentication);
    expect(mockAuthentication.getAuthorities()).andReturn(dummyAuthorities);
    final String dummyRole1 = Role.ROLE_ADMIN.toString();
    final String dummyRole2 = Role.ROLE_USER.toString();
    final String dummyRole3 = "dummyRole3";
    expect(mockSimpleGrantedAuthority1.getAuthority()).andReturn(dummyRole1);
    expect(mockSimpleGrantedAuthority2.getAuthority()).andReturn(dummyRole2);
    expect(mockSimpleGrantedAuthority3.getAuthority()).andReturn(dummyRole3);

    replayAll();
    mocksControl.replay();

    hasRoles = SecurityUtil.hasRoles(dummyRoles);

    verifyAll();
    mocksControl.verify();

    assertTrue(hasRoles);
    resetAll();
    mocksControl.reset();

    /*
     * Security context and authentication, user doesn't have roles.
     */
    dummyRoles.clear();
    dummyRoles.add(Role.ROLE_USER);
    dummyRoles.add(Role.ROLE_ADMIN);
    dummyRoles.add(Role.ROLE_POWER_USER);

    expect(SecurityContextHolder.getContext()).andReturn(mockSecurityContext);
    expect(mockSecurityContext.getAuthentication())
          .andReturn(mockAuthentication);
    expect(mockAuthentication.getAuthorities()).andReturn(dummyAuthorities);
    expect(mockSimpleGrantedAuthority1.getAuthority()).andReturn(dummyRole1);
    expect(mockSimpleGrantedAuthority2.getAuthority()).andReturn(dummyRole2);
    expect(mockSimpleGrantedAuthority3.getAuthority()).andReturn(dummyRole3);

    replayAll();
    mocksControl.replay();

    hasRoles = SecurityUtil.hasRoles(dummyRoles);

    verifyAll();
    mocksControl.verify();

    assertFalse(hasRoles);
  }
}