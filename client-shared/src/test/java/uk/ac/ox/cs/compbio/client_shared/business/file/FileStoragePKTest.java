/*

  Copyright (c) 2018, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.business.file;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit test the PK file storage.
 *
 * @author geoff
 */
public class FileStoragePKTest {

  private FileStorage fileStoragePK;

  private static final int arbitraryMaxProblemCount = 5;

  @Before
  public void setUp() {
    fileStoragePK = new FileStoragePK();
  }

  @Test
  public void testContentCheck() throws IOException {
    /*
     * No content scenario.
     */
    String[] dummyValues = new String[] { };
    String dummyString = StringUtils.join(Arrays.asList(dummyValues), "\t");

    InputStream dummyInputStream = new ByteArrayInputStream(dummyString.getBytes(StandardCharsets.UTF_8));

    List<String> problems = fileStoragePK.contentCheck(dummyInputStream);

    assertSame(1, problems.size());
    assertEquals("Empty file content encountered", problems.get(0));

    /*
     * Only a single numeric value in one column scenario.
     */
    dummyValues = new String[] { "1" };
    dummyString = StringUtils.join(Arrays.asList(dummyValues), "\t");

    dummyInputStream = new ByteArrayInputStream(dummyString.getBytes(StandardCharsets.UTF_8));

    problems = fileStoragePK.contentCheck(dummyInputStream);

    assertSame(1, problems.size());
    assertEquals("missing values at line '1'", problems.get(0));

    /*
     * Two numeric values in separate columns scenario.
     */
    dummyValues = new String[] { "1", "2" };
    dummyString = StringUtils.join(Arrays.asList(dummyValues), "\t");

    dummyInputStream = new ByteArrayInputStream(dummyString.getBytes(StandardCharsets.UTF_8));

    problems = fileStoragePK.contentCheck(dummyInputStream);

    assertSame(0, problems.size());

    /*
     * Two values (one a non-numeric) in separate columns scenario.
     */
    dummyValues = new String[] { "1", "2fish" };
    dummyString = StringUtils.join(Arrays.asList(dummyValues), "\t");

    dummyInputStream = new ByteArrayInputStream(dummyString.getBytes(StandardCharsets.UTF_8));

    problems = fileStoragePK.contentCheck(dummyInputStream);

    assertSame(1, problems.size());
    assertEquals("non-numeric content at line '1', col '2'", problems.get(0));

    /*
     * 31 numeric values in separate columns scenario.
     */
    dummyValues = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
                                 "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
                                 "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
                                 "31" };
    dummyString = StringUtils.join(Arrays.asList(dummyValues), "\t");

    dummyInputStream = new ByteArrayInputStream(dummyString.getBytes(StandardCharsets.UTF_8));

    problems = fileStoragePK.contentCheck(dummyInputStream);

    assertSame(0, problems.size());

    /*
     * 32 numeric values in separate columns scenario.
     */
    dummyValues = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
                                 "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
                                 "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
                                 "31", "32" };
    dummyString = StringUtils.join(Arrays.asList(dummyValues), "\t");

    dummyInputStream = new ByteArrayInputStream(dummyString.getBytes(StandardCharsets.UTF_8));

    problems = fileStoragePK.contentCheck(dummyInputStream);

    assertSame(1, problems.size());
    assertEquals("too many concentrations (max. 30) at line '1'", problems.get(0));

    /*
     * More than one line, some with problems.
     */
    List<String> lines = new ArrayList<String>();
    dummyValues = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
                                 "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
                                 "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
                                 "31" };
    dummyString = StringUtils.join(Arrays.asList(dummyValues), "\t");
    lines.add(dummyString);
    dummyValues = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10",
                                 "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
                                 "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
                                 "31", "32" };
    dummyString = StringUtils.join(Arrays.asList(dummyValues), "\t");
    lines.add(dummyString);
    lines.add(dummyString);
    dummyString = StringUtils.join(lines, "\n");

    dummyInputStream = new ByteArrayInputStream(dummyString.getBytes(StandardCharsets.UTF_8));

    problems = fileStoragePK.contentCheck(dummyInputStream);

    assertSame(2, problems.size());
    assertEquals("too many concentrations (max. 30) at line '2'", problems.get(0));
    assertEquals("too many concentrations (max. 30) at line '3'", problems.get(1));

    /*
     * Test against the arbitrary max problems count.
     */
    lines.clear();

    dummyValues = new String[] { "1", "2", "3", "4", "5", "fish" };
    dummyString = StringUtils.join(Arrays.asList(dummyValues), "\t");
    lines.add(dummyString);
    lines.add(dummyString);
    lines.add(dummyString);
    lines.add(dummyString);
    lines.add(dummyString);
    lines.add(dummyString);
    lines.add(dummyString);
    dummyString = StringUtils.join(lines, "\n");

    dummyInputStream = new ByteArrayInputStream(dummyString.getBytes(StandardCharsets.UTF_8));

    problems = fileStoragePK.contentCheck(dummyInputStream);

    assertSame(arbitraryMaxProblemCount + 1, problems.size());
  }

  @Test
  public void testWriteToDatabase() {
    assertTrue(fileStoragePK.writeToDatabase());
  }
}