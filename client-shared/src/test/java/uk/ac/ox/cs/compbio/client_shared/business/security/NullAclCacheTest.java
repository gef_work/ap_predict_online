/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.business.security;

import static org.easymock.EasyMock.createStrictControl;
import static org.junit.Assert.assertNull;

import java.io.Serializable;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.acls.model.MutableAcl;
import org.springframework.security.acls.model.ObjectIdentity;

/**
 * Unit test the null ACL cache.
 * 
 * @author geoff
 */
public class NullAclCacheTest {

  private IMocksControl mocksControl;
  private MutableAcl mockMutableAcl;
  private NullAclCache nullAclCache;
  private ObjectIdentity mockObjectIdentity;
  private Serializable mockSerializable;

  @Before
  public void setUp() {
    nullAclCache = new NullAclCache();

    mocksControl = createStrictControl();

    mockMutableAcl = mocksControl.createMock(MutableAcl.class);
    mockObjectIdentity = mocksControl.createMock(ObjectIdentity.class);
    mockSerializable = mocksControl.createMock(Serializable.class);
  }

  @Test
  public void testClearCache() {
    nullAclCache.clearCache();
  }

  @Test
  public void testEvictObjectIdentityFromCache() {
    mocksControl.replay();

    nullAclCache.evictFromCache(mockObjectIdentity);

    mocksControl.verify();
  }

  @Test
  public void testEvictSerializableFromCache() {
    mocksControl.replay();

    nullAclCache.evictFromCache(mockSerializable);

    mocksControl.verify();
  }

  @Test
  public void testObjectIdentityGetFromCache() {
    mocksControl.replay();

    assertNull(nullAclCache.getFromCache(mockObjectIdentity));

    mocksControl.verify();
  }

  @Test
  public void testGetSerializableFromCache() {
    mocksControl.replay();

    assertNull(nullAclCache.getFromCache(mockSerializable));

    mocksControl.verify();
  }

  @Test
  public void testPutInCache() {
    mocksControl.replay();

    nullAclCache.putInCache(mockMutableAcl);

    mocksControl.verify();
  }
}