/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.controller;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.Model;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.service.ClientSharedService;

/**
 * Unit test the Login controller.
 * 
 * @author geoff
 */
public class LoginTest {

  private ClientSharedService mockClientSharedService;
  private IMocksControl mocksControl;
  private Login loginController;
  private Model mockModel;

  @Before
  public void setUp() {
    loginController = new Login();

    mocksControl = createStrictControl();
    mockClientSharedService = mocksControl.createMock(ClientSharedService.class);
    ReflectionTestUtils.setField(loginController,
                                 ClientSharedIdentifiers.COMPONENT_CLIENT_SHARED_SERVICE,
                                 mockClientSharedService);

    mockModel = mocksControl.createMock(Model.class);
  }

  @Test
  public void testShowLogin() {
    final boolean dummyIsMailerConfigured = true;
    expect(mockClientSharedService.isMailerConfigured())
          .andReturn(dummyIsMailerConfigured);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_PUBLIC_RECAPTCHA_KEY,
                                  null)).andReturn(mockModel);
    expect(mockModel.addAttribute(ClientSharedIdentifiers.MODEL_ATTRIBUTE_MAILER_CONFIGURED,
                                  dummyIsMailerConfigured)).andReturn(mockModel);
    replay(mockModel);
    assertEquals(ClientSharedIdentifiers.PAGE_LOGIN, loginController.showLogin(mockModel));
    verify(mockModel);
  }
}