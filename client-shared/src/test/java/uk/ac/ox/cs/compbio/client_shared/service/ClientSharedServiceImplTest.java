/*

  Copyright (c) 2017, University of Oxford.
  All rights reserved.

  University of Oxford means the Chancellor, Masters and Scholars of the
  University of Oxford, having an administrative office at Wellington
  Square, Oxford OX1 2JD, UK.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of the University of Oxford nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
package uk.ac.ox.cs.compbio.client_shared.service;

import static org.easymock.EasyMock.createStrictControl;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.easymock.IMocksControl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import uk.ac.ox.cs.compbio.client_shared.ClientSharedIdentifiers;
import uk.ac.ox.cs.compbio.client_shared.api.exception.DownstreamCommunicationException;
import uk.ac.ox.cs.compbio.client_shared.api.manager.ClientSharedManager;
import uk.ac.ox.cs.compbio.client_shared.api.manager.MailManager;
import uk.ac.ox.cs.compbio.client_shared.api.manager.MailManager.EMAIL_TYPE;

/**
 * Unit test the client (direct) service interface implementation.
 *
 * @author geoff
 */
public class ClientSharedServiceImplTest {

  private ClientSharedManager mockClientSharedManager;
  private ClientSharedService clientSharedService;
  private IMocksControl mocksControl;
  private MailManager mockMailManager;

  @Before
  public void setUp() {
    clientSharedService = new ClientSharedServiceImpl();

    mocksControl = createStrictControl();

    mockClientSharedManager = mocksControl.createMock(ClientSharedManager.class);
    mockMailManager = mocksControl.createMock(MailManager.class);
    ReflectionTestUtils.setField(clientSharedService,
                                 ClientSharedIdentifiers.COMPONENT_CLIENT_SHARED_MANAGER,
                                 mockClientSharedManager);
    ReflectionTestUtils.setField(clientSharedService,
                                 ClientSharedIdentifiers.COMPONENT_MAIL_MANAGER,
                                 mockMailManager);
  }

  @Test
  public void testIsMailerConfigured() {
    boolean dummyIsMailerConigured = false;
    expect(mockMailManager.isMailerConfigured())
          .andReturn(dummyIsMailerConigured);

    mocksControl.replay();

    boolean isMailerConfigured = clientSharedService.isMailerConfigured();

    mocksControl.verify();

    assertFalse(isMailerConfigured);

    mocksControl.reset();

    dummyIsMailerConigured = true;
    expect(mockMailManager.isMailerConfigured()).andReturn(dummyIsMailerConigured);

    mocksControl.replay();

    isMailerConfigured = clientSharedService.isMailerConfigured();

    mocksControl.verify();

    assertTrue(isMailerConfigured);
  }

  @Test
  public void testRetrieveAppManagerWorkload() throws DownstreamCommunicationException {
    final String dummyWorkload = "dummyWorkload";

    expect(mockClientSharedManager.retrieveAppManagerWorkload())
          .andReturn(dummyWorkload);

    mocksControl.replay();

    final String workload = clientSharedService.retrieveAppManagerWorkload();

    mocksControl.verify();

    assertSame(dummyWorkload, workload);
  }

  @Test
  public void testSendRegistrationEmail() {
    final String dummyEmailAddress = "dummyEmailAddress";
    mockMailManager.sendEmail(dummyEmailAddress, EMAIL_TYPE.REGISTRATION);

    mocksControl.replay();

    clientSharedService.sendRegistrationEmail(dummyEmailAddress);

    mocksControl.verify();
  }
}